﻿Imports System.Xml
Imports System.Xml.Serialization
Imports APDWCFServiceClient.APDFoundation
Imports APDWCFServiceClient.APDFoundationLocal
Imports APDWCFServiceClient.APDFoundationDirectLocal
Imports System.IO

Public Class _Default
    Inherits System.Web.UI.Page
    'Dim APDFoundation As New APDFoundation.APDService
    'Dim wsAPDClaim As New APDFoundation.APDClaim
    'Dim wsAPDVehicle As APDFoundation.APDVehicle

    '---------------------------------------------
    ' This WS connects to the DEV front end WS and 
    ' processes to the backend DEV WS Foundation
    '---------------------------------------------
    Dim APDFoundation As New APDClaimPointFoundationDEV.APDService

    '---------------------------------------------
    ' This WS connects to the Stage front end WS and 
    ' processes to the backend WS Foundation
    '---------------------------------------------
    'Dim APDFoundation As New APDClaimPointFoundation.APDService

    '---------------------------------------------
    ' This WS connects to the PRODUCTION front end WS and 
    ' processes to the backend PRODUCTION WS Foundation
    '---------------------------------------------
    'Dim APDFoundation As New APDClaimPointFoundationPROD.APDService

    '---------------------------------------------
    ' This WS connects to the back end DEV WS and 
    ' processes without the front end
    '---------------------------------------------
    'Dim APDFoundation As New APDFoundation.APDService

    '---------------------------------------------
    ' This WS connects directly to Local DotNet 
    ' running debug session 
    '---------------------------------------------
    'Dim APDFoundation As New APDFoundationLocal.APDService

    '---------------------------------------------
    ' This WS connects directly to Stage backend
    ' WS Foundation
    '---------------------------------------------
    'Dim APDFoundation As New APDFoundationDirectLocal.APDService

    'Dim wsAPDClaim As New APDFoundationLocal.APDClaim
    'Dim wsAPDVehicle As APDFoundationLocal.APDVehicle

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim XMLInput As New XmlDocument
        Dim sLynxID As String = ""

        'XMLInput.Load("c:\temp\QBEWSClaim13Stage.xml")
        'XMLInput.Load("c:\temp\QBEWSClaim13AddVehicle.xml")
        'XMLInput.Load("c:\temp\QBEWSClaim13MinData.xml")
        'XMLInput.Load("c:\temp\QBEWSClaim13MinDataMultiVehicle.xml")
        'XMLInput.Load("c:\temp\QBEWSClaim13.xml")

        '------------------------------------------
        ' Dev Test Cases - Carrier Rep validate/create
        '------------------------------------------
        '---- Good DEV PS Claim ---
        XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_DEV_1st_Party_MASTER.xml") '-- (GOOD - Tested 08Jul2014)

        '---- Misc ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Westfield_PS_DEV_Request-MinimalVehicleData.xml") '-- (GOOD - Tested 24Oct2013)
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Real_ClaimPoint_PS_Claim.xml") '-- 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP.xml")  ' -- GOOD Claim Process --
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_Diff_Ins_Inv.xml")  ' -- GOOD Claim Process Different Insured vs Involved --
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_DEV_NoRep.xml")  ' -- No CarrierRepID passed, but CarrierRepEmail is good --
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_DEV_CreateNewCarrierRep.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_MultiVehicle.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_MultiVehicle_two_first_party.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_MultiVehicle_Exact_Claimpoint.xml")

        '---- Process a single vehicle claim and then a vehicle add ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_SingleVehicle.xml")  '-- Create new Claim Rep with Office

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_SingleVehicle.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_AddVehicle_Exp3.xml")  '-- Vehicle Add Exp 3 Good
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_DEV_AddVehicle_Exp1.xml")  '-- Vehicle Add Exp 3 Good
        '---- Rental Testcase ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_Request_With_Rental.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_UM_Request_With_Rental.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_UIM_Request_With_Rental.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_COMP_Request_With_Rental.xml")  '-- Claim Rep via RepID
        '---- Create new Claim Rep with Office/Claimpoint Add ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_DEV_Request_With_New_Claim_Rep.xml")  '-- Create new Claim Rep with Office

        '------------------------------------------
        ' Stage Test Cases - Carrier Rep validate/create
        '------------------------------------------
        ' === QBE RRP ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Westfield_PS_STAGE_Request-MinimalVehicleData.xml") '-- (GOOD - Tested 24Oct2013)

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_STAGE_Request.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_Request-MinimalVehicleData.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_Request-MinimalVehicleData_ClaimRep_via_Email.xml")  '-- Claim Rep via Email
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_Request-MinimalVehicleData_ClaimRep_New.xml")  '-- Claim Rep doesn't exist, New create
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_RRP_STAGE_Request_Multi_Vehicle.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_MultiVehicle_Exact_Claimpoint.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadXML_13Mar2013.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadXML_13Mar2013_Orig.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\PayloadWestfield_25Jul2013.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload20130319.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload20130319x.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Test100674Ncc1212.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_TestKen002.xml")
        '---- Process a single vehicle claim and then a vehicle add ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_QBE_RRP_STAGE_SingleVehicle.xml")  '-- Claim Rep via RepID
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_02Apr2013.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload20130404.xml")
        '---- Carrier Rep Testing ----
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_New_Rep3.xml")  '-- Create new 1st party new carrier rep.  Comp/
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_New_Rep4.xml")  '-- Veh Add 3rd party same carrier rep as above.  Liab/
        '---- Below errors 
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_New_Rep1.xml")  '-- Create new 3rd party new carrier rep.  Liab/1756447
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_New_Rep2.xml")  '-- Veh Add 1st party same carrier rep as above.  Comp/1756447

        ' === Misc ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\ClaimpointUsingWS.xml")

        ' === Electric DRP ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_17Apr2013.xml") '-- (GOOD - Tested 24Oct2013) 1st party

        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_STAGE_Request.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_DRP_STAGE_Request_2_Vehicles.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\LivePayload_08Apr2013A.xml")
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_ShopError.xml") '-- Correcting shop key constraints
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\WSClaim_Electric_Stage_3rd_Party_First.xml") '--3rd party 1st create insured involved.
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_ServChanError.xml") '--Service Channel Error.
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_ServChanErrorA.xml") '--Service Channel Error.
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_Error.xml") '--Multi 1st party???
        '--------------------------
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_3rdAdd_Party_17Apr2013.xml") '--3rd party
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Payload_Electric_Stage_1st_Party_NewRep.xml") '--1st party NewRep

        ' === ClaimPoint Direct WS Claims ===
        'XMLInput.Load("C:\Users\GLSD347\Documents\Web Service Assignments\Real_ClaimPoint_PS_Claim.xml")

        'XMLInput.Load("c:\temp\NugenPayload1.xml")

        'Response.Write(APDFoundation.Ping())
        sLynxID = APDFoundation.CreateAPDClaim(XMLInput.InnerXml)
        'sLynxID = APDFoundation.CreateAPDClaim("x")
        'Response.Write(APDFoundation.LogEvent("TVD1", "TVD1", "TVD1", "TVD1", "TVD1"))

        Response.Write(Server.HtmlEncode(sLynxID))
        'Response.Write(sLynxID)




        'Dim validationEventHandler As Schema.ValidationEventHandler
        'Dim sLynxID As String = ""

        ''-----------------------------------------
        '' Build Claim
        ''-----------------------------------------
        'wsAPDClaim.NewClaimFlag = "1"
        'wsAPDClaim.NoticeMethodID = "5"

        'wsAPDClaim.DataSource = "Claim Point Web Assignment"
        'wsAPDClaim.FNOLUserID = "5850"
        'wsAPDClaim.InsuranceCompanyID = "304"
        'wsAPDClaim.AssignmentAtSelectionFlag = "0"
        'wsAPDClaim.DemoFlag = "0"
        'wsAPDClaim.AssignmentDescription = "Program Shop"
        'wsAPDClaim.CarrierOfficeName = ""
        'wsAPDClaim.CarrierRepNameFirst = "Tom"
        'wsAPDClaim.CarrierRepNameLast = "Duranko"
        'wsAPDClaim.CarrierRepUserID = "4642"
        'wsAPDClaim.CarrierRepPhoneDay = "412-995-0000"
        'wsAPDClaim.CarrierRepEmailAddress = "tduranko@pgwglass.com"
        'wsAPDClaim.CarrierName = "QBE Insurance"
        'wsAPDClaim.CoverageClaimNumber = "2345"
        'wsAPDClaim.LossDate = "08/01/2012"
        'wsAPDClaim.LossAddressState = "PA"
        'wsAPDClaim.CallerNameFirst = "Richard (General)"
        'wsAPDClaim.CallerNameLast = "Abrams"
        'wsAPDClaim.CallerRelationToInsuredID = "14"
        'wsAPDClaim.CallerRelationToInsuredIDDescription = "Carrier Representative"
        'wsAPDClaim.InsuredNameFirst = "sdfkjg"
        'wsAPDClaim.InsuredNameLast = "hgf"
        'wsAPDClaim.InsuredPhone = "4121111111"
        'wsAPDClaim.InsuredBusinessName = "business"
        'wsAPDClaim.CollisionDeductibleAmt = "100.00"
        'wsAPDClaim.CollisionLimitAmt = "1000.00"
        'wsAPDClaim.ComprehensiveDeductibleAmt = ""
        'wsAPDClaim.ComprehensiveLimitAmt = ""
        'wsAPDClaim.LiabilityDeductibleAmt = ""
        'wsAPDClaim.CollisionLimitAmt = ""
        'wsAPDClaim.UnderInsuredDeductibleAmt = ""
        'wsAPDClaim.UnderInsuredLimitAmt = ""
        'wsAPDClaim.CollisionDeductibleAmt = ""
        'wsAPDClaim.UnderInsuredLimitAmt = ""
        'wsAPDClaim.TimeStarted = "08/02/2012 09:04:57 AM"
        'wsAPDClaim.TimeFinished = "08/02/2012 09:32:19 AM"
        'wsAPDClaim.IntakeStartSeconds = "1343912697584"
        'wsAPDClaim.IntakeEndSeconds = "1343914339883"
        'wsAPDClaim.IntakeSeconds = "1642"
        'wsAPDClaim.InsuredName = ""
        'wsAPDClaim.InsuredPhoneSumm = "412-111-1111"
        'wsAPDClaim.RentalAuthorized = "No"
        'wsAPDClaim.RentalDays = ""
        'wsAPDClaim.RentalDayAmount = ""
        'wsAPDClaim.RentalMaxAmount = ""
        'wsAPDClaim.RentalDaysAuthorized = ""
        'wsAPDClaim.Mileage = ""
        'wsAPDClaim.LossDescription = "Test"
        'wsAPDClaim.Remarks = ""
        'wsAPDClaim.RentalInstructions = ""

        ''-----------------------------------------
        '' Build Vehicle
        ''-----------------------------------------
        'Dim wsClass As New APDVehicle
        'Dim wsData As New List(Of APDVehicle)

        'wsClass = New APDVehicle
        'wsClass.AssignmentTypeID = "4"
        'wsClass.ExposureCD = "1"
        'wsClass.CoverageProfileCD = "COLL"
        'wsClass.OwnerNameFirst = "sdfkjg"
        'wsClass.OwnerNameLast = "hgf"
        'wsClass.OwnerBusinessName = "business"
        'wsClass.ContactNameFirst = "sdfkjg"
        'wsClass.ContactNameLast = "hgf"
        'wsClass.ContactEmailAddress = ""
        'wsClass.ContactBestPhoneCD = ""
        'wsClass.VIN = "76asdchj"
        'wsClass.LicensePlateNumber = "875"
        'wsClass.VehicleYear = "2000"
        'wsClass.Make = "lkj"
        'wsClass.Model = "jhg"
        'wsClass.Drivable = "1"
        'wsClass.PrimaryDamage = "8"
        'wsClass.SecondaryDamage = "32"
        'wsClass.ImpactLocations = "8,32"
        'wsClass.ShopLocationID = "57415"
        'wsClass.ShopSearchLogID = "400561"
        'wsClass.SelectedShopRank = "1"
        'wsClass.SelectedShopScore = "100"
        'wsClass.OwnerPhone = "4121111111"
        'wsClass.ContactPhone = "4121111111"
        'wsClass.ContactNightPhone = ""
        'wsClass.ContactAltPhone = ""
        'wsClass.CellPhoneCarrier = ""
        'wsClass.ContactCellPhone = ""
        'wsClass.OwnerPhoneSumm = "412-111-1111"
        'wsClass.ContactPhoneSumm = "412-111-1111"
        'wsClass.ContactNightPhoneSumm = ""
        'wsClass.ContactAltPhoneSumm = ""
        'wsClass.ContactCellPhoneSumm = ""
        'wsClass.PrefMethodUpd = "NU"
        'wsClass.ShopName = "Wolbert Auto Body Repair"
        'wsClass.ShopAddress1 = "47 East Crafton Avenue"
        'wsClass.ShopAddress2 = ""
        'wsClass.ShopCity = "Pittsburgh"
        'wsClass.ShopState = "PA"
        'wsClass.ShopZip = "15205"
        'wsClass.ShopPhone = "412-922-8898"
        'wsClass.ShopFax = "412-922-8021"
        'wsClass.RentalAuthorized = "No"
        'wsClass.RentalDays = ""
        'wsClass.RentalDayAmount = ""
        'wsClass.RentalMaxAmount = ""
        'wsClass.RentalDaysAuthorized = ""
        'wsClass.DeductibleAmt = "100.00"
        'wsClass.LimitAmt = "1000.00"
        'wsClass.CoverageProfileUiCD = "COLL|175"
        'wsClass.PrimaryDamageDescription = "Rear Corner Passenger's Side"
        'wsClass.SecondaryDamageDescription = "Front Bumper"
        'wsClass.AssignmentTypeIDDescription = "Program Shop"
        'wsClass.Mileage = "564"
        'wsClass.ClientCoverageTypeID = "175"
        'wsClass.ClientCoverageTypeDesc = "Collision"
        'wsClass.VehicleNumber = "1"
        'wsClass.ShowScriptingMemoFlag = "0"
        'wsClass.SourceApplicationPassthruDataVeh = ""
        'wsClass.rbTypeAuthorized = "on"
        'wsClass.rbTypeEstimate = "on"
        'wsClass.ShopRemarks = ""
        'wsClass.DrivingDirections = "Take Route 60 (Steuben Street)into Crafton. Turn on Noble Avenue then right on East Crafton."
        'wsClass.RentalInstructions = ""

        'wsData.Add(wsClass)

        'wsClass = New APDVehicle
        'wsClass.AssignmentTypeID = "5"
        'wsClass.ExposureCD = "1"
        'wsClass.CoverageProfileCD = "COLL"
        'wsClass.OwnerNameFirst = "2ndsdfkjg"
        'wsClass.OwnerNameLast = "2ndhgf"
        'wsClass.OwnerBusinessName = "2ndbusiness"
        'wsClass.ContactNameFirst = "2ndsdfkjg"
        'wsClass.ContactNameLast = "2ndhgf"
        'wsClass.ContactEmailAddress = ""
        'wsClass.ContactBestPhoneCD = ""
        'wsClass.VIN = "2nd76asdchj"
        'wsClass.LicensePlateNumber = "2nd875"
        'wsClass.VehicleYear = "2005"
        'wsClass.Make = "2ndlkj"
        'wsClass.Model = "2ndjhg"
        'wsClass.Drivable = "1"
        'wsClass.PrimaryDamage = "8"
        'wsClass.SecondaryDamage = "32"
        'wsClass.ImpactLocations = "8,32"
        'wsClass.ShopLocationID = "57415"
        'wsClass.ShopSearchLogID = "400561"
        'wsClass.SelectedShopRank = "1"
        'wsClass.SelectedShopScore = "100"
        'wsClass.OwnerPhone = "4121112222"
        'wsClass.ContactPhone = "4121112222"
        'wsClass.ContactNightPhone = ""
        'wsClass.ContactAltPhone = ""
        'wsClass.CellPhoneCarrier = ""
        'wsClass.ContactCellPhone = ""
        'wsClass.OwnerPhoneSumm = "412-111-2222"
        'wsClass.ContactPhoneSumm = "412-111-2222"
        'wsClass.ContactNightPhoneSumm = ""
        'wsClass.ContactAltPhoneSumm = ""
        'wsClass.ContactCellPhoneSumm = ""
        'wsClass.PrefMethodUpd = "NU"
        ''wsClass.ShopName = "2nd Wolbert Auto Body & Repair"
        'wsClass.ShopName = "2nd Wolbert Auto Body Repair"
        'wsClass.ShopAddress1 = "2nd 47 East Crafton Avenue"
        'wsClass.ShopAddress2 = ""
        'wsClass.ShopCity = "Pittsburgh"
        'wsClass.ShopState = "PA"
        'wsClass.ShopZip = "15222"

        'wsClass.ShopPhone = "412-922-8123"
        'wsClass.ShopFax = "412-922-8123"
        'wsClass.RentalAuthorized = "No"
        'wsClass.RentalDays = ""
        'wsClass.RentalDayAmount = ""
        'wsClass.RentalMaxAmount = ""
        'wsClass.RentalDaysAuthorized = ""
        'wsClass.DeductibleAmt = "200.00"
        'wsClass.LimitAmt = "3000.00"
        'wsClass.CoverageProfileUiCD = "COLL|175"
        'wsClass.PrimaryDamageDescription = "2nd Rear Corner Passenger's Side"
        'wsClass.SecondaryDamageDescription = "2nd Front Bumper"
        'wsClass.AssignmentTypeIDDescription = "Program Shop"
        'wsClass.Mileage = "222564"
        'wsClass.ClientCoverageTypeID = "175"
        'wsClass.ClientCoverageTypeDesc = "Collision"
        'wsClass.VehicleNumber = "1"
        'wsClass.ShowScriptingMemoFlag = "0"
        'wsClass.SourceApplicationPassthruDataVeh = ""
        'wsClass.rbTypeAuthorized = "on"
        'wsClass.rbTypeEstimate = "on"
        'wsClass.ShopRemarks = ""
        'wsClass.DrivingDirections = "2nd Take Route 60 (Steuben Street)into Crafton. Turn on Noble Avenue then right on East Crafton."
        'wsClass.RentalInstructions = ""
        'wsData.Add(wsClass)

        'Dim sXML As String = ""
        'sXML = SerializeAnObject(wsData)

        'Dim XMLLynx As New XmlDocument
        'sLynxID = APDFoundation.CreateAPDClaim(wsAPDClaim, wsAPDVehicle)
        'sLynxID = APDFoundation.CreateAPDClaim(wsAPDClaim, wsData.ToArray)
        'sLynxID = APDFoundation.CreateAPDClaim(wsAPDClaim, sXML)
        'Response.Write(Server.HtmlEncode(sLynxID))


        'wsAPDVehicle.Item(0).AssignmentTypeID = "4"
        'wsAPDVehicle(0).ExposureCD = "1"
        'wsAPDVehicle(0).CoverageProfileCD = "COLL"
        'wsAPDVehicle(0).OwnerNameFirst = "sdfkjg"
        'wsAPDVehicle(0).OwnerNameLast = "hgf"
        'wsAPDVehicle(0).OwnerBusinessName = "business"
        'wsAPDVehicle(0).ContactNameFirst = "sdfkjg"
        'wsAPDVehicle(0).ContactNameLast = "hgf"
        'wsAPDVehicle(0).ContactEmailAddress = ""
        'wsAPDVehicle(0).ContactBestPhoneCD = ""
        'wsAPDVehicle(0).VIN = "76asdchj"
        'wsAPDVehicle(0).LicensePlateNumber = "875"
        'wsAPDVehicle(0).VehicleYear = "2000"
        'wsAPDVehicle(0).Make = "lkj"
        'wsAPDVehicle(0).Model = "jhg"
        'wsAPDVehicle(0).Drivable = "1"
        'wsAPDVehicle(0).PrimaryDamage = "8"
        'wsAPDVehicle(0).SecondaryDamage = "32"
        'wsAPDVehicle(0).ImpactLocations = "8,32"
        'wsAPDVehicle(0).ShopLocationID = "57415"
        'wsAPDVehicle(0).ShopSearchLogID = "400561"
        'wsAPDVehicle(0).SelectedShopRank = "1"
        'wsAPDVehicle(0).SelectedShopScore = "100"
        'wsAPDVehicle(0).OwnerPhone = "4121111111"
        'wsAPDVehicle(0).ContactPhone = "4121111111"
        'wsAPDVehicle(0).ContactNightPhone = ""
        'wsAPDVehicle(0).ContactAltPhone = ""
        'wsAPDVehicle(0).CellPhoneCarrier = ""
        'wsAPDVehicle(0).ContactCellPhone = ""
        'wsAPDVehicle(0).OwnerPhoneSumm = "412-111-1111"
        'wsAPDVehicle(0).ContactPhoneSumm = "412-111-1111"
        'wsAPDVehicle(0).ContactNightPhoneSumm = ""
        'wsAPDVehicle(0).ContactAltPhoneSumm = ""
        'wsAPDVehicle(0).ContactCellPhoneSumm = ""
        'wsAPDVehicle(0).PrefMethodUpd = "NU"
        'wsAPDVehicle(0).ShopName = "Wolbert Auto Body & Repair"
        'wsAPDVehicle(0).ShopAddress1 = "47 East Crafton Avenue"
        'wsAPDVehicle(0).ShopAddress2 = ""
        'wsAPDVehicle(0).ShopCity = "Pittsburgh"
        'wsAPDVehicle(0).ShopState = "PA"
        'wsAPDVehicle(0).ShopZip = "15205"
        'wsAPDVehicle(0).ShopPhone = "412-922-8898"
        'wsAPDVehicle(0).ShopFax = "412-922-8021"
        'wsAPDVehicle(0).RentalAuthorized = "No"
        'wsAPDVehicle(0).RentalDays = ""
        'wsAPDVehicle(0).RentalDayAmount = ""
        'wsAPDVehicle(0).RentalMaxAmount = ""
        'wsAPDVehicle(0).RentalDaysAuthorized = ""
        'wsAPDVehicle(0).DeductibleAmt = "100.00"
        'wsAPDVehicle(0).LimitAmt = "1000.00"
        'wsAPDVehicle(0).CoverageProfileUiCD = "COLL|175"
        'wsAPDVehicle(0).PrimaryDamageDescription = "Rear Corner Passenger's Side"
        'wsAPDVehicle(0).SecondaryDamageDescription = "Front Bumper"
        'wsAPDVehicle(0).AssignmentTypeIDDescription = "Program Shop"
        'wsAPDVehicle(0).Mileage = "564"
        'wsAPDVehicle(0).ClientCoverageTypeID = "175"
        'wsAPDVehicle(0).ClientCoverageTypeDesc = "Collision"
        'wsAPDVehicle(0).VehicleNumber = "1"
        'wsAPDVehicle(0).ShowScriptingMemoFlag = "0"
        'wsAPDVehicle(0).SourceApplicationPassthruDataVeh = ""
        'wsAPDVehicle(0).rbTypeAuthorized = "on"
        'wsAPDVehicle(0).rbTypeEstimate = "on"
        'wsAPDVehicle(0).ShopRemarks = ""
        'wsAPDVehicle(0).DrivingDirections = "Take Route 60 (Steuben Street)into Crafton. Turn on Noble Avenue then right on East Crafton."
        'wsAPDVehicle(0).RentalInstructions = ""


        'APDVehicle.Vehicle_AssignmentTypeID = "<AssignmentTypeID><![CDATA[4]]></AssignmentTypeID>"
        'APDVehicle.Vehicle_ExposureCD = "<ExposureCD><![CDATA[1]]></ExposureCD>"
        'APDVehicle.Vehicle_CoverageProfileCD = "<CoverageProfileCD><![CDATA[COLL]]></CoverageProfileCD>"
        'APDVehicle.Vehicle_OwnerNameFirst = "<OwnerNameFirst><![CDATA[sdfkjg]]></OwnerNameFirst>"
        'APDVehicle.Vehicle_OwnerNameLast = "<OwnerNameLast><![CDATA[hgf]]></OwnerNameLast>"
        'APDVehicle.Vehicle_OwnerBusinessName = "<OwnerBusinessName><![CDATA[business]]></OwnerBusinessName>"
        'APDVehicle.Vehicle_ContactNameFirst = "<ContactNameFirst><![CDATA[sdfkjg]]></ContactNameFirst>"
        'APDVehicle.Vehicle_ContactNameLast = "<ContactNameLast><![CDATA[hgf]]></ContactNameLast>"
        'APDVehicle.Vehicle_ContactEmailAddress = "<ContactEmailAddress><![CDATA[]]></ContactEmailAddress>"
        'APDVehicle.Vehicle_ContactBestPhoneCD = "<ContactBestPhoneCD><![CDATA[]]></ContactBestPhoneCD>"
        'APDVehicle.Vehicle_VIN = "<VIN><![CDATA[76asdchj]]></VIN>"
        'APDVehicle.Vehicle_LicensePlateNumber = "<LicensePlateNumber><![CDATA[875]]></LicensePlateNumber>"
        'APDVehicle.Vehicle_VehicleYear = "<VehicleYear><![CDATA[2000]]></VehicleYear>"
        'APDVehicle.Vehicle_Make = "<Make><![CDATA[lkj]]></Make>"
        'APDVehicle.Vehicle_Model = "<Model><![CDATA[jhg]]></Model>"
        'APDVehicle.Vehicle_Drivable = "<Drivable><![CDATA[1]]></Drivable>"
        'APDVehicle.Vehicle_PrimaryDamage = "<PrimaryDamage><![CDATA[8]]></PrimaryDamage>"
        'APDVehicle.Vehicle_SecondaryDamage = "<SecondaryDamage><![CDATA[32]]></SecondaryDamage>"
        'APDVehicle.Vehicle_ImpactLocations = "<ImpactLocations><![CDATA[8,32]]></ImpactLocations>"
        'APDVehicle.Vehicle_ShopLocationID = "<ShopLocationID><![CDATA[57415]]></ShopLocationID>"
        'APDVehicle.Vehicle_ShopSearchLogID = "<ShopSearchLogID><![CDATA[400561]]></ShopSearchLogID>"
        'APDVehicle.Vehicle_SelectedShopRank = "<SelectedShopRank><![CDATA[1]]></SelectedShopRank>"
        'APDVehicle.Vehicle_SelectedShopScore = "<SelectedShopScore><![CDATA[100]]></SelectedShopScore>"
        'APDVehicle.Vehicle_OwnerPhone = "<OwnerPhone><![CDATA[4121111111]]></OwnerPhone>"
        'APDVehicle.Vehicle_ContactPhone = "<ContactPhone><![CDATA[4121111111]]></ContactPhone>"
        'APDVehicle.Vehicle_ContactNightPhone = "<ContactNightPhone><![CDATA[]]></ContactNightPhone>"
        'APDVehicle.Vehicle_ContactAltPhone = "<ContactAltPhone><![CDATA[]]></ContactAltPhone>"
        'APDVehicle.Vehicle_CellPhoneCarrier = "<CellPhoneCarrier><![CDATA[]]></CellPhoneCarrier>"
        'APDVehicle.Vehicle_ContactCellPhone = "<ContactCellPhone><![CDATA[]]></ContactCellPhone>"
        'APDVehicle.Vehicle_OwnerPhoneSumm = "<OwnerPhoneSumm><![CDATA[412-111-1111]]></OwnerPhoneSumm>"
        'APDVehicle.Vehicle_ContactPhoneSumm = "<ContactPhoneSumm><![CDATA[412-111-1111]]></ContactPhoneSumm>"
        'APDVehicle.Vehicle_ContactNightPhoneSumm = "<ContactNightPhoneSumm><![CDATA[]]></ContactNightPhoneSumm>"
        'APDVehicle.Vehicle_ContactAltPhoneSumm = "<ContactAltPhoneSumm><![CDATA[]]></ContactAltPhoneSumm>"
        'APDVehicle.Vehicle_ContactCellPhoneSumm = "<ContactCellPhoneSumm><![CDATA[]]></ContactCellPhoneSumm>"
        'APDVehicle.Vehicle_PrefMethodUpd = "<PrefMethodUpd><![CDATA[NU]]></PrefMethodUpd>"
        'APDVehicle.Vehicle_ShopName = "<ShopName><![CDATA[Wolbert Auto Body & Repair]]></ShopName>"
        'APDVehicle.Vehicle_ShopAddress1 = "<ShopAddress1><![CDATA[47 East Crafton Avenue]]></ShopAddress1>"
        'APDVehicle.Vehicle_ShopAddress2 = "<ShopAddress2><![CDATA[]]></ShopAddress2>"
        'APDVehicle.Vehicle_ShopCity = "<ShopCity><![CDATA[Pittsburgh]]></ShopCity>"
        'APDVehicle.Vehicle_ShopState = "<ShopState><![CDATA[PA]]></ShopState>"
        'APDVehicle.Vehicle_ShopZip = "<ShopZip><![CDATA[15205]]></ShopZip>"
        'APDVehicle.Vehicle_ShopPhone = "<ShopPhone><![CDATA[412-922-8898]]></ShopPhone>"
        'APDVehicle.Vehicle_ShopFax = "<ShopFax><![CDATA[412-922-8021]]></ShopFax>"
        'APDVehicle.Vehicle_RentalAuthorized = "<RentalAuthorized_><![CDATA[No]]></RentalAuthorized_>"
        'APDVehicle.Vehicle_RentalDays = "<RentalDays><![CDATA[]]></RentalDays>"
        'APDVehicle.Vehicle_RentalDayAmount = "<RentalDayAmount><![CDATA[]]></RentalDayAmount>"
        'APDVehicle.Vehicle_RentalMaxAmount = "<RentalMaxAmount><![CDATA[]]></RentalMaxAmount>"
        'APDVehicle.Vehicle_RentalDaysAuthorized = "<RentalDaysAuthorized><![CDATA[]]></RentalDaysAuthorized>"
        'APDVehicle.Vehicle_DeductibleAmt = "<DeductibleAmt><![CDATA[100.00]]></DeductibleAmt>"
        'APDVehicle.Vehicle_LimitAmt = "<LimitAmt><![CDATA[1000.00]]></LimitAmt>"
        'APDVehicle.Vehicle_CoverageProfileUiCD = "<CoverageProfileUiCD><![CDATA[COLL|175]]></CoverageProfileUiCD>"
        'APDVehicle.Vehicle_PrimaryDamageDescription = "<PrimaryDamageDescription><![CDATA[Rear Corner Passenger's Side]]></PrimaryDamageDescription>"
        'APDVehicle.Vehicle_SecondaryDamageDescription = "<SecondaryDamageDescription><![CDATA[Front Bumper]]></SecondaryDamageDescription>"
        'APDVehicle.Vehicle_AssignmentTypeIDDescription = "<AssignmentTypeIDDescription><![CDATA[Program Shop]]></AssignmentTypeIDDescription>"
        'APDVehicle.Vehicle_Mileage = "<Mileage><![CDATA[564]]></Mileage>"
        'APDVehicle.Vehicle_ClientCoverageTypeID = "<ClientCoverageTypeID><![CDATA[175]]></ClientCoverageTypeID>"
        'APDVehicle.Vehicle_ClientCoverageTypeDesc = "<ClientCoverageTypeDesc><![CDATA[Collision]]></ClientCoverageTypeDesc>"
        'APDVehicle.Vehicle_VehicleNumber = "<VehicleNumber><![CDATA[1]]></VehicleNumber>"
        'APDVehicle.Vehicle_ShowScriptingMemoFlag = "<ShowScriptingMemoFlag><![CDATA[0]]></ShowScriptingMemoFlag>"
        'APDVehicle.Vehicle_SourceApplicationPassthruDataVeh = "<SourceApplicationPassthruDataVeh><![CDATA[]]></SourceApplicationPassthruDataVeh>"
        'APDVehicle.Vehicle_rbTypeAuthorized = "<rbTypeAuthorized><![CDATA[on]]></rbTypeAuthorized>"
        'APDVehicle.Vehicle_rbTypeEstimate = "<rbTypeEstimate><![CDATA[on]]></rbTypeEstimate>"
        'APDVehicle.Vehicle_ShopRemarks = "<ShopRemarks><![CDATA[]]></ShopRemarks>"
        'APDVehicle.Vehicle_DrivingDirections = "<DrivingDirections><![CDATA[Take Route 60 (Steuben Street)into Crafton. Turn on Noble Avenue then right on East Crafton.]]></DrivingDirections>"
        'APDVehicle.Vehicle_RentalInstructions = "<RentalInstructions><![CDATA[]]></RentalInstructions>"

        'Dim XMLVehicle As XmlDocument = New XmlDocument
        'XMLVehicle.Load(APDVehicle)

        '-----------------------------------------
        ' Process the claim
        '-----------------------------------------
        'sLynxID = APDWCFFoundation.Ping()
        'sLynxID = APDWCFFoundation.CreatewsAPDClaim(wsAPDClaim, APDVehicle)
        'sLynxID = APDFoundation.CreateAPDClaim(wsAPDClaim, wsAPDVehicle)

        'Dim oXS As XmlSerializer = New XmlSerializer(GetType(APDClaim))
        'Dim oLucky As New APDClaim()
        'Response.Write(Server.HtmlEncode(oLucky.NewClaimFlag))

        'Dim oStmW As StreamWriter
        'oStmW = New StreamWriter(Server.MapPath("lucky.xml"))
        'oXS.Serialize(oStmW, oLucky)
        'oStmW.Close()
        'Response.Write(Server.HtmlEncode(XMLLynx.InnerText))
    End Sub

    'Private Function SerializeAnObject(ByVal AnObject As Object) As String
    '    Dim Xml_Serializer As XmlSerializer = New XmlSerializer(AnObject.GetType)
    '    Dim Writer As StringWriter = New StringWriter
    '    Xml_Serializer.Serialize(Writer, AnObject)
    '    Return Writer.ToString
    'End Function
End Class