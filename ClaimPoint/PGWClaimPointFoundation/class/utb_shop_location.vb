﻿Public Class utb_shop_location
    Private m_iShopLocationID As Integer

    Private m_sShopGUID As String
    Private m_sSource As String
    Private m_sDistance As String

    Private m_iShopID As Integer
    Private m_sName As String
    Private m_sAddress1 As String
    Private m_sAddress2 As String
    Private m_sAddressCity As String
    Private m_sAddressCounty As String
    Private m_sAddressState As String
    Private m_sAddressZip As String
    Private m_sPhoneNumber As String
    Private m_sFaxNumber As String
    Private m_sShopOpen As String
    Private m_sTimeZone As String
    Private m_sContactName As String
    Private m_sContactPhoneNumber As String
    Private m_sOperatingMondayStartTime As String
    Private m_sOperatingMondayEndTime As String
    Private m_sOperatingTuesdayStartTime As String
    Private m_sOperatingTuesdayEndTime As String
    Private m_sOperatingWednesdayStartTime As String
    Private m_sOperatingWednesdayEndTime As String
    Private m_sOperatingThursdayStartTime As String
    Private m_sOperatingThursdayEndTime As String
    Private m_sOperatingFridayStartTime As String
    Private m_sOperatingFridayEndTime As String
    Private m_sOperatingSaturdayStartTime As String
    Private m_sOperatingSaturdayEndTime As String
    Private m_sOperatingSundayStartTime As String
    Private m_sOperatingSundayEndTime As String
    Private m_sEmailAddress As String
    Private m_sWebSiteAddress As String
    Private m_sDrivingDirections As String
    Private m_sLatitude As String
    Private m_sLongitude As String

    Private m_sErrors As String

    Public Property ShopLocationID() As Integer
        Get
            Return m_iShopLocationID
        End Get
        Set(ByVal Value As Integer)
            m_iShopLocationID = Value
        End Set
    End Property
    Public Property ShopGUID() As String
        Get
            Return m_sShopGUID
        End Get
        Set(ByVal Value As String)
            m_sShopGUID = Value
        End Set
    End Property

    Public Property Source() As String
        Get
            Return m_sSource
        End Get
        Set(ByVal Value As String)
            m_sSource = Value
        End Set
    End Property

    Public Property Distance() As String
        Get
            Return m_sDistance
        End Get
        Set(ByVal Value As String)
            m_sDistance = Value
        End Set
    End Property

    Public Property ShopName() As String
        Get
            Return m_sName
        End Get
        Set(ByVal Value As String)
            m_sName = Value
        End Set
    End Property

    Public Property Address1() As String
        Get
            Return m_sAddress1
        End Get
        Set(ByVal Value As String)
            m_sAddress1 = Value
        End Set
    End Property

    Public Property Address2() As String
        Get
            Return m_sAddress2
        End Get
        Set(ByVal Value As String)
            m_sAddress2 = Value
        End Set
    End Property

    Public Property AddressCity() As String
        Get
            Return m_sAddressCity
        End Get
        Set(ByVal Value As String)
            m_sAddressCity = Value
        End Set
    End Property

    Public Property AddressCounty() As String
        Get
            Return m_sAddressCounty
        End Get
        Set(ByVal Value As String)
            m_sAddressCounty = Value
        End Set
    End Property

    Public Property AddressState() As String
        Get
            Return m_sAddressState
        End Get
        Set(ByVal Value As String)
            m_sAddressState = Value
        End Set
    End Property

    Public Property AddressZip() As String
        Get
            Return m_sAddressZip
        End Get
        Set(ByVal Value As String)
            m_sAddressZip = Value
        End Set
    End Property

    Public Property PhoneNumber() As String
        Get
            Return m_sPhoneNumber
        End Get
        Set(ByVal Value As String)
            m_sPhoneNumber = Value
        End Set
    End Property

    Public Property FaxNumber() As String
        Get
            Return m_sFaxNumber
        End Get
        Set(ByVal Value As String)
            m_sFaxNumber = Value
        End Set
    End Property

    Public Property ShopOpen() As String
        Get
            Return m_sShopOpen
        End Get
        Set(ByVal Value As String)
            m_sShopOpen = Value
        End Set
    End Property

    Public Property TimeZone() As String
        Get
            Return m_sTimeZone
        End Get
        Set(ByVal Value As String)
            m_sTimeZone = Value
        End Set
    End Property

    Public Property ContactName() As String
        Get
            Return m_sContactName
        End Get
        Set(ByVal Value As String)
            m_sContactName = Value
        End Set
    End Property

    Public Property ContactPhoneNumber() As String
        Get
            Return m_sContactPhoneNumber
        End Get
        Set(ByVal Value As String)
            m_sContactPhoneNumber = Value
        End Set
    End Property

    Public Property OperatingMondayStartTime() As String
        Get
            Return m_sOperatingMondayStartTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingMondayStartTime = Value
        End Set
    End Property

    Public Property OperatingMondayEndTime() As String
        Get
            Return m_sOperatingMondayEndTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingMondayEndTime = Value
        End Set
    End Property

    Public Property OperatingTuesdayStartTime() As String
        Get
            Return m_sOperatingTuesdayStartTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingTuesdayStartTime = Value
        End Set
    End Property

    Public Property OperatingTuesdayEndTime() As String
        Get
            Return m_sOperatingTuesdayEndTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingTuesdayEndTime = Value
        End Set
    End Property

    Public Property OperatingWednesdayStartTime() As String
        Get
            Return m_sOperatingWednesdayStartTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingWednesdayStartTime = Value
        End Set
    End Property

    Public Property OperatingWednesdayEndTime() As String
        Get
            Return m_sOperatingWednesdayEndTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingWednesdayEndTime = Value
        End Set
    End Property

    Public Property OperatingThursdayStartTime() As String
        Get
            Return m_sOperatingThursdayStartTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingThursdayStartTime = Value
        End Set
    End Property

    Public Property OperatingThursdayEndTime() As String
        Get
            Return m_sOperatingThursdayEndTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingThursdayEndTime = Value
        End Set
    End Property

    Public Property OperatingFridayStartTime() As String
        Get
            Return m_sOperatingFridayStartTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingFridayStartTime = Value
        End Set
    End Property

    Public Property OperatingFridayEndTime() As String
        Get
            Return m_sOperatingFridayEndTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingFridayEndTime = Value
        End Set
    End Property

    Public Property OperatingSaturdayStartTime() As String
        Get
            Return m_sOperatingSaturdayStartTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingSaturdayStartTime = Value
        End Set
    End Property

    Public Property OperatingSaturdayEndTime() As String
        Get
            Return m_sOperatingSaturdayEndTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingSaturdayEndTime = Value
        End Set
    End Property

    Public Property OperatingSundayStartTime() As String
        Get
            Return m_sOperatingSundayStartTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingSundayStartTime = Value
        End Set
    End Property

    Public Property OperatingSundayEndTime() As String
        Get
            Return m_sOperatingSundayEndTime
        End Get
        Set(ByVal Value As String)
            m_sOperatingSundayEndTime = Value
        End Set
    End Property

    Public Property EmailAddress() As String
        Get
            Return m_sEmailAddress
        End Get
        Set(ByVal Value As String)
            m_sEmailAddress = Value
        End Set
    End Property

    Public Property WebSiteAddress() As String
        Get
            Return m_sWebSiteAddress
        End Get
        Set(ByVal Value As String)
            m_sWebSiteAddress = Value
        End Set
    End Property

    Public Property DrivingDirections() As String
        Get
            Return m_sDrivingDirections
        End Get
        Set(ByVal Value As String)
            m_sDrivingDirections = Value
        End Set
    End Property
    Public Property Latitude() As String
        Get
            Return m_sLatitude
        End Get
        Set(ByVal Value As String)
            m_sLatitude = Value
        End Set
    End Property
    Public Property Longitude() As String
        Get
            Return m_sLongitude
        End Get
        Set(ByVal Value As String)
            m_sLongitude = Value
        End Set
    End Property
    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class

