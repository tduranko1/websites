﻿Public Class utb_zip_code
    Private m_sZipCode As String = ""
    Private m_sTimeZone As String = ""
    Private m_sAreaCode As String = ""
    Private m_sCity As String = ""
    Private m_bCityPrimaryZipFlag As Boolean = False
    Private m_sCounty As String = ""
    Private m_sCountyType As String = ""
    Private m_bDaylightSavingsTimeFlag As Boolean = False
    Private m_sFIPS As String = ""
    Private m_dLatitude As Double
    Private m_dLongitude As Double
    Private m_sMSAName As String = ""
    Private m_sState As String = ""
    Private m_sType As String = ""
    Private m_bSysMaintainedFlag As Boolean = False
    Private m_iSysLastUserID As Integer = 0
    Private m_dtSysLastUpdatedDate As DateTime

    'Private m_sErrors As String

    Public Property ZipCode() As String
        Get
            Return m_sZipCode
        End Get
        Set(ByVal Value As String)
            m_sZipCode = Value
        End Set
    End Property

    Public Property TimeZone() As String
        Get
            Return m_sTimeZone
        End Get
        Set(ByVal Value As String)
            m_sTimeZone = Value
        End Set
    End Property

    Public Property AreaCode() As String
        Get
            Return m_sAreaCode
        End Get
        Set(ByVal Value As String)
            m_sAreaCode = Value
        End Set
    End Property

    Public Property City() As String
        Get
            Return m_sCity
        End Get
        Set(ByVal Value As String)
            m_sCity = Value
        End Set
    End Property

    Public Property CityPrimaryZipFlag() As Boolean
        Get
            Return m_bCityPrimaryZipFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bCityPrimaryZipFlag = Value
        End Set
    End Property

    Public Property County() As String
        Get
            Return m_sCounty
        End Get
        Set(ByVal Value As String)
            m_sCounty = Value
        End Set
    End Property

    Public Property CountyType() As String
        Get
            Return m_sCountyType
        End Get
        Set(ByVal Value As String)
            m_sCountyType = Value
        End Set
    End Property

    Public Property DaylightSavingsTimeFlag() As Boolean
        Get
            Return m_bDaylightSavingsTimeFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bDaylightSavingsTimeFlag = Value
        End Set
    End Property

    Public Property FIPS() As String
        Get
            Return m_sFIPS
        End Get
        Set(ByVal Value As String)
            m_sFIPS = Value
        End Set
    End Property

    Public Property Latitude() As Double
        Get
            Return m_dLatitude
        End Get
        Set(ByVal Value As Double)
            m_dLatitude = Value
        End Set
    End Property

    Public Property Longitude() As Double
        Get
            Return m_dLongitude
        End Get
        Set(ByVal Value As Double)
            m_dLongitude = Value
        End Set
    End Property

    Public Property MSAName() As String
        Get
            Return m_sMSAName
        End Get
        Set(ByVal Value As String)
            m_sMSAName = Value
        End Set
    End Property

    Public Property State() As String
        Get
            Return m_sState
        End Get
        Set(ByVal Value As String)
            m_sState = Value
        End Set
    End Property

    Public Property Type() As String
        Get
            Return m_sType
        End Get
        Set(ByVal Value As String)
            m_sType = Value
        End Set
    End Property

    Public Property SysMaintainedFlag() As Boolean
        Get
            Return m_bSysMaintainedFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bSysMaintainedFlag = Value
        End Set
    End Property

    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    'Public Property Errors() As String
    '    Get
    '        Return m_sErrors
    '    End Get
    '    Set(ByVal Value As String)
    '        m_sErrors = Value
    '    End Set
    'End Property

End Class
