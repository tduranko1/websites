﻿'--------------------------------------------------------------
' Program: PGWClaimPointFoundation WebServices
' Author:  Thomas Duranko
' Date:    Aug 15, 2012
' Version: 1.0 - 15Aug2012 - Initial Development
' Version: 1.1 - 22Jun2017 - Added code to check if an existing claim exists before processing
'                            a new one.
' Version: 1.2 - 26Oct2018 - Added Rental Mgt Service - This processes in to APD Rental
'                            Management data.
'              - 09Jan2019 - Add check if ShopLocationID is blank, if so call ChoiceShopProcess WS
'                          - Update Carrier Rep details if different than existing
'              - 06May2019 - Correcting Carrier Rep update issues.  Check to make sure names are not blank
'                            and the phone number is correct before updating
' Version: 1.3 - 09Oct2019 - TVD - Check and see if vehicle Number already exists instead of if it is 
'                            greater than the max number
' Version: 1.4 - 02Nov2020 - Mahes - PostOffice interface added.
'
' Description:  This site provides a number of Client Side web service
'               requestes via HTTP/XML SOAP requests for ASP.NET 
'               applications.  
'--------------------------------------------------------------
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

Imports System.Configuration.ConfigurationManager
Imports System.Xml
Imports System.IO
Imports System.Net
Imports System.Xml.Schema

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class APDService
    Inherits System.Web.Services.WebService

    '---------------------------------
    ' Web Service Location
    '---------------------------------
    Dim APDFoundation As New APDFoundation.APDService  ' -- Points to Dev Server --
    Dim sProcessingServer As String = AppSettings("ServerName")

    Dim EventTransactionID As String = ""  'Handles APDPostOffice job'
    Dim iEventSeq As Integer = 0    'Handles APDPostOffice job'
    Dim strXMLErrorHandler = ""

    'Dim APDFoundation As New APDFoundationLocal.APDService  ' -- Points to Local Dev Workstation --
    'Dim APDFoundation As New APDFoundationStage.APDService  ' -- Points to Stage Workstation --

    '--------------------------------------------
    ' This function returns is a simple ping test 
    ' to see if both the Client side Web Service 
    ' and Server side Web Service is working.
    '--------------------------------------------
    <WebMethod(Description:="This function returns is a simple ping test to see if both the Client side Web Service and Server side Web Service is working.")>
    Public Function Ping() As String
        Dim sReturnData As String = ""
        Dim sRemoteData As String = ""

        Try
            '-------------------------------------
            ' Testing Client Side Web Service
            '-------------------------------------
            sReturnData = "Clent Side WebServices Ready!!! Processing Server: " & sProcessingServer

            '-------------------------------------
            ' Testing Server Side Web Service
            '-------------------------------------
            sRemoteData = APDFoundation.Ping

            Return sReturnData & " ----> " & sRemoteData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return "WebServices Failed from Processing Server: " & sProcessingServer & " - " & oExcept.ToString
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: CreateAPDClaim
    ' This APD function creates a claim into APD using Claim XML
    ' that is passed from the client
    '-----------------------------------------------------------
    'Public Function CreateAPDClaim(ByVal Claim As APDClaim, ByVal Vehicles As APDVehicle) As String
    <WebMethod(Description:="This APD function creates a claim into APD.")>
    Public Function CreateAPDClaim(ByVal WSClaim As String) As String
        Dim XMLWSClaim As New XmlDocument
        Dim XMLWSExistingClaim As New XmlDocument
        Dim XMLReturn As New XmlDocument
        Dim sReturnXML As String = ""
        Dim XMLNodeValidate As XmlNode
        'Dim XMLExistingNodeValidate As XmlNode
        Dim XMLVehicleNodeValidate As XmlNodeList
        Dim sAPDNetDebugging As String = "FALSE"
        Dim sAssignmentSource As String = ""

        '--------------------------------------
        ' Existing ClaimSearch Check Variables
        '--------------------------------------
        'Dim ExistingClaimXML As XmlDocument
        Dim sExistingClaimXML As String = ""
        Dim sNewClientClaimNumber As String = ""
        Dim sNewExposureCD As String = ""
        Dim sNewVehicleNumber As String = ""
        Dim sExistingLynxID As String = ""
        Dim sExistingVehicleNumber As String = ""
        Dim sExistingExposure As String = ""
        Dim sExistingClaimNumber As String = ""
        Dim sReturnData As String = ""
        Dim sExistingClaimReturnData As String = ""
        Dim sStoredProcedure As String = ""
        Dim sParams As String = ""
        Dim oExistingClaimReturnXML As New XmlDocument
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim sExistingInsuranceCompanyID As String = ""
        Dim sExistingCoverageClaimNumber As String = ""
        Dim sExistingCarrierRepUserID As String = ""
        Dim sExistingCarrierRepNameFirst As String = ""
        Dim sExistingCarrierRepNameLast As String = ""
        Dim sExistingCarrierRepPhoneDay As String = ""
        Dim sExistingCarrierRepEmailAddress As String = ""

        '--------------------------------------
        ' ShopLocation Check Variables
        '--------------------------------------
        Dim XMLNodeShopLocationID As XmlNode = Nothing
        Dim sShopPhone As String = ""
        Dim sShopFax As String = ""
        Dim sPhoneAreaCode As String = ""
        Dim sPhoneExchangeNumber As String = ""
        Dim sPhoneUnitNumber As String = ""
        Dim sFaxAreaCode As String = ""
        Dim sFaxExchangeNumber As String = ""
        Dim sFaxUnitNumber As String = ""
        Dim sShopLocationID As String = ""

        Try
            sAPDNetDebugging = AppSettings("Debug")

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer, "Client Side Claim process started", "Claim XML passed-in is in EventXML", WSClaim)
            End If

            '---------------------------------------
            ' Load claim from client
            '---------------------------------------
            XMLWSClaim.LoadXml(WSClaim)
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/DataSource")

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUGGING", "Claim XML loaded successfully into XmlDocument", "Claim is source is " & XMLNodeValidate.InnerText, "")
            End If

            '-----------------------------------------
            ' 23Oct2013 - TVD - Modified to handle
            ' ClaimPoint Assignments as well
            '-----------------------------------------
            Select Case UCase(XMLNodeValidate.InnerText)
                Case "WEB SERVICES ASSIGNMENT"
                    sAssignmentSource = "WS"
                Case "CLAIM POINT WEB ASSIGNMENT"
                    sAssignmentSource = "CP"
                    Dim elem As XmlElement = Nothing

                    '---------------------------------
                    ' Debug Data
                    '---------------------------------
                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        APDFoundation.LogEvent("WSClientSideClaimProcessor", "CLAIMPOINT", "Cleaning up the XML tags", "", "")
                    End If

                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LynxID")
                    If XMLNodeValidate Is Nothing Then
                        '---------------------------------------
                        ' Add missing/unused tags - Claim
                        '---------------------------------------
                        elem = XMLWSClaim.CreateElement("LynxID")
                        elem.InnerText = ""

                        XMLVehicleNodeValidate = XMLWSClaim.SelectNodes("WebAssignment/Claim")
                        For Each XMLNode In XMLVehicleNodeValidate
                            XMLNode.InsertAfter(elem, XMLNode.SelectSingleNode("WebAssignment/Claim"))
                        Next
                    End If

                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/Remarks")
                    If XMLNodeValidate Is Nothing Then
                        '---------------------------------------
                        ' Add missing/unused tags - Vehicle
                        '---------------------------------------
                        elem = XMLWSClaim.CreateElement("Remarks")
                        elem.InnerText = ""

                        XMLVehicleNodeValidate = XMLWSClaim.SelectNodes("WebAssignment/Vehicle")
                        For Each XMLNode In XMLVehicleNodeValidate
                            XMLNode.InsertAfter(elem, XMLNode.SelectSingleNode("WebAssignment/Vehicle/ShopFax"))
                        Next
                    End If

                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/SourceApplicationPassthruDataVeh")
                    If XMLNodeValidate Is Nothing Then
                        '---------------------------------------
                        ' Add missing/unused tags - Vehicle
                        '---------------------------------------
                        elem = XMLWSClaim.CreateElement("SourceApplicationPassthruDataVeh")
                        elem.InnerText = ""

                        XMLVehicleNodeValidate = XMLWSClaim.SelectNodes("WebAssignment/Vehicle")
                        For Each XMLNode In XMLVehicleNodeValidate
                            XMLNode.InsertAfter(elem, XMLNode.SelectSingleNode("WebAssignment/Vehicle/ShopFax"))
                        Next
                    End If

                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/rbTypeAuthorized")
                    If XMLNodeValidate Is Nothing Then
                    Else
                        '---------------------------------------
                        ' Remove Tags
                        '---------------------------------------
                        XMLVehicleNodeValidate = XMLWSClaim.SelectNodes("WebAssignment/Vehicle")
                        For Each XMLNode In XMLVehicleNodeValidate
                            XMLNode.RemoveChild(XMLNode.SelectSingleNode("rbTypeAuthorized"))
                        Next
                    End If

                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/rbTypeEstimate")
                    If XMLNodeValidate Is Nothing Then
                    Else
                        '---------------------------------------
                        ' Remove Tags
                        '---------------------------------------
                        XMLVehicleNodeValidate = XMLWSClaim.SelectNodes("WebAssignment/Vehicle")
                        For Each XMLNode In XMLVehicleNodeValidate
                            XMLNode.RemoveChild(XMLNode.SelectSingleNode("rbTypeEstimate"))
                        Next
                    End If
            End Select

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUGGING", "XML Modified", "Modified XML version is in EventXML", XMLWSClaim.OuterXml)
            End If

            '---------------------------------------
            ' Parse and validate the Claim Rep info
            '---------------------------------------
            Dim sValidClaimRepID As String
            sValidClaimRepID = ValidateClaimRep(XMLWSClaim)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUGGING", "Checking if Claim Rep is valid", "Claim Rep ID return is in EventXML", sValidClaimRepID)
            End If

            If sValidClaimRepID <> "" Then
                '-------------------------------------
                ' Update the CarrierRepUserID
                ' to match the Validate lookup/create
                '-------------------------------------
                XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepUserID")
                XMLNodeValidate.InnerText = sValidClaimRepID
            Else
                '-------------------------------------
                ' Invalid sValidClaimRepID
                '-------------------------------------
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "Validate", "Client Side carrier rep validation process failed", "Carrier rep validate failed.", "sValidClaimRepID = " & sValidClaimRepID)
                Throw New SystemException("XMLValidationError: Claim representative ID does not exist and was not created successfully.")
            End If

            '---------------------------------------
            ' Parse and validate the Claim.xml
            '---------------------------------------
            Dim isXMLValid As New Boolean
            isXMLValid = ValidateClaim(XMLWSClaim, XMLWSClaim.SelectSingleNode("WebAssignment/Claim/AssignmentDescription").InnerText)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUGGING", "Check if Claim XML is valid", "Result in EventXML", isXMLValid)
            End If

            '---------------------------------------
            ' Process the Claim XML from the customer
            '---------------------------------------
            If isXMLValid Then
                '-----------------------------------
                ' TVD - 04May2017 - Existing Claim 
                ' Check and see if the claim already
                ' exists in APD before processing
                '-----------------------------------
                ' Start Code Modification
                '-----------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "CLAIMCHECK", "Checking if Existing Claim", "", "")
                End If

                '--------------------------------------------
                ' Get the parameter for the ClaimSearch call
                '--------------------------------------------
                sNewClientClaimNumber = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CoverageClaimNumber").InnerText

                ' Remove special characters from ClientClaimNumber
                ' Squish the ClientClaimNumber
                sNewClientClaimNumber = Regex.Replace(sNewClientClaimNumber, "[^A-Za-z0-9\/]", "")

                sNewExposureCD = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ExposureCD").InnerText
                sNewVehicleNumber = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/VehicleNumber").InnerText

                '------------------------------------------------------------------
                ' ShopLocationID is Required Must be Numeric and no more 
                ' then 4 characters
                ' - If ShopLocationID is 0 or null then create a new choice shop
                '------------------------------------------------------------------
                XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopLocationID")
                If (XMLNodeValidate.InnerText = "" Or XMLNodeValidate.InnerText Is Nothing Or XMLNodeValidate.InnerText = "0") Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "ShopLocationID Check", "Attempting to create NEW CHOICE Shop", "Creating NEW CHOICE Shop in utb_shop_location", "New Shop: " & XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopName").InnerText)

                    '----------------------------
                    ' Check if shop already 
                    ' exists based on 3 criteria
                    '----------------------------
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "New Shop Check", "Checking NEW CHOICE Shop for duplicates", "Checking NEW CHOICE Shop in utb_shop_location if exists", "New Shop Criteria: " & XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopName").InnerText & " Address: " & XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopAddress1").InnerText & " Zipcode: " & XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopZip").InnerText & " Phone: " & XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopPhone").InnerText)

                    '----------------------------
                    ' Parse Phone/Fax 
                    '----------------------------
                    sShopPhone = ""
                    sShopFax = ""
                    sPhoneAreaCode = ""
                    sPhoneExchangeNumber = ""
                    sPhoneUnitNumber = ""
                    sFaxAreaCode = ""
                    sFaxExchangeNumber = ""
                    sFaxUnitNumber = ""

                    sShopPhone = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopPhone").InnerText
                    sShopFax = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopFax").InnerText

                    If sShopPhone <> "" Then
                        If sShopPhone.Contains("-") Then
                            sPhoneAreaCode = sShopPhone.Substring(0, 3)
                            sPhoneExchangeNumber = sShopPhone.Substring(4, 3)
                            sPhoneUnitNumber = sShopPhone.Substring(8, 4)
                        Else
                            sPhoneAreaCode = sShopPhone.Substring(0, 3)
                            sPhoneExchangeNumber = sShopPhone.Substring(3, 3)
                            sPhoneUnitNumber = sShopPhone.Substring(6, 4)
                        End If
                    End If

                    If sShopFax <> "" Then
                        If sShopFax.Contains("-") Then
                            sFaxAreaCode = sShopFax.Substring(0, 3)
                            sFaxExchangeNumber = sShopFax.Substring(43, 3)
                            sFaxUnitNumber = sShopFax.Substring(8, 4)
                        Else
                            sFaxAreaCode = sShopFax.Substring(0, 3)
                            sFaxExchangeNumber = sShopFax.Substring(3, 3)
                            sFaxUnitNumber = sShopFax.Substring(6, 4)
                        End If
                    End If

                    sShopLocationID = CheckExistingShop(
                        XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopName").InnerText _
                        , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopAddress1").InnerText _
                        , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopAddress2").InnerText _
                        , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopCity").InnerText _
                        , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopState").InnerText _
                        , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopZip").InnerText _
                        , sPhoneAreaCode _
                        , sPhoneExchangeNumber _
                        , sPhoneUnitNumber _
                        , "B" _
                        , XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuranceCompanyID").InnerText
                    )

                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "SHOPID", "Current Shop Location", "ShopLocationID in EventXML", "ShopLocationID = " & sShopLocationID)

                    If sShopLocationID <> "" Then
                        '-----------------------------------
                        ' Debugging
                        '-----------------------------------
                        If UCase(sAPDNetDebugging) = "TRUE" Then
                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "DUPCHECK", "Shop already exists", "Results in EventXML", "ShopLocationID = " & sShopLocationID)
                        End If
                    End If

                    '-------------------------
                    ' Call the ChoiceShopWS to
                    ' create new one
                    '-------------------------
                    If sShopLocationID = "" Then
                        '----------------------------
                        ' Create Choice Shop 
                        '----------------------------
                        APDFoundation.LogEvent("WSClientSideClaimProcessor", "ChoiceShopProcess()", "Calling WS ChoiceShopProcess", "Creating NEW CHOICE Shop", "")
                        sShopLocationID = ChoiceShopProcess(
                            "True" _
                            , "" _
                            , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopAddress1").InnerText _
                            , "" _
                            , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopCity").InnerText _
                            , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopState").InnerText _
                            , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopZip").InnerText _
                            , "" _
                            , "True" _
                            , sFaxAreaCode _
                            , sFaxExchangeNumber _
                            , sFaxUnitNumber _
                            , "" _
                            , XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopName").InnerText _
                            , sPhoneAreaCode _
                            , sPhoneExchangeNumber _
                            , sPhoneUnitNumber _
                            , 0 _
                            , "APD" _
                            , "" _
                            , "")
                    End If

                    ' -- Set ShopLocationID
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "ChoiceShopProcess()", "ChoiceShopProcess call complete", "Selected or Created ShopLocationID in EventXML", sShopLocationID)
                    XMLNodeShopLocationID = XMLWSClaim.SelectSingleNode("WebAssignment/Vehicle/ShopLocationID")
                    XMLNodeShopLocationID.InnerText = sShopLocationID

                End If

                '-----------------------------------
                ' Debugging
                '-----------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "CLAIMCHECK", "Checking if Existing Claim parameters", "Parameters in EventXML", "PARAMS: sNewClientClaimNumber=" & sNewClientClaimNumber & ", sNewExposureCD=" & sNewExposureCD & ", sNewVehicleNumber=" & sNewVehicleNumber)
                End If

                ' Claim already exists by checking lynxid, claimnumber, vehicle number and exposure
                sStoredProcedure = "uspClaimCheckIFExistsWSXML"
                sParams = "'" + sNewClientClaimNumber + "', " & sNewExposureCD & ", " & sNewVehicleNumber

                '-----------------------------------
                ' Debugging
                '-----------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "EXECDB", "Execute SP to check if claim exists", "EXEC Call in EventXML", sStoredProcedure & " " & sParams)
                End If

                sExistingClaimReturnData = ExecuteSpAsXML(sStoredProcedure, sParams)
                oExistingClaimReturnXML.LoadXml(sExistingClaimReturnData)
                oXMLNodeList = oExistingClaimReturnXML.SelectNodes("/Root")

                '-----------------------------------
                ' Debugging
                '-----------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "CLAIMCHECK", "ClaimSearch Response XML", "ClaimSearch(uspClaimCheckIFExistsWSXML) Returned XML in EventXML", "uspClaimCheckIFExistsWSXML=" & sExistingClaimReturnData)
                End If

                For Each oXMLNode In oXMLNodeList
                    Try
                        sExistingLynxID = oXMLNode.Attributes("LynxID").InnerText
                        sExistingInsuranceCompanyID = oXMLNode.Attributes("InsuranceCompanyID").InnerText
                        sExistingCoverageClaimNumber = oXMLNode.Attributes("ClientClaimNumber").InnerText
                        sExistingCarrierRepUserID = oXMLNode.Attributes("CarrierRepUserID").InnerText
                        sExistingCarrierRepNameFirst = oXMLNode.Attributes("CarrierRepNameFirst").InnerText
                        sExistingCarrierRepNameLast = oXMLNode.Attributes("CarrierRepNameLast").InnerText
                        sExistingCarrierRepPhoneDay = oXMLNode.Attributes("CarrierRepPhoneDay").InnerText
                        sExistingCarrierRepEmailAddress = oXMLNode.Attributes("CarrierRepEmailAddress").InnerText
                    Catch ex As Exception
                        Throw New SystemException("ERROR: (WS:GetNewChoiceShopAssignments) Missing tag or bad data while parsing the Check Existing Claim details.  (" & sStoredProcedure & ")")
                    End Try
                Next

                ' Get Existing claim details
                If sExistingLynxID <> "" Then
                    ' Claim already exists by checking lynxid, claimnumber, vehicle number and exposure

                    '-----------------------------------
                    ' Debugging
                    '-----------------------------------
                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        APDFoundation.LogEvent("WSClientSideClaimProcessor", "EXISTINGCLAIM", "Existing Claim Details", "Existing claim details returned in XML in EventXML", "sExistingLynxID=" & sExistingLynxID & ", " & "sExistingInsuranceCompanyID=" & sExistingInsuranceCompanyID & ", " & "sExistingCoverageClaimNumber=" & sExistingCoverageClaimNumber & ", " & "sExistingCarrierRepNameFirst=" & sExistingCarrierRepNameFirst & ", " & "sExistingCarrierRepNameLast=" & sExistingCarrierRepNameLast & ", " & "sExistingCarrierRepPhoneDay=" & sExistingCarrierRepPhoneDay & ", " & "sExistingCarrierRepEmailAddress=" & sExistingCarrierRepEmailAddress)
                    End If

                    '-----------------------------------
                    ' Return existing details like a
                    ' successful claim
                    '-----------------------------------
                    '------------------------------
                    ' Build return error XML
                    '------------------------------
                    sReturnXML = "<ClaimResponse>"
                    sReturnXML += "<LynxID>" & sExistingLynxID & "</LynxID>"
                    sReturnXML += "<InsuranceCompanyName>" & sExistingInsuranceCompanyID & "</InsuranceCompanyName>"
                    sReturnXML += "<CoverageClaimNumber>" & sExistingCoverageClaimNumber & "</CoverageClaimNumber>"
                    sReturnXML += "<CarrierRepNameFirst>" & sExistingCarrierRepNameFirst & "</CarrierRepNameFirst>"
                    sReturnXML += "<CarrierRepNameLast>" & sExistingCarrierRepNameLast & "</CarrierRepNameLast>"
                    sReturnXML += "<CarrierRepPhoneDay>" & sExistingCarrierRepPhoneDay & "</CarrierRepPhoneDay>"
                    sReturnXML += "<CarrierRepEmailAddress>" & sExistingCarrierRepEmailAddress & "</CarrierRepEmailAddress>"
                    sReturnXML += "<StatusID>0</StatusID>"
                    sReturnXML += "<Status>Claim ALREADY EXISTS</Status>"
                    sReturnXML += "</ClaimResponse>"

                    XMLReturn.LoadXml(sReturnXML)
                    '-----------------------------------
                    ' Debugging
                    '-----------------------------------
                    If UCase(sAPDNetDebugging) = "TRUE" Then
                        APDFoundation.LogEvent("WSClientSideClaimProcessor", "XMLRET2CLIENT", "XML Returned to Client", "Client returned XML in EventXML", sReturnXML)
                    End If
                Else
                    ' New Claim process as normal
                    sReturnXML = APDFoundation.CreateAPDClaim(XMLWSClaim.InnerXml)
                    XMLReturn.LoadXml(sReturnXML)
                End If

                '-----------------------------------
                ' TVD - 04May2017 - Existing Claim 
                ' Check and see if the claim already
                ' exists in APD before processing
                '-----------------------------------
                ' End Code Modification
                '-----------------------------------

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUGGING", "Claim Create Attempted", "Results in EventXML", sReturnXML)
                End If

                '---------------------------------
                ' Test based on assignment source
                '---------------------------------
                Select Case sAssignmentSource
                    Case "WS"
                        If XMLReturn.SelectSingleNode("ClaimResponse/StatusID").InnerText <> "0" Then
                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR", "Client side claim failed to processed", "Returned data is in EventXML", sReturnXML)
                        Else
                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "SUCCESSFUL", "Client side claim processed", "LynxID returned data is in EventXML", sReturnXML)
                        End If
                    Case "CP"
                        If XMLReturn.SelectSingleNode("Root/Vehicle") Is Nothing Then
                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR", "Client side claim failed to processed", "Returned data is in EventXML", sReturnXML)
                        Else
                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "SUCCESSFUL", "Client side claim processed", "LynxID returned data is in EventXML", sReturnXML)
                        End If
                End Select

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer, "Client Side Claim process ended", "", "")
                End If

                Return Server.HtmlEncode(sReturnXML)
            Else
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "Client side validation failed: ", "Returned data is in EventXML", sReturnXML)
                Return "XML Validation Errors"
            End If


        Catch oExcept As Exception
            '--------------------------------------
            ' Notify of the Error - Collect params
            '--------------------------------------
            '------------------------------
            ' Build return error XML
            '------------------------------
            Dim sRetData As String = ""
            sRetData = "<ClaimResponse>"
            sRetData += "<LynxID></LynxID>"
            sRetData += "<InsuranceCompanyName>" & XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuranceCompanyID").InnerText & "</InsuranceCompanyName>"
            sRetData += "<CoverageClaimNumber>" & XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CoverageClaimNumber").InnerText & "</CoverageClaimNumber>"
            sRetData += "<CarrierRepNameFirst></CarrierRepNameFirst>"
            sRetData += "<CarrierRepNameLast></CarrierRepNameLast>"
            sRetData += "<CarrierRepPhoneDay></CarrierRepPhoneDay>"
            sRetData += "<CarrierRepEmailAddress></CarrierRepEmailAddress>"
            sRetData += "<StatusID>600</StatusID>"
            sRetData += "<Status>Claim failed to created successfully: Error: " & oExcept.Message & "</Status>"
            sRetData += "</ClaimResponse>"

            'Dim response1 As New XmlDocument
            'response1.LoadXml(sRetData)

            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "Client side validation failed: ", "Reason: " & oExcept.Message, sRetData)

            Return Server.HtmlEncode(sRetData)

        End Try
    End Function

    '-----------------------------------------
    ' These validation parameters are required
    ' for PS, CS and RRP transactions
    '-----------------------------------------
    Private Function ValidateClaimRep(ByVal XMLWSClaim As XmlDocument) As String
        Dim bRC As Boolean = True
        Dim XMLNodeValidate As XmlNode
        Dim sReturnXML As String = ""
        Dim sUserID As String = ""
        Dim sEmailAddress As String = ""
        Dim oXMLReturn As Object
        Dim oXMLClaimRepReturn As Object
        Dim sCarrierRepEmailAddress As String = ""
        Dim sCarrierRepNameFirst As String = ""
        Dim sCarrierRepNameLast As String = ""
        Dim sCarrierRepPhoneDay As String = ""
        Dim sPhoneAreaCode As String = ""
        Dim sPhoneExchangeNumber As String = ""
        Dim sPhoneUnitNumber As String = ""
        Dim iInsuranceCompanyID As Integer = 0
        Dim bNewClaim As Boolean = True
        Dim bUpdateCarrierRep As Boolean = False
        Dim sCarrierRepPhone As String = ""
        Dim aCarrierRepPhoneDay As Array = Nothing
        Dim sNewNameFirst As String = ""
        Dim sNewNameLast As String = ""
        Dim sNewPhone As String = ""

        Try
            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "START Validate - Server: " & sProcessingServer, "Client Side claim rep validate process started", "Claim XML passed-in is in EventXML", XMLWSClaim.InnerXml)

            '---------------------------------------
            ' Get InsuranceCompanyID from XML
            '---------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuranceCompanyID")
            iInsuranceCompanyID = CInt(XMLNodeValidate.InnerText)

            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/NewClaimFlag")
            If XMLNodeValidate.InnerText = "0" Then
                '---------------------------------------
                ' If NewClaimFlag = 0 then this is a 
                ' Vehicle Add we need to lookup the 
                ' claim rep ID from the existing claim
                '---------------------------------------
                bNewClaim = False

                '--------------------------------------------
                ' Get the LynxID and lookup the CarrierRepID
                '--------------------------------------------
                XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LynxID")
                If XMLNodeValidate.InnerText <> "" Then
                    oXMLClaimRepReturn = APDFoundation.GetClaimRepByLynxID(CInt(XMLNodeValidate.InnerText))
                    If oXMLClaimRepReturn(0).Errors <> "" Then
                        '---------------------------------------
                        ' Log Client Side Event
                        '---------------------------------------
                        APDFoundation.LogEvent("WSClientSideClaimProcessor", "Error", "Could not get the claim rep assigned to claim for LynxID: " & XMLNodeValidate.InnerText, "Error occured during the vehicle add.", "")

                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " did not return a valid carrier rep for LynxID " & XMLNodeValidate.InnerText & " When NewClaimFlag = 0 (Vehicle Add) the LynxID must be valid.")
                    Else
                        sUserID = oXMLClaimRepReturn(0).UserID
                    End If
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required when NewClaimFlag = 0 (Vehicle Add) and must be a valid LynxID.  Value received: " & XMLNodeValidate.InnerText)
                End If
            Else
                '---------------------------------------
                ' Not a Vehicle Add process normally
                '---------------------------------------
                bNewClaim = True

                '-----------------------------------------
                ' Check Claim Rep userid passed to see if 
                ' it's valid.
                '-----------------------------------------
                XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepUserID")
                If XMLNodeValidate.InnerText <> "" Then
                    sUserID = XMLNodeValidate.InnerText

                    '-----------------------------------------
                    ' Check if Claim Rep ID was passed, if so 
                    ' do a lookup and validate that it's valid.
                    '-----------------------------------------
                    oXMLReturn = APDFoundation.GetUserInfoByUserID(CInt(sUserID))

                    For Each oItem In oXMLReturn
                        If oItem.Errors <> "" Then
                            '---------------------------------------
                            ' Log Client Side Event
                            '---------------------------------------
                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "Validate", "Client Side CarrierRepUserID is not valid", "Claim rep " & sUserID & " was not found or is not enabled.", "CarrierRepUserID = " & sUserID)

                            '-----------------------------------------
                            ' Error occurred, notify user
                            '-----------------------------------------
                            bRC = False
                            'Throw New SystemException("<b>XMLValidationError:<br/>&nbsp;&nbsp;&nbsp;&nbsp; Tag:</b> " & XMLNodeValidate.Name & " Is not a valid claim representative userid.")
                        Else
                            '-----------------------------------------
                            ' Check returned xml to make sure the rep
                            ' userid is valid
                            '-----------------------------------------
                            If oItem.EnabledFlag = True And oItem.EmailAddress <> "" Then
                                '-----------------------------------------
                                ' User is valid
                                '-----------------------------------------
                                sUserID = oItem.UserID
                                bRC = True
                            Else
                                '---------------------------------------
                                ' Log Client Side Event
                                '---------------------------------------
                                APDFoundation.LogEvent("WSClientSideClaimProcessor", "Validate", "Client Side CarrierRepUserID is not valid", "Claim rep " & sUserID & " was not found or is not enabled.", "CarrierRepUserID = " & sUserID)

                                '-----------------------------------------
                                ' User is not valid
                                '-----------------------------------------
                                sUserID = ""
                                bRC = False
                            End If
                        End If
                    Next
                Else
                    '-----------------------------------------
                    ' User is not valid
                    '-----------------------------------------
                    bRC = False
                End If

                '-----------------------------------------
                ' If userid wasn't valid, lets try the email
                '-----------------------------------------
                If bRC = False Then
                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepEmailAddress")
                    If XMLNodeValidate.InnerText <> "" Then
                        sEmailAddress = XMLNodeValidate.InnerText

                        '-----------------------------------------
                        ' Check if Claim Rep Email was passed, if so 
                        ' do a lookup and validate that it's valid.
                        '-----------------------------------------
                        oXMLReturn = APDFoundation.GetUserInfoByEmail(sEmailAddress)

                        For Each oItem In oXMLReturn
                            If oItem.Errors <> "" Then
                                '---------------------------------------
                                ' Log Client Side Event
                                '---------------------------------------
                                APDFoundation.LogEvent("WSClientSideClaimProcessor", "Validate", "Client Side email address is not valid", "Claim rep " & sEmailAddress & " was not found or is not enabled.", "CarrierRepEmailAddress = " & sEmailAddress)

                                '-----------------------------------------
                                ' Error occurred, notify user
                                '-----------------------------------------
                                sUserID = ""
                                bRC = False
                                'Throw New SystemException("<b>XMLValidationError:<br/>&nbsp;&nbsp;&nbsp;&nbsp; Tag:</b> " & XMLNodeValidate.Name & " Is not a valid claim representative userid.")
                            Else
                                '-----------------------------------------
                                ' Check returned xml to make sure the rep
                                ' userid is valid
                                '-----------------------------------------
                                If oItem.EnabledFlag = True And oItem.EmailAddress <> "" Then
                                    '-----------------------------------------
                                    ' Check the existing carrier rep user info
                                    ' and update the first/last name or phone
                                    ' number if different
                                    '-----------------------------------------
                                    ' -- Check and make sure NameFirst/NameLast and Phone are ready for update 
                                    If XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameFirst").InnerText <> "" And XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameLast").InnerText <> "" And XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepPhoneDay").InnerText <> "" Then
                                        ' -- Check NameFirst Update 
                                        sNewNameFirst = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameFirst").InnerText
                                        If XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameFirst").InnerText <> oItem.NameFirst Then

                                            '---------------------------------------
                                            ' Log Client Side Event
                                            '---------------------------------------
                                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "CarrierUpdate", "Update Carrier Rep Name", "Carrier rep " & sEmailAddress & " First Name was updated.", "CarrierRepNameFirst = " & sNewNameFirst)
                                            bUpdateCarrierRep = True
                                        End If

                                        ' -- Check NameLast Update 
                                        sNewNameLast = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameLast").InnerText
                                        If XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameLast").InnerText <> oItem.NameLast Then

                                            '---------------------------------------
                                            ' Log Client Side Event
                                            '---------------------------------------
                                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "CarrierUpdate", "Update Carrier Rep Name", "Carrier rep " & sEmailAddress & " Last Name was updated.", "CarrierRepNameLast = " & sNewNameLast)
                                            bUpdateCarrierRep = True
                                        End If

                                        ' -- Check Phone Update 
                                        'sCarrierRepPhone = oItem.PhoneAreaCode & "-" & oItem.PhoneExchangeNumber & "-" & oItem.PhoneUnitNumber
                                        sNewPhone = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepPhoneDay").InnerText
                                        If XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepPhoneDay").InnerText <> oItem.PhoneCombined Then

                                            '---------------------------------------
                                            ' Log Client Side Event
                                            '---------------------------------------
                                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "CarrierUpdate", "Update Carrier Rep Phone", "Carrier rep " & sEmailAddress & " Phone Number was updated.", "CarrierRepPhone = " & sNewPhone)
                                            bUpdateCarrierRep = True
                                        End If

                                        '-----------------------------------------
                                        ' Update Carrier Rep Information
                                        '-----------------------------------------
                                        If bUpdateCarrierRep = True Then
                                            '-- Parse Phone --
                                            If sNewPhone = "" Then
                                                sNewPhone = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepPhoneDay").InnerText
                                            End If

                                            aCarrierRepPhoneDay = sNewPhone.Split("-")

                                            '---------------------------------------
                                            ' Log Client Side Event
                                            '---------------------------------------
                                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUGGING", "DEBUGGING", "DEBUGGING " & sEmailAddress & " Phone Area Code: " & aCarrierRepPhoneDay(0), sNewPhone)

                                            Call UpdateCarrierRep(sEmailAddress, sNewNameFirst, sNewNameLast, aCarrierRepPhoneDay(0), aCarrierRepPhoneDay(1), aCarrierRepPhoneDay(2), iInsuranceCompanyID)
                                        End If
                                    End If

                                    '-----------------------------------------
                                    ' User is valid, save userid returned into
                                    ' the request XML
                                    '-----------------------------------------
                                    'XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepUserID")
                                    'XMLNodeValidate.InnerText = oItem.UserID
                                    sUserID = oItem.UserID
                                    bRC = True
                                Else
                                    '---------------------------------------
                                    ' Log Client Side Event
                                    '---------------------------------------
                                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "Validate", "Client Side email address is not valid", "Claim rep " & sEmailAddress & " was not found or is not enabled.", "CarrierRepEmailAddress = " & sEmailAddress)

                                    '-----------------------------------------
                                    ' User is not valid
                                    '-----------------------------------------
                                    sUserID = ""
                                    bRC = False
                                End If
                            End If
                        Next
                    End If
                End If

                '-----------------------------------------
                ' If userid and Email wasn't valid, 
                ' create a new user.
                '-----------------------------------------
                If bRC = False Then
                    '-----------------------------------------
                    ' Check to see if all the other parameters
                    ' are passed so that we can create the new
                    ' claim rep
                    '-----------------------------------------

                    '---------------------------------------------
                    ' Check if CarrierRepEmailAddress is provided
                    '---------------------------------------------
                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepEmailAddress")
                    If XMLNodeValidate.InnerText <> "" Then
                        sCarrierRepEmailAddress = XMLNodeValidate.InnerText
                        bRC = True
                    Else
                        bRC = False
                    End If

                    '---------------------------------------------
                    ' Check if CarrierRepNameFirst is provided
                    '---------------------------------------------
                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameFirst")
                    If XMLNodeValidate.InnerText <> "" Then
                        sCarrierRepNameFirst = XMLNodeValidate.InnerText
                        bRC = True
                    Else
                        bRC = False
                    End If

                    '---------------------------------------------
                    ' Check if CarrierRepNameLast is provided
                    '---------------------------------------------
                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameLast")
                    If XMLNodeValidate.InnerText <> "" Then
                        If (Len(XMLNodeValidate.InnerText) > 0) Then
                            If (Len(XMLNodeValidate.InnerText) > 50) Then
                                bRC = False
                                Throw New SystemException("Invalid carrier rep last name.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                            End If
                        Else
                            If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9',.\s-]+$") Then
                                bRC = False
                                Throw New SystemException("Invalid carrier rep last name.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                            End If
                        End If

                        sCarrierRepNameLast = XMLNodeValidate.InnerText
                        bRC = True
                    Else
                        bRC = False
                    End If

                    '---------------------------------------------
                    ' Check if CarrierRepPhoneDay is provided
                    '---------------------------------------------
                    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepPhoneDay")
                    If XMLNodeValidate.InnerText <> "" Then
                        sCarrierRepPhoneDay = XMLNodeValidate.InnerText
                        bRC = True
                    Else
                        bRC = False
                    End If

                    '-----------------------------------------
                    ' If parameters don't exist, return an error
                    ' to client
                    '-----------------------------------------
                    If bRC = False Then
                        '---------------------------------------
                        ' Log Client Side Event
                        '---------------------------------------
                        APDFoundation.LogEvent("WSClientSideClaimProcessor", "Validate", "Client Side claim representative does not exist and required fields to create one are not in the passed XML.", "Notified client to update XML and resubmit.", XMLWSClaim.InnerXml)

                        Throw New SystemException("XMLValidationError: Tags: CarrierRepEmailAddress, CarrierRepNameFirst, CarrierRepNameLast, CarrierRepPhoneAreaCode, CarrierRepPhoneExchangeNumber and CarrierRepPhoneUnitNumber are required fields to create a new Carrier Rep.")
                    Else
                        '---------------------------------------
                        ' Log Client Side Event
                        '---------------------------------------
                        APDFoundation.LogEvent("WSClientSideClaimProcessor", "Create Rep", "Client Side create new carrier rep process started", "Claim rep " & sEmailAddress, XMLWSClaim.InnerXml)

                        '---------------------------------------
                        ' Create new carrier rep
                        '---------------------------------------
                        If sCarrierRepPhoneDay.Contains("-") Then
                            sPhoneAreaCode = sCarrierRepPhoneDay.Substring(0, 3)
                            sPhoneExchangeNumber = sCarrierRepPhoneDay.Substring(4, 3)
                            sPhoneUnitNumber = sCarrierRepPhoneDay.Substring(8, 4)
                        Else
                            sPhoneAreaCode = sCarrierRepPhoneDay.Substring(0, 3)
                            sPhoneExchangeNumber = sCarrierRepPhoneDay.Substring(3, 3)
                            sPhoneUnitNumber = sCarrierRepPhoneDay.Substring(6, 4)
                        End If

                        '----------------------------------
                        ' Create new carrier user
                        '----------------------------------
                        'oXMLReturn = APDFoundation.AdminCreateClaimRep(sCarrierRepEmailAddress, sCarrierRepNameFirst, sCarrierRepNameLast, sPhoneAreaCode, sPhoneExchangeNumber, "", sPhoneUnitNumber)
                        sUserID = APDFoundation.AdminCreateClaimRep(sCarrierRepEmailAddress, sCarrierRepNameFirst, sCarrierRepNameLast, sPhoneAreaCode, sPhoneExchangeNumber, "", sPhoneUnitNumber, iInsuranceCompanyID)

                        If sUserID.Contains("Error:") Then
                            '---------------------------------------
                            ' Log Client Side Event
                            '---------------------------------------
                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "Error", "Client Side claim rep create failed", "Claim rep " & sEmailAddress & " was not created successfully.", sUserID)
                            bRC = False
                            Throw New SystemException(sUserID)
                        Else
                            '---------------------------------------
                            ' Log Client Side Event
                            '---------------------------------------
                            APDFoundation.LogEvent("WSClientSideClaimProcessor", "SUCCESSFUL", "Client Side create claim rep processed successfully", "Claim rep = " & sEmailAddress & " with a UserID = " & sUserID & ", InsuranceCompanyID = " & iInsuranceCompanyID, sUserID)

                            '---------------------------------------
                            ' Notify email group that a new carrier
                            ' rep was created and assigned to the
                            ' first office and must be reviewed
                            '---------------------------------------
                            Dim sBody As String = ""

                            sBody = AppSettings("CarrierRepCreated_Mail_Body")
                            sBody += "<br/>Carrier Rep = " & sCarrierRepNameFirst & " " & sCarrierRepNameLast
                            sBody += "<br/>Email Address = " & sCarrierRepEmailAddress
                            sBody += "<br/>Phone = " & sCarrierRepPhoneDay

                            APDFoundation.SendMail(
                                AppSettings("CarrierRepCreated_Mail_TO"),
                                AppSettings("CarrierRepCreated_Mail_FROM"),
                                AppSettings("CarrierRepCreated_Mail_BCC"),
                                AppSettings("CarrierRepCreated_Mail_Subject"),
                                sBody,
                                AppSettings("SMTPServer")
                            )


                            bRC = True
                        End If

                        'For Each oItem In oXMLReturn
                        '    If oItem.Errors <> "" Then
                        '        '---------------------------------------
                        '        ' Log Client Side Event
                        '        '---------------------------------------
                        '        APDFoundation.LogEvent("WSClientSideClaimProcessor", "Validate", "Client Side claim rep create failed", "Claim rep " & sEmailAddress & " was not created successfully.", oXMLReturn.InnerXML)

                        '        '-----------------------------------------
                        '        ' Error occurred, notify user
                        '        '-----------------------------------------
                        '        sUserID = ""
                        '        bRC = False
                        '        'Throw New SystemException("<b>XMLValidationError:<br/>&nbsp;&nbsp;&nbsp;&nbsp; Tag:</b> " & XMLNodeValidate.Name & " Is not a valid claim representative userid.")
                        '    Else
                        '        '-----------------------------------------
                        '        ' Check returned xml to make sure the rep
                        '        ' userid was created succesfully
                        '        '-----------------------------------------
                        '        If oItem.UserID <> "" Then
                        '            '-----------------------------------------
                        '            ' User was created, save userid returned into
                        '            ' the request XML
                        '            '-----------------------------------------
                        '            sUserID = oItem.UserID
                        '            bRC = True
                        '        Else
                        '            '---------------------------------------
                        '            ' Log Client Side Event
                        '            '---------------------------------------
                        '            APDFoundation.LogEvent("WSClientSideClaimProcessor", "Validate", "Client Side claim rep create failed", "Claim rep " & sEmailAddress & " was not created successfully.", oXMLReturn.InnerXML)

                        '            '-----------------------------------------
                        '            ' User is not valid
                        '            '-----------------------------------------
                        '            sUserID = ""
                        '            bRC = False
                        '        End If
                        '    End If
                        'Next
                    End If


                End If
            End If

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "END Validate - Server: " & sProcessingServer, "Client Side claim rep validate process ended", "", "Return Code: " & CStr(bRC) & " - UserID: " & sUserID)

            '-----------------------------------
            ' Return the userid
            '-----------------------------------
            If bRC = True Then
                Return sUserID
            Else
                Return ""
            End If

        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            If oExcept.Message.Contains("XMLValidationError") Then
                sError = oExcept.Message
            Else
                sError = String.Format("Error: {0}", "WebSerices Failed: Validate Claim Rep failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            End If

            '------------------------------
            ' Log the results 
            '------------------------------
            'sError

            Return ""
        End Try
    End Function

    '-----------------------------------------
    ' These validation parameters are required
    ' for both PS and RRP transactions
    '-----------------------------------------
    Private Function ValidateClaim(ByVal XMLWSClaim As XmlDocument, ByVal sAssignmentDescription As String) As Boolean
        Dim bRC As Boolean = True
        Dim XMLNodeValidate As XmlNode
        Dim XMLVehicleNodeValidate As XmlNodeList
        'Dim XMLVehicleExposureNodeValidate As XmlNode
        Dim XMLNode As XmlNode
        Dim XMLVehicleNode As XmlNode
        'Dim XMLVehicleNodeUpdate As XmlNodeList
        'Dim iClientCoverageTypeID As Integer
        Dim iCnt As Integer = 0
        Dim iInsuranceCompanyID As Integer = 0
        Dim iVehicleNumber As Integer = 0
        Dim bRental As Boolean = False
        Dim bNewClaim As Boolean = True
        Dim iLynxID As Integer = 0
        Dim oXMLReturn As Object
        Dim bFirstPartyExposureExists As Boolean = False
        Dim iMaxVehicleNumber As Integer = 0
        Dim bClientFriendlyMessage As Boolean = False

        Dim reg As New Regex("")

        '---------------------------------------
        ' Log Client Side Event
        '---------------------------------------
        APDFoundation.LogEvent("WSClientSideClaimProcessor", "START Validate - Server: " & sProcessingServer, "Client Side claim validate process started", "Claim XML passed-in is in EventXML", XMLWSClaim.InnerXml)

        '========================================
        ' Handle all common Claim validation 1st
        '========================================

        '---------------------------------------
        ' Get InsuranceCompanyID for future use
        '---------------------------------------
        iInsuranceCompanyID = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuranceCompanyID").InnerText

        '-----------------------------------------
        ' NewClaimFlag REQUIRED and Must be boolean and 
        ' either 0-Meaning update of vechicle info by
        ' LynxID or a 1-New Claim
        '-----------------------------------------
        XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/NewClaimFlag")
        If (XMLNodeValidate.InnerText = "0") Or (XMLNodeValidate.InnerText = "1") Then
            '---------------------------------------
            ' If NewClaimFlag = 0 then this is a 
            ' Vehicle Add and the Claim validation
            ' is completely different
            '---------------------------------------
            Select Case XMLNodeValidate.InnerText
                Case "0"
                    bNewClaim = False
                Case Else
                    bNewClaim = True
            End Select
        Else
            bRC = False
            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required must be one of the following values:  0 = False, 1 = True.  Value received: " & XMLNodeValidate.InnerText)
        End If

        '-----------------------------------------
        ' Validation for ALL types of Assignments
        '-----------------------------------------
        '-----------------------------------------
        ' DataSource must be "Web Services Assignment"
        '-----------------------------------------
        XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/DataSource")

        '-----------------------------------------
        ' 23Oct2013 - TVD - Modified to handle
        ' ClaimPoint Assignments as well
        '-----------------------------------------
        Select Case UCase(XMLNodeValidate.InnerText)
            Case "WEB SERVICES ASSIGNMENT"
                'sClaimSource = "WS"
            Case "CLAIM POINT WEB ASSIGNMENT"
                bClientFriendlyMessage = True
                'sClaimSource = "CLMPT"
            Case Else
                bRC = False
                Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required and must be 'Web Services Assignment', but instead was: " & XMLNodeValidate.InnerText)
        End Select

        '-------------------------------------------------
        ' InsuranceCompanyID REQUIRED and Must be numeric 
        ' and a valid insurance company ID
        '-------------------------------------------------
        XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuranceCompanyID")
        If (XMLNodeValidate.InnerText = "") Then
            bRC = False

            If bClientFriendlyMessage Then
                Throw New SystemException("Invalid insurance company received.  Value received: " & XMLNodeValidate.InnerText)
            Else
                Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required and must be a valid insurance company ID.  Value received: " & XMLNodeValidate.InnerText)
            End If
        Else
            If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^\d+$") Then
                bRC = False

                If bClientFriendlyMessage Then
                    Throw New SystemException("Invalid insurance company received.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If
        End If

        '-------------------------------------------------
        ' FNOLUserID REQUIRED and Must be numeric and a 
        ' valid userid
        '-------------------------------------------------
        XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/FNOLUserID")
        If (XMLNodeValidate.InnerText = "") Then
            bRC = False
            If bClientFriendlyMessage Then
                Throw New SystemException("Invalid FNOL userid received.  Value received: " & XMLNodeValidate.InnerText)
            Else
                Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required and must be a valid userid.  Value received: " & XMLNodeValidate.InnerText)
            End If
        Else
            If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                bRC = False

                If bClientFriendlyMessage Then
                    Throw New SystemException("Invalid FNOL userid received.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If
        End If

        '-----------------------------------------
        ' Validation for ONLY NEW Assignments
        '-----------------------------------------
        If bNewClaim = True Then
            '-------------------------------------------------
            ' NoticeMethodID REQUIRED and Must a 5 = ???
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/NoticeMethodID")
            If (XMLNodeValidate.InnerText <> "5") Then
                bRC = False

                If bClientFriendlyMessage Then
                    Throw New SystemException("Invalid notification type received.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required must be 5.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '-----------------------------------------
            ' DemoFlag Must be "0"
            '-----------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/DemoFlag")
            Select Case XMLNodeValidate.InnerText
                Case "0"
                Case "1"
                Case Else
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid demo flag received.  Must be a 0 or 1.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " Must be a 0 or 1, but instead was: " & XMLNodeValidate.InnerText)
                    End If
            End Select

            '-----------------------------------------
            ' AssignmentDescription must be a valid
            ' assignment type Repair Referral, 
            ' Program Shop
            '-----------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/AssignmentDescription")
            Select Case UCase(XMLNodeValidate.InnerText)
                Case "REPAIR REFERRAL"
                Case "PROGRAM SHOP"
                    'Case "DEMAND ESTIMATE AUDIT"
                Case Else
                    bRC = False
                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid assignment.  Must be REPAIR REFERRAL or PROGRAM SHOP.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required and must be 'Repair Referral' or 'Program Shop', but instead was: " & XMLNodeValidate.InnerText)
                    End If
            End Select

            '------------------------------------------------------------------
            ' CarrierOfficeName Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierOfficeName")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False
                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid office name.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid office name.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CarrierRepNameFirst Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameFirst")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False
                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid carrier rep first name.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid carrier rep first name.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CarrierRepNameLast Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameLast")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid carrier rep last name.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9',.\s-]+$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid carrier rep last name.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CarrierRepUserID Must be Numeric
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepUserID")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid carrier rep userid.  Must be numeric.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be Numeric.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CarrierRepPhoneDay Must be Phone formatted as xxxxxxxxxx
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepPhoneDay")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 15) Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid carrier rep phone.  Must be no longer then 15 numbers.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must not be longer than 15 numbers: .  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If

                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]+$") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid carrier rep phone.  Must be in the format xxx-xxx-xxxx.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be in the format xxx-xxx-xxxx.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CarrierRepEmailAddress Must be Email formatted xxx@yyy.zzz
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepEmailAddress")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "[^@]+@[^\.]+\..+$") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid carrier rep email address.  Must be in the format xxxx@yyyyy.zzz.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be in the format xxxx@yyyyy.zzz.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CarrierRepNameLast if supplied Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierName")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid carrier name.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9',.\s-]+$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid carrier name.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CoverageClaimNumber REQUIRED and Must be Alphanumeric and no more 
            ' then 30 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CoverageClaimNumber")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 30) Then
                bRC = False

                If bClientFriendlyMessage Then
                    Throw New SystemException("Invalid claim number.  Must be no longer than 30 characters.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required must be no longer than 30 characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '--------------------------------------------------------------
            ' LossDate REQUIRED and Must be a date in the form mm/dd/yyyy
            '--------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LossDate")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 10) Then
                bRC = False

                If bClientFriendlyMessage Then
                    Throw New SystemException("Invalid loss date.  Must be a date in the format mm/dd/yyyy and no longer than 10 digits.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required must be a date in the format mm/dd/yyyy and no longer than 10 digits.  Value received: " & XMLNodeValidate.InnerText)
                End If
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid loss date.  Must be a date in the format mm/dd/yyyy and cannot contain special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be a date in the format mm/dd/yyyy and cannot contain special characters other than /.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-----------------------------------------
            ' LossAddressState if supplied Must be a 
            ' valid state
            '-----------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LossAddressState")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 2) Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid loss state.  Must be no longer than 2 characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 2 characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(?-i:A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid loss state.  Must be Alpha and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alpha and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CallerNameFirst if supplied Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerNameFirst")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid caller first name.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9()\s-]+$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid caller first name.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CallerNameLast if supplied Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerNameLast")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid caller last name.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9',.\s-]+$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid caller last name.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '-----------------------------------------------
            ' CallerRelationToInsuredID if supplied and must 
            ' be numeric between 0 and 14
            '-----------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerRelationToInsuredID")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 2) Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid caller relation.  Must be no longer than 2 characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 2 integers.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^([0-9]|[0,1][0-4])$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid caller relation.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be one of the following numeric values: 0 = Unknown, 1 = Agent, 2 = Attorney, 3 = Child, 4 = Doctor, 5 = Employer, 6 = Friend, 7 = Insured, 8 = Parent, 9 = Spouce, 10 = Third Party, 11 = Third Party Agent, 12 = Third Party Attorney, 13 = Other, 14 = Carrier Representative.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' CallerRelationToInsuredIDDescription if supplied Must be 
            ' Alphanumeric and no more then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerRelationToInsuredIDDescription")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid caller relation description.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid caller relation description.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' InsuredNameFirst is Required Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredNameFirst")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 50) Then
                bRC = False

                If bClientFriendlyMessage Then
                    Throw New SystemException("Invalid insured first name.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid insured first name.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' InsuredNameLast is Required Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredNameLast")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 50) Then
                bRC = False

                If bClientFriendlyMessage Then
                    Throw New SystemException("Invalid insured last name.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9',.\s-]+$") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid insured last name.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' InsuredPhone Must be Phone formatted as xxxxxxxxxx
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredPhone")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 15) Then
                bRC = False

                If bClientFriendlyMessage Then
                    Throw New SystemException("Invalid insured phone.  Must be no longer than 15 characters.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must not be longer than 15 numbers: .  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                bRC = False

                If bClientFriendlyMessage Then
                    Throw New SystemException("Invalid insured phone.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' InsuredBusinessName if supplied Must be 
            ' Alphanumeric and no more then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredBusinessName")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid business name.  Must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                        bRC = False

                        If bClientFriendlyMessage Then
                            Throw New SystemException("Invalid business name.  Must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        Else
                            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                        End If
                    End If
                End If
            End If

            '-------------------------------------------------
            ' CollisionDeductibleAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CollisionDeductibleAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                'If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                If (Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)")) Or (Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\d{1,2}$)")) Then
                Else
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid collision deductible amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' CollisionLimitAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CollisionLimitAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid collision limit amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' ComprehensiveDeductibleAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ComprehensiveDeductibleAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid comprehensive deductible amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' ComprehensiveLimitAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ComprehensiveLimitAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid comprehensive limit amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' LiabilityDeductibleAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LiabilityDeductibleAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid liability deductible amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' LiabilityLimitAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LiabilityLimitAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid liability limit amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' UnderInsuredDeductibleAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/UnderInsuredDeductibleAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid underinsured deductible amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' UnderInsuredLimitAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/UnderInsuredLimitAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid underinsured limit amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' UnInsuredDeductibleAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/UnInsuredDeductibleAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid uninsured deductible amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' UnInsuredLimitAmt if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/UnInsuredLimitAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "(^\d*\.\d{1,2}$)") Then
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Invalid uninsured limit amt.  Must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' TimeStarted if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/TimeStarted")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(0[1-9]|1[012])/(0[1-9]|[12][0-9]|3[01])/\d\d+ (0[0-9]|1[0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])+ [AM|PM]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must in the format: mm/dd/yyyy hh:mm:ss AM|PM.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '-------------------------------------------------
            ' TimeFinished if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/TimeFinished")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(0[1-9]|1[012])/(0[1-9]|[12][0-9]|3[01])/\d\d+ (0[0-9]|1[0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])+ [AM|PM]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must in the format: mm/dd/yyyy hh:mm:ss AM|PM.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '-------------------------------------------------
            ' IntakeStartSeconds if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/IntakeStartSeconds")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '-------------------------------------------------
            ' IntakeEndSeconds if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/IntakeEndSeconds")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '-------------------------------------------------
            ' IntakeSeconds if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/IntakeSeconds")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' InsuredPhoneSumm Must be Phone formatted as xxx-xxx-xxxx
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredPhoneSumm")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 15) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must not be longer than 15 numbers: .  Value received: " & XMLNodeValidate.InnerText)
                End If

                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be in the format xxx-xxx-xxxx.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' RentalAuthorized if supplied must be Yes or No
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalAuthorized")
            If (XMLNodeValidate.InnerText = "") Then
                XMLNodeValidate.InnerText = "NO"
                bRental = False
            Else
                If Not Regex.IsMatch(UCase(XMLNodeValidate.InnerText), "^(YES|NO)$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be Yes or No.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' Rentals, if either of the dollar amount fields are populate, then
            ' Rentals is a authorized.
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalAuthorized")
            If (XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalDayAmount").InnerText <> "") And (XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalMaxAmount").InnerText <> "") Then
                XMLNodeValidate.InnerText = "YES"
                bRental = True
            Else
                XMLNodeValidate.InnerText = "NO"
                bRental = False
            End If

            '------------------------------------------------------------------
            ' Rentals 
            '------------------------------------------------------------------
            If bRental Then
                '-------------------------------------
                ' Rental Days
                '-------------------------------------
                XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalDays")
                If (Len(XMLNodeValidate.InnerText) > 0) Then
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                        bRC = False
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If

                '-------------------------------------
                ' Rental Days Authorized
                '-------------------------------------
                XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalDaysAuthorized")
                If (Len(XMLNodeValidate.InnerText) > 0) Then
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                        bRC = False
                        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '-------------------------------------------------
            ' Mileage if supplied and Must be 
            ' numeric
            '-------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/Mileage")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' LossDescription if supplied Must be 
            ' Alphanumeric and no more then 1000 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LossDescription")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 1000) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 1000 characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' Remarks if supplied Must be 
            ' Alphanumeric and no more then 1000 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/Remarks")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 1000) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 1000 characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' RentalInstructions if supplied Must be 
            ' Alphanumeric and no more then 1000 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalInstructions")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 1000) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 1000 characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If
        Else
            '------------------------------------------------------------------
            ' This is a Vehicle add and the LynxID is a required field
            '------------------------------------------------------------------
            XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LynxID")
            If (XMLNodeValidate.InnerText = "") Then
                bRC = False
                Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required when NewClaimFlag = 0 (Vehicle Add) and must be a valid LynxID.  Value received: " & XMLNodeValidate.InnerText)
            Else
                iLynxID = XMLNodeValidate.InnerText
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^\d+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

        End If

        '========================================
        '========================================
        ' Handle all assignment specific vehicle
        ' validation 
        '========================================
        '========================================
        ' --- Build loop for multiple vehicles
        '-----------------------------------------
        ' AssignmentTypeID must be a valid
        ' assignment type Repair Referral, 
        ' Program Shop
        '-----------------------------------------
        bRental = False

        XMLVehicleNodeValidate = XMLWSClaim.SelectNodes("WebAssignment/Vehicle/AssignmentTypeID")
        For Each XMLNode In XMLVehicleNodeValidate
            '-----------------------------------------
            ' Validate what type of assignment we are
            '-----------------------------------------
            Select Case UCase(XMLNode.InnerText)
                Case "4"
                    ' 4 = Program Shop
                Case "16"
                    ' 16 = Repair Referral
                Case "17"
                    ' 17 = Choice Program Shop
                Case Else
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNode.Name & " is required and must be one of the following numeric values: 4 = Program Shop, 16 = Repair Referral, 17 - Choice Program Shop, but instead was: " & XMLNode.InnerText)
            End Select
        Next

        '---------------------------------------------
        ' Check if this is a New claim or vehicle add
        '---------------------------------------------
        If bNewClaim = True Then
        Else
            oXMLReturn = APDFoundation.GetClaimAspectByLynxID(iLynxID)

            For Each oItem In oXMLReturn
                If oItem.Errors <> "" Then
                    '---------------------------------------
                    ' Log Client Side Event
                    '---------------------------------------
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "Error", "Client Side call to server side GetClaimAspectByLynxID failed.", "An error was returned for LynxID = " & iLynxID & " while trying to get the claim aspects.", "LynxID = " & iLynxID)

                    '-----------------------------------------
                    ' Error occurred, notify user
                    '-----------------------------------------
                    bRC = False
                    Throw New SystemException("XMLValidationError: Error: validating the Exposures for the vehicle add.  LynxID = " & iLynxID & " failed while returning the Claim Aspect data.")
                Else
                    '---------------------------------------------
                    ' Check and see if we already have a 1st party
                    ' exposure vehicle in the claim.
                    '---------------------------------------------
                    If oItem.ExposureCD = "1" Then
                        bFirstPartyExposureExists = True
                    End If

                    '--------------------------------------------- 
                    ' Check the vehicle numbers in the claim and
                    ' make sure we have a valid vehicle number
                    '---------------------------------------------
                    If oItem.ClaimAspectNumber > iMaxVehicleNumber Then
                        iMaxVehicleNumber = oItem.ClaimAspectNumber
                    End If

                End If
            Next
        End If

        XMLVehicleNodeValidate = XMLWSClaim.SelectNodes("WebAssignment/Vehicle")
        For Each XMLNode In XMLVehicleNodeValidate
            '-----------------------------------------
            ' Get the vehicle number for use later
            '-----------------------------------------
            iVehicleNumber = CInt(XMLNode.SelectSingleNode("VehicleNumber").InnerText)

            '--------------------------------------------
            ' Verify that the CoverageProfileCD is valid
            '--------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("CoverageProfileCD")
            Select Case UCase(XMLNodeValidate.InnerText)
                Case "COLL"
                Case "COMP"
                Case "LIAB"
                Case "UIM"
                Case "UM"
                Case Else
                    bRC = False

                    If bClientFriendlyMessage Then
                        Throw New SystemException("Coverage must be one of the following: Collision, Comprehensive, Liability, Underinsured, Uninsured.  Value received: " & XMLNodeValidate.InnerText)
                    Else
                        Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be on of the following: COLL = Collision, COMP = Comprehensive, LIAB = Liability, UIM = Underinsured, UM = Uninsured.  Value received: " & XMLNodeValidate.InnerText)
                    End If
            End Select

            '-----------------------------------------
            ' Check exposures.  You can only have 
            ' 1 - 1st Party
            ' Many - 3rd Party
            '-----------------------------------------
            XMLVehicleNode = XMLNode.SelectSingleNode("ExposureCD")
            Select Case XMLVehicleNode.InnerText
                Case "1"
                    '-----------------------------------------
                    ' Count 1st party exposures
                    '-----------------------------------------
                    iCnt += 1
                Case "3"
                    '-----------------------------------------
                    ' If 3rd party, then they have to be
                    ' liability coverage only
                    '-----------------------------------------
                    XMLNodeValidate = XMLNode.SelectSingleNode("CoverageProfileCD")
                    If UCase(XMLNodeValidate.InnerText) <> "LIAB" Then
                        bRC = False
                        Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " Must be LIAB for 3rd party exposures.  Value received: " & XMLNodeValidate.InnerText)
                    End If
            End Select

            '-------------------------------------------------
            ' Verify that we only have one 1st party exposure
            ' for either New Claims or Add Vehicles
            '-------------------------------------------------
            If (iCnt > 1) Or (iCnt > 0 And bFirstPartyExposureExists) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNode.Name & " - You can only have one 1st party exposure.  More than one was received.")
            End If

            '------------------------------------------------------------------
            ' OwnerNameFirst is Required Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("OwnerNameFirst")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 50) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' OwnerNameLast is Required Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("OwnerNameLast")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 50) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9',.\s-]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' OwnerBusinessName must not contain special characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("OwnerBusinessName")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must not be longer than 50 numbers: .  Value received: " & XMLNodeValidate.InnerText)
                End If

                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' ContactNameFirst is not Required if supplied must be Alphanumeric 
            ' and no more then 50 characters. If blank, owner first name will
            ' be substituted.
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("ContactNameFirst")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                        bRC = False
                        Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' ContactNameLast is not Required if supplies must be Alphanumeric 
            ' and no more then 50 characters. If blank, owner last name will
            ' be substituted.
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("ContactNameLast")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 50) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9',.\s-]+$") Then
                        bRC = False
                        Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' VIN is Required Must be Alphanumeric and no more 
            ' then 17 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("VIN")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 17) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 17 characters.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' LicensePlateNumber is Required Must be Alphanumeric and no more 
            ' then 10 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("LicensePlateNumber")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 10) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 10 characters.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' VehicleYear is Required Must be Numeric and no more 
            ' then 4 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("VehicleYear")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 4) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 4 characters.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be Numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' Make is Required Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("Make")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 50) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' Model is Required Must be Alphanumeric and no more 
            ' then 50 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("Model")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 50) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 50 characters.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^[A-Za-z0-9\s-]+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be Alphanumeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' Drivable is Required Must be Numeric and must be a 0 = No or
            ' 1 = Yes
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("Drivable")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 1) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 1 characters.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(0|1)$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be 0 = No or 1 = Yes.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' PrimaryDamage is Required Must be Numeric and no more 
            ' then 1 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("PrimaryDamage")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 2) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be no longer than 2 characters.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If (CInt(XMLNodeValidate.InnerText) > 38) _
                    Or (CInt(XMLNodeValidate.InnerText) = 21) _
                    Or (CInt(XMLNodeValidate.InnerText) = 23) _
                    Or (CInt(XMLNodeValidate.InnerText) = 24) _
                    Or (CInt(XMLNodeValidate.InnerText) = 25) _
                    Or (CInt(XMLNodeValidate.InnerText) = 26) _
                    Or (CInt(XMLNodeValidate.InnerText) = 28) _
                    Or (CInt(XMLNodeValidate.InnerText) = 29) _
                    Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name &
                        " must be one of the following: 0 - Unknown, " _
                       & "1 - Front Corner Driver's Side, " _
                       & "2 - Front Bumper Driver's Side, " _
                       & "3 - Front Bumper Passenger's Side, " _
                       & "4 - Front Corner Passenger's Side, " _
                       & "5 - Front Fender Passenger's Side, " _
                       & "6 - Passenger's Side, " _
                       & "7 - Rear Quarterpanel Passenger's Side, " _
                       & "8 - Rear Corner Passenger's Side, " _
                       & "9 - Rear Bumper Passenger's Side, " _
                       & "10 - Rear Bumper Driver's Side, " _
                       & "11 - Rear Corner Driver's Side, " _
                       & "12 - Rear Quarterpanel Driver's Side, " _
                       & "13 - Driver's Side, " _
                       & "14 - Front Fender Driver's Side, " _
                       & "15 - Trunk, " _
                       & "16 - Roof, " _
                       & "17 - Hood, " _
                       & "18 - Undercarriage, " _
                       & "20 - Interior, " _
                       & "22 - Sun/Moon Roof, " _
                       & "27 - Rollover, " _
                       & "30 - Other (Details in Notes), " _
                       & "31 - Front, " _
                       & "32 - Front Bumper, " _
                       & "33 - Front Door Driver's Side, " _
                       & "34 - Front Door Passenger's Side, " _
                       & "35 - Rear Door Driver's Side, " _
                       & "36 - Rear Door Passenger's Side, " _
                       & "37 - Rear Bumper, " _
                       & "38 - Rear, " _
                       & ".  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' SecondaryDamage if is supplied must be Numeric and no more 
            ' then 1 characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("SecondaryDamage")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 2) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied and must be no longer than 2 characters.  Value received: " & XMLNodeValidate.InnerText)
                Else
                    If (CInt(XMLNodeValidate.InnerText) > 38) _
                        Or (CInt(XMLNodeValidate.InnerText) = 21) _
                        Or (CInt(XMLNodeValidate.InnerText) = 23) _
                        Or (CInt(XMLNodeValidate.InnerText) = 24) _
                        Or (CInt(XMLNodeValidate.InnerText) = 25) _
                        Or (CInt(XMLNodeValidate.InnerText) = 26) _
                        Or (CInt(XMLNodeValidate.InnerText) = 28) _
                        Or (CInt(XMLNodeValidate.InnerText) = 29) _
                        Then
                        bRC = False
                        Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name &
                            " if supplied must be one of the following: 0 - Unknown, " _
                           & "1 - Front Corner Driver's Side, " _
                           & "2 - Front Bumper Driver's Side, " _
                           & "3 - Front Bumper Passenger's Side, " _
                           & "4 - Front Corner Passenger's Side, " _
                           & "5 - Front Fender Passenger's Side, " _
                           & "6 - Passenger's Side, " _
                           & "7 - Rear Quarterpanel Passenger's Side, " _
                           & "8 - Rear Corner Passenger's Side, " _
                           & "9 - Rear Bumper Passenger's Side, " _
                           & "10 - Rear Bumper Driver's Side, " _
                           & "11 - Rear Corner Driver's Side, " _
                           & "12 - Rear Quarterpanel Driver's Side, " _
                           & "13 - Driver's Side, " _
                           & "14 - Front Fender Driver's Side, " _
                           & "15 - Trunk, " _
                           & "16 - Roof, " _
                           & "17 - Hood, " _
                           & "18 - Undercarriage, " _
                           & "20 - Interior, " _
                           & "22 - Sun/Moon Roof, " _
                           & "27 - Rollover, " _
                           & "30 - Other (Details in Notes), " _
                           & "31 - Front, " _
                           & "32 - Front Bumper, " _
                           & "33 - Front Door Driver's Side, " _
                           & "34 - Front Door Passenger's Side, " _
                           & "35 - Rear Door Driver's Side, " _
                           & "36 - Rear Door Passenger's Side, " _
                           & "37 - Rear Bumper, " _
                           & "38 - Rear, " _
                           & ".  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' Check to make sure Primary and Secondary impacts are not the same
            '------------------------------------------------------------------
            If XMLNode.SelectSingleNode("PrimaryDamage").InnerText = XMLNode.SelectSingleNode("SecondaryDamage").InnerText Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: PrimaryDamage and SecondaryDamage can not be the same value.  Value received: " & XMLNode.SelectSingleNode("PrimaryDamage").InnerText & " and " & XMLNode.SelectSingleNode("SecondaryDamage").InnerText)
            End If

            '------------------------------------------------------------------
            ' OwnerPhone is required and must not contain special characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("OwnerPhone")
            If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 15) Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must not be longer than 15 numbers: .  Value received: " & XMLNodeValidate.InnerText)
            End If

            If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
            End If

            '------------------------------------------------------------------
            ' ContactPhone is required and must not contain special characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("ContactPhone")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 15) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must not be longer than 15 numbers: .  Value received: " & XMLNodeValidate.InnerText)
                End If

                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' ContactNightPhone is required and must not contain special characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("ContactNightPhone")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 15) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must not be longer than 15 numbers: .  Value received: " & XMLNodeValidate.InnerText)
                End If

                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' ContactAltPhone is required and must not contain special characters
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("ContactAltPhone")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 15) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must not be longer than 15 numbers: .  Value received: " & XMLNodeValidate.InnerText)
                End If

                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' Remarks - if supplied must be not be more than 1000 chars
            '------------------------------------------------------------------
            'If sClaimSource = "WS" Then
            XMLNodeValidate = XMLNode.SelectSingleNode("Remarks")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 1000) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be no longer than 1000 characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If
            'Else
            'Dim elem As XmlElement = XMLWSClaim.CreateElement("Remarks")
            'elem.InnerText = ""
            'XMLNode.InsertAfter(elem, XMLNode.SelectSingleNode("WebAssignment/Vehicle/ShopFax"))
            'End If

            '------------------------------------------------------------------
            ' RentalAuthorized_ if supplied must be Yes or No
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("RentalAuthorized_")
            If (XMLNodeValidate.InnerText = "") Then
                XMLNodeValidate.InnerText = "NO"
                bRental = False
            Else
                If Not Regex.IsMatch(UCase(XMLNodeValidate.InnerText), "^(YES|NO)$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be Yes or No.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' Rentals, if either of the dollar amount fields are populate, then
            ' Rentals is a authorized.
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("RentalAuthorized_")
            If (XMLNode.SelectSingleNode("RentalDayAmount").InnerText <> "") And (XMLNode.SelectSingleNode("RentalMaxAmount").InnerText <> "") Then
                XMLNodeValidate.InnerText = "YES"
                bRental = True
            Else
                XMLNodeValidate.InnerText = "NO"
                bRental = False
            End If

            'If Regex.IsMatch(UCase(XMLNodeValidate.InnerText), "^(YES)$") Then
            '    bRental = True
            'Else
            '    bRental = False
            'End If

            '------------------------------------------------------------------
            ' Rentals 
            '------------------------------------------------------------------
            If bRental Then
                '-------------------------------------
                ' Rental Days
                '-------------------------------------
                XMLNodeValidate = XMLNode.SelectSingleNode("RentalDays")
                If (Len(XMLNodeValidate.InnerText) > 0) Then
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                        bRC = False
                        Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If

                '-------------------------------------
                ' Rental Days Authorized
                '-------------------------------------
                XMLNodeValidate = XMLNode.SelectSingleNode("RentalDaysAuthorized")
                If (Len(XMLNodeValidate.InnerText) > 0) Then
                    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                        bRC = False
                        Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                    End If
                End If
            End If

            '------------------------------------------------------------------
            ' DeductibleAmt if supplied must be greater than 0
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("DeductibleAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d).+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' DeductibleAmt if supplied must be greater than 0
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("LimitAmt")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d).+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' AssignmentTypeIDDescription if supplied must be REPAIR REFERRAL 
            ' or PROGRAM SHOP
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("AssignmentTypeIDDescription")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(UCase(XMLNodeValidate.InnerText), "^(REPAIR REFERRAL)|(PROGRAM SHOP)$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be REPAIR REFERRAL or PROGRAM SHOP and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' Mileage is Required Must be Numeric 
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("Mileage")
            If (XMLNodeValidate.InnerText = "") Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be greater than 0.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' VehicleNumber is Required Must be Numeric and unique
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("VehicleNumber")
            If (XMLNodeValidate.InnerText = "") Then
                bRC = False
                Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " is required and must be greater than 0.  Value received: " & XMLNodeValidate.InnerText)
            Else
                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If

                '-------------------------------------
                ' 09Oct2019 - TVD - Check and see if
                ' vehicle Number already exists instead
                ' of if it is greater than the max number
                ' already in the DB for a vehicle add
                '-------------------------------------
                Dim bVehicleExists As Boolean = False
                bVehicleExists = CheckIfVehicleAlreadyExist(iInsuranceCompanyID, iLynxID, CInt(XMLNodeValidate.InnerText))

                If (bNewClaim = False) And (bVehicleExists) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " already exists in this claim.  Existing vehicle number: " & XMLNodeValidate.InnerText)
                Else
                End If
            End If

            '------------------------------------------------------------------
            ' CellPhoneCarrier if supplied and must not contain special characters
            ' and not be more than 15 numbers
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("CellPhoneCarrier")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 15) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must not be longer than 15 numbers: .  Value received: " & XMLNodeValidate.InnerText)
                End If

                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' ContactEmailAddress if supplied must be a valid email address
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("ContactEmailAddress")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If Not Regex.IsMatch(UCase(XMLNodeValidate.InnerText), "^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be a valid formatted email address.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If

            '------------------------------------------------------------------
            ' ContactCellPhone if supplied and must not contain special characters
            ' and not be more than 15 numbers
            '------------------------------------------------------------------
            XMLNodeValidate = XMLNode.SelectSingleNode("ContactCellPhone")
            If (Len(XMLNodeValidate.InnerText) > 0) Then
                If (Len(XMLNodeValidate.InnerText) > 15) Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must not be longer than 15 numbers: .  Value received: " & XMLNodeValidate.InnerText)
                End If

                If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^(\d)+$") Then
                    bRC = False
                    Throw New SystemException("XMLValidationError: Vehicle Tag: " & XMLNodeValidate.Name & " if supplied must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
                End If
            End If
        Next

        '---------------------------------------
        ' Log Client Side Event
        '---------------------------------------
        APDFoundation.LogEvent("WSClientSideClaimProcessor", "END Validate - Server: " & sProcessingServer, "Client Side claim validate process ended", "", "Return Code: " & CStr(bRC))

        Return bRC
    End Function

    '-----------------------------------------
    ' These validation parameters are required
    ' for RRP in addition to the transactions
    ' above.
    '-----------------------------------------
    'Private Function ValidateClaimRRP(ByVal XMLWSClaim As XmlDocument) As Boolean
    '    Dim bRC As Boolean = True
    '    Dim XMLNodeValidate As XmlNode

    '    Dim reg As New Regex("")

    '    '-----------------------------------------
    '    ' REQUIRED and Must be boolean and 
    '    ' either a 0 or 1
    '    '-----------------------------------------
    '    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/AssignmentAtSelectionFlag")
    '    If (XMLNodeValidate.InnerText = "0") Or (XMLNodeValidate.InnerText = "1") Then
    '    Else
    '        bRC = False
    '        Throw New SystemException("<b>XMLValidationError:<br/>&nbsp;&nbsp;&nbsp;&nbsp; Tag:</b> " & XMLNodeValidate.Name & " is required must be one of the following values:  0 = False, 1 = True.  Value received: " & XMLNodeValidate.InnerText)
    '    End If

    '    '-------------------------------------------------
    '    ' REQUIRED and Must be numeric and a valid 
    '    ' Carrier Rep ID
    '    '-------------------------------------------------
    '    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepUserID")
    '    If (XMLNodeValidate.InnerText = "") Then
    '        bRC = False
    '        Throw New SystemException("<b>XMLValidationError:<br/>&nbsp;&nbsp;&nbsp;&nbsp; Tag:</b> " & XMLNodeValidate.Name & " is required must be a valid carrier representative ID.  Value received: " & XMLNodeValidate.InnerText)
    '    Else
    '        If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^\d+$") Then
    '            bRC = False
    '            Throw New SystemException("<b>XMLValidationError:<br/>&nbsp;&nbsp;&nbsp;&nbsp; Tag:</b> " & XMLNodeValidate.Name & " must be numeric and cannot contain special characters.  Value received: " & XMLNodeValidate.InnerText)
    '        End If
    '    End If

    '    '-----------------------------------------
    '    ' REQUIRED and Must be Alphanumeric and 
    '    ' =< 50
    '    '-----------------------------------------
    '    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierName")
    '    If (XMLNodeValidate.InnerText = "") Or (Len(XMLNodeValidate.InnerText) > 50) Then
    '        bRC = False
    '        Throw New SystemException("<b>XMLValidationError:<br/>&nbsp;&nbsp;&nbsp;&nbsp; Tag:</b> " & XMLNodeValidate.Name & " is required must be no longer than 50 charaters.  Value received: " & XMLNodeValidate.InnerText)
    '    End If

    '    '-----------------------------------------
    '    ' REQUIRED and Must be Yes or No
    '    '-----------------------------------------
    '    XMLNodeValidate = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalAuthorized")
    '    If (UCase(XMLNodeValidate.InnerText) = "YES") Or (UCase(XMLNodeValidate.InnerText) = "NO") Then

    '    Else
    '        bRC = False
    '        Throw New SystemException("<b>XMLValidationError:<br/>&nbsp;&nbsp;&nbsp;&nbsp; Tag:</b> " & XMLNodeValidate.Name & " is required must be Yes or No.  Value received: " & XMLNodeValidate.InnerText)
    '    End If

    '    Return bRC
    'End Function

    '-----------------------------------------------------------
    ' APD: LogEvent
    ' This APD function make a log entry into the utb_apd_event_log
    ' table for debug purposes
    '-----------------------------------------------------------
    'Public Function CreateAPDClaim(ByVal Claim As APDClaim, ByVal Vehicles As APDVehicle) As String
    <WebMethod(Description:="This APD function make a log entry into the utb_apd_event_log table for debug purposes.")>
    Public Function LogEvent(ByVal EventType As String, ByVal EventStatus As String, ByVal EventDescription As String, ByVal EventDetailedDescription As String, ByVal EventXml As String) As Integer

        Try
            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            APDFoundation.LogEvent(EventType, EventStatus, EventDescription, EventDetailedDescription, EventXml)

            Return 0
        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            If oExcept.Message.Contains("XMLValidationError") Then
                sError = oExcept.Message
            Else
                sError = String.Format("Error: {0}", "WebSerices Failed: Log Event failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            End If

            Return 1
        End Try

    End Function

    '-----------------------------------------------------------
    ' APD: GetLYNXSelectShops
    ' This APD function returns a list of LYNX Select Shops
    ' with in a range of your zip code.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a list of LYNX Select Shops with in a range of your zip code.")>
    Public Function GetLYNXSelectShops(
        ByVal ZipCode As String _
        , ByVal ShopName As String _
        , ByVal ShopCity As String _
        , ByVal ShopState As String _
        , ByVal InsuranceCompanyID As Integer
        ) As List(Of utb_shop_location)

        Dim oXMLReturn As Object
        Dim wsClass As New utb_shop_location
        Dim wsData As New List(Of utb_shop_location)

        '---------------------------------------
        ' Check parameters
        '---------------------------------------
        If ZipCode = Nothing Then
            ZipCode = ""

            If ShopName = Nothing Then ShopName = ""
            If ShopCity = Nothing Then ShopCity = ""
            If ShopState = Nothing Then
                '------------------------------
                ' Email Notify of the Error
                '------------------------------
                'Throw New Exception("When searching by Name or City, you must provide a State.")
                wsClass = New utb_shop_location
                wsClass.Errors = "When searching by Name or City, you must provide a State."
                wsData.Add(wsClass)

                Return wsData
            End If
        Else
            ShopName = ""
            ShopCity = ""
            ShopState = ""
        End If

        Try
            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            APDFoundation.LogEvent("WSClientSide_GetLYNXSelectShops", "START - Server: " & sProcessingServer, "Client Side - GetLYNXSelectShops process started", "Data passed-in is in EventXML", "ZipCode=" & ZipCode & ", " & "InsuranceCompanyID=" & InsuranceCompanyID & ", " & "MaxShops=" & 50)

            '--------------------------------------------------
            ' Process the GetLYNXSelectShops from the customer
            '--------------------------------------------------
            If ZipCode <> "" Then
                oXMLReturn = APDFoundation.GetLYNXSelectShopsByZipcode(ZipCode, InsuranceCompanyID, 50)
            Else
                oXMLReturn = APDFoundation.GetLYNXSelectShopsByNameOrCity(ShopName, ShopCity, ShopState, InsuranceCompanyID, 50)
            End If

            For Each oItem In oXMLReturn
                If oItem.Errors <> "" Then
                    wsClass = New utb_shop_location
                    wsClass.Errors = "No records found..."

                    wsData.Add(wsClass)
                Else
                    wsClass = New utb_shop_location
                    wsClass.ShopLocationID = oItem.ShopLocationID
                    wsClass.ShopName = oItem.ShopName
                    wsClass.Address1 = oItem.Address1
                    wsClass.Address2 = oItem.Address2
                    wsClass.AddressCity = oItem.AddressCity
                    wsClass.AddressState = oItem.AddressState
                    wsClass.AddressZip = oItem.AddressZip
                    wsClass.PhoneNumber = oItem.PhoneNumber
                    wsClass.FaxNumber = oItem.FaxNumber
                    wsClass.ShopOpen = oItem.ShopOpen
                    wsClass.TimeZone = oItem.TimeZone
                    wsClass.OperatingMondayStartTime = oItem.OperatingMondayStartTime
                    wsClass.OperatingMondayEndTime = oItem.OperatingMondayEndTime
                    wsClass.OperatingTuesdayStartTime = oItem.OperatingTuesdayStartTime
                    wsClass.OperatingTuesdayEndTime = oItem.OperatingTuesdayEndTime
                    wsClass.OperatingWednesdayStartTime = oItem.OperatingWednesdayStartTime
                    wsClass.OperatingWednesdayEndTime = oItem.OperatingWednesdayEndTime
                    wsClass.OperatingThursdayStartTime = oItem.OperatingThursdayStartTime
                    wsClass.OperatingThursdayEndTime = oItem.OperatingThursdayEndTime
                    wsClass.OperatingFridayStartTime = oItem.OperatingFridayStartTime
                    wsClass.OperatingFridayEndTime = oItem.OperatingFridayEndTime
                    wsClass.OperatingSaturdayStartTime = oItem.OperatingSaturdayStartTime
                    wsClass.OperatingSaturdayEndTime = oItem.OperatingSaturdayEndTime
                    wsClass.OperatingSundayStartTime = oItem.OperatingSundayStartTime
                    wsClass.OperatingSundayEndTime = oItem.OperatingSundayEndTime
                    wsClass.DrivingDirections = oItem.DrivingDirections

                    wsData.Add(wsClass)
                End If
            Next

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            APDFoundation.LogEvent("WSClientSide_GetLYNXSelectShops", "END - Server: " & sProcessingServer, "Client Side - GetLYNXSelectShops process ended", "", "")

            Return wsData

        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: APDService")
            sBody = String.Format("Error: {0}", "WebSerices Failed: Could not get the LYNX Select Shops (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_shop_location
            wsClass.Errors = sError & " - " & oExcept.ToString

            wsData.Add(wsClass)

            Return wsData
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ZipCodeLookup
    ' This APD function looks up city/state information based
    ' on a ZipCode and returns as XML
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function looks up city/state information based on a ZipCode and returns as XML,")>
    Public Function ZipCodeLookup(ByVal ZipCode As String) As XmlNode
        Dim oXMLReturn As Object
        Dim wsClass As New utb_zip_code
        Dim wsData As New List(Of utb_zip_code)
        Dim sAPDNetDebugging As String = "FALSE"
        Dim oNode As XmlNode = Nothing

        Try
            sAPDNetDebugging = AppSettings("Debug")

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer, "Client side ZipCodeLookup process started @ " & Now, "ZipCode passed-in is in EventXML", ZipCode)
            End If

            '-------------------------------
            ' Session/Local Variables
            '-------------------------------
            Dim sStoredProcedure As String = "uspZipCodeSearchWSXML"

            '---------------------------------------
            ' Check parameters
            '---------------------------------------
            'wsClass.Errors = ""
            If ZipCode = Nothing Then
                '------------------------------
                ' Email Notify of the Error
                '------------------------------
                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR", "Client side ZipCode not valid", "ZipCode was not passed-in", "")
                End If
            Else
                oXMLReturn = APDFoundation.ExecuteSpAsXML(sStoredProcedure, "'" & ZipCode & "'")

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUG", "Data returned for zipcode lookup: " & ZipCode, "Return data from the WS is in the EventXML", oXMLReturn.innerXML)
                End If

                oNode = oXMLReturn.selectsinglenode("//ZipInfo")
                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUG", "Single Node Data returned for zipcode lookup: " & ZipCode, "Return data from the WS is in the EventXML", oNode.OuterXml)
                End If
            End If

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer, "Client side ZipCodeLookup process ended @ " & Now, "", "")
            End If

            Return oNode
        Catch oExcept As Exception
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "Client side ZipCodeLookup failed: ", "Reason: " & oExcept.Message, ZipCode)
            Return Nothing
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ClaimSearch
    ' This APD function looks up existing claims by ClaimNumber
    ' and returns as XML
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function looks up existing claims by ClaimNumber and returns as XML,")>
    Public Function ClaimSearch(ByVal ClaimNumber As String) As String
        Dim oXMLReturn As Object = Nothing
        Dim sAPDNetDebugging As String = "FALSE"

        Try
            sAPDNetDebugging = AppSettings("Debug")

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer, "Client search process started @ " & Now, "ClaimNumber passed-in is in EventXML", ClaimNumber)
            End If

            ' Remove special characters from ClientClaimNumber
            ClaimNumber = Regex.Replace(ClaimNumber, "[^A-Za-z0-9\/]", "")

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUG", "Squishing the Claim Number: " & ClaimNumber, "Removed special characters from ClaimNumber", ClaimNumber)
            End If

            '-------------------------------
            ' Session/Local Variables
            '-------------------------------
            Dim sStoredProcedure As String = "uspClaimSearchWSXML"

            '---------------------------------------
            ' Check parameters
            '---------------------------------------
            'wsClass.Errors = ""
            If ClaimNumber = Nothing Then
                '------------------------------
                ' Email Notify of the Error
                '------------------------------
                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR", "Client side ClaimNumber is not valid", "ClaimNumber was not passed-in", "")
                End If
            Else
                oXMLReturn = APDFoundation.ExecuteSpAsXML(sStoredProcedure, ClaimNumber)

                '---------------------------------
                ' Debug Data
                '---------------------------------
                If UCase(sAPDNetDebugging) = "TRUE" Then
                    APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUG", "Data returned for ClaimSearch lookup: " & ClaimNumber, "Return data from the WS is in the EventXML", oXMLReturn.innerXML)
                End If
            End If

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer, "Client side Claim Search process ended @ " & Now, "", "")
            End If

            Return oXMLReturn.innerXML
        Catch oExcept As Exception
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "Client side Claim Search failed: ", "Reason: " & oExcept.Message, ClaimNumber)
            Return Nothing
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ChoiceShopProcess
    ' This APD function Create/Updates Choice Shops in the
    ' APD database.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function Create/Updates Choice Shops in the APD database.")>
    Public Function ChoiceShopProcess(
        ByVal bNewShopFlag As Boolean _
        , ByVal sHQBusinessTypeCD As String _
        , ByVal sHQAddress1 As String _
        , ByVal sHQAddress2 As String _
        , ByVal sHQAddressCity As String _
        , ByVal sHQAddressState As String _
        , ByVal sHQAddressZip As String _
        , ByVal sHQEmailAddress As String _
        , ByVal bHQEnabledFlag As Boolean _
        , ByVal sHQFaxAreaCode As String _
        , ByVal sHQFaxExchangeNumber As String _
        , ByVal sHQFaxUnitNumber As String _
        , ByVal sHQFedTaxId As String _
        , ByVal sHQName As String _
        , ByVal sHQPhoneAreaCode As String _
        , ByVal sHQPhoneExchangeNumber As String _
        , ByVal sHQPhoneUnitNumber As String _
        , ByVal iHQSysLastUserID As Integer _
        , ByVal sHQApplicationCD As String _
        , ByVal sHQShopUID As String _
        , ByVal sHQShopPartnerID As String
        ) As String

        Dim sReturnData As String = ""
        Dim sAPDNetDebugging As String = "FALSE"
        Dim sParams As String = ""
        Dim bRC As Boolean = True

        'Dim wsClass As New utb_zip_code
        'Dim wsData As New List(Of utb_zip_code)
        'Dim oNode As XmlNode = Nothing

        sParams = "bNewShopFlag: " & CStr(bNewShopFlag)
        sParams += ", sHQBusinessTypeCD: " & sHQBusinessTypeCD
        sParams += ", sHQAddress1: " & sHQAddress1
        sParams += ", sHQAddress2: " & sHQAddress2
        sParams += ", sHQAddressCity: " & sHQAddressState
        sParams += ", sHQAddressState: " & sHQAddressState
        sParams += ", sHQAddressZip: " & sHQAddressZip
        sParams += ", sHQEmailAddress: " & sHQEmailAddress
        sParams += ", bHQEnabledFlag: " & CStr(bHQEnabledFlag)
        sParams += ", sHQFaxAreaCode: " & sHQFaxAreaCode
        sParams += ", sHQFaxExchangeNumber: " & sHQFaxExchangeNumber
        sParams += ", sHQFaxUnitNumber: " & sHQFaxUnitNumber
        sParams += ", sHQFedTaxId: " & sHQFedTaxId
        sParams += ", sHQName: " & sHQName
        sParams += ", sHQPhoneAreaCode: " & sHQPhoneAreaCode
        sParams += ", sHQPhoneExchangeNumber: " & sHQPhoneExchangeNumber
        sParams += ", sHQPhoneUnitNumber: " & sHQPhoneUnitNumber
        sParams += ", iHQSysLastUserID: " & CStr(iHQSysLastUserID)
        sParams += ", sHQApplicationCD: " & sHQApplicationCD
        sParams += ", sHQShopUID: " & sHQShopUID
        sParams += ", sHQShopPartnerID: " & sHQShopPartnerID

        Try
            sAPDNetDebugging = AppSettings("Debug")

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer, "Client side ChoiceShopProcess started @ " & Now, "ChoiceShopProcess params are in EventXML", sParams)
            End If

            '-------------------------------------------------------------------
            ' Validation - Check parameters  (Update this later if time allows)
            '-------------------------------------------------------------------
            'If (sHQBusinessTypeCD = "" Or sHQBusinessTypeCD = Nothing) Then
            '    bRC = False
            '    Throw New SystemException("Invalid BusinessTypeCD.  Must be a 'C'.  Value received: " & sHQBusinessTypeCD)
            'End If

            'If (sHQBusinessTypeCD = "" Or sHQBusinessTypeCD = Nothing) Then
            '    bRC = False

            '    If bClientFriendlyMessage Then
            '        Throw New SystemException("Invalid insurance company received.  Value received: " & XMLNodeValidate.InnerText)
            '    Else
            '        Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " is required and must be a valid insurance company ID.  Value received: " & XMLNodeValidate.InnerText)
            '    End If
            'Else
            '    If Not Regex.IsMatch(XMLNodeValidate.InnerText, "^\d+$") Then
            '        bRC = False

            '        If bClientFriendlyMessage Then
            '            Throw New SystemException("Invalid insurance company received.  Value received: " & XMLNodeValidate.InnerText)
            '        Else
            '            Throw New SystemException("XMLValidationError: Claim Tag: " & XMLNodeValidate.Name & " must be numeric and cannot contain any special characters.  Value received: " & XMLNodeValidate.InnerText)
            '        End If
            '    End If
            'End If

            '-------------------------------
            ' Call backend ChoiceShopProcess
            '-------------------------------
            sReturnData = APDFoundation.ChoiceShopProcess(bNewShopFlag, sHQBusinessTypeCD, sHQAddress1, sHQAddress2, sHQAddressCity, sHQAddressState, sHQAddressZip, sHQEmailAddress, bHQEnabledFlag, sHQFaxAreaCode, sHQFaxExchangeNumber, sHQFaxUnitNumber, sHQFedTaxId, sHQName, sHQPhoneAreaCode, sHQPhoneExchangeNumber, sHQPhoneUnitNumber, iHQSysLastUserID, sHQApplicationCD, sHQShopUID, sHQShopPartnerID)

            If sReturnData = "" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "ShopLocationID was not returned successfully", "Value Receive: " & sReturnData, "")
            End If

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer, "Client side ChoiceShopProcess ended @ " & Now, "", "")
            End If

            Return sReturnData
        Catch oExcept As Exception
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "Client side ChoiceShopProcess failed: ", "Reason: " & oExcept.Message, sReturnData)
            Return Nothing
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: RentalMgt
    ' This APD function processes Rental Management details
    ' into and existing APD Lynx claim by LynxID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function processes Rental Management details into and existing APD Lynx claim by LynxID.")>
    Public Function RentalMgt(
        ByVal LynxID As String _
        , ByVal RentalAgency As String _
        , ByVal VehicleNumber As String _
        , ByVal InsuranceCompanyID As String _
        , ByVal RentalResConf As String _
        , ByVal RentalVehicleClass As String _
        , ByVal RentalRateType As String _
        , ByVal RentalRate As String _
        , ByVal RentalAuthPickup As String _
        , ByVal RentalTaxRate As String _
        , ByVal RentalPhone As String _
        , ByVal RentalAddress As String _
        , ByVal RentalCity As String _
        , ByVal RentalState As String _
        , ByVal RentalZip As String
        ) As String

        ', ByVal sClaimAspectID As String _

        Dim sParams As String = ""
        Dim sReturnData As String = ""
        Dim sClaimAspectID As String = ""
        Dim bError As Boolean = False
        Dim sStatus As String = ""
        Dim sStatusID As String = "0"
        Dim dTaxedRate As Decimal = 0

        Try
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer & " - " & Date.Now, "Rental Mgt process started", "Processing in new Rental Mgt Claim", "")

            '-- Initialize Status
            sStatus = ""
            sStatusID = "0"

            '---------------------------------------
            ' Validation
            '---------------------------------------
            '-- Get ClaimAspectID based on vehicle passed in
            sClaimAspectID = GetVehicleInfo(LynxID, VehicleNumber)
            '-- Check ClaimAspectID is valid
            If sClaimAspectID.Contains("ERROR") Then
                bError = True
                sStatusID = "200" '-- ClaimAspectID does not exist
                sStatus = "ERROR: Vehicle does not exist for this LynxID and InsuranceCompanyID. "
                Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - InsuranceCompanyID
            sReturnData = ValidateInscCompany(InsuranceCompanyID)
            If sReturnData.Contains("ERROR") Then
                bError = True
                sStatusID = "201" '-- Insurance Company does not exist
                sStatus = "ERROR: Invalid InsuranceCompanyID.  InsuranceCompanyID is required and must be a valid InsuranceCompanyID.  Value Received: " & InsuranceCompanyID
                Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - RentalAgency
            Select Case RentalAgency
                Case "Enterprise"
                Case "Hertz"
                Case "Other"
                Case Else
                    bError = True
                    sStatusID = "202" '-- Invalid RentalAgency entered
                    sStatus = "ERROR: RentalAgency must be Enterprise, Hertz or Other."
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End Select

            '-- Check passed params - RentalResConf
            If (Len(RentalResConf) > 0) Then
                If (Len(RentalResConf) > 100) Then
                    bError = True
                    sStatusID = "204" '-- Invalid RentalResConf Max lenght exceeded
                    sStatus = "ERROR: RentalResConf length can not be longer than 100 characters.  Value Received: " & RentalResConf
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                Else
                    If Not Regex.IsMatch(RentalResConf, "^[A-Za-z0-9\s-]+$") Then
                        bError = True
                        sStatusID = "205" '-- Invalid RentalResConf contains special characters
                        sStatus = "ERROR: RentalResConf must be Alphanumeric and cannot contain any special characters.  Value Received: " & RentalResConf
                        Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                    End If
                End If
            Else
                bError = True
                sStatusID = "203" '-- Invalid RentalResConf.  RentalResConf is a required field.
                sStatus = "ERROR: RentalResConf is required.  Value Received: " & RentalResConf
                Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - RentalVehicleClass
            Select Case RentalVehicleClass
                Case "Compact"
                Case "Economy"
                Case "Full Size"
                Case "Intermediate"
                Case "Large Pickup"
                Case "Luxury"
                Case "Midsize SUV"
                Case "Mini Van"
                Case "Premium"
                Case "Premium SUV"
                Case "Standard"
                Case "SUV"
                Case Else
                    bError = True
                    sStatusID = "206" '-- Invalid RentalVehicleClass entered
                    sStatus = "ERROR: RentalVehicleClass must be Compact, Economy, Full Size, Intermediate, Large Pickup, Luxury, Midsize SUV, Mini Van, Premium, Premium SUV, Standard or SUV.  Value Received: " & RentalVehicleClass
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End Select

            '-- Check passed params - RentalRateType
            Select Case RentalRateType
                Case "Daily"
                Case "Weekly"
                Case Else
                    bError = True
                    sStatusID = "207" '-- Invalid RentalRateType entered
                    sStatus = "ERROR: RentalRateType must be Daily or Weekly.  Value Received: " & RentalRateType
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End Select

            '-- Check passed params - RentalRate
            If (Len(RentalRate) > 0) Then
                If Not (Regex.IsMatch(RentalRate, "(^\d*\.\d{1,2}$)")) Or (Regex.IsMatch(RentalRate, "(^\d*\d{1,2}$)")) Then
                    bError = True
                    sStatusID = "209" '-- Invalid RentalRate entered
                    sStatus = "ERROR: RentalRate must be decimal and cannot contain any special characters.  Value Received: " & RentalRate
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                End If
            Else
                bError = True
                sStatusID = "208" '-- Invalid RentalResConf.  RentalResConf is a required field.
                sStatus = "ERROR: RentalRate is required.  Value Received: " & RentalRate
                Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - RentalAuthPickup
            If (Len(RentalAuthPickup) > 0) Then
                If Not Regex.IsMatch(RentalAuthPickup, "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$") Then
                    bError = True
                    sStatusID = "211" '-- Invalid RentalAuthPickup entered
                    sStatus = "ERROR: RentalAuthPickup is required must be a date in the format mm/dd/yyyy and no longer than 10 digits.  Value Received: " & RentalAuthPickup
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                End If
            Else
                bError = True
                sStatusID = "210" '-- Invalid RentalAuthPickup.  RentalAuthPickup is a required field.
                sStatus = "ERROR: RentalAuthPickup date is required.  Value Received: " & RentalAuthPickup
                Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - RentalTaxRate
            If (Len(RentalTaxRate) > 0) Then
                If Not (Regex.IsMatch(RentalTaxRate, "(^\d*\.\d{1,2}$)")) Or (Regex.IsMatch(RentalTaxRate, "(^\d*\d{1,2}$)")) Then
                    bError = True
                    sStatusID = "213" '-- Invalid RentalTaxRate entered
                    sStatus = "ERROR: RentalTaxRate must be numeric and cannot contain any special characters.  Value Received: " & RentalTaxRate
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                End If
            Else
                RentalTaxRate = 0
                '    bError = True
                '    sStatusID = "212" '-- Invalid RentalTaxRate.  RentalTaxRate is a required field.
                '    sStatus = "ERROR: RentalTaxRate is required.  Value Received: " & RentalTaxRate
                '    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - RentalPhone
            If (Len(RentalPhone) > 0) Then
                If (Len(RentalPhone) > 15) Then
                    bError = True
                    sStatusID = "215" '-- Invalid RentalPhone Max lenght exceeded.
                    sStatus = "ERROR: RentalPhone length can not be longer than 15 characters.  Value Received: " & RentalPhone
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                Else
                    If Not Regex.IsMatch(RentalPhone, "^[0-9][0-9][0-9]-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]+$") Then
                        bError = True
                        sStatusID = "216" '-- Invalid RentalPhone entered
                        sStatus = "ERROR: RentalPhone must be in the format xxx-xxx-xxxx.  Value Received: " & RentalPhone
                        Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                    End If
                End If
            Else
                bError = True
                sStatusID = "214" '-- Invalid RentalTaxRate.  RentalTaxRate is a required field.
                sStatus = "ERROR: RentalPhone is required.  Value Received: " & RentalPhone
                Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - RentalAddress
            If (Len(RentalAddress) > 0) Then
                ' Remove special characters
                RentalAddress = Regex.Replace(RentalAddress, "[^A-Za-z0-9 \/]", "")

                If (Len(RentalAddress) > 100) Then
                    bError = True
                    sStatusID = "218" '-- Invalid RentalAddress entered
                    sStatus = "ERROR: RentalAddress length can not be longer than 100 characters.  Value Received: " & RentalAddress
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                End If
                'Else
                '    bError = True
                '    sStatusID = "217" '-- Invalid RentalAddress.  RentalAddress is a required field.
                '    sStatus = "ERROR: RentalAddress is required.  Value Received: " & RentalAddress
                '    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - RentalCity
            If (Len(RentalCity) > 0) Then
                ' Remove special characters
                RentalCity = Regex.Replace(RentalCity, "[^A-Za-z0-9\/]", "")

                If (Len(RentalCity) > 100) Then
                    bError = True
                    sStatusID = "220" '-- Invalid RentalCity entered
                    sStatus = "ERROR: RentalCity length can not be longer than 100 characters.  Value Received: " & RentalCity
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                End If
                'Else
                '    bError = True
                '    sStatusID = "219" '-- Invalid RentalCity.  RentalCity is a required field.
                '    sStatus = "ERROR: RentalCity is required.  Value Received: " & RentalCity
                '    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - RentalState
            If (Len(RentalState) > 0) Then
                ' Remove special characters
                RentalState = Regex.Replace(RentalState, "[^A-Z\/]", "")

                If (Len(RentalState) > 2) Then
                    bError = True
                    sStatusID = "222" '-- Invalid RentalState entered
                    sStatus = "ERROR: RentalState length can not be longer than 2 characters.  Exp: PA, TX.  Value Received: " & RentalState
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                End If
                'Else
                '    bError = True
                '    sStatusID = "221" '-- Invalid RentalState.  RentalState is a required field.
                '    sStatus = "ERROR: RentalState is required.  Exp: PA, TX.  Value Received: " & RentalState
                '    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '-- Check passed params - RentalZip
            If (Len(RentalZip) > 0) Then
                If (Len(RentalZip) > 10) Then
                    bError = True
                    sStatusID = "224" '-- Invalid RentalZip entered
                    sStatus = "ERROR: RentalZip length can not be longer than 10 characters.  Value Received: " & RentalZip
                    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
                End If
                'Else
                '    bError = True
                '    sStatusID = "223" '-- Invalid RentalZip.  RentalZip is a required field.
                '    sStatus = "ERROR: RentalZip is required.  Value Received: " & RentalZip
                '    Throw New SystemException("ValidationError(" & sStatusID & "): " & sStatus)
            End If

            '--------------------------------------
            ' Calculate Data
            '--------------------------------------
            dTaxedRate = CDec(RentalTaxRate) + CDec(RentalRate)

            If sClaimAspectID <> "" And bError = False Then
                '---------------------------------------
                ' Log Client Side Event
                '---------------------------------------
                sParams = "@LynxID = " & LynxID
                sParams += ", @ClaimAspectID = " & sClaimAspectID
                sParams += ", @InsuranceCompanyID = " & InsuranceCompanyID
                sParams += ", @RentalAgency = '" & RentalAgency & "'"
                sParams += ", @ResConfNumber = '" & RentalResConf & "'"
                sParams += ", @VehicleClass = '" & RentalVehicleClass & "'"
                sParams += ", @RentalStatus = 'New'"
                sParams += ", @WarrantyFlag = 0"
                sParams += ", @RateTypeCD = '" & RentalRateType & "'"
                sParams += ", @Rate = '" & RentalRate & "'"
                sParams += ", @RentalNoOfDays = ''"
                sParams += ", @Rental = ''"
                sParams += ", @TaxRate = '" & RentalTaxRate & "'"
                sParams += ", @AuthPickupDate = '" & RentalAuthPickup & "'"
                sParams += ", @TaxedRate = " & dTaxedRate
                sParams += ", @StartDate = NULL"
                sParams += ", @EndDate = NULL"
                sParams += ", @RentalAgencyPhone = '" & RentalPhone & "'"
                sParams += ", @RentalAgencyAddress1 = '" & RentalAddress & "'"
                sParams += ", @RentalAgencyCity = '" & RentalCity & "'"
                sParams += ", @RentalAgencyState = '" & RentalState & "'"
                sParams += ", @RentalAgencyZip = '" & RentalZip & "'"
                sParams += ", @SysLastUserID = 0"
                sParams += ", @Type = 'SAVE'"
                sParams += ", @RentalID = '0'"

                '-------------------------------
                ' Session/Local Variables
                '-------------------------------
                Dim sStoredProcedure As String = "uspRentalMgtUpdDetails"

                sReturnData = APDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "EXECDB - Server: " & sProcessingServer & " - " & Date.Now, "Rental Mgt processing claim", "Processing in new Rental Mgt Claim", sStoredProcedure & " " & sParams & " RC=" & sReturnData)

                If sReturnData.Contains("ERROR") Then
                    sStatusID = "300"
                    sStatus = "Rental for Claim-Vehicle " & LynxID & "-" & VehicleNumber & " Failed.  ERROR: RC = " & sReturnData
                Else
                    sStatusID = "0"
                    sStatus = "Rental for Claim-Vehicle " & LynxID & "-" & VehicleNumber & " processed successfully."
                End If
            Else
                Return "ERROR: LynxID or Vehicle not found or data validation failure.  Error: ClaimAspectID = " & sClaimAspectID & "bError = " & bError
            End If

            '------------------------------
            ' Build return error XML
            '------------------------------
            Dim sRetData As String = ""
            sRetData = "<RentalMgtResponse>"
            sRetData += "<LynxID>" & LynxID & "</LynxID>"
            sRetData += "<InsuranceCompanyName>" & InsuranceCompanyID & "</InsuranceCompanyName>"
            sRetData += "<VehicleNumber>" & VehicleNumber & "</VehicleNumber>"
            sRetData += "<RentalAgency>" & RentalAgency & "</RentalAgency>"
            sRetData += "<StatusID>" & sStatusID & "</StatusID>"
            sRetData += "<Status>" & sStatus & "</Status>"
            sRetData += "</RentalMgtResponse>"

            'Dim response1 As New XmlDocument
            'response1.LoadXml(sRetData)

            '-------------------------------
            ' Logging
            '-------------------------------
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer & " - " & Date.Now, "Rental Mgt process Ended", "Done processing Rental Mgt Claim", "")

            'Return Server.HtmlEncode(sRetData)
            Return sRetData

        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            If oExcept.Message.Contains("ValidationError") Then
                sStatus = oExcept.Message
            Else
                sStatusID = "100"
                sStatus = String.Format("Error: {0}", "WebSerices Failed: Rental Management create failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            End If

            '------------------------------
            ' Build return error XML
            '------------------------------
            Dim sRetData As String = ""
            sRetData = "<RentalMgtResponse>"
            sRetData += "<LynxID>" & LynxID & "</LynxID>"
            sRetData += "<InsuranceCompanyName>" & InsuranceCompanyID & "</InsuranceCompanyName>"
            sRetData += "<VehicleNumber>" & VehicleNumber & "</VehicleNumber>"
            sRetData += "<RentalAgency>" & RentalAgency & "</RentalAgency>"
            sRetData += "<StatusID>" & sStatusID & "</StatusID>"
            sRetData += "<Status>" & sStatus & "</Status>"
            sRetData += "</RentalMgtResponse>"

            'Dim response1 As New XmlDocument
            'response1.LoadXml(sRetData)

            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "RentalMgt process failed: ", "Reason: " & oExcept.Message, "")
            'Return Server.HtmlEncode(sRetData)
            Return sRetData
        End Try
    End Function


    '-----------------------------------------------------------
    ' APD: GetVehicleInfo
    ' This fuction get the vehicle details for this claim
    '-----------------------------------------------------------
    Private Function GetVehicleInfo(ByVal sLynxID As String, ByVal sVehicleNumber As String) As String
        '-------------------------------
        ' Session/Local Variables
        '-------------------------------  
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim sClaimAspectID As String = ""

        Try
            '-------------------------------
            ' Session/Local Variables
            '-------------------------------
            Dim sStoredProcedure As String = "uspCPGetAllVehicleListWSXML"
            Dim sParams As String = "@LynxID = " & sLynxID

            APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer & " - " & Date.Now, "GetVehicleInfo process started", "Getting ClaimAspectID based on LynxID", "LynxID: " & sLynxID & ", VehicleNumber: " & sVehicleNumber)

            oReturnXML = APDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "EXECDB - Server: " & sProcessingServer & " - " & Date.Now, "GetVehicleInfo processing", "Getting vehicle data", sStoredProcedure & " " & sParams & " RC=" & oReturnXML.InnerXml)

            '---------------------------------------
            ' Load vehicle data
            '---------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("/Vehicle")

            For Each oXMLNode In oXMLNodeList
                If GetXMLNodeattrbuteValue(oXMLNode, "VehicleNumber") = sVehicleNumber Then
                    sClaimAspectID = GetXMLNodeattrbuteValue(oXMLNode, "ClaimAspectID")
                End If
            Next

            APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer & " - " & Date.Now, "GetVehicleInfo process ended", "ClaimAspectID value or error message in EventXML", "ClaimAspectID: " & sClaimAspectID)

            If sClaimAspectID <> "" Then
                Return sClaimAspectID
            Else
                Return "ERROR: ClaimAspectID not found for LynxID: " & sLynxID & ", VehicleNumber: " & sVehicleNumber
            End If

        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            If oExcept.Message.Contains("XMLValidationError") Then
                sError = oExcept.Message
            Else
                sError = String.Format("Error: {0}", "WebSerices Failed: Could not find the vehicle info, (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            End If

            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "Get vehicle information failed: ", "Reason: " & oExcept.Message, "")

            Return "ERROR: ClaimAspectID not found for LynxID: " & sLynxID & ", VehicleNumber: " & sVehicleNumber
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetVehicleInfo
    ' This fuction get the vehicle details for this claim
    '-----------------------------------------------------------
    Private Function ValidateInscCompany(ByVal sInscCompanyID As String) As String
        '-------------------------------
        ' Session/Local Variables
        '-------------------------------  
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing

        Try
            '-------------------------------
            ' Session/Local Variables
            '-------------------------------
            Dim sStoredProcedure As String = "uspECADGetInsuranceCoListWSXML"
            Dim sParams As String = ""

            oReturnXML = APDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------------
            ' Load vehicle data
            '---------------------------------------
            oXMLNodeList = oReturnXML.SelectNodes("InsuranceCompany")

            Dim bInscExists As Boolean = False
            For Each oXMLNode In oXMLNodeList
                If GetXMLNodeattrbuteValue(oXMLNode, "InsuranceCompanyID") = sInscCompanyID Then
                    bInscExists = True
                End If
            Next

            If bInscExists Then
                Return "VALID"
            Else
                Return "ERROR: InsuranceCompanyID: " & sInscCompanyID & " does not exist."
            End If

        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            If oExcept.Message.Contains("XMLValidationError") Then
                sError = oExcept.Message
            Else
                sError = String.Format("ERROR: {0}", "WebSerices Failed: Could not find the insurance company info, (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            End If

            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "Get insurance company info failed: ", "Reason: " & oExcept.Message, "")
            Return sError
        End Try
    End Function


    '-----------------------------------------------------------
    ' APD: ExecuteSPAsString
    ' This fuction calls an SP with Params and returns a String
    '-----------------------------------------------------------
    Private Function ExecuteSpAsString(ByVal StoredProcedure As String, ByVal Params As String) As String
        Dim sStringReturn As String
        Dim wsClass As New utb_zip_code
        Dim wsData As New List(Of utb_zip_code)
        Dim sAPDNetDebugging As String = "FALSE"
        Dim oNode As XmlNode = Nothing

        Try
            sAPDNetDebugging = AppSettings("Debug")

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer, "ExecuteSpAsString started @ " & Now, "ExecuteSpAsString StoredProc and Params in EventXML", "StoredProcedure=" & StoredProcedure & ", Prams=" & Params)
            End If

            '-------------------------------
            ' Session/Local Variables
            '-------------------------------
            Dim sStoredProcedure As String = StoredProcedure

            sStringReturn = APDFoundation.ExecuteSpAsString(sStoredProcedure, Params)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUG", "Executing StoredProcedure: " & sStoredProcedure, "String Return data from the WS is in the EventXML", sStringReturn)
            End If

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer, "ExecuteSpAsString process ended @ " & Now, "", "")
            End If

            Return sStringReturn
        Catch oExcept As Exception
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "ExecuteSpAsString failed: ", "Reason: " & oExcept.Message, "")
            Return Nothing
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ExecuteSPAsXML
    ' This fuction calls an SP with Params and returns a XML
    '-----------------------------------------------------------
    Private Function ExecuteSpAsXML(ByVal StoredProcedure As String, ByVal Params As String) As String
        Dim oReturnXML As New Object
        Dim sReturnXML As String = Nothing
        Dim sAPDNetDebugging As String = "FALSE"

        Try
            sAPDNetDebugging = AppSettings("Debug")

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer, "ExecuteSpAsXML started @ " & Now, "ExecuteSpAsXML StoredProc and Params in EventXML", "StoredProcedure=" & StoredProcedure & ", Prams=" & Params)
            End If

            '-------------------------------
            ' Session/Local Variables
            '-------------------------------
            Dim sStoredProcedure As String = StoredProcedure

            oReturnXML = APDFoundation.ExecuteSpAsXML(sStoredProcedure, Params)
            'sReturnXML = oReturnXML.OuterXml

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUG", "Executing StoredProcedure: " & sStoredProcedure, "XML Return data from the WS is in the EventXML", oReturnXML.OuterXml)
            End If

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer, "ExecuteSpAsXML process ended @ " & Now, "", "")
            End If

            Return oReturnXML.OuterXml
        Catch oExcept As Exception
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "ExecuteSpAsXML failed: ", "Reason: " & oExcept.Message, "")
            Return Nothing
        End Try
    End Function

    '-----------------------------------------------------------
    ' ClaimPoint: GetXMLNodeattrbuteValue
    ' This function gets the attribute data by name from the 
    ' XMLNode.  If it doesn't exist returns ""
    '-----------------------------------------------------------
    Protected Function GetXMLNodeattrbuteValue(ByVal XMLnode As XmlNode, strAttributes As String) As String
        Try
            Dim strRetVal As String = ""
            If Not XMLnode Is Nothing Then
                If Not XMLnode.Attributes(strAttributes) Is Nothing Then
                    strRetVal = XMLnode.Attributes(strAttributes).InnerText
                End If
            End If

            Return strRetVal
        Catch oExcept As Exception
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "ERROR: Parsing XMLNode Attribute: ", "Reason: " & oExcept.Message, "")
            Return Nothing
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetLYNXCombinedShops
    ' This APD function returns a list of both LYNX Select Shops
    '  and Choice shops with in a range of your zip code.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a list of LYNX Select Shops and Choice shops with in a range of your zip code.")>
    Public Function GetLYNXCombinedShops(
        ByVal ZipCode As String _
        , ByVal ShopName As String _
        , ByVal ShopCity As String _
        , ByVal ShopState As String _
        , ByVal InsuranceCompanyID As Integer
        ) As List(Of utb_shop_location)

        '---------------------------------------
        ' Declarations
        '---------------------------------------
        Dim oXMLReturn As Object = Nothing
        Dim wsClass As New utb_shop_location
        Dim wsData As New List(Of utb_shop_location)

        '---------------------------------------
        ' Check parameters
        '---------------------------------------
        If ZipCode = Nothing Then
            ZipCode = ""

            If ShopName = Nothing Then ShopName = ""
            If ShopCity = Nothing Then ShopCity = ""
            If ShopState = Nothing Then
                '------------------------------
                ' Email Notify of the Error
                '------------------------------
                'Throw New Exception("When searching by Name or City, you must provide a State.")
                wsClass = New utb_shop_location
                wsClass.Errors = "When searching by Name or City, you must provide a State."
                wsData.Add(wsClass)

                Return wsData
            End If
        Else
            ShopName = ""
            ShopCity = ""
            ShopState = ""
        End If

        Try
            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            APDFoundation.LogEvent("WSClientSide_GetLYNXCombinedShops", "START - Server: " & sProcessingServer, "Client Side - GetLYNXCombinedShops process started", "Data passed-in is in EventXML", "ZipCode=" & ZipCode & ", " & "InsuranceCompanyID=" & InsuranceCompanyID & ", " & "MaxShops=" & 50)

            '--------------------------------------------------
            ' Process the GetLYNXSelectShops from the customer
            '--------------------------------------------------
            If ZipCode <> "" Then
                oXMLReturn = APDFoundation.GetLYNXCombinedShopsByZipcode(ZipCode, InsuranceCompanyID, 50)
            Else
                'oXMLReturn = APDFoundation.GetLYNXCombinedShopsByNameOrCity(ShopName, ShopCity, ShopState, InsuranceCompanyID, 50)
            End If

            For Each oItem In oXMLReturn
                If oItem.Errors <> "" Then
                    wsClass = New utb_shop_location
                    wsClass.Errors = "No records found..."

                    wsData.Add(wsClass)
                Else
                    wsClass = New utb_shop_location

                    wsClass.Source = oItem.Source
                    wsClass.Distance = oItem.Distance

                    wsClass.ShopLocationID = oItem.ShopLocationID
                    wsClass.ShopGUID = oItem.ShopGUID
                    wsClass.ShopName = oItem.ShopName
                    wsClass.Address1 = oItem.Address1
                    wsClass.Address2 = oItem.Address2
                    wsClass.AddressCity = oItem.AddressCity
                    wsClass.AddressState = oItem.AddressState
                    wsClass.AddressZip = oItem.AddressZip
                    wsClass.PhoneNumber = oItem.PhoneNumber
                    wsClass.FaxNumber = oItem.FaxNumber
                    wsClass.ShopOpen = oItem.ShopOpen
                    wsClass.TimeZone = oItem.TimeZone
                    wsClass.OperatingMondayStartTime = oItem.OperatingMondayStartTime
                    wsClass.OperatingMondayEndTime = oItem.OperatingMondayEndTime
                    wsClass.OperatingTuesdayStartTime = oItem.OperatingTuesdayStartTime
                    wsClass.OperatingTuesdayEndTime = oItem.OperatingTuesdayEndTime
                    wsClass.OperatingWednesdayStartTime = oItem.OperatingWednesdayStartTime
                    wsClass.OperatingWednesdayEndTime = oItem.OperatingWednesdayEndTime
                    wsClass.OperatingThursdayStartTime = oItem.OperatingThursdayStartTime
                    wsClass.OperatingThursdayEndTime = oItem.OperatingThursdayEndTime
                    wsClass.OperatingFridayStartTime = oItem.OperatingFridayStartTime
                    wsClass.OperatingFridayEndTime = oItem.OperatingFridayEndTime
                    wsClass.OperatingSaturdayStartTime = oItem.OperatingSaturdayStartTime
                    wsClass.OperatingSaturdayEndTime = oItem.OperatingSaturdayEndTime
                    wsClass.OperatingSundayStartTime = oItem.OperatingSundayStartTime
                    wsClass.OperatingSundayEndTime = oItem.OperatingSundayEndTime
                    wsClass.DrivingDirections = oItem.DrivingDirections
                    wsClass.Latitude = oItem.Latitude
                    wsClass.Longitude = oItem.Longitude

                    wsData.Add(wsClass)
                End If
            Next

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            APDFoundation.LogEvent("WSClientSide_GetLYNXCombinedShops", "END - Server: " & sProcessingServer, "Client Side - GetLYNXCombinedShops process ended", "", "")

            Return wsData

        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: APDService")
            sBody = String.Format("Error: {0}", "WebSerices Failed: Could not get the LYNX Select Shops (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_shop_location
            wsClass.Errors = sError & " - " & oExcept.ToString

            wsData.Add(wsClass)

            Return wsData
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: CheckExistingShop
    ' This fuction calls an SP with Params and checks the 
    ' 3 criteria for existing shops and returns a ShopLocationID
    ' String
    '-----------------------------------------------------------
    Private Function CheckExistingShop(ByVal sShopName As String, ByVal sShopAddress1 As String, ByVal sShopAddress2 As String, ByVal sShopCity As String, ByVal sShopState As String, ByVal sShopZip As String, ByVal sPhoneAreaCode As String, ByVal sPhoneExchangeNumber As String, ByVal sPhoneUnitNumber As String, ByVal sShopTypeCode As String, ByVal sInsuranceCompanyID As String) As String
        Dim sShopLocationID As String = ""
        Dim sAPDNetDebugging As String = "FALSE"
        Dim sParams As String = ""
        Dim sSource As String = ""
        Dim sStoredProcedure As String = ""
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing

        '---------------------------------------
        ' Declarations
        '---------------------------------------
        Dim oXMLReturn As XmlElement = Nothing

        Try
            sAPDNetDebugging = AppSettings("Debug")

            '-------------------------------
            ' Session/Local Variables
            '-------------------------------
            sStoredProcedure = "uspShopSimilarMatchWSXML"
            sParams = "@ShopName = '" & sShopName & "', @ShopAddress1 = '" & sShopAddress1 & "', @ShopAddress2 = '" & sShopAddress2 & "', @ShopCity = '" & sShopCity & "', @ShopState = '" & sShopState & "', @ShopZip = '" & sShopZip & "', @PhoneAreaCode = " & sPhoneAreaCode & ", @PhoneExchangeNumber = " & sPhoneExchangeNumber & ", @PhoneUnitNumber = " & sPhoneUnitNumber & ", @ShopTypeCode = '" & sShopTypeCode & "', @InsuranceCompanyID = " & sInsuranceCompanyID

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer, "CheckExistingShop started @ " & Now, "Execute checks if shop already exists.  Details in EventXML", "StoredProcedure=" & sStoredProcedure & ", Prams=" & sParams)
            End If

            '---------------------------------
            ' Execute and check results to see
            ' if a ShopLocationID is returned
            '---------------------------------
            oXMLReturn = APDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "EXECDB", "Executing StoredProcedure: " & sStoredProcedure, "XML Return data from the WS is in the EventXML", oXMLReturn.OuterXml)
            End If

            '---------------------------------------
            ' Get ShopLoctionID if Exists
            '---------------------------------------
            'oXMLNode = oXMLReturn.SelectSingleNode("Root/Shop")

            'oXMLNode.SelectSingleNode("WebAssignment/Vehicle/ShopLocationID").InnerText
            'sShopLocationID = GetXMLNodeattrbuteValue(oXMLNode, "ShopLocationID")

            oXMLNodeList = oXMLReturn.SelectNodes("Shop")

            APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUGGING", "Parsing XML results", "XML in EventXML", oXMLReturn.OuterXml)

            For Each oXMLNode In oXMLNodeList
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUGGING", "Parsing XML Nodes", "Node details source/ShopLocation in EventXML", oXMLNode.Attributes("Source").InnerText & "/" & oXMLNode.Attributes("ShopLocationID").InnerText)

                If oXMLNode.Attributes("Source").InnerText = "LYNX" Or oXMLNode.Attributes("Source").InnerText = "HQNew" Then
                    sShopLocationID = oXMLNode.Attributes("ShopLocationID").InnerText
                    sSource = oXMLNode.Attributes("Source").InnerText
                End If
            Next

            APDFoundation.LogEvent("WSClientSideClaimProcessor", "DEBUGGING", "REsults", "Results", oXMLNodeList.Count)
            'sShopLocationID = oXMLReturn.SelectSingleNode("Root/Shop/@ShopLocationID").InnerText

            '---------------------------------
            ' Debug Data
            '---------------------------------
            'If UCase(sAPDNetDebugging) = "TRUE" Then
            '    'APDFoundation.LogEvent("WSClientSideClaimProcessor", "XMLPARSE", "Parsing XML to get ShopLocationID", "ShopLocationID is in the EventXML", "NodeValue=" & oXMLNode.InnerXml & " ShopLocationID=" & sShopLocationID)
            '    APDFoundation.LogEvent("WSClientSideClaimProcessor", "XMLPARSE", "Parsing XML to get ShopLocationID", "ShopLocationID is in the EventXML", "NodeValue=" & oXMLReturn.SelectSingleNode("Root/Shop/@ShopLocationID").InnerText & " ShopLocationID=" & sShopLocationID)
            'End If

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer, "CheckExistingShop process ended @ " & Now, "", "")
            End If

            Return sShopLocationID
        Catch oExcept As Exception
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "ExecuteSpAsString failed: ", "Reason: " & oExcept.Message, "")
            Return Nothing
        End Try
    End Function

    Private Sub UpdateCarrierRep(ByVal sEmailAddress As String, ByVal sNameFirst As String, ByVal sNameLast As String, ByVal sPhoneAreaCode As String, ByVal sPhoneExchangeNumber As String, ByVal sPhoneUnitNumber As String, ByVal sInsuranceCompanyID As String)
        Dim sAPDNetDebugging As String = "FALSE"
        Dim sParams As String = ""
        Dim sSource As String = ""
        Dim sStoredProcedure As String = ""

        '---------------------------------------
        ' Declarations
        '---------------------------------------
        Dim sReturnData As String = ""

        Try
            sAPDNetDebugging = AppSettings("Debug")

            '-------------------------------
            ' Session/Local Variables
            '-------------------------------
            sStoredProcedure = "uspUpdateClaimRep"
            sParams = "@vEmailAddress = '" & sEmailAddress & "', @vNameFirst = '" & sNameFirst & "', @vNameLast = '" & sNameLast & "', @vPhoneAreaCode = '" & sPhoneAreaCode & "', @vPhoneExchangeNumber = '" & sPhoneExchangeNumber & "', @vPhoneUnitNumber = '" & sPhoneUnitNumber & "', @iInsuranceCompanyID = " & CInt(sInsuranceCompanyID)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer, "Update carrier rep details started @ " & Now, "Execute carrier rep update.  Details in EventXML", "StoredProcedure=" & sStoredProcedure & ", Prams=" & sParams)
            End If

            '---------------------------------
            ' Execute and check results to see
            ' if a ShopLocationID is returned
            '---------------------------------
            sReturnData = APDFoundation.ExecuteSpAsString(sStoredProcedure, sParams)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "EXECDB", "Executing StoredProcedure: " & sStoredProcedure, "String Return data from the WS is in the EventXML", sReturnData)
            End If

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer, "Update carrier rep details process ended @ " & Now, "", "")
            End If

        Catch oExcept As Exception
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "ExecuteSpAsString failed: ", "Reason: " & oExcept.Message, "")
        End Try
    End Sub



    '-----------------------------------------------------------
    ' This function is used to insert the new Job in utb_postoffice_jobs table
    ' Then the APDPostOffice will process the job
    ' Author : Mahes
    '-----------------------------------------------------------
    <WebMethod(Description:="This function is used to insert a new Job in utb_postoffice_jobs table")>
    Public Function CreatePostOfficeJob(ByVal Method As String, ByVal ClientID As String, ByVal LynxID As String, ByVal ClaimNumber As String, ByVal JobPriority As String, ByVal ResponseRequired As String, ByVal ResponseThresholdMins As String, ByVal JobDetails As String) As String
        Dim parameters As String = ""
        Dim err As String = ""
        Dim sResult = ""
        Dim sXMLResult = ""
        Dim sStoredProcedure As String = ""

        Dim EventTransactionID As String = System.Guid.NewGuid.ToString()
        Dim sLogParams As String = ""
        Dim jobStatus As String = ""
        Dim JobID As String = System.Guid.NewGuid.ToString()


        Try

            '---------------------------------
            ' Debug Data
            '---------------------------------
            APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "APDPostOffice - CreateJob", CStr(iEventSeq), "Start", "<==Web service started==>", "PostOffice Create Job web service started", JobDetails)
            iEventSeq = iEventSeq + 1

            '---------------------------------
            ' Validations of input params
            '---------------------------------
            If JobID <> "" Then
                parameters += "@JobID='" & JobID & "',@StepID=1,"
            Else
                err += "|JobID is required|"
            End If

            Method = Method.Trim()
            If Method = "EML" Or Method = "FTP" Or Method = "FAX" Or Method = "WS" Or Method = "UPL" Or Method = "NOTE" Then
                parameters += "@Method='" & Method & "',"
            Else
                err += "Incorrect Method(" + Method + ")"
            End If

            ClientID = ClientID.Trim()
            If ClientID <> "" Then
                parameters += " @ClientID=" & ClientID & ","
            Else
                parameters += " @ClientID=NULL,"
            End If

            If LynxID <> "" Then
                parameters += "@LynxID=" & LynxID & ","
            Else
                parameters += "@LynxID=NULL,"
            End If

            If ClaimNumber <> "" Then
                parameters += "@ClaimNumber='" & ClaimNumber & "',"
            Else
                parameters += "@ClaimNumber=NULL,"
            End If

            If JobPriority <> "" Then
                parameters += "@JobPriority=" & JobPriority & ","
            Else
                parameters += "@JobPriority=5,"
            End If

            If ResponseRequired <> "" Then
                parameters += "@ResponseRequired=" & ResponseRequired & ","
            Else
                parameters += "@ResponseRequired=false,"
            End If

            If ResponseThresholdMins <> "" Then
                parameters += "@ResponseThresholdMins=" & ResponseThresholdMins & ","
            Else
                parameters += "@ResponseThresholdMins=0,"
            End If

            If JobDetails <> "" Then
                parameters += "@JobDetails='" & JobDetails & "'"
            Else
                err += " Job Details is required"
            End If


            'Xml XSD validation
            If Method <> "" And JobDetails <> "" Then
                Dim strSchmaPath As String = AppSettings(Method)
                If strSchmaPath <> "" Then
                    sXMLResult += ValidateXML(JobDetails, strSchmaPath)
                Else
                    err += ""
                End If
            End If

            '---------------------------------
            ' Debug Data
            '---------------------------------
            'sLogParams = " JobID- " & JobID & " StepID-1 Method- " & Method & " ClientID- " & ClientID & " LynxID- " & LynxID & " ClaimNumber- " & ClaimNumber & " JobPriority- " & JobPriority & " ResponseRequired- " & ResponseRequired & " ResponseThresholdMins- " & ResponseThresholdMins & " JobDetails- " & JobDetails
            APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "APDPostOffice - CreateJob", CStr(iEventSeq), "Debugging", "<==Parameters==>", "Params = " & parameters.Replace("'", "''") & "Require fields Fields =" + err, JobDetails)
            iEventSeq = iEventSeq + 1

            '---------------------------------
            ' Executing SP
            '---------------------------------
            If strXMLErrorHandler = "" And sXMLResult = "" Then
                sStoredProcedure = "uspPostOffice_AddJob"
                sResult = APDFoundation.ExecuteSp(sStoredProcedure, parameters)
            Else
                sResult = "Error"
            End If

            If sResult.Contains("Error") Then
                '---------------------------------
                ' Debug Data
                '---------------------------------
                jobStatus = "Failure | Unmatched Fields - " & err & " | Result - " & sResult & " | XMLErrorHandler - " & strXMLErrorHandler & sXMLResult
                APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "APDPostOffice - CreateJob", CStr(iEventSeq), "ERROR", "<==Error after executing stored procedure==>", jobStatus.Replace("'", "''"), "")
                iEventSeq = iEventSeq + 1

            Else
                jobStatus = "success"
            End If

            '---------------------------------
            ' Debug Data
            '---------------------------------
            APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "APDPostOffice - CreateJob", CStr(iEventSeq), "END", "<==End of process==>", jobStatus.Replace("'", "''"), "")
            iEventSeq = iEventSeq + 1
        Catch oExcept As Exception
            '---------------------------------
            ' Debug Data
            '---------------------------------
            jobStatus = "Failure | Result - " & sResult & " | Exception - " & oExcept.Message & " | XMLErrorHandler - " & strXMLErrorHandler
            APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "APDPostOffice - CreateJob", CStr(iEventSeq), "ERROR", "<==Eror catched==>", jobStatus.Replace("'", "''"), "")
            iEventSeq = iEventSeq + 1
        End Try

        Return jobStatus
    End Function

    '-----------------------------------------------------------
    ' This function is used to insert the log details in utb_postoffice_event_log table
    ' This function only used by APDPostOffice
    ' Author : Mahes
    '-----------------------------------------------------------
    Public Function APDPostOfficeEventLog(ByVal ProcessingServer As String, ByVal EventTransactionID As String, ByVal EventType As String, ByVal EventSeq As Integer, ByVal EventStatus As String, ByVal EventDescription As String, ByVal EventDetailedDescription As String, ByVal EventXML As String) As Long
        Dim logParms As String = ""
        Dim sResult As String = ""
        Dim sStoredProcedure As String = ""
        Try
            sStoredProcedure = "uspPostOfficeEventInsLogEntry"
            logParms = " @vProcessingServer = '" & ProcessingServer & "', @vEventTransactionID ='" + EventTransactionID & "', @vEventType = '" + EventType & "', @iEventSeq = " + CStr(EventSeq) & ", @vEventStatus = '" + EventStatus & "', @vEventDescription = '" + EventDescription & "', @vEventDetailedDescription = '" + EventDetailedDescription & "', @vEventXML = '" + EventXML & "'"
            '---------------------------------
            ' Executing SP
            '---------------------------------
            sResult = APDFoundation.ExecuteSp(sStoredProcedure, logParms)
        Catch oExcept As Exception
            '---------------------------------
            ' Debug Data
            '---------------------------------
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "ExecuteSpAsString failed: ", "Reason: " & oExcept.Message, "")
        End Try
        Return 0
    End Function

    Public Function ValidateXML(ByVal strJobDetailsXML As String, ByVal strSchmaPath As String)
        Try
            Dim strError As String = ""
            APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "PostOffice-PerformJob(XSDVALIDATIONHANDLER)", iEventSeq, "XSDValidationHandler", DateTime.Now & " - Validating XML vs XSD.", String.Format("<<<JobXML: {0}, SchmaPath: {1} >>>", strJobDetailsXML, strSchmaPath), "")
            iEventSeq = (iEventSeq + 1)
            Dim XMLErrHandler As String = ""
            Dim doc As XmlDocument = New XmlDocument()
            doc.LoadXml(strJobDetailsXML)
            doc.Schemas.Add("", strSchmaPath)
            Dim eventHandler As ValidationEventHandler = New ValidationEventHandler(AddressOf ValidationEventHandler)
            doc.Validate(eventHandler)

        Catch oExcept As Exception
            Dim strError As String = ""
            Dim FunctionName As StackFrame = New StackFrame()
            strError = String.Format("Error: {0}", ("PostOffice-ValidateXML: ValidateXML process failed (" & (FunctionName.GetMethod().Name & ")...  " + oExcept.ToString())))
            APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "ERROR", iEventSeq, DateTime.Now & " - Error occurred...", "Error: " & strError, "", "")
            iEventSeq = (iEventSeq + 1)
            Return strError
        End Try
        Return strXMLErrorHandler
    End Function

    Public Sub ValidationEventHandler(ByVal sender As Object, ByVal e As ValidationEventArgs)
        APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "PostOffice-XSDValidatedeventHandler(XSDERRORHANDLER)", iEventSeq, "XSDValidatedeventHandler", DateTime.Now & " - an XSD Error was thrown.", String.Format("<<<XSDError: {0} >>>", e.Message), "")
        iEventSeq = (iEventSeq + 1)

        Try

            If e.Severity = XmlSeverityType.Warning Then
                APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "PostOffice-PerformJob(XSDWARNING)", iEventSeq, "XSDSchemaValidationWarning", DateTime.Now & " - a Warning was thrown while validating the DocumentUpload XML against the DocumentUpload.xsd", String.Format("<<<Validation Warning Results: {0} >>>", e.Message.Replace("'", "''")), "")
                iEventSeq = (iEventSeq + 1)
            ElseIf e.Severity = XmlSeverityType.[Error] Then
                strXMLErrorHandler += e.Message & "|"
                APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "PostOffice-PerformJob(XSDERROR)", iEventSeq, "XSDSchemaValidationError", DateTime.Now & " - an Error was thrown while validating the DocumentUpload XML against the DocumentUpload.xsd", String.Format("<<<Validation Error Results: {0} >>>", e.Message.Replace("'", "''")), "")
                iEventSeq = (iEventSeq + 1)
            End If

        Catch oExcept As Exception
            Dim strError As String = ""
            Dim FunctionName As StackFrame = New StackFrame()
            strError = String.Format("Error: {0}", ("PostOffice-XSDValidatedeventHandler: ValidateXMLeventHandler process failed (" & (FunctionName.GetMethod().Name & ")...  " + oExcept.ToString())))
            APDPostOfficeEventLog(sProcessingServer, EventTransactionID, "ERROR", iEventSeq, DateTime.Now & " - Error occurred...", "Error: " & strError, "", "")
            iEventSeq = (iEventSeq + 1)
        End Try
    End Sub

    Private Function CheckIfVehicleAlreadyExist(ByVal iInsuranceCompanyID As Integer, ByVal iLynxID As Integer, ByVal iVehicleNumber As Integer) As Boolean
        '---------------------------------------
        ' Declarations
        '---------------------------------------
        Dim sParams As String = ""
        Dim sStoredProcedure As String = ""
        Dim sAPDNetDebugging As String = "FALSE"
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim oXMLReturn As XmlElement = Nothing

        Try
            sAPDNetDebugging = AppSettings("Debug")

            '-------------------------------
            ' Session/Local Variables
            '-------------------------------
            sStoredProcedure = "uspClaimVehicleGetListWSXML"
            sParams = "@LynxID = " & iLynxID & ", @InsuranceCompanyID = " & iInsuranceCompanyID

            '---------------------------------
            ' Executing SP
            '---------------------------------
            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "START - Server: " & sProcessingServer, "Checking if vehicle already exists - started @ " & Now, "Execute vehicle check.  Details in EventXML", "StoredProcedure=" & sStoredProcedure & ", Prams=" & sParams)
            End If

            '---------------------------------
            ' Execute and check results to see
            ' if a ShopLocationID is returned
            '---------------------------------
            oXMLReturn = APDFoundation.ExecuteSpAsXML(sStoredProcedure, sParams)
            If oXMLReturn.InnerText.Contains("ERROR") Then
                '---------------------------------
                ' Debug Data
                '---------------------------------
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "ERROR occured while checking vehicle exists: @ " & Now, "ERROR: Execute check if vehicle exists.  Details in EventXML", "StoredProcedure=" & sStoredProcedure & ", Prams=" & sParams & ", Results=" & oXMLReturn.InnerText)
                Return True
            Else
                ' Continue checking vehicle number
                oXMLNode = oXMLReturn.SelectSingleNode("/Vehicle[@VehicleNumber=" & Str(iVehicleNumber) & "]")

                Try
                    If oXMLNode.InnerXml <> "" Then
                        Return True
                    Else
                        Return False
                    End If
                Catch ex As Exception
                    Return False
                End Try
            End If

            '---------------------------------------
            ' Log Client Side Event
            '---------------------------------------
            If UCase(sAPDNetDebugging) = "TRUE" Then
                APDFoundation.LogEvent("WSClientSideClaimProcessor", "END - Server: " & sProcessingServer, "Checking if vehicle already exists - ended @ " & Now, "", "")
            End If
        Catch oExcept As Exception
            APDFoundation.LogEvent("WSClientSideClaimProcessor", "ERROR - Server: " & sProcessingServer, "CheckIfVehicleAlreadyExist - ExecuteSpAsString failed: ", "Reason: " & oExcept.Message, "")
            Return True
        End Try

    End Function

End Class