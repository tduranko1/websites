﻿Imports PGW.Business.Environment
Public Class SubmitAPDWcf

    Public Function GetXML(ByVal strParamXML As String) As String
        Dim strReturnXMl As String = String.Empty
        Dim objEcadDev As EcadAccessor.Dev.EcadAccessorClient = Nothing
        Dim objEcadStg As EcadAccessor.STG.EcadAccessorClient = Nothing
        Dim objEcacPRD As Object = Nothing

        Try

            Select Case (PGW.Business.Environment.getEnvironment())
                Case "DEV"
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    strReturnXMl = objEcadDev.GetXML(strParamXML)
                Case "STG"
                    objEcadStg = New EcadAccessor.STG.EcadAccessorClient()
                    strReturnXMl = objEcadStg.GetXML(strParamXML)
                Case "PRD"
                Case Else
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    strReturnXMl = objEcadDev.GetXML(strParamXML)
            End Select
        Catch ex As Exception
            Throw ex
        End Try

        Return strReturnXMl
    End Function

    Public Function PutXML(ByVal strParamXML As String) As String
        Dim strReturnXMl As String = String.Empty
        Dim objEcadDev As EcadAccessor.Dev.EcadAccessorClient = Nothing
        Dim objEcadStg As EcadAccessor.STG.EcadAccessorClient = Nothing
        Dim objEcacPRD As Object = Nothing

        Try

            Select Case (PGW.Business.Environment.getEnvironment())
                Case "DEV"
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    strReturnXMl = objEcadDev.PutXML(strParamXML)
                Case "STG"
                    objEcadStg = New EcadAccessor.STG.EcadAccessorClient()
                    strReturnXMl = objEcadStg.PutXML(strParamXML)
                Case "PRD"
                Case Else
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    strReturnXMl = objEcadDev.PutXML(strParamXML)
            End Select
        Catch ex As Exception
            Throw ex
        End Try

        Return strReturnXMl
    End Function

    Public Function GetDocumentXML(ByVal strParamXML As String) As String
        Dim strReturnXMl As String = String.Empty
        Dim objEcadDev As EcadAccessor.Dev.EcadAccessorClient = Nothing
        Dim objEcadStg As EcadAccessor.STG.EcadAccessorClient = Nothing
        Dim objEcacPRD As Object = Nothing

        Try

            Select Case (PGW.Business.Environment.getEnvironment())
                Case "DEV"
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    strReturnXMl = objEcadDev.GetDocumentXML(strParamXML)
                Case "STG"
                    objEcadStg = New EcadAccessor.STG.EcadAccessorClient()
                    strReturnXMl = objEcadStg.GetDocumentXML(strParamXML)
                Case "PRD"
                Case Else
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    strReturnXMl = objEcadDev.GetDocumentXML(strParamXML)
            End Select
        Catch ex As Exception
            Throw ex
        End Try

        Return strReturnXMl
    End Function

    Public Function GetDocument(ByVal strParamXML As String) As Object
        Dim objReturn As Object = Nothing
        Dim objEcadDev As EcadAccessor.Dev.EcadAccessorClient = Nothing
        Dim objEcadStg As EcadAccessor.STG.EcadAccessorClient = Nothing
        Dim objEcacPRD As Object = Nothing
        Try

            Select Case (PGW.Business.Environment.getEnvironment())
                Case "DEV"
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    objReturn = objEcadDev.GetDocument(strParamXML)
                Case "STG"
                    objEcadStg = New EcadAccessor.STG.EcadAccessorClient()
                    objReturn = objEcadStg.GetDocument(strParamXML)
                Case "PRD"
                Case Else
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    objReturn = objEcadDev.GetDocument(strParamXML)
            End Select
        Catch ex As Exception
            Throw ex
        End Try

        Return objReturn
    End Function

    Public Function GetDateTime() As String
        Dim strReturnXMl As String = String.Empty
        Dim objEcadDev As EcadAccessor.Dev.EcadAccessorClient = Nothing
        Dim objEcadStg As EcadAccessor.STG.EcadAccessorClient = Nothing
        Dim objEcacPRD As Object = Nothing

        Try

            Select Case (PGW.Business.Environment.getEnvironment())
                Case "DEV"
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    strReturnXMl = objEcadDev.GetDateTime()
                Case "STG"
                    objEcadStg = New EcadAccessor.STG.EcadAccessorClient()
                    strReturnXMl = objEcadStg.GetDateTime()
                Case "PRD"
                Case Else
                    objEcadDev = New EcadAccessor.Dev.EcadAccessorClient()
                    strReturnXMl = objEcadDev.GetDateTime()
            End Select
        Catch ex As Exception
            Throw ex
        End Try

        Return strReturnXMl
    End Function

End Class
