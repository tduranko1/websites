VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsAPDData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************************************************************************
'* Component PPGNAAPD : Class clsAppData
'*
'* This class wraps calls to the LYNX APD LAPDEcadAccessor component proxy.
'* This wrapping provides a retry loop for the RPC.  It also provides specific
'* error logging.  Its primary purpose, however, was to run under the lynxetem
'* local user account so that an NT securiy hole could be used to slip through
'* the eTemplate firewall.
'*
'********************************************************************************
Option Explicit

Const MODULE_NAME As String = "PPGNAAPD.clsAPDData."
Const PPGNAAPD_FirstError As Long = &H80061100

'API declares
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'App Logger error levels
Private Enum enmErrorLevel
    Informational = 1
    Warning
    ApplicationError
    CriticalError
End Enum

'App Logger object.
Private moAppLog As Object

'********************************************************************************
'* These are available to the caller for managed event handling.
'********************************************************************************
Public InternalEvent As String
Public ExternalEvent As String
Public EventNumber As Long

'********************************************************************************
'* Clears the above error information.
'********************************************************************************
Private Sub ClearEvent()
10        InternalEvent = ""
20        ExternalEvent = ""
30        EventNumber = 0
End Sub

Private Function GetWebServiceURL() As String
Dim strmexMonikerString
Const PROC_NAME As String = MODULE_NAME & "GetWebServiceURL: "
    Dim objFSO, ts, GetEnvironment
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    On Error GoTo ErrHandler
    Call ClearEvent
    
    Select Case GetEnvironment
        Case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        Case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        Case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
     Case "DR"
        strmexMonikerString = "service:mexAddress=http://DRlynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://DRlynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
            
    End Select
    
   GetWebServiceURL = strmexMonikerString
   
ErrHandler:

       HandleEvents Err.Number, PROC_NAME, Erl, Err.Source, Err.Description, "Issue on GetWebServiceURL " & "strmexMonikerString"
          
End Function

Private Function GetHTTPServiceURL()
    Dim strServiceURL
    Dim objFSO, ts, GetEnvironment
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select Case GetEnvironment
        Case "DEV"
             strServiceURL = "http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc?wsdl"
        Case "STG"
             strServiceURL = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc?wsdl"
        Case "PRD"
             strServiceURL = "http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc?wsdl"
      Case "DR"
             strServiceURL = "http://DRlynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc?wsdl"
    End Select
    GetHTTPServiceURL = strServiceURL
End Function

Private Function GetXMLHttp(ByVal strparamXML As String) As String
'/* Call the web service */
Dim xmlHttp, xmlResponse, postUrl
Dim dataToSend
Dim sXmlCall
Dim strReturnXML, strSplitResult, strResult


Set xmlHttp = CreateObject("MSXML2.ServerXMLHTTP")
postUrl = GetHTTPServiceURL()

dataToSend = "" & "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">" & "<soapenv:Body>" & "<GetXML xmlns=""http://LynxAPDComponentServices""><strParamXML>" & "<![CDATA[" & strparamXML & " ]]>" & "</strParamXML></GetXML>" & "</soapenv:Body>" & "</soapenv:Envelope>"
Call xmlHttp.Open("POST", postUrl, False, "", "")

xmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
xmlHttp.setRequestHeader "SOAPAction", "http://LynxAPDComponentServices/IEcadAccessor/GetXML"
xmlHttp.send dataToSend

strReturnXML = HtmlDecode(xmlHttp.responseText)

strSplitResult = Split(strReturnXML, "<GetXMLResult>")(1)
strResult = Split(strSplitResult, "</GetXMLResult>")(0)

GetXMLHttp = strResult

Set xmlHttp = Nothing

End Function

Private Function HtmlDecode(htmlString)
                Dim returnValue
                
                returnValue = htmlString
                returnValue = Replace(returnValue, "&lt;", "<")
                returnValue = Replace(returnValue, "&gt;", ">")
                returnValue = Replace(returnValue, "&quot;", """")
                returnValue = Replace(returnValue, "&amp;", "&")
                
                HtmlDecode = returnValue
End Function

'********************************************************************************
'* Standard XML Get from APD
'********************************************************************************
Public Function GetXML(ByVal strparamXML As String) As String
          Const PROC_NAME As String = MODULE_NAME & "GetXML: "
          
10        On Error GoTo ErrHandler
          
20        Call ClearEvent
          
          Dim mexMonikerString
          Dim loData As Object
          
          'If Len(strParamXML) > 5000 Then
            'Get the web service string to create the object
            'mexMonikerString = GetWebServiceURL()
            'Set loData = GetObject(mexMonikerString)
            'GetXML = loData.GetXML(strParamXML)
         ' Else
            GetXML = GetXMLHttp(strparamXML)
          'End If
          
          'Get the web service string to create the object
          'mexMonikerString = GetWebServiceURL()
30        'Set loData = CreateObjectEx("LAPDEcadAccessor.CData")
          'Set loData = GetObject(mexMonikerString)
          
40        'GetXML = GetXMLHttp(strParamXML)
          
ErrHandler:

50        Set loData = Nothing

60        HandleEvents Err.Number, PROC_NAME, Erl, Err.Source, Err.Description, "ParamXML = " & strparamXML
          
End Function


'********************************************************************************
'* Standard XML Put to APD
'********************************************************************************
Public Function PutXML(ByVal strparamXML As String) As Long
          Const PROC_NAME As String = MODULE_NAME & "PutXML: "
          
10        On Error GoTo ErrHandler
          
20        PutXML = -1
          
30        Call ClearEvent
          
          Dim mexMonikerString
          Dim loData As Object
          
           'Get the web service string to create the object
          mexMonikerString = GetWebServiceURL()
40        'Set loData = CreateObjectEx("LAPDEcadAccessor.CData")
           Set loData = GetObject(mexMonikerString)
          
50        PutXML = loData.PutXML(strparamXML)
          
ErrHandler:

60        Set loData = Nothing

70        HandleEvents Err.Number, PROC_NAME, Erl, Err.Source, Err.Description, "ParamXML = " & strparamXML
          
End Function

'********************************************************************************
'* Get Special Document XML (CCC print images)
'********************************************************************************
Public Function GetDocumentXML(ByVal strparamXML As String) As String
          Const PROC_NAME As String = MODULE_NAME & "GetDocumentXML: "
          
10        On Error GoTo ErrHandler
          
20        Call ClearEvent
          
          Dim mexMonikerString
          Dim loData As Object
          
          'Get the web service string to create the object
          mexMonikerString = GetWebServiceURL()
30        'Set loData = CreateObjectEx("LAPDEcadAccessor.CDocument")
          Set loData = GetObject(mexMonikerString)
          
40        GetDocumentXML = loData.GetDocumentXML(strparamXML)
          
ErrHandler:

50        Set loData = Nothing

60        HandleEvents Err.Number, PROC_NAME, Erl, Err.Source, Err.Description, "ParamXML = " & strparamXML

End Function

'********************************************************************************
'* Get a Binary Document from APD
'********************************************************************************
Public Function GetDocument(ByVal strparamXML As String) As Variant
          Const PROC_NAME As String = MODULE_NAME & "GetDocument: "
          
10        On Error GoTo ErrHandler
          
20        Call ClearEvent
          
          Dim mexMonikerString
          Dim loData As Object
          
          'Get the web service string to create the object
          mexMonikerString = GetWebServiceURL()
30        'Set loData = CreateObjectEx("LAPDEcadAccessor.CDocument")
          Set loData = GetObject(mexMonikerString)
40        GetDocument = loData.GetDocument(strparamXML)
          
ErrHandler:

50        Set loData = Nothing

60        HandleEvents Err.Number, PROC_NAME, Erl, Err.Source, Err.Description, "ParamXML = " & strparamXML
          
End Function

'********************************************************************************
'* Class Initialize
'********************************************************************************
Public Sub Initialize(ByVal piAppLogID As Long)
          Const PROC_NAME As String = MODULE_NAME & "Initialize: "
          
10        On Error Resume Next
          
          'Initialize the App Log Object
20        Set moAppLog = CreateObjectEx("eTemAppLog.clsAppLog")
          
30        moAppLog.AppID = CInt(piAppLogID)
40        moAppLog.Component = MODULE_NAME
          
50        HandleEvents Err.Number, PROC_NAME, Erl, Err.Source, Err.Description, "AppLogID = " & piAppLogID
          
End Sub

'********************************************************************************
'* Class Terminate
'********************************************************************************
Private Sub Class_Terminate()
10        On Error Resume Next
20        Set moAppLog = Nothing
End Sub

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Private Function CreateObjectEx(ByVal strObjectName As String) As Object
          Dim obj As Object
          Dim liCount As Integer
          
10        On Error Resume Next
          
20        Set CreateObjectEx = Nothing
          
30        For liCount = 0 To 10
          
40            Set obj = CreateObject(strObjectName)
              
50            If Not obj Is Nothing Then
60                liCount = 10
70            Else
80                DoSleep 200
90            End If
                  
100       Next

110       Set CreateObjectEx = obj
          
120       On Error GoTo 0
          
130       If obj Is Nothing Then
140           Err.Raise PPGNAAPD_FirstError + 10, "CreateObjectEx()", _
                  "CreateObject('" & strObjectName & "') returned Nothing."
150       End If
          
160       Set obj = Nothing
          
End Function

'********************************************************************************
'* Syntax:      DoSleep( 500 )
'* Parameters:  lngMS - milliseconds to sleep.
'* Purpose:     Wraps Sleep() with calls to VB DoEvents().
'* Returns:     Nothing.
'********************************************************************************
Private Sub DoSleep(ByVal lngMS As Long)
          Dim lCount As Long
10        For lCount = 1 To lngMS \ 10
20            Sleep 10
30            DoEvents
40        Next
End Sub

'********************************************************************************
'* General event handler and logger.  Does nothing if no error occurs.
'* Will attempt to extract external and internal messages from what APD
'* threw back, if anything.
'********************************************************************************
Private Sub HandleEvents( _
    ByVal lngNumber As Long, _
    ByVal strMethod As String, _
    ByVal lngLine As Long, _
    ByVal strSource As String, _
    ByVal strDesc As String, _
    ByVal strTrace As String)
          
10        EventNumber = lngNumber
20        InternalEvent = ""
30        ExternalEvent = ""
          
40        If lngNumber <> 0 Then
          
50            InternalEvent = ExtractApdMessage(strDesc, "SI")
60            ExternalEvent = ExtractApdMessage(strDesc, "SE")
              
              'Log internal events.
70            If InternalEvent <> "" Then
80                moAppLog.logMessage strMethod & "[Line " & lngLine & "]-->" & strSource & "-->" & _
                      InternalEvent & vbCrLf & strTrace, _
                      lngNumber, ApplicationError, strMethod
90            End If
              
100       End If
          
End Sub

'********************************************************************************
'* Extracts APD messages stored in the passed Err.Description.
'* These messages are wrapped by "|##>" and "<|", where
'* ## is equal to strMoniker: "SE" for external, "SI" for internal.
'********************************************************************************
Private Function ExtractApdMessage(ByVal strDesc As String, ByVal strMoniker As String) As String

          Dim intPos As Integer
          Dim strExt As String
          Dim strMsg As String
          Dim intIdx As Integer

          'Does this look like and APD message?
10        If InStr(strDesc, "Time =") > 0 Then
          
20            intPos = InStr(strDesc, "|" & strMoniker & ">")

30            While intPos > 0
          
                  'The code below is heavy because the data we are extracting
                  'may have been truncated.  All the if-thens below prevent
                  'an error from being tossed below by this.
          
40                strExt = Mid(strDesc, intPos + Len(strMoniker) + 4)
          
50                intIdx = InStr(strExt, "<|")
60                If intIdx > 2 Then
          
70                    strDesc = Mid(strExt, intIdx + 2)
80                    strExt = Left(strExt, intIdx - 1)
          
90                    If Len(strMsg) > 0 Then
100                       strMsg = strMsg & "|" & strExt
110                   Else
120                       strMsg = strExt
130                   End If
140                   intPos = InStr(strDesc, "|" & strMoniker & ">")
          
150               Else
160                   intPos = 0
170               End If
          
180           Wend
          
190           ExtractApdMessage = strMsg
              
200       ElseIf strMoniker = "SI" Then
210           ExtractApdMessage = strDesc
220       ElseIf strMoniker = "SE" Then
230           ExtractApdMessage = "An unspecified error has occurred.  Please logout and try again in a few minutes."
240       End If
          
End Function

