VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsXML"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***********************************************************************************
'Class:  clsXML.cls
'Description:  Helper utilities wrapping MSXML funcitonality
'***********************************************************************************
Option Explicit

'---------------------------------------------------------------------------------
'Error handling declarations
'Pick a base number for our errors
Private Const mcBaseErrorNum As Long = vbObjectError + 600

'Error numbers
Public Enum XMLErrors
    ERRNUM_CANNOT_LOAD_XML_FILE = mcBaseErrorNum + 1
    ERRNUM_NODE_DOES_NOT_EXIST
    ERRNUM_CANNOT_LOAD_XML_DATA
End Enum

'Error messages
Private Const ERRMSG_CANNOT_LOAD_XML_FILE As String = "Could not load XML file"
Private Const ERRMSG_NODE_DOES_NOT_EXIST As String = _
    "The referenced node does not exist"
Private Const ERRMSG_CANNOT_LOAD_XML_DATA As String = "Could not load XML data"

Private msCodeLocation As String
Private mobjDOM As MSXML2.DOMDocument40

'Standard error-handling function
'Adds descriptions to custom errors and adds the entire call stack to err.source
'Usage:  call RecordError
Private Sub RecordError()
          
          'Add messages for custom errors
10        Select Case Err.Number
              Case ERRNUM_CANNOT_LOAD_XML_FILE:
20                Err.Description = ERRMSG_CANNOT_LOAD_XML_FILE & ":" & mobjDOM.parseError.reason
        Case ERRNUM_NODE_DOES_NOT_EXIST:
30                Err.Description = ERRMSG_NODE_DOES_NOT_EXIST
        Case ERRNUM_CANNOT_LOAD_XML_DATA:
40                Err.Description = ERRMSG_CANNOT_LOAD_XML_DATA & ":" & mobjDOM.parseError.reason
50        End Select

          'Add the location of the current exception to the exception call stack
          'This way  err.source reports the entire call stack
60        Err.Source = msCodeLocation & " --> " & Err.Source
          
          'Log the error message
          Dim moAppLog As Object
70        Set moAppLog = CreateObjectEx("eTemAppLog.clsLog")
80        Call moAppLog.LogMessage(Err.Description, CStr(Err.Number), 3, Err.Source)
90        Set moAppLog = Nothing
          
End Sub

'Loads an XML File from a resource
'Usage:  Call lobjXML.load(lsXMLFile)
'Where: sXMLFile is either:
'           1)A URL to the XML file (i.e. http://www.foo.com/file.xml)
'           2)A UNC to the XML file (i.e. \\foo\bar\file.xml, d:\foo\file.xml)
Public Sub load(sXMLFile)

10        On Error GoTo errorhandler
            
          'Load the XML file into memory
20        If Not (mobjDOM.load(sXMLFile)) Then
30            Err.Raise ERRNUM_CANNOT_LOAD_XML_FILE, "clsXML.load()", "Could not open file " & sXMLFile
40        End If

50    Exit Sub
errorhandler:

60        msCodeLocation = "clsXML.load(" & sXMLFile & ")"
70        Call RecordError
80        Err.Raise Err.Number, Err.Source, Err.Description
End Sub

'Returns the value of a particular node in the XML tree
'Usage: lvNodeValue = getNodeValue(sNode)
'Where: sNode is the node to get the value of
'           (i.e. /FOO/BAR/ITEM, or /BARS/BAR[@NAME='BAR3']/ITEM
'       lvNodeValue is the resultant value of the node
Public Function getNodeValue(ByVal sNode As String) As Variant
          
10        On Error GoTo errorhandler
          
          Dim lobjNode As MSXML2.IXMLDOMNode
          Dim lblnResult As Boolean
             
          'Find the value of the specified node and return it
20        Set lobjNode = mobjDOM.selectSingleNode(sNode)
          'Make sure the node exists
          'For some reason, isObject always returns true, so we have to
          'catch a 91--Object not set error
          
          'Try to get the value.  If we're not instantiated, we will fail
          'with an error of 91, object not found
30        If lobjNode Is Nothing Then
40            getNodeValue = ""
50        Else
60            getNodeValue = lobjNode.Text
70        End If
             
          'Cleanup
80        Set lobjNode = Nothing
          
90    Exit Function
errorhandler:
          'Cleanup
100       Set lobjNode = Nothing

110       msCodeLocation = "clsXML.getNodeValue"
120       Call RecordError
130       Err.Raise Err.Number
End Function

'Adds a Node to an XML document
'Usage: call addChildNode(sParentNode ,sChildNodeName,
'                vChildNodeValue)
'Where: sParentNode defines the node of the parent
'           (i.e. /FOO/BAR/ITEM, or /BARS/BAR[@NAME='BAR3']/ITEM
'       sChildNodeName is the name of the Child node to be added
'       sChildNodeValue is the value of the child node to be added
Public Sub addChildNode(ByVal sParentNode As String, _
            ByVal sChildNodeName As String, ByVal vChildNodeValue As Variant)
                  
10        On Error GoTo errorhandler
              
          Dim lblnResult As Boolean
          Dim lobjParentNode As MSXML2.IXMLDOMNode
          Dim lobjChildNode As MSXML2.IXMLDOMNode
                
          'Get the parent node
20        Set lobjParentNode = mobjDOM.selectSingleNode(sParentNode)
          
          'Create the child node
30        Set lobjChildNode = mobjDOM.createElement(sChildNodeName)
             
          'Set the value of the child node
40        lobjChildNode.Text = vChildNodeValue
          
          'Append the child to the parent
50        lobjParentNode.appendChild lobjChildNode
          
          'Cleanup
60        Set lobjParentNode = Nothing
70        Set lobjChildNode = Nothing
          
80    Exit Sub
errorhandler:

          'If we had try/catch/finally, this would be more elegant
          'Cleanup
90        Set lobjParentNode = Nothing
100       Set lobjChildNode = Nothing

110       msCodeLocation = "clsXML.addChildNode"
120       Call RecordError
130       Err.Raise Err.Number
End Sub

'Change the value of a particular node in the tree
'Usage: call changeNodeValue(sNode,vNewValue)
'Where: sNode is the node to be modified
'           (i.e. /FOO/BAR/ITEM, or /BARS/BAR[@NAME='BAR3']/ITEM
'       vNewValue is the new value for the node
Public Sub changeNodeValue(ByVal sNode As String, ByVal vNewValue As Variant)
10        On Error GoTo errorhandler
          
          Dim lobjNode As MSXML2.IXMLDOMNode
          Dim lblnResult As Boolean
            
          'get the specified node
20        Set lobjNode = mobjDOM.selectSingleNode(sNode)
          
          'Make sure the node exists by catching error 91--object not set
30        On Error Resume Next
          
          'Change the value of the specified node
40        lobjNode.Text = vNewValue
       
50        If Err.Number <> 0 Then
60            If Err.Number = 91 Then 'Object or with block variable not set
70                On Error GoTo errorhandler
80                Err.Raise ERRNUM_NODE_DOES_NOT_EXIST
90            Else
                  'We have a real error
100               GoTo errorhandler
110           End If
120       End If
          
          'Go back to the standard error handler
130       On Error GoTo errorhandler
          
          'Cleanup
140       Set lobjNode = Nothing
          
150   Exit Sub
errorhandler:
          'Cleanup
160       Set lobjNode = Nothing

          'Record the error and raise up to the next level
170       msCodeLocation = "clsXML.changeNodeValue"
180       Call RecordError
190       Err.Raise Err.Number
End Sub
'Delete a particular node from the tree
'Usage: call deleteNodeValue(sNode)
'Where: sNode is the node to be deleted
'           (i.e. /FOO/BAR/ITEM, or /BARS/BAR[@NAME='BAR3']/ITEM
Public Sub deleteNode(ByVal sNode As String)
10        On Error GoTo errorhandler
          
          Dim lblnResult As Boolean
          Dim lobjNode As MSXML2.IXMLDOMNode
           
          'Get the specified node
20        Set lobjNode = mobjDOM.selectSingleNode(sNode)
          
          'Make sure the node exists
          'For some reason isObject doesn't work.
          'Instead we need to check for an error when referencing the method
30        On Error Resume Next
          
          'Try to Delete the specified node
          'If the node does not exist, we'll get an error (91 -- object not set)
40        lobjNode.parentNode.removeChild (lobjNode)
           
50        If Err.Number <> 0 Then
60            If Err.Number = 91 Then 'Object or with block variable not set
70                On Error GoTo errorhandler
80                Err.Raise ERRNUM_NODE_DOES_NOT_EXIST
90            Else
                  'we have a real error
100               GoTo errorhandler
110           End If
120       End If
          
          'Go back to the standard error handler
130       On Error GoTo errorhandler
          
          'Cleanup
140       Set lobjNode = Nothing
          
150   Exit Sub
errorhandler:

          'Cleanup
160       Set lobjNode = Nothing

170       msCodeLocation = "clsXML.deleteNode"
180       Call RecordError
190       Err.Raise Err.Number
          
End Sub
'Check to see whether a node exists
'Usage: blnResult = isNode(sNode)
'Where: sNode is the node to check for existence of
'           (i.e. /FOO/BAR/ITEM, or /BARS/BAR[@NAME='BAR3']/ITEM
Public Function isNode(ByVal sNode As String) As Variant

10        On Error GoTo errorhandler
          
          Dim lobjNode As MSXML2.IXMLDOMNode
          Dim lblnResult As Boolean
          Dim lvNodeValue As Variant
            
          'Try to get the specified node.  If we can't, it doesn't exist.
20        Set lobjNode = mobjDOM.selectSingleNode(sNode)
          
          'Find out if the node was returned.
          'Originally, wanted to use isObject, but this doesn't work
          'Catching error 91 after checking value instead
                  
30        If lobjNode Is Nothing Then
40            isNode = False
50        Else
60            isNode = True
70        End If
          
          
          'Cleanup
80        Set lobjNode = Nothing
          
90    Exit Function
errorhandler:

          'Cleanup
100       Set lobjNode = Nothing

110       msCodeLocation = "clsXML.NodeHasText"
120       Call RecordError
130       Err.Raise Err.Number
End Function
'Add the root node to the DOM Document
'Usage: call AddRootElement(sElementName)
'Where: sElementName is the name of the root node element
Public Sub AddRoot(sNodeName As String)

10        On Error GoTo errorhandler

20        Call mobjDOM.appendChild(mobjDOM.createElement(sNodeName))

30    Exit Sub
errorhandler:
40        msCodeLocation = "clsXML.AddRoot"
50        Call RecordError
60        Err.Raise Err.Number
End Sub

Private Sub Class_Initialize()

10        On Error GoTo errorhandler
          
          'Create a new DOM to hold our data
20        Set mobjDOM = New MSXML2.DOMDocument40
30        mobjDOM.Async = False
40        mobjDOM.ValidateOnParse = True
50    Exit Sub
errorhandler:
60        msCodeLocation = "clsXML.Class_Initialize"
70        Call RecordError
80        Err.Raise Err.Number
End Sub

Private Sub Class_Terminate()
10        Set mobjDOM = Nothing
End Sub

'Sets the async property for the DOM Document
'Determines whether DOMDocument should be processed asynchronously
'(Usually not necessary or recommended)
Public Property Let Async(ByVal blnNewValue As Boolean)

10        On Error GoTo errorhandler

20        mobjDOM.Async = blnNewValue

30    Exit Property
errorhandler:
40        msCodeLocation = "clsXML.Let Async"
50        Call RecordError
60        Err.Raise Err.Number
End Property
'Sets the validate on Parse Value for the DOM Document
'Determines whether the DOM Document is auto-validated
Public Property Let ValidateOnParse(ByVal blnNewValue As Boolean)

10        On Error GoTo errorhandler

20        mobjDOM.ValidateOnParse = blnNewValue

30    Exit Property
errorhandler:
40        msCodeLocation = "clsXML.Let ValidateOnParse"
50        Call RecordError
60        Err.Raise Err.Number
End Property
'Returns the DOM Document Object
'This may be important if some functionality is required of the DOM
'That is not subclassed in the currenlty available object
'Don't you wish VB had inheritance?  We could have inherited everything
Public Property Get DOM() As Object
10        On Error GoTo errorhandler
20        Set DOM = mobjDOM
30    Exit Property
errorhandler:
40        msCodeLocation = "clsXML.Get DOM"
50        Call RecordError
60        Err.Raise Err.Number
End Property
'Sets the DOM Document Object
'Usage:  set lobjXML.DOM = lobjMyNewDOM
'Where lobjMyNewDOM is the new DOM you can lobjXML to use
'This may be important if some functionality is required of the DOM
'That is not subclassed in the currenlty available object
'Don't you wish VB had inheritance?  We could have inherited everything
Public Property Set DOM(ByVal objNewDOM As MSXML2.DOMDocument40)
10        On Error GoTo errorhandler
20        Set mobjDOM = objNewDOM
30    Exit Property
errorhandler:
40        msCodeLocation = "clsXML.Set DOM"
50        Call RecordError
60        Err.Raise Err.Number
End Property

'Returns the XML for the DOM
Public Property Get XML() As String
10        On Error GoTo errorhandler
20            XML = mobjDOM.XML
30    Exit Property
errorhandler:
40        msCodeLocation = "clsXML.Get XML"
50        Call RecordError
60        Err.Raise Err.Number
End Property

'Returns the TEXT for the DOM
Public Property Get Text() As String
10        On Error GoTo errorhandler
20        Text = mobjDOM.Text
30    Exit Property
errorhandler:
40        msCodeLocation = "clsXML.Get Text"
50        Call RecordError
60        Err.Raise Err.Number
End Property

'Adds a Node to an XML document and returns that node
'Usage: set lobjNewNode = appendChildToNode(lobjParentNode,
'           sChildNodeName, vChildNodeValue)
'Where: lobjParentNode is the node of the parent
'       sChildNodeName is the name of the Child node to be added
'       sChildNodeValue is the value of the child node to be added
'       lobjNewNode is the new node
Public Function appendChildToNode(ByVal objParentNode As MSXML2.IXMLDOMNode, _
    ByVal sChildNodeName As String, ByVal vChildNodeValue As Variant) As MSXML2.IXMLDOMNode
          
10        On Error GoTo errorhandler
              
          Dim lblnResult As Boolean
          Dim lobjChildNode As MSXML2.IXMLDOMNode
                   
          'Create the child node
20        Set lobjChildNode = mobjDOM.createElement(sChildNodeName)
             
          'Set the value of the child node
30        lobjChildNode.Text = vChildNodeValue
          
          'Append the child to the parent
40        Set lobjChildNode = objParentNode.appendChild(lobjChildNode)
          
          'Return the childnode
50        Set appendChildToNode = lobjChildNode
          
          'Cleanup
60        Set lobjChildNode = Nothing
          
70    Exit Function
errorhandler:

          'If we had try/catch/finally, this would be more elegant
          'Cleanup
80        Set lobjChildNode = Nothing

90        msCodeLocation = "clsXML.appendChildToNode"
100       Call RecordError
110       Err.Raise Err.Number
End Function

'Returns a node from the tree
'Usage: lobjNode = getNode(sNode)
'Where: sNode is the XPATH reference to the node
'           (i.e. /FOO/BAR/ITEM, or /BARS/BAR[@NAME='BAR3']/ITEM
Public Function selectSingleNode(ByVal sNode As String) As MSXML2.IXMLDOMNode

10        On Error GoTo errorhandler
                
          'Get the specified node
20        Set selectSingleNode = mobjDOM.selectSingleNode(sNode)
          
30    Exit Function
errorhandler:
40        msCodeLocation = "clsXML.selectSingleNode"
50        Call RecordError
60        Err.Raise Err.Number
End Function

'Loads an XML Object from text
'Usage:  Call lobjXML.load(lsXMLText)
'Where: lsXMLTest is the XML data stored in a string
Public Sub loadXML(sXMLText As String)

10        On Error GoTo errorhandler
            
          'Load the XML file into memory
20        If Not (mobjDOM.loadXML(sXMLText)) Then
30            Err.Raise ERRNUM_CANNOT_LOAD_XML_DATA
40        End If

50    Exit Sub
errorhandler:
60        msCodeLocation = "clsXML.loadXML"
70        Call RecordError
80        Err.Raise Err.Number
End Sub
'Save the xml file to the specified filename
Public Sub Save(sFileName As String)
10        On Error GoTo errorhandler
20        Call mobjDOM.Save(sFileName)
30    Exit Sub
errorhandler:
40        msCodeLocation = "clsXML.Save"
50        Call RecordError
60        Err.Raise Err.Number
End Sub


'Creates a well-formed node based on input.  Marks up problematic characters
'Usage:  lsNode = lobjXML.createNode(lsNodeName,lsNodeValue)
'Where:  lsNodeName is the name of the node
'        lsNodeValue is the text to be placed inside the node
'        lsNode is the resultant well-formed node (i.e. <foo>bar</foo>)
Public Function createNode(sNodeName As String, sNodeText As String) As String

10        On Error GoTo errorhandler
          Dim lsNode As String
          Dim lsNodeText As String
          
          'Start Tag
20        lsNode = "<" & sNodeName & ">"
          
          'Value -- clean it up
30        lsNodeText = Trim(Replace(sNodeText, "&", "&amp;"))
40        lsNodeText = Replace(lsNodeText, "<", "&lt;")
50        lsNodeText = Replace(lsNodeText, ">", "&gt;")
60        lsNodeText = Replace(lsNodeText, """", "&quot;")
70        lsNodeText = Replace(lsNodeText, "'", "&apos;")
          
80        lsNode = lsNode & lsNodeText
              
          'End Tag
90        lsNode = lsNode & "</" & sNodeName & ">"
          
100       createNode = lsNode
          
110   Exit Function
errorhandler:
120       createNode = "Error"
130       msCodeLocation = "clsXML.createNode"
140       Call RecordError
150       Err.Raise Err.Number
End Function

'Check to see whether a node contains anything
'Usage: blnResult = NodeHasText(sNode)
'Where: sNode is the node to check for existence of text in
'           (i.e. /FOO/BAR/ITEM, or /BARS/BAR[@NAME='BAR3']/ITEM
Public Function NodeHasText(ByVal sNode As String) As Variant

10        On Error GoTo errorhandler
          
          Dim lobjNode As MSXML2.IXMLDOMNode
          Dim lblnResult As Boolean
          Dim lvNodeValue As Variant
            
          'Try to get the specified node.  If we can't, it doesn't exist.
20        Set lobjNode = mobjDOM.selectSingleNode(sNode)
          
          'Find out if the node was returned.
          'Originally, wanted to use isObject, but this doesn't work
          'Catching error 91 after checking value instead
30        On Error Resume Next
              
40        lvNodeValue = lobjNode.Text
          
50        If Err.Number <> 0 Then
60            If Err.Number = 91 Then
70                NodeHasText = False
80            Else
                  'We have a real error
90                GoTo errorhandler
100           End If
110       Else
120           If Not IsNull(lvNodeValue) Then
130               If Len(Trim(lvNodeValue)) > 0 Then
140                   NodeHasText = True
150               Else
160                   NodeHasText = False
170               End If
180           Else
190               NodeHasText = False
200           End If
210       End If
          
          'Cleanup
220       Set lobjNode = Nothing
          
230   Exit Function
errorhandler:

          'Cleanup
240       Set lobjNode = Nothing

250       msCodeLocation = "clsXML.NodeHasText"
260       Call RecordError
270       Err.Raise Err.Number
End Function


