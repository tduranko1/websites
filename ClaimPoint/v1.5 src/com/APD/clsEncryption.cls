VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsEncryption"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***********************************************************************************
'Class:  clsEncryption.cls
'**********************************************************************************
Option Explicit

'Error Handling Constants

'Pick a base number for our errors
Private Const mcBaseErrorNum As Long = vbObjectError + 500

Private msCodeLocation As String

Private Sub RecordError()
          
          'Add the location of the current exception to the exception call stack
10        Err.Source = msCodeLocation & " --> " & Err.Source
          
          'Log the error message
          'Dim lobjLOG As clsLog
          'Set lobjLOG = New clsLog
          'Call lobjLOG.LogMessage(Err.Source & "::(" & Err.Number & ")" & Err.Description)
End Sub

'Use variants for returning values so that we can be called from ASP
Public Function getEncryptedValue(ByVal sData As String, ByVal sKey As String) As Variant


          'For error handling
10        On Error GoTo errorhandler
                             
20        getEncryptedValue = Crypt(sData, sKey)
              
30    Exit Function
errorhandler:
40        msCodeLocation = "clsEncryption.getEncryptedValue"
50        Call RecordError
60        Err.Raise Err.Number
End Function
'Use variants for returning values so that we can be called from ASP
Public Function getDecryptedValue(sData As String, sKey As String) As Variant
          
          'For error handling
10        On Error GoTo errorhandler
          
          Dim lKey As Long
             
20        getDecryptedValue = DeCrypt(sData, sKey)
          
30    Exit Function
errorhandler:
40        msCodeLocation = "clsEncryption.getDecryptedValue"
50        Call RecordError
60        Err.Raise Err.Number
End Function

Public Function Crypt(texti, salasana) As String

10    On Error Resume Next

      Dim t
      Dim sana
      Dim x1
      Dim g
      Dim tt
      Dim crypted


20    For t = 1 To Len(salasana)
30    sana = Asc(Mid(salasana, t, 1))
40    x1 = x1 + sana
50    Next

60    x1 = Int((x1 * 0.1) / 6)
70    salasana = x1
80    g = 0

90    For tt = 1 To Len(texti)
100   sana = Asc(Mid(texti, tt, 1))
110   g = g + 1

120   If g = 6 Then g = 0
130   x1 = 0

140   If g = 0 Then x1 = sana - (salasana - 2)

150   If g = 1 Then x1 = sana + (salasana - 5)

160   If g = 2 Then x1 = sana - (salasana - 4)

170   If g = 3 Then x1 = sana + (salasana - 2)

180   If g = 4 Then x1 = sana - (salasana - 3)

190   If g = 5 Then x1 = sana + (salasana - 5)
200          x1 = x1 + g
210          crypted = crypted & Format$(x1, "000")
220   Next


230   Crypt = crypted
End Function


Public Function DeCrypt(texti, salasana) As String

          
      Dim t
      Dim sana
      Dim x1
      Dim g
      Dim tt
      Dim decrypted
          
10        On Error Resume Next

20        For t = 1 To Len(salasana)
30               sana = Asc(Mid(salasana, t, 1))
40               x1 = x1 + sana
50        Next

60    x1 = Int((x1 * 0.1) / 6)
70    salasana = x1
80    g = 0

90    For tt = 1 To Len(texti) Step 3
100   sana = Mid(texti, tt, 3)
110   g = g + 1

120   If g = 6 Then g = 0
130   x1 = 0

140   If g = 0 Then x1 = sana + (salasana - 2)

150   If g = 1 Then x1 = sana - (salasana - 5)

160   If g = 2 Then x1 = sana + (salasana - 4)

170   If g = 3 Then x1 = sana - (salasana - 2)

180   If g = 4 Then x1 = sana + (salasana - 3)

190   If g = 5 Then x1 = sana - (salasana - 5)
200          x1 = x1 - g
210          decrypted = decrypted & Chr(x1)
220   Next


230   DeCrypt = decrypted
End Function


