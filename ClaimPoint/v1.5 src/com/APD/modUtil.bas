Attribute VB_Name = "modUtil"
'********************************************************************************
'* Component APD : Module modUtil
'*
'*
'********************************************************************************
Option Explicit

Public Const APP_NAME As String = "APD."

Public Const APD_FirstError As Long = &H80061000

'API declares
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

'********************************************************************************
'* Writes a message to the event log
'* If an error occurs in processing of eTemAppLog component, we can't
'* very well write to the database.  Instead, we write to the
'* Windows NT event log using the PPGEventLog component
'* If the PPGEventLog component fails, it will write to a file called exception.log
'********************************************************************************
Public Sub WriteEventLogMessage(ByVal sMessage As String)

10        On Error Resume Next
          
          Dim loEventLog As PPGEventLog
20        Set loEventLog = New PPGEventLog
          
30        Call loEventLog.LogNTEvent("APD.dll", sMessage, ltypError)
          
40        Set loEventLog = Nothing

End Sub

'********************************************************************************
'* Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
'* Parameters:  strObjectName = The object to call CreateObject for.
'* Purpose:     Wraps CreateObject() with better debug information.
'* Returns:     The Object created or Nothing.
'********************************************************************************
Public Function CreateObjectEx(ByVal strObjectName As String) As Object
          Dim obj As Object
          
10        On Error GoTo errorhandler
          
20        Set CreateObjectEx = Nothing
          
30        Set obj = CreateObject(strObjectName)
          
40        If obj Is Nothing Then
50            Err.Raise APD_FirstError + 10, "", "CreateObject('" & strObjectName & "') returned Nothing."
60        End If
          
70        Set CreateObjectEx = obj
          
errorhandler:

80        Set obj = Nothing
          
90        If Err.Number <> 0 Then
100           Err.Raise Err.Number, "CreateObjectEx('" & strObjectName & "') " & Err.Source, Err.Description & " " & strObjectName
110       End If
          
End Function

'********************************************************************************
'* Syntax:      DoSleep( 500 )
'* Parameters:  lngMS - milliseconds to sleep.
'* Purpose:     Wraps Sleep() with calls to VB DoEvents().
'* Returns:     Nothing.
'********************************************************************************
Public Sub DoSleep(ByVal lngMS As Long)
          Dim lCount As Long
10        For lCount = 1 To lngMS \ 10
20            Sleep 10
30            DoEvents
40        Next
End Sub

