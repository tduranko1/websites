VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'***********************************************************************************
'Class: clsLog
'Description:  Generic Logging functions
'Writes to app.path\Application.Log by default
'Can specify another file as an optional parameter
'Prepends a date/time stamp to all log messages
'***********************************************************************************
Option Explicit
'Logs a message to a log file
'Usage: call logStatus(sFileName,sMessage)
'Where: sMessage is data to be logged
'       sFileName is the name of the logfile (optional)
'If sFileName is not present, writes to app.path\Application.Log
Public Sub logMessage(ByVal sMessage As String, Optional ByVal sFileName As String, Optional ByVal blnCycleLogs As Boolean)
    
    'On Error GoTo errorhandler

    Dim objFS As New FileSystemObject
    Dim objStream As Variant
    Dim lsFileName As String
    
    lsFileName = ""
    'If we're not given a filename, log to application.log in the application's path
    If IsMissing(sFileName) Then
        lsFileName = App.Path & "\Application.Log"
    Else
        lsFileName = sFileName
    End If
    
    If IsNull(lsFileName) Then
        lsFileName = App.Path & "\Application.Log"
    End If
    
    If Len(lsFileName) < 1 Then
        lsFileName = App.Path & "\Application.Log"
    End If

    'Cycle the log, if necessary
    If blnCycleLogs Then
        If LogFull(lsFileName) Then
            Call cycleLog(lsFileName)
        End If
    End If
    
    'Open the Log file for appending and write out the status line
    Set objStream = objFS.OpenTextFile(lsFileName, ForAppending, True, TristateFalse)
    objStream.WriteLine ("(" & Format$(Now, "YYYY-MM-DD HH:MM:SS") & ")" & sMessage)
    objStream.Close
    
    'Cleanup
    Set objStream = Nothing
    Set objFS = Nothing
'Exit Sub
'errorhandler:
'    On Error Resume Next
'    'If we can't log to the log file, log to the application log
'    Call App.logEvent("Could not write to logfile:" & lsFileName, vbLogEventTypeError)
'    Call App.logEvent(sMessage, vblogevent, vbLogEventTypeError)
End Sub

Public Sub cycleLog(Optional ByVal lsFileName As String)

    Dim lobjFS As New FileSystemObject
    
    'If we're not given a filename, log to application.log in the application's path
    If IsMissing(lsFileName) Then
        lsFileName = App.Path & "\Application.Log"
    End If
    
    If IsNull(lsFileName) Then
        lsFileName = App.Path & "\Application.Log"
    End If
    
    If Len(lsFileName) < 1 Then
        lsFileName = App.Path & "\Application.Log"
    End If

    If lobjFS.FileExists(lsFileName & ".old") Then
        Call lobjFS.DeleteFile(lsFileName & ".old")
    End If
    Call lobjFS.MoveFile(lsFileName, lsFileName & ".old")
    
    Set lobjFS = Nothing
End Sub

Public Function LogFull(ByVal lsFileName As String)
    Dim lobjFS As FileSystemObject
    Dim lobjFile As Scripting.File
    Set lobjFS = New FileSystemObject
    
    If lobjFS.FileExists(lsFileName) Then
        Set lobjFile = lobjFS.GetFile(lsFileName)
        If lobjFile.Size > 250000 Then
            LogFull = True
        Else
            LogFull = False
        End If
    Else
        LogFull = False
    End If
    Set lobjFS = Nothing
    Set lobjFile = Nothing
End Function

Public Function GetLog(Optional ByVal lsFileName As String)
    Dim lobjFS As FileSystemObject
    Dim lobjStream As Variant
    
    Set lobjFS = New FileSystemObject
        
    'If we're not given a filename, log to application.log in the application's path
    If IsMissing(lsFileName) Then
        lsFileName = App.Path & "\Application.Log"
    End If
    
    If IsNull(lsFileName) Then
        lsFileName = App.Path & "\Application.Log"
    End If
    
    If Len(lsFileName) < 1 Then
        lsFileName = App.Path & "\Application.Log"
    End If
    
    
    Set lobjStream = lobjFS.OpenTextFile(lsFileName)
    GetLog = lobjStream.ReadAll
    Set lobjStream = Nothing
End Function




