VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsConfig"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'***********************************************************************************
'Class:  clsConfig.cls
'Description:   This class reads/writes a configuration file, which is in the
'form of an XML document.  The structure of the xml is constant, and looks like:
'<Config>
'  <Class1>
'     <Option1>Value1</Option1>
'     <Option2>Value2</Option2>
'  </Class1>
'</Config>
'Where item CLASS1 is the namespace for the component (i.e. clsConfig)
'and OPTION1, OPTION2 are the names of configuration options
'
'Dependencies:  clsXML
'***********************************************************************************
Option Explicit

'---------------------------------------------------------------------------------
'Module-level declarations

'Constants
Private msRootNode As String
Private msEncryptionPrefix As String
Private msConfigFile As String
Private mobjXML As clsXML
'---------------------------------------------------------------------------------

'---------------------------------------------------------------------------------
'Error handling declarations
'Pick a base number for our errors
Private Const mcBaseErrorNum As Long = vbObjectError + 700

'Error numbers
Private Enum ConfigErrors
    ERRNUM_CANT_GET_ENCRYPTED_OPTION = mcBaseErrorNum + 1
    ERRNUM_CANT_SET_ENCRYPTED_OPTION = mcBaseErrorNum + 2
End Enum

'Error messages
Private Const ERRMSG_CANT_GET_ENCRYPTED_OPTION As String = _
    "An attempt was made to use getOption to read an encrypted " & _
    "option.  Use getEncryptedOption instead."
Private Const ERRMSG_CANT_SET_ENCRYPTED_OPTION As String = _
    "An attempt was made to use setOption to set an encrypted " & _
    "option.  Use setEncryptedOption instead."

Private msCodeLocation As String
'---------------------------------------------------------------------------------

'Standard error-handling function
'Adds descriptions to custom errors and adds the entire call stack to err.source
'Usage:  call RecordError
'Dependencies:  msCodeLocation has been set to an appropriate value
'               Err object has an error in it
Private Sub RecordError()
          
          'Add messages for custom errors
10        Select Case Err.Number
              Case ERRNUM_CANT_GET_ENCRYPTED_OPTION:
20                Err.Description = ERRMSG_CANT_GET_ENCRYPTED_OPTION
        Case ERRNUM_CANT_SET_ENCRYPTED_OPTION:
30                Err.Description = ERRMSG_CANT_SET_ENCRYPTED_OPTION
              
40        End Select

          'Add the location of the current exception to the exception call stack
          'This way  err.source reports the entire call stack
50        Err.Source = msCodeLocation & " --> " & Err.Source
          
          'Log the error message
          'Dim lobjLOG As clsLog
          'Set lobjLOG = New clsLog
          'Call lobjLOG.logMessage(Err.Source & "::(" & Err.Number & ")" & Err.Description)
End Sub

'Checks to see whether the option has been named correctly as an encrypted option
'All encrypted options should start with the encryption prefix
'usage: blnResult = isEncryptedOption(sOptionName)
'where: sOptionName is the name of the option
'       blnResult is a boolean return value representing whether or not the
'       option name starts with the encryption prefix
Private Function isEncryptedOption(sOptionName) As Boolean
          
          'Encrypted Options should start with the Encryption Prefix
10        If Left$(sOptionName, Len(msEncryptionPrefix)) = msEncryptionPrefix Then
20            isEncryptedOption = True
30        Else
40            isEncryptedOption = False
50        End If

60    Exit Function
70        msCodeLocation = "clsConfig.isEncryptedOption"
80        Call RecordError
90        Err.Raise Err.Number
End Function
'Gets an option from the config file
'Usage: lsValue = getOption(sNamespace,sOptionName)
'Where: sNameSpace is the namespace for the option
'       sOptionName is the name of the option
'       lsValue is the corresponding value of the option
Public Function getOption(ByVal sNamespace As String, ByVal sOptionName As String) _
                            As String
          
10        On Error GoTo errorhandler
          
          Dim lsNode As String
          
          'Define the node
20        lsNode = "/" & msRootNode & "/" & sNamespace & "/" & sOptionName
          
30        getOption = mobjXML.getNodeValue(lsNode)
          
40    Exit Function
errorhandler:
          
50        msCodeLocation = "clsConfig.getOption"
60        Call RecordError
70        Err.Raise Err.Number

End Function
'Sets an option in the config file
'Usage: call setOption(sNamespace,sOptionName,sOptionValue)
'Where: sNameSpace is the namespace for the option
'       sOptionName is the name of the option
'       sOptionValue is the value of the option
Public Sub setOption(ByVal sNamespace As String, ByVal sOptionName As String, _
                    ByVal sOptionValue As String)

10        On Error GoTo errorhandler
          
          Dim lsParentNode As String
          Dim lsNodeValue
          
          'Define the parent node
20        lsParentNode = "/" & msRootNode & "/" & sNamespace
          
          'Check to see if the Option already exists.  If it does, we
          'want to modify it.  Otherwise, we want to add it.
30        If mobjXML.isNode(lsParentNode & "/" & sOptionName) Then
40            Call mobjXML.changeNodeValue(lsParentNode & "/" & sOptionName, sOptionValue)
50        Else
60            Call mobjXML.addChildNode(lsParentNode, sOptionName, sOptionValue)
70        End If
          
          
80    Exit Sub
errorhandler:
          
90        msCodeLocation = "clsConfig.setOption"
100       Call RecordError
110       Err.Raise Err.Number
End Sub
'Gets an encrypted option from the config file
'Usage: lsValue = getOption(sNamespace,sOptionName,sPassword)
'Where: sNameSpace is the namespace for the option
'       sOptionName is the name of the option
'       lsValue is the corresponding value of the option
'       sPassword is the password for the hash function
Public Function getEncryptedOption(ByVal sNamespace As String, _
                                ByVal sOptionName As String, _
                                ByVal sPassword As String) As Variant
                                      
10        On Error GoTo errorhandler
              
          Dim objEncrypt As clsEncryption
          Dim sEncryptedValue As String
          
          'Make sure the key starts with the encryption prefix
20        If Not isEncryptedOption(sOptionName) Then
30            sOptionName = msEncryptionPrefix & sOptionName
40        End If
          
          'Get the encrytped value of the option
50        sEncryptedValue = getOption(sNamespace, sOptionName)
          
          'Get a copy of the encryption object
60        Set objEncrypt = New clsEncryption
          
          'Return the decrypted value of the option
70        getEncryptedOption = objEncrypt.getDecryptedValue(sEncryptedValue, sPassword)
          
          'Cleanup
80        Set objEncrypt = Nothing
          
90    Exit Function
errorhandler:
          'Cleanup
100       Set objEncrypt = Nothing
          
110       msCodeLocation = "clsConfig.getEncryptedOption"
120       Call RecordError
130       Err.Raise Err.Number

End Function
'Sets an encrypted option to the config file
'Usage: lsValue = getOption(sNamespace,sOptionName)
'Where: sNameSpace is the namespace for the option
'       sOptionName is the name of the option
'       sOptionValue is the corresponding value of the option
'       sPassword is the password for the hash function
Public Sub setEncryptedOption(ByVal sNamespace As String, _
                                ByVal sOptionName As String, _
                                ByVal sOptionValue As String, _
                                ByVal sPassword As String)
                                      
10        On Error GoTo errorhandler
          
          Dim objEncrypt As clsEncryption
          Dim sEncryptedValue As String
          
          'Make sure the key starts with the encryption prefix
20        If Not isEncryptedOption(sOptionName) Then
30            sOptionName = msEncryptionPrefix & sOptionName
40        End If
          
          'Get a copy of the encryption object
50        Set objEncrypt = New clsEncryption
          
          'Encrypt the value
60        sEncryptedValue = objEncrypt.getEncryptedValue(sOptionValue, sPassword)
          
          'Cleanup
70        Set objEncrypt = Nothing
          
          'Encrypted options are prefaced with the word "encrypted"
          'Add this to the option name and store the key
80        Call setOption(sNamespace, sOptionName, sEncryptedValue)

90    Exit Sub
errorhandler:
          'Cleanup
100       Set objEncrypt = Nothing
          
110       msCodeLocation = "clsConfig.setEncryptedOption"
120       Call RecordError
130       Err.Raise Err.Number
End Sub
'Set default values and create the XML object
Private Sub Class_Initialize()
10        On Error GoTo errorhandler
          
          'Set default values
20        Me.RootNode = "Config"
          'By default, look for a file called config.xml is the apps dir
30        Me.FileName = App.Path & "\config.xml" 'the config file location
40        Me.EncryptionPrefix = "Encrypt"
          
          'Create the XML object
50        Set mobjXML = New clsXML
60    Exit Sub
errorhandler:
70        msCodeLocation = "clsConfig.setEncryptedOption"
80        Call RecordError
90        Err.Raise Err.Number
End Sub
'Gets the filename
Public Property Get FileName() As String
10        On Error GoTo errorhandler
20        FileName = msConfigFile
30    Exit Property
errorhandler:
40        msCodeLocation = "clsConfig.getFileName"
50        Call RecordError
60        Err.Raise Err.Number
End Property
'Sets the filename
Public Property Let FileName(ByVal sNewFileName As String)
10        On Error GoTo errorhandler
20        msConfigFile = sNewFileName
30    Exit Property
errorhandler:
40        msCodeLocation = "clsConfig.LetFilename"
50        Call RecordError
60        Err.Raise Err.Number
End Property
'Loads the XML document residing in Me.FileName into the mobjXML object
Public Sub load()
10        On Error GoTo errorhandler
20        mobjXML.load (Me.FileName)
30    Exit Sub
errorhandler:
40        msCodeLocation = "clsConfig.setEncryptedOption"
50        Call RecordError
60        Err.Raise Err.Number
End Sub
'Saves the XML document
Public Sub Save()
10        On Error GoTo errorhandler
20        Call mobjXML.Save(Me.FileName)
30    Exit Sub
errorhandler:
40        msCodeLocation = "clsConfig.Save"
50        Call RecordError
60        Err.Raise Err.Number
End Sub
'Gets the root node
Public Property Get RootNode() As String
10        On Error GoTo errorhandler
20        RootNode = msRootNode
30    Exit Property
errorhandler:
40        msCodeLocation = "clsConfig.GetRootNode"
50        Call RecordError
60        Err.Raise Err.Number
End Property
'Sets the root node
Public Property Let RootNode(ByVal sNewRoot As String)
10        On Error GoTo errorhandler
20        msRootNode = sNewRoot
30    Exit Property
errorhandler:
40        msCodeLocation = "clsConfig.LetRootNode"
50        Call RecordError
60        Err.Raise Err.Number
End Property
'Gets the encryption prefix
Public Property Get EncryptionPrefix() As Variant
10        On Error GoTo errorhandler
20        EncryptionPrefix = msEncryptionPrefix
30    Exit Property
errorhandler:
40        msCodeLocation = "clsConfig.GetEncryptionPrefix"
50        Call RecordError
60        Err.Raise Err.Number
End Property
'Sets the encryption prefix
Public Property Let EncryptionPrefix(ByVal vNewValue As Variant)
10        On Error GoTo errorhandler
20        msEncryptionPrefix = vNewValue
30    Exit Property
errorhandler:
40        msCodeLocation = "clsConfig.LetEncryptionPrefix"
50        Call RecordError
60        Err.Raise Err.Number
End Property
'Terminates the class
Private Sub Class_Terminate()
10        On Error GoTo errorhandler
20        Set mobjXML = Nothing
30    Exit Sub
errorhandler:
40        msCodeLocation = "clsConfig.Class_Terminate"
50        Call RecordError
60        Err.Raise Err.Number
End Sub


