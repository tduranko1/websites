VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   1215
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3255
   LinkTopic       =   "Form1"
   ScaleHeight     =   1215
   ScaleWidth      =   3255
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox User 
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Text            =   "test'me@stefano.ods.org"
      Top             =   120
      Width           =   3015
   End
   Begin VB.CommandButton Add 
      Caption         =   "Add User"
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1455
   End
   Begin VB.CommandButton Go 
      Caption         =   "Forgot Pwd"
      Height          =   495
      Left            =   1680
      TabIndex        =   0
      Top             =   600
      Width           =   1455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Add_Click()

    On Error GoTo ErrHandler
    
    Dim loAware As Object
    Dim Result As Variant
    
    Set loAware = CreateObject("eTemGlobalReg.clsRegistration")
    
    Result = loAware.Request(User.Text, "", "", "", "", "", "password", 16)
    
    MsgBox "Request = '" & Result & "'" & vbCrLf & vbCrLf & _
    "-5 � User Has Access (already been approved for access)" & vbCrLf & _
    "-4 � User Has Requested Access" & vbCrLf & _
    "-3 � Invalid App ID" & vbCrLf & _
    "-2 � Alias already in use (Alias collision)" & vbCrLf & _
    "-1 - Unknown Error (any error handled by the Error Handler in this function)" & vbCrLf & _
    "1 � Successfully Requested access"
    
    Result = loAware.CreateUser(User.Text, "password")

    MsgBox "Create = '" & Result & "'" & vbCrLf & vbCrLf & _
    "0 - Unknown Error (any error handled by the Error Handler in this function)" & vbCrLf & _
    "1 - User Added Successfully" & vbCrLf & _
    "2 - User Already existed in AD"

    Result = loAware.GetUserIDFromEmail(User.Text)
    
    MsgBox "UserID = '" & Result & "'"
    
    Result = loAware.Approve(16, Result, "", "APDApprove@ppg.com", 1)

    MsgBox "Approve = '" & Result & "'" & vbCrLf & vbCrLf & _
    "0 - Success" & vbCrLf & _
    "-1 - Unknown Error (any error handled by the Error Handler in this function)"
    
ErrHandler:
    
    Set loAware = Nothing
    
    If Err.Number <> 0 Then
        MsgBox "Error = '" & Err.Description & "'"
    End If

End Sub

Private Sub Go_Click()

    On Error GoTo ErrHandler
    
    Dim loAware As Object
    Dim Result As Variant
    
    Set loAware = CreateObject("eTemGlobalReg.clsRegistration")
    
    Result = loAware.ResetPassword(User.Text, "", "", True)
    
    MsgBox "Return = '" & Result & "'" & vbCrLf & vbCrLf & _
    "0 - Unknown Error (any error handled by the Error Handler in this function)" & vbCrLf & _
    "1 - User Does not exist in AD" & vbCrLf & _
    "2 - Password Successfully reset" & vbCrLf & _
    "3 - Wrong answers to the forgot password questions" & vbCrLf & _
    "4 - New password value violates the eTemplate policy"
    
ErrHandler:
    
    Set loAware = Nothing
    
    If Err.Number <> 0 Then
        MsgBox "Error = '" & Err.Description & "'"
    End If

End Sub
