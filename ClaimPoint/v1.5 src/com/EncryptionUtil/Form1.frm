VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   6450
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9390
   LinkTopic       =   "Form1"
   ScaleHeight     =   6450
   ScaleWidth      =   9390
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtBack 
      Height          =   1605
      Left            =   960
      MultiLine       =   -1  'True
      TabIndex        =   7
      Top             =   4680
      Width           =   8295
   End
   Begin VB.TextBox txtTo 
      Height          =   1515
      Left            =   960
      MultiLine       =   -1  'True
      TabIndex        =   6
      Top             =   2640
      Width           =   8295
   End
   Begin VB.CommandButton Decrypt 
      Caption         =   "Decrypt"
      Height          =   375
      Left            =   4320
      TabIndex        =   5
      Top             =   4200
      Width           =   1575
   End
   Begin VB.CommandButton Encrypt 
      Caption         =   "Encrypt"
      Height          =   375
      Left            =   4320
      TabIndex        =   4
      Top             =   2160
      Width           =   1575
   End
   Begin VB.TextBox txtFrom 
      Height          =   1485
      Left            =   960
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   600
      Width           =   8295
   End
   Begin VB.TextBox txtPassword 
      Height          =   285
      Left            =   960
      TabIndex        =   0
      Text            =   "LYNXAPD"
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label Label4 
      Caption         =   "And Back"
      Height          =   375
      Left            =   0
      TabIndex        =   9
      Top             =   4680
      Width           =   855
   End
   Begin VB.Label Label3 
      Caption         =   "To"
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   2640
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "From"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "Password"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Function getEncryptedValue(ByVal sData As String, ByVal sKey As String) As Variant
    getEncryptedValue = fnCrypt(sData, sKey)
End Function

Private Function getDecryptedValue(sData As String, sKey As String) As Variant
    getDecryptedValue = fnDecrypt(sData, sKey)
End Function

Private Function fnCrypt(texti, salasana) As String

    Dim t
    Dim sana
    Dim x1
    Dim g
    Dim tt
    Dim crypted
    
    On Error Resume Next
    
    For t = 1 To Len(salasana)
    sana = Asc(Mid(salasana, t, 1))
    x1 = x1 + sana
    Next
    
    x1 = Int((x1 * 0.1) / 6)
    salasana = x1
    g = 0
    
    For tt = 1 To Len(texti)
    sana = Asc(Mid(texti, tt, 1))
    g = g + 1
    
    If g = 6 Then g = 0
    x1 = 0
    
    If g = 0 Then x1 = sana - (salasana - 2)
    
    If g = 1 Then x1 = sana + (salasana - 5)
    
    If g = 2 Then x1 = sana - (salasana - 4)
    
    If g = 3 Then x1 = sana + (salasana - 2)
    
    If g = 4 Then x1 = sana - (salasana - 3)
    
    If g = 5 Then x1 = sana + (salasana - 5)
           x1 = x1 + g
           crypted = crypted & Format$(x1, "000")
    Next
    
    
    fnCrypt = crypted
End Function


Private Function fnDecrypt(texti, salasana) As String
        
    Dim t
    Dim sana
    Dim x1
    Dim g
    Dim tt
    Dim decrypted
    
    On Error Resume Next

    For t = 1 To Len(salasana)
           sana = Asc(Mid(salasana, t, 1))
           x1 = x1 + sana
    Next

    x1 = Int((x1 * 0.1) / 6)
    salasana = x1
    g = 0
    
    For tt = 1 To Len(texti) Step 3
    sana = Mid(texti, tt, 3)
    g = g + 1
    
    If g = 6 Then g = 0
    x1 = 0
    
    If g = 0 Then x1 = sana + (salasana - 2)
    
    If g = 1 Then x1 = sana - (salasana - 5)
    
    If g = 2 Then x1 = sana + (salasana - 4)
    
    If g = 3 Then x1 = sana - (salasana - 2)
    
    If g = 4 Then x1 = sana + (salasana - 3)
    
    If g = 5 Then x1 = sana - (salasana - 5)
           x1 = x1 - g
           decrypted = decrypted & Chr(x1)
    Next
    

    fnDecrypt = decrypted
End Function

Private Sub Encrypt_Click()
    txtTo.Text = getEncryptedValue(txtFrom.Text, txtPassword.Text)
End Sub

Private Sub Decrypt_Click()
    txtBack.Text = getDecryptedValue(txtTo.Text, txtPassword.Text)
End Sub


