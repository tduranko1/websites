<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="Vehicle">

	<tr><td colspan="5"><p class="mini">&#160;</p></td></tr>

	<tr>
		<td nowrap="" >
			<p class="bodyBlue">Appraiser Name:</p>
		</td>
		<td nowrap="" class="body" >
			<p/><xsl:value-of select="Appraiser/@Name"/>
		</td>
		<td nowrap="">
			<p class="bodyBlue"/>
		</td>
		<td nowrap="" >
			<p class="bodyBlue">Phone:</p>
		</td>
		<td class="body" nowrap="" height="23" >
			<p/>
			(<xsl:value-of select="Appraiser/@PhoneAreaCode"/>) <xsl:value-of select="concat(' ', Appraiser/@PhoneExchangeNumber)"/> - <xsl:value-of select="Appraiser/@PhoneUnitNumber"/>
			<xsl:if test="string-length(Appraiser/@PhoneExtensionNumber) &gt; 0">
				<xsl:value-of select="concat(' x', Appraiser/@PhoneExtensionNumber)"/>
			</xsl:if>
		</td>
	</tr>
	<tr>
		<td nowrap="" >
			<p class="bodyBlue">Address:</p>
		</td>
		<td nowrap="" class="body" >
			<p/><xsl:value-of select="Appraiser/@Address1"/>
		</td>
		<td nowrap="">
			<p class="bodyBlue"/>
		</td>
		<td nowrap="" >
			<p class="bodyBlue"/>
		</td>
		<td class="body" nowrap="" height="23" >
			<p/>
			
		</td>
	</tr>
	<tr>
		<td nowrap="" height="19">
			&#160;
		</td>
		<td class="body" nowrap="" height="19" >
			<p/><xsl:value-of select="Appraiser/@Address2"/>
		</td>
		<td nowrap="">
			<p class="bodyBlue"/>
		</td>
		<td nowrap="" height="19" >
		</td>
		<td nowrap="" class="body" height="19" >
		</td>
	</tr>
	<tr>
		<td nowrap="" height="19">
			&#160;
		</td>
		<td class="body" nowrap="" height="19" >
			<p/>
			<xsl:value-of select="concat(Appraiser/@AddressCity, ', ')"/><xsl:value-of select="concat(Appraiser/@AddressState, '  ')"/><xsl:value-of select="Appraiser/@AddressZip"/>
		</td>
		<td nowrap="">
			<p class="bodyBlue"/>
		</td>
		<td nowrap=""  class="body" height="19">
		</td>
		<td nowrap="" class="body" height="19" >
		</td>
	</tr>

</xsl:template>

</xsl:stylesheet>