<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   xmlns:user="http://mycompany.com/mynamespace">
  <xsl:output method="text" omit-xml-declaration="yes"/>
  <msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[
   function formatCurrencyString(sVal){
      strRet = "";
      sVal = String(sVal);
      if (isNaN(sVal) == false && sVal != ""){
         if (sVal.indexOf(".") == -1) {
            strRet = sVal + ".00";
         } else {
            var strParts = sVal.split(".");
            strRet = strParts[0];
            if (strParts.length > 1) {
               var strCents = strParts[1];
               if (strCents.length < 4){
                  strCents += charFill("0", 4 - strCents.length);
               }
               strCents = Math.round(parseFloat(strCents)/100) + "";
               strCents = strCents + charFill("0", 2 - strCents.length);
               strRet += "." + strCents;
            }
         }
      }
      return strRet;
   }

   function charFill(sChar, iLen){
      var sRet = "";
      if (isNaN(iLen)) iLen = 0;
      
      for (var i = iLen; i > 0; i--)
         sRet += sChar;
      
      return sRet;
   }
  ]]>
  </msxsl:script>

  <xsl:template match="/Root">
   laborData = { 
      match : "<xsl:value-of select="concat('State: ', @MatchStateCode, ', City: ', @MatchCity, ', County: ', @MatchCounty)"/>",
      BodyRate:"<xsl:value-of select="user:formatCurrencyString(string(LaborRate/@BodyRateMin))"/>",
      FrameRate:"<xsl:value-of select="user:formatCurrencyString(string(LaborRate/@FrameRateMin))"/>",
      MaterialRate:"<xsl:value-of select="user:formatCurrencyString(string(LaborRate/@MaterialRateMin))"/>",
      MechRate:"<xsl:value-of select="user:formatCurrencyString(string(LaborRate/@MechRateMin))"/>",
      RefinishRate:"<xsl:value-of select="user:formatCurrencyString(string(LaborRate/@RefinishRateMin))"/>",
      LaborTax:"<xsl:value-of select="user:formatCurrencyString(string(LaborRate/@LaborTax))"/>",
      PartsTax:"<xsl:value-of select="user:formatCurrencyString(string(LaborRate/@PartsTax))"/>",
      MaterialsTax:"<xsl:value-of select="user:formatCurrencyString(string(LaborRate/@MaterialsTax))"/>"};
  </xsl:template>
</xsl:stylesheet>