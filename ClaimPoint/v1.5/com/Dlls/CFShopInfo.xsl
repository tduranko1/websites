<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
         

  function ConvertHour(time)
  {
    var lsTime = ldTrim(time);
    var lsHH;
    var lsMM;
    var lsDelimeter = ":";

    if (lsTime == null || lsTime == "")
     return "";
    else
    {
      if (lsTime.length == 3)
      {
        lsHH = lsTime.substr(0,1);
        lsMM = lsTime.substr(1,2);
      }
      else
      {
        lsHH = lsTime.substr(0,2);
        lsMM = lsTime.substr(2,2);
      }
      
      if (lsHH > 12)
        lsHH = lsHH - 12;
      else
        lsHH = lsHH - 0;
    }

    var strDate = lsHH + lsDelimeter + lsMM;
    return strDate;
  }

  function ldTrim(val)
  { 
    var strText = val
    while (strText.substring(0,1) == ' ') // this will get rid of leading spaces
      strText = strText.substring(1, strText.length);
    
    while (strText.substring(strText.length-1,strText.length) == ' ') // this will get rid of trailing spaces
      strText = strText.substring(0, strText.length-1);
    
    return strText;
  }

  ]]>
</msxsl:script>

<xsl:template match="/Root">

  <xsl:comment>v1.4.4.0</xsl:comment>

  <!-- Get Shop Info -->
  <span style="background-color:#DDDDDD; width:660px; padding:2px; margin-bottom:6px; font-size:13px; font-weight:bold;">Business Information:</span>
  <xsl:apply-templates select="BusinessInfo"/>
  <br/>

  <span style="background-color:#DDDDDD; width:660px; padding:2px; margin-bottom:6px; font-size:13px; font-weight:bold;">Shop Information:</span>
  <xsl:apply-templates select="ShopInfo"/>
  <br/><br/>

  <span style="background-color:#DDDDDD; width:660px; padding:2px; margin-bottom:6px; font-size:13px; font-weight:bold;">Primary Shop Contact:</span>
  <xsl:apply-templates select="BusinessPersonnel"/>
  <br/><br/>

  <span style="background-color:#DDDDDD; width:660px; padding:2px; margin-bottom:6px; font-size:13px; font-weight:bold;">Shop Contact:</span>
  <xsl:apply-templates select="ShopPersonnel"/>
  <br/><br/>

  <span style="background-color:#DDDDDD; width:660px; padding:2px; margin-bottom:6px; font-size:13px; font-weight:bold;">Pricing:</span>
  <xsl:apply-templates select="Pricing"/>
  <br/>

  <input type="hidden" id="txtBusinessInfoID" name="BusinessInfoID">
    <xsl:attribute name="value"><xsl:value-of select="@BusinessInfoID"/></xsl:attribute>
  </input>
  
  <input type="hidden" id="txtShopID" name="ShopID">
    <xsl:attribute name="value"><xsl:value-of select="@ShopID"/></xsl:attribute>
  </input>
  
  <input type="hidden" id="txtFinalSubmitFlag" name="FinalSubmitFlag">
    <xsl:attribute name="value"><xsl:value-of select="ShopInfo/@FinalSubmitFlag"/></xsl:attribute>
  </input>

</xsl:template>

<xsl:template match="BusinessInfo"> <!-- Business Information -->
  <table border="0" cellspacing="0" cellpadding="1">
    <tr>
      <td class="bodyBlue" nowrap="on"><em>CertifiedFirst</em> Network ID:</td>
      <td>
        <input type="text" id="txtCertifiedFirstID" name="CertifiedFirstID" size="9" readonly="" style="font-family: monospace; font-style:italic; text-align:center; border:1px solid #111111">
          <xsl:attribute name="value"><xsl:value-of select="/Root/ShopInfo/@CertifiedFirstID"/></xsl:attribute>
        </input>
      </td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td class="bodyBlue" nowrap="on">Federal Tax ID: *</td>
      <td>
        <input type="text" id="txtFedTaxID" name="FedTaxID" size="10" class="inputFld" maxlength="9">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="@FedTaxID"/></xsl:attribute>
        </input>
      </td>
      <td>
          <img src="images/spacer.gif" width="20px"/>
      </td>
      <td class="bodyBlue" nowrap="on">Business Type: *</td>
      <td>
        <select id="selBusinessTypeCD" name="BusinessTypeCD" class="inputFld">
         <option></option>
           <xsl:for-each select="/Root/Reference[@ListName='BusinessType']">
             <xsl:call-template name="BuildSelectOptions">
             <xsl:with-param name="current" select="/Root/BusinessInfo/@BusinessTypeCD"/>
           </xsl:call-template>
          </xsl:for-each>
        </select>
      </td>
    </tr>
  </table>
</xsl:template>

<xsl:template match="ShopInfo"> <!-- Shop Information -->
  <TABLE border="0" cellSpacing="0" cellPadding="0">  <!-- master table -->
    <TR>
      <TD valign="top"><!-- column 1 : shop info -->

        <TABLE border="0" cellspacing="0" cellpadding="1">
          <TR>
            <TD class="bodyBlue">Name: *</TD>
            <TD nowrap="nowrap">
              <input type="text" id="txtName" name="Name" size="35" class="inputFld" maxlength="50" onBlur="ldTrim(this); updTOAShop(this);">
                <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
              </input>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue" nowrap="nowrap">Address 1: *</TD>
            <TD>
              <input type="text" id="txtAddress1" name="Address1" size="35" class="inputFld" maxlength="50" onBlur="ldTrim(this)">
                <xsl:attribute name="value"><xsl:value-of select="@Address1"/></xsl:attribute>
              </input>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">Address 2:</TD>
            <TD>
              <input type="text" id="txtAddress2" name="Address2" size="35" class="inputFld" maxlength="50" onBlur="ldTrim(this)">
                <xsl:attribute name="value"><xsl:value-of select="@Address2"/></xsl:attribute>
              </input> 
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">City: *</TD>
            <TD>
              <input type="text" id="txtAddressCity" name="AddressCity" size="20" class="inputFld" maxlength="30" onBlur="ldTrim(this)">
                <xsl:attribute name="value"><xsl:value-of select="@AddressCity"/></xsl:attribute>
              </input>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">State: *</TD>
            <TD nowrap="nowrap">
              <select id="selState" name="AddressState" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='State']">
                <xsl:call-template name="BuildSelectOptions">
                <xsl:with-param name="current"><xsl:value-of select="/Root/ShopInfo/@AddressState"/></xsl:with-param>
                </xsl:call-template>
                </xsl:for-each>
              </select>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">Zip: *</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtAddressZip" name="AddressZip" size="6" class="inputFld" onkeypress="return numbersOnly(this,event,0)" maxlength="5">
                <xsl:attribute name="value"><xsl:value-of select="@AddressZip"/></xsl:attribute>
              </input>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">County:</TD>
            <TD>
              <input type="text" id="txtAddressCounty" name="AddressCounty" size="20" class="inputFld" maxlength="30" onBlur="ldTrim(this)">
                <xsl:attribute name="value"><xsl:value-of select="@AddressCounty"/></xsl:attribute>
              </input>
            </TD>
          </TR>
        </TABLE>

      </TD><!-- end of column 1 -->
      <TD noWrap="nowrap"><img src="images/spacer.gif" width="14px"/></TD> <!-- spacer column -->
      <TD width="100%" valign="top"> <!-- column 2 Shop Program info -->

        <TABLE border="0" cellspacing="0" cellpadding="1">
          <TR>
            <TD class="bodyBlue">Phone: *</TD>
            <TD class="bodyBlue" noWrap="nowrap">
              <input name="PhoneAreaCode" id="txtPhoneAreaCode" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
                <xsl:attribute name="value"><xsl:value-of select="@PhoneAreaCode"/></xsl:attribute>
              </input>
              <input name="PhoneExchangeNumber" id="txtPhoneExchangeNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
                <xsl:attribute name="value"><xsl:value-of select="@PhoneExchangeNumber"/></xsl:attribute>
              </input>
              <input name="PhoneUnitNumber" id="txtPhoneUnitNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" onBlur="ldTrim(this)" size="4" maxlength="4">
                <xsl:attribute name="value"><xsl:value-of select="@PhoneUnitNumber"/></xsl:attribute>
              </input>
              ext.
              <input name="PhoneExtensionNumber" id="txtPhoneExtensionNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 5, event);" onBlur="ldTrim(this)" value="" size="5" maxlength="5">
                <xsl:attribute name="value"><xsl:value-of select="@PhoneExtensionNumber"/></xsl:attribute>
              </input>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">Fax: *</TD>
            <TD class="bodyBlue" noWrap="nowrap">
              <input name="FaxAreaCode" id="txtFaxAreaCode" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
                <xsl:attribute name="value"><xsl:value-of select="@FaxAreaCode"/></xsl:attribute>
              </input>
              <input name="FaxExchangeNumber" id="txtFaxExchangeNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
                <xsl:attribute name="value"><xsl:value-of select="@FaxExchangeNumber"/></xsl:attribute>
              </input>
              <input name="FaxUnitNumber" id="txtFaxUnitNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" onBlur="ldTrim(this)" size="4" maxlength="4">
                <xsl:attribute name="value"><xsl:value-of select="@FaxUnitNumber"/></xsl:attribute>
              </input>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue">Email: *</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtEmailAddress" name="EmailAddress" size="35" class="inputFld" onbeforedeactivate="ldTrim(this); checkEMail(this)" maxlength="50">
                <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
              </input>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue" noWrap="nowrap">Web Site:&#160;&#160;&#160;&#160;</TD>
            <TD noWrap="nowrap">
              <input type="text" id="txtWebSiteAddress" name="WebSiteAddress" size="35" class="inputFld" maxlength="256" onBlur="ldTrim(this)">
                <xsl:attribute name="value"><xsl:value-of select="@WebSiteAddress"/></xsl:attribute>
              </input>
            </TD>
          </TR>
        </TABLE>

        <TABLE border="0" cellspacing="0" cellpadding="1">
          <TR>
            <TD class="bodyBlue" nowrap="nowrap">Repair Facility Type: *</TD>
            <TD>
              <select id="selRepairFacilityType" name="RepairFacilityTypeCD" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='RepairFacilityType']">
                <xsl:call-template name="BuildSelectOptions">
                <xsl:with-param name="current" select="/Root/ShopInfo/@RepairFacilityTypeCD"/>
                </xsl:call-template>
                </xsl:for-each>
              </select>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue" nowrap="nowrap">Lifetime Workmanship Warranty: *</TD>
            <TD>
              <input type="radio" name="rdWorkmanshipWarranty" id="txtWorkmanshipWarrantyYes" value="99" onclick="document.frmShop.txtWarrantyPeriodWorkmanshipCD.value=this.value">
                <xsl:if test="/Root/ShopInfo/@WarrantyPeriodWorkmanshipCD = '99'">
                  <xsl:attribute name="checked"/>
                </xsl:if>
              </input> Yes

              <input type="radio" name="rdWorkmanshipWarranty" id="txtWorkmanshipWarrantyNo" value="3" onclick="document.frmShop.txtWarrantyPeriodWorkmanshipCD.value=this.value">
                <xsl:if test="/Root/ShopInfo/@WarrantyPeriodWorkmanshipCD != '99'">
                  <xsl:attribute name="checked"/>
                </xsl:if>
              </input> No

              <input type="hidden" name="WarrantyPeriodWorkmanshipCD" id="txtWarrantyPeriodWorkmanshipCD">
                <xsl:if test="@WarrantyPeriodWorkmanshipCD = '99'">
                  <xsl:attribute name="value"><xsl:value-of select="@WarrantyPeriodWorkmanshipCD"/></xsl:attribute>
                </xsl:if>
                <xsl:if test="@WarrantyPeriodWorkmanshipCD != '99'">
                  <xsl:attribute name="value">3</xsl:attribute>
                </xsl:if>
              </input>
            </TD>
          </TR>
          <TR>
            <TD class="bodyBlue" nowrap="nowrap">Lifetime Refinish Warranty: *</TD>
            <TD>
              <input type="radio" name="rdRefinishWarranty" id="txtRefinishWarrantyYes" value="99" onclick="document.frmShop.txtWarrantyPeriodRefinishCD.value=this.value">
                <xsl:if test="/Root/ShopInfo/@WarrantyPeriodRefinishCD = '99'">
                  <xsl:attribute name="checked"/>
                </xsl:if>
              </input> Yes
              
              <input type="radio" name="rdRefinishWarranty" id="txtRefinishWarrantyNo" value="3" onclick="document.frmShop.txtWarrantyPeriodRefinishCD.value=this.value">
                <xsl:if test="/Root/ShopInfo/@WarrantyPeriodRefinishCD != '99'">
                  <xsl:attribute name="checked"/>
                </xsl:if>
              </input> No

              <input type="hidden" name="WarrantyPeriodRefinishCD" id="txtWarrantyPeriodRefinishCD">
                <xsl:if test="@WarrantyPeriodRefinishCD = '99'">
                  <xsl:attribute name="value"><xsl:value-of select="@WarrantyPeriodRefinishCD"/></xsl:attribute>
                </xsl:if>
                <xsl:if test="@WarrantyPeriodRefinishCD != '99'">
                  <xsl:attribute name="value">3</xsl:attribute>
                </xsl:if>
              </input>
             </TD>
          </TR>
          <TR>
            <TD class="bodyBlue" nowrap="nowrap">Preferred Estimate Package: * </TD>
            <TD>
              <select id="selPreferredEstimatePackageID" name="PreferredEstimatePackageID" class="inputFld">
                <option></option>
                <xsl:for-each select="/Root/Reference[@ListName='EstimatePackage' and (@ReferenceID='1' or @ReferenceID='2' or @ReferenceID='4')]">
                <xsl:call-template name="BuildSelectOptions">
                <xsl:with-param name="current" select="/Root/ShopInfo/@PreferredEstimatePackageID"/>
                </xsl:call-template>
                </xsl:for-each>
              </select>
            </TD>
          </TR>
        </TABLE>

      </TD> <!-- end of column 2 -->
    </TR>
    <TR>
      <TD valign="top">
        <TABLE border="0" cellspacing="0" cellpadding="1">
          <TR>
            <TD noWrap="nowrap" valign="top" class="bodyBlue">How to Get There: *&#160;</TD>
          </TR>
          <TR>
            <TD valign="top">
              <span style="font-size:13px">Please enter localized directions to your facilities such as:
              <em>"We are located southbound on Route 9, two miles south of the Freehold mall and across from the Pinebrook Shopping Center."</em></span><br/>
              &#160;<span id="commFldMsg" style="color:#000000; font-weight:bold; font-size:10pt">(max. 250 characters)</span><br/>
            </TD>
          </TR>
          <TR>
            <TD valign="top">
              <textarea cols="" rows="" name="DrivingDirections" id="txtDrivingDirections" style="width: 100%; height: 120px;" onKeyDown="CheckInputLength(this, 250)" onbeforedeactivate="ldTrim(this); ChkMaxLength(this, 250)"><xsl:value-of select="@DrivingDirections"/></textarea>
            </TD>
          </TR>
        </TABLE>
      </TD><!-- end of column 1 -->

      <TD noWrap="nowrap"><img src="images/spacer.gif" width="14px"/></TD> <!-- spacer column -->

      <TD valign="top"> <!-- column 2 Shop Program info -->
      <xsl:apply-templates select="/Root/ShopHours"/>

      </TD>
    </TR>
  </TABLE>

</xsl:template>

<xsl:template match="/Root/ShopHours">

  <table border="0" cellspacing="0" cellpadding="1">
    <TR>
      <TD colspan="3" noWrap="nowrap" valign="top" class="bodyBlue">
      Hours of Operation:
      <a href="javascript:poplateHours()" title="Click to Populate with Default Hours"><img border="0" src="images/clock_sm.gif" WIDTH="19" HEIGHT="20"/></a>
      </TD>
    </TR>

    <TR>
      <td width="40"></td>
      <TD class="bodyBlue">Mon: *</TD>
      <TD vAlign="top" noWrap="nowrap">
        <input type="text" id="txtMondayStartTime" name="OperatingMondayStartTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingMondayStartTime))"/></xsl:attribute>
        </input> AM
        -
        <input type="text" id="txtMondayEndTime" name="OperatingMondayEndTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
           <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingMondayEndTime))"/></xsl:attribute>
        </input> PM
      </TD>
    </TR>
    <TR>
      <TD></TD>
      <TD class="bodyBlue">Tue: *</TD>
      <TD vAlign="top" noWrap="nowrap">
        <input type="text" id="txtTuesdayStartTime" name="OperatingTuesdayStartTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingTuesdayStartTime))"/></xsl:attribute>
        </input> AM
        -
        <input type="text" id="txtTuesdayEndTime" name="OperatingTuesdayEndTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingTuesdayEndTime))"/></xsl:attribute>
        </input> PM
      </TD>
    </TR>
    <TR>
      <TD></TD>
      <TD class="bodyBlue">Wed: *</TD>
      <TD vAlign="top" noWrap="nowrap">
        <input type="text" id="txtWednesdayStartTime" name="OperatingWednesdayStartTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingWednesdayStartTime))"/></xsl:attribute>
        </input> AM
        -
        <input type="text" id="txtWednesdayEndTime" name="OperatingWednesdayEndTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingWednesdayEndTime))"/></xsl:attribute>
        </input> PM
      </TD>
    </TR>
    <TR>
      <TD></TD>
      <TD class="bodyBlue">Thu: *</TD>
      <TD vAlign="top" noWrap="nowrap">
        <input type="text" id="txtThursdayStartTime" name="OperatingThursdayStartTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingThursdayStartTime))"/></xsl:attribute>
        </input> AM
        -
        <input type="text" id="txtThursdayEndTime" name="OperatingThursdayEndTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingThursdayEndTime))"/></xsl:attribute>
        </input> PM
      </TD>
    </TR>
    <TR>
      <TD></TD>
      <TD class="bodyBlue">Fri: *</TD>
      <TD vAlign="top" noWrap="nowrap">
        <input type="text" id="txtFridayStartTime" name="OperatingFridayStartTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingFridayStartTime))"/></xsl:attribute>
        </input> AM
        -
        <input type="text" id="txtFridayEndTime" name="OperatingFridayEndTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingFridayEndTime))"/></xsl:attribute>
        </input> PM
      </TD>
    </TR>
    <TR>
      <TD></TD>
      <TD class="bodyBlue">Sat:</TD>
      <TD vAlign="top" noWrap="nowrap">
        <input type="text" id="txtSaturdayStartTime" name="OperatingSaturdayStartTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingSaturdayStartTime))"/></xsl:attribute>
        </input> AM
        -
        <input type="text" id="txtSaturdayEndTime" name="OperatingSaturdayEndTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingSaturdayEndTime))"/></xsl:attribute>
        </input> PM
      </TD>
    </TR>
    <TR>
      <TD></TD>
      <TD class="bodyBlue">Sun:</TD>
      <TD vAlign="top" noWrap="nowrap">
        <input type="text" id="txtSundayStartTime" name="OperatingSundayStartTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingSundayStartTime))"/></xsl:attribute>
        </input> AM
        -
        <input type="text" id="txtSundayEndTime" name="OperatingSundayEndTime" size="3" class="inputFld" onblur="hrsChk(this)" maxlength="5" style="text-align:right;">
           <xsl:attribute name="onkeypress">return numbersOnly(this,event,0,0,1);</xsl:attribute>
          <xsl:attribute name="value"><xsl:value-of select="user:ConvertHour(string(@OperatingSundayEndTime))"/></xsl:attribute>
        </input> PM
      </TD>
    </TR>
  </table>

</xsl:template>


<xsl:template match="BusinessPersonnel"> <!-- Shop Contact One -->
   <xsl:variable name="PersonnelID" select="@PersonnelID"/>
  
    <input type="hidden" id="txtSPPersonnelID" name="SPPersonnelID">
      <xsl:if test="@PersonnelID != ''">
        <xsl:attribute name="value"><xsl:value-of select="@PersonnelID"/></xsl:attribute>
      </xsl:if>
    </input>
    
    <input type="hidden" id="txtSPGenderCD" name="SPGenderCD"></input>
  <table border="0" cellSpacing="0" cellPadding="1">
    <tr>
      <td class="bodyBlue" nowrap="nowrap">Name: *</td>
      <td>
        <input type="text" id="txtSPName" name="SPName" size="35"  class="inputFld" autocomplete="off" maxlength="50" onBlur="ldTrim(this)"> 
          <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
        </input>
      </td>
      <TD noWrap="nowrap"><img src="images/spacer.gif" width="14px"/></TD> <!-- spacer column -->
      <td class="bodyBlue">Role: *</td>

      <td>
        <select id="txtSPPersonnelTypeID" name="SPPersonnelTypeID" class="inputFld">
          <option></option>
          <xsl:for-each select="/Root/Reference[@ListName='PersonnelType']">
          <xsl:call-template name="BuildSelectOptions">
          <xsl:with-param name="current" select="/Root/BusinessPersonnel/@PersonnelTypeID"/>
          </xsl:call-template>
          </xsl:for-each>
        </select>

        <input type="hidden" id="txtSPShopManagerFlag" name="SPShopManagerFlag">
          <xsl:attribute name="value"><xsl:value-of select="@ShopManagerFlag"/></xsl:attribute>
        </input> 
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" nowrap="nowrap">Phone: *</td>
      <td class="bodyBlue" nowrap="nowrap" align="left">
        <input name="SPPhoneAreaCode" id="txtSPPhoneAreaCode" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@PhoneAreaCode"/></xsl:attribute>
        </input>
        <input name="SPPhoneExchangeNumber" id="txtSPPhoneExchangeNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@PhoneExchangeNumber"/></xsl:attribute>
        </input>
        <input name="SPPhoneUnitNumber" id="txtSPPhoneUnitNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" onBlur="ldTrim(this)" size="4" maxlength="4">
          <xsl:attribute name="value"><xsl:value-of select="@PhoneUnitNumber"/></xsl:attribute>
        </input>
        ext.
        <input name="SPPhoneExtensionNumber" id="txtSPPhoneExtensionNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 5, event);" onBlur="ldTrim(this)" value="" size="5" maxlength="5">
          <xsl:attribute name="value"><xsl:value-of select="@PhoneExtensionNumber"/></xsl:attribute>
        </input>
      </td>
      <td></td>
      <td class="bodyBlue">Fax:</td>
      <td class="bodyBlue" nowrap="nowrap">
        <input name="SPFaxAreaCode" id="txtSPFaxAreaCode" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@FaxAreaCode"/></xsl:attribute>
        </input>
        <input name="SPFaxExchangeNumber" id="txtSPFaxExchangeNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@FaxExchangeNumber"/></xsl:attribute>
        </input>
        <input name="SPFaxUnitNumber" id="txtSPFaxUnitNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" onBlur="ldTrim(this)" size="4" maxlength="4">
          <xsl:attribute name="value"><xsl:value-of select="@FaxUnitNumber"/></xsl:attribute>
        </input>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">Cell:</td>
      <td class="bodyBlue">
        <input name="SPCellAreaCode" id="txtSPCellAreaCode" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@CellAreaCode"/></xsl:attribute>
        </input>
        <input name="SPCellExchangeNumber" id="txtSPCellExchangeNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@CellExchangeNumber"/></xsl:attribute>
        </input>
        <input name="SPCellUnitNumber" id="txtSPCellUnitNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" onBlur="ldTrim(this)" size="4" maxlength="4">
          <xsl:attribute name="value"><xsl:value-of select="@CellUnitNumber"/></xsl:attribute>
        </input>
      </td>
      <td></td>
      <td class="bodyBlue">Email:&#160;</td>
      <td>
        <input id="txtSPEmailAddress" name="SPEmailAddress" size="35" class="inputFld" onbeforedeactivate="ldTrim(this); checkEMail(this)" maxlength="50">
          <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
        </input>
      </td>
    </tr>
  </table>

</xsl:template> 

<xsl:template match="ShopPersonnel"> <!-- Shop Contact Two -->
   <xsl:variable name="PersonnelID" select="@PersonnelID"/>
  
    <input type="hidden" id="txtSLPPersonnelID" name="SLPPersonnelID">
      <xsl:if test="@PersonnelID != ''">
        <xsl:attribute name="value"><xsl:value-of select="@PersonnelID"/></xsl:attribute>
      </xsl:if>
    </input>
    
    <input type="hidden" id="txtSLPGenderCD" name="SLPGenderCD"></input>
  <table border="0" cellSpacing="0" cellPadding="1">
    <tr>
      <td class="bodyBlue" nowrap="nowrap">Name: *</td>
      <td>
        <input type="text" id="txtSLPName" name="SLPName" size="35"  class="inputFld" autocomplete="off" maxlength="50" onBlur="ldTrim(this)"> 
          <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
        </input>
      </td>
      <TD noWrap="nowrap"><img src="images/spacer.gif" width="14px"/></TD> <!-- spacer column -->
      <td class="bodyBlue">Role: *</td>

      <td>
        <select id="txtSLPPersonnelTypeID" name="SLPPersonnelTypeID" class="inputFld">
          <option></option>
          <xsl:for-each select="/Root/Reference[@ListName='PersonnelType']">
          <xsl:call-template name="BuildSelectOptions">
          <xsl:with-param name="current" select="/Root/ShopPersonnel/@PersonnelTypeID"/>
          </xsl:call-template>
          </xsl:for-each>
        </select>

        <input type="hidden" id="txtSLPShopManagerFlag" name="SLPShopManagerFlag">
          <xsl:attribute name="value"><xsl:value-of select="@ShopManagerFlag"/></xsl:attribute>
        </input> 

      </td>
    </tr>
    <tr>
      <td class="bodyBlue" nowrap="nowrap">Phone: *</td>
      <td class="bodyBlue" nowrap="nowrap" align="left">
        <input name="SLPPhoneAreaCode" id="txtSLPPhoneAreaCode" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@PhoneAreaCode"/></xsl:attribute>
        </input>
        <input name="SLPPhoneExchangeNumber" id="txtSLPPhoneExchangeNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@PhoneExchangeNumber"/></xsl:attribute>
        </input>
        <input name="SLPPhoneUnitNumber" id="txtSLPPhoneUnitNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" onBlur="ldTrim(this)" size="4" maxlength="4">
          <xsl:attribute name="value"><xsl:value-of select="@PhoneUnitNumber"/></xsl:attribute>
        </input>
        ext.
        <input name="SLPPhoneExtensionNumber" id="txtSLPPhoneExtensionNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 5, event);" onBlur="ldTrim(this)" value="" size="5" maxlength="5">
          <xsl:attribute name="value"><xsl:value-of select="@PhoneExtensionNumber"/></xsl:attribute>
        </input>
      </td>
      <td></td>
      <td class="bodyBlue">Fax:</td>
      <td class="bodyBlue" nowrap="nowrap">
        <input name="SLPFaxAreaCode" id="txtSLPFaxAreaCode" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@FaxAreaCode"/></xsl:attribute>
        </input>
        <input name="SLPFaxExchangeNumber" id="txtSLPFaxExchangeNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@FaxExchangeNumber"/></xsl:attribute>
        </input>
        <input name="SLPFaxUnitNumber" id="txtSLPFaxUnitNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" onBlur="ldTrim(this)" size="4" maxlength="4">
          <xsl:attribute name="value"><xsl:value-of select="@FaxUnitNumber"/></xsl:attribute>
        </input>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">Cell:</td>
      <td class="bodyBlue">
        <input name="SLPCellAreaCode" id="txtSLPCellAreaCode" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@CellAreaCode"/></xsl:attribute>
        </input>
        <input name="SLPCellExchangeNumber" id="txtSLPCellExchangeNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" onBlur="ldTrim(this)" size="3" maxlength="3">
          <xsl:attribute name="value"><xsl:value-of select="@CellExchangeNumber"/></xsl:attribute>
        </input>
        <input name="SLPCellUnitNumber" id="txtSLPCellUnitNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" onBlur="ldTrim(this)" size="4" maxlength="4">
          <xsl:attribute name="value"><xsl:value-of select="@CellUnitNumber"/></xsl:attribute>
        </input>
      </td>
      <td></td>
      <td class="bodyBlue">Email:&#160;</td>
      <td>
        <input id="txtSLPEmailAddress" name="SLPEmailAddress" size="35" class="inputFld" onbeforedeactivate="ldTrim(this); checkEMail(this)" maxlength="50">
          <xsl:attribute name="value"><xsl:value-of select="@EmailAddress"/></xsl:attribute>
        </input>
      </td>
    </tr>
  </table>

</xsl:template> 

<xsl:template match="Pricing"> <!-- Pricing -->
  <TABLE border="0" cellspacing="0" cellpadding="0">
    <TR>
      <TD><img src="images/spacer.gif" width="1px" height="4px"/></TD>
    </TR>
    <TR>
      <TD valign="top">

        <table border="0" cellspacing="0" cellpadding="1">
          <tr>
            <td class="bodyBlue" colspan="2" align="center" style="color:#000000">Hourly Labor Rates:</td>
          </tr>
          <tr>
            <td class="bodyBlue">Sheet Metal: *</td>
            <td class="bodyBlue">$
              <input type="text" id="txtHourlyRateSheetMetal" name="HourlyRateSheetMetal" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)" onChange="FormatCurrencyObj(this)">
                 <xsl:attribute name="value"><xsl:value-of select="@HourlyRateSheetMetal"/></xsl:attribute>
              </input>
            </td>
          </tr>
          <tr>
            <td class="bodyBlue">Refinishing: *</td>
            <td class="bodyBlue">$
              <input type="text" id="txtHourlyRateRefinishing" name="HourlyRateRefinishing" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)" onChange="FormatCurrencyObj(this)">
                <xsl:attribute name="value"><xsl:value-of select="@HourlyRateRefinishing"/></xsl:attribute>
              </input>
            </td>
          </tr>
          <tr>
            <td class="bodyBlue" nowrap="nowrap">Unibody/Frame: *</td>
            <td class="bodyBlue">$
              <input type="text" id="txtHourlyRateUnibodyFrame" name="HourlyRateUnibodyFrame" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)" onChange="FormatCurrencyObj(this)">
                <xsl:attribute name="value"><xsl:value-of select="@HourlyRateUnibodyFrame"/></xsl:attribute>
              </input>
            </td>
          </tr>
          <tr>
            <td class="bodyBlue">Mechanical: *</td>
            <td class="bodyBlue">$
              <input type="text" id="txtHourlyRateMechanical" name="HourlyRateMechanical" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)" onChange="FormatCurrencyObj(this)">
                <xsl:attribute name="value"><xsl:value-of select="@HourlyRateMechanical"/></xsl:attribute>
              </input>
            </td>
          </tr>
        </table>

      </TD>
      <TD><img src="images/spacer.gif" width="60px"/></TD> <!-- spacer column -->
      <TD valign="top">

        <table border="0" cellpadding="0" cellspacing="1">
          <tr>
            <td class="bodyBlue" colspan="2" align="center" style="color:#000000">Refinish Materials:</td>
          </tr>
          <tr>
            <td class="bodyBlue">Hour Rate: *</td>
            <td class="bodyBlue">$
              <input type="text" id="txtRefinishSingleStageHourly" name="RefinishSingleStageHourly" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)" onChange="FormatCurrencyObj(this)">
                <xsl:attribute name="value"><xsl:value-of select="@RefinishSingleStageHourly"/></xsl:attribute>
              </input>
            </td>
          </tr>
          <tr>
            <td colspan="2"><img src="images/spacer.gif" alt="" width="10px" height="34" border="0"/></td>
          </tr>

          <tr>
            <td class="bodyBlue" colspan="2" align="center" style="color:#000000">Storage Fee:</td>
          </tr>
          <tr>
            <td class="bodyBlue">Daily: *</td>
            <td class="bodyBlue">$
              <input type="text" id="txtDailyStorage" name="DailyStorage" style="text-align:right;" wizard="yes" size="8" maxlength="8" class="inputFld" onkeypress="CheckNum(event)" onChange="FormatCurrencyObj(this)">
                <xsl:attribute name="value"><xsl:value-of select="@DailyStorage"/></xsl:attribute>
              </input>
            </td>
          </tr>

        </table>

      </TD>
      <TD><img src="images/spacer.gif" width="10px"/></TD> <!-- spacer column -->
      <TD valign="top">
      </TD>
    </TR>
  </TABLE>
</xsl:template>

<!-- Build an option list for a Select box from xml reference data.  Set the contact by wrapping call to
    this template in a for-each loop.  The parameter 'current' represents the option that should be 
  currently selected.  If you need a blank option, add it manually. -->     
  <xsl:template name="BuildSelectOptions">
  <xsl:param name="current"/>
  <option>
    <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
    
    <xsl:if test="@ReferenceID = $current">
      <xsl:attribute name="selected"/>
    </xsl:if>
    
    <xsl:value-of select="@Name"/>  
  </option>
  </xsl:template>

</xsl:stylesheet>  