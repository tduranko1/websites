<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" indent="yes" encoding="UTF-8" />

<xsl:param name="UserType"  select="'U'"/>

<xsl:template match="/Root">
  <script language="javascript">
    var bShowInactive = false;
    <![CDATA[
      	function setUserID(piUserID,psProcess,piSec)
      	{
      		document.frmMainForm.hidUserID.value = piUserID;
      		document.frmMainForm.hidUserType.value = sUserType;
      		document.frmMainForm.submit();	
      	}
        
        function doAddUser(){
          window.document.location.href = "frmUserDetail.asp?hidUserType=" + sUserType + "&hidUserID=-1";
        }
        
        function showHideInactive2() {
          var obj = document.getElementById("ShowInactive");
          var sShowStyle;
          var sText;
          if (bShowInactive) {
            sText = "Show Inactive users";
            sShowStyle = "none";
            
          } else {
            sText = "Hide Inactive users";
            sShowStyle = "inline";
          }
          
          try {
          var oUsers = document.getElementById("srhList");
          if (oUsers) {
            var iStatusCell = oUsers.rows[0].cells.length - 1;
            //alert(oUsers.rows[0].cells.length);
            for (var i = 0; i < oUsers.rows.length; i++) {
              var oCells = oUsers.rows[i].cells;
            //  alert(oUsers.rows[i].cells[iStatusCell].innerText);
              if (oCells[iStatusCell].innerHTML == "Inactive") {
                oUsers.rows[i].style.display = sShowStyle;
                for (var j = 0; j < oCells.length; j++)
                  oCells[j].style.display = sShowStyle;
              }
            }
          }
          } catch (e) {
            alert(e.description);
          }
          
          if (obj)
            obj.innerHTML = sText;
          bShowInactive = !bShowInactive;
          repaintTable();
        }
        
        function showHideInactive(){
          var oTbl = document.getElementById("InactiveList");
          var oImg = document.getElementById("imgExpand");
          if (oTbl) {
            if (bShowInactive) {
              oTbl.style.display = "none";
              oImg.src = "images/btn_showinactiveusers.gif";
            } else {
              oTbl.style.display = "inline";
              oImg.src = "images/btn_hideinactiveusers.gif";
            }
            bShowInactive = !bShowInactive;
          }
        }
    ]]>
  </script>
  

<form name="frmMainForm" id="frmMainForm" method="POST" action="frmUserDetail.asp">
  <input type="hidden" name="hidUserID" id="hidUserID"/>
  <input type="hidden" name="hidUserType" id="hidUserType"/>
    <table border="0" cellpadding="0" cellspacing="0" style="width:98%">
      <tr>
        <td width1="60px"><img border="0" src="images/btn_add.gif" width="83" height="31" onclick="doAddUser();" style="cursor:hand"/></td>
        <!-- <td width1="*" style="text-align:right"><a name="ShowInactive" id="ShowInactive" href="javascript:showHideInactive()" onmousemove="window.status='Show Inactive users'" onmouseout="window.status=''">Show Inactive users</a></td> -->
      </tr>
    </table>
    
		<TABLE width="98%" border="0" cellspacing="1" cellpadding="0" bgcolor="#EEEEEE" id="userList">
      <thead>
				<TR>
					<TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Name&#xa0;(Last,&#xa0;First)</TD>
					<TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Login&#xa0;ID</TD>
          <xsl:if test="$UserType != 'M'">
					<TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Office</TD>
					<TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Client User id</TD>
          </xsl:if>
					<!-- <TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Status</TD> -->
					<!-- <TD class="theader" align="center"><a href="frmUserDetail.asp?hidUserType=" style="vertical-align:middle;"><img border="0" src="images/btn_add_new.gif" width="83" height="31"/></a></TD> -->
				</TR>
      </thead>
      <xsl:variable name="CPAppID">
        <xsl:value-of select="Reference[@List='Application' and @Name='ClaimPoint']/@ReferenceID"/>
      </xsl:variable>
      <tbody id="srhList">
        <xsl:for-each select="User">
        <xsl:sort select="@NameLast"/>
        <xsl:sort select="Application[@ApplicationID=$CPAppID]/@LogonId"/>
        <xsl:if test="Application[@ApplicationID=$CPAppID]/@Active = '1'">
				<tr style="curso:hand" onMouseOver="GridMouseOver(this)" onMouseOut="GridMouseOut(this)" title="Click to edit user">
          <xsl:attribute name="onclick">setUserID('<xsl:value-of select="@UserID"/>', 'U', '')</xsl:attribute>
            <td nowrap="" style="padding:2px">
              <xsl:value-of select="@NameLast"/>, <xsl:value-of select="@NameFirst"/>
              <xsl:if test="@ManageCPUsers = 1">
               <img src="images/icon_mini_profile.gif" title="Super User" width="12" height="13" hspace="10" border="0" align="absbottom"/>
              </xsl:if>
            </td>
            <td nowrap="" style="padding:2px"><xsl:value-of select="Application[@ApplicationID=$CPAppID]/@LogonId"/></td>
            <xsl:if test="$UserType != 'M'">
            <td nowrap="" style="padding:2px"><xsl:value-of select="@ClientOfficeId"/></td>
            <td nowrap="" style="padding:2px"><xsl:value-of select="@ClientUserId"/></td>
            </xsl:if>
            <!-- <td nowrap="" style="padding:2px;text-align:center">
              
              <xsl:choose>
                <xsl:when test="Application[@ApplicationID=$CPAppID]/@Active = '1'">Active</xsl:when>
                <xsl:otherwise>Inactive</xsl:otherwise>
              </xsl:choose>
            </td> -->
				</tr>
        </xsl:if>
        </xsl:for-each>
       </tbody>
      </TABLE>
    <div style="padding:0px;padding-top:20px;padding-bottom:5px;font-weight:bold" onclick="showHideInactive()">
      <img id="imgExpand" name="imgExpand" src="images/btn_ShowInactiveUsers.gif" align="absmiddle"/>
    </div>
		<TABLE width="98%" border="0" cellspacing="1" cellpadding="0" bgcolor="#EEEEEE" id="InactiveList" style="display:none">
      <thead>
				<TR>
					<TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Name&#xa0;(Last,&#xa0;First)</TD>
					<TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Login&#xa0;ID</TD>
          <xsl:if test="$UserType != 'M'">
					<TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Office</TD>
					<TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Client User id</TD>
          </xsl:if>
					<!-- <TD class="theader" valign="center" title="Click to sort ascending/descending" style="vertical-align:middle;"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Status</TD> -->
					<!-- <TD class="theader" align="center"><a href="frmUserDetail.asp?hidUserType=" style="vertical-align:middle;"><img border="0" src="images/btn_add_new.gif" width="83" height="31"/></a></TD> -->
				</TR>
      </thead>
      <xsl:variable name="CPAppID">
        <xsl:value-of select="Reference[@List='Application' and @Name='ClaimPoint']/@ReferenceID"/>
      </xsl:variable>
       <tbody id="srhInactiveList">
        <xsl:for-each select="User">
        <xsl:sort select="@NameLast"/>
        <xsl:sort select="Application[@ApplicationID=$CPAppID]/@LogonId"/>
        <xsl:if test="Application[@ApplicationID=$CPAppID]/@Active = '0'">
				<tr style="curso:hand" onMouseOver="GridMouseOver(this)" onMouseOut="GridMouseOut(this)" title="Click to edit user">
          <xsl:attribute name="onclick">setUserID('<xsl:value-of select="@UserID"/>', 'U', '')</xsl:attribute>
            <td nowrap="" style="padding:2px"><xsl:value-of select="@NameLast"/>, <xsl:value-of select="@NameFirst"/></td>
            <td nowrap="" style="padding:2px"><xsl:value-of select="Application[@ApplicationID=$CPAppID]/@LogonId"/></td>
            <xsl:if test="$UserType != 'M'">
            <td nowrap="" style="padding:2px"><xsl:value-of select="@ClientOfficeId"/></td>
            <td nowrap="" style="padding:2px"><xsl:value-of select="@ClientUserId"/></td>
            </xsl:if>
            <!-- <td nowrap="" style="padding:2px;text-align:center">
              
              <xsl:choose>
                <xsl:when test="Application[@ApplicationID=$CPAppID]/@Active = '1'">Active</xsl:when>
                <xsl:otherwise>Inactive</xsl:otherwise>
              </xsl:choose>
            </td> -->
				</tr>
        </xsl:if>
        </xsl:for-each>
       </tbody>
  </TABLE>      
</form>

</xsl:template>
</xsl:stylesheet>
