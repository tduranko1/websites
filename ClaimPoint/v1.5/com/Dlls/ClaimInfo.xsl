<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

  <xsl:param name="LynxID"/>
  <xsl:param name="AssignmentType"/>
  <xsl:param name="VehiclesList"/>
  <xsl:param name="AspectID"/>
  <xsl:param name="VehNumber"/>

  <xsl:template match="Claim">

    <xsl:comment>v1.5.0.2</xsl:comment>

    <tr>
      <td colspan="5">
        <p class="headerRedData">Caller Information:</p>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Name:
      </td>
      <td>
        <xsl:value-of select="concat(Caller/@NameTitle, ' ')"/>
        <xsl:value-of select="concat(Caller/@NameFirst, ' ')"/>
        <xsl:value-of select="Caller/@NameLast"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Day Phone:
      </td>
      <td>
        <xsl:value-of select="Caller/@DayAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Caller/@DayAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Caller/@DayExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Caller/@DayExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Caller/@DayExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Caller/@DayUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Caller/@DayUnitNumber"/>
        <xsl:if test="string-length(Caller/@DayExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Caller/@DayExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Address:
      </td>
      <td class="body" rowspan="2" valign="top">
        <xsl:value-of select="Caller/@Address1"/>
        <br/>
        <xsl:if test="Caller/@Address2 != ''">
          <xsl:value-of select="Caller/@Address2"/>
          <br/>
        </xsl:if>
        <xsl:value-of select="Caller/@AddressCity"/>
        <xsl:if test="string-length(translate(normalize-space(Caller/@AddressCity),' ','')) &gt; 0 and string-length(translate(normalize-space(Caller/@AddressState),' ',''))">
          <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:value-of select="concat(Caller/@AddressState, ' ')"/>
        <xsl:value-of select="Caller/@AddressZip"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Night Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Caller/@NightAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Caller/@NightAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Caller/@NightExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Caller/@NightExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Caller/@NightExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Caller/@NightUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Caller/@NightUnitNumber"/>
        <xsl:if test="string-length(Caller/@NightExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Caller/@NightExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td>&#160;</td>
      <td width="20"></td>
      <td class="bodyBlue">
        Alt. Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Caller/@AlternateAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Caller/@AlternateAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Caller/@AlternateExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Caller/@AlternateExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Caller/@AlternateExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Caller/@AlternateUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Caller/@AlternateUnitNumber"/>
        <xsl:if test="string-length(Caller/@AlternateExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Caller/@AlternateExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td valign="top" class="bodyBlue">
        Relation to Insured:
      </td>
      <td class="body" valign="top">
        <xsl:value-of select="/Root/Reference[@List='CallerRelationToInsured' and @ReferenceID=current()/Caller/@InsuredRelationID]/@Name"/>
      </td>
      <td width="20"></td>
      <td valign="top" class="bodyBlue">
        Best Phone:
      </td>
      <td class="body" valign="top">
        <xsl:value-of select="Caller/@BestContactPhone"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Notice Method:
      </td>
      <td class="body">
        <xsl:value-of select="@ClaimantContactMethod"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Best Time:
      </td>
      <td class="body">
        <xsl:value-of select="Caller/@BestContactTime"/>
      </td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
    </tr>
    <tr>
      <td colspan="5" height="20">
        <p class="headerRedData">Insured Information:</p>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Name:
      </td>
      <td class="body">
        <xsl:value-of select="concat(Insured/@NameTitle, ' ')"/>
        <xsl:value-of select="concat(Insured/@NameFirst, ' ')"/>
        <xsl:value-of select="Insured/@NameLast"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Day Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Insured/@DayAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Insured/@DayAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Insured/@DayExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Insured/@DayExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Insured/@DayExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Insured/@DayUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Insured/@DayUnitNumber"/>
        <xsl:if test="string-length(Insured/@DayExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Insured/@DayExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Business:
      </td>
      <td class="body">
        <xsl:value-of select="Insured/@BusinessName"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Night Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Insured/@NightAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Insured/@NightAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Insured/@NightExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Insured/@NightExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Insured/@NightExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Insured/@NightUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Insured/@NightUnitNumber"/>
        <xsl:if test="string-length(Insured/@NightExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Insured/@NightExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Address:
      </td>
      <td class="body" rowspan="2" valign="top">
        <xsl:value-of select="Insured/@Address1"/>
        <br/>
        <xsl:if test="Insured/@Address2 != ''">
          <xsl:value-of select="Insured/@Address2"/>
          <br/>
        </xsl:if>
        <xsl:value-of select="Insured/@AddressCity"/>
        <xsl:if test="string-length(translate(normalize-space(Insured/@AddressCity),' ','')) &gt; 0 and string-length(translate(normalize-space(Insured/@AddressState),' ',''))">
          <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:value-of select="concat(Insured/@AddressState, ' ')"/>
        <xsl:value-of select="Insured/@AddressZip"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Alt. Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Insured/@AlternateAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Insured/@AlternateAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Insured/@AlternateExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Insured/@AlternateExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Insured/@AlternateExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Insured/@AlternateUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Insured/@AlternateUnitNumber"/>
        <xsl:if test="string-length(Insured/@AlternateExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Insured/@AlternateExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td>&#160;</td>
      <td width="20"></td>
      <td class="bodyBlue">
        Best Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Insured/@BestContactPhone"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        SSN/EIN:
      </td>
      <td class="body">
        <xsl:value-of select="Insured/@FedTaxId"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Best Time:
      </td>
      <td class="body">
        <xsl:value-of select="Insured/@BestContactTime"/>
      </td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
    </tr>
    <tr>
      <td colspan="5" height="20">
        <p class="headerRedData">Contact Information:</p>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Name:
      </td>
      <td class="body">
        <xsl:value-of select="concat(Contact/@NameTitle, ' ')"/>
        <xsl:value-of select="concat(Contact/@NameFirst, ' ')"/>
        <xsl:value-of select="Contact/@NameLast"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Day Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Contact/@DayAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@DayAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@DayExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@DayExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@DayExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@DayUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@DayUnitNumber"/>
        <xsl:if test="string-length(Contact/@DayExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Contact/@DayExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Address:
      </td>
      <td class="body" rowspan="2" valign="top">
        <xsl:value-of select="Contact/@Address1"/>
        <br/>
        <xsl:if test="Contact/@Address2 != ''">
          <xsl:value-of select="Contact/@Address2"/>
          <br/>
        </xsl:if>
        <xsl:value-of select="Contact/@AddressCity"/>
        <xsl:if test="string-length(translate(normalize-space(Insured/@AddressCity),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@AddressState),' ',''))">
          <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:value-of select="concat(Contact/@AddressState, ' ')"/>
        <xsl:value-of select="Contact/@AddressZip"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Night Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Contact/@NightAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@NightAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@NightExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@NightExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@NightExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@NightUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@NightUnitNumber"/>
        <xsl:if test="string-length(Contact/@NightExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Contact/@NightExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td>&#160;</td>
      <td width="20"></td>
      <td class="bodyBlue">
        Alt. Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Contact/@AlternateAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@AlternateAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@AlternateExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@AlternateExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@AlternateExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@AlternateUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@AlternateUnitNumber"/>
        <xsl:if test="string-length(Contact/@AlternateExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Contact/@AlternateExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Best Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Contact/@BestContactPhone"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Best Contact Time:
      </td>
      <td class="body">
        <xsl:value-of select="Insured/@BestContactTime"/>
      </td>
    </tr>
    <tr>
      <td valign="top" class="bodyBlue">
        Relation to Insured:
      </td>
      <td class="body">
        <xsl:value-of select="/Root/Reference[@List='ContactRelationToInsured' and @ReferenceID=current()/Contact/@InsuredRelationID]/@Name"/>
      </td>
      <td width="20"></td>
      <td valign="top" class="bodyBlue">
        Insurance Agent Name:
      </td>
      <td class="body">
        <xsl:value-of select="@AgentName"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" colspan="2">
        E-Mail:
        <span style="font-weight:normal; color:#000000;">
          <xsl:value-of select="Contact/@EmailAddress"/>
        </span>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Agent Phone:
      </td>
      <td class="body">
        <xsl:value-of select="@AgentAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(@AgentAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@AgentExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="@AgentExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(@AgentExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@AgentUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="@AgentUnitNumber"/>
        <xsl:if test="string-length(@AgentExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', @AgentExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
    </tr>
    <tr>
      <td colspan="5" height="20">
        <p class="headerRedData">Loss Information:</p>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Trip Purpose:
      </td>
      <td class="body">
        <xsl:value-of select="@TripPurpose"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Weather:
      </td>
      <td class="body">
        <xsl:value-of select="@WeatherCondition"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Loss Location:
      </td>
      <td class="body">
        <xsl:value-of select="@LossLocation"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Road Type:
      </td>
      <td class="body">
        <xsl:value-of select="@RoadType"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        City/State/Zip:
      </td>
      <td class="body">
        <xsl:value-of select="@LossCity"/>
        <xsl:if test="string-length(translate(normalize-space(@LossCity),' ','')) &gt; 0 and string-length(translate(normalize-space(@LossState),' ',''))">
          <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:value-of select="concat(@LossState, ' ')"/>
        <xsl:value-of select="@LossZip"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Road Location:
      </td>
      <td class="body">
        <xsl:value-of select="@RoadLocation"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        County:
      </td>
      <td class="body">
        <xsl:value-of select="@LossCounty"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Police Dept:
      </td>
      <td class="body">
        <xsl:value-of select="@PoliceDepartmentName"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Loss Type:
      </td>
      <td class="body" colspan="4">
        <xsl:if test="@LossTypeGrandParentID != '0'">
          <xsl:value-of select="concat(@LossTypeGrandParent, ' - ')"/>
        </xsl:if>
        <xsl:if test="@LossTypeParentID != '0'">
          <xsl:value-of select="concat(@LossTypeParent, ' - ')"/>
        </xsl:if>
        <xsl:value-of select="@LossType"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" valign="top">
        Loss Description:
      </td>
      <td class="body" colspan="4" valign="top">
        <xsl:value-of select="@LossDescription"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" valign="top">
        Remarks to Claim Rep:
      </td>
      <td class="body" colspan="4" valign="top">
        <xsl:value-of select="@Remarks"/>
      </td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
    </tr>
    <tr>
      <td colspan="5" height="20">
        <p class="headerRedData">Coverage Information:</p>
      </td>
    </tr>
    <tr>
      <td colspan="5">

        <table width="100%" border="0" cellspacing="1" cellpadding="4" bgcolor="#d0d0d0">
          <thead>
            <TR>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;">Description</TD>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;">Additional</TD>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;">Deductible</TD>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;">Limit</TD>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;" nowrap="">Max Days</TD>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;" nowrap="">Daily Limit</TD>
            </TR>
          </thead>

          <xsl:apply-templates select="Coverage" />

        </table>

      </td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
    </tr>
    <tr>
      <xsl:choose>
        <xsl:when test="$AssignmentType = 'Choice Shop Assignment'">
          <td colspan="5" height="20" nowrap="">
            <span class="headerRedData">Carrier Contact Information:</span>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td colspan="5" height="20" nowrap="">
            <span class="headerRedData">Carrier Contact Information:</span>
            <img src="images/spacer.gif" width="4" height="1" border="0"/>
            <a href="javascript:void(0)"
               title="Update Carrier Contact"
               onMouseOver="window.status='Update Carrier Contact'; return true"
               onMouseOut="window.status=''; return true">
              <xsl:attribute name="onclick">JavaScript:showCalIframe(this)</xsl:attribute>
              <img src="images/icon_mini_profile.gif" width="12" height="13" border="0"/>
              <img src="images/spacer.gif" width="3" height="1" border="0"/>Update
            </a>
          </td>
        </xsl:otherwise>
      </xsl:choose>

      <form id="frmCarrierUpdate" name="frmCarrierUpdate" method="post" target="IFrmCarrierUpdate" action="frmCarrierContactUpd.asp">
        <input type="hidden" name="CurrCarrierOfficeID" id="txtCurrCarrierOfficeID">
          <xsl:attribute name="value">
            <xsl:value-of select="Carrier/@OfficeID"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="CurrCarrierUserID" id="txtCurrCarrierUserID">
          <xsl:attribute name="value">
            <xsl:value-of select="Carrier/@UserID"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="CurrCarrierUserEmail" id="txtCurrCarrierUserEmail">
          <xsl:attribute name="value">
            <xsl:value-of select="Carrier/@EmailAddress"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="CurrCarrierUserName" id="txtCurrCarrierUserName">
          <xsl:attribute name="value">
            <xsl:value-of select="concat(Carrier/@NameFirst, ' ')"/>
            <xsl:value-of select="Carrier/@NameLast"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="CarrierLastUpdatedDate" id="txtCarrierLastUpdatedDate">
          <xsl:attribute name="value">
            <xsl:value-of select="Carrier/@SysLastUpdatedDate"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="ClaimNumber" id="txtClaimNumber">
          <xsl:attribute name="value">
            <xsl:value-of select="@ClientClaimNumber"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="LynxID" id="txtLynxID">
          <xsl:attribute name="value">
            <xsl:value-of select="$LynxID"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="AssignmentType" id="txtAssignmentType">
          <xsl:attribute name="value">
            <xsl:value-of select="$AssignmentType"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="VehiclesList" id="txtVehiclesList">
          <xsl:attribute name="value">
            <xsl:value-of select="$VehiclesList"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="AspectID" id="txtAspectID">
          <xsl:attribute name="value">
            <xsl:value-of select="$AspectID"/>
          </xsl:attribute>
        </input>
        <input type="hidden" name="VehNumber" id="txtVehNumber">
          <xsl:attribute name="value">
            <xsl:value-of select="$VehNumber"/>
          </xsl:attribute>
        </input>
      </form>

    </tr>
    <tr>
      <td class="bodyBlue">
        Name:
      </td>
      <td class="body">
        <xsl:value-of select="concat(Carrier/@NameTitle, ' ')"/>
        <xsl:value-of select="concat(Carrier/@NameFirst, ' ')"/>
        <xsl:value-of select="Carrier/@NameLast"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Carrier/@PhoneAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Carrier/@PhoneExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Carrier/@PhoneExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Carrier/@PhoneExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Carrier/@PhoneExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Carrier/@PhoneUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Carrier/@PhoneUnitNumber"/>
        <xsl:if test="string-length(Carrier/@PhoneExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Carrier/@PhoneExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        E-mail:
      </td>
      <td class="body">
        <xsl:value-of select="Carrier/@EmailAddress"/>
      </td>
      <td class="body" width="20">&#160;</td>
      <td class="bodyBlue">
        Office:
      </td>
      <td class="body">
        <xsl:value-of select="Carrier/@OfficeName"/>
      </td>
    </tr>


  </xsl:template>

  <xsl:template name="commaDelimitList">

    <xsl:param name="list"/>
    <xsl:if test="$list">
      <xsl:variable name="first" select="$list[1]"/>
      <xsl:variable name="rest">
        <xsl:call-template name="commaDelimitList">
          <xsl:with-param name="list" select="$list[position()!=1]"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:value-of select="concat(concat($first, ','), $rest)"/>
    </xsl:if>

  </xsl:template>


  <xsl:template match="Coverage">

    <tr bgcolor="#FFFFFF">
      <td style="border: 1px solid #d0d0d0;">
        <xsl:value-of select="@Description"/>
      </td>
      <td align="center" style="border: 1px solid #d0d0d0;">
        <xsl:if test="@AddtlCoverageFlag = 1">
          <img src="images/cb_checked.gif" alt="" width="12" height="12" border="0"/>
        </xsl:if>
      </td>
      <td align="center" style="border: 1px solid #d0d0d0;">
        <xsl:if test="@DeductibleAmt != ''">
          <xsl:variable name="DeductibleAmt">
            <xsl:value-of select="@DeductibleAmt"/>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="contains($DeductibleAmt, '.')">
              $<xsl:value-of select="substring-before($DeductibleAmt, '.')"/>
            </xsl:when>
            <xsl:otherwise>
              $<xsl:value-of select="$DeductibleAmt"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
      </td>

      <td align="center" style="border: 1px solid #d0d0d0;">
        <xsl:if test="@LimitAmt != ''">
          <xsl:variable name="LimitAmt">
            <xsl:value-of select="@LimitAmt"/>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="contains($LimitAmt, '.')">
              $<xsl:value-of select="substring-before($LimitAmt, '.')"/>
            </xsl:when>
            <xsl:otherwise>
              $<xsl:value-of select="$LimitAmt"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
      </td>
      <td align="center" style="border: 1px solid #d0d0d0;">
        <xsl:value-of select="@MaximumDays"/>
      </td>
      <td align="center" style="border: 1px solid #d0d0d0;">
        <xsl:if test="@LimitDailyAmt != ''">
          <xsl:variable name="LimitDailyAmt">
            <xsl:value-of select="@LimitDailyAmt"/>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="contains($LimitDailyAmt, '.')">
              $<xsl:value-of select="substring-before($LimitDailyAmt, '.')"/>
            </xsl:when>
            <xsl:otherwise>
              $<xsl:value-of select="$LimitDailyAmt"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
      </td>

    </tr>

  </xsl:template>

</xsl:stylesheet>