<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="UserID"/>
<xsl:param name="OfficeID"/>

  <xsl:template match="Root">

  <xsl:comment>v1.4.5.0</xsl:comment>

    <xsl:choose>
      <xsl:when test="$OfficeID = '' or $OfficeID = 0"> 
        <select name="CarrierUser" id="txtCarrierUser" disabled="" style="font-weight: bold; font-size: 10px;">
        	<option value="">Please Select Office</option>
        </select>
      </xsl:when> 

      <xsl:otherwise>
        <select name="CarrierUser" id="txtCarrierUser" onChange="parent.updUserIDValue()">
         	<option value="0"></option>
  
          <xsl:for-each select="Office/OfficeUser[../@OfficeID = $OfficeID]" >
            <xsl:sort select="@NameLast" order="ascending"/>
              <xsl:call-template name="getCarrierUsersList"/>
          </xsl:for-each>
  
        </select>
      </xsl:otherwise>
    </xsl:choose>
  
  </xsl:template>
  
  <xsl:template name="getCarrierUsersList">

    <xsl:if test="translate(../@ClientOfficeId, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') != 'FCS'">
    	<option>
    		<xsl:attribute name="value">
    			<xsl:value-of select="@UserID"/>|<xsl:value-of select="@EmailAddress"/>|<xsl:value-of select="concat(@NameFirst, ' ')"/><xsl:value-of select="@NameLast"/>
    		</xsl:attribute>
        <xsl:if test="@UserID = $UserID">
          <xsl:attribute name="Selected"/>
        </xsl:if>
    			<xsl:value-of select="@NameLast"/>, <xsl:value-of select="@NameFirst"/>
    	</option>
    </xsl:if>
  
  </xsl:template>

</xsl:stylesheet>
