<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="/">
	<xsl:apply-templates select="Root/InsuranceCompany"> 
   		<xsl:sort select="@Name"/> 
    </xsl:apply-templates>
</xsl:template>

<xsl:template match="InsuranceCompany">
	<option>
		<xsl:attribute name="value">
			<xsl:value-of select="@InsuranceCompanyID"/>~^~<xsl:value-of select="@Name"/>
		</xsl:attribute>
		<xsl:value-of select="@Name"/>
	</option>
</xsl:template>

</xsl:stylesheet>