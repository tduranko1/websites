<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="HistoryFilter"/>

<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
         
  function ConvertDate(time)
  {
    var lsTime = time;
    var lsMO;
    var lsDD;
    var lsYYYY;

    lsMO = lsTime.substr(5,2);
    lsDD = lsTime.substr(8,2);
    lsYYYY = lsTime.substr(0,4);

    var strDate = lsTime.substr( 5, 2 ) + "-"
                 + lsTime.substr( 8, 2 ) + "-"
                 + lsTime.substr( 0, 4 )

    return strDate;
  }

  function ConvertTime(time)
  {
    var lsTime = time;
  	if (lsTime == null || lsTime == "") return "";
    var lsHH;
    var lsMM;
    var lsAmPm = 'AM';

    if (lsTime.length <= 4)
    {
      lsHH = lsTime.substr(0,2);
      lsMM = lsTime.substr(2,2);
    }
    else if (lsTime.length > 4)
    {
      lsHH = lsTime.substr(11,2);
      lsMM = lsTime.substr(14,2);
    }
    
    if (lsHH >= 1 && lsHH < 10)
    {
      lsHH = lsHH.substr(1,1);
    }
    else if (lsHH == 12)
    {
      lsAmPm = 'PM';
    }
    else if (lsHH > 12)
    {
      lsHH = lsHH-12;
      lsAmPm = 'PM';
    }

    var lsRetTime = lsHH + ':' + lsMM  + ' ' + lsAmPm;
    return lsRetTime;
  }
  ]]>
</msxsl:script>

<xsl:template match="/">

  <xsl:comment>v1.5.0.0</xsl:comment>
      <tr>
        <td colspan="5">

          <table width="98%" border="0" cellspacing="1" cellpadding="2" bgcolor="#DDDDDD" id="HistoryResults">
            <thead>
              <TR>
                <TD style="width:70px" class="theader" title="Click to sort ascending/descending"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Date</TD>
                <TD class="theader" title="Click to sort ascending/descending"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Service Channel</TD>
                <TD class="theader" title="Click to sort ascending/descending"><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Note Type</TD>
             <TD sortReq="No" style="font-weight: bold; text-align: center; vertical-align: top; padding: 2px; background: #E7E7F7; border-bottom: 1px solid #99CCFF; border-right: 1px solid #99CCFF; border-left: 1px solid #99CCFF; border-top: 1px solid #99CCFF;">
              <strong>&#xa0;Description&#xa0;</strong>
            </TD>
              </TR>
            </thead>
            <tbody id="srhList">
      
            <xsl:choose>
              <xsl:when test="string($HistoryFilter) != 'none'">
                <xsl:apply-templates select="Root/History[@PertainsTo = $HistoryFilter]" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:apply-templates />
              </xsl:otherwise>
            </xsl:choose>
  
            </tbody>
          </table>

        </td>
      </tr>

</xsl:template>

<xsl:template match="History">
  
      <tr>
        <xsl:attribute name="bgcolor">
          <xsl:choose>
            <xsl:when test="position() mod 2 = 1">#FFFFFF</xsl:when>
            <xsl:when test="position() mod 2 = 0">#DFEFFF</xsl:when>
          </xsl:choose>
        </xsl:attribute>
        <td class="body" align="center" valign="top">
          <xsl:value-of select="user:ConvertDate(string(@CompletedDate))"/><br/><xsl:value-of select="user:ConvertTime(string(@CompletedDate))"/>
        </td>
        <td class="body" align="center" valign="top">
          <xsl:value-of select="@ServiceChannelCDName"/>
        </td>
        <td class="body" align="center" valign="top">
          <xsl:value-of select="@EventName"/>
        </td>
        <td class="body" valign="top">
          <xsl:value-of select="@Description"/>
        </td>
      </tr>

</xsl:template>

</xsl:stylesheet>