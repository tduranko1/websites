<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

  <xsl:param name="OfficeID"/>
  <xsl:param name="tabIndexNo"/>
  
  <xsl:template match="Root">
  
    <select name="AssignmentTypeID" id="txtAssignmentTypeID" required="true">
  		<xsl:if test="$tabIndexNo != ''">
        <xsl:attribute name="tabIndex">
    			<xsl:value-of select="$tabIndexNo"/>
    		</xsl:attribute>
  		</xsl:if>
      <option value=""></option>
  
      <xsl:choose>
        <xsl:when test="$OfficeID = 0"> 
          <xsl:for-each select="Reference[@List = 'AssignmentType' and @ServiceChannelName='Ind Adjuster']" >
            <xsl:call-template name="getAllIATypes"/>
          </xsl:for-each>
        </xsl:when> 
        <xsl:otherwise>
          <xsl:for-each select="Office/OfficeAssignmentType[../@OfficeID = $OfficeID]" >
            <xsl:call-template name="getContractIATypes"/>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>

    </select>
  
  </xsl:template>
  
  <xsl:template name="getAllIATypes">
  
  	<option>
  		<xsl:attribute name="value">
  			<xsl:value-of select="@ReferenceID"/>
  		</xsl:attribute>
  		  <xsl:value-of select="@Name"/>
  	</option>
  
  </xsl:template>

  <xsl:template name="getContractIATypes">
  
  <xsl:if test="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = current()/@AssignmentTypeID and @ServiceChannelName = 'Ind Adjuster']">
  	<option>
      <xsl:variable name="currentID"><xsl:value-of select="@AssignmentTypeID"/></xsl:variable>
  		<xsl:attribute name="value">
  			<xsl:value-of select="$currentID"/>
  		</xsl:attribute>
        <xsl:value-of select="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = $currentID]/@Name"/>
  	</option>
  </xsl:if>
  
  </xsl:template>

</xsl:stylesheet>

