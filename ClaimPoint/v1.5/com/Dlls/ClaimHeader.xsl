<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="AspectID"/>

<xsl:template match="Claim">

  <xsl:comment>v1.5.0.0</xsl:comment>

  <table border="0" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000099; background: #E6E5F5; border-collapse:collapse;" width="100%">
    <tr>
      <td height="20">
        <span class="bodyBlue">LYNX ID:</span>
        <span class="body"><xsl:value-of select="@LynxID"/>-<xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@VehicleNumber"/></span>
      </td>
      <td align="right" nowrap="">
        <span class="bodyBlue">Claim Number:</span>
        <span class="body"><xsl:value-of select="@ClientClaimNumber"/></span>
      </td>
    </tr>
  </table>

</xsl:template>

</xsl:stylesheet>
