<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">

  <xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
    function ShopPhoneNember(ShopPhoneNo)
    {
    debugger;
    var PhoneNo,liPhoneAreaCode,liPhoneExchangeNumber,liPhoneUnitNumber,ReshopPhoneNo;
    PhoneNo=ShopPhoneNo;
    if(PhoneNo != "")
    {
    liPhoneAreaCode = PhoneNo.substring(1, 4);
    liPhoneExchangeNumber = PhoneNo.substring(4, 7);
    liPhoneUnitNumber = PhoneNo.substring(7, 11);
    ReshopPhoneNo = liPhoneAreaCode + "-" + liPhoneExchangeNumber + "-" + liPhoneUnitNumber;
    }
    else
    {
    ReshopPhoneNo=PhoneNo;
    }
    return ReshopPhoneNo;
    }
    function ShopDistance(ShopDistance)
    {
    var Distance = ShopDistance;
    if(Distance == "")
    {
    Distance=0;
    }
    return Distance;
    }
  ]]>
  </msxsl:script>
  
  <xsl:param name="VehDesc"/>
  <xsl:param name="PageTitle"/>
  <xsl:param name="AddVehicle"/>


  <xsl:template match="HQProviderSearchRs">
    <xsl:apply-templates select="ShopSearchResults"></xsl:apply-templates>
  </xsl:template>

  <xsl:template match="ShopSearchResults">

    <xsl:comment>v1.4.4.1</xsl:comment>

    <xsl:choose>
      <xsl:when test="count(Shop) &gt; 0">
        <div id ="ChoiceShopSelection">
          <table border="0" cellpadding="0" cellspacing="0" style="width:800px">
            <tr>
              <td align="left" style="width:620px;">
                <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000099" style="border-collapse:collapse">
                  <tr>
                    <td style="font-size:10pt">
                      <xsl:choose>
                        <xsl:when test="$VehDesc != ''">
                          <p style="font-weight:bold;">
                            Shop selection for vehicle <xsl:value-of select="$VehDesc"/>:
                          </p>
                        </xsl:when>
                        <xsl:otherwise>
                          <p style="font-weight:bold;">Selecting the desired shop:</p>
                        </xsl:otherwise>
                      </xsl:choose>
                      <ol>
                        <li>Select the desired repair shop by marking the appropriated checkbox.</li>
                        <li>Enter comments to repair shop in the field below if necessary.</li>
                        <li>Press the "NEXT" button below to continue entering information for this claim.</li>
                        <br/>
                      </ol>
                    </td>
                  </tr>
                </table>
               </td>
            </tr>
          </table>

          <!-- table for Add new button-->
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td align="right">
                <a href="javascript:AddChoiceShop()"
                   onMouseOut="window.status=''; return true" style="display:inline-block;">
                  <img border="0" src="images/btn_Add_Choice_Shop.gif" WIDTH="83" HEIGHT="31" />
                </a>
              </td>
            </tr>
            <tr>
              <td>
                <!-- Table for search result -->
                <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#000099" style="border-collapse:collapse">
                  <tr bgcolor="#E5E5F5" style="height: 28px;">
                    <td style="font-weight:bold; text-align:center"></td>
                    <td style="font-weight:bold; text-align:center">Shop Name</td>
                    <td style="font-weight:bold; text-align:center">Address</td>
                    <td style="font-weight:bold; text-align:center">Phone</td>
                    <td style="font-weight:bold; text-align:center">Approx. Dist.</td>
                  </tr>
                  <xsl:for-each select="Shop" >
                    <xsl:call-template name="ShopInfo"/>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
            <tr>
              <td align="right">
                <a href="javascript:AddChoiceShop()"
                   onMouseOut="window.status=''; return true" style="display:inline-block;">
                  <img border="0" src="images/btn_Add_Choice_Shop.gif" WIDTH="83" HEIGHT="31" />
                </a>
              </td>
            </tr>
          </table >
          <br/>
          <br/>

          <table width="600" border="1" cellpadding="0" cellspacing="0" bordercolor="#000099" style="border-collapse:collapse">
            <tr bgcolor="#E5E5F5">
              <td style="padding:8px;">
                <span style="color:#000099; font-size:10pt; font-weight:bold;">Comments to Repair Shop:</span>
                <span id="commFldMsg" style="color:#000000; font-weight:normal; font-size:7pt"> (max.:250 characters)</span>
              </td>
            </tr>
            <tr>
              <td style="padding:10px;">
                <textarea name="ShopRemarks" id="txtShopRemarks" cols="110" rows="5" wrap="physical" style="width:100%; height:60px;" onKeyDown="CheckInputLength(this, 250)"></textarea>
              </td>
            </tr>
          </table>

          <br/>

          <table width="650" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td align="right" valign="top" nowrap="">
                  <a href="javascript:selectShop()"
                   onMouseOut="window.status=''; return true" style="display:inline-block;">
                  <xsl:choose>
                    <xsl:when test="$AddVehicle = '1'">
                      <xsl:attribute name="title">To New Vehicle Infomation Page</xsl:attribute>
                      <xsl:attribute name="onMouseOver">window.status='To New Vehicle Infomation Page'; return true</xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:attribute name="title">To Claim Infomation Page</xsl:attribute>
                      <xsl:attribute name="onMouseOver">window.status='To Claim Information Page'; return true</xsl:attribute>
                    </xsl:otherwise>
                  </xsl:choose>
                  <img border="0" src="images/btn_next.gif" WIDTH="83" HEIGHT="31" />
                </a>
              </td>
            </tr>
          </table>
        </div>
        <div id="Space">
          <table>
            <tr>
              <td></td>
            </tr>
          </table>
        </div>
      </xsl:when>
      <xsl:otherwise>

        <br/>

        <table width="600" border="0" cellpadding="0" cellspacing="0">

          <xsl:if test="$PageTitle = 'Shop Selection'">
            <tr>
              <td colspan="5" nowrap="">
                <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="40" />
              </td>
            </tr>
            <tr>
              <td nowrap="">
                <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
              </td>
              <td colspan="4" nowrap="" style="color:#FF0000; font-size:11pt; font-weight:bold;">
                This query yielded no results.
              </td>
            </tr>
            <tr>
              <td nowrap="">&#160;</td>
              <td width="100%" nowrap="" class="legendnopad" style="font-size:10pt;">
                Go to the <a href="javascript:tryAdvSearch()" title="Search Shops">Shop Search</a> page to change your search query.
              </td>
              <td align="right" valign="top" nowrap="">
                <a href='javascript:goCancel()'
                   title='Cancel'
                   style='cursor:pointer; cursor:hand;'>
                  <img src='images/btn_cancel.gif' border='0' WIDTH='83' HEIGHT='31' />
                </a>
              </td>
              <td align="right" valign="top" nowrap="">
                <img border="0" src="images/spacer.gif" WIDTH="10" HEIGHT="1" />
              </td>
              <td align="right" valign="top" nowrap="">
                <a href="javascript:tryAdvSearch()"
                   title="OK"
                    style="cursor:pointer; cursor:hand;">
                  <img border="0" src="images/btn_ok.gif" WIDTH="52" HEIGHT="31" />
                </a>
              </td>
            </tr>
          </xsl:if>

          <xsl:if test="contains($PageTitle, 'Shop Search')">
            <tr>
              <td valign="top">
                <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
              </td>
              <td width="100%" nowrap="" class="legendnopad" style="font-size: 10pt; color:#FF0000; font-weight:bold;">
                Your query didn't return any results. Please redefine your search.
              </td>
            </tr>
          </xsl:if>

        </table>
      </xsl:otherwise>

    </xsl:choose>

  </xsl:template>

  <xsl:template name="ShopInfo" >

    <tr style="padding:4px; border: 1px solid #000099;">
      <xsl:attribute name="id">
        <xsl:text disable-output-escaping="yes">tblRowShop_</xsl:text>
        <xsl:value-of select="ShopUID"/>
      </xsl:attribute>
      <td valign="top">
        <input name="cb_SelectShop" type="checkbox" onClick="setSelectedShop(this)">
          <xsl:attribute name="value">
            <xsl:value-of select="ShopUID"/>|<xsl:value-of select="ShopName"/>|<xsl:value-of select="ShopAddr1"/>|<xsl:value-of select="ShopAddr2"/>|<xsl:value-of select="ShopCity"/>|<xsl:value-of select="ShopStateCd"/>|<xsl:value-of select="ShopZipCode"/>|<xsl:value-of select="ShopTelephoneNo"/>|<xsl:value-of select="ShopFaxNo"/>|<xsl:value-of select="ShopEmailAddress"/>|<xsl:value-of select="ShopFederalTaxID"/>|<xsl:value-of select="ShopLicenseNo"/>|<xsl:value-of select="ShopLink"/>|<xsl:value-of select="ShopPartnerID"/>|<xsl:value-of select="ShopContactFirstName"/>|<xsl:value-of select="ShopContactLastName"/>|<xsl:value-of select="Distance"/>
          </xsl:attribute>
        </input>
      </td>
      <td valign="top">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
          <tr>
            <td valign="top" style="padding: 3px; spacing: 3px;">
              <xsl:value-of select="ShopName"/>
            </td>
            <xsl:if test="@CertifiedFirstFlag = '1'">
              <td align="right" valign="top">
                <img src="images/CFLogo_small.jpg" alt="CertifiedFirst Network shop" border="0" WIDTH="84" HEIGHT="27" style="cursor:help" />
              </td>
            </xsl:if>
          </tr>
        </table>
      </td>
      <td valign="top" style="padding: 3px; spacing: 3px;">
        <xsl:value-of select="ShopAddr1"/>
        <br/>
        <xsl:if test="ShopAddr2 != ''">
          <xsl:value-of select="ShopAddr2"/>
          <br/>
        </xsl:if>
        <xsl:value-of select="ShopCity"/>
        <xsl:text disable-output-escaping="yes">, </xsl:text>
        <xsl:value-of select="ShopStateCd"/>
        <xsl:text disable-output-escaping="yes"> </xsl:text>
        <xsl:value-of select="ShopZipCode"/>
      </td>
          <!--"ShopTelephoneNo"-->
      <td align="center" valign="top" nowrap="" style="padding: 3px; spacing: 3px;">
        <xsl:value-of select="user:ShopPhoneNember(translate(translate(translate(ShopTelephoneNo,'+',''),'-',''),' ',''))"/>
      </td>
      <td valign="top" align="center" nowrap="" style="padding: 3px; spacing: 3px;">
        <xsl:value-of select="user:ShopDistance(format-number(Distance, '##'))"/>
      </td>
      <td style="display:none">
        <input name="ShopName" type="hidden">
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes">txtShopName_</xsl:text>
            <xsl:value-of select="ShopUID"/>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="ShopName"/>
          </xsl:attribute>
        </input>
        <input name="ShopAddress1" type="hidden">
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes">txtShopAddress1_</xsl:text>
            <xsl:value-of select="ShopUID"/>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="ShopAddr1"/>
          </xsl:attribute>
        </input>
        <input name="ShopAddress2" type="hidden">
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes">txtShopAddress2_</xsl:text>
            <xsl:value-of select="ShopUID"/>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="ShopAddr2"/>
          </xsl:attribute>
        </input>
        <input name="ShopCity" type="hidden">
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes">txtShopCity_</xsl:text>
            <xsl:value-of select="ShopUID"/>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="ShopCity"/>
          </xsl:attribute>
        </input>
        <input name="ShopState" type="hidden">
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes">txtShopState_</xsl:text>
            <xsl:value-of select="ShopUID"/>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="ShopStateCd"/>
          </xsl:attribute>
        </input>
        <input name="ShopZip" type="hidden">
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes">txtShopZip_</xsl:text>
            <xsl:value-of select="ShopUID"/>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="ShopZipCode"/>
          </xsl:attribute>
        </input>
        <input name="ShopPhone" type="hidden">
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes">txtShopPhone_</xsl:text>
            <xsl:value-of select="ShopUID"/>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="ShopTelephoneNo"/>
          </xsl:attribute>
        </input>
        <input name="ShopFax" type="hidden">
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes">txtShopFax_</xsl:text>
            <xsl:value-of select="ShopUID"/>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="ShopFaxNo"/>
          </xsl:attribute>
        </input>
        <!--<input name="ShopEmail" type="hidden">
          <xsl:attribute name="id">
            <xsl:text disable-output-escaping="yes">txtShopEmail_</xsl:text>
            <xsl:value-of select="ShopUID"/>
          </xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="ShopEmailAddress"/>
          </xsl:attribute>
        </input>-->
      </td>
    </tr>

  </xsl:template>

</xsl:stylesheet>
