<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
         
  function ConvertTime(time)
  {
    var lsTime = time;
  	if (lsTime == null || lsTime == "") return "";
    var lsMO;
    var lsDD;
    var lsYYYY;
    var lsHH;
    var lsMM;
    var lsAmPm = 'AM';

    lsMO = lsTime.substr(5,2);
    lsDD = lsTime.substr(8,2);
    lsYYYY = lsTime.substr(0,4);

    var strDate = lsTime.substr( 5, 2 ) + "-"
                 + lsTime.substr( 8, 2 ) + "-"
                 + lsTime.substr( 0, 4 ) + " "

    if (lsTime.length <= 4)
    {
      lsHH = lsTime.substr(0,2);
      lsMM = lsTime.substr(2,2);
    }
    else if (lsTime.length > 4)
    {
      lsHH = lsTime.substr(11,2);
      lsMM = lsTime.substr(14,2);
    }
    
    if (lsHH >= 1 && lsHH < 10)
    {
      lsHH = lsHH.substr(1,1);
    }
    else if (lsHH == 12)
    {
      lsAmPm = 'PM';
    }
    else if (lsHH > 12)
    {
      lsHH = lsHH-12;
      lsAmPm = 'PM';
    }

    var lsRetTime = strDate + ' ' + lsHH + ':' + lsMM  + ' ' + lsAmPm;
    return lsRetTime;
  }

  ]]>
</msxsl:script>

<xsl:variable name="documentCount">
  <xsl:value-of select="count(Root/Document)" /> 
</xsl:variable>

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>


<xsl:template match="/">
      <tr>
      	<td colspan="5">

      		<TABLE width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="#EEEEEE">
            <thead>
      				<TR>
      					<TD class="theader">Date</TD>
      					<TD class="theader">Received/Sent</TD>
      					<TD class="theader">Source</TD>
      					<TD class="theader">Type</TD>
      					<TD class="theader">Image</TD>
      				</TR>
            </thead>
      
             <xsl:apply-templates/>
      
            </TABLE>

        </td>
      </tr>

</xsl:template>

  <xsl:template match="Document">

    <xsl:if test="@ImageLocation != ''">

        <tr>
          <xsl:attribute name="bgcolor">
            <xsl:choose>
              <xsl:when test="position() mod 2 = 1">#FFFFFF</xsl:when>
              <xsl:when test="position() mod 2 = 0">#DFEFFF</xsl:when>
            </xsl:choose>
          </xsl:attribute>
          <td align="center">
            <xsl:value-of select="user:ConvertTime(string(@ImageDate))"/>
          </td>
          <td align="center">
        	  <xsl:value-of select="@Directional"/>
          </td>
          <td align="center">
        	  <xsl:value-of select="@DocumentSource"/>
          </td>
          <td align="center">
            <xsl:value-of select="@DocumentType"/><xsl:if test="@DocumentType = 'Supplement'">&#160;#&#160;<xsl:value-of select="@SupplementSeqNumber"/></xsl:if>
          </td>
          <td align="center">
            <xsl:variable name="ImageType">
              <xsl:value-of select="translate(substring-after(@ImageLocation, '.'), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
            </xsl:variable>
        		<a title="Click to view document/image">
          		<xsl:attribute name="href">
                JavaScript:goDocument("<xsl:value-of select="@ImageLocation"/>","<xsl:value-of select="@Archived"/>")
              </xsl:attribute>
              <xsl:choose>
                <xsl:when test="$ImageType = 'xml'">
                  <img src="images/icon_xml.gif" border="0" width="28" height="26"/>
                </xsl:when>
                <xsl:when test="$ImageType = 'jpg'">
                  <img src="images/icon_jpg.gif" border="0" width="28" height="26"/>
                </xsl:when>
                <xsl:when test="$ImageType = 'tif'">
                  <img src="images/icon_tif.gif" border="0" width="28" height="26"/>
                </xsl:when>
                <xsl:when test="$ImageType = 'gif'">
                  <img src="images/icon_gif.gif" border="0" width="28" height="26"/>
                </xsl:when>
                <xsl:when test="$ImageType = 'bmp'">
                  <img src="images/icon_bmp.gif" border="0" width="28" height="26"/>
                </xsl:when>
                <xsl:when test="$ImageType = 'pdf'">
                  <img src="images/icon_pdf.gif" border="0" width="26" height="26"/>
                </xsl:when>
                <xsl:when test="$ImageType = 'doc'">
                  <img src="images/icon_doc.gif" border="0" width="28" height="26"/>
                </xsl:when>
                <xsl:otherwise>
                  <img src="images/icon_any.gif" border="0" width="21" height="26"/>
                </xsl:otherwise>
              </xsl:choose>
        		</a>
          </td>
        </tr>

    </xsl:if>	

  </xsl:template>

</xsl:stylesheet>