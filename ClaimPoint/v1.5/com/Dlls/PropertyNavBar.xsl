<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />
    
<xsl:param name="currentArea"  select="'Information'"/>

<xsl:template match="Property">

  <xsl:comment>v1.4.2.0</xsl:comment>

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
  <tr>
    <td>
      <img border="0" src="images/mark_property2.jpg" width="96" height="63"/>
    </td>
  </tr>
  <tr>
    <td background="images/submenu_mid_bg1.jpg">

      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
        <colgroup>
          <col width="20"/>
          <col width="76"/>
        </colgroup>
        <tr>
          <td height="22">
            <xsl:choose>
              <xsl:when test="$currentArea = 'Information'">
                <img src="images/submenu_bg_sel.gif" alt="" width="20" height="22" border="0"/>
              </xsl:when>
              <xsl:otherwise>
                <img src="images/submenu_bg_desel.gif" alt="" width="20" height="22" border="0"/>
              </xsl:otherwise>
            </xsl:choose>  
          </td>
          <td bgcolor="#E6E5F5">
            <xsl:choose>
              <xsl:when test="$currentArea = 'Information'">
                <xsl:attribute name="bgcolor">#FFFFFF</xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:attribute name="bgcolor">#E6E5F5</xsl:attribute>
              </xsl:otherwise>
            </xsl:choose>
            <a href="frmPropertyInfo.asp" class="subnav"
               title="Property Information"
               onMouseOver="window.status='Property Information'; return true"
               onMouseOut="window.status=''; return true">
            Information</a>
          </td>
        </tr>
        <tr>
          <td height="22">
            <xsl:choose>
              <xsl:when test="$currentArea = 'Involved'">
                <img src="images/submenu_bg_sel.gif" alt="" width="20" height="22" border="0"/>
              </xsl:when>
              <xsl:otherwise>
                <img src="images/submenu_bg_desel.gif" alt="" width="20" height="22" border="0"/>
              </xsl:otherwise>
            </xsl:choose>  
          </td>
          <td bgcolor="#E6E5F5">
            <xsl:if test="$currentArea = 'Involved'">
              <xsl:attribute name="bgcolor">#FFFFFF</xsl:attribute>
            </xsl:if>
            <xsl:choose>
              <xsl:when test="count(Involved[@InvolvedID != '0']) &gt; 0">
                <xsl:variable name="involvedList">
                  <xsl:call-template name="commaDelimitList">
                    <xsl:with-param name="list" select="Involved/@InvolvedID"/>
                  </xsl:call-template>
                </xsl:variable>
                <a class="subNav"
                   title="Involved Information"
                   onMouseOver="window.status='Involved Information'; return true"
                   onMouseOut="window.status=''; return true">
                  <xsl:attribute name="href">
                    frmPropertyInvolved.asp?I=<xsl:value-of select="$involvedList"/>
                  </xsl:attribute>
                  Involved (<xsl:value-of select="count(Involved)"/>)</a>
              </xsl:when>
              <xsl:otherwise>
                <p class="noLink">Involved</p>
              </xsl:otherwise>
            </xsl:choose>  
          </td>
        </tr>
        <xsl:if test="../@ContextSupportedFlag = '1'">
        <tr>
          <td height="22">
            <xsl:choose>
              <xsl:when test="$currentArea = 'Estimates'">
                <img src="images/submenu_bg_sel.gif" alt="" width="20" height="22" border="0"/>
              </xsl:when>
              <xsl:otherwise>
                <img src="images/submenu_bg_desel.gif" alt="" width="20" height="22" border="0"/>
              </xsl:otherwise>
            </xsl:choose>  
          </td>
          <td bgcolor="#E6E5F5">
            <xsl:if test="$currentArea = 'Estimates'">
              <xsl:attribute name="bgcolor">#FFFFFF</xsl:attribute>
            </xsl:if>
            <xsl:choose>
              <xsl:when test="count(Document[@DocumentID > 0]) &gt; 0">
                <a class="subNav"
                   title="Estimates"
                   onMouseOver="window.status='Estimates'; return true"
                   onMouseOut="window.status=''; return true">
                  <xsl:attribute name="href">
                    frmPropertyDocument.asp?M=E
                  </xsl:attribute>
                  Estimates</a>
              </xsl:when>
              <xsl:otherwise>
                <p class="noLink">Estimates</p>
              </xsl:otherwise>
            </xsl:choose>  
          </td>
        </tr>
        <tr>
          <td height="22">
            <xsl:choose>
              <xsl:when test="$currentArea = 'Documents'">
                <img src="images/submenu_bg_sel.gif" alt="" width="20" height="22" border="0"/>
              </xsl:when>
              <xsl:otherwise>
                <img src="images/submenu_bg_desel.gif" alt="" width="20" height="22" border="0"/>
              </xsl:otherwise>
            </xsl:choose>  
          </td>
          <td bgcolor="#E6E5F5">
            <xsl:if test="$currentArea = 'Documents'">
              <xsl:attribute name="bgcolor">#FFFFFF</xsl:attribute>
            </xsl:if>
            <xsl:choose>
              <xsl:when test="count(Document[@DocumentID > 0]) &gt; 0">
                <a class="subNav"
                   title="Documents"
                   onMouseOver="window.status='Documents'; return true"
                   onMouseOut="window.status=''; return true">
                  <xsl:attribute name="href">
                    frmPropertyDocument.asp
                  </xsl:attribute>
                  Documents</a>
              </xsl:when>
              <xsl:otherwise>
                <p class="noLink">Documents</p>
              </xsl:otherwise>
            </xsl:choose>  
          </td>
        </tr>
        </xsl:if>
      </table>

    </td>
  </tr>
  <tr>
    <td>
      <img border="0" src="images/submenu_bg_bot.gif" width="96" height="1"/>
    </td>
  </tr>
  <tr>
    <td>
      <img border="0" src="images/spacer.gif" width="1" height="10"/>
    </td>
  </tr>
  <tr>
    <td>

      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
        <colgroup>
          <col width="10"/>
          <col width="86"/>
        </colgroup>
        <tr>
          <td></td>
          <td align="center">
            <a href="javascript:void(PrintPage())" class="subnav"
                 title="Click for a printer friendly version"
                 onMouseOver="window.status='Click for a printer friendly version'; return true"
                 onMouseOut="window.status=''; return true">
            <img src="images/icon_print.gif" width="31" height="20" border="0"/>
            <br/>Print this page</a> 
          </td>
        </tr>
      </table>

    </td>
  </tr>
</table>

</xsl:template>

<xsl:template name="commaDelimitList">
  <xsl:param name="list"/>
  <xsl:if test="$list">
    <xsl:variable name="first" select="$list[1]"/>
    <xsl:variable name="rest">
      <xsl:call-template name="commaDelimitList">
        <xsl:with-param name="list" select="$list[position()!=1]"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="concat(concat($first, ','), $rest)"/>
  </xsl:if>  
</xsl:template>

</xsl:stylesheet>
