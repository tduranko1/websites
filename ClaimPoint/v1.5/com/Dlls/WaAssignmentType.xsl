<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="CarrierRep"/>
<xsl:param name="OfficeID"/>
<xsl:param name="ClaimViewLevel"/>
<xsl:param name="ClaimViewLevel_All"/>

<xsl:template match="Root">

  <select name="AssignmentType" id="txtAssignmentTypeID" onChange="clickAssignment()" required="true">
    <option value="">Please select one</option>
    <xsl:choose>
      <xsl:when test="$CarrierRep = 0"> 
        <xsl:for-each select="AssignmentType" >
          <xsl:call-template name="getAssignmentTypes"/>
        </xsl:for-each>
      </xsl:when> 
      <xsl:otherwise>
        <xsl:for-each select="Office/OfficeAssignmentType[../@OfficeID = $OfficeID and @AssignmentTypeID > 0]" >
          <xsl:call-template name="getAssignmentTypes"/>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
  </select>

</xsl:template>

<xsl:template name="getAssignmentTypes">

  <xsl:param name="currentID"><xsl:value-of select="@AssignmentTypeID"/></xsl:param>
	<option>
		<xsl:attribute name="value">
			<xsl:value-of select="$currentID"/>
		</xsl:attribute>
		  <xsl:value-of select="/Root/Reference[@List='AssignmentType' and @ReferenceID=$currentID]/@Name"/>
	</option>

</xsl:template>

</xsl:stylesheet>
