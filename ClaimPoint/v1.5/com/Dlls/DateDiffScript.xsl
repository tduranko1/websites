<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet 	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
								xmlns:msxml="urn:schemas-microsoft-com:xslt" 
								xmlns:vbdate="urn:schemas-xmlpitstop-com:vbdate" 
								version="1.0">
	<xsl:output method="xml" media-type="text/html" encoding="UTF-8"/>
	
	<msxml:script language="VBScript" implements-prefix="vbdate">
		function VBDateDiff(strDate1, strDate2)
		
			VBDateDiff = DateDiff("d", CDate(strDate1), CDate(strDate2)) 
		end function

		'Ideas: DayOfWeek, Today, DateDiff functions, WeekDay, Monthday, etc, Today

	</msxml:script>
</xsl:stylesheet>
