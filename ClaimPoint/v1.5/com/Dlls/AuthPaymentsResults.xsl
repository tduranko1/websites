<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    xmlns:vbdate="urn:schemas-xmlpitstop-com:vbdate"
    exclude-result-prefixes="msxsl user">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />
<xsl:include href="DateDiffScript.xsl" />

<xsl:param name="UserID"/>
<xsl:param name="PaymentAuthorizationLevel"/>
<xsl:param name="TodayIs"/>

<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
         
  function ConvertDate(time)
  {
    var lsTime = time;
    if (lsTime == null || lsTime == "") return "";
    var lsDelimeter = "/";

    var strDate = lsTime.substr(5,2) + lsDelimeter + lsTime.substr(8,2) + lsDelimeter + lsTime.substr(0,4);
    return strDate;
  }


  ]]>
</msxsl:script>
<xsl:template match="/Root">

  <xsl:comment>v1.4.5.0</xsl:comment>

  <DIV class="div_popup" id="DivEditClaim" style="DISPLAY: none; FILTER: progid:DXImageTransform.Microsoft.alpha(enabled=1,opacity=100)progid:DXImageTransform.Microsoft.shadow(direction=135,color=#8E8E8E,strength=3); CLIP: auto; POSITION: absolute;">
    <TABLE cellSpacing="1" cellPadding="4" border="0">
      <TR>
        <TD class="thead" nowrap="">
          <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
            <TR>
              <TD width="100%" class="thead" onMouseDown="dragStart(event, 'DivEditClaim')" style="cursor:move" title="Drag to move">
                Edit Claim
              </TD>
              <TD id="CloseEditDivTD">
                <img src="images/x.gif" onClick="CloseEditDiv()" alt="Close" width="16" height="14" border="0" style="cursor:hand;"/>
              </TD>
            </TR>
          </TABLE>
        </TD>
      </TR>
      <TR>
        <FORM action="rspUpdClaimInfo.asp" method="post" name="frmUpdClaimInfo" id="frmUpdClaimInfo" target="ifrmClaimInfoUpd">
        <TD class="div_popup_td" nowrap="">
          <SPAN style="width:94px; height:18px">LYNX ID: </SPAN><SPAN id="divLynxID" style="height:18px; font-weight:bold"></SPAN>
          <BR/>
          <SPAN style="width:94px">Claim Number: </SPAN><INPUT type="text" value="" name="divClaimNumber" id="divClaimNumber"/>
          <BR/>

          <SPAN style="width:94px">Loss Date: </SPAN>
          <input name="LossDate" id="txtLossDate" type="text" value="" AllowFutureDate="0" onfocus="setCurrLossDate('txtLossDate');" onKeyPress="return numbersOnly(this,event,0,1)" onBlur="formatLossDate()" size="10" maxlength="10" required="true"/>
          <a href="javascript:void(0)" onclick="showCalIframe(this, 'txtLossDate')" id="calCall" tabIndex="-1"><img src="images/calendar.gif" alt="" width="24" height="17" border="0" title="Click to select date from calendar"/></a>
          <BR/>

          <SPAN style="width:94px">Coverage: </SPAN>
          <select name="divCoverage" id="divCoverage">
            <option value=""></option>
          </select>
          <BR/>
          <SPAN style="width:94px">Loss State: </SPAN>
          <select name="divLossState" id="divLossState">
             <option value=""></option>
          </select>
          <BR/>

          <img src="images/spacer.gif" alt="" width="1" height="6" border="0"/><BR/>
          <DIV align="right">
            <INPUT id="CloseEditDivBtn" class="button" type="button" onClick="CloseEditDiv()" value="Cancel" style="cursor:hand;"/>
            <img src="images/spacer.gif" alt="" width="20" height="1" border="0"/>
            <INPUT class="button" type="button" onClick="SaveClaimInfo()" value="Save" style="cursor:hand;"/>
          </DIV>
          <INPUT type="hidden" value="" name="divClaimAspectID" id="divClaimAspectID"/>
        </TD>
        </FORM>
      </TR>
    </TABLE>
  </DIV>

<xsl:choose>
  <xsl:when test="count(Claim) &gt; 0">

    <div style="font-size:10pt">
      <p>
        <li>Mark (<img src="images/cb_checked.gif" width="12" height="12" border="0"/>) the claim(s) to be authorized and press the <strong>Authorize</strong> button below the table.</li>
        <li>To edit the Claim Number, Loss Date, Coverage Type and/or Loss State, click the <strong>Edit</strong> icon (<img src="images/edit.gif" alt="" width="14" height="14" border="0"/>).</li>
      </p>
    </div>
    <TABLE  border="0" cellspacing="1" cellpadding="2" bgcolor="#DDDDDD" id="srhResults">  <!-- style="border:1px solid #CCCCCC" -->
      <thead>
        <TR>
           <TD sortReq="No" width="30" align="center" style="background:#E7E7F7; border-bottom:1px solid #99CCFF; border-right:1px solid #99CCFF; border-left:1px solid #99CCFF; border-top:1px solid #99CCFF;">
            <img src="images/spacer.gif" width="1" height="1" border="0"/>
          </TD>
           <TD sortReq="No" align="center" style="background:#E7E7F7; border-bottom:1px solid #99CCFF; border-right:1px solid #99CCFF; border-left:1px solid #99CCFF; border-top:1px solid #99CCFF;">
            <TABLE border="0" cellspacing="0" cellpadding="0"><TR><TD>
              <img src="images/notepad.gif" width="16" height="16" border="0" title="Edit the claim"/>
            </TD></TR></TABLE>
          </TD>
           <TD sortReq="No" width="30" align="center" valign="top" style="background:#E7E7F7; border-bottom:1px solid #99CCFF; border-right:1px solid #99CCFF; border-left:1px solid #99CCFF; border-top:1px solid #99CCFF;">
            <strong>Docs</strong>
          </TD>
           <TD sortReq="No" align="center" style="font-weight:bold; background:#E7E7F7; border-bottom:1px solid #99CCFF; border-right:1px solid #99CCFF; border-left:1px solid #99CCFF; border-top:1px solid #99CCFF;">
            <strong>&#xa0;&#xa0;&#xa0;Submit<br/>Comments</strong>
          </TD>
          <TD class="theader" title="Click to sort ascending/descending" nowrap=""><span>Request<br/>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;Date</span></TD>
          <TD class="theader" title="Click to sort ascending/descending" nowrap=""><img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>LYNX&#xa0;ID</TD>
          <TD class="theader" title="Click to sort ascending/descending" nowrap=""><span>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;Claim&#xa0;#</span></TD>
          <TD class="theader" title="Click to sort ascending/descending" nowrap=""><span>Loss<br/>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;Date</span></TD>
          <TD class="theader" title="Click to sort ascending/descending" nowrap=""><span>Loss<br/>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;State</span></TD>
          <TD class="theader" title="Click to sort ascending/descending" nowrap=""><span>Coverage<br/>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;Type</span></TD>
          <TD class="theader" title="Click to sort ascending/descending" nowrap=""><span>Fee<br/>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;Code</span></TD>
          <TD class="theader" title="Click to sort ascending/descending" nowrap=""><span>Indemnity<br/>&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;&#xa0;Amount</span></TD>
          <TD class="theader" title="Click to sort ascending/descending" nowrap=""><span>&#xa0;&#xa0;&#xa0;&#xa0;Rental</span></TD>
        </TR>
      </thead>
      <tbody id="srhList">
        <xsl:apply-templates select="Claim">
          <!--           
          <xsl:sort select="@AuthorizationRequestDate" order="ascending"/>
          <xsl:sort select="@LynxID" order="ascending"/>
          -->
        </xsl:apply-templates>
      </tbody>
    </TABLE>
    <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
      <TR>
        <td align="right">
        <a href="javascript:AuthorizeClaims()"
            title="Authorize selected claims"
            onMouseOver="window.status='Authorize selected claims'; return true"
            onMouseOut="window.status=''; return true">
          <img border="0" src="images/btn_authorize.gif" WIDTH="83" HEIGHT="31" />
        </a>
        <img src="images/spacer.gif" alt="" width="40" height="1" border="0"/>
        </td>
        </TR>
      </TABLE>

    <form id="frmClaimDetailUpd" name="frmClaimDetailUpd" method="post" target="IFrmClaimDetailUpd" action="rspClaimDetailUpd.asp">
      <input type="hidden" id="hidClaimAspectID" name="ClaimAspectID" value=""/>
      <input type="hidden" id="hidClientClaimNumber" name="ClientClaimNumber" value=""/>
      <input type="hidden" id="hidLossDate" name="LossDate" value=""/>
      <input type="hidden" id="hidLossState" name="LossState" value=""/>
      <input type="hidden" id="hidClientCoverageTypeID" name="ClientCoverageTypeID" value=""/>
      <input type="hidden" id="hidUserID" name="UserID">
        <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
      </input>
    </form>

    <form id="frmAuthorizeClaims" name="frmAuthorizeClaims" method="post" target="IFrmAuthorizeClaims" action="rspAuthorizeClaims.asp">
      <input type="hidden" id="hidAuthorizedClaimAspectIDs" name="AuthorizedClaimAspectIDs" value=""/>
       <input type="hidden" id="hidUserID" name="UserID">
        <xsl:attribute name="value"><xsl:value-of select="$UserID"/></xsl:attribute>
      </input>
    </form>

    <iframe src="blank.asp" id="IFrmClaimDetailUpd" name="IFrmClaimDetailUpd" style="height:0px; width:0px; display:none;" frameBorder="0" />

    <iframe src="blank.asp" id="IFrmAuthorizeClaims" name="IFrmAuthorizeClaims" style="height:0px; width:0px; display:none;" frameBorder="1" />

  </xsl:when>
  <xsl:otherwise>
    <table id="srhResultsNot">
      <tr>
        <td height="100" nowrap="">
          <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
        </td>
        <td nowrap="" style="color:#FF0000; font-size:11pt; font-weight:bold;">
          There are no pending claims to be authorized.
        </td>
      </tr>
    </table>
  </xsl:otherwise>

</xsl:choose>

</xsl:template>

<xsl:template match="Claim">

        <tr onMouseOver="GridMouseOver(this,0)" onMouseOut="GridMouseOut(this)" ActOnClick="false" unselectable="on">
           <xsl:attribute name="id">
             <xsl:value-of select="@LynxID"/>
          </xsl:attribute>

           <xsl:attribute name="ClaimAspectID">
             <xsl:value-of select="@ClaimAspectID"/>
          </xsl:attribute>

           <xsl:attribute name="party">
             <xsl:value-of select="@ExposureCD"/>
           </xsl:attribute>

           <xsl:if test="(number(@IndemnityAmount) &gt; $PaymentAuthorizationLevel)">
            <xsl:attribute name="bgcolor">#f5f5dc</xsl:attribute>
            <xsl:attribute name="KeepBGColor">true</xsl:attribute>
            <xsl:attribute name="Title">Authorization Amount Exceeded</xsl:attribute>
           </xsl:if>

          <td align="center">
            <input type="checkbox" value="" title="Mark the cLaim to authorize payment" >
              <xsl:attribute name="id">cb_<xsl:value-of select="@ClaimAspectID"/></xsl:attribute>
              <xsl:if test="(number(@IndemnityAmount) &gt; $PaymentAuthorizationLevel)">
                <xsl:attribute name="disabled">true</xsl:attribute>
              </xsl:if>
            </input>
          </td>

          <td align="center">
            <a onClick="EditClaim(this)" style="cursor:hand;">
              <img src="images/edit.gif" alt="" width="14" height="14" border="0" title="Click to edit the Claim Number, Loss Date, Coverage Type and/or Loss State"/>
            </a>
          </td>

          <td align="center" valign="middle" nowrap="">
            <a style="cursor:hand;">
              <xsl:attribute name="onclick">javascript:showDocs("<xsl:value-of select='@LynxID'/>", "<xsl:value-of select='@ClaimAspectID'/>")</xsl:attribute>
              <img src="images/openfoldericon.gif" alt="" width="16" height="16" border="0" title="Click to view documents pertaining to this vehicle."/>
            </a>
          </td>

          <td align="center" valign="middle" nowrap="">
            <a style="cursor:hand;">
              <xsl:attribute name="onclick">javascript:sendComment("<xsl:value-of select='@LynxID'/>", "<xsl:value-of select='@ClientClaimNumber'/>", "<xsl:value-of select="user:ConvertDate(string(@LossDate))"/>")</xsl:attribute>
              <img src="images/doc_single.gif" alt="" width="12" height="14" border="0" title="Click to send a comment to the LYNX representative."/>
            </a>
          </td>

          <td align="center" valign="middle" nowrap="">
            <xsl:variable name="AuthorizationRequestDate">
               <xsl:value-of select="user:ConvertDate(string(@AuthorizationRequestDate))"/>
            </xsl:variable>

             <xsl:if test="vbdate:VBDateDiff(string($AuthorizationRequestDate), string($TodayIs)) &gt; 1">
              <img src="images/action_req.gif" alt="" width="14" height="14" border="0" title="Overdue"/>
              <img src="images/spacer.gif" alt="" width="4" height="1" border="0"/>
            </xsl:if>

            <xsl:choose>
              <xsl:when test="string-length(@AuthorizationRequestDate) = 0">
                &#xa0;
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$AuthorizationRequestDate"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>

          <td align="center" valign="middle" nowrap="">
            <xsl:choose>
              <xsl:when test="string-length(@LynxID) = 0">
                &#xa0;
              </xsl:when>
              <xsl:otherwise>
               <xsl:if test="@DataChanged = 1">
                <img src="images/reviewed.gif" alt="" width="16" height="15" border="0" title="Payment information has been updated."/>
                <img src="images/spacer.gif" alt="" width="4" height="1" border="0"/>
              </xsl:if>
                <xsl:value-of select="@LynxID"/> - <xsl:value-of select="@VehicleNumber"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>
          
          <td align="center" valign="middle" nowrap="">
            <xsl:choose>
              <xsl:when test="string-length(@ClientClaimNumber) = 0">
                &#xa0;
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@ClientClaimNumber"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>
          
          <td align="center" valign="middle" nowrap="">
            <xsl:choose>
              <xsl:when test="string-length(@LossDate) = 0">
                &#xa0;
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="user:ConvertDate(string(@LossDate))"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>
          
          <td align="center" valign="middle" nowrap="">
            <xsl:choose>
              <xsl:when test="string-length(@LossState) = 0">
                &#xa0;
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@LossState"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>
          
          <td align="center" valign="middle">
            <xsl:choose>
              <xsl:when test="string-length(@ClientCoverageTypeID) = 0">
                &#xa0;
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="/Root/Reference[@List='ClientCoverageType' and @ReferenceID=current()/@ClientCoverageTypeID]/@Name"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>

          <td align="center" valign="middle" nowrap="">
            <xsl:choose>
              <xsl:when test="string-length(@FeeCode) = 0">
                &#xa0;
              </xsl:when>
              <xsl:otherwise>
               <xsl:attribute name="Title"><xsl:value-of select="@FeeName"/></xsl:attribute>
               <xsl:attribute name="Style">cursor:help</xsl:attribute>
                <xsl:value-of select="@FeeCode"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>

          <td align="right" valign="middle" nowrap="">
            <xsl:choose>
              <xsl:when test="string-length(@IndemnityAmount) = 0">
                &#xa0;
              </xsl:when>
              <xsl:otherwise>
                $<xsl:value-of select="@IndemnityAmount"/>
                <img src="images/spacer.gif" alt="" width="6" height="1" border="0"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>

          <td align="center" valign="middle" nowrap="">
            <xsl:choose>
              <xsl:when test="@RentalFlag = 1">Yes</xsl:when>
              <xsl:otherwise>No</xsl:otherwise>
            </xsl:choose>
          </td>
          
          <td style="display:none">
            <xsl:value-of select="@ClientCoverageTypeID"/>
          </td>
         </tr>

</xsl:template>

</xsl:stylesheet>