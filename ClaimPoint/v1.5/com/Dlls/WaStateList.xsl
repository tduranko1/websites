<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

  <xsl:param name="tabIndexNo"/>
  <xsl:param name="ObjName"/>
  
  <xsl:template match="Root">
  
    <select required="true">
  		<xsl:attribute name="name"><xsl:value-of select="$ObjName"/></xsl:attribute>
  		<xsl:attribute name="id">txt<xsl:value-of select="$ObjName"/></xsl:attribute>
  		<xsl:if test="$tabIndexNo != ''">
        <xsl:attribute name="tabIndex">
    			<xsl:value-of select="$tabIndexNo"/>
    		</xsl:attribute>
  		</xsl:if>
      <option value=""></option>
  
      <xsl:for-each select="Reference[@List = 'ContractState']" >
        <xsl:call-template name="getAllStates"/>
      </xsl:for-each>

    </select>
  
  </xsl:template>
  
  <xsl:template name="getAllStates">
  
  	<option>
  		<xsl:attribute name="value">
  			<xsl:value-of select="@ReferenceID"/>
  		</xsl:attribute>
  		  <xsl:value-of select="@Name"/>
  	</option>
  
  </xsl:template>

</xsl:stylesheet>

