<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="WitnessCount"/>
<xsl:param name="currentArea" select="'Information'"/>
<xsl:param name="LynxID"/>
<xsl:param name="AssignmentType"/>
<xsl:param name="AspectID"/>
<xsl:param name="VehNumber"/>

<xsl:template match="Claim">

  <xsl:comment>v1.5.0.0</xsl:comment>

  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
    <tr>
      <td>
        <img border="0" src="images/mark_claim2.jpg" width="96" height="63"/>
      </td>
    </tr>
    <tr>
      <td>

        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
          <colgroup>
            <col width="20"/>
            <col width="76"/>
          </colgroup>
          <tr>
            <td height="22">
              <xsl:choose>
                <xsl:when test="$currentArea = 'Information'">
                  <img src="images/submenu_bg_sel.gif" alt="" width="20" height="22" border="0"/>
                </xsl:when>
                <xsl:otherwise>
                  <img src="images/submenu_bg_desel.gif" alt="" width="20" height="22" border="0"/>
                </xsl:otherwise>
              </xsl:choose>  
            </td>
            <td bgcolor="#E6E5F5">
              <xsl:choose>
                <xsl:when test="$currentArea = 'Information'">
                  <xsl:attribute name="bgcolor">#FFFFFF</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="bgcolor">#E6E5F5</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <a class="subnav"
                   title="Claim Information"
                   onMouseOver="window.status='Claim Information'; return true"
                   onMouseOut="window.status=''; return true">
                <xsl:attribute name="href">
                  frmClaimInfo.asp?LynxID=<xsl:value-of select="$LynxID"/>&amp;AspectID=<xsl:value-of select="$AspectID"/>&amp;AssignmentType=<xsl:value-of select="$AssignmentType"/>&amp;VehNumber=<xsl:value-of select="$VehNumber"/>
                </xsl:attribute>
              Information</a>
            </td>
          </tr>
          <tr>
            <td height="22">
              <xsl:choose>
                <xsl:when test="$currentArea = 'History'">
                  <img src="images/submenu_bg_sel.gif" alt="" width="20" height="22" border="0"/>
                </xsl:when>
                <xsl:otherwise>
                  <img src="images/submenu_bg_desel.gif" alt="" width="20" height="22" border="0"/>
                </xsl:otherwise>
              </xsl:choose>  
            </td>
            <td bgcolor="#E6E5F5">
              <xsl:choose>
                <xsl:when test="$currentArea = 'History'">
                  <xsl:attribute name="bgcolor">#FFFFFF</xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:attribute name="bgcolor">#E6E5F5</xsl:attribute>
                </xsl:otherwise>
              </xsl:choose>
              <a class="subnav"
                 title="Claim History"
                 onMouseOver="window.status='Claim History'; return true"
                 onMouseOut="window.status=''; return true">
                <xsl:attribute name="href">
                  frmClaimHistory.asp?LynxID=<xsl:value-of select="$LynxID"/>&amp;AspectID=<xsl:value-of select="$AspectID"/>&amp;AssignmentType=<xsl:value-of select="$AssignmentType"/>&amp;VehNumber=<xsl:value-of select="$VehNumber"/>
                </xsl:attribute>
              History</a>
            </td>
          </tr>
        </table>

      </td>
    </tr>
    <tr>
      <td>
        <img border="0" src="images/submenu_bg_bot.gif" width="96" height="1"/>
      </td>
    </tr>
    <tr>
      <td>
        <img border="0" src="images/spacer.gif" width="1" height="10"/>
      </td>
    </tr>
    <tr>
      <td>
              <a href="Javascript:self.print()" class="subnav" title="Click to print this page.">
              <img src="images/icon_print.gif" align="top" width="31" height="20" border="0"/>Print</a>
      </td>
    </tr>
    <tr>
      <td>
        <img border="0" src="images/spacer.gif" width="1" height="6"/>
      </td>
    </tr>
    <tr>
      <td>
        <a class="subnav" title="Click to send a comment to LYNX's rep">
          <xsl:attribute name="href">
            frmSubmitComment.asp?LynxID=<xsl:value-of select="$LynxID"/>&amp;AspectID=<xsl:value-of select="$AspectID"/>&amp;AssignmentType=<xsl:value-of select="$AssignmentType"/>&amp;StatusID=<xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@StatusID"/>&amp;VehNumber=<xsl:value-of select="$VehNumber"/>
          </xsl:attribute>
        <img src="images/icon_mail.gif" align="top" width="31" height="20" border="0"/>Comment</a>
      </td>
    </tr>
  </table>

</xsl:template>

<xsl:template name="commaDelimitList">
  <xsl:param name="list"/>
  <xsl:if test="$list">
    <xsl:variable name="first" select="$list[1]"/>
    <xsl:variable name="rest">
      <xsl:call-template name="commaDelimitList">
        <xsl:with-param name="list" select="$list[position()!=1]"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:value-of select="concat(concat($first, ','), $rest)"/>
  </xsl:if>  
</xsl:template>

</xsl:stylesheet>
