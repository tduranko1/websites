<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="Root">

    <TR>
      <TD><p class="bodyBlue">APD Version:</p></TD>
      <TD>
        <xsl:for-each select="Config" >
          <xsl:value-of select="@version"/>
        </xsl:for-each>
      </TD>
    </TR>
    <TR>
      <TD><p class="bodyBlue">Database Version:</p></TD>
      <TD>
        <xsl:for-each select="Database" >
          <xsl:value-of select="@version"/>
        </xsl:for-each>
      </TD>
    </TR>

</xsl:template>

</xsl:stylesheet>
