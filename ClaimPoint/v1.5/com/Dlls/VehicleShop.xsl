<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
         
  function ConvertDate(time)
  {
    var lsTime = time;
    var lsMO;
    var lsDD;
    var lsYYYY;

    lsMO = lsTime.substr(5,2);
    lsDD = lsTime.substr(8,2);
    lsYYYY = lsTime.substr(0,4);

    var strDate = lsTime.substr( 5, 2 ) + "-"
                 + lsTime.substr( 8, 2 ) + "-"
                 + lsTime.substr( 0, 4 ) + " "

    return strDate;
  }

  ]]>
</msxsl:script>

  <xsl:template match="Vehicle">
  
  <xsl:comment>v1.5.0.2</xsl:comment>

  <xsl:for-each select="ClaimAspectServiceChannel">

    <xsl:for-each select="Assignment">

    <tr>
      <td colspan="5">
        <p class="headerRedData"><xsl:value-of select="../@ServiceChannelName"/>:</p>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Shop Name:
      </td>
      <td class="body">
        <xsl:value-of select="@ShopLocationName"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Contact:
      </td>
      <td class="body">
        <xsl:value-of select="@ShopLocationContactName"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" valign="top" rowspan="2">
        Address:
      </td>
      <td class="body" valign="top" rowspan="2">
        <xsl:value-of select="@ShopLocationAddressLine1"/><br/>
        <xsl:if test="@ShopLocationAddressLine2 != ''">
          <xsl:value-of select="@ShopLocationAddressLine2"/><br/>
        </xsl:if>
        <xsl:value-of select="@ShopLocationAddressCity"/>
        <xsl:if test="string-length(translate(normalize-space(@ShopLocationAddressCity),' ','')) &gt; 0 and string-length(translate(normalize-space(@ShopLocationAddressState),' ',''))">
          <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:value-of select="concat(@ShopLocationAddressState, ' ')"/><xsl:value-of select="@ShopLocationAddressZip"/>
      </td>
      <td rowspan="2"></td>
      <td class="bodyBlue">
        Phone:
      </td>
      <td class="body">
        <xsl:value-of select="@ShopLocationPhoneAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(@ShopLocationPhoneAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@ShopLocationPhoneExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="@ShopLocationPhoneExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(@ShopLocationPhoneExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@ShopLocationPhoneUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="@ShopLocationPhoneUnitNumber"/>
        <xsl:if test="string-length(@ShopLocationPhoneExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', @ShopLocationPhoneExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" valign="top">
        Fax:
      </td>
      <td class="body" valign="top">
        <xsl:value-of select="@ShopLocationFaxAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(@ShopLocationFaxAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@ShopLocationFaxExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="@ShopLocationFaxExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(@ShopLocationFaxExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@ShopLocationFaxUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="@ShopLocationFaxUnitNumber"/>
        <xsl:if test="string-length(@ShopLocationFaxExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', @ShopLocationFaxExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" valign="top">
        Remarks to<br/>the Shop:
      </td>
      <td colspan="4" class="body" valign="top">
        <xsl:value-of select="@AssignmentRemarks"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Assignment Date:
      </td>
      <td class="body">
        <xsl:if test="@AssignmentDate != ''">
          <xsl:value-of select="user:ConvertDate(string(@AssignmentDate))"/>
        </xsl:if>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Previous Assignment Date:
      </td>
      <td class="body">
        <xsl:if test="@PrevAssignmentDate != ''">
          <xsl:value-of select="user:ConvertDate(string(@PrevAssignmentDate))"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Work Status:
      </td>
      <td class="body">
        <xsl:value-of select="../@StatusName"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Effective Deductible Sent:
      </td>
      <td class="body">
        <xsl:value-of select="@EffectiveDeductibleSentAmt"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Electronic Status:
      </td>
      <td class="body">
        <xsl:value-of select="@VANAssignmentStatusName"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Fax Status:
      </td>
      <td class="body">
        <xsl:value-of select="@FaxAssignmentStatusName"/>
      </td>
    </tr>
    <tr>
      <td colspan="5">&#160;</td>
    </tr>
    <tr>
      <td class="bodyBlue" valign="top">
        Coverages:
      </td>
      <td colspan="4">
        <table width="100%" border="0" cellspacing="1" cellpadding="4" bgcolor="#d0d0d0">
          <thead>
            <TR>
              <TD style="text-align:center; padding:2px; background:#FFFFFF;">Coverage</TD>
              <TD style="text-align:center; padding:2px; background:#FFFFFF;" nowrap="">Ded. Applied</TD>
              <TD style="text-align:center; padding:2px; background:#FFFFFF;" nowrap="">Overage/Limit</TD>
              <TD style="text-align:center; padding:2px; background:#FFFFFF;">Partial</TD>
            </TR>
          </thead>
    
          <xsl:apply-templates select="../Coverage" />

        </table>
      </td>
    </tr>

    <tr>
      <td class="bodyBlue" valign="top">
        Current Estimate:
      </td>
      <td colspan="4">
        <table width="100%" border="0" cellspacing="1" cellpadding="4" bgcolor="#d0d0d0">
          <thead>
            <TR>
              <TD style="text-align:center; padding:2px; background:#FFFFFF;" nowrap="">Gross Amt.</TD>
              <TD style="text-align:center; padding:2px; background:#FFFFFF;" nowrap="">Net Amt.</TD>
              <TD style="text-align:center; padding:2px; background:#FFFFFF;" nowrap="">Ded. Applied</TD>
              <TD style="text-align:center; padding:2px; background:#FFFFFF;" nowrap="">Limits Applied</TD>
            </TR>
          </thead>
          <tr bgcolor="#FFFFFF">
            <td>
              <xsl:value-of select="@CurEstGrossRepairTotal"/>
            </td>
            <td align="center">
              <xsl:value-of select="@CurEstNetRepairTotal"/>
            </td>
            <td align="center">
              <xsl:value-of select="@CurEstDeductiblesApplied"/>
            </td>
            <td align="center">
              <xsl:value-of select="@CurEstLimitsEffect"/>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Disposition:
      </td>
      <td class="body">
        <xsl:variable name="DispositionTypeCD"><xsl:value-of select="../@DispositionTypeCD"/></xsl:variable>
        <xsl:value-of select="/Root/Reference[@List='DispositionTypeCD' and @ReferenceID=$DispositionTypeCD]/@Name"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue"></td>
      <td class="body"></td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Inspection Date:
      </td>
      <td class="body">
        <xsl:if test="../@InspectionDate != ''">
          <xsl:value-of select="user:ConvertDate(string(../@InspectionDate))"/>
        </xsl:if>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Original Estimate Date:
      </td>
      <td class="body">
        <xsl:if test="../@OriginalEstimateDate != ''">
          <xsl:value-of select="user:ConvertDate(string(../@OriginalEstimateDate))"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Repair Start Date:
      </td>
      <td class="body">
        <xsl:if test="../@WorkStartDate != ''">
          <xsl:value-of select="user:ConvertDate(string(../@WorkStartDate))"/>
        </xsl:if>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Repair End Date:
      </td>
      <td class="body">
        <xsl:if test="../@WorkEndDate != ''">
          <xsl:value-of select="user:ConvertDate(string(../@WorkEndDate))"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Final Estimate Received Date:
      </td>
      <td class="body">
        <xsl:if test="../@FinalEstDate != ''">
          <xsl:value-of select="user:ConvertDate(string(../@FinalEstDate))"/>
        </xsl:if>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Cash Out Date:
      </td>
      <td class="body">
        <xsl:if test="../@CashOutDate != ''">
          <xsl:value-of select="user:ConvertDate(string(../@CashOutDate))"/>
        </xsl:if>
      </td>
    </tr>

  <tr>
    <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
  </tr>

    </xsl:for-each>
  
  </xsl:for-each>

  </xsl:template>

        <xsl:template match="Coverage">
  
          <tr bgcolor="#FFFFFF">
            <td>
              <xsl:value-of select="@CoverageTypeCD"/>
            </td>
            <td align="center">
              <xsl:value-of select="@DeductibleAmt"/> / <xsl:value-of select="@DeductibleAppliedAmt"/>
            </td>
            <td align="center">
              <xsl:value-of select="@LimitAmt"/> / <xsl:value-of select="@LimitAppliedAmt"/>
            </td>
            <td align="center">
              
            </td>
          </tr>

        </xsl:template>

</xsl:stylesheet>