<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="pageTitle"/>
<xsl:param name="currentProperty"/>
<xsl:param name="ClientOfficeId"/>

<xsl:variable name="propertyCount">
	<xsl:value-of select="count(Root/Claim/Property)"/>
</xsl:variable>
					
<xsl:template match="Root">
	<xsl:apply-templates select="Claim"/>
</xsl:template>

  <xsl:template match="Claim">
  
  	<tr>
  		<td height="20" colspan="5" style="border-top: 1px solid #000099; border-bottom: 1px solid #000099; background: #E6E5F5;">
  
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="bodyBlue" nowrap="">
              <xsl:value-of select="$pageTitle"/>
              <img src="images/spacer.gif" width="20" height="1" border="0"/>
        		</td>
            <td>
              <xsl:choose>
                <xsl:when test="$propertyCount > 1">
                  <select name="cboProperty" id="cboProperty" size="1" style="font-family: Arial; font-size: 9pt; font-weight: bold">
                    <xsl:apply-templates select="Property"/>
                  </select>
                </xsl:when>
                <xsl:otherwise>
                  <img src="images/spacer.gif" style="height:1px;width:100px"/>
                </xsl:otherwise>
              </xsl:choose>
        		</td>
            <td valign="center" align="left"> 
              <xsl:choose>
                <xsl:when test="$propertyCount > 1">
                  <a border="0" href="JavaScript:goProperty()"><img border="0" src="images/btn_view_new.gif" alt="View" wi1dth="41" h1eight="17"  align="absmiddle"/></a>
                </xsl:when>
                <xsl:otherwise>
                <img src="images/spacer.gif" style="height:21px;width:50px"/>
                </xsl:otherwise>
              </xsl:choose>
      			</td>
          </tr>
        </table>
  
      </td>
  	</tr>

  </xsl:template>

  <xsl:template match="Property">
  
  	<option>
  		<xsl:attribute name="value">
  			<xsl:value-of select="@ClaimAspectID"/>
  		</xsl:attribute>
  		<xsl:if test="@ClaimAspectID = $currentProperty">
  			<xsl:attribute name="SELECTED"/>
  		</xsl:if>
  		Property <xsl:value-of select="@PropertyNumber"/>
  	</option>
  
  </xsl:template>

</xsl:stylesheet>
