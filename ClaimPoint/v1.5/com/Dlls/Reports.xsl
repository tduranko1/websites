<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" indent="yes" encoding="UTF-8" />

<xsl:template match="/Root">

  <xsl:comment>v1.4.2.0</xsl:comment>

  <script language="javascript">
    <![CDATA[
        function getCriteria(sReportID){
          try {
            var iFrm;
            iFrm = document.getElementById("ifrmReportCriteria");
            if (iFrm) {
              iFrm.style.visibility = "hidden";
            }
            showAnimation('show');
            if (sReportID != "") {
              document.getElementById("reportID").value = sReportID;
              document.frmReportCriteria.submit();
            }
          } catch (e) {}
        }
        
        function criteriaReady(){
          try {
            var iFrm;
            iFrm = document.getElementById("ifrmReportCriteria");
            iFrm.style.visibility = "visible";
          } catch (e) {}
        }
        
        function resizeRptIFrame(iHeight) {
          try {
            var iFrm;
            iFrm = document.getElementById("ifrmReportCriteria");
            iFrm.style.height = iHeight;
          } catch (e) {}
        }

        function showAnimation(state)
        {
          if (state == "hide")
            gifState = "none";
          else
            gifState = "inline";
          
          if (document.getElementById('animateGif'))
            document.getElementById('animateGif').style.display = gifState;
        }

    ]]>
  </script>

<xsl:choose>
  <xsl:when test="count(Report) &gt; 0">

  <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
      <colgroup>
        <col width="160px"/>
        <col width="*"/>
      </colgroup>
    <tr>
      <td class="bodyBlue">Report Name:</td>
      <td nowrap="">
        <select id="selReports" name="selReports" onchange="getCriteria(this.options[this.selectedIndex].value)">
          <option value="">&lt;Select a report&gt;</option>
          <xsl:for-each select="Report">
            <xsl:sort select="@DisplayOrder" data-type="number" order="ascending"/> 
	        <option>
            <xsl:attribute name="value"><xsl:value-of select="@ReportID"/></xsl:attribute>
            <xsl:value-of select="@ReportName"/>
          </option>
          </xsl:for-each>
        </select>

      </td>
    </tr>
    <tr id="animateGif" style="display:none;">
      <td></td>
      <td nowrap="" style="font-weight:bold; color:#008B8B;">
          Retrieving Options...
          <img src="images/loading.gif" alt="" width="78" height="7" border="0"/>
      </td>
    </tr>
  </table>

  
  <IFRAME src="blank.asp" id="ifrmReportCriteria" name="ifrmReportCriteria" style="width:98%; height:250px;border:0px solid #000000;" frameBorder="0">
  </IFRAME>
  <form id="frmReportCriteria" name="frmReportCriteria" method="post" target="ifrmReportCriteria" action="frmReportCriteria.asp">
    <input type="hidden" name="reportID" id="reportID"/>
  </form>

  </xsl:when>
  <xsl:otherwise>
    <P>There are no reports available.</P>
  </xsl:otherwise>
</xsl:choose>

</xsl:template>
</xsl:stylesheet>
