<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="Witness">

  <xsl:comment>v1.4.2.0</xsl:comment>

<tr>
  <td class="bodyBlue">
    Name:
  </td>
  <td class="body">
    <xsl:value-of select="concat(@NameTitle, ' ')"/> <xsl:value-of select="concat(@NameFirst, ' ')"/> <xsl:value-of select="@NameLast"/>
  </td>
  <td width="20">&#160;</td>
  <td class="bodyBlue">
    SSN:
  </td>
  <td class="body">
    <xsl:value-of select="@FedTaxId"/>
  </td>
</tr>
<tr>
  <td class="bodyBlue">
    Address:
  </td>
  <td class="body" rowspan="3" valign="top">
    <xsl:value-of select="@Address1"/><br/>
    <xsl:if test="@Address2 != ''">
      <xsl:value-of select="@Address2"/><br/>
    </xsl:if>
    <xsl:value-of select="concat(@AddressCity, ', ')"/><xsl:value-of select="concat(@AddressState, '  ')"/><xsl:value-of select="@AddressZip"/>
  </td>
  <td width="20">&#160;</td>
  <td class="bodyBlue">
    Day Phone:
  </td>
  <td class="body">
    <xsl:value-of select="@DayAreaCode"/>
    <xsl:if test="string-length(translate(normalize-space(@DayAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@DayExchangeNumber),' ',''))">
      <xsl:text>-</xsl:text>
    </xsl:if>
    <xsl:value-of select="@DayExchangeNumber"/>
    <xsl:if test="string-length(translate(normalize-space(@DayExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@DayUnitNumber),' ',''))">
      <xsl:text>-</xsl:text>
    </xsl:if>
    <xsl:value-of select="@DayUnitNumber"/>
    <xsl:if test="string-length(@DayExtensionNumber) &gt; 0">
      <xsl:value-of select="concat(' x', @DayExtensionNumber)"/>
    </xsl:if>
  </td>
</tr>
<tr>
  <td>&#160;</td>
  <td width="20">
    <img border="0" src="images/spacer.gif" width="8" height="8"/>
  </td>
  <td class="bodyBlue">
    Night Phone:
  </td>
  <td class="body">
    <xsl:value-of select="@NightAreaCode"/>
    <xsl:if test="string-length(translate(normalize-space(@NightAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@NightExchangeNumber),' ',''))">
      <xsl:text>-</xsl:text>
    </xsl:if>
    <xsl:value-of select="@NightExchangeNumber"/>
    <xsl:if test="string-length(translate(normalize-space(@NightExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@NightUnitNumber),' ',''))">
      <xsl:text>-</xsl:text>
    </xsl:if>
    <xsl:value-of select="@NightUnitNumber"/>
    <xsl:if test="string-length(@NightExtensionNumber) &gt; 0">
      <xsl:value-of select="concat(' x', @NightExtensionNumber)"/>
    </xsl:if>
  </td>
</tr>
<tr>
  <td>&#160;</td>
  <td width="20">&#160;</td>
  <td class="bodyBlue">
    Alt. Phone:
  </td>
  <td class="body">
    <xsl:value-of select="@AlternateAreaCode"/>
    <xsl:if test="string-length(translate(normalize-space(@AlternateAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@AlternateExchangeNumber),' ',''))">
      <xsl:text>-</xsl:text>
    </xsl:if>
    <xsl:value-of select="@AlternateExchangeNumber"/>
    <xsl:if test="string-length(translate(normalize-space(@AlternateExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@AlternateUnitNumber),' ',''))">
      <xsl:text>-</xsl:text>
    </xsl:if>
    <xsl:value-of select="@AlternateUnitNumber"/>
    <xsl:if test="string-length(@AlternateExtensionNumber) &gt; 0">
      <xsl:value-of select="concat(' x', @AlternateExtensionNumber)"/>
    </xsl:if>
  </td>
</tr>
<tr>
  <td class="bodyBlue" rowspan="2" valign="top">
    Location of Witness:
  </td>
  <td class="body" rowspan="2" valign="top">
    <xsl:value-of select="@Location"/>
  </td>
  <td width="20">&#160;</td>
  <td class="bodyBlue">
    Best Phone:
  </td>
  <td class="body">
    <xsl:value-of select="@BestContactPhone"/>
  </td>
</tr>
<tr>
  <td width="20">&#160;</td>
  <td class="bodyBlue">
    Best Time:
  </td>
  <td class="body">
    <xsl:value-of select="@BestContactTime"/>
  </td>
</tr>
    
</xsl:template>

</xsl:stylesheet>