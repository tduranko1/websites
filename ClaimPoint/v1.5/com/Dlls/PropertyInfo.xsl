<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="Property">

  <xsl:comment>v1.4.2.0</xsl:comment>

  <tr>
    <td colspan="5" height="20">
      <p class="headerRedData">General:</p>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue" valign="top">
      Description:
    </td>
    <td class="body" colspan="4" valign="top">
      <xsl:value-of select="@PropertyDescription"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue" valign="top" >
      Damage:
    </td>
    <td class="body" colspan="4" valign="top">
      <xsl:value-of select="@DamageDescription"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Coverage Type:
    </td>
    <td class="body">
      <xsl:value-of select="@CoverageProfile"/>
    </td>
    <td  class="body" width="20"></td>
    <td class="bodyBlue">
      Estimated Damage:
    </td>
    <td class="body">
      <xsl:value-of select="@DamageAmt"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Name:
    </td>
    <td class="body">
      <xsl:value-of select="@Name"/>
    </td>
    <td  class="body"></td>
    <td class="bodyBlue" valign="top" >
      Address:
    </td>
    <td class="body" valign="top" rowspan="2">
      <xsl:value-of select="@LocationAddress1"/><br/>
      <xsl:if test="@LocationAddress2 != ''">
        <xsl:value-of select="@LocationAddress2"/><br/>
      </xsl:if>
      <xsl:value-of select="@LocationCity"/>
      <xsl:if test="string-length(translate(normalize-space(@LocationCity),' ','')) &gt; 0 and string-length(translate(normalize-space(@LocationState),' ',''))">
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:value-of select="concat(@LocationState, ' ')"/><xsl:value-of select="@LocationZip"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Phone:
    </td>
    <td class="body">
      <xsl:value-of select="@LocationAreaCode"/>
      <xsl:if test="string-length(translate(normalize-space(@LocationAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@LocationExchangeNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="@LocationExchangeNumber"/>
      <xsl:if test="string-length(translate(normalize-space(@LocationExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@LocationUnitNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="@LocationUnitNumber"/>
      <xsl:if test="string-length(@LocationExtensionNumber) &gt; 0">
        <xsl:value-of select="concat(' x', @LocationExtensionNumber)"/>
      </xsl:if>
    </td>
    <td width="20"></td>
    <td></td>
  </tr>
  <tr>
    <td class="bodyBlue" valign="top" >
      Remarks to Claim Rep:
    </td>
    <td class="body" colspan="4" valign="top">
      <xsl:value-of select="@Remarks"/>
    </td>
  </tr>
  <tr>
    <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
  </tr>
  <tr>
    <td colspan="5" height="20">
      <p class="headerRedData">Property Contact:</p>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Name:
    </td>
    <td class="body">
      <xsl:value-of select="concat(Contact/@NameTitle, ' ')"/> <xsl:value-of select="concat(Contact/@NameFirst, ' ')"/> <xsl:value-of select="Contact/@NameLast"/>
    </td>
    <td>&#160;</td>
    <td class="bodyBlue">
      Address:
    </td>
    <td rowspan="3" class="body" valign="top" >
      <xsl:value-of select="Contact/@Address1"/><br/>
      <xsl:if test="Contact/@Address2 != ''">
        <xsl:value-of select="Contact/@Address2"/><br/>
      </xsl:if>
      <xsl:value-of select="Contact/@AddressCity"/>
      <xsl:if test="string-length(translate(normalize-space(Contact/@AddressCity),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@AddressState),' ',''))">
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:value-of select="concat(Contact/@AddressState, ' ')"/><xsl:value-of select="Contact/@AddressZip"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Phone:
    </td>
    <td class="body">
      <xsl:value-of select="Contact/@DayAreaCode"/>
      <xsl:if test="string-length(translate(normalize-space(Contact/@DayAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@DayExchangeNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="Contact/@DayExchangeNumber"/>
      <xsl:if test="string-length(translate(normalize-space(Contact/@DayExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(Contact/@DayUnitNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="Contact/@DayUnitNumber"/>
      <xsl:if test="string-length(Contact/@DayExtensionNumber) &gt; 0">
        <xsl:value-of select="concat(' x', Contact/@DayExtensionNumber)"/>
      </xsl:if>
    </td>
    <td>&#160;</td>
    <td>&#160;</td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Relation to Insured:
    </td>
    <td class="body">
      <xsl:value-of select="/Root/Reference[@List='ContactRelationToInsured' and @ReferenceID=current()/Contact/@InsuredRelationID]/@Name"/>
    </td>
    <td>&#160;</td>
    <td>&#160;</td>
  </tr>

</xsl:template>

</xsl:stylesheet>