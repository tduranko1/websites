<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="pageTitle"/>
<xsl:param name="currentVehicle"/>
<xsl:param name="ClientOfficeId"/>

<xsl:variable name="vehicleCount">
	<xsl:value-of select="count(Root/Claim/Vehicle)"/>
</xsl:variable>
					
<xsl:template match="Root">

  <xsl:comment>v1.4.6.0</xsl:comment>

	<xsl:apply-templates select="Claim"/>
</xsl:template>

  <xsl:template match="Claim">
  
  	<tr>
  		<td height="20" colspan="5" style="border-top: 1px solid #000099; border-bottom: 1px solid #000099; background: #E6E5F5;">
  
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td class="bodyBlue" nowrap="">
              <xsl:value-of select="$pageTitle"/>
              <img src="images/spacer.gif" width="20" height="1" border="0"/>
        		</td>
            <td>
              <xsl:choose>
                <xsl:when test="$vehicleCount > 1">
        			    <select name="cboVehicle" id="cboVehicle" size="1" style="font-family: Arial; font-size: 9pt; font-weight: bold;" title="Select another vehicle and click on the VIEW button.">
        				    <xsl:apply-templates select="Vehicle"/>
        			    </select>
                </xsl:when>
                <xsl:otherwise>
                  <img src="images/spacer.gif" style="height:1px;width:100px"/>
                </xsl:otherwise>
              </xsl:choose>
        		</td>
            <td valign="center" align="left"> 
              <xsl:choose>
                <xsl:when test="$vehicleCount > 1">
                  <a border="0" href="JavaScript:goVehicle()"><img border="0" src="images/btn_view_new.gif" alt="View" wi1dth="41" h1eight="17"  align="absmiddle"/></a>
                </xsl:when>
                <xsl:otherwise>
                <img src="images/spacer.gif" style="height:21px;width:50px"/>
                </xsl:otherwise>
              </xsl:choose>
      			</td>
          </tr>
        </table>
  
      </td>
  	</tr>
  
  </xsl:template>

  <xsl:template match="Vehicle">
  
  	<option>
  		<xsl:attribute name="value">
  			<xsl:value-of select="@ClaimAspectID"/>
  		</xsl:attribute>
  		<xsl:if test="@ClaimAspectID = $currentVehicle">
  			<xsl:attribute name="SELECTED"/>
  		</xsl:if>
  		Vehicle <xsl:value-of select="@VehicleNumber"/>
  	</option>
  
  </xsl:template>

</xsl:stylesheet>
