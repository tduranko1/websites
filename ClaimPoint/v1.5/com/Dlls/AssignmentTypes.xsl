<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="CarrierRep"/>
<xsl:param name="OfficeID"/>
<xsl:param name="ClaimViewLevel"/>
<xsl:param name="ClaimViewLevel_All"/>

<xsl:template match="Root">
    
    <xsl:choose>
      <xsl:when test="$CarrierRep = 0"> 
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img border="0" src="images/spacer.gif" width="10" height="1"/></td>
            <td valign="top" nowrap="">
              <xsl:variable name="scPSPattern">
                <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @ServiceChannelName = 'Program Shop']"><xsl:value-of select="@ReferenceID"/>,</xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(AssignmentType[contains($scPSPattern,concat(string(@AssignmentTypeID), ','))]) > 0">
                <xsl:call-template name="getPSAssignmentType"/>

                 <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
              </xsl:if>

			  <!-- Assignment Type Choice Shop assignment Added by glsd451 -->
              <xsl:variable name="scCSAPattern">
                <xsl:for-each select="/Root/AssignmentType[@AssignmentTypeID ='17']">
                  <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @Name = 'Choice Shop Assignment']">
                    <xsl:value-of select="@ReferenceID"/>,
                  </xsl:for-each>
                </xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(AssignmentType[contains($scCSAPattern,concat(string(@AssignmentTypeID), ','))]) > 0">
                <xsl:call-template name="getCSAssignmentType"/>

                <img border="0" src="images/spacer.gif" width="20" height="1"/>
                <br/>
                <img border="0" src="images/spacer.gif" width="1" height="6"/>
                <br/>
              </xsl:if>

			  <!-- Assignment Type for Secondary Towing
              <xsl:variable name="scSTPattern">
                <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @ServiceChannelName = 'Secondary Towing']"><xsl:value-of select="@ReferenceID"/>,</xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(AssignmentType[contains($scPSPattern,concat(string(@AssignmentTypeID), ','))]) > 0">
                <xsl:call-template name="getSTAssignmentType"/>

                 <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
              </xsl:if> -->

			  
              <xsl:variable name="scRRPPattern">
                <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @ServiceChannelName = 'Repair Referral']">
                  <xsl:value-of select="@ReferenceID"/>
                </xsl:for-each>
              </xsl:variable>
              <xsl:variable name="possibleAssignments">
                <xsl:for-each select="/Root/AssignmentType">
                  <xsl:value-of select="@AssignmentTypeID"/>,
                </xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(AssignmentType[contains($possibleAssignments, $scRRPPattern)]) > 0">
                <xsl:call-template name="getRRPAssignmentType"/>

                <img border="0" src="images/spacer.gif" width="20" height="1"/>
                <br/>
                <img border="0" src="images/spacer.gif" width="1" height="6"/>
                <br/>
              </xsl:if>

              <xsl:variable name="scIAPattern">
                <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @ServiceChannelName = 'Ind Adjuster']"><xsl:value-of select="@ReferenceID"/>,</xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(AssignmentType[contains($scIAPattern,concat(string(@AssignmentTypeID), ','))]) > 0">
                <xsl:call-template name="getIAAssignmentType"/>

                 <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
              </xsl:if>

              <xsl:variable name="scTLPattern">
                <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @ServiceChannelName = 'Total Loss']"><xsl:value-of select="concat('|',@ReferenceID,'|')"/>,</xsl:for-each>
              </xsl:variable>
              <!-- <xsl:value-of select="concat('scTLPattern=', $scTLPattern)"/> -->
              <xsl:if test="count(AssignmentType[contains($scTLPattern,concat('|', string(@AssignmentTypeID), '|,'))]) > 0">
                <xsl:call-template name="getTLAssignmentType"/>

                <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
              </xsl:if>

             
            </td>
            <td valign="top" nowrap="">
              <xsl:for-each select="AssignmentType" >
                <xsl:if test="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = current()/@AssignmentTypeID and @ServiceChannelName = 'Desk Audit']">
                  <xsl:call-template name="getAssignmentTypes"/>
                  <xsl:if test="not(position()=last())">
                    <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
                  </xsl:if>
                </xsl:if>
              </xsl:for-each>
            </td>
            <td valign="top" nowrap="">
              <xsl:for-each select="AssignmentType" >
                <xsl:if test="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = current()/@AssignmentTypeID and @ServiceChannelName = 'Desk Review']">
                  <xsl:call-template name="getAssignmentTypes"/>
                  <xsl:if test="not(position()=last())">
                    <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
                  </xsl:if>
                </xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </table>
      </xsl:when> 
      <xsl:otherwise>

        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img border="0" src="images/spacer.gif" width="10" height="1"/></td>
            <td valign="top" nowrap="">
              <xsl:variable name="scPSPattern">
                <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @ServiceChannelName = 'Program Shop']"><xsl:value-of select="@ReferenceID"/>,</xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(Office/OfficeAssignmentType[contains($scPSPattern,concat(string(@AssignmentTypeID), ','))]) > 0">
                <xsl:call-template name="getPSAssignmentType"/>
                <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
              </xsl:if>

			   <!--Assignment type Choice Shop Added by Glsd451 -->

              <xsl:variable name="scCSAPattern">
                <xsl:for-each select="/Root/AssignmentType[@AssignmentTypeID ='17']">
                  <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @Name = 'Choice Shop Assignment']">
                    <xsl:value-of select="@ReferenceID"/>,
                  </xsl:for-each>
                </xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(Office/OfficeAssignmentType[contains($scCSAPattern,concat(string(@AssignmentTypeID), ','))]) > 0">
                <xsl:call-template name="getCSAssignmentType"/>
                <img border="0" src="images/spacer.gif" width="20" height="1"/>
                <br/>
                <img border="0" src="images/spacer.gif" width="1" height="6"/>
                <br/>
              </xsl:if>

              <xsl:variable name="scRRPPattern">
                <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @ServiceChannelName = 'Repair Referral']">
                  <xsl:value-of select="@ReferenceID"/>
                </xsl:for-each>
              </xsl:variable>
              <xsl:variable name="possibleAssignments">
                <xsl:for-each select="/Root/AssignmentType">
                  <xsl:value-of select="@AssignmentTypeID"/>,
                </xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(AssignmentType[contains($possibleAssignments, $scRRPPattern)]) > 0">
                <xsl:call-template name="getRRPAssignmentType"/>

                <img border="0" src="images/spacer.gif" width="20" height="1"/>
                <br/>
                <img border="0" src="images/spacer.gif" width="1" height="6"/>
                <br/>
              </xsl:if>
            

              <xsl:variable name="scIAPattern">
                <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @ServiceChannelName = 'Ind Adjuster']"><xsl:value-of select="@ReferenceID"/>,</xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(Office/OfficeAssignmentType[contains($scIAPattern,concat(string(@AssignmentTypeID), ','))]) > 0">
                <xsl:call-template name="getIAAssignmentType"/>
                <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
              </xsl:if>

              <xsl:variable name="scTLPattern">
                <xsl:for-each select="/Root/Reference[@List = 'AssignmentType' and @ServiceChannelName = 'Total Loss']"><xsl:value-of select="concat('|',@ReferenceID,'|')"/>,</xsl:for-each>
              </xsl:variable>
              <xsl:if test="count(Office/OfficeAssignmentType[contains($scTLPattern,concat('|', string(@AssignmentTypeID), '|,'))]) > 0">
                <xsl:call-template name="getTLAssignmentType"/>

                <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
              </xsl:if>
            </td>
            <td valign="top" nowrap="">
              <xsl:for-each select="Office/OfficeAssignmentType[../@OfficeID = $OfficeID and @AssignmentTypeID > 0]" >
                <xsl:if test="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = current()/@AssignmentTypeID and @ServiceChannelName = 'Desk Audit']">
                  <xsl:call-template name="getAssignmentTypes"/>
                  <xsl:if test="not(position()=last())">
                    <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
                  </xsl:if>
                </xsl:if>
              </xsl:for-each>
            </td>
            <td valign="top" nowrap="">
              <xsl:for-each select="Office/OfficeAssignmentType[../@OfficeID = $OfficeID and @AssignmentTypeID > 0]" >
                <xsl:if test="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = current()/@AssignmentTypeID and @ServiceChannelName = 'Desk Review']">
                  <xsl:call-template name="getAssignmentTypes"/>
                  <xsl:if test="not(position()=last())">
                    <img border="0" src="images/spacer.gif" width="20" height="1"/><br/><img border="0" src="images/spacer.gif" width="1" height="6"/><br/>
                  </xsl:if>
                </xsl:if>
              </xsl:for-each>
            </td>
          </tr>
        </table>
      </xsl:otherwise>
      
    </xsl:choose>

</xsl:template>

<xsl:template name="getAssignmentTypes">
  <xsl:variable name="currentID"><xsl:value-of select="@AssignmentTypeID"/></xsl:variable>
    <a>
      <xsl:attribute name="id">txtAssignmentTypeID_<xsl:value-of select="$currentID"/></xsl:attribute>
      <xsl:attribute name="href">javascript:clickAssignment("<xsl:value-of select='$currentID'/>", "<xsl:value-of select="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = $currentID]/@Name"/>")</xsl:attribute>
      <xsl:attribute name="onMouseOver">MM_swapImage("btn_SC_<xsl:value-of select='$currentID'/>","","images/btn_SC_<xsl:value-of select='$currentID'/>_o.gif",1); window.status='<xsl:value-of select="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = $currentID]/@Name"/> Assignment'; return true;</xsl:attribute>
      <xsl:attribute name="onMouseOut">MM_swapImgRestore(); window.status=''; return true</xsl:attribute>
        <img width="130" height="28" border="0">
          <xsl:attribute name="title">Click to start a <xsl:value-of select="/Root/Reference[@List = 'AssignmentType' and @ReferenceID = $currentID]/@Name"/> assignment</xsl:attribute>
          <xsl:attribute name="name">btn_SC_<xsl:value-of select='$currentID'/></xsl:attribute>
          <xsl:attribute name="src">images/btn_SC_<xsl:value-of select='$currentID'/>.gif</xsl:attribute>
        </img>
    </a>

</xsl:template>

<xsl:template name="getIAAssignmentType">

  <xsl:variable name="currentID">3</xsl:variable>
    <a>
      <xsl:attribute name="id">txtAssignmentTypeID_<xsl:value-of select="$currentID"/></xsl:attribute>
      <xsl:attribute name="href">javascript:clickAssignment("<xsl:value-of select='$currentID'/>", "Independent Adjuster")</xsl:attribute>
      <xsl:attribute name="onMouseOver">MM_swapImage("btn_SC_<xsl:value-of select='$currentID'/>","","images/btn_SC_<xsl:value-of select='$currentID'/>_o.gif",1); window.status='IA Assignment'; return true;</xsl:attribute>
      <xsl:attribute name="onMouseOut">MM_swapImgRestore(); window.status=''; return true</xsl:attribute>
        <img width="130" height="28" border="0" title="Click to start an IA assignment">
          <xsl:attribute name="name">btn_SC_<xsl:value-of select='$currentID'/></xsl:attribute>
          <xsl:attribute name="src">images/btn_SC_<xsl:value-of select='$currentID'/>.gif</xsl:attribute>
        </img>
    </a>

</xsl:template>

<xsl:template name="getPSAssignmentType">

  <xsl:variable name="currentID">4</xsl:variable>
    <a>
      <xsl:attribute name="id">txtAssignmentTypeID_<xsl:value-of select="$currentID"/></xsl:attribute>
      <xsl:attribute name="href">javascript:clickAssignment("<xsl:value-of select='$currentID'/>", "Program Shop")</xsl:attribute>
      <xsl:attribute name="onMouseOver">MM_swapImage("btn_SC_<xsl:value-of select='$currentID'/>","","images/btn_SC_<xsl:value-of select='$currentID'/>_o.gif",1); window.status='Program Shop Assignment'; return true;</xsl:attribute>
      <xsl:attribute name="onMouseOut">MM_swapImgRestore(); window.status=''; return true</xsl:attribute>
        <img width="130" height="28" border="0" title="Click to start a Program Shop assignment">
          <xsl:attribute name="name">btn_SC_<xsl:value-of select='$currentID'/></xsl:attribute>
          <xsl:attribute name="src">images/btn_SC_<xsl:value-of select='$currentID'/>.gif</xsl:attribute>
        </img>
    </a>

</xsl:template>

<!-- Assignment type Choice Shop  Added by glsd451-->
   <xsl:template name="getCSAssignmentType">

    <xsl:variable name="currentID">17</xsl:variable>
    <a>
      <xsl:attribute name="id">txtAssignmentTypeID_<xsl:value-of select="$currentID"/></xsl:attribute>
      <xsl:attribute name="href">javascript:clickAssignment("<xsl:value-of select='$currentID'/>", "Choice Shop")</xsl:attribute>
      <xsl:attribute name="onMouseOver">MM_swapImage("btn_SC_<xsl:value-of select='$currentID'/>","","images/btn_SC_<xsl:value-of select='$currentID'/>_o.gif",1); window.status='Choice Shop Assignment'; return true;</xsl:attribute>
      <xsl:attribute name="onMouseOut">MM_swapImgRestore(); window.status=''; return true</xsl:attribute>
        <img width="130" height="28" border="0" title="Click to start a Choice Shop assignment">
         <xsl:attribute name="name">btn_SC_<xsl:value-of select='$currentID'/></xsl:attribute>
         <xsl:attribute name="src">images/btn_SC_<xsl:value-of select='$currentID'/>.gif</xsl:attribute>
      </img>
    </a>

  </xsl:template>



<xsl:template name="getTLAssignmentType">

  <xsl:variable name="currentID">14</xsl:variable>
    <a>
      <xsl:attribute name="id">txtAssignmentTypeID_<xsl:value-of select="$currentID"/></xsl:attribute>
      <xsl:attribute name="href">javascript:clickAssignment("<xsl:value-of select='$currentID'/>", "Total Loss")</xsl:attribute>
      <xsl:attribute name="onMouseOver">MM_swapImage("btn_SC_<xsl:value-of select='$currentID'/>","","images/btn_SC_<xsl:value-of select='$currentID'/>_o.gif",1); window.status='Program Shop Assignment'; return true;</xsl:attribute>
      <xsl:attribute name="onMouseOut">MM_swapImgRestore(); window.status=''; return true</xsl:attribute>
        <img width="130" height="28" border="0" title="Click to start a Total Loss assignment">
          <xsl:attribute name="name">btn_SC_<xsl:value-of select='$currentID'/></xsl:attribute>
          <xsl:attribute name="src">images/btn_SC_<xsl:value-of select='$currentID'/>.gif</xsl:attribute>
        </img>
    </a>

</xsl:template>


  <xsl:template name="getRRPAssignmentType">

    <xsl:variable name="currentID">16</xsl:variable>
    <a>
      <xsl:attribute name="id">txtAssignmentTypeID_<xsl:value-of select="$currentID"/></xsl:attribute>
      <xsl:attribute name="href">javascript:clickAssignment("<xsl:value-of select='$currentID'/>", "Repair Referral")</xsl:attribute>
      <xsl:attribute name="onMouseOver">MM_swapImage("btn_SC_<xsl:value-of select='$currentID'/>","","images/btn_SC_<xsl:value-of select='$currentID'/>_o.gif",1); window.status='Repair Referral Assignment'; return true;</xsl:attribute>
      <xsl:attribute name="onMouseOut">MM_swapImgRestore(); window.status=''; return true</xsl:attribute>
      <img width="130" height="28" border="0" title="Click to start a Repair Referral assignment">
        <xsl:attribute name="name">btn_SC_<xsl:value-of select='$currentID'/></xsl:attribute>
        <xsl:attribute name="src">images/btn_SC_<xsl:value-of select='$currentID'/>.gif</xsl:attribute>
      </img>
    </a>

  </xsl:template>
  
</xsl:stylesheet>
