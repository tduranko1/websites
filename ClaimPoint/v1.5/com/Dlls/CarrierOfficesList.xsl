<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="OfficeID"/>

<xsl:template match="Root">

  <xsl:comment>v1.4.6.0</xsl:comment>

  <select name="OfficeID" id="txtOfficeID" onChange="carrierOfficesSelect()">
  	<option value="0">
      <xsl:if test="$OfficeID = '0'">
        <xsl:attribute name="Selected"/>
      </xsl:if>
      * All Offices *
    </option>
    <xsl:for-each select="Office[@EnabledFlag='1']" >
      <xsl:sort select="@ClientOfficeId" order="ascending"/>
        <xsl:call-template name="getCarrierOfficesList"/>
    </xsl:for-each>
  </select>

</xsl:template>

<xsl:template name="getCarrierOfficesList">

  <xsl:if test="translate(@ClientOfficeId, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') != 'FCS'">
  	<option>
  		<xsl:attribute name="value">
  			<xsl:value-of select="@OfficeID"/>
  		</xsl:attribute>
      <xsl:if test="@OfficeID = $OfficeID">
        <xsl:attribute name="Selected"/>
      </xsl:if>
  			<xsl:value-of select="@ClientOfficeId"/>
  	</option>
  </xsl:if>

</xsl:template>

</xsl:stylesheet>
