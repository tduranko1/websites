<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="ms user">

  <xsl:output method="html" omit-xml-declaration="yes" standalone="yes" indent="yes" encoding="UTF-8" />

  <ms:script language="JScript" implements-prefix="user">
    <![CDATA[
    function extractSQLDateTime( strDate )
    {
        var strConv =
             strDate.substr( 5, 2 ) + "/"
           + strDate.substr( 8, 2 ) + "/"
           + strDate.substr( 0, 4 ) + " "
           + strDate.substr( 11, 8 );
        return strConv;
    }
  ]]>
  </ms:script>
  <xsl:param name="UserType"  select="'U'"/>
  <xsl:param name="InsuranceID"  select="''"/>
  <xsl:param name="CarrierAuthorizesPaymentsFlag"/>

  <xsl:template match="/Root">

    <xsl:comment>v1.4.2.0</xsl:comment>

    <xsl:variable name="AppID">
      <xsl:value-of select="/Root/Reference[@List='Application' and @Name='ClaimPoint']/@ReferenceID"/>
    </xsl:variable>

    <form id="frmUserDetail" name="frmUserDetail" method="post" target="IFrmUserDetailUpd" action="rspUserDetailUpd.asp">

      <xsl:apply-templates select="User"/>

      <input type="hidden" id="UserID" name="UserID"/>
      <input type="hidden" id="ApplicationID" name="ApplicationID"/>
      <input type="hidden" id="SupervisorUserID" name="SupervisorUserID">
        <xsl:attribute name="value">
          <xsl:if test="User/@SupervisorUserID != '0'">
            <xsl:value-of select="User/@SupervisorUserID"/>
          </xsl:if>
        </xsl:attribute>
      </input>
      <input type="hidden" id="AssignmentBeginDate" name="AssignmentBeginDate">
        <xsl:attribute name="value">
          <xsl:value-of select="User/@AssignmentBeginDate"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="AssignmentEndDate" name="AssignmentEndDate">
        <xsl:attribute name="value">
          <xsl:value-of select="User/@AssignmentEndDate"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="NameTitle" name="NameTitle">
        <xsl:attribute name="value">
          <xsl:value-of select="User/@NameTitle"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="SupervisorFlag" name="SupervisorFlag">
        <xsl:attribute name="value">
          <xsl:value-of select="User/@SupervisorFlag"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="WorkdayList" name="WorkdayList">
        <xsl:attribute name="value">
          <xsl:value-of select="concat(User/@WorkDaySundayFlag, ',', User/@WorkDayMondayFlag, ',', User/@WorkDayTuesdayFlag, ',', User/@WorkDayWednesdayFlag, ',', User/@WorkDayThursdayFlag, ',', User/@WorkDayFridayFlag, ',', User/@WorkDaySaturdayFlag)"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="LicenseAssignStList" name="LicenseAssignStList">
        <xsl:attribute name="value">
          <xsl:for-each select="User/States[@LicenseFlag='1']">
            <xsl:value-of select="@StateCode"/>,<xsl:value-of select="@LicenseFlag"/>,<xsl:value-of select="@AssignWorkFlag"/><xsl:if test="position() &lt; last()">;</xsl:if>
          </xsl:for-each>
        </xsl:attribute>
      </input>
      <input type="hidden" id="RoleList" name="RoleList">
        <xsl:attribute name="value">
          <xsl:for-each select="User/Role">
            <xsl:sort select="@PrimaryRoleFlag" data-type="number" order="descending"/>
            <xsl:value-of select="@RoleID"/>
            <xsl:if test="position() &lt; last()">,</xsl:if>
          </xsl:for-each>
        </xsl:attribute>
      </input>
      <input type="hidden" id="ProfileList" name="ProfileList"/>
      <input type="hidden" id="PermissionList" name="PermissionList"/>
      <input type="hidden" id="SysLastUserID" name="SysLastUserID"/>
      <input type="hidden" id="SysLastUpdatedDate" name="SysLastUpdatedDate">
        <xsl:attribute name="value">
          <xsl:value-of select="User/@SysLastUpdatedDate"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="EmailAddress" name="EmailAddress"/>
      <input type="hidden" id="NameFirst" name="NameFirst"/>
      <input type="hidden" id="NameLast" name="NameLast"/>
      <input type="hidden" id="PasswordHint" name="PasswordHint">
        <xsl:attribute name="value">
          <xsl:value-of select="User/Application[@ApplicationID=$AppID]/@PasswordHint"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="AppSysLastUpdatedDate" name="AppSysLastUpdatedDate">
        <xsl:attribute name="value">
          <xsl:value-of select="User/Application[@ApplicationID=$AppID]/@SysLastUpdatedDate"/>
        </xsl:attribute>
      </input>
      <xsl:if test="$UserType = 'M'">
        <input type="hidden" id="OfficeID" name="OfficeID"/>
      </xsl:if>
      <input type="hidden" id="mode" name="mode"/>
      <input type="hidden" id="InsuranceCompanyID" name="InsuranceCompanyID"/>
      <input type="hidden" id="ApplicationCD" name="ApplicationCD"/>
      <input type="hidden" id="UserExistsInCP" name="UserExistsInCP">
        <xsl:attribute name="value">
          <xsl:value-of select="count(/Root/User/Application[@ApplicationID=$AppID])"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="UserType" name="UserType">
        <xsl:attribute name="value">
          <xsl:value-of select="$UserType"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="UserActiveStatus" name="UserActiveStatus">
        <xsl:attribute name="value">
          <xsl:choose>
            <xsl:when test="/Root/User/Application[@ApplicationID=$AppID]/@AccessEndDate != ''">0</xsl:when>
            <xsl:when test="/Root/User/Application[@ApplicationID=$AppID]/@AccessEndDate = ''">1</xsl:when>
          </xsl:choose>
        </xsl:attribute>
      </input>
      <input type="hidden" id="UserEmailAddress" name="UserEmailAddress">
        <xsl:attribute name="value">
          <xsl:value-of select="/Root/User/@EmailAddress"/>
        </xsl:attribute>
      </input>
      <input type="hidden" id="ClientUserID" name="ClientUserID"/>
    </form>
    <input type="hidden" id="OverriddenProfileItems" name="OverriddenProfileItems">
      <xsl:attribute name="value">
        <xsl:for-each select="User/Profile[@Show='0' and @OverriddenFlag='1']">
          <xsl:value-of select="@ProfileID"/>,<xsl:value-of select="@ProfileValue"/><xsl:if test="position() &lt; last()">,</xsl:if>
        </xsl:for-each>
      </xsl:attribute>
    </input>
    <input type="hidden" id="OverriddenPermissionItems" name="OverriddenPermissionItems">
      <xsl:attribute name="value">
        <xsl:for-each select="User/Permission[@Show='0' and @OverriddenFlag='1']">
          <xsl:value-of select="@PermissionID"/>,<xsl:value-of select="@CreateFlag"/>,<xsl:value-of select="@ReadFlag"/>,<xsl:value-of select="@UpdateFlag"/>,<xsl:value-of select="@DeleteFlag"/><xsl:if test="position() &lt; last()">,</xsl:if>
        </xsl:for-each>
      </xsl:attribute>
    </input>
    <iframe src="blank.asp" id="IFrmUserDetailUpd" name="IFrmUserDetailUpd" style="height:100px;width:500px;display:none" frameBorder="0"/>
    <script language="javascript">
      sAppID = "<xsl:value-of select="/Root/Reference[@List='Application' and @Name='ClaimPoint']/@ReferenceID"/>";
    </script>
  </xsl:template>

  <xsl:template match="User">
    <xsl:variable name="AppID">
      <xsl:value-of select="/Root/Reference[@List='Application' and @Name='ClaimPoint']/@ReferenceID"/>
    </xsl:variable>
    <xsl:variable name="APDAppID">
      <xsl:value-of select="/Root/Reference[@List='Application' and @Name='APD']/@ReferenceID"/>
    </xsl:variable>

    <table width="577" border="0" cellpadding="4" cellspacing="0">
      <tr>
        <td width="275">
          <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="0"/>
        </td>
        <td width="27">
          <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="0"/>
        </td>
        <td width="275">
          <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="0"/>
        </td>
      </tr>
      <tr>
        <td colspan="2" class="bodyBlue">Login ID: (User email address) *</td>
        <td class="bodyBlue">Client ID:</td>
      </tr>
      <tr style="padding:0px;padding-bottom:10px;">
        <td nowrap="" colspan="2">
          <input name="txtEmailAddress" id="txtEmailAddress" type="text" onBlur="ldTrim(this);doCheckUser()" size="40">
            <xsl:attribute name="maxLength">
              <xsl:value-of select="/Root/Metadata[@Entity='User']/Column[@Name='EmailAddress']/@MaxLength"/>
            </xsl:attribute>

            <xsl:attribute name="value">
              <xsl:choose>
                <xsl:when test="Application[@ApplicationID=$AppID]/@LogonId != ''">
                  <xsl:value-of select="Application[@ApplicationID=$AppID]/@LogonId"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="@EmailAddress"/>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
            <xsl:if test="@UserID != '-1'">
              <xsl:attribute name="disabled">True</xsl:attribute>
              <xsl:attribute name="readonly">True</xsl:attribute>
            </xsl:if>
          </input>
          <xsl:if test="@UserID != '-1'">
            <img border="0" src="images/icon_UserProfile.jpg" align="absbottom" hspace="6" width="28" height="22" style="cursor:hand" title="Change User Login ID">
              <xsl:attribute name="onclick">changeLoginID()</xsl:attribute>
            </img>
          </xsl:if>
        </td>
        <td nowrap="">
          <input name="txtClientID" id="txtClientID" type="text" size="40" >
            <xsl:attribute name="maxLength">
              <xsl:value-of select="/Root/Metadata[@Entity='User']/Column[@Name='ClientUserId']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@ClientUserID"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">First Name: *</td>
        <td></td>
        <td class="bodyBlue">Last Name: *</td>
      </tr>
      <tr style="padding:0px;padding-bottom:10px;">
        <td nowrap="">
          <input name="txtNameFirst" id="txtNameFirst" type="text" onBlur="ldTrim(this)" size="40">
            <xsl:attribute name="maxLength">
              <xsl:value-of select="/Root/Metadata[@Entity='User']/Column[@Name='NameFirst']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@NameFirst"/>
            </xsl:attribute>
          </input>
        </td>
        <td nowrap="">&#160;</td>
        <td nowrap="">
          <input name="txtNameLast" id="txtNameLast" type="text" onBlur="ldTrim(this)" size="40">
            <xsl:attribute name="maxLength">
              <xsl:value-of select="/Root/Metadata[@Entity='User']/Column[@Name='NameLast']/@MaxLength"/>
            </xsl:attribute>
            <xsl:attribute name="value">
              <xsl:value-of select="@NameLast"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">Phone: *</td>
        <td></td>
        <td class="bodyBlue">Fax:</td>
      </tr>
      <tr style="padding:0px;padding-bottom:10px;">
        <td nowrap="">
          <input name="PhoneAreaCode" id="txtPhoneAreaCode" type="text" onBlur="ldTrim(this)" size="3" maxLength="3" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="autoTab(this, 3, event)">
            <xsl:attribute name="value">
              <xsl:value-of select="normalize-space(@PhoneAreaCode)"/>
            </xsl:attribute>
          </input>&#160;
          <input name="PhoneExchangeNumber" id="txtPhoneExchangeNumber" type="text" value="" onBlur="ldTrim(this)" size="3" maxLength="3" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="autoTab(this, 3, event)">
            <xsl:attribute name="value">
              <xsl:value-of select="normalize-space(@PhoneExchangeNumber)"/>
            </xsl:attribute>
          </input>&#160;
          <input name="PhoneUnitNumber" id="txtPhoneUnitNumber" type="text" value="" onBlur="ldTrim(this)" size="4" maxLength="4" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="autoTab(this, 4, event)">
            <xsl:attribute name="value">
              <xsl:value-of select="normalize-space(@PhoneUnitNumber)"/>
            </xsl:attribute>
          </input>&#160;&#160;Ext.:
          <input name="PhoneExtensionNumber" id="txtPhoneExtensionNumber" type="text" value="" onBlur="ldTrim(this)" size="5" maxLength="7" onKeyPress="return numbersOnly(this,event,0)">
            <xsl:attribute name="value">
              <xsl:value-of select="normalize-space(@PhoneExtensionNumber)"/>
            </xsl:attribute>
          </input>
        </td>
        <td nowrap="">&#160;</td>
        <td nowrap="">
          <input name="FaxAreaCode" id="txtFaxAreaCode" type="text" value="" onBlur="ldTrim(this)" size="3" maxLength="3" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="autoTab(this, 3, event)">
            <xsl:attribute name="value">
              <xsl:value-of select="normalize-space(@FaxAreaCode)"/>
            </xsl:attribute>
          </input>&#160;
          <input name="FaxExchangeNumber" id="txtFaxExchangeNumber" type="text" value="" onBlur="ldTrim(this)" size="3" maxLength="3" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="autoTab(this, 3, event)">
            <xsl:attribute name="value">
              <xsl:value-of select="normalize-space(@FaxExchangeNumber)"/>
            </xsl:attribute>
          </input>&#160;
          <input name="FaxUnitNumber" id="txtFaxUnitNumber" type="text" value="" onBlur="ldTrim(this)" size="4" maxLength="4" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="autoTab(this, 4, event)">
            <xsl:attribute name="value">
              <xsl:value-of select="normalize-space(@FaxUnitNumber)"/>
            </xsl:attribute>
          </input>&#160;&#160;Ext.:
          <input name="FaxExtensionNumber" id="txtFaxExtensionNumber" type="text" value="" onBlur="ldTrim(this)" size="5" maxLength="5" onKeyPress="return numbersOnly(this,event,0)">
            <xsl:attribute name="value">
              <xsl:value-of select="normalize-space(@FaxExtensionNumber)"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <xsl:if test="$UserType != 'M'">
        <tr>
          <td colspan="3" class="bodyBlue">Office: *</td>
        </tr>
        <tr style="padding:0px;padding-bottom:10px;">
          <td nowrap="" colspan="3">
            <xsl:variable name="OfficeID">
              <xsl:value-of select="@OfficeID"/>
            </xsl:variable>
            <select name="OfficeID" id="selOfficeID">
              <option value=""></option>
              <xsl:for-each select="/Root/Reference[@List='Office' and @InsuranceCompanyID=$InsuranceID]">
                <option>
                  <xsl:attribute name="value">
                    <xsl:value-of select="@ReferenceID"/>
                  </xsl:attribute>
                  <xsl:if test="@ReferenceID = $OfficeID">
                    <xsl:attribute name="selected"/>
                  </xsl:if>
                  <xsl:value-of select="@ClientOfficeId"/>
                </option>
              </xsl:for-each>
            </select>
          </td>
        </tr>
      </xsl:if>
      <tr>
        <td colspan="2" class="bodyBlue">
          <xsl:if test="Profile[@ProfileID='18' and @Show='1']">
            <xsl:value-of select="Profile[@ProfileID='18']/@ProfileName"/>: *
          </xsl:if>
        </td>
        <td id="ChangeLoginIDifrmTD" rowspan="7" valign="top" style="display:none">

          <iframe src="blank.asp" name="IFrmAwareIDChange" id="IFrmAwareIDChange" frameborder="0" style="height:160px; width:275px; position:absolute; margin-top:22px;"></iframe>

        </td>
      </tr>
      <tr style="padding:0px;padding-bottom:10px;">
        <td nowrap="" colspan="2">
          <xsl:if test="Profile[@ProfileID='18' and @Show='1']">
            <xsl:variable name="ProfileValue">
              <xsl:value-of select="Profile[@ProfileID='18']/@ProfileValue"/>
            </xsl:variable>
            <xsl:variable name="AllClaimsID">
              <xsl:value-of select="/Root/Reference[@List='ProfileSelection' and normalize-space(@Name)='All Claims']/@ReferenceID"/>
            </xsl:variable>
            <select name="UserClaimViewLevel" id="txtUserClaimViewLevel" ProfileID="18">
              <option value=""></option>
              <xsl:if test="$UserType != 'M'">
                <xsl:for-each select="/Root/Reference[@List='ProfileSelection' and @ProfileID='18']">
                  <option>
                    <xsl:attribute name="value">
                      <xsl:value-of select="@ReferenceID"/>
                    </xsl:attribute>
                    <xsl:if test="@ReferenceID = $ProfileValue">
                      <xsl:attribute name="selected"/>
                    </xsl:if>
                    <xsl:value-of select="@Name"/>
                  </option>
                </xsl:for-each>
              </xsl:if>
              <xsl:if test="$UserType = 'M'">
                <xsl:for-each select="/Root/Reference[@List='ProfileSelection' and @ProfileID='18' and @ReferenceID=$AllClaimsID]">
                  <option>
                    <xsl:attribute name="value">
                      <xsl:value-of select="@ReferenceID"/>
                    </xsl:attribute>
                    <xsl:if test="@ReferenceID = $ProfileValue">
                      <xsl:attribute name="selected"/>
                    </xsl:if>
                    <xsl:value-of select="@Name"/>
                  </option>
                </xsl:for-each>
              </xsl:if>
            </select>
          </xsl:if>
        </td>
      </tr>
      <tr>
        <td height="14" colspan="2" class="bodyBlue">
          Permissions:
        </td>
      </tr>
      <xsl:if test="Permission[@PermissionID='2' and @Show='1']">
        <xsl:variable name="PermissionValue">
          <xsl:value-of select="Permission[@PermissionID='2']/@ReadFlag"/>
          <xsl:value-of select="Permission[@PermissionID='2']/@UpdateFlag"/>
        </xsl:variable>
        <xsl:variable name="DefaultPermissionValue">
          <xsl:value-of select="Permission[@PermissionID='2']/@DefaultCreateFlag"/>
          <xsl:value-of select="Permission[@PermissionID='2']/@DefaultReadFlag"/>
          <xsl:value-of select="Permission[@PermissionID='2']/@DefaultUpdateFlag"/>
          <xsl:value-of select="Permission[@PermissionID='2']/@DefaultDeleteFlag"/>
        </xsl:variable>
        <xsl:variable name="Overridden">
          <xsl:choose>
            <xsl:when test="$PermissionValue != $DefaultPermissionValue">1</xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
      </xsl:if>
      <tr style="padding:0px;padding-bottom:10px;">
        <td colspan="2">
          <input type="checkbox" name="UserViewClaim" id="txtUserViewClaim" PermissionID="2">
            <xsl:if test="(Permission[@PermissionID='2']/@OverriddenFlag='1' and Permission[@PermissionID='2']/@ReadFlag='1') or (Permission[@PermissionID='2']/@OverriddenFlag='0' and Permission[@PermissionID='2']/@DefaultReadFlag='1')">
              <xsl:attribute name="checked"/>
            </xsl:if>
            <xsl:attribute name="defaultPermission">
              <xsl:value-of select="Permission[@PermissionID='2']/@DefaultReadFlag"/>
            </xsl:attribute>
          </input>
          <label for="txtUserViewClaim">View Claim</label>
          <br/>
        </td>
      </tr>
      <tr style="padding:0px;padding-bottom:10px;">
        <td colspan="2">
          <input type="checkbox" name="UserCreateClaim" id="txtUserCreateClaim" PermissionID="2" onblur="checkReadPermission()">
            <xsl:if test="(Permission[@PermissionID='2']/@OverriddenFlag='1' and Permission[@PermissionID='2']/@CreateFlag='1') or (Permission[@PermissionID='2']/@OverriddenFlag='0' and Permission[@PermissionID='2']/@DefaultCreateFlag='1')">
              <xsl:attribute name="checked"/>
            </xsl:if>
            <xsl:attribute name="defaultPermission">
              <xsl:value-of select="Permission[@PermissionID='2']/@DefaultCreateFlag"/>
            </xsl:attribute>
          </input>
          <label for="txtUserCreateClaim">Submit Assignment</label>&#160;&#160;
          <input type="checkbox" name="UserUpdateClaim" id="txtUserUpdateClaim" PermissionID="2" style="display:none">
            <xsl:if test="(Permission[@PermissionID='2']/@OverriddenFlag='1' and Permission[@PermissionID='2']/@UpdateFlag='1') or (Permission[@PermissionID='2']/@OverriddenFlag='0' and Permission[@PermissionID='2']/@DefaultUpdateFlag='1')">
              <xsl:attribute name="checked"/>
            </xsl:if>
            <xsl:attribute name="defaultPermission">
              <xsl:value-of select="Permission[@PermissionID='2']/@DefaultUpdateFlag"/>
            </xsl:attribute>
          </input>
          <label for="txtUserUpdateClaim" style="display:none">Update</label>
        </td>
      </tr>
      <tr style="padding:0px;padding-bottom:10px;">
        <td nowrap="" colspan="2">
          <xsl:if test="Permission[@PermissionID='40' and @Show='1']">
            <xsl:variable name="PermissionValue">
              <xsl:value-of select="Permission[@PermissionID='40']/@ReadFlag"/>
              <xsl:value-of select="Permission[@PermissionID='40']/@UpdateFlag"/>
            </xsl:variable>
            <xsl:variable name="DefaultPermissionValue">
              <xsl:value-of select="Permission[@PermissionID='40']/@DefaultReadFlag"/>
              <xsl:value-of select="Permission[@PermissionID='40']/@DefaultUpdateFlag"/>
            </xsl:variable>
            <xsl:variable name="Overridden">
              <xsl:choose>
                <xsl:when test="$PermissionValue != $DefaultPermissionValue">1</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <input type="checkbox" name="UserViewReports" id="txtUserViewReports" PermissionID="40">
              <xsl:if test="(Permission[@PermissionID='40']/@OverriddenFlag='1' and $PermissionValue='11')">
                <xsl:attribute name="checked"/>
              </xsl:if>
              <xsl:attribute name="defaultPermission">
                <xsl:value-of select="Permission[@PermissionID='40']/@DefaultCreateFlag"/>
                <xsl:value-of select="Permission[@PermissionID='40']/@DefaultReadFlag"/>
                <xsl:value-of select="Permission[@PermissionID='40']/@DefaultUpdateFlag"/>
                <xsl:value-of select="Permission[@PermissionID='40']/@DefaultDeleteFlag"/>
              </xsl:attribute>
            </input>
            <label for="txtUserViewReports">View Reports</label>
          </xsl:if>
        </td>
      </tr>
      <tr style="padding:0px;padding-bottom:10px;">
        <td nowrap="" colspan="2">
          <xsl:if test="Permission[@PermissionID='41' and @Show='1']">
            <xsl:variable name="PermissionValue">
              <xsl:value-of select="Permission[@PermissionID='41']/@CreateFlag"/>
              <xsl:value-of select="Permission[@PermissionID='41']/@ReadFlag"/>
              <xsl:value-of select="Permission[@PermissionID='41']/@UpdateFlag"/>
            </xsl:variable>
            <xsl:variable name="DefaultPermissionValue">
              <xsl:value-of select="Permission[@PermissionID='41']/@DefaultCreateFlag"/>
              <xsl:value-of select="Permission[@PermissionID='41']/@DefaultReadFlag"/>
              <xsl:value-of select="Permission[@PermissionID='41']/@DefaultUpdateFlag"/>
            </xsl:variable>
            <xsl:variable name="Overridden">
              <xsl:choose>
                <xsl:when test="$PermissionValue != $DefaultPermissionValue">1</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
              </xsl:choose>
            </xsl:variable>
            <input type="checkbox" name="UserManageUsers" id="txtUserManageUsers" PermissionID="41">
              <xsl:if test="(Permission[@PermissionID='41']/@OverriddenFlag='1' and $PermissionValue='111') or (Permission[@PermissionID='41']/@OverriddenFlag='1' and $PermissionValue='011') or (Permission[@PermissionID='41']/@OverriddenFlag='0' and $DefaultPermissionValue='111') or (Permission[@PermissionID='41']/@OverriddenFlag='0' and $DefaultPermissionValue='011')">
                <xsl:attribute name="checked"/>
              </xsl:if>
              <xsl:attribute name="defaultPermission">
                <xsl:value-of select="$DefaultPermissionValue"/>
              </xsl:attribute>
            </input>
            <label for="txtUserManageUsers">Manage Users</label>
          </xsl:if>
        </td>
      </tr>

      <xsl:if test="$CarrierAuthorizesPaymentsFlag = '1'">
        <tr style="padding:0px;padding-bottom:10px;">
          <td nowrap="" colspan="3">
            <xsl:if test="Permission[@PermissionID='44' and @Show='1']">
              <xsl:variable name="PermissionValue">
                <xsl:value-of select="Permission[@PermissionID='44']/@ReadFlag"/>
                <xsl:value-of select="Permission[@PermissionID='44']/@UpdateFlag"/>
              </xsl:variable>
              <xsl:variable name="DefaultPermissionValue">
                <xsl:value-of select="Permission[@PermissionID='44']/@DefaultReadFlag"/>
                <xsl:value-of select="Permission[@PermissionID='44']/@DefaultUpdateFlag"/>
              </xsl:variable>
              <xsl:variable name="Overridden">
                <xsl:choose>
                  <xsl:when test="$PermissionValue != $DefaultPermissionValue">1</xsl:when>
                  <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <input type="checkbox" name="AuthorizePayments" id="txtAuthorizePayments" PermissionID="44">
                <xsl:if test="(Permission[@PermissionID='44']/@OverriddenFlag='1' and $PermissionValue='11') or (Permission[@PermissionID='44']/@OverriddenFlag='0' and $DefaultPermissionValue='11')">
                  <xsl:attribute name="checked"/>
                </xsl:if>
                <xsl:attribute name="defaultPermission">
                  <xsl:value-of select="Permission[@PermissionID='44']/@DefaultCreateFlag"/>
                  <xsl:value-of select="Permission[@PermissionID='44']/@DefaultReadFlag"/>
                  <xsl:value-of select="Permission[@PermissionID='44']/@DefaultUpdateFlag"/>
                  <xsl:value-of select="Permission[@PermissionID='44']/@DefaultDeleteFlag"/>
                </xsl:attribute>
              </input>
              <label for="txtAuthorizePayments">Authorize Payments</label>
            </xsl:if>

            <img border="0" src="images/spacer.gif" WIDTH="30" HEIGHT="1"/>

            <xsl:if test="Permission[@PermissionID='44' and @Show='1']">
              <input type="text" name="PaymentAuthorizationLevel" id="txtPaymentAuthorizationLevel" ProfileID="1" size="10" maxLength="9" onKeyPress="return numbersOnly(this,event,0)">
                <xsl:attribute name="value">
                  <xsl:choose>
                    <xsl:when test="Profile[@ProfileID='1']/@ProfileValue != 0 or Profile[@ProfileID='1']/@ProfileValue != ''">
                      <xsl:value-of select="Profile[@ProfileID='1']/@ProfileValue"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="Profile[@ProfileID='1']/@DefaultProfileValue"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:attribute>
              </input>
              <label for="txtPaymentAuthorizationLevel">Payment Authorization Level ($ amount)</label>
            </xsl:if>

          </td>
        </tr>
      </xsl:if>

      <tr>
        <td height="14" colspan="2" class="bodyBlue">
          User Status:
        </td>
        <td class="bodyBlue">
          <xsl:if test="@UserID != '-1'">
            Login Status:
          </xsl:if>
        </td>
      </tr>
      <tr>
        <td colspan="2" id="tdUserStatus" name="tdUserStatus" valign="top">
          <input type="radio" name="rbUserActive" id="rbUserActiveYes" value="1">
            <xsl:if test="Application[@ApplicationID=$AppID]/@AccessEndDate = ''">
              <xsl:attribute name="checked"/>
            </xsl:if>
          </input>
          <label for="rbUserActiveYes">Active</label>&#160;&#160;&#160;&#160;&#160;&#160;
          <input type="radio" name="rbUserActive" id="rbUserActiveNo" value="0">
            <xsl:if test="Application[@ApplicationID=$AppID]/@AccessEndDate != ''">
              <xsl:attribute name="checked"/>
            </xsl:if>
          </input>
          <label for="rbUserActiveNo">Inactive</label>
          <br/>

          <xsl:if test="@UserID != '-1' and count(Application[@ApplicationID=$AppID]) &gt; 0">
            <xsl:choose>
              <xsl:when test="Application[@ApplicationID=$AppID]/@AccessEndDate != ''">
                Inactive since <xsl:value-of select="user:extractSQLDateTime(string(Application[@ApplicationID=$AppID]/@AccessEndDate))"/>
              </xsl:when>
              <xsl:otherwise>
                Active since <xsl:value-of select="user:extractSQLDateTime(string(Application[@ApplicationID=$AppID]/@AccessBeginDate))"/>&#160;
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
        </td>
        <td valign="top">
          <xsl:if test="@UserID != '-1'">
            <iframe id="IFrmAwareStatus" name="IFrmAwareStatus" style="height:60px;width:275px;display:inline" frameBorder="0">
              <xsl:attribute name="src">
                frmUserLoginStatus.asp?UserName=<xsl:value-of select="/Root/User/@EmailAddress"/>
              </xsl:attribute>
            </iframe>
          </xsl:if>
        </td>
        <!-- <td>
          <xsl:choose>
            <xsl:when test="Application[@ApplicationID=$AppID]/@AccessEndDate != ''">
              <img id="imgActiveInactive" name="imgActiveInactive" border="0" src="images/btn_activate.gif" width="80" height="28" style="cursor:hand" title="Activate user" onclick="activateDeactivateUser()"/>
            </xsl:when>
            <xsl:otherwise>
              <img id="imgActiveInactive" name="imgActiveInactive" border="0" src="images/btn_deactivate.gif" width="80" height="28" style="cursor:hand" title="Deactivate user" onclick="activateDeactivateUser()"/>
            </xsl:otherwise>
          </xsl:choose>
        </td> -->
      </tr>
      <tr>
        <td colspan="3">&#160;</td>
      </tr>
      <tr>
        <td colspan="3">All fields marked with an asterisk (*) are required.</td>
      </tr>
      <tr>
        <td colspan="3">&#160;</td>
      </tr>
      <tr>
        <td colspan="3" valign="top">
          <img align="top" border="0" src="images/btn_save.gif" width="80" height="28" style="cursor:hand" title="Save">
            <xsl:attribute name="onclick">updateDetails()</xsl:attribute>
          </img>
          <img border="0" src="images/spacer.gif" WIDTH="30" HEIGHT="1"/>
          <img align="top" border="0" src="images/btn_cancel.gif" width="83" height="31" style="cursor:hand" title="Back to the User List">
            <xsl:attribute name="onclick">goBack()</xsl:attribute>
          </img>
          <!-- <xsl:choose>
            <xsl:when test="/Root/@UserID = '-1'"> -->
          <!-- </xsl:when>
            <xsl:otherwise>
              <img border="0" src="images/btn_update_transp.gif" width="80" height="28" style="cursor:hand" title="Update changes">
                <xsl:attribute name="onclick">updateDetails()</xsl:attribute>
              </img>
              <img border="0" src="images/btn_back.gif" width="80" height="28" style="cursor:hand" title="Back to the User List" onclick="goBack()"/>
            </xsl:otherwise>
          </xsl:choose> -->
        </td>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>
