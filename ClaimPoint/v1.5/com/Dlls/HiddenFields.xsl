<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="claimNumber" select="1"/>

<xsl:template match="Root">
	<xsl:apply-templates select="Claim[@LynxId = $claimNumber]"/>
</xsl:template>

<xsl:template match="Claim">
	<input type="hidden" name="hidFirstVehicle">
		<xsl:attribute name="value">
			<xsl:apply-templates select="ClaimVehicle[position() = 1]"/>
		</xsl:attribute>
	</input>

	<input type="hidden" name="hidFirstProperty">
		<xsl:attribute name="value">
			<xsl:apply-templates select="ClaimProperty[position() = 1]"/>
		</xsl:attribute>
	</input>
</xsl:template>

<xsl:template match="ClaimVehicle">
	<xsl:value-of select="@ClaimAspectID"/>
</xsl:template>

<xsl:template match="ClaimProperty">
	<xsl:value-of select="@ClaimAspectID"/>
</xsl:template>

</xsl:stylesheet>