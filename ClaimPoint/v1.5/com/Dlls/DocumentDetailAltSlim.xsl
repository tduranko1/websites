<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
         
  function ConvertTime(time)
  {
    var lsTime = time;
  	if (lsTime == null || lsTime == "") return "";
    var lsMO;
    var lsDD;
    var lsYYYY;
    var lsHH;
    var lsMM;
    var lsAmPm = 'AM';

    lsMO = lsTime.substr(5,2);
    lsDD = lsTime.substr(8,2);
    lsYYYY = lsTime.substr(0,4);

    var strDate = lsTime.substr( 5, 2 ) + "-"
                 + lsTime.substr( 8, 2 ) + "-"
                 + lsTime.substr( 0, 4 ) + " "

    if (lsTime.length <= 4)
    {
      lsHH = lsTime.substr(0,2);
      lsMM = lsTime.substr(2,2);
    }
    else if (lsTime.length > 4)
    {
      lsHH = lsTime.substr(11,2);
      lsMM = lsTime.substr(14,2);
    }
    
    if (lsHH >= 1 && lsHH < 10)
    {
      lsHH = lsHH.substr(1,1);
    }
    else if (lsHH == 12)
    {
      lsAmPm = 'PM';
    }
    else if (lsHH > 12)
    {
      lsHH = lsHH-12;
      lsAmPm = 'PM';
    }

    var lsRetTime = strDate + ' ' + lsHH + ':' + lsMM  + ' ' + lsAmPm;
    return lsRetTime;
  }

  ]]>
</msxsl:script>

<xsl:variable name="documentCount">
  <xsl:value-of select="count(Root/Document)" /> 
</xsl:variable>

<xsl:template match="/">

  <div style="font-size:12pt;font-weight:bold;color:#FF0000;padding:0px;padding-top:5px;">Estimates:</div>
  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="#EEEEEE">
    <thead>
		<TR>
			<TD class="theader">Date</TD>
			<TD class="theader">Doc Indicators</TD>
			<TD class="theader">Rcvd/Sent</TD>
			<TD class="theader">Source</TD>
			<TD class="theader">Type</TD>
			<TD class="theader">Pertains To</TD>
			<TD class="theader">Image</TD>
		</TR>
    </thead>

      <!-- <xsl:apply-templates/> -->
      <xsl:for-each select="/Root/Document">
        <xsl:sort select="@DocumentID" order="descending" data-type="number"/>
        <xsl:variable name="DocTypeID"><xsl:value-of select="@DocumentTypeID"/></xsl:variable>
        <xsl:if test="/Root/Reference[@List='DocumentType' and @ReferenceID=$DocTypeID and @EstimateTypeFlag='1']">
        <xsl:call-template name="Estimate"/>
        </xsl:if>
      </xsl:for-each>

    </TABLE>

  <xsl:variable name="nonEstiamteDocTypeID"><xsl:for-each select="/Root/Reference[@List='DocumentType' and @EstimateTypeFlag='0']">|<xsl:value-of select="@ReferenceID"/>|,</xsl:for-each></xsl:variable>
  <div style="font-size:12pt;font-weight:bold;color:#FF0000;padding:0px;padding-top:15px;">Documents:</div>
  <TABLE border="0" cellspacing="1" cellpadding="10">
      <xsl:for-each select="/Root/Document[contains($nonEstiamteDocTypeID, concat('|', @DocumentTypeID, '|'))]">
      <xsl:sort select="@DocumentID" order="descending" data-type="number"/>
        <xsl:if test="position()=1">
          <xsl:text disable-output-escaping="yes">&lt;</xsl:text>tr<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        </xsl:if>
        <xsl:call-template name="NonEstimate"/>
        <xsl:if test="position() mod 4 = 0">
          <xsl:text disable-output-escaping="yes">&lt;</xsl:text>/tr<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
          <xsl:text disable-output-escaping="yes">&lt;</xsl:text>tr<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        </xsl:if>
        <xsl:if test="position()=last()">
          <xsl:text disable-output-escaping="yes">&lt;</xsl:text>/tr<xsl:text disable-output-escaping="yes">&gt;</xsl:text>
        </xsl:if>
      </xsl:for-each>
  </TABLE>

</xsl:template>

<xsl:template name="Estimate">

  <xsl:if test="@ImageLocation != ''">

      <tr>
        <xsl:attribute name="bgcolor">
          <xsl:choose>
            <xsl:when test="position() mod 2 = 1">#FFFFFF</xsl:when>
            <xsl:when test="position() mod 2 = 0">#DFEFFF</xsl:when>
          </xsl:choose>
        </xsl:attribute>
        <td align="center">
          <xsl:value-of select="user:ConvertTime(string(@CreatedDate))"/>
        </td>

        <td align="center">
      	  <xsl:if test="@FinalEstimateFlag = '1'">Final Estimate<br/></xsl:if>
      	  <xsl:if test="@DirectionToPayFlag = '1'">Direction To Pay<br/></xsl:if>
      	  <xsl:if test="@EstimateTypeCD != ''">
            <xsl:variable name="estimateTypeCD"><xsl:value-of select="@EstimateTypeCD"/></xsl:variable>
            <xsl:value-of select="/Root/Reference[@List='EstimateType' and @ReferenceID=$estimateTypeCD]/@Name"/><br/>
          </xsl:if>
      	  <xsl:if test="@DuplicateFlag = '1'">Duplicate<br/></xsl:if>
        </td>

        <td align="center">
      	  <xsl:value-of select="@Directional"/>
        </td>
        <td align="center">
      	  <xsl:value-of select="@DocumentSource"/>
        </td>
        <td align="center">
          <xsl:value-of select="@DocumentType"/><xsl:if test="@DocumentType = 'Supplement'">&#160;#&#160;<xsl:value-of select="@SupplementSeqNumber"/></xsl:if>
        </td>
        <td align="center">
        	<xsl:variable name="claimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
          <xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ReferenceID=$claimAspectID]/@Name"/>
        </td>
        <td align="center">
          <xsl:variable name="ImageType">
            <xsl:value-of select="translate(substring-after(@ImageLocation, '.'), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
          </xsl:variable>
      		<a title="Click to view document/image">
      		<xsl:attribute name="href">
            JavaScript:goDocument("<xsl:value-of select="@ImageLocation"/>","<xsl:value-of select="@Archived"/>")
          </xsl:attribute>
          <xsl:choose>
            <xsl:when test="$ImageType = 'xml'">
              <img src="images/icon_xml.gif" border="0" width="28" height="26"/>
            </xsl:when>
            <xsl:when test="$ImageType = 'jpg'">
              <img src="images/icon_jpg.gif" border="0" width="28" height="26"/>
            </xsl:when>
            <xsl:when test="$ImageType = 'tif'">
              <img src="images/icon_tif.gif" border="0" width="28" height="26"/>
            </xsl:when>
            <xsl:when test="$ImageType = 'gif'">
              <img src="images/icon_gif.gif" border="0" width="28" height="26"/>
            </xsl:when>
            <xsl:when test="$ImageType = 'bmp'">
              <img src="images/icon_bmp.gif" border="0" width="28" height="26"/>
            </xsl:when>
            <xsl:when test="$ImageType = 'pdf'">
              <img src="images/icon_pdf.gif" border="0" width="26" height="26"/>
            </xsl:when>
            <xsl:when test="$ImageType = 'doc'">
              <img src="images/icon_doc.gif" border="0" width="28" height="26"/>
            </xsl:when>
            <xsl:otherwise>
              <img src="images/icon_any.gif" border="0" width="21" height="26"/>
            </xsl:otherwise>
          </xsl:choose>
      		</a>
        </td>
      </tr>
  </xsl:if>

</xsl:template>

<xsl:template name="NonEstimate">
    <td align="center" valign="top">
      <div style="width:100%;border:1px solid #C0C0C0;border-bottom:2px solid #696969; border-right:2px solid #696969">
        <table cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td align="center">
              <xsl:variable name="ImageType">
                <xsl:value-of select="translate(substring-after(@ImageLocation, '.'), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="$ImageType = 'xml'">
                  <xsl:call-template name="nonJPG">
                    <xsl:with-param name="ImageType" select="$ImageType"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$ImageType = 'jpg'">
                  <xsl:call-template name="JPG">
                    <xsl:with-param name="ImageType" select="$ImageType"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$ImageType = 'tif'">
                  <xsl:call-template name="nonJPG">
                    <xsl:with-param name="ImageType" select="$ImageType"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$ImageType = 'gif'">
                  <xsl:call-template name="JPG">
                    <xsl:with-param name="ImageType" select="$ImageType"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$ImageType = 'bmp'">
                  <xsl:call-template name="JPG">
                    <xsl:with-param name="ImageType" select="$ImageType"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$ImageType = 'pdf'">
                  <xsl:call-template name="nonJPG">
                    <xsl:with-param name="ImageType" select="$ImageType"/>
                  </xsl:call-template>
                </xsl:when> 
                <xsl:when test="$ImageType = 'doc'">
                  <xsl:call-template name="nonJPG">
                    <xsl:with-param name="ImageType" select="$ImageType"/>
                  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:call-template name="nonJPG">
                    <xsl:with-param name="ImageType">any</xsl:with-param>
                  </xsl:call-template>
                </xsl:otherwise>
              </xsl:choose>
           </td>
          </tr>
          <tr>
            <td align="center">
              <div style="font-size:7pt;"><xsl:value-of select="user:ConvertTime(string(@CreatedDate))"/></div>
              <xsl:if test="@FinalEstimateFlag = '1'">Final Estimate<br/></xsl:if>
              <xsl:if test="@DirectionToPayFlag = '1'">Direction To Pay<br/></xsl:if>
              <xsl:if test="@EstimateTypeCD != ''">
                <xsl:variable name="estimateTypeCD"><xsl:value-of select="@EstimateTypeCD"/></xsl:variable>
                <xsl:value-of select="/Root/Reference[@List='EstimateType' and @ReferenceID=$estimateTypeCD]/@Name"/><br/>
              </xsl:if>
              <xsl:if test="@DuplicateFlag = '1'">Duplicate<br/></xsl:if>
            </td>
          </tr>
        </table>
      </div>
    </td>
</xsl:template>
  
<xsl:template name="nonJPG">
  <xsl:param name="ImageType"/>
	<xsl:variable name="claimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
  		<a title="Click to view document/image">
    		<xsl:attribute name="onclick">JavaScript:goDocument('<xsl:value-of select="@ImageLocation"/>','<xsl:value-of select="@Archived"/>')</xsl:attribute>
        <xsl:attribute name="href">#</xsl:attribute>
        <div style="width:100px;height:80px;border:0px solid #000000;cursor:hand">
        <table border="0" cellpaddign="0" cellspacing="0" style="width:100%;height:100%">
		  	  <tr style="height:18px">
  				  <td style="background-color:#C0C0C0;font-size:8pt;text-align:center">
  					  <xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ReferenceID=$claimAspectID]/@Name"/>
  				  </td>
			    </tr>
          <tr valign="middle">
            <td align="center">
              <img border="0" >
                <xsl:attribute name="src">images/icon_<xsl:value-of select="$ImageType"/>.gif</xsl:attribute>
              </img>
              <br/>
              <div style="font-size:8pt"><xsl:value-of select="@DocumentType"/></div>
            </td>
          </tr>
        </table>
      </div>
     </a>
</xsl:template>

<xsl:template name="JPG">
  <xsl:param name="ImageType"/>
 	<xsl:variable name="claimAspectID"><xsl:value-of select="@ClaimAspectID"/></xsl:variable>
    <a title="Click to view document/image">
      <xsl:attribute name="onclick">JavaScript:goDocument('<xsl:value-of select="@ImageLocation"/>','<xsl:value-of select="@Archived"/>')</xsl:attribute>
      <xsl:attribute name="href">#</xsl:attribute>
      <div style="width:100px;height:80px;border:0px solid #000000;cursor:hand">
      <table border="0" cellpaddign="0" cellspacing="0" style="width:100%;height:100%">
		    <tr style="height:18px">
				  <td style="background-color:#C0C0C0;font-size:8pt;text-align:center">
					  <xsl:value-of select="/Root/Reference[@List='PertainsTo' and @ReferenceID=$claimAspectID]/@Name"/>
				  </td>
			  </tr>
        <tr valign="middle">
          <td align="center">
            <img border="0" >
              <xsl:attribute name="src">frmThumbnail.aspx?doc=<xsl:value-of select="@ImageLocation"/>&amp;Arc=<xsl:value-of select="@Archived"/></xsl:attribute>
            </img>
          </td>
        </tr>
      </table>
      </div>
    </a>
</xsl:template>

</xsl:stylesheet>