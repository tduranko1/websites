<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="/">

<tr><td colspan="5"><p class="mini">&#160;</p></td></tr>
<tr><td colspan="5">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse" bordercolor="#00099" height="48">
	<tr>
		<td width="10" valign="top" height="8">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
		<td width="8" valign="top" background="images/table_CL_white.gif" height="8">
			<img border="0" src="images/table_TL_white.gif" width="8" height="8"/>
		</td>
		<td background="images/table_TC_white.gif" height="8">
			<img border="0" src="images/spacer.gif" width="25" height="8"/>
		</td>
		<td width="8" background="images/table_CR_white.gif" valign="top" height="8">
			<img border="0" src="images/table_TR_white.gif" width="8" height="8"/>
		</td>
		<td width="10" valign="top" bordercolor="#FFFFFF" height="8">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
	</tr>
	<tr>
		<td width="10" height="31">&#160;</td>
		<td width="8" background="images/table_CL_white.gif" height="31">&#160;</td>
		<td background="images/lt_white.gif" nowrap="">
			<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
				<tr>
					<td style="border-left-width: 1; border-right-style: solid; border-right-width: 1; border-top-width: 1; border-bottom-style: solid; border-bottom-width: 1">
						<p class="bodyBlue">Date Completed:</p>
					</td>
					<td style="border-left-width: 1; border-right-style: solid; border-right-width: 1; border-top-width: 1; border-bottom-style: solid; border-bottom-width: 1">
						<p class="bodyBlue">User:</p>
					</td>
					<td style="border-left-width: 1; border-right-style: solid; border-right-width: 1; border-top-width: 1; border-bottom-style: solid; border-bottom-width: 1">
						<p class="bodyBlue">Activity:</p>
					</td>
					<td style="border-left-width: 1; border-right-style: solid; border-right-width: 1; border-top-width: 1; border-bottom-style: solid; border-bottom-width: 1">
						<p class="bodyBlue">Pertains To:</p>
					</td>
					<td style="border-left-width: 1; border-right-width: 1; border-top-width: 1; border-bottom-style: solid; border-bottom-width: 1">
						<p class="bodyBlue">Description:</p>
					</td>
				</tr>
				<xsl:apply-templates/>
			</table>
		</td>
		<td width="8" background="images/table_CR_white.gif" height="31">&#160;</td>
		<td width="10" bordercolor="#FFFFFF" height="31">&#160;</td>
	</tr>
	<tr>
		<td width="10" valign="bottom" height="9">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
		<td width="8" background="images/table_CL_white.gif" valign="bottom" height="9">
			<img border="0" src="images/table_BL_white.gif" width="8" height="8"/>
		</td>
		<td class="cell" height="9" background="images/lt_white.gif" >
			<img border="0" src="images/spacer.gif" width="25" height="8"/>
		</td>
		<td width="8" background="images/table_CR_white.gif" valign="bottom" height="9">
			<img border="0" src="images/table_BR_white.gif" width="8" height="8"/>
		</td>
		<td width="10" valign="bottom" bordercolor="#FFFFFF" height="9">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
	</tr>
</table>
</td></tr>	
</xsl:template>

<xsl:template match="History">
				<tr>
					<td style="border-left-width: 1; border-right-style: solid; border-right-width: 1; border-top-width: 1; border-bottom-width: 1">
						<p class="body">
						<xsl:value-of select="substring(@CompletedDate, 6, 2)"/>/<xsl:value-of select="substring(@CompletedDate, 9, 2)"/>/<xsl:value-of select="substring(@CompletedDate, 1, 4)"/></p>
					</td>
					<td style="border-left-width: 1; border-right-style: solid; border-right-width: 1; border-top-width: 1; border-bottom-width: 1">
						<p class="body">
						<xsl:value-of select="concat(@CompletedUserNameFirst, concat(' ', @CompletedUserNameLast))"/></p>
					</td>
					<td style="border-left-width: 1; border-right-style: solid; border-right-width: 1; border-top-width: 1; border-bottom-width: 1">
						<p class="body">
						<xsl:value-of select="concat(@TaskName,@EventName)"/></p>
					</td>
					<td style="border-left-width: 1; border-right-style: solid; border-right-width: 1; border-top-width: 1; border-bottom-width: 1">
						<p class="body">
						<xsl:value-of select="@PertainsTo"/></p>
					</td>
					<td>
						<p class="body">
						<xsl:value-of select="@Description"/></p>
					</td>
				</tr>
</xsl:template>

</xsl:stylesheet>