<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

  <xsl:param name="OfficeID"/>
  <xsl:param name="tabIndexNo"/>
  <xsl:param name="ShowAllStates"/>
  
  <xsl:template match="Root">
  
  <input name="AssignmentAtSelectionFlag" id="txtAssignmentAtSelectionFlag" type="hidden">
    <xsl:attribute name="value"><xsl:value-of select="@AssignmentAtSelectionFlag"/></xsl:attribute>
  </input>
  <input name="DemoFlag" id="txtDemoFlag" type="hidden">
    <xsl:attribute name="value"><xsl:value-of select="@DemoFlag"/></xsl:attribute>
  </input>
  
    <select name="LossState" id="txtLossAddressState" required="true">
  		<xsl:if test="$tabIndexNo != ''">
        <xsl:attribute name="tabIndex">
    			<xsl:value-of select="$tabIndexNo"/>
    		</xsl:attribute>
  		</xsl:if>
      <option value=""></option>
  
      <xsl:choose>
        <xsl:when test="$ShowAllStates = 1">
          <xsl:for-each select="Reference[@List = 'ContractState']" >
            <xsl:call-template name="getAllStates"/>
          </xsl:for-each>
        </xsl:when>

        <xsl:when test="$OfficeID = 0">
          <xsl:for-each select="ContractStates" >
            <xsl:call-template name="getContractedStates"/>
          </xsl:for-each>
        </xsl:when>

        <xsl:otherwise>
          <xsl:for-each select="Office/OfficeContractStates[../@OfficeID = $OfficeID]" >
            <xsl:call-template name="getContractStates"/>
          </xsl:for-each>
        </xsl:otherwise>
      </xsl:choose>

    </select>
  
  </xsl:template>
  
  <xsl:template name="getAllStates">
  
  	<option>
  		<xsl:attribute name="value">
  			<xsl:value-of select="@ReferenceID"/>
  		</xsl:attribute>
  		  <xsl:value-of select="@Name"/>
  	</option>
  
  </xsl:template>

  <xsl:template name="getContractedStates">
  
  	<option>
      <xsl:variable name="currentST"><xsl:value-of select="@StateCode"/></xsl:variable>
  		<xsl:attribute name="value">
  			<xsl:value-of select="$currentST"/>
  		</xsl:attribute>
        <xsl:value-of select="/Root/Reference[@List = 'ContractState' and @ReferenceID = $currentST]/@Name"/>
  	</option>
  
  </xsl:template>

  <xsl:template name="getContractStates">
  
  	<option>
      <xsl:variable name="currentST"><xsl:value-of select="@StateCode"/></xsl:variable>
  		<xsl:attribute name="value">
  			<xsl:value-of select="$currentST"/>
  		</xsl:attribute>
        <xsl:value-of select="/Root/Reference[@List = 'ContractState' and @ReferenceID = $currentST]/@Name"/>
  	</option>
  
  </xsl:template>

</xsl:stylesheet>

