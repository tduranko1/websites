<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="Root">

  <xsl:comment>v1.4.2.0</xsl:comment>

  <table border="0" cellpadding="0" cellspacing="0" width="600" style="padding:2px; border:1px solid #000099; border-collapse:collapse">

  <xsl:choose>

    <xsl:when test="count(InsuranceCompany) &gt; 00">
      <xsl:for-each select="InsuranceCompany" >
        <xsl:sort select="@Name"/>

          <xsl:choose>
            <xsl:when test="position() mod 2 = 1">
              <xsl:call-template name="getInsCoListBlue"/>
            </xsl:when>
            <xsl:when test="position() mod 2 = 0">
              <xsl:call-template name="getInsCoListWhite"/>
            </xsl:when>
          </xsl:choose>

      </xsl:for-each>
    </xsl:when>

    <xsl:otherwise>
      <tr>
        <td nowrap="">
          <p class="bodyBlue">No Insurance Companies Configured.</p>
        </td>
      </tr>
    </xsl:otherwise>
  </xsl:choose>

  </table>

  </xsl:template>
  
  <xsl:template name="getInsCoListWhite">
    <tr>
      <td nowrap="" class="cell">
        <p class="bodyBlue"><xsl:value-of select="@Name"/></p>
      </td>
      <td class="cell" width="20%">
        <a class="subNav">
          <xsl:attribute name="href">
            JavaScript:setInsuranceCompanyID(<xsl:value-of select="@InsuranceCompanyID"/>, '<xsl:value-of select="@Name"/>')
          </xsl:attribute>
          <img border="0" src="images/btn_select.gif" width="83" height="31"/>
        </a>
      </td>
    </tr>
  </xsl:template>

  <xsl:template name="getInsCoListBlue">
    <tr>
      <td class="cell" nowrap="" style="background-color:#E7E7F7;">
        <p class="bodyBlue"><xsl:value-of select="@Name"/></p>
      </td>
      <td style="background-color:#E7E7F7;" class="cell" width="20%">
        <a class="subNav">
          <xsl:attribute name="href">
            JavaScript:setInsuranceCompanyID(<xsl:value-of select="@InsuranceCompanyID"/>, '<xsl:value-of select="@Name"/>')
          </xsl:attribute>
          <img border="0" src="images/btn_select_blue.gif" width="83" height="31"/>
        </a>
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
