<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

  <xsl:param name="EmailCell"/>

  <xsl:template match="Vehicle">

    <xsl:comment>v1.5.0.0</xsl:comment>

    <tr>
      <td colspan="5">
        <p class="headerRedData">LDescription:</p>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Year:
      </td>
      <td class="body">
        <xsl:value-of select="@VehicleYear"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        VIN:
      </td>
      <td class="body">
        <xsl:value-of select="@VIN"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Make:
      </td>
      <td class="body">
        <xsl:value-of select="@Make"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Odometer:
      </td>
      <td class="body">
        <xsl:value-of select="@Mileage"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Model:
      </td>
      <td class="body">
        <xsl:value-of select="@Model"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Color:
      </td>
      <td class="body">
        <xsl:value-of select="@Color"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Body:
      </td>
      <td class="body">
        <xsl:value-of select="@BodyStyle"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        License Plate:
      </td>
      <td class="body">
        <xsl:value-of select="@LicensePlateNumber"/>
        <xsl:if test="string-length(translate(normalize-space(@LicensePlateState),' ','')) &gt; 0">
          / <xsl:value-of select="@LicensePlateState"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" valign="top">
        Coverage:
      </td>
      <td class="body" valign="top">
        <xsl:value-of select="@CoverageProfile"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue" valign="top">
        Rental Days Authorized:
      </td>
      <td class="body" valign="top">
        <xsl:value-of select="@RentalDaysAuthorized"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" valign="top">
        Rental Instructions:
      </td>
      <td class="body" colspan="4" valign="top">
        <xsl:value-of select="@RentalInstructions"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue" valign="top">
        Remarks to Claim Rep:
      </td>
      <td class="body" colspan="4" valign="top">
        <xsl:value-of select="@Remarks"/>
      </td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
    </tr>
    <tr>
      <td colspan="5">
        <p class="headerRedData">Vehicle Location:</p>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Location Name:
      </td>
      <td class="body">
        <xsl:value-of select="@LocationName"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue" valign="top">
        Address:
      </td>
      <td rowspan="2" class="body" valign="top">
        <xsl:value-of select="@LocationAddress1"/>
        <br/>
        <xsl:if test="@LocationAddress2 != ''">
          <xsl:value-of select="@LocationAddress2"/>
          <br/>
        </xsl:if>
        <xsl:value-of select="@LocationCity"/>
        <xsl:if test="string-length(translate(normalize-space(@LocationCity),' ','')) &gt; 0 and 

string-length(translate(normalize-space(@LocationState),' ',''))">
          <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:value-of select="concat(@LocationState, ' ')"/>
        <xsl:value-of select="@LocationZip"/>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Phone:
      </td>
      <td class="body">
        <xsl:value-of select="@LocationAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(@LocationAreaCode),' ','')) &gt; 0 and 

string-length(translate(normalize-space(@LocationExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="@LocationExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(@LocationExchangeNumber),' ','')) &gt; 0 and 

string-length(translate(normalize-space(@LocationUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="@LocationUnitNumber"/>
        <xsl:if test="string-length(@LocationExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', @LocationExtensionNumber)"/>
        </xsl:if>
      </td>
      <td width="20"></td>
      <td></td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
    </tr>
    <tr>
      <td colspan="5" height="20">
        <p class="headerRedData">Vehicle Contact:</p>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Name:
      </td>
      <td class="body">
        <xsl:value-of select="concat(Contact/@NameTitle, ' ')"/>
        <xsl:value-of select="concat(Contact/@NameFirst, ' ')"/>
        <xsl:value-of select="Contact/@NameLast"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Day Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Contact/@DayAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@DayAreaCode),' ','')) &gt; 0 and 

string-length(translate(normalize-space(Contact/@DayExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@DayExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@DayExchangeNumber),' ','')) &gt; 0 and 

string-length(translate(normalize-space(Contact/@DayUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@DayUnitNumber"/>
        <xsl:if test="string-length(Contact/@DayExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Contact/@DayExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Address:
      </td>
      <td class="body" rowspan="2">
        <xsl:value-of select="Contact/@Address1"/>
        <br/>
        <xsl:if test="Contact/@Address2 != ''">
          <xsl:value-of select="Contact/@Address2"/>
          <br/>
        </xsl:if>
        <xsl:value-of select="Contact/@AddressCity"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@AddressCity),' ','')) &gt; 0 and 

string-length(translate(normalize-space(Contact/@AddressState),' ',''))">
          <xsl:text>, </xsl:text>
        </xsl:if>
        <xsl:value-of select="concat(Contact/@AddressState, ' ')"/>
        <xsl:value-of select="Contact/@AddressZip"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Night Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Contact/@NightAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@NightAreaCode),' ','')) &gt; 0 and 

string-length(translate(normalize-space(Contact/@NightExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@NightExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@NightExchangeNumber),' ','')) &gt; 0 and 

string-length(translate(normalize-space(Contact/@NightUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@NightUnitNumber"/>
        <xsl:if test="string-length(Contact/@NightExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Contact/@NightExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">&#160;</td>
      <td width="20"></td>
      <td class="bodyBlue">
        Alt. Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Contact/@AlternateAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@AlternateAreaCode),' ','')) &gt; 0 and 

string-length(translate(normalize-space(Contact/@AlternateExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@AlternateExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@AlternateExchangeNumber),' ','')) &gt; 0 and 

string-length(translate(normalize-space(Contact/@AlternateUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@AlternateUnitNumber"/>
        <xsl:if test="string-length(Contact/@AlternateExtensionNumber) &gt; 0">
          <xsl:value-of select="concat(' x', Contact/@AlternateExtensionNumber)"/>
        </xsl:if>
      </td>
    </tr>

    <tr>
      <td class="bodyBlue">
        Relation to Insured:
      </td>
      <td class="body">
        <xsl:value-of select="/Root/Reference[@List='ContactRelationToInsured' and 

@ReferenceID=current()/Contact/@InsuredRelationID]/@Name"/>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Cell Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Contact/@CellAreaCode"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@CellAreaCode),' ','')) &gt; 0 and 

string-length(translate(normalize-space(Contact/@CellExchangeNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@CellExchangeNumber"/>
        <xsl:if test="string-length(translate(normalize-space(Contact/@CellExchangeNumber),' ','')) &gt; 0 and 

string-length(translate(normalize-space(Contact/@CellUnitNumber),' ',''))">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@CellUnitNumber"/>
      </td>
    </tr>

    <tr>
      <td class="bodyBlue">
        <xsl:if test="$EmailCell = 'true'">
          <xsl:if test="@InitialAssignmentTypeID = 4 or @InitialAssignmentTypeID = 16">
            E-Mail:
          </xsl:if>
        </xsl:if>
      </td>
      <td class="body">
        <xsl:if test="$EmailCell = 'true'">
          <xsl:if test="@InitialAssignmentTypeID = 4 or @InitialAssignmentTypeID = 16">
            <xsl:value-of select="Contact/@EmailAddress"/>
          </xsl:if>
        </xsl:if>
      </td>
      <td width="20"></td>
      <td class="bodyBlue">
        Best Phone:
      </td>
      <td class="body">
        <xsl:value-of select="Contact/@BestContactPhone"/>
        <xsl:if test="Contact/@BestContactTime != ''">
          <xsl:text>-</xsl:text>
        </xsl:if>
        <xsl:value-of select="Contact/@BestContactTime"/>
      </td>
    </tr>

    <tr>
      <xsl:choose>
        <xsl:when test="$EmailCell = 'true'">
          <xsl:if test="@InitialAssignmentTypeID = 4 or @InitialAssignmentTypeID = 16">
            <td class="bodyBlue">
              Preferred Method of Status Updates:
            </td>
            <td class="body">
              <xsl:value-of select="/Root/Reference[@List='PrefMethodUpd' and 

@ReferenceID=current()/Contact/@PrefMethodUpd]/@Name"/>
            </td>
          </xsl:if>
        </xsl:when>
        <xsl:otherwise>
          <td width="20"></td>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:choose>
        <xsl:when test="$EmailCell = 'true'">
          <xsl:if test="@InitialAssignmentTypeID = 4 or @InitialAssignmentTypeID = 16">
            <td width="20" ></td>
          </xsl:if>
        </xsl:when>
        <xsl:otherwise>
          <td width="20" colspan="2"></td>
        </xsl:otherwise>
      </xsl:choose>
    </tr>

    <xsl:if test="$EmailCell = 'true'">
      <xsl:if test="@InitialAssignmentTypeID = 4 or @InitialAssignmentTypeID = 16">
        <tr>
          <td class="bodyBlue">
            Cell Phone Carrier:
          </td>
          <td class="body" colspan="4">
            <xsl:value-of select="/Root/Reference[@List='CellPhoneCarrier' and 

@ReferenceID=current()/Contact/@CellPhoneCarrier]/@Name"/>
          </td>
        </tr>
      </xsl:if>
    </xsl:if>
    <tr>
      <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
    </tr>
    <tr>
      <td colspan="5" height="20">
        <p class="headerRedData">Damage Information:</p>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Driveable?
      </td>
      <td class="body">
        <xsl:choose>
          <xsl:when test="@DriveableFlag = '1'">Yes</xsl:when>
          <xsl:otherwise>No</xsl:otherwise>
        </xsl:choose>
      </td>
      <td width="20"></td>
      <td class="bodyBlue"></td>
      <td class="body">
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Primary Damage:
      </td>
      <td class="body" colspan="4">
        <xsl:if test="count(Impact) &gt; 0">
          <xsl:variable name="PrimaryImpactList">
            <xsl:call-template name="commaDelimitList">
              <xsl:with-param name="list" select="Impact[@PrimaryImpactFlag = '1']/@Name"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:value-of select="substring($PrimaryImpactList, 1, string-length($PrimaryImpactList) - 2)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Impact Area:
      </td>
      <td class="body" colspan="4">
        <xsl:if test="count(Impact) &gt; 0">
          <xsl:variable name="CurrentImpactList">
            <xsl:call-template name="commaDelimitList">
              <xsl:with-param name="list" select="Impact[@CurrentImpactFlag = '1']/@Name"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:value-of select="substring($CurrentImpactList, 1, string-length($CurrentImpactList) - 2)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td class="bodyBlue">
        Prior Damage:
      </td>
      <td class="body" colspan="4">
        <xsl:if test="count(Impact) &gt; 0">
          <xsl:variable name="priorImpactList">
            <xsl:call-template name="commaDelimitList">
              <xsl:with-param name="list" select="Impact[@PriorImpactFlag = '1']/@Name"/>
            </xsl:call-template>
          </xsl:variable>
          <xsl:value-of select="substring($priorImpactList, 1, string-length($priorImpactList) - 2)"/>
        </xsl:if>
      </td>
    </tr>
    <tr>
      <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
    </tr>
    <tr>
      <td colspan="5" height="20">
        <p class="headerRedData">Service Channels Summary:</p>
      </td>
    </tr>
    <tr>
      <td colspan="5">

        <table width="100%" border="0" cellspacing="1" cellpadding="4" bgcolor="#d0d0d0">
          <thead>
            <TR>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;">Channel</TD>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;">Status</TD>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;" nowrap="">Estimate Amt.</TD>
              <TD style="font-weight:bold; text-align:center; padding:2px; background:#FFFFFF; border: 1px solid #d0d0d0;" nowrap="">
                Deductible/Amt.

                Applied
              </TD>
            </TR>
          </thead>

          <xsl:apply-templates select="ClaimAspectServiceChannel" />

        </table>

      </td>
    </tr>
    <xsl:if test="@InitialAssignmentTypeID = 14">
      <!-- Total Loss -->
      <tr>
        <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
      </tr>
      <tr>
        <td colspan="5" height="20">
          <p class="headerRedData">Settlement Information:</p>
        </td>
      </tr>
      <tr>
        <td colspan="2" class="bodyBlue">
          Settlement Date:
        </td>
        <td class="body" colspan="3">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@SettlementDate"/>
        </td>
      </tr>
      <tr>
        <td colspan="2" class="bodyBlue">
          Settlement Amount:
        </td>
        <td class="body" colspan="3">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@SettlementAmount"/>
        </td>
      </tr>
      <tr>
        <td colspan="2" class="bodyBlue">
          Advance Amount:
        </td>
        <td class="body" colspan="3">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@AdvanceAmount"/>
        </td>
      </tr>
      <tr>
        <td colspan="2" class="bodyBlue">
          Letter of Guarantee Amount:
        </td>
        <td class="body" colspan="3">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LoGAmount"/>
        </td>
      </tr>
      <tr>
        <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
      </tr>
      <tr>
        <td colspan="5" height="20">
          <p class="headerRedData">Lien Holder Information:</p>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Name:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LHName"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Address:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LHAddress1"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Phone:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LHPhone"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Fax:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LHFax"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Email:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LHEmailAddress"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Contact Name:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LHContactName"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Payoff Amount:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LHPayoffAmount"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Payoff Expiration Date:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LHPayoffExpirationDate"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Account Number:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@LHAccountNumber"/>
        </td>
      </tr>
      <tr>
        <td colspan="5" height="20">
          <p class="headerRedData">Salvage Vendor Information:</p>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Name:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@SalvageName"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Address:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@SalvageAddress1"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Phone:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@SalvagePhone"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Fax:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@SalvageFax"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Email:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@SalvageEmailAddress"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Contact Name:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@SalvageContactName"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Account Number:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@SalvageControlNumber"/>
        </td>
      </tr>
      <tr>
        <td colspan="5" style="border-bottom: 1px dotted #000099;">&#160;</td>
      </tr>
      <tr>
        <td colspan="5" height="20">
          <p class="headerRedData">Title Information:</p>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Title Name(s):
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@TitleName"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Title State:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@TitleState"/>
        </td>
      </tr>
      <tr>
        <td class="bodyBlue">
          Status:
        </td>
        <td class="body" colspan="4">
          <xsl:value-of select="ClaimAspectServiceChannel[@ServiceChannelCD='TL']/Settlement/@TitleStatus"/>
        </td>
      </tr>
    </xsl:if>
  </xsl:template>

  <xsl:template match="Impact">

    <xsl:if test="@CurrentImpactFlag = '1'">
      <tr>
        <td class="body" colspan="2">
          &#160;&#160;<xsl:value-of select="@Name"/>
        </td>
        <td>
          <br/>
        </td>
        <td class="body">
          &#160;&#160;
          <xsl:choose>
            <xsl:when test="@PrimaryImpactFlag = '1'">Yes</xsl:when>
            <xsl:otherwise>No</xsl:otherwise>
          </xsl:choose>
        </td>
        <td>
          <br/>
        </td>
      </tr>
    </xsl:if>

  </xsl:template>

  <xsl:template name="commaDelimitList">

    <xsl:param name="list"/>
    <xsl:if test="$list">
      <xsl:variable name="first" select="$list[1]"/>
      <xsl:variable name="rest">
        <xsl:call-template name="commaDelimitList">
          <xsl:with-param name="list" select="$list[position()!=1]"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:value-of select="concat(concat($first, ', '), $rest)"/>
    </xsl:if>

  </xsl:template>


  <xsl:template match="ClaimAspectServiceChannel">

    <tr bgcolor="#FFFFFF">
      <td style="border: 1px solid #d0d0d0;">
        <xsl:value-of select="@ServiceChannelName"/>
      </td>
      <td align="center" style="border: 1px solid #d0d0d0;">
        <xsl:value-of select="@StatusName"/>
      </td>
      <td align="center" style="border: 1px solid #d0d0d0;">
        <xsl:value-of select="@CurEstGrossRepairTotal"/>
      </td>
      <td align="center" style="border: 1px solid #d0d0d0;">
        <xsl:for-each select="Coverage[@DeductibleAppliedAmt != '' or @LimitAppliedAmt != '']">
          <xsl:sort select="@ClientCode" data-type="text" order="ascending"/>
          <xsl:sort select="@CoverageTypeCD" data-type="text" order="ascending"/>
          <xsl:choose>
            <xsl:when test="@ClientCode != ''">
              <xsl:value-of select="@ClientCode"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="@CoverageTypeCD"/>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;-<xsl:text

disable-output-escaping="yes">&amp;</xsl:text>nbsp;
          <xsl:value-of select="concat(@DeductibleAppliedAmt, ' / ', @DeductibleAmt)"/>
          <xsl:if test="@LimitAppliedAmt != '' and number(@LimitAppliedAmt) &gt; 0">
            <xsl:text disable-output-escaping="yes">&amp;</xsl:text>nbsp;
            <xsl:value-of select="concat('Limit: ', @LimitAppliedAmt)"/>
          </xsl:if>
          <br/>
        </xsl:for-each>
      </td>

    </tr>

  </xsl:template>

</xsl:stylesheet>
