<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="AssignmentType"/>
<xsl:param name="PageName"/>
<xsl:param name="pageTitle"/>
<xsl:param name="AspectID"/>

<xsl:template match="Claim">

  <xsl:comment>v1.5.0.0</xsl:comment>

  <tr>
    <td class="bodyBlue">
      Assignment:
    </td>
    <td class="body">
      <xsl:value-of select="$AssignmentType"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Policy No.:
    </td>
    <td class="body">
      <xsl:value-of select="@PolicyNumber"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Date of Loss:
    </td>
    <td class="body">
      <xsl:value-of select="substring(@LossDate, 6, 2)"/>-<xsl:value-of select="substring(@LossDate, 9, 2)"/>-<xsl:value-of select="substring(@LossDate, 1, 4)"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Date of Notice:
    </td>
    <td class="body" >
      <xsl:value-of select="substring(@IntakeStartDate, 6, 2)"/>-<xsl:value-of select="substring(@IntakeStartDate, 9, 2)"/>-<xsl:value-of select="substring(@IntakeStartDate, 1, 4)"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Exposure:
    </td>
    <td class="body" >
      <xsl:variable name="ExposureCD"><xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@ExposureCD"/></xsl:variable>
      <xsl:if test="$ExposureCD = 1">1st Party</xsl:if>
      <xsl:if test="$ExposureCD = 3">3rd Party</xsl:if>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      LYNX Rep.:
    </td>
    <td class="body" >
      <xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@ClaimOwnerNameFirst"/>&#xa0;<xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@ClaimOwnerNameLast"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Vehicle <xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@VehicleNumber"/>:
    </td>
    <td class="body" >
      <xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@VehicleYear"/>&#xa0;<xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@Make"/>&#xa0;<xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@Model"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Phone:
    </td>
    <td class="body" >
      <xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@ClaimOwnerAreaCode"/>-<xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@ClaimOwnerExchangeNumber"/>-<xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@ClaimOwnerUnitNumber"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Veh Owner:
    </td>
    <td class="body" >
      <xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@VehicleOwnerNameFirst"/>&#xa0;<xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@VehicleOwnerNameLast"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      E-mail:
    </td>
    <td class="body">
      <xsl:value-of select="/Root/Vehicle[@ClaimAspectID=$AspectID]/@ClaimOwnerEmail"/>
    </td>
  </tr>

</xsl:template>

</xsl:stylesheet>