<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />
    
<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
         
        function getAge( nodeList )
        {
			if (nodeList != null)
			{
				var nde = nodeList.nextNode().firstChild;
            
				if (nde != null)
				{
					var lsBirthDate = nde.nodeTypedValue;
					
					if (lsBirthDate != "" && lsBirthDate != null)
					{
						var ldToday = new Date();
						var ldBirthDate = new Date(parseInt(lsBirthDate.substr(0, 4)), parseInt(lsBirthDate.substr(5, 2)) - 1, parseInt(lsBirthDate.substr(8, 2)));
						var liAge = Math.floor((ldToday - ldBirthDate)/24/60/60/1000/365.25);
						return liAge;
					}
				}
            }
            return "N/A";
        }    
  ]]>
</msxsl:script>

<xsl:param name="callNumber" select="0"/>

<xsl:template match="Pedestrian">

<tr><td colspan="5"><p class="mini">&#160;</p></td></tr>
<tr>
	<td nowrap="" colspan="2">
		<p class="headerRedData">
		Pedestrian</p>
	</td>
	<td width="20">
		&#160;
	</td>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td nowrap="" class="body">
		&#160;
	</td>
</tr>
<tr>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Name:</p>
	</td>
	<td class="body">
		<p class="resultData">
		<xsl:value-of select="concat(@NameTitle, ' ')"/> <xsl:value-of select="concat(@NameFirst, ' ')"/> <xsl:value-of select="@NameLast"/></p>
	</td>
	<td width="20">
		&#160;
	</td>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Gender:</p>
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="@Gender"/>
	</td>
</tr>
<tr>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Address:</p>
	</td>
	<td nowrap="" class="body">
		<p class="resultData">
		<xsl:value-of select="@Address1"/></p>
	</td>
	<td width="20">
	</td>
	<td nowrap="" width="125">
		<p class="bodyBlue">SSN:</p>
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="@FedTaxId"/>
	</td>
</tr>
<tr>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td class="body">
		<xsl:value-of select="@Address2"/>
	</td>
	<td width="20">
		<img border="0" src="images/spacer.gif" width="8" height="8"/>
	</td>
	<td nowrap="" width="125">
		<p class="bodyBlue">DOB:</p>
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="substring(@BirthDate, 6, 2)"/>/<xsl:value-of select="substring(@BirthDate, 9, 2)"/>/<xsl:value-of select="substring(@BirthDate, 1, 4)"/>
	</td>
</tr>
<tr>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td class="body">
		<p class="resultData">
		<xsl:value-of select="concat(@AddressCity, ', ')"/><xsl:value-of select="concat(@AddressState, '  ')"/><xsl:value-of select="@AddressZip"/></p>
	</td>
	<td width="20">
		&#160;
	</td>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Age:</p>
	</td>
	<td nowrap="" class="body">
		<input type="hidden" name="hidBirthDate" id="hidBirthDate">
			<xsl:attribute name="value">
				<xsl:value-of select="substring(@BirthDate, 6, 2)"/>/<xsl:value-of select="substring(@BirthDate, 9, 2)"/>/<xsl:value-of select="substring(@BirthDate, 1, 4)"/>
			</xsl:attribute>
		</input>
		<span>
			<xsl:attribute name="id">spanAge<xsl:value-of select="$callNumber"/></xsl:attribute>
			<xsl:value-of select="user:getAge(@BirthDate)"/>
		</span>
	</td>
</tr>
<tr>
	<td nowrap="">&#160;</td>
	<td nowrap="">&#160;</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">&#160;</td>
	<td nowrap="" class="body">&#160;</td>
</tr>
<tr>
	<td nowrap="">
		<p class="resultCaption">
		Injured?</p>
	</td>
	<td nowrap="">
		<p class="body">
		<xsl:value-of select="@Injured"/></p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">
		<p class="resultCaption">Day Phone:</p>
	</td>
	<td nowrap="" class="body">
		(<xsl:value-of select="@DayAreaCode"/>) <xsl:value-of select="concat(' ', @DayExchangeNumber)"/> - <xsl:value-of select="@DayUnitNumber"/>
		<xsl:if test="string-length(@DayExtensionNumber) &gt; 0 and @DayExtensionNumber != '   '">
			<xsl:value-of select="concat(' x', @DayExtensionNumber)"/>
		</xsl:if>
	</td>
</tr>
<tr>
	<td>
		<p class="bodyBlue">Injury:</p>
	</td>
	<td>
		<p class="body">
		<xsl:value-of select="Injury/@InjuryType"/></p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">
		<p class="bodyBlue">Night Phone:</p>
	</td>
	<td nowrap="" class="body">
		(<xsl:value-of select="@NightAreaCode"/>) <xsl:value-of select="concat(' ', @NightExchangeNumber)"/> - <xsl:value-of select="@NightUnitNumber"/>
		<xsl:if test="string-length(@NightExtensionNumber) &gt; 0">
			<xsl:value-of select="concat(' x', @NightExtensionNumber)"/>
		</xsl:if>
	</td>
</tr>
<tr>
	<td valign="top">
		<p class="bodyBlue">Description:</p>
	</td>
	<td>
		<p class="body">
		<xsl:value-of select="Injury/@InjuryDescription"/></p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">
		<p class="bodyBlue">Alt. Phone:</p>
	</td>
	<td nowrap="" class="body">
		(<xsl:value-of select="@AlternateAreaCode"/>) <xsl:value-of select="concat(' ', @AlternateExchangeNumber)"/> - <xsl:value-of select="@AlternateUnitNumber"/>
		<xsl:if test="string-length(@AlternateExtensionNumber) &gt; 0">
			<xsl:value-of select="concat(' x', @AlternateExtensionNumber)"/>
		</xsl:if>
	</td>
</tr>
<tr>
	<td>
		&#160;
	</td>
	<td>
		&#160;
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td nowrap="" class="body">&#160;</td>
</tr>
<tr>
	<td>
		<p class="bodyBlue">Violation?</p>
	</td>
	<td>
		<p class="body">
		<xsl:choose>
			<xsl:when test="@ViolationFlag = '1'">Yes</xsl:when>
			<xsl:otherwise>No</xsl:otherwise>
		</xsl:choose></p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">
		<p class="bodyBlue">Best Phone #:</p>
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="@BestContactPhone"/>
	</td>
</tr>
<tr>
	<td valign="top">
		<p class="bodyBlue">Description:</p>
	</td>
	<td>
		<p class="body">
		<xsl:value-of select="@ViolationDescription"/></p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">
		<p class="bodyBlue">Best Time:</p>
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="@BestContactTime"/>
	</td>
</tr>
<tr>
	<td colspan="5"><hr noshade="" color="#000099" size="1" /></td>
</tr>
<tr>
	<td nowrap="" colspan="2">
		<p class="headerRedData">Employer</p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">&#160;</td>
	<td nowrap="" class="body">&#160;</td>
</tr>
<tr>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Name:</p>
	</td>
	<td class="body">
		<p class="resultData">
		<xsl:value-of select="Employer/@EmployerBusinessName"/></p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Address:</p>
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="Employer/@EmployerAddress1"/>
	</td>
</tr>
<tr>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Phone:</p>
	</td>
	<td nowrap="" class="body">
		<p class="resultData">
		(<xsl:value-of select="Employer/@EmployerDayAreaCode"/>) <xsl:value-of select="concat(' ', Employer/@EmployerDayExchangeNumber)"/> - <xsl:value-of select="Employer/@EmployerDayUnitNumber"/>
		<xsl:if test="string-length(Employer/@EmployerDayExtensionNumber) &gt; 0">
			<xsl:value-of select="concat(' x', Employer/@EmployerDayExtensionNumber)"/>
		</xsl:if></p>
	</td>
	<td width="20"></td>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="Employer/@EmployerAddress2"/>
	</td>
</tr>
<tr>
	<td nowrap="" width="125">&#160;</td>
	<td class="body">&#160;</td>
	<td width="20">
		<img border="0" src="images/spacer.gif" width="8" height="8"/>
	</td>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="concat(Employer/@EmployerAddressCity, ', ')"/><xsl:value-of select="concat(Employer/@EmployerAddressState, '  ')"/><xsl:value-of select="Employer/@EmployerAddressZip"/>
	</td>
</tr>
<tr>
	<td colspan="5"><hr noshade="" color="#000099" size="1" /></td>
</tr>
<tr>
	<td nowrap="" colspan="2">
		<p class="headerRedData">Doctor/Hospital</p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">&#160;</td>
	<td nowrap="" class="body">&#160;</td>
</tr>
<tr>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Name:</p>
	</td>
	<td class="body">
		<p class="resultData">
		<xsl:value-of select="Doctor/@DoctorBusinessName"/></p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Address:</p>
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="Doctor/@DoctorAddress1"/>
	</td>
</tr>
<tr>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Phone:</p>
	</td>
	<td nowrap="" class="body">
		<p class="resultData">
		(<xsl:value-of select="Doctor/@DoctorDayAreaCode"/>) <xsl:value-of select="concat(' ', Doctor/@DoctorDayExchangeNumber)"/> - <xsl:value-of select="Doctor/@DoctorDayUnitNumber"/>
		<xsl:if test="string-length(Doctor/@DoctorDayExtensionNumber) &gt; 0">
			<xsl:value-of select="concat(' x', Doctor/@DoctorDayExtensionNumber)"/>
		</xsl:if></p>
	</td>
	<td width="20"></td>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="Doctor/@DoctorAddress2"/>
	</td>
</tr>
<tr>
	<td nowrap="" width="125">&#160;</td>
	<td class="body">&#160;</td>
	<td width="20">
		<img border="0" src="images/spacer.gif" width="8" height="8"/>
	</td>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="concat(Doctor/@DoctorAddressCity, ', ')"/><xsl:value-of select="concat(Doctor/@DoctorAddressState, '  ')"/><xsl:value-of select="Doctor/@DoctorAddressZip"/>
	</td>
</tr>
<tr>
	<td colspan="5"><hr noshade="" color="#000099" size="1" /></td>
</tr>
<tr>
	<td nowrap="" colspan="2">
		<p class="headerRedData">Attorney</p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">&#160;</td>
	<td nowrap="" class="body">&#160;</td>
</tr>
<tr>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Name:</p>
	</td>
	<td class="body">
		<p class="resultData">
		<xsl:value-of select="Attorney/@AttorneyBusinessName"/></p>
	</td>
	<td width="20">&#160;</td>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Address:</p>
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="Attorney/@AttorneyAddress1"/>
	</td>
</tr>
<tr>
	<td nowrap="" width="125">
		<p class="resultCaption">
		Business Type:</p>
	</td>
	<td nowrap="" class="body">
		<p class="resultData">
		<xsl:value-of select="Attorney/@AttorneyBusinessType"/></p>
	</td>
	<td width="20"></td>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="Attorney/@AttorneyAddress2"/>
	</td>
</tr>
<tr>
	<td nowrap="" width="125">
		<p class="bodyBlue">
		Phone:</p>
	</td>
	<td class="body">
		(<xsl:value-of select="Attorney/@AttorneyDayAreaCode"/>) <xsl:value-of select="concat(' ', Attorney/@AttorneyDayExchangeNumber)"/> - <xsl:value-of select="Attorney/@AttorneyDayUnitNumber"/>
		<xsl:if test="string-length(Attorney/@AttorneyDayExtensionNumber) &gt; 0">
			<xsl:value-of select="concat(' x', Attorney/@AttorneyDayExtensionNumber)"/>
		</xsl:if>
	</td>
	<td width="20">
		<img border="0" src="images/spacer.gif" width="8" height="8"/>
	</td>
	<td nowrap="" width="125">
		&#160;
	</td>
	<td nowrap="" class="body">
		<xsl:value-of select="concat(Attorney/@AttorneyAddressCity, ', ')"/><xsl:value-of select="concat(Attorney/@AttorneyAddressState, '  ')"/><xsl:value-of select="Attorney/@AttorneyAddressZip"/>
	</td>
</tr>
			
</xsl:template>

</xsl:stylesheet>