<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />
    
<xsl:param name="LynxID"/>
<xsl:param name="CarrierRep"/>
<xsl:param name="LynxContactPhone"/>

<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[

  function updExposure(val)
  {
  	var lsText = "";
    if (val == "1") lsText = "1st Party";
  	else if (val == "3") lsText = "3rd Party";
    return lsText;
  }

  function updCoverageProfile(val)
  {
  	var lsText = "";
    if (val == "COLL") lsText = "Collision";
  	else if (val == "COMP") lsText = "Comprehensive";
  	else if (val == "LIAB") lsText = "Liability";
  	else if (val == "UIM") lsText = "Underinsured";
  	else if (val == "UM") lsText = "Uninsured";
    return lsText;
  }

  function updBestContact(val)
  {
  	var lsText = "";
    if (val == "D") lsText = "Day";
  	else if (val == "N") lsText = "Night";
  	else if (val == "A") lsText = "Cell/Alt.";
    return lsText;
  }

  function ConvertTime(time)
  {
    var lsTime = time;
  	if (lsTime == null || lsTime == "") return "";
    var lsHH;
    var lsMM;
    var lsAmPm = 'AM';

    if (lsTime.length <= 4)
    {
      lsHH = lsTime.substr(0,2);
      lsMM = lsTime.substr(2,2);
    }
    else if (lsTime.length > 4)
    {
      lsHH = lsTime.substr(11,2);
      lsMM = lsTime.substr(14,2);
    }
    
    if (lsHH >= 1 && lsHH < 10)
    {
      lsHH = lsHH.substr(1,1);
    }
    else if (lsHH == 12)
    {
      lsAmPm = 'PM';
    }
    else if (lsHH > 12)
    {
      lsHH = lsHH-12;
      lsAmPm = 'PM';
    }

    var lsRetTime = lsHH + ':' + lsMM  + ' ' + lsAmPm;
    return lsRetTime;
  }
    
  function getTimeZone(tz)
  {
    var lsTimeZone = tz;
    if (lsTimeZone != null || lsTimeZone != "")
    {
      var lsTZ = lsTimeZone.substr(0,lsTimeZone.indexOf(" "));
      return lsTZ;
    }
    else return "";
  }

  ]]>
</msxsl:script>

<xsl:template match="WebAssignment">

  <xsl:choose>
    <xsl:when test="$LynxID &gt; 0">
  
      <img src="images/Lynx_logo.gif" name="sdfds" width="111" height="53" border="0" />
      
      <table width="680" border="0" cellpadding="0" cellspacing="0">
      	<tr>
          <td nowrap="" class="nb">&#160;&#160;</td>
      		<td width="100%" valign="top" nowrap="" class="nb">
            6351 Bayshore Road Suite 18<br/>
            Fort Myers, FL 33917-3172<br/>
            Phone: <xsl:value-of select="$LynxContactPhone"/><br/>
           	Fax: 239-479-5943
          </td>
          <td valign="top" align="center" nowrap="">
            <span style="font-size:16pt; font-weight:bold;">REPORT<br/></span>
            <span style="font-size:14pt;"><xsl:value-of select="Claim/AssignmentDescription"/></span>
          </td>
      	</tr>
      </table>
      
      <br/>
      
      <table width="680" border="0" cellpadding="0" cellspacing="0">
      	<tr>
      		<td valign="bottom" nowrap="" class="nb">
          	<div class="border">
              <table border="0" cellpadding="1" cellspacing="0">
              	<tr>
                  <td nowrap=""><span style="font-size:14pt;">LYNX ID:</span></td>
                  <td nowrap="">&#160;<span style="font-size:14pt;"><xsl:value-of select="$LynxID"/></span></td>
              	</tr>
              	<tr>
                  <td nowrap=""><strong>Submitted:</strong></td>
                  <td nowrap="">&#160;<xsl:value-of select="Claim/TimeFinished"/></td>
              	</tr>
              </table>
            </div>
          </td>
          <td width="100%" nowrap="">&#160;</td>
      		<td valign="top" nowrap="" class="nb">
            <div class="border">
              <table border="0" cellpadding="1" cellspacing="0">
              	<tr>
                  <td nowrap=""><strong>Carrier:</strong></td><td nowrap="">&#160;<xsl:value-of select="Claim/CarrierName"/></td>
              	</tr>
          <xsl:choose>
            <xsl:when test="$CarrierRep = 1">
              	<tr>
                  <td nowrap=""><strong>Office:</strong></td><td nowrap="">&#160;<xsl:value-of select="Claim/CarrierOfficeName"/></td>
              	</tr>
              	<tr>
                  <td nowrap=""><strong>Rep:</strong></td><td nowrap="">&#160;<xsl:value-of select="Claim/CarrierRepNameFirst"/>&#160;<xsl:value-of select="Claim/CarrierRepNameLast"/></td>
              	</tr>
              	<tr>
                  <td nowrap=""><strong>Phone:</strong></td><td nowrap="">&#160;<xsl:value-of select="Claim/CarrierRepPhoneDay"/></td>
              	</tr>
              	<tr>
                  <td nowrap=""><strong>Email:</strong></td><td nowrap="">&#160;<xsl:value-of select="Claim/CarrierRepEmailAddress"/></td>
              	</tr>
            </xsl:when>
            <xsl:otherwise>
              	<tr>
                  <td nowrap=""><strong>Entered by:</strong></td><td nowrap="">&#160;<xsl:value-of select="Claim/CarrierRepNameFirst"/>&#160;<xsl:value-of select="Claim/CarrierRepNameLast"/></td>
              	</tr>
              	<tr>
                  <td nowrap=""><strong>Phone:</strong></td><td nowrap="">&#160;<xsl:value-of select="Claim/CarrierRepPhoneDay"/></td>
              	</tr>
              	<tr>
                  <td nowrap=""><strong>Email:</strong></td><td nowrap="">&#160;<xsl:value-of select="Claim/CarrierRepEmailAddress"/></td>
              	</tr>
            </xsl:otherwise>
          </xsl:choose>
              </table>
            </div>
          </td>
      	</tr>
      </table>
      
      <br/>
      
      <table width="680" border="0" cellpadding="0" cellspacing="0">
      	<tr bgcolor="#CCCCCC">
      		<td nowrap="" class="nb" style="font-size:14pt;">Claim Information</td>
      	</tr>
      </table>
      
<table width="680" border="0" cellpadding="0" cellspacing="0" style="border: 1px solid #808080; padding:4px;">
	<tr>
		<td>
    
      <table width="670" border="0" cellpadding="0" cellspacing="0" style="padding:1px;">
      	<tr>
      		<td nowrap="" class="bold">Claim Assigned by:</td>
      		<td nowrap="">&#160;</td>
      		<td colspan="2" nowrap="" class="bold">Relation to Insured:</td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="">&#160;</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">
            <xsl:value-of select="Claim/CallerNameFirst"/>&#160;<xsl:value-of select="Claim/CallerNameLast"/>
          </td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">
            <xsl:value-of select="Claim/CallerRelationToInsuredIDDescription"/>
          </td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"></td>
      	</tr>
      	<tr>
      		<td width="17%" nowrap="" class="bold">Claim Number:</td>
      		<td width="17%" nowrap="">&#160;</td>
      		<td width="17%" nowrap="" class="bold">Loss Date:</td>
      		<td width="17%" nowrap="">&#160;</td>
      		<td width="16%" nowrap="" class="bold">Loss State:</td>
      		<td width="16%" nowrap="">&#160;</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Claim/CoverageClaimNumber"/></td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Claim/LossDate"/></td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Claim/LossAddressState"/></td>
      	</tr>
      	<tr>
      		<td nowrap="" class="bold">Insured Name:</td>
      		<td nowrap="">&#160;</td>
      		<td colspan="2" nowrap="" class="bold">Insured Business Name:</td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="">&#160;</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">
            <xsl:value-of select="Claim/InsuredNameFirst"/>&#160;<xsl:value-of select="Claim/InsuredNameLast"/>
          </td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">
            <xsl:value-of select="Claim/InsuredBusinessName"/>
          </td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="">&#160;</td>
      	</tr>

      	<tr>
      		<td colspan="2" nowrap="" class="bold">IA Assignment Type:</td>
      		<td colspan="4" nowrap="" class="bold">Special Handling Instructions for Independant Appraiser:</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/AssignmentTypeIDDescription"/></td>
      		<td colspan="4" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/Remarks"/></td>
      	</tr>

      	<tr>
      		<td nowrap="" class="bold">Party:</td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="" class="bold">Coverage:</td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="" class="bold">Deductible:</td>
      		<td nowrap="" class="bold">Limit:</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="user:updExposure(string(Vehicle/ExposureCD))"/></td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/ClientCoverageTypeDesc"/></td>
      		<td height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/DeductibleAmt"/></td>
      		<td height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/LimitAmt"/></td>
      	</tr>
      	<tr>
      		<td colspan="6" nowrap="" class="bold">
      		 Description of Loss/Damages:</td>
      	</tr>
      	<tr>
      		<td colspan="6" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Claim/Remarks"/></td>
      	</tr>
      	<tr>
      		<td nowrap="" class="bold">Owner Name:</td>
      		<td nowrap="">&#160;</td>
      		<td colspan="2" nowrap="" class="bold">Owner Business Name:</td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="">&#160;</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">
            <xsl:value-of select="Vehicle/OwnerNameFirst"/>&#160;<xsl:value-of select="Vehicle/OwnerNameLast"/>
          </td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/OwnerBusinessName"/></td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="">&#160;</td>
      	</tr>
      	<tr>
      		<td nowrap="" class="bold">Owner Phone:</td>
      		<td nowrap="">&#160;</td>
      		<td colspan="2" nowrap="" class="bold">Preferred Contact Phone:</td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="">&#160;</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">
            <xsl:if test="Vehicle/OwnerPhoneSumm != ''">
              <xsl:value-of select="Vehicle/OwnerPhoneSumm"/> (Day)<br/>
            </xsl:if>
            <xsl:if test="Vehicle/OwnerNightPhoneSumm != ''">
              <xsl:value-of select="Vehicle/OwnerNightPhoneSumm"/> (Night)<br/>
            </xsl:if>
            <xsl:if test="Vehicle/OwnerAltPhoneSumm != ''">
              <xsl:value-of select="Vehicle/OwnerAltPhoneSumm"/> (Cell/Alt.)
            </xsl:if>
          </td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="user:updBestContact(string(Vehicle/OwnerBestPhoneCD))"/></td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="">&#160;</td>
      	</tr>
      	<tr>
      		<td colspan="2" nowrap="" class="bold">Vehicle Year/Make/Model:</td>
      		<td colspan="2" nowrap="" class="bold">Vehicle VIN:</td>
      		<td nowrap="" class="bold">License Plate:</td>
      		<td nowrap="" class="bold">Color:</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">
            <xsl:value-of select="Vehicle/VehicleYear"/>&#160;
            <xsl:value-of select="Vehicle/Make"/>&#160;
            <xsl:value-of select="Vehicle/Model"/>
          </td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/VIN"/></td>
      		<td height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/LicensePlateNumber"/></td>
      		<td height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/Color"/></td>
      	</tr>
      	<tr>
      		<td colspan="2" nowrap="" class="bold">Odometer:</td>
      		<td colspan="2" nowrap="" class="bold">&#160;</td>
      		<td nowrap="" class="bold">&#160;</td>
      		<td nowrap="" class="bold">&#160;</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">
            <xsl:value-of select="Vehicle/Mileage"/>
          </td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">&#160;</td>
      		<td height="26" valign="top" style="padding-bottom:4px">&#160;</td>
      		<td height="26" valign="top" style="padding-bottom:4px">&#160;</td>
      	</tr>

      	<tr>
      		<td colspan="2" nowrap="" class="bold">Vehicle Location:</td>
      		<td colspan="2" nowrap="" class="bold">Phone:</td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="">&#160;</td>
      	</tr>
      	<tr>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px">
            <xsl:if test="Vehicle/LocationName != ''">
              <xsl:value-of select="Vehicle/LocationName"/><br/>
            </xsl:if>
            <xsl:if test="Vehicle/LocationAddress1 != ''">
              <xsl:value-of select="Vehicle/LocationAddress1"/><br/>
            </xsl:if>
            <xsl:value-of select="Vehicle/LocationAddressCity"/>
            <xsl:if test="Vehicle/LocationAddressState != '' or Vehicle/LocationAddressZip != ''">
              <xsl:text>,</xsl:text> 
            </xsl:if>
            <xsl:text> </xsl:text> 
            <xsl:value-of select="Vehicle/LocationAddressState"/>
            <xsl:text> </xsl:text> 
            <xsl:value-of select="Vehicle/LocationAddressZip"/>
          </td>
      		<td colspan="2" height="26" valign="top" style="padding-bottom:4px"><xsl:value-of select="Vehicle/LocationPhoneSumm"/></td>
      		<td nowrap="">&#160;</td>
      		<td nowrap="">&#160;</td>
      	</tr>
      	<tr>
      		<td colspan="6" nowrap="" class="bold">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="display:inline">
            	<tr>
            		<td nowrap="" class="bold">
            		Document Uploaded:</td>
            	</tr>
            	<tr>
            		<td id="txtFileUploadName" height="50" valign="top" style="padding-bottom:4px"></td>
            	</tr>
            </table>
          </td>
      	</tr>
      </table>
      
    </td>
	</tr>
</table>  
  
    </xsl:when>
  
    <xsl:otherwise>
    nothing to display
      <!-- Don't display anything -->
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>

</xsl:stylesheet>  