<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" indent="yes" encoding="UTF-8" />

<xsl:template match="Root">
  <xsl:for-each select="/Root/CoverageType">
    <option>
      <xsl:attribute name="value"><xsl:value-of select="@CoverageProfileCD"/>|<xsl:value-of select="@ClientCoverageTypeID"/></xsl:attribute>
      <xsl:value-of select="@Name"/>
    </option>
  </xsl:for-each>
  <script language="JavaScript" type="text/JavaScript">
  laryCoverage[0] = new Array(<xsl:for-each select="/Root/CoverageType">
      "<xsl:value-of select="@Name"/>", "<xsl:value-of select="@CoverageProfileCD"/>|<xsl:value-of select="@ClientCoverageTypeID"/>"<xsl:if test="position() != last()">, </xsl:if>
  </xsl:for-each>
  )

  laryCoverage[1] = new Array(<xsl:for-each select="/Root/CoverageType[@CoverageProfileCD != 'LIAB']">
      "<xsl:value-of select="@Name"/>", "<xsl:value-of select="@CoverageProfileCD"/>|<xsl:value-of select="@ClientCoverageTypeID"/>"<xsl:if test="position() != last()">, </xsl:if>
  </xsl:for-each>
  )

  laryCoverage[2] = new Array(<xsl:for-each select="/Root/CoverageType[@CoverageProfileCD = 'LIAB']">
      "<xsl:value-of select="@Name"/>", "<xsl:value-of select="@CoverageProfileCD"/>|<xsl:value-of select="@ClientCoverageTypeID"/>"<xsl:if test="position() != last()">, </xsl:if>
  </xsl:for-each>
  )
  </script>
</xsl:template>

</xsl:stylesheet>
