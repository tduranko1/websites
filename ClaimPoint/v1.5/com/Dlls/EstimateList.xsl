<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="/">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="Document">
<tr><td colspan="5"><p class="mini">&#160;</p></td></tr>
<tr><td colspan="5">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse" bordercolor="#00099" height="48">
	<tr>
		<td width="10" valign="top" height="8">
			<img border="0" src="images/spacer.gif" width="10" height="8"/></td>
		<td width="8" valign="top" background="images/table_CL_white.gif" height="8">
			<img border="0" src="images/table_TL_white.gif" width="8" height="8"/>
		</td>
		<td background="images/table_TC_white.gif" height="8">
			<img border="0" src="images/spacer.gif" width="25" height="8"/>
		</td>
		<td width="8" background="images/table_CR_white.gif" valign="top" height="8">
			<img border="0" src="images/table_TR_white.gif" width="8" height="8"/>
		</td>
		<td width="10" valign="top" bordercolor="#FFFFFF" height="8">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
	</tr>
	<tr>
		<td width="10" height="31">&#160;</td>
		<td width="8" background="images/table_CL_white.gif" height="31">&#160;</td>
		<td background="images/lt_white.gif" nowrap="">


			<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" height="37">
				<tr>
					<td width="125" height="18">
						<p class="bodyBlue">Type:</p>
					</td>
					<td height="18">
						<p class="body"><xsl:value-of select="@DocumentType"/></p>
					</td>
					<td height="18" width="20">
					</td>
					<td height="18" width="85">
						<p class="bodyBlue">Image:</p>
					</td>
					<td height="37" rowspan="2">
						<a border="0">
						<xsl:attribute name="href">JavaScript:goDocument("<xsl:value-of select="@ImageLocation"/>","<xsl:value-of select="@Archived"/>")</xsl:attribute>
						<img border="0" src="images/icon_page.gif" width="26" height="32"/>
						</a>
					</td>
				</tr>
				<tr>
					<td width="125" height="19">
						<p class="bodyBlue">Imaged on:</p>
					</td>
					<td height="19">
						<p class="body"><xsl:value-of select="substring(@ImageDate, 6, 2)"/>/<xsl:value-of select="substring(@ImageDate, 9, 2)"/>/<xsl:value-of select="substring(@ImageDate, 1, 4)"/></p>
					</td>
					<td height="19" width="20">&#160;</td>
					<td height="19" width="85">&#160;</td>
				</tr>
				<tr>
					<td height="19">
						<p class="bodyBlue">Source:</p>
					</td>
					<td height="19">
						<p class="body"><xsl:value-of select="@DocumentSource"/></p>
					</td>
					<td height="19">&#160;</td>
				</tr>
				
			</table>

		</td>
		<td width="8" background="images/table_CR_white.gif" height="31">&#160;</td>
		<td width="10" bordercolor="#FFFFFF" height="31">&#160;</td>
	</tr>
	<tr>
		<td width="10" valign="bottom" height="9">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
		<td width="8" background="images/table_CL_white.gif" valign="bottom" height="9">
			<img border="0" src="images/table_BL_white.gif" width="8" height="8"/>
		</td>
		<td class="cell" height="9" background="images/lt_white.gif" >
			<img border="0" src="images/spacer.gif" width="25" height="8"/>
		</td>
		<td width="8" background="images/table_CR_white.gif" valign="bottom" height="9">
			<img border="0" src="images/table_BR_white.gif" width="8" height="8"/>
		</td>
		<td width="10" valign="bottom" bordercolor="#FFFFFF" height="9">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
	</tr>
</table>
</td></tr>
</xsl:template>

</xsl:stylesheet>