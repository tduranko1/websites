<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:ms="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace" 
    id="ReportCriteria" 
    exclude-result-prefixes="ms user">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<ms:script language="JScript" implements-prefix="user">
  <![CDATA[
    function makeOptions(strValues, strCaptions) {
      if (strValues != "" && strCaptions != "") {
        var strRet = "";
        var aValues, aCaptions;
        aValues = strValues.split("|");
        aCaptions = strCaptions.split("|");
        iLen = Math.min(aValues.length, aCaptions.length);
        for (var i = 0; i < iLen; i++){
          strRet += "<option value='" + aValues[i] + "'>" + aCaptions[i] + "</option>";
        }
        
        return strRet;
      } else
        return "";
    }
    
  ]]>
</ms:script>

<xsl:template match="/Root">
  <!-- exclude the All Clients (CriteriaID = 10). In future this has to be a column in the table to indicate visibility in ClaimPoint -->
  <script language="javascript">
    aCriteriaObjs = new Array(<xsl:for-each select="Criteria[@CriteriaID!='10']">"<xsl:value-of select="@ParamName"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsReq = new Array(<xsl:for-each select="Criteria[@CriteriaID!='10']">"<xsl:value-of select="@RequiredField"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsName = new Array(<xsl:for-each select="Criteria[@CriteriaID!='10']">"<xsl:value-of select="@DisplayText"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsDataType = new Array(<xsl:for-each select="Criteria[@CriteriaID!='10']">"<xsl:value-of select="@DataType"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsMaxValue = new Array(<xsl:for-each select="Criteria[@CriteriaID!='10']">"<xsl:value-of select="@MaxValue"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    aCriteriaObjsDefaultValueScript = new Array(<xsl:for-each select="Criteria[@CriteriaID!='10']">"<xsl:value-of select="@DefaultValueScript"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    sReportTitle = "<xsl:value-of select="@ReportName"/>";
    sProcName = "<xsl:value-of select="@ProcName"/>";
    sRptName = "<xsl:value-of select="@RptName"/>";
    sStaticParam = "<xsl:value-of select="@StaticParameters"/>";
  </script>
    <table border="0" cellspacing="0" cellpadding="2" style="table-layout:fixed">
      <colgroup>
        <col width="160px"/>
        <col width="*"/>
      </colgroup>
    <xsl:for-each select="Criteria[@CriteriaID!='10']">
      <xsl:sort data-type="number" select="@DisplayOrder" order="ascending" />
      <xsl:variable name="CriteriaID"><xsl:value-of select="@CriteriaID"/></xsl:variable>
      <tr>
        <td class="bodyBlue" styl12e="width:100px;" nowrap="on">
          <xsl:value-of select="@DisplayText"/>:
          <xsl:if test="@RequiredField = '1'">*</xsl:if>
        </td>
        <td>
          <xsl:choose>
            <!-- Reference Data. Data in the XML -->
            <xsl:when test="@DataType='R' and @ReferenceType='S'">
              <select>
                <xsl:attribute name="id"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <option value=""></option>
                <xsl:for-each select="/Root/Reference[@List=$CriteriaID]">
                <xsl:sort select="@Name" data-type="text" order="ascending"/>
              	<option>
                  <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                  <xsl:value-of select="@Name"/>
                </option>
                </xsl:for-each>
              </select>
            </xsl:when>
            <!-- Reference Data with list as a string value1,value2|Display1,Display2 -->
            <xsl:when test="@DataType='R' and @ReferenceType='L'">
              <xsl:variable name="valueAndCaption"><xsl:value-of select="/Root/Reference[@List=$CriteriaID]/@Name"/></xsl:variable>
              <xsl:variable name="valueList">
                <xsl:value-of select="substring-before($valueAndCaption, ',')"/>
              </xsl:variable>
              <xsl:variable name="CaptionList">
                <xsl:value-of select="substring-after($valueAndCaption, ',')"/>
              </xsl:variable>
              <select>
                <xsl:attribute name="id"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <option value=""></option>
                <xsl:value-of disable-output-escaping="yes" select="user:makeOptions(string($valueList), string($CaptionList))"/>
              </select>
            </xsl:when>
            <!-- Numeric data -->            
            <xsl:when test="@DataType='N'">
              <input type="text">
                <xsl:attribute name="id"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="maxlength"><xsl:value-of select="@MaxLength"/></xsl:attribute>
                <xsl:attribute name="size"><xsl:value-of select="string(number(@MaxLength) + 1)"/></xsl:attribute>
              </input>
            </xsl:when>
            <!-- Boolean data -->
            <xsl:when test="@DataType='B'">
              <input type="checkbox" style="margin:0px;padding:0px;">
                <xsl:attribute name="id"><xsl:value-of select="@ParamName"/></xsl:attribute>
                <xsl:attribute name="name"><xsl:value-of select="@ParamName"/></xsl:attribute>
              </input>
            </xsl:when>
          </xsl:choose>
        </td>
      </tr>
    </xsl:for-each>
      <tr>
        <td colspan="2">    
          All fields marked with an asterisk (*) are required.
          <br/><br/>
          <div id="divExportFormat" name="divExportFormat">Report Format:
            <SELECT id="csExportFormat" name="csExportFormat">
              <xsl:variable name="DefaultValue"><xsl:value-of select="/Root/Reference[@List='ExportFormat' and @DefaultFlag='1']/@ReferenceID"/></xsl:variable>
              <xsl:for-each select="/Root/Reference[@List='ExportFormat']">
                <xsl:sort select="@Name" data-type="text"/>
                <OPTION>
                  <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
                  <xsl:if test="@ReferenceID = $DefaultValue">
                    <xsl:attribute name="SELECTED"/>
                  </xsl:if>
                  <xsl:value-of select="@Name"/>
                </OPTION>
              </xsl:for-each>
            </SELECT>
          </div>
          <span id="divViewExternal" name="divViewExternal">
            <input type="checkbox" id="chkOpenExternal" name="chkOpenExternal"/> <label for="chkOpenExternal">View report in separate window</label> &#160;&#160;
          </span>
          <input type="button" id="viewReport" name="viewReport" value="View Report" onclick="getReport()"/>
        </td>
      </tr>
    </table>
  <br/>
  <form name="frmGetReport" id="frmGetReport" method="post" target="ifrmReport" action="frmGetReport.asp">
    <input type="hidden" name="reportFileName" id="reportFileName"/>
    <input type="hidden" name="storedProc" id="storedProc"/>
    <input type="hidden" name="reportParams" id="reportParams"/>
    <input type="hidden" name="staticParams" id="staticParams"/>
    <input type="hidden" name="friendlyName" id="friendlyName"/>
    <input type="hidden" name="reportFormat" id="reportFormat"/>
  </form>
  <IFRAME src="blank.asp" id="ifrmReport" name="ifrmReport" style="width:98%; height:445px; border:0px solid #000000;" frameBorder="0">
  </IFRAME>
</xsl:template>
</xsl:stylesheet>
