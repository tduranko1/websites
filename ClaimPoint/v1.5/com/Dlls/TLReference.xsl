<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" omit-xml-declaration="yes" standalone="yes"/>
  <xsl:param name="UserID"/>
  <xsl:param name="NameFirst"/>
  <xsl:param name="NameLast"/>
  <xsl:param name="Email"/>
  <xsl:param name="Phone"/>
  <xsl:param name="OfficeName"/>
  <xsl:param name="InsuranceCompanyName"/>
  <xsl:template match="Root">
    <xsl:element name="Root">
      <xsl:element name="CarrierRep">
        <xsl:attribute name="userid"><xsl:value-of select="$UserID"/></xsl:attribute>
        <xsl:attribute name="namefirst"><xsl:value-of select="$NameFirst"/></xsl:attribute>
        <xsl:attribute name="namelast"><xsl:value-of select="$NameLast"/></xsl:attribute>
        <xsl:attribute name="email"><xsl:value-of select="$Email"/></xsl:attribute>
        <xsl:attribute name="phone"><xsl:value-of select="$Phone"/></xsl:attribute>
        <xsl:attribute name="officename"><xsl:value-of select="$OfficeName"/></xsl:attribute>
        <xsl:attribute name="insurancecompanyname"><xsl:value-of select="$InsuranceCompanyName"/></xsl:attribute>
      </xsl:element>
      <xsl:element name="Insurance">
        <xsl:attribute name="id">
          <xsl:value-of select="/Root/@InsuranceCompanyID"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of select="/Root/@InsuranceCompanyName"/>
        </xsl:attribute>
      </xsl:element>
      <xsl:for-each select="//OfficeUser">
        <xsl:sort data-type="text" order="ascending" select="@NameFirst"/>
        <xsl:element name="OfficeUser">
          <xsl:attribute name="userid">
            <xsl:value-of select="@UserID"/>
          </xsl:attribute>
          <xsl:attribute name="namefirst">
            <xsl:value-of select="@NameFirst"/>
          </xsl:attribute>
          <xsl:attribute name="namelast">
            <xsl:value-of select="@NameLast"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:for-each>
      <xsl:for-each select="//Reference[@List='CallerRelationToInsured']">
        <xsl:sort data-type="text" order="ascending" select="@Name"/>
        <xsl:element name="Reference">
          <xsl:attribute name="list">Relation</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="@ReferenceID"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="@Name"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:for-each>
      <xsl:for-each select="//CoverageType">
        <xsl:sort data-type="text" order="ascending" select="@Name"/>
        <xsl:element name="Reference">
          <xsl:attribute name="list">Coverage</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="@CoverageProfileCD"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="@Name"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:for-each>
      <xsl:for-each select="//Reference[@List='ContractState']">
        <xsl:sort data-type="text" order="ascending" select="@Name"/>
        <xsl:element name="Reference">
          <xsl:attribute name="list">State</xsl:attribute>
          <xsl:attribute name="value">
            <xsl:value-of select="@ReferenceID"/>
          </xsl:attribute>
          <xsl:attribute name="name">
            <xsl:value-of select="@Name"/>
          </xsl:attribute>
        </xsl:element>
      </xsl:for-each>
      <Reference list="Party" value="1" name="1st Party"/>
      <Reference list="Party" value="3" name="3rd Party"/>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
