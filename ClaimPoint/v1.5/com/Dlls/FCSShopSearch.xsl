<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />
    
<msxsl:script language="JScript" implements-prefix="user">
  <![CDATA[

    function ConvertTime(time)
    {
      var lsTime = time;
			if (lsTime == null || lsTime == "") return "";
      var lsHH;
      var lsMM;
      var lsAmPm = 'AM';

      if (lsTime.length <= 4)
      {
        lsHH = lsTime.substr(0,2);
        lsMM = lsTime.substr(2,2);
      }
      else if (lsTime.length > 4)
      {
        lsHH = lsTime.substr(11,2);
        lsMM = lsTime.substr(14,2);
      }
      
      if (lsHH >= 1 && lsHH < 10)
      {
        lsHH = lsHH.substr(1,1);
      }
      else if (lsHH == 12)
      {
        lsAmPm = 'PM';
      }
      else if (lsHH > 12)
      {
        lsHH = lsHH-12;
        lsAmPm = 'PM';
      }

      var lsRetTime = lsHH + ':' + lsMM  + ' ' + lsAmPm;
      return lsRetTime;
    }
    
    function getTimeZone(tz)
    {
			var lsTimeZone = tz;
      if (lsTimeZone != null || lsTimeZone != "")
			{
        var lsTZ = lsTimeZone.substr(0,lsTimeZone.indexOf(" "));
        return lsTZ;
      }
      else return "";
    }

  ]]>
</msxsl:script>

<xsl:template match="Root">

  <xsl:comment>v1.5.0.0</xsl:comment>

  <xsl:choose>
    <xsl:when test="count(Shop) &gt; 0">
  
      <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#000099" style="border-collapse:collapse">
        <tr bgcolor="#E5E5F5" style="height: 28px;">
          <td style="font-weight:bold; text-align:center">Shop Name</td>
          <td style="font-weight:bold; text-align:center">Address</td>
          <td style="font-weight:bold; text-align:center">Phone</td>
          <td style="font-weight:bold; text-align:center">&#160;Approx.&#160; Dist.</td>
          <td style="font-weight:bold; text-align:center">&#160;Weekday&#160; Hours</td>
          <td style="font-weight:bold; text-align:center">Saturday Hours</td>
          <td style="font-weight:bold; text-align:center">Local Time</td>
        </tr>
          <xsl:for-each select="Shop" >
            <xsl:call-template name="ShopInfo"/>
          </xsl:for-each>
      </table>

      <br/><br/>

    </xsl:when>
  
    <xsl:otherwise>

      <table width="600" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td nowrap="">
            <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
          </td>
          <td width="100%" nowrap="" class="legendnopad" style="color:#FF0000; font-size:11pt; font-weight:bold;">
            This query yielded no results.
          </td>
        </tr>
        <tr>
          <td nowrap="">&#160;</td>
          <td width="100%" height="30" nowrap="" class="legendnopad" style="font-size:10pt;">
            Please try another search with different search parameters.
          </td>
        </tr>
      </table>

    </xsl:otherwise>
  </xsl:choose>

</xsl:template>

<xsl:template name="ShopInfo" >

        <tr style="padding:4px; border: 1px solid #000099;">
        <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">tblRowShop_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
          <td valign="top">
            <xsl:value-of select="@Name"/>
            <xsl:if test="@CertifiedFirstFlag = '1'">
              <span style="text-align:right;"><img src="images/CFLogo_small.jpg" alt="CertifiedFirst Network shop" align="right" border="0" valign="top" WIDTH="84" HEIGHT="27" style="cursor:help" /></span>
            </xsl:if>
          </td>
          <td valign="top">
            <xsl:if test="@DrivingDirections != ''">
              <img border="0" src="images/icon_directions.gif" align="right" valign="top" WIDTH="10" HEIGHT="11" onMouseOut="hideDiv()" style="cursor:help">
                <xsl:attribute name="onMouseOver">popDiv('txtDrivingDirections_<xsl:value-of select="@ShopLocationID"/>')</xsl:attribute>
              </img>
            </xsl:if>
            <xsl:value-of select="@Address1"/>
            <br/>
            <xsl:if test="@Address2 != ''">
              <xsl:value-of select="@Address2"/><br/>
            </xsl:if>
            <xsl:value-of select="@AddressCity"/>
       			<xsl:text disable-output-escaping="yes">, </xsl:text>
            <xsl:value-of select="@AddressState"/>
       			<xsl:text disable-output-escaping="yes"> </xsl:text>
            <xsl:value-of select="@AddressZip"/>
          </td>
          <td align="center" valign="top" nowrap="">
            <xsl:value-of select="@PhoneAreaCode"/>
      			<xsl:text disable-output-escaping="yes">-</xsl:text>
            <xsl:value-of select="@PhoneExchangeNumber"/>
      			<xsl:text disable-output-escaping="yes">-</xsl:text>
            <xsl:value-of select="@PhoneUnitNumber"/>
          </td>
          <td valign="top" align="center" nowrap="">
        	  <xsl:value-of select="@Distance"/>
          </td>
          <td align="center" valign="top">
            <xsl:if test="@OperatingMondayStartTime != '' or @OperatingMondayEndTime != ''">
              <xsl:value-of select="user:ConvertTime(string(@OperatingMondayStartTime))"/><br/>
          		<xsl:text disable-output-escaping="yes">- </xsl:text>
              <xsl:value-of select="user:ConvertTime(string(@OperatingMondayEndTime))"/>
            </xsl:if>
          </td>
          <td align="center" valign="top" nowrap="">
            <xsl:if test="@OperatingSaturdayStartTime != '' or @OperatingSaturdayEndTime != ''">
              <xsl:value-of select="user:ConvertTime(string(@OperatingSaturdayStartTime))"/><br/>
          		<xsl:text disable-output-escaping="yes">- </xsl:text>
              <xsl:value-of select="user:ConvertTime(string(@OperatingSaturdayEndTime))"/>
            </xsl:if>
          </td>
          <td colspan="2" align="center" valign="top" nowrap="">
            <xsl:value-of select="user:ConvertTime(string(@TimeAtShop))"/>
          </td>
          <td style="display:none">
      			<input name="ShopName" type="hidden">
              <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">txtShopName_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
            </input>
      			<input name="ShopAddress1" type="hidden">
              <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">txtShopAddress1_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@Address1"/></xsl:attribute>
            </input>
      			<input name="ShopAddress2" type="hidden">
              <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">txtShopAddress2_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@Address2"/></xsl:attribute>
            </input>
      			<input name="ShopCity" type="hidden">
              <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">txtShopCity_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@AddressCity"/></xsl:attribute>
            </input>
      			<input name="ShopState" type="hidden">
              <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">txtShopState_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@AddressState"/></xsl:attribute>
            </input>
      			<input name="ShopZip" type="hidden">
              <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">txtShopZip_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@AddressZip"/></xsl:attribute>
            </input>
      			<input name="ShopPhone" type="hidden">
              <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">txtShopPhone_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
              <xsl:attribute name="value">
                <xsl:value-of select="@PhoneAreaCode"/><xsl:text disable-output-escaping="yes">-</xsl:text><xsl:value-of select="@PhoneExchangeNumber"/><xsl:text disable-output-escaping="yes">-</xsl:text><xsl:value-of select="@PhoneUnitNumber"/>
              </xsl:attribute>
            </input>
      			<input name="ShopFax" type="hidden">
              <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">txtShopFax_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
              <xsl:attribute name="value">
                <xsl:value-of select="@FaxAreaCode"/><xsl:text disable-output-escaping="yes">-</xsl:text><xsl:value-of select="@FaxExchangeNumber"/><xsl:text disable-output-escaping="yes">-</xsl:text><xsl:value-of select="@FaxUnitNumber"/>
              </xsl:attribute>
            </input>
      			<input name="DrivingDirections" type="hidden">
              <xsl:attribute name="id"><xsl:text disable-output-escaping="yes">txtDrivingDirections_</xsl:text><xsl:value-of select="@ShopLocationID"/></xsl:attribute>
              <xsl:attribute name="value"><xsl:value-of select="@DrivingDirections"/></xsl:attribute>
            </input>
          </td>
        </tr>

</xsl:template>

</xsl:stylesheet>  