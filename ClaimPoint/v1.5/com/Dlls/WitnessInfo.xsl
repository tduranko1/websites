<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="Witness">

<tr><td colspan="5"><p class="mini">&#160;</p></td></tr>

<tr><td colspan="5">
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse" bordercolor="#00099" height="48">
	<tr>
		<td width="10" valign="top" height="8">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
		<td width="8" valign="top" background="images/table_CL_white.gif" height="8">
			<img border="0" src="images/table_TL_white.gif" width="8" height="8"/>
		</td>
		<td background="images/table_TC_white.gif" height="8">
			<img border="0" src="images/spacer.gif" width="25" height="8"/>
		</td>
		<td width="8" background="images/table_CR_white.gif" valign="top" height="8">
			<img border="0" src="images/table_TR_white.gif" width="8" height="8"/>
		</td>
		<td width="10" valign="top" bordercolor="#FFFFFF" height="8">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
	</tr>
	<tr>
		<td width="10" height="31">
			&#160;
		</td>
		<td width="8" background="images/table_CL_white.gif" height="31">
			&#160;
		</td>
		<td background="images/lt_white.gif" nowrap="">
			<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
				<tr>
					<td nowrap="" width="125">
						<p class="resultCaption">
						Name:</p>
					</td>
					<td class="body">
						<p class="resultData">
						<xsl:value-of select="concat(@NameTitle, ' ')"/> <xsl:value-of select="concat(@NameFirst, ' ')"/> <xsl:value-of select="@NameLast"/></p>
					</td>
					<td width="20">
						&#160;
					</td>
					<td nowrap="" width="125">
						<p class="resultCaption">
						SSN:</p>
					</td>
					<td nowrap="" class="body">
						<xsl:value-of select="@FedTaxID"/>
					</td>
				</tr>
				<tr>
					<td nowrap="" width="125">
						<p class="resultCaption">
						Address:</p>
					</td>
					<td nowrap="" class="body">
						<p class="resultData">
						<xsl:value-of select="@Address1"/></p>
					</td>
					<td width="20">
					</td>
					<td nowrap="" width="125">
						<p class="bodyBlue">
						Day Phone:</p>
					</td>
					<td nowrap="" class="body">
						(<xsl:value-of select="@DayAreaCode"/>) <xsl:value-of select="concat(' ', @DayExchangeNumber)"/> - <xsl:value-of select="@DayUnitNumber"/>
						<xsl:if test="string-length(@DayExtensionNumber) &gt; 0">
							<xsl:value-of select="concat(' x', @DayExtensionNumber)"/>
						</xsl:if>
					</td>
				</tr>
				<tr>
					<td nowrap="" width="125">
						&#160;
					</td>
					<td class="body">
						<xsl:value-of select="@Address2"/>
					</td>
					<td width="20">
						<img border="0" src="images/spacer.gif" width="8" height="8"/>
					</td>
					<td nowrap="" width="125">
						<p class="bodyBlue">Night Phone:</p>
					</td>
					<td nowrap="" class="body">
						(<xsl:value-of select="@NightAreaCode"/>) <xsl:value-of select="concat(' ', @NightExchangeNumber)"/> - <xsl:value-of select="@NightUnitNumber"/>
						<xsl:if test="string-length(@NightExtensionNumber) &gt; 0">
							<xsl:value-of select="concat(' x', @NightExtensionNumber)"/>
						</xsl:if>
					</td>
				</tr>
				<tr>
					<td nowrap="" width="125">
						&#160;
					</td>
					<td class="body">
						<p class="resultData">
						<xsl:value-of select="concat(@AddressCity, ', ')"/><xsl:value-of select="concat(@AddressState, '  ')"/><xsl:value-of select="@AddressZip"/></p>
					</td>
					<td width="20">
						&#160;
					</td>
					<td nowrap="" width="125">
						<p class="resultCaption">
						Alt. Phone:</p>
					</td>
					<td nowrap="" class="body">
						(<xsl:value-of select="@AlternateAreaCode"/>) <xsl:value-of select="concat(' ', @AlternateExchangeNumber)"/> - <xsl:value-of select="@AlternateUnitNumber"/>
						<xsl:if test="string-length(@AlternateExtensionNumber) &gt; 0">
							<xsl:value-of select="concat(' x', @AlternateExtensionNumber)"/>
						</xsl:if>
					</td>
				</tr>
				<tr>
					<td nowrap="" colspan="2">
						&#160;
					</td>
					<td width="20">
						&#160;
					</td>
					<td nowrap="" width="125">
						&#160;
					</td>
					<td nowrap="" class="body">
						&#160;
					</td>
				</tr>
				<tr>
					<td nowrap="" colspan="2">
						<p class="resultCaption">
						Location of Witness:</p>
					</td>
					<td width="20">
						&#160;
					</td>
					<td nowrap="" width="125">
						<p class="resultCaption">
						Best Phone:</p>
					</td>
					<td nowrap="" class="body">
						<xsl:value-of select="@BestContactPhone"/>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<p class="body">
						<xsl:value-of select="@Location"/></p>
					</td>
					<td width="20">
						&#160;
					</td>
					<td nowrap="" width="125">
						<p class="bodyBlue">Best Time:</p>
					</td>
					<td nowrap="" class="body">
						<xsl:value-of select="@BestContactTime"/>
					</td>
				</tr>
			</table>
		</td>
		<td width="8" background="images/table_CR_white.gif" height="31">
			&#160;
		</td>
		<td width="10" bordercolor="#FFFFFF" height="31">
			&#160;
		</td>
	</tr>
	<tr>
		<td width="10" valign="bottom" height="9">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
		<td width="8" background="images/table_CL_white.gif" valign="bottom" height="9">
			<img border="0" src="images/table_BL_white.gif" width="8" height="8"/>
		</td>
		<td class="cell" height="9" background="images/lt_white.gif" >
			<img border="0" src="images/spacer.gif" width="25" height="8"/>
		</td>
		<td width="8" background="images/table_CR_white.gif" valign="bottom" height="9">
			<img border="0" src="images/table_BR_white.gif" width="8" height="8"/>
		</td>
		<td width="10" valign="bottom" bordercolor="#FFFFFF" height="9">
			<img border="0" src="images/spacer.gif" width="10" height="8"/>
		</td>
	</tr>
</table>
</td></tr>	
</xsl:template>

</xsl:stylesheet>