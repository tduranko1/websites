<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">
    
<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />
    
<msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
         
    function getAge( nodeList )
    {
      if (nodeList != null)
      {
        var nde = nodeList.nextNode().firstChild;
            
        if (nde != null)
        {
          var lsBirthDate = nde.nodeTypedValue;
          
          if (lsBirthDate != "" && lsBirthDate != null)
          {
            var ldToday = new Date();
            var ldBirthDate = new Date(parseInt(lsBirthDate.substr(0, 4)), parseInt(lsBirthDate.substr(5, 2)) - 1, parseInt(lsBirthDate.substr(8, 2)));
            var liAge = Math.floor((ldToday - ldBirthDate)/24/60/60/1000/365.25);
            return liAge;
          }
        }
      }
      return "N/A";
    } 
        
    function formatSpacelessString (nodeList, piChars)
    {
      var lsSpacedString;
      var laStrings;
      var liCounter;
      var liStop;
      var lsString;
      
      if (nodeList != null)
      {
        var nde = nodeList.nextNode().firstChild;
            
        if (nde != null)
        {
          var laStrings = nde.nodeTypedValue.split(' ');
          liStop = laStrings.length;
          
          for (liCounter=0;liCounter<liStop;liCounter++)
          {
            lsString = laStrings[liCounter];
            lsSpacedString = "";
            
            while (lsString.length > piChars)
            {
              lsSpacedString = lsSpacedString + lsString.substr(0, piChars) + " ";
              lsString = lsString.substr(piChars, lsString.length - piChars);  
            }
              
              
            lsSpacedString = lsSpacedString + lsString;
            
            laStrings[liCounter] = lsSpacedString;
          }
          
          return laStrings.join(' ');
        }
      }
      return '';
    }   
  ]]>
</msxsl:script>

<xsl:param name="callNumber" select="0"/>
<xsl:variable name="wordLength" select="30"/>

<xsl:template match="Involved">

  <xsl:comment>v1.5.0.0</xsl:comment>

  <tr>
    <td class="bodyBlue" colspan="2">
      <p class="headerRedData">General:</p>
    </td>
    <td width="20">&#160;</td>
    <td>&#160;</td>
    <td>&#160;</td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Name:
    </td>
    <td class="body">
      <xsl:value-of select="concat(@NameTitle, ' ')"/> <xsl:value-of select="concat(@NameFirst, ' ')"/> <xsl:value-of select="@NameLast"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      SSN/EIN:
    </td>
    <td class="body">
      <xsl:value-of select="@FedTaxId"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Business:
    </td>
    <td class="body">
      <xsl:value-of select="@BusinessName"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Bus. Type:
    </td>
    <td class="body">
      <xsl:value-of select="@BusinessType"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Involved Type:
    </td>
    <td class="body">
      <xsl:if test="count(InvolvedType) &gt; 0">
        <xsl:variable name="involvedTypeList">
          <xsl:call-template name="commaDelimitList">
            <xsl:with-param name="list" select="InvolvedType/@InvolvedTypeName"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="substring($involvedTypeList, 1, string-length($involvedTypeList) - 2)"/>
      </xsl:if>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Gender:
    </td>
    <td class="body">
      <xsl:value-of select="@Gender"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Address:
    </td>
    <td class="body" rowspan="2" valign="top">
      <xsl:value-of select="@Address1"/><br/>
      <xsl:if test="@Address2 != ''">
        <xsl:value-of select="@Address2"/><br/>
      </xsl:if>
      <xsl:value-of select="@AddressCity"/>
      <xsl:if test="string-length(translate(normalize-space(@AddressCity),' ','')) &gt; 0 and string-length(translate(normalize-space(@AddressState),' ',''))">
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:value-of select="concat(@AddressState, ' ')"/><xsl:value-of select="@AddressZip"/>
    </td>
    <td width="20"></td>
    <td class="bodyBlue">
      DOB:
    </td>
    <td class="body">
      <xsl:value-of select="substring(@BirthDate, 6, 2)"/>
      <xsl:if test="string-length(translate(normalize-space(@BirthDate),' ','')) &gt; 0">
        <xsl:text>/</xsl:text>
      </xsl:if>
      <xsl:value-of select="substring(@BirthDate, 9, 2)"/>
      <xsl:if test="string-length(translate(normalize-space(@BirthDate),' ','')) &gt; 0">
        <xsl:text>/</xsl:text>
      </xsl:if>
      <xsl:value-of select="substring(@BirthDate, 1, 4)"/>
    </td>
  </tr>
  <tr>
    <td>&#160;</td>
    <td width="20"></td>
    <td class="bodyBlue">
      Day Phone:
    </td>
    <td class="body">
      <xsl:value-of select="@DayAreaCode"/>
      <xsl:if test="string-length(translate(normalize-space(@DayAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@DayExchangeNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="@DayExchangeNumber"/>
      <xsl:if test="string-length(translate(normalize-space(@DayExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@DayUnitNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="@DayUnitNumber"/>
      <xsl:if test="string-length(@DayExtensionNumber) &gt; 0">
        <xsl:value-of select="concat(' x', @DayExtensionNumber)"/>
      </xsl:if>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Driver License:
    </td>
    <td class="body">
      <xsl:value-of select="@DriverLicenseNumber"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Night Phone:
    </td>
    <td class="body">
      <xsl:value-of select="@NightAreaCode"/>
      <xsl:if test="string-length(translate(normalize-space(@NightAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@NightExchangeNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="@NightExchangeNumber"/>
      <xsl:if test="string-length(translate(normalize-space(@NightExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@NightUnitNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="@NightUnitNumber"/>
      <xsl:if test="string-length(@NightExtensionNumber) &gt; 0">
        <xsl:value-of select="concat(' x', @NightExtensionNumber)"/>
      </xsl:if>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      License State:
    </td>
    <td class="body">
      <xsl:value-of select="@DriverLicenseState"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Alt. Phone:
    </td>
    <td class="body">
      <xsl:value-of select="@AlternateAreaCode"/>
      <xsl:if test="string-length(translate(normalize-space(@AlternateAreaCode),' ','')) &gt; 0 and string-length(translate(normalize-space(@AlternateExchangeNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="@AlternateExchangeNumber"/>
      <xsl:if test="string-length(translate(normalize-space(@AlternateExchangeNumber),' ','')) &gt; 0 and string-length(translate(normalize-space(@AlternateUnitNumber),' ',''))">
        <xsl:text>-</xsl:text>
      </xsl:if>
      <xsl:value-of select="@AlternateUnitNumber"/>
      <xsl:if test="string-length(@AlternateExtensionNumber) &gt; 0">
        <xsl:value-of select="concat(' x', @AlternateExtensionNumber)"/>
      </xsl:if>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Location in Vehicle:
    </td>
    <td class="body">
      <xsl:value-of select="@PersonLocation"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Best Phone:
    </td>
    <td class="body">
      <xsl:value-of select="@BestContactPhone"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Seatbelt Used?
    </td>
    <td class="body">
      <xsl:value-of select="@SeatBelt"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Best Time:
    </td>
    <td class="body">
      <xsl:value-of select="@BestContactTime"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Injured?
    </td>
    <td class="body">
      <xsl:value-of select="@Injured"/>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue">
      Injury:
    </td>
    <td class="body">
      <xsl:value-of select="Injury/@InjuryType"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue" valign="top">
      Injury Description:
    </td>
    <td colspan="4" class="body">
      <xsl:value-of select="user:formatSpacelessString(Injury/@InjuryDescription, $wordLength)"/>
    </td>
  </tr>
  <tr>
    <td class="bodyBlue">
      Violation?
    </td>
    <td class="body">
      <xsl:choose>
        <xsl:when test="@ViolationFlag = '1'">Yes</xsl:when>
        <xsl:otherwise>No</xsl:otherwise>
      </xsl:choose>
    </td>
    <td width="20">&#160;</td>
    <td class="bodyBlue"></td>
    <td class="body"></td>
  </tr>
  <tr>
    <td class="bodyBlue" valign="top">
      Violation Description:
    </td>
    <td colspan="4" class="body">
      <xsl:value-of select="@ViolationDescription"/>
    </td>
  </tr>

</xsl:template>

  <xsl:template name="commaDelimitList">
  
    <xsl:param name="list"/>
    <xsl:if test="$list">
      <xsl:variable name="first" select="$list[1]"/>
      <xsl:variable name="rest">
        <xsl:call-template name="commaDelimitList">
          <xsl:with-param name="list" select="$list[position()!=1]"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:value-of select="concat(concat($first, ', '), $rest)"/>
    </xsl:if>  
  
  </xsl:template>

</xsl:stylesheet>