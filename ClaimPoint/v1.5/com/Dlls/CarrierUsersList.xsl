<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="UserID"/>
<xsl:param name="OfficeID"/>
<xsl:param name="ClaimViewLevel"/>
<xsl:param name="AppConstClaimViewAllLevel"/>
<xsl:param name="AppConstClaimViewOfficeLevel"/>

  <xsl:template match="Root">

    <xsl:choose>
      <xsl:when test="$OfficeID = ''"> 
        <select name="CarrierUser" id="txtCarrierUser" disabled="" style="font-weight: bold; font-size: 10px;">
        	<option value="">Please Select Office</option>
        </select>
      </xsl:when> 

      <xsl:when test="$ClaimViewLevel = $AppConstClaimViewAllLevel and $OfficeID = 0"> 
        <select name="CarrierUser" id="txtCarrierUser" disabled="" style="font-weight: bold; font-size: 10px;">
        	<option value="0">* All Users-All Offices *</option>
        </select>
      </xsl:when> 

      <xsl:when test="$ClaimViewLevel = $AppConstClaimViewAllLevel or $ClaimViewLevel = $AppConstClaimViewOfficeLevel and $OfficeID > 0"> 
        <select name="CarrierUser" id="txtCarrierUser" onChange="parent.updUserIDValue()">
        	<option value="0">* All Users *</option>

          <xsl:for-each select="Office/OfficeUser[../@OfficeID = $OfficeID and (@EnabledFlag = 1 or @ClaimsFlag = 1)]" >
            <xsl:sort select="@NameLast" order="ascending"/>
              <xsl:call-template name="getCarrierUsersList"/>
          </xsl:for-each>

        </select>
      </xsl:when> 

      <xsl:otherwise>
        <select name="CarrierUser" id="txtCarrierUser" onChange="parent.updUserIDValue()">
          <xsl:if test="$UserID = ''">
          	<option value="0">* All Users *</option>
          </xsl:if>
  
          <xsl:for-each select="Office/OfficeUser[../@OfficeID = $OfficeID and (@EnabledFlag = 1 or @ClaimsFlag = 1)]" >
            <xsl:sort select="@NameLast" order="ascending"/>
              <xsl:call-template name="getCarrierUsersList"/>
          </xsl:for-each>
  
        </select>
      </xsl:otherwise>
    </xsl:choose>
  
  </xsl:template>
  
  <xsl:template name="getCarrierUsersList">

    <xsl:if test="translate(../@ClientOfficeId, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') != 'FCS'">
    	<option>
    		<xsl:attribute name="value">
    			<xsl:value-of select="@UserID"/>
    		</xsl:attribute>
        <xsl:if test="@UserID = $UserID">
          <xsl:attribute name="Selected"/>
        </xsl:if>
    			<xsl:value-of select="@NameLast"/>, <xsl:value-of select="@NameFirst"/>
    	</option>
    </xsl:if>
  
  </xsl:template>

</xsl:stylesheet>
