<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="EnabledFlag"/>
<xsl:param name="AllowCarrierRepSelectionFlag"/>
<xsl:param name="UserID"/>

<xsl:template match="Root">

  <xsl:comment>v1.4.2.0</xsl:comment>

    <select name="CarrierUser" id="txtCarrierUser" onChange="updCallerName()" tabIndex="2">
      <option value="">Please Select One</option>
        <xsl:choose>
          <xsl:when test="$EnabledFlag != ''">
            <!-- The Enabled Flag was passed in. So filter the list based on that -->
            <xsl:for-each select="Office/OfficeUser[@EnabledFlag=$EnabledFlag]" >
              <xsl:sort select="@NameLast" order="ascending"/>
                <xsl:call-template name="getUsersList"/>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <!-- Enabled Flag was not passed in. So display all users -->
            <xsl:for-each select="Office/OfficeUser" >
              <xsl:sort select="@NameLast" order="ascending"/>
                <xsl:call-template name="getUsersList"/>
            </xsl:for-each>
          </xsl:otherwise>
        </xsl:choose>
    </select>
  
</xsl:template>
  
  <xsl:template name="getUsersList">
  
    <option>
      <xsl:attribute name="value">
        <xsl:value-of select="@NameLast"/>|<xsl:value-of select="@NameFirst"/>|<xsl:value-of select="../@OfficeID"/>|<xsl:value-of select="@UserID"/>
      </xsl:attribute>
      <xsl:if test="$AllowCarrierRepSelectionFlag = 1 and @UserID = $UserID">
        <xsl:attribute name="Selected"/>
      </xsl:if>
      <xsl:value-of select="@NameLast"/>, <xsl:value-of select="@NameFirst"/>
    </option>
  
  </xsl:template>

</xsl:stylesheet>
