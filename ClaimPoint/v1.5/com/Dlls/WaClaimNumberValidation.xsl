<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" indent="yes" encoding="UTF-8" />

<xsl:param name="OfficeID" select="''"/>

<xsl:template match="Root">
  <xsl:choose>
    <xsl:when test="$OfficeID = '0' or $OfficeID = '' or (/Root/Office[@OfficeID=$OfficeID]/@ClaimNumberValidJS='')"> <!--if the user does not belong to any office or if the validationJS is empty for the user's office (meaning corporate)-->
      var larrOfficeID = new Array(<xsl:for-each select="/Root/Office">"<xsl:value-of select="@OfficeID"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
      var larrClaimNumberFormatJS = new Array(<xsl:for-each select="/Root/Office">"<xsl:value-of select="@ClaimNumberFormatJS"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
      var larrClaimNumberValidJS = new Array(<xsl:for-each select="/Root/Office">"<xsl:value-of select="@ClaimNumberValidJS"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
      var larrClaimNumberMsgText = new Array(<xsl:for-each select="/Root/Office">"<xsl:value-of select="@ClaimNumberMsgText"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    </xsl:when>
    <xsl:otherwise>
      var larrOfficeID = new Array(<xsl:for-each select="/Root/Office[@OfficeID=$OfficeID]">"<xsl:value-of select="@OfficeID"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
      var larrClaimNumberFormatJS = new Array(<xsl:for-each select="/Root/Office[@OfficeID=$OfficeID]">"<xsl:value-of select="@ClaimNumberFormatJS"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
      var larrClaimNumberValidJS = new Array(<xsl:for-each select="/Root/Office[@OfficeID=$OfficeID]">"<xsl:value-of select="@ClaimNumberValidJS"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
      var larrClaimNumberMsgText = new Array(<xsl:for-each select="/Root/Office[@OfficeID=$OfficeID]">"<xsl:value-of select="@ClaimNumberMsgText"/>"<xsl:if test="position() != last()">,</xsl:if></xsl:for-each>);
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
