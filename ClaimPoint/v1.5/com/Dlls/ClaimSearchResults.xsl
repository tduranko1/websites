<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:user="http://mycompany.com/mynamespace"
    exclude-result-prefixes="msxsl user">

  <xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

  <xsl:param name="UserID"/>
  <xsl:param name="ClaimCreate"/>
  <xsl:param name="ClientOfficeId"/>

  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
         
  function ConvertDate(time)
  {
    var lsTime = time;
    if (lsTime == null || lsTime == "") return "";
    var lsDelimeter = "-";

    var strDate = lsTime.substr( 5, 2 ) + lsDelimeter + lsTime.substr( 8, 2 ) + lsDelimeter + lsTime.substr( 0, 4 );
    var lsRetTime = strDate;
    return lsRetTime;
  }

  ]]>
  </msxsl:script>

  <xsl:template match="/Root">

    <xsl:comment>v1.5.0.0</xsl:comment>

    <xsl:choose>

      <xsl:when test="count(Claim) &gt; 0">

        <span style="font-size: 10pt; height:26px;">
          Number of claims found: <xsl:value-of select="count(Claim/Vehicle)"/>
          <img src="images/spacer.gif" width="120" height="1" border="0"/>
          <a href="javascript:void(PrintPage())" class="subnav"
             title="Click for a printer friendly version"
             onMouseOver="window.status='Click for a printer friendly version'; return true"
             onMouseOut="window.status=''; return true">
            <img src="images/icon_print.gif" width="31" height="20" border="0" align="top"/>Print these results
          </a>
        </span>

        <!-- Start Printer Friendly DIV -->
        <div id="PrintPageDiv">

          <TABLE width="98%" border="0" cellspacing="1" cellpadding="2" bgcolor="#DDDDDD" id="srhResults">
            <!-- style="border:1px solid #CCCCCC" -->
            <thead>
              <TR>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Date
                </TD>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Status
                </TD>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Claim&#xa0;#
                </TD>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="0" height="7"/>LYNX&#xa0;ID
                </TD>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Type
                </TD>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Insured
                </TD>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Vehicle&#xa0;Info
                </TD>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Vehicle&#xa0;Owner
                </TD>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Source
                </TD>
                <xsl:if test="$UserID = 0">
                  <TD class="theader" title="Click to sort ascending/descending">
                    <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Carrier&#xa0;Rep
                  </TD>
                </xsl:if>
                <TD class="theader" title="Click to sort ascending/descending">
                  <img src="images/grid_blank.gif" class="sort-arrow" border="0" width="8" height="7"/>Claim&#xa0;Disposition
                </TD>
                <xsl:if test="$ClaimCreate='1' and $ClientOfficeId != 'FCS'">
                  <TD sortReq="No" style="font-weight: bold; text-align: center; vertical-align: top; padding: 2px; background: #E7E7F7; border-bottom: 1px solid #99CCFF; border-right: 1px solid #99CCFF; border-left: 1px solid #99CCFF; border-top: 1px solid #99CCFF;">
                    <strong>&#xa0;Action&#xa0;</strong>
                  </TD>
                </xsl:if>
              </TR>
            </thead>
            <tbody id="srhList">

              <xsl:for-each select="/Root/Claim">
                <xsl:sort select="@LynxId" order="ascending"/>
                <xsl:sort select="@IntakeFinishDate" order="ascending"/>
                <xsl:call-template name="Claim"/>
              </xsl:for-each>

            </tbody>
          </TABLE>
        </div>

      </xsl:when>

      <xsl:otherwise>

        <table id="srhResultsNoResult">
          <tr>
            <td nowrap="">
              <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
            </td>
            <td nowrap="" style="color:#FF0000; font-size:11pt; font-weight:bold;">
              There are no claims for the criteria above.
            </td>
          </tr>
          <tr>
            <td nowrap=""> </td>
            <td nowrap="">
              <br/>
              <p style="font-size:14px; font-weight:bold">
                Please try another search.
              </p>
            </td>
          </tr>
        </table>

      </xsl:otherwise>

    </xsl:choose>

  </xsl:template>

  <xsl:template name="Claim">

    <xsl:for-each select="Vehicle">

      <tr title="Click to open claim" onMouseOver="GridMouseOver(this)" onMouseOut="GridMouseOut(this)">
        <xsl:attribute name="onClick">
          <xsl:text disable-output-escaping="yes">GridClickGetClaim(this, </xsl:text>
          <xsl:value-of select="../@LynxId"/>
          <xsl:text disable-output-escaping="yes">, </xsl:text>
          <xsl:value-of select="@ClaimAspectID"/>
          <xsl:text disable-output-escaping="yes">, </xsl:text>
          <xsl:value-of select="@VehicleNumber"/>
          <xsl:text disable-output-escaping="yes">)</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="bgcolor">
          <xsl:choose>
            <xsl:when test="position() mod 2 = 1">#FFFFFF</xsl:when>
            <xsl:when test="position() mod 2 = 0">#DFEFFF</xsl:when>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="VehiclesList">
          <xsl:for-each select="Vehicle" >
            <xsl:call-template name="getVehiclesList"/>
          </xsl:for-each>
        </xsl:attribute>

        <td align="center" valign="top" nowrap="">
          <xsl:choose>
            <xsl:when test="string-length(user:ConvertDate(string(../@IntakeFinishDate))) = 0">
              &#xa0;
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="user:ConvertDate(string(../@IntakeFinishDate))"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td align="left" valign="top" nowrap="">
          <xsl:for-each select="ServiceChannel">
            <xsl:value-of select="@ServiceChannelCD"/> - <xsl:value-of select="@ClaimAspectStatusDesc"/><br/>
          </xsl:for-each>
        </td>
        <td valign="top">
          <xsl:choose>
            <xsl:when test="string-length(../@ClaimNumber) = 0">
              &#xa0;
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="../@ClaimNumber"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td align="center" valign="top" nowrap="">
          <xsl:choose>
            <xsl:when test="string-length(../@LynxId) = 0">
              &#xa0;
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="../@LynxId"/>-<xsl:value-of select="@VehicleNumber"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td align="center" valign="top">
          <!--<xsl:choose>
              <xsl:when test="string-length(ServiceChannel[@PrimaryFlag='1' and @EnabledFlag='1']/@ServiceChannelCDDesc) = 0">
                &#xa0;
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="ServiceChannel[@PrimaryFlag='1' and @EnabledFlag='1']/@ServiceChannelCDDesc"/>
              </xsl:otherwise>
            </xsl:choose>-->
          <xsl:value-of select="@InitialAssignmentTypeName"/>
        </td>
        <td valign="top">
          <xsl:variable name="InsuredName">
            <xsl:choose>
              <xsl:when test="../@InsuredBusinessName != ''">
                <xsl:value-of select="../@InsuredBusinessName"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="../@InsuredLastName"/>
                <xsl:if test="../@InsuredLastName != ''">
                  <xsl:text disable-output-escaping="yes">, </xsl:text>
                </xsl:if>
                <xsl:value-of select="../@InsuredFirstName"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="string-length($InsuredName) = 0">
              &#xa0;
            </xsl:when>
            <xsl:when test="string-length(normalize-space($InsuredName)) &gt; 30">
              <DIV style="overflow:auto;">
                <xsl:value-of select="$InsuredName"/>
              </DIV>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$InsuredName"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td valign="top">
          <xsl:variable name="VehDescription">
            <xsl:value-of select="@VehicleYear"/>
            <xsl:text disable-output-escaping="yes"> </xsl:text>
            <xsl:value-of select="@Make"/>
            <xsl:text disable-output-escaping="yes"> </xsl:text>
            <xsl:value-of select="@Model"/>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="string-length($VehDescription) = 0">
              &#xa0;
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$VehDescription"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td valign="top">
          <xsl:variable name="VehOwnerName">
            <xsl:choose>
              <xsl:when test="@OwnerBusinessName != ''">
                <xsl:value-of select="@OwnerBusinessName"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@OwnerNameLast"/>
                <xsl:if test="@OwnerNameLast != ''">
                  <xsl:text disable-output-escaping="yes">, </xsl:text>
                </xsl:if>
                <xsl:value-of select="@OwnerNameFirst"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="string-length($VehOwnerName) = 0">
              &#xa0;
            </xsl:when>
            <xsl:when test="string-length(normalize-space($VehOwnerName)) &gt; 30">
              <DIV style="overflow:auto;">
                <xsl:value-of select="$VehOwnerName"/>
              </DIV>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$VehOwnerName"/>
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td>
          <xsl:choose>
            <xsl:when test="@SourceApplicationName='FNOL'">FNOL</xsl:when>
            <xsl:otherwise>Adjuster</xsl:otherwise>
          </xsl:choose>
        </td>

        <xsl:if test="$UserID = 0">
          <td valign="top">
            <xsl:variable name="CarrierRepName">
              <xsl:value-of select="../@CarrierRepNameLast"/>
              <xsl:if test="../@CarrierRepNameLast != ''">
                <xsl:text disable-output-escaping="yes">, </xsl:text>
              </xsl:if>
              <xsl:value-of select="../@CarrierRepNameFirst"/>
            </xsl:variable>
            <xsl:choose>
              <xsl:when test="string-length($CarrierRepName) = 0">
                &#xa0;
              </xsl:when>
              <xsl:when test="string-length(normalize-space($CarrierRepName)) &gt; 30">
                <DIV style="overflow:auto;">
                  <xsl:value-of select="$CarrierRepName"/>
                </DIV>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$CarrierRepName"/>
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </xsl:if>

        <td align="center" valign="center">
          <xsl:for-each select="ServiceChannel">
            <xsl:choose>
              <xsl:when test="@DispositionTypeCD = 'CA'">Cancelled</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'CO'">Cash-Out</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'IC'">Insurance Co Managing</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'IME'">Included with ME Estimate</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'IPD'">Included with Body Estimate</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'NA'">Not Authorized</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'NOCA'">Not Covered</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'PCO'">Partial Cash-Out</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'RC'">Repair Complete</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'RNC'">Repair Not Completed</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'ST'">Stale</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'TL'">Total Loss</xsl:when>
              <xsl:when test="@DispositionTypeCD = 'VD'">Voided</xsl:when>
              <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
            <br/>
          </xsl:for-each>
        </td>


        <xsl:if test="$ClaimCreate = '1' and $ClientOfficeId != 'FCS'">
          <td title="Click to add vehicle" onmousemove="window.status='Click to add vehicle'" onmouseout="window.status=''" style="text-align:center;">
            <xsl:attribute name="onClick">
              <xsl:text disable-output-escaping="yes">GridClickAddVehicle(</xsl:text>
              <xsl:value-of select="../@LynxId"/>
              <xsl:text disable-output-escaping="yes">, event)</xsl:text>
            </xsl:attribute>
            <img src="images/AddVehicle.gif" alt="" width="40" height="30" border="0"/>
          </td>
        </xsl:if>


      </tr>

    </xsl:for-each>

  </xsl:template>

  <xsl:template name="getVehiclesList">

    <xsl:variable name="currentVehicle">
      VEH<xsl:value-of select="@VehicleNumber"/>,<xsl:value-of select="@VehicleYear"/>&#160;<xsl:value-of select="@Make"/>&#160;<xsl:value-of select="@Model"/><xsl:if test="not(position()=last())">,</xsl:if>
    </xsl:variable>
    <xsl:value-of select="normalize-space($currentVehicle)"/>

  </xsl:template>

</xsl:stylesheet>