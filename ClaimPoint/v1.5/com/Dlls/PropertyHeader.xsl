<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="pageTitle" select="'Information'"/>
<xsl:param name="claimNumber" select="1"/>
<xsl:param name="currentProperty"/>

<xsl:template match="Root">

  <xsl:comment>v1.4.2.0</xsl:comment>

  <xsl:apply-templates select="Claim"/>

</xsl:template>

<xsl:template match="Claim">

  <table border="0" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000099; background: #E6E5F5; border-collapse:collapse;" width="100%">
    <tr>
      <td height="20">
        <span class="bodyBlue">LYNX ID:</span>
        <span class="body"><xsl:value-of select="@LynxId"/></span>
      </td>
      <td align="right" nowrap="">
        <span class="bodyBlue">Claim Number:</span>
        <span class="body"><xsl:value-of select="@ClaimNumber"/></span>
      </td>
    </tr>
  </table>

</xsl:template>

</xsl:stylesheet>