<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" omit-xml-declaration="yes"/>
  <xsl:template match="/Root">
    <xsl:value-of select="concat(Detail/@City, '|', Detail/@StateCode, '|', Detail/@County)"/>
  </xsl:template>
</xsl:stylesheet>