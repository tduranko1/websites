<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:param name="tabIndexNo"/>

<xsl:template match="Root">

  <xsl:comment>v1.4.2.0</xsl:comment>

<tr>
  <td valign="top" nowrap="">
    <select name="PrimaryDamage" id="txtPrimaryDamage" required="true">
    <xsl:if test="$tabIndexNo != ''">
      <xsl:attribute name="tabIndex">
        <xsl:value-of select="$tabIndexNo"/>
      </xsl:attribute>
    </xsl:if>
      <option value=""></option>
        <xsl:for-each select="Reference [@List = 'ImpactPoint']" >
          <xsl:call-template name="getImpactPoints"/>
        </xsl:for-each>
    </select>
  </td>
  <td valign="top" nowrap="">
    <select name="SecondaryDamage" id="txtSecondaryDamage" size="6" multiple="true">
    <xsl:if test="$tabIndexNo != ''">
      <xsl:attribute name="tabIndex">
        <xsl:value-of select="$tabIndexNo"/>+1
      </xsl:attribute>
    </xsl:if>
      <option value=""></option>
        <xsl:for-each select="Reference [@List = 'ImpactPoint']" >
          <xsl:call-template name="getImpactPoints"/>
        </xsl:for-each>
    </select>
  </td>
  <td valign="middle" align="center" nowrap="">
    <img src="images/arrow.left.png" width="3" height="6" border="0" />
  </td>
  <td valign="top" style="font-size:8pt">
    To select multiple items, hold down the CTRL key while clicking on the desired options.
  </td>
</tr>

</xsl:template>

<xsl:template name="getImpactPoints">

  <option>
    <xsl:attribute name="value">
      <xsl:value-of select="@ReferenceID"/>
    </xsl:attribute>
      <xsl:value-of select="@Name"/>
  </option>

</xsl:template>

</xsl:stylesheet>
