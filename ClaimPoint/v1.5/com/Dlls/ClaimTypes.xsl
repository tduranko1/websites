<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="html" omit-xml-declaration="yes" standalone="yes" />

<xsl:template match="Root">

  <xsl:comment>v1.4.6.0</xsl:comment>

  <select name="AssignmentType" id="txtAssignmentType">
  	<option value="">
      * All *
    </option>
    <xsl:for-each select="Reference[@List='ClientAssignmentType']" >
      <xsl:sort select="@Name" order="ascending"/>
        <option>
            <xsl:attribute name="value"><xsl:value-of select="@ReferenceID"/></xsl:attribute>
            <xsl:value-of select="@Name"/>
        </option>
    </xsl:for-each>
  </select>

</xsl:template>
</xsl:stylesheet>
