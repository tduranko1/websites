<!--#include file="includes/incCommonTop.asp"-->
<%
  lsPageName = "frmWaPrint.asp"
  lsPageTitle = "Claim Print Layout"
%>

<%
  Dim liLynxID, lsGUID, lsGetWebAssignmentCall, lsUserClaimDetailCall
  Dim lsCRName, lsCRPhone, lsCRPhoneExt, lsCREmail, liCarrierRep, liInsCoLynxContactPhone
  Dim docXML, Node
  
  liLynxID = Request("LynxID")
  lsGUID = Request.Cookies("CPSession")("guidArray0")
  
  lsUserClaimDetailCall = "<UserClaimDetail LynxID=""" & server.urlencode(liLynxID) & """ />"
  
  set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML GetData(lsUserClaimDetailCall, "", "")

  If Not docXML.ParseError = 0 Then
    'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
    'Response.Write "<b>Error Description:</b> " & docXML.ParseError.reason & "<br>"
    'Response.Write "<b>Error File Position:</b> " & docXML.ParseError.filepos & "<br>"
    'Response.Write "<b>Error Line:</b> " & docXML.ParseError.line & "<br>"
    'Response.Write "<b>Error Line Position:</b> " & docXML.ParseError.linepos & "<br>"
    'Response.Write "<b>Error Source Text:</b> " & docXML.ParseError.srcText & "<br>"
  Else
    Set Node = docXML.documentElement.selectSingleNode("Claim") 
  
    lsCRName = Node.getAttribute("OwnerUserNameFirst") & " " & Node.getAttribute("OwnerUserNameLast")
    lsCRPhone = Node.getAttribute("OwnerUserPhoneAreaCode") & "-" & Node.getAttribute("OwnerUserPhoneExchangeNumber") & "-" & Node.getAttribute("OwnerUserPhoneUnitNumber")
    lsCRPhoneExt = Node.getAttribute("OwnerUserExtensionNumber")
    lsCREmail = Node.getAttribute("OwnerUserEmail")
    Set Node = Nothing
  End If
 
  Set docXML = Nothing

  liCarrierRep = Request.Cookies("CPSession")("CarrierRep")
  liInsCoLynxContactPhone = Request.Cookies("CPSession")("InsCoLynxContactPhone")
%>
<!--#include file="includes/incGetData.asp"-->

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

<html>
<head>
<title>Assignment Report</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" type="text/JavaScript">

  function ResizeMe()
  {
  	if(parent)
  		var objIFrame = parent.document.getElementById('prtFrame');
  	else
  		return false;
  
  	var objContainer = document.getElementById("divContainer")
  
  	if(objContainer)
  	{
  		objIFrame.style.height = objContainer.offsetHeight + 10 ; // resize iframe according to the height of its contents
  		top.window.scrollTo(0, 0);
  	}
  	else
  		objIFrame.height = 400; // default size
  } 


</script>


</head>

<body onLoad="ResizeMe();">

<div id="divContainer">

<%
  lsGetWebAssignmentCall = "<GetWebAssignmentXML LynxID=""" & server.urlencode(liLynxID) & """/>"

  Response.Write GetPersistedData(lsGetWebAssignmentCall, lsGUID, "WaPrintLayout.xsl", "LynxID=" & liLynxID & lcDelimiter & "CRName=" & lsCRName & lcDelimiter & "CRPhone=" & lsCRPhone & lcDelimiter & "CRPhoneExt=" & lsCRPhoneExt & lcDelimiter & "CREmail=" & lsCREmail & lcDelimiter & "CarrierRep=" & liCarrierRep & lcDelimiter & "LynxContactPhone=" & liInsCoLynxContactPhone, false)
%>

</div>

</body>
</html>
