<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.4.2.0 -->

<%
  lsPageName = "frmWaShopSelection.asp"
  lsPageTitle = "Shop Selection"
%>

<%
  dim liAreaCode, liExchangeNum, liZip, lsShopState, lsShopTypeCD, liMaxShops, lsVehDesc
  Dim lsGUID
  dim lsShopSrhByDistCall
  
  liAreaCode = Request("AreaCode")
  liExchangeNum = Request("ExchangeNumber")
  liZip = ""
  lsShopState = Request("ShopState")
  lsVehDesc = Request("vehDesc")
  lsShopTypeCD = "P"
  liMaxShops= "3"
  lsGUID = Request.Cookies("CPSession")("guidArray0")
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->


<html>
<head>
<title>Shop Selection</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "<%=Request("fromPage")%>";
  gsVehNum = "<%=Request("vehNum")%>";
  gsVehCount = "";
  
  function pageInit()
  {
    if(parent)
      parent.resetTimeOut();
  
    var liVehNum = gsVehNum;
    if (parent && document.getElementById("txtShopRemarks"))
      parent.getShopComments(liVehNum);
  }
  
  
  function setSelectedShop(cbObj)
  {
    var lcolElmsCB = document.getElementsByName("cb_SelectShop");
    var liCBLength = lcolElmsCB.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i) == cbObj)
        lcolElmsCB.item(i).checked = true;
      else
        lcolElmsCB.item(i).checked = false;
    }
  }
  
  
  function selectShop()
  {
    var liShopLocationID = "";
    var lsShopName = "";
    var lsShopChecked = "false";
    var lcolElmsCB = document.getElementsByName("cb_SelectShop");
    var liCBLength = lcolElmsCB.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i).checked == true)
      {
        liShopLocationID = lcolElmsCB.item(i).value;
        lsShopName = lcolElmsCB.item(i).nextSibling.value;
        lsShopChecked = "true";
      }
    }
  
    if (lsShopChecked == "false")
    {
      alert("* Please selected a shop before continuing.\n" +
            "    To return to the Summary screen, please press the CANCEL button.");
      return;
    }
    else
    {
      var lsShopComments = document.getElementById("txtShopRemarks").value;
      var liTxtAreaLength = lsShopComments.length;
      if (liTxtAreaLength > 250)
      {
        alert("* Only 250 characters are allowed for Comments to Repair Shop.\n" +
              "    You have typed "+ liTxtAreaLength +" characters.");
        return;
      }
  
      if(parent)
        parent.updShopSelection(gsVehNum, liShopLocationID, lsShopName, lsShopComments);
      else
        return false;
    }
  }
  
  
  function goAdvSearch()
  {
    if (parent)
    {
      var lsQS = "&AreaCode=<%=liAreaCode%>&ExchangeNumber=<%=liExchangeNum%>&ShopState=<%=lsShopState%>";
      parent.updShopComments(gsVehNum);
      parent.document.getElementById("uiFrame").src = "frmWaShopSelectionAdv.asp?fromPage=ShopSelection&vehNum=" + gsVehNum + lsQS + "&vehDesc=<%=lsVehDesc%>";
    }
  }
  
  
  function tryAdvSearch()
  {
    if (parent)
    {
      var lsQS = "&AreaCode=<%=liAreaCode%>&ExchangeNumber=<%=liExchangeNum%>&ShopState=<%=lsShopState%>";
      parent.document.getElementById("uiFrame").src = "frmWaShopSelectionAdv.asp?fromPage=ShopSelection&vehNum=" + gsVehNum + lsQS + "&vehDesc=<%=lsVehDesc%>";
    }
  }
  
  
  function goCancel()
  {
    if (parent)
    {
      gsVehCount = parent.getVehCount();
      parent.document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ShopSelection&vehCount="+gsVehCount;
    }
  }
  
  
  function reset_onclick() 
  {
    var loTblObj = document.getElementById("tblSearch");
    var lcolElms = loTblObj.getElementsByTagName("INPUT");
    var iLength = lcolElms.length;
    for(var i=0; i<iLength; i++)
    {
      if (lcolElms[i].type != "radio")
        lcolElms[i].value = "";
    }
    var elmsRD = document.getElementsByName("rd_searchType");
    elmsRD.item(0).checked = true;
  }


</script>

</head>

<body onLoad="resizeWaIframe(0); pageInit();">

<div id="divContainer">

<%
  lsShopSrhByDistCall = "<ShopSearchByDistance AreaCode=""" & server.urlencode(liAreaCode) & """ ExchangeNumber=""" & server.urlencode(liExchangeNum) & _
                        """ Zip="""" InsuranceCompanyID=""" & liInsuranceCompanyID & """ ShopTypeCode=""" & lsShopTypeCD & _
                        """ MaxShops=""" & liMaxShops & """/>"

  Response.Write GetPersistedData(lsShopSrhByDistCall, lsGUID, "WaShopSelection.xsl", "VehDesc=" & lsVehDesc & lcDelimiter & "PageTitle=" & lsPageTitle, false)
%>

</div>

</body>

</html>
