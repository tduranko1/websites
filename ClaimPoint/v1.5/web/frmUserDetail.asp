<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!-- v1.4.2.0 -->

<%
Dim lsUserType
lsUserType = request("hidUserType")
'if lsUserType <> "M" then
if (liOfficeID = "0" or liOfficeID = "") and lsUserType="M" then
  lsUserType = "M"
else
  lsUserType = "U"
end if

lsPageName = "frmUserDetail.asp"
lsHelpPage = "CP_User_Management.htm"

if request("hidUserID") = "-1" then
  lsPageTitle = "New User"
else
  lsPageTitle = "User Detail"
end if
if lsUserType = "M" then
  lsPageTitle = lsPageTitle + " for LYNX"
else
  lsPageTitle = lsPageTitle + " for " + lsInsuranceCompanyName
end if

liPageType = lcAfterInsuranceCompanySelect

bModal = false
Dim lsGUID, lsUserDetailCall
Dim liUserDetailID
liUserDetailID = request("hidUserID")

dim lsBrowserIE
if instr(Request.ServerVariables("HTTP_USER_AGENT"), "MSIE") > 0 then
  lsBrowserIE = 1
else
  lsBrowserIE = 0
end if

  lsGUID = Request.Cookies("CPSession")("guidArray0")
%>
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="javascript">
  var sInsuranceCompany = "<%=lsInsuranceCompanyName%>";
  var sInsuranceCompanyID = "<%=liInsuranceCompanyID%>";
  var sUserID = "<%=liUserDetailID%>";
  var sLoggedOnUserID = "<%=liUserID%>";
  var sRoleCarrierRepID = "17"; // Role: Carrier Representative
  var sRoleManageUsers = "18"; // Role: ClaimPoint Security Administrator
  var sAppCD = "<%=lsApplicationCD%>";
  var sAppID = "";
  var sActiveImgSrc = "images/btn_activate.gif";
  var sInactiveImgSrc = "images/btn_deactivate.gif";
  var sUserType = "<%=lsUserType%>";
  var bUserChecked = false;
  var bInUserCheck = false;
  var bInUpdateProcess = false;
  var iTimer = null;
  var sBrowserIE = "<%=lsBrowserIE%>";
  
  function updateDetails(){
    //if (bInUpdateProcess == true) return;

    /*if (bUserChecked == false) {
      if (bInUserCheck == false) {
        bInUserCheck = true;
        doCheckUser();
      }
      iTimer = window.setTimeout("updateDetails()", 250);
      return;
    }
    
    if (isNaN(iTimer) == false)
      window.clearTimeout(iTimer);*/
    if (validateData() == true) {
      //check if the user's email address was verified as duplicate?
      bInUserCheck = false;
      bInUpdateProcess = true;
      
      //this email address does not exist in the app.
      var objForm = document.getElementById("frmUserDetail");
      if (objForm) {
        objForm.UserID.value = sUserID;
        var bCarrierRep, bCPSecurityAdmin;
        var aRoleList = new Array();

        if (sUserID != "-1") {
          ldTrim(objForm.RoleList);
          var sRoleList = objForm.RoleList.value;
          aRoleList = sRoleList.split(",");
          bCarrierRep = false;
          bCPSecurityAdmin = false;
          for (var i = 0; i < aRoleList.length; i++) {
            if (aRoleList[i] == sRoleCarrierRepID)
              bCarrierRep = true;
            if (aRoleList[i] == sRoleManageUsers)
              bCPSecurityAdmin = true;
          }
          
          if (bCarrierRep == false) {
            aRoleList.push(sRoleCarrierRepID);
          }
        } else {
          aRoleList.push(sRoleCarrierRepID);
        }
        
          
        var obj = document.getElementById("txtUserManageUsers");
        if (obj && bCPSecurityAdmin == false && obj.value == true) {
          aRoleList.push(sRoleManageUsers);
        }
        sRoleList = aRoleList.join(",");
        objForm.RoleList.value = sRoleList;
        
        obj = document.getElementById("txtUserClaimViewLevel");
        if (obj && (sUserType != "M" || obj.value != ""))
          objForm.ProfileList.value = obj.getAttribute("ProfileID") + "," + obj.value;
                
        obj = document.getElementById("txtPaymentAuthorizationLevel");
        if (obj && obj.value != "") {
          if (objForm.ProfileList.value != "")
            objForm.ProfileList.value += ","; //add this comma only when stuff were overridden from CP
          objForm.ProfileList.value += obj.getAttribute("ProfileID") + "," + obj.value;; //add the payment authoriation level
        }

        obj = document.getElementById("OverriddenProfileItems");
        if (obj && obj.value != "") {
          if (objForm.ProfileList.value != "")
            objForm.ProfileList.value += ","; //add this comma only when stuff were overridden from CP
          objForm.ProfileList.value += obj.value; //add the overridden stuff from APD
        }

        var aCRUD = new Array(0,0,0,0);
        var aDefaultCRUD = new Array(0,0,0,0);
        obj = document.getElementById("txtUserViewClaim");
        var sPermissionID = obj.getAttribute("PermissionID")
        aDefaultCRUD[1] = obj.getAttribute("defaultPermission");
        if (obj && obj.checked == true)
          aCRUD[1] = 1;

        obj = document.getElementById("txtUserCreateClaim");
        aDefaultCRUD[0] = obj.getAttribute("defaultPermission");
        if (obj && obj.checked == true) {
          aCRUD[0] = 1;
          aCRUD[1] = 1;
          aCRUD[2] = 1;
        }
        
        obj = document.getElementById("txtUserUpdateClaim");
        aDefaultCRUD[2] = obj.getAttribute("defaultPermission");
        if (obj && obj.checked == true) {
          aCRUD[1] = 1;
          aCRUD[2] = 1;
        }

        objForm.PermissionList.value = "";
        if (aDefaultCRUD.join("") != aCRUD.join(""))
          objForm.PermissionList.value = sPermissionID + "," + aCRUD.join(",");
        
        obj = document.getElementById("txtUserViewReports");
        if (obj){
          if ((obj.getAttribute("defaultPermission") != "0110") && (obj.checked == true) || (obj.getAttribute("defaultPermission") == "0110") && (obj.checked == false)) {
            if (objForm.PermissionList.value != "")
              objForm.PermissionList.value += ",";
              
            objForm.PermissionList.value += obj.getAttribute("PermissionID");
            if (obj.checked == true)
              objForm.PermissionList.value += ",0,1,1,0";
            else
              objForm.PermissionList.value += ",0,0,0,0";
          }
        }
        
        obj = document.getElementById("txtUserManageUsers");
        if (obj){
          if ((!(obj.getAttribute("defaultPermission") == "111" || obj.getAttribute("defaultPermission") == "011") && (obj.checked == true)) || ((obj.getAttribute("defaultPermission") == "011") && (obj.checked == false))) {
            if (objForm.PermissionList.value != "")
              objForm.PermissionList.value += ",";
              
            objForm.PermissionList.value += obj.getAttribute("PermissionID");
            if (obj.checked == true)
              objForm.PermissionList.value += ",1,1,1,0";
            else
              objForm.PermissionList.value += ",0,0,0,0";
          }
        }

        obj = document.getElementById("txtAuthorizePayments");
        if (obj){
          if ((!(obj.getAttribute("defaultPermission") == "111" || obj.getAttribute("defaultPermission") == "011") && (obj.checked == true)) || ((obj.getAttribute("defaultPermission") == "011") && (obj.checked == false))) {
            if (objForm.PermissionList.value != "")
              objForm.PermissionList.value += ",";
              
            objForm.PermissionList.value += obj.getAttribute("PermissionID");
            if (obj.checked == true)
              objForm.PermissionList.value += ",0,0,1,1";
            else
              objForm.PermissionList.value += ",0,0,0,0";
          }
        }

        obj = document.getElementById("OverriddenPermissionItems");
        if (obj && obj.value != "") {
          if (objForm.PermissionList.value != "")
            objForm.PermissionList.value += ",";
          objForm.PermissionList.value += obj.value;
        }

        obj = document.getElementById("SysLastUserID");
        if (obj)
          obj.value = sLoggedOnUserID;

        objForm.ApplicationID.value = sAppID;
        objForm.EmailAddress.value = escape(objForm.txtEmailAddress.value);
        objForm.NameFirst.value = escape(objForm.txtNameFirst.value);
        objForm.NameLast.value = escape(objForm.txtNameLast.value);
        objForm.ClientUserID.value = escape(objForm.txtClientID.value);
        objForm.InsuranceCompanyID.value = sInsuranceCompanyID;
        objForm.ApplicationCD.value = sAppCD;

        if (objForm.UserEmailAddress.value == "")
          objForm.UserEmailAddress.value = objForm.EmailAddress.value;

        if (sUserID == "-1") {
          objForm.mode.value = "insert";
          showMsg("Creating the New User...");
        } else {
          objForm.mode.value = "update";
          showMsg("Updating User Details...");
        }
        objForm.submit();
      }
    } else
      hideMsg();
  }
  
  function validateData(){
    showMsg("Validating User information...");
    var obj = document.getElementById("txtEmailAddress");
    if (obj.value == "") {
      alert("Login Id cannot be empty. Please enter an valid email address for the Login Id.");
      obj.focus();
      return false;
    }

    ldTrim(obj); //remove any leading/trailing spaces in the email address
    if (isEmail(obj.value) == false) {
      alert("Invalid e-mail address specified for Login ID. Please enter it in the following format: name@domain.nn \n" +
            "The following characters are not valid for the name part of the e-mail address. \n" +
            "     > greater than \n" +
            "     < less than \n" +
            "     ( open parentheses \n" +
            "     ) close parentheses \n" +
            "     [ open brackets \n" +
            "     ] close brackets \n" +
            "     \\ back slash \n" +
            "     % percent \n" +
            "     & ampersand \n" +
            "     + plus \n" +
            "     . period (see note below)\n" +
            "     , comma \n" +
            "     ; semicolon \n" +
            "     : colon \n" +
            "       whitespace character (such as a space or tab) \n" +
            "     @ at sign (as part of the name) \n" +
            "     \" double quote \n" +
            "Note: If there are any periods before the @ sign they must be followed by one or more valid characters. \n" +
            "A period cannot be the first character and there cannot be consecutive periods before the @ sign.");
      obj.value = "";
      obj.focus();
      obj.select();
      return false;
    }

    ldTrim(obj); //remove any leading/trailing spaces in the first name
    obj = document.getElementById("txtNameFirst");
    if (obj.value == "") {
      alert("First Name cannot be empty. Please enter an valid First Name.");
      obj.focus();
      return false;
    }

    ldTrim(obj); //remove any leading/trailing spaces in the last name
    obj = document.getElementById("txtNameLast");
    if (obj.value == "") {
      alert("Last Name cannot be empty. Please enter an valid Last Name.");
      obj.focus();
      return false;
    }

    obj = document.getElementById("txtPhoneAreaCode");
    if (obj.value == "") {
      alert("Phone area code number cannot be empty. Please enter an valid phone area code number.");
      obj.focus();
      return false;
    }

    if (isNaN(obj.value)) {
      alert("A numeric value is expected for the Phone area code.");
      obj.focus();
      obj.select();
      return false;
    }

    if (obj.value.length < 3) {
      alert("Phone area code must be a 3 digit numeric value.");
      obj.focus();
      obj.select();
      return false;
    }
    
    obj = document.getElementById("txtPhoneExchangeNumber");
    if (obj.value == "") {
      alert("Phone exchange number cannot be empty. Please enter an valid phone exchange number.");
      obj.focus();
      return false;
    }

    if (isNaN(obj.value)) {
      alert("A numeric value is expected for the Phone exchange.");
      obj.focus();
      obj.select();
      return false;
    }
    
    if (obj.value.length < 3) {
      alert("Phone exchange must be a 3 digit numeric value.");
      obj.focus();
      obj.select();
      return false;
    }
    obj = document.getElementById("txtPhoneUnitNumber");
    if (obj.value == "") {
      alert("Phone unit  number cannot be empty. Please enter an valid phone unit number.");
      obj.focus();
      return false;
    }

    if (isNaN(obj.value)) {
      alert("A numeric value is expected for the Phone unit");
      obj.focus();
      obj.select();
      return false;
    }
    
    if (obj.value.length < 4) {
      alert("Phone unit must be a 4 digit numeric value.");
      obj.focus();
      obj.select();
      return false;
    }
    
    obj = document.getElementById("txtPhoneExtensionNumber");
    if (isNaN(obj.value)) {
      alert("A numeric value is expected for the Phone extension.");
      obj.focus();
      obj.select();
      return false;
    }
    
    obj = document.getElementById("selOfficeID");
    if (obj && obj.selectedIndex < 1) {
      alert("Please select an office from the list.");
      obj.focus();
      return false;
    }

    if (sUserType != "M") {
      obj = document.getElementById("txtUserClaimViewLevel");
      if (obj && obj.selectedIndex < 1 && document.getElementById("txtUserViewClaim").checked == true) {
        alert("Please select an item from the ClaimPoint Claim View Level list.");
        obj.focus();
        return false;
      }
    }
    
    obj = document.getElementById("txtAuthorizePayments");
    if (obj)
    {
      var objPaired = document.getElementById("txtPaymentAuthorizationLevel");
      var objPairedVal = objPaired.value;
      if ((obj && obj.checked == true) && (parseInt(objPairedVal, 10) < 0 || objPairedVal == "")) {
        alert("You have checked the Authorize Payments checkbox. Please enter the Payment Authorization Level amount.");
        objPaired.focus();
        return false;
      }
    }

    obj = document.getElementById("txtAuthorizePayments");
    if (obj)
    {
      var objPaired = document.getElementById("txtPaymentAuthorizationLevel");
      var objPairedVal = objPaired.value;
      if (obj && obj.checked == false && parseInt(objPairedVal, 10) > 0) { 
        if (confirm("You have entered a Payment Authorization Level amount. Do you want to check 'Authorize Payments'?") == true)
          obj.checked = true;
        else 
          objPaired.value = "";
      }
    }

    if (document.getElementById("rbUserActiveYes").checked == false && document.getElementById("rbUserActiveNo").checked == false) {
      var bRet = false;
      bRet = confirm("Would you like to activate the user while saving?.");
      if (bRet == true) {
        obj = document.getElementById("rbUserActiveYes");
        obj.checked = true;
      } else {
        obj = document.getElementById("rbUserActiveNo");
        obj.checked = true;
      }
    }
    return true;
  }
  
  function checkReadPermission(){
    if (document.getElementById("txtUserCreateClaim").checked == true)
      document.getElementById("txtUserViewClaim").checked = true;
  }
  
  function activateDeactivateUser(){
      var objForm = document.getElementById("frmUserDetail");
      if (objForm) {
        objForm.UserID.value = sUserID;
        objForm.ApplicationID.value = sAppID;
        var obj = document.getElementById("SysLastUserID");
        if (obj)
          obj.value = sLoggedOnUserID;
        objForm.mode.value = "activate";
        
        objForm.submit();
      }
    
  }
  
  function setUserStatus(sActive, sDate, sLastUpdatedDate) {
    var sRet = "";
    if (sActive == "1") {
      sRet = "Active since ";
      document.getElementById("imgActiveInactive").src = sInactiveImgSrc;
      document.getElementById("imgActiveInactive").title = "Deactivate User";
    } else {
      sRet = "Inactive since ";
      document.getElementById("imgActiveInactive").src = sActiveImgSrc;
      document.getElementById("imgActiveInactive").title = "Activate User";
    }
      
      sRet += extractSQLDateTime(sDate);
      var obj = document.getElementById("tdUserStatus");
      if (obj) {
        obj.innerHTML = sRet;
      }
      
      var objForm = document.getElementById("frmUserDetail");
      if (objForm && sLastUpdatedDate != "") {
        objForm.AppSysLastUpdatedDate.value = sLastUpdatedDate;
        objForm.SysLastUpdatedDate.value = sLastUpdatedDate;
      }
  }

  function extractSQLDateTime( strDate )
  {
      var strConv =
           strDate.substr( 5, 2 ) + "/"
         + strDate.substr( 8, 2 ) + "/"
         + strDate.substr( 0, 4 ) + " "
         + strDate.substr( 11, 8 );
      return strConv;
  }
  
  function isEmail(strEmail) {
    var re = /^(([^<>()[\]\\%&+.,;:\s@\"]+(\.[^<>()[\]\\%&+.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(strEmail);
  }
  
  function goBack() {
    window.document.location.href = "frmUserList.asp?M=" + sUserType;
  }
  
  function doCheckUser(){
    //User clicked on a hyperlink (cancel or menu option). Don't check Login ID
    if (document.activeElement.tagName == "A")
      return;

    if (isNaN(iTimer) == false)
      window.clearTimeout(iTimer);

      var objForm = document.getElementById("frmUserDetail");
      if (objForm) {
        var obj = objForm.txtEmailAddress;
        if (obj.value == "") return;

        if (isEmail(obj.value) == false) {
          alert("Invalid e-mail address specified for Login ID. Please enter it in the following format: name@domain.nn \n" +
                "The following characters are not valid for the name part of the e-mail address. \n" +
                "     > greater than \n" +
                "     < less than \n" +
                "     ( open parentheses \n" +
                "     ) close parentheses \n" +
                "     [ open brackets \n" +
                "     ] close brackets \n" +
                "     \\ back slash \n" +
                "     % percent \n" +
                "     & ampersand \n" +
                "     + plus \n" +
                "     . period (see note below)\n" +
                "     , comma \n" +
                "     ; semicolon \n" +
                "     : colon \n" +
                "       whitespace character (such as a space or tab) \n" +
                "     @ at sign (as part of the name) \n" +
                "     \" double quote \n" +
                "Note: If there are any periods before the @ sign they must be followed by one or more valid characters. \n" +
                "A period cannot be the first character and there cannot be consecutive periods before the @ sign.");
          obj.value = "";
          obj.focus();
          obj.select();
          return false;
        }
        showMsg("Checking for duplicate Login ID...");
        objForm.ApplicationID.value = sAppID;
        objForm.EmailAddress.value = escape(obj.value);
        objForm.InsuranceCompanyID.value = sInsuranceCompanyID;
        objForm.ApplicationCD.value = sAppCD;
        objForm.mode.value = "check";
        objForm.submit();
      }
  }
  
  function showMsg(str){
    window.scrollTo(0,0);
    var objMsg = document.getElementById("msgDiv");
    if (objMsg){
      var objMsgTxt = document.getElementById("msgTxt");
      if (objMsgTxt) {
        objMsgTxt.innerHTML = str;
      }
      objMsg.style.display = "inline";
    }
  }

  function hideMsg(){
    var objMsg = document.getElementById("msgDiv");
    if (objMsg){
      objMsg.style.display = "none";
    }
  }
  
  function changeLoginID()
  {
    var iFrmObj = document.getElementById("IFrmAwareIDChange");
    var sCurrLoginID = document.getElementById("txtEmailAddress").value;
    iFrmObj.src = "frmUserLoginChange.asp?CurrLoginID=" + sCurrLoginID;
    document.getElementById("ChangeLoginIDifrmTD").style.display = "inline";
  }

  function close()
  {
    var iFrmObj = document.getElementById("IFrmAwareIDChange");
    iFrmObj.src = "blank.asp";
    document.getElementById("ChangeLoginIDifrmTD").style.display = "none";
  }

</script>
<%

  if liManageUsers = 1 then

      lsUserDetailCall = "<UserDetail ApplicationCD=""" + lsApplicationCD + """ UserID=""" + liUserDetailID + """/>"

    Dim lsUserDetail
    lsUserDetail = GetPersistedData(lsUserDetailCall, lsGUID, "UserDetail.xsl", "UserType=" & lsUserType & lcDelimiter & "InsuranceID=" & liInsuranceCompanyID & lcDelimiter & "CarrierAuthorizesPaymentsFlag=" & liCarrierAuthorizesPaymentsFlag, false)

    response.write lsUserDetail
  else
    response.write "You don't have permission to maintain users. Please contact your supervisor."
  end if
%>

  <div name="msgDiv" id="msgDiv" style="position:absolute;display:none;">
    <table width="220" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #808080; background-color:#EEEEEE; border-collapse:collapse;">
      <tr>
        <td height="30" id="msgTxt" name="msgTxt" style="text-align:center; font-size:12px; font-weight:bold; color:#000080;"></td>
      </tr>
      <tr>
        <td height="10" style="text-align:center"><img src="images/loading_anim.gif" alt="" width="94" height="17" border="0"></td>
      </tr>
      <tr>
        <td height="10">&nbsp;</td>
      </tr>
    </table>
  </div>
<script language="JavaScript" src="includes/jquery-1.3.2.min.js" type="text/javascript"></script>
<script language="javascript">
  var objMsg = document.getElementById("msgDiv");
  if (objMsg){
    objMsg.style.top = (document.body.offsetHeight - 100) / 2;
    objMsg.style.left = (document.body.offsetWidth - 300) / 2;
  }
 window.setTimeout("sortDropDownListByText('selOfficeID')", 250);
  function sortDropDownListByText(selectId) {
      if(document.getElementById(selectId) != null || document.getElementById(selectId) != undefined)
      {
          var OriginalVal = $('#' + selectId).val();
          var foption = $('#' + selectId + ' option:first');
          var soptions = $('#' + selectId + ' option:not(:first)').sort(function (a, b) {
              return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
          });
          $('#' + selectId).html(soptions).prepend(foption);
          $('#' + selectId)[0].value = OriginalVal;
      }
  }
</script>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
