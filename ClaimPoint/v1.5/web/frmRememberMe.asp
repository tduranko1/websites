<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>About Remember Me</title>
  <link href="includes/apd_style.css" type="text/css" rel="stylesheet">
</head>

<body>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
  <TR>
  	<TD><IMG SRC="images/modal_TL.gif" ALT="" width="90" height="45"></TD>
  	<TD width="100%" align="right" valign="top">
    <a href=# onclick="javascript:window.close()" class="mini">Close</a>&nbsp;&nbsp;
    </TD>
  </TR>
</TABLE>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
  <TR>
  	<TD width="10%"><IMG SRC="images/modal_ML.gif" ALT="" width="70" height="65"></TD>
  	<TD width="100%" background="images/modal_MC.gif" valign="top">
    <br>
    <div align="center"><p class="bodyBlue">Remember My Login</p></div>
    </TD>
  	<TD width="10%"><IMG SRC="images/modal_MR.gif" ALT="" width="70" height="65"></TD>
  </TR>
</TABLE>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="10">
  <TR>
  	<TD width="10%">
      <p class="login">If you leave the box unchecked then you will be required to type in your login information each time you visit our site on this computer. Users who share a computer or anyone concerned about someone else accessing our site using their computer should leave this box UNCHECKED.</p>
      <p class="login">If you check the box, then a cookie will be used to remember your login name and password on this computer. This cookie is only used by our site and contains no personal information. Users that have their own computer and are not concerned about others using their computer to access our site can CHECK this box to make their browsing experience easier.</p>
    </TD>
  </TR>
</TABLE>
 
 
</body>
</html>
