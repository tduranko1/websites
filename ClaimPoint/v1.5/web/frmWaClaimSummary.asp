<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetCurrAssignment.asp"-->
<!-- v1.4.5.0 -->

<%

Dim  lsShopAssignmentType

lsShopAssignmentType= Request("ShopAssignmentType")

  'Response.Write lsShopAssignmentType
 If Not lsShopAssignmentType Is Nothing Then
   If lsShopAssignmentType = "ChoiceShop" Then
     lsCurrentAssignmentDesc = "Choice Shop"
   End If

   If lsShopAssignmentType = "ProgramShop" Then
     lsCurrentAssignmentDesc = "Program Shop"
   End If

  End If

  'lsCurrentAssignmentDesc="Program Shop"

  lsPageName = "frmWaClaimSummary.asp"
  lsPageTitle = lsCurrentAssignmentDesc & " Assignment - Summary"
  liPageType = lcAssignmentPage
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

<!-- include to check for and display if necessary APD offline/outage messages -->
<!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->
 <% Dim IsEmailCell
        IsEmailCell = "<Config Path=""ClaimPoint/Customizations/InsuranceCompany[@ID='" & liInsuranceCompanyID & "']/@RepairReferral""/>"

        IsEmailCell = GetData(IsEmailCell, "", "")
        docXML.Async = False
        docXML.loadXML IsEmailCell
        IsEmailCell = docXML.documentElement.text
        IsEmailCell = Lcase(IsEmailCell) %>

<html>
<head>
<title><%= lsCurrentAssignmentDesc %> - Summary</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "";
  gsVehNum = "";
  gsVehCount = "<%=Request("vehCount")%>";
  var gsClaimNumber = "";
  giReserveLineTypeScript = <%= liCurrAssignmentReserveLineType %>;
  // 10Feb2012 - TVD - Elephant
  giInsuranceCompanyID = <%= liInsuranceCompanyID %>;
  
  function pageInit()
  {
     if(parent)
      parent.resetTimeOut();
  
    var lsCurrAssignment = "<%= lsCurrentAssignmentDesc %>";
    var lsCurrHelpURL;

    if (lsCurrAssignment == "Adverse Subro Desk Review")
      lsCurrHelpURL = "CP_Adverse_Subro_Desk_Review.htm";
    else if (lsCurrAssignment == "Demand Estimate Review")
      lsCurrHelpURL = "CP_Demand_Estimate_Review.htm";
    else if (lsCurrAssignment == "IA Estimate Review")
      lsCurrHelpURL = "CP_IA_Estimate_Review.htm";
    else if (lsCurrAssignment == "Program Shop")
      lsCurrHelpURL = "CP_Program_Shop_Assignment.htm";

    if (parent)
      parent.gsHelpSubTopicPage = lsCurrHelpURL;

    addVehicleDiv(gsVehCount);
    
     if(parent)
      parent.getClaimSummaryData(gsVehCount);

      var IsDRPChoice =  "<%= lsShopAssignmentType %>"; 
        //alert(IsDRPChoice);
      if(IsDRPChoice != ""){
         document.getElementById("AddNewVehDiv").style.display = "none";
          }
      else{
         document.getElementById("AddNewVehDiv").style.display = "block";
         }
  }
  
  function setRental() 
    {
        var lsCurrAssignment = "<%= lsCurrentAssignmentDesc %>";
        
        if (lsCurrAssignment == "Repair Referral")
        {
            var trRow1 = document.getElementById("trRental1");
            var trRow2 = document.getElementById("trRental2");
            var trRow3 = document.getElementById("trRental3");
            var trRow4 = document.getElementById("trRental4");

//Additional Rental info
            var trRow5 = document.getElementById("trRental5");
            var trRow6 = document.getElementById("trRental6");
            var trRow7 = document.getElementById("trRental7");
            var trRow8 = document.getElementById("trRental8");
            var trRow9 = document.getElementById("trRental9");
            var trRow10 = document.getElementById("trRental10");
           

            if (trRow1.id != null) 
            {
                trRow1.style.display = "none";
            }
            if (trRow2.id != null) 
            {
                trRow2.style.display = "none";
            }
            if (trRow3.id != null) 
            {
                trRow3.style.display = "none";
            }
            if (trRow4.id != null) 
            {
                trRow4.style.display = "none";
            }
 //Additional Rental info
            if (trRow5.id != null) 
            {
                trRow5.style.display = "none";
            }
            if (trRow6.id != null) 
            {
                trRow6.style.display = "none";
            }
            if (trRow7.id != null) 
            {
                trRow7.style.display = "none";
            }
            if (trRow8.id != null) 
            {
                trRow8.style.display = "none";
            }
            if (trRow9.id != null) 
            {
                trRow9.style.display = "none";
            }
            if (trRow10.id != null) 
            {
                trRow10.style.display = "none";
            }
            
        }

    }
  
  function addVehicleDiv(gsVehCount)
  {
    if(parent)
    {
      var lsCurrAssignment = "<%= lsCurrentAssignmentDesc %>";
      var liVehNum;
      var liDispOrder = 0;
      var liCurrRow;
      var liVehSummDispType;
      var sHtml;
      var laryVehCount = gsVehCount.split(",");
      var liAryLength = laryVehCount.length;
      var loVehInsDiv = document.getElementById("vehTblHolder");
      var IsRepairEmailCell = "<%= IsEmailCell %>"
      IsRepairEmailCell = IsRepairEmailCell.replace("<config>", "");
      IsRepairEmailCell = IsRepairEmailCell.replace("</config>", "");
      IsRepairEmailCell = IsRepairEmailCell.toLowerCase();

      loVehInsDiv.innerHTML = "";  

      //10Feb2012 - TVD - Get vehicle data from data container - Elephant
      loDcIFrameDoc = parent.getdcIFrameDoc();

//alert(loDcIFrameDoc.getElementById("txtInsuredNameFirst").value);
//window.clipboardData.setData("Text", loDcIFrameDoc);
//alert('Clipboard copied');
	  
      for (x=2; x<liAryLength; x++)
      {
        if (laryVehCount[x] != "")
        {
          liVehNum = laryVehCount[x];
          liDispOrder++;
          liCurrRow = x-2;
          loNewDiv = document.createElement("DIV");
          loVehInsDiv.appendChild(loNewDiv);
          liVehSummDispType = parent.getVehSummType(liCurrRow);
          
          //alert(liVehSummDispType);
          if (liVehSummDispType == 1)
          {
            sHtml = "<br><br><table width=700 border=10 cellpadding=0 cellspacing=0 style='border:1px solid #000099; border-collapse:collapse'><tr><td><table width=100% border=0 cellpadding=0 cellspacing=0 style='border-collapse:collapse; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>&nbsp;Vehicle "+liDispOrder+" Information</td><td align=right><a href=javascript:deleteVehicle("+liVehNum+") title='Delete this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_delete_blue.gif WIDTH=83 HEIGHT=31></a><img border=0 src=images/spacer.gif WIDTH=10 HEIGHT=1><a href=javascript:editVehicle("+liVehNum+") title='Edit this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_edit_blue.gif WIDTH=83 HEIGHT=31></a></td></tr></table></td></tr><tr><td valign=top style='padding: 0px 10px 10px 10px;'><table name=VehicleInfoTable id=VehicleInfoTable_"+liVehNum+" border=0 cellpadding=0 cellspacing=0><tr><td width=152 nowrap class=legendSumm>Party:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Coverage:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Deductible:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Limit:</td></tr><tr><td nowrap><input type=text class=inputView id=txtExposureCD_"+liVehNum+" size=9 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtClientCoverageTypeDesc_"+liVehNum+" size=17 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtDeductibleAmt_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtLimitAmt_"+liVehNum+" size=16 readonly=true></td></tr>";

            if (giReserveLineTypeScript == 1)
              sHtml += "<TR><TD nowrap class=legendSumm>	Line Type:</TD><TD nowrap></TD><TD nowrap class=legendSumm>	Reserve Number:</TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR><TR><TD nowrap><INPUT type=text class=inputView id=txtLineType_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap><INPUT type=text class=inputView id=txtReserve_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR>";

            //15Feb2012 - TVD - Elephant Changes
            if (giInsuranceCompanyID == 374) {
				var txtPassThruDataVeh;
				txtPassThruDataVeh = loDcIFrameDoc.getElementById("txtSourceApplicationPassthruDataVeh_" + liVehNum ).value;

                if (txtPassThruDataVeh == "E") {
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<br/>"; 
                    sHtml += "<input type=radio id=rbTypeAuthorized_" + liVehNum + "  enabled=false>Authorized to Proceed with Repairs";
                    sHtml += "</td></tr>";
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<input type=radio  id=rbTypeEstimate_" + liVehNum + "  enabled=false checked>Secure Estimate Only";
                    sHtml += "<br/></td></tr>";
                }
                else {
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<br/>"; 
                    sHtml += "<input type=radio id=rbTypeAuthorized_" + liVehNum + " enabled=false checked>Authorized to Proceed with Repairs";
                    sHtml += "</td></tr>";
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<input type=radio  id=rbTypeEstimate_" + liVehNum + "  enabled=false>Secure Estimate Only";
                    sHtml += "<br/></td></tr>";
                }
            }
           
           //glsd451 Changes - Adding new controls as per andrew's requirement 
           if((lsCurrAssignment == "Program Shop" || lsCurrAssignment == "Repair Referral") && IsRepairEmailCell == "true")
           {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap class=legendSumm colspan=3>Email: </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap class=legendSumm>Cell phone carrier:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactEmailAddress_"+liVehNum+" readonly=true> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtCellPhoneCarrier_"+liVehNum+" readonly=true> </td></tr><tr><td nowrap class=legendSumm colspan=3>Preferred method of status updates:</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtPrefMethodUpd_"+liVehNum+"  readonly=true></td><td nowrap> </td><td nowrap></td><td nowrap> </td><td nowrap> </td></tr>"           
            }
            else
            {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr>"
            }
            sHtml += "<tr id='trRental1'><td width=152 nowrap class=legendSumm>Apply Rental:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Days:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Rental Daily Maximum:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Rental Maximum:</td></tr>"
            sHtml += "<tr id='trRental2'><td nowrap><input type=text class=inputView id=txtRentalAuthorized_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalDays_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalDayAmount_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalMaxAmount_"+liVehNum+" size=16 readonly=true></td></tr>"
            sHtml += "<tr id='trRental3'><td nowrap class=legendSumm>Rental Days Auth.:</td><td nowrap> </td><td nowrap colspan=3 class=legendSumm>Rental Instructions:</td><td nowrap> </td><td nowrap> </td></tr>"
            sHtml += "<tr id='trRental4'><td nowrap valign=top><input type=text class=inputView id=txtRentalDaysAuthorized_"+liVehNum+" style='text-align:right' size=3 readonly=true></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea wrap='physical' class=inputView id=txtRentalInstructions_"+liVehNum+" readonly=true style='width:100%; height:50px;'></textarea></td></tr>"

   //Additional Rental Info
            sHtml += "<tr><td colspan='7'><table id=tblAdditionalRental_"+liVehNum+" style='border:1px solid #000099; margin-top:10px;'><tr><td colspan='7'><table cellpadding=5 cellspacing=0 style='border-collapse:collapse; width:100%; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>Additional Rental Info : </td></tr></table></td></tr><td style='padding:0px 10px 10px 10px;'><table>"
            sHtml += "<tr id='trRental5'><td width=150 nowrap class=legendSumm>Rental Vendor:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Res/Conf:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Vehicle Class:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rate Type:</td></tr>"
            sHtml += "<tr id='trRental6'><td nowrap><input type=text class=inputView id=hidRentalVendor_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalResConf_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalVehicleClass_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalRateType_"+liVehNum+" size=15 readonly=true></td></tr>"

            sHtml += "<tr id='trRental7'><td width=150 nowrap class=legendSumm>Rate:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm> Auth Pickup: </td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Tax Rate:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Phone:</td></tr>"
            sHtml += "<tr id='trRental8'><td nowrap>$<input type=text class=inputView id=txtRentalRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalAuthPickup_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalTaxRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalPhoneAC_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneEN_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneUN_"+liVehNum+" size=7 readonly=true></td></tr>"

            sHtml += "<tr id='trRental9'><td width=150 nowrap class=legendSumm>Rental Address: </td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Rental City:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental State</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Zip:</td></tr>"
            sHtml += "<tr id='trRental10'><td nowrap><input type=text class=inputView id=txtRentalAddress_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalCity_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalState_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalZip_"+liVehNum+" size=15 readonly=true></td></tr>"
            sHtml +="</table></td></tr></table></td></tr>"
//END

            sHtml += "<tr><td colspan=2 nowrap class=legendSumm>Vehicle VIN:</td><td colspan=4 nowrap class=legendSumm>Vehicle YMM:</td><td> </td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtVIN_"+liVehNum+" size=24 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtVehicleYear_"+liVehNum+" size=5 readonly=true> <input type=text class=inputView id=txtMake_"+liVehNum+" size=15 readonly=true> <input type=text class=inputView id=txtModel_"+liVehNum+" size=15 readonly=true></td><td nowrap></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle License Plate:</td><td colspan=1 nowrap class=legendSumm>Vehicle Drivable:</td><td nowrap> </td><td colspan=3 nowrap class=legendSumm>Odometer:</td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtLicensePlateNumber_"+liVehNum+" size=24 readonly=true></td><td nowrap colspan=2><input type=text class=inputView id=txtDrivable_"+liVehNum+" size=5 readonly=true></td><td nowrap colspan=3><input type=text class=inputView colspan=4 id=txtMileage_"+liVehNum+" size=8 readonly=true></td></tr><tr><td nowrap class=legendSumm>Primary Damage:</td><td nowrap> </td><td colspan=5 nowrap class=legendSumm>Secondary Damages:</td></tr><tr><td nowrap valign=top><textarea wrap='physical' class=inputView id=txtPrimaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtSecondaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr></table></tr></table>"
          }
          else if (liVehSummDispType == 2)
          {
            sHtml = "<br><br><table width=700 border=0 cellpadding=0 cellspacing=0 style='border:1px solid #000099; border-collapse:collapse'><tr><td><table width=100% border=0 cellpadding=0 cellspacing=0 style='border-collapse:collapse; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>&nbsp;Vehicle "+liDispOrder+" Information</td><td align=right><a href=javascript:deleteVehicle("+liVehNum+") title='Delete this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;'><img border=0 src=images/btn_delete_blue.gif WIDTH=83 HEIGHT=31></a><img border=0 src=images/spacer.gif WIDTH=10 HEIGHT=1><a href=javascript:editVehicle("+liVehNum+") title='Edit this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;'><img border=0 src=images/btn_edit_blue.gif WIDTH=83 HEIGHT=31></a></td></tr></table></td></tr><tr><td valign=top style='padding: 0px 10px 10px 10px;'><table name=VehicleInfoTable id=VehicleInfoTable_"+liVehNum+" border=0 cellpadding=0 cellspacing=0><tr><td width=152 nowrap class=legendSumm>Party:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Coverage:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Deductible:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Limit:</td></tr><tr><td nowrap><input type=text class=inputView id=txtExposureCD_"+liVehNum+" size=9 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtClientCoverageTypeDesc_"+liVehNum+" size=17 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtDeductibleAmt_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtLimitAmt_"+liVehNum+" size=16 readonly=true></td></tr>";

            if (giReserveLineTypeScript == 1)
              sHtml += "<TR><TD nowrap class=legendSumm>	Line Type:</TD><TD nowrap></TD><TD nowrap class=legendSumm>	Reserve Number:</TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR><TR><TD nowrap><INPUT type=text class=inputView id=txtLineType_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap><INPUT type=text class=inputView id=txtReserve_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR>";
            //15Feb2012 - TVD - Elephant Changes
            if (giInsuranceCompanyID == 374) {
				var txtPassThruDataVeh;
				txtPassThruDataVeh = loDcIFrameDoc.getElementById("txtSourceApplicationPassthruDataVeh_" + liVehNum ).value;

                if (txtPassThruDataVeh == "E") {
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<br/>"; 
                    sHtml += "<input type=radio id=rbTypeAuthorized_" + liVehNum + "  enabled=false>Authorized to Proceed with Repairs";
                    sHtml += "</td></tr>";
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<input type=radio  id=rbTypeEstimate_" + liVehNum + "  enabled=false checked>Secure Estimate Only";
                    sHtml += "<br/></td></tr>";
                }
                else {
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<br/>"; 
                    sHtml += "<input type=radio id=rbTypeAuthorized_" + liVehNum + " enabled=false checked>Authorized to Proceed with Repairs";
                    sHtml += "</td></tr>";
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<input type=radio  id=rbTypeEstimate_" + liVehNum + "  enabled=false>Secure Estimate Only";
                    sHtml += "<br/></td></tr>";
                }
            }
           
           //glsd451 Changes - Adding new controls as per andrew's requirement 

            if((lsCurrAssignment == "Program Shop" || lsCurrAssignment == "Repair Referral") && IsRepairEmailCell == "true")
           {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap class=legendSumm colspan=3>Email: </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap class=legendSumm>Cell phone carrier:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactEmailAddress_"+liVehNum+" readonly=true> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtCellPhoneCarrier_"+liVehNum+" readonly=true> </td></tr><tr><td nowrap class=legendSumm colspan=3>Preferred method of status updates:</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtPrefMethodUpd_"+liVehNum+"  readonly=true></td><td nowrap> </td><td nowrap></td><td nowrap> </td><td nowrap> </td></tr>"           
            }
            else
            {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr>"
            }
            sHtml += "<tr id='trRental1'><td width=152 nowrap class=legendSumm>Apply Rental:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Days:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Rental Daily Maximum:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Rental Maximum:</td></tr>"
            sHtml += "<tr id='trRental2'><td nowrap><input type=text class=inputView id=txtRentalAuthorized_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalDays_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalDayAmount_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalMaxAmount_"+liVehNum+" size=16 readonly=true></td></tr>"
            sHtml += "<tr id='trRental3'><td nowrap class=legendSumm>Rental Days Auth.:</td><td nowrap> </td><td nowrap colspan=3 class=legendSumm>Rental Instructions:</td><td nowrap> </td><td nowrap> </td></tr>"
            sHtml += "<tr id='trRental4'><td nowrap valign=top><input type=text class=inputView id=txtRentalDaysAuthorized_"+liVehNum+" style='text-align:right' size=3 readonly=true></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtRentalInstructions_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr>"
           
   //Additional Rental Info
            sHtml += "<tr><td colspan='7'><table id=tblAdditionalRental_"+liVehNum+" style='border:1px solid #000099; margin-top:10px;'><tr><td colspan='7'><table cellpadding=5 cellspacing=0 style='border-collapse:collapse; width:100%; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>Additional Rental Info : </td></tr></table></td></tr><td style='padding:0px 10px 10px 10px;'><table>"
            sHtml += "<tr id='trRental5'><td width=150 nowrap class=legendSumm>Rental Vendor:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Res/Conf:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Vehicle Class:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rate Type:</td></tr>"
            sHtml += "<tr id='trRental6'><td nowrap><input type=text class=inputView id=hidRentalVendor_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalResConf_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalVehicleClass_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalRateType_"+liVehNum+" size=15 readonly=true></td></tr>"

            sHtml += "<tr id='trRental7'><td width=150 nowrap class=legendSumm>Rate:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm> Auth Pickup: </td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Tax Rate:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Phone:</td></tr>"
            sHtml += "<tr id='trRental8'><td nowrap>$<input type=text class=inputView id=txtRentalRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalAuthPickup_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalTaxRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalPhoneAC_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneEN_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneUN_"+liVehNum+" size=7 readonly=true></td></tr>"

            sHtml += "<tr id='trRental9'><td width=150 nowrap class=legendSumm>Rental Address: </td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Rental City:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental State</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Zip:</td></tr>"
            sHtml += "<tr id='trRental10'><td nowrap><input type=text class=inputView id=txtRentalAddress_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalCity_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalState_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalZip_"+liVehNum+" size=15 readonly=true></td></tr>"
            sHtml +="</table></td></tr></table></td></tr>"
//END


            sHtml += "<tr><td colspan=2 nowrap class=legendSumm>Vehicle VIN:</td><td colspan=4 nowrap class=legendSumm>Vehicle YMM:</td><td> </td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtVIN_"+liVehNum+" size=24 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtVehicleYear_"+liVehNum+" size=5 readonly=true> <input type=text class=inputView id=txtMake_"+liVehNum+" size=15 readonly=true> <input type=text class=inputView id=txtModel_"+liVehNum+" size=15 readonly=true></td><td nowrap></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle License Plate:</td><td colspan=1 nowrap class=legendSumm>Vehicle Drivable:</td><td colspan=3 nowrap class=legendSumm>Odometer:</td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtLicensePlateNumber_"+liVehNum+" size=24 readonly=true></td><td nowrap><input type=text class=inputView id=txtDrivable_"+liVehNum+" size=5 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtMileage_"+liVehNum+" size=8 readonly=true></td></tr><tr><td nowrap class=legendSumm>Primary Damage:</td><td nowrap> </td><td colspan=5 nowrap class=legendSumm>Secondary Damages:</td></tr><tr><td nowrap valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtPrimaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtSecondaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr><tr><td nowrap class=legendSumm>Shop Selected:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td colspan=5 valign=top style='color:#FF0000;#font-size:10pt;font-weight:bold;'><img border=0 src=images/warn_img.gif WIDTH=16 HEIGHT=16 hspace=6 align=bottom>No shop selected. Please select a shop for this vehicle.</td><td nowrap> </td><td nowrap><a href='javascript:selectShop(" + liVehNum.toString() + "," + liDispOrder.toString() + ",\"" + lsCurrAssignment.toString() + "\")' title='Select a shop' style='cursor:pointer;cursor:hand;'><img border=0 src=images/btn_select_shop.gif WIDTH=104 HEIGHT=31></a></td></tr></table></tr></table>"
          }
          else if (liVehSummDispType == 3)
          {
            sHtml = "<br><br><table width=700 border=10 cellpadding=0 cellspacing=0 style='border:1px solid #000099; border-collapse:collapse'><tr><td><table width=100% border=0 cellpadding=0 cellspacing=0 style='border-collapse:collapse; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>&nbsp;Vehicle "+liDispOrder+" Information</td><td align=right><a href=javascript:deleteVehicle("+liVehNum+") title='Delete this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_delete_blue.gif WIDTH=83 HEIGHT=31></a><img border=0 src=images/spacer.gif WIDTH=10 HEIGHT=1><a href=javascript:editVehicle("+liVehNum+") title='Edit this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_edit_blue.gif WIDTH=83 HEIGHT=31></a></td></tr></table></td></tr><tr><td valign=top style='padding: 0px 10px 10px 10px;'><table name=VehicleInfoTable id=VehicleInfoTable_"+liVehNum+" border=0 cellpadding=0 cellspacing=0><tr><td width=152 nowrap class=legendSumm>Party:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Coverage:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Deductible:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Limit:</td></tr><tr><td nowrap><input type=text class=inputView id=txtExposureCD_"+liVehNum+" size=9 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtClientCoverageTypeDesc_"+liVehNum+" size=17 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtDeductibleAmt_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtLimitAmt_"+liVehNum+" size=16 readonly=true></td></tr>";

            if (giReserveLineTypeScript == 1)
              sHtml += "<TR><TD nowrap class=legendSumm>	Line Type:</TD><TD nowrap></TD><TD nowrap class=legendSumm>	Reserve Number:</TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR><TR><TD nowrap><INPUT type=text class=inputView id=txtLineType_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap><INPUT type=text class=inputView id=txtReserve_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR>";
            //15Feb2012 - TVD - Elephant Changes
            if (giInsuranceCompanyID == 374) {
				//alert(liVehNum);			
				//alert(loDcIFrameDoc.getElementById("txtSourceApplicationPassthruDataVeh_" + liVehNum ).value);
				var txtPassThruDataVeh;
				txtPassThruDataVeh = loDcIFrameDoc.getElementById("txtSourceApplicationPassthruDataVeh_" + liVehNum ).value;

                //sHtml += "</table><table>";
                //alert(txtPassThruDataVeh);
                if (txtPassThruDataVeh == "E") {
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<br/>"; 
                    sHtml += "<input type=radio id=rbTypeAuthorized_" + liVehNum + "  enabled=false>Authorized to Proceed with Repairs";
                    sHtml += "</td></tr>";
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<input type=radio  id=rbTypeEstimate_" + liVehNum + "  enabled=false checked>Secure Estimate Only";
                    sHtml += "<br/></td></tr>";
                }
                else {
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<br/>"; 
                    sHtml += "<input type=radio id=rbTypeAuthorized_" + liVehNum + " enabled=false checked>Authorized to Proceed with Repairs";
                    sHtml += "</td></tr>";
                    sHtml += "<tr><td class=legendnopad>";
                    sHtml += "<input type=radio  id=rbTypeEstimate_" + liVehNum + "  enabled=false>Secure Estimate Only";
                    sHtml += "<br/></td></tr>";
                }
               //sHtml += "</table><table name=VehicleInfoTable id=VehicleInfoTable_"+liVehNum+" border=0 cellpadding=0 cellspacing=0>";
            }
          
          //glsd451 Changes - Adding new controls as per andrew's requirement 

             if((lsCurrAssignment == "Program Shop" || lsCurrAssignment == "Repair Referral") && IsRepairEmailCell == "true")
           {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap class=legendSumm colspan=3>Email: </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap class=legendSumm>Cell phone carrier:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactEmailAddress_"+liVehNum+" readonly=true> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtCellPhoneCarrier_"+liVehNum+" readonly=true> </td></tr><tr><td nowrap class=legendSumm colspan=3>Preferred method of status updates:</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtPrefMethodUpd_"+liVehNum+"  readonly=true></td><td nowrap> </td><td nowrap></td><td nowrap> </td><td nowrap> </td></tr>"           
            }
            else
            {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr>"
            }
            sHtml += "<tr id='trRental1'><td width=152 nowrap class=legendSumm>Apply Rental:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Days:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Rental Daily Maximum:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Rental Maximum:</td></tr>"
            sHtml += "<tr id='trRental2'><td nowrap><input type=text class=inputView id=txtRentalAuthorized_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalDays_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalDayAmount_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalMaxAmount_"+liVehNum+" size=16 readonly=true></td></tr>"
            sHtml += "<tr id='trRental3'><td nowrap class=legendSumm>Rental Days Auth.:</td><td nowrap> </td><td nowrap colspan=3 class=legendSumm>Rental Instructions:</td><td nowrap> </td><td nowrap> </td></tr>"
            sHtml += "<tr id='trRental4'><td nowrap valign=top><input type=text class=inputView id=txtRentalDaysAuthorized_"+liVehNum+" style='text-align:right' size=3 readonly=true></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtRentalInstructions_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr>"
           
   //Additional Rental Info
            sHtml += "<tr><td colspan='7'><table id=tblAdditionalRental_"+liVehNum+" style='border:1px solid #000099; margin-top:10px;'><tr><td colspan='7'><table cellpadding=5 cellspacing=0 style='border-collapse:collapse; width:100%; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>Additional Rental Info : </td></tr></table></td></tr><td style='padding:0px 10px 10px 10px;'><table>"
            sHtml += "<tr id='trRental5'><td width=150 nowrap class=legendSumm>Rental Vendor:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Res/Conf:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Vehicle Class:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rate Type:</td></tr>"
            sHtml += "<tr id='trRental6'><td nowrap><input type=text class=inputView id=hidRentalVendor_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalResConf_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalVehicleClass_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalRateType_"+liVehNum+" size=15 readonly=true></td></tr>"

            sHtml += "<tr id='trRental7'><td width=150 nowrap class=legendSumm>Rate:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm> Auth Pickup: </td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Tax Rate:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Phone:</td></tr>"
            sHtml += "<tr id='trRental8'><td nowrap>$<input type=text class=inputView id=txtRentalRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalAuthPickup_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalTaxRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalPhoneAC_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneEN_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneUN_"+liVehNum+" size=7 readonly=true></td></tr>"

            sHtml += "<tr id='trRental9'><td width=150 nowrap class=legendSumm>Rental Address: </td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Rental City:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental State</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Zip:</td></tr>"
            sHtml += "<tr id='trRental10'><td nowrap><input type=text class=inputView id=txtRentalAddress_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalCity_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalState_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalZip_"+liVehNum+" size=15 readonly=true></td></tr>"
            sHtml +="</table></td></tr></table></td></tr>"
//END


            sHtml += "<tr><td colspan=2 nowrap class=legendSumm>Vehicle VIN:</td><td colspan=4 nowrap class=legendSumm>Vehicle YMM:</td><td> </td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtVIN_"+liVehNum+" size=24 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtVehicleYear_"+liVehNum+" size=5 readonly=true> <input type=text class=inputView id=txtMake_"+liVehNum+" size=15 readonly=true> <input type=text class=inputView id=txtModel_"+liVehNum+" size=15 readonly=true></td><td nowrap></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle License Plate:</td><td colspan=1 nowrap class=legendSumm>Vehicle Drivable:</td><td colspan=3 nowrap class=legendSumm>Odometer:</td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtLicensePlateNumber_"+liVehNum+" size=24 readonly=true></td><td nowrap><input type=text class=inputView id=txtDrivable_"+liVehNum+" size=5 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtMileage_"+liVehNum+" size=8 readonly=true></td></tr><tr><td nowrap class=legendSumm>Primary Damage:</td><td nowrap> </td><td colspan=5 nowrap class=legendSumm>Secondary Damages:</td></tr><tr><td nowrap valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtPrimaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtSecondaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr><tr><td nowrap class=legendSumm>Shop Selected:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td colspan=5 valign=top><input type=text class=inputView id=txtShopName_"+liVehNum+" size=45 style='width:380px' readonly=true></td><td nowrap> </td><td nowrap valign=top rowspan=2><a href='javascript:selectShop(" + liVehNum.toString() + "," + liDispOrder.toString() + ",\"" + lsCurrAssignment.toString() + "\")' title='Select a shop' style='cursor:pointer;cursor:hand;'><img border=0 src=images/btn_select_shop.gif WIDTH=104 HEIGHT=31></a></td></tr><tr><td colspan=3 class=legendSumm nowrap>Shop Address:</td><td nowrap> </td><td nowrap class=legendSumm>Shop Phone:</td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtShopAddress1_"+liVehNum+" size=40 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtShopPhone_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td colspan=3 nowrap><input type=text class=inputView id=txtShopAddress2_"+liVehNum+" size=40 readonly=true></td><td nowrap> </td><td nowrap class=legendSumm>Shop Fax:</td><td nowrap> </td><td nowrap> </td></tr><tr><td colspan=3 nowrap><input type=text class=inputView id=txtShopCity_"+liVehNum+" size=25 readonly=true> <input type=text class=inputView id=txtShopState_"+liVehNum+" size=2 readonly=true> <input type=text class=inputView id=txtShopZip_"+liVehNum+" size=5 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtShopFax_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=7 class=legendSumm>Shop Directions:</td></tr><tr><td colspan=7 valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtDrivingDirections_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td></tr><tr><td nowrap colspan=7 class=legendSumm>Comments to Shop:</td></tr><tr><td colspan=5 valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtShopRemarks_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td><td nowrap> </td><td nowrap valign=top><a href=javascript:editShopComments("+liVehNum+") title='Edit comments to shop' style='cursor:pointer;cursor:hand;'><img border=0 src=images/btn_edit_comments.gif WIDTH=120 HEIGHT=31></a></td></tr></table></td></tr></table>"
          }
          else if (liVehSummDispType == 0)
            sHtml = "";

          loNewDiv.innerHTML = sHtml;
        }
      }
    }
  }
  
  
  function addNewVehicle()
  {
  var liLastvehNumber = parent.getLastVehNum();
  var SelectedShop = document.getElementById("txtShopName_" + liLastvehNumber).value;

  if(SelectedShop == "" || SelectedShop == undefined || SelectedShop == null)
  { 
     alert("Please select the shop for vehicle "+liLastvehNumber +", before add the new vehicle");
     document.getElementById("txtShopName_" + liLastvehNumber).focus();
     return;
  }


    if(parent)
    {
        var liVehNum = parent.getLastVehNum() + 1;
        parent.document.getElementById("uiFrame").src = "frmWaVehicleInfo.asp?fromPage=ClaimSummary_addNew&vehNum=" + liVehNum;
    }
    else
      return false;
  }
  
  
  function submitClaim()
  {

  var liLastvehNumber = parent.getLastVehNum();
  var SelectedShop = document.getElementById("txtShopName_" + liLastvehNumber).value;

  if(SelectedShop == "" || SelectedShop == undefined || SelectedShop == null)
  { 
     alert("Please select the shop for vehicle "+liLastvehNumber +", before submit the claim");
     document.getElementById("txtShopName_" + liLastvehNumber).focus();
     return;
  }

    gsClaimNumber = loDcIFrameDoc.getElementById("txtCoverageClaimNumber").value;
    if(parent)
    {
      var lsWasSubmitted = parent.chkClaimSubmitted();
      if (lsWasSubmitted == "false")
      {
        var lsShopReq = parent.chkReqSP();
        if (lsShopReq == "")
        {
          parent.document.getElementById("uiFrame").src = "frmWaSubmitAssignment.asp?fromPage=ClaimSummary&ClaimNumber=" + gsClaimNumber;
        }
        else
        {
          alert(lsShopReq);
          return;
        }
      }
      else
      {
        alert("Unable to submit claim. This claim has already been submitted successfully.");
        return;
      }
    }
    else
      return false;
  }
  
  
  function editClaim()
  {
    if(parent)
    {
      dspMsg();
      parent.document.getElementById("uiFrame").src = "frmWaClaimInfo.asp?fromPage=ClaimSummary";
    }
    else
      return false;
  }
  
  
  function editVehicle(vehNum)
  {
    var liVehNum = vehNum;
    if(parent)
    {
      dspMsg();
      parent.document.getElementById("uiFrame").src = "frmWaVehicleInfo.asp?fromPage=ClaimSummary&vehNum=" + liVehNum;
    }
    else
      return false;
  }
  
  
  function dspMsg()
  {
    var objIFrame = parent.document.getElementById('uiFrame');
    objIFrame.style.height = "0";
    parent.showMsg();
  }

  
  function deleteVehicle(vehNum)
  {
    if(parent)
    {
      var liVehNum = vehNum;
      var lbDelVehQ = window.confirm("Vehicle " + liVehNum + " will be deleted. Are you sure you want to delete this vehicle?");
      if (lbDelVehQ == true)
        parent.delVehicle(liVehNum);
      else return;
    }
  }
  
  
  function selectShop(vehNum, DispOrder,AssignmentType)
  {
    var liVehNum = vehNum;
    var liDispOrder = DispOrder;
    var liAssignmentType = AssignmentType;

    
    var CurrentAssignmentType ="<%= lsShopAssignmentType %>";
    
    if(CurrentAssignmentType != "" && CurrentAssignmentType != undefined && CurrentAssignmentType != null)
      if(CurrentAssignmentType == "ProgramShop")
         liAssignmentType = "Choice Shop";

    if(parent)
      parent.getShopSelectionData(liVehNum, liDispOrder,liAssignmentType);
    else
      return false;
  }
  
  
  function editShopComments(vehNum)
  {
    var liVehNum = vehNum;
    if(parent)
      parent.document.getElementById("uiFrame").src = "frmWaShopComments.asp?fromPage=ClaimSummary&vehNum=" + liVehNum;
    else
      return false;
  }
  
  
  function goCancel()
  {
    if (parent)
    {
      var lbConfirmCancel = window.confirm("Stop entering this assignment and return?");
      if (lbConfirmCancel)
      {
        top.location = "frmMyClaimsDesktop.asp";
      }
    }
  }


</script>

</head>
<body onload="resizeWaIframe(gsVehCount); pageInit(); parent.hideMsg(); setRental();">
<div id="divContainer">
  <form name="frmSummaryInfo">

    <table id="tblClaimInfoBlock" width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse;">
      <tr>
        <td>

          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-bottom: 1px solid #000099;">
            <tr bgcolor="#E5E5F5"">
              <td style="color: #000099; font-size: 10pt; font-weight: bold;">
                &nbsp;Claim Information
              </td>
              <td align="right">
                <a href="javascript:editClaim()"
                   title="Edit claim information"
                   onMouseOver="window.status='Edit claim information'; return true"
                   onMouseOut="window.status=''; return true">
                <img border="0" src="images/btn_edit_blue.gif" WIDTH="83" HEIGHT="31">
                </a>
              </td>
            </tr>
          </table>

        </td>
      </tr>
      <tr>
        <td valign="top" style="padding: 0px 10px 10px 10px;">

          <table name="ClaimInfoTable" id="ClaimInfoTable" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
            <colgroup>
              <col width="170px"/>
              <col width="150px"/>
              <col width="250px"/>
            </colgroup>
            <tr>
              <td class="legendSumm">Claim Number:</td>
              <td class="legendSumm">Loss Date:</td>
              <td class="legendSumm">Loss State:</td>
            </tr>
            <tr>
              <td>
                <input name="CoverageClaimNumber" id="txtCoverageClaimNumber" type="text" class="inputView" value="" size="20" readonly="true">
              </td>
              <td>
                <input name="LossDate" id="txtLossDate" type="text" class="inputView" value="" size="12" readonly="true">
              </td>
              <td>
                <input name="LossAddressState" id="txtLossAddressState" type="text" class="inputView" value="" size="4" readonly="true">
              </td>
            </tr>
            <tr>
              <td nowrap colspan="3" class="legendSumm">Description of Loss:</td>
            </tr>
            <tr>
              <td nowrap colspan="3">
                <textarea name="LossDescription" id="txtLossDescription" wrap="physical" class="inputView" readonly="true" style="width:100%; height:50px;"></textarea>
              </td>
            </tr>
            <tr>
              <td class="legendSumm">Claim Assigned by:</td>
              <td>&nbsp;</td>
              <td class="legendSumm">Relation to Insured:</td>
            </tr>
            <tr>
              <td colspan="2">
                <input name="CallerNameFirst" id="txtCallerNameFirst" type="text" class="inputView" value="" size="16" readonly="true">
                <input name="CallerNameLast" id="txtCallerNameLast" type="text" class="inputView" value="" size="21" readonly="true">
              </td>
              <td>
                <input name="CallerRelationToInsuredIDDescription" id="txtCallerRelationToInsuredIDDescription" type="text" class="inputView" value="" size="22" readonly="true">
              </td>
            </tr>
            <tr>
              <td class="legendSumm">Insured Name:</td>
              <td>&nbsp;</td>
              <td class="legendSumm">Insured Phone:</td>
            </tr>
            <tr>
              <td colspan="2">
                <input name="InsuredNameFirst" id="txtInsuredNameFirst" type="text" class="inputView" value="" size="16" readonly="true">
                <input name="InsuredNameLast" id="txtInsuredNameLast" type="text" class="inputView" value="" size="21" readonly="true">
              </td>
              <td>
                <input name="InsuredPhoneSumm" id="txtInsuredPhoneSumm" type="text" class="inputView" value="" size="15" readonly="true">
              </td>
            </tr>

            <tr>
              <td colspan="3" class="legendSumm">
                Insured Business Name:
              </td>
            </tr>
            <tr>
              <td colspan="3">
                <input name="InsuredBusinessName" id="txtInsuredBusinessName" type="text" class="inputView" value="" size="50" readonly="true">
              </td>
            </tr>

<%
  If liCurrAssignmentEmergencyLiability = 1 Then
%>

            <tr>
              <td colspan="3" class="legendSumm">

                <table width="100%" border="0" cellpadding="2" cellspacing="0">
                  <tr valign="top">
                    <td class="legendnopad">Liability:</td>
                    <td id="LiabilityText"></td>
                    <td nowrap>
                      <input name="LiabilityFlag" id="txtLiabilityFlag" type="text" class="inputView" value="" size="2" readonly="true">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td class="legendnopad">Emergency:</td>
                    <td id="EmergencyText"></td>
                    <td nowrap>
                      <input name="EmergencyFlag" id="txtEmergencyFlag" type="text" class="inputView" value="" size="2" readonly="true">
                    </td>
                  </tr>
                </table>

              </td>
            </tr>
<%
  End If
%>

            <tr>
              <td colspan="3" class="legendSumm">Comments to LYNX Claim Representative:</td>
            </tr>
            <tr>
              <td colspan="3">
                <textarea name="Remarks" id="txtRemarks" cols="110" rows="5" wrap="physical" class="inputView" readonly="true" style="width:100%; height:80px;"></textarea>
              </td>
              
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <DIV id="vehTblHolder"><br><SPAN style="font-size:11px; font-weight:bold">Loading Vehicle Data...</SPAN></DIV>

    <table id="lastTbl" width="600" border="0" cellpadding="0" cellspacing="0" style="align:right;">
      <tr>
        <td colspan="4" nowrap>
          <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="10">
        </td>
      </tr>
      <tr>
      <td width="100%">
      <!-- Add veh button div and table starts here -->
      <div id="AddNewVehDiv">
      <table border="0" cellpadding="0" cellspacing="0" width="150%">
      <tr>
        <td width="100%" valign="middle" class="legendnopad" style="font-size:14px;">
          Add Another Vehicle
        </td>
        <td nowrap>
         <img border="0" src="images/spacer.gif" WIDTH="10" HEIGHT="1">
        </td>
        <td nowrap>
         <img border="0" src="images/spacer.gif" WIDTH="10" HEIGHT="1">
        </td>
        <td align="right" nowrap>
          <a href="javascript:addNewVehicle()"
             title="Add another vehicle"
             onMouseOver="window.status='Add another vehicle'; return true"
             onMouseOut="window.status=''; return true">
            <img border="0" src="images/btn_AddVehicle.gif" WIDTH="104" HEIGHT="28">
          </a>
        </td>
      </tr>
      <tr>
        <td colspan="4" class="legendnopad">
          <span style="color:#000000; font-weight:normal; font-size:10pt">
          If you would like to add another vehicle to this claim, please press the "Add Vehicle" button.
          </span>
        </td>
      </tr>
      
      </table>
      </div> 

       <!-- Add veh button div and table ends here -->

      </td>
       </tr>
      
      
      <tr>
        <td colspan="4" nowrap>
          <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="10">
        </td>
      </tr>
      <tr>
        <td width="100%" valign="middle" class="legendnopad" style="font-size:14px;">
          Submit Claim to LYNX Services
        </td>
        <td align="right" valign="top" nowrap>
          <a href='javascript:goCancel()'
             title='Cancel this assignment and return to My Claims Desktop'
             onMouseOver="window.status='Cancel this assignment and return to My Claims Desktop'; return true"
             onMouseOut="window.status=''; return true">
            <img src='images/btn_cancel.gif' border='0' WIDTH='83' HEIGHT='31'>
          </a>
        </td>
        <td nowrap>
         <img border="0" src="images/spacer.gif" WIDTH="30" HEIGHT="1">
        </td>
        <td align="right" nowrap>
          <a href="javascript:submitClaim()"
             title="Submit Claim to LYNX"
             onMouseOver="window.status='Submit Claim to LYNX'; return true"
             onMouseOut="window.status=''; return true">
            <img border="0" src="images/btn_submit_green.gif" WIDTH="83" HEIGHT="31">
          </a>
        </td>
      </tr>
      <tr>
        <td colspan="5" class="legendnopad">
          <span style="color:#000000; font-weight:normal; font-size:10pt">
          If you have entered all the vehicles that pertain to this claim, please review the information for accuracy
          and make any necessary changes by pressing the "EDIT" button. When you are done, please press the "SUBMIT" button
          to send the Claim information to LYNX Services and receive a LYNX ID number as confirmation of your claim.
          </span>
        </td>
      </tr>
    </table>

  </form>

</div>

</body>
</html>