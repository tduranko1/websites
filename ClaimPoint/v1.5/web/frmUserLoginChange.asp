<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incMail.asp"-->
<!-- v1.4.2.0 -->

<%
  lsPageName = "frmUserLoginChange.asp"

  Dim loAware, liCaResult, liUpResult, lsCurrLoginID, lsNewLoginID
  Dim docXML, lsEmailStrXML, sUserAdminEmail, sAPDSrv, sAPDEnv, lsResult
  
  lsCurrLoginID = Request("CurrLoginID")
  lsNewLoginID = Request("NewLoginID")

  liCaResult = 0

  If Request.Form("realSubmit") = "fromButtonChange" Then

    Set loAware = Server.CreateObject("AWARE.clsAWARE")

    'Change LoginID in AWARE
    liCaResult = loAware.ChangeAlias(lsCurrLoginID, lsNewLoginID)

    If liCaResult = 1 Then
      lsResult = "Login ID changed successfully. User will be notified via e-mail."
      
      'Change e-mail address in AWARE DB. Need for e-mails sent by AWARE
      liUpResult = loAware.SetUserProfile(lsNewLoginID, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", lsNewLoginID)
      
      'Get Admin e-mail from APD
      Set docXML = Server.CreateObject("MSXML2.DOMDocument")
      docXML.Async = False
        
      lsEmailStrXML = "<Config Path='ClaimPoint/NewUserAdd/NotificationEmail'/>"
      docXML.LoadXML GetData(lsEmailStrXML, "", "")
      sUserAdminEmail = docXML.documentElement.text
      
      Set docXML = Nothing

      If Instr(1,strServerName, "DEV") > 0 Then	'Development
      		sAPDSrv = "http://sgofetemdev1/apd"
          sAPDEnv = " (Development environment)"
      
      ElseIf Instr(1,strServerName, "STG") > 0 Then		'Staging
      		sAPDSrv = "https://stgbuyat.ppg.com/apd"
          sAPDEnv = " (Staging environment)"
      
      ElseIf Instr(1,strServerName, "PRD") > 0 Or (uCASE(Left(strServerName,3)) = "BUY") Then	'Production
      		sAPDSrv = "https://apd.lynxservices.com/apd"
          sAPDEnv = ""
      
      Else	'default
      		sAPDSrv = "https://apd.lynxservices.com/apd"
          sAPDEnv = ""
      
      End If

      'Sending an e-mail to the user
      Dim sToEmail, sFromEmail, sSubjectEmail, sBodyEmail
      sToEmail = lsNewLoginID
      sFromEmail = sUserAdminEmail
      sSubjectEmail = "ClaimPoint Login ID Updated"
      sBodyEmail = "<p>Your Login ID for the ClaimPoint from LYNX Services website has been changed.</p>"_
            & "<p><strong>Previous Login ID:</strong> " & lsCurrLoginID & "<br>"_
            & "<strong>New Login ID:</strong> " & lsNewLoginID & "</p>"_
            & "<p>Your password has not changed. Please continue to use your current password.</p>"_
            & "<p>URL: " & sAPDSrv & sAPDEnv & "</p>"_
            & "<p>Thank you. <br>"_
            & "LYNX Services Team </p>"_
            & "<p>Login ID changed by " & lsUserNameFirst & " " & lsUserNameLast & " (" & lsUserEmail & ").</p>"

      SendMail sToEmail, sFromEmail, sSubjectEmail, sBodyEmail, "" , ""

      Set loAPDData = Nothing
    
    ElseIf liCaResult = -1 Then
      lsResult = "Unknown Error (any error handled by the Error Handler in this function)"

    ElseIf liCaResult = -2 Then
      lsResult = "Current Login ID doesn't exist"

    ElseIf liCaResult = -3 Then
      lsResult = "New Login ID is already in use"

    ElseIf liCaResult = -4 Then
      lsResult = "New Login ID is invalid"

    End If

    Set loAware = Nothing

  End If

%>

<html>
  <head>

  <link href="includes/apd_style.css" type="text/css" rel="stylesheet">

<script language="javascript">

  var liUpdResult = <%=liCaResult%>;
  var lsNewLoginID = "<%=lsNewLoginID%>";
  if (liUpdResult == 1)
  {
    parent.document.getElementById("txtEmailAddress").value = lsNewLoginID;
    parent.document.getElementById("UserEmailAddress").value = lsNewLoginID;
    parent.updateDetails();
  }
  
  function chkEmail()
  {
    var lsLoginID = document.getElementById("NewLoginID").value;
    if (isEmail(lsLoginID) == false)
    {
      alert("Invalid e-mail address specified for Login ID. Please enter it in the following format: name@domain.nn \n" +
            "The following characters are not valid for the name part of the e-mail address. \n" +
            "     > greater than \n" +
            "     < less than \n" +
            "     ( open parentheses \n" +
            "     ) close parentheses \n" +
            "     [ open brackets \n" +
            "     ] close brackets \n" +
            "     \\ back slash \n" +
            "     % percent \n" +
            "     & ampersand \n" +
            "     + plus \n" +
            "     . period (see note below)\n" +
            "     , comma \n" +
            "     ; semicolon \n" +
            "     : colon \n" +
            "       whitespace character (such as a space or tab) \n" +
            "     @ at sign (as part of the name) \n" +
            "     \" double quote \n" +
            "Note: If there are any periods before the @ sign they must be followed by one or more valid characters. \n" +
            "A period cannot be the first character and there cannot be consecutive periods before the @ sign.");
            
      document.getElementById("NewLoginID").focus();
    }
    else
      document.frmChangeLoginID.submit();
  }

  function isEmail(strEmail)
  {
    var re = /^(([^<>()[\]\\%&+.,;:\s@\"]+(\.[^<>()[\]\\%&+.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(strEmail);
  }

</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<div style="border:1px solid #808080; padding:5px; background-color:#F0F0F0;">
  <a href="#" onClick="javascript:parent.close()"><img src="images/x.gif" alt="" width="16" height="14" border="0" align="right" title="Close"></a>
  <strong>Change User Login ID:</strong>
  <br><br>

<%
  If liCaResult = 0 Then
%>

  	<table border="0" cellpadding="0" cellspacing="0">
      <form action="frmUserLoginChange.asp" method="post" name="frmChangeLoginID" id="frmChangeLoginID">

      	<input type="hidden" name="realSubmit" id="realSubmit" value="fromButtonChange">
        <input type="hidden" name="CurrLoginID" id="CurrLoginID" value="<%= lsCurrLoginID %>">

      <tr>
        <td class="bodyBlue">New Login ID: (User email address) *</td>
      </tr>
      <tr>
        <td><input type="text" name="NewLoginID" id="NewLoginID" value="<%= lsNewLoginID %>" size="40" AUTOCOMPLETE="OFF"></td>
      </tr>
      <tr>
        <td><input type="button" name="changeIDbtn" id="changeIDbtn" value="Change Login ID" onClick="chkEmail()"></td>
      </tr>
      </form>
    </table>

<%
  ElseIf liCaResult < 0 Then
%>

    <table border="0">
      <tr>
        <td valign="top">
          <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom"/>
        </td>
        <td class="errorMessage">
          <%=lsResult%>
        </td>
      </tr>
      <tr>
        <td></td>
        <td nowrap class="bodyleaded">
          <a href="javascript:void(0)"
             title="Retry"
             onMouseOver="window.status='Retry'; return true"
             onMouseOut="window.status=''; return true"
  		  	   onclick="javascript:parent.changeLoginID(); return false;">
          Click here to retry</a>.

        </td>
      </tr>
    </table>

<%
  ElseIf liCaResult > 0 Then
%>

    <table border="0">
      <tr>
        <td valign="top">
          <img border="0" src="images/small_check.gif" WIDTH="11" HEIGHT="11" hspace="6" align="bottom"/>
        </td>
        <td class="bodyblue">
          <%=lsResult%>
        </td>
      </tr>
    </table>

<%
  End If
%>

</div>

<script language="JavaScript">

  if(document.getElementById("NewLoginID"))
    document.getElementById("NewLoginID").focus();

</script>

</body>
</html>