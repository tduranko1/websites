<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->

<%
  Dim liLynxID, liVehNumber, lsChannel
  
  liLynxID = Request("LynxID")
  liVehNumber = Request("VehNumber")
  lsChannel = Request("ch")
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetData.asp"-->

<HTML>
<HEAD>
<TITLE>Document Upload</TITLE>
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incBrowserSniffer.js" type="text/javascript"></script>

<script language="JavaScript">

var sReturnVal = "";

function closeMe2()
{
  if (is_ie5_5up == true)
    window.returnValue = sReturnVal;
  else
		opener.getReturnVal();

  window.close();
}

function closeMe()
{
  if (sReturnVal != "")
  {
    if (is_ie5_5up == true)
      window.returnValue = sReturnVal;
    else
  		opener.getReturnVal();
  }
  window.close();
}
</script>

</HEAD>
<BODY>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
  <TR>
  	<TD width="10%"><IMG SRC="images/modal_ML.gif" ALT="" width="70" height="65"></TD>
  	<TD width="100%" background="images/modal_MC.gif" valign="top">
      <br>
      <div align="center"><p class="bodyBlue">Add Document to LYNX ID <%=liLynxID%>-<%=liVehNumber%></p></div>
    </TD>
  	<TD width="10%"><IMG SRC="images/modal_MR.gif" ALT="" width="70" height="65"></TD>
  </TR>
</TABLE>

<div align="center">
    <IFRAME SRC="frmWaFileUploadPrompt.asp?currentFileUploadPage=frmDocumentUpload&LynxID=<%=liLynxID%>&ch=<%=lsChannel%>&VehNumber=<%=liVehNumber%>" NAME="uploadFrame" ID="uploadFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="width:640px; height:185px;"></IFRAME>
</div>

</body>
</html>

