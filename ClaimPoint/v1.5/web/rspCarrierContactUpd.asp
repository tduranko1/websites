<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incMail.asp"-->
<!-- v1.5.0.2 -->

<%
  lsPageName = "rspCarrierContactUpd.asp"

  Dim lsStrXML, loAPDData, lsRetVal, docXML, oNode, oNodes, sAlertMsg, lsAssignmentType, lsVehiclesList
  Dim lsClaimDetailCall, lsGUID, lsGetNewClaimXML, liLynxID, lsClaimNumber, liAspectID, liVehNumber
  Dim liCurrCarrierUserID, lsCurrCarrierUserEmail, lsCurrCarrierUserName, liNewCarrierUserID, lsNewCarrierUserEmail, lsNewCarrierUserName, lsSysLastUpdatedDate

  liLynxID = Request("LynxID")
  lsAssignmentType = Request("AssignmentType")
  lsVehiclesList = Request("VehiclesList")
  lsClaimNumber = Request("ClaimNumber")
  liCurrCarrierUserID = Request("CurrCarrierUserID")
  lsCurrCarrierUserEmail = Request("CurrCarrierUserEmail")
  lsCurrCarrierUserName = Request("CurrCarrierUserName")
  liNewCarrierUserID = Request("NewCarrierUserID")
  lsNewCarrierUserEmail = Request("NewCarrierUserEmail")
  lsNewCarrierUserName = Request("NewCarrierUserName")
  lsSysLastUpdatedDate = Request("SysLastUpdatedDate")
  liAspectID = Request("AspectID")
  liVehNumber = Request("VehNumber")
  
  lsStrXML =  "<CarrierContactUpdate" + _
                  " LynxID=""" + liLynxID & """" + _
                  " CarrierRepUserID=""" + liNewCarrierUserID & """" + _
                  " UserID=""" + liUserID + """" + _
                  " SysLastUpdatedDate=""" + lsSysLastUpdatedDate + """" + _
                  " />"

  On Error Resume Next
  
  Set loAPDData = Server.CreateObject("PPGNAAPD.clsAPDData")
  
  sAlertMsg = ""
  lsRetVal = GetData(lsStrXML, "", "")
  
  Set docXML = Server.CreateObject("MSXML2.DOMDocument")
  docXML.Async = False
  docXML.LoadXML lsRetVal
  
  If Not docXML.ParseError = 0 Then
    Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
  Else

    If lcase(docXML.documentElement.nodeName) <> "root" Then

      sAlertMsg = "Error Saving"

    Else

      Dim lsEmailStrXML, sUserAdminEmail, sToEmail, sFromEmail, sSubjectEmail, sBodyEmail
      'Get Admin e-mail from APD
      lsEmailStrXML = "<Config Path='ClaimPoint/NewUserAdd/NotificationEmail'/>"
      docXML.LoadXML GetData(lsEmailStrXML, "", "")
      sUserAdminEmail = docXML.documentElement.text
    
      'Sending an e-mail to the newly assigned user
      sToEmail = lsNewCarrierUserEmail
      sFromEmail = sUserAdminEmail
      sSubjectEmail = "Claim #:" & lsClaimNumber & " / LYNX ID:" & liLynxID
      sBodyEmail = "<p>Please note that this claim has been assigned to you by " & lsUserNameFirst & " " & lsUserNameLast & ". "&_
                   "It was previously assigned to " & lsCurrCarrierUserName & ".</p>"&_
                   "<p>The current claim status and information can be found on the ClaimPoint website.</p>"&_
                   "<p>Thank you. <br>"&_
                   "LYNX Services Team </p>"
 
      SendMail sToEmail, sFromEmail, sSubjectEmail, sBodyEmail, "" , ""
      
      If liUserID <> liCurrCarrierUserID Then
      
        'Sending an e-mail to the previously assigned user
        sToEmail = lsCurrCarrierUserEmail
        sFromEmail = sUserAdminEmail
        sSubjectEmail = "Claim #:" & lsClaimNumber & " / LYNX ID:" & liLynxID
        sBodyEmail = "<p>Please note that this claim previously assigned to you has been reassigned to " & lsNewCarrierUserName &_
                     " by " & lsUserNameFirst & " " & lsUserNameLast & "." &_
                     "<p>Thank you. <br>"&_
                     "LYNX Services Team </p>"
   
        SendMail sToEmail, sFromEmail, sSubjectEmail, sBodyEmail, "" , ""

      End If

    End If

  End If

  Set docXML = Nothing
  Set loAPDData = Nothing
%>

<script language="JavaScript" type="text/JavaScript">

  var sApprovedMsg = "<%=sAlertMsg%>";
  var liLynxID = "<%=liLynxID%>";
  var lsAssignmentType = "<%=lsAssignmentType%>";
  var shidVehiclesList = "<%=lsVehiclesList%>";
  var liAspectID = "<%=liAspectID%>";
  var liVehNumber = "<%=liVehNumber%>";

  if (sApprovedMsg != "")
  {
    parent.document.getElementById('SavingAnimateGif').style.display = "none";
    alert("Error saving the new Carrier Contact.\nPlease click the OK to refresh the page and try again.");
  }

  parent.parent.document.location.href = "frmClaimInfo.asp?LynxID=" + liLynxID + "&AspectID=" + liAspectID + "&AssignmentType=" + "<%=lsAssignmentType%>" + "&VehNumber=" + liVehNumber;

</script>

