<%@ Language=VBScript %>
<%
  Option Explicit
  
  Dim lsDateElmId, lbAllowFutureDates
  lbAllowFutureDates = 0
  lsDateElmId = Request("DateElmId")
  lbAllowFutureDates = Request("AllowFutureDates")
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- v1.4.5.0 -->

<html>
<head>
	<title>Pick a Date</title>
  <style>
    .WholeCalendar {
      border-collapse  : collapse;
      background-color : #FFFFFF;
      cursor           : default;
      width            : 100%;
      padding          : 4px;
    }
    
    .CalTable {
      width  : 100%;
    }
    
    .DayTitle {
      color               : black;
      font-family         : Arial, Tahoma, Verdana;
      font-size           : 8pt;
      font-weight         : normal;
      text-align          : center;
    }
    
    .Day {
      color               : #000000;
      background-color    : #FFFFFF;
      font-family         : Arial, Tahoma, Verdana;
      font-size           : 8pt;
      font-weight         : bold;
      text-align          : center;
      vertical-align      : middle;
      cursor              : hand;
    }
    
    .Button {
    	font: 9px Tahoma, Arial, Helvetica, sans-serif;
    	color: #000000;
    	cursor: hand;
    }
    
    .calendarMonth {
    	font: bold 10px Verdana, Arial, Helvetica, sans-serif;
    	color: #003399;
      text-align          : center;
    }
  </style>
  
  <script language="JavaScript" type="text/JavaScript">
    <!-- 
    
    var gsDateElmId = "<%= lsDateElmId %>";
    var lbAllowFutureDates = <%= lbAllowFutureDates %>;

    var gToday = new Date();
    var liTdyYear = gToday.getUTCFullYear();
    var liTdyMonth = gToday.getUTCMonth()+1;
    var liTdyDate = gToday.getUTCDate();
    var iDate = null;
    var iMonth = null;
    var iYear = null;
    var oCurCell = null;
    var bDteCtrlBlank = false;
    var gDteCtrlSelMonth = 0;
    var gDteCtrlSelYear = 0;
    var months = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
    var gsBgColor;
    var retDate = "";

    function initCalendar()
    {
      var dteCtrlDate = "";
      try {
         if (gsDateElmId.indexOf("/") != -1 )
            dteCtrlDate = gsDateElmId;
         else {
            if (gsDateElmId != "")
               dteCtrlDate = parent.document.getElementById(gsDateElmId).value;
         }

         if (typeof(dteCtrlDate) == "object"){
         } else {
            if (gsDateElmId.indexOf("/") != -1 )
               dteCtrlDate = gsDateElmId;
         }
      } catch (e) {}
      
      var dteNow = new Date();
      var oSelMonth, oSelYear;
      var iCurYear = dteNow.getUTCFullYear();
      
      if (dteCtrlDate == undefined || dteCtrlDate == "")
      {
        dteCtrlDate = new Date();
        bDteCtrlBlank = true;
      }
      else
      {
        dteCtrlDate = new Date(dteCtrlDate);
        gDteCtrlSelMonth = dteCtrlDate.getUTCMonth()+1;
        gDteCtrlSelYear = dteCtrlDate.getUTCFullYear();
      }
      
      iDate = dteCtrlDate.getUTCDate();
      iMonth = dteCtrlDate.getUTCMonth() + 1;
      iYear = dteCtrlDate.getUTCFullYear();

      oSelYear = document.getElementById("txtYear");
      if (oSelYear)
        oSelYear.value = iYear;

      oSelMonth = document.getElementById("txtMonth");
      if (oSelMonth)
        oSelMonth.value = iMonth;

      refreshCal();
    }

    
    function incMonth()
    {
      oSelMonth = document.getElementById("txtMonth");
      oSelYear = document.getElementById("txtYear");
      iMonth = oSelMonth.value;
      iYear = oSelYear.value;
    	iMonth++;
    	if (iMonth > 12)
      {
        iMonth = 1;
    		iYear++;
    	}
      oSelMonth.value = iMonth;
      oSelYear.value = iYear;
      refreshCal();
    }

    
    function decMonth()
    {
      oSelMonth = document.getElementById("txtMonth");
      oSelYear = document.getElementById("txtYear");
      iMonth = oSelMonth.value;
      iYear = oSelYear.value;
    	iMonth--;
    	if (iMonth < 1)
      {
    		iMonth = 12;
    		iYear--;
    	}
      oSelMonth.value = iMonth;
      oSelYear.value = iYear;
      refreshCal();
    }

    
    function showToday()
    {
      var dteCtrlDate = new Date();
      iMonth = dteCtrlDate.getUTCMonth() + 1;
      iYear = dteCtrlDate.getUTCFullYear();
      
      oSelMonth = document.getElementById("txtMonth");
      oSelYear = document.getElementById("txtYear");
      
      if (oSelYear)
        oSelYear.value = iYear;
      
      if (oSelMonth)
        oSelMonth.value = iMonth;
      
      refreshCal();
    }

    
    function refreshCal()
    {
      var oTblCal = document.getElementById("tblCal");
      if (oTblCal)
      {
        var bCurrMonth = false;
        var bCurrDteCtrlDate = false;

        oSelMonth = document.getElementById("txtMonth");
        if (oSelMonth)
          iMonth = oSelMonth.value;
        
        oSelYear = document.getElementById("txtYear");
        if (oSelYear)
          iYear = oSelYear.value;

        var dt = new Date(iYear, (iMonth - 1), 1);        
        
        //this will check if the date exceeds the current month and properly adjust the date.
        var dt2 = new Date(iYear, (iMonth - 1), iDate);
        
        if ( (dt.getUTCMonth()+1 == liTdyMonth) && (dt.getUTCFullYear() == liTdyYear) )
          bCurrMonth = true;
          
        if (!bDteCtrlBlank && (dt.getUTCMonth()+1 == gDteCtrlSelMonth) && (dt.getUTCFullYear() == gDteCtrlSelYear) )
          bCurrDteCtrlDate = true;

        document.getElementById("calendarMonthTitle").innerHTML = months[iMonth-1] + ', ' + iYear;

        dt.setUTCDate(1);
        var startCell = 7 + dt.getUTCDay();

        //reset the calendar
        for (var i = 7; i <= 48; i++)
        {
          var curCell = document.getElementById("cell" + i);
          curCell.innerHTML = " ";
          curCell.className = "Day";
          curCell.style.border = "1px solid #FFFFFF";
          curCell.style.backgroundColor = "#FFFFFF";
          curCell.style.cursor = "hand";
          curCell.style.color = "";
          curCell.style.fontWeight = "normal";
          curCell.style.color = "";
          curCell.selected = false;
          curCell.enabled = true;
          curCell.onclick = "";
          curCell.unselectable = "on";
        }

        //update with appropriate calendar dates.
        for(var i = startCell; i <= 48; i++)
        {
          var iCurDate = dt.getUTCDate();
          var curCell = document.getElementById("cell" + i);
          curCell.innerHTML = iCurDate;
          
          //if current cell is date from form input
          if (bCurrDteCtrlDate && (iCurDate == iDate))
          {
            curCell.style.backgroundColor = "#DDDDDD";
            curCell.style.fontWeight = "bold";
            oCurCell = curCell;
          }

          if (bCurrMonth && iCurDate == liTdyDate)
          {
            curCell.style.border = "1px solid #404040";
            oCurCell = curCell;
          }
          else
            curCell.className = "Day";

          curCell.onmouseover = new Function ("", "fnCalMouseOver(this)");
          curCell.onmouseout = new Function ("", "fnCalMouseOut(this)");
          curCell.onclick = new Function ("", "fnCalSelectDate(this); closeCal()");

          dt.setUTCDate(dt.getUTCDate() + 1);
          if (dt.getUTCMonth() != (iMonth-1))
            break;
        }
        
        if (iMonth != liTdyMonth || iYear != liTdyYear)
         document.getElementById("tdyButton").style.display = "inline";
       else
         document.getElementById("tdyButton").style.display = "none";
      }
    }

    
    function fnCalMouseOver(obj)
    {
      gsBgColor = obj.style.backgroundColor;
      obj.style.backgroundColor = "#d3d3d3";
    }

    
    function fnCalMouseOut(obj)
    {
      if (!obj)
        obj = this;
      obj.style.backgroundColor = gsBgColor;
    }

    
    function fnCalSelectDate(obj)
    {
      if (obj)
      {
        iDate = parseInt(obj.innerHTML, 10);
        document.getElementById("DateSelected").value = iMonth + "/" + iDate + "/" + iYear;
        if (oCurCell)
        {
          oCurCell.style.backgroundColor = "#FFFFFF";
          oCurCell.style.color = "#000000";
          oCurCell.style.fontWeight = "normal";
        }
        oCurCell = obj;
        oCurCell.style.backgroundColor = "#DDDDDD";
        oCurCell.style.color = "#FF0000";
        oCurCell.style.fontWeight = "bold";
      }
    }

    
    function closeCal()
    {
      var sDateSelected = document.getElementById("DateSelected").value;
      var dt = new Date(sDateSelected);
      iMonth = dt.getUTCMonth() + 1;
      iDate = dt.getUTCDate();
      iYear = dt.getUTCFullYear();
      var dteCtrlDate = null;
      if (parent.document)
         dteCtrlDate = parent.document.getElementById(gsDateElmId);
         //dteCtrlDate = parent.document.getElementById("txtLossDate");

      var sNewDate = (iMonth < 10 ? "0" + iMonth : iMonth) + "/" + (iDate < 10 ? "0" + iDate : iDate) + "/" + iYear;
      
      if (lbAllowFutureDates == 0)
      {
        var lsMyDate = new Date(sNewDate);
        var liDateDiff = Math.round((new Date() - lsMyDate) / 24 / 60 / 60 / 1000);

        if (liDateDiff < 0)
        {
          alert("Date cannot be in the future.\n");
          return;
        }
        else
          if (dteCtrlDate) 
            dteCtrlDate.value = sNewDate;
          else
            retDate = sNewDate;
      }
      else
        if (dteCtrlDate) 
         dteCtrlDate.value = sNewDate;
        else
         retDate = sNewDate;
      closeMe();
    }

    
    function closeMe()
    {
      if (parent.document.getElementById(gsDateElmId)) {
         parent.document.getElementById("CalFrame").src = "blank.asp";
         parent.document.getElementById("CalFrame").style.display = "none";
         parent.document.getElementById(gsDateElmId).focus();
      } else {
         window.returnValue = retDate;
         window.close();
      }
    }

     -->
  </script>
</head>

<body border="0" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="initCalendar()">

  <input type="hidden" name="Month" id="txtMonth">
  <input type="hidden" name="Year" id="txtYear">
  <input type="hidden" name="DateSelected" id="DateSelected">

  <table border="0" cellspacing="0" cellpadding="0" class="WholeCalendar">
    <tr style="height:14px;">
      <td colspan="2" align="center">
        <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
          <tr>
            <td width="32" align="center"><img src="images/leftArrow.gif" id="PrevMonthImg" class="Button" alt="Previous Month" width="17" height="14" border="0" onClick="decMonth();" onMouseDown="this.src='images/leftArrow_pressed.gif'" onMouseUp="this.src='images/leftArrow.gif'" onMouseOut="this.src='images/leftArrow.gif'"></td>
            <td><div class="calendarMonth" id="calendarMonthTitle">November, 2005</DIV></td>
            <td width="32" align="center"><img src="images/rightArrow.gif" id="NextMonthImg" class="Button" alt="Next Month" width="17" height="14" border="0" onClick="incMonth();" onMouseDown="this.src='images/rightArrow_pressed.gif'" onMouseUp="this.src='images/rightArrow.gif'" onMouseOut="this.src='images/rightArrow.gif'"></td>
          </tr>
        </table>
      </td>
    </tr>                                         
    <tr valign="top" style="height:124px;"> 
      <td colspan="2" style="padding-top:6px" unselectable="on">
        <table name="tblCal" id="tblCal" class="CalTable" cellspacing="0" cellpadding="0" border="0" unselectable="on"> 
          <tr>
            <td colspan="7" style="border-top: 1px solid #696969; height: 1px;"><img src="images/spacer.gif" alt="" width="0" height="1" border="0"></td>
          </tr>
          <tr style="height:14px;">
            <td class="DayTitle">S</td>
            <td class="DayTitle">M</td>
            <td class="DayTitle">T</td>
            <td class="DayTitle">W</td>
            <td class="DayTitle">T</td>
            <td class="DayTitle">F</td>
            <td class="DayTitle">S</td>
          </tr>
          <tr>
            <td colspan="7" style="border-top: 1px solid #696969; height: 1px;"><img src="images/spacer.gif" alt="" width="0" height="1" border="0"></td>
          </tr>
          <tr unselectable="on"><td id="cell7"></td><td id="cell8"></td><td id="cell9"></td><td id="cell10"></td><td id="cell11"></td><td id="cell12"></td><td id="cell13"></td></tr>
          <tr unselectable="on"><td id="cell14"></td><td id="cell15"></td><td id="cell16"></td><td id="cell17"></td><td id="cell18"></td><td id="cell19"></td><td id="cell20"></td></tr>
          <tr unselectable="on"><td id="cell21"></td><td id="cell22"></td><td id="cell23"></td><td id="cell24"></td><td id="cell25"></td><td id="cell26"></td><td id="cell27"></td></tr>
          <tr unselectable="on"><td id="cell28"></td><td id="cell29"></td><td id="cell30"></td><td id="cell31"></td><td id="cell32"></td><td id="cell33"></td><td id="cell34"></td></tr>
          <tr unselectable="on"><td id="cell35"></td><td id="cell36"></td><td id="cell37"></td><td id="cell38"></td><td id="cell39"></td><td id="cell40"></td><td id="cell41"></td></tr>
          <tr unselectable="on"><td id="cell42"></td><td id="cell43"></td><td id="cell44"></td><td id="cell45"></td><td id="cell46"></td><td id="cell47"></td><td id="cell48"></td></tr>
        </table> 
      </td>
    </tr>
    <tr> 
      <td colspan="2" unselectable="on">
        <table style="width:100%;" cellspacing="0" cellpadding="0" border="0" unselectable="on"> 
          <tr>
            <td colspan="7" style="border-top: 1px solid #696969; height:1px;"><img src="images/spacer.gif" alt="" width="0" height="1" border="0"></td>
          </tr>
        </table> 
      </td>
    </tr>
    <tr>
      <td align="center" style="width:65%;">
        <input type="button" id="tdyButton" name="tdyButton" value="Today's Date" title="Show today's date" class="Button" onClick="showToday();" style="display:none">
      </td>
      <td align="center" style="width:35%;">
        <input type="button" name="Close" value="Close" title="Close without selecting a date" class="Button" onClick="closeMe();">
      </td>
    </tr>
  </table>

</body>
</html>
