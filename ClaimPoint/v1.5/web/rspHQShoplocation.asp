<%@ Language=VBScript %>

<%

dim lsStrXML

response.ContentType ="text/xml"

lsStrXML = BinaryToString(Request.BinaryRead(Request.TotalBytes))

  Response.Write(CallSoapWS(lsStrXML))

Function BinaryToString(Binary)
Dim I, S
For I = 1 To LenB(Binary)
S = S & Chr(AscB(MidB(Binary, I, 1)))
Next
BinaryToString = S
End Function

function CallSoapWS(claimXML)

'/* Call the web service */
Dim xmlHttp, xmlResponse, postUrl
Dim dataToSend
Dim sXmlCall
Dim docXML
Dim NodeList
Dim strXmlArray
Dim strReturnXML, strResult

sXMLCall = claimXML
Set xmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
postUrl = GetWebServiceURL()

dataToSend = "" & _
                "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" & _   
                   "<soap:Body>" & _
                   "<ChoiceShopProcess xmlns=""http://service.pgwapdfoundation.com/"">" & sXmlCall & "</ChoiceShopProcess>" &_                                 
                   "</soap:Body>" & _
                "</soap:Envelope>"


xmlHttp.setTimeouts 60000,60000,120000,120000
Call xmlHttp.Open("POST", postUrl, False, "", "")
xmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
xmlHttp.setRequestHeader "SOAPAction", "http://service.pgwapdfoundation.com/ChoiceShopProcess"
xmlHttp.setRequestHeader "Content-length", len(dataToSend)
xmlHttp.send dataToSend

Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
docXML.Async = False
docXML.LoadXML HTMLDecode(xmlHttp.responseText)
strReturnXML = docXML.xml


CallSoapWS = strReturnXML

end function


Function HtmlDecode(htmlString)
                Dim returnValue
                
                returnValue = htmlString
                returnValue = Replace(returnValue, "&lt;", "<")
                returnValue = Replace(returnValue, "&gt;", ">")
                returnValue = Replace(returnValue, "&quot;", """")		
		        returnValue = Replace(returnValue, "&amp;", "&")
                
                HtmlDecode = returnValue
End Function


 function GetWebServiceURL()

    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "http://dlynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"       
        case "STG"
        strmexMonikerString = "http://slynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"       
        case "PRD"
        strmexMonikerString = "http://lynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"     
    End Select
    
   GetWebServiceURL = strmexMonikerString


end function



 %>