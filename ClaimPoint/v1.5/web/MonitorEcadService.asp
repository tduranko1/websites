<%@ Language=VBScript %>
<% Option Explicit %>
<%

On Error resume Next

Dim strReturnData

strReturnData = CallMexMoniker()

if strReturnData = "" Then
response.write "Failure"
response.end
End if

response.write strReturnData 
response.end

function CallMexMoniker()

 Dim mexMonikerString, strReturn
	Dim loAPDData

         mexMonikerString = GetWebServiceURL()
 

	Set loAPDData = GetObject(mexMonikerString)

	If loAPDData is Nothing Then
	strReturn = "Failure"
	End If 

	strReturn = loAPDData.CheckService()

	If Err.Number <> 0 Then
	strReturn = "Failure"
	End If
	
	CallMexMoniker = strReturn
End function

function CallSoap()
'/* Call the web service */

Dim xmlHttp, xmlResponse, postUrl
Dim dataToSend
Dim sXmlCall
Dim docXML
Dim NodeList
Dim strXmlArray
Dim strReturnXML, strResult
Dim oXML 

Set xmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
postUrl = GetSoapURL()

dataToSend = "" & _
                "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">" & _   
                   "<soapenv:Body>" & _
                   "<CheckService xmlns=""http://LynxAPDComponentServices""></CheckService>" &_                                 
                   "</soapenv:Body>" & _
                "</soapenv:Envelope>"

xmlHttp.setTimeouts 60000,60000,120000,120000
Call xmlHttp.Open("POST", postUrl, False, "", "")
xmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
xmlHttp.setRequestHeader "SOAPAction", "http://LynxAPDComponentServices/IEcadAccessor/CheckService"
xmlHttp.setRequestHeader "Content-length", len(dataToSend)

If Err.Number <> 0 then
HandleError Err.Description
Error.Clear
End If

xmlHttp.send dataToSend

'Error.Clear

If Err.Number <> 0 then
HandleError Err.Description
Error.Clear
End If

'Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
'docXML.Async = False
'docXML.LoadXML HTMLDecode(xmlHttp.responseText)

'strReturnXML = docXML.xml

'strResult = Split(strReturnXML,"<CheckServiceResult>")(1)
'xmlResponse = Split(strResult, "</CheckServiceResult>")(0)

CallSoap = xmlHttp.responseText


CallSoap = "Failure"
End function

function GetSoapURL()

Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc" '"http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"       
        case "STG"
        strmexMonikerString = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"        
        case "PRD"
        strmexMonikerString = "http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"       
    End Select

   GetSoapURL = strmexMonikerString


end function

Function HtmlDecode(htmlString)
                Dim returnValue
                
                returnValue = htmlString
                returnValue = Replace(returnValue, "&lt;", "<")
                returnValue = Replace(returnValue, "&gt;", ">")
                returnValue = Replace(returnValue, "&quot;", """")		
		returnValue = Replace(returnValue, "&amp;", "&")
                
                HtmlDecode = returnValue
End Function



function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
   Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function

%>