<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.4.4.0 -->

<%
  If Request.Cookies("CPSession")("guidArray0") = "" Then
    Dim loSession, laGUIDs, liGUIDs
    
    Set loSession = Server.CreateObject("APD.clsAppData")
    
    liGUIDs = 6
    
    laGUIDs = loSession.GenerateXGuids(liGUIDs)
    
    liGUIDs = liGUIDs - 1
    
    For liCounter = 0 to liGUIDs
      Response.Cookies("CPSession")("guidArray" & liCounter) = laGUIDs(liCounter)
    Next
    
    Set loSession = Nothing
  End If


Dim liCertifiedFirstID, liAddressZip, lsGUID, lsShopInfoCall, lsFromPage, liUpdateResult, liPreviousVisited

liUpdateResult = Request("updateResult")
liPreviousVisited = Request("PreviousVisited")
liCertifiedFirstID = Request("CertifiedFirstID")
liAddressZip = Request("AddressZip")
lsGUID = Request.Cookies("CPSession")("guidArray0")
%>

<!--#include file="includes/incGetData.asp"-->

<html>
<head>
<title>LYNXSelect Program Registration</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">

<SCRIPT language="JavaScript" src="includes/incCFShopInfo.js"></SCRIPT>

<script language="JavaScript" type="text/JavaScript">

function InitPage()
{
  var PreviousVisitedFlag = "<%=liPreviousVisited%>";
  var lsPVMsg = "Thank you for returning to the LYNXSelect enrollment page.  Since your last visit the enrollment process has been modified for quicker completion.  Any required information you may have saved on previous visits is reflected below.  The LYNXSelect General Requirements & Procedures have been modified slightly to remove duplication.\n";
  lsPVMsg += "You are no longer asked to print, sign or fax documents as part of the enrollment process.\n";
  lsPVMsg += "We look forward to receiving your electronic enrollment in LYNXSelect.\n";

  updTOAShop(document.getElementById("txtName"));
  if (PreviousVisitedFlag == 1)
    alert(lsPVMsg);
}


function GetXmlElements()
{
  var liCertifiedFirstID = document.getElementById("txtCertifiedFirstID").value;
  var lsElms = "<CFShopInfoUpdate CertifiedFirstID='" + liCertifiedFirstID + "'>";
  var loDiv = document.getElementById("printDiv");
  var lcolElms = loDiv.getElementsByTagName("INPUT");
  var liElmsLength = lcolElms.length;
  for(var x=0; x<liElmsLength; x++)
  {
    if (lcolElms[x].type != 'radio')
    {
      if (lcolElms[x].name.indexOf('Operating') != -1 && lcolElms[x].value != "") //look for Operating Hours
      {
        if (lcolElms[x].name.indexOf('StartTime') != -1)
          lsElms += "<" + lcolElms[x].name + ">" + "<![CDATA[" + escape(convMilTime(lcolElms[x].value, false)) + "]]>" + "</"+ lcolElms[x].name + ">";
        else
          lsElms += "<" + lcolElms[x].name + ">" + "<![CDATA[" + escape(convMilTime(lcolElms[x].value, true)) + "]]>" + "</"+ lcolElms[x].name + ">";
      }
      else
        lsElms += "<" + lcolElms[x].name + ">" + "<![CDATA[" + escape(lcolElms[x].value) + "]]>" + "</"+ lcolElms[x].name + ">";
    }
  }

  var lcolElmsSel = loDiv.getElementsByTagName("SELECT");
  var liElmsSelLength = lcolElmsSel.length;
  for(var y=0; y<liElmsSelLength; y++)
  {
    var selValue = lcolElmsSel[y].options[lcolElmsSel[y].selectedIndex].value;
    lsElms += "<" + lcolElmsSel[y].name + ">" + "<![CDATA[" + escape(selValue) + "]]>" + "</"+ lcolElmsSel[y].name + ">";
  }
  
  var loDrivingDirections = document.getElementById("txtDrivingDirections");
    lsElms += "<" + loDrivingDirections.name + ">" + "<![CDATA[" + escape(loDrivingDirections.value) + "]]>" + "</"+ loDrivingDirections.name + ">";

  lsElms += "</CFShopInfoUpdate>";
  return lsElms;
}


function convMilTime(sTime, sPM)
{
  if (sTime == "")
    return;
  
  var aTime = sTime.split(':');
  var sMilTime;

  if (sPM == false)
    sMilTime = aTime[0] + aTime[1];
  else
    sMilTime = (new Number(aTime[0])+12) + aTime[1];
  
  return sMilTime;
}


function submitData()
{
  var lbIsValid = true;
  
  lbIsValid = chkReqFields();

  if (lbIsValid)
  {    
    var lbPrint = window.confirm("Would you like to print a copy of this completed Business and Shop Information registration form for your records before submitting?" + "\n" + "Press OK to print. Otherwise, press Cancel.");
    if (lbPrint == true)
      printFormDiv();

    document.getElementById("txtFinalSubmitFlag").value = "1";
    var lsXML = GetXmlElements();
    if (lsXML != "")
    {
      document.getElementById("txtlstrXML").value = lsXML;
      
      if (document.getElementById("txtSLPEmailAddress").value.length > 0)
        document.getElementById("txtsendShopEmail").value = document.getElementById("txtSLPEmailAddress").value;
      else if (document.getElementById("txtSPEmailAddress").value.length > 0)
        document.getElementById("txtsendShopEmail").value = document.getElementById("txtSPEmailAddress").value;
      else if (document.getElementById("txtEmailAddress").value.length > 0)
        document.getElementById("txtsendShopEmail").value = document.getElementById("txtEmailAddress").value;

      document.getElementById("txtsendShopID").value = document.getElementById("txtCertifiedFirstID").value;
      document.getElementById("txtsendShopName").value = document.getElementById("txtName").value;
      document.getElementById("txtsendShopAddress1").value = document.getElementById("txtAddress1").value;
      document.getElementById("txtsendShopAddress2").value = document.getElementById("txtAddress2").value;
      document.getElementById("txtsendShopCity").value = document.getElementById("txtAddressCity").value;
      document.getElementById("txtsendShopState").value = document.getElementById("selState").value;
      document.getElementById("txtsendShopZip").value = document.getElementById("txtAddressZip").value;

      document.getElementById("txtsendShopContact").value = document.getElementById("txtSLPName").value;
      
      var sShopContPhone, sShopContFax, sShopEstPkg

      sShopContPhone = document.getElementById("txtSLPPhoneAreaCode").value +"-"+ document.getElementById("txtSLPPhoneExchangeNumber").value +"-"+ document.getElementById("txtSLPPhoneUnitNumber").value;
      if (document.getElementById("txtSLPPhoneExtensionNumber").value.length > 0)
        sShopContPhone += "  ext. "+ document.getElementById("txtSLPPhoneExtensionNumber").value;

      document.getElementById("txtsendShopContactPhone").value = sShopContPhone;

      if (document.getElementById("txtSLPFaxAreaCode").value.length > 0 && document.getElementById("txtSLPFaxExchangeNumber").value.length > 0 && document.getElementById("txtSLPFaxUnitNumber").value.length > 0)
        sShopContFax = document.getElementById("txtSLPFaxAreaCode").value +"-"+ document.getElementById("txtSLPFaxExchangeNumber").value +"-"+ document.getElementById("txtSLPFaxUnitNumber").value;
      else
        sShopContFax = document.getElementById("txtFaxAreaCode").value +"-"+ document.getElementById("txtFaxExchangeNumber").value +"-"+ document.getElementById("txtFaxUnitNumber").value;

      document.getElementById("txtsendShopFax").value = sShopContFax;
      
      if (document.getElementById("selPreferredEstimatePackageID").value == "1")
        sShopEstPkg = "ADP Shoplink";
      else if (document.getElementById("selPreferredEstimatePackageID").value == "2")
        sShopEstPkg = "CCC Pathways";
      else if (document.getElementById("selPreferredEstimatePackageID").value == "4")
        sShopEstPkg = "Mitchell Ultramate";
 
      document.getElementById("txtsendShopEstPkg").value = sShopEstPkg;

      if (document.getElementById("txtlstrXML").value != "")
        document.frmCFShopXML.submit();
    }
  }  
}


function saveData()
{
  var lbIsValid = true;
  var lsMsg = "";
  var lsReq = "";
  
  if (document.getElementById("txtName").value.length == 0)
    lsReq += "    " + "Shop Name" + "\n";
  if (document.getElementById("txtAddressZip").value.length == 0)
    lsReq += "    " + "Shop Zip Code" + "\n";

  if (lsReq.length > 0) // display errors if any
  {
    lsMsg += "* The following fields are required:\n";
    lsMsg += lsReq;
    alert(lsMsg);
    lbIsValid = false;
    return;
  }
    
  if (lbIsValid)
  {    
    document.getElementById("txtFinalSubmitFlag").value = "0";
    var lsXML = GetXmlElements();
    if (lsXML != "")
    {
      document.getElementById("txtlstrXML").value = lsXML;
      document.getElementById("saveOnly").value = "1";

      if (document.getElementById("txtlstrXML").value != "")
        document.frmCFShopXML.submit();
    }
  }  
}


function chkReqFields()
{
  var lsReq = "";
  var lsMsg = "";
  var lsMaxLength = "";
  var lsEmailChkt = "";
  var lsMaxValues = "";

  if (document.getElementById("txtFedTaxID").value.length == 0)
    lsReq +=  "    " + "Shop Federal Tax ID" + "\n";
  if (document.getElementById("selBusinessTypeCD").value.length == 0)
    lsReq +=  "    " + "Business Type" + "\n";
  if (document.getElementById("txtName").value.length == 0)
    lsReq +=  "    " + "Shop Name" + "\n";
  if (document.getElementById("txtAddress1").value.length == 0)
    lsReq +=  "    " + "Shop Street Address" + "\n";
  if (document.getElementById("txtAddressCity").value.length == 0)
    lsReq +=  "    " + "Shop City" + "\n";
  if (document.getElementById("selState").value.length == 0)
    lsReq +=  "    " + "Shop State" + "\n";
  if (document.getElementById("txtAddressZip").value.length == 0)
    lsReq +=  "    " + "Shop Zip Code" + "\n";
  if (document.getElementById("txtPhoneAreaCode").value.length != 3 && document.getElementById("txtPhoneExchangeNumber").value.length != 3 && document.getElementById("txtPhoneUnitNumber").value.length != 4)
    lsReq +=  "    " + "Shop Phone Number" + "\n";
  if (document.getElementById("txtFaxAreaCode").value.length != 3 && document.getElementById("txtFaxExchangeNumber").value.length != 3 && document.getElementById("txtFaxUnitNumber").value.length != 4)
    lsReq +=  "    " + "Shop Fax Number" + "\n";
  if (document.getElementById("txtEmailAddress").value.length == 0)
    lsReq +=  "    " + "Shop Email Address" + "\n";
  if (document.getElementById("selRepairFacilityType").value.length == 0)
    lsReq +=  "    " + "Repair Facility Type" + "\n";
  if (document.getElementById("txtWarrantyPeriodWorkmanshipCD").value.length == 0)
    lsReq +=  "    " + "Workmanship Warranty" + "\n";
  if (document.getElementById("txtWarrantyPeriodRefinishCD").value.length == 0)
    lsReq +=  "    " + "Refinish Warranty" + "\n";
  if (document.getElementById("selPreferredEstimatePackageID").value.length == 0)
    lsReq +=  "    " + "Preferred Estimate Package" + "\n";
  if (document.getElementById("txtMondayStartTime").value.length == 0 || document.getElementById("txtMondayEndTime").value.length == 0)
    lsReq +=  "    " + "Monday Hours of Operation" + "\n";
  if (document.getElementById("txtTuesdayStartTime").value.length == 0 || document.getElementById("txtTuesdayEndTime").value.length == 0)
    lsReq +=  "    " + "Tuesday Hours of Operation" + "\n";
  if (document.getElementById("txtWednesdayStartTime").value.length == 0 || document.getElementById("txtWednesdayEndTime").value.length == 0)
    lsReq +=  "    " + "Wednesday Hours of Operation" + "\n";
  if (document.getElementById("txtThursdayStartTime").value.length == 0 || document.getElementById("txtThursdayEndTime").value.length == 0)
    lsReq +=  "    " + "Thursday Hours of Operation" + "\n";
  if (document.getElementById("txtFridayStartTime").value.length == 0 || document.getElementById("txtFridayEndTime").value.length == 0)
    lsReq +=  "    " + "Friday Hours of Operation" + "\n";
  if (document.getElementById("txtDrivingDirections").value.length == 0)
    lsReq +=  "    " + "How to Get There Directions" + "\n";
  if (document.getElementById("txtSPName").value.length == 0)
    lsReq +=  "    " + "Primary Shop Contact Name" + "\n";
  if (document.getElementById("txtSPPersonnelTypeID").value.length == 0)
    lsReq +=  "    " + "Primary Shop Contact Role" + "\n";
  if (document.getElementById("txtSPPhoneAreaCode").value.length != 3 && document.getElementById("txtSPPhoneExchangeNumber").value.length != 3 && document.getElementById("txtSPPhoneUnitNumber").value.length != 4)
    lsReq +=  "    " + "Primary Shop Contact Phone" + "\n";
  if (document.getElementById("txtSLPName").value.length == 0)
    lsReq +=  "    " + "Secondary Shop Contact Name" + "\n";
  if (document.getElementById("txtSLPPersonnelTypeID").value.length == 0)
    lsReq +=  "    " + "Secondary Shop Contact Role" + "\n";
  if (document.getElementById("txtSLPPhoneAreaCode").value.length != 3 && document.getElementById("txtSLPPhoneExchangeNumber").value.length != 3 && document.getElementById("txtSLPPhoneUnitNumber").value.length != 4)
    lsReq +=  "    " + "Secondary Shop Contact Phone" + "\n";
  if (document.getElementById("txtHourlyRateSheetMetal").value < 1)
    lsReq +=  "    " + "Sheet Metal Hourly Labor Rates - greater than 1" + "\n";
  if (document.getElementById("txtHourlyRateRefinishing").value < 1)
    lsReq +=  "    " + "Refinishing Hourly Labor Rates - greater than 1" + "\n";
  if (document.getElementById("txtHourlyRateUnibodyFrame").value < 1)
    lsReq +=  "    " + "Unibody/Frame Hourly Labor Rates - greater than 1" + "\n";
  if (document.getElementById("txtHourlyRateMechanical").value < 1)
    lsReq +=  "    " + "Mechanical Hourly Labor Rates - greater than 1" + "\n";
  if (document.getElementById("txtRefinishSingleStageHourly").value < 1)
    lsReq +=  "    " + "Refinish Single Stage Charge Per Hour - greater than 1" + "\n";
  if (document.getElementById("txtDailyStorage").value < 1)
    lsReq +=  "    " + "Daily Storage Fee - greater than 1" + "\n";

  if (lsReq.length > 0)
  {
    lsMsg += "* The following fields are required:\n";
    lsMsg += lsReq;
  }

  var liDrvDirectionsTxtAreaLength = document.getElementById("txtDrivingDirections").value.length;
  if (liDrvDirectionsTxtAreaLength > 250)
  {
    lsMaxLength += "    Driving Directions - You have typed "+ liDrvDirectionsTxtAreaLength +" characters.";
  }

  if (lsMaxLength.length > 0)
  {
    lsMsg += "\n";
    lsMsg += "* Only 250 characters are allowed for the following:\n";
    lsMsg += lsMaxLength;
  }

  if (isEmail(document.getElementById("txtEmailAddress").value) == false)
  {
    lsEmailChkt += "    Shop e-mail";
  }
  
  if (lsEmailChkt.length > 0)
  {
    lsMsg += "\n";
    lsMsg += "* Verify the following e-mail address:\n";
    lsMsg += lsEmailChkt;
  }
  
  if (lsMsg.length > 0)
  {
    alert(lsMsg); // display errors
    return false;
  }

  if (document.getElementById("txtHourlyRateSheetMetal").value > 99)
    lsMaxValues += "Sheet Metal Hourly Rate = " + document.getElementById("txtHourlyRateSheetMetal").value + "\n";
  if (document.getElementById("txtHourlyRateRefinishing").value > 99)
    lsMaxValues += "Refinishing Hourly Rate = " + document.getElementById("txtHourlyRateRefinishing").value + "\n";
  if (document.getElementById("txtHourlyRateUnibodyFrame").value > 99)
    lsMaxValues += "Unibody/Frame Hourly Rate = " + document.getElementById("txtHourlyRateUnibodyFrame").value + "\n";
  if (document.getElementById("txtHourlyRateMechanical").value > 99)
    lsMaxValues += "Mechanical Hourly Rate = " + document.getElementById("txtHourlyRateMechanical").value + "\n";
  if (document.getElementById("txtRefinishSingleStageHourly").value > 99)
    lsMaxValues += "Refinish Two Stage Charge per Hour = " + document.getElementById("txtRefinishSingleStageHourly").value + "\n";
  if (document.getElementById("txtDailyStorage").value > 99)
    lsMaxValues += "Daily Storage Fee = " + document.getElementById("txtDailyStorage").value + "\n";

  if (lsMaxValues.length > 0) // display Max values
  {
    var lbChkValues = window.confirm(lsMaxValues + "\n" + "Did you mean to enter the values above?");
    if (lbChkValues == false)
      return false;
  }

  if (document.getElementById("txtAgreeTerms").checked == false)
  {
    alert("Please acknowledge the Terms of Agreement by marking the checbox.");
    document.getElementById("txtAgreeTerms").focus();
    return false;
  }

  return true;
}


function printFrame(frm)
{
  var loiFrm = frames[frm];
  if (loiFrm)
  {
    loiFrm.focus(); //IE requirement
    loiFrm.print();
  }
}

function printFormDiv()
{
  var sFormHtml = document.getElementById('printDiv').innerHTML;
  document.getElementById('txtlstrShopData').value = document.getElementById('printDiv').innerHTML;
  document.frmCFShopData.submit();
}


function chkWrnty(obj)
{
  if (obj.value.length > 0 && obj.value != "99")
    alert("LYNXSelect program participants that do not offer lifetime warranty will not be eligible for referrals from the current insurance company opportunities available through the LYNXSelect Collision Shop Program.");
}


function cpyContactData()
{
  document.getElementById('txtSLPName').value = document.getElementById('txtSPName').value;

  if (document.getElementById('txtSPPhoneAreaCode').value.length != 0 && document.getElementById('txtSPPhoneExchangeNumber').value.length != 0 && document.getElementById('txtSPPhoneUnitNumber').value.length != 0)
  {
    document.getElementById('txtSLPPhoneAreaCode').value = document.getElementById('txtSPPhoneAreaCode').value;
    document.getElementById('txtSLPPhoneExchangeNumber').value = document.getElementById('txtSPPhoneExchangeNumber').value;
    document.getElementById('txtSLPPhoneUnitNumber').value = document.getElementById('txtSPPhoneUnitNumber').value;
    document.getElementById('txtSLPPhoneExtensionNumber').value = document.getElementById('txtSPPhoneExtensionNumber').value;
  }
  else
  {
    document.getElementById('txtSLPPhoneAreaCode').value = document.getElementById('txtPhoneAreaCode').value;
    document.getElementById('txtSLPPhoneExchangeNumber').value = document.getElementById('txtPhoneExchangeNumber').value;
    document.getElementById('txtSLPPhoneUnitNumber').value = document.getElementById('txtPhoneUnitNumber').value;
    document.getElementById('txtSLPPhoneExtensionNumber').value = document.getElementById('txtPhoneExtensionNumber').value;
  }

  document.getElementById('txtSLPFaxAreaCode').value = document.getElementById('txtFaxAreaCode').value;
  document.getElementById('txtSLPFaxExchangeNumber').value = document.getElementById('txtFaxExchangeNumber').value;
  document.getElementById('txtSLPFaxUnitNumber').value = document.getElementById('txtFaxUnitNumber').value;

  if (document.getElementById('txtSPEmailAddress').value.length != 0)
    document.getElementById('txtSLPEmailAddress').value = document.getElementById('txtSPEmailAddress').value;
  else
    document.getElementById('txtSLPEmailAddress').value = document.getElementById('txtEmailAddress').value;
}


function poplateHours()
{
  document.getElementById('txtMondayStartTime').value = "7:00";
  document.getElementById('txtMondayEndTime').value = "6:00";
  document.getElementById('txtTuesdayStartTime').value = "7:00";
  document.getElementById('txtTuesdayEndTime').value = "6:00";
  document.getElementById('txtWednesdayStartTime').value = "7:00";
  document.getElementById('txtWednesdayEndTime').value = "6:00";
  document.getElementById('txtThursdayStartTime').value = "7:00";
  document.getElementById('txtThursdayEndTime').value = "6:00";
  document.getElementById('txtFridayStartTime').value = "7:00";
  document.getElementById('txtFridayEndTime').value = "6:00";
}



function updTOAShop(obj)
{
  if (document.getElementById("frmTOA"))
    window.frames['frmTOA'].document.getElementById('txtShopName').innerText = obj.value;
}

function ChkMaxLength(obj, max)
{
  if (obj.tagName == "TEXTAREA" && obj.value.length > max)
  {
    alert("Only "+max+" characters are allowed for this entry. You have typed " +obj.value.length+ " characters.");
    event.returnValue = false;
    obj.focus();
  }
}


function cancelData()
{
  top.location = 'frmCFShopLogin.asp';
}


// PDF Popup
function showPDF(lsUrl)
{
  day = new Date();
  id = day.getTime();
  var lsQuery = "frmCFShowPDF.asp?pdfURL="+lsUrl;
  eval("page" + id + " = window.open(lsQuery, 'pdfWindow', 'toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=800,height=600');");
}


// Popup About page with application details
function callAbout()
{
  if (window.event.ctrlKey)
  {
    var lsDimensions = "dialogHeight:300px; dialogWidth:380px; "
    var lsSettings = "scroll:no; resizable:yes; status:no; help:no; center:yes; unadorned:yes;"
    var lsQuery = "frmAbout.asp";
    window.showModalDialog( lsQuery, window, lsDimensions + lsSettings );
  }
}

</script>

</head>

<BODY LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 background="./images/page_bg.jpg" onLoad="InitPage()" >
  
  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
    <tr>
      <td width="169" align="left" valign="top">
        <IMG SRC="images/logo_corner.jpg" ALT="" width="169" height="202">
      </td>
      <td align="left" valign="top">

        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" background="images/header_bg.jpg" height="132">
          <tr>
            <td height="50">&nbsp;</td>
            <td align="right" valign="top" height="50">&nbsp;</td>
          </tr>
          <tr>
            <td colspan=2 valign="top">&nbsp;</td>
          </tr>
          <tr>
            <td align="left" valign="bottom" nowrap width="50%">
              <p class="header">
              LYNXSelect<sup style="font-size: 9px;">TM</sup> Registration for 
              <span id="spanlsPageTitle" lang="en-us"><em>CertifiedFirst </em><sup style="font-size: 9px;">TM</sup> Network</span>
            </td>
            <td align="right" valign="bottom" width="50%">&nbsp;</td>
          </tr>
        </table>
        <br>
        
        <form id="frmShop" name="frmShop">
  
        <table width="640" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td align="right" nowrap="">
              <p class="bodyBlue"> All fields marked with an asterisk (*) are required.</p>
            </td>
          </tr>
        </table>

<DIV id="printDiv">
        <%
          If len(liCertifiedFirstID) = 0 or len(liAddressZip) = 0 Then
             Response.Redirect "frmCFShopLogin.asp"
          Else
            lsShopInfoCall = "<CFShopInfo CertifiedFirstID=""" & server.urlencode(liCertifiedFirstID) & """ AddressZip=""" & server.urlencode(liAddressZip) & """/>"
            Response.Write GetPersistedData(lsShopInfoCall, lsGUID, "CFShopInfo.xsl", "", false)
          End If
        %>
</DIV>

        <table width="640" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td nowrap="">
            <p class="bodyBlue"> All fields marked with an asterisk (*) are required.</p>
            </td>
          </tr>
        </table>

    <br/><br/>

    <table width="640" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td>

      <p class="login" style="font-size:14px; font-weight:bold;">
        To complete your registration process for the LYNXSelect Program, please complete the following four steps:
      </p>
      <p class="login" style="font-size:13px">
        <strong>Step 1: Review</strong> the <a href="javascript:showPDF('pdf/LYNXSelect General Requirements Procedures.pdf')" title="LYNXSelect General Requirements &amp; Procedures">LYNXSelect General Requirements &amp; Procedures</a>
        <a href="javascript:showPDF('pdf/LYNXSelect General Requirements Procedures.pdf')" title="LYNXSelect General Requirements &amp; Procedures"><img src="images/pdfdoc.gif" width="32" height="32" hspace="6" border="0" align="middle"/></a>
        (<a href="javascript:showPDF('pdf/LYNXSelect General Requirements Procedures.pdf')" title="Print the LYNXSelect General Requirements &amp; Procedures">Print</a> if desired)
      </p>
      
      <p class="login" style="font-size:13px">
       <strong>Step 2: Review</strong> the LYNXSelect Terms of Agreement.
       <a href="#" onClick="printFrame('frmTOA')" title="Print a copy for your records"><strong>Print</strong></a> a copy for your records.
       <a href="#" onClick="printFrame('frmTOA')" title="Print a copy for your records"><img src="images/icon_print.gif" alt="Print a copy for your records" WIDTH="31" HEIGHT="20" BORDER="0" /></a>
      </p>
      <p class="login" style="font-size:13px">
        You will need to agree to the Terms of Agreement by checking the checkbox below in order to submit your registration.
      </p>

      <iframe name="frmTOA" id="frmTOA" src="frmCFTermsAgreement.asp" style="width:100%; height:300px;"></iframe>

      <p class="login" style="font-size:13px">
        &nbsp;&nbsp;<input type="checkbox" name="AgreeTerms" id="txtAgreeTerms">
        By checking this box you agree to the Terms of Agreement above. 
      </p>

      <p class="login" style="font-size:13px">
        <strong>Step 3:</strong> <img src="images/spacer.gif" width="10px"/>
        <a href="javascript:printFormDiv()" title="Print a copy for your records"><img src="images/icon_print.gif" WIDTH="31" HEIGHT="20" BORDER="0" /></a>
        <img src="images/spacer.gif" width="10px"/>
        <a href="javascript:printFormDiv()"><strong>Print</strong></a> a copy of  this completed Business and Shop Information registration form for your records.
      </p>

      <p class="login" style="font-size:13px">
        <strong>Step 4:</strong> Click the <strong>Submit</strong> button below to send your registration information to LYNX Services.
      </p>
            
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="100%"> </td>
          <td align="right" nowrap="">
            <a href="javascript:cancelData()" title="Cancel and Logout" style="cursor:pointer; cursor:hand;"><img border="0" src="images/btn_cancel.gif" WIDTH="83" HEIGHT="31"/></a>
          </td>
          <td nowrap="">
           <img border="0" src="images/spacer.gif" WIDTH="20" HEIGHT="1"/>
          </td>
          <td align="right" nowrap="">
            <a href="javascript:submitData()" title="Submit Shop Information to LYNX" style="cursor:pointer; cursor:hand;"><img border="0" src="images/btn_submit_green.gif" WIDTH="83" HEIGHT="31"/></a>
          </td>
          <td>&nbsp;&nbsp;</td>
        </tr>
      </table>

      <blockquote>
        <li>
          <p class="login" style="font-size:13px">
            <strong>If</strong> you press the <strong>Cancel</strong> button, you will be taken back to the Login screen, and will lose these changes.
          </p>
        </li>
        <li>
          <p class="login" style="font-size:13px">
            <strong>You may also save information you have already entered, and come back at a later time to complete
            the registration process.</strong> To do this, please click on this button:
          </p>
        </li>
        <li>
          <p class="login" style="font-size:13px">
            <a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank"><img src="http://www.adobe.com/images/get_adobe_reader.gif" border="0" alt="Download PDF Viewer" align="right"></a>
            <strong>Note:</strong> You will need a PDF Viewer to view the PDF files. If you don't have a viewer, you can download it from the Adobe website by clicking on the Adobe logo.
            
          </p>
        </li>
        <p>
          <a href="javascript:saveData()" title="Save and Come Back Later" style="cursor:pointer; cursor:hand;"><img border="0" src="images/btn_savecomeback.gif" WIDTH="192" HEIGHT="31"/></a>
        </p>
      </blockquote>

      <p class="login">
        For questions regarding the LYNXSelect Program or this enrollment process, please email us at <a href="mailto:lynxselect@lynxservices.com?subject=Questions regarding the LYNXSelect Program">lynxselect@lynxservices.com</a>.
      </p>
      <p class="login">
        For questions regarding the <em>CertifiedFirst</em> Network Program, please call 866-CERT-1ST.
      </p>

        </td>
      </tr>
    </table>
  
    </form>

    </td>
  </tr>
</table>

<br>

<!--#include file="includes/incFooter.asp"-->

  <form name="frmCFShopXML" method="post" action="rspCFSubmitShopData.asp">
    <textarea cols="" rows="" name="lstrXML" id="txtlstrXML" readonly style="visibility: hidden;"></textarea>
    <input type="hidden" name="saveOnly" id="txtsaveOnly" value="0" style="visibility: hidden;">
    <input type="hidden" name="sendShopEmail" id="txtsendShopEmail" style="visibility: hidden;">
    <input type="hidden" name="sendShopID" id="txtsendShopID" style="visibility: hidden;">
    <input type="hidden" name="sendShopName" id="txtsendShopName" style="visibility: hidden;">
    <input type="hidden" name="sendShopAddress1" id="txtsendShopAddress1" style="visibility: hidden;">
    <input type="hidden" name="sendShopAddress2" id="txtsendShopAddress2" style="visibility: hidden;">
    <input type="hidden" name="sendShopCity" id="txtsendShopCity" style="visibility: hidden;">
    <input type="hidden" name="sendShopState" id="txtsendShopState" style="visibility: hidden;">
    <input type="hidden" name="sendShopZip" id="txtsendShopZip" style="visibility: hidden;">
    <input type="hidden" name="sendShopContact" id="txtsendShopContact" style="visibility: hidden;">
    <input type="hidden" name="sendShopContactPhone" id="txtsendShopContactPhone" style="visibility: hidden;">
    <input type="hidden" name="sendShopFax" id="txtsendShopFax" style="visibility: hidden;">
    <input type="hidden" name="sendShopEstPkg" id="txtsendShopEstPkg" style="visibility: hidden;">
  </form>

  <form name="frmCFShopData" method="post" action="frmCFShopInfoPrint.asp" target="_blank">
    <textarea cols="" rows="" name="lstrShopData" id="txtlstrShopData" readonly style="visibility: hidden;"></textarea>
  </form>

</body>
</html>
