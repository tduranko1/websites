<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!-- v1.4.4.0 -->

<!-- OneTrust Cookies Consent Notice start -->
<script type="text/javascript" src="https://cdn.cookielaw.org/consent/439da07c-6375-4ece-a5e8-38f54267770a-test/OtAutoBlock.js"></script>
<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="439da07c-6375-4ece-a5e8-38f54267770a-test"></script>
<script type="text/javascript">
function OptanonWrapper() { }
</script>
<!-- OneTrust Cookies Consent Notice end -->

<script language="javascript" type="text/javascript">


  /***************************************/
  /* 19Dec2013 - TVD - IE11 Changes      */
  /* Detect what browser is being used   */
  /***************************************/
  navigator.version = (function () {
      var ua = navigator.userAgent, tem,
    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*([\d\.]+)/i) || [];
      if (/trident/i.test(M[1])) {
          tem = /\brv[ :]+(\d+(\.\d+)?)/g.exec(ua) || [];
          return 'IE ' + (tem[1] || '');
      }
      M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
      if ((tem = ua.match(/version\/([\.\d]+)/i)) != null) M[2] = tem[1];
      return M.join(' ');
  })();

      //alert(navigator.version);
      var sBrowser;
      sBrowser = '';

      switch (navigator.version) {
          case 'IE 11.0':
              sBrowser = '11';
              break;
          case 'MSIE 7.0':
              sBrowser = '7';
              break;
          default:
              sBrowser = 'OTHER';
              break;
      }
      document.cookie = 'BrowserVer=' + sBrowser;
</script>

<%
  '********************************
  'from global.asa...didn't work there in staging,
  'so we'll try here.  everybody has to log in, so
  'it's no big deal where the code actually resides.
  
  If Request.Cookies("CPSession")("guidArray0") = "" Then
    Dim loSession, laGUIDs, liGUIDs
    
    Set loSession = CreateObject("APD.clsAppData")
    
    liGUIDs = 6
    
    laGUIDs = loSession.GenerateXGuids(liGUIDs)
    
    liGUIDs = liGUIDs - 1
    
    For liCounter = 0 To liGUIDs
      Response.Cookies("CPSession")("guidArray" & liCounter) = laGUIDs(liCounter)
    Next
    
    Set loSession = Nothing
  End If
  'end segment from global.asa
  '********************************  

  Dim lsGUID, docXML, lsUserSessionCall, NodeUser, NodeClaimPointPermission
  Dim sEmailAddress, sPassword, sRememberUser, sUserAuthenticateResult, lbMaintenanceModeAlertMsg
  Dim liCarrierRep, lsInsCoListCall, NodeInsurance, InsuranceCompanyID, InsuranceCompanyName, sLynxSrv      
  Dim strAwareResult, strAwareMessage


  lsGUID = Request.Cookies("CPSession")("guidArray0")

  '---------------------------------------
  ' 19Dec2013 - TVD - IE11 Changes      
  ' Debug code to test browser cookie 
  '---------------------------------------
  'Response.Write("---> " & Request.Cookies("BrowserVer"))
  'response.end

%>

<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/IncAuthenticationTools.asp"-->
<!--#include file="includes/incCryptTools.asp"-->

<%
  lsPageName = "frmLogin.asp"
  lsPageTitle = "ClaimPoint<SUP><FONT SIZE=-2>TM</FONT></SUP> Login"
  liPageType = lcAuthSection
  
  If Request("CP")= "Success" Then
    lsMessage = "Password reset successful."
  ElseIf Request("fromPage")= "ChangePassword" Then
    lsMessage = Request("ErrorMessage")
  ElseIf Request("NL")= 1 Then
    lsMessage = "You have been logged out due to invalid security settings. Please login again."
  End If

  lbMaintenanceModeAlertMsg = false

  If Request.Form("MaintenanceMode") = "true" Then
    lbMaintenanceModeAlertMsg = true
  End If

  If Not(lbMaintenanceModeAlertMsg) and Request.Form("realSubmit") = "fromButton" Then
    sEmailAddress = Request.Form("txtUserName")
    sPassword  = Request.Form("txtPassword")

    sUserAuthenticateResult = UserAuthenticate(sEmailAddress,sPassword)
    
    if Instr(1, sUserAuthenticateResult, "|") > 0 then
      strAwareResult = mid(sUserAuthenticateResult, 1, Instr(1, sUserAuthenticateResult, "|") - 1)
      strAwareMessage = mid(sUserAuthenticateResult, Instr(1, sUserAuthenticateResult, "|") + 1)
      sUserAuthenticateResult = strAwareResult
    end if
    
    If sUserAuthenticateResult = "Success" Then
      Response.Cookies("CPSession")("authUser") = fCrypt("LYNXAPDKEY", sEmailAddress)
      sRememberUser = Request.Form("txtRemember")

      If sRememberUser = "on" Then
        Response.Cookies("APDLogin")("txtUserName") = fCrypt("LYNXAPDKEY", sEmailAddress)
        Response.Cookies("APDLogin")("txtPassword") = fCrypt("LYNXAPDKEY", sPassword)
        Response.Cookies("APDLogin")("txtRemember") = sRememberUser 
        Response.Cookies("APDLogin").Expires = Now()+60 
      Else
        Response.Cookies("APDLogin")("txtUserName") = ""
        Response.Cookies("APDLogin")("txtPassword") = ""
        Response.Cookies("APDLogin")("txtRemember") = ""
        Response.Cookies("APDLogin").Expires = Now()-1
      End If

      lsUserSessionCall = "<UserSessionDetail Login=""" & server.urlencode(sEmailAddress) & """ ApplicationCD=""" & lsApplicationCD & """ />"

      Set docXML = Server.CreateObject("MSXML2.DOMDocument.6.0")
      docXML.Async = False
      docXML.LoadXML GetData(lsUserSessionCall, "", "")
      
      If Not docXML.ParseError = 0 Then
        'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
      Else

        Set NodeUser = docXML.documentElement.selectSingleNode("User")
        Response.Cookies("CPSession")("UserID") = NodeUser.getAttribute("UserID")
        Response.Cookies("CPSession")("UserEmail") = sEmailAddress
        Response.Cookies("CPSession")("OfficeID") = NodeUser.getAttribute("OfficeID")
        Response.Cookies("CPSession")("OfficeName") = NodeUser.getAttribute("OfficeName")
        Response.Cookies("CPSession")("ClientOfficeId") = NodeUser.getAttribute("ClientOfficeID")
        Response.Cookies("CPSession")("NameFirst") = NodeUser.getAttribute("NameFirst")
        Response.Cookies("CPSession")("NameLast") = NodeUser.getAttribute("NameLast")
        Response.Cookies("CPSession")("PhoneAreaCode") = NodeUser.getAttribute("PhoneAreaCode")
        Response.Cookies("CPSession")("PhoneExchangeNumber") = NodeUser.getAttribute("PhoneExchangeNumber")
        Response.Cookies("CPSession")("PhoneUnitNumber") = NodeUser.getAttribute("PhoneUnitNumber")
        Response.Cookies("CPSession")("PhoneExtensionNumber") = NodeUser.getAttribute("PhoneExtensionNumber")

        InsuranceCompanyID = NodeUser.getAttribute("InsuranceCompanyID")
        InsuranceCompanyName = Server.URLEncode(NodeUser.getAttribute("InsuranceCompanyName"))

        If NodeUser.getAttribute("OfficeID") > 0 Then
          liCarrierRep = 1 'Carrier Rep User
        ElseIf NodeUser.getAttribute("OfficeID") = 0 and NodeUser.getAttribute("InsuranceCompanyID") = 0 Then
          liCarrierRep = 0 'LYNX Employee User
        End If
        Response.Cookies("CPSession")("CarrierRep") = liCarrierRep

        Set NodeClaimPointPermission = docXML.documentElement.selectSingleNode("User/ClaimPointPermission")
        Response.Cookies("CPSession")("ClaimRead") = NodeClaimPointPermission.getAttribute("ClaimRead")
        Response.Cookies("CPSession")("ClaimCreate") = NodeClaimPointPermission.getAttribute("ClaimCreate")
        Response.Cookies("CPSession")("ViewReport") = NodeClaimPointPermission.getAttribute("ViewReport")
        Response.Cookies("CPSession")("ManageUsers") = NodeClaimPointPermission.getAttribute("ManageUsers")
        Response.Cookies("CPSession")("AuthorizePayments") = NodeClaimPointPermission.getAttribute("AuthorizePayments")
        Response.Cookies("CPSession")("PaymentAuthorizationLevel") = NodeClaimPointPermission.getAttribute("PaymentAuthorizationLevel")
        Response.Cookies("CPSession")("ClaimViewLevel") = NodeClaimPointPermission.getAttribute("ClaimViewLevel")
        Response.Cookies("CPSession")("AwareMessage") = strAwareMessage

        Set NodeClaimPointPermission = Nothing
        Set NodeUser = Nothing
          
      End If

      If liCarrierRep = 0 Then
        lsInsCoListCall = "<InsuranceCompanyList/>"
        docXML.LoadXML GetData(lsInsCoListCall, "", "")

        If Not docXML.ParseError = 0 Then
          'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
        Else
          Set NodeInsurance = docXML.documentElement.selectSingleNode("InsuranceCompany[@DemoFlag='1']")
          InsuranceCompanyID = NodeInsurance.getAttribute("InsuranceCompanyID")
          InsuranceCompanyName = Server.URLEncode(NodeInsurance.getAttribute("Name"))
          Set NodeInsurance = Nothing
        End If

      End If

      'Clear the JS cookie before moving on
      Response.Cookies("cookieEnabled") = ""
      Response.Cookies("cookieEnabled").Expires = Now()-1

      Response.Redirect "rspInsuranceCoSelect.asp?hidInsuranceCompanyID=" & InsuranceCompanyID & "&hidInsuranceCompanyName=" & InsuranceCompanyName & "&fromPage=Login"
      
    Elseif Instr(1, sUserAuthenticateResult, "Password has expired") > 0 then %>
    <script language="javascript" type="text/javascript">
       var answer = confirm("Your password has expired. Would you like to reset it now?\n\nA new password will be sent to your email address when you reset it.");
       if (answer) {
          top.location = "frmForgotPassword.asp";
       }
    </script>
  <%Else
      lsMessage = sUserAuthenticateResult
    End If

  ElseIf Not(lbMaintenanceModeAlertMsg) and Request.Form("realSubmit") = "fromLink" Then
    lsMessage = GetPasswordHint(Request.Form("txtUserName"))
    
    sRememberUser = Request.Cookies("APDLogin")("txtRemember")
    If sRememberUser = "on" Then
      sEmailAddress = fDecrypt("LYNXAPDKEY", Request.Cookies("APDLogin")("txtUserName"))
      sPassword = ""
    End If

  Else
    sRememberUser = Request.Cookies("APDLogin")("txtRemember")
    If sRememberUser = "on" Then
      sEmailAddress = fDecrypt("LYNXAPDKEY", Request.Cookies("APDLogin")("txtUserName"))
      sPassword = fDecrypt("LYNXAPDKEY", Request.Cookies("APDLogin")("txtPassword"))
    Else
      sEmailAddress = ""
      sPassword = ""
    End If

  End If
  
  If Instr(1,strServerName, "DEV") > 0 Then  'Development
      sLynxSrv = "http://wwwdevlynx2.web.ppg.com"
  ElseIf Instr(1,strServerName, "STG") > 0 Then    'Staging
      sLynxSrv = "http://wwwtestlynx2.web.ppg.com"
  ElseIf Instr(1,strServerName, "PRD") > 0 Or (uCASE(Left(strServerName,3)) = "BUY") Then  'Production
      sLynxSrv = "https://www.lynxservices.com"
  Else  'default
      sLynxSrv = "https://www.lynxservices.com"
  End if
%>

<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->

<script language="JavaScript" src="includes/incBrowserSniffer.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script LANGUAGE="javascript">
<!--
  var glbMaintenanceMode = "<%= lcase(lbMaintenanceMode) %>";

  if (glbMaintenanceMode == "true")
  {
    document.getElementById("lnkChangePassword").href = "#";
    document.getElementById("lnkForgotPassword").href = "#";
  }

  function login_onclick(sValue) 
  {
    var queryString = window.location.search;
    if (checkvulnerability(queryString)) {
        alert("Login was not successful.\nPlease try again.");
		return;
	}
    if (!is_cookie)
    {
      alert("You will need to enable cookies on your browser in order to login and use this site.");
      return;
    }

  if ((is_ie && is_ie5_5up == false) || (is_moz && is_moz_ver < 1.4) || (is_os2 && is_os2 == false))//|| (is_chrome == false)
  {
      alert("This site requires Internet Explorer v.5.5 or higher or Netscape v.7.1 or higher or Mozilla v1.4  or higher. or Chrome");
      return;
  }

    var lbIsValid = true;

    var lsLoginID = document.getElementById("txtUsername").value;
    lsLoginID = lsLoginID.split(' ').join('');
    document.getElementById("txtUsername").value = lsLoginID;

    if (isEmail(lsLoginID) == false)
    {
      alert("Invalid e-mail address specified for Login ID. Please enter it in the following format: name@domain.nn \n" +
            "The following characters are not valid for the name part of the e-mail address. \n" +
            "     > greater than \n" +
            "     < less than \n" +
            "     ( open parentheses \n" +
            "     ) close parentheses \n" +
            "     [ open brackets \n" +
            "     ] close brackets \n" +
            "     \\ back slash \n" +
            "     % percent \n" +
            "     & ampersand \n" +
            "     + plus \n" +
            "     . period (see note below)\n" +
            "     , comma \n" +
            "     ; semicolon \n" +
            "     : colon \n" +
            "       whitespace character (such as a space or tab) \n" +
            "     @ at sign (as part of the name) \n" +
            "     \" double quote \n" +
            "Note: If there are any periods before the @ sign they must be followed by one or more valid characters. \n" +
            "A period cannot be the first character and there cannot be consecutive periods before the @ sign.");
            
      document.getElementById("txtUsername").focus();
      lbIsValid = false;    
    }

    if (lbIsValid)
    {
      if ( (document.getElementById("txtPassword").value == "") && (sValue == "L") ) 
      {
        alert("Password is required to perform this action.")
        document.getElementById("txtPassword").focus();
        lbIsValid = false;
      }
    }
    
    if (lbIsValid)
    {    
      if (sValue == "P")
      {
        document.frmLogin.realSubmit.value  = "fromLink" 
      }
    
      document.frmLogin.submit();
    }  
  }
  

  function isEmail(strEmail)
  {
    var re = /^(([^<>()[\]\\%&+.,;:\s@\"]+(\.[^<>()[\]\\%&+.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(strEmail);
  }

  function pswd_onclick(event) 
  {
    if ( event.keyCode == 13 ){
      login_onclick('L')
    }
  }

  function reset_onclick() 
  {
    document.getElementById("txtUsername").value  = "";
    document.getElementById("txtPassword").value = ""; 
  }
  
//-->
</script>

<form NAME="frmLogin" METHOD="post" ACTION="frmLogin.asp">
  <input type="hidden" name="realSubmit" value="fromButton">
  <input type="hidden" name="MaintenanceMode" value="<%= lcase(lbMaintenanceMode) %>">

  <div style="width:590px">
    <p class="login">
      Your company�s specific information on first and third party auto physical damage and related claims can be viewed by logging in below.
      A login ID and password are required to access this data.
      If you do not have a login ID and password, please contact your supervisor or the designated person in your company responsible for setting up ClaimPoint login IDs.
    </p>
    <p class="login">
      Once logged in, you can assign work, view real-time specific information on your claims and access various management reports.
    </p>
  </div>

  <br><br>
        
  <table width="600" border="0" cellspacing="0" cellpadding="4" style="border-collapse:collapse; border:1px solid #000099;">
    <colgroup>
      <col width="230px"/>
      <col width="380px"/>
    </colgroup>
    <tr bgcolor="#E5E5F5">
      <td>
        <p class="bodyBlue">Login ID:</p>
        <p class="mini">(User's valid e-mail address)</p>
      </td>
      <td>
        <input type="text" value="<%=sEmailAddress%>" name="txtUsername" id="txtUsername" onkeypress="pswd_onclick( event );" onBlur="ldTrim(this)" size="35" AUTOCOMPLETE="OFF">
      </td>
    </tr>
    <tr>
      <td nowrap class="box">
        <p class="bodyBlue">Password:</p>
        <p class="mini">Note: Passwords are case sensitive</p>
      </td>
      <td class="box">
        <input type="password" value="<%=sPassword%>" name="txtPassword" id="txtPassword" size="20" onkeypress="pswd_onclick( event );" onBlur="ldTrim(this)" AUTOCOMPLETE="OFF">
        <input type="checkbox" name="txtRemember" id="txtRemember" <% if sRememberUser = "on" then %> checked <% end if %> onkeypress="pswd_onclick( event );" title="Check this box to remember your login information">
        <a href="#" onClick="window.open('/frmRememberMe.asp', 'Remember me!', 'width=440,height=360,resizable=no')"
           title="What is remember my Login"
           onMouseOver="window.status='What is remember my Login'; return true"
           onMouseOut="window.status=''; return true">
          <span class="mini">Remember my Login</span>
        </a>
      </td>
    </tr>
  </table>
      
  <table width="600" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
    <colgroup>
      <col width="500px"/>
      <col width="100px"/>
    </colgroup>
    <tr>
      <td>&nbsp;</td>
      <td>
        <a href="Javascript:login_onclick('L');" 
           title="Click to Login"
           onMouseOver="window.status='Click to Login'; return true"
           onMouseOut="window.status=''; return true">
          <img border="0" src="images/btn_login.gif" WIDTH="83" HEIGHT="31">
        </a>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <p class="bodyLeaded">
          <a href="Javascript:login_onclick('P');"
             title="Click to get your Password Hint"
             onMouseOver="window.status='Click for Password Hint'; return true"
             onMouseOut="window.status=''; return true">
            Password Hint
          </a>
        </p>
      </td>  
    </tr>
  </table>

  <br>

  <div style="width:590px">
    <p class="login">
      If you are experiencing problems logging into your account, first be sure you are not using caps-lock. Passwords are case-sensitive. Another possible issue is that your browser may be set to not accept cookies. For further help, please call the LYNX helpdesk at 239-479-5999, and select option 1.
    </p>
  </div>

<!-- OneTrust Cookies Settings button start -->
<button class="ot-sdk-show-settings" type="button"> Cookie Settings</button>
<!-- OneTrust Cookies Settings button end -->

</form>

<script language="JavaScript">

    if ((is_ie && is_ie5_5up == false) || (is_moz && is_moz_ver < 1.4) || (is_os2 && is_os2 == false))//|| (is_chrome == false)
    {
        alert("This site requires Internet Explorer v.5.5 or higher or Netscape v.7.1 or higher or Mozilla v1.4  or higher. or Chrome");
    }

  // check if cookies are enabled
   if (!is_cookie)
     alert("You will need to enable cookies on your browser in order to login and use this site.");

  document.getElementById("txtUsername").focus();

</script>

<script language="JavaScript" defer>

  var lbMaintenanceModeAlertMsg = "<%= lcase(lbMaintenanceModeAlertMsg) %>";

  if (lbMaintenanceModeAlertMsg == "true" && glbMaintenanceMode == "true")
  {
    var lsMaintenanceModeMsg = "<%= lsMaintenanceModeMsg %>";
    alert("Unable to login. \n" + lsMaintenanceModeMsg);
  }
  else if (lbMaintenanceModeAlertMsg == "true" && glbMaintenanceMode == "false")
  {
    var lsMaintenanceModeMsg = "<%= lsMaintenanceModeMsg %>";
    alert("Unable to login. Please try again in a few minutes. \n");
  }
    
</script>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
