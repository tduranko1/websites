<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->

<%
  lsPageName = "frmSubmitComment.asp"
  lsPageTitle = "Submit Comment"
  lsPageSubTitle = "Submit Comment"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcSubmitComment
  
  Dim liLynxID, liAspectID, lsNoteSent, lsOkMessage, lsAssignmentType, liStatusID, liVehNumber
  
  liLynxID = Request("LynxID")
  liVehNumber = Request("VehNumber")
  liAspectID = request("AspectID")
  lsAssignmentType = Request("AssignmentType")
  liStatusID = request("StatusID")
  
  If Request("FromPage") = "rsp" Then
    If Request("SubmitStatus") = 1 Then
      lsOkMessage = "Comment was submitted successfully."
      lsNoteSent = ""
    Else
      lsMessage = "An error occurred while submitting your comment. Please try again."
      lsNoteSent = Request("Note")
    End If
  End If
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
  Dim lsNoteTypeIDGetStrXML, liNoteTypeID, liEventID, lsEventIDGetStrXML, docXML, ElemList

  Set docXML = Server.CreateObject("MSXML2.DOMDocument")
  docXML.Async = False
  lsNoteTypeIDGetStrXML = "<Config Path='ClaimPoint/CarrierComment/NoteTypeID'/>"
  docXML.LoadXML GetData(lsNoteTypeIDGetStrXML, "", "")
  liNoteTypeID = docXML.documentElement.text

  lsEventIDGetStrXML = "<Config Path='ClaimPoint/CarrierComment/EventID'/>"
  docXML.LoadXML GetData(lsEventIDGetStrXML, "", "")
  liEventID = docXML.documentElement.text
%>

<!--#include file="includes/incCommonHTMLStartClm.asp"-->

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspClaimHistory.asp">

<!--#include file="includes/incHeaderTableWithIcons.asp"-->
<!--#include file="includes/incClaimCommonTop.asp"-->

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
  <tr>
    <td><img border="0" src="images/mark_comments2.jpg" width="96" height="63"></td>
  </tr>
  <tr>
    <td><img border="0" src="images/spacer.gif" width="1" height="10"/></td>
  </tr>
  <tr>
    <td>
      <a href="Javascript:self.print()" class="subnav" title="Click to print this page.">
      <img src="images/icon_print.gif" align="top" width="31" height="20" border="0"/>Print</a>
    </td>
  </tr>
</table>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
  Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "ClaimHeader.xsl", "pageTitle=" & lsPageSubTitle & lcDelimiter & "AspectID=" & liAspectID, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

</form>

<script language="JavaScript">

  function goSubmit()
  {
    /*
    var liVehCounter = <%= liVehSelCounter %>; //comes from incHeaderTableWithIcons.asp
    var loSel = document.getElementById('loSelVehComment');
    var lsSelVehicle = loSel.options[loSel.selectedIndex].value;
    if (liVehCounter > 1 && lsSelVehicle == "")
    {
      alert("Please select a vehicle before submitting.");
      return;
    }
    */
    if (document.getElementById('txtComment').value.length < 10)
    {
      alert("Please enter a comment before submitting.");
      return;
    }

    //alert(document.getElementById("ddlCommentType").value);
    if (document.getElementById('divCommentType').style.display == "inline")
    {
        document.getElementById('txtComment').value = document.getElementById("ddlCommentType").value + " " + document.getElementById('txtComment').value
    }

    if (document.getElementById('txtComment').value.length > 1000)
    {
      alert("You have exceeded the maximum length for comment. (" +document.getElementById('txtComment').value.length + ") \nPlease shorthen the comment entered before submitting.");
      return;
    }
    document.getElementById('animeImg').style.display = "inline";
    document.getElementById('hidNote').value = document.getElementById('txtComment').value;

    var objForm = document.getElementById("frmSubmitComment");
    objForm.submit();
  }

</script>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="20" nowrap >&nbsp;</td>
    <td colspan="2" nowrap >

    <!-- TVD - 03Nov2016 - Choice process only Comment Type -->
    <% if lsAssignmentType="Choice Shop Assignment" then %>
        <div id="divCommentType" style="display: inline;">    
    <% else %>
        <div id="divCommentType" style="display: none;">
    <% end if %>

        <br />
        Comment Type:  &nbsp;&nbsp;
        <select id="ddlCommentType">
            <option selected value="[Other]">Other</option>
            <option value="[109-Cancel]">Claim Cancel</option>
            <option value="[110-ReOpen]">Claim ReOpen</option>
          </select> 
        <br />
      </div>

      <br>
      <span id="commFldMsg" style="color:#000000; font-weight:normal; font-size:8pt">(max. 1000 characters)</span>
      <br>
      <textarea name="txtComment" id="txtComment" wrap="physical" style="width:100%; height:140px;" onKeyDown="CheckInputLength(this, 1000)"><%= lsNoteSent %></textarea>
    </td>
    <td width="20" nowrap >&nbsp;</td>
  </tr>
  <tr>
    <td width="20" nowrap >&nbsp;</td>
    <td align="right" nowrap>
    <span class="okMessage"><%=lsOkMessage%></span>
    <div id="animeImg" style="display:none"><img src="images/loading_anim.gif" alt="" width="94" height="17" border="0"><div>
    </td>
    <td align="right" nowrap>
      <a href="Javascript:goSubmit();"
         title="Click to Submit"
         onMouseOver="window.status='Click to Submit'; return true"
         onMouseOut="window.status=''; return true">
        <img border="0" src="images/btn_submit.gif" WIDTH="83" HEIGHT="31"></a>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </td>
    <td width="20" nowrap >&nbsp;</td>
  </tr>
</table>

<!--#include file="includes/incClaimCommonBottom.asp"-->

  <form id="frmSubmitComment" name="frmSubmitComment" method="post" target="IFrmSubmitComment" action="rspSubmitComment.asp">
    <input type="hidden" id="hidAspectID" name="hidAspectID" value="<%= liAspectID %>"/>
    <input type="hidden" id="hidNoteTypeID" name="hidNoteTypeID" value="<%= liNoteTypeID %>"/>
    <input type="hidden" id="hidStatusID" name="hidStatusID" value="<%= liStatusID %>"/>
    <input type="hidden" id="hidNote" name="hidNote" value=""/>
    <input type="hidden" id="hidUserID" name="hidUserID" value="<%= liUserID %>">
    <input type="hidden" id="hidEventID" name="hidEventID" value="<%= liEventID %>">
    <input type="hidden" id="hidLynxID" name="hidLynxID" value="<%= liLynxID %>">
    <input type="hidden" id="hidAssignmentType" name="hidAssignmentType" value="<%= lsAssignmentType %>">
    <input type="hidden" id="hidVehNumber" name="hidVehNumber" value="<%= liVehNumber %>">

  </form>
    
  <iframe src="blank.asp" id="IFrmSubmitComment" name="IFrmSubmitComment" style="height:0px; width:0px; display:none;" frameBorder="0" />

<!--#include file="includes/incCommonHTMLEndClm.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
