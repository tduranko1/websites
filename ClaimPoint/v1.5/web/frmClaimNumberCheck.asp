<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!-- v1.5.0.0 -->

<%
  Dim lsCheckClaimNumberCall, lsCheckClaimNumberRet, lsGUID
  Dim loXML, loVehicles, loVehicle, lbClaimNumberExists, loRoot, liLynxID
  Dim lsControlName, lsClaimNumber, lsVehicleList
  Dim lsCurrentAssignmentTypeID, lsCurrentAssignmentTypeDesc
  
  lsControlName = Request.QueryString("ob")
  lsClaimNumber = Request.QueryString("cn")
  lsGUID = Request.Cookies("CPSession")("guidArray0")
  lsCurrentAssignmentTypeID = Request.Cookies("CPSession")("CurrAssignmentId")
  lsCurrentAssignmentTypeDesc = Request.Cookies("CPSession")("CurrAssignmentDescription")
  
  lsCheckClaimNumberCall = "<ClaimNumberCheck ClientClaimNumber=""" + lsClaimNumber + """ InsuranceCompanyID=""" + liInsuranceCompanyID + """/>"
  lsCheckClaimNumberRet =  GetPersistedData(lsCheckClaimNumberCall, lsGUID, "", "", false)
  'response.write lsCheckClaimNumberRet
  set loXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  loXML.Async = False
  loXML.LoadXML lsCheckClaimNumberRet
  
  set loRoot = loXML.selectSingleNode("/Root")
  if not loRoot is nothing then
    liLynxID = loRoot.getAttribute("LynxID")
  end if
    
  lbClaimNumberExists = false
  set loVehicles = loXML.selectNodes("/Root/Vehicle[@ClaimAspectID != '0']")
  if not loVehicles is nothing then
    if loVehicles.length > 0 then
      lbClaimNumberExists = true
      on error goto 0
      for each loVehicle in loVehicles
        lsVehicleList = lsVehicleList & "Vehicle " & loVehicle.getAttribute("VehicleNumber") & ": "
        lsVehicleList = lsVehicleList & loVehicle.getAttribute("CurrentAssignmentType") & "; " 
        if trim(loVehicle.getAttribute("BusinessName")) <> "" then
          lsVehicleList = lsVehicleList & trim(loVehicle.getAttribute("BusinessName"))
        else
          lsVehicleList = lsVehicleList & trim(loVehicle.getAttribute("NameFirst")) & " " + trim(loVehicle.getAttribute("NameLast"))
        end if
        lsVehicleList = lsVehicleList & "; " & _
                        loVehicle.getAttribute("VehicleYear") & " " & loVehicle.getAttribute("Make") & " " & loVehicle.getAttribute("Model") & "\n"
      next
    end if
  end if
  set loXML = nothing
%>

<html>
<head>
	<title>Claim Number Check</title>
</head>

<body>
  <form name="frmMainForm" id="frmMainForm" method="POST" action="frmWaMainAddVehicle.asp" target="_top">
    <input type="hidden" name="LynxID" id="LynxID" value="<%=liLynxID%>">
    <input type="hidden" name="hidAssignmentType" id="hidAssignmentType">
    <input type="hidden" name="hidVehiclesList" id="hidVehiclesList" >
    <input name="AssignmentDesc" id="txtAssignmentDesc" type="hidden" value="<%=lsCurrentAssignmentTypeDesc%>">
    <input name="AssignmentId" id="txtAssignmentId" type="hidden"  value="<%=lsCurrentAssignmentTypeID%>">
    <input name="FrameSrc" id="txtFrameSrc" type="hidden" value="">
    <%if lsCurrentAssignmentTypeID = "4" or lsCurrentAssignmentTypeID = "16" then%>
    <input name="ShopLocationID" id="txtShopLocationID" type="hidden">
    <input name="ShopSearchLogID" id="txtShopSearchLogID" type="hidden">
    <input name="SelectedShopRank" id="txtSelectedShopRank" type="hidden">
    <input name="SelectedShopScore" id="txtSelectedShopScore" type="hidden">
    <textarea name="ShopRemarks" id="txtShopRemarks"></textarea>
    <input name="ShopName" id="txtShopName" type="hidden">
    <input name="ShopAddress1" id="txtShopAddress1" type="hidden">
    <input name="ShopAddress2" id="txtShopAddress2" type="hidden">
    <input name="ShopCity" id="txtShopCity" type="hidden">
    <input name="ShopState" id="txtShopState" type="hidden">
    <input name="ShopZip" id="txtShopZip" type="hidden">
    <input name="ShopPhone" id="txtShopPhone" type="hidden">
    <input name="ShopFax" id="txtShopFax" type="hidden">
    <input name="ShopMFopen" id="txtShopMFopen" type="hidden">
    <input name="ShopMFClosed" id="txtShopMFClosed" type="hidden">
    <input name="ShopSatOpen" id="txtShopSatOpen" type="hidden">
    <input name="ShopSatClosed" id="txtShopSatClosed" type="hidden">
    <input name="vehNum" id="txtVehNum" type="hidden" value="1">    
    <%end if%>
  </form>
</body>
</html>
  <script language="JavaScript">
    var lbClaimNumberExists = <%=lcase(CStr(lbClaimNumberExists))%>;
    var lsClaimNumber = "<%=lsClaimNumber%>";
    var lsControlName = "<%=lsControlName%>";
    var lsVehicleList = "<%=lsVehicleList%>";
    var liLynxID = "<%=liLynxID%>";

    if (lbClaimNumberExists) {
      var bRet = confirm("Claim Number " + lsClaimNumber + " already exist in the system. The LYNX Id corresponding to this claim number is " + liLynxID + " and has the following vehicles:\n\n" + lsVehicleList + "\nWould you like to add a vehicle to this claim?")
      if (bRet == true) {
        parent.parent.document.body.onbeforeunload = "";
        document.getElementById("txtFrameSrc").value = top.lsAssignmentType;

        if ((document.getElementById("txtAssignmentId").value == "4") || (document.getElementById("txtAssignmentId").value == "16")){
          var oDC = top.getdcIFrameDoc();
          if (oDC) {
            document.getElementById("txtShopLocationID").value = oDC.getElementById("txtShopLocationID_1").value;
            document.getElementById("txtShopSearchLogID").value = oDC.getElementById("txtShopSearchLogID_1").value;
            document.getElementById("txtSelectedShopRank").value = oDC.getElementById("txtSelectedShopRank_1").value;
            document.getElementById("txtSelectedShopScore").value = oDC.getElementById("txtSelectedShopScore_1").value;
            document.getElementById("txtShopRemarks").innerHTML = oDC.getElementById("txtShopRemarks_1").innerHTML;
            document.getElementById("txtShopName").value = oDC.getElementById("txtShopName_1").value;
            document.getElementById("txtShopAddress1").value = oDC.getElementById("txtShopAddress1_1").value;
            document.getElementById("txtShopAddress2").value = oDC.getElementById("txtShopAddress2_1").value;
            document.getElementById("txtShopCity").value = oDC.getElementById("txtShopCity_1").value;
            document.getElementById("txtShopState").value = oDC.getElementById("txtShopState_1").value;
            document.getElementById("txtShopZip").value = oDC.getElementById("txtShopZip_1").value;
            document.getElementById("txtShopPhone").value = oDC.getElementById("txtShopPhone_1").value;
            document.getElementById("txtShopFax").value = oDC.getElementById("txtShopFax_1").value;
            document.getElementById("txtShopMFopen").value = oDC.getElementById("txtShopMFopen_1").value;
            document.getElementById("txtShopMFClosed").value = oDC.getElementById("txtShopMFClosed_1").value;
            document.getElementById("txtShopSatOpen").value = oDC.getElementById("txtShopSatOpen_1").value;
            document.getElementById("txtShopSatClosed").value = oDC.getElementById("txtShopSatClosed_1").value;
          }
        }
        document.frmMainForm.submit();
      } 
    }
  </script>

<!--#include file="includes/incCommonBottom.asp"-->
