<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incMail.asp"-->
<!--#include file="includes/incCryptTools.asp"-->
<%
lsPageName = "p_add_user.asp"
lsPageTitle = "Add User"
liPageType = lcBeforeInsuranceCompanySelect

dim sUserName
dim sPassword
dim sParameters
dim sProcess
dim sBody
dim loAware
dim liReqResult
dim liAppResult
dim InsuranceCompanyID
dim sAPDSrv
dim sAPDEnv

If Instr(1,strServerName, "DEV") > 0 Then	'Development
		sAPDSrv = "<a href='http://sgofetemdev1/apd'>http://sgofetemdev1/apd</a>"
    sAPDEnv = "(Development environment) "
ElseIf Instr(1,strServerName, "STG") > 0 Then		'Staging
		sAPDSrv = "<a href='https://stgbuyat.ppg.com/apd'>http://stgbuyat.ppg.com/apd</a>"
    sAPDEnv = "(Staging environment) "
ElseIf Instr(1,strServerName, "PRD") > 0 Or (uCASE(Left(strServerName,3)) = "BUY") Then	'Production
		sAPDSrv = "<a href='https://apd.lynxservices.com/apd'>https://apd.lynxservices.com/apd</a>"
    sAPDEnv = ""
Else	'default
		sAPDSrv = "<a href='https://apd.lynxservices.com/apd'>https://apd.lynxservices.com/apd</a>"
    sAPDEnv = ""
End if

sUserName	=	Request.form("txtUserName")
sProcess	=	Request.form("HidProcess")

Randomize
sPassword = "Lynx" & Int(90000 * Rnd + 10000)

set loAware = CreateObject("eTemGlobalReg.clsRegistration")

sBody = "Welcome to the LYNX Services Claim Point website " & sAPDEnv & "(" & sAPDSrv & ").<br><br>"_ 
			& "Please keep this e-mail for future reference. It contains important information regarding your login ID and password.<br><br>"_
			& "You are registered to use the LYNX Services Claim Point website.<br><br>"_
      & "LoginID:&nbsp;&nbsp;&nbsp;<b>" & sUserName & "</b><br>"_
      & "Password:&nbsp;<b>" & sPassword & "</b><br><br>"_
      & "Please remember that the password is case sensitive and must be enter exactly as shown above.<br><br>"_
      & "Thank you.<br>"_
      & "LYNX Services Team"
			
if Request("hidInsuranceCompanyID")<> "" then
	InsuranceCompanyID = cint(Request("hidInsuranceCompanyID"))
else
	InsuranceCompanyID = 0
end if

if sProcess = "Add" then
	
	'************* Code to check if the user with the e-mail address already exist ******
	Dim aRs
	Dim liUserCount 
	aRs = loAPD.GetData("GetUserCount",sUserName,false)
	liUserCount = 1
	if isArray(aRs) then
		liUserCount = aRs(0,0)
	end if
	
	if liUserCount > 0 then 'Check to see if any users are present with this e-mail address
		Response.Redirect "frmUserDetail.asp?M=" & Request("HidUserType")&"&msg=3"&"&username=" & sUserName & "&firstname=" & Request.form("txtFirstName") & "&lastname=" & Request.form("txtLastName") &"&Telephone=" & Request.form("txtTelephone") 
	end if
	'***********************************************************************************
	
	liReqResult  =	loAware.Request(sUserName, "", "", "", "", "", sPassword, liLynxAppID)
	if liReqResult = 0 then
		if loAware.CreateUser( cstr(sUserName),cstr(sPassword) ) = 2 then
      'User Already Exists in Active Directory
			'Just to tell the user that he needs to use his old password as he is already created 
      sBody = "Welcome to the LYNX Services Claim Point website " & sAPDEnv & "(" & sAPDSrv & ").<br><br>"_ 
            & "You are already registered to use the LYNX Services Claim Point website. "_
            & "Please use your password to login to the website. If you can't recall your current password, "_
            & "you can't have it reset by clicking on the Forgot Password button.<br><br>"_
            & "LoginID:&nbsp;&nbsp;&nbsp;<b>" & sUserName & "</b><br><br>"_
            & "Please remember that the password is case sensitive and must be enter exactly as shown above.<br><br>"_
            & "Thank you.<br>"_
            & "LYNX Services Team"
		end if	
		liAppResult = loAware.Approve(liLynxAppID,loAware.GetUserIDFromEmail(sUserName),"","APDApprove@ppg.com",1)
		if liAppResult <> 0 then 
			Response.Redirect "frmUserDetail.asp?M=" & Request("HidUserType")&"&msg=2" 
		end if
	end if
	'Sending an e-mail
  Dim sSenderEmail
  sSenderEmail = fDecrypt("LYNXAPDKEY", Request.Cookies("authUser"))
	SendMail sUserName, sSenderEmail, "Registration", sBody , "" , ""
end if
	
if loAPD.GetData("AddUpateUser", InsuranceCompanyID & lcDelimiter & sUserName & lcDelimiter & Request.form("txtFirstName")& lcDelimiter & Request.form("txtLastName")& lcDelimiter & Request.form("txtTelephone")& lcDelimiter & Request.form("cbUserType")& lcDelimiter & Request.form("cbSecuritylevel") , false) = 0 then
	Response.Redirect "frmUserList.asp?M=" & Request("hidUserType")&"&msg=1"
else
	Response.Redirect "frmUserDetail.asp?M=" & Request("HidUserType")&"&msg=2" 	 
end if
%>



