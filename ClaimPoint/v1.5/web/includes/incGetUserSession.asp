
<%
  <!-- v1.4.5.0 -->

  Dim liUserID, lsUserEmail, liOfficeID, lsOfficeName, lsClientOfficeId, liInsuranceCompanyID, lsInsuranceCompanyName, liDocumentUploadFlag
  Dim lsUserNameFirst, lsUserNameLast, lsPhoneAreaCode, lsPhoneExchangeNumber, lsPhoneUnitNumber, lsPhoneExtensionNumber, liAuthorizePayments
  Dim liPaymentAuthorizationLevel, liClaimRead, liClaimCreate, liViewReport, liManageUsers, liClaimViewLevel, liCarrierRep, lsInsCoConfigCall, docInsCoXML
  Dim liAllowCarrierRepSelectionFlag, liCarrierAuthorizesPaymentsFlag, lsElemList, loNode
  Dim liShowPromiseToPay, liReserveLineTypeScript, liEmergencyLiabilityScript, liSB511Script, liDisableAmicaFields, lsPromiseToPayStatement
  Dim liLaborRatesSetupFlag, liRequireFirstPartyDeductible, liDisableInsuredAddress, liDisablePolicyNumber
  Dim liAuthTypeScript
  
  liUserID = Request.Cookies("CPSession")("UserID")
  lsUserEmail = Request.Cookies("CPSession")("UserEmail")
  liOfficeID = Request.Cookies("CPSession")("OfficeID")
  lsOfficeName = Request.Cookies("CPSession")("OfficeName")
  lsClientOfficeId = Request.Cookies("CPSession")("ClientOfficeId")
  liInsuranceCompanyID = Request.Cookies("CPSession")("InsuranceCompanyID")
  lsInsuranceCompanyName = Request.Cookies("CPSession")("InsuranceCompanyName")
  liDocumentUploadFlag = Request.Cookies("CPSession")("DocumentUploadFlag")
  lsUserNameFirst = Request.Cookies("CPSession")("NameFirst")
  lsUserNameLast = Request.Cookies("CPSession")("NameLast")
  lsPhoneAreaCode = Request.Cookies("CPSession")("PhoneAreaCode")
  lsPhoneExchangeNumber = Request.Cookies("CPSession")("PhoneExchangeNumber")
  lsPhoneUnitNumber = Request.Cookies("CPSession")("PhoneUnitNumber")
  lsPhoneExtensionNumber = Request.Cookies("CPSession")("PhoneExtensionNumber")
  liClaimRead = Request.Cookies("CPSession")("ClaimRead")
  liClaimCreate = Request.Cookies("CPSession")("ClaimCreate")
  liViewReport = Request.Cookies("CPSession")("ViewReport")
  liManageUsers = Request.Cookies("CPSession")("ManageUsers")
  liClaimViewLevel = Request.Cookies("CPSession")("ClaimViewLevel")
  liCarrierRep = Request.Cookies("CPSession")("CarrierRep")
  liAuthorizePayments = Request.Cookies("CPSession")("AuthorizePayments")
  liPaymentAuthorizationLevel = Request.Cookies("CPSession")("PaymentAuthorizationLevel")
  liShowPromiseToPay =  Request.Cookies("CPSession")("ShowPromiseToPay")
  liReserveLineTypeScript = Request.Cookies("CPSession")("ReserveLineTypeScript")
  liEmergencyLiabilityScript = Request.Cookies("CPSession")("EmergencyLiabilityScript")
  liSB511Script = Request.Cookies("CPSession")("SB511Script")
  liDisableAmicaFields = Request.Cookies("CPSession")("DisableAmicaFields")
  liLaborRatesSetupFlag = Request.Cookies("CPSession")("LaborRatesSetupFlag")
'  liRequireFirstPartyDeductible = Response.Cookies("CPSession")("RequireFirstPartyDeductible")
  liDisableInsuredAddress = Request.Cookies("CPSession")("DisableInsuredAddress")
  liDisablePolicyNumber = Request.Cookies("CPSession")("DisablePolicyNumber")

  ' 19Jan2012 - TVD - Elephant Custom Scripting
  liAuthTypeScript =  Request.Cookies("CPSession")("CurrAuthType")
  liAuthTypeScript = 1
  
  lsInsCoConfigGUID = Request.Cookies("CPSession")("guidArray5")

  lsInsCoConfigCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"

  Set docInsCoXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docInsCoXML.Async = False
  'docInsCoXML.LoadXML GetData(lsInsCoConfigCall, "", "")
  docInsCoXML.LoadXML GetPersistedData(lsInsCoConfigCall, lsInsCoConfigGUID, "", "", true)

  If Not docInsCoXML.ParseError = 0 Then
    'Response.Write "<b>Error Code:</b> " & docInsCoXML.ParseError & "<br>"
  Else
    liAllowCarrierRepSelectionFlag = docInsCoXML.documentElement.getAttribute("AllowCarrierRepSelectionFlag")
    liCarrierAuthorizesPaymentsFlag = docInsCoXML.documentElement.getAttribute("CarrierAuthorizesPaymentsFlag")

    If liShowPromiseToPay = 1 Then

      Set lsElemList = docInsCoXML.SelectNodes("/Root/CustomScripting")
    
      If lsElemList.length > 0 Then
    
        For each loNode in lsElemList

          'Promise To Pay Statement
          If LCASE(loNode.getAttribute("InputResponseName")) = "promisetopay" Then
            lsPromiseToPayStatement = loNode.getAttribute("ScriptText")
          End If

        Next
    
      End If
  
      Set lsElemList = Nothing

    End If

  End If

  Set docInsCoXML = Nothing

%>
