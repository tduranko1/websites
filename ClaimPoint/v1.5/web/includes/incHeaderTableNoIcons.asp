<!-- v1.4.2.0 -->

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="100%" height="132">
  <tr>
    <td width="596" align="right" valign="top" height="50">

    <%if liPageType > lcAuthSection then%>
      <img border="0" src="images/logout.jpg" width="228" height="34" usemap="#help_logout">
      <map name="help_logout">
        <area id="HelpBtn" coords="82,15,128,32" title="Help"
          href="javascript:openHelpWin('<%= lsHelpPage %>')"
          onMouseOver="window.status='Help'; return true"
          onMouseOut="window.status=''; return true">
        <area coords="158,14,215,32" href="frmLogout.asp" title="Logout"
          onMouseOver="window.status='Logout'; return true"
          onMouseOut="window.status=''; return true">
      </map>
    <%end if%>

    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" valign="top" height="100%">

      <%if liPageType > lcAuthSection then%>
        <span class="loggedInAs">Logged in as:</span><span class="loggedInName"> <%=lsUserNameFirst%>&nbsp;<%=lsUserNameLast%></span>
        <span class="loggedInAs">&nbsp;<br></span>
        <%if len(lsInsuranceCompanyName) > 0 then%>
          <span class="loggedInAs">Insurance Company:</span>
          <span class="loggedInName"><%=lsInsuranceCompanyName%></span>  
        <%end if%>  
        <%if len(lsClientOfficeId) > 0 then%>
          <span class="loggedInName"> - <%=lsClientOfficeId%></span>
        <%end if%>  
      <%end if%>

      <span class="errorMessage"><%=lsMessage%></span>
    </td>
  </tr>
  <tr>
    <td colspan="2" valign="bottom" nowrap>
      <p class="header"><span id="spanlsPageTitle"><%=lsPageTitle%></span></p>
    </td>
  </tr>
</table>

<% If lsPageName <> "frmWaMain.asp" Then %>

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
  <!--#include file="incAPDOutageCheck.asp"-->
  
<% End If %>

<img src="images/spacer.gif" border="0" width="10" height="10"><br>

