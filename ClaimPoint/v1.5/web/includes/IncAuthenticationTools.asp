<%
Function UserAuthenticate(sUserName,sPassword)
  Dim lsUserSessionCall, docXML, NodeUser, liUserID, lsSysLastUpdatedDate

  lsUserSessionCall = "<UserSessionDetail Login=""" & sUserName & """ ApplicationCD=""" & lsApplicationCD & """ />"

  set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML GetData(lsUserSessionCall, "", "")

  If Not docXML.ParseError = 0 Then

  Else
    Set NodeUser = docXML.documentElement.selectSingleNode("User")

    If NodeUser.getAttribute("UserID") = "0" Then
      liUserID = "0"

    Else
      liUserID = NodeUser.getAttribute("UserID")

    End If

  End If

  Set NodeUser = Nothing

  Dim loAware, iResult, sADGroupName, bLockedOut
  sADGroupName = "GOFWebLynxAPD"
  set loAware = CreateObject("AWARE.clsAWARE")

  If err.number <>0 Then
    UserAuthenticate = "gaged"
  End If
  If liUserID = "0" Then
    UserAuthenticate = "Invalid Login ID. User does not exist in the LYNX ClaimPoint system. " & _
                       "Please check the Login ID entered for accuracy."

  Else
    bLockedOut = loAware.IsAccountLockedOut(sUserName)

    If bLockedOut = true Then
      UserAuthenticate = "Your account has been locked due to too many invalid login retries. " & _
                         "Please ask you supervisor to unlock your account and retain your current password. " & _
                         "Or you can unlock it yourself by clicking on the Forgot Password button. " & _
                         "Your account will be unlocked and a new password will be e-mailed to you."

    ElseIf bLockedOut = false Then
    	iResult = loAware.Authenticate(sUserName,sPassword,sADGroupName)
    	'iResult = 0
    	Select Case iResult
    		Case 0
    			UserAuthenticate = "Password has expired. (0)"
         case 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14
            UserAuthenticate = "Success|" & iResult & " day(s) until password to expire."
    		Case 100
    			UserAuthenticate = "Unable to validate your Login ID. User does not exist in the LYNX ClaimPoint system. " & _
                             "Please check the Login ID entered for accuracy. (100)"
    		Case 200
    			UserAuthenticate = "Invalid user name and password combination. " & _
                             "Click the Password Hint below to view your password hint, or click Forgot Password to have your password reset and sent to your e-mail address. (200)"
    		Case 300
    			UserAuthenticate = "Unable to validate your Login ID. Please check the Login ID entered for accuracy. (300)"
    		Case 400
    			UserAuthenticate = "Success"	
    		Case 500
    			UserAuthenticate = "Unable to validate your Login ID. Please check the Login ID entered for accuracy. (500)"
         case else
            UserAuthenticate = "Unable to validate your Login ID. Please check the Login ID entered for accuracy. (" & iResult & ")"
    	End Select

    Else ' -1 returned
      UserAuthenticate = "Unexpected system error. Please try again or contact the system administrator. " 

    End If

  End If

  Set loAware = Nothing
  'test to see if this will trigger the alert message
  'UserAuthenticate = "0"

End Function


Function GetPasswordHint(sUserName)
  lsUserSessionCall = "<UserSessionDetail Login=""" & sUserName & """ ApplicationCD=""" & lsApplicationCD & """ />"

  set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML GetData(lsUserSessionCall, "", "")

  If Not docXML.ParseError = 0 Then

  Else
    Set NodeUser = docXML.documentElement.selectSingleNode("User")

    If NodeUser.getAttribute("UserID") = "0" Then
      GetPasswordHint = "User does not exist in the LYNX ClaimPoint system. Please check the Login ID entered for accuracy."
    
    Else
      GetPasswordHint = "Your Password Hint is: " & NodeUser.getAttribute("PasswordHint")

    End If

  End If

  Set NodeUser = Nothing

End Function


Function ChangePassword(sUserName,sOldPassword,sNewPassword,sPasswordHint)
  Dim lsUserSessionCall, docXML, NodeUser, liUserID, lsSysLastUpdatedDate
  lsUserSessionCall = "<UserSessionDetail Login=""" & sUserName & """ ApplicationCD=""" & lsApplicationCD & """ />"

  Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML GetData(lsUserSessionCall, "", "")

  If Not docXML.ParseError = 0 Then
    liUserID = 0

  Else
    Set NodeUser = docXML.documentElement.selectSingleNode("User")

    If NodeUser.getAttribute("UserID") = "0" Then
      liUserID = 0

    Else
      liUserID = NodeUser.getAttribute("UserID")
      lsSysLastUpdatedDate = NodeUser.getAttribute("UASysLastUpdatedDate")

    End If

  End If

  Set NodeUser = Nothing

  If liUserID = 0 Then
    ChangePassword = "User does not exist in the LYNX ClaimPoint system. Please check the Login ID entered for accuracy. (0)"

  Else
  	Dim sActUserName, loAware, iResult, sADGroupName, lsStrXML, loAPDData, liRetVal, NodeClaimPointPermission, liChangePassword, lsTempInsuranceCompanyName
  	sActUserName = Replace(sUserName,"$","@")
  	'ChangePassword = 0 
  	sADGroupName = "GOFWebLynxAPD"
  	set loAware = CreateObject("AWARE.clsAWARE")
  	liChangePassword = loAware.ChangePassword(sUserName,sOldPassword,sNewPassword)
  	
    Set loAware = Nothing

    If liChangePassword = 0 Then
      ChangePassword = "Unexpected system error. Please try again or contact the system administrator. (2)"
    
    ElseIf liChangePassword = 1 Then
      ChangePassword = "Unable to validate your Login ID. User does not exist in the LYNX ClaimPoint system. " & _
                       "Please check the Login ID entered for accuracy. (0)"
  	
    ElseIf liChangePassword = 2 Then
      ChangePassword = "Old Password is incorrect. Please try again with the correct Old Password."

    ElseIf liChangePassword = 4 Then
      ChangePassword = "Your new password does not meet the minimum requirements. " & _
                       "See the requirements below."

    ElseIf liChangePassword = 5 Then
      ChangePassword = "Unable to validate your Login ID. User does not exist in the LYNX ClaimPoint system. " & _
                       "Please check the Login ID entered for accuracy. (1)"

    ElseIf liChangePassword = 3 Then 'Success
      ChangePassword = "Successful"
      lsStrXML = "<UserApplicationUpdate " + _
                  " UserID=""" + liUserID + """" + _
                  " ApplicationID=""" + ApplicationID + """" + _
                  " LogonId=""" + sUserName + """" + _
                  " PasswordHint=""" + sPasswordHint + """" + _
                  " SysLastUserID=""" + liUserID + """" + _
                  " SysLastUpdatedDate=""" + lsSysLastUpdatedDate + """" + _
                  " />"
Dim mexMonikerString 
         mexMonikerString = GetWebServiceURL()      
	Set loAPDData = GetObject(mexMonikerString)
      'Set loAPDData = CreateObject("PPGNAAPD.clsAPDData")

      liRetVal = loAPDData.PutXML(lsStrXML)
      Set loAPDData = Nothing

    End If

  End if

End Function


Function ForgotPassword(sUserName)
  Dim lsUserSessionCall, docXML, NodeUser, liUserID, lsSysLastUpdatedDate

  lsUserSessionCall = "<UserSessionDetail Login=""" & sUserName & """ ApplicationCD=""" & lsApplicationCD & """ />"

  set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML GetData(lsUserSessionCall, "", "")

  If Not docXML.ParseError = 0 Then
    'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"

  Else
    Set NodeUser = docXML.documentElement.selectSingleNode("User")

    If NodeUser.getAttribute("UserID") = "0" Then
      liUserID = 0

    Else
      liUserID = NodeUser.getAttribute("UserID")
      lsSysLastUpdatedDate = NodeUser.getAttribute("UASysLastUpdatedDate")

    End If

  End If

  Set NodeUser = Nothing

  Dim loAware, iResult, sADGroupName, lsStrXML, loAPDData, liRetVal, sPasswordHint, iResetPassword

  sADGroupName = "GOFWebLynxAPD"
  set loAware = CreateObject("AWARE.clsAWARE")

  If liUserID = 0 Then
    ForgotPassword = "User does not exist in the LYNX ClaimPoint system. Please check the Login ID entered for accuracy. (0)"

  Else
    iResetPassword = loAware.ResetPassword(sUserName, "" ,"", true)


    If iResetPassword = 0 Then
      ForgotPassword = "Unexpected system error. Please try again or contact the system administrator. (1)"
      
    ElseIf iResetPassword = 1 Then
      ForgotPassword = "User does not exist in the LYNX ClaimPoint system. Please check the Login ID entered for accuracy. (2)"

  	Else 'Success
      ForgotPassword = "Password reset was successful. Your reset Password will be sent to your e-mail address soon."

      sPasswordHint = "Password was reset to a random value."
      lsStrXML = "<UserApplicationUpdate " + _
                 " UserID=""" + liUserID + """" + _
                 " ApplicationID=""" + ApplicationID + """" + _
                 " LogonId=""" + sUserName + """" + _
                 " PasswordHint=""" + sPasswordHint + """" + _
                 " SysLastUserID=""" + liUserID + """" + _
                 " SysLastUpdatedDate=""" + lsSysLastUpdatedDate + """" + _
                 " />"
	Dim mexMonikerString 
         mexMonikerString = GetWebServiceURL()      
	Set loAPDData = GetObject(mexMonikerString)
      'Set loAPDData = CreateObject("PPGNAAPD.clsAPDData")
      liRetVal = loAPDData.PutXML(lsStrXML)
      Set loAPDData = Nothing

    End If

  End If

  Set loAware = Nothing

End function

function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function


%>