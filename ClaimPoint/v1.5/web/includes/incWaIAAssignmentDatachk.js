// JavaScript Document
// v1.5.0.0

var gsPrevPage = ""; //global previous page
var gsNextPage = ""; //global next page
var gsVehNum = "";
var gsVehCount = "";
var gsRetToClaim = "false";
var gsRetFromClaim = "false";
var gsEditVehMode = "false";
var gLogoutTimer = null;
var gsSrhZipCode = "";
var lsPageName;


function resetTimeOut()
{
  if (gLogoutTimer != null)
    window.clearTimeout(gLogoutTimer);
  gLogoutTimer = window.setTimeout("top.location = 'frmLogout.asp'", 240*60*1000);
}


function getdcIFrameDoc()
{
	var loDcIFrameDoc;
	var loIFrameObj = document.getElementById("dataContIfr");
	if (loIFrameObj.contentDocument) // For NS6
		return loDcIFrameDoc = loIFrameObj.contentDocument;
	else if (loIFrameObj.contentWindow) // For IE5.5 and IE6
		return loDcIFrameDoc = loIFrameObj.contentWindow.document;
  else if (loIFrameObj.document) // For IE5
  	return loDcIFrameDoc = loIFrameObj.document;
  else
		return;
}


function getuiIFrameDoc()
{
	var loUiIFrameDoc;
	var loIFrameObj = document.getElementById("uiFrame");
	if (loIFrameObj.contentDocument) // For NS6
		return loUiIFrameDoc = loIFrameObj.contentDocument;
	else if (loIFrameObj.contentWindow) // For IE5.5 and IE6
		return loUiIFrameDoc = loIFrameObj.contentWindow.document;
	else if (loIFrameObj.document) // For IE5
		return loUiIFrameDoc = loIFrameObj.document;
	else
		return;
}


function tsIAAssignment()
{

	var loUiIFrameDoc = getuiIFrameDoc();
	var lsMsg = "";
	var lsReq = "";
	var lsNumOnly = "";

	var sOwnerPhone = loUiIFrameDoc.getElementById("txtOwnerPhoneAC").value + loUiIFrameDoc.getElementById("txtOwnerPhoneEN").value + loUiIFrameDoc.getElementById("txtOwnerPhoneUN").value;
	var sOwnerNightPhone = loUiIFrameDoc.getElementById("txtOwnerNightPhoneAC").value + loUiIFrameDoc.getElementById("txtOwnerNightPhoneEN").value + loUiIFrameDoc.getElementById("txtOwnerNightPhoneUN").value;
	var sOwnerAltPhone = loUiIFrameDoc.getElementById("txtOwnerAltPhoneAC").value + loUiIFrameDoc.getElementById("txtOwnerAltPhoneEN").value + loUiIFrameDoc.getElementById("txtOwnerAltPhoneUN").value;

	if ((loUiIFrameDoc.getElementById("txtCallerNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtCallerNameLast").value.length == 0))
		lsReq +=  "    " + "Claim Assigner Fisrt and Last name" + "\n";
	if (loUiIFrameDoc.getElementById("txtCallerRelationToInsuredID").value.length == 0)
		lsReq +=  "    " + "Assigner Relation to Insured" + "\n";
	if (loUiIFrameDoc.getElementById("txtCoverageClaimNumber").value.length == 0)
		lsReq +=  "    " + "Claim Number" + "\n";
	if (loUiIFrameDoc.getElementById("txtLossDate").value.length == 0)
		lsReq +=  "    " + "Loss Date" + "\n";
	if (loUiIFrameDoc.getElementById("txtLossAddressState").value.length == 0)
		lsReq +=  "    " + "Loss State" + "\n";
	if ((loUiIFrameDoc.getElementById("txtInsuredNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtInsuredNameLast").value.length == 0) && loUiIFrameDoc.getElementById("txtInsuredBusinessName").value.length == 0)
		lsReq +=  "    " + "Insured Fisrt and Last name or Business name" + "\n";
  if (loUiIFrameDoc.getElementById("txtAssignmentTypeID").value.length == 0)
  	lsReq +=  "    " + "IA Assignment Type" + "\n";
  if (loUiIFrameDoc.getElementById("txtExposureCD").value.length == 0)
  	lsReq +=  "    " + "Party" + "\n";
  if (loUiIFrameDoc.getElementById("txtCoverageProfileUiCD").value.length == 0)
  	lsReq +=  "    " + "Coverage" + "\n";
  if ((loUiIFrameDoc.getElementById("txtOwnerNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtOwnerNameLast").value.length == 0) && loUiIFrameDoc.getElementById("txtOwnerBusinessName").value.length == 0)
  	lsReq +=  "    " + "Owner Fisrt and Last name or Business name" + "\n";
  if (sOwnerPhone.length == 0 && sOwnerNightPhone.length == 0 && sOwnerAltPhone.length == 0)
    lsReq +=  "    " + "Owner Phone Number" + "\n";

	if (lsReq.length > 0)
	{
		lsMsg += "* The following fields are required:\n";
		lsMsg += lsReq;
	}

  if (sOwnerPhone.length > 0)
  {
  	if (isNumeric(sOwnerPhone) == false) //this will check for numeric only
  		lsNumOnly += "* Phone number must be a numeric value.\n";
  	else if (loUiIFrameDoc.getElementById("txtOwnerPhoneAC").value.length != 3 || loUiIFrameDoc.getElementById("txtOwnerPhoneEN").value.length != 3 || loUiIFrameDoc.getElementById("txtOwnerPhoneUN").value.length != 4) //this will check the format
  	{
  		lsMsg += "* The Owner Day Phone Number is invalid.\n";
  		lsMsg += "    Phone number must be formated as 999 999 9999.\n";
  	}
  }
  
  if (sOwnerNightPhone.length > 0)
  {
  	if (isNumeric(sOwnerNightPhone) == false) //this will check for numeric only
  		lsNumOnly += "* Phone number must be a numeric value.\n";
  	else if (loUiIFrameDoc.getElementById("txtOwnerNightPhoneAC").value.length != 3 || loUiIFrameDoc.getElementById("txtOwnerNightPhoneEN").value.length != 3 || loUiIFrameDoc.getElementById("txtOwnerNightPhoneUN").value.length != 4) //this will check the format
  	{
  		lsMsg += "* The Owner Night Phone Number is invalid.\n";
  		lsMsg += "    Phone number must be formated as 999 999 9999.\n";
  	}
  }
  
  if (sOwnerAltPhone.length > 0)
  {
  	if (isNumeric(sOwnerAltPhone) == false) //this will check for numeric only
  		lsNumOnly += "* Phone number must be a numeric value.\n";
  	else if (loUiIFrameDoc.getElementById("txtOwnerAltPhoneAC").value.length != 3 || loUiIFrameDoc.getElementById("txtOwnerAltPhoneEN").value.length != 3 || loUiIFrameDoc.getElementById("txtOwnerAltPhoneUN").value.length != 4) //this will check the format
  	{
  		lsMsg += "* The Owner Cell/Alt. Phone Number is invalid.\n";
  		lsMsg += "    Phone number must be formated as 999 999 9999.\n";
  	}
  }

	var sValue = loUiIFrameDoc.getElementById("txtLossDate").value;
	if (sValue.length > 0)
	{
		var lsRe = /^(\d{2})\/(\d{2})\/(\d{4})/;
		var lsRet = sValue.match(lsRe);

		if (lsRet == null)
			lsMsg += "* The Loss Date must be formated as MM/DD/YYYY.\n";
		else if (sValue.substr(0,2) > 12)
			lsMsg += "* The Loss Date month is incorrect.\n";
		else if (sValue.substr(3,2) > 31)
			lsMsg += "* The Loss Date day is incorrect.\n";
		else if (sValue.substr(6,4) < 2000)
			lsMsg += "* The Loss Date year is incorrect.\n";
		else
		{
			var lsMyDate = new Date(loUiIFrameDoc.getElementById("txtLossDate").value);
		  var liDateDiff = Math.round((new Date() - lsMyDate) / 24 / 60 / 60 / 1000);
			if (liDateDiff < 0)
				lsMsg += "* The Loss Date cannot be in the future.\n";
		}
	}
  
	var liTxtAreaLength_1 = loUiIFrameDoc.getElementById("txtRemarksToIA").value.length;
	if (liTxtAreaLength_1 > 500)
	{
		lsMsg += "* Only 500 characters are allowed for Special Handling Instructions for Independant Appraiser;\n";
		lsMsg += "    You have typed "+ liTxtAreaLength_1 +" characters.\n";
	}

	var liTxtAreaLength_2 = loUiIFrameDoc.getElementById("txtDescLossDamage").value.length;
	if (liTxtAreaLength_2 > 500)
	{
		lsMsg += "* Only 500 characters are allowed for Description of Loss/Damages;\n";
		lsMsg += "    You have typed "+ liTxtAreaLength_2 +" characters.\n";
	}

	if (loUiIFrameDoc.getElementById("txtVehicleYear").value.length > 0 && loUiIFrameDoc.getElementById("txtVehicleYear").value.length < 4)
	{
		lsMsg += "* The Vehicle Year is invalid.\n";
		lsMsg += "    Year must be formated as 9999.\n";
	}
  
	if (loUiIFrameDoc.getElementById("txtVIN").value.length > 0 && loUiIFrameDoc.getElementById("txtVIN").value.length < 6)
	{
		lsMsg += "* The Vehicle VIN is invalid.\n";
		lsMsg += "    At least the last 6 digits of the VIN is required.\n";
	}

	if (lsMsg.length > 0) // display errors if any
		alert(lsMsg);
	else // otherwise copy data to dataContainerFrame
	{
		if(parent)
		{
			var lsElmId;
			var loDcDataElm;
			var loDcIFrameDoc = getdcIFrameDoc();

			// update inputs
      if (loUiIFrameDoc.getElementById('txtExposureCD').value == "1")
        clrDedLimAmt(); //clear all Deductibles and Limits except Liability

      var loSelCov = loUiIFrameDoc.getElementById('txtCoverageProfileUiCD').value;
      var lsCovSelected, lsCovSelected2;
      if (loSelCov != "") {
        loSelCov = loSelCov.split("|")[0];
        var loSelCov2 = loUiIFrameDoc.getElementById('txtCoverageProfileUiCD');
        lsCovSelected = loSelCov2.options[loSelCov2.selectedIndex].text;
        switch (loSelCov) {
          case "COLL":
            lsCovSelected2 = "Collision";
            break;
          case "COMP":
            lsCovSelected2 = "Comprehensive";
            break;
          case "UIM":
            lsCovSelected2 = "UnderInsured";
            break;
          case "UM":
            lsCovSelected2 = "UnInsured";
            break;
          case "LIAB":
            lsCovSelected2 = "Liability";
            break;
          case "":
            lsCovSelected2 = "";
            break;
        }
      } else
        lsCovSelected2 = "";

			var lcolUiElms = loUiIFrameDoc.getElementsByTagName("INPUT");
		  var liUiElmsLength = lcolUiElms.length;
			for(var i=0; i<liUiElmsLength; i++)
			{
				if ((lcolUiElms[i].type == "text" || lcolUiElms[i].type == "hidden") && lcolUiElms[i].getAttribute("opt") != "yes")
        {
          lsElmId = lcolUiElms[i].id;

  				//loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
  				//loDcDataElm.value = lcolUiElms[i].value;

          if (lsElmId.indexOf('Amt') != -1 && lsCovSelected2 != "")
            loDcIFrameDoc.getElementById("txt"+lsCovSelected2+lcolUiElms[i].name).value = lcolUiElms[i].value;
          
          loDcIFrameDoc.getElementById(lsElmId).value = lcolUiElms[i].value;
        }
			}
			// update selects
			var lcolUiElmsSel = loUiIFrameDoc.getElementsByTagName("SELECT");
		  var liUiElmsSelLength = lcolUiElmsSel.length;
      var loSel;
			for(var z=0; z<liUiElmsSelLength; z++)
			{
        lsElmId = lcolUiElmsSel[z].id;
        if (lcolUiElmsSel[z].getAttribute("opt") != "yes")
        {
          loDcIFrameDoc.getElementById(lsElmId).value = lcolUiElmsSel[z].value;
          if (loDcIFrameDoc.getElementById(lsElmId+"Description"))
          {
            loSel = lcolUiElmsSel[z];
            loDcIFrameDoc.getElementById(lsElmId+"Description").value = loSel.options[loSel.selectedIndex].text;
          }
        }
			}

    	var lsOwnerBestPhoneCB = window.frames["uiFrame"].getOwnerBestPhone(); //get Owner Best Phone value
      loDcIFrameDoc.getElementById("txtOwnerBestPhoneCD").value = lsOwnerBestPhoneCB;

      loDcIFrameDoc.getElementById("txtRemarksToIA").value =  loUiIFrameDoc.getElementById("txtRemarksToIA").value;
      loDcIFrameDoc.getElementById("txtDescLossDamage").value =  loUiIFrameDoc.getElementById("txtDescLossDamage").value;

			window.frames["dataContIfr"].updClaimValues();

			dspMsg();

      if (lsPageName == "frmWaMainAddVehicle.asp")
        document.getElementById("uiFrame").src = "frmWaIASummaryAddVeh.asp?fromPage=IAAssignment&vehCount=0,0";
      else
        document.getElementById("uiFrame").src = "frmWaIAAssignmentSummary.asp?fromPage=IAAssignment&vehCount=0,0";
		}
		else
			return false;
	}
}

function dspMsg()
{
  var objIFrame = parent.document.getElementById('uiFrame');
  objIFrame.style.height = "0";
  parent.showMsg();
}

function clrDedLimAmt()
{
  var loDcIFrameDoc = getdcIFrameDoc();
  loDcIFrameDoc.getElementById("txtCollisionDeductibleAmt").value = "";
  loDcIFrameDoc.getElementById("txtCollisionLimitAmt").value = "";
  loDcIFrameDoc.getElementById("txtComprehensiveDeductibleAmt").value = "";
  loDcIFrameDoc.getElementById("txtComprehensiveLimitAmt").value = "";
  loDcIFrameDoc.getElementById("txtUnderInsuredDeductibleAmt").value = "";
  loDcIFrameDoc.getElementById("txtUnderInsuredLimitAmt").value = "";
  loDcIFrameDoc.getElementById("txtUnInsuredDeductibleAmt").value = "";
  loDcIFrameDoc.getElementById("txtUnInsuredLimitAmt").value = "";
}


function getClaimSummaryData()
{
	var loUiIFrameDoc = getuiIFrameDoc();
	var loDcIFrameDoc = getdcIFrameDoc();
	var lsElmId;
	var loDcDataElm;

	// update claim
	var loUiClaimTbl = loUiIFrameDoc.getElementById("IAAssignmentInfoTable");
	var lcolClaimElms = loUiClaimTbl.getElementsByTagName("INPUT");
	var liClaimElmsLength = lcolClaimElms.length;
	for(var i=0; i<liClaimElmsLength; i++)
	{
    lsElmId = lcolClaimElms[i].id;

		if (lsElmId == "txtExposureCD")
		{
			loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
			if (loDcDataElm.value == "1") lcolClaimElms[i].value = "1st Party";
			else if (loDcDataElm.value == "3") lcolClaimElms[i].value = "3rd Party";
		}
		else if (lsElmId == "txtCoverageProfileUiCD")
		{
      loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
			if (loDcDataElm.value == "COLL") lcolClaimElms[i].value = "Collision";
			else if (loDcDataElm.value == "COMP") lcolClaimElms[i].value = "Comprehensive";
			else if (loDcDataElm.value == "LIAB") lcolClaimElms[i].value = "Liability";
			else if (loDcDataElm.value == "UIM") lcolClaimElms[i].value = "Underinsured";
			else if (loDcDataElm.value == "UM") lcolClaimElms[i].value = "Uninsured";
		}
		else if (lsElmId == "txtOwnerBestPhoneCD")
		{
      loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
      if (loDcDataElm.value == "D") lcolClaimElms[i].value = "Day";
			else if (loDcDataElm.value == "N") lcolClaimElms[i].value = "Night";
			else if (loDcDataElm.value == "A") lcolClaimElms[i].value = "Cell/Alt.";
			else lcolClaimElms[i].value = "";
		}
    else
    {
      lcolClaimElms[i].value = loDcIFrameDoc.getElementById(lsElmId).value;
    }
	}
	loUiIFrameDoc.getElementById("txtCallerRelationToInsuredIDDescription").value = loDcIFrameDoc.getElementById("txtCallerRelationToInsuredIDDescription").value;
	loUiIFrameDoc.getElementById("txtRemarksToIA").value = loDcIFrameDoc.getElementById("txtRemarksToIA").value;
	loUiIFrameDoc.getElementById("txtDescLossDamage").value = loDcIFrameDoc.getElementById("txtDescLossDamage").value;

}

function getClaimInfoData()
{
	var loUiIFrameDoc = getuiIFrameDoc();
	var loDcIFrameDoc = getdcIFrameDoc();
	var lsElmId;
	var loDcDataElm;

	// update inputs
  var dc_OwnerBestPhoneCD = loDcIFrameDoc.getElementById("txtOwnerBestPhoneCD").value;

	var lcolUiElms = loUiIFrameDoc.getElementsByTagName("INPUT");
	var liUiElmsLength = lcolUiElms.length;
	for(var i=0; i<liUiElmsLength; i++)
	{
    if (lcolUiElms[i].type == "text" || lcolUiElms[i].type == "hidden" && lcolUiElms[i].id.indexOf('Amt') != -1)
    {
  		lsElmId = lcolUiElms[i].id;
      lcolUiElms[i].value = loDcIFrameDoc.getElementById(lsElmId).value;
    }
		else if (lcolUiElms[i].type == "checkbox")
		{
			if (lcolUiElms[i].name == "cb_OwnerBestPhoneCD" && lcolUiElms[i].value == dc_OwnerBestPhoneCD)
			{
				lcolUiElms[i].checked = true;
			}
		}
	}

	// update selects
	var lcolUiElmsSel = loUiIFrameDoc.getElementsByTagName("SELECT");
	var liUiElmsSelLength = lcolUiElmsSel.length;
	for(var z=0; z<liUiElmsSelLength; z++)
	{
		lsElmId = lcolUiElmsSel[z].id;
    lcolUiElmsSel[z].value = loDcIFrameDoc.getElementById(lsElmId).value;
	}

	// update textarea
	var lcolUiElmsTxtArea = loUiIFrameDoc.getElementsByTagName("TEXTAREA");
	var liUiElmsTxtAreaLength = lcolUiElmsTxtArea.length;
	for(var z=0; z<liUiElmsTxtAreaLength; z++)
	{
		lsElmId = lcolUiElmsTxtArea[z].id;
    lcolUiElmsTxtArea[z].value = loDcIFrameDoc.getElementById(lsElmId).value;
	}
}

function chkClaimSubmitted()
{
	var loDcIFrameDoc = getdcIFrameDoc();
  var lsWasSubmitted = "false";

  if (loDcIFrameDoc.getElementById("txtClaimSubmitted").value == "true")
    lsWasSubmitted = "true";
  
  return lsWasSubmitted;
}


function chkReqParams()
{
  var lbReqParams = true;
  var lsReqFields1 = "";
  var lsReqFields2 = "";
  var lsReqFields3 = "";
  var loDcIFrameDoc = getdcIFrameDoc();

  var lsFNOLUserID = loDcIFrameDoc.getElementById("txtFNOLUserID").value;

  if (lsFNOLUserID == 0 || lsFNOLUserID == "")
  {
    lbReqParams = false;
    lsReqFields1 = "User ID";
  }

	var loVehTbl = loDcIFrameDoc.getElementById("tblVehicle");
  var lcolElms = loVehTbl.getElementsByTagName("INPUT");
	var liElmsLength = lcolElms.length;
	for(var y=0; y<liElmsLength; y++)
  {
    if (lcolElms[y].id.indexOf('txtAssignmentTypeID') != -1 && lcolElms[y].value == "")
    {
      lbReqParams = false;
      lsReqFields2 = "Assignment Type";
    }
    else if (lcolElms[y].id.indexOf('txtExposureCD') != -1 && lcolElms[y].value == "")
    {
      lbReqParams = false;
      lsReqFields3 = "Coverage";
    }
  }
  
  if (lbReqParams == false)
    return lsReqParams = "false" +"|"+ lsReqFields1 +"|"+ lsReqFields2 +"|"+ lsReqFields3;
  else
    return lsReqParams = "true";
}


function getXMLstr()
{
	var loDcIFrameDoc = getdcIFrameDoc();
  loDcIFrameDoc.getElementById("txtTimeFinished").value = getCurrentDT();
  var startTime = loDcIFrameDoc.getElementById("txtIntakeStartSeconds").value;
  var endTime = getCurrentTimeSec();
  var totalTime = parseInt((endTime - startTime) / 1000);
  loDcIFrameDoc.getElementById("txtIntakeEndSeconds").value = endTime;
  loDcIFrameDoc.getElementById("txtIntakeSeconds").value = totalTime;

  var lsXML = "<WebAssignment version='100'>";

	var loClaimTbl = loDcIFrameDoc.getElementById("tblClaim");
  if (loClaimTbl)
  {
    lsXML += "<Claim>";
    lsXML += "<NewClaimFlag>1</NewClaimFlag>";
    lsXML += GetXmlElements(loClaimTbl);
    lsXML += "</Claim>";
  }

  var lbFirstPartyExist = false;
  var liNextVehNum = 0;
  var laVeh;
  
  for (var x=0; x<laVehicleList.length; x++) {
    laVeh = laVehicleList[x].split("|");
    if (laVeh[0] == "1") {
      lbFirstPartyExist = true;
    }
  }

	var liNextVehNum = parseInt(laVehicleList[laVehicleList.length - 1].split("|")[0], 10) + 1;

	var loVehTbl = loDcIFrameDoc.getElementById("tblVehicle");
  if (loVehTbl)
  {
    if (loDcIFrameDoc.getElementById("txtExposureCD").value == "1")
      liNextVehNum = 1;

    lsXML += "<Vehicle ProgramShop='0'>";
    lsXML += "<VehicleNumber>" + liNextVehNum + "</VehicleNumber>";
    lsXML += GetXmlElements(loVehTbl);
    lsXML += "</Vehicle>";
  }
  lsXML += "</WebAssignment>";
  return lsXML;
}

function getNewVehicleXMLstr()
{
	var loDcIFrameDoc = getdcIFrameDoc();
  loDcIFrameDoc.getElementById("txtTimeFinished").value = getCurrentDT();
  var startTime = loDcIFrameDoc.getElementById("txtIntakeStartSeconds").value;
  var endTime = getCurrentTimeSec();
  var totalTime = parseInt((endTime - startTime) / 1000);
  loDcIFrameDoc.getElementById("txtIntakeEndSeconds").value = endTime;
  loDcIFrameDoc.getElementById("txtIntakeSeconds").value = totalTime;
  var lsCarrierRepUserID = loDcIFrameDoc.getElementById("txtCarrierRepUserID").value;
  var lsInsuranceCompanyID = loDcIFrameDoc.getElementById("txtInsuranceCompanyID").value;

  var lsXML = "<WebAssignment version='100'>";

	var loClaimTbl = loDcIFrameDoc.getElementById("tblClaim");
  if (loClaimTbl)
  {
    lsXML += "<Claim>";
    lsXML += "<LynxID>" + liLynxID + "</LynxID>";
    lsXML += "<ClientClaimNumber><![CDATA[" + lsClaimNumber + "]]></ClientClaimNumber>";
    lsXML += "<CarrierRepUserID>" + lsCarrierRepUserID + "</CarrierRepUserID>";
    lsXML += "<DataSource>Claim Point Web Assignment</DataSource>";
    lsXML += "<FNOLUserID>" + liUserID + "</FNOLUserID>";
    lsXML += "<InsuranceCompanyID>" + lsInsuranceCompanyID + "</InsuranceCompanyID>";
    lsXML += "<NewClaimFlag>0</NewClaimFlag>";

    
    lsXML += "<CoverageClaimNumber><![CDATA[" + lsClaimNumber + "]]></CoverageClaimNumber>";
    lsXML += "<LossDate><![CDATA[" + lsLossDate + "]]></LossDate>";
    lsXML += "<LossAddressState><![CDATA[" + lsLossState + "]]></LossAddressState>";
    lsXML += "<CallerNameFirst><![CDATA[" + lsCallerFName + "]]></CallerNameFirst>";
    lsXML += "<CallerNameLast><![CDATA[" + lsCallerLName + "]]></CallerNameLast>";
    lsXML += "<CallerRelationToInsuredIDDescription><![CDATA[" + lsCallerRelation + "]]></CallerRelationToInsuredIDDescription>";
    lsXML += "<InsuredNameFirst><![CDATA[" + lsInsuredFName + "]]></InsuredNameFirst>";
    lsXML += "<InsuredNameLast><![CDATA[" + lsInsuredLName + "]]></InsuredNameLast>";
    lsXML += "<InsuredBusinessName><![CDATA[" + lsInsuredBName + "]]></InsuredBusinessName>";
    lsXML += "<InsuredPhoneSumm><![CDATA[" + lsInsuredPhoneArea + "-" + lsInsuredPhoneEx + "-" + lsInsuredPhoneNum + "]]></InsuredPhoneSumm>";
    lsXML += "<TimeFinished>" + getCurrentDT() + "</TimeFinished>"
    lsXML += "<CarrierName><![CDATA[" + lsCarrierName + "]]></CarrierName>"
    lsXML += "<CarrierOfficeName><![CDATA[" + lsCarrierOfficeName + "]]></CarrierOfficeName>"
    lsXML += "<CarrierRepNameFirst><![CDATA[" + lsCarrierRepNameFirst + "]]></CarrierRepNameFirst>"
    lsXML += "<CarrierRepNameLast><![CDATA[" + lsCarrierRepNameLast + "]]></CarrierRepNameLast>"
    lsXML += "<CarrierRepPhoneDay><![CDATA[" + lsCarrierRepPhoneDay + "]]></CarrierRepPhoneDay>"
    lsXML += "<CarrierRepEmailAddress><![CDATA[" + lsCarrierRepEmailAddress + "]]></CarrierRepEmailAddress>"
    lsXML += "<AssignmentDescription><![CDATA[" + lsAssignmentDesc + "]]></AssignmentDescription>"
    lsXML += "<AssignmentAtSelectionFlag>" + lsAssignmentAtSelectionFlag + "</AssignmentAtSelectionFlag>";
    lsXML += "</Claim>";
  }

  var lbFirstPartyExist = false;
  var liNextVehNum = 0;
  var laVeh;
  
  for (var x=0; x<laVehicleList.length; x++) {
    laVeh = laVehicleList[x].split("|");
    if (laVeh[0] == "1") {
      lbFirstPartyExist = true;
    }
  }

	var liNextVehNum = parseInt(laVehicleList[laVehicleList.length - 1].split("|")[0], 10) + 1;
  var loVehTbl = loDcIFrameDoc.getElementById("tblVehicle");
  if (loVehTbl)
  {
    if (loDcIFrameDoc.getElementById("txtExposureCD").value == "1")
      liNextVehNum = 1;
      
    lsXML += "<Vehicle ProgramShop='0'>";
    lsXML += "<VehicleNumber>" + liNextVehNum + "</VehicleNumber>";
    lsXML += GetXmlElements(loVehTbl);
    lsXML += "</Vehicle>";
  }
  lsXML += "</WebAssignment>";
  return lsXML;
}

function GetXmlElements(oName)
{
  var lsElms = ""
  var lcolElms = oName.getElementsByTagName("INPUT");
	var liElmsLength = lcolElms.length;
	for(var y=0; y<liElmsLength; y++)
	{
		if (lcolElms[y].getAttribute("opt") != "yes")
      lsElms += "<" + lcolElms[y].name + ">" + "<![CDATA[" + lcolElms[y].value + "]]>" + "</"+ lcolElms[y].name + ">";
      //lsElms += "<" + lcolElms[y].name + ">" + "<![CDATA[" + escape(lcolElms[y].value) + "]]>" + "</"+ lcolElms[y].name + ">";
  }

  var lcolElmsTxt = oName.getElementsByTagName("TEXTAREA");
	var liElmsTxtLength = lcolElmsTxt.length;
	for(var z=0; z<liElmsTxtLength; z++)
	{
		if (lcolElmsTxt[z].getAttribute("opt") != "yes")
      lsElms += "<" + lcolElmsTxt[z].name + ">" + "<![CDATA[" + lcolElmsTxt[z].value + "]]>" + "</"+ lcolElmsTxt[z].name + ">";
      //lsElms += "<" + lcolElmsTxt[z].name + ">" + "<![CDATA[" + escape(lcolElmsTxt[z].value) + "]]>" + "</"+ lcolElmsTxt[z].name + ">";
  }
  return lsElms;
}


function getSubmitDate()
{
	var loUiIFrameDoc = getuiIFrameDoc();
	var loDcIFrameDoc = getdcIFrameDoc();
  var lsDate = loDcIFrameDoc.getElementById("txtTimeFinished").value;
  return getDisplayDate(lsDate);
}


function markClaimSubmitted(flag)
{
	var loDcIFrameDoc = getdcIFrameDoc();
  loDcIFrameDoc.getElementById("txtClaimSubmitted").value = flag;
}

