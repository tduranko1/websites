<!-- v1.4.2.0 -->

<HTML>
<HEAD>
<TITLE>

<%
  If lsPageName = "frmLogin.asp" Then
    Response.Write "ClaimPoint from LYNX Services"
  Else
    Response.Write "ClaimPoint - " & lsPageTitle
  End If
%>

</TITLE>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script type="text/javascript" src="includes/incUtils.js"></script>

<%if lsPageName = "frmMyClaimsDesktop.asp" then %>
  <script type="text/javascript" src="includes/incMM_Functions.js"></script>
<%end if%>

<% If bGridSort = True Then %>
  <script type="text/javascript" src="includes/incGridSort.js"></script>
  <link href="includes/incGridSort.css" type="text/css" rel="stylesheet">
<% End If %>

<% If bGridHover = True Then %>
  <script type="text/javascript" src="includes/incGridHover.js"></script>
<% End If %>

<script language="JavaScript" for="window">

<%if lsPageName = "frmWaMain.asp" then%>
  gLogoutTimer = window.setTimeout("top.location = 'frmLogout.asp'", 240*60*1000);
<%else%>
  window.setTimeout("document.location = 'frmLogout.asp'", 480*60*1000);
<%end if%>

</script>

</HEAD>
<BODY LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0
  <%if not bModal then %>
    background="images/page_bg.jpg"
  <%end if%>
  <%if lsPageName = "frmMyClaimsDesktop.asp" then %>
    onLoad="MM_preloadImages('images/btn_SC_1_o.gif','images/btn_SC_2_o.gif','images/btn_SC_3_o.gif','images/btn_SC_4_o.gif','images/btn_SC_5_o.gif','images/btn_SC_6_o.gif','images/btn_SC_7_o.gif','images/btn_SC_8_o.gif');"
  <%elseif lsPageName = "frmDocument.asp" then %>
    onClick="checkMyModal()" onFocus="return checkMyModal()"
  <%elseif lsPageName = "frmWaMain.asp" then%>
    onbeforeunload="chkBeforeUnloadWindow()"
  <%end if%>
>

<div id="outer" align="left">

  <table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; width:100%; height:100%;">
    <tr>
      <td width="169" align="left" valign="top" style="height:100%">

        <!--#include file="incMenu.asp"-->

      </td>
      <td align="left" valign="top" style="background:url(images/header_bg.jpg); background-repeat:repeat-x;">
