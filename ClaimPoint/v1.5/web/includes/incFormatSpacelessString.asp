<%@ Language=VBScript %>
<HTML>
<HEAD>
<script language="JavaScript">
function formatSpacelessString (psString, piChars)
{
	var psSpacedString = "";
	
	if (psString.length > piChars && (psString.search(' ') > piChars || psString.search(' ') == -1))
	{
		while (psString.search(' ') > piChars || (psString.search(' ') == -1 && psString.length > piChars))
		{
			psSpacedString = psSpacedString + psString.substr(0, piChars) + " ";
			psString = psString.substr(piChars, psString.length - piChars);	
		}
		
		psSpacedString = psSpacedString + psString;
	}else{
		psSpacedString = psString;
	}
	
	return psSpacedString;
}
</script>
</HEAD>
<BODY>

<P>&nbsp;</P>
<form name=frmForm>
<input type=text name=txtText>
<input type=button onclick="formatSpacelessString(document.frmForm.txtText.value, 26);">
</form>

</BODY>
</HTML>
