  gsVehCount = "";
  gsStep = 0;
  var Insurance = {"id":"", "Name":""};
  var CarrierRep = {"FirstName":"","LastName":"","Phone":"", "Email":"","Office":"", "id":"", "InsuranceName":""};
  var Caller = {"FirstName":"","LastName":""};
  var dataXML = null;
  var strStartTime = getCurrentDT();
  var strIntakeStartSeconds = getCurrentTimeSec();
  
  var stepsValidated = [0,0,0,0];
  var strSteps = ["Basic Claim", "Insured", "Vehicle", "Settlement"];
  var blnSubmitting = false;
  var blnReady = false;
  var strData = "";
  var strStepData = new Array();
  var strCurEditStepInputs = null;

  function pageInit()
  {
    try {
    if(parent)
      parent.resetTimeOut();
    } catch(e) {}

    $("#txtInsuredAddressZip").bind("blur", getZipInfo);
    $("#txtOwnerAddressZip").bind("blur", getZipInfo);
    $("#txtLHAddressZip").bind("blur", getZipInfo);
    $("#txtSalvageAddressZip").bind("blur", getZipInfo);

    $("#txtLossDate").mask("99/99/9999");
    $("#txtInsuredAddressZip").mask("99999");
    $("#txtOwnerAddressZip").mask("99999");
    $("#txtOwnerPhoneDay").mask("(999) 999-9999");
    $("#txtOwnerPhoneAlt").mask("(999) 999-9999");
    $("#txtVehicleYear").mask("9999");
    $("#txtSettlementDate").mask("99/99/9999");
    $("#txtLHAddressZip").mask("99999");
    $("#txtLHPhone").mask("(999) 999-9999");
    $("#txtLHFax").mask("(999) 999-9999");
    $("#txtLHPayoffExpDate").mask("99/99/9999");
    $("#txtSalvageAddressZip").mask("99999");
    $("#txtSalvagePhone").mask("(999) 999-9999");
    $("#txtSalvageFax").mask("(999) 999-9999");
    $("#txtTitleNames").tooltip({ position: "center right", offset: [-2, 10], effect: "fade", opacity: 0.7 });
  }
  
  function getZipInfo(e){
      var strSrc = event.srcElement.id;
      var strEntity = "";
      if (strSrc.indexOf("Insured") != -1) strEntity = "Insured";
      if (strSrc.indexOf("Owner") != -1) strEntity = "Owner";
      if (strSrc.indexOf("LH") != -1) strEntity = "LH";
      if (strSrc.indexOf("Salvage") != -1) strEntity = "Salvage";
      var strZip = $("#txt" + strEntity + "AddressZip").val();
      if ( strZip != "" && strZip.indexOf("_") == -1){
         $.get("ziplookup.asp?z=" + $("#txt" + strEntity + "AddressZip").val(), function(data){
            if (data != "" && data != "||"){
               zipResolve(data, strEntity);
            } else {
               alert("Zip Code could not be resolved. Please type a valid zip code.");
            }
         });
      }
  }

   function zipResolve(data, strEntity){
      if (data != "" && data != "||"){
         lsCityData = data.split("|");
         $("#txt" + strEntity + "AddressCity").val(lsCityData[0]); 
         $("#sel" + strEntity + "AddressState").val(lsCityData[1]); 
      }
   }
  
  
  function showStep(intStep){
      if (blnSubmitting == true) return;
      if (intStep >= 0 && intStep < 6){

         intHeight = 200;

         try {
             divBasic.style.display = "none";
             divInsured.style.display = "none";
             divVehicle.style.display = "none";
             divSettlement.style.display = "none";
             divSummary.style.display = "none";
             divSubmit.style.display = "none";
         } catch (e) { }
         
         tdBasicStep.className = "breadcrumb_item";
         tdInsuredStep.className = "breadcrumb_item";
         tdVehicleStep.className = "breadcrumb_item";
         tdSettlementStep.className = "breadcrumb_item";
         tdSummaryStep.className = "breadcrumb_item";
         tdSubmitStep.className = "breadcrumb_item";
         
         imgCancel.className = "visible";
         imgBack.className = "visible";
         imgNext.className = "visible";
         imgSubmit.className = "hidden";
         
         switch(intStep){
            case 0:
               divBasic.style.display = "inline";
               tdBasicStep.className = "breadcrumb_selected";
               selAssignedBy.focus();
               imgBack.className = "hidden";
               intHeight += divBasic.offsetHeight;
               break;
            case 1:
               divInsured.style.display = "inline";
               tdInsuredStep.className = "breadcrumb_selected";
               txtInsuredFirstName.focus();
               intHeight += divInsured.offsetHeight;
               break;
            case 2:
               divVehicle.style.display = "inline";
               tdVehicleStep.className = "breadcrumb_selected";
               if (selPartyCD.value != "1") {
                  txtOwnerFirstName.focus();
               } else {
                  txtOwnerPhoneDay.focus();
               }
               intHeight += divVehicle.offsetHeight;
               break;
            case 3:
               divSettlement.style.display = "inline";
               tdSettlementStep.className = "breadcrumb_selected";
               txtSettlementDate.focus();
               intHeight += divSettlement.offsetHeight;
               break;
            case 4:
               divSummary.style.display = "inline";
               tdSummaryStep.className = "breadcrumb_selected";
               imgNext.className = "hidden";
               imgSubmit.className = "visible";
               intHeight += divSummary.offsetHeight;
               refreshSummary();
               break;
            case 5:
               divSubmit.style.display = "inline";
               tdSubmitStep.className = "breadcrumb_selected";
               imgCancel.className = "hidden";
               imgBack.className = "hidden";
               imgNext.className = "hidden";
               imgSubmit.className = "hidden";
               intHeight += divSubmit.offsetHeight;
               break;
         }
         var objImg = document.getElementById("validation_step_" + intStep);
         if (objImg){
            objImg.src = "images/spacer.gif";
            objImg.title = "";
         }
         
         try {
            if (parent.outer){
               parent.outer.scrollIntoView(true);
            }
         } catch (e) {}
         setFrameHieght(intHeight);
      }
  }

  function setFrameHieght(intHeight) {
      var objIFrame = parent.document.getElementById('uiFrame');
      if (objIFrame) {
          objIFrame.style.height = intHeight; // +  60 + (530 * liVehTotal) + (280 * liSpTotal); // resize iframe according to the height of its contents
      }
  }
  
  function prevStep(){
      if (gsStep > 0) {
         gsStep--;
         showStep(gsStep);
      }
  }
  
  function nextStep(){
      if (gsStep < 5) {
         if (validateStep()){
            gsStep++;
            showStep(gsStep);
         }
      }
  }
  
  function gotoStep(intStep){
      if (blnSubmitting == true) return;
      if (intStep != gsStep && intStep > gsStep) {
         if (validateStep() == false) return;
      }
      gsStep = intStep;
      showStep(intStep);
  }
  
  function editStep(intStep){
      var strStepInputs = "";
      switch (intStep){
         case 0:
            strStepInputs = "#divBasic :input";
            break;
         case 1:
            strStepInputs = "#divInsured :input";
            break;
         case 2:
            strStepInputs = "#divVehicle :input";
            break;
         case 3:
            strStepInputs = "#divSettlement :input";
            break;
      }
      
      strCurEditStepInputs = $(strStepInputs);
      saveStepData(strCurEditStepInputs);
      gotoStep(intStep);
      
      imgCancel.className = "hidden";
      imgBack.className = "hidden";
      imgNext.className = "hidden";
      imgSubmit.className = "hidden";
      imgEditCancel.className = "visible";
      imgEditOK.className = "visible";
  }
  
  function editSave(){
      if (validateStep()){
         imgEditCancel.className = "hidden";
         imgEditOK.className = "hidden";
         showStep(4);
      }
  }
  
  function editCancel(){
      imgEditCancel.className = "hidden";
      imgEditOK.className = "hidden";
      revertStepData();
      showStep(4);
  }
  
  function validateStep(){
      /*if (gsStep < 4) {
         document.getElementById("validation_step_" + gsStep).src = "images/tick_green.gif";
      }*/
      
      switch (gsStep){
         case 0:
            return validateBasic();
            break;
         case 1:
            return validateInsured();
            break;
         case 2:
            return validateVehicle();
            break;
         case 3:
            return validateSettlement();
            break;
      }
      return true;
  }
  
  function refreshSummary(){
  }
  
  function submitClaim(){
      for (var i = 0; i < 4; i++){
         if (stepsValidated[i] != 1){
            var strMessage = "";
            if (stepsValidated[i] == 0) strMessage = strSteps[i] + " information needs to be filled out.";
            if (stepsValidated[i] == -1) strMessage = "Missing/Invalid data in " + strSteps[i] + " tab.";
            
            alert(strMessage);
            gotoStep(i);
            return;
         }
      }
      showStep(5);
      divSubmitFailure.className = "hidden";
      divSubmitSuccess.className = "hidden";
      divShowWADocument.className = "hidden";
      divShowWADocumentError.className = "hidden";
      blnSubmitting = true;
      showMessage("Submitting claim. Please wait...");
      var strXML = getAssignmentXML();
      //window.clipboardData.setData("Text", strXML);
      $("#txtSubmitWA").text(strXML);
      $("#lstrXML").val($("#txtSubmitWA").text());
      var strData = $("#frmAssignment").serialize();
      setFrameHieght(925);
      $.ajax({
          type: "POST",
          url: "rspsubmitwa.asp",
          data: strData,
          error: function(XMLHttpRequest, textStatus, errorThrown) {
              alert("An error occured while submitting the assignment.");
              //alert(XMLHttpRequest.responseText);
              hideMessage();
          },
          success: function(xml) {
              var strLynxID = "";
              var strDocument = "";
              var blnError = false;
              $(xml).find("Error").each(function() {
                  blnError = true;
              });
              $(xml).find("Root").each(function() {
                  strLynxID = $(this).attr("LynxID");
                  strDocument = $(this).attr("WebAssignmentDocument");
                  $("#txtLynxID").text(strLynxID);
              });
              if (strLynxID != "" && blnError == false) {
                  divSubmitSuccess.className = "visible";
                  $("#txtSubmitDate").text(getCurrentDT());
                  var strVehList = "";
                  $(xml).find("Vehicle").each(function() {
                      strVehList += $(this).attr("VehicleNumber") + ","; //$("#txtLynxID").text($(this).attr("LynxID"));
                      strVehList += $(this).attr("VehicleYear") + " " + $(this).attr("Make") + " " + $(this).attr("Model") + ",";
                  });
                  if (strVehList != "") strVehList = strVehList.substr(0, strVehList.length - 1);
                  document.getElementById("uploadFrame").src = "frmWaFileUploadPrompt.asp?LynxID=" + strLynxID + "&ch=TL&VehList=" + strVehList;

                  if (strDocument != "") {
                      divShowWADocument.className = "visible";
                      document.getElementById("prtFrame").src = "frmDisplayDocument.asp?D=" + strDocument;
                  } else {
                      divShowWADocumentError.className = "visible";
                  }
              } else {
                  divSubmitFailure.className = "visible";
              }
              //$.modal.close();
              hideMessage();
          }
      }); 
  }
  
  function goCancel(){
    if (parent)
    {
        if (typeof(parent.document.body.onbeforeunload) == "function")
            parent.document.body.onbeforeunload = "";
        var lbConfirmCancel = window.confirm("Stop entering this assignment and return to My Claims Desktop?");
        if (lbConfirmCancel)
          top.location = "frmMyClaimsDesktop.asp";
    }
  }
  
  function validateBasic(blnSuppressDialog){
      ldTrim(txtClaimNumber);
      ldTrim(txtLossDate);
      
      if (selAssignedBy.selectedIndex == 0){
         return displayError("Select Claim Assigned by from the list.", gsStep, selAssignedBy, blnSuppressDialog);
      }

      if (dataXML !== null){
         $(dataXML).find("OfficeUser[userid|=" + selAssignedBy.value + "]").each(function(){
            Caller.FirstName = $(this).attr("namefirst");
            Caller.LastName = $(this).attr("namelast");
            Caller.id = $(this).attr("userid");
         });
      }
      
      if (selAssignorRelation.selectedIndex == 0){
         return displayError("Select Assigner relation to Insured from the list.", gsStep, selAssignorRelation, blnSuppressDialog);
      }

      if (txtClaimNumber.value == ""){
         return displayError("Enter a Claim Number", gsStep, txtClaimNumber, blnSuppressDialog);
      }

      if (txtLossDate.value == 0){
         return displayError("Enter a valid Loss Date", gsStep, txtLossDate, blnSuppressDialog);
      }

      if (selLossState.selectedIndex == 0){
         return displayError("Select a Loss State from the list.", gsStep, selLossState, blnSuppressDialog);
      }

      if (selPartyCD.selectedIndex == 0){
         return displayError("Select a Party from the list.", gsStep, selPartyCD, blnSuppressDialog);
      }

      stepsValidated[gsStep] = 1;
      document.getElementById("validation_step_" + gsStep).src = "images/tick_green.gif";

      setSummaryData($("#divBasic :input"));

      $("#divSummary_selAssignedBy").text($("#selAssignedBy").selectedTexts()[0]);
      $("#divSummary_selAssignorRelation").text($("#selAssignorRelation").selectedTexts()[0]);
      $("#divSummary_selPartyCD").text($("#selPartyCD").selectedTexts()[0]);
      $("#divSummary_selCoverage").text($("#selCoverage").selectedTexts()[0]);
      
      if (selPartyCD.value != "1"){
         $("#txtOwnerFirstName").val("");
         $("#txtOwnerLastName").val("");
         $("#txtOwnerBusinessName").val("");
         $("#txtOwnerAddress1").val("");
         $("#txtOwnerAddress2").val("");
         $("#txtOwnerAddressZip").val("");
         $("#txtOwnerAddressCity").val("");
         $("#selOwnerAddressState").val("");
         
         if (stepsValidated[2] == 1){
            stepsValidated[2] = -1;
            var objImg = document.getElementById("validation_step_2");
            if (objImg){
               objImg.src = "images/error.gif";
               objImg.title = "Vehicle Owner Information missing";
            }
            setSummaryData($("#divVehicle :input"));
         }
      } else {
         $("#txtOwnerFirstName").val($("#txtInsuredFirstName").val());
         $("#txtOwnerLastName").val($("#txtInsuredLastName").val());
         $("#txtOwnerBusinessName").val($("#txtInsuredBusinessName").val());
         $("#txtOwnerAddress1").val($("#txtInsuredAddress1").val());
         $("#txtOwnerAddress2").val($("#txtInsuredAddress2").val());
         $("#txtOwnerAddressZip").val($("#txtInsuredAddressZip").val());
         $("#txtOwnerAddressCity").val($("#txtInsuredAddressCity").val());
         $("#selOwnerAddressState").val($("#selInsuredAddressState").val());
      }
      
      return true;
  }
  
  function validateInsured(blnSuppressDialog){
      ldTrim(txtInsuredFirstName);
      ldTrim(txtInsuredLastName);
      ldTrim(txtInsuredBusinessName);
      ldTrim(txtInsuredAddress1);
      ldTrim(txtInsuredAddress2);
      ldTrim(txtInsuredAddressZip);
      ldTrim(txtInsuredAddressCity);

      if (txtInsuredFirstName.value == "" && txtInsuredLastName.value == "" && txtInsuredBusinessName.value == ""){
         return displayError("Enter Insured's First and Last Name or Business Name", gsStep, txtInsuredFirstName, blnSuppressDialog);
      }
      
      if (txtInsuredBusinessName.value == "") {
         if (txtInsuredFirstName.value != "" && txtInsuredLastName.value == "") {
            return displayError("Enter Insured's Last Name", gsStep, txtInsuredLastName, blnSuppressDialog);
         }
         
         if (txtInsuredFirstName.value == "" && txtInsuredLastName.value != "") {
            return displayError("Enter Insured's First Name", gsStep, txtInsuredFirstName, blnSuppressDialog);
         }
      }

      if (txtInsuredAddress1.value == ""){
         return displayError("Enter Insured's Address", gsStep, txtInsuredAddress1, blnSuppressDialog);
      }

      if (txtInsuredAddressZip.value == ""){
         return displayError("Enter Insured's Address Zip Code", gsStep, txtInsuredAddressZip, blnSuppressDialog);
      }

      if (txtInsuredAddressCity.value == ""){
         return displayError("Enter Insured's Address City", gsStep, txtInsuredAddressCity, blnSuppressDialog);
      }

      if (selInsuredAddressState.selectedIndex == 0 || selInsuredAddressState.value == ""){
         return displayError("Select Insured's Address State from the list.", gsStep, selInsuredAddressState, blnSuppressDialog);
      }

      stepsValidated[gsStep] = 1;
      document.getElementById("validation_step_" + gsStep).src = "images/tick_green.gif";

      setSummaryData($("#divInsured :input"));

      if (selPartyCD.value == "1"){
         $("#txtOwnerFirstName").val($("#txtInsuredFirstName").val());
         $("#txtOwnerLastName").val($("#txtInsuredLastName").val());
         $("#txtOwnerBusinessName").val($("#txtInsuredBusinessName").val());
         $("#txtOwnerAddress1").val($("#txtInsuredAddress1").val());
         $("#txtOwnerAddress2").val($("#txtInsuredAddress2").val());
         $("#txtOwnerAddressZip").val($("#txtInsuredAddressZip").val());
         $("#txtOwnerAddressCity").val($("#txtInsuredAddressCity").val());
         $("#selOwnerAddressState").val($("#selInsuredAddressState").val());
      }

      return true;
  }
  
  function validateVehicle(blnSuppressDialog){
      ldTrim(txtOwnerFirstName);
      ldTrim(txtOwnerLastName);
      ldTrim(txtOwnerBusinessName);
      ldTrim(txtOwnerAddress1);
      ldTrim(txtOwnerAddress2);
      ldTrim(txtOwnerAddressZip);
      ldTrim(txtOwnerAddressCity);
      ldTrim(txtOwnerPhoneDay);
      ldTrim(txtOwnerPhoneAlt);
      ldTrim(txtVehicleYear);
      ldTrim(txtVehicleMake);
      ldTrim(txtVehicleModel);
      ldTrim(txtVehicleVIN);
      ldTrim(txtLicensePlate);
      ldTrim(txtTitleNames);
      
      if (txtOwnerFirstName.value == "" && txtOwnerLastName.value == "" && txtOwnerBusinessName.value == ""){
         return displayError("Enter Owner's First and Last Name or Business Name", gsStep, txtOwnerFirstName, blnSuppressDialog);
      }

      if (txtOwnerAddress1.value == ""){
         return displayError("Enter Owner's Address", gsStep, txtOwnerAddress1, blnSuppressDialog);
      }

      if (txtOwnerAddressZip.value == ""){
         return displayError("Enter Owner's Address Zip Code", gsStep, txtOwnerAddressZip, blnSuppressDialog);
      }

      if (txtOwnerAddressCity.value == ""){
         return displayError("Enter Owner's Address City", gsStep, txtOwnerAddressCity, blnSuppressDialog);
      }

      if (selOwnerAddressState.selectedIndex == 0){
         return displayError("Enter Owner's Address State", gsStep, selOwnerAddressState, blnSuppressDialog);
      }
      
      if (txtOwnerPhoneDay.value == "" || txtOwnerPhoneDay.value.length < 13){
         return displayError("Enter Owner's Primary Phone Number", gsStep, txtOwnerPhoneDay, blnSuppressDialog);
      }

      if (txtVehicleYear.value == ""){
         return displayError("Enter Vehicle's Year information", gsStep, txtVehicleYear, blnSuppressDialog);
      }

      if (txtVehicleMake.value == ""){
         return displayError("Enter Vehicle's Make information", gsStep, txtVehicleMake, blnSuppressDialog);
      }

      if (txtVehicleModel.value == ""){
         return displayError("Enter Vehicle's Model information", gsStep, txtVehicleModel, blnSuppressDialog);
      }

      if (txtVehicleVIN.value == ""){
         return displayError("Enter Vehicle's VIN", gsStep, txtVehicleVIN, blnSuppressDialog);
      }

      if (txtLicensePlate.value == ""){
         return displayError("Enter Vehicle's License Plate Number", gsStep, txtLicensePlate, blnSuppressDialog);
      }

      if (selLicenseState.selectedIndex == 0){
         return displayError("Enter Vehicle's License Plate State", gsStep, selLicenseState, blnSuppressDialog);
      }

      if (txtTitleNames.value == ""){
         return displayError("Enter the Name(s) as it appears on the Vehicle's Title", gsStep, txtTitleNames, blnSuppressDialog);
      }

      if (selTitleState.selectedIndex == 0){
         return displayError("Select the State in which the Vehicle's Title is registered", gsStep, selTitleState, blnSuppressDialog);
      }

      stepsValidated[gsStep] = 1;
      document.getElementById("validation_step_" + gsStep).src = "images/tick_green.gif";

      setSummaryData($("#divVehicle :input"));
      
      $("#divSummary_selTitleStatus").text($("#selTitleStatus").selectedTexts()[0]);

      return true;
  }
  
  function validateSettlement(blnSuppressDialog){
      ldTrim(txtSettlementDate);
      ldTrim(txtSettlementAmount);
      ldTrim(txtAdvanceAmount);
      ldTrim(txtLoGAmount);
      ldTrim(txtLienHolderName);
      ldTrim(txtLHAddress1);
      ldTrim(txtLHAddress2);
      ldTrim(txtLHAddressZip);
      ldTrim(txtLHAddressCity);
      ldTrim(txtLHPhone);
      ldTrim(txtLHFax);
      ldTrim(txtLHEmailAddress);
      ldTrim(txtLHContactName);
      ldTrim(txtLHPayoffAmount);
      ldTrim(txtLHPayoffExpDate);
      ldTrim(txtLHAccountNumber);
      ldTrim(txtSalvageName);
      ldTrim(txtSalvageAddress1);
      ldTrim(txtSalvageAddress2);
      ldTrim(txtSalvageAddressZip);
      ldTrim(txtSalvageAddressCity);
      ldTrim(txtSalvagePhone);
      ldTrim(txtSalvageFax);
      ldTrim(txtSalvageEmailAddress);
      ldTrim(txtSalvageContactName);
      ldTrim(txtSalvageControlNumber);

      if (txtSettlementDate.value == ""){
         return displayError("Enter Settlement Date", gsStep, txtSettlementDate, blnSuppressDialog);
      }

      if (txtSettlementAmount.value == ""){
         return displayError("Enter Settlement Amount", gsStep, txtSettlementAmount, blnSuppressDialog);
      }

      if (txtAdvanceAmount.value == "") {
          return displayError("Enter Advance Amount or $0.00", gsStep, txtAdvanceAmount, blnSuppressDialog);
      }

      if (chkLienHolder.checked == true){
         if (txtLienHolderName.value == ""){
            return displayError("Enter the Lien Holder's Name", gsStep, txtLienHolderName, blnSuppressDialog);
         }
   
         if (txtLHAddress1.value == ""){
            return displayError("Enter Lien Holder's Address", gsStep, txtLHAddress1, blnSuppressDialog);
         }
   
         if (txtLHAddressZip.value == ""){
            return displayError("Enter Lien Holder's Address Zip", gsStep, txtLHAddressZip, blnSuppressDialog);
         }
   
         if (txtLHAddressCity.value == ""){
            return displayError("Enter Lien Holder's Address City", gsStep, txtLHAddressCity, blnSuppressDialog);
         }
   
         if (selLHAddressState.selectedIndex == 0){
            return displayError("Select Lien Holder's Address state", gsStep, selLHAddressState, blnSuppressDialog);
         }
   
         if (txtLHPhone.value == "" || txtLHPhone.valuelength < 13){
            return displayError("Enter Lien Holder's Phone number. Example: (888) 888 8888", gsStep, txtLHPhone, blnSuppressDialog);
         }
   
         if (txtLHFax.value != "" && txtLHFax.valuelength < 13){
            return displayError("Enter Lien Holder's Fax number. Example: (888) 888 8888", gsStep, txtLHFax, blnSuppressDialog);
         }
   
         if (txtLHEmailAddress.value != "" && isEmail(txtLHEmailAddress.value) == false){
            return displayError("Enter a valid Lien Holder's Email address. Example: john.doe@company.com", gsStep, txtLHEmailAddress, blnSuppressDialog);
         }
   
         if (txtLHContactName.value == ""){
            return displayError("Enter the Contact Name at the Lien Holder", gsStep, txtLHContactName, blnSuppressDialog);
         }
   
         if (txtLHPayoffAmount.value == ""){
            return displayError("Enter the Payoff amount", gsStep, txtLHPayoffAmount, blnSuppressDialog);
         }
   
         if (txtLHPayoffExpDate.value == ""){
            return displayError("Enter the Payoff expiration date. Example: mm/dd/yyyy", gsStep, txtLHPayoffExpDate, blnSuppressDialog);
         }

         if (txtLoGAmount.value == "") {
             return displayError("Enter the amount in Letter of Guarantee", gsStep, txtLoGAmount, blnSuppressDialog);
         }

         if (txtLHAccountNumber.value == "") {
            return displayError("Enter Lien Holder Account number", gsStep, txtLHAccountNumber, blnSuppressDialog);
         }
      }

      if (txtSalvageName.value == ""){
         return displayError("Enter the Salvage Vendor's Name", gsStep, txtSalvageName, blnSuppressDialog);
      }

      /*if (txtSalvageAddress1.value == ""){
         return displayError("Enter Salvage Vendor's Address", gsStep, txtSalvageAddress1, blnSuppressDialog);
      }

      if (txtSalvageAddressZip.value == ""){
         return displayError("Enter Salvage Vendor's Address Zip", gsStep, txtSalvageAddressZip, blnSuppressDialog);
      }

      if (txtSalvageAddressCity.value == ""){
         return displayError("Enter Salvage Vendor's Address City", gsStep, txtSalvageAddressCity, blnSuppressDialog);
      }

      if (selSalvageAddressState.selectedIndex == 0){
         return displayError("Select Salvage Vendor's Address state", gsStep, selSalvageAddressState, blnSuppressDialog);
      }

      if (txtSalvagePhone.value == "" || txtSalvagePhone.valuelength < 13){
         return displayError("Enter Salvage Vendor's Phone number. Example: (888) 888 8888", gsStep, txtSalvagePhone, blnSuppressDialog);
      }

      if (txtSalvageFax.value != "" && txtSalvageFax.valuelength < 13){
         return displayError("Enter Salvage Vendor's Fax number. Example: (888) 888 8888", gsStep, txtSalvageFax, blnSuppressDialog);
      }

      if (txtSalvageEmailAddress.value != "" && isEmail(txtSalvageEmailAddress.value) == false){
         return displayError("Enter a valid Salvage Vendor's Email address. Example: john.doe@company.com", gsStep, txtSalvageEmailAddress, blnSuppressDialog);
      }*/

      if (txtSalvageControlNumber.value == ""){
         return displayError("Enter Salvage Control number", gsStep, txtSalvageControlNumber, blnSuppressDialog);
      }

      stepsValidated[gsStep] = 1;
      document.getElementById("validation_step_" + gsStep).src = "images/tick_green.gif";

      setSummaryData($("#divSettlement :input"));

      return true;
  }
  
  function displayError(strMessage, intStep, obj, blnSuppressDialog){
      if (blnSuppressDialog == true){
      } else {
         alert(strMessage);
         if (intStep != gsStep){
            gotoStep(intStep);
         }
         if (obj){
            try {
               obj.focus();
            } catch(e) {}
         }
      }
      stepsValidated[intStep] = -1;
      var objImg = document.getElementById("validation_step_" + intStep);
      if (objImg){
         objImg.src = "images/error.gif";
         objImg.title = strMessage;
      }
      return false;
  }
  
  function formatDateObject(obj) 
  {
    if (obj.value.length != 0)
    {
      var lsDate = obj.value;
      var lsMonth, lsDay, lsYear;
      if (lsDate.indexOf("/") != -1) {
        var laryDate = lsDate.split("/");
        lsMonth = laryDate[0];
        lsDay = laryDate[1];
        lsYear = laryDate[2];
      } else {
        //try to format with regular expression.
        lsYear = lsDate.substr(lsDate.length - 4, 4);
        var lsReminder = lsDate.substr(0, lsDate.length - 4);
        switch (lsReminder.length) {
          case 4:
            lsMonth = lsReminder.substr(0, 2);
            lsDay = lsReminder.substr(2, 2);
            break;
          case 3:
            if (parseInt(lsReminder.substr(0, 2), 10) > 12) {
              lsMonth = lsReminder.substr(0, 1);
              lsDay = lsReminder.substr(1, 2)
            } else {
              lsMonth = lsReminder.substr(0, 2);
              lsDay = lsReminder.substr(2, 1)              
            }
            break;
          case 2:
            lsMonth = lsReminder.substr(0, 1);
            lsDay = lsReminder.substr(1, 1);
            break;
        }
      }
  
      var lbBadDate = false;
      if (lsMonth == undefined || lsMonth < 1 || lsMonth > 12)
        lbBadDate = true;
      if (lsDay == undefined || lsDay < 1 || lsDay > 31)
        lbBadDate = true;
      if (lsYear == undefined || lsYear < 1)
        lbBadDate = true;
      if (lbBadDate == true)
      {
        alert("Please enter the date as MM/DD/YYYY or M/D/YY or MMDDYYYY.");
        obj.focus();
        return;
      }
  
      if (lsMonth < 10 && lsMonth.charAt(0) != 0)
        lsMonth = "0"+lsMonth;

      if (lsDay < 10 && lsDay.charAt(0) != 0)
        lsDay = "0"+lsDay;

      var lsMydate = new Date();
      if (lsYear == undefined || lsYear == "" || (lsYear >= 100 && lsYear < 1900))
      {
        lsYear = lsMydate.getFullYear();
      }
      else if (lsYear >= 0 && lsYear < 100)
      {
        lsYear = Number(lsYear) + 2000;
      }
      else if ((lsYear >= 1900 && lsYear < 2000) || (lsYear > 2000))
      {
        lsYear = Number(lsYear);
      }
      else
      {
        lsYear = lsMydate.getFullYear();
      }

      obj.value = lsMonth + "/" + lsDay + "/" + lsYear;
    }
  }
  
  function setSummaryData(allInputs){
      allInputs.each(function(i){
         var objId = this.id;
         if(objId != ""){
            $("#divSummary_" + objId).text(this.value);
         }
      });
  }
  
  function saveStepData(allInputs){
      strStepData = new Array();
      allInputs.each(function(i){
         var objId = this.id;
         if(objId != ""){
            strStepData.push({id: objId, value: this.value});
         }
      });
  }
  
  function revertStepData(){
      var strInputId, strInputValue;
      for (var i = 0; i < strStepData.length; i++){
         strInputId = strStepData[i].id;
         strInputValue = strStepData[i].value;
         $("#" + strInputId).val(strInputValue);
      }
      strStepData = new Array();
  }
  
  function formatPhone(strPhone){
      if  (strPhone == "") {  
         return strPhone;
      } else {
         return strPhone.substr(1,3) + strPhone.substr(6, 3) + strPhone.substr(10, 4);
      }
  }
  
  function chkLienHolder_click(){
      var blnDisable = (chkLienHolder.checked == false);
      var strClassName = (blnDisable == true ? "breadcrumb_disabled" : "");
      $("#txtLienHolderName").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHAddress1").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHAddress2").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHAddressZip").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHAddressCity").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#selLHAddressState").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHPhone").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHFax").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHEmailAddress").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHContactName").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHPayoffAmount").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHPayoffExpDate").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLHAccountNumber").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      $("#txtLoGAmount").attr("disabled", blnDisable).removeClass().addClass(strClassName);
      
      if (blnDisable == false){
         txtLienHolderName.focus();
      }
  }
  
  function getAssignmentXML(){
      var strXML = "<WebAssignment version='400'>";
      //fill claim data
      strXML += "<Claim>";
      strXML += "<NewClaimFlag><![CDATA[1]]></NewClaimFlag>";
        strXML += "<NoticeMethodID><![CDATA[5]]></NoticeMethodID>";
        strXML += "<DataSource><![CDATA[Claim Point Web Assignment]]></DataSource>";
        strXML += "<FNOLUserID><![CDATA[" + CarrierRep.id + "]]></FNOLUserID>";
        strXML += "<InsuranceCompanyID><![CDATA[" + Insurance.id + "]]></InsuranceCompanyID>";
        strXML += "<AssignmentAtSelectionFlag><![CDATA[0]]></AssignmentAtSelectionFlag>";
        strXML += "<DemoFlag><![CDATA[0]]></DemoFlag>";
        strXML += "<AssignmentDescription><![CDATA[Total Loss]]></AssignmentDescription>";
        strXML += "<CarrierOfficeName><![CDATA[" + CarrierRep.Office + "]]></CarrierOfficeName>";
        strXML += "<CarrierRepNameFirst><![CDATA[" + CarrierRep.FirstName + "]]></CarrierRepNameFirst>";
        strXML += "<CarrierRepNameLast><![CDATA[" + CarrierRep.LastName + "]]></CarrierRepNameLast>";
        strXML += "<CarrierRepUserID><![CDATA[" + Caller.id + "]]></CarrierRepUserID>";
        strXML += "<CarrierRepPhoneDay><![CDATA[" + CarrierRep.Phone + "]]></CarrierRepPhoneDay>";
        strXML += "<CarrierRepEmailAddress><![CDATA[" + CarrierRep.Email + "]]></CarrierRepEmailAddress>";
        strXML += "<CarrierName><![CDATA[" + CarrierRep.InsuranceName + "]]></CarrierName>";
        strXML += "<CallerNameFirst><![CDATA[" + Caller.FirstName + "]]></CallerNameFirst>";
        strXML += "<CallerNameLast><![CDATA[" + Caller.LastName + "]]></CallerNameLast>";
        strXML += "<CallerRelationToInsuredID><![CDATA[" + $("#selAssignorRelation").val() + "]]></CallerRelationToInsuredID>";
        strXML += "<CallerRelationToInsuredIDDescription><![CDATA[" + $("#selAssignorRelation").selectedTexts()[0] + "]]></CallerRelationToInsuredIDDescription>";
        strXML += "<InsuredNameFirst><![CDATA[" + $("#txtInsuredFirstName").val() + "]]></InsuredNameFirst>";
        strXML += "<InsuredNameLast><![CDATA[" + $("#txtInsuredLastName").val() + "]]></InsuredNameLast>";
        strXML += "<InsuredBusinessName><![CDATA[" + $("#txtInsuredBusinessName").val() + "]]></InsuredBusinessName>";
        strXML += "<InsuredAddress1><![CDATA[" + $("#txtInsuredAddress1").val() + "]]></InsuredAddress1>";
        strXML += "<InsuredAddress2><![CDATA[" + $("#txtInsuredAddress2").val() + "]]></InsuredAddress2>";
        strXML += "<InsuredAddressCity><![CDATA[" + $("#txtInsuredAddressCity").val() + "]]></InsuredAddressCity>";
        strXML += "<InsuredAddressState><![CDATA[" + $("#selInsuredAddressState").val() + "]]></InsuredAddressState>";
        strXML += "<InsuredAddressZip><![CDATA[" + $("#txtInsuredAddressZip").val() + "]]></InsuredAddressZip>";
        strXML += "<TimeStarted><![CDATA[" + strStartTime + "]]></TimeStarted>";
        strXML += "<TimeFinished><![CDATA[" + getCurrentDT() + "]]></TimeFinished>";
        strXML += "<IntakeStartSeconds><![CDATA[" + strIntakeStartSeconds + "]]></IntakeStartSeconds>";
        strXML += "<IntakeEndSeconds><![CDATA[" + getCurrentTimeSec() + "]]></IntakeEndSeconds>";
        strXML += "<IntakeSeconds><![CDATA[" + Math.floor((getCurrentTimeSec() - strIntakeStartSeconds)/1000) + "]]></IntakeSeconds>";
        strXML += "<InsuredName><![CDATA[" + $("#txtInsuredFirstName").val() + " " + $("#txtInsuredLastName").val() + "]]></InsuredName>";
        strXML += "<LossDate><![CDATA[" + $("#txtLossDate").val() + "]]></LossDate>";
        strXML += "<LossAddressState><![CDATA[" + $("#selLossState").val() + "]]></LossAddressState>";
        strXML += "<CoverageClaimNumber><![CDATA[" + $("#txtClaimNumber").val() + "]]></CoverageClaimNumber>";
        strXML += "<SettlementDate><![CDATA[" + $("#txtSettlementDate").val() + "]]></SettlementDate>";
        strXML += "<SettlementAmount><![CDATA[" + $("#txtSettlementAmount").val() + "]]></SettlementAmount>";
        strXML += "<AdvanceAmount><![CDATA[" + $("#txtAdvanceAmount").val() + "]]></AdvanceAmount>";
        strXML += "<LoGAmount><![CDATA[" + $("#txtLoGAmount").val() + "]]></LoGAmount>";
        strXML += "<LienHolderName><![CDATA[" + $("#txtLienHolderName").val() + "]]></LienHolderName>";
        strXML += "<LienHolderAddress1><![CDATA[" + $("#txtLHAddress1").val() + "]]></LienHolderAddress1>";
        strXML += "<LienHolderAddress2><![CDATA[" + $("#txtLHAddress2").val() + "]]></LienHolderAddress2>";
        strXML += "<LienHolderAddressCity><![CDATA[" + $("#txtLHAddressCity").val() + "]]></LienHolderAddressCity>";
        strXML += "<LienHolderAddressState><![CDATA[" + $("#selLHAddressState").val() + "]]></LienHolderAddressState>";
        strXML += "<LienHolderAddressZip><![CDATA[" + $("#txtLHAddressZip").val() + "]]></LienHolderAddressZip>";
        strXML += "<LienHolderPhone><![CDATA[" + formatPhone($("#txtLHPhone").val()) + "]]></LienHolderPhone>";
        strXML += "<LienHolderFax><![CDATA[" + formatPhone($("#txtLHFax").val()) + "]]></LienHolderFax>";
        strXML += "<LienHolderEmail><![CDATA[" + $("#txtLHEmailAddress").val() + "]]></LienHolderEmail>";
        strXML += "<LienHolderContactName><![CDATA[" + $("#txtLHContactName").val() + "]]></LienHolderContactName>";
        strXML += "<LienHolderPayoffAmount><![CDATA[" + $("#txtLHPayoffAmount").val() + "]]></LienHolderPayoffAmount>";
        strXML += "<LienHolderPayoffExpirationDate><![CDATA[" + $("#txtLHPayoffExpDate").val() + "]]></LienHolderPayoffExpirationDate>";
        strXML += "<LienHolderAccountNumber><![CDATA[" + $("#txtLHAccountNumber").val() + "]]></LienHolderAccountNumber>";
        strXML += "<SalvageName><![CDATA[" + $("#txtSalvageName").val() + "]]></SalvageName>";
        strXML += "<SalvageAddress1><![CDATA[" + $("#txtSalvageAddress1").val() + "]]></SalvageAddress1>";
        strXML += "<SalvageAddress2><![CDATA[" + $("#txtSalvageAddress2").val() + "]]></SalvageAddress2>";
        strXML += "<SalvageAddressCity><![CDATA[" + $("#txtSalvageAddressCity").val() + "]]></SalvageAddressCity>";
        strXML += "<SalvageAddressState><![CDATA[" + $("#selSalvageAddressState").val() + "]]></SalvageAddressState>";
        strXML += "<SalvageAddressZip><![CDATA[" + $("#txtSalvageAddressZip").val() + "]]></SalvageAddressZip>";
        strXML += "<SalvagePhone><![CDATA[" + formatPhone($("#txtSalvagePhone").val()) + "]]></SalvagePhone>";
        strXML += "<SalvageFax><![CDATA[" + formatPhone($("#txtSalvageFax").val()) + "]]></SalvageFax>";
        strXML += "<SalvageEmail><![CDATA[" + $("#txtSalvageEmailAddress").val() + "]]></SalvageEmail>";
        strXML += "<SalvageContactName><![CDATA[" + $("#txtSalvageContactName").val() + "]]></SalvageContactName>";
        strXML += "<SalvageControlNumber><![CDATA[" + $("#txtSalvageControlNumber").val() + "]]></SalvageControlNumber>";
        strXML += "<Remarks><![CDATA[" + $("#txtComments").val() + "]]></Remarks>";
      strXML += "</Claim>";

      
      //fill vehicle data
      strXML += "<Vehicle ProgramShop='0'>";
      strXML += "<AssignmentTypeID><![CDATA[14]]></AssignmentTypeID>";
      strXML += "<ExposureCD><![CDATA[" + $("#selPartyCD").val() + "]]></ExposureCD>";
      strXML += "<CoverageProfileCD><![CDATA[" + $("#selCoverage").val() + "]]></CoverageProfileCD>";
      strXML += "<OwnerNameFirst><![CDATA[" + $("#txtOwnerFirstName").val() + "]]></OwnerNameFirst>";
      strXML += "<OwnerNameLast><![CDATA[" + $("#txtOwnerLastName").val() + "]]></OwnerNameLast>";
      strXML += "<OwnerBusinessName><![CDATA[" + $("#txtOwnerBusinessName").val() + "]]></OwnerBusinessName>";
      strXML += "<OwnerAddress1><![CDATA[" + $("#txtOwnerAddress1").val() + "]]></OwnerAddress1>";
      strXML += "<OwnerAddress2><![CDATA[" + $("#txtOwnerAddress2").val() + "]]></OwnerAddress2>";
      strXML += "<OwnerAddressCity><![CDATA[" + $("#txtOwnerAddressCity").val() + "]]></OwnerAddressCity>";
      strXML += "<OwnerAddressState><![CDATA[" + $("#selOwnerAddressState").val() + "]]></OwnerAddressState>";
      strXML += "<OwnerAddressZip><![CDATA[" + $("#txtOwnerAddressZip").val() + "]]></OwnerAddressZip>";
      strXML += "<OwnerPhoneDay><![CDATA[" + formatPhone($("#txtOwnerPhoneDay").val()) + "]]></OwnerPhoneDay>";
      strXML += "<OwnerPhoneAlt><![CDATA[" + formatPhone($("#txtOwnerPhoneAlt").val()) + "]]></OwnerPhoneAlt>";
      strXML += "<ContactNameFirst><![CDATA[" + $("#txtOwnerFirstName").val() + "]]></ContactNameFirst>";
      strXML += "<ContactNameLast><![CDATA[" + $("#txtOwnerLastName").val() + "]]></ContactNameLast>";
      strXML += "<ContactAddress1><![CDATA[" + $("#txtOwnerAddress1").val() + "]]></ContactAddress1>";
      strXML += "<ContactAddress2><![CDATA[" + $("#txtOwnerAddress2").val() + "]]></ContactAddress2>";
      strXML += "<ContactAddressCity><![CDATA[" + $("#txtOwnerAddressCity").val() + "]]></ContactAddressCity>";
      strXML += "<ContactAddressState><![CDATA[" + $("#selOwnerAddressState").val() + "]]></ContactAddressState>";
      strXML += "<ContactAddressZip><![CDATA[" + $("#txtOwnerAddressZip").val() + "]]></ContactAddressZip>";
      strXML += "<ContactsRelationToInsuredID><![CDATA[" + ($("#selPartyCD").val() == "1" ? "7" : "10") + "]]></ContactsRelationToInsuredID>";
      strXML += "<ContactBestPhoneCD><![CDATA[D]]></ContactBestPhoneCD>";
      strXML += "<VIN><![CDATA[" + $("#txtVehicleVIN").val() + "]]></VIN>";
      strXML += "<LicensePlateNumber><![CDATA[" + $("#txtLicensePlate").val() + "]]></LicensePlateNumber>";
      strXML += "<LicensePlateState><![CDATA[" + $("#selLicenseState").val() + "]]></LicensePlateState>";
      strXML += "<VehicleYear><![CDATA[" + $("#txtVehicleYear").val() + "]]></VehicleYear>";
      strXML += "<Make><![CDATA[" +  $("#txtVehicleMake").val()+ "]]></Make>";
      strXML += "<Model><![CDATA[" + $("#txtVehicleModel").val() + "]]></Model>";
      strXML += "<OwnerPhone><![CDATA[" + formatPhone($("#txtOwnerPhoneDay").val()) + "]]></OwnerPhone>";
      strXML += "<ContactPhone><![CDATA[" + formatPhone($("#txtOwnerPhoneDay").val()) + "]]></ContactPhone>";
      strXML += "<ContactAltPhone><![CDATA[" + formatPhone($("#txtOwnerPhoneAlt").val()) + "]]></ContactAltPhone>";
      strXML += "<OwnerPhoneSumm><![CDATA[" + $("#txtOwnerPhoneDay").val() + "]]></OwnerPhoneSumm>";
      strXML += "<ContactPhoneSumm><![CDATA[" + $("#txtOwnerPhoneDay").val() + "]]></ContactPhoneSumm>";
      strXML += "<ContactAltPhoneSumm><![CDATA[" + $("#txtOwnerPhoneAlt").val() + "]]></ContactAltPhoneSumm>";;
      strXML += "<VehicleNumber><![CDATA[" + ($("#selPartyCD").val() == "1" ? "1" : "2") + "]]></VehicleNumber>";
      strXML += "<TitleName><![CDATA[" + $("#txtTitleNames").val() + "]]></TitleName>";
      strXML += "<TitleState><![CDATA[" + $("#selTitleState").val() + "]]></TitleState>";
      strXML += "<TitleStatus><![CDATA[" + $("#selTitleStatus").val() + "]]></TitleStatus>";
      
      strXML += "</Vehicle>";

      
      strXML += "</WebAssignment>";
      return strXML;
  }
  
  function formatCurrencyObject(obj){
      if (obj){
         ldTrim(obj);
         if (isNumeric(obj.value)){
            if (obj.value != "")
               obj.value = formatcurrency(obj.value);
         } else {
            alert("Enter a numeric value. Example: 100.00");
            obj.value = "";
         }
      }
  }

  function rtnMyClmDsktp()
  {
    top.location = "frmMyClaimsDesktop.asp";
  }

function showMessage(strMessage) {
    if (parent && typeof (parent.showModalMessage) == "function")
        parent.showModalMessage(strMessage);
}

function hideMessage() {
    if (parent && typeof (parent.hideModalMessage) == "function")
        parent.hideModalMessage();
}

function populateLoGAmount() {
    var strPayoffAmount = $("#txtLHPayoffAmount").val();
    var strSettlementAmount = $("#txtSettlementAmount").val();
    if (strPayoffAmount != "") strPayoffAmount = parseFloat(strPayoffAmount);
    if (strSettlementAmount != "") strSettlementAmount = parseFloat(strSettlementAmount);
    if (strPayoffAmount >= 0 && strSettlementAmount >= 0) {
        $("#txtLoGAmount").val((strPayoffAmount < strSettlementAmount ? strPayoffAmount : strSettlementAmount));
    }
}