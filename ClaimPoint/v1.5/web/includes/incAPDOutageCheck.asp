
<%
  'Outage constants
  Dim lsOutageMsg, lsOutageTitleMsg, ltNowTime
  ltNowTime = Now
  
  'Maintenance Mode constants
  Dim lsMaintenanceModeMsg, ltMaintenanceModeLastUpdate, ltMaintenanceModeRuntime, liMaintenanceModeMinutesDiff, liMaintenanceModeTimer
  
  Dim docXMLpMsg, lsStrXMLpMsg, pMsgRoot, lsRetValpMsg

  'Maintenance Outage notification check
  liMaintenanceModeTimer = Application("CP_MaintenanceModeTimer")
  If Application("CP_MaintenanceModeLastUpdate") <> "" Then
    ltMaintenanceModeLastUpdate = CDate(Application("CP_MaintenanceModeLastUpdate"))
    ltMaintenanceModeRuntime = CDate(ltNowTime - ltMaintenanceModeLastUpdate)
    liMaintenanceModeMinutesDiff = Minute(ltMaintenanceModeRuntime)
  Else
    liMaintenanceModeMinutesDiff = liMaintenanceModeTimer + 1
  End If
  
  If liMaintenanceModeMinutesDiff > liMaintenanceModeTimer Then
 
    Set docXMLpMsg = Server.CreateObject("MSXML2.DOMDocument")
    docXMLpMsg.Async = False
  
    lsStrXMLpMsg = "<Config Path='ClaimPoint/Maintenance/@ModeActive' />"
    docXMLpMsg.LoadXML GetData(lsStrXMLpMsg, "", "")
    'Set pMsgRoot = docXMLpMsg.documentElement
    lsMaintenanceMode = lcase(docXMLpMsg.documentElement.text)
    
    If lsMaintenanceMode = "true" Then
  
      lbMaintenanceMode = true
      lsStrXMLpMsg = "<Config Path='ClaimPoint/Maintenance/MaintenanceModeMsg' />"
      docXMLpMsg.LoadXML GetData(lsStrXMLpMsg, "", "")
      lsRetValpMsg = docXMLpMsg.documentElement.text
  
      Application("CP_MaintenanceModeMsg") = lsRetValpMsg
      lsMaintenanceModeMsg = lsRetValpMsg
  
      Application("CP_MaintenanceModeLastUpdate") = ltNowTime
  
    Else
      Application("CP_MaintenanceModeMsg") = ""
    End If
  
    Set docXMLpMsg = Nothing
    
  Else
  
    lsMaintenanceModeMsg = Application("CP_MaintenanceModeMsg")
  
    If lsMaintenanceModeMsg <> "" Then
      lbMaintenanceMode = true
    End If
    
  End If

  If lbMaintenanceMode = true and liPageType > lcAuthSection Then
  
    Response.Write "<P style='font-size:16px; font-weight:bold; color:#FF0000'>"
    Response.Write "<img border='0' src='images/warn_img.gif' WIDTH='16' HEIGHT='16' hspace='6' >"
    Response.Write "ClaimPoint is offline for maintenance and is currently unavailable." & "<br>"
    Response.Write "<img border='0' src='images/spacer.gif' WIDTH='16' HEIGHT='16' hspace='6' >"
    Response.Write "You will be logged out in 15 seconds. Thank you for your understanding."
    Response.Write "</P>"
  
    Response.write  "<script language='JavaScript'>"
    Response.write  "  setTimeout('top.document.location=\'frmLogout.asp\'',15000);"
    Response.write  "</script>"
  
    Response.end
  
  End If
  
  
  If Not(lbMaintenanceMode) and liPageType = lcAuthSection Then
  
    'Planned Outage constants
    Dim lsPlannedOutageMsg, ltPlannedOutageLastUpdate, ltPlannedOutageRuntime, liPlannedMinutesDiff, liPlannedOutageTimer
    
    'Planned Outage notification check
    liPlannedOutageTimer = Application("CP_PlannedOutageTimer")
    If Application("CP_PlannedOutageLastUpdate") <> "" Then
      ltPlannedOutageLastUpdate = CDate(Application("CP_PlannedOutageLastUpdate"))
      ltPlannedOutageRuntime = CDate(ltNowTime - ltPlannedOutageLastUpdate)
      liPlannedMinutesDiff = Minute(ltPlannedOutageRuntime)
    Else
      liPlannedMinutesDiff = liPlannedOutageTimer + 1
    End If
    
    If liPlannedMinutesDiff > liPlannedOutageTimer Then
      
      Set docXMLpMsg = Server.CreateObject("MSXML2.DOMDocument")
      docXMLpMsg.Async = False
      
      lsStrXMLpMsg = "<Config Path='ClaimPoint/PlannedOutageMsg' />"
      docXMLpMsg.LoadXML GetData(lsStrXMLpMsg, "", "")
      Set pMsgRoot = docXMLpMsg.documentElement
      lsRetValpMsg = pMsgRoot.text
      
      Application("CP_PlannedOutageMsg") = lsRetValpMsg
      lsPlannedOutageMsg = lsRetValpMsg
      
      Application("CP_PlannedOutageLastUpdate") = ltNowTime
      
      Set pMsgRoot = Nothing
      Set docXMLpMsg = Nothing
    Else
      lsPlannedOutageMsg = Application("CP_PlannedOutageMsg")
    End If
  
  ElseIf Not(lbMaintenanceMode) and liPageType > lcAuthSection Then
  
    'Emergency Outage constants
    Dim lsEmergencyOutageMsg, ltEmergencyOutageLastUpdate, ltEmergencyOutageRuntime, liEmergencyMinutesDiff, liEmergencyOutageTimer
    
    'Emergency Outage notification check
    liEmergencyOutageTimer = Application("CP_EmergencyOutageTimer")
    If Application("CP_EmergencyOutageLastUpdate") <> "" Then
      ltEmergencyOutageLastUpdate = CDate(Application("CP_EmergencyOutageLastUpdate"))
      ltEmergencyOutageRuntime = CDate(ltNowTime - ltEmergencyOutageLastUpdate)
      liEmergencyMinutesDiff = Minute(ltEmergencyOutageRuntime)
    Else
      liEmergencyMinutesDiff = liEmergencyOutageTimer + 1
    End If
  
    If liEmergencyMinutesDiff > liEmergencyOutageTimer Then
      Dim docXMLeMsg, lsStrXMLeMsg, eMsgRoot, lsRetValeMsg
      
      Set docXMLeMsg = Server.CreateObject("MSXML2.DOMDocument")
      docXMLeMsg.Async = False
      
      lsStrXMLeMsg = "<Config Path='ClaimPoint/EmergencyOutageMsg' />"
      docXMLeMsg.LoadXML GetData(lsStrXMLeMsg, "", "")
      Set eMsgRoot = docXMLeMsg.documentElement
      lsRetValeMsg = eMsgRoot.text
      
      Application("CP_EmergencyOutageMsg") = lsRetValeMsg
      lsEmergencyOutageMsg = lsRetValeMsg
      
      Application("CP_EmergencyOutageLastUpdate") = ltNowTime
    
      Set eMsgRoot = Nothing
      Set docXMLeMsg = Nothing
    Else
      lsEmergencyOutageMsg = Application("CP_EmergencyOutageMsg")
    End If
  
  End If
%>

<%
  If lsExternalEventMsg <> "" Then

    Response.Write lsExternalEventMsg
  
  End If

  If liPageType = lcAuthSection and lsMaintenanceModeMsg <> "" Then

    lsOutageMsg = lsMaintenanceModeMsg
    lsOutageTitleMsg = "Offline/Maintenance Outage Notification"
%>
    <!--#include file="incOutage.asp"-->
  
<%
  ElseIf liPageType > lcAuthSection and lsEmergencyOutageMsg <> "" Then

    lsOutageMsg = lsEmergencyOutageMsg
    lsOutageTitleMsg = "Emergency Outage Notification"
%>
    <!--#include file="incOutage.asp"-->

<%
  ElseIf liPageType = lcAuthSection and lsPlannedOutageMsg <> "" Then

    lsOutageMsg = lsPlannedOutageMsg
    lsOutageTitleMsg = "Planned Outage Notification"
%>
    <!--#include file="incOutage.asp"-->

<% End If %>
