// v1.5.0.0 

function GridSort(oTable, oSortTypes)
{
	this.element = oTable;
	this.tHead = oTable.tHead;
	this.tBody = oTable.tBodies[0];
	this.document = oTable.ownerDocument || oTable.document;
	this.sortColumn = null;
//	if (iSortColumn != undefined || iSortColumn != '')
//    this.sortColumn = iSortColumn;
	this.descending = null;
//	if (iDescending != undefined || iDescending != '')
//	  this.descending = iDescending;

	var oThis = this;
	this._headerOnclick = function (e)
  {
		oThis.headerOnclick(e);
	};

	var win = this.document.defaultView || this.document.parentWindow;
	this._onunload = function ()
  {
		oThis.destroy();
	};
  
	if (win && typeof win.attachEvent != "undefined")
  {
		win.attachEvent("onunload", this._onunload);
	}

	this.initHeader(oSortTypes || []);

}

GridSort.gecko = navigator.product == "Gecko";
GridSort.msie = /msie/i.test(navigator.userAgent);
GridSort.removeBeforeSort = GridSort.gecko;

GridSort.prototype.onsort = function () {};

GridSort.prototype.initHeader = function (oSortTypes)
{
	var cells = this.tHead.rows[0].cells;
	var l = cells.length;
	var img, c;
	for (var i = 0; i < l; i++)
  {
    c = cells[i];
		if (c.sortReq != "No")
    {
  		img = this.document.createElement("IMG");
  		img.src = "images/grid_blank.gif";
  		c.appendChild(img);
  		if (oSortTypes[i] != null)
      {
  			c._sortType = oSortTypes[i];
  		}
  		if (typeof c.addEventListener != "undefined")
  			c.addEventListener("click", this._headerOnclick, false);
  		else if (typeof c.attachEvent != "undefined")
  			c.attachEvent("onclick", this._headerOnclick);
    }
	}
	this.updateHeaderArrows();
};

GridSort.prototype.uninitHeader = function ()
{
	var cells = this.tHead.rows[0].cells;
	var l = cells.length;
	var c;
	for (var i = 0; i < l; i++)
  {
		c = cells[i];
		c.removeChild(c.lastChild);
		if (typeof c.removeEventListener != "undefined")
			c.removeEventListener("click", this._headerOnclick, false);
		else if (typeof c.detachEvent != "undefined")
			c.detachEvent("onclick", this._headerOnclick);
	}
};

GridSort.prototype.updateHeaderArrows = function ()
{
	var cells = this.tHead.rows[0].cells;
	var l = cells.length;
	var img;
	for (var i = 0; i < l; i++)
  {
		img = cells[i].lastChild;
		if (i == this.sortColumn)
      img.className = "sort-arrow " + (this.descending ? "descending" : "ascending");
    else
			img.className = "sort-arrow";
	}
};


GridSort.prototype.repaintRows = function ()
{
  var tblRows = this.element.rows;
	var l = tblRows.length;

  for (i=0; i<l; i++)
  {
    if (tblRows[i].KeepBGColor != "true")
      tblRows[i].style.background = "#FFFFFF";
  }

  for (i=0; i<l; i = i + 2)
  {
    if (tblRows[i].KeepBGColor != "true")
      tblRows[i].style.background = "#DFEFFF";
  }
};


GridSort.prototype.headerOnclick = function (e)
{
	if (e != undefined)
  {
    var el = e.target || e.srcElement || e ;
  	while (el.tagName != "TD")
  		el = el.parentNode;
  
  	this.sort(GridSort.msie ? GridSort.getCellIndex(el) : el.cellIndex);
  }
};

GridSort.getCellIndex = function (oTd)
{
	var cells = oTd.parentNode.childNodes
	var l = cells.length;
	var i;
	for (i = 0; cells[i] != oTd && i < l; i++)
		;
	return i;
};

GridSort.prototype.getSortType = function (nColumn)
{
	var cell = this.tHead.rows[0].cells[nColumn];
	var val = cell._sortType;
	if (val != "")
		return val;
	return "String";
};

GridSort.prototype.sort = function (nColumn, bDescending, sSortType)
{
	if (sSortType == null)
		sSortType = this.getSortType(nColumn);

	if (sSortType == "None")
		return;

	if (bDescending == null || bDescending == undefined)
  {
		if (this.sortColumn != nColumn)
			this.descending = false;
		else
			this.descending = !this.descending;
	}
  else
    this.descending = bDescending;

	this.sortColumn = nColumn;

	if (typeof this.onbeforesort == "function")
		this.onbeforesort();

	var f = this.getSortFunction(sSortType, nColumn);
	var a = this.getCache(sSortType, nColumn);
	var tBody = this.tBody;

	a.sort(f);

	if (this.descending)
		a.reverse();

	if (GridSort.removeBeforeSort)
  {
		var nextSibling = tBody.nextSibling;
		var p = tBody.parentNode;
		p.removeChild(tBody);
	}

	var l = a.length;
	for (var i = 0; i < l; i++)
		tBody.appendChild(a[i].element);

	if (GridSort.removeBeforeSort)
  {
		p.insertBefore(tBody, nextSibling);
	}

	this.updateHeaderArrows();

	this.destroyCache(a);
  
	if (typeof this.onsort == "function")
		this.onsort();

  this.repaintRows();
};

GridSort.prototype.asyncSort = function (nColumn, bDescending, sSortType)
{
	var oThis = this;
	this._asyncsort = function () {
		oThis.sort(nColumn, bDescending, sSortType);
	};
	window.setTimeout(this._asyncsort, 1);
};

GridSort.prototype.getCache = function (sType, nColumn)
{
	var rows = this.tBody.rows;
	var l = rows.length;
	var a = new Array(l);
	var r;
	for (var i = 0; i < l; i++)
  {
		r = rows[i];
		a[i] = {value:this.getRowValue(r, sType, nColumn), element: r};
	};
	return a;
};

GridSort.prototype.destroyCache = function (oArray)
{
	var l = oArray.length;
	for (var i = 0; i < l; i++)
  {
		oArray[i].value = null;
		oArray[i].element = null;
		oArray[i] = null;
	}
}

GridSort.prototype.getRowValue = function (oRow, sType, nColumn)
{
	var s;
	var c = oRow.cells[nColumn];
	if (typeof c.innerText != "undefined")
		s = c.innerText;
	else
		s = GridSort.getInnerText(c);
	return this.getValueFromString(s, sType);
};

GridSort.getInnerText = function (oNode)
{
	var s = "";
	var cs = oNode.childNodes;
	var l = cs.length;
	for (var i = 0; i < l; i++)
  {
		switch (cs[i].nodeType)
    {
			case 1:
				s += GridSort.getInnerText(cs[i]);
				break;
			case 3:
				s += cs[i].nodeValue;
				break;
		}
	}
	return s;
}

GridSort.prototype.getValueFromString = function (sText, sType)
{
	switch (sType)
  {
		case "Number":
      return Number(sText);
		case "USCurrency":
      sText = sText.substr(1, sText.length - 1);
      return Number(sText);
		case "CaseInsensitiveString":
			return sText.toUpperCase();
		case "Date":
			var parts = sText.split("-");
			var d = new Date(0);
			d.setFullYear(parts[2]);
			d.setDate(parts[1]);
			d.setMonth(parts[0]-1);
			return d.valueOf();
    case "DateTime":
      sText = sText.replace(/[ \t]/g, "-");
      sText = sText.replace(/:/, "-");
      sText = escape(sText);
      sText = sText.replace(/%0D%0A/, "-");
      var parts = sText.split("-");
      var d = "";
      d += parts[2];
      d += parts[0];
      d += parts[1];
      if (parts[5] == "PM" && parts[3] < 12)
        d += Number(parts[3])+12;
      else
      {
        if (parts[3].length == 1)
          d += "0" + parts[3];
        else
          d += parts[3];
      }
      d += parts[4];
      return Number(d);
	}
	return sText;
};

GridSort.prototype.getSortFunction = function (sType, nColumn)
{
	return function compare(n1, n2)
  {
		if (n1.value < n2.value)
			return -1;
		if (n2.value < n1.value)
			return 1;
		return 0;
	};
};

GridSort.prototype.destroy = function ()
{
	this.uninitHeader();
	var win = this.document.parentWindow;
	if (win && typeof win.detachEvent != "undefined")
  {
		win.detachEvent("onunload", this._onunload);
	}
	this._onunload = null;
	this.element = null;
	this.tHead = null;
	this.tBody = null;
	this.document = null;
	this._headerOnclick = null;
	this.sortTypes = null;
	this._asyncsort = null;
	this.onsort = null;
};

