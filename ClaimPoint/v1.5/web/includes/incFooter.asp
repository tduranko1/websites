<!-- v1.4.6.0 -->

<%if not bModal Then%>

  <%
    If liPageType > lcAuthSection Then
  
      Dim docXMLftr, lsStrXML, lsVerRetVal, ftrGUID
      
      Set docXMLftr = Server.CreateObject("MSXML2.DOMDocument")
      docXMLftr.Async = False
      
      If (loAPD is Nothing) Then
        Set loAPD = Server.CreateObject("APD.clsAppData")
      End If
  
      ftrGUID = Request.Cookies("CPSession")("guidArray0")
      lsStrXML = "<Config Path='ClaimPoint/Version' />"
      'docXMLftr.LoadXML GetData(lsStrXML, "", "")
      docXMLftr.LoadXML GetPersistedData(lsStrXML, ftrGUID, "", "", true)
      
      lsVerRetVal = docXMLftr.documentElement.text
  
      Set docXMLftr = Nothing
    
    End If
  %>

  <p class="login" style="padding:10px; font-size:8pt; text-align:center; margin-left:100px;">
    Copyright &copy; 2020 <span onclick="callAbout('frmAbout.asp',1,380,300)"">LYNX</span> Services, L.L.C. All Rights Reserved
	<br/>
	<a href="pdf/PN-LYNX-01-29-2020.pdf" target="_blank">LYNX Privacy Notice</a>

    <% If liPageType > lcAuthSection Then %>
      <span onclick="openHelpWin(gsRelNotesPage)" style="cursor:hand;" title="Click to view release notes" ><%= lsVerRetVal%></span>
    <% End If %>

  </p>
  <p>
  </p>

<%End If%>

<IFRAME SRC="frmUserLoginInfo.asp" NAME="UserLoginInfo" ID="UserLoginInfo" SCROLLING="no" FRAMEBORDER="0" STYLE="width:0; height:0; visibility:hidden; display:none;"></IFRAME>
