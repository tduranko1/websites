<script language=JavaScript src="includes/IncModal.js"></script>

<TABLE WIDTH=169 BORDER=0 CELLPADDING=0 CELLSPACING=0>
	<TR>
		<TD>
			<IMG SRC="images/logo_corner.jpg" ALT="" width="169" height="202"></TD>
	</TR>
	<TR>
		<TD>

      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
<%
  If liPageType = lcAuthSection Then
%>
  <%
    If lsPageTitle = "Forgot Password" or lsPageTitle = "Change Password" Then
  %>
				<tr>
					<td width="100%" background="images/button_small.jpg" height="30">
  					<a href="frmLogin.asp"
               class="leftNav"
               onMouseOver="window.status='Login'; return true"
               onMouseOut="window.status=''; return true">
              Login
            </a>
          </td>
				</tr>
  <%
    End If
  %>
  <%
    If lsPageTitle = "Forgot Password" or Instr(lsPageTitle,"Login") > 0 Then
  %>
				<tr>
					<td width="100%" background="images/button_small.jpg" height="30">
					  <a href="frmChangePassword.asp"
               id="lnkChangePassword"
               class="leftNav"
               onMouseOver="window.status='Change Password'; return true"
               onMouseOut="window.status=''; return true">
              Change Password
            </a>
          </td>
				</tr>
  <%
    End If
  %>
  <%
    If lsPageTitle = "Change Password" or Instr(lsPageTitle,"Login") Then
  %>
				<tr>
					<td width="100%" background="images/button_small.jpg" height="30">
					  <a href="frmForgotPassword.asp"
               id="lnkForgotPassword"
               class="leftNav"
               onMouseOver="window.status='Forgot Password'; return true"
               onMouseOut="window.status=''; return true">
              Forgot Password
            </a>
          </td>
				</tr>
  <%
    End If
  %>
<%
  ElseIf liPageType = lcAfterInsuranceCompanySelect or liPageType = lcAfterClaimSearch Then
%>
				<tr>
					<td width="100%" background="images/button_small.jpg" height="30">
					  <a href="frmMyClaimsDesktop.asp"
               class="leftNav"
               onMouseOver="window.status='My Claims Desktop'; return true"
               onMouseOut="window.status=''; return true">
             My Claims Desktop
            </a>
          </td>
				</tr>
  <%
    If liViewReport = "1" Then
  %>			
				<tr>
					<td width="100%" background="images/button_small.jpg" height="30">
					  <a href="frmReports.asp"
               class="leftNav"
               onMouseOver="window.status='View Reports'; return true"
               onMouseOut="window.status=''; return true">
              View Reports
            </a>
          </td>
				</tr>
  <%
    End If
  %>			

  <%
    If liCarrierAuthorizesPaymentsFlag = "1" and liAuthorizePayments = "1" Then
  %>			
				<tr>
					<td width="100%" background="images/button_small.jpg" height="30">
					  <a href="frmAuthPayments.asp"
               class="leftNav"
               onMouseOver="window.status='Authorize Payments'; return true"
               onMouseOut="window.status=''; return true">
              Authorize Payments
            </a>
          </td>
				</tr>
  <%
    End If
  %>			

  <%
    If liCarrierRep = "0" Then
  %>
				<tr>
					<td width="100%" background="images/button_small.jpg" height="30">
					  <a href="frmInsuranceCoSelect.asp"
               class="leftNav"
               onMouseOver="window.status='Select Insurance Co.'; return true"
               onMouseOut="window.status=''; return true">
              Select Insurance Co.
            </a>
          </td>
				</tr>
  <%
    End If
  %>
  <%
    If liManageUsers = "1" Then
  %>
				<tr>
					<td width="100%" background="images/button_small.jpg" height="30">
    <%
      If liCarrierRep = "0" Then
    %>
					<a href="javascript:goUserList('U');"
             class="leftNav"
             onMouseOver="window.status='Manage Carrier Users'; return true"
             onMouseOut="window.status=''; return true">
            Manage Carrier Users
          </a>
    <%
      ElseIf liCarrierRep = "1" Then
    %>
					<a href="javascript:goUserList('U');"
             class="leftNav"
             onMouseOver="window.status='Manage Users'; return true"
             onMouseOut="window.status=''; return true">
            Manage Users
          </a>
    <%
      End If
    %>
          </td>
				</tr>
    <%
      If liCarrierRep = "0" Then
    %>
				<tr>
					<td width="100%" background="images/button_small.jpg" height="30">
					  <a href="javascript:goUserList('M');"
               class="leftNav"
               onMouseOver="window.status='Manage LYNX Users'; return true"
               onMouseOut="window.status=''; return true">
              Manage LYNX Users
            </a>
          </td>
				</tr>
    <%
      End If
    %>

  <%
    End If
  %>
<%
  End If
%>			
			</table>

		</TD>
	</TR>
</TABLE>

<form name="frmUserList" id="frmUserList" method="post" action="frmUserList.asp" target="_self">
  <input type="hidden" name="M" id="M"/>
</form>