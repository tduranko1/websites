<!-- v1.5.0.0 -->

<%
Function SendMail(sTo, sFrom, sSubject, sBody , sCC , sBCC )

On Error Resume Next

  Dim WshNetwork, strServerName, sSMTPServer
      
  Set WshNetwork = Server.CreateObject("WScript.Network")
  strServerName = WshNetwork.ComputerName
  Set WshNetwork = Nothing
  
  If Instr(1,strServerName, "STG") > 0 Or Instr(1,strServerName, "DEV") > 0 Or Instr(1,strServerName, "WFTM") > 0 Then
  		sTo = sFrom
  End If

	Dim iMsg, iConf, Flds

	Set iMsg = Server.CreateObject("CDO.Message")

	Set iConf = Server.CreateObject("CDO.Configuration")

	Set Flds = iConf.Fields

	With Flds
	  .Item("http://schemas.microsoft.com/cdo/configuration/sendusing")					    = 2 ' cdoSendUsingPort
	  .Item("http://schemas.microsoft.com/cdo/configuration/smtpserver")				    = sSMTPServer
	  .Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 10 ' quick timeout
	  .Item("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate")		  = 0
	  .Update
	End With

	With iMsg
	  Set .Configuration = iConf
	      .To         = sTo
	      .From       = sFrom
	      .Subject    = sSubject
	      .HTMLBody   = sBody
	      .CC         = sCC
	      .BCC        = sBCC
	      .Send
	End With

	If err.number <> 0 Then
		Err.Clear
		'Response.Write "E-mail Failure. Close the window and Try again." & "<BR>"
		'Response.Write sSMTPServer
		'Response.end
	End If

	Set Flds = Nothing
	Set iMsg = Nothing
	Set iConf = Nothing

End Function
%>


