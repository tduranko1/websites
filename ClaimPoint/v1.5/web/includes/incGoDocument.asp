<!-- v1.5.0.0 -->

<script language="JavaScript">

	function goDocument(sPath)
	{
    var lsPath = "frmDisplayDocument.asp?D=" + escape(sPath);
    var win = window.open(lsPath, 'docViewerWindow', 'width=675,height=510,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no');
    win.focus();
	}

  function addDocument()
  {
    if (is_ie5_5up == true)
    {
      var sDimensions = "dialogHeight:300px; dialogWidth:680px; ";
      var sSettings = "center:Yes; help:No; resizable:No; status:No; unadorned:Yes;";
      var sUrl = "frmDocumentUpload.asp?LynxID=" + "<%=liLynxID%>" + "&VehNumber=" + "<%=liVehNumber%>" + "&ch=<%=lsServiceChannelCD%>";
      var strRet = window.showModalDialog(sUrl, "", sDimensions + sSettings);
      if (strRet == "1")
        window.location.href = "frmDocument.asp?LynxID=" + "<%=liLynxID%>" + "&ch=<%=lsServiceChannelCD%>" + "&AssignmentType=" + "<%=lsAssignmentType%>" + "&AspectID=" + "<%=liAspectID%>" + "&VehNumber=" + "<%=liVehNumber%>";
    }
    else
    {
      mozModal();
    }
  }

  var myModalWin = new Object();
  function mozModal()
  {
   if (!myModalWin.win || (myModalWin.win && myModalWin.win.closed))
    {
      myModalWin.returnedValue = "";
      myModalWin.url = "frmDocumentUpload.asp";
      myModalWin.name = (new Date()).getSeconds().toString();
      myModalWin.width = "670";
      myModalWin.height = "290";
      myModalWin.left = (screen.width - myModalWin.width) / 2;
      myModalWin.top = (screen.height - myModalWin.height) / 2;
      var attr = "left=" + myModalWin.left + ",top=" + myModalWin.top + ",resizable=no,width="
                + myModalWin.width + ",height=" + myModalWin.height;
      myModalWin.win=window.open(myModalWin.url, myModalWin.name, attr);
      myModalWin.win.focus();
  	}
    else
    {
     	myModalWin.win.focus();
  	}
  }

  function getReturnVal()
  {
    if (myModalWin.returnedValue == "1")
      window.location.href = "frmDocument.asp?LynxID=" + "<%=liLynxID%>" + "&AssignmentType=" + "<%=lsAssignmentType%>" + "&AspectID=" + "<%=liAspectID%>";
  }

  function checkMyModal()
  {
  	setTimeout("isMyModalUp()", 50);
  	return true;
  }

  function isMyModalUp()
  {
  	if (myModalWin.win && !myModalWin.win.closed)
  		myModalWin.win.focus();
  }

</script>

