<%
  <!-- v1.4.5.0 -->

  function GetData(psCall, psStyleSheetName, psParams)
    GetData = loAPD.GetAPDData(psCall, psStyleSheetName, psParams)
  
    If loAPD.EventNumber <> 0 Then
        'Response.Write "EventNumber: " & loAPD.EventNumber & "<br>"
        'Response.Write "InternalEvent: " & loAPD.InternalEvent & "<br>"
        'Response.Write "ExternalEvent: " & loAPD.ExternalEvent & "<br>"
  
        If loAPD.ExternalEvent <> "" Then
           lsExternalEventMsg = loAPD.ExternalEvent
        Else
           lsExternalEventMsg = "An internal system error has occurred. Please try again"
        End If
    End If
  
  end function
  
  'Set pbOverwrite to TRUE to pull the XML data from the database hosted in eTemplate.
  'Set pbOverwrite to FALSE to pull the XML data from the database in Fort Myers.
  function GetPersistedData(psCall, psGUID, psStyleSheetName, psParams, pbOverwrite)
    GetPersistedData = loAPD.GetPersistedData(psCall, psGUID, psStyleSheetName, psParams, pbOverwrite)
  
    If loAPD.EventNumber <> 0 Then
        'Response.Write "EventNumber: " & loAPD.EventNumber & "<br>"
        'Response.Write "InternalEvent: " & loAPD.InternalEvent & "<br>"
        'Response.Write "ExternalEvent: " & loAPD.ExternalEvent & "<br>"
  
        If loAPD.ExternalEvent <> "" Then
           lsExternalEventMsg = loAPD.ExternalEvent
        Else
           lsExternalEventMsg = "An internal system error has occurred. Please try again"
        End If
    End If
  
  end function

%>
