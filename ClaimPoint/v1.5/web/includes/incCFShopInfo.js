
function CheckNum(event){
	var evt = (evt) ? evt : ((event) ? event : null);
	var obj = eval("frmShop." + evt.srcElement.id);
	var idx = obj.value.indexOf(".")
	var pct = false;
	var prec;
	var code = evt.keyCode;
  
  	if (obj.getAttribute("precision") != null)
    	prec = obj.getAttribute("precision");
  	else
    	prec = 2;
	
	if ((code < 48 && code != 46) || code > 57){ // limit to numerals and decimal point
		evt.returnValue = false;
		return;	
	}
	
	if (arguments[1])
		pct = true;
	
	if (code == 45){				// no negatives
		evt.returnValue = false;
		return;
	}
		
	if (idx > -1){
		//if (obj.value.length - idx > prec){		// allow only proper number decimal places -- 1 for percentages, 2 otherwise
		//	evt.returnValue = false;
		//	return;
		//}
		
		if (code == 46){			// allow only one decimal point
			evt.returnValue = false;
			return;
		}
	}
	
	if (pct){								// ensure percentages do not exceed 100
		if (obj.value == 100){
			evt.returnValue = false
			return;
		}
		
		if (obj.value == 10 && code!= 46 && code != 48 && idx == -1){
			evt.returnValue = false;
			return;
		}
	
		if (obj.value > 10 && code != 46 && idx == -1){
			evt.returnValue = false;
			return;
		}
	}
}   
  

// check maxlength for textarea
function CheckInputLength(obj, max, id)
{
	if (id)
    var msgObj = document.getElementById(id);
  else
    var msgObj = document.getElementById("commFldMsg");
	
  if (obj.tagName == "TEXTAREA" && obj.value.length > max)
	{
		msgObj.style.backgroundColor = "#000099";
		msgObj.style.color = "#FFFF66";
	}
	else
	{
		msgObj.style.backgroundColor = "";
		msgObj.style.color = "#000000";
	}
}


function getCurrentDT()
{
  var mydate = new Date();
  var year = mydate.getFullYear();
  if (year < 1000) year += 1900;
  var month = mydate.getMonth()+1; if (month<=9) month="0"+month;
  var day = mydate.getDate(); if (day<=9) day="0"+day;
  var hours = mydate.getHours();
  var dn="AM";
    if ( hours>=12 ) dn="PM";
    if (hours>12) hours=hours-12;
    if (hours==0) hours=12;
    if (hours<=9) hours="0"+hours;
  var minutes = mydate.getMinutes(); if (minutes<=9) minutes="0"+minutes;
  var seconds = mydate.getSeconds(); if (seconds<=9) seconds="0"+seconds;
  var cdate = month +"/"+ day +"/"+ year +" "+ hours +":"+ minutes +":"+ seconds +" "+ dn;
  return cdate;
}

/*
		Allow numeric values only
		Parameters:
			obj				= element
			evt       = event
			decPntKey = allow decimal point (.) for monetary entries 1-yes 0-no
			slashKey  = allow slash (/) for date entries 1-yes 0-no
      colonKey  = allow slash (:) for hour entries 1-yes 0-no
		Usage:
			onKeyPress="return numbersOnly(this,event,0,1)"
*/
function numbersOnly(obj,evt,decPntKey,slashKey,colonKey)
{
//	var maxLen = obj.getAttribute("maxLength");
	if (obj.value.length > obj.maxLength) return;
	var bOK = false;
	var evt = (evt) ? evt : ((event) ? event : null);
	if (evt)
	{
		var charCode = (evt.charCode || evt.charCode == 0) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : evt.which);
		if (!(charCode > 13 && (charCode < 48 || charCode > 57))) bOK = true; // normal numbers
		if (decPntKey == 1 && charCode == 46) bOK = true; // allow decimals '.'
		if (slashKey == 1 && charCode == 47) bOK = true; // allow date entry '/'
		if (colonKey == 1 && charCode == 58) bOK = true; // allow date entry '/'

		if (bOK != true)
		{
			if (evt.returnValue) evt.returnValue = false;
			else if (evt.preventDefault) evt.preventDefault();
			else return false;
    }
	}
}

function NumbersOnly2(event)
{
  var bOK = false;
	//if ((event.keyCode >= 45 && event.keyCode <= 57) && (event.keyCode != 47))  bOK = true;
	if (event.keyCode >= 48 && event.keyCode <= 57)  bOK = true;

    try {
        if (event.srcElement.allowDecimal != "undefined")
        {
        if (event.srcElement.allowDecimal == true){
            if (event.keyCode == 46) bOK = true;
        }
        }
        else {
	        if (event.keyCode == 46) bOK = false;
        }
    }
    catch(e) {
        bOK = false;
    }
    if (event.keyCode == 13) bOK = true;  //pass thru an <Enter> key
	if (event.shiftKey == true || bOK == false )
		event.returnValue = false;
}


function checkEMail(obj)
{
  if (obj.value != "")
  {
    if (!isEmail(obj.value))
    {
      alert(" Invalid E-Mail specified. \n Example: johnsmith@yahoo.com \n Please try again");
      event.returnValue = false;
      obj.focus();
    }
  }
}

function isEmail(strEmail)
{
  if (strEmail.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
    return true;
  else
    return false;
}


//currency formating
function FormatCurrencyObj(obj)
{
  if (obj.value == "")
    return;
  
  X = parseFloat(obj.value);
  if (isNaN(X) || X == '.')
    return obj.value = '0.00'
  else
  {
    var currNum = formatcurrency(X);
    obj.value = currNum;
  }
}

function formatcurrency(X)
{
  var roundNum = Math.round(X*100)/100; //round to 2 decimal places
  var strValue = new String(roundNum);
  if (strValue.indexOf('.') == -1)
    strValue = strValue + '.00';
  else if (strValue.indexOf('.') == strValue.length - 2)
    strValue = strValue + '0';
  else
    strValue = strValue.substring(0, strValue.indexOf('.') + 3);
  
  if (strValue.indexOf('.') == 0)
    return '0' + strValue;
  else
    return strValue;
}


//hours formating
function FormatHoursObj(obj)
{
  if (obj.value == "")
    return;
  
  X = obj.value;
  if (isNaN(X) || X == '.')
    return obj.value = '0.0'
  else
  {
    var hrsNum = formathours(X);
    obj.value = hrsNum;
  }
}

function formathours(X)
{
  var roundNum = Math.round(X*10)/10; //round to 1 decimal places
  var strValue = new String(roundNum);
  if (strValue.indexOf('.') == -1)
    strValue = strValue + '.0';
  else
    strValue = strValue.substring(0, strValue.indexOf('.') + 2);

  if (strValue.indexOf('.') == 0)
    return '0' + strValue;
  else
    return strValue;
}


//percentage formating
function FormatPercentageObj(obj)
{
  if (obj.value == "")
    return;
  
  X = obj.value;
  if (isNaN(X) || X == '.')
    return obj.value = '0.000'
  else
  {
    var pctNum = formatpercentage(X);
    obj.value = pctNum;
  }
}

function formatpercentage(X)
{
  var roundNum = Math.round(X*1000)/1000; //round to 3 decimal places
  var strValue = new String(roundNum);
  if (strValue.indexOf('.') == -1)
    strValue = strValue + '.000';
  else if (strValue.indexOf('.') == strValue.length - 2)
    strValue = strValue + '00';
  else if (strValue.indexOf('.') == strValue.length - 3)
    strValue = strValue + '0';
  else
    strValue = strValue.substring(0, strValue.indexOf('.') + 6);

  if (strValue.indexOf('.') == 0)
    return '0' + strValue;
  else
    return strValue;
}

var isNN = (navigator.appName.indexOf("Netscape")!=-1);
function autoTab(input, len, e)
{
  var keyCode = (isNN) ? e.which : e.keyCode; 
  var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
  if (input.value.length >= len && !containsElement(filter, keyCode))
  {
    input.value = input.value.slice(0, len);
    input.form[(getIndex(input)+1) % input.form.length].focus();
  }
  return true;
}


function containsElement(arr, ele)
{
  var found = false, index = 0;
  while (!found && index < arr.length)
  if (arr[index] == ele)
    found = true;
  else
    index++;
  return found;
}


function getIndex(input)
{
  var index = -1, i = 0, found = false;
  while (i < input.form.length && index == -1)
  if (input.form[i] == input)
    index = i;
  else i++;
    return index;
}

function ldTrim(obj)
{ 
  var strText = obj.value;
  while (strText.substring(0,1) == ' ') // this will get rid of leading spaces
    strText = strText.substring(1, strText.length);
  
  while (strText.substring(strText.length-1,strText.length) == ' ') // this will get rid of trailing spaces
    strText = strText.substring(0, strText.length-1);
  
  obj.value = strText;
}


function hrsChk(obj)
{
  if (obj.value == "")
    return;

  if (obj.value.indexOf(':') == -1)
    bOK = false;

  var bOK = true;
  var oVal = obj.value;
  var aVal = oVal.split(':');
  var sHH = aVal[0];
  var sMM = aVal[1];
  
  if (sHH == "" || sHH > 12 || sMM == "" || sMM > 60)
    bOK = false;

  if (bOK == false)
  {
    alert("Please enter in the hour in a format such as 8:00 / 5:30.")
    obj.focus();
  }
}
 
