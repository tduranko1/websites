// v1.4.2.0

// Resize the parent IFrame height depending on the IFrame content page height and update header title.
function resizeWaIframe(numTotalVeh)
{
  if(parent)
    var objIFrame = parent.document.getElementById('uiFrame');
  else
    return false;

  var liRentalAdditional = 0;// additional height for Rental additional
var liRentalAdditionalSummery = 0;// additional height for Rental additional summery page
  var liMsgScrollerHeight = 0; //additional height for Messsage Scroller
  var liVehTotal; //total num of vehicles
  var liSpTotal; //number of Shop Program assignments to adjust page height
  var objContainer = document.getElementById("divContainer")

  if (numTotalVeh == 0 || numTotalVeh == "" || numTotalVeh == undefined)
  {
    liVehTotal = 0;
    liSpTotal = 0;
  }
  else
  {
    var arr = numTotalVeh.split(",");
    liVehTotal = arr[0];
    liSpTotal = arr[1];
  }

  if (document.getElementById('MsgScroller'))
    liMsgScrollerHeight = 30;

if (document.getElementById('tblRentalAdditional'))
      liRentalAdditional = 200;
if (document.getElementById('tblAdditionalRental_1'))
      liRentalAdditionalSummery = 100;

  if (objContainer)
  {
    if (parent.document.getElementById('spanlsPageTitle'))
      parent.document.getElementById('spanlsPageTitle').innerHTML = document.title; //update header title

    objIFrame.style.height = objContainer.offsetHeight + liMsgScrollerHeight + 140 + (580 * liVehTotal) + (280 * liSpTotal) + liRentalAdditional + liRentalAdditionalSummery; // resize iframe according to the height of its contents
    top.window.scrollTo(0, 0);
  }
  else
    objIFrame.height = 600; // default size
} 


// Check if the input param is numeric
function isNumeric(sValue)
{
    return !isNaN(sValue);
}


/*
    Allow numeric values only
    Parameters:
      obj        = element
      evt       = event
      decPntKey = allow decimal point (.) for monetary entries 1-yes 0-no
      slashKey  = allow slash (/) for date entries 1-yes 0-no
    Usage:
      onKeyPress="return numbersOnly(this,event,0,1)"
*/
function numbersOnly(obj,evt,decPntKey,slashKey)
{
//  var maxLen = obj.getAttribute("maxLength");
  if (obj.value.length > obj.maxLength) return;
  var bOK = false;
  evt = (evt) ? evt : ((event) ? event : null);
  if (evt)
  {
    var charCode = (evt.charCode || evt.charCode == 0) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : evt.which);
    if (!(charCode > 13 && (charCode < 48 || charCode > 57))) bOK = true; // normal numbers
    if (decPntKey == 1 && charCode == 46) bOK = true; // allow decimals '.'
    if (slashKey == 1 && charCode == 47) bOK = true; // allow date entry '/'

    if (bOK != true)
    {
      if (evt.returnValue) evt.returnValue = false;
      else if (evt.preventDefault) evt.preventDefault();
      else return false;
    }
  }
}


function alphaOnly(obj,evt)
{
  if (obj.value.length > obj.maxLength) return;
  var bOK = false;
  evt = (evt) ? evt : ((event) ? event : null);
  if (evt)
  {
    var charCode = (evt.charCode || evt.charCode == 0) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : evt.which);
    if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) bOK = true; // normal letters

    if (bOK != true)
    {
      if (evt.returnValue) evt.returnValue = false;
      else if (evt.preventDefault) evt.preventDefault();
      else return false;
    }
  }
}


//currency formating
function FormatCurrencyObj(obj)
{
  X = parseFloat(obj.value);
  if (isNaN(X) || X == '.')
    return obj.value = ''
  else
  {
    var currNum = formatcurrency(X);
    return obj.value = currNum;
  }
}


function formatcurrency(X)
{
  var roundNum = Math.round(X*100)/100; //round to 2 decimal places
  var strValue = new String(roundNum);
  if (strValue.indexOf('.') == -1)
    strValue = strValue + '.00';
  else if (strValue.indexOf('.') == strValue.length - 2)
    strValue = strValue + '0';
  else
    strValue = strValue.substring(0, strValue.indexOf('.') + 3);
  
  if (strValue.indexOf('.') == 0)
    return '0' + strValue;
  else
    return strValue;
}


//percentage formating
function FormatPercentageObj(obj)
{
  X = obj.value;
  if (isNaN(X) || X == '.' || X == "")
    return obj.value = ''
  else
  {
    var pctNum = formatpercentage(X);
    obj.value = pctNum;
  }
}

function formatpercentage(X)
{
  var roundNum = Math.round(X*1000)/1000; //round to 3 decimal places
  var strValue = new String(roundNum);
  if (strValue.indexOf('.') == -1)
    strValue = strValue + '.000';
  else if (strValue.indexOf('.') == strValue.length - 2)
    strValue = strValue + '00';
  else if (strValue.indexOf('.') == strValue.length - 3)
    strValue = strValue + '0';
  else
    strValue = strValue.substring(0, strValue.indexOf('.') + 6);

  if (strValue.indexOf('.') == 0)
    return '0' + strValue;
  else
    return strValue;
}


// check maxlength for textarea
function CheckInputLength(obj, max, id)
{
  if (id)
    var msgObj = document.getElementById(id);
  else
    var msgObj = document.getElementById("commFldMsg");
  
  if (obj.tagName == "TEXTAREA" && obj.value.length > max)
  {
    msgObj.style.backgroundColor = "#000099";
    msgObj.style.color = "#FFFF66";
  }
  else
  {
    msgObj.style.backgroundColor = "";
    msgObj.style.color = "#000000";
  }
}


function getDisplayDate(endTime)
{
  var dayarray=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday")
  var montharray=new Array("January","February","March","April","May","June","July","August","September","October","November","December")
  
  var mydate = new Date(endTime);
  var year = mydate.getFullYear();
  
  if ( year < 1000 ) year+=1900;
  
  var day = mydate.getDay();
  var month = mydate.getMonth();
  var daym = mydate.getDate();
  
  if (daym<10) daym="0"+daym;
  
  var hours=mydate.getHours();
  var minutes=mydate.getMinutes();
  var dn="AM";
  
  if ( hours>=12 ) dn="PM";
  if ( hours>12 ) hours=hours-12;
  if ( hours==0 ) hours=12;
  if ( minutes<=9 ) minutes="0"+minutes;
  
  var cdate = dayarray[day]+", "+montharray[month]+" "+daym+", "+year+" "+hours+":"+minutes+" "+dn;
  return cdate;
}


function getCurrentDT()
{
  var mydate = new Date();
  var year = mydate.getFullYear();
  if (year < 1000) year += 1900;
  var month = mydate.getMonth()+1; if (month<=9) month="0"+month;
  var day = mydate.getDate(); if (day<=9) day="0"+day;
  var hours = mydate.getHours();
  var dn="AM";
  if ( hours>=12 ) dn="PM";
  if (hours>12) hours=hours-12;
  if (hours==0) hours=12;
  if (hours<=9) hours="0"+hours;
  var minutes = mydate.getMinutes(); if (minutes<=9) minutes="0"+minutes;
  var seconds = mydate.getSeconds(); if (seconds<=9) seconds="0"+seconds;
  var cdate = month +"/"+ day +"/"+ year +" "+ hours +":"+ minutes +":"+ seconds +" "+ dn;
  return cdate;
}

function getCurrentTimeSec()
{
  var mydate = new Date();
  var mydateSeconds = mydate.getTime();
  return mydateSeconds;
}

var isNN = (navigator.appName.indexOf("Netscape")!=-1);
function autoTab(input, len, e) {
  var keyCode = (isNN) ? e.which : e.keyCode;
  var filter = (isNN) ? [0, 8, 9] : [0, 8, 9, 16, 17, 18, 37, 38, 39, 40, 46];
  if (input.value.length >= len && !containsElement(filter, keyCode)) {
      input.value = input.value.slice(0, len);
    input.form[(getIndex(input)+1) % input.form.length].focus();
  }
  return true;
}
function movetoNext(current, nextFieldID) {
    if (current.value.length >= current.maxLength) {
        document.getElementById(nextFieldID).focus();
    }
}


function containsElement(arr, ele)
{
  var found = false, index = 0;
  while (!found && index < arr.length)
  if (arr[index] == ele)
    found = true;
  else
    index++;
  return found;
}


function getIndex(input) {
  var index = -1, i = 0, found = false;
  while (i < input.form.length && index == -1)
      if (input.form[i] == input) {
          index = i; 
      }
      else i++;  
    return index;
}



function ldTrim(obj)
{ 
  try {
  var strText = obj.value;
  while (strText.substring(0,1) == ' ') // this will get rid of leading spaces
    strText = strText.substring(1, strText.length);
  
  while (strText.substring(strText.length-1,strText.length) == ' ') // this will get rid of trailing spaces
    strText = strText.substring(0, strText.length-1);
  
  obj.value = strText;
  } catch (e) {}
} 

function ValidateClaimNumber(objInput, sValidateExpression){
  if (objInput && objInput.value != "") {
    if (sValidateExpression != "") {
      var re = new RegExp(sValidateExpression, "i"); //ignore case
      if (re)
        return re.test(objInput.value);
      else
        return true;
    }
  }
}

function FormatClaimNumber(objInput, sCarrierRepOfficeID, arrOfficeID, arrFormatExpression, arrValidateExpression, arrValidationHelper, bAutoFocus) {
  if (objInput && objInput.value != "") {
    if (arrFormatExpression && arrValidateExpression) {
      var arrMsg = new Array(); 
      var bValid = true;
      // find if the data matches any of the validation expression
      for( var i = 0; i < arrFormatExpression.length; i++) {
        if (arrOfficeID[i] != sCarrierRepOfficeID) continue;
        if (arrValidateExpression[i] != "") {
          if (ValidateClaimNumber(objInput, arrValidateExpression[i]) == true) {
            bValid = true;
            break;
          } else {
            bValid = false;
            if (arrValidationHelper[i] != "") {
              var bFound = false;
              // check if this validation helper message was added or not.
              for (var j = 0; j < arrMsg.length; j++) {
                if (arrMsg[j] == arrValidationHelper[i]){
                  bFound = true;
                  break;
                }
              }
              //current validation helper message was not found in the message going to the user. so add it to the list.
              if (!bFound)
                arrMsg.push(arrValidationHelper[i]);
            }
          }
        }
      }

      if (bValid) {
        // data is valid. Now format it
        if (i < arrFormatExpression.length) {
          var bUpperCase = false;
          var sFormatExpression = arrFormatExpression[i];
          var sValidateExpression = arrValidateExpression[i];
          if (sFormatExpression != "" && sValidateExpression != "" ) {
            if (sFormatExpression.substr(0,1) == "^")
              bUpperCase = true;
            
            var sRet;
            if (bUpperCase)
              sRet = sFormatExpression.substr(1, sFormatExpression.length);
            else
              sRet = sFormatExpression;
            
            var re = new RegExp(sValidateExpression, "i"); //ignore case
            var arr = re.exec(objInput.value);
    
            for (var i = 0; i < arr.length; i++) {
              var re = new RegExp("@" + i, "ig");
              sRet = sRet.replace(re, arr[i]);
            }
            
            if (bUpperCase)
              sRet = sRet.toUpperCase();
            
            objInput.value = sRet;
            ldTrim(objInput);
          }
        }
      } else {
        //data is invalid. Display the expected format if available.
        var sMsg = arrMsg.join("\n\n");
        if (sMsg != ""){
          sMsg = "Invalid data entered. Expecting a data in the following format:\n\n" + sMsg + "\n\nPlease try again."
        } else {
          sMsg = "Invalid data format entered. Please try again.";
        }
        alert(sMsg);
        if (bAutoFocus == true) {
          try {
            var bDisabled = true;
            var bReadOnly = true;
            bDisabled = objInput.getAttribute("disabled");
            bReadOnly = objInput.getAttribute("readonly");
            if (bDisabled == false && bReadOnly == false){
              objInput.focus();
              objInput.select();
            }
          } catch (e) {}
        }
      }
      return bValid;
    }
  }
  return true; //default return
}

function getFNOLData(strPath){
   var strRet = "";
   if (strPath != "") {
      try {
         var objFNOLXML = document.getElementById("xmlFNOLData");
         if (objFNOLXML){
            if (objFNOLXML.parseError.errorCode == 0){
               var objNode = objFNOLXML.selectSingleNode(strPath);
               if (objNode){
                  strRet = objNode.text;
               }
            }
            
         }
      } catch (e) {}
   }
   return strRet;
}

function isEmail(strEmail) {
   var re = /^(([^<>()[\]\\%&+.,;:\s@\"]+(\.[^<>()[\]\\%&+.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   return re.test(strEmail);
}

function formatDateObject(obj) {
   if (obj.value.length != 0) {
      var lsDate = obj.value;
      var lsMonth, lsDay, lsYear;
      if (lsDate.indexOf("/") != -1) {
         var laryDate = lsDate.split("/");
         lsMonth = laryDate[0];
         lsDay = laryDate[1];
         lsYear = laryDate[2];
      } else {
         //try to format with regular expression.
         lsYear = lsDate.substr(lsDate.length - 4, 4);
         var lsReminder = lsDate.substr(0, lsDate.length - 4);
         switch (lsReminder.length) {
            case 4:
               lsMonth = lsReminder.substr(0, 2);
               lsDay = lsReminder.substr(2, 2);
               break;
            case 3:
               if (parseInt(lsReminder.substr(0, 2), 10) > 12) {
                  lsMonth = lsReminder.substr(0, 1);
                  lsDay = lsReminder.substr(1, 2)
               } else {
                  lsMonth = lsReminder.substr(0, 2);
                  lsDay = lsReminder.substr(2, 1)
               }
               break;
            case 2:
               lsMonth = lsReminder.substr(0, 1);
               lsDay = lsReminder.substr(1, 1);
               break;
         }
      }

      var lbBadDate = false;
      if (lsMonth == undefined || lsMonth < 1 || lsMonth > 12)
         lbBadDate = true;
      if (lsDay == undefined || lsDay < 1 || lsDay > 31)
         lbBadDate = true;
      if (lsYear == undefined || lsYear < 1)
         lbBadDate = true;
      if (lbBadDate == true) {
         alert("Please enter the date as MM/DD/YYYY or M/D/YY or MMDDYYYY.");
         obj.focus();
         return;
      }

      if (lsMonth < 10 && lsMonth.charAt(0) != 0)
         lsMonth = "0" + lsMonth;

      if (lsDay < 10 && lsDay.charAt(0) != 0)
         lsDay = "0" + lsDay;

      var lsMydate = new Date();
      if (lsYear == undefined || lsYear == "" || (lsYear >= 100 && lsYear < 1900)) {
         lsYear = lsMydate.getFullYear();
      }
      else if (lsYear >= 0 && lsYear < 100) {
         lsYear = Number(lsYear) + 2000;
      }
      else if ((lsYear >= 1900 && lsYear < 2000) || (lsYear > 2000)) {
         lsYear = Number(lsYear);
      }
      else {
         lsYear = lsMydate.getFullYear();
      }

      obj.value = lsMonth + "/" + lsDay + "/" + lsYear;
   }
}

function formatPhone(strPhone) {
   if (strPhone == "") {
      return strPhone;
   } else {
      return strPhone.substr(1, 3) + strPhone.substr(6, 3) + strPhone.substr(10, 4);
   }
}

function checkvulnerability(queryString) {
    if (queryString !== '') {
        return true;
    }
    return false;
}