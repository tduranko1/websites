// v1.4.6.0

// Remove leading and trailing spaces
function ldTrim(obj)
{ 
  var strText = obj.value;
  while (strText.substring(0,1) == ' ') // this will get rid of leading spaces
    strText = strText.substring(1, strText.length);
  
  while (strText.substring(strText.length-1,strText.length) == ' ') // this will get rid of trailing spaces
    strText = strText.substring(0, strText.length-1);
  
  obj.value = strText;
}

// Popup About page with application details
function callAbout(callPage,altReq, W, H)
{
  if ((altReq == 1) && (window.event.ctrlKey) || (altReq == 0))
  {
    var lsDimensions = "dialogWidth:"+W+"px; dialogHeight:"+H+"px;"
    var lsSettings = "scroll:no; resizable:yes; status:no; help:no; center:yes; unadorned:yes;"
    var lsQuery = callPage;
    window.showModalDialog( lsQuery, window, lsDimensions + lsSettings );
  }
}

// Resize the parent IFrame height depending on the IFrame content page height.
function resizeIframe(oFrameName)
{
  if(parent)
    var objIFrame = parent.document.getElementById(oFrameName);
  else
    return false;

  var oTables = document.getElementsByTagName("TABLE");
  var oTablesLength = oTables.length;
  var objContainer;
    
  for(var y=0; y < oTablesLength; y++)
  {
    var loObjID = oTables[y].id;
    if (loObjID.indexOf('srhResults') != -1)
    {
      var objContainer = document.getElementById(loObjID);
      break;
    }
  }

  if(objContainer)
  {
    var iWidth = objContainer.offsetWidth;
    objIFrame.style.height = objContainer.offsetHeight + 100;
    if (iWidth > 600)
      objIFrame.style.width = objContainer.offsetWidth + 6;
    top.window.scrollTo(0, 0);
  }
  else
    objIFrame.height = 400; // default size
} 


// Popup window for Help system

var oHelpWin = null;
var gsHelpSubTopicPage;
var gsRelNotesPage = "CP_Release_Notes.htm";
  
function openHelpWin(HelpTopicPage)
{
  var sHelpURL;

  if (oHelpWin && oHelpWin.open && !oHelpWin.closed)
    oHelpWin.document.write("<HTML><BODY></BODY></HTML>");

  if (gsHelpSubTopicPage && gsHelpSubTopicPage != "" && HelpTopicPage != gsRelNotesPage)
    sHelpURL = "help/ClaimPoint_Help_File.htm#" + gsHelpSubTopicPage;
  else if (HelpTopicPage != "")
    sHelpURL = "help/ClaimPoint_Help_File.htm#" + HelpTopicPage;
  else
    sHelpURL = "help/ClaimPoint_Help_File.htm";

  oHelpWin = window.open(sHelpURL, 'helpWindow', 'toolbar=0,scrollbars=1,location=0,status=0,menubar=0,resizable=1,width=800,height=600')
}


function PrintPage()
{
  if (document.getElementById != null)
  {
    var html = "<HTML>\n<HEAD>\n";
    html += "<TITLE>ClaimPoint from LYNX Services</TITLE>";
    html += "<LINK href='includes/apd_style.css' type=text/css rel=stylesheet>";
    html += "<LINK href='includes/incWaStyle.css' type=text/css rel=stylesheet>";
    html += "<SCRIPT src='includes/incUtils.js' type=text/javascript></SCRIPT>";
    html += "<script type='text/javascript' src='includes/incGridHover.js'></script>";
    html += "<script language=JavaScript>";
    html += "function GridClickGetClaim()";
    html += "{return;}";
    html += "</script>";
    html += "\n</HEAD>\n<BODY>\n";
    
    html += "\n<img src='images/Lynx_logo.gif' width='111' height='53' border='0'>\n";
    
    var printReadyElem = document.getElementById("PrintPageDiv");
    if (printReadyElem != null)
      html += printReadyElem.innerHTML;
    else
    {
      alert("Could not find the printReady function");
      return;
    }

    html += "\n</BODY>\n</HTML>";
    
    var printWin = window.open('', 'PrintPageWin', 'toolbar=1,scrollbars=1,location=0,status=1,menubar=0,resizable=1,width=700,height=500');
    printWin.document.open();
    printWin.document.write(html);
    printWin.document.close();
    printWin.focus();
    printWin.print();

  }
  else
  {
    alert("The print Friendly feature is not available for your current browser. Please update your browser.");
  }
}


function getWindowSize(axis)
{
  var myWidth = 0;
  var myHeight = 0;
  if( typeof( window.innerWidth ) == 'number' )
  {
    //Non-IE
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
  }
  else if (document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ))
  {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
  }
  else if( document.body && ( document.body.clientWidth || document.body.clientHeight ))
  {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
  }
  
  if (axis == "width")
    return myWidth;
  else
    return myHeight;
}

function getScrollXY(axis)
{
  var scrOfX = 0;
  var scrOfY = 0;
  if( typeof( window.pageYOffset ) == 'number' )
  {
    //Netscape compliant
    scrOfY = window.pageYOffset;
    scrOfX = window.pageXOffset;
  }
  else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) )
  {
    //DOM compliant
    scrOfY = document.body.scrollTop;
    scrOfX = document.body.scrollLeft;
  }
  else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ))
  {
    //IE6 standards compliant mode
    scrOfY = document.documentElement.scrollTop;
    scrOfX = document.documentElement.scrollLeft;
  }
  if (axis == "x")
    return scrOfX;
  else
    return scrOfY;

  //return [ scrOfX, scrOfY ];
}

function findPosX(obj)
{
	var curleft = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft;
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	
  return curleft;
}

function findPosY(obj)
{
	var curtop = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop;
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;
	
  return curtop;
}  


// check maxlength for textarea
function CheckInputLength(obj, max, id)
{
  if (id)
    var msgObj = document.getElementById(id);
  else
    var msgObj = document.getElementById("commFldMsg");
  
  if (obj.tagName == "TEXTAREA" && obj.value.length > max)
  {
    msgObj.style.backgroundColor = "#000099";
    msgObj.style.color = "#FFFF66";
  }
  else
  {
    msgObj.style.backgroundColor = "";
    msgObj.style.color = "#000000";
  }
}
