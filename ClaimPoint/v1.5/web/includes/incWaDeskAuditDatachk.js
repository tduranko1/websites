// JavaScript Document
// v1.5.0.0

var gsPrevPage = ""; //global previous page
var gsNextPage = ""; //global next page
var gsVehNum = "";
var gsVehCount = "";
var gsRetToClaim = "false";
var gsRetFromClaim = "false";
var gsEditVehMode = "false";
var gLogoutTimer = null;
var gsSrhZipCode = "";
var lsPageName;
var gbEmergencyLiabilityScript = 0;
var gbReserveLineTypeScript = 0;
var gbDisableAmicaFields = 0;
var gbLaborRatesSetupFlag = 0;
var gbDisableInsuredAddress = 0;
var gbDisablePolicyNumber = 0;

function resetTimeOut()
{
  if (gLogoutTimer != null)
    window.clearTimeout(gLogoutTimer);
  gLogoutTimer = window.setTimeout("top.location = 'frmLogout.asp'", 240*60*1000);
}


function getdcIFrameDoc()
{
	var loDcIFrameDoc;
	var loIFrameObj = document.getElementById("dataContIfr");
	if (loIFrameObj.contentDocument) // For NS6
		return loDcIFrameDoc = loIFrameObj.contentDocument;
	else if (loIFrameObj.contentWindow) // For IE5.5 and IE6
		return loDcIFrameDoc = loIFrameObj.contentWindow.document;
  else if (loIFrameObj.document) // For IE5
  	return loDcIFrameDoc = loIFrameObj.document;
  else
		return;
}


function getuiIFrameDoc()
{
	var loUiIFrameDoc;
	var loIFrameObj = document.getElementById("uiFrame");
	if (loIFrameObj.contentDocument) // For NS6
		return loUiIFrameDoc = loIFrameObj.contentDocument;
	else if (loIFrameObj.contentWindow) // For IE5.5 and IE6
		return loUiIFrameDoc = loIFrameObj.contentWindow.document;
	else if (loIFrameObj.document) // For IE5
		return loUiIFrameDoc = loIFrameObj.document;
	else
		return;
}


function tsDeskAudit(gsPrevPage)
{
	var loUiIFrameDoc = getuiIFrameDoc();
	var lsMsg = "";
	var lsReq = "";
	var lsNumOnly = "";

  if (gbEmergencyLiabilityScript == 1 && lsPageName != "frmWaMainAddVehicle.asp")
  {
    var lcolElmsRdLiability = loUiIFrameDoc.getElementsByName("Liability");
  	var liLiability;
  	var lsRdLiability = "";
    var liRdLength = lcolElmsRdLiability.length;
  	for (var i=0; i<liRdLength; i++)
  	{
  		if (lcolElmsRdLiability.item(i).checked == true)
      {
  			liLiability = lcolElmsRdLiability.item(i).value;
        lsRdLiability = "Liability=" + lcolElmsRdLiability.item(i).value;
      }
  	}
    if (lsRdLiability == "")
  		lsReq += "    " + "The Liability question must be answered." + "\n";
    
    var lcolElmsRdEmergency = loUiIFrameDoc.getElementsByName("Emergency");
  	var liEmergency;
  	var lsRdEmergency = "";
    liRdLength = lcolElmsRdEmergency.length;
  	for (var i=0; i<liRdLength; i++)
  	{
  		if (lcolElmsRdEmergency.item(i).checked == true)
      {
  			liEmergency = lcolElmsRdEmergency.item(i).value;
  			lsRdEmergency = "Emergency=" + lcolElmsRdEmergency.item(i).value;
      }
  	}
    if (lsRdEmergency == "")
  		lsReq += "    " + "The Emergency question must be answered." + "\n";
  }

  if (gbReserveLineTypeScript == 1)
  {
    var loElmLineType = loUiIFrameDoc.getElementById("txtLineType");
  	var lsLineType = "LineType=" + loElmLineType.value;
    if (loElmLineType.value.length == 0)
      lsReq +=  "    " + "Line Type" + "\n";
  
    var loElmReserve = loUiIFrameDoc.getElementById("txtReserve");
  	var lsReserve = "Reserve=" + loElmReserve.value;
    if (loElmReserve.value.length == 0)
      lsReq +=  "    " + "Reserve Number" + "\n";
  }

  if (lsPageName != "frmWaMainAddVehicle.asp")
  {
  	if ((loUiIFrameDoc.getElementById("txtCallerNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtCallerNameLast").value.length == 0))
  		lsReq +=  "    " + "Claim Assigner First and Last name" + "\n";
  	if (loUiIFrameDoc.getElementById("txtCallerRelationToInsuredID").value.length == 0)
  		lsReq +=  "    " + "Assigner Relation to Insured" + "\n";
  	if (loUiIFrameDoc.getElementById("txtCoverageClaimNumber").value.length == 0)
  		lsReq +=  "    " + "Claim Number" + "\n";
  	if (loUiIFrameDoc.getElementById("txtLossDate").value.length == 0)
  		lsReq +=  "    " + "Loss Date" + "\n";
  	if (loUiIFrameDoc.getElementById("txtLossAddressState").value.length == 0)
  		lsReq +=  "    " + "Loss State" + "\n";
    if (gbDisableAmicaFields == 1)
    {
    	if (loUiIFrameDoc.getElementById("txtInsuredNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtInsuredNameLast").value.length == 0)
    		lsReq +=  "    " + "Insured First and Last name" + "\n";
    }
    else
    {
    	if ((loUiIFrameDoc.getElementById("txtInsuredNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtInsuredNameLast").value.length == 0) && loUiIFrameDoc.getElementById("txtInsuredBusinessName").value.length == 0)
    		lsReq +=  "    " + "Insured First and Last name or Business name" + "\n";
    }
  }  

  if (loUiIFrameDoc.getElementById("txtExposureCD").value.length == 0)
  	lsReq +=  "    " + "Party" + "\n";
  if (loUiIFrameDoc.getElementById("txtCoverageProfileUiCD").value.length == 0)
  	lsReq +=  "    " + "Coverage" + "\n";

  if (lsPageName != "frmWaMainAddVehicle.asp")
  {
    if (loUiIFrameDoc.getElementById("txtLossDescription").value.length == 0)
    	lsReq +=  "    " + "Description of Loss" + "\n";
  }

  if ((loUiIFrameDoc.getElementById("txtOwnerNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtOwnerNameLast").value.length == 0) && loUiIFrameDoc.getElementById("txtOwnerBusinessName").value.length == 0)
  	lsReq +=  "    " + "Owner First and Last name or Business name" + "\n";

  var lsMydate = new Date();
  var liCurrYear = lsMydate.getFullYear();
  if (loUiIFrameDoc.getElementById("txtVehicleYear").value.length > 0 && loUiIFrameDoc.getElementById("txtVehicleYear").value.length < 4)
  	lsReq +=  "    " + "Vehicle Year requires 4 digits" + "\n";
  else if (loUiIFrameDoc.getElementById("txtVehicleYear").value < 1900 || loUiIFrameDoc.getElementById("txtVehicleYear").value > liCurrYear+1)
  	lsReq +=  "    " + "Vehicle Year must be between 1900 and " + (liCurrYear+1) + ".\n";
    
  if (loUiIFrameDoc.getElementById("txtVehicleYear").value.length == 0 || loUiIFrameDoc.getElementById("txtMake").value.length == 0 || loUiIFrameDoc.getElementById("txtModel").value.length == 0)
  	lsReq +=  "    " + "Vehicle Year/Make/Model" + "\n";

  if (gbDisableInsuredAddress == 0 && loUiIFrameDoc.getElementById("txtInsuredAddress1") !== null && loUiIFrameDoc.getElementById("txtExposureCD").value == "1"){
      if (loUiIFrameDoc.getElementById("txtInsuredAddress1").value == ""){
         lsReq += "    Insured Address\n";
      }

      if (loUiIFrameDoc.getElementById("txtInsuredAddressZip").value == ""){
         lsReq += "    Insured Address Zip\n";
      }

      if (loUiIFrameDoc.getElementById("txtInsuredAddressCity").value == ""){
         lsReq += "    Insured Address City\n";
      }

      if (loUiIFrameDoc.getElementById("txtInsuredAddressState").value == ""){
         lsReq += "    Insured Address State\n";
      }
  }
  
  if (gbDisablePolicyNumber == 0 && loUiIFrameDoc.getElementById("txtPolicyNumber") !== null){
      if (loUiIFrameDoc.getElementById("txtPolicyNumber").value == ""){
         lsReq += "    Insured Policy Number\n";
      }
  }


	if (lsReq.length > 0)
	{
		lsMsg += "* The following fields are required:\n";
		lsMsg += lsReq;
	}

  if (lsPageName != "frmWaMainAddVehicle.asp")
  {
  	var sValue = loUiIFrameDoc.getElementById("txtLossDate").value;
  	if (sValue.length > 0)
  	{
  		var lsRe = /^(\d{2})\/(\d{2})\/(\d{4})/;
  		var lsRet = sValue.match(lsRe);
  
  		if (lsRet == null)
  			lsMsg += "* The Loss Date must be formated as MM/DD/YYYY.\n";
  		else if (sValue.substr(0,2) > 12)
  			lsMsg += "* The Loss Date month is incorrect.\n";
  		else if (sValue.substr(3,2) > 31)
  			lsMsg += "* The Loss Date day is incorrect.\n";
  		else if (sValue.substr(6,4) < 2000)
  			lsMsg += "* The Loss Date year is incorrect.\n";
  		else
  		{
  			var lsMyDate = new Date(loUiIFrameDoc.getElementById("txtLossDate").value);
  		  var liDateDiff = Math.round((new Date() - lsMyDate) / 24 / 60 / 60 / 1000);
  			if (liDateDiff < 0)
  				lsMsg += "* The Loss Date cannot be in the future.\n";
  		}
  	}
  
  	var liTxtAreaLength = loUiIFrameDoc.getElementById("txtLossDescription").value.length;
  	if (liTxtAreaLength > 500)
  	{
  		lsMsg += "* Only 500 characters are allowed for Loss Description;\n";
  		lsMsg += "    You have typed "+ liTxtAreaLength +" characters.\n";
  	}
  }
  
	var liTxtAreaLength = loUiIFrameDoc.getElementById("txtRemarks").value.length;
	if (liTxtAreaLength > 500)
	{
		lsMsg += "* Only 500 characters are allowed for Special Instructions;\n";
		lsMsg += "    You have typed "+ liTxtAreaLength +" characters.\n";
	}
   
   if (loUiIFrameDoc.getElementById("txtRepairLocationCity")){
      if (loUiIFrameDoc.getElementById("txtRepairLocationCity").value == "" ||
          loUiIFrameDoc.getElementById("txtRepairLocationState").value == "" || 
          loUiIFrameDoc.getElementById("txtRepairLocationCounty").value == ""){
          lsMsg += "* Repair Location Information (City/State/County) is required.\n" +
                   "  Use Zip Code to lookup City/State/County or provide these individually.\n";
      }
   }

  if (gbLaborRatesSetupFlag == 0)
  {
      if (loUiIFrameDoc.getElementById("txtLaborTax")){
        	var liLaborTax = loUiIFrameDoc.getElementById("txtLaborTax").value;
        	if (liLaborTax > 999)
        	{
        		lsMsg += "* The Labor Tax rate entered exceeds the $999 maximum value.\n";
        	}
        
        	var liPartsTax = loUiIFrameDoc.getElementById("txtPartsTax").value;
        	if (liPartsTax > 999)
        	{
        		lsMsg += "* The Parts Tax rate entered exceeds the $999 maximum value.\n";
        	}
        
        	var liMaterialsTax = loUiIFrameDoc.getElementById("txtMaterialsTax").value;
        	if (liMaterialsTax > 999)
        	{
        		lsMsg += "* The Materials Tax rate entered exceeds the $999 maximum value.\n";
        	}
        
        	var liPartsDiscount = loUiIFrameDoc.getElementById("txtPartsDiscount").value;
        	if (liPartsDiscount > 999)
        	{
        		lsMsg += "* The Parts Discount rate entered exceeds the $999 maximum value.\n";
        	}
        
        	var liGlassDiscount = loUiIFrameDoc.getElementById("txtGlassDiscount").value;
        	if (liGlassDiscount > 999)
        	{
        		lsMsg += "* The Glass Discount rate entered exceeds the $999 maximum value.\n";
        	}
      }
  }
  
	if (lsMsg.length > 0) // display errors if any
		alert(lsMsg);
	else // otherwise copy data to dataContainerFrame
	{
		if(parent)
		{
			var lsElmId;
			var loDcDataElm;
			var loDcIFrameDoc = getdcIFrameDoc();

			// update inputs

      if (gbEmergencyLiabilityScript == 1 && lsPageName != "frmWaMainAddVehicle.asp")
      {
        // insert radio delimited values into the hidden field 
        loDcIFrameDoc.getElementById("txtLiabilityFlag").value = liLiability;
        loDcIFrameDoc.getElementById("txtLiabilityText").value = loUiIFrameDoc.getElementById("txtLiabilityText").value;
        loDcIFrameDoc.getElementById("txtEmergencyFlag").value = liEmergency;
        loDcIFrameDoc.getElementById("txtEmergencyText").value = loUiIFrameDoc.getElementById("txtEmergencyText").value;
        loUiIFrameDoc.getElementById("txtSourceApplicationPassThruDataClm").value = lsRdLiability + "|" + lsRdEmergency;
        loDcIFrameDoc.getElementById("txtSourceApplicationPassThruDataClm").value = loUiIFrameDoc.getElementById("txtSourceApplicationPassThruDataClm").value;
      }

      if (gbReserveLineTypeScript == 1)
      {
        loDcIFrameDoc.getElementById("txtLineType").value = loElmLineType.value;
        loDcIFrameDoc.getElementById("txtReserve").value = loElmReserve.value;
        loUiIFrameDoc.getElementById("txtSourceApplicationPassThruData").value = lsLineType + "|"+ lsReserve;
        loDcIFrameDoc.getElementById("txtSourceApplicationPassThruData").value = loUiIFrameDoc.getElementById("txtSourceApplicationPassThruData").value;
      }

      if (loUiIFrameDoc.getElementById('txtExposureCD').value == "1")
        clrDedLimAmt(); //clear all Deductibles and Limits except Liability

      var loSelCov = loUiIFrameDoc.getElementById('txtCoverageProfileUiCD').value;
      var lsCovSelected, lsCovSelected2;

      if (loSelCov != "") {
        loSelCov = loSelCov.split("|")[0];
        var loSelCov2 = loUiIFrameDoc.getElementById('txtCoverageProfileUiCD');
        lsCovSelected = loSelCov2.options[loSelCov2.selectedIndex].text;
        switch (loSelCov) {
          case "COLL":
            lsCovSelected2 = "Collision";
            break;
          case "COMP":
            lsCovSelected2 = "Comprehensive";
            break;
          case "UIM":
            lsCovSelected2 = "UnderInsured";
            break;
          case "UM":
            lsCovSelected2 = "UnInsured";
            break;
          case "LIAB":
            lsCovSelected2 = "Liability";
            break;
          case "":
            lsCovSelected2 = "";
            break;
        }
      } else
        lsCovSelected2 = "";
        

			var lcolUiElms = loUiIFrameDoc.getElementsByTagName("INPUT");
		  var liUiElmsLength = lcolUiElms.length;
			for(var i=0; i<liUiElmsLength; i++)
			{
        if (lcolUiElms[i].getAttribute("opt") != "yes")
        {
          lsElmId = lcolUiElms[i].id;

          if (lsElmId.indexOf('Amt') != -1 && lsCovSelected2 != "")
            loDcIFrameDoc.getElementById("txt"+lsCovSelected2+lcolUiElms[i].name).value = lcolUiElms[i].value;

          if (loDcIFrameDoc.getElementById(lsElmId) == null){
            alert(lsElmId);
            return false;
          }
          loDcIFrameDoc.getElementById(lsElmId).value = lcolUiElms[i].value;
        }
			}

			// update selects
      var loUIPriDmgSel = loUiIFrameDoc.getElementById("txtPrimaryDamage");
			var liPriDmgSel = loUIPriDmgSel.options.value;

      var lsPriDmgSelected;
      if (loUIPriDmgSel.selectedIndex == "-1")
        lsPriDmgSelected = "";
      else
        lsPriDmgSelected = loUIPriDmgSel.options[loUIPriDmgSel.selectedIndex].text;

      loDcIFrameDoc.getElementById("txtPrimaryDamageDescription").value = lsPriDmgSelected;
			var lcolUiElmsSel = loUiIFrameDoc.getElementsByTagName("SELECT");
		  var liUiElmsSelLength = lcolUiElmsSel.length;
			var loSel;
      for(var z=0; z<liUiElmsSelLength; z++)
			{
        lsElmId = lcolUiElmsSel[z].id;
        if (lcolUiElmsSel[z].getAttribute("opt") != "yes")
        {
          if (lcolUiElmsSel[z].name == "SecondaryDamage")
          {
            loSel = lcolUiElmsSel[z];
            var lsSecDmg = "";
            var lsSecDmgDesc = "";
            var loSelLength = loSel.length;
            for (var x=0; x<loSelLength; x++)
            {
              if (loSel[x].selected == true)
              {
          			if (loSel.options[x].value != liPriDmgSel)
                {
                  if (lsSecDmg != "")
                  {
            				lsSecDmg += "," + loSel.options[x].value;
                    lsSecDmgDesc += ", " + loSel.options[x].text;
            			}
                  else
                  {
            				lsSecDmg += loSel.options[x].value;
            				lsSecDmgDesc += loSel.options[x].text;
                  }
                }
              }
            }
            loDcIFrameDoc.getElementById(lsElmId).value = lsSecDmg;
            loDcIFrameDoc.getElementById("txtSecondaryDamageDescription").value = lsSecDmgDesc;
          }
          else
          {
            loDcIFrameDoc.getElementById(lsElmId).value = lcolUiElmsSel[z].value;
            if (loDcIFrameDoc.getElementById(lsElmId+"Description"))
            {
              loSel = lcolUiElmsSel[z];
              loDcIFrameDoc.getElementById(lsElmId+"Description").value = loSel.options[loSel.selectedIndex].text;
            }
          }
        }
			}

      // update textarea
      loDcIFrameDoc.getElementById("txtLossDescription").value =  loUiIFrameDoc.getElementById("txtLossDescription").value;
      loDcIFrameDoc.getElementById("txtRemarks").value =  loUiIFrameDoc.getElementById("txtRemarks").value;

			window.frames["dataContIfr"].updImpactValues();

			dspMsg();

      if (lsPageName == "frmWaMainAddVehicle.asp")
        document.getElementById("uiFrame").src = "frmWaDASummaryAddVeh.asp?fromPage=DeskAudit&vehCount=0,0";
      else
        document.getElementById("uiFrame").src = "frmWaDeskAuditSummary.asp?fromPage=DeskAudit&vehCount=0,0";
		}
		else
			return false;
	}
}


function dspMsg()
{
  var objIFrame = parent.document.getElementById('uiFrame');
  objIFrame.style.height = "0";
  parent.showMsg();
}

function clrDedLimAmt()
{
  var loDcIFrameDoc = getdcIFrameDoc();
  loDcIFrameDoc.getElementById("txtCollisionDeductibleAmt").value = "";
  loDcIFrameDoc.getElementById("txtCollisionLimitAmt").value = "";
  loDcIFrameDoc.getElementById("txtComprehensiveDeductibleAmt").value = "";
  loDcIFrameDoc.getElementById("txtComprehensiveLimitAmt").value = "";
  loDcIFrameDoc.getElementById("txtUnderInsuredDeductibleAmt").value = "";
  loDcIFrameDoc.getElementById("txtUnderInsuredLimitAmt").value = "";
  loDcIFrameDoc.getElementById("txtUnInsuredDeductibleAmt").value = "";
  loDcIFrameDoc.getElementById("txtUnInsuredLimitAmt").value = "";
}


function getClaimSummaryData()
{
	var loUiIFrameDoc = getuiIFrameDoc();
	var loDcIFrameDoc = getdcIFrameDoc();
	var lsElmId;
	var loDcDataElm;

	// update claim
	var loUiClaimTbl = loUiIFrameDoc.getElementById("DeskAuditInfoTable");
	var lcolClaimElms = loUiClaimTbl.getElementsByTagName("INPUT");
	var liClaimElmsLength = lcolClaimElms.length;
	for(var i=0; i<liClaimElmsLength; i++)
	{
    lsElmId = lcolClaimElms[i].id;
		if (lsElmId == "txtExposureCD")
		{
			loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
			if (loDcDataElm.value == "1") lcolClaimElms[i].value = "1st Party";
			else if (loDcDataElm.value == "3") lcolClaimElms[i].value = "3rd Party";
		}
		else if (lsElmId == "txtCoverageProfileUiCD")
		{
      loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
			if (loDcDataElm.value == "COLL") lcolClaimElms[i].value = "Collision";
			else if (loDcDataElm.value == "COMP") lcolClaimElms[i].value = "Comprehensive";
			else if (loDcDataElm.value == "LIAB") lcolClaimElms[i].value = "Liability";
			else if (loDcDataElm.value == "UIM") lcolClaimElms[i].value = "Underinsured";
			else if (loDcDataElm.value == "UM") lcolClaimElms[i].value = "Uninsured";
		}
		else if (lsElmId == "txtLiabilityFlag")
		{
      loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
			if (loDcDataElm.value == "0") lcolClaimElms[i].value = "No";
			else if (loDcDataElm.value == "1") lcolClaimElms[i].value = "Yes";
		}
		else if (lsElmId == "txtEmergencyFlag")
		{
			loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
			if (loDcDataElm.value == "0") lcolClaimElms[i].value = "No";
			else if (loDcDataElm.value == "1") lcolClaimElms[i].value = "Yes";
		}
    else
    {
      lcolClaimElms[i].value = loDcIFrameDoc.getElementById(lsElmId).value;
    }
	}
  loUiIFrameDoc.getElementById("txtPrimaryDamageDescription").value = loDcIFrameDoc.getElementById("txtPrimaryDamageDescription").value;
  loUiIFrameDoc.getElementById("txtSecondaryDamageDescription").value = loDcIFrameDoc.getElementById("txtSecondaryDamageDescription").value;
	loUiIFrameDoc.getElementById("txtCallerRelationToInsuredIDDescription").value = loDcIFrameDoc.getElementById("txtCallerRelationToInsuredIDDescription").value;
	loUiIFrameDoc.getElementById("txtLossDescription").value = loDcIFrameDoc.getElementById("txtLossDescription").value;
	loUiIFrameDoc.getElementById("txtRemarks").value = loDcIFrameDoc.getElementById("txtRemarks").value;

  if (gbEmergencyLiabilityScript == 1 && lsPageName != "frmWaMainAddVehicle.asp")
  {
    loUiIFrameDoc.getElementById("LiabilityText").innerHTML = loDcIFrameDoc.getElementById("txtLiabilityText").value;
    loUiIFrameDoc.getElementById("EmergencyText").innerHTML = loDcIFrameDoc.getElementById("txtEmergencyText").value;
  }

  if (gbReserveLineTypeScript == 1)
  {
    loUiIFrameDoc.getElementById("txtLineType").value = loDcIFrameDoc.getElementById("txtLineType").value;
    loUiIFrameDoc.getElementById("txtReserve").value = loDcIFrameDoc.getElementById("txtReserve").value;
  }
}


function getClaimInfoData()
{
  var loUiIFrameDoc = getuiIFrameDoc();
	var loDcIFrameDoc = getdcIFrameDoc();
	var lsElmId;
	var loDcDataElm;

	// update inputs
	var lcolUiElms = loUiIFrameDoc.getElementsByTagName("INPUT");
	var liUiElmsLength = lcolUiElms.length;
	for(var i=0; i<liUiElmsLength; i++)
	{
    if ((lcolUiElms[i].id).indexOf('Amt') != -1 || lcolUiElms[i].getAttribute("opt") != "yes")
    {
  		lsElmId = lcolUiElms[i].id;
      lcolUiElms[i].value = loDcIFrameDoc.getElementById(lsElmId).value;
    }
	}

	// update selects
	var dcSecDamage = loDcIFrameDoc.getElementById("txtSecondaryDamage").value;
	var dcSecDamageArr = dcSecDamage.split(",");
	var dcSecDamageArrLength = dcSecDamageArr.length;

	var lcolUiElmsSel = loUiIFrameDoc.getElementsByTagName("SELECT");
	var liUiElmsSelLength = lcolUiElmsSel.length;
	for(var z=0; z<liUiElmsSelLength; z++)
	{
		lsElmId = lcolUiElmsSel[z].id;
    if (lcolUiElmsSel[z].name == "SecondaryDamage")
    {
      var loSel = lcolUiElmsSel[z];
      var loSelLength = loSel.length;
      for (var x=0; x<loSelLength; x++)
      {
        for (var y=0; y<dcSecDamageArrLength; y++)
        {
          if (loSel.options[x].value == dcSecDamageArr[y])
            loSel[x].selected = true;
        }
      }
    }
    else
      lcolUiElmsSel[z].value = loDcIFrameDoc.getElementById(lsElmId).value;
	}

	if (loUiIFrameDoc.getElementById("txtPrimaryDamage").selectedIndex == -1)
    loUiIFrameDoc.getElementById("txtPrimaryDamage").selectedIndex = 0;
  
	// update textarea
	var lcolUiElmsTxtArea = loUiIFrameDoc.getElementsByTagName("TEXTAREA");
	var liUiElmsTxtAreaLength = lcolUiElmsTxtArea.length;
	for(var z=0; z<liUiElmsTxtAreaLength; z++)
	{
		lsElmId = lcolUiElmsTxtArea[z].id;
    lcolUiElmsTxtArea[z].value = loDcIFrameDoc.getElementById(lsElmId).value;
	}

  //If Emergency/Liability script exists
  if (gbEmergencyLiabilityScript == 1 && lsPageName != "frmWaMainAddVehicle.asp")
  {
    setRadioByValue(loUiIFrameDoc.forms[0].Liability, loDcIFrameDoc.getElementById("txtLiabilityFlag").value);
    setRadioByValue(loUiIFrameDoc.forms[0].Emergency, loDcIFrameDoc.getElementById("txtEmergencyFlag").value);
  }
}

//Set radio value by passing radio group and value
function setRadioByValue(rdBtnGroup, bValue)
{
  for (var x = 0; x < rdBtnGroup.length; x++)
  {
    if (rdBtnGroup[x].value == bValue)
      rdBtnGroup[x].checked = true;
  }
}

function chkClaimSubmitted()
{
	var loDcIFrameDoc = getdcIFrameDoc();
  var lsWasSubmitted = "false";

  if (loDcIFrameDoc.getElementById("txtClaimSubmitted").value == "true")
    lsWasSubmitted = "true";
  
  return lsWasSubmitted;
}


function chkReqParams()
{
  var lbReqParams = true;
  var lsReqFields1 = "";
  var lsReqFields2 = "";
  var lsReqFields3 = "";
  var loDcIFrameDoc = getdcIFrameDoc();

  var lsFNOLUserID = loDcIFrameDoc.getElementById("txtFNOLUserID").value;

  if (lsFNOLUserID == 0 || lsFNOLUserID == "")
  {
    lbReqParams = false;
    lsReqFields1 = "User ID";
  }

	var loVehTbl = loDcIFrameDoc.getElementById("tblVehicle");
  var lcolElms = loVehTbl.getElementsByTagName("INPUT");
	var liElmsLength = lcolElms.length;
	for(var y=0; y<liElmsLength; y++)
  {
    if (lcolElms[y].id.indexOf('txtAssignmentTypeID') != -1 && lcolElms[y].value == "")
    {
      lbReqParams = false;
      lsReqFields2 = "Assignment Type";
    }
    else if (lcolElms[y].id.indexOf('txtExposureCD') != -1 && lcolElms[y].value == "")
    {
      lbReqParams = false;
      lsReqFields3 = "Coverage";
    }
  }
  
  if (lbReqParams == false)
    return lsReqParams = "false" +"|"+ lsReqFields1 +"|"+ lsReqFields2 +"|"+ lsReqFields3;
  else
    return lsReqParams = "true";
}


function getXMLstr()
{
	var loDcIFrameDoc = getdcIFrameDoc();
  loDcIFrameDoc.getElementById("txtTimeFinished").value = getCurrentDT();
  var startTime = loDcIFrameDoc.getElementById("txtIntakeStartSeconds").value;
  var endTime = getCurrentTimeSec();
  var totalTime = parseInt((endTime - startTime) / 1000);
  loDcIFrameDoc.getElementById("txtIntakeEndSeconds").value = endTime;
  loDcIFrameDoc.getElementById("txtIntakeSeconds").value = totalTime;
  var lsEmergencyLiabilityMemoTxt = "";
  var lsReserveLineTypeMemoTxt = "";

  //update the txtCustomScriptingMemo field with data from all vehicles and claim
  if (gbEmergencyLiabilityScript == 1)
  {
    loDcIFrameDoc.getElementById("txtSourceApplicationPassThruData").value = loDcIFrameDoc.getElementById("txtSourceApplicationPassThruDataClm").value;

    lsEmergencyLiabilityMemoTxt = (loDcIFrameDoc.getElementById("txtLiabilityText").value).replace('<li>', ' ') + ": "; 
    if (loDcIFrameDoc.getElementById("txtLiabilityFlag").value == 1)
      lsEmergencyLiabilityMemoTxt += "Yes\n";
    else
      lsEmergencyLiabilityMemoTxt += "No\n";

    lsEmergencyLiabilityMemoTxt += (loDcIFrameDoc.getElementById("txtEmergencyText").value).replace('<li>', ' ') + ": ";
    if (loDcIFrameDoc.getElementById("txtEmergencyFlag").value == 1)
      lsEmergencyLiabilityMemoTxt += "Yes\n";
    else
      lsEmergencyLiabilityMemoTxt += "No\n";

    loDcIFrameDoc.getElementById("txtCustomScriptingMemoClm").value = lsEmergencyLiabilityMemoTxt;
    loDcIFrameDoc.getElementById("txtCustomScriptingMemo").value = loDcIFrameDoc.getElementById("txtCustomScriptingMemoClm").value;
  }
    
  if (gbReserveLineTypeScript == 1)
  {
    if (loDcIFrameDoc.getElementById("txtCustomScriptingMemo").value != "")
      loDcIFrameDoc.getElementById("txtCustomScriptingMemo").value += "\n\n";

    lsReserveLineTypeMemoTxt = (loDcIFrameDoc.getElementById("txtSourceApplicationPassThruData").value).replace('|', ' '); 
    loDcIFrameDoc.getElementById("txtCustomScriptingMemo").value = lsReserveLineTypeMemoTxt;
  }

  if ((gbEmergencyLiabilityScript == 1 || gbReserveLineTypeScript == 1) && loDcIFrameDoc.getElementById("txtCustomScriptingMemo").value != "")
  {
    loDcIFrameDoc.getElementById("txtShowScriptingMemoFlag").value = 1;
  }
  
  var lsXML = "<WebAssignment version='100'>";

	var loClaimTbl = loDcIFrameDoc.getElementById("tblClaim");
  if (loClaimTbl)
  {
    lsXML += "<Claim>";
    lsXML += "<NewClaimFlag><![CDATA[1]]></NewClaimFlag>";
    lsXML += GetXmlElements(loClaimTbl);
    var loGuidelinesTbl = loDcIFrameDoc.getElementById("tblGuidelines");
    lsXML += GetGuidelinesElements(loGuidelinesTbl);
    lsXML += "</Claim>";
  }

  if (loDcIFrameDoc.getElementById("txtExposureCD").value == "1") {
    //first party vehicle. must be 1.
    loDcIFrameDoc.getElementById("txtVehicleNumber").value = 1;
  } else {
    loDcIFrameDoc.getElementById("txtVehicleNumber").value = 2;
  }

	var loVehTbl = loDcIFrameDoc.getElementById("tblVehicle");
  if (loVehTbl)
  {
    lsXML += "<Vehicle ProgramShop='0'>";
    lsXML += GetXmlElements(loVehTbl);
      var loGuidelinesTbl = loDcIFrameDoc.getElementById("tblGuidelines");
      lsXML += GetXmlElements(loGuidelinesTbl);
    lsXML += "</Vehicle>";
  }
  lsXML += "</WebAssignment>";
  return lsXML;
}

function getNewVehicleXMLstr()
{
	var loDcIFrameDoc = getdcIFrameDoc();
  loDcIFrameDoc.getElementById("txtTimeFinished").value = getCurrentDT();
  var startTime = loDcIFrameDoc.getElementById("txtIntakeStartSeconds").value;
  var endTime = getCurrentTimeSec();
  var totalTime = parseInt((endTime - startTime) / 1000);
  loDcIFrameDoc.getElementById("txtIntakeEndSeconds").value = endTime;
  loDcIFrameDoc.getElementById("txtIntakeSeconds").value = totalTime;
  var lsCarrierRepUserID = loDcIFrameDoc.getElementById("txtCarrierRepUserID").value;
  var lsInsuranceCompanyID = loDcIFrameDoc.getElementById("txtInsuranceCompanyID").value;

  var lsXML = "<WebAssignment version='100'>";

	var loClaimTbl = loDcIFrameDoc.getElementById("tblClaim");
  if (loClaimTbl)
  {
    lsXML += "<Claim>";
    lsXML += "<LynxID><![CDATA[" + liLynxID + "]]></LynxID>";
    lsXML += "<ClientClaimNumber><![CDATA[" + lsClaimNumber + "]]></ClientClaimNumber>";
    lsXML += "<CarrierRepUserID><![CDATA[" + lsCarrierRepUserID + "]]></CarrierRepUserID>";
    lsXML += "<DataSource><![CDATA[Claim Point Web Assignment]]></DataSource>";
    lsXML += "<FNOLUserID><![CDATA[" + liUserID + "]]></FNOLUserID>";
    lsXML += "<InsuranceCompanyID><![CDATA[" + lsInsuranceCompanyID + "]]></InsuranceCompanyID>";
    lsXML += "<NewClaimFlag><![CDATA[0]]></NewClaimFlag>";
    lsXML += "<CoverageClaimNumber><![CDATA[" + lsClaimNumber + "]]></CoverageClaimNumber>";
    lsXML += "<LossDescription><![CDATA[" + lsLossDescription + "]]></LossDescription>";
    lsXML += "<LossDate><![CDATA[" + lsLossDate + "]]></LossDate>";
    lsXML += "<LossAddressState><![CDATA[" + lsLossState + "]]></LossAddressState>";
    lsXML += "<CallerNameFirst><![CDATA[" + lsCallerFName + "]]></CallerNameFirst>";
    lsXML += "<CallerNameLast><![CDATA[" + lsCallerLName + "]]></CallerNameLast>";
    lsXML += "<CallerRelationToInsuredIDDescription><![CDATA[" + lsCallerRelation + "]]></CallerRelationToInsuredIDDescription>";
    lsXML += "<InsuredNameFirst><![CDATA[" + lsInsuredFName + "]]></InsuredNameFirst>";
    lsXML += "<InsuredNameLast><![CDATA[" + lsInsuredLName + "]]></InsuredNameLast>";
    lsXML += "<InsuredBusinessName><![CDATA[" + lsInsuredBName + "]]></InsuredBusinessName>";
    lsXML += "<InsuredPhoneSumm><![CDATA[" + lsInsuredPhoneArea + "-" + lsInsuredPhoneEx + "-" + lsInsuredPhoneNum + "]]></InsuredPhoneSumm>";
    if (gbDisableInsuredAddress == 0 && loDcIFrameDoc.getElementById("txtExposureCD").value == "1"){
      lsXML += "<InsuredAddress1><![CDATA[" + loDcIFrameDoc.getElementById("txtInsuredAddress1").value + "]]></InsuredAddress1>";
      lsXML += "<InsuredAddress2><![CDATA[" + loDcIFrameDoc.getElementById("txtInsuredAddress2").value + "]]></InsuredAddress2>";
      lsXML += "<InsuredAddressCity><![CDATA[" + loDcIFrameDoc.getElementById("txtInsuredAddressCity").value + "]]></InsuredAddressCity>";
      lsXML += "<InsuredAddressState><![CDATA[" + loDcIFrameDoc.getElementById("txtInsuredAddressState").value + "]]></InsuredAddressState>";
      lsXML += "<InsuredAddressZip><![CDATA[" + loDcIFrameDoc.getElementById("txtInsuredAddressZip").value + "]]></InsuredAddressZip>";
    }
    lsXML += "<TimeFinished><![CDATA[" + getCurrentDT() + "]]></TimeFinished>"
    lsXML += "<CarrierName><![CDATA[" + lsCarrierName + "]]></CarrierName>"
    lsXML += "<CarrierOfficeName><![CDATA[" + lsCarrierOfficeName + "]]></CarrierOfficeName>"
    lsXML += "<CarrierRepNameFirst><![CDATA[" + lsCarrierRepNameFirst + "]]></CarrierRepNameFirst>"
    lsXML += "<CarrierRepNameLast><![CDATA[" + lsCarrierRepNameLast + "]]></CarrierRepNameLast>"
    lsXML += "<CarrierRepPhoneDay><![CDATA[" + lsCarrierRepPhoneDay + "]]></CarrierRepPhoneDay>"
    lsXML += "<CarrierRepEmailAddress><![CDATA[" + lsCarrierRepEmailAddress + "]]></CarrierRepEmailAddress>"
    lsXML += "<AssignmentDescription><![CDATA[" + lsAssignmentDesc + "]]></AssignmentDescription>"
    lsXML += "<CollisionDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtCollisionLimitAmt").value + "]]></CollisionDeductibleAmt>"
    lsXML += "<CollisionLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtCollisionLimitAmt").value + "]]></CollisionLimitAmt>"
    lsXML += "<ComprehensiveDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtComprehensiveDeductibleAmt").value + "]]></ComprehensiveDeductibleAmt>"
    lsXML += "<ComprehensiveLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtComprehensiveLimitAmt").value + "]]></ComprehensiveLimitAmt>"
    lsXML += "<LiabilityDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtLiabilityDeductibleAmt").value + "]]></LiabilityDeductibleAmt>"
    lsXML += "<LiabilityLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtLiabilityLimitAmt").value + "]]></LiabilityLimitAmt>"
    lsXML += "<UnderInsuredDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtUnderInsuredDeductibleAmt").value + "]]></UnderInsuredDeductibleAmt>"
    lsXML += "<UnderInsuredLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtUnderInsuredLimitAmt").value + "]]></UnderInsuredLimitAmt>"
    lsXML += "<UnInsuredDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtUnInsuredDeductibleAmt").value + "]]></UnInsuredDeductibleAmt>"
    lsXML += "<UnInsuredLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtUnInsuredLimitAmt").value + "]]></UnInsuredLimitAmt>"
    lsXML += "</Claim>";
  }

  var lbFirstPartyExist = false;
  var liNextVehNum = 0;
  var laVeh;
  var lsReserveLineTypeMemoTxt = "";
  
  for (var x=0; x<laVehicleList.length; x++) {
    laVeh = laVehicleList[x].split("|");
    if (laVeh[0] == "1") {
      lbFirstPartyExist = true;
    }
  }
  
  if (gbReserveLineTypeScript == 1)
  {
    if (loDcIFrameDoc.getElementById("txtCustomScriptingMemo").value != "")
      loDcIFrameDoc.getElementById("txtCustomScriptingMemo").value += "\n\n";

    lsReserveLineTypeMemoTxt = (loDcIFrameDoc.getElementById("txtSourceApplicationPassThruData").value).replace('|', ' '); 
    loDcIFrameDoc.getElementById("txtCustomScriptingMemo").value = lsReserveLineTypeMemoTxt;
  }

  if ((gbEmergencyLiabilityScript == 1 || gbReserveLineTypeScript == 1) && loDcIFrameDoc.getElementById("txtCustomScriptingMemo").value != "")
  {
    loDcIFrameDoc.getElementById("txtShowScriptingMemoFlag").value = 1;
  }

  liNextVehNum = parseInt(laVehicleList[laVehicleList.length - 1].split("|")[0], 10) + 1;

	var loVehTbl = loDcIFrameDoc.getElementById("tblVehicle");
  if (loVehTbl)
  {
    var oVehNum = loDcIFrameDoc.getElementById("txtVehicleNumber");
    if (loDcIFrameDoc.getElementById("txtExposureCD").value == "1"){
      oVehNum.value = 1;
    } else {
      oVehNum.value = liNextVehNum;
    }
      
    lsXML += "<Vehicle ProgramShop='0'>";
    lsXML += GetXmlElements(loVehTbl);
      var loGuidelinesTbl = loDcIFrameDoc.getElementById("tblGuidelines");
      lsXML += GetXmlElements(loGuidelinesTbl);
    lsXML += "</Vehicle>";
  }
  lsXML += "</WebAssignment>";
  return lsXML;
}

function GetXmlElements(oName)
{

  var lsElms = ""
  var lcolElms = oName.getElementsByTagName("INPUT");
	var liElmsLength = lcolElms.length;

  
	for(var y=0; y<liElmsLength; y++)
	{
		if (lcolElms[y].getAttribute("opt") != "yes")
      lsElms += "<" + lcolElms[y].name + ">" + "<![CDATA[" + lcolElms[y].value + "]]>" + "</"+ lcolElms[y].name + ">";
  }

  var lcolElmsTxt = oName.getElementsByTagName("TEXTAREA");
	var liElmsTxtLength = lcolElmsTxt.length;
	for(var z=0; z<liElmsTxtLength; z++)
	{
		if (lcolElmsTxt[z].getAttribute("opt") != "yes")
      lsElms += "<" + lcolElmsTxt[z].name + ">" + "<![CDATA[" + lcolElmsTxt[z].value + "]]>" + "</"+ lcolElmsTxt[z].name + ">";
  }
  return lsElms;
}


function GetGuidelinesElements(oName)
{
  var liSystemLaborRates;
  try {
     if (liLaborRatesSetupFlag == "1")
         liSystemLaborRates = 1;
     else
         liSystemLaborRates = 0
  } catch (e) {}
  
  var lsElms = ""
  var lsHasElms = "false";
  lsElms += "<Remarks><![CDATA[";
  if (liSystemLaborRates == 0){
     var lcolElms = oName.getElementsByTagName("INPUT");
   	var liElmsLength = lcolElms.length;
   	for(var y=0; y<liElmsLength; y++)
   	{
       if (lcolElms[y].value != "")
       {
         lsHasElms = "true";
         lsElms += lcolElms[y].name + ":" + lcolElms[y].value + ", ";
       }
     }
     if (lsHasElms == "true")
       lsElms = lsElms.substring(0, lsElms.length-2);
  } else {
   lsElms += "See profile for client approved rates.";
  }
  lsElms += "]]></Remarks>";
  return lsElms;
}


function getSubmitDate()
{
	var loUiIFrameDoc = getuiIFrameDoc();
	var loDcIFrameDoc = getdcIFrameDoc();
  var lsDate = loDcIFrameDoc.getElementById("txtTimeFinished").value;
  return getDisplayDate(lsDate);
}


function markClaimSubmitted(flag)
{
	var loDcIFrameDoc = getdcIFrameDoc();
  loDcIFrameDoc.getElementById("txtClaimSubmitted").value = flag;
}

