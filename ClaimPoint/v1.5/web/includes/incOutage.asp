
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
    <td width="20">&nbsp;</td>
		<td width="560" nowrap style="border-top:1px solid #CCCCCC; border-left:1px solid #CCCCCC; border-bottom: 2px solid #909090; border-right: 2px solid #909090;">

      <Div id="MsgScroller" style="width:560px; font-family: Verdana, Arial, Helvetica, sans-serif;">
      <marquee title="<%= lsOutageTitleMsg %>"
      
          <% If lsMaintenanceModeMsg <> "" Then %>
               bgcolor="#FF0000"
               style="font-size:15px; font-weight:bold; color:#FFFFFF; padding:1px;"
          <% Else %>
               bgcolor="#FFFFFF"
               style="font-size:12px; font-weight:bold; color:#FF0000; padding:1px;"
          <% End If %>
               width="100%">
      <%= lsOutageMsg %>
      </marquee>
      </Div>

		</td>
	</tr>
</table>
