<%
  'sInvalid = "bcdfghijklopqruvwxyz0134678BCEFGHIJKLMOQSVWXYZ"
  'sValid   = "2RDPsANtame95nUT"

  sInvalid = chr(229) & chr(222) & chr(224) & chr(212) & chr(213) & chr(231) & chr(201) & chr(233) &_
            chr(202) & chr(217) & chr(235) & chr(232) & chr(215) & chr(239) & chr(203) & chr(238) &_
            chr(221) & chr(219) & chr(225) & chr(226) & chr(211) & chr(234) & chr(240) & chr(237) 
  sValid   = chr(204) & chr(218) & chr(223) & chr(220) & chr(227) & chr(206) & chr(236) & chr(230) &_ 
             chr(210) & chr(228) & chr(205) & chr(216) & chr(207) & chr(214) & chr(209) & chr(208)

  randomize
  
  function int2hex(iNum) 
       Dim sHex
      sHex = mid(sValid,cint(iNum \ 16)+1,1) & mid(sValid,cint(iNum mod 16)+1,1)
      int2hex = sHex
  End Function
  
  function hex2int(sHex) 
       Dim iNum
      iNum = (instr(1,sValid,left(sHex,1))-1)*16 + instr(1,sValid,right(sHex,1))-1
      hex2int = iNum
  End Function
  
  Function fCrypt(sClave,sTmp)
    Dim iKeyChar, iStringChar, i, ii, sEncrypted, iCryptChar
    sEncrypted = ""
    ii = 1
    for i = 1 to Len(sTmp)
       iKeyChar    = Asc(mid(sClave,ii,1))
       iStringChar = Asc(mid(sTmp,i,1))
       iCryptChar  = iKeyChar Xor iStringChar
       sEncrypted  = sEncrypted & int2hex(iCryptChar)
       ii = ii + 1
       if ii > len(sClave) then ii = 1 end if
    next
    for i=1 to len(sEncrypted)
           ii = cint(rnd*(len(sEncrypted)))+1
           sEncrypted = left(sEncrypted,ii-1) & mid(sInvalid,cint(rnd*(len(sInvalid)))+1,1) & right(sEncrypted,len(sEncrypted)-(ii-1))
    next 
     fCrypt = sEncrypted
  End Function
  
  Function fDecrypt(sClave, sTmp)
    Dim iKeyChar, iStringChar, i, ii, sEncrypted, iCryptChar, sTmp2
    sEncrypted = ""
    sTmp2 = ""
    for i = 1 to Len(sTmp)
           if instr(1,sValid,mid(sTmp,i,1)) > 0 then
                sTmp2 = sTmp2 & mid(sTmp,i,1)
           end if
    next
    ii = 1
    for i = 1 to Len(sTmp2)/2
       iKeyChar    = Asc(mid(sClave,ii,1))
       iStringChar = hex2int(mid(sTmp2,((i-1)*2)+1,2))
       iCryptChar  = iKeyChar Xor iStringChar
       sEncrypted  = sEncrypted & chr(iCryptChar)
       ii = ii + 1
       if ii > len(sClave) then ii = 1 end if
    next
    fDecrypt = sEncrypted
  End Function
%>
