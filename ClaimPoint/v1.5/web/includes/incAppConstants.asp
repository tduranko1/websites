<%

  Dim ApplicationID, lsApplicationCD
  Dim liClaimViewLevel_Own, liClaimViewLevel_Office, liClaimViewLevel_All
  Dim liAssignmentType_ASDR, liAssignmentType_DER, liAssignmentType_IAFA, liAssignmentType_PSA
  Dim liAssignmentType_IAER, liAssignmentType_ASDA, liAssignmentType_DEA, liAssignmentType_IAEA, liAssignmentType_NPESA
  Dim liAssignmentType_IAPO,  liAssignmentType_IAACV, liAssignmentType_MEA, liAssignmentType_DRSER, liAssignmentType_SER
  Dim liAssignmentType_TL, liAssignmentType_RRP,liAssignmentType_CSA

  ApplicationID = "2"
  lsApplicationCD = "CP"

  liClaimViewLevel_Own = "5"
  liClaimViewLevel_Office = "6"
  liClaimViewLevel_All = "7"
  
  liAssignmentType_ASDR = "1"
  liAssignmentType_DER = "2"
  liAssignmentType_IAFA  = "3"
  liAssignmentType_PSA  = "4"
  liAssignmentType_IAER  = "5"
  liAssignmentType_ASDA  = "6"
  liAssignmentType_DEA  = "7"
  liAssignmentType_IAEA  = "8"
  liAssignmentType_IAPO  = "9"
  liAssignmentType_IAACV  = "10"
  liAssignmentType_MEA  = "11"
  liAssignmentType_DRSER  = "12"
  liAssignmentType_SER  = "13"
  liAssignmentType_TL  = "14"
  liAssignmentType_NPESA = "15"
  liAssignmentType_RRP = "16"  
  liAssignmentType_CSA = "17"
  
%>