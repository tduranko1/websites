// v1.4.5.0

  function showCalIframe(obj, DateElmId, blnFuture)
  {
    if(DateElmId != "" && document.getElementById(DateElmId).disabled == true) return;
    var iDivPosY;
    if( findPosY(obj) > (getWindowSize("height") + getScrollXY("y") - 180) )
      iDivPosY = getWindowSize("height") + getScrollXY("y") - 180;
    else
      iDivPosY = findPosY(obj) + 22;

    if (blnFuture != true) blnFuture = false;
    document.getElementById("CalFrame").style.top = iDivPosY;
    document.getElementById("CalFrame").style.left = findPosX(obj) + 4;
    document.getElementById("CalFrame").src = "frmCalendar.asp?DateElmId=" + DateElmId + "&AllowFutureDates=" + (blnFuture ? 1 : 0);
    document.getElementById("CalFrame").style.display = "inline";
  
  }

  function getWindowSize(axis)
  {
    var myWidth = 0;
    var myHeight = 0;
    if( typeof( window.innerWidth ) == 'number' )
    {
      //Non-IE
      myWidth = window.innerWidth;
      myHeight = window.innerHeight;
    }
    else if (document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ))
    {
      //IE 6+ in 'standards compliant mode'
      myWidth = document.documentElement.clientWidth;
      myHeight = document.documentElement.clientHeight;
    }
    else if( document.body && ( document.body.clientWidth || document.body.clientHeight ))
    {
      //IE 4 compatible
      myWidth = document.body.clientWidth;
      myHeight = document.body.clientHeight;
    }
    
    if (axis == "width")
      return myWidth;
    else
      return myHeight;
  }
  
  function getScrollXY(axis)
  {
    var scrOfX = 0;
    var scrOfY = 0;
    if( typeof( window.pageYOffset ) == 'number' )
    {
      //Netscape compliant
      scrOfY = window.pageYOffset;
      scrOfX = window.pageXOffset;
    }
    else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) )
    {
      //DOM compliant
      scrOfY = document.body.scrollTop;
      scrOfX = document.body.scrollLeft;
    }
    else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ))
    {
      //IE6 standards compliant mode
      scrOfY = document.documentElement.scrollTop;
      scrOfX = document.documentElement.scrollLeft;
    }
    if (axis == "x")
      return scrOfX;
    else
      return scrOfY;
  
    //return [ scrOfX, scrOfY ];
  }

  function hideCalFrame()
  {
    if (window.document.activeElement.id.indexOf("calCall") == -1)
    {
      if (document.getElementById("CalFrame").style.display == "inline")
      {
        document.getElementById("CalFrame").style.display = "none";
        document.getElementById("CalFrame").src = "blank.asp";
      }
    }
  }


  function findPosX(obj)
  {
  	var curleft = 0;
  	if (obj.offsetParent)
  	{
  		while (obj.offsetParent)
  		{
  			curleft += obj.offsetLeft;
  			obj = obj.offsetParent;
  		}
  	}
  	else if (obj.x)
  		curleft += obj.x;
  	
    return curleft;
  }
  
  function findPosY(obj)
  {
  	var curtop = 0;
  	if (obj.offsetParent)
  	{
  		while (obj.offsetParent)
  		{
  			curtop += obj.offsetTop;
  			obj = obj.offsetParent;
  		}
  	}
  	else if (obj.y)
  		curtop += obj.y;
  	
    return curtop;
  }  
  