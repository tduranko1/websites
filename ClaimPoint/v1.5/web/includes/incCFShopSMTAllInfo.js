// JavaScript Document
// v1.4.6.0

//this will attach the docNoBkSpace function to the document which will alter all documents that include this script file.
document.onkeydown = docNoBkSpace;
var gbDirtyFlag = false;		

//if (document.attachEvent)
//  document.attachEvent("onclick", top.hideAllMenuScriptlets);
//else
//  document.onclick = top.hideAllMenuScriptlets;
  
  
// Function that will prevent page to navigate one level back in history when backspace is pressed in a
// read-only field. This function must be called in body's onkeyDown event.
function docNoBkSpace()  {
    if ((event.altKey) && ((event.keyCode == 37) || (event.keyCode == 39))){ //alt+left arrow, alt+ right arrow
        event.returnValue = false;
        return;
    }
    if ((event.altKey) && (event.keyCode == 36)){
        event.returnValue = false;
        return;
    }

    if (event.keyCode == 8) {
    	var srcTagName;
    	srcTagName = event.srcElement.tagName;

        switch (srcTagName) {
            case 'INPUT':
        		if (event.srcElement.readOnly == true ||
        			event.srcElement.type == 'checkbox' ||
        			event.srcElement.type == 'radio' ||
        			event.srcElement.type == 'button')
        		    event.returnValue = false;
        		break;
            case 'TEXTAREA':
        		if (event.srcElement.readOnly == true)
        			event.returnValue = false;
                break;
            default:  event.returnValue = false;
        }
    }
}

function NumbersOnly(event)
{
  var bOK = false;
	//if ((event.keyCode >= 45 && event.keyCode <= 57) && (event.keyCode != 47))  bOK = true;
	if (event.keyCode >= 48 && event.keyCode <= 57)  bOK = true;

    try {
        if (event.srcElement.allowDecimal != "undefined")
        {
        if (event.srcElement.allowDecimal == true){
            if (event.keyCode == 46) bOK = true;
        }
        }
        else {
	        if (event.keyCode == 46) bOK = false;
        }
    }
    catch(e) {
        bOK = false;
    }
    if (event.keyCode == 13) bOK = true;  //pass thru an <Enter> key
	if (event.shiftKey == true || bOK == false )
		event.returnValue = false;
}


function CheckMilitaryTime()
{
	// Checks if time is valid Military time (no colon(:))

	var obj = eval('document.all.' + event.srcElement.id);

	var timeStr = String(obj.value);
	if (timeStr == "") return true;

	var nums = '0123456789';
	var len = timeStr.length;

	if (len != 4){
        alert("This is not a valid Military time.  Must be in the form 'hhmm'.");
		obj.select();
		return false;
	}

	for (i=0;i<len;i++){
		if (nums.indexOf(timeStr.charAt(i)) == -1){
            alert('Military times must contain only numbers.');
			obj.select();
			return false;
		}
	}

	var mins = timeStr.substr(len-2);
	var hrs = timeStr.substr(0, len-2);
    if (hrs < 0  || hrs > 24)
	{
        alert("Hour must be between 0 and 24 for military time");
		obj.select();
		return false;
	}

	if (mins < 0 || mins > 59)
	{
        alert("Minute must be between 0 and 59.");
		obj.select();
		return false;
	}

	if (hrs == 24 && mins != 0)
	{
        alert("2400 is the maximum value allowed for military time.");
		obj.select();
		return false;
	}

	return true;
}


//checks if the input is an area code
function isAreaCode(sValue) {
	var sVal = "";
	sVal = parseNumber(sValue);

	if (sVal == "") return false;
	if (!(sVal.length == 3)) return false;
	//area code cannot start with 0 or 1
	if (parseInt(sVal.substr(0,1)) < 2) return false;

	return true;
}

//checks if the input is a phone exchange
function isPhoneExchange(sValue) {
	var sVal = "";
	sVal = parseNumber(sValue);

	if (sVal == "") return false;
	if ((sVal.length != 3)) return false;
	//if (sValue == "000") return false;

	return true;
}

//checks if the input is a phone number
function isPhoneNumber(sValue) {
	var sVal = "";
	sVal = parseNumber(sValue);

	if (sVal == "") return false;
	if ((sVal.length != 4)) return false;

	return true;
}

//Check Area code
var bInCheckAreaCode = false;
function checkAreaCode(obj) {
  var sVal;
  var sType = obj.getAttribute("type");
  if (sType == "password") return;
  if (obj.getAttribute("ValidityStatus") == "pending") return;
  
  //get the next element that will receive focus. ActiveElement will hold the next element in focus.
  //by the time this function is executed, the next element would have become the activeElement. This was the reason
  //for infinite loop when typing invalid stuff very fast.
  var objTo = document.activeElement;
  if (objTo) objTo.ValidityStatus = "pending";

	sVal = obj.value;
	if (sVal.length > 0)
	if (!isAreaCode(sVal) && !(bInCheckAreaCode)) {

    event.returnValue = false;
    bInCheckAreaCode = true;
    alert("Invalid phone area code specified.");
		if (obj !== objTo)
      obj.select();
		obj.focus();
    obj.ValidityStatus = "";
    bInCheckAreaCode = false;
	}
  else {
    obj.setAttribute("prevValue", obj.value, 0);
    if (objTo) {
      // if the area code was valid then reset the ValidityStatus of the next element so that it can receive focus
      objTo.ValidityStatus = "";
    }
  }
}

//Check Phone Exchange
var bInCheckPhoneExchange = false;
function checkPhoneExchange(obj) {
	var sVal;
  var sType = obj.getAttribute("type");
  if (sType == "password") return;
  if (obj.getAttribute("ValidityStatus") == "pending") return;

  //get the next element that will receive focus. ActiveElement will hold the next element in focus.
  //by the time this function is executed, the next element would have become the activeElement. This was the reason
  //for infinite loop when typing invalid stuff very fast.
  var objTo = document.activeElement;
  if (objTo) objTo.ValidityStatus = "pending";

	sVal = obj.value;
	if (sVal.length > 0)
	if (!isPhoneExchange(sVal) && !(bInCheckPhoneExchange)) {
    event.returnValue = false;
    bInCheckPhoneExchange = true;
    alert("Invalid phone exchange specified.");
		obj.select();
		obj.focus();
    obj.ValidityStatus = "";
    bInCheckPhoneExchange = false;
	}
  else {
    obj.setAttribute("prevValue", obj.value, 0);
    if (objTo) {
      // if the area code was valid then reset the ValidityStatus of the next element so that it can receive focus
      objTo.ValidityStatus = "";
    }
  }
}

//Check Phone Number
var bInCheckPhoneNumber = false;
function checkPhoneNumber(obj) {
	var sVal;
  var sType = obj.getAttribute("type");
  if (sType == "password") return;
  if (obj.getAttribute("ValidityStatus") == "pending") return;

  //get the next element that will receive focus. ActiveElement will hold the next element in focus.
  //by the time this function is executed, the next element would have become the activeElement. This was the reason
  //for infinite loop when typing invalid stuff very fast.
  var objTo = document.activeElement;
  if (objTo) objTo.ValidityStatus = "pending";

	sVal = obj.value;
	if (obj.type == "password") return;
	if (sVal.length > 0)
	if (!isPhoneNumber(sVal) && !(bInCheckPhoneNumber)) {
    event.returnValue = false;
    bInCheckPhoneNumber = true;
    alert("Invalid phone number specified.");
		obj.select();
		obj.focus();
    obj.ValidityStatus = "";
    bInCheckPhoneNumber = false;
	}
  else {
    obj.setAttribute("prevValue", obj.value, 0);
    if (objTo) {
      // if the area code was valid then reset the ValidityStatus of the next element so that it can receive focus
      objTo.ValidityStatus = "";
    }
  }
}


//function that will find missing phone sections and focus
function validatePhoneSections(objAreaCode, objExchange, objUnit) {
	var srcElementID = '';
    srcElementID = document.activeElement.id;
		if (objAreaCode.type == "password") return true;

    if (objAreaCode.id == srcElementID)
        if (objAreaCode.value.length > 0)
            if (!isAreaCode(objAreaCode.value)){
                objAreaCode.onblur();
                return false;
            }
		
    if (objExchange.id == srcElementID)
        if (objExchange.value.length > 0)
            if (!isPhoneExchange(objExchange.value)){
                objExchange.onblur();
                return false;
            }
    if (objUnit.id == srcElementID)
        if (objUnit.value.length > 0)
            if (!isPhoneNumber(objUnit.value)){
                objUnit.onblur();
                return false;
            }
    
  	if ((objAreaCode.value.length == 0) || (objExchange.value.length == 0) || (objUnit.value.length == 0)) {
	
        //all the sections are empty - this is OK.
      if ((objAreaCode.value.length == 0) && (objExchange.value.length == 0) && (objUnit.value.length == 0)) return true;
        
        if (isAreaCode(objAreaCode.value) && isPhoneExchange(objExchange.value) && isPhoneNumber(objUnit.value)) return true;
        //some sections are empty - not OK.
        alert("Some sections of the phone number are missing.  Please enter a valid area code, exchange and phone number.");
        if ((objAreaCode.value.length == 0) || (!isAreaCode(objAreaCode.value))) {
            objAreaCode.focus();
            objAreaCode.select();
            return false;
        }
		if ((objExchange.value.length == 0) || (!isPhoneExchange(objExchange.value))) {
            objExchange.focus();
            objExchange.select();
            return false;
        }
        if ((objUnit.value.length == 0) || (!isPhoneNumber(objUnit.value))) {
            objUnit.focus();
            objUnit.select();
            return false;
        }
        //this line will never be executed. Just in case!!.
        //objAreaCode.focus();
        //objAreaCode.select();
        return false;
    }
    else {
		if (!isAreaCode(objAreaCode.value)) {
            alert("Invalid phone area code specified.");
            objAreaCode.focus();
            objAreaCode.select();
            return false;
        }
		if (!isPhoneExchange(objExchange.value)) {
            alert("Invalid phone exchange specified.");
            objExchange.focus();
            objExchange.select();
            return false;
        }
        if (!isPhoneNumber(objUnit.value)) {
            alert("Invalid phone number specified.");
            objUnit.focus();
            objUnit.select();
            return false;
        }
    }
    return true;
}


function isNumeric(sValue) {
	/*for (var i=0; i < sValue.length; i++){
        if (sValue.substr(i,1) == '.')
            continue;
		if (isNaN(parseInt(sValue.substr(i, 1))))
			return false;
    }

	return true;*/
    return !isNaN(sValue);
}


//Parse and return only numeric characters found as a string.
function parseNumber(sValue) {
	var strRet = "";
	for (var i=0; i < sValue.length; i++)
		if (!isNaN(parseInt(sValue.substr(i, 1))))
			strRet += sValue.substr(i, 1);
	return strRet;
}

//this function simulates the NumbersOnly functionality. This function is intended to be used by the onpaste event.
function parseNumbersOnly(sValue){
	var strRet = "";
    var iChrCode;
    var iNxtChrCode;
    var bAllowDecimal = false;
    var sdbType = "";
    var iPrecision = 0;
    var iScale = 0;
    var bDecimal = false;

    bAllowDecimal = event.srcElement.getAttribute("allowDecimal");
    sdbType = event.srcElement.getAttribute("dbtype");
    iPrecision = parseInt(event.srcElement.getAttribute("Precision"));
    iScale = parseInt(event.srcElement.getAttribute("Scale"));
	for (var i=0; i < sValue.length; i++){
        iChrCode = sValue.charCodeAt(i);

        if (bDecimal == true) {
            //checks if the decimal scale does not exceed.
            if ((strRet.split(".")[1].length) >= iScale)
                return strRet;
        }
        if ((strRet.indexOf(".") == -1) && (strRet.length >= (iPrecision-iScale))) {
            if (iChrCode >= 48 && iChrCode <= 57 && (iScale > 0) && (sdbType == "money")) {
                strRet += ".";
                bDecimal = true;
            }
        }

        if (iChrCode >= 48 && iChrCode <= 57)
            strRet += sValue.substr(i, 1);
        else {
            if (i < sValue.length)
                iNxtChrCode = sValue.charCodeAt(i+1);
            else
                iNxtChrCode = -1;
            switch (iChrCode){
                case 45:// "-"
                    break;
                case 46:// "."
                        //for shop maintenance screens we don't have dbtype, precision and scale.
                        if ((bAllowDecimal == true) && (strRet.indexOf('.') == -1)){ // a decimal does not exist already
                            if (iNxtChrCode >= 48 && iNxtChrCode <= 57){
                                strRet += '.';
                                bDecimal = true;
                            }
                        }
                        if ((sdbType == "money") && (iScale > 0) && (strRet.indexOf('.') == -1)) {// a decimal does not exist already
                            if (iNxtChrCode >= 48 && iNxtChrCode <= 57){
                                strRet += '.';
                                bDecimal = true;
                            }
                        }
                        if (sdbType == "tinyint") {
                            //rounds to the nearest integer and returns
                            if (iNxtChrCode >= 53 && iNxtChrCode <= 57)
                                strRet = (parseInt(strRet) + 1).toString();
                            return strRet;
                        }
                        if (sdbType == "varchar")
                            continue;
                    break;
                case 47:// "/"
                    break;
                default:
                    if (sdbType != "varchar")
                        return strRet;
            }//switch
        }//else
    }//for
	return strRet;
}

//this function will attach onpaste and onbeforepaste events to all INPUTs who have NumbersOnly function attached
//to the onkeypress event.
//Call this function from a page's onLoad event.
function onpasteAttachEvents(){
    var oObjInputs = null;
    var strOnKeyPress = '';
    oObjInputs = document.all.tags('INPUT');
    for (var i = 0; i < oObjInputs.length; i++) {
        try {
            strOnKeyPress = '';
            strOnKeyPress = oObjInputs[i].onkeypress;
            if (strOnKeyPress != null) {
                if (strOnKeyPress.toString().toUpperCase().indexOf('NUMBERSONLY') > 0){
                    oObjInputs[i].attachEvent('onbeforepaste', onBeforePasteNumbersOnly);
                    //remove the generic paste event which was set in the InitFields().
                    oObjInputs[i].onpaste = new Function("");
                    //attach the more specific onpaste function.
                    oObjInputs[i].attachEvent('onpaste', onPasteNumbersOnly);
                }
            }
        }
        catch (e) {continue;}
    }
}

//this function will attach onpaste and onbeforepaste events to the object specified.
function attachPasteEvents(obj){
    if (obj) {
        obj.attachEvent('onbeforepaste', onBeforePasteNumbersOnly);
        //remove the generic paste event which was set in the InitFields().
        obj.onpaste = new Function("");
        //attach the more specific onpaste function.
        obj.attachEvent('onpaste', onPasteNumbersOnly);
    }
}


  //generic currency formating x.00
  function formatcurrency(X)
  {
    var roundNum = Math.round(X*100)/100; //round to 2 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.00';
    else if (strValue.indexOf('.') == strValue.length - 2)
      strValue = strValue + '0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 3);
    
    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }

  //generic hours formating x.0
  function formathours(X)
  {
    var roundNum = Math.round(X*10)/10; //round to 1 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 2);

    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }
  
  //generic percentage formating x.000
  function formatpercentage(X)
  {
    var roundNum = Math.round(X*1000)/1000; //round to 3 decimal places
    var strValue = new String(roundNum);
    if (strValue.indexOf('.') == -1)
      strValue = strValue + '.000';
    else if (strValue.indexOf('.') == strValue.length - 2)
      strValue = strValue + '00';
    else if (strValue.indexOf('.') == strValue.length - 3)
      strValue = strValue + '0';
    else
      strValue = strValue.substring(0, strValue.indexOf('.') + 6);

    if (strValue.indexOf('.') == 0)
      return '0' + strValue;
    else
      return strValue;
  }


function isURL(strURL) {
    if (strURL.search(/^((http:\/\/)|(www.))(\w+)*\.[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
      return true;
    else
      return false;
  }
  
  function isEmail(strEmail) {
    if (strEmail.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
      return true;
    else
      return false;
  }  
  
  function checkEMail(obj){
    if (obj.value != ""){
      if (!isEmail(obj.value)) {
        event.returnValue = false;
        alert(" Invalid E-Mail specified. \n Example: johndoe@yahoo.com \n Please try again");
        obj.focus();
        obj.select();
      }
    }
  }
  
  function checkURL(obj){
    if (obj.value != ""){
      if (!isURL(obj.value)) {
        event.returnValue = false;
        alert(" Invalid Internet website address (URL) specified. \n Example: http://www.yahoo.com \n\n Please try again");
        obj.focus();
        obj.select();
      }
    }
  }