<%@ Language=VBScript %>
<%
Option Explicit

On Error Resume Next

Response.Expires  = -1000
Response.Buffer = true

'External Event/Error Message constant
Dim lsExternalEventMsg, lsASPError
lsExternalEventMsg = ""
lsASPError = "<font color='#FF0000'>An ASP page error occured. Please try again. <br> If the problem persists, please report it to your supervisor.<font-color>"

const lcDelimiter= "~^~"
const lcUserType = 1

'crypt constants
Dim sValid, sInvalid

'page type constants
const lcAuthSection = 0
const lcBeforeInsuranceCompanySelect = 1
const lcAfterInsuranceCompanySelect = 2
const lcAfterClaimSearch = 3
const lcAssignmentPage = 4

'icon highlight constants
const lcClaim = 0
const lcVehicle = 1
const lcProperty = 2
const lcDocument = 3
const lcSubmitComment = 4

'Session variables
Dim lsInsCoConfigGUID 'InsuranceCompanyConfig call GUID

'misc variables
dim lsPageName, lsPageTitle, lsPageSubTitle
dim liPageType 'set the page type to one of the page type constants to get the right menu options
dim liPageIcon 'set the page icon to one of the icon highlight constants to get the right icon highlighted
dim lsMessage
dim liCounter, liStop
dim lsStyleSheet
dim lsHelpPage 'help page name

dim bModal 'Tells whether the screen is a modal
bModal = False

dim bGridSort 'Tells whether the page requires incGridSort included
bGridSort = False

dim bGridHover 'Tells whether the page requires incGridHover included
bGridHover = False

dim lsInsuranceCoName

lsMessage = ""

dim loAPD
set loAPD = Server.CreateObject("APD.clsAppData")
If Err.Number <> 0 Then
  lsExternalEventMsg = lsASPError
  'Response.Write "<p>ASP Error: " & Err.Description & "</p>"
  'Response.Write "<p>ASP Error: " & Err.Description & "</p>"
  'Response.Write "<p>ASP Error Number: " & Err.Number & "</p>"
End If

dim liLynxAppID 'AppID for LynxAPD configuration in aware

dim WshNetwork
dim strServerName
dim sSMTPServer

    
Set WshNetwork = Server.CreateObject("WScript.Network")
strServerName = WshNetwork.ComputerName
Set WshNetwork = Nothing

If Instr(1,strServerName, "DEV") > 0 Then	''' Development
		liLynxAppID = 16
		sSMTPServer = "rocexch03.pgw.local"
ElseIf Instr(1,strServerName, "STG") > 0 Then		''' Staging
		liLynxAppID = 16
		sSMTPServer = "penexch01.pgw.local"
ElseIf Instr(1,strServerName, "DR") > 0 Then		''' DR
		liLynxAppID = 16
		sSMTPServer = "penexch01.pgw.local"
ElseIf Instr(1,strServerName, "PRD") > 0 Or (uCASE(Left(strServerName,3)) = "BUY") Then	''' Production		
		liLynxAppID = 16
		sSMTPServer = "rocexch03.pgw.local"
Else	'default
		liLynxAppID = 16
		sSMTPServer = "rocexch03.pgw.local"
End if

'Maintenance Mode constants
Dim lbMaintenanceMode, lsMaintenanceMode
lbMaintenanceMode = false

%>

