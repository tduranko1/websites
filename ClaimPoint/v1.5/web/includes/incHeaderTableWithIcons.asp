<!-- v1.5.0.0 -->

<%
  Dim lsClaimIcon, lsPropertyIcon, lsVehicleIcon, lsDocumentIcon

  Function DisplayIconType(liPage)
    Dim lsIcon
    If liPageIcon = liPage Then
      lsIcon = "sel"
    Else
      lsIcon = "on"
    End If
    DisplayIconType = lsIcon
  End Function
  
  lsClaimIcon = DisplayIconType(lcClaim)
  lsVehicleIcon = DisplayIconType(lcVehicle)
  lsPropertyIcon = DisplayIconType(lcProperty)
  lsDocumentIcon = DisplayIconType(lcDocument)

%>

<script language="JavaScript">

  // Begin Image Mouseover Code
  image0 = new Image();
  image0.src = "images/icon_claim_sel.jpg";
  
  image1 = new Image();
  image1.src = "images/icon_vehicle_sel.jpg";
  
  image2 = new Image();
  image2.src = "images/icon_documents_sel.jpg";
  // end Image Mouseover Code

</script>

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" height1="110">
  <tr>
    <td width="268" height="45" valign="top">
    <IMG SRC="images/modal_TL2.gif" ALT="" width="90" height="45" border="0">
    </td>
    <td align="left" valign="top" width="328" height="92" rowspan="2">

      <table width="328" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
<!-- 
<a href="/html/red.htm" target="_blank" onmouseover="image0.src='images/pink.jpg';"
onmouseout="image0.src='images/red.jpg';">
<img name="image0" src="images/red.jpg" border=0 width=100 height=100></a>
 -->
              <a href="frmClaimInfo.asp?LynxID=<%= liLynxID %>&AssignmentType=<%= lsAssignmentType %>&AspectID=<%= liAspectID %>&VehNumber=<%= liVehNumber %>"
                 title="Click to view claim"
                 onMouseOver="window.status='Click to view claim'; return true"
                 onMouseOut="window.status=''; return true">
                 <IMG SRC="images/icon_claim_<%=lsClaimIcon%>.jpg" border="0" width="82" height="82">
               </a>
          </td>
          <td>
<!--               <a href="javascript:goFirstVehicle()" -->
              <a href="frmVehicleInfo.asp?LynxID=<%= liLynxID %>&AssignmentType=<%= lsAssignmentType %>&AspectID=<%= liAspectID %>&VehNumber=<%= liVehNumber %>"
                 title="Click to view vehicle"
                 onMouseOver="window.status='Click to view vehicle'; return true"
                 onMouseOut="window.status=''; return true">
                <IMG SRC="images/icon_vehicle_<%=lsVehicleIcon%>.jpg" border="0" width="82" height="82">
              </a>
          </td>
          <td>
<!--               <a href="javascript:goDocuments()" -->
              <a href="frmDocument.asp?LynxID=<%= liLynxID %>&AssignmentType=<%= lsAssignmentType %>&AspectID=<%= liAspectID %>&VehNumber=<%= liVehNumber %>"
                 title="Click to view documents"
                 onMouseOver="window.status='Click to view documents'; return true"
                 onMouseOut="window.status=''; return true">
                <IMG SRC="images/icon_documents_<%=lsDocumentIcon%>.jpg" border="0" width="84" height="82">
              </a>
          </td>
        </tr>
      </table>

    </td>
    <td>&nbsp;</td>
  </tr>
</table>

<span class="errorMessage"><%=lsMessage%></span>

<!-- include to check for and display if necessary APD offline/outage messages -->
<!-- this message will display under the page title -->
<!--#include file="incAPDOutageCheck.asp"-->
