
// JavaScript Document
// v1.5.0.0

var gsPrevPage = ""; //global previous page
var gsNextPage = ""; //global next page
var gsVehNum = "";
var gsVehCount = "";
var gsRetToClaim = "false";
var gsRetFromClaim = "false";
var gsEditVehMode = "false";
var gLogoutTimer = null;
var gsSrhZipCode = "";
var lsPageName;
var gbEmergencyLiabilityScript = 0;
var gbReserveLineTypeScript = 0;
var gsAddVeh = 0;

function resetTimeOut() {
    if (gLogoutTimer != null)
        window.clearTimeout(gLogoutTimer);
    gLogoutTimer = window.setTimeout("top.location = 'frmLogout.asp'", 240 * 60 * 1000);
}


function getdcIFrameDoc() {
    var loDcIFrameDoc;
    var loIFrameObj = document.getElementById("dataContIfr");
    if (loIFrameObj.contentDocument) // For NS6
        return loDcIFrameDoc = loIFrameObj.contentDocument;
    else if (loIFrameObj.contentWindow) // For IE5.5 and IE6
        return loDcIFrameDoc = loIFrameObj.contentWindow.document;
    else if (loIFrameObj.document) // For IE5
        return loDcIFrameDoc = loIFrameObj.document;
    else
        return;
}

function getuiIFrameDoc() {
    var loUiIFrameDoc;
    var loIFrameObj = document.getElementById("uiFrame");
    if (loIFrameObj.contentDocument) // For NS6
        return loUiIFrameDoc = loIFrameObj.contentDocument;
    else if (loIFrameObj.contentWindow) // For IE5.5 and IE6
        return loUiIFrameDoc = loIFrameObj.contentWindow.document;
    else if (loIFrameObj.document) // For IE5
        return loUiIFrameDoc = loIFrameObj.document;
    else
        return;
}

function tsClaimData(gsPrevPage) {
    var loUiIFrameDoc = getuiIFrameDoc();
    var lsMsg = "";
    var lsReq = "";
    var lsNumOnly = "";

    if (gbEmergencyLiabilityScript == 1) {
        var lcolElmsRdLiability = loUiIFrameDoc.getElementsByName("Liability");
        var liLiability;
        var lsRdLiability = "";
        var liRdLength = lcolElmsRdLiability.length;
        for (var i = 0; i < liRdLength; i++) {
            if (lcolElmsRdLiability.item(i).checked == true) {
                liLiability = lcolElmsRdLiability.item(i).value;
                lsRdLiability = "Liability=" + lcolElmsRdLiability.item(i).value;
            }
        }
        if (lsRdLiability == "")
            lsReq += "    " + "The Liability question must be answered." + "\n";

        lcolElmsRdEmergency = loUiIFrameDoc.getElementsByName("Emergency");
        var liEmergency;
        var lsRdEmergency = "";
        liRdLength = lcolElmsRdEmergency.length;
        for (var i = 0; i < liRdLength; i++) {
            if (lcolElmsRdEmergency.item(i).checked == true) {
                liEmergency = lcolElmsRdEmergency.item(i).value;
                lsRdEmergency = "Emergency=" + lcolElmsRdEmergency.item(i).value;
            }
        }
        if (lsRdEmergency == "")
            lsReq += "    " + "The Emergency question must be answered." + "\n";
    }

    if (loUiIFrameDoc.getElementById("txtCoverageClaimNumber").value.length == 0)
        lsReq += "    " + "Claim Number" + "\n";
    if (loUiIFrameDoc.getElementById("txtLossDate").value.length == 0)
        lsReq += "    " + "Loss Date" + "\n";
    if (loUiIFrameDoc.getElementById("txtLossAddressState").value.length == 0)
        lsReq += "    " + "Loss State" + "\n";
    if (loUiIFrameDoc.getElementById("txtLossDescription").value.length == 0)
        lsReq += "    " + "Description of Loss" + "\n";
    if ((loUiIFrameDoc.getElementById("txtCallerNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtCallerNameLast").value.length == 0))
        lsReq += "    " + "Assigner First and Last name" + "\n";
    if (loUiIFrameDoc.getElementById("txtCallerRelationToInsuredID").value.length == 0)
        lsReq += "    " + "Relation to Insured" + "\n";
    if ((loUiIFrameDoc.getElementById("txtInsuredNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtInsuredNameLast").value.length == 0) && loUiIFrameDoc.getElementById("txtInsuredBusinessName").value.length == 0)
        lsReq += "    " + "Insured First and Last name or Business name" + "\n";

    if (lsReq.length > 0) {
        lsMsg += "* The following fields are required:\n";
        lsMsg += lsReq;
    }

    var sValue = loUiIFrameDoc.getElementById("txtLossDate").value;
    if (sValue.length > 0) {
        var lsRe = /^(\d{2})\/(\d{2})\/(\d{4})/;
        var lsRet = sValue.match(lsRe);

        if (lsRet == null)
            lsMsg += "* The Loss Date must be formated as MM/DD/YYYY.\n";
        else if (sValue.substr(0, 2) > 12)
            lsMsg += "* The Loss Date month is incorrect.\n";
        else if (sValue.substr(3, 2) > 31)
            lsMsg += "* The Loss Date day is incorrect.\n";
        else if (sValue.substr(6, 4) < 2000)
            lsMsg += "* The Loss Date year is incorrect.\n";
        else {
            var lsMyDate = new Date(loUiIFrameDoc.getElementById("txtLossDate").value);
            var liDateDiff = Math.round((new Date() - lsMyDate) / 24 / 60 / 60 / 1000);
            if (liDateDiff < 0)
                lsMsg += "* The Loss Date cannot be in the future.\n";
        }
    }

    var lsPhone = loUiIFrameDoc.getElementById("txtInsuredPhoneAC").value + loUiIFrameDoc.getElementById("txtInsuredPhoneEN").value + loUiIFrameDoc.getElementById("txtInsuredPhoneUN").value;
    if (lsPhone.length > 0) {
        if (isNumeric(lsPhone) == false) //this will check for numeric only
            lsNumOnly += "* Phone number must be a numeric value.\n";
        else if (loUiIFrameDoc.getElementById("txtInsuredPhoneAC").value.length != 3 || loUiIFrameDoc.getElementById("txtInsuredPhoneEN").value.length != 3 || loUiIFrameDoc.getElementById("txtInsuredPhoneUN").value.length != 4) //this will check the format
        {
            lsMsg += "* Phone number is invalid.\n";
            lsMsg += "    Phone number must be formated as 999 999 9999.\n";
        }
    }

    var liTxtAreaLength = loUiIFrameDoc.getElementById("txtRemarks").value.length;
    if (liTxtAreaLength > 500) {
        lsMsg += "* Only 500 characters are allowed for Comments;\n";
        lsMsg += "    You have typed " + liTxtAreaLength + " characters.\n";
    }

    if (lsMsg.length > 0) // display errors if any
        alert(lsMsg);
    else // otherwise copy data to dataContainerFrame
    {
        if (parent) {
            var lsElmId;
            var loDcDataElm;
            var loDcIFrameDoc = getdcIFrameDoc();

            // update inputs
            if (gbEmergencyLiabilityScript == 1) {
                loUiIFrameDoc.getElementById("txtSourceApplicationPassThruData").value = lsRdLiability + "|" + lsRdEmergency;
                loDcIFrameDoc.getElementById("txtLiabilityFlag").value = liLiability;
                loDcIFrameDoc.getElementById("txtEmergencyFlag").value = liEmergency;
            }

            var lcolUiElms = loUiIFrameDoc.getElementsByTagName("INPUT");
            var liUiElmsLength = lcolUiElms.length;
            for (var i = 0; i < liUiElmsLength; i++) {
                if (lcolUiElms[i].getAttribute("opt") != "yes") {
                    lsElmId = lcolUiElms[i].id;

                    loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                    loDcDataElm.value = lcolUiElms[i].value;

                }
            }

            // update selects
            var lcolUiElmsSel = loUiIFrameDoc.getElementsByTagName("SELECT");
            var liUiElmsSelLength = lcolUiElmsSel.length;
            var loSel;
            for (var z = 0; z < liUiElmsSelLength; z++) {
                if (lcolUiElmsSel[z].getAttribute("opt") != "yes") {
                    lsElmId = lcolUiElmsSel[z].id;
                    loDcIFrameDoc.getElementById(lsElmId).value = lcolUiElmsSel[z].value;
                    if (loDcIFrameDoc.getElementById(lsElmId + "Description")) {
                        loSel = lcolUiElmsSel[z];
                        loDcIFrameDoc.getElementById(lsElmId + "Description").value = loSel.options[loSel.selectedIndex].text;
                    }
                }
            }

            // update textarea
            loDcIFrameDoc.getElementById("txtLossDescription").value = loUiIFrameDoc.getElementById("txtLossDescription").value;
            loDcIFrameDoc.getElementById("txtRemarks").value = loUiIFrameDoc.getElementById("txtRemarks").value;

            window.frames["dataContIfr"].updClaimValues();

            dspMsg(); //display Loading... message between page loads

            if (gsPrevPage == "ClaimSummary") {
                gsVehCount = getVehCount();
                document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ClaimInfo&vehCount=" + gsVehCount;
            }
            else if (gsPrevPage == "VehicleInfo") {
                document.getElementById("uiFrame").src = "frmWaVehicleInfo.asp?fromPage=retFromClaim";
            }
            else if (gsPrevPage == "ShopSelection") {
                document.getElementById("uiFrame").src = "frmWaVehicleInfo.asp?fromPage=ShopSelection&vehNum=1";
            }
            else {
                document.getElementById("uiFrame").src = "frmWaVehicleInfo.asp?fromPage=ClaimInfo";
            }
        }
        else
            return false;
    }
}


function tsVehicleData(gsRetToClaim, gsRetFromClaim, gsEditVehMode, AddNewVeh, currVehNum) {
    var loUiIFrameDoc = getuiIFrameDoc();

    var IsRepairEmailCell = loUiIFrameDoc.getElementById("txtConfigRepairReferral").value


    var lsMsg = "";
    var lsReq = "";

    var sContPhone = loUiIFrameDoc.getElementById("txtContactPhoneAC").value + loUiIFrameDoc.getElementById("txtContactPhoneEN").value + loUiIFrameDoc.getElementById("txtContactPhoneUN").value;
    var sContNightPhone = loUiIFrameDoc.getElementById("txtContactNightPhoneAC").value + loUiIFrameDoc.getElementById("txtContactNightPhoneEN").value + loUiIFrameDoc.getElementById("txtContactNightPhoneUN").value;
    var sContAltPhone = loUiIFrameDoc.getElementById("txtContactAltPhoneAC").value + loUiIFrameDoc.getElementById("txtContactAltPhoneEN").value + loUiIFrameDoc.getElementById("txtContactAltPhoneUN").value;




    if (gsRetToClaim != "true") {
        if (gbReserveLineTypeScript == 1) {
            var loElmLineType = loUiIFrameDoc.getElementById("txtLineType");
            if (loElmLineType) {
                var lsLineType = "LineType=" + loElmLineType.value;
                if (loElmLineType.value.length == 0)
                    lsReq += "    " + "Line Type" + "\n";
            }

            var loElmReserve = loUiIFrameDoc.getElementById("txtReserve");
            if (loElmReserve) {
                var lsReserve = "Reserve=" + loElmReserve.value;
                if (loElmReserve.value.length == 0)
                    lsReq += "    " + "Reserve Number" + "\n";
            }
        }

        if (loUiIFrameDoc.getElementById("txtExposureCD").value.length == 0)
            lsReq += "    " + "Party" + "\n";
        if (loUiIFrameDoc.getElementById("txtCoverageProfileUiCD").value.length == 0)
            lsReq += "    " + "Coverage" + "\n";
        if (loUiIFrameDoc.getElementById("txtDeductibleAmt").required == "true" && loUiIFrameDoc.getElementById("txtDeductibleAmt").value.length == 0)
            lsReq += "    " + "Deductible" + "\n";
        if (loUiIFrameDoc.getElementById("txtLimitAmt").required == "true" && loUiIFrameDoc.getElementById("txtLimitAmt").value.length == 0)
            lsReq += "    " + "Limit" + "\n";
        if ((loUiIFrameDoc.getElementById("txtOwnerNameFirst").value.length == 0 || loUiIFrameDoc.getElementById("txtOwnerNameLast").value.length == 0) && loUiIFrameDoc.getElementById("txtOwnerBusinessName").value.length == 0)
            lsReq += "    " + "Owner First and Last Name or Business Name" + "\n";
        if (loUiIFrameDoc.getElementById("txtContactNameFirst").value.length == 0)
            lsReq += "    " + "Contact First Name" + "\n";
        if (loUiIFrameDoc.getElementById("txtContactNameLast").value.length == 0)
            lsReq += "    " + "Contact Last Name" + "\n";
        if (sContPhone.length == 0 && sContNightPhone.length == 0 && sContAltPhone.length == 0)
            lsReq += "    " + "Contact Phone Number" + "\n";

        var lsMydate = new Date();
        var liCurrYear = lsMydate.getFullYear();
        if (loUiIFrameDoc.getElementById("txtVehicleYear").value.length > 0 && loUiIFrameDoc.getElementById("txtVehicleYear").value.length < 4)
            lsReq += "    " + "Vehicle Year requires 4 digits" + "\n";
        else if (loUiIFrameDoc.getElementById("txtVehicleYear").value < 1900 || loUiIFrameDoc.getElementById("txtVehicleYear").value > liCurrYear + 1)
            lsReq += "    " + "Vehicle Year must be between 1900 and " + (liCurrYear + 1) + ".\n";

        if (loUiIFrameDoc.getElementById("txtVehicleYear").value.length == 0 || loUiIFrameDoc.getElementById("txtMake").value.length == 0 || loUiIFrameDoc.getElementById("txtModel").value.length == 0)
            lsReq += "    " + "Vehicle Year/Make/Model" + "\n";
        if (loUiIFrameDoc.getElementById("txtDrivable").value.length == 0)
            lsReq += "    " + "Vehicle Drivable" + "\n";
        if (loUiIFrameDoc.getElementById("txtAssignmentTypeID").value != "16" && loUiIFrameDoc.getElementById("txtLicensePlateNumber").value.length == 0 && loUiIFrameDoc.getElementById("txtVIN").value.length == 0)
            lsReq += "    " + "License Plate or VIN" + "\n";
        if (loUiIFrameDoc.getElementById("txtPrimaryDamage").value.length == 0)
            lsReq += "    " + "Primary Impact Damage" + "\n";

        if (IsRepairEmailCell == "true") {
            if (loUiIFrameDoc.getElementById("txtPrefMethodUpd").value == "EM") {
                if (loUiIFrameDoc.getElementById("txtContactEmailAddress").value == "") {
                    lsReq += "    " + "Email" + "\n";
                }
            }

            if (loUiIFrameDoc.getElementById("txtContactEmailAddress").value != "") {
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                var EmailAddress = loUiIFrameDoc.getElementById("txtContactEmailAddress").value;
                if (reg.test(EmailAddress) == false) {
                    lsReq += "    " + "Invalid Email Address" + "\n";
                }
            }

            if (loUiIFrameDoc.getElementById("txtPrefMethodUpd").value == "CE") {
                if (loUiIFrameDoc.getElementById("txtCellAreaCode").value.length == 0 && loUiIFrameDoc.getElementById("txtCellExchangeNumber").value.length == 0 && loUiIFrameDoc.getElementById("txtCellUnitNumber").value.length == 0) {
                    lsReq += "    " + "Cell" + "\n";
                }
                if (loUiIFrameDoc.getElementById("txtCellPhoneCarrier").value.length == 0)
                    lsReq += "    " + "Cell phone carrier" + "\n";
            }
        }

        if (lsReq.length > 0) {
            lsMsg += "* The following fields are required:\n";
            lsMsg += lsReq;
        }

        var sOwnerPhone = loUiIFrameDoc.getElementById("txtOwnerPhoneAC").value + loUiIFrameDoc.getElementById("txtOwnerPhoneEN").value + loUiIFrameDoc.getElementById("txtOwnerPhoneUN").value;
        if (sOwnerPhone.length > 0) {
            if (isNumeric(sOwnerPhone) == false) //this will check for numeric only
                lsNumOnly += "* Phone number must be a numeric value.\n";
            else if (loUiIFrameDoc.getElementById("txtOwnerPhoneAC").value.length != 3 || loUiIFrameDoc.getElementById("txtOwnerPhoneEN").value.length != 3 || loUiIFrameDoc.getElementById("txtOwnerPhoneUN").value.length != 4) { //this will check the format
                lsMsg += "* The Owner Phone Number is invalid.\n";
                lsMsg += "    Phone number must be formated as 999 999 9999.\n";
            }
        }

        if (sContPhone.length > 0) {
            if (isNumeric(sContPhone) == false) //this will check for numeric only
                lsNumOnly += "* Phone number must be a numeric value.\n";
            else if (loUiIFrameDoc.getElementById("txtContactPhoneAC").value.length != 3 || loUiIFrameDoc.getElementById("txtContactPhoneEN").value.length != 3 || loUiIFrameDoc.getElementById("txtContactPhoneUN").value.length != 4) //this will check the format
            {
                lsMsg += "* The Contact Day Phone Number is invalid.\n";
                lsMsg += "    Phone number must be formated as 999 999 9999.\n";
            }
        }

        if (sContNightPhone.length > 0) {
            if (isNumeric(sContNightPhone) == false) //this will check for numeric only
                lsNumOnly += "* Phone number must be a numeric value.\n";
            else if (loUiIFrameDoc.getElementById("txtContactNightPhoneAC").value.length != 3 || loUiIFrameDoc.getElementById("txtContactNightPhoneEN").value.length != 3 || loUiIFrameDoc.getElementById("txtContactNightPhoneUN").value.length != 4) //this will check the format
            {
                lsMsg += "* The Contact Night Phone Number is invalid.\n";
                lsMsg += "    Phone number must be formated as 999 999 9999.\n";
            }
        }

        if (sContAltPhone.length > 0) {
            if (isNumeric(sContAltPhone) == false) //this will check for numeric only
                lsNumOnly += "* Phone number must be a numeric value.\n";
            else if (loUiIFrameDoc.getElementById("txtContactAltPhoneAC").value.length != 3 || loUiIFrameDoc.getElementById("txtContactAltPhoneEN").value.length != 3 || loUiIFrameDoc.getElementById("txtContactAltPhoneUN").value.length != 4) //this will check the format
            {
                lsMsg += "* The Contact Alt. Phone Number is invalid.\n";
                lsMsg += "    Phone number must be formated as 999 999 9999.\n";
            }
        }

        if (IsRepairEmailCell == "true") {
            var sContCellPhone = loUiIFrameDoc.getElementById("txtCellAreaCode").value + loUiIFrameDoc.getElementById("txtCellExchangeNumber").value + loUiIFrameDoc.getElementById("txtCellUnitNumber").value;
            if (sContCellPhone.length > 0) {
                if (isNumeric(sContCellPhone) == false) //this will check for numeric only
                    lsNumOnly += "* Cell number must be a numeric value.\n";
                else if (loUiIFrameDoc.getElementById("txtCellAreaCode").value.length != 3 || loUiIFrameDoc.getElementById("txtCellExchangeNumber").value.length != 3 || loUiIFrameDoc.getElementById("txtCellUnitNumber").value.length != 4) { //this will check the format
                    lsMsg += "* The Cell Phone Number is invalid.\n";
                    lsMsg += "    Cell number must be formated as 999 999 9999.\n";
                }
            }
        }

        if (parseInt(loUiIFrameDoc.getElementById("txtRentalDaysAuthorized").value) > 255) {
            lsMsg += "* Rental Days maximum value is 255.\n";
        }

        var liTxtAreaLength = loUiIFrameDoc.getElementById("txtRentalInstructions").value.length;
        if (liTxtAreaLength > 250) {
            lsMsg += "* Only 250 characters are allowed for Rental Instructions;\n";
            lsMsg += "    You have typed " + liTxtAreaLength + " characters.\n";
        }

        if (loUiIFrameDoc.getElementById("txtVIN").value.length > 0 && loUiIFrameDoc.getElementById("txtVIN").value.length < 6) {
            lsMsg += "* The Vehicle VIN is invalid.\n";
            lsMsg += "    At least the last 6 digits of the VIN is required.\n";
        }

 // Validation of Additional rental fields
        if (loUiIFrameDoc.getElementById("txtcb_Rental_additional").checked) {
            var lsRentalReq = "";
            var lsRentalMsg = "*Please fill below fields in Additional Rental Info." + "\n";
            if (loUiIFrameDoc.getElementById("txtRentalVendor").value.length == 0)
                lsRentalReq += "    " + "Rental Vendor" + "\n";
            if (loUiIFrameDoc.getElementById("txtRentalResConf").value.length == 0)
                lsRentalReq += "    " + "Rental Res/Conf" + "\n";
            if (loUiIFrameDoc.getElementById("txtRentalVehicleClass").value.length == 0)
                lsRentalReq += "    " + "Rental Vehicle Class" + "\n";
            if (loUiIFrameDoc.getElementById("txtRentalRateType").value.length == 0)
                lsRentalReq += "    " + "Rental Rate Type" + "\n";
            if (loUiIFrameDoc.getElementById("txtRentalRate").value.length == 0)
                lsRentalReq += "    " + "Rental Rate" + "\n";
            if (loUiIFrameDoc.getElementById("txtRentalAuthPickup").value.length == 0)
                lsRentalReq += "    " + "Rental Auth Pickup" + "\n";
             
            if (loUiIFrameDoc.getElementById("txtRentalPhoneAC").value.length != 3 || loUiIFrameDoc.getElementById("txtRentalPhoneEN").value.length != 3 || loUiIFrameDoc.getElementById("txtRentalPhoneUN").value.length != 4)
                lsRentalReq += "    " + "Rental Phone should have 10 digit" + "\n";

            if (lsRentalReq.length != 0) {
                lsRentalReq = lsRentalMsg + lsRentalReq;
            }

            lsMsg += lsRentalReq
        }

    }

    if (lsMsg.length > 0) // display errors if any
        alert(lsMsg);
    else { // otherwise copy data to dataContainerFrame
        if (parent) {
            var loDcIFrameDoc = getdcIFrameDoc();
            var liVehNum;
            var lsElmId;
            var loDcDataElm;
            if (AddNewVeh == "true") {
                var lastVehNum = new Number(getLastVehNum());
                liVehNum = lastVehNum + 1;

            } else if (gsRetToClaim == "true" || gsRetFromClaim == "true") {
                liVehNum = 1;
            } else {
                liVehNum = currVehNum;
            }

            var isFirstparty = false;
            if (loUiIFrameDoc.getElementById("txtExposureCD").value == "1")
                isFirstparty = true;

            var isProgramShop = false;
            if (loUiIFrameDoc.getElementById("txtAssignmentTypeID").value == "4")
                isProgramShop = true;

            var isChoiceShop = false;
            if (loUiIFrameDoc.getElementById("txtAssignmentTypeID").value == "17")
                isChoiceShop = true;

            var tblRows = loDcIFrameDoc.getElementById("tblVehicle").rows;
            var liRowsLength = tblRows.length;

            if (gsRetFromClaim == "true") {
                tblRows[0].firstParty = isFirstparty;
                if (isProgramShop)
                    tblRows[0].programShop = isProgramShop;
                else if (isChoiceShop)
                    tblRows[0].hoiceShop = isChoiceShop;

                gsRetFromClaim = "false";
            }
            else if (gsEditVehMode == "true") {
                for (var i = 0; i < liRowsLength; i++) {
                    if (tblRows[i].getAttribute("vehNum") == liVehNum) {
                        tblRows[i].setAttribute("firstParty", isFirstparty);
                        if (isProgramShop)
                            tblRows[i].setAttribute("programShop", isProgramShop);
                        else if (isChoiceShop)
                            tblRows[i].setAttribute("choiceShop", isChoiceShop);
                    }
                }
            }
            else if (AddNewVeh == "true") {
                if (isProgramShop)
                    window.frames["dataContIfr"].insNewVeh(liVehNum, isFirstparty, isProgramShop);
                else if (isChoiceShop)
                    window.frames["dataContIfr"].insNewVeh(liVehNum, isFirstparty, isChoiceShop);
            }

            // update inputs
            if (loUiIFrameDoc.getElementById('txtExposureCD').value == "1")
                clrDedLimAmt(); //clear all Deductibles and Limits except Liability

            var loSelCov = loUiIFrameDoc.getElementById('txtCoverageProfileUiCD').value;
            var lsCovSelected, lsCovSelected2;

            if (loSelCov != "") {
                loSelCov = loSelCov.split("|")[0];
                var loSelCov2 = loUiIFrameDoc.getElementById('txtCoverageProfileUiCD');
                lsCovSelected = loSelCov2.options[loSelCov2.selectedIndex].text;
                switch (loSelCov) {
                    case "COLL":
                        lsCovSelected2 = "Collision";
                        break;
                    case "COMP":
                        lsCovSelected2 = "Comprehensive";
                        break;
                    case "UIM":
                        lsCovSelected2 = "UnderInsured";
                        break;
                    case "UM":
                        lsCovSelected2 = "UnInsured";
                        break;
                    case "LIAB":
                        lsCovSelected2 = "Liability";
                        break;
                    default:
                        lsCovSelected2 = "";
                        break;
                }
            }
            else
                lsCovSelected2 = "";

            if (gbReserveLineTypeScript == 1) {
                loDcIFrameDoc.getElementById("txtLineType_" + liVehNum).value = loElmLineType.value;
                loDcIFrameDoc.getElementById("txtReserve_" + liVehNum).value = loElmReserve.value;

                loDcIFrameDoc.getElementById("txtCustomScriptingMemo_" + liVehNum).value = "Vehicle " + liVehNum + " " + lsLineType + "; " + lsReserve;
                loUiIFrameDoc.getElementById("txtSourceApplicationPassThruData").value = lsLineType + "|" + lsReserve;
                loDcIFrameDoc.getElementById("txtSourceApplicationPassThruData_" + liVehNum).value = loUiIFrameDoc.getElementById("txtSourceApplicationPassThruData").value;
            }

loUiIFrameDoc.getElementById("hidRentalVehicleClass").value = loUiIFrameDoc.getElementById("txtRentalVehicleClass").value;
loUiIFrameDoc.getElementById("hidRentalVendor").value = loUiIFrameDoc.getElementById("txtRentalVendor").value;
                   loUiIFrameDoc.getElementById("hidRentalRateType").value = loUiIFrameDoc.getElementById("txtRentalRateType").value;
 loUiIFrameDoc.getElementById("hidRentalState").value = loUiIFrameDoc.getElementById("txtRentalState").value;

            var lcolUiElms = loUiIFrameDoc.getElementsByTagName("INPUT");
            var liUiElmsLength = lcolUiElms.length;
            for (var i = 0; i < liUiElmsLength; i++) {
                if ((lcolUiElms[i].type == "text" || lcolUiElms[i].type == "hidden") && lcolUiElms[i].getAttribute("inputTag") != 'SourceApplicationPassThruData') {

                    lsElmId = lcolUiElms[i].id;
                    if (lsElmId.indexOf('Amt') != -1 && lsCovSelected2 != "")
                        loDcIFrameDoc.getElementById("txt" + lsCovSelected2 + lcolUiElms[i].name).value = lcolUiElms[i].value;

                    if (lsElmId.indexOf('Rental') != -1 && lcolUiElms[i].value.length != 0)
                        loDcIFrameDoc.getElementById(lsElmId).value = lcolUiElms[i].value;

                    // 23Feb2012 - TVD - Added try catch to ignore the elephant passthruvehicle field
                    try {

                        loDcIFrameDoc.getElementById(lsElmId + "_" + liVehNum).value = lcolUiElms[i].value;

                    } catch (e) {
                        //alert("Error copying UI data to Data container for element: " + lsElmId + "\nError Description: " + e.description);
                    }
                }
            }

            // update selects

            var loUIPriDmgSel = loUiIFrameDoc.getElementById("txtPrimaryDamage");
            var liPriDmgSel = loUIPriDmgSel.options.value;

            var lsPriDmgSelected;
            if (loSelCov.selectedIndex == "-1")
                lsPriDmgSelected = "";
            else
                lsPriDmgSelected = loUIPriDmgSel.options[loUIPriDmgSel.selectedIndex].text;

            if (loDcIFrameDoc.getElementById("txtPrimaryDamageDescription_" + liVehNum) != null)
                loDcIFrameDoc.getElementById("txtPrimaryDamageDescription_" + liVehNum).value = lsPriDmgSelected;

            var lcolUiElmsSel = loUiIFrameDoc.getElementsByTagName("SELECT");
            var liUiElmsSelLength = lcolUiElmsSel.length;
            var loSel;

            for (var z = 0; z < liUiElmsSelLength; z++) {
                lsElmId = lcolUiElmsSel[z].id;
                if (lcolUiElmsSel[z].name == "SecondaryDamage") {
                    loSel = lcolUiElmsSel[z];
                    var lsSecDmg = "";
                    var lsSecDmgDesc = "";
                    var loSelLength = loSel.length;
                    for (var x = 0; x < loSelLength; x++) {
                        if (loSel[x].selected == true) {
                            if (loSel.options[x].value != liPriDmgSel) {
                                if (lsSecDmg != "") {
                                    lsSecDmg += "," + loSel.options[x].value;
                                    lsSecDmgDesc += ", " + loSel.options[x].text;
                                }
                                else {
                                    lsSecDmg += loSel.options[x].value;
                                    lsSecDmgDesc += loSel.options[x].text;
                                }
                            }
                        }
                    }

                    if (loDcIFrameDoc.getElementById(lsElmId + "_" + liVehNum) != null)
                        loDcIFrameDoc.getElementById(lsElmId + "_" + liVehNum).value = lsSecDmg;

                    if (loDcIFrameDoc.getElementById(lsElmId + "Description_" + liVehNum) != null)
                        loDcIFrameDoc.getElementById(lsElmId + "Description_" + liVehNum).value = lsSecDmgDesc;
                }
                else {
                    var lsElmId = loDcIFrameDoc.getElementById(lsElmId + "_" + liVehNum);

                    if (lsElmId != undefined && lsElmId != null)
                        lsElmId.value = lcolUiElmsSel[z].value;

                    var lsElmIdDescription = loDcIFrameDoc.getElementById(lsElmId + "Description_" + liVehNum);
                    if (lsElmIdDescription != undefined && lsElmIdDescription != null)
                        if (loDcIFrameDoc.getElementById(lsElmId + "Description_" + liVehNum)) {
                            loSel = lcolUiElmsSel[z];
                            var lsDescSelected;
                            if (loSel.selectedIndex == "-1")
                                lsDescSelected = "";
                            else
                                lsDescSelected = loSel.options[loSel.selectedIndex].text;
                            loDcIFrameDoc.getElementById(lsElmId + "Description_" + liVehNum).value = lsDescSelected;
                        }
                }
            }
            var lsContactBestPhoneCB = window.frames["uiFrame"].getContactBestPhone(); //get Contact Best Phone value
            loDcIFrameDoc.getElementById("txtContactBestPhoneCD_" + liVehNum).value = lsContactBestPhoneCB;

            loDcIFrameDoc.getElementById("txtRentalInstructions_" + liVehNum).value = loUiIFrameDoc.getElementById("txtRentalInstructions").value;

            // 16Feb2012 - TVD - Elephant
            try {
                var AdditionalInfo = loDcIFrameDoc.getElementById("txtSourceApplicationPassthruDataVeh_" + liVehNum).value
                var CurrShopRemarks = loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value;
                if (AdditionalInfo == "E") {
                    //alert(CurrShopRemarks.indexOf("Estimate"));
                    if (CurrShopRemarks.indexOf("Estimate") == -1) {
                        loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = " *** Estimate Only *** " + loUiIFrameDoc.getElementById("txtShopRemarks").value;
                    }
                }
                else {
                    if (CurrShopRemarks.indexOf("Estimate") != -1) {
                        var ShopComm = loUiIFrameDoc.getElementById("txtShopRemarks").value;
                        ShopComm = ShopComm.replace("*** Estimate Only ***", "");
                        loUiIFrameDoc.getElementById("txtShopRemarks").value = ShopComm;
                        loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = ShopComm;
                    }
                    loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = loUiIFrameDoc.getElementById("txtShopRemarks").value;
                }
            } catch (e) {
                //alert("Error updating the shop comments data into Data container" + e.description);
            }


            dspMsg(); //display Loading... message between page loads

            gsEditVehMode = "false";
            if (gsRetToClaim == "true") {
                gsRetToClaim = "false";
                document.getElementById("uiFrame").src = "frmWaClaimInfo.asp?fromPage=VehicleInfo";
            }
            else {
                window.frames["dataContIfr"].updVehValues(liVehNum);
                gsVehCount = getVehCount();

                if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp")
                    document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=VehicleInfo&vehCount=" + gsVehCount;
                else
                    document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=VehicleInfo&vehCount=" + gsVehCount;
            }
        }
        else
            return false;
    }
}


function dspMsg() {

    var objIFrame = parent.document.getElementById('uiFrame');
    objIFrame.style.height = "0";
    parent.showMsg();
}

function clrDedLimAmt() {

    var loDcIFrameDoc = getdcIFrameDoc();
    loDcIFrameDoc.getElementById("txtCollisionDeductibleAmt").value = "";
    loDcIFrameDoc.getElementById("txtCollisionLimitAmt").value = "";
    loDcIFrameDoc.getElementById("txtComprehensiveDeductibleAmt").value = "";
    loDcIFrameDoc.getElementById("txtComprehensiveLimitAmt").value = "";
    loDcIFrameDoc.getElementById("txtUnderInsuredDeductibleAmt").value = "";
    loDcIFrameDoc.getElementById("txtUnderInsuredLimitAmt").value = "";
    loDcIFrameDoc.getElementById("txtUnInsuredDeductibleAmt").value = "";
    loDcIFrameDoc.getElementById("txtUnInsuredLimitAmt").value = "";
}


function partySelChange(party) {
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();

    if (party == 1) {
        if (loUiIFrameDoc.getElementById("txtOwnerNameFirst").value == "")
            loUiIFrameDoc.getElementById("txtOwnerNameFirst").value = loDcIFrameDoc.getElementById("txtInsuredNameFirst").value;

        if (loUiIFrameDoc.getElementById("txtOwnerNameLast").value == "")
            loUiIFrameDoc.getElementById("txtOwnerNameLast").value = loDcIFrameDoc.getElementById("txtInsuredNameLast").value;

        if (loUiIFrameDoc.getElementById("txtOwnerPhoneAC").value == "")
            loUiIFrameDoc.getElementById("txtOwnerPhoneAC").value = loDcIFrameDoc.getElementById("txtInsuredPhoneAC").value;

        if (loUiIFrameDoc.getElementById("txtOwnerPhoneEN").value == "")
            loUiIFrameDoc.getElementById("txtOwnerPhoneEN").value = loDcIFrameDoc.getElementById("txtInsuredPhoneEN").value;

        if (loUiIFrameDoc.getElementById("txtOwnerPhoneUN").value == "")
            loUiIFrameDoc.getElementById("txtOwnerPhoneUN").value = loDcIFrameDoc.getElementById("txtInsuredPhoneUN").value;

        if (loUiIFrameDoc.getElementById("txtOwnerBusinessName").value == "")
            loUiIFrameDoc.getElementById("txtOwnerBusinessName").value = loDcIFrameDoc.getElementById("txtInsuredBusinessName").value;

        if (loUiIFrameDoc.getElementById("txtContactNameFirst").value == "")
            loUiIFrameDoc.getElementById("txtContactNameFirst").value = loDcIFrameDoc.getElementById("txtInsuredNameFirst").value;

        if (loUiIFrameDoc.getElementById("txtContactNameLast").value == "")
            loUiIFrameDoc.getElementById("txtContactNameLast").value = loDcIFrameDoc.getElementById("txtInsuredNameLast").value;

        if (loUiIFrameDoc.getElementById("txtContactPhoneAC").value == "")
            loUiIFrameDoc.getElementById("txtContactPhoneAC").value = loDcIFrameDoc.getElementById("txtInsuredPhoneAC").value;

        if (loUiIFrameDoc.getElementById("txtContactPhoneEN").value == "")
            loUiIFrameDoc.getElementById("txtContactPhoneEN").value = loDcIFrameDoc.getElementById("txtInsuredPhoneEN").value;

        if (loUiIFrameDoc.getElementById("txtContactPhoneUN").value == "")
            loUiIFrameDoc.getElementById("txtContactPhoneUN").value = loDcIFrameDoc.getElementById("txtInsuredPhoneUN").value;
    }
    else if (party == 3) {
        if ((loUiIFrameDoc.getElementById("txtOwnerNameFirst").value == loDcIFrameDoc.getElementById("txtInsuredNameFirst").value) && (loUiIFrameDoc.getElementById("txtOwnerNameLast").value == loDcIFrameDoc.getElementById("txtInsuredNameLast").value)) {
            loUiIFrameDoc.getElementById("txtOwnerNameFirst").value = "";
            loUiIFrameDoc.getElementById("txtOwnerNameLast").value = "";
        }

        if ((loUiIFrameDoc.getElementById("txtOwnerPhoneAC").value == loDcIFrameDoc.getElementById("txtInsuredPhoneAC").value) && (loUiIFrameDoc.getElementById("txtOwnerPhoneEN").value == loDcIFrameDoc.getElementById("txtInsuredPhoneEN").value) && (loUiIFrameDoc.getElementById("txtOwnerPhoneUN").value == loDcIFrameDoc.getElementById("txtInsuredPhoneUN").value)) {
            loUiIFrameDoc.getElementById("txtOwnerPhoneAC").value = "";
            loUiIFrameDoc.getElementById("txtOwnerPhoneEN").value = "";
            loUiIFrameDoc.getElementById("txtOwnerPhoneUN").value = "";
        }

        if (loUiIFrameDoc.getElementById("txtOwnerBusinessName").value == loDcIFrameDoc.getElementById("txtInsuredBusinessName").value)
            loUiIFrameDoc.getElementById("txtOwnerBusinessName").value = "";

        if ((loUiIFrameDoc.getElementById("txtContactNameFirst").value == loDcIFrameDoc.getElementById("txtInsuredNameFirst").value) && (loUiIFrameDoc.getElementById("txtContactNameLast").value == loDcIFrameDoc.getElementById("txtInsuredNameLast").value)) {
            loUiIFrameDoc.getElementById("txtContactNameFirst").value = "";
            loUiIFrameDoc.getElementById("txtContactNameLast").value = "";
        }

        if ((loUiIFrameDoc.getElementById("txtContactPhoneAC").value == loDcIFrameDoc.getElementById("txtInsuredPhoneAC").value) && (loUiIFrameDoc.getElementById("txtContactPhoneEN").value == loDcIFrameDoc.getElementById("txtInsuredPhoneEN").value) && (loUiIFrameDoc.getElementById("txtContactPhoneUN").value == loDcIFrameDoc.getElementById("txtInsuredPhoneUN").value)) {
            loUiIFrameDoc.getElementById("txtContactPhoneAC").value = "";
            loUiIFrameDoc.getElementById("txtContactPhoneEN").value = "";
            loUiIFrameDoc.getElementById("txtContactPhoneUN").value = "";
        }
    }
}


function getClaimSummaryData(VehCount) {
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var lsElmId;
    var loDcDataElm;

    // update claim - skip if adding a vehicle
    if (gsAddVeh == 0) {
        var loUiClaimTbl = loUiIFrameDoc.getElementById("ClaimInfoTable");
        var lcolClaimElms = loUiClaimTbl.getElementsByTagName("INPUT");
        var liClaimElmsLength = lcolClaimElms.length;
        for (var i = 0; i < liClaimElmsLength; i++) {
            lsElmId = lcolClaimElms[i].id;
            if (lsElmId == "txtLiabilityFlag") {
                loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                if (loDcDataElm.value == "0") lcolClaimElms[i].value = "No";
                else if (loDcDataElm.value == "1") lcolClaimElms[i].value = "Yes";
            }
            else if (lsElmId == "txtEmergencyFlag") {
                loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                if (loDcDataElm.value == "0") lcolClaimElms[i].value = "No";
                else if (loDcDataElm.value == "1") lcolClaimElms[i].value = "Yes";
            }
            else {
                lcolClaimElms[i].value = loDcIFrameDoc.getElementById(lsElmId).value;

            }
        }

        loUiIFrameDoc.getElementById("txtCallerRelationToInsuredIDDescription").value = loDcIFrameDoc.getElementById("txtCallerRelationToInsuredIDDescription").value;
        loUiIFrameDoc.getElementById("txtLossDescription").value = loDcIFrameDoc.getElementById("txtLossDescription").value;
        loUiIFrameDoc.getElementById("txtRemarks").value = loDcIFrameDoc.getElementById("txtRemarks").value;

        if (gbEmergencyLiabilityScript == 1) {
            loUiIFrameDoc.getElementById("LiabilityText").innerHTML = loDcIFrameDoc.getElementById("txtLiabilityText").value;
            loUiIFrameDoc.getElementById("EmergencyText").innerHTML = loDcIFrameDoc.getElementById("txtEmergencyText").value;
        }
    }

    // update vehicle
    var liVehNum;
    var laryVeh = VehCount.split(",");
    var liaryVehLength = laryVeh.length;
    for (var x = 2; x < liaryVehLength; x++) {
        if (laryVeh[x] != "") {
            liVehNum = laryVeh[x];
            var loVehTbl = loUiIFrameDoc.getElementById("VehicleInfoTable_" + liVehNum);
            var lcolUiTblElms = loVehTbl.getElementsByTagName("INPUT");
            var liTblElmsLength = lcolUiTblElms.length;
            for (var z = 0; z < liTblElmsLength; z++) {
                lsElmId = lcolUiTblElms[z].id;
                if (lsElmId == "txtExposureCD_" + liVehNum) {
                    loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                    if (loDcDataElm.value == "1") lcolUiTblElms[z].value = "1st Party";
                    else if (loDcDataElm.value == "3") lcolUiTblElms[z].value = "3rd Party";
                }
                else if (lsElmId == "txtCoverageProfileUiCD_" + liVehNum) {
                    loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                    if (loDcDataElm.value == "COLL") lcolUiTblElms[z].value = "Collision";
                    else if (loDcDataElm.value == "COMP") lcolUiTblElms[z].value = "Comprehensive";
                    else if (loDcDataElm.value == "LIAB") lcolUiTblElms[z].value = "Liability";
                    else if (loDcDataElm.value == "UIM") lcolUiTblElms[z].value = "Underinsured";
                    else if (loDcDataElm.value == "UM") lcolUiTblElms[z].value = "Uninsured";
                }
                else if (lsElmId == "txtDrivable_" + liVehNum) {
                    loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                    if (loDcDataElm.value == "0") lcolUiTblElms[z].value = "No";
                    else if (loDcDataElm.value == "1") lcolUiTblElms[z].value = "Yes";
                }
                else if (lsElmId == "txtPrefMethodUpd_" + liVehNum) {
                    loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                    if (loDcDataElm.value == "NU") lcolUiTblElms[z].value = "No Updates";
                    else if (loDcDataElm.value == "EM") lcolUiTblElms[z].value = "E-Mail";
                    else if (loDcDataElm.value == "CE") lcolUiTblElms[z].value = "Cell";
                }
                else if (lsElmId == "txtCellPhoneCarrier_" + liVehNum) {
                    loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                    if (loDcDataElm.value == "AT") lcolUiTblElms[z].value = "AT&T";
                    else if (loDcDataElm.value == "SP") lcolUiTblElms[z].value = "Sprint";
                    else if (loDcDataElm.value == "TM") lcolUiTblElms[z].value = "T-Mobile";
                    else if (loDcDataElm.value == "VW") lcolUiTblElms[z].value = "Verizon Wireless";
                    else if (loDcDataElm.value == "OT") lcolUiTblElms[z].value = "Other";
                }
                else if (lsElmId == "txtContactBestPhoneCD_" + liVehNum) {
                    loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                    if (loDcDataElm.value == "D") lcolUiTblElms[z].value = "Day";
                    else if (loDcDataElm.value == "N") lcolUiTblElms[z].value = "Night";
                    else if (loDcDataElm.value == "A") lcolUiTblElms[z].value = "Alt.";
                    else if (loDcDataElm.value == "C") lcolUiTblElms[z].value = "Cell";
                    else lcolUiTblElms[z].value = "";
                }
                else {
                    try {
                        loDcDataElm = loDcIFrameDoc.getElementById(lsElmId);
                        lcolUiTblElms[z].value = loDcDataElm.value;
                    } catch (e) {
                        alert("Error copying DC data to User interface for element: " + lsElmId + "\nError Description: " + e.description);
                    }
                }
            }
            loUiIFrameDoc.getElementById("txtRentalInstructions_" + liVehNum).value = loDcIFrameDoc.getElementById("txtRentalInstructions_" + liVehNum).value;
            loUiIFrameDoc.getElementById("txtPrimaryDamageDescription_" + liVehNum).value = loDcIFrameDoc.getElementById("txtPrimaryDamageDescription_" + liVehNum).value;
            loUiIFrameDoc.getElementById("txtSecondaryDamageDescription_" + liVehNum).value = loDcIFrameDoc.getElementById("txtSecondaryDamageDescription_" + liVehNum).value;

            if (loUiIFrameDoc.getElementById("txtShopRemarks_" + liVehNum))
                loUiIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value;
            if (loUiIFrameDoc.getElementById("txtDrivingDirections_" + liVehNum))
                loUiIFrameDoc.getElementById("txtDrivingDirections_" + liVehNum).value = loDcIFrameDoc.getElementById("txtDrivingDirections_" + liVehNum).value;

if (loUiIFrameDoc.getElementById("tblAdditionalRental_" + liVehNum)) {
                 if (loDcIFrameDoc.getElementById("hidRentalVendor_" + liVehNum).value == "" && loDcIFrameDoc.getElementById("txtRentalResConf_" + liVehNum).value == "") {
                    loUiIFrameDoc.getElementById("tblAdditionalRental_" + liVehNum).style.display = "none"; 
                }
                else {
                    loUiIFrameDoc.getElementById("tblAdditionalRental_" + liVehNum).style.display = "block";
                }
            }

        }
    }
}


function getClaimInfoData() {
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var lsElmId;
    var loDcDataElm;

    // update inputs
    var lcolUiElms = loUiIFrameDoc.getElementsByTagName("INPUT");
    var liUiElmsLength = lcolUiElms.length;
    for (var i = 0; i < liUiElmsLength; i++) {
        if ((lcolUiElms[i].id).indexOf('Amt') != -1 || lcolUiElms[i].getAttribute("opt") != "yes") {
            lsElmId = lcolUiElms[i].id;
            lcolUiElms[i].value = loDcIFrameDoc.getElementById(lsElmId).value;
        }
    }

    // update selects
    var lcolUiElmsSel = loUiIFrameDoc.getElementsByTagName("SELECT");
    var liUiElmsSelLength = lcolUiElmsSel.length;
    for (var z = 0; z < liUiElmsSelLength; z++) {
        lsElmId = lcolUiElmsSel[z].id;
        lcolUiElmsSel[z].value = loDcIFrameDoc.getElementById(lsElmId).value;
    }

    // update textarea
    var lcolUiElmsTxtArea = loUiIFrameDoc.getElementsByTagName("TEXTAREA");
    var liUiElmsTxtAreaLength = lcolUiElmsTxtArea.length;
    for (var z = 0; z < liUiElmsTxtAreaLength; z++) {
        lsElmId = lcolUiElmsTxtArea[z].id;
        lcolUiElmsTxtArea[z].value = loDcIFrameDoc.getElementById(lsElmId).value;
    }

    //If Emergency/Liability script exists
    if (gbEmergencyLiabilityScript == 1) {
        setRadioByValue(loUiIFrameDoc.forms[0].Liability, loDcIFrameDoc.getElementById("txtLiabilityFlag").value);
        setRadioByValue(loUiIFrameDoc.forms[0].Emergency, loDcIFrameDoc.getElementById("txtEmergencyFlag").value);
    }
}

//Set radio value by passing radio group and value
function setRadioByValue(rdBtnGroup, bValue) {
    for (var x = 0; x < rdBtnGroup.length; x++) {
        if (rdBtnGroup[x].value == bValue)
            rdBtnGroup[x].checked = true;
    }
}

//check this for only tbl gathering of inputs
function getVehicleInfoData(vehNum) {

    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var liVehNum = vehNum;
    var lsElmId;
    var loDcDataElm;

    // update inputs
    var dc_ContactBestPhoneCD = loDcIFrameDoc.getElementById("txtContactBestPhoneCD_" + liVehNum).value;

    var lcolUiElms = loUiIFrameDoc.getElementsByTagName("INPUT");
    var liUiElmsLength = lcolUiElms.length;

    for (var i = 0; i < liUiElmsLength; i++) {
        if (lcolUiElms[i].type == "text" || lcolUiElms[i].type == "hidden") {
            lsElmId = lcolUiElms[i].id;
            if (lsElmId.indexOf('SourceApplicationPassThruData') == -1 && (lsElmId.indexOf('Amt') == -1 || lsElmId.indexOf('txtDeductibleAmt') != -1 || lsElmId.indexOf('txtLimitAmt') != -1)) {
                try {
                    lcolUiElms[i].value = loDcIFrameDoc.getElementById(lsElmId + "_" + liVehNum).value;
                    //alert(lsElmId);
                } catch (e) {
                    lcolUiElms[i].value = "";
                    //alert("Error copying UI data to Data container for element: " + lsElmId + "\nError Description: " + e.description);
                }
            }
        }
        else if (lcolUiElms[i].type == "checkbox") {
            if (lcolUiElms[i].name == "cb_ContactBestPhoneCD" && lcolUiElms[i].value == dc_ContactBestPhoneCD) {
                lcolUiElms[i].checked = true;
            }

//Additional rental info
            if (lcolUiElms[i].name == "cb_Rental_additional" && loDcIFrameDoc.getElementById("hidRentalVendor_" + liVehNum).value != "")
            {
                loUiIFrameDoc.getElementById("txtcb_Rental_additional").checked = true;
 loUiIFrameDoc.getElementById("tblRentalAdditional").style.display = "block";
        }

        }
    }

//Additional rental info Update the select area.
    loUiIFrameDoc.getElementById("txtRentalVehicleClass").value = loDcIFrameDoc.getElementById("hidRentalVehicleClass_" + liVehNum).value;
 loUiIFrameDoc.getElementById("txtRentalVendor").value = loDcIFrameDoc.getElementById("hidRentalVendor_" + liVehNum).value;
    
    loUiIFrameDoc.getElementById("txtRentalRateType").value = loDcIFrameDoc.getElementById("hidRentalRateType_" + liVehNum).value;
loUiIFrameDoc.getElementById("txtRentalState").value = loDcIFrameDoc.getElementById("hidRentalState_" + liVehNum).value;

    var dcPriDamage = loDcIFrameDoc.getElementById("txtPrimaryDamage_" + liVehNum).value;
    var dcSecDamage = loDcIFrameDoc.getElementById("txtSecondaryDamage_" + liVehNum).value;
    var dcSecDamageArr = dcSecDamage.split(",");
    var dcSecDamageArrLength = dcSecDamageArr.length;

    // update selects
    var lcolUiElmsSel = loUiIFrameDoc.getElementsByTagName("SELECT");
    var liUiElmsSelLength = lcolUiElmsSel.length;
    for (var z = 0; z < liUiElmsSelLength; z++) {
        lsElmId = lcolUiElmsSel[z].id;
        if (lcolUiElmsSel[z].name == "SecondaryDamage") {
            var loSel = lcolUiElmsSel[z];
            var loSelLength = loSel.length;
            for (var x = 0; x < loSelLength; x++) {
                for (var y = 0; y < dcSecDamageArrLength; y++) {
                    if (loSel.options[x].value == dcSecDamageArr[y])
                        loSel[x].selected = true;
                }
            }
        }
        else {
            var selectliVehNum = loDcIFrameDoc.getElementById(lsElmId + "_" + liVehNum);
            //glsd451 Changes - For check Drop Dwon value is Null or Undefined
            if (selectliVehNum != undefined && selectliVehNum != null)
                lcolUiElmsSel[z].value = loDcIFrameDoc.getElementById(lsElmId + "_" + liVehNum).value;
        }

    }

    // update textarea
    var lcolUiElmsTxtArea = loUiIFrameDoc.getElementsByTagName("TEXTAREA");
    var liUiElmsTxtAreaLength = lcolUiElmsTxtArea.length;
    for (var x = 0; x < liUiElmsTxtAreaLength; x++) {
        lsElmId = lcolUiElmsTxtArea[x].id;
        lcolUiElmsTxtArea[x].value = loDcIFrameDoc.getElementById(lsElmId + "_" + liVehNum).value;
    }
}


function getVehCount() {
    var loDcIFrameDoc = getdcIFrameDoc();
    var loVehicleTbl = loDcIFrameDoc.getElementById("tblVehicle");
    var oRows = loVehicleTbl.rows;
    var SPcount = 0;
    var vehCount = oRows.length;
    var sVehAry = "";
    for (var i = 0; i < vehCount; i++) {
        if (oRows[i]) {
            sVehAry += "," + oRows[i].getAttribute("vehNum");
            var elms = oRows[i].cells[0].getElementsByTagName("INPUT");
            if (elms[0].value == "4" || elms[0].value == "16" || elms[0].value == "17")
                SPcount = SPcount + 1;
        }
    }
    var retVehAry = vehCount + "," + SPcount + sVehAry;
    return retVehAry;
}


function getLastVehNum() {
    var lastRowVehNum;
    var loDcIFrameDoc = getdcIFrameDoc();
    var loVehicleTbl = loDcIFrameDoc.getElementById("tblVehicle");
    if (loVehicleTbl.rows.length == 0)
        lastRowVehNum = 0;
    else
        lastRowVehNum = loVehicleTbl.rows[loVehicleTbl.rows.length - 1].getAttribute("vehNum");
    return lastRowVehNum;
}



function getfirstPartyAvail() {
    var firstPartyAvail = true;
    var loDcIFrameDoc = getdcIFrameDoc();
    var loVehicleTbl = loDcIFrameDoc.getElementById("tblVehicle");
    var rowItems = loVehicleTbl.rows;
    var rowCount = rowItems.length;

    if (rowCount == 0)
        return firstPartyAvail;
    else {
        for (var i = 0; i < rowCount; i++) {
            if (rowItems[i].getAttribute("firstParty") == "true" || rowItems[i].getAttribute("firstParty") == true)
                firstPartyAvail = false;
        }
        return firstPartyAvail;
    }
}


function delVehicle(vehNum, lsFromPage) {
    var liVehNum = vehNum;
    var loDcIFrameDoc = getdcIFrameDoc();
    var loVehicleTbl = loDcIFrameDoc.getElementById("tblVehicle");
    var rowItems = loVehicleTbl.rows;
    var rowCount = rowItems.length;
    for (var i = 0; i < rowCount; i++) {
        if (rowItems[i].getAttribute("vehNum") == liVehNum) {
            loVehicleTbl.deleteRow(i);
            break;
        }
    }
    gsVehCount = getVehCount();
    if (lsFromPage == "ClaimSummary_addNew")
        document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=frmClaimSummaryNewVeh&vehCount=" + gsVehCount;
    else
        document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=SummaryInfo&vehCount=" + gsVehCount;
}


function getVehSummType(oRow) {
    var liCurrVehSP = 0;
    var loDcIFrameDoc = getdcIFrameDoc();
    var loVehicleTbl = loDcIFrameDoc.getElementById("tblVehicle");

    if (loVehicleTbl.rows.length == 0)
        return liCurrVehSP;
    else {
        var elms = loVehicleTbl.rows[oRow].cells[0].getElementsByTagName("INPUT");

        if (elms[0].value != "4" && elms[0].value != "16" && elms[0].value != "17")
            liCurrVehSP = 1;                                        //not a SP assignment
        if ((elms[0].value == "4" || elms[0].value == "16" || elms[0].value == "17") && (elms[30].value == ""))
            liCurrVehSP = 2;                                        //SP assignment with no shop selected
        if ((elms[0].value == "4" || elms[0].value == "16" || elms[0].value == "17") && (elms[30].value != ""))
            liCurrVehSP = 3;                                        //SP assignment with shop selected
    }
    return liCurrVehSP;
}


function chkReqSP() {
    var lsResults = "";
    var lsVehReq = "";
    var liVehNum;
    var lcolElms;
    var loDcIFrameDoc = getdcIFrameDoc();
    var lcolVehTblRows = loDcIFrameDoc.getElementById("tblVehicle").rows;
    var liVehTblLength = lcolVehTblRows.length;

    if (liVehTblLength == 0) {
        lsResults += "A vehicle is required. Please add at least one (1) vehicle before submitting the claim." + "\n";
    }
    else {
        for (var x = 0; x < liVehTblLength; x++) {
            liVehNum = lcolVehTblRows[x].getAttribute("vehNum");
            lcolElms = lcolVehTblRows[x].cells[0].getElementsByTagName("INPUT");
            if ((lcolElms[0].value == "4" || lcolElms[0].value == "17") && (lcolElms[30].value == ""))
                lsVehReq += "   *  Vehicle " + liVehNum + "\n";
        }

        if (lsVehReq != "") {
            lsResults += "The following vehicle(s) required a shop to be selected before submitting the claim." + "\n";
            lsResults += lsVehReq;
        }
    }
    return lsResults;
}


function getShopSelectionData(vehNum, DispOrder, CurrAssignment) {

    var loDcIFrameDoc = getdcIFrameDoc();
    var liVehNum = vehNum;
    var liDispOrder = DispOrder;
    var liCurrAssignment = CurrAssignment;
    var lsQS = "";

    lsQS += "&ShopState=" + loDcIFrameDoc.getElementById("txtLossAddressState").value;
    lsQS += "&AreaCode=" + loDcIFrameDoc.getElementById("txtContactPhoneAC_" + liVehNum).value;
    lsQS += "&ExchangeNumber=" + loDcIFrameDoc.getElementById("txtContactPhoneEN_" + liVehNum).value;

    var lsVehYear = loDcIFrameDoc.getElementById("txtVehicleYear_" + liVehNum).value;
    var lsVehMake = loDcIFrameDoc.getElementById("txtMake_" + liVehNum).value;
    var lsVehModel = loDcIFrameDoc.getElementById("txtModel_" + liVehNum).value;
    var lsVehDesc = liDispOrder + " - " + lsVehYear + " " + lsVehMake + " " + lsVehModel;

    var lsFromAddVehicle = ""
    if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp")
        lsFromAddVehicle = "&AddVehicle=1"

    if (parent) {
        if (liCurrAssignment == "Choice Shop")
            parent.document.getElementById("uiFrame").src = "frmWaChoiceShopSelectionAdv.asp?fromPage=ClaimSummary&vehNum=" + liVehNum + lsQS + "&vehDesc=" + lsVehDesc + lsFromAddVehicle;

        else
            parent.document.getElementById("uiFrame").src = "frmWaShopSelectionAdv.asp?fromPage=ClaimSummary&vehNum=" + liVehNum + lsQS + "&vehDesc=" + lsVehDesc + lsFromAddVehicle;
    }
}


function updShopSelection(vehNum, ShopLocationID, ShopComments, RetToSummary, ShopSearchLogID, SelectedShopRank, SelectedShopScore) {
    var lsInputID;
    var lsTmpID;
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var liVehNum = vehNum;
    var liShopLocationID = ShopLocationID;
    var liShopSearchLogID = ShopSearchLogID;
    var liSelectedShopRank = SelectedShopRank;
    var liSelectedShopScore = SelectedShopScore;

    var loShopTblRow = loUiIFrameDoc.getElementById("tblRowShop_" + liShopLocationID);
    var lcolElms = loShopTblRow.getElementsByTagName("INPUT");
    var liElmsLength = lcolElms.length;
    for (var i = 0; i < liElmsLength; i++) {
        lsTmpID = lcolElms[i].id;
        if (lsTmpID.indexOf('_' + ShopLocationID) != -1) {
            lsInputID = lsTmpID.substring(0, lsTmpID.indexOf('_'));
            loDcIFrameDoc.getElementById(lsInputID + "_" + liVehNum).value = lcolElms[i].value;
        }
    }

    loDcIFrameDoc.getElementById("txtShopLocationID_" + liVehNum).value = liShopLocationID;
    loDcIFrameDoc.getElementById("txtShopSearchLogID_" + liVehNum).value = liShopSearchLogID;
    loDcIFrameDoc.getElementById("txtSelectedShopRank_" + liVehNum).value = liSelectedShopRank;
    loDcIFrameDoc.getElementById("txtSelectedShopScore_" + liVehNum).value = liSelectedShopScore;
    loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = loUiIFrameDoc.getElementById("txtShopRemarks").value;

    dspMsg(); //display Loading... message between page loads
    document.cookie = "ShopAssignmentType=";
    //alert("currenttxtAssignmentTypeID : " + loDcIFrameDoc.getElementById("txtAssignmentTypeID_" + liVehNum).value);
    //loDcIFrameDoc.getElementById("txtAssignmentTypeID_" + liVehNum).value;


    if (RetToSummary == 1) {
        gsVehCount = getVehCount();
        if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp")
            document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=ShopSelection&vehCount=" + gsVehCount;
        else
            document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ShopSelection&vehCount=" + gsVehCount;
    }
    else if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp") {
        document.getElementById("uiFrame").src = "frmWaAddVehicle.asp?fromPage=ShopSelection&vehNum=" + liVehNum;
    }
    else {
        try {
            document.getElementById("uiFrame").src = "frmWaClaimInfo.asp?fromPage=ShopSelection";
        }
        catch (e) {
            var lsArgs = "vehNum=" + vehNum + "<br>" +
                   "ShopLocationID=" + ShopLocationID + "<br>" +
                   "ShopComments=" + ShopComments + "<br>" +
                   "RetToSummary=" + RetToSummary + "<br>" +
                   "ShopSearchLogID=" + ShopSearchLogID + "<br>" +
                   "SelectedShopRank=" + SelectedShopRank + "<br>" +
                   "SelectedShopScore=" + SelectedShopScore;

            document.getElementById("UserLoginInfo").src = "frmUserLoginInfo.asp?args=" + lsArgs + "&fn=updShopSelection";
            alert("Unable to update the claim information with the shop selected. Please try again.");
            top.document.location.href = "frmMyClaimsDesktop.asp"
        }
    }
}

function updProgramShopSelection(vehNum, ShopLocationID, ShopComments, RetToSummary, ShopSearchLogID, SelectedShopRank, SelectedShopScore) {
    var lsInputID;
    var lsTmpID;
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var liVehNum = vehNum;
    var liShopLocationID = ShopLocationID;
    var liShopSearchLogID = ShopSearchLogID;
    var liSelectedShopRank = SelectedShopRank;
    var liSelectedShopScore = SelectedShopScore;
    //alert("liShopLocationID : " + liShopLocationID);
    var loShopTblRow = loUiIFrameDoc.getElementById("tblRowShop_" + liShopLocationID);
    //alert("test");
    var lcolElms = loShopTblRow.getElementsByTagName("INPUT");
    // alert("test");
    var liElmsLength = lcolElms.length;
    for (var i = 0; i < liElmsLength; i++) {
        lsTmpID = lcolElms[i].id;
        if (lsTmpID.indexOf('_' + ShopLocationID) != -1) {
            lsInputID = lsTmpID.substring(0, lsTmpID.indexOf('_'));
            loDcIFrameDoc.getElementById(lsInputID + "_" + liVehNum).value = lcolElms[i].value;
        }
    }

    loDcIFrameDoc.getElementById("txtShopLocationID_" + liVehNum).value = liShopLocationID;
    loDcIFrameDoc.getElementById("txtShopSearchLogID_" + liVehNum).value = liShopSearchLogID;
    loDcIFrameDoc.getElementById("txtSelectedShopRank_" + liVehNum).value = liSelectedShopRank;
    loDcIFrameDoc.getElementById("txtSelectedShopScore_" + liVehNum).value = liSelectedShopScore;
    loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = loUiIFrameDoc.getElementById("txtShopRemarks").value;

    dspMsg(); //display Loading... message between page loads
    document.cookie = "ShopAssignmentType=ProgramShop";
    //alert("currenttxtAssignmentTypeID : " + loDcIFrameDoc.getElementById("txtAssignmentTypeID_" + liVehNum).value);
    loDcIFrameDoc.getElementById("txtAssignmentTypeID_" + liVehNum).value = "4";


    if (RetToSummary == 1) {
        gsVehCount = getVehCount();
        if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp")
            document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=ShopSelection&vehCount=" + gsVehCount;
        else
            document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ShopSelection&vehCount=" + gsVehCount;
    }
    else if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp") {
        document.getElementById("uiFrame").src = "frmWaAddVehicle.asp?fromPage=ShopSelection&vehNum=" + liVehNum;
    }
    else {
        try {
            document.getElementById("uiFrame").src = "frmWaClaimInfo.asp?fromPage=ShopSelection";
        }
        catch (e) {
            var lsArgs = "vehNum=" + vehNum + "<br>" +
                   "ShopLocationID=" + ShopLocationID + "<br>" +
                   "ShopComments=" + ShopComments + "<br>" +
                   "RetToSummary=" + RetToSummary + "<br>" +
                   "ShopSearchLogID=" + ShopSearchLogID + "<br>" +
                   "SelectedShopRank=" + SelectedShopRank + "<br>" +
                   "SelectedShopScore=" + SelectedShopScore;

            document.getElementById("UserLoginInfo").src = "frmUserLoginInfo.asp?args=" + lsArgs + "&fn=updShopSelection";
            alert("Unable to update the claim information with the shop selected. Please try again.");
            top.document.location.href = "frmMyClaimsDesktop.asp"
        }
    }
}



function updChoiceShopSelection(vehNum, ShopUID, ShopComments, liSelectedShopLocationID, RetToSummary) {
    var lsInputID;
    var lsTmpID;
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var liVehNum = vehNum;
    var liShopLocationID = ShopUID;
    var liShopSearchLogID = 0;
    var liSelectedShopRank = 0;
    var liSelectedShopScore = 0;
    var loShopTblRow = loUiIFrameDoc.getElementById("tblRowShop_" + liShopLocationID);
    var lcolElms = loShopTblRow.getElementsByTagName("INPUT");
    var liElmsLength = lcolElms.length;


    for (var i = 0; i < liElmsLength; i++) {
        lsTmpID = lcolElms[i].id;
        if (lsTmpID.indexOf('_' + ShopUID) != -1) {
            lsInputID = lsTmpID.substring(0, lsTmpID.indexOf('_'));
            loDcIFrameDoc.getElementById(lsInputID + "_" + liVehNum).value = lcolElms[i].value;
        }
    }
    loDcIFrameDoc.getElementById("txtShopLocationID_" + liVehNum).value = liSelectedShopLocationID;
    loDcIFrameDoc.getElementById("txtShopSearchLogID_" + liVehNum).value = liShopSearchLogID;
    loDcIFrameDoc.getElementById("txtSelectedShopRank_" + liVehNum).value = liSelectedShopRank;
    loDcIFrameDoc.getElementById("txtSelectedShopScore_" + liVehNum).value = liSelectedShopScore;
    loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = loUiIFrameDoc.getElementById("txtShopRemarks").value;

    dspMsg(); //display Loading... message between page loads
    document.cookie = "ShopAssignmentType=ChoiceShop";
    //document.cookie = "NewCookie=Test";
    loDcIFrameDoc.getElementById("txtAssignmentTypeID_" + liVehNum).value = "17";
    if (RetToSummary == 1) {
        gsVehCount = getVehCount();
        if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp")
            document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=ShopSelection&vehCount=" + gsVehCount;
        else
            document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ShopSelection&vehCount=" + gsVehCount;
    }
    else if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp") {
        document.getElementById("uiFrame").src = "frmWaAddVehicle.asp?fromPage=ShopSelection&vehNum=" + liVehNum;
    }
    else {
        try {
            document.getElementById("uiFrame").src = "frmWaClaimInfo.asp?fromPage=ShopSelection";
        }
        catch (e) {
            var lsArgs = "vehNum=" + vehNum + "<br>" +
                   "ShopLocationID=" + ShopLocationID + "<br>" +
                   "ShopComments=" + ShopComments + "<br>" +
                   "RetToSummary=" + RetToSummary + "<br>" +
                   "ShopSearchLogID=" + ShopSearchLogID + "<br>" +
                   "SelectedShopRank=" + SelectedShopRank + "<br>" +
                   "SelectedShopScore=" + SelectedShopScore;

            document.getElementById("UserLoginInfo").src = "frmUserLoginInfo.asp?args=" + lsArgs + "&fn=updShopSelection";
            alert("Unable to update the claim information with the shop selected. Please try again.");
            top.document.location.href = "frmMyClaimsDesktop.asp"
        }
    }
}

function updChoiceNewShopSelection(vehNum, ShopComments, liSelectedShopLocationID, RetToSummary, laryShopInfo) {


    var lsInputID;
    var lsTmpID;
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var liVehNum = vehNum;
    var liShopSearchLogID = 0;
    var liSelectedShopRank = 0;
    var liSelectedShopScore = 0;

    //alert("laryShopInfo[1] : " + laryShopInfo[1]);

    loDcIFrameDoc.getElementById("txtShopName_" + liVehNum).value = laryShopInfo[1];
    loDcIFrameDoc.getElementById("txtShopAddress1_" + liVehNum).value = laryShopInfo[2];
    loDcIFrameDoc.getElementById("txtShopAddress2_" + liVehNum).value = laryShopInfo[3];
    loDcIFrameDoc.getElementById("txtShopCity_" + liVehNum).value = laryShopInfo[4];
    loDcIFrameDoc.getElementById("txtShopState_" + liVehNum).value = laryShopInfo[5];
    loDcIFrameDoc.getElementById("txtShopZip_" + liVehNum).value = laryShopInfo[6];
    loDcIFrameDoc.getElementById("txtShopPhone_" + liVehNum).value = laryShopInfo[7];
    loDcIFrameDoc.getElementById("txtShopFax_" + liVehNum).value = laryShopInfo[8];


    loDcIFrameDoc.getElementById("txtShopLocationID_" + liVehNum).value = liSelectedShopLocationID;
    loDcIFrameDoc.getElementById("txtShopSearchLogID_" + liVehNum).value = liShopSearchLogID;
    loDcIFrameDoc.getElementById("txtSelectedShopRank_" + liVehNum).value = liSelectedShopRank;
    loDcIFrameDoc.getElementById("txtSelectedShopScore_" + liVehNum).value = liSelectedShopScore;
    loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = loUiIFrameDoc.getElementById("txtNewShopRemarks").value;



    dspMsg(); //display Loading... message between page loads
    document.cookie = "ShopAssignmentType=ChoiceShop";
    //document.cookie = "NewCookie=Test";

    if (RetToSummary == 1) {
        gsVehCount = getVehCount();
        if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp")
            document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=ShopSelection&vehCount=" + gsVehCount;
        else
            document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ShopSelection&vehCount=" + gsVehCount;
    }
    else if (lsPageName && lsPageName == "frmWaMainAddVehicle.asp") {
        document.getElementById("uiFrame").src = "frmWaAddVehicle.asp?fromPage=ShopSelection&vehNum=" + liVehNum;
    }
    else {
        try {
            document.getElementById("uiFrame").src = "frmWaClaimInfo.asp?fromPage=ShopSelection";
        }
        catch (e) {
            var lsArgs = "vehNum=" + vehNum + "<br>" +
                   "ShopLocationID=" + ShopLocationID + "<br>" +
                   "ShopComments=" + ShopComments + "<br>" +
                   "RetToSummary=" + RetToSummary + "<br>" +
                   "ShopSearchLogID=" + ShopSearchLogID + "<br>" +
                   "SelectedShopRank=" + SelectedShopRank + "<br>" +
                   "SelectedShopScore=" + SelectedShopScore;

            document.getElementById("UserLoginInfo").src = "frmUserLoginInfo.asp?args=" + lsArgs + "&fn=updShopSelection";
            alert("Unable to update the claim information with the shop selected. Please try again.");
            top.document.location.href = "frmMyClaimsDesktop.asp"
        }
    }
}

function getShopComments(vehNum) {
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var liVehNum = vehNum;

    if (loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum))
        loUiIFrameDoc.getElementById("txtShopRemarks").value = loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value;
}


function updShopComments(vehNum) {
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var liVehNum = vehNum;

    // 16Feb2012 - TVD - Elephant
    var AdditionalInfo = loDcIFrameDoc.getElementById("txtSourceApplicationPassthruDataVeh_" + liVehNum).value
    var CurrShopRemarks = loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value;
    if (AdditionalInfo == "E") {
        //alert(CurrShopRemarks.indexOf("Estimate"));
        if (CurrShopRemarks.indexOf("Estimate") == -1) {
            loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = " *** Estimate Only *** " + loUiIFrameDoc.getElementById("txtShopRemarks").value;
        }
    }
    else {
        if (CurrShopRemarks.indexOf("Estimate") != -1) {
            var ShopComm = loUiIFrameDoc.getElementById("txtShopRemarks").value;
            ShopComm = ShopComm.replace("*** Estimate Only ***", "");
            loUiIFrameDoc.getElementById("txtShopRemarks").value = ShopComm;
            loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = ShopComm;
        }
        loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = loUiIFrameDoc.getElementById("txtShopRemarks").value;
    }
}


function clrShopValues(vehNum) {
    var loDcIFrameDoc = getdcIFrameDoc();
    var liVehNum = vehNum;

    if (loDcIFrameDoc.getElementById("txtShopLocationID_" + liVehNum)) {
        loDcIFrameDoc.getElementById("txtShopLocationID_" + liVehNum).value = "";
        loDcIFrameDoc.getElementById("txtShopName_" + liVehNum).value = "";
        loDcIFrameDoc.getElementById("txtShopRemarks_" + liVehNum).value = "";
    }
}

function chkReqParams() {
    var lbReqParams = true;
    var lsReqFields1 = "";
    var lsReqFields2 = "";
    var lsReqFields3 = "";
    var loDcIFrameDoc = getdcIFrameDoc();

    var lsFNOLUserID = loDcIFrameDoc.getElementById("txtFNOLUserID").value;

    if (lsFNOLUserID == 0 || lsFNOLUserID == "") {
        lbReqParams = false;
        lsReqFields1 = "User ID";
    }

    var loVehTbl = loDcIFrameDoc.getElementById("tblVehicle");
    var lcolVehTblRows = loVehTbl.rows;
    var liVehTblRowsLength = lcolVehTblRows.length;
    for (var x = 0; x < liVehTblRowsLength; x++) {
        if (lcolVehTblRows[x]) {
            var currVehNum = lcolVehTblRows[x].getAttribute("vehNum");

            var lcolElms = lcolVehTblRows[x].getElementsByTagName("INPUT");
            var liElmsLength = lcolElms.length;
            for (var y = 0; y < liElmsLength; y++) {
                if (lcolElms[y].id.indexOf('txtAssignmentTypeID_') != -1 && lcolElms[y].value == "") {
                    lbReqParams = false;
                    lsReqFields2 = "Assignment Type";
                }
                else if (lcolElms[y].id.indexOf('txtExposureCD_') != -1 && lcolElms[y].value == "") {
                    lbReqParams = false;
                    lsReqFields3 = "Coverage";
                }
            }
        }
    }

    if (lbReqParams == false)
        return lsReqParams = "false" + "|" + lsReqFields1 + "|" + lsReqFields2 + "|" + lsReqFields3;
    else
        return lsReqParams = "true";
}


function getXMLstr() {
    var loDcIFrameDoc = getdcIFrameDoc();
    loDcIFrameDoc.getElementById("txtTimeFinished").value = getCurrentDT();
    var startTime = loDcIFrameDoc.getElementById("txtIntakeStartSeconds").value;
    var endTime = getCurrentTimeSec();
    var totalTime = parseInt((endTime - startTime) / 1000);
    loDcIFrameDoc.getElementById("txtIntakeEndSeconds").value = endTime;
    loDcIFrameDoc.getElementById("txtIntakeSeconds").value = totalTime;
    var lsEmergencyLiabilityMemoTxt = "";


    //update the txtCustomScriptingMemo field with data from all vehicles and claim
    if (gbEmergencyLiabilityScript == 1) {
        lsEmergencyLiabilityMemoTxt = (loDcIFrameDoc.getElementById("txtLiabilityText").value).replace('<li>', ' ') + ": ";
        if (loDcIFrameDoc.getElementById("txtLiabilityFlag").value == 1)
            lsEmergencyLiabilityMemoTxt += "Yes\n";
        else
            lsEmergencyLiabilityMemoTxt += "No\n";

        lsEmergencyLiabilityMemoTxt += (loDcIFrameDoc.getElementById("txtEmergencyText").value).replace('<li>', ' ') + ": ";
        if (loDcIFrameDoc.getElementById("txtEmergencyFlag").value == 1)
            lsEmergencyLiabilityMemoTxt += "Yes\n";
        else
            lsEmergencyLiabilityMemoTxt += "No\n";
    }

    var lsXML = "<WebAssignment version='100'>";

    var loClaimTbl = loDcIFrameDoc.getElementById("tblClaim");
    if (loClaimTbl) {
        lsXML += "<Claim>";
        lsXML += "<NewClaimFlag><![CDATA[1]]></NewClaimFlag>";
        lsXML += GetXmlElements(loClaimTbl);
        lsXML += "</Claim>";
    }

    var loVehTbl = loDcIFrameDoc.getElementById("tblVehicle");
    var lcolVehTblRows = loVehTbl.rows;
    var liVehTblRowsLength = lcolVehTblRows.length;
    var liNextVehNum = 2;
    var lbFirstPartyExist = false;

    for (var x = 0; x < liVehTblRowsLength; x++) {
        if (lcolVehTblRows[x]) {
            if (lcolVehTblRows[x].getAttribute("programShop") == "true" || lcolVehTblRows[x].getAttribute("programShop") == true)
                lsXML += "<Vehicle ProgramShop='1'>";
            else
                lsXML += "<Vehicle ProgramShop='0'>";

            var currVehNum = lcolVehTblRows[x].getAttribute("vehNum");

            //update the vehicle txtCustomScriptingMemo and txtSourceApplicationPassThruData fields with data from the claim node
            if (gbEmergencyLiabilityScript == 1) {
                if (loDcIFrameDoc.getElementById("txtCustomScriptingMemo_" + currVehNum).value != "")
                    loDcIFrameDoc.getElementById("txtCustomScriptingMemo_" + currVehNum).value += "\n\n";
                loDcIFrameDoc.getElementById("txtCustomScriptingMemo_" + currVehNum).value += lsEmergencyLiabilityMemoTxt;

                if (loDcIFrameDoc.getElementById("txtSourceApplicationPassThruData_" + currVehNum).value != "")
                    loDcIFrameDoc.getElementById("txtSourceApplicationPassThruData_" + currVehNum).value += "|";
                loDcIFrameDoc.getElementById("txtSourceApplicationPassThruData_" + currVehNum).value += loDcIFrameDoc.getElementById("txtSourceApplicationPassThruData").value;
            }

            if ((gbEmergencyLiabilityScript == 1 || gbReserveLineTypeScript == 1) && loDcIFrameDoc.getElementById("txtCustomScriptingMemo_" + currVehNum).value != "") {
                loDcIFrameDoc.getElementById("txtShowScriptingMemoFlag_" + currVehNum).value = 1;
            }

            if (loDcIFrameDoc.getElementById("txtExposureCD_" + currVehNum).value == "1") {
                //first party vehicle. must be 1.
                loDcIFrameDoc.getElementById("txtVehicleNumber_" + currVehNum).value = 1;
                lbFirstPartyExist = true;
            } else {
                loDcIFrameDoc.getElementById("txtVehicleNumber_" + currVehNum).value = liNextVehNum;
                liNextVehNum++;
            }

            if (loDcIFrameDoc.getElementById("txtOwnerPhoneAC_" + currVehNum).value == "")
                window.frames["dataContIfr"].UpdContactPhone(currVehNum);

            lsXML += GetXmlElements(lcolVehTblRows[x]);
            // 03Feb2012 - TVD - Elephant
            //if (loDcIFrameDoc.getElementById("txtSourceApplicationPassthruDataVeh_" + currVehNum))
            //    lsXML += "<AdditionalInfo>" + loDcIFrameDoc.getElementById("txtSourceApplicationPassthruDataVeh_" + currVehNum).value + "</AdditionalInfo>";
            lsXML += "</Vehicle>";
        }
    }
    lsXML += "</WebAssignment>";
    return lsXML;
}

function getNewVehicleXMLstr(AssignmentTypeDesc) {
    var loDcIFrameDoc = getdcIFrameDoc();
    loDcIFrameDoc.getElementById("txtTimeFinished").value = getCurrentDT();
    var startTime = loDcIFrameDoc.getElementById("txtIntakeStartSeconds").value;
    var endTime = getCurrentTimeSec();
    var totalTime = parseInt((endTime - startTime) / 1000);
    loDcIFrameDoc.getElementById("txtIntakeEndSeconds").value = endTime;
    loDcIFrameDoc.getElementById("txtIntakeSeconds").value = totalTime;

    var lsCarrierRepUserID = loDcIFrameDoc.getElementById("txtFNOLUserID").value;
    var lsInsuranceCompanyID = liInsuranceCompanyID;

    var lsXML = "<WebAssignment version='100'>";

    var loClaimTbl = loDcIFrameDoc.getElementById("tblClaim");
    if (loClaimTbl) {
        lsXML += "<Claim>";
        lsXML += "<LynxID><![CDATA[" + liLynxID + "]]></LynxID>";
        lsXML += "<ClientClaimNumber><![CDATA[" + lsClaimNumber + "]]></ClientClaimNumber>";
        lsXML += "<CarrierRepUserID><![CDATA[" + lsCarrierRepUserID + "]]></CarrierRepUserID>";
        lsXML += "<DataSource><![CDATA[Claim Point Web Assignment]]></DataSource>";
        lsXML += "<FNOLUserID><![CDATA[" + liUserID + "]]></FNOLUserID>";
        lsXML += "<InsuranceCompanyID><![CDATA[" + lsInsuranceCompanyID + "]]></InsuranceCompanyID>";
        lsXML += "<NewClaimFlag><![CDATA[0]]></NewClaimFlag>";
        lsXML += "<CoverageClaimNumber><![CDATA[" + lsClaimNumber + "]]></CoverageClaimNumber>";
        lsXML += "<LossDescription><![CDATA[" + lsLossDescription + "]]></LossDescription>";
        lsXML += "<LossDate><![CDATA[" + lsLossDate + "]]></LossDate>";
        lsXML += "<LossAddressState><![CDATA[" + lsLossState + "]]></LossAddressState>";
        lsXML += "<CallerNameFirst><![CDATA[" + lsCallerFName + "]]></CallerNameFirst>";
        lsXML += "<CallerNameLast><![CDATA[" + lsCallerLName + "]]></CallerNameLast>";
        lsXML += "<CallerRelationToInsuredIDDescription><![CDATA[" + lsCallerRelation + "]]></CallerRelationToInsuredIDDescription>";
        lsXML += "<InsuredNameFirst><![CDATA[" + lsInsuredFName + "]]></InsuredNameFirst>";
        lsXML += "<InsuredNameLast><![CDATA[" + lsInsuredLName + "]]></InsuredNameLast>";
        lsXML += "<InsuredBusinessName><![CDATA[" + lsInsuredBName + "]]></InsuredBusinessName>";
        lsXML += "<InsuredPhoneSumm><![CDATA[" + lsInsuredPhoneArea + "-" + lsInsuredPhoneEx + "-" + lsInsuredPhoneNum + "]]></InsuredPhoneSumm>";
        lsXML += "<TimeFinished><![CDATA[" + getCurrentDT() + "]]></TimeFinished>"
        lsXML += "<CarrierName><![CDATA[" + lsCarrierName + "]]></CarrierName>"
        lsXML += "<CarrierOfficeName><![CDATA[" + lsCarrierOfficeName + "]]></CarrierOfficeName>"
        lsXML += "<CarrierRepNameFirst><![CDATA[" + lsCarrierRepNameFirst + "]]></CarrierRepNameFirst>"
        lsXML += "<CarrierRepNameLast><![CDATA[" + lsCarrierRepNameLast + "]]></CarrierRepNameLast>"
        lsXML += "<CarrierRepPhoneDay><![CDATA[" + lsCarrierRepPhoneDay + "]]></CarrierRepPhoneDay>"
        lsXML += "<CarrierRepEmailAddress><![CDATA[" + lsCarrierRepEmailAddress + "]]></CarrierRepEmailAddress>"
        lsXML += "<AssignmentDescription><![CDATA[" + AssignmentTypeDesc + "]]></AssignmentDescription>"
        lsXML += "<AssignmentAtSelectionFlag><![CDATA[" + lsAssignmentAtSelectionFlag + "]]></AssignmentAtSelectionFlag>";
        lsXML += "<CollisionDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtCollisionLimitAmt").value + "]]></CollisionDeductibleAmt>"
        lsXML += "<CollisionLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtCollisionLimitAmt").value + "]]></CollisionLimitAmt>"
        lsXML += "<ComprehensiveDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtComprehensiveDeductibleAmt").value + "]]></ComprehensiveDeductibleAmt>"
        lsXML += "<ComprehensiveLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtComprehensiveLimitAmt").value + "]]></ComprehensiveLimitAmt>"
        lsXML += "<LiabilityDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtLiabilityDeductibleAmt").value + "]]></LiabilityDeductibleAmt>"
        lsXML += "<LiabilityLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtLiabilityLimitAmt").value + "]]></LiabilityLimitAmt>"
        lsXML += "<UnderInsuredDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtUnderInsuredDeductibleAmt").value + "]]></UnderInsuredDeductibleAmt>"
        lsXML += "<UnderInsuredLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtUnderInsuredLimitAmt").value + "]]></UnderInsuredLimitAmt>"
        lsXML += "<UnInsuredDeductibleAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtUnInsuredDeductibleAmt").value + "]]></UnInsuredDeductibleAmt>"
        lsXML += "<UnInsuredLimitAmt><![CDATA[" + loDcIFrameDoc.getElementById("txtUnInsuredLimitAmt").value + "]]></UnInsuredLimitAmt>"
        lsXML += "</Claim>";
    }

    var loVehTbl = loDcIFrameDoc.getElementById("tblVehicle");
    var lcolVehTblRows = loVehTbl.rows;
    var liVehTblRowsLength = lcolVehTblRows.length;
    var lbFirstPartyExist = false;
    var liNextVehNum = 0;
    var laVeh;

    for (var x = 0; x < laVehicleList.length; x++) {
        laVeh = laVehicleList[x].split("|");
        if (laVeh[0] == "1") {
            lbFirstPartyExist = true;
        }
    }

    liNextVehNum = parseInt(laVehicleList[laVehicleList.length - 1].split("|")[0], 10) + 1;

    for (var x = 0; x < liVehTblRowsLength; x++) {
        if (lcolVehTblRows[x]) {
            if (lcolVehTblRows[x].getAttribute("programShop") == "true" || lcolVehTblRows[x].getAttribute("programShop") == true)
                lsXML += "<Vehicle ProgramShop='1'>";
            else
                lsXML += "<Vehicle ProgramShop='0'>";

            var currVehNum = lcolVehTblRows[x].getAttribute("vehNum");

            if (loDcIFrameDoc.getElementById("txtExposureCD_" + currVehNum).value == "1") {
                //first party vehicle. must be 1.
                loDcIFrameDoc.getElementById("txtVehicleNumber_" + currVehNum).value = 1;
                lbFirstPartyExist = true;
            } else {
                loDcIFrameDoc.getElementById("txtVehicleNumber_" + currVehNum).value = liNextVehNum;
                liNextVehNum++;
            }

            if (loDcIFrameDoc.getElementById("txtOwnerPhoneAC_" + currVehNum).value == "")
                window.frames["dataContIfr"].UpdContactPhone(currVehNum);

            if ((gbEmergencyLiabilityScript == 1 || gbReserveLineTypeScript == 1) && loDcIFrameDoc.getElementById("txtCustomScriptingMemo_" + currVehNum).value != "") {
                loDcIFrameDoc.getElementById("txtShowScriptingMemoFlag_" + currVehNum).value = 1;
            }

            lsXML += GetXmlElements(lcolVehTblRows[x]);
            lsXML += "</Vehicle>";
        }
    }
    lsXML += "</WebAssignment>";
    return lsXML;
}


function GetXmlElements(oName) {
    var lsElms = ""
    var lcolElms = oName.getElementsByTagName("INPUT");
    var liElmsLength = lcolElms.length;
    for (var y = 0; y < liElmsLength; y++) {
        if (lcolElms[y].getAttribute("opt") != "yes")
            lsElms += "<" + lcolElms[y].name + ">" + "<![CDATA[" + lcolElms[y].value + "]]>" + "</" + lcolElms[y].name + ">";
    }

    var lcolElmsTxt = oName.getElementsByTagName("TEXTAREA");
    var liElmsTxtLength = lcolElmsTxt.length;
    for (var z = 0; z < liElmsTxtLength; z++) {
        if (lcolElmsTxt[z].getAttribute("opt") != "yes")
            lsElms += "<" + lcolElmsTxt[z].name + ">" + "<![CDATA[" + lcolElmsTxt[z].value + "]]>" + "</" + lcolElmsTxt[z].name + ">";
    }
    return lsElms;
}


function getSubmitDate() {
    var loUiIFrameDoc = getuiIFrameDoc();
    var loDcIFrameDoc = getdcIFrameDoc();
    var lsDate = loDcIFrameDoc.getElementById("txtTimeFinished").value;
    return getDisplayDate(lsDate);
}


function markClaimSubmitted(flag) {
    var loDcIFrameDoc = getdcIFrameDoc();
    loDcIFrameDoc.getElementById("txtClaimSubmitted").value = flag;
}


function chkClaimSubmitted() {
    var loDcIFrameDoc = getdcIFrameDoc();
    var lsWasSubmitted = "false";

    if (loDcIFrameDoc.getElementById("txtClaimSubmitted").value == "true")
        lsWasSubmitted = "true";

    return lsWasSubmitted;
}

function createNewVeh() {
    //alert("createNewVeh");
    var liVehNum = 1;
    var isFirstparty = false;
    var isProgramShop = true;
    window.frames["dataContIfr"].insNewVeh(liVehNum, isFirstparty, isProgramShop);
}

function createCSNewVeh(SourceID) {
    //alert("createCSNewVeh");
    // alert("SourceID : " + SourceID);
    var liVehNum = 1;
    var isFirstparty = false;
    if (SourceID == "HQ" || SourceID == "HQNEW")
        window.frames["dataContIfr"].insNewVeh(liVehNum, isFirstparty, true);
    else
        window.frames["dataContIfr"].insNewVeh(liVehNum, isFirstparty, false);
}