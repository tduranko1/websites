<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>LYNXSelect Terms of Agreement</title>
<style>
	body {
		font-family: Georgia, Tahoma;
		font-size: 11px;
	}
	.bold {
		font-size: 12px;
		font-weight: bold;
	}
	.title {
		font-size: 16px;
		font-weight: bold;
	}

</style>
</head>

<body>

<img width="185" height="52" src="images/lynxselect_logo.jpg">

<div align="center" class="title">Terms of Agreement</div>
 
<p>The following sets forth the terms of Agreement between LYNX Services, L.L.C. (�LYNX�) 6351 Bayshore Road Suite 18 Ft. Myers FL 33917
and <SPAN id="txtShopName">test</SPAN> (�Participant Shop�).</p>

<ol type="A">

	<SPAN class="bold"><li>Referral Authorization and Non-Exclusivity</SPAN></li>
    <ol type="1">
  	  <li>LYNX will honor Customer choice when making referrals; otherwise, LYNX will have no obligation to refer Customers to any particular Participant Shop. In general, the basis for making referrals will include but is not necessarily limited to factors related to various performance metrics and will be at the sole discretion of LYNX and/or LYNX�s insurance s customers.</li>
    	<li>Participant Shop acknowledges that participation in the LYNXSelect Program is non-exclusive.  LYNX may have similar agreements with other repair facilities in the same geographic area, and will provide Customers with a choice of more than one Participant Shop in any geographic area.</li>
    </ol>
  <br>
	<SPAN class="bold"><li>Regulatory Compliance</li></SPAN>
    <ol type="1">
  	  <li>Participant Shop agrees to comply with all federal and state local rules, regulations, and statutes applicable to the Participant Shop and its business, and to the LYNXSelect Program (�Applicable Laws�).  In some states, regulatory agencies have established specific procedures for handling auto physical damage claims. If any provision of this Agreement is in conflict with the Applicable Laws and deemed unlawful or unenforceable, it shall not affect the validity or enforceability of the remaining provisions.</li>
  	  <li>Participant Shop agrees to comply with all safety and environmental standards when handling or disposing of toxic and hazardous materials.</li>
  	  <li>Participant Shop agrees to take appropriate steps to prevent illegal or fraudulent business practices.</li>
  	  <li>Participant Shop agrees to comply with all Vehicle Anti-Theft Regulations to deter auto theft and the use of stolen vehicle parts.</li>
  	  <li>Participant Shop agrees to comply with Applicable Laws regarding disclosure of Consumer records and information.</li>
    </ol>
  <br>
	<SPAN class="bold"><li>Insurance Coverage</li></SPAN>
    <ol type="1">
  	  <li>Participant Shop shall carry Worker�s Compensation Insurance to statutory limits in any state where it has employees.</li>
  	  <li>Participant Shop shall carry Employer�s Liability Insurance (minimum $100,000 bodily injury by accident or disease).</li>
  	  <li>Participant Shop shall carry Commercial General Liability Insurance, including Contractual Liability and Products/Completed Operations Coverage with the limits of not less than $1,000,000 each occurrence/combined single limit for property damage, bodily injury, and personal injury liability.</li>
  	  <li>Participant Shop shall carry insurance for Vehicle Liability and insurance for damages to Customers� vehicles while in the care, custody and control of the Participant Shop.  The exact limit of such coverage shall be at the sole discretion and control of the Participant Shop, subject to Applicable Laws.</li>
  	  <li>Participant Shop agrees to provide a copy of its Certificate of Insurance as proof of required coverage.</li>
    </ol>
  <br>
	<SPAN class="bold"><li>Taxes</li></SPAN>
    <ol type="1">
  	  <li>Participant Shop will determine and include on the final estimate for repair work performed, as appropriate, all governmental taxes or charges of any kind or type whatever that are imposed upon its services or sale of products to Customer at Participant Shop�s place of business. Taxes properly included on final estimates will be reimbursed with claim payments.</li>
  	  <li>Participant Shop agrees to remit all such taxes or charges to the appropriate governmental entity as and when due, and will release, indemnify and defend LYNX from liability for any claims arising from Participant Shop�s failure to fully comply with this section.</li>
    </ol>
	<br>
	<SPAN class="bold"><li>Term</li></SPAN>
    <ol type="1">
  	  <li>This Agreement has a one-year term from the date of signing by both parties.  This Agreement will automatically renew annually unless terminated by either party as provided for in this Agreement.</li>
    </ol>
	<br>
	<SPAN class="bold"><li>Termination</li></SPAN>
    <ol type="1">
  	  <li>Participant Shop may terminate this Agreement by providing 30-day advance written notice to LYNX.</li>
  	  <li>LYNX may terminate this Agreement by written notice subject but not limited to one of the following events occurring:</li>
        <ol type="a">
        	<li>Participant Shop engages in fraudulent business practices.</li>
        	<li>Any substantial owner or principal manager of Participant Shop is convicted of a felony or engages in activities that would be chargeable as a felony under Applicable Laws.</li>
        	<li>Participant Shop becomes insolvent, assigns or attempts to assign its business assets for the benefit of creditors, institutes or has instituted against it proceedings of bankruptcy, or dissolves or liquidates the business.</li>
        </ol>
  	  <li>LYNX may immediately terminate this Agreement by written notice if there is a material breach of the Agreement by the Participant Shop.</li>
  	  <li>LYNX may terminate this Agreement if the Participant Shop refuses to submit to a background check or does not pass the background check.</li>
  	  <li>LYNX may terminate this Agreement by written notice if the LYNXSelect Program is no longer in existence.</li>
    </ol>
	<br>
	<SPAN class="bold"><li>Entire Agreement</li></SPAN>
    <ol type="1">
  	  <li>This Agreement is composed of the following documents, all of which are incorporated by reference and are made a part of this Agreement:</li>
        <ol type="a">
        	<li>LYNXSelect Program - Terms of Agreement</li>
        	<li>LYNXSelect Program - General Requirements & Procedures</li>
        	<li>LYNXSelect Program - Repair Facility Information</li>
        	<li>LYNXSelect Program - Record of Repair Cost Quotations</li>
        </ol>
  	  <li>LYNX may, at any time, make changes only to items a and b in Section G.1 above and will provide written notice of these changes to Participant Shop.  These changes will become part of this Agreement, and Participant Shop agrees to accept these changes unless the Participant Shop provides written objection of the change and notice of termination to LYNX at the address listed above within 10 days after being notified of the changes.</li>
  	  <li>This Agreement constitutes the complete Agreement between LYNX and the Participant Shop.</li>
    </ol>
	<br>
	<SPAN class="bold"><li>Independent Contractor</li></SPAN>
    <ol type="1">
  	  <li>Participant Shop is an independent contractor for all purposes of this Agreement and shall not be deemed an employee, partner, agent or joint venturer of or with LYNX.  Participant Shop acknowledges that LYNX shall have no obligation to Participant Shop other than as specifically provided for in this Agreement, nor any liability to third parties for the debts or other business expenses incurred by the Participant Shop in the course of its business activities.</li>
    </ol>
	<br>
	<SPAN class="bold"><li>Interpretation of Agreement</li></SPAN>
    <ol type="1">
  	  <li>This Agreement shall be interpreted under the laws of the state in which the Participant Shop resides, as shown in the Repair Facility Information document.</li>
    </ol>
</ol>

</body>
</html>
