<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
  response.contentType = "text/xml"
  Dim lsStrXML, lsRetVal
  Dim docXML, docRoot, docNew, strRet
  
  lsStrXML = request("lstrXML")
  
  On Error Resume Next
  
  if lsStrXML <> "" then
  
      lsRetVal = GetData(lsStrXML, "", "")
      
      Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
      docXML.Async = False
      docXML.LoadXML lsRetVal
      
      If Not docXML.ParseError = 0 Then
        strRet = "<Root><Error><Description> " & docXML.ParseError & "</Description></Error></Root>"
      Else

        If docXML.documentElement.nodeName = "Root" Then
            set docRoot = docXML.selectSingleNode("/Root")
            set docNew = docXML.createElement("Root")
            docNew.setAttribute "LynxID", docRoot.getAttribute("LynxID")
            docNew.setAttribute "WebAssignmentDocument", docRoot.getAttribute("WebAssignmentDocument")
            docRoot.appendChild docNew
            strRet = docXML.xml
        Else
          'get the returned error msg and stuff it in the global message holder
          lsExternalEventMsg = docXML.documentElement.getAttribute("Description")
          strRet = "<Root><Error><Description> " & lsExternalEventMsg & "</Description></Error></Root>"
        End If

      End If
  else
    strRet = "<Root><Error><Description>No assignment</Description></Erro></Root>"
  end if
  response.write strRet
  Set docXML = Nothing

%>
