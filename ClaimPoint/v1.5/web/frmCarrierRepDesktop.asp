<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->

<%
  lsPageName = "frmCarrierRepDesktop.asp"
  lsPageTitle = "Carrier Rep Calim Vew"
  liPageType = lcAfterInsuranceCompanySelect
  bGridSort = True
  bGridHover = True
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
  Dim lsSelectionCode, lsDays, lsCarrierRepDesktopCall, lsGUID, lsServiceChannelCD, lsAssignmentType
  
  lsSelectionCode = Request.Form("SelectionCode")
  lsDays = Request.Form("Days")
  liUserID = Request.Form("UserID")
  liOfficeID = Request.Form("OfficeID")
  lsAssignmentType = Request.Form("AssignmentType")
  lsGUID = Request.Cookies("CPSession")("guidArray0")
  
  lsCarrierRepDesktopCall = "<CarrierRepDesktop InsuranceCompanyID=""" & liInsuranceCompanyID & """ UserID=""" & server.urlencode(liUserID) &_
                            """ OfficeID=""" & server.urlencode(liOfficeID) & """ SelectionCode=""" & lsSelectionCode & """ Days=""" & lsDays & """ AssignmentTypeID=""" & lsAssignmentType & """/>"
  Dim lsPageNo, lbOverwriteExisting
  
  lbOverwriteExisting = false
  
  lsPageNo = Request("pageNo")
  
  If lsPageNo = "" Then 
  	lsPageNo = "1"
  	lbOverwriteExisting = true
  End If
%>

<HTML>
<HEAD>
<TITLE><%=lsPageTitle%></TITLE>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="includes/incUtils.js"></script>
<script type="text/javascript" src="includes/incBrowserSniffer.js"></script>

<% If bGridSort = True Then %>
  <script type="text/javascript" src="includes/incGridSort.js"></script>
  <link href="includes/incGridSort.css" type="text/css" rel="stylesheet">
<% End If %>

<% If bGridHover = True Then %>
  <script type="text/javascript" src="includes/incGridHover.js"></script>
<% End If %>

<script language="JavaScript">

<%if lsPageName = "frmWaMain.asp" then%>
  gLogoutTimer = window.setTimeout("top.location = 'frmLogout.asp'", 240*60*1000);
<%else%>
  window.setTimeout("document.location = 'frmLogout.asp'", 480*60*1000);
<%end if%>

  function GridClickGetClaim(obj, iLynxID, iAspectID, iVehNumber)
  {
    var loRow = obj;
    document.frmMainForm.hidLynxID.value = iLynxID;
    document.frmMainForm.hidAspectID.value = iAspectID;
    document.frmMainForm.hidVehNumber.value = iVehNumber;
    document.frmMainForm.hidAssignmentType.value = loRow.cells[4].innerHTML;
    document.frmMainForm.hidVehiclesList.value = loRow.getAttribute("VehiclesList");
    document.frmMainForm.submit();
  }

  function GridClickAddVehicle(iLynxID, evt)
  {
    evt.cancelBubble = true;
    document.frmAddVehForm.hidAddLynxID.value = iLynxID;
    document.frmAddVehForm.submit();
  }

</script>

</HEAD>
<BODY onLoad="resizeIframe('claimViewFrame'); parent.hideMsg()">

<div id="divContainer">

<form name="frmAddVehForm" id="frmAddVehForm" method="POST" action="rspClaimAddVehicle.asp" target="_parent">
  <input type="hidden" name="hidAddLynxID" id="hidAddLynxID">
</form>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspClaimSearchResults.asp" target="claimGetInfoFrame">
  <input type="hidden" name="hidLynxID" id="hidLynxID">
  <input type="hidden" name="hidAspectID" id="hidAspectID">
  <input type="hidden" name="hidVehNumber" id="hidVehNumber">
  <input type="hidden" name="hidAssignmentType" id="hidAssignmentType">
  <input type="hidden" name="hidVehiclesList" id="hidVehiclesList">
  
<%
  Response.Write GetPersistedData(lsCarrierRepDesktopCall, lsGUID, "ClaimSearchResults.xsl", "UserID=" & liUserID & lcDelimiter & "ClaimCreate=" & liClaimCreate & lcDelimiter & "ClientOfficeId=" & UCase(lsClientOfficeId), false)
%>

<script type="text/javascript">
  if (document.getElementById("srhResults"))
  {
<%
  If liUserID = 0 Then
%>
    var SrtViewTbl = new GridSort(document.getElementById("srhResults"), ["Date", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "None"]);
<%
  Else
%>
    var SrtViewTbl = new GridSort(document.getElementById("srhResults"), ["Date", "CaseInsensitiveString", "CaseInsensitiveString", "Number", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "None"]);
<%
  End If
%>
    SrtViewTbl.sort(0, false);
  }
</script>

</form>

</div>

<IFRAME SRC="blank.asp" NAME="claimGetInfoFrame" ID="claimGetInfoFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="display:none; width:100%; border:0px;"></IFRAME>

</BODY>
</HTML>

<%
  set loAPD = nothing
%>
