<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!-- v1.4.5.0 -->

<%
  lsPageName = "frmAuthPayments.asp"
  lsPageTitle = "Authorize Payments"
  liPageType = lcAfterInsuranceCompanySelect
  lsHelpPage = "CP_Authorize_Payments.htm"
  bGridSort = True
  bGridHover = True

  Response.Cookies("CPSession")("currentClaim") = ""
  Response.Cookies("CPSession")("currentClaim").Expires = Now()-1
  
  Dim lsToday
  'format todays date for JS datediff function in the XSL
  lsToday = DatePart("m", Date()) & "/" & DatePart("d", Date()) & "/" & DatePart("yyyy", Date())
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->


<STYLE type=text/css>
<!--
  .button
  {
  	font: 10px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
  }
  .div_popup_td
  {
  	background-color: #CCCCCC;
  	color: #000000;
  	font: 11px verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
  	white-space: nowrap;
  }
  .thead
  {
  	background: #5C7099;
  	color: #FFFFFF;
  	font: bold 11px tahoma, verdana, geneva, lucida, 'lucida grande', arial, helvetica, sans-serif;
  }
  .div_popup
  {
  	background-color: #FFFFFF;
  	color: #000000;
  	border: 1px solid #0B198C;
  }
-->
</STYLE>

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/incCalendar.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/incGenericMove.js" type="text/javascript"></script>

<SCRIPT type=text/javascript>
<!--
  var gsSaveRowColor;
  var gsCurrentRow;
  var gsOldLossDate = "";

  
  function SaveClaimInfo()
  {
    var sLynxId = document.getElementById("divLynxID").innerHTML;
    var sClaimNumber = document.getElementById("divClaimNumber").value;
    var sLossDate = document.getElementById("txtLossDate").value;
    var loSel = document.getElementById('divCoverage');
    var sCoverage = loSel.options[loSel.selectedIndex].value;
    loSel = document.getElementById('divLossState');
    var sLossState = loSel.options[loSel.selectedIndex].value;
    
    if (sClaimNumber.length != 0 && sLossDate.length != 0 && sCoverage.length != 0 && sLossState.length != 0)
    {
      document.getElementById("hidClaimAspectID").value = document.getElementById("divClaimAspectID").value;
      document.getElementById("hidClientClaimNumber").value = sClaimNumber;
      document.getElementById("hidLossDate").value = sLossDate;
      document.getElementById("hidLossState").value = sLossState;
      document.getElementById("hidClientCoverageTypeID").value = sCoverage;
  
      var objForm = document.getElementById("frmClaimDetailUpd");
      objForm.submit();
    }
    else
    {
      alert("Blank entries are not valid.\nPlease correct your entries before saving.");
    }
  }
  
  function ClaimUpdResults(retValue)
  {
    if (retValue == "1")
    {
      var oRow = gsCurrentRow;
      oRow.getElementsByTagName("td").item(6).innerHTML = document.getElementById("hidClientClaimNumber").value;
      oRow.getElementsByTagName("td").item(7).innerHTML = document.getElementById("hidLossDate").value;
      oRow.getElementsByTagName("td").item(8).innerHTML = document.getElementById("hidLossState").value;
      oRow.getElementsByTagName("td").item(13).innerHTML = document.getElementById("hidClientCoverageTypeID").value;
      var sCoverage = document.getElementById("hidClientCoverageTypeID").value;
      
      var laryTmp = laryCoverage[0];
    	for (var i=0; i<laryTmp.length; i++)
    	{
    		if (sCoverage == laryTmp[i+1])
          oRow.getElementsByTagName("td").item(9).innerHTML = laryTmp[i];
    		i++;
    	}
    }
    else
    {
      alert("Error saving updates.\nPlease try again.");
    }
  
    CloseEditDiv();
  }
  
  function EditClaim(objId)
  {
    if(document.getElementById("DivEditClaim").style.display == "inline")
      CloseEditDiv();
  
    var oDiv = objId.firstChild;
    var oRow = objId.offsetParent.parentNode;
    gsCurrentRow = oRow;
  
    document.getElementById("divLynxID").innerHTML = oRow.id;
    document.getElementById("divClaimAspectID").value = oRow.getAttribute("ClaimAspectID");
    document.getElementById("divClaimNumber").value = oRow.getElementsByTagName("td").item(6).innerHTML;
    document.getElementById("txtLossDate").value = oRow.getElementsByTagName("td").item(7).innerHTML;
    var iIndex = 0;
    if (oRow.getAttribute("party") == 3)
      iIndex = 2;
    else if (oRow.getAttribute("party") == 1)
      iIndex = 1;
    
    updateSelect("Coverage", iIndex);
    updSelect("divCoverage", oRow.getElementsByTagName("td").item(13).innerHTML);
    updSelect("divLossState", oRow.getElementsByTagName("td").item(8).innerHTML);
  
    oRow.onmouseover = new Function("");
    oRow.onmouseout = new Function("");
    gsSaveRowColor = saveBG;
    //gsSaveRowColor = oRow.style.backgroundColor; 
    oRow.style.backgroundColor = "#CCCCCC";
  
    var iDivPosY;
    if( findPosY(oDiv) > (getWindowSize("height") + getScrollXY("y") - 180) )
      iDivPosY = getWindowSize("height") + getScrollXY("y") - 180;
    else
      iDivPosY = findPosY(oDiv)+10;
  
    document.getElementById("DivEditClaim").style.top = iDivPosY;
    document.getElementById("DivEditClaim").style.left = findPosX(oDiv) + 30;
    document.getElementById("DivEditClaim").style.display = "inline";
  }
  
  function updSelect(id, value)
  {
    var loSel = document.getElementById(id);
    var loSelLength = loSel.options.length;
    loSel.options[0].selected = true;
    for (var i=0; i<loSelLength; i++)
    {
      if (loSel.options[i].value == value)
        loSel.options[i].selected = true;
    }
  }
  
  function CloseEditDiv()
  {
    if(document.getElementById("DivEditClaim").style.display == "inline")
    {
      document.getElementById("DivEditClaim").style.display = "none";
      var oRow = gsCurrentRow;
      oRow.style.backgroundColor = gsSaveRowColor;
      oRow.onmouseover = new Function("", "GridMouseOver(this,0)");
      oRow.onmouseout = new Function("", "GridMouseOut(this)");
  
      document.getElementById("divLynxID").innerHTML = "";
      document.getElementById("divClaimNumber").value = "";
      document.getElementById("txtLossDate").value = "";
      document.getElementById("divCoverage").selectedIndex = 0;
    }
  }
  
  
  function AuthorizeClaims()
  {
    var lsClaimAspectIDs = "";
    var loTbl = document.getElementById("srhResults");
    var laryChkBxs = loTbl.getElementsByTagName("INPUT");
    var cbLength = laryChkBxs.length;
  	for (var j=0; j<cbLength; j++)
  	{
  		if (laryChkBxs[j].type == "checkbox" && laryChkBxs[j].checked == true)
  		{
  			if (lsClaimAspectIDs != "")
  				lsClaimAspectIDs += ","
        lsClaimAspectIDs += laryChkBxs[j].id.substr(3, laryChkBxs[j].id.length);
      }
    }
    if (lsClaimAspectIDs != "")
    {
      document.getElementById("hidAuthorizedClaimAspectIDs").value = lsClaimAspectIDs;
      var objForm = document.getElementById("frmAuthorizeClaims");
      objForm.submit();
    }
    else
      alert("No claims have been marked for authorization.\nPlease review your selections.");
  }
  
  
  function setCurrLossDate(obj) 
  {
    gsOldLossDate = document.getElementById(obj).value;
  }

  function formatLossDate() 
  {
    var lsMyDate = new Date(document.getElementById("txtLossDate").value);
    var liDateDiff = Math.round((new Date() - lsMyDate) / 24 / 60 / 60 / 1000);
    if (liDateDiff < 0)
    {
      alert("The Loss Date cannot be in the future.\n");
      document.getElementById("txtLossDate").value = gsOldLossDate;
      document.getElementById("txtLossDate").focus();
    }

    if (document.activeElement.id == "CloseEditDivTD" || document.activeElement.id == "CloseEditDivBtn")
      return;

    if (document.getElementById('txtLossDate').value.length != 0)
    {
      var lsDate = document.getElementById('txtLossDate').value;
      var lsMonth, lsDay, lsYear;
      if (lsDate.indexOf("/") != -1) {
        var laryDate = lsDate.split("/");
        lsMonth = laryDate[0];
        lsDay = laryDate[1];
        lsYear = laryDate[2];
      } else {
        //try to format with regular expression.
        lsYear = lsDate.substr(lsDate.length - 4, 4);
        var lsReminder = lsDate.substr(0, lsDate.length - 4);
        switch (lsReminder.length) {
          case 4:
            lsMonth = lsReminder.substr(0, 2);
            lsDay = lsReminder.substr(2, 2);
            break;
          case 3:
            if (parseInt(lsReminder.substr(0, 2), 10) > 12) {
              lsMonth = lsReminder.substr(0, 1);
              lsDay = lsReminder.substr(1, 2)
            } else {
              lsMonth = lsReminder.substr(0, 2);
              lsDay = lsReminder.substr(2, 1)              
            }
            break;
          case 2:
            lsMonth = lsReminder.substr(0, 1);
            lsDay = lsReminder.substr(1, 1);
            break;
        }
      }
  
      var lbBadDate = false;
      if (lsMonth == undefined || lsMonth < 1 || lsMonth > 12)
        lbBadDate = true;
      if (lsDay == undefined || lsDay < 1 || lsDay > 31)
        lbBadDate = true;
      if (lsYear == undefined || lsYear < 1)
        lbBadDate = true;
      if (lbBadDate == true)
      {
        alert("Please enter the date as MM/DD/YYYY or M/D/YY or MMDDYYYY.");
        document.getElementById('txtLossDate').focus();
        return;
      }
  
      if (lsMonth < 10 && lsMonth.charAt(0) != 0)
        lsMonth = "0"+lsMonth;

      if (lsDay < 10 && lsDay.charAt(0) != 0)
        lsDay = "0"+lsDay;

      var lsMydate = new Date();
      if (lsYear == undefined || lsYear == "" || (lsYear >= 100 && lsYear < 1900))
      {
        lsYear = lsMydate.getFullYear();
      }
      else if (lsYear >= 0 && lsYear < 100)
      {
        lsYear = Number(lsYear) + 2000;
      }
      else if ((lsYear >= 1900 && lsYear < 2000) || (lsYear > 2000))
      {
        lsYear = Number(lsYear);
      }
      else
      {
        lsYear = lsMydate.getFullYear();
      }

      document.getElementById('txtLossDate').value = lsMonth + "/" + lsDay + "/" + lsYear;
    }
  }


  function showDocs(sLynxID, sClaimAspectID)
  {
    if (sLynxID.indexOf("-") != -1)
      sLynxID = sLynxID.substring(0, sLynxID.indexOf("-"));
  
    var lsPath = "frmVehicleDocumentSlim.asp?LynxID=" + sLynxID + "&ClaimAspectID=" + sClaimAspectID;
    window.open(lsPath, 'docsListWindow', 'width=675,height=510,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no');
  }
  
  function sendComment(sLynxID, sClaimNumber, sLossDate)
  {
    var lsPath = "frmSubmitClaimAuthComment.asp?LynxID=" + sLynxID + "&ClaimNumber=" + sClaimNumber + "&LossDate=" + sLossDate;
    window.open(lsPath, 'CommentsWindow', 'width=600,height=380,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no');
  }
  

  //hides the calender if clicked outside of it
  document.onclick = hideCalFrame;

// -->
</SCRIPT>

<%

  Dim  lsGUID, lsReportsListCall
  lsGUID = Request.Cookies("CPSession")("guidArray0")

  If liAuthorizePayments = 1 Then
    Dim lsClaimSearchCall, lsClaimSearchGUID
    lsClaimSearchCall = "<AuthorizationList InsuranceCompanyID='" & liInsuranceCompanyID  & "'/>"
    lsClaimSearchGUID = Request.Cookies("CPSession")("guidArray0")
  
    Response.Write GetPersistedData(lsClaimSearchCall, lsClaimSearchGUID, "AuthPaymentsResults.xsl", "PaymentAuthorizationLevel=" & liPaymentAuthorizationLevel & lcDelimiter & "TodayIs=" & lsToday & lcDelimiter & "UserID=" & liUserID, false)
  
  Else
    response.write "You don't have permission to Authorize Payments. Please contact your supervisor."
  End If

%>

<IFRAME SRC="blank.asp" NAME="ifrmClaimInfoUpd" ID="ifrmClaimInfoUpd" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="display:none; width:100%; border:0px;"></IFRAME>

<%

  Dim docXML, lsInsConfigXML, ElemList, i, lsCoverage_0, lsCoverage_1, lsCoverage_2, lsStates, ElemList2
  set docXML = Server.CreateObject("MSXML2.DOMDocument")
  
  lsInsConfigXML = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
  docXML.LoadXML GetPersistedData(lsInsConfigXML, lsInsCoConfigGUID, "", "", true)

  If Not docXML.ParseError = 0 Then
    Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
  End If

  Set ElemList = docXML.getElementsByTagName("CoverageType")

  For i = 0 To (ElemList.length -1)

    If lsCoverage_0 <> "" and ElemList.item(i).getAttribute("Name") <> "" Then
      lsCoverage_0 = lsCoverage_0 & ","
    End If
    lsCoverage_0 = lsCoverage_0 & """" & ElemList.item(i).getAttribute("Name") & """,""" 
    'lsCoverage_0 = lsCoverage_0 & ElemList.item(i).getAttribute("CoverageProfileCD") & "|"
    lsCoverage_0 = lsCoverage_0 & ElemList.item(i).getAttribute("ClientCoverageTypeID") & """"

    If ElemList.item(i).getAttribute("CoverageProfileCD") <> "LIAB" Then
      If lsCoverage_1 <> "" and ElemList.item(i).getAttribute("Name") <> "" Then
        lsCoverage_1 = lsCoverage_1 & ","
      End If
      lsCoverage_1 = lsCoverage_1 & """" & ElemList.item(i).getAttribute("Name") & """,""" 
      'lsCoverage_1 = lsCoverage_1 & ElemList.item(i).getAttribute("CoverageProfileCD") & "|"
      lsCoverage_1 = lsCoverage_1 & ElemList.item(i).getAttribute("ClientCoverageTypeID") & """"
    End If

    If ElemList.item(i).getAttribute("CoverageProfileCD") = "LIAB" Then
      If lsCoverage_2 <> "" and ElemList.item(i).getAttribute("Name") <> "" Then
        lsCoverage_2 = lsCoverage_2 & ","
      End If
      lsCoverage_2 = lsCoverage_2 & """" & ElemList.item(i).getAttribute("Name") & """,""" 
      'lsCoverage_2 = lsCoverage_2 & ElemList.item(i).getAttribute("CoverageProfileCD") & "|"
      lsCoverage_2 = lsCoverage_2 & ElemList.item(i).getAttribute("ClientCoverageTypeID") & """"
    End If

  Next

  Set ElemList2 = docXML.SelectNodes("/Root/ContractStates")
  Dim lsCurrState, oNode

  For each oNode in ElemList2

    lsCurrState = oNode.getAttribute("StateCode")

    If lsStates <> "" and lsCurrState <> "" Then
      lsStates = lsStates & ","
    End If
     lsStates = lsStates & """" & GetStateName(lsCurrState) & """," 
     lsStates = lsStates & """" & lsCurrState & """"

  Next

  Set ElemList = Nothing
  
  Function GetStateName(sState)
    Dim ElemList3
    Set ElemList3 = docXML.SelectSingleNode("/Root/Reference[@List='ContractState' and @ReferenceID='"& sState & "']")
    If Not(ElemList3 is Nothing) Then
      GetStateName = ElemList3.getAttribute("Name")
    Else
      GetStateName = ""
    End If
    Set ElemList3 = Nothing
  
  End Function
%>

<script type="text/javascript">

  var laryCoverage = new Array(3);
  laryCoverage[0] = new Array(<%= lsCoverage_0 %>)
  laryCoverage[1] = new Array(<%= lsCoverage_1 %>)
  laryCoverage[2] = new Array(<%= lsCoverage_2 %>)

  var laryStates = new Array(1);
  laryStates[0] = new Array(<%= lsStates %>)

  function updateSelect(sel, n)
  {
  	var loSel;
  	var laryTmp;
  	if (sel == "States")
  	{
  		loSel = document.getElementById('divLossState');
  		laryTmp = laryStates[n];
  	}
  	else if (sel == "Coverage")
  	{
  		loSel = document.getElementById('divCoverage');
  		laryTmp = laryCoverage[n];
  	}
  	var liCurr = loSel.options.length;
  	for (var j=liCurr; j>0; j--)
  		loSel.options[j] = null;
  	for (var i=0; i<laryTmp.length; i++)
  	{
  		loSel.options[loSel.options.length] = new Option(laryTmp[i], laryTmp[i+1]);
  		i++;
  	}
  }

  if (document.getElementById("srhResults"))
  {
    var SrtSrhTbl = new GridSort(document.getElementById("srhResults"), ["None", "None", "None", "None", "Date", "CaseInsensitiveString", "CaseInsensitiveString", "Date", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "USCurrency", "CaseInsensitiveString"]);
    SrtSrhTbl.sort(4, false);
  }

  updateSelect("States", 0);

</script>

<iframe ID="CalFrame" NAME="CalFrame" SRC="blank.asp" STYLE="border:2px solid #404040; left:-500px; top:0px; display:none; position:absolute; width:170; height:186; z-index:100; filter: progid:DXImageTransform.Microsoft.Shadow(color='#666666', Direction=135, Strength=3);" MARGINHEIGHT=0 MARGINWIDTH=0 NORESIZE FRAMEBORDER=0 SCROLLING=NO></iframe>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
