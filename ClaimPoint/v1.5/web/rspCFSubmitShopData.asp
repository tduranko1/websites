<!--#include file="includes/incCommonTop.asp"-->

<html>
<head>
<title>LYNXSelect Program Registration</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
</head>
<BODY LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 background="./images/page_bg.jpg">
	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
		<tr>
			<td width="169" align="left" valign="top">
  			<IMG SRC="images/logo_corner.jpg" ALT="" width="169" height="202">
			</td>
			<td align="left" valign="top">

        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" background="images/header_bg.jpg" height="132">
        	<tr>
        		<td height="50">&nbsp;</td>
        		<td align="right" valign="top" height="50">&nbsp;</td>
        	</tr>
        	<tr>
        		<td colspan=2 valign="top">&nbsp;</td>
        	</tr>
        	<tr>
        		<td align="left" valign="bottom" nowrap width="50%">
              <p class="header">
              LYNXSelect<sup style="font-size: 9px;">TM</sup> Registration for 
              <span id="spanlsPageTitle" lang="en-us"><em>CertifiedFirst </em><sup style="font-size: 9px;">TM</sup> Network</span>
            </td>
        		<td align="right" valign="bottom" width="50%">&nbsp;</td>
        	</tr>
        </table>

  <br><br><br>
  
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000099" style="border-collapse: collapse">
      <tr>
        <td width="8" height="8" valign="top"><img border="0" src="images/table_TL_white.gif" WIDTH="8" HEIGHT="8"></td>
        <td width="10" background="images/table_TC_white.gif"><img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="1"></td>
        <td width="450" background="images/table_TC_white.gif"><img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="1"></td>
        <td width="8" height="8" valign="top"><img border="0" src="images/table_TR_white.gif" WIDTH="8" HEIGHT="8"></td>
      </tr>
      <tr>
        <td background="images/table_CL_white.gif" valign="bottom">&nbsp;</td>
        <td nowrap>&nbsp;</td>
        <td nowrap>

      <DIV ALIGN="center">
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td nowrap align="center" class="login">Submitting Registration. Please wait...</td>
          </tr>
          <tr>
            <td nowrap align="center"><img src="images/loading_anim.gif" alt="" width="94" height="17" border="0"></td>
          </tr>
        </table>
      </DIV>

        </td>
        <td background="images/table_CR_white.gif" valign="bottom">&nbsp;</td>
      </tr>
      <tr>
        <td background="images/table_CL_white.gif" valign="bottom"><img border="0" src="images/table_BL_white.gif" WIDTH="8" HEIGHT="8"></td>
        <td class="cell">&nbsp;</td>
        <td class="cell">&nbsp;</td>
        <td background="images/table_CR_white.gif" valign="bottom"><img border="0" src="images/table_BR_white.gif" WIDTH="8" HEIGHT="8"></td>
      </tr>
    </table>
		<br>

		</td>
	</tr>
</table>

<%
  dim loStrXML, lsStrXML, liRetVal, loXML, lsShopEmail, liSaveOnly
  dim liShopID, lsShopName, lsShopAddress1, lsShopAddress2, lsShopCity, lsShopState, liShopZip, lsShopContact, lsShopContactPhone, lsShopFax, lsShopEstPkg
  
  lsStrXML = request("lstrXML")
  lsShopEmail = request("sendShopEmail")
  liSaveOnly = request("saveOnly")
  liShopID = request("sendShopID")
  lsShopName = request("sendShopName")
  lsShopAddress1 = request("sendShopAddress1")
  lsShopAddress2 = request("sendShopAddress2")
  lsShopCity = request("sendShopCity")
  lsShopState = request("sendShopState")
  liShopZip = request("sendShopZip")
  lsShopContact = request("sendShopContact")
  lsShopContactPhone = request("sendShopContactPhone")
  lsShopFax = request("sendShopFax")
  lsShopEstPkg = request("sendShopEstPkg")
  
  'On Error Resume Next
  Dim mexMonikerString 
	mexMonikerString = GetWebServiceURL()
    'set loStrXML = Server.CreateObject("PPGNAAPD.clsAPDData")
    set loStrXML = GetObject(mexMonikerString)
    liRetVal = loStrXML.PutXML(lsStrXML)
  
    set loStrXML = nothing

function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function
%>

  <form name="frmCFShopValues" method="post" action="frmCFShopResults.asp" >
    <input type="hidden" name="updateResult" value="<%=liRetVal%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopEmail" value="<%=lsShopEmail%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopID" value="<%=liShopID%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopName" value="<%=lsShopName%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopAddress1" value="<%=lsShopAddress1%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopAddress2" value="<%=lsShopAddress2%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopCity" value="<%=lsShopCity%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopState" value="<%=lsShopState%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopZip" value="<%=liShopZip%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopContact" value="<%=lsShopContact%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopContactPhone" value="<%=lsShopContactPhone%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopFax" value="<%=lsShopFax%>" style="visibility: hidden;" >
    <input type="hidden" name="ShopEstPkg" value="<%=lsShopEstPkg%>" style="visibility: hidden;" >
  </form>

<script language="javascript">

    var liUpdateResult = "<%=liRetVal%>";
    var saveOnly = "<%=liSaveOnly%>";

    if (saveOnly == 1)
        top.location = 'frmCFShopSave.asp?updateResult=' + liUpdateResult;
    else
        document.frmCFShopValues.submit();

</script>

</body>
</html>

