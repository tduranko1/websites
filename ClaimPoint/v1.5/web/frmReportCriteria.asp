<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!-- v1.4.2.0 -->

<%
lsPageName = "frmReportCriteria.asp"
lsPageTitle = "Report Criteria"
liPageType = lcAfterInsuranceCompanySelect
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
  if request("reportID") = "" then
    response.write "Unable to determine the report."
    response.end
  end if
  
  Dim  lsGUID, lsReportsCriteriaCall
  Dim lsBrowser
  lsBrowser = Request.ServerVariables("HTTP_USER_AGENT")
  
  
  if liViewReport <> 1 then
    response.write "You don't have permission to view Reports. Please contact your supervisor."
    response.end
  end if
  
  lsGUID = Request.Cookies("CPSession")("guidArray0")

  if liClaimViewLevel = liClaimViewLevel_All then
    ' User has View All privilege. Do not send the Office id. All states contracted to the insurance company will be visible.
    lsReportsCriteriaCall = "<ReportsCriteria ReportID=""" + request("reportID") + """ InsuranceCompanyID=""" + liInsuranceCompanyID + """/>"
  else
    ' User has View Own / View Office privilege. Send the office id so that the states returned will be
    ' restricted to the contracted states of the office.
    lsReportsCriteriaCall = "<ReportsCriteria ReportID=""" + request("reportID") + """ OfficeID=""" + liOfficeID + """ InsuranceCompanyID=""" + liInsuranceCompanyID + """/>"
  end if
  
  Dim lsReportsCriteria
  lsReportsCriteria = GetPersistedData(lsReportsCriteriaCall, lsGUID, "ReportCriteria.xsl", "", false)

'  response.write lsReportsCriteria
'  response.end
%>
<html>
<head>
  <link href="includes/apd_style.css" type="text/css" rel="stylesheet">
  <link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">
  <script language="javascript">
    var aCriteriaObjs = null;
    var aCriteriaObjsReq = null;
    var aCriteriaObjsName = null;
    var aCriteriaObjsDataType = null;
    var aCriteriaObjsMaxValue = null;
    var aCriteriaObjsDefaultValueScript = null;
    var sReportTitle = "";
    var sInsuranceCompanyID = "<%=liInsuranceCompanyID%>";
    var sOfficeID = "<%=liOfficeID%>";
    var sReportTitle = "";
    var sProcName = "";
    var sRptName = "";
    var iExtBrowser = null;
    var isIE = "<%=InStr(lsBrowser, "MSIE")%>";
    var iClaimViewLevel = "<%=liClaimViewLevel%>";
    var iClaimViewLevel_All = "<%=liClaimViewLevel_All%>";
  
    function getReport() {
      if (isDataValid()) {
        var sCriteria = "&InsuranceCompanyID=" + sInsuranceCompanyID;
        var sStateList = "";
        var bAllStates = false;

        var objStateCDList = document.getElementById("StateCDList");
        if (objStateCDList) {
          if (objStateCDList.selectedIndex < 1) {
            sStateList = "";
            bAllStates = true;
          } else {
            sStateList = objStateCDList.value;
          }
        }
        
        var sFriendlyName = sReportTitle;
        for (var i = 0; i < aCriteriaObjs.length; i++) {
          var obj = document.getElementById(aCriteriaObjs[i]);
          if (obj) {
            switch (aCriteriaObjs[i]) {
              case "StateCDList":
                sCriteria += "&" + aCriteriaObjs[i] + "=" + sStateList;
                if (bAllStates)
                  sFriendlyName += "-All States";
                else
                  sFriendlyName += "-" + sStateList;
                sFriendlyName = sFriendlyName.replace(/by State/g, "");
                break;
              case "ShowUserDetails":
                if (obj.checked)
                  sCriteria += "&" + aCriteriaObjs[i] + "=1";
                else
                  sCriteria += "&" + aCriteriaObjs[i] + "=0";
                break;
              default:
                sCriteria += "&" + aCriteriaObjs[i] + "=" + obj.value;
                sFriendlyName += "-" + obj.value;
                break;
            }
          }
        }      

        var objForm = document.getElementById("frmGetReport");
        if (document.getElementById("chkOpenExternal").checked == true) {
          if ((iExtBrowser == null) || (iExtBrowser.closed == true)) {
            var sFeatures = "directories=no, location=no, menubar=no, resizable=yes, scrollbars=no, status=no, titlebar=no, toolbar=no "
            iExtBrowser = window.open("blank.asp", "sExtBrowser", sFeatures);
          }
          objForm.target = "sExtBrowser";
          try {
            document.getElementById("ifrmReport").frameElement.style.display = "none";
          } catch (e) {}
        } else {
          objForm.target = "ifrmReport";
          try {
            document.getElementById("ifrmReport").frameElement.style.display = "inline";
            document.getElementById("ifrmReport").frameElement.style.border = "1px solid #C0C0C0";
          } catch (e) {}
        }

        var sFormat = "PDF";
        if (document.getElementById("divExportFormat").style.display != "none"){
          sFormat = document.getElementById("csExportFormat").value;
        }

        objForm.reportFileName.value = sRptName;
        objForm.storedProc.value = sProcName;
        objForm.reportParams.value = sCriteria;
        objForm.staticParams.value = sStaticParam;
        objForm.friendlyName.value = sFriendlyName + "." + sFormat;
        objForm.reportFormat.value = sFormat;
        objForm.submit();

        if (document.getElementById("chkOpenExternal").checked == true) {
          if (iExtBrowser != null)
            iExtBrowser.focus();
        }
      }
    }
    
    function isDataValid() {
      if (aCriteriaObjs !== null) {
        for (var i = 0; i < aCriteriaObjs.length; i++) {
          if (aCriteriaObjsReq[i] == "1") {
            var obj = document.getElementById(aCriteriaObjs[i]);
            switch (aCriteriaObjsDataType[i]) {
              case "R":
                if (obj) {
                  if (obj.selectedIndex < 1) {
                    alert(aCriteriaObjsName[i] + " is a required field. Please select a value from the list and try again.");
                    obj.focus();
                    return false;
                  }
                }
                break;
              case "N":
                if (obj) {
                  if (isNaN(obj.value)){
                    alert(aCriteriaObjsName[i] + " is a numeric field. Please enter a numeric value and try again.");
                    obj.focus();
                    return false;
                  }
                  if (obj.value == "") {
                    alert(aCriteriaObjsName[i] + " is a required field. Please enter a value and try again.");
                    obj.focus();
                    return false;
                  }
                  if (parseFloat(obj.value) > parseFloat(aCriteriaObjsMaxValue[i])) {
                    alert(aCriteriaObjsName[i] + " value cannot exceed " + aCriteriaObjsMaxValue[i] + ". Please try again.");
                    obj.focus();
                    return false;
                  }
                }
                break;
            }
          }
        }
      }
      return true;
    }
    
    function pageInit(){
      if (isIE == "0")
        //3898 - View report in separate window - display checbox issue fix - start
        //document.getElementById("divViewExternal").style.display = "none";
        //3898 - View report in separate window - display checbox issue fix - start
      try {
        parent.criteriaReady();
      } catch (e) {}
      
      if (aCriteriaObjs != null) {
        var sDefaultValue = null;
        for (var i = 0; i < aCriteriaObjs.length; i++) {
          if (aCriteriaObjsDefaultValueScript[i] != "") {
            eval("sDefaultValue = " + aCriteriaObjsDefaultValueScript[i] + ";");
            if (sDefaultValue != null) {
              document.getElementById(aCriteriaObjs[i]).value = sDefaultValue;
              sDefaultValue = null;
            }
          }
        }
      }
      
      try {
        parent.resizeRptIFrame(document.getElementById("ifrmReport").offsetTop + document.getElementById("ifrmReport").offsetHeight + 50);
      } catch (e) {}
      
      var objFrameCriteria = window.frameElement;
      if (objFrameCriteria) {
        objFrameCriteria.style.height = document.getElementById("ifrmReport").offsetTop + document.getElementById("ifrmReport").offsetHeight + 50;
      }
      
      if (iClaimViewLevel != iClaimViewLevel_All) {
        var objSelOffice = document.getElementById("OfficeID");
        if (objSelOffice) {
          objSelOffice.value = sOfficeID;
          objSelOffice.disabled = true;
        }
      }

      if (document.getElementById("csExportFormat").selectedIndex != -1 && document.getElementById("csExportFormat").options.length == 1) {
        document.getElementById("divExportFormat").style.display = "none";
      }
    }
  </script>
</head>
<body style="margin:0px;padding:0px;border:0px" tabindex="-1" onload="pageInit(); parent.showAnimation('hide')">
<%
  response.write lsReportsCriteria
%>

</body>
</html>