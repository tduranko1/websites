<!--#include file="includes/incCommonTop.asp"-->
<%
Dim liUpdateResult
Dim lsShopEmail
Dim lsShopName

liUpdateResult = Request("updateResult")
lsShopEmail = Request("ShopEmail")
lsShopName = Request("ShopName")
%>

<!--#include file="includes/incGetData.asp"-->

<html>
<head>
<title>LYNXSelect Program Registration</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">

<script language="JavaScript" type="text/JavaScript">

function cancelData()
{
  top.location = 'frmCFShopLogin.asp';
}


// Popup About page with application details
function callAbout()
{
  if (window.event.ctrlKey)
  {
    var lsDimensions = "dialogHeight:300px; dialogWidth:380px; "
    var lsSettings = "scroll:no; resizable:yes; status:no; help:no; center:yes; unadorned:yes;"
    var lsQuery = "frmAbout.asp";
    window.showModalDialog( lsQuery, window, lsDimensions + lsSettings );
  }
}

</script>

</head>

<BODY LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 background="./images/page_bg.jpg">

<div align="left">

	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
		<tr>
			<td width="169" align="left" valign="top">
  			<IMG SRC="images/logo_corner.jpg" ALT="" width="169" height="202">
			</td>
			<td align="left" valign="top">

        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" background="images/header_bg.jpg" height="132">
        	<tr>
        		<td height="50">&nbsp;</td>
        		<td align="right" valign="top" height="50">&nbsp;</td>
        	</tr>
        	<tr>
        		<td colspan=2 valign="top">&nbsp;</td>
        	</tr>
        	<tr>
        		<td align="left" valign="bottom" nowrap width="50%">
              <p class="header">
              LYNXSelect<sup style="font-size: 9px;">TM</sup> Registration for 
              <span id="spanlsPageTitle" lang="en-us"><em>CertifiedFirst </em><sup style="font-size: 9px;">TM</sup> Network</span>
            </td>
        		<td align="right" valign="bottom" width="50%">&nbsp;</td>
        	</tr>
        </table>
        <br>

        <%
          If liUpdateResult = "1" Then
        %>

          <br/><br/>
          <p class="login">
          Your <em>CertifiedFirst</em> Network shop information has been saved successfully.
          </p>
          <p class="login">
          Please remember to come back and finish the registration process at your earliest convenience.
          </p>
          <p class="login">
          Thank you for your time and interest in LYNXSelect.  We look forward to teaming with the <em>CertifiedFirst</em> Network program,
          and to exciting future opportunities.
          </p>
          <p class="login">
          Return to the <a href="frmCFShopLogin.asp" target="_top">Login</a> screen.
          </p>

        <%
          Else
        %>

          <table>
            <tr>
              <td nowrap="">
                <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
              </td>
              <td colspan="4" nowrap="" style="color:#FF0000; font-size:11pt; font-weight:bold;">
                Submit Error!
              </td>
            </tr>
            <tr>
              <td nowrap=""> </td>
              <td nowrap=""><br/>
              <p class="login">
              There has been a problem sending your <em>CertifiedFirst</em> Network shop information to LYNX Services.
              </p>
              <p class="login">
              You can go <a href="javascript:history.go(-2)">Back to the form</a> and resubmit it or
              you may return to the <a href="frmCFShopLogin.asp" target="_top">Login</a> screen.
              </p>
              </td>
            </tr>
          </table>
        
        <%
          End If
        %>
  
          <br/>
          <p class="login">
          For questions regarding the LYNXSelect Program or this enrollment process, please email us at <a href="mailto:lynxselect@lynxservices.com?subject=Questions regarding the LYNXSelect Program">lynxselect@lynxservices.com</a>.
          </p>
          <p class="login">
          For questions regarding the <em>CertifiedFirst</em> Network Program, please call 866-CERT-1ST.
          </p>

		</td>
	</tr>
</table>

</div>

<br>
<br>
<br>

<!--#include file="includes/incFooter.asp"-->

</body>
</html>
