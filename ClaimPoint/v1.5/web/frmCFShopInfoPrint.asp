<!--#include file="includes/incCommonTop.asp"-->

<%
  Dim lsInnerHtml
  lsInnerHtml = Request.Form("lstrShopData")
%>

<html>
<head>
<title>LYNXSelect Program Registration</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incBrowserSniffer.js" type="text/javascript"></script>
<SCRIPT language="JavaScript" src="includes/incCFShopInfo.js"></SCRIPT>

<script language="JavaScript" type="text/JavaScript">

function InitPage()
{
  setRO("INPUT");
  setRO("SELECT");
  setRO("TEXTAREA");
  setRO("A");
  if (is_ie5_5up == true)
  {
    window.print();
    window.close();
  }
  else
  {
    alert("Please print this form and then close it to return to the enrollment form. Thanks");
  }
}

function setRO(sTag)
{
  var lcolElms = document.getElementsByTagName(sTag);
	var liElmsLength = lcolElms.length;
	for(var x=0; x<liElmsLength; x++)
	{
    if (sTag == "SELECT")
      lcolElms[x].disabled = "true";
    else
      lcolElms[x].readOnly = "true";

    if (lcolElms[x].onblur != null)
      lcolElms[x].onblur = null;
    if (lcolElms[x].onkeypress != null)
      lcolElms[x].onkeypress = null;
    if (lcolElms[x].onchange != null)
      lcolElms[x].onchange = null;
    if (lcolElms[x].onbeforedeactivate != null)
      lcolElms[x].onbeforedeactivate = null;
    if (lcolElms[x].onkeydown != null)
      lcolElms[x].onkeydown = null;
    if (lcolElms[x].href != null)
      lcolElms[x].href= "javascript:doNone()";
  }
}

function doNone()
{return;}


</script>

</head>

<BODY onload=InitPage()>
	
<p><img src="images/LYNXSelect_ logo.jpg"></p>

<DIV id="txtFormBody"><%=lsInnerHtml%></DIV>

</body>
</html>
