<!--#include file="includes/incCommonTop.asp"-->

<%
  Dim liLynxID
  liLynxID = Request("LynxID")
%>

<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!-- v1.5.0.0 -->

<%
  lsPageName = "frmClaimAddVehicle.asp"
  lsPageTitle = "Add Vehicle to LYNX Id " & liLynxID
  lsPageSubTitle = "Add Vehicle"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcVehicle
  lsHelpPage = "CP_Claim_Information.htm"
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->

<script type="text/javascript" src="includes/incMM_Functions.js"></script>
<script language="JavaScript">
 document.cookie = "CombinedType=";
    function clickAssignmentCombined(Atype, Adesc,Combined)
  {
    if(Combined == "B")
       document.cookie = "CombinedType=B";
    else if(Combined == "C")
        document.cookie = "CombinedType=C";

      clickAssignment(Atype, Adesc);
  }

  function clickAssignment(Atype, Adesc)
  {
    var liAssignType = Atype;
    var lsAssignDesc = Adesc;
  	
    if (liAssignType == <%=liAssignmentType_PSA%>) //Program Shop Assignment
    {
  		document.getElementById('txtFrameSrc').value = "ProgramShop";
  		document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
  		document.getElementById('txtAssignmentId').value = liAssignType;
  	}
    else if (liAssignType == <%=liAssignmentType_CSA%>) 
    {
         	document.getElementById('txtFrameSrc').value = "ChoiceShop";
  		document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
  		document.getElementById('txtAssignmentId').value = liAssignType;
  	}
  	else if (liAssignType == <%=liAssignmentType_RRP%>) 
    {
  		document.getElementById('txtFrameSrc').value = "RepairReferral";
  		document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
  		document.getElementById('txtAssignmentId').value = liAssignType;
  	}
    else if (liAssignType == <%=liAssignmentType_ASDA%> || liAssignType == <%=liAssignmentType_DEA%> || liAssignType == <%=liAssignmentType_IAEA%>) //Audit
    {
  		document.getElementById('txtFrameSrc').value = "DeskAudit";
  		document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
  		document.getElementById('txtAssignmentId').value = liAssignType;
  	}
    else if (liAssignType == <%=liAssignmentType_ASDR%> || liAssignType == <%=liAssignmentType_DER%> || liAssignType ==  <%=liAssignmentType_IAER%> || liAssignType ==  <%=liAssignmentType_DRSER%> || liAssignType ==  <%=liAssignmentType_SER%>) //Desk Review
    {
  		document.getElementById('txtFrameSrc').value = "DeskReview";
  		document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
  		document.getElementById('txtAssignmentId').value = liAssignType;
  	}
    else if (liAssignType == <%=liAssignmentType_IAFA%> || liAssignType == <%=liAssignmentType_IAPO%> || liAssignType == <%=liAssignmentType_IAACV%>) //IA Assignment
    {
  		document.getElementById('txtFrameSrc').value = "IA";
  		document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
  		document.getElementById('txtAssignmentId').value = liAssignType;
  	}
    else
      return;
  	
    document.frmAssignment.submit();

  }
</script>

<!--#include file="includes/incHeaderTableNoIcons.asp"-->
<%
      dim lsInsCoDetailsCall, lsGUID
      lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"

      Dim lsAssignReturn
      lsAssignReturn = GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "AssignmentTypes.xsl", "CarrierRep=" & liCarrierRep & lcDelimiter & "OfficeID=" & liOfficeID & lcDelimiter & "ClaimViewLevel=" & liClaimViewLevel & lcDelimiter & "ClaimViewLevel_All=" & liClaimViewLevel_All, true)

      if lsAssignReturn <> "-1" then
        Response.write "Please select the vehicle assignment type to submit from the options below:<br><br>"
        Response.write lsAssignReturn
      else
        Response.write "Error! Unable to get assignment types."
      end if
%>

  <form name="frmAssignment" method="post" action="frmWaMainAddVehicle.asp">
  	<input name="FrameSrc" id="txtFrameSrc" type="hidden" value="">
    <input name="AssignmentDesc" id="txtAssignmentDesc" type="hidden" value="">
    <input name="AssignmentId" id="txtAssignmentId" type="hidden" value="">
    <input name="LynxID" id="txtLynxID" type="hidden" value="<%=liLynxID%>">
  </form>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
