<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.5 -->

<%
  Dim liLynxID, liAspectID, lsAssignmentType, liVehNumber, lsHistoryFilter
  
  liLynxID = Request("LynxID")
  liVehNumber = Request("VehNumber")
  liAspectID = Request("AspectID")
  lsAssignmentType = Request("AssignmentType")
  
  If liVehNumber <> "" Then
    lsHistoryFilter = "Vehicle " & liVehNumber
  Else
    lsHistoryFilter = "none"
  End If

  lsPageName = "frmClaimHistory.asp"
  lsPageTitle = "Claim Details"
  lsPageSubTitle = "History"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcClaim
  lsStyleSheet = "ClaimHistoryAlt.xsl"
  lsHelpPage = "CP_Claim_History.htm"
  bGridSort = True
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStartClm.asp"-->

<%
dim lbOverwriteExisting
lbOverwriteExisting = false
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspClaimHistory.asp">

<!--#include file="includes/incHeaderTableWithIcons.asp"-->

<!--#include file="includes/incClaimCommonTop.asp"-->

<%
Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "NavBar.xsl", "currentArea=" & lsPageSubTitle & lcDelimiter & "LynxID=" & liLynxID & lcDelimiter & "AssignmentType=" & lsAssignmentType & lcDelimiter & "AspectID=" & liAspectID & lcDelimiter & "VehNumber=" & liVehNumber, true)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
  Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "ClaimHeader.xsl", "pageTitle=" & lsPageSubTitle & lcDelimiter & "AspectID=" & liAspectID, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

<%
Response.Write GetData("<ClaimHistory InsuranceCompanyID=""" & liInsuranceCompanyID & """ LynxID=""" & liLynxID & """/>", lsStyleSheet, "HistoryFilter=" & lsHistoryFilter)
%>

<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>


<script type="text/javascript">
  if (document.getElementById("HistoryResults"))
  {
    var SrtViewTbl = new GridSort(document.getElementById("HistoryResults"), ["DateTime", "CaseInsensitiveString", "CaseInsensitiveString", "None"]);
    SrtViewTbl.sort(0, true);
  }
</script>


<!--#include file="includes/incCommonHTMLEndClm.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
