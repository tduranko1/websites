﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RentalCoverage.aspx.vb" Inherits="ClaimPointNet.RentalCoverage" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rental Management</title>
    <base target="_self" />
    <link href="includes/apd_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="diaRentalCoverage" title="Rental Coverage">
            <table style="width: 100%; border: 0;">
                <tr>
                    <td>
                        <img style="position:relative;left:15px;top:-1px;" src="images/modal_ML.gif" alt="" width="70" height="65" /></td>
                    <td style="width: 100%; background-image: url(images/modal_MC.gif);" valign="top">
                        <br />
                        <div style="text-align: center">
                            <p style="font-size: 15px;" class="bodyblue">Rental Management</p>
                        </div>
                    </td>
                    <td>
                        <img style="position:relative;left:-15px;top:-1px;"" src="images/modal_MR.gif" alt="" width="70" height="65" /></td>
                </tr>
            </table>

            <table border="0" class="topTableRentalClass" cellpadding="0" cellspacing="0" style="border: 1px solid #000099; border-collapse: collapse; margin: auto;" width="98%">
                <tbody>
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" style="border-bottom: 1px solid #000099; background: #E6E5F5; border-collapse: collapse;"
                                width="100%">
                                <tr>
                                    <td style="height: 20px">
                                        <span class="bodyblue">LYNX ID:</span>
                                        <span class="body">
                                            <asp:Label ID="txtRentalLynxID" runat="server" CssClass="lblRentalClass"
                                                Enabled="false" /></span>
                                    </td>
                                    <td align="right">
                                        <span class="bodyblue"></span>
                                        <span class="body"></span>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="4" cellspacing="0" style="border-collapse: collapse;"
                                width="100%">
                                <tr>
                                    <td class="bodyblue">Daily $ Max:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalDailyMax" runat="server" CssClass="lblRentalClass"
                                            Enabled="false" MaxLength="5" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Rental $ Max:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalMax" runat="server" CssClass="lblRentalClass"
                                            Enabled="false" MaxLength="5" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bodyblue">Days Max:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtDaysMax" runat="server" CssClass="lblRentalClass"
                                            Enabled="false" MaxLength="5" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Days Authorized:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalDaysAuthorized" runat="server"
                                            CssClass="lblRentalClass" Enabled="false" Width="25px" MaxLength="5" />
                                    </td>
                                </tr>
                            </table>

                            <table border="0" cellpadding="4" cellspacing="0" style="margin-top: 10px; border-bottom: 1px solid #000099; border-collapse: collapse;"
                                width="100%">
                                <tr>
                                    <td style="width: 20%; vertical-align: top;" class="bodyblue">RENTAL INSTRUCTIONS:
                                    </td>
                                    <td colspan="3" class="body">
                                        <asp:Label ID="txtRentalInstructions" runat="server" CssClass="lblRentalClass"
                                            Enabled="false" Width="100%" MaxLength="250" Height="60px" TextMode="MultiLine" Text="" />

                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="4" cellspacing="0" style="border-bottom: 1px solid #000099; border-collapse: collapse;"
                                width="100%">
                                <tr>
                                    <td colspan="5" style="border-top: 1px solid #000099; border-bottom: 1px solid #000099; background: #E6E5F5;">
                                        <table border="0" cellpadding="0" cellspacing="0" style="background: #E6E5F5; border-collapse: collapse;"
                                            width="100%">
                                            <tr>
                                                <td style="height: 20px">
                                                    <span class="bodyblue">Rental Details:</span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>



                            <div id="divscrollRentalCovarage" style="width: 100%; height: 80px; overflow-y: auto; border: 1px solid black;">
                                <table class="topTable" style="border-spacing: 0px; width: 100%;">
                                    <tr style="vertical-align: top;">
                                        <td style="width: 500px;">
                                             <asp:GridView ID="gvRentals" runat="server" AutoGenerateColumns="False" Font-Names="Tahoma" Font-Size="X-Small" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="6" GridLines="Horizontal" Width="100%" OnSelectedIndexChanged="gvRentals_SelectedIndexChanged">
                                                <AlternatingRowStyle BackColor="#F7F7F7" />
                                                <Columns>
                                                    <asp:BoundField DataField="RentalID" HeaderText="RentalID"
                                                        SortExpression="RentalID" HeaderStyle-Width="5px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="RentalAgency" HeaderText="Agency"
                                                        SortExpression="RentalAgency" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="ResConfNumber"
                                                        HeaderText="Res/Conf" SortExpression="ResConfNumber" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="RateTypeCD" HeaderText="Rate Type"
                                                        SortExpression="RateTypeCD" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="RentalStatus" HeaderText="Rental Status"
                                                        SortExpression="RentalStatus" HeaderStyle-Width="120px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="ClaimAspectID"
                                                        HeaderText="ClaimAspectID" SortExpression="ClaimAspectID" HeaderStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="VehicleClass"
                                                        HeaderText="VehicleClass" SortExpression="VehicleClass" HeaderStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="Rate" HeaderText="Rate"
                                                        SortExpression="Rate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="TaxRate" HeaderText="TaxRate"
                                                        SortExpression="TaxRate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="TaxedRate" HeaderText="TaxedRate"
                                                        SortExpression="TaxedRate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="Rental" HeaderText="Rental"
                                                        SortExpression="Rental" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="RentalNoOfDays"
                                                        HeaderText="RentalNoOfDays" SortExpression="RentalNoOfDays" HeaderStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="AuthPickupDate"
                                                        HeaderText="AuthPickupDate" SortExpression="AuthPickupDate" HeaderStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="StartDate" HeaderText="StartDate"
                                                        SortExpression="StartDate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="EndDate" HeaderText="EndDate"
                                                        SortExpression="EndDate" HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="RentalAgencyAddress1"
                                                        HeaderText="RentalAgencyAddress1" SortExpression="RentalAgencyAddress1" HeaderStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="RentalAgencyCity"
                                                        HeaderText="RentalAgencyCity" SortExpression="RentalAgencyCity" HeaderStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="RentalAgencyState"
                                                        HeaderText="RentalAgencyState" SortExpression="RentalAgencyState" HeaderStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="RentalAgencyZip"
                                                        HeaderText="RentalAgencyZip" SortExpression="RentalAgencyZip" HeaderStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="RentalAgencyPhone"
                                                        HeaderText="RentalAgencyPhone" SortExpression="RentalAgencyPhone" HeaderStyle-Width="100px"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                </Columns>
                                                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                                                <HeaderStyle BackColor="#629ad6" Font-Bold="True"
                                                    ForeColor="#F7F7F7" />
                                                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C"
                                                    HorizontalAlign="Right" />
                                                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                                                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True"
                                                    ForeColor="#F7F7F7" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <table border="0" cellpadding="4" cellspacing="0" style="border-collapse: collapse;"
                                width="100%">
                                <tr>
                                    <td class="bodyblue">Res/Conf:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalResConf" runat="server" CssClass="lblRentalClass"
                                            Width="85px" MaxLength="15" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Vehicle Class:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalVehicleClass" runat="server" CssClass="lblRentalClass"
                                            Width="85px" MaxLength="15" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bodyblue">Status:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalStatus" runat="server" CssClass="lblRentalClass"
                                            Width="85px" MaxLength="15" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Rate Type:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalRateType" runat="server" CssClass="lblRentalClass"
                                            Enabled="false" Width="25px" MaxLength="5" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bodyblue">Rate:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalRate" runat="server" CssClass="lblRentalClass"
                                            Width="100px" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Tax Rate:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalTaxRate" runat="server" CssClass="lblRentalClass"
                                            Width="100px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bodyblue">Rental Vendor:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalVendor" runat="server" CssClass="lblRentalClass"
                                            Width="100px" Enabled="false" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Auth Pickup:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalAuthPickup" runat="server" CssClass="lblRentalClass"
                                            Width="75px" MaxLength="10" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="bodyblue">Rental Start Date:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalStartDate" runat="server" CssClass="lblRentalClass"
                                            Width="75px" MaxLength="10" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Rental End Date:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalEndDate" runat="server" CssClass="lblRentalClass"
                                            Width="75px" MaxLength="10" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="bodyblue">Rental Days:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalNoOfDays" runat="server" CssClass="lblRentalClass"
                                            Width="25px" Enabled="false" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Rental:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalDollars" runat="server" CssClass="lblRentalClass"
                                            Width="100px" Enabled="false" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="bodyblue">Rental Phone:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalPhone" runat="server" CssClass="lblRentalClass"
                                             Enabled="false" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Rental Address:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalAddress" runat="server" CssClass="lblRentalClass"
                                            Width="130px" MaxLength="100" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="bodyblue">Rental City:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalCity" runat="server" CssClass="lblRentalClass"
                                            Width="130px" MaxLength="100" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>
                                    <td class="bodyblue">Rental State:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalState" runat="server" CssClass="lblRentalClass"
                                            Width="30px" MaxLength="2" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="bodyblue">Rental Zip:
                                    </td>
                                    <td class="body">
                                        <asp:Label ID="txtRentalZip" runat="server" CssClass="lblRentalClass"
                                            Width="70px" MaxLength="10" />
                                    </td>
                                    <td style="width: 20px">&#160;</td>

                                </tr>

                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

    </form>
</body>
</html>
