<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->

<%
lsPageName = "getReportPDF.asp"
liPageType = lcAfterInsuranceCompanySelect

if liViewReport <> 1 then
  response.write "You don't have permission to view Reports. Please contact your supervisor."
  response.end
end if
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->

<%
  On Error Resume Next

  'Create and initialize DataPresenter
  Dim lsReportParams
  dim lsMode
  dim lsXMLCall
  Dim loStream
  Dim sFormat  
  Dim docFormat
  Dim strEncodedXMl
  lsReportParams = request.form("reportParams")
  sFormat = request.form("reportFormat")
  
  lsReportParams = lsReportParams & "&ShowAllClients=0"
  
  lsReportParams = Replace(lsReportParams, "&", "&amp;")
  lsXMLCall = "<Report sReportFileName='" & request.form("reportFileName") & "' " & _
              "sStoredProc='" & request.form("storedProc") & "' " & _
              "sReportParams='" & lsReportParams & "' " & _
              "sReportStaticParams='" & replace(request.form("staticParams"), "&", "&amp;") & "' " & _
              "sReportFormat='" & request.form("reportFormat") & "' />"

Dim lsReportFileName
Dim lssStoredProc
Dim lssReportParams
Dim lssReportStaticParams
Dim lssReportFormat


lsReportFileName = request.form("reportFileName")
lssStoredProc = request.form("storedProc")
lssReportParams = lsReportParams
lssReportStaticParams = replace(request.form("staticParams"), "&", "&amp;")
lssReportFormat = request.form("reportFormat")


  lsMode = request.QueryString("mode")
  
'  set loStream = loAPD.GetAPDDocument(lsXMLCall)
  
'  if not (loStream is Nothing) then
      SELECT CASE sFormat
        Case "DOC"
          docFormat = "application/msword"
        Case "XLS"
          docFormat = "application/x-msexcel"
        Case "RTF"
          docFormat = "application/msword"
        Case ELSE
          docFormat = "application/pdf"
      END SELECT
      
      if (lsMode = "save") then
        Response.AddHeader "Content-Disposition", "attachment;filename=" & request.form("friendlyName")
      else
        Response.AddHeader "Content-Disposition", "inline;filename=" & request.form("friendlyName")
      end if
      'Response.BinaryWrite loAPD.GetAPDDocument(lsXMLCall)

'strEncodedXMl = Server.HTMLEncode(lsXMLCall)


'response.write(lsXMLCall)
'response.end


response.cookies("ReportFileName") = lsReportFileName 
response.cookies("StoredProc") = lssStoredProc 
response.cookies("ReportParams") = lssReportParams 
response.cookies("ReportStaticParams") = lssReportStaticParams 
response.cookies("ReportFormat") = lssReportFormat 
response.cookies("PrintFormat") = docFormat
 
       Response.Redirect "frmGetReportPDF.aspx"
'  end if
  
'  set loStream = nothing

  On Error GoTo 0
%>
