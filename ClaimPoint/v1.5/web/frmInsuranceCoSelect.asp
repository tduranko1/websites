<!--#include file="includes/incCommonTop.asp"-->
<%
  lsPageName = "frmInsuranceCoSelect.asp"
  lsPageTitle = "Select Insurance Company"
  liPageType = lcBeforeInsuranceCompanySelect

  Response.Cookies("CPSession")("currentClaim") = ""
  Response.Cookies("CPSession")("currentClaim").Expires = Now()-1
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->

<script language="JavaScript">
	function setInsuranceCompanyID(piInsuranceCompanyID, psInsuranceCompanyName)
	{
		document.getElementById("txthidInsuranceCompanyID").value = piInsuranceCompanyID;
    document.getElementById("txthidInsuranceCompanyName").value = psInsuranceCompanyName;
//    document.frmMainForm.hidInsuranceCompanyID.value = piInsuranceCompanyID;
		document.frmMainForm.submit();
	}
</script>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspInsuranceCoSelect.asp">
<input type="hidden" name="hidInsuranceCompanyID" id="txthidInsuranceCompanyID">
<input type="hidden" name="hidInsuranceCompanyName" id="txthidInsuranceCompanyName">
<input type="hidden" name="fromPage" id="fromPage" value="InsuranceCoSelect">

<%
  Dim lsGUID, lsInsCoListCall
  lsGUID = Request.Cookies("CPSession")("guidArray0")

  lsInsCoListCall = "<InsuranceCompanyList />"

  Response.Write GetPersistedData(lsInsCoListCall, lsGUID, "InsuranceCoSelect.xsl", "", true)
%>

</form>



<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
