<!--#include file="includes/incCommonTop.asp"-->
<%
lsPageName = "frmPedestrianInfo.asp"
lsPageTitle = "Claim Details"
lsPageSubTitle = "Pedestrian"
lsStyleSheet = "PedestrianInfoAlt.xsl"
liPageType = lcAfterClaimSearch
liPageIcon = lcClaim
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->
<%
dim lsPedestrian 
dim laPedestrian
lsPedestrian = Trim(Request("P"))

if lsPedestrian = "" or lsPedestrian="0" then Response.Redirect "frmClaimInfo.asp"
if right(lsPedestrian, 1) = "," then lsPedestrian = left(lsPedestrian, len(lsPedestrian) - 1)

laPedestrian = split(lsPedestrian, ",")

dim lbOverwriteExisting
lbOverwriteExisting = false
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspPedestrianInfo.asp">

<!--#include file="includes/incHeaderTableWithIcons.asp"-->

<!--#include file="includes/incClaimCommonTop.asp"-->

<%
Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "NavBar.xsl", "currentArea=" & lsPageSubTitle, true)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "ClaimHeader.xsl", "pageTitle=" & lsPageSubTitle, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

<%

liStop = ubound(laPedestrian)

for liCounter = 0 to liStop
	lsPedestrian = laPedestrian(liCounter)
	if len(lsPedestrian) > 0 and lsPedestrian <> "0" then
		Response.Write GetData("<ClaimPedestrian InsuranceCompanyID=""" & liInsuranceCompanyID & """ InvolvedID=""" & lsPedestrian & """/>", lsStyleSheet, "callNumber=" & liCounter)
		if liCounter < liStop then
		%> 
			<!--#include file="includes/incMultipleCallSeparator.asp"-->
		<%
		end if
	end if
next

%>

<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
