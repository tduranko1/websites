<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!-- v1.4.5.0 -->

<%
  Dim lsCurrentAssignmentDesc, lsPrevPage
  lsCurrentAssignmentDesc = Request.Cookies("CPSession")("CurrAssignmentDescription")
  lsPrevPage = Request("fromPage")

  lsPageName = "frmWaIAAssignment.asp"
  lsPageTitle = lsCurrentAssignmentDesc & " - Claim Info"
  liPageType = lcAssignmentPage

  Dim lsInsCoDetailsCall, lsCarrierUsersListCall
  
  lsInsCoConfigGUID = Request.Cookies("CPSession")("guidArray5")
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<html>
<head>
<title><%=lsCurrentAssignmentDesc%> - Claim Info</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/incCalendar.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "<%= lsPrevPage %>";
  gsVehCount = "";

            <%
              lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
            
              Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaClaimNumberValidation.xsl", "OfficeID=" & liOfficeID, true)
            %>
  
  function pageInit()
  {
    if(parent)
      parent.resetTimeOut();
      
    if (parent)
      parent.gsHelpSubTopicPage = "CP_IA_Assignment.htm";
    
    var loSel = document.getElementById('txtCallerRelationToInsuredID');
    var loSelLength = loSel.options.length;
    for (var i=0; i<loSelLength; i++)
    {
      if (loSel.options[i].value == '14')
        loSel.options[i].selected = true;
    }
    loSel.disabled = true;

    var liUserType = <%=liCarrierRep%>;
    var liAllowCarrierRepSelectionFlag = <%=liAllowCarrierRepSelectionFlag%>;

    if (liUserType == 1 && liAllowCarrierRepSelectionFlag == 0) //carrier user and AllowCarrierRepSelectionFlag is false
    {
      document.getElementById("txtCallerNameFirst").disabled = true;
      document.getElementById("txtCallerNameLast").disabled = true;
      document.getElementById('txtCoverageClaimNumber').focus();
    }
    else
      document.getElementById('txtCarrierUser').focus();
    

    if (gsPrevPage == "ClaimSummary")
    {
      if (parent)
      {
        parent.getClaimInfoData();
  
        var lotmpCoverageSelID = document.getElementById("txtCoverageProfileUiCD").value;
        var lotmpExposureSelID = document.getElementById("txtExposureCD").value;
        document.getElementById("txtExposureCD").value = lotmpExposureSelID;
        updParty(lotmpExposureSelID);
        document.getElementById("txtCoverageProfileUiCD").value = lotmpCoverageSelID;
        
        if (liUserType == 0 || liAllowCarrierRepSelectionFlag == 1) //non-carrier user or AllowCarrierRepSelectionFlag is true
          updCallerName();
      }
    }
    else
    {
      document.getElementById("txtTimeStarted").value = getCurrentDT();
      document.getElementById("txtIntakeStartSeconds").value = getCurrentTimeSec();

      if (liAllowCarrierRepSelectionFlag == 1) //AllowCarrierRepSelectionFlag is true
        updCallerName();

      updParty("");
    }
  }
  
  
  function chkClaimNumValidity()
  {
    if ((document.getElementById("txtCallerNameFirst").value.length == 0 || document.getElementById("txtCallerNameLast").value.length == 0))
    {
      alert("Please select a Claim Assigner name from the drop-down list.");
      return;
    }

    var objInput = document.getElementById('txtCoverageClaimNumber');
    var lbReturn = ClaimNumberFormat(objInput);
    if (lbReturn == true)
      parent.tsIAAssignment();
    else
      return;
  }


  function updCallerName()
  {
    var lsCarrierName = document.getElementById('txtCarrierUser').value;
    var laryUser = lsCarrierName.split("|");
    document.getElementById("txtCallerNameLast").value = laryUser[0];
    document.getElementById("txtCallerNameFirst").value = laryUser[1];
    document.getElementById("txtCarrierRepOfficeID").value = laryUser[2];
    document.getElementById("txtCarrierRepUserID").value = laryUser[3];
  }

  function updParty(selIndex)
  {
    if (selIndex == "")
    {
      updateSelect('selCoverage',0);
    }
    else if (selIndex == "1")
    {
      updateSelect('selCoverage',1);
       partySelChange(1);
    }
    else if (selIndex == "3")
    {
      updateSelect('selCoverage',2);
      partySelChange(3);
    }
    
    var selObj = document.getElementById('txtCoverageProfileUiCD');
    updCovReqFlds(selObj.options[selObj.selectedIndex].value, false);
  }
  
  function updCovReqFlds(selIndex, bUpdCovFlds)
  {
    var oWarnParty = document.getElementById("warnParty");
    if (bUpdCovFlds == true && document.getElementById("txtExposureCD").value == "")
    {
      oWarnParty.style.display = "inline";
      document.getElementById("txtCoverageProfileUiCD").selectedIndex = 0;
      setTimeout("document.getElementById('warnParty').style.display = 'none'", 3000)
      return;
    }
    else
    {
      oWarnParty.style.display = "none";
    }

    var aVal = new Array("", "")
    if (selIndex != "") 
      var aVal = selIndex.split("|");

    document.getElementById("txtCoverageProfileCD").value = aVal[0];
    document.getElementById("txtClientCoverageTypeUiID").value = aVal[1];
    var oSel = document.getElementById("txtCoverageProfileUiCD");
    document.getElementById("txtClientCoverageTypeDesc").value = oSel.options[oSel.selectedIndex].text;
  }

  var laryParty = new Array(1);
  laryParty[0] = new Array("3rd Party","3");
  
  var laryCoverage = new Array(3);
  
  
  function updateSelect(sel, n)
  {
    var loSel;
    var laryTmp;
    if (sel == "selParty")
    {
      loSel = document.getElementById('txtExposureCD');
      laryTmp = laryParty[n];
    }
    else if (sel == "selCoverage")
    {
      loSel = document.getElementById('txtCoverageProfileUiCD');
      laryTmp = laryCoverage[n];
    }
    var liCurr = loSel.options.length;
    for (var j=liCurr; j>0; j--)
      loSel.options[j] = null;
    for (var i=0; i<laryTmp.length; i++)
    {
      loSel.options[loSel.options.length] = new Option(laryTmp[i], laryTmp[i+1]);
      i++;
    }
    
    if (loSel.id == "txtCoverageProfileUiCD" && loSel.options.length == 2) {//the first item is blank.
      // auto select the only item in the coverage.
      loSel.selectedIndex = 1;
    }
  }
  

  function updDeductible(selIndex)
  {
    if (selIndex == "LIAB")
    {
      document.getElementById('txtDeductibleAmt').value = "";
      document.getElementById('txtDeductibleAmt').disabled = "true";
      document.getElementById('txtDeductibleAmt').style.background = "ButtonHighlight";
    }
    else
    {
      document.getElementById('txtDeductibleAmt').disabled = "";
      document.getElementById('txtDeductibleAmt').style.background = "White";
    }
  }
  
  
  function formatLossDate() 
  {
    if (document.getElementById('txtLossDate').value.length != 0)
    {
      var lsDate = document.getElementById('txtLossDate').value;
      var lsMonth, lsDay, lsYear;
      if (lsDate.indexOf("/") != -1) {
        var laryDate = lsDate.split("/");
        lsMonth = laryDate[0];
        lsDay = laryDate[1];
        lsYear = laryDate[2];
      } else {
        //try to format with regular expression.
        lsYear = lsDate.substr(lsDate.length - 4, 4);
        var lsReminder = lsDate.substr(0, lsDate.length - 4);
        switch (lsReminder.length) {
          case 4:
            lsMonth = lsReminder.substr(0, 2);
            lsDay = lsReminder.substr(2, 2);
            break;
          case 3:
            if (parseInt(lsReminder.substr(0, 2), 10) > 12) {
              lsMonth = lsReminder.substr(0, 1);
              lsDay = lsReminder.substr(1, 2)
            } else {
              lsMonth = lsReminder.substr(0, 2);
              lsDay = lsReminder.substr(2, 1)              
            }
            break;
          case 2:
            lsMonth = lsReminder.substr(0, 1);
            lsDay = lsReminder.substr(1, 1);
            break;
        }
      }
  
      var lbBadDate = false;
      if (lsMonth == undefined || lsMonth < 1 || lsMonth > 12)
        lbBadDate = true;
      if (lsDay == undefined || lsDay < 1 || lsDay > 31)
        lbBadDate = true;
      if (lsYear == undefined || lsYear < 1)
        lbBadDate = true;
      if (lbBadDate == true)
      {
        alert("Please enter the date as MM/DD/YYYY or M/D/YY or MMDDYYYY.");
        document.getElementById('txtLossDate').focus();
        return;
      }
  
      if (lsMonth < 10 && lsMonth.charAt(0) != 0)
        lsMonth = "0"+lsMonth;

      if (lsDay < 10 && lsDay.charAt(0) != 0)
        lsDay = "0"+lsDay;

      var lsMydate = new Date();
      if (lsYear == undefined || lsYear == "" || (lsYear >= 100 && lsYear < 1900))
      {
        lsYear = lsMydate.getFullYear();
      }
      else if (lsYear >= 0 && lsYear < 100)
      {
        lsYear = Number(lsYear) + 2000;
      }
      else if ((lsYear >= 1900 && lsYear < 2000) || (lsYear > 2000))
      {
        lsYear = Number(lsYear);
      }
      else
      {
        lsYear = lsMydate.getFullYear();
      }

      document.getElementById('txtLossDate').value = lsMonth + "/" + lsDay + "/" + lsYear;
    }
  }
  
  function partySelChange(party)
  {
    if (party == 1)
    {
      if (document.getElementById("txtOwnerNameFirst").value == "")
        document.getElementById("txtOwnerNameFirst").value = document.getElementById("txtInsuredNameFirst").value;
      
      if (document.getElementById("txtOwnerNameLast").value == "")
        document.getElementById("txtOwnerNameLast").value = document.getElementById("txtInsuredNameLast").value;
      
      if (document.getElementById("txtOwnerBusinessName").value == "")
        document.getElementById("txtOwnerBusinessName").value = document.getElementById("txtInsuredBusinessName").value;
    }
    else if (party == 3)
    {
      if ((document.getElementById("txtOwnerNameFirst").value == document.getElementById("txtInsuredNameFirst").value) && (document.getElementById("txtOwnerNameLast").value == document.getElementById("txtInsuredNameLast").value))
      {
        document.getElementById("txtOwnerNameFirst").value = "";
        document.getElementById("txtOwnerNameLast").value = "";
      }
      
      if (document.getElementById("txtOwnerBusinessName").value == document.getElementById("txtInsuredBusinessName").value)
        document.getElementById("txtOwnerBusinessName").value = "";
    }
  }

  
  function goCancel()
  {
    if (parent)
    {
      if (gsPrevPage == "ClaimSummary")
      {
        parent.document.getElementById("uiFrame").src = "frmWaIAAssignmentSummary.asp?fromPage=IAAssignment&vehCount=0,0";
      }
      else
      {
        var lbConfirmCancel = window.confirm("Stop entering this assignment and return to My Claims Desktop?");
        if (lbConfirmCancel)
          top.location = "frmMyClaimsDesktop.asp";
      }
    }
  }


  var gPrefFlag
  function setOwnerCB(cbObj)
  {
    var loTbl = document.getElementById("tblOwnerCb");
    var laryChkBxs = loTbl.getElementsByTagName("INPUT");
    var cbLength = laryChkBxs.length;
    for (var j=0; j<cbLength; j++)
    {
      if (laryChkBxs[j].type == "checkbox")
      {
        if (laryChkBxs[j] == cbObj)
        {
          if (gPrefFlag)
          {
            laryChkBxs[j].checked = false;
            break;
          }
        }
        else
          laryChkBxs[j].checked = false;
      }
    }
  }

  
  function getOwnerBestPhone()
  {
    var laryChkBxs = document.frmIAAssignmentInfo.cb_OwnerBestPhoneCD;
    var lsOwnerBestPhoneCB = "";
    var cbLength = laryChkBxs.length;
    for (var i=0; i<cbLength; i++)
    {
      if (laryChkBxs[i].checked == true)
        lsOwnerBestPhoneCB = laryChkBxs[i].value;
    }
    return lsOwnerBestPhoneCB;
  }

  
  function ClaimNumberFormat(objInput) {
    var bValid = false;
    var obj = document.getElementById("txtCarrierRepOfficeID");
    if (obj) {
      if (obj.value > "0")
        var bValid = FormatClaimNumber(objInput, obj.value, larrOfficeID, larrClaimNumberFormatJS, larrClaimNumberValidJS, larrClaimNumberMsgText, true);
    }
    return bValid;
  }

  function checkIfExists(){
    var lsClaimNumber;
    lsClaimNumber = document.getElementById("txtCoverageClaimNumber").value;
    document.getElementById("frmCheckClaimNumber").src = "frmClaimNumberCheck.asp?cn=" + lsClaimNumber + "&ob=txtCoverageClaimNumber";
  }


  //hides the calender if clicked outside of it
  document.onclick = hideCalFrame;

</script>

</head>

<body onload="resizeWaIframe(0); pageInit(); parent.hideMsg();" tabIndex="-1">

<div id="divContainer">

  <form name="frmIAAssignmentInfo">
    <input id="txtTimeStarted" type="hidden">
    <input id="txtIntakeStartSeconds" type="hidden">
    <input id="txtInsuranceCompanyID" type="hidden" value="<%=liInsuranceCompanyID%>" >
    <input id="txtCarrierRepUserID" type="hidden" value="<%=liUserID%>" >
    <input id="txtCarrierRepOfficeID" type="hidden" value="<%=liOfficeID%>" opt="yes">
    <input id="txtCarrierRepNameFirst" type="hidden" value="<%=lsUserNameFirst%>" >
    <input id="txtCarrierRepNameLast" type="hidden" value="<%=lsUserNameLast%>">
    <input id="txtCarrierRepPhoneDay" type="hidden"  value="<%=lsPhoneAreaCode%>-<%=lsPhoneExchangeNumber%>-<%=lsPhoneUnitNumber%>" >
    <input id="txtCarrierRepEmailAddress" type="hidden" value="<%=lsUserEmail%>" >
    <input id="txtCarrierOfficeName" type="hidden" value="<%=lsOfficeName%>" >
    <input id="txtCarrierName" type="hidden" value="<%=lsInsuranceCompanyName%>" >
    <input id="txtFNOLUserID" type="hidden" value="<%=liUserID%>" >
    <input id="txtCoverageProfileCD" type="hidden" value="">
    <input id="txtAssignmentDescription" type="hidden" value="<%=lsCurrentAssignmentDesc%>" >

  <table width="600" border="0" cellpadding="10" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse">
    <tr>
      <td nowrap valign="top">

        <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
          <DIV style="position:relative; top:-10px; width:100px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
            &nbsp;&nbsp;Claim Information&nbsp;&nbsp;
          </DIV>
          
          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
            <colgroup>
              <col width="128px"/>
              <col width="18px"/>
              <col width="160px"/>
              <col width="273px"/>
            </colgroup>
            <tr>
              <td colspan="3" nowrap class="legendnopad">
  <%
    If liCarrierRep = 0 or liAllowCarrierRepSelectionFlag = 1 Then
  %>
                Claim Assigned by: <span style="font-size:11pt;">*</span>
  <%
    Else
  %>
                Claim Assigned by: <span style="font-size:11pt;">*</span>
                <span style="color:#000000; font-weight:normal; font-size:7pt">(First/Last)</span>
  <%
    End If
  %>
              </td>
              <td nowrap class="legendnopad">
                Assigner Relation to Insured: <span style="font-size:11pt;">*</span>
              </td>
            </tr>
            <tr>
              <td colspan="3" nowrap>
  <%
    If liCarrierRep = 0 or liAllowCarrierRepSelectionFlag = 1 Then

    lsCarrierUsersListCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
  
    Response.Write GetPersistedData(lsCarrierUsersListCall, lsInsCoConfigGUID, "CarrierUsersListAll.xsl", "EnabledFlag=1" & lcDelimiter & "AllowCarrierRepSelectionFlag=" & liAllowCarrierRepSelectionFlag & lcDelimiter & "UserID=" & liUserID, true)
  %>
                <input name="CallerNameFirst" id="txtCallerNameFirst" type="hidden" value="">
                <input name="CallerNameLast" id="txtCallerNameLast" type="hidden" value="">
  <%
    Else
  %>
                <input name="CallerNameFirst" id="txtCallerNameFirst" type="text" value="<%=lsUserNameFirst%>" onBlur="ldTrim(this)" size="15" maxlength="50" tabIndex="1">
                &nbsp;&nbsp;
                <input name="CallerNameLast" id="txtCallerNameLast" type="text" value="<%=lsUserNameLast%>" onBlur="ldTrim(this)" size="25" maxlength="50" tabIndex="2">
  <%
    End If
  %>
              </td>
              <td nowrap>

                <%
                  lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
                
                  Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaCallerRelations.xsl", "tabIndexNo=3", true)
                %>

              </td>
            </tr>
            <tr>
              <td colspan="2" nowrap class="legend">Claim Number: <span style="font-size:11pt;">*</span></span></td>
              <td nowrap class="legend">Loss Date: <span style="font-size:11pt;">*</span>
              <span style="color:#000000; font-weight:normal; font-size:7pt">(MM/DD/YYYY)</span></td>
              <td nowrap class="legend">Loss State: <span style="font-size:11pt;">*</span></td>
            </tr>
            <tr>
              <td colspan="2" nowrap>
                <input name="CoverageClaimNumber" id="txtCoverageClaimNumber" type="text" value="" onBlur="ldTrim(this);ClaimNumberFormat(this);checkIfExists()" size="20" maxlength="30" required="true" tabIndex="4">
              </td>
              <td nowrap>
                <input name="LossDate" id="txtLossDate" type="text" value="" onKeyPress="return numbersOnly(this,event,0,1)" onBlur="formatLossDate()" size="10" maxlength="10" required="true" tabIndex="6">
                <a href="javascript:void(0)" onclick="showCalIframe(this, 'txtLossDate')" id="calCall" tabIndex="-1"><img src="images/calendar.gif" alt="" width="24" height="17" border="0" title="Click to select date from calendar"></a>
              </td>
              <td nowrap>

                <%
                   lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
                
                   Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaLossState.xsl", "OfficeID=" & liOfficeID & lcDelimiter & "tabIndexNo=6" & lcDelimiter & "ShowAllStates=1", true)
                %>

              </td>
            </tr>
            <tr>
              <td nowrap class="legend">Insured First Name: <span style="font-size:11pt;">*</span></td>
              <td colspan="2" nowrap class="legend">Insured Last Name: <span style="font-size:11pt;">*</span></td>
              <td nowrap class="legend">Insured Business Name: *
                <span style="color:#000000; font-weight:normal; font-size:8pt">(If Insured is a business)</span>
              </td>
            </tr>
            <tr>
              <td nowrap>
                <input name="InsuredNameFirst" id="txtInsuredNameFirst" type="text" value="" onBlur="ldTrim(this)" size="15" maxlength="50" tabIndex="7">
              </td>
              <td colspan="2" nowrap>
                <input name="InsuredNameLast" id="txtInsuredNameLast" type="text" value="" onBlur="ldTrim(this)" size="25" maxlength="50" tabIndex="8">
              </td>
              <td nowrap>
                <input name="InsuredBusinessName" id="txtInsuredBusinessName" type="text" value="" onBlur="ldTrim(this)" size="40" maxlength="50" tabIndex="9">
              </td>
            </tr>
            <tr>
              <td colspan="4" nowrap valign="top">

                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
                  <colgroup>
                    <col width="200px"/>
                    <col width="370px"/>
                  </colgroup>
                  <tr>
                    <td nowrap class="legend">IA Assignment Type: <span style="font-size:11pt;">*</span></span></td>
                    <td width="100%" nowrap class="legend">Special Handling Instructions for the IA:
                      <span id="commFldMsgRemarks" style="color:#000000; font-weight:normal; font-size:7pt">(max.:500 characters)</span>
                    </td>
                  </tr>
                  <tr>
                    <td nowrap valign="top">
      
                      <%
                         lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
                      
                         Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaIAAssignmentTypes.xsl", "OfficeID=" & liOfficeID & lcDelimiter & "tabIndexNo=10", true)
                      %>
      
                    </td>
                    <td nowrap>
                      <textarea name="Remarks" id="txtRemarksToIA" wrap="physical" style="width:100%; height:66px;" onKeyDown="CheckInputLength(this, 500, 'commFldMsgRemarks')" tabIndex="11"></textarea>
                    </td>
                  </tr>
                </table>

              </td>
            </tr>
          </table>

        </DIV>

      </td>
    </tr>
    <tr>
      <td nowrap valign="top">
        
        <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
          <DIV style="position:relative; top:-10px; width:70px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
            &nbsp;&nbsp;Loss information&nbsp;&nbsp;
          </DIV>

          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
            <colgroup>
              <col width="100px"/>
              <col width="230px"/>
              <col width="120px"/>
              <col width="120px"/>
            </colgroup>
            <tr>
              <td nowrap class="legendnopad">Party: <span style="font-size:11pt;">*</span></span></td>
              <td nowrap class="legendnopad">Coverage: <span style="font-size:11pt;">*</span>
              <span id="warnParty" style="color:#FF0000; display:none; font-size:11px; font-weight:bold;"> [Please select a Party first]</span></td>
              <td nowrap class="legendnopad">&nbsp;&nbsp;&nbsp;Deductible:</td>
              <td nowrap class="legendnopad">&nbsp;&nbsp;&nbsp;Limit:</td>
            </tr>
            <tr>
              <td nowrap>
                <select name="ExposureCD" id="txtExposureCD" onChange="updParty(this.options[this.selectedIndex].value)" required="true" tabIndex="12">
                  <option value=""></option>
                  <option value="1">1st Party</option>
                  <option value="3">3rd Party</option>
                </select>
              </td>
              <td nowrap>
                <select name="CoverageProfileUiCD" id="txtCoverageProfileUiCD" onChange="updCovReqFlds(this.options[this.selectedIndex].value, true)" required="true" tabIndex="13">
                  <option value="" selected></option>

                    <%
                      lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
                    
                      Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaCoverageTypes.xsl", "", true)
                    %>

                </select>
                <input type="hidden" name="ClientCoverageTypeUiID" id="txtClientCoverageTypeUiID"/>
                <input type="hidden" name="ClientCoverageTypeDesc" id="txtClientCoverageTypeDesc"/>
              </td>
              <td nowrap>
                $<input name="DeductibleAmt" id="txtDeductibleAmt" type="text" onBlur="FormatCurrencyObj(this)" onKeyPress="return numbersOnly(this,event,1)" value="" size="12" maxlength="6" tabIndex="14">
              </td>
              <td nowrap>
                $<input name="LimitAmt" id="txtLimitAmt" type="text" onBlur="FormatCurrencyObj(this)" onKeyPress="return numbersOnly(this,event,1)" value="" size="12" maxlength="6" tabIndex="15">
              </td>
            </tr>
            <tr>
              <td colspan="4" nowrap class="legend">
                Description of Loss/Damages:
                <span id="commFldMsgLoss" style="color:#000000; font-weight:normal; font-size:7pt">(max.:500 characters)</span>
              </td>
            </tr>
            <tr>
              <td colspan="4" nowrap>
                <textarea name="Remarks" id="txtDescLossDamage" wrap="physical" style="width:100%; height:52px;" onKeyDown="CheckInputLength(this, 500, 'commFldMsgLoss')" tabIndex="16"></textarea>
              </td>
            </tr>
          </table>

        </DIV>

      </td>
    </tr>
    <tr>
      <td nowrap valign="top">
        
        <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
          <DIV style="position:relative; top:-10px; width:60px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
            &nbsp;&nbsp;Vehicle Information&nbsp;&nbsp;
          </DIV>

          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
            <tr>
              <td nowrap valign="top">

                <table border="0" cellpadding="0" cellspacing="0" id="tblContactCb" style="border-collapse:collapse; table-layout:fixed;">
                  <colgroup>
                    <col width="122px"/>
                    <col width="148px"/>
                  </colgroup>
                  <tr>
                    <td nowrap class="legendnopad">Owner First Name: <span style="font-size:11pt;">*</span></td>
                    <td nowrap class="legendnopad">Owner Last Name: <span style="font-size:11pt;">*</span></td>
                  </tr>
                  <tr>
                    <td nowrap>
                      <input name="OwnerNameFirst" id="txtOwnerNameFirst" type="text" onBlur="ldTrim(this)" size="15" maxlength="50" required="true" tabIndex="17">
                    </td>
                    <td nowrap>
                      <input name="OwnerNameLast" id="txtOwnerNameLast" type="text" onBlur="ldTrim(this);" size="23" maxlength="50" required="true" tabIndex="18">
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" nowrap class="legendnopad">Owner Business Name: <span style="font-size:11pt;">*</span>
                      <span style="color:#000000; font-weight:normal; font-size:8pt">(If owner is a business)</span>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" nowrap>
                      <input name="OwnerBusinessName" id="txtOwnerBusinessName" type="text" onBlur="ldTrim(this)" size="40" maxlength="50" tabIndex="19">
                    </td>
                  </tr>
                </table>

              </td>
              <td nowrap valign="top">

                <table border="0" cellpadding="0" cellspacing="0" id="tblOwnerCb" style="border-collapse:collapse; table-layout:fixed;">
                  <colgroup>
                    <col width="100px"/>
                    <col width="190px"/>
                  </colgroup>
                  <tr>
                    <td valign="bottom" align="center" class="legendnopad">Preferred</td>
                    <td valign="bottom" nowrap class="legendnopad">
                      Phone Number: <span style="font-size:11pt;">*</span>
                      <span style="color:#000000; font-weight:normal; font-size:8pt">(One is required)</span>
                    </td>
                  </tr>
                  <tr>
                    <td valign="bottom" align="center" class="legendnopad">Contact</td>
                    <td valign="bottom" nowrap class="legendnopad">
                      Day: <span style="color:#000000; font-weight:normal; font-size:7pt">(numbers only)</span>
                    </td>
                  </tr>
                  <tr>
                     <td align="center" nowrap>
                       <input name="cb_OwnerBestPhoneCD" type="checkbox" onFocus="javascript:gPrefFlag=this.checked" onClick="setOwnerCB(this)" value="D" tabIndex="20">
                     </td>
                    <td nowrap>
                      <input name="OwnerPhoneAC" id="txtOwnerPhoneAC" type="text" value="" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" size="3" maxlength="3" required="true" tabIndex="21">
                      <input name="OwnerPhoneEN" id="txtOwnerPhoneEN" type="text" value="" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" size="3" maxlength="3" required="true" tabIndex="22">
                      <input name="OwnerPhoneUN" id="txtOwnerPhoneUN" type="text" value="" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" size="4" maxlength="4" required="true" tabIndex="23">
                    </td>
                  </tr>
                  <tr>
                    <td nowrap class="legendnopad">
                    </td>
                    <td nowrap class="legendnopad">
                      Night:
                      <span style="color:#000000; font-weight:normal; font-size:7pt">(numbers only)</span>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" nowrap>
                      <input name="cb_OwnerBestPhoneCD" type="checkbox" onFocus="javascript:gPrefFlag=this.checked" onClick="setOwnerCB(this)" value="N" tabIndex="24">
                    </td>
                    <td nowrap>
                      <input name="OwnerNightPhoneAC" id="txtOwnerNightPhoneAC" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" size="3" maxlength="3" tabIndex="25">
                      <input name="OwnerNightPhoneEN" id="txtOwnerNightPhoneEN" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" size="3" maxlength="3" tabIndex="26">
                      <input name="OwnerNightPhoneUN" id="txtOwnerNightPhoneUN" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" size="4" maxlength="4" tabIndex="27">
                    </td>
                  </tr>
                  <tr>
                    <td nowrap class="legendnopad">
                    </td>
                    <td nowrap class="legendnopad">
                      Cell/Alternate:
                      <span style="color:#000000; font-weight:normal; font-size:7pt">(numbers only)</span>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" nowrap>
                      <input name="cb_OwnerBestPhoneCD" type="checkbox" onFocus="javascript:gPrefFlag=this.checked" onClick="setOwnerCB(this)" value="A" tabIndex="28">
                    </td>
                    <td nowrap>
                      <input name="OwnerAltPhoneAC" id="txtOwnerAltPhoneAC" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" size="3" maxlength="3" tabIndex="29">
                      <input name="OwnerAltPhoneEN" id="txtOwnerAltPhoneEN" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" size="3" maxlength="3" tabIndex="30">
                      <input name="OwnerAltPhoneUN" id="txtOwnerAltPhoneUN" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" size="4" maxlength="4" tabIndex="31">
                    </td>
                  </tr>
                </table>

              </td>
            </tr>
            <tr>
              <td colspan="2" nowrap>

                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
                  <colgroup>
                    <col width="90px"/>
                    <col width="150px"/>
                    <col width="140px"/>
                    <col width="200px"/>
                  </colgroup>
                  <tr>
                    <td nowrap class="legendnopad">Year: <span style="color:#000000; font-weight:normal; font-size:7pt">(YYYY)</span></td>
                    <td nowrap class="legendnopad">Make:</td>
                    <td nowrap class="legendnopad">Model:</td>
                    <td nowrap>&nbsp;</td>
                  </tr>
                  <tr>
                    <td nowrap>
                      <input name="VehicleYear" id="txtVehicleYear" type="text" onKeyPress="return numbersOnly(this,event,0)" value="" size="4" maxlength="4" required="true" tabIndex="32">
                    </td>
                    <td nowrap>
                      <input name="Make" id="txtMake" type="text" value="" onBlur="ldTrim(this)" size="20" maxlength="15" required="true" tabIndex="33">
                    </td>
                    <td nowrap>
                      <input name="Model" id="txtModel" type="text" value="" onBlur="ldTrim(this)" size="20" maxlength="15" required="true" tabIndex="34">
                    </td>
                    <td nowrap>&nbsp;</td>
                  </tr>
                  <tr>
                    <td nowrap class="legendnopad">Odometer:</td>
                    <td nowrap class="legendnopad">Color:</td>
                    <td nowrap class="legendnopad">License Plate:</td>
                    <td nowrap class="legendnopad">VIN: <span style="color:#000000; font-weight:normal; font-size:8pt">(At least the last 6 digits of the VIN)</span></td>
                  </tr>
                  <tr>
                    <td nowrap>
                      <input name="Mileage" id="txtMileage" type="text" value="" size="8" maxlength="7" tabIndex="35">
                    </td>
                    <td nowrap>
                      <input name="Color" id="txtColor" type="text" value="" onBlur="ldTrim(this)" size="15" maxlength="10" required="true" tabIndex="36">
                    </td>
                    <td nowrap>
                      <input name="LicensePlateNumber" id="txtLicensePlateNumber" type="text" value="" onBlur="ldTrim(this)" size="10" maxlength="10" required="true" tabIndex="37">
                    </td>
                    <td nowrap>
                      <input name="VIN" id="txtVIN" type="text" value="" onBlur="ldTrim(this)" size="20" maxlength="17" tabIndex="38">
                    </td>
                  </tr>
                </table>

              </td>
            </tr>
          </table>
          
        </DIV>

      </td>
    </tr>
    <tr>
      <td nowrap valign="top">
        
        <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
          <DIV style="position:relative; top:-10px; width:60px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
            &nbsp;&nbsp;Vehicle Location&nbsp;&nbsp;
          </DIV>

          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
            <colgroup>
              <col width="160px"/>
              <col width="170px"/>
              <col width="180px"/>
            </colgroup>
            <tr>
              <td nowrap class="legendnopad" colspan="2">Location Name:</td>
              <td nowrap class="legendnopad">Phone Number:</td>
            </tr>
            <tr>
              <td nowrap colspan="2">
                <input name="LocationName" id="txtLocationName" type="text" value="" onBlur="ldTrim(this)" size="40" maxlength="25" required="true" tabIndex="38">
              </td>
              <td nowrap>
                <input name="LocationAreaCode" id="txtLocationAreaCode" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" size="3" maxlength="3" tabIndex="39">
                <input name="LocationExchangeNumber" id="txtLocationExchangeNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" size="3" maxlength="3" tabIndex="40">
                <input name="LocationUnitNumber" id="txtLocationUnitNumber" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" size="4" maxlength="4" tabIndex="41">
              </td>
            </tr>
            <tr>
              <td nowrap class="legendnopad" colspan="3">Address:</td>
            </tr>
            <tr>
              <td nowrap colspan="3">
                <input name="LocationAddress1" id="txtLocationAddress1" type="text" value="" onBlur="ldTrim(this)" size="40" maxlength="25" required="true" tabIndex="42">
              </td>
            </tr>
            <tr>
              <td nowrap class="legendnopad">City:</td>
              <td nowrap class="legendnopad">State:</td>
              <td nowrap class="legendnopad">ZIP:</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="LocationAddressCity" id="txtLocationAddressCity" type="text" value="" onBlur="ldTrim(this)" size="20" maxlength="15" required="true" tabIndex="43">
              </td>
              <td nowrap>

                <%
                   lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
                
                   Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaStateList.xsl", "ObjName=LocationAddressState" & lcDelimiter & "tabIndexNo=44", true)
                %>

              </td>
              <td nowrap>
                <input name="LocationAddressZip" id="txtLocationAddressZip" type="text" value="" onKeyPress="return numbersOnly(this,event,0)" size="10" maxlength="8" required="true" tabIndex="45">
              </td>
            </tr>
          </table>

        </DIV>

      </td>
    </tr>
  </table>

  <p class="bodyBlue"> All fields marked with an asterisk (*) are required.</p>

  <table border="0" cellpadding="0" cellspacing="0" style="align:right;">
    <colgroup>
      <col width="400px"/>
      <col width="100px"/>
      <col width="90px"/>
    </colgroup>
    <tr>
      <td>&nbsp;</td>
      <td id="cancelImgSrc">
        <a href='javascript:goCancel()'
           title="<% If lsPrevPage = "ClaimSummary" Then %> Cancel and Return to Summary Page <% Else %> Return to My Claims Desktop <% End If %>"
           onMouseOver="window.status='<% If lsPrevPage = "ClaimSummary" Then %> Cancel and Return to Summary Page <% Else %> Return to My Claims Desktop <% End If %>'; return true"
           onMouseOut="window.status=''; return true">
           <% If lsPrevPage = "ClaimSummary" Then %>
            <img src='images/btn_return.gif' border='0' WIDTH='83' HEIGHT='31'>
           <% Else %>
            <img src='images/btn_cancel.gif' border='0' WIDTH='83' HEIGHT='31'>
           <% End If %>
        </a>
      </td>
      <td id="nextImgSrc">
        <a href="javascript:chkClaimNumValidity()"
        <% If lsPrevPage = "ClaimSummary" Then %>
           title="Save and Return to Summary Page"
           onMouseOver="window.status='Save and Return to Summary Page'; return true"
        <% Else %>
           title="To Vehicle Info Page"
           onMouseOver="window.status='To Vehicle Info Page'; return true"
        <% End If %>
           onMouseOut="window.status=''; return true">

        <% If lsPrevPage = "ClaimSummary" Then %>
          <img src="images/btn_ok.gif" border="0" WIDTH="52" HEIGHT="31">
        <% Else %>
          <img src="images/btn_next.gif" border="0" WIDTH="83" HEIGHT="31">
        <% End If %>
        </a>
      </td>
    </tr>
  </table>

  </form>

</div>

<iframe ID="CalFrame" NAME="CalFrame" SRC="blank.asp" STYLE="border:2px solid #404040; left:-500px; top:0px; display:none; position:absolute; width:170; height:186; z-index:100; filter: progid:DXImageTransform.Microsoft.Shadow(color='#666666', Direction=135, Strength=3);" MARGINHEIGHT=0 MARGINWIDTH=0 NORESIZE FRAMEBORDER=0 SCROLLING=NO></iframe>

<iframe name="frmCheckClaimNumber" id="frmCheckClaimNumber" style="height:0px;width:0px;visibility:hidden" src="blank.asp"></iframe>

</body>
</html>
