<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.2 -->

<%
  Dim liLynxID, liAspectID, lsAssignmentType, lsVehiclesList, liVehNumber
  
  liLynxID = Request("LynxID")
  liVehNumber = Request("VehNumber")
  liAspectID = Request("AspectID")
  lsAssignmentType = Request("AssignmentType")
  lsVehiclesList = Request.Cookies("CPSession")(liLynxID & "VehiclesList")

  lsPageName = "frmClaimInfo.asp"
  lsPageTitle = "Claim Details"
  lsPageSubTitle = "Information"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcClaim
  lsStyleSheet = "ClaimInfo.xsl"
  lsHelpPage = "CP_Claim_Information.htm"
  bGridSort = True
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStartClm.asp"-->

<%
  dim lbOverwriteExisting
  lbOverwriteExisting = false
%>

<script language="JavaScript" type="text/JavaScript">

  function showCalIframe(obj)
  {
    document.getElementById("frmCarrierUpdate").submit();
    document.getElementById("IFrmCarrierUpdate").style.top = findPosY(obj) - 130;
    document.getElementById("IFrmCarrierUpdate").style.left = findPosX(obj) - 120;
    document.getElementById("IFrmCarrierUpdate").style.display = "inline";
  }

  function CloseCarrierContact()
  {
    document.getElementById("IFrmCarrierUpdate").src = "blank.asp";
    document.getElementById("IFrmCarrierUpdate").style.display = "none";
  }

  function hideIFrmCarrierUpdate()
  {
    if (window.document.activeElement.id != "calCall")
    {
      if (document.getElementById("IFrmCarrierUpdate").style.display == "inline")
      {
        document.getElementById("IFrmCarrierUpdate").style.display = "none";
        document.getElementById("IFrmCarrierUpdate").src = "blank.asp";
      }
    }
  }

</script>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspClaimSearchResults.asp">

<!--#include file="includes/incHeaderTableWithIcons.asp"-->
<!--#include file="includes/incClaimCommonTop.asp"-->

<%
  'First time retrieving Claim Detail information - set pbOverwrite parameter to false to get info from APD
  'Hereafter, all lsClaimDetailCall are set to true to retrieve info from data stored in eTemplate databse
  Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "NavBar.xsl", "currentArea=" & lsPageSubTitle & lcDelimiter & "LynxID=" & liLynxID & lcDelimiter & "AssignmentType=" & lsAssignmentType & lcDelimiter & "AspectID=" & liAspectID & lcDelimiter & "VehNumber=" & liVehNumber, false)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
  Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "ClaimHeader.xsl", "pageTitle=" & lsPageSubTitle & lcDelimiter & "AspectID=" & liAspectID, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->
<!--#include file="includes/incClaimDetailShort.asp"-->

</form>

<%
  Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "ClaimInfo.xsl", "LynxID=" & liLynxID & lcDelimiter & "AssignmentType=" & lsAssignmentType & lcDelimiter & "VehiclesList=" & lsVehiclesList & lcDelimiter & "AspectID=" & liAspectID & lcDelimiter & "VehNumber=" & liVehNumber, true)
%>

<!--#include file="includes/incClaimCommonBottom.asp"-->

<iframe ID="IFrmCarrierUpdate" NAME="IFrmCarrierUpdate" SRC="blank.asp" STYLE="border:2px solid #404040; left:-500px; top:0px; display:none; position:absolute; width:420; height:100; z-index:100; filter: progid:DXImageTransform.Microsoft.Shadow(color='#666666', Direction=135, Strength=3);" MARGINHEIGHT=0 MARGINWIDTH=0 NORESIZE FRAMEBORDER=0 SCROLLING=NO></iframe>


<!--#include file="includes/incCommonHTMLEndClm.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
