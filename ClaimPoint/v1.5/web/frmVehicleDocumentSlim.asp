<!--#include file="includes/incCommonTop.asp"-->
<%
  Dim lsXMLCall, lsDocType, liClaimID, liVehicleID
  
  lsPageSubTitle = "Estimates and Documents"
  lsStyleSheet = "DocumentDetailAltSlim.xsl"
  lsDocType = "A"
  
  lsPageTitle = "Estimates and Documents"
  liClaimID = Request("LynxID")
  liVehicleID = Request("ClaimAspectID")
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incGoDocument.asp"-->

<HTML>
<HEAD>
<TITLE><%=lsPageTitle%></TITLE>

<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">
  <link href="includes/incGridSort.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="includes/incUtils.js"></script>

</HEAD>
<BODY>

<%
  lsXMLCall = "<DocumentList InsuranceCompanyID=""" & liInsuranceCompanyID & """ LynxID=""" & liClaimID & """ ClaimAspectID=""" & liVehicleID & """ DocumentClassCD=""" & lsDocType & """/>"
  
  Response.Write GetData(lsXMLCall, lsStyleSheet, "")
%>

</BODY>
</HTML>

<!--#include file="includes/incCommonBottom.asp"-->
