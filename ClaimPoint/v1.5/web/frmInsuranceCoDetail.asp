<!--#include file="includes/incCommonTop.asp"-->
<%
lsPageName = "frmInsuranceCoDetail.asp"
lsPageTitle = "Add Insurance Company"
liPageType = lcBeforeInsuranceCompanySelect
bModal= true

dim InsuranceCompanyListXml
InsuranceCompanyListXml = loAPD.GetAPDData("<InsuranceCompanyList/>", "InsuranceCompanies.xsl")

%>
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->
<script language="JavaScript">
	function setInsuranceCompanyID()
	{
		if (document.frmMainForm.cbInsuranceCo.value== "")
		{
			alert("You must select the insurance company you want to add.");
			return true;
		}
		  
		document.frmMainForm.hidInsuranceCompanyID.value = document.frmMainForm.cbInsuranceCo.value;
		document.frmMainForm.submit();
	}
	
	function cancel_onclick()
	{
		document.frmMainForm.action = "frmInsuranceCoList.asp";
		document.frmMainForm.submit();
	}
	
</script>

<form name="frmMainForm" id="frmMainForm" method="POST" action="p_add_InsuranceCo.asp">
<input type="hidden" name="hidInsuranceCompanyID" id="hidInsuranceCompanyID">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse" bordercolor="#00099">
					<tr>
						<td width="8" valign="top" background="images/table_CL_blue.gif" height="8">
						<img border="0" src="images/table_TL_blue.gif" width="8" height="8"></td>
						<td background="images/table_TC_blue.gif" height="8"><img border="0" src="images/spacer.gif" width="25" height="8"></td>
						<td background="images/table_TC_blue.gif" height="8"><img border="0" src="images/spacer.gif" width="25" height="8"></td>
						<td width="8" background="images/table_CR_blue.gif" valign="top" height="8">
						<img border="0" src="images/table_TR_blue.gif" width="8" height="8"></td>
					</tr>
					<tr>
						<td width="8" background="images/table_CL_blue.gif">&nbsp;</td>
						<td background="images/lt_blue.gif" nowrap>
						<p class="bodyBlue">Ins. Co. Name:</td>
						<td background="images/lt_blue.gif" align="center">
							<select name="cbInsuranceCo">
								<option value = ''></option>
								<%=InsuranceCompanyListXml%>
							</select>
						</td>
						<td width="8" background="images/table_CR_blue.gif">&nbsp;</td>
					</tr>
					<tr>
						<td width="8" background="images/table_CL_blue.gif" valign="bottom" height="8">
						<img border="0" src="images/table_BL_blue.gif" width="8" height="8"></td>
						<td class="cell" height="8" background="images/lt_blue.gif" ><img border="0" src="images/spacer.gif" width="25" height="8"></td>
						<td class="cell" height="8" background="images/lt_blue.gif" ><img border="0" src="images/spacer.gif" width="25" height="8"></td>
						<td width="8" background="images/table_CR_blue.gif" valign="bottom" height="8">
						<img border="0" src="images/table_BR_blue.gif" width="8" height="8"></td>
					</tr>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
					<tr>
						<td style="CURSOR:pointer;CURSOR:Hand" align="right" valign="top">
							<img border="0" src="images/btn_add.gif" onclick="setInsuranceCompanyID();" WIDTH="83" HEIGHT="31"><img border="0" src="images/btn_cancel.gif" onclick="cancel_onclick();" WIDTH="83" HEIGHT="31"></td>
						<td width="25" align="right">&nbsp;</td>
					</tr>
				</table>	
			</td>
		</tr>		
	</table>	
</form>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
