<!--#include file="includes/incCommonTop.asp"-->
<%
dim lsMode
dim lsXMLCall
dim lsDocType

lsMode = request("M")

lsPageSubTitle = "Documents"
lsStyleSheet = "DocumentListAlt.xsl"
lsDocType = "A"

if lsMode = "E" then 
	lsStyleSheet = "EstimateListAlt.xsl"
	lsPageSubTitle = "Estimates"
	lsDocType = "E"
end if

lsPageName = "frmPropertyDocument.asp"
lsPageTitle = "Claim Details"
liPageType = lcAfterClaimSearch
liPageIcon = lcProperty
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incVerifyCurrentProperty.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->
<%
dim lbOverwriteExisting
lbOverwriteExisting = false

lsXMLCall = "<DocumentList InsuranceCompanyID=""" & liInsuranceCompanyID & """ LynxID=""" & liClaimID & """ PertainsTo=""prp" & liPropertyID & """ DocumentClassCD=""" & lsDocType & """/>"
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspPropertyInfo.asp">
<!--#include file="includes/incGoDocument.asp"-->
<!--#include file="includes/incHeaderTableWithIcons.asp"-->

<!--#include file="includes/incClaimCommonTop.asp"-->

<%
Response.Write GetPersistedData(lsPropertyDetailCall, lsPropertyDetailGUID, "PropertyNavBar.xsl", "currentArea=" & lsPageSubTitle, true)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
Response.Write GetPersistedData(lsClaimInfoCall, lsClaimInfoGUID, "PropertyHeader.xsl", "claimNumber=" & liClaimID & lcDelimiter & "pageTitle=" & lsPageSubTitle & lcDelimiter & "currentProperty=" & liPropertyID, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

<%
Response.Write GetData(lsXMLCall, lsStyleSheet, "")
%>
<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
