<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.2 -->

<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
  lsPageName = "frmCarrierContactUp.asp"
  liPageType = lcAfterInsuranceCompanySelect

  Dim lsCarrierOfficesListCall, liCurrCarrierOfficeID, liCurrCarrierUserID, lsCurrCarrierUserEmail, lsCurrCarrierUserName
  Dim lsCarrierLastUpdatedDate, lsClaimNumber, liLynxID, lsAssignmentType, lsVehiclesList, liAspectID, liVehNumber

  
  liCurrCarrierOfficeID = Request("CurrCarrierOfficeID")
  liCurrCarrierUserID = Request("CurrCarrierUserID")
  lsCurrCarrierUserEmail = Request("CurrCarrierUserEmail")
  lsCurrCarrierUserName = Request("CurrCarrierUserName")
  lsCarrierLastUpdatedDate = Request("CarrierLastUpdatedDate")
  lsClaimNumber = Request("ClaimNumber")
  liLynxID = Request("LynxID")
  lsAssignmentType = Request("AssignmentType")
  lsVehiclesList = Request("VehiclesList")
  liAspectID = Request("AspectID")
  liVehNumber = Request("VehNumber")
%>

<HTML>
<HEAD>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript">

  var giCurrUserID = <%=liCurrCarrierUserID%>;
  var gsCarrierLastUpdatedDate = "<%=lsCarrierLastUpdatedDate%>";

  function carrierOfficesSelect()
  {
    var loSel = document.getElementById('txtOfficeID');
    var liOffice = loSel.options[loSel.selectedIndex].value;
    document.getElementById('txtNewCarrierUserID').value = 0;

    document.getElementById('ifrmCarrierUsersList').src = "frmCarrierUsersUpd.asp?OfficeID=" + liOffice + "&UserID=0";
  }

  function showGoBtn(state)
  {
    if (state == "show")
    {
      gifState = "none";
      btnState = "inline"  
    }
    else
    {
      gifState = "inline";
      btnState = "none"  
    }
    
    if (document.getElementById('animateGif'))
      document.getElementById('animateGif').style.display = gifState;

    if (document.getElementById('ifrmCarrierUsersList'))
      document.getElementById('ifrmCarrierUsersList').style.display = btnState;

    if (document.getElementById('btnSave'))
      document.getElementById('btnSave').style.display = btnState;
  }

  function updUserIDValue()
  {
    var loCarrierUserSel = window.frames['ifrmCarrierUsersList'].document.getElementById('txtCarrierUser');
    var laCarrierInfo = new Array();
    var lsCarrierInfo = loCarrierUserSel.options[loCarrierUserSel.selectedIndex].value;
    laCarrierInfo = lsCarrierInfo.split("|");
    document.getElementById('txtNewCarrierUserID').value = laCarrierInfo[0];
    document.getElementById('txtNewCarrierUserEmail').value = laCarrierInfo[1];
    document.getElementById('txtNewCarrierUserName').value = laCarrierInfo[2];
  }

  function SaveCarrierContact()
  {
    var lsCarrierRepUserID = document.getElementById('txtNewCarrierUserID').value;
    if (lsCarrierRepUserID == 0)
    {
      alert("Please select a contact from the User drop-down before saving.");
      return;
    }
    else if (lsCarrierRepUserID == giCurrUserID)
    {
      alert("The contact selected is the same as the one currently assigned. \nPlease select a different contact from the User drop-down before saving.");
      return;
    }

    document.getElementById('btnSave').style.display = "none";
    document.getElementById('SavingAnimateGif').style.display = "inline";
    var objForm = document.getElementById("frmCarrierContactUpd");
    objForm.submit();
  }
  
</script>

</HEAD>
<BODY LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>

  <form id="frmCarrierContactUpd" name="frmCarrierContactUpd" method="post" target="IFrmCarrierContactUpd" action="rspCarrierContactUpd.asp">
    <input type="hidden" name="LynxID" id="txtLynxID" value="<%=liLynxID%>">
    <input type="hidden" name="AssignmentType" id="txtAssignmentType" value="<%=lsAssignmentType%>">
    <input type="hidden" name="VehiclesList" id="txtVehiclesList" value="<%=lsVehiclesList%>">
    <input type="hidden" name="ClaimNumber" id="txtClaimNumber" value="<%=lsClaimNumber%>">
    <input type="hidden" name="UserID" id="txtUserID" value="<%=liUserID%>">
    <input type="hidden" name="CurrCarrierUserID" id="txtCurrCarrierUserID" value="<%=liCurrCarrierUserID%>">
    <input type="hidden" name="CurrCarrierUserEmail" id="txtCurrCarrierUserEmail" value="<%=lsCurrCarrierUserEmail%>">
    <input type="hidden" name="CurrCarrierUserName" id="txtCurrCarrierUserName" value="<%=lsCurrCarrierUserName%>">
    <input type="hidden" name="NewCarrierUserID" id="txtNewCarrierUserID">
    <input type="hidden" name="NewCarrierUserEmail" id="txtNewCarrierUserEmail">
    <input type="hidden" name="NewCarrierUserName" id="txtNewCarrierUserName">
    <input type="hidden" name="SysLastUpdatedDate" id="txtSysLastUpdatedDate" value="<%=lsCarrierLastUpdatedDate%>">
    <input type="hidden" name="AspectID" id="txtAspectID" value="<%=liAspectID%>">
    <input type="hidden" name="VehNumber" id="txtVehNumber" value="<%=liVehNumber%>">
  </form>

  <table width="420" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td colspan="4" nowrap style="border-bottom: 1px solid #000099; background: #E6E5F5;"><p class="bodyBlue">Select a New Carrier Contact</p></td>
      <td nowrap style="border-bottom: 1px solid #000099; background: #E6E5F5;">
        <a href="JavaScript:parent.CloseCarrierContact()"
           title="Close"
           onMouseOver="window.status='Close'; return true"
           onMouseOut="window.status=''; return true">
        <img src="images/close.gif" alt="Close" width="18" height="18" border="0">
      </a>
      </td>
    </tr>
    <tr style="padding-top:16px;">
      <td nowrap><p class="bodyBlue">Office:</td>
      <td height="34" nowrap>
    <%
      lsCarrierOfficesListCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
      
      Response.Write GetPersistedData(lsCarrierOfficesListCall, lsInsCoConfigGUID, "CarrierOfficesList.xsl", "OfficeID=" & liCurrCarrierOfficeID, true)
    %>
        <img border="0" src="images/spacer.gif" width="10" height="1">
      </td>

      <td nowrap><p class="bodyBlue">User:</td>
      <td width="100%" nowrap>
        
        <SPAN id="animateGif" style="font-weight:bold; color:#008B8B;">
          Retrieving List...&nbsp;&nbsp;&nbsp;
          <img src="images/loading.gif" alt="" width="78" height="7" border="0">
        </SPAN>
        
        <IFRAME SRC="frmCarrierUsersUpd.asp?OfficeID=<%=liCurrCarrierOfficeID%>&UserID=<%=liCurrCarrierUserID%>" NAME="ifrmCarrierUsersList" ID="ifrmCarrierUsersList" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="width:100%; height:24px; border:0px; display:none">
        </IFRAME>

      </td>
      <td nowrap></td>
    </tr>        
    <tr>
      <td colspan="5" nowrap align="right">
        <a href="JavaScript:SaveCarrierContact()"
           title="Save Carrier Contact"
           onMouseOver="window.status='Save Carrier Contact'; return true"
           onMouseOut="window.status=''; return true">
          <img id="btnSave" border="0" src="images/btn_save.gif" width="80" height="28" hspace="6" vspace="4" style="display:none;"></a>
        <SPAN id="SavingAnimateGif" style="vertical-align:bottom; font-weight:bold; color:#008B8B; display:none;">
          Saving...&nbsp;&nbsp;&nbsp;
          <img src="images/loading.gif" alt="" width="78" height="7" border="0">&nbsp;&nbsp;&nbsp;
        </SPAN>

      </td>
    </tr>
  </table>

  <iframe src="blank.asp" id="IFrmCarrierContactUpd" name="IFrmCarrierContactUpd" style="height:0px; width:0px; display:none;" frameBorder="0" />

</body>
</html>
