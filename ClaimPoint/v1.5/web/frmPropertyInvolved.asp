<!--#include file="includes/incCommonTop.asp"-->
<%
  lsPageName = "frmPropertyInvolved.asp"
  lsPageTitle = "Claim Details"
  lsPageSubTitle = "Involved"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcProperty
  lsStyleSheet = "InvolvedInfoProperty.xsl"
  lsHelpPage = "CP_Property.htm"
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incVerifyCurrentProperty.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->
<%
dim lsInvolved 
dim laInvolved
lsInvolved = Trim(Request("I"))

if lsInvolved = "" or lsInvolved="0" then Response.Redirect "frmPropertyInfo.asp"

if right(lsInvolved, 1) = "," then lsInvolved = left(lsInvolved, len(lsInvolved) - 1)

laInvolved = split(lsInvolved, ",")

dim lbOverwriteExisting
lbOverwriteExisting = false
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspPropertyInfo.asp">

<!--#include file="includes/incHeaderTableWithIcons.asp"-->

<!--#include file="includes/incClaimCommonTop.asp"-->

<%
Response.Write GetPersistedData(lsPropertyDetailCall, lsPropertyDetailGUID, "PropertyNavBar.xsl", "currentArea=" & lsPageSubTitle, true)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
Response.Write GetPersistedData(lsClaimInfoCall, lsClaimInfoGUID, "PropertyHeader.xsl", "claimNumber=" & liClaimID & lcDelimiter & "pageTitle=" & lsPageSubTitle & lcDelimiter & "currentProperty=" & liPropertyID, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

<%

liStop = ubound(laInvolved)

for liCounter = 0 to liStop
	lsInvolved = laInvolved(liCounter)
	if len(lsInvolved) > 0 and lsInvolved <> "0" then
		Response.Write GetData("<PropertyInvolved InsuranceCompanyID=""" & liInsuranceCompanyID & """ InvolvedID=""" & lsInvolved & """/>", lsStyleSheet, "callNumber=" & liCounter)
		if liCounter < liStop then
		%> 
			<!--#include file="includes/incMultipleCallSeparator.asp"-->
		<%
		end if
	end if
next


%>
<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
