<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.4.2.0 -->

<%
  lsPageName = "frmWaShopComments.asp"
  lsPageTitle = "Shop Comments"
  liPageType = lcAssignmentPage
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<html>
<head>
<title>Shop Comments</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "<%=Request("fromPage")%>";
  gsVehNum = "<%=Request("vehNum")%>";
  gsVehCount = "";
  
  function pageInit()
  {
    if(parent)
      parent.resetTimeOut();

    var liVehNum = gsVehNum;
    if (parent && document.getElementById("txtShopRemarks"))
      parent.getShopComments(liVehNum);

    document.getElementById('txtShopRemarks').focus();
  }


  function updShopComments() 
  {
    var liTxtAreaLength = document.getElementById("txtShopRemarks").value.length;
    if (liTxtAreaLength > 250)
    {
      alert("* Only 250 characters are allowed for Comments to Repair Shop.\n" +
            "    You have typed "+ liTxtAreaLength +" characters.");
      return;
    }
    else
    {
      if (parent)
      {
        parent.updShopComments(gsVehNum);
        gsVehCount = parent.getVehCount();
        if (gsPrevPage == "ClaimSummary_addNew")
          parent.document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=ShopComments&vehCount="+gsVehCount;
        else
          parent.document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ShopComments&vehCount="+gsVehCount;
      }
    }
  }
  
  
  function goCancel()
  {
    if (parent)
    {
      gsVehCount = parent.getVehCount();
        if (gsPrevPage == "ClaimSummary_addNew")
          parent.document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=ShopComments&vehCount="+gsVehCount;
        else
          parent.document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ShopComments&vehCount="+gsVehCount;
    }
  }
  
  
  function reset_onclick() 
  {
    document.getElementById("txtShopRemarks").value = "";
  }


</script>

</head>
<body onLoad="resizeWaIframe(0); pageInit();">
<div id="divContainer">
  <br>

    <table width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse;">
      <tr>
        <td>

          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-bottom: 1px solid #000099;">
            <tr style="height: 28px;" bgcolor="#E5E5F5">
              <td style="color: #000099; font-size: 10pt; font-weight: bold;">
                &nbsp;Comments to Repair Shop:
                <span id="commFldMsg" style="color:#000000; font-weight:normal; font-size:7pt">(max.:250 characters)</span>
              </td>
            </tr>
          </table>

        </td>
      </tr>
      <tr>
        <td valign="top" style="padding: 20px 10px 10px 10px;">
          <textarea name="ShopRemarks" id="txtShopRemarks" cols="" rows="" wrap="physical" style="width:100%; height:80px;" onKeyDown="CheckInputLength(this, 250)"> </textarea>
        </td>
      </tr>
    </table>

    <br>

    <table width="600" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="100%">&nbsp;</td>
        <td align="right" valign="top" nowrap>
          <a href="javascript:goCancel()"
             title="Back to Summary screen"
             onMouseOver="window.status='Back to Summary screen'; return true"
             onMouseOut="window.status=''; return true">
          <img src="images/btn_cancel.gif" border="0" WIDTH="83" HEIGHT="31">
          </a>
        </td>
        <td align="right" valign="top" nowrap><img border="0" src="images/spacer.gif" WIDTH="10" HEIGHT="1"></td>
        <td align="right" valign="top" nowrap>
          <a href="javascript:updShopComments()"
             title="Save and Return to Summary screen"
             onMouseOver="window.status='Save and Return to Summary screen'; return true"
             onMouseOut="window.status=''; return true">
          <img src="images/btn_ok.gif" border="0" WIDTH="52" HEIGHT="31">
          </a>
        </td>
      </tr>
    </table>

</div>

</body>
</html>
