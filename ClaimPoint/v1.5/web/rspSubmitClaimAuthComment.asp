<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!-- v1.4.3.0 -->

<%
  lsPageName = "rspSubmitClaimAuthComment.asp"

  Dim loComment, lsComment, liRetVal, loXML, loRoot, loAttribute, liLynxID, lsDispMessage, lsSendToClaimOwner
  
  liLynxID = Request.Form("LynxID")
  lsComment = Request.Form("txtComment")
  lsSendToClaimOwner = Request.Form("SendToClaimOwner")
  
  Set loXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  
  loXML.loadXML("<SubmitComment/>")
  
  Set loRoot = loXML.documentElement
      
  set loAttribute = loXML.createAttribute("InsuranceCompanyID")
  loAttribute.Value = liInsuranceCompanyID
  loRoot.Attributes.setNamedItem loAttribute
  set loAttribute = nothing
          
  set loAttribute = loXML.createAttribute("LynxID")
  loAttribute.Value = liLynxID
  loRoot.Attributes.setNamedItem loAttribute
  set loAttribute = nothing
          
  set loAttribute = loXML.createAttribute("Comment")
  loAttribute.Value = lsComment
  loRoot.Attributes.setNamedItem loAttribute
  set loAttribute = nothing
          
  set loAttribute = loXML.createAttribute("UserName")
  loAttribute.Value = lsUserNameFirst & " " & lsUserNameLast
  loRoot.Attributes.setNamedItem loAttribute
  set loAttribute = nothing
          
  set loAttribute = loXML.createAttribute("UserEmail")
  loAttribute.Value = lsUserEmail
  loRoot.Attributes.setNamedItem loAttribute
  set loAttribute = nothing

  set loAttribute = loXML.createAttribute("SendToClaimOwner")
  loAttribute.Value = lsSendToClaimOwner
  loRoot.Attributes.setNamedItem loAttribute
  set loAttribute = nothing
          
Dim mexMonikerString 
         mexMonikerString = GetWebServiceURL()

  'Set loComment = Server.CreateObject("PPGNAAPD.clsAPDData")
  Set loComment = GetObject(mexMonikerString)
  liRetVal = loComment.PutXML(loXML.xml)
  
  Set loXML = Nothing
  Set loRoot = Nothing
  Set loComment = Nothing

function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function

%>

<!--#include file="includes/incCommonBottom.asp"-->

<script language="JavaScript" type="text/JavaScript">

    var liRetValue = "<%= liRetVal %>";

    if (liRetValue == 1) {
        parent.document.getElementById("DisplayMessage").innerHTML = "Your comment was submitted successfully.";
        parent.document.getElementById("txtComment").value = "";
    }
    else
        parent.document.getElementById("DisplayMessage").innerHTML = "An error occurred submitting your comments. If problem persists, please contact a system administrator.";
      
</script>
