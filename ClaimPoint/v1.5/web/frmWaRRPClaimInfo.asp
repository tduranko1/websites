<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetCurrAssignment.asp"-->
<!-- v1.4.5.0 -->

<%
  Dim lsInsCoDetailsCall, lsCarrierUsersListCall, lsPrevPage
  Dim lsInsConfigXML, loXML, ElemList, oNode, lsAssignmentTypeIDList, laAssignmentTypeID
  Dim lsFNOLFeature

  lsPrevPage = Request("fromPage")
  
  lsPageName = "frmWaRRPClaimInfo.asp"
  lsPageTitle =  lsCurrentAssignmentDesc & " Assignment - Claim Info"
  liPageType = lcAssignmentPage

  'Create Elemlist if either of these variables = 1
  'We need Elemlist later in the page
  If liCurrAssignmentEmergencyLiability = 1 or liCurrAssignmentSB511 = 1 Then

    Set loXML = Server.CreateObject("MSXML2.DOMDocument")
    loXML.Async = False
  
    lsInsConfigXML = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
    loXML.LoadXML GetPersistedData(lsInsConfigXML, lsInsCoConfigGUID, "", "", false)
  
    If Not loXML.ParseError = 0 Then
      Response.Write "<b>Error Code:</b> " & loXML.ParseError & "<br>"
  
    Else
      Set ElemList = loXML.SelectNodes("/Root/CustomScripting")
    
    End If

  End If
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<html>
<head>
<title><%=lsCurrentAssignmentDesc%> - Claim Info</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/incCalendar.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "<%= lsPrevPage %>";
  gsVehCount = "";
  gbSB511Script = <%= liCurrAssignmentSB511 %>;
  var lbEmergencyLiabilityScript = <%= liCurrAssignmentEmergencyLiability %>;

<%
  lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"

  Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaClaimNumberValidation.xsl", "OfficeID=" & liOfficeID, true)
%>
  
  function pageInit()
  {
    if(parent)
      parent.resetTimeOut();
  
    var lsCurrAssignment = "<%=lsCurrentAssignmentDesc%>";
    var lsCurrHelpURL;

    if (lsCurrAssignment == "Adverse Subro Desk Review")
      lsCurrHelpURL = "CP_Adverse_Subro_Desk_Review.htm";
    else if (lsCurrAssignment == "Demand Estimate Review")
      lsCurrHelpURL = "CP_Demand_Estimate_Review.htm";
    else if (lsCurrAssignment == "IA Estimate Review")
      lsCurrHelpURL = "CP_IA_Estimate_Review.htm";
    else if (lsCurrAssignment == "Program Shop")
      lsCurrHelpURL = "CP_Program_Shop_Assignment.htm";

    if (parent)
      parent.gsHelpSubTopicPage = lsCurrHelpURL;

    var loSel = document.getElementById('txtCallerRelationToInsuredID');
    var loSelLength = loSel.options.length;
    for (var i=0; i<loSelLength; i++)
    {
      if (loSel.options[i].value == '14')
        loSel.options[i].selected = true;
    }
    loSel.disabled = true;

    var liUserType = <%=liCarrierRep%>;
    var liAllowCarrierRepSelectionFlag = <%=liAllowCarrierRepSelectionFlag%>;

    if (liUserType == 1 && liAllowCarrierRepSelectionFlag == 0) //carrier user and AllowCarrierRepSelectionFlag is false
    {
      document.getElementById("txtCallerNameFirst").disabled = true;
      document.getElementById("txtCallerNameLast").disabled = true;
      document.getElementById('txtCoverageClaimNumber').focus();
    }
    else
      document.getElementById('txtCarrierUser').focus();

    if (gsPrevPage == "VehicleInfo" || gsPrevPage == "ClaimSummary")
    {
      if (parent)
        parent.getClaimInfoData();

      if (liUserType == 0 || liAllowCarrierRepSelectionFlag == 1) //non-carrier user or AllowCarrierRepSelectionFlag is true
        updCallerName();

      // update radio
      if (document.getElementById("txtSourceApplicationPassThruData2"))
      {
        var dcRadioValues = document.getElementById("txtSourceApplicationPassThruData").value;
        var dcRadioValuesArr = dcRadioValues.split("|");
        var dcRadioValuesArrLength = dcRadioValuesArr.length;
        for (var y=0; y<dcRadioValuesArrLength; y++)
        {
          var lsRadName = dcRadioValuesArr[y].substring(0, dcRadioValuesArr[y].indexOf("="));
          var RadValue = dcRadioValuesArr[y].substring(dcRadioValuesArr[y].indexOf("=")+1, dcRadioValuesArr[y].length);
          setRadioByValue(lsRadName, RadValue);
        }
      }
    }
    else
    {
      if (liAllowCarrierRepSelectionFlag == 1) //AllowCarrierRepSelectionFlag is true
        updCallerName();
      
      document.getElementById("txtTimeStarted").value = getCurrentDT();
      document.getElementById("txtIntakeStartSeconds").value = getCurrentTimeSec();
      if (typeof(parent.setFNOLData) == "function"){
         parent.setFNOLData(null);
      }
    }

    if (gbSB511Script == 1)
    {
      var selObj = document.getElementById('txtLossAddressState');
      selObj.onblur = new Function("", "ShwHideSB511(this)");
    }
    
  }
  
  function setRadioByValue(radioButton, val)
  {
    if (radioButton == "Liability")
      var radGroup = document.forms[0].Liability;
    if (radioButton == "Emergency")
      var radGroup = document.forms[0].Emergency;
    for (var x = 0; x < radGroup.length; x++)
    {
      if (radGroup[x].value == val)
        radGroup[x].checked = true;
    }
  }

  function ShwHideSB511(obj)
  {
    if (obj.value.toLowerCase() == "ca")
      document.getElementById("tblSB511").style.display = "inline";
    else
    {
      if (document.getElementById("tblSB511").style.display == "inline")
        document.getElementById("tblSB511").style.display = "none";
    }
  }

  function chkClaimNumValidity(gsPrevPage)
  {
  
    if ((document.getElementById("txtCallerNameFirst").value.length == 0 || document.getElementById("txtCallerNameLast").value.length == 0))
    {
      alert("Please select a Claim Assigner name from the drop-down list.");
      return;
    }

    var objInput = document.getElementById('txtCoverageClaimNumber');
    var lbReturn = ClaimNumberFormat(objInput);
    if (lbReturn == true)
    {
      parent.gbEmergencyLiabilityScript = lbEmergencyLiabilityScript;
      parent.tsClaimData(gsPrevPage);
    }
    else
      return;
  }

  function updCallerName()
  {
    var lsCarrierName = document.getElementById('txtCarrierUser').value;
    var laryUser = lsCarrierName.split("|");
    document.getElementById("txtCallerNameLast").value = laryUser[0];
    document.getElementById("txtCallerNameFirst").value = laryUser[1];
    document.getElementById("txtCarrierRepOfficeID").value = laryUser[2];
    document.getElementById("txtCarrierRepUserID").value = laryUser[3];
  }
  
  function formatLossDate() 
  {
    if (document.getElementById('txtLossDate').value.length != 0)
    {
      var lsDate = document.getElementById('txtLossDate').value;
      var lsMonth, lsDay, lsYear;
      if (lsDate.indexOf("/") != -1) {
        var laryDate = lsDate.split("/");
        lsMonth = laryDate[0];
        lsDay = laryDate[1];
        lsYear = laryDate[2];
      } else {
        //try to format with regular expression.
        lsYear = lsDate.substr(lsDate.length - 4, 4);
        var lsReminder = lsDate.substr(0, lsDate.length - 4);
        switch (lsReminder.length) {
          case 4:
            lsMonth = lsReminder.substr(0, 2);
            lsDay = lsReminder.substr(2, 2);
            break;
          case 3:
            if (parseInt(lsReminder.substr(0, 2), 10) > 12) {
              lsMonth = lsReminder.substr(0, 1);
              lsDay = lsReminder.substr(1, 2)
            } else {
              lsMonth = lsReminder.substr(0, 2);
              lsDay = lsReminder.substr(2, 1)              
            }
            break;
          case 2:
            lsMonth = lsReminder.substr(0, 1);
            lsDay = lsReminder.substr(1, 1);
            break;
        }
      }
  
      var lbBadDate = false;
      if (lsMonth == undefined || lsMonth < 1 || lsMonth > 12)
        lbBadDate = true;
      if (lsDay == undefined || lsDay < 1 || lsDay > 31)
        lbBadDate = true;
      if (lsYear == undefined || lsYear < 1)
        lbBadDate = true;
      if (lbBadDate == true)
      {
        alert("Please enter the date as MM/DD/YYYY or M/D/YY or MMDDYYYY.");
        document.getElementById('txtLossDate').focus();
        return;
      }
  
      if (lsMonth < 10 && lsMonth.charAt(0) != 0)
        lsMonth = "0"+lsMonth;

      if (lsDay < 10 && lsDay.charAt(0) != 0)
        lsDay = "0"+lsDay;

      var lsMydate = new Date();
      if (lsYear == undefined || lsYear == "" || (lsYear >= 100 && lsYear < 1900))
      {
        lsYear = lsMydate.getFullYear();
      }
      else if (lsYear >= 0 && lsYear < 100)
      {
        lsYear = Number(lsYear) + 2000;
      }
      else if ((lsYear >= 1900 && lsYear < 2000) || (lsYear > 2000))
      {
        lsYear = Number(lsYear);
      }
      else
      {
        lsYear = lsMydate.getFullYear();
      }

      document.getElementById('txtLossDate').value = lsMonth + "/" + lsDay + "/" + lsYear;
    }
  }
  
  function goCancel()
  {
    if (parent)
    {
      if (gsPrevPage == "ClaimSummary")
      {
        gsVehCount = parent.getVehCount();
        parent.document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ClaimInfo&vehCount="+gsVehCount;
      }
      else
      {
        var lbConfirmCancel = window.confirm("Stop entering this assignment and return to My Claims Desktop?");
        if (lbConfirmCancel)
          top.location = "frmMyClaimsDesktop.asp";
      }
    }
  }

  function ClaimNumberFormat(objInput) {
    
    var obj = document.getElementById("txtCarrierRepOfficeID");
    if (obj) {
      if (obj.value > "0")
        var bValid = FormatClaimNumber(objInput, obj.value, larrOfficeID, larrClaimNumberFormatJS, larrClaimNumberValidJS, larrClaimNumberMsgText, true);
    }
    return bValid;
  }

  function checkIfExists(){
    var lsClaimNumber;
    lsClaimNumber = document.getElementById("txtCoverageClaimNumber").value;
    document.getElementById("frmCheckClaimNumber").src = "frmClaimNumberCheck.asp?cn=" + lsClaimNumber + "&ob=txtCoverageClaimNumber";
  }
  
  function showFNOLSearch(){
    var lsDimensions = "dialogHeight:235px; dialogWidth:575px; "
    var lsSettings = "scroll:no; resizable:no; status:no; help:no; center:yes; unadorned:yes;"
    var lsQuery = "frmFNOLSearch.asp";
    var objRet = window.showModalDialog( lsQuery, window, lsDimensions + lsSettings );
    //window.open(lsQuery, window, lsDimensions + lsSettings);
    //window.open(lsQuery);
    if (typeof(objRet) == "object"){
         /*if (typeof(objRet.claimxml) == "string" && objRet.claimxml != ""){
            parent.objFNOLSelection = objRet;
         } else {
            parent.objFNOLSelection = null;
         }*/
         
         if (typeof(parent.setFNOLData) == "function"){
            parent.setFNOLData(objRet);
            populateClaimInfoFromFNOL();
         }
    } else {
      if (typeof(parent.setFNOLData) == "function"){
         parent.setFNOLData(null);
      }
    }
  }
  
  function populateClaimInfoFromFNOL(){
      if (typeof(parent.getFNOLClaimNumber) == "function"){
         document.getElementById("txtCoverageClaimNumber").value = parent.getFNOLClaimNumber();
         ldTrim(document.getElementById("txtCoverageClaimNumber"));
      }

      if (typeof(parent.getFNOLData) == "function"){
         document.getElementById("txtLossDate").value = parent.getFNOLData("/Root/Claim/@LossDate");
         document.getElementById("txtLossAddressState").value = parent.getFNOLData("/Root/Claim/@LossState");
         document.getElementById("txtLossDescription").value = parent.getFNOLData("/Root/Claim/@LossDescription");
         
         document.getElementById("txtInsuredNameFirst").value = parent.getFNOLData("/Root/Claim/Insured/@NameFirst");
         document.getElementById("txtInsuredNameLast").value = parent.getFNOLData("/Root/Claim/Insured/@NameLast");
         var strPhone = parent.getFNOLData("/Root/Claim/Insured/@Phone1AreaCode");
         if (isNaN(strPhone) == false)
            document.getElementById("txtInsuredPhoneAC").value = strPhone;
         strPhone = parent.getFNOLData("/Root/Claim/Insured/@Phone1Exchange")
         if (isNaN(strPhone) == false)
            document.getElementById("txtInsuredPhoneEN").value = strPhone;
         strPhone = parent.getFNOLData("/Root/Claim/Insured/@Phone1UnitNumber")
         if (isNaN(strPhone) == false)
            document.getElementById("txtInsuredPhoneUN").value = strPhone;
         
         //document.getElementById("txtRemarks").value = parent.getFNOLData("/Root/Claim/@Remarks");
      }
  }

  //hides the calender if clicked outside of it
  document.onclick = hideCalFrame;


</script>

</head>

<body onload="resizeWaIframe(0); pageInit(); parent.hideMsg();" tabIndex="-1">

<div id="divContainer">

  <form name="frmClaimInfo">
    <input id="txtTimeStarted" type="hidden">
    <input id="txtIntakeStartSeconds" type="hidden">
    <input id="txtInsuranceCompanyID" type="hidden" value="<%=liInsuranceCompanyID%>" >
    <input id="txtCarrierRepUserID" type="hidden" value="<%=liUserID%>" >
    <input id="txtCarrierRepOfficeID" type="hidden" value="<%=liOfficeID%>" opt="yes">
    <input id="txtCarrierRepNameFirst" type="hidden" value="<%=lsUserNameFirst%>">
    <input id="txtCarrierRepNameLast" type="hidden" value="<%=lsUserNameLast%>">
    <input id="txtCarrierRepPhoneDay" type="hidden"  value="<%=lsPhoneAreaCode%>-<%=lsPhoneExchangeNumber%>-<%=lsPhoneUnitNumber%>"   >
    <input id="txtCarrierRepEmailAddress" type="hidden" value="<%=lsUserEmail%>" >
    <input id="txtCarrierOfficeName" type="hidden" value="<%=lsOfficeName%>" >
    <input id="txtCarrierName" type="hidden" value="<%=lsInsuranceCompanyName%>" >
    <input id="txtFNOLUserID" type="hidden" value="<%=liUserID%>" >
    <input id="txtAssignmentDescription" type="hidden" value="<%=lsCurrentAssignmentDesc%>" >

  <%
    If liCurrAssignmentEmergencyLiability = 1 Then
  %>
      <!-- input for custom scripting -->
      <input type="hidden" name="SourceApplicationPassThruData" id="txtSourceApplicationPassThruData" inputTag="SourceApplicationPassThruData" value="">
  <%
    End If
  %>

    <table width="600" border="0" cellpadding="10" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse">
      <tr>
        <td nowrap valign="top">

        <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
          <DIV style="position:relative; top:-10px; width:100px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
            &nbsp;&nbsp;Claim Assigner
          </DIV>

          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
            <colgroup>
              <col width="320px"/>
              <col width="245px"/>
            </colgroup>
            <tr>
              <td nowrap class="legendnopad">

    <%
      If liCarrierRep = 0 or liAllowCarrierRepSelectionFlag = 1 Then
    %>

                Claim Assigned by: <span style="font-size:11pt;">*</span>

    <%
      Else
    %>

                Claim Assigned by: <span style="font-size:11pt;">*</span>
                <span style="color:#000000; font-weight:normal; font-size:7pt">(First/Last)</span>

    <%
      End If
    %>

              </td>
              <td nowrap class="legendnopad">
                Assigner Relation to Insured: <span style="font-size:11pt;">*</span>
              </td>
            </tr>
            <tr>
              <td nowrap>

    <%
      If liCarrierRep = 0 or liAllowCarrierRepSelectionFlag = 1 Then
  
      lsCarrierUsersListCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
    
      Response.Write GetPersistedData(lsCarrierUsersListCall, lsInsCoConfigGUID, "CarrierUsersListAll.xsl", "EnabledFlag=1" & lcDelimiter & "AllowCarrierRepSelectionFlag=" & liAllowCarrierRepSelectionFlag & lcDelimiter & "UserID=" & liUserID, true)
    %>

                <input name="CallerNameFirst" id="txtCallerNameFirst" type="hidden" value="">
                <input name="CallerNameLast" id="txtCallerNameLast" type="hidden" value="">

    <%
      Else
    %>

                <input name="CallerNameFirst" id="txtCallerNameFirst" type="text" value="<%=lsUserNameFirst%>" onBlur="ldTrim(this)" size="15" maxlength="50" tabIndex="2">
                &nbsp;&nbsp;
                <input name="CallerNameLast" id="txtCallerNameLast" type="text" value="<%=lsUserNameLast%>" onBlur="ldTrim(this)" size="25" maxlength="50" tabIndex="3">

    <%
      End If
    %>

              </td>
              <td nowrap>

            <%
              lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
            
              Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaCallerRelations.xsl", "tabIndexNo=4", true)
            %>

              </td>
            </tr>
          </table>

        </DIV>

        </td>
      </tr>
      <tr>
        <td nowrap valign="top">

        <DIV style="width:590px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
          <DIV style="position:relative; top:-10px; width:120px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
            &nbsp;&nbsp;Claim Information
          </DIV>

          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
            <colgroup>
              <col width="160px"/>
              <col width="160px"/>
              <col width="145px"/>
              <col width="100px"/>
            </colgroup>
            <tr>
              <td nowrap class="legendnopad">Claim Number: <span style="font-size:11pt;">*</span></span></td>
              <td nowrap class="legendnopad">Loss Date: <span style="font-size:11pt;">*</span>
              <span style="color:#000000; font-weight:normal; font-size:7pt">(MM/DD/YYYY)</span></td>
              <td nowrap class="legendnopad">Loss State: <span style="font-size:11pt;">*</span></td>
              <td rowspan="2">
              <%
                  lsFNOLFeature = "<Config Path=""ClaimPoint/Customizations/InsuranceCompany[@ID='" & liInsuranceCompanyID & "']/@FNOL""/>"
                  lsFNOLFeature = GetData(lsFNOLFeature, "", "")
                  
                  Dim docXML
                  
                  Set docXML = Server.CreateObject("MSXML2.DOMDocument")
                  docXML.Async = False
                  docXML.loadXML lsFNOLFeature
                  lsFNOLFeature = docXML.documentElement.text
              %>
              
                <button style="width:98px" onclick="showFNOLSearch()" <%if LCase(lsFNOLFeature) <> "true" then%>disabled="true"<%end if%>>Retrieve LYNX<br/>FNOL Details</button>
              </td>
            </tr>
            <tr>
              <td nowrap class="legendnopad">
              <input name="CoverageClaimNumber" id="txtCoverageClaimNumber" type="text" value="" onBlur="ldTrim(this);ClaimNumberFormat(this);checkIfExists()" size="20" maxlength="30" required="true" tabIndex="5">
              </td>
              <td nowrap class="legendnopad">
                <input name="LossDate" id="txtLossDate" type="text" value="" onKeyPress="return numbersOnly(this,event,0,1)" onBlur="formatLossDate()" size="10" maxlength="10" required="true" tabIndex="6">
                <a href="javascript:void(0)" onclick="showCalIframe(this, 'txtLossDate')" id="calCall" tabIndex="-1"><img src="images/calendar.gif" alt="" width="24" height="17" border="0" title="Click to select date from calendar"></a>
              </td>
              <td nowrap class="legendnopad">
              
            <%
               lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
            
               Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaLossState.xsl", "OfficeID=" & liOfficeID & lcDelimiter & "tabIndexNo=7", true)
            %>

              </td>
            </tr>

  <%
    If liCurrAssignmentSB511 = 1 and liInsuranceCompanyID = 99 Then
      ' Reset pointer to before first node
      ElemList.Reset
      Set oNode = ElemList.nextNode
  %>

            <tr id="tblSB511" style="display:none">
              <td colspan="4">

                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="border:1px solid #C0C0C0; border-collapse:collapse; margin-top:8px;">
              
  <%
      For each oNode in ElemList
    
        lsAssignmentTypeIDList = oNode.getAttribute("AssignmentTypeIDList")

        laAssignmentTypeID = split(lsAssignmentTypeIDList, "|")
        liStop = ubound(laAssignmentTypeID)
  
        For liCounter = 0 to liStop
  
          If liCurrentAssignmentId = laAssignmentTypeID(liCounter) Then
          
            lsAlertText = oNode.getAttribute("ScriptText")
  %>

                  <tr>
                    <td class="legendnopad" style="background-color:#f8f8f8;">
                      <%= lsAlertText %>
                    </td>
                  </tr>

  <%
          End If
        
        Next
      Next
  %>

                </table>
              </td>
            </tr>

  <%
    End If
  %>

            <tr>
              <td colspan="4" nowrap class="legend">
                Description of Loss: <span style="font-size:11pt;">*</span>
                <span id="commFldMsgLoss" style="color:#000000; font-weight:normal; font-size:7pt">(max.:500 characters)</span>
              </td>
            </tr>
            <tr>
              <td colspan="4" nowrap>
                <textarea name="LossDescription" id="txtLossDescription" wrap="physical" style="width:100%; height:52px;" onKeyDown="CheckInputLength(this, 500, 'commFldMsgLoss')" tabIndex="8"></textarea>
              </td>
            </tr>
          </table>

        </DIV>

        </td>
      </tr>
      <tr>
        <td nowrap valign="top">

        <DIV style="width:590px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
        <DIV style="position:relative; top:-10px; width:60px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
          &nbsp;&nbsp;Insured
        </DIV>

          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
            <colgroup>
              <col width="130px"/>
              <col width="190px"/>
              <col width="245px"/>
            </colgroup>
            <tr>
              <td nowrap class="legendnopad">
                First Name: <span style="font-size:11pt;">*</span>
              </td>
              <td nowrap class="legendnopad">
                Last Name: <span style="font-size:11pt;">*</span>
              </td>
              <td nowrap class="legendnopad">
                Phone Number:
                <span style="color:#000000; font-weight:normal; font-size:7pt">(numbers only)</span>
              </td>
            </tr>
            <tr>
              <td nowrap>
                <input name="InsuredNameFirst" id="txtInsuredNameFirst" type="text" value="" onBlur="ldTrim(this)" size="15" maxlength="50" tabIndex="9">
              </td>
              <td nowrap>
                <input name="InsuredNameLast" id="txtInsuredNameLast" type="text" value="" onBlur="ldTrim(this)" size="25" maxlength="50" tabIndex="10">
              </td>
              <td nowrap>
                <input name="InsuredPhoneAC" id="txtInsuredPhoneAC" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" value="" size="3" maxlength="3" tabIndex="11">
                <input name="InsuredPhoneEN" id="txtInsuredPhoneEN" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 3, event);" value="" size="3" maxlength="3" tabIndex="12">
                <input name="InsuredPhoneUN" id="txtInsuredPhoneUN" type="text" onKeyPress="return numbersOnly(this,event,0)" onKeyUp="return autoTab(this, 4, event);" value="" size="4" maxlength="4" tabIndex="13">
              </td>
            </tr>
            <tr>
              <td colspan="3" nowrap class="legend">Business Name: *
                <span style="color:#000000; font-weight:normal; font-size:8pt">(If Insured is a business)</span>
              </td>
            </tr>
            <tr>
              <td colspan="3" nowrap>
                <input name="InsuredBusinessName" id="txtInsuredBusinessName" type="text" value="" onBlur="ldTrim(this)" size="40" maxlength="50" tabIndex="14">
              </td>
            </tr>
            <tr>
              <td colspan="3" nowrap>

  <%
    If liCurrAssignmentEmergencyLiability = 1 Then
      Dim liTabIdx, lsRdName, lsAlertText, lsInputTag, lsInputRequiredFlag
      ' Reset pointer to before first node
      ElemList.Reset
      Set oNode = ElemList.nextNode
      liTabIdx = 14
  %>

                <table width="100%" border="0" cellspacing="0" cellpadding="4" style="border:1px solid #C0C0C0; border-collapse:collapse; margin-top:8px;">

  <%
    For each oNode in ElemList
  
      lsAssignmentTypeIDList = oNode.getAttribute("AssignmentTypeIDList")
  
      laAssignmentTypeID = split(lsAssignmentTypeIDList, "|")
  
      liStop = ubound(laAssignmentTypeID)
      For liCounter = 0 to liStop
  
        If liCurrentAssignmentId = laAssignmentTypeID(liCounter) Then
        
          lsRdName = oNode.getAttribute("InputResponseName")
          lsAlertText = oNode.getAttribute("ScriptText")
          lsInputTag = oNode.getAttribute("InputTag")
          lsInputRequiredFlag = oNode.getAttribute("InputRequiredFlag")
  
  %>

                  <tr>
                    <td class="legendnopad" style="background-color:#f8f8f8;">
                      <%= lsRdName %>:&nbsp;<% If lsInputRequiredFlag = 1 Then %>* <% End If %><br>
                      <span style="color:#000000; font-weight:normal"><%= lsAlertText %></span>
                    </td>
                    <td valign="top" nowrap="" style="background-color:#F8F8F8; font-weight:bold;">
                      <% liTabIdx = liTabIdx+1 %>
                      <input type="radio" name="<%= lsRdName %>" inputTag="<%= lsInputTag %>" tabIndex="<%= liTabIdx %>" value="1" opt="yes">Yes
                      <% liTabIdx = liTabIdx+1 %>
                      <input type="radio" name="<%= lsRdName %>" inputTag="<%= lsInputTag %>" tabIndex="<%= liTabIdx %>" value="0" opt="yes">No
                    </td>
                  </tr>
                    <input name="<%= lsRdName %>Text" id="txt<%= lsRdName %>Text" type="hidden" value="<%= lsAlertText %>">

  <%
          End If
        
        Next
      Next
  %>

                  <!-- <input name="<%= lsInputTag %>" id="txt<%= lsInputTag %>" type="hidden" value=""> -->
                </table>

  <%
    End If
  %>

              </td>
            </tr>

          </table>

        </DIV>

        </td>
      </tr>
      <tr>
        <td nowrap class="legendnopad">

          <DIV style="width:586px; padding:0px 10px 5px 10px;">
            Comments to LYNX Claim Representative:
            <span id="commFldMsg" style="color:#000000; font-weight:normal; font-size:7pt">(max. 500 characters)</span>
            <br>
            <textarea name="Remarks" id="txtRemarks" wrap="physical" style="width:100%; height:52px;" onKeyDown="CheckInputLength(this, 500)" tabIndex="20"></textarea>
          </DIV>

        </td>
      </tr>
    </table>

    <p class="bodyBlue"> All fields marked with an asterisk (*) are required.</p>

    <table width="572" border="0" cellpadding="0" cellspacing="0" style="align:right;">
      <tr>
        <td width="100%">&nbsp;</td>
        <td id="cancelImgSrc" align="right" valign="top" nowrap>
          <a href="javascript:goCancel()" tabIndex="21"
             title="Return to <% If lsPrevPage = "ClaimSummary" Then %> Summary <% Else %> My Claims Desktop <% End If %>"
             onMouseOver="window.status='Return to <% If lsPrevPage = "ClaimSummary" Then %> Summary <% Else %> My Claims Desktop <% End If %>'; return true"
             onMouseOut="window.status=''; return true">
            <img src='images/btn_cancel.gif' border='0' WIDTH='83' HEIGHT='31'>
          </a>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td id="nextImgSrc" align="right" valign="top" nowrap>
          <a href="javascript:chkClaimNumValidity('<%= lsPrevPage %>')" tabIndex="22"
          <% If lsPrevPage = "ClaimSummary" Then %>
             title="Save and Return to Summary Page"
             onMouseOver="window.status='Save and Return to Summary Page'; return true"
          <% Else %>
             title="To Vehicle Info Page"
             onMouseOver="window.status='To Vehicle Info Page'; return true"
          <% End If %>
             onMouseOut="window.status=''; return true">

          <% If lsPrevPage = "ClaimSummary" Then %>
            <img src="images/btn_ok.gif" border="0" WIDTH="52" HEIGHT="31">
          <% Else %>
            <img src="images/btn_next.gif" border="0" WIDTH="83" HEIGHT="31">
          <% End If %>
          </a>
        </td>
      </tr>
    </table>

  </form>

</div>

<iframe ID="CalFrame" NAME="CalFrame" SRC="blank.asp" STYLE="border:2px solid #404040; left:-500px; top:0px; display:none; position:absolute; width:170; height:186; z-index:100; filter: progid:DXImageTransform.Microsoft.Shadow(color='#666666', Direction=135, Strength=3);" MARGINHEIGHT=0 MARGINWIDTH=0 NORESIZE FRAMEBORDER=0 SCROLLING=NO></iframe>

<iframe name="frmCheckClaimNumber" id="frmCheckClaimNumber" style="height:0px;width:0px;visibility:hidden" src="blank.asp">
</iframe>

</body>
</html>

<%
  Set oNode = Nothing
  Set ElemList = Nothing
  Set loXML = Nothing
%>
