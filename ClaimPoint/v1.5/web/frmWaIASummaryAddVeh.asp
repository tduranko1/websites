<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.4.2.0 -->

<%
  Dim lsCurrentAssignment
  lsCurrentAssignment = Request.Cookies("CPSession")("CurrAssignmentDescription")

  lsPageName = "frmWaIAAssignmentSummary.asp"
  lsPageTitle = lsCurrentAssignment & " - Summary"
  liPageType = lcAssignmentPage
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<html>
<head>
<title><%=lsCurrentAssignment%> - Summary</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "";
  gsVehNum = "";
  gsVehCount = "<%=Request("vehCount")%>";
  
  function pageInit()
  {
     if(parent)
      parent.resetTimeOut();
  
    if (parent)
      parent.gsHelpSubTopicPage = "CP_IA_Assignment.htm";
    
    //addVehicleDiv(gsVehCount);
     if(parent)
      parent.getClaimSummaryData();
  }
  
  
  function submitClaim()
  {
    if(parent)
    {
      var lsWasSubmitted = parent.chkClaimSubmitted();
      if (lsWasSubmitted == "false")
      {
        parent.document.getElementById("uiFrame").src = "frmWaSubmitNewVehicle.asp?fromPage=IAAssignmentSummary";
      }
      else
      {
        alert("Unable to submit claim. This IA Assignment has already been submitted successfully.");
        return;
      }
    }
    else
      return false;
  }
  
  function editIAAssignment()
  {
    if(parent)
    {
      dspMsg();
      parent.document.getElementById("uiFrame").src = "frmWaIAAddVehicle.asp?fromPage=ClaimSummary";
    }
    else
      return false;
  }
  
  function dspMsg()
  {
    var objIFrame = parent.document.getElementById('uiFrame');
    objIFrame.style.height = "0";
    parent.showMsg();
  }
  
  function goCancel()
  {
    if (parent)
    {
      var lbConfirmCancel = window.confirm("Stop entering this assignment and return?");
      if (lbConfirmCancel)
      {
        top.location = "frmMyClaimsDesktop.asp";
      }
    }
  }


</script>

</head>
<body onload="resizeWaIframe(gsVehCount); pageInit(); parent.hideMsg();">

<div id="divContainer">

  <form name="frmSummaryInfo">

    <table id="tblClaimInfoBlock" width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse;">
      <tr>
        <td>

          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-bottom: 1px solid #000099;">
            <tr bgcolor="#E5E5F5"">
              <td align="right">
                <a href="javascript:editIAAssignment()"
                   title="Edit IA Assignment information"
                   onMouseOver="window.status='Edit IA Assignment information'; return true"
                   onMouseOut="window.status=''; return true">
                   <img border="0" src="images/btn_edit_blue.gif" WIDTH="83" HEIGHT="31">
                </a>
              </td>
            </tr>
          </table>

        </td>
      </tr>
      <tr>
        <td valign="top" style="padding: 0px 10px 10px 10px;">

          <table name="IAAssignmentInfoTable" id="IAAssignmentInfoTable" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
            <colgroup>
              <col width="162px"/>
              <col width="140px"/>
              <col width="160px"/>
              <col width="128px"/>
            </colgroup>
            <tr>
              <td nowrap class="legendSumm">Claim Assigned by:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap class="legendSumm">Relation to Insured:</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap colspan="2">
                <input name="CallerNameFirst" id="txtCallerNameFirst" type="text" class="inputView" value="" size="16" readonly="true">
                <input name="CallerNameLast" id="txtCallerNameLast" type="text" class="inputView" value="" size="21" readonly="true">
              </td>
              <td nowrap>
                <input name="CallerRelationToInsuredIDDescription" id="txtCallerRelationToInsuredIDDescription" type="text" class="inputView" value="" size="22" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td class="legendSumm">Claim Number:</td>
              <td class="legendSumm">Loss Date:</td>
              <td class="legendSumm">Loss State:</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="CoverageClaimNumber" id="txtCoverageClaimNumber" type="text" class="inputView" value="" size="20" readonly="true">
              </td>
              <td nowrap>
                <input name="LossDate" id="txtLossDate" type="text" class="inputView" value="" size="12" readonly="true">
              </td>
              <td nowrap>
                <input name="LossAddressState" id="txtLossAddressState" type="text" class="inputView" value="" size="4" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Insured Name:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap class="legendSumm">Insured Business Name:</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap colspan="2">
                <input name="InsuredNameFirst" id="txtInsuredNameFirst" type="text" class="inputView" value="" size="16" readonly="true">
                <input name="InsuredNameLast" id="txtInsuredNameLast" type="text" class="inputView" value="" size="21" readonly="true">
              </td>
              <td nowrap colspan="2">
                <input name="InsuredBusinessName" id="txtInsuredBusinessName" type="text" class="inputView" value="" size="50" readonly="true">
              </td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">IA Assignment Type:</td>
              <td colspan="3" nowrap class="legendSumm">Special Handling Instructions for Independant Appraiser:</td>
            </tr>
            <tr>
              <td nowrap valign="top">
                <input name="AssignmentTypeIDDescription" id="txtAssignmentTypeIDDescription" type="text" class="inputView" value="" size="24" readonly="true">
              </td>
              <td colspan="3" nowrap>
                <TEXTAREA cols="78" rows="4" wrap="physical" class="inputView" id="txtRemarksToIA" readonly="true" style="width:100%;height:50px;"></TEXTAREA>
              </td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Party:</td>
              <td nowrap class="legendSumm">Coverage:</td>
              <td nowrap class="legendSumm">Deductible:</td>
              <td nowrap class="legendSumm">Limit:</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="ExposureCD" id="txtExposureCD" type="text" class="inputView" value="" size="5" readonly="true">
              </td>
              <td nowrap>
                <input name="CoverageProfileUiCD" id="txtClientCoverageTypeDesc" type="text" class="inputView" value="" size="13" readonly="true">
              </td>
              <td nowrap>
                $<input name="DeductibleAmt" id="txtDeductibleAmt" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>
                $<input name="LimitAmt" id="txtLimitAmt" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
            </tr>
            <tr>
              <td nowrap colspan="4" class="legendSumm">Description of Loss/Damages:</td>
            </tr>
            <tr>
              <td nowrap colspan="4">
                <textarea id="txtDescLossDamage" cols="110" rows="4" wrap="physical" class="inputView" readonly="true" style="width:100%; height:50px;"></textarea>
              </td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Owner Name:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap class="legendSumm">Owner Phone:</td>
              <td nowrap class="legendSumm">Pref. Contact Phone:</td>
            </tr>
            <tr>
              <td nowrap colspan="2">
                <input name="OwnerNameFirst" id="txtOwnerNameFirst" type="text" class="inputView" value="" size="16" readonly="true">
                <input name="OwnerNameLast" id="txtOwnerNameLast" type="text" class="inputView" value="" size="21" readonly="true">
              </td>
              <td nowrap>
                <input id="txtOwnerPhoneSumm" type="text" class="inputView" value="" size="15" readonly="true">(Day)
              </td>
              <td nowrap>
                <input id="txtOwnerBestPhoneCD" type="text" class="inputView" value="" size="10" readonly="true">
              </td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Owner Business Name:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>
                <input id="txtOwnerNightPhoneSumm" type="text" class="inputView" value="" size="15" readonly="true">(Night)
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap colspan="2">
                <input name="OwnerBusinessName" id="txtOwnerBusinessName" type="text" class="inputView" value="" size="50" readonly="true">
              </td>
              <td nowrap>
                <input id="txtOwnerAltPhoneSumm" type="text" class="inputView" value="" size="15" readonly="true">(Cell/Alt)
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Vehicle Year:</td>
              <td nowrap class="legendSumm">Vehicle Make:</td>
              <td nowrap class="legendSumm">Vehicle Model:</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="VehicleYear" id="txtVehicleYear" type="text" class="inputView" value="" size="3" readonly="true">
              </td>
              <td nowrap>
                <input name="Make" id="txtMake" type="text" class="inputView" value="" size="12" readonly="true">
              </td>
              <td nowrap>
                <input name="Model" id="txtModel" type="text" class="inputView" value="" size="16" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">License Plate:</td>
              <td nowrap class="legendSumm">Vehicle VIN:</td>
              <td nowrap class="legendSumm">Color:</td>
              <td nowrap class="legendSumm">Odometer:</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="LicensePlateNumber" id="txtLicensePlateNumber" type="text" class="inputView" value="" size="18" readonly="true">
              </td>
              <td nowrap>
                <input name="VIN" id="txtVIN" type="text" class="inputView" value="" size="18" readonly="true">
              </td>
              <td nowrap>
                <input name="Color" id="txtColor" type="text" class="inputView" value="" size="18" readonly="true">
              </td>
              <td nowrap><input name="Color" id="txtMileage" type="text" class="inputView" value="" size="8" readonly="true"></td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Vehicle Location Name:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap class="legendSumm">Phone:</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap colspan="2">
                <input name="LocationName" id="txtLocationName" type="text" class="inputView" value="" size="50" readonly="true">
              </td>
              <td nowrap>
                <input name="LocationPhoneSum" id="txtLocationPhoneSumm" type="text" class="inputView" value="" size="15" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap colspan="4" class="legendSumm">Addess:</td>
            </tr>
            <tr>
              <td nowrap colspan="4">
                <input name="LocationAddress1" id="txtLocationAddress1" type="text" class="inputView" value="" size="50" readonly="true">
              </td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">City:</td>
              <td nowrap class="legendSumm">State:</td>
              <td nowrap class="legendSumm">Zip:</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="LocationAddressCity" id="txtLocationAddressCity" type="text" class="inputView" value="" size="16" readonly="true">
              </td>
              <td nowrap>
                <input name="LocationAddressState" id="txtLocationAddressState" type="text" class="inputView" value="" size="10" readonly="true">
              </td>
              <td nowrap>
                <input name="LocationAddressZip" id="txtLocationAddressZip" type="text" class="inputView" value="" size="10" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <table id="lastTbl" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
      <colgroup>
        <col width="400px"/>
        <col width="100px"/>
        <col width="90px"/>
      </colgroup>
      <tr>
        <td colspan="3" nowrap>
          <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="10">
        </td>
      </tr>
      <tr>
        <td valign="middle" class="legendnopad" style="font-size:14px;">
          Submit IA Assignment to LYNX Services
        </td>
        <td valign="top" nowrap>
          <a href='javascript:goCancel()'
             title='Cancel this assignment and return to My Claims Desktop'
             onMouseOver="window.status='Cancel this assignment and return to My Claims Desktop'; return true"
             onMouseOut="window.status=''; return true">
            <img src='images/btn_cancel.gif' border='0' WIDTH='83' HEIGHT='31'>
          </a>
        </td>
        <td nowrap>
          <a href="javascript:submitClaim()"
             title="Submit IA Assignment to LYNX"
             onMouseOver="window.status='Submit IA Assignment to LYNX'; return true"
             onMouseOut="window.status=''; return true">
            <img border="0" src="images/btn_submit_green.gif" WIDTH="83" HEIGHT="31">
          </a>
        </td>
      </tr>
      <tr>
        <td colspan="3" class="legendnopad">
          <span style="color:#000000; font-weight:normal; font-size:10pt">
          Please review the information for accuracy
          and make any necessary changes by pressing the "EDIT" button. When you are done, please press the "SUBMIT" button
          to send the information to LYNX Services and receive a LYNX ID.
          </span>
        </td>
      </tr>
    </table>

  </form>

</div>

</body>
</html>
