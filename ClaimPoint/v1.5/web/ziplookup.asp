<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
on error goto 0
dim strZip, strRet, lsZipXML, strGoodRet

strZip = UCase(trim(request("z")))

if strZip <> "" and isNumeric(strZip) = true then
   strRet = strZip
   lsZipXML = "<ZipLookup zipcode=""" & strZip & """/>"

   '------------------------------------
   ' Get the address of the web service
   '------------------------------------
    dim siteRootUrl,sServer, protocol, hostname, port

    if Request.ServerVariables("HTTPS") = "off" then
        protocol = "http"
    else
        protocol = "https"
    end if
    siteRootUrl = protocol & "://"

    hostname = Request.ServerVariables("HTTP_HOST")
    siteRootUrl = siteRootUrl & hostname        

    port = Request.ServerVariables("SERVER_PORT")
    if port <> 80 and port <> 443 then
        siteRootUrl = siteRootUrl & ":" & port
    end if

    sServer = siteRootUrl

   '-----------------------------
   ' 21Oct2013 - TVD
   ' Integrating Dot Net WS to
   ' replace the COM Calls
   '-----------------------------
   Dim xmlHttp, postUrl, xmlResponse, dataToSend, strResponse
   Set xmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
   dataToSend = "ZipCode=" & strZip
   postUrl = sServer &  "/PGWClaimPointFoundation/APDService.asmx/ZipCodeLookup"
   
   '-----------------------------------
   ' Post this data to the Web Service
   '-----------------------------------
   Call xmlHttp.Open("POST", postUrl, False, "", "")
   xmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
   xmlHttp.send dataToSend

   '-------------------------------
   ' Get the data into XML so we 
   ' can use the xpath
   '-------------------------------
   Set xmlResponse = Server.CreateObject("MSXML2.DOMDocument.4.0")
   xmlResponse.loadXML(xmlHttp.responseText)

   '-------------------------------
   ' Format the request so it can
   ' be handled by the TestBoxes
   ' "City|State|County"
   '-------------------------------
   strResponse = xmlResponse.documentElement.selectSingleNode("//ZipInfo").GetAttribute("City") & "|"  
   strResponse = strResponse & xmlResponse.documentElement.selectSingleNode("//ZipInfo").GetAttribute("State") & "|"  
   strResponse = strResponse & xmlResponse.documentElement.selectSingleNode("//ZipInfo").GetAttribute("County")

end if
   '-------------------------------
   ' Return the formatted data
   '-------------------------------
   response.write(strResponse)
   'Server.ScriptTimeout=200
   'response.write(Server.ScriptTimeout)
%>