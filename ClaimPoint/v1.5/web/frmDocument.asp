<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->

<%
  Dim liLynxID, liAspectID, lsAssignmentType, lsDocumentCall, liVehNumber
  
  liLynxID = Request("LynxID")
  liVehNumber = Request("VehNumber")
  liAspectID = Request("AspectID")
  lsAssignmentType = Request("AssignmentType")

  lsStyleSheet = "DocumentDetailAlt.xsl"
  lsPageName = "frmDocument.asp"
  lsPageTitle = "Document Details"
  lsPageSubTitle = "Estimates and Documents"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcDocument
  lsHelpPage = "CP_Claim_Documents.htm"
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incVerifyCurrentVehicle.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStartClm.asp"-->

<%
  dim lbOverwriteExisting
  lbOverwriteExisting = false
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspDocumentInfo.asp">

<script language="JavaScript" src="includes/incBrowserSniffer.js" type="text/javascript"></script>

<%
  Dim docXML, ElemList, liStatusID, lsServiceChannelCD

  set docXML = Server.CreateObject("MSXML2.DOMDocument")
  docXML.LoadXML GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "", "", true)

  If Not docXML.ParseError = 0 Then
    Response.Write "<b>Error Code:</b> " & docXML2.ParseError & "<br>"
  End If

  Set ElemList = docXML.SelectSingleNode("/Root/Vehicle[@ClaimAspectID='"& liAspectID & "']")
  If Not(ElemList is Nothing) Then
    liStatusID = ElemList.getAttribute("StatusID")
    lsServiceChannelCD = ElemList.getAttribute("PrimaryServiceChannelCD")
  Else
    liStatusID = ""
    lsServiceChannelCD = ""
  End If

  'If Not(ElemList is Nothing) Then
  '  liVehNumber = ElemList.getAttribute("VehicleNumber")
  'Else
  '  liVehNumber = 0
  'End If
  '
  Set ElemList = Nothing
  Set docXML = Nothing
%>

<!--#include file="includes/incGoDocument.asp"-->
<!--#include file="includes/incHeaderTableWithIcons.asp"-->
<!--#include file="includes/incClaimCommonTop.asp"-->

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
  <tr>
    <td><img border="0" src="images/mark_documents2.jpg" width="96" height="63"></td>
  </tr>
  <tr>
    <td>

      <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
        <colgroup>
          <col width="20">
          <col width="76">
        </colgroup>
        <tr>
          <td height="22"><img src="images/submenu_bg_sel.gif" alt="" width="20" height="22" border="0"></td>
          <td bgcolor="#FFFFFF">
            <a href="javascript:addDocument()" class="subnav"
               title="Add Document"
               onMouseOver="window.status='Add Document'; return true"
               onMouseOut="window.status=''; return true">
            Add Document</a>
          </td>
        </tr>
      </table>

    </td>
  </tr>
  <tr>
    <td><img border="0" src="images/submenu_bg_bot.gif" width="96" height="1"></td>
  </tr>
  <tr>
    <td>
      <img border="0" src="images/spacer.gif" width="1" height="10"/>
    </td>
  </tr>
  <tr>
    <td>
      <a href="Javascript:self.print()" class="subnav" title="Click to print this page.">
      <img src="images/icon_print.gif" align="top" width="31" height="20" border="0"/>Print</a>
    </td>
  </tr>
  <tr>
    <td>
      <img border="0" src="images/spacer.gif" width="1" height="6"/>
    </td>
  </tr>
  <tr>
    <td>
      <a href="frmSubmitComment.asp?LynxID=<%=liLynxID%>&AspectID=<%=liAspectID%>&AssignmentType=<%=lsAssignmentType%>&StatusID=<%=liStatusID%>&VehNumber=<%=liVehNumber%>" class="subnav" title="Click to send a comment to LYNX's rep">
      <img src="images/icon_mail.gif" align="top" width="31" height="20" border="0"/>Comment</a>
    </td>
  </tr>
</table>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
  Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "ClaimHeader.xsl", "pageTitle=" & lsPageSubTitle & lcDelimiter & "AspectID=" & liAspectID, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

<%
  lsDocumentCall = "<DocumentList InsuranceCompanyID=""" & liInsuranceCompanyID & """ LynxID=""" & liLynxID & """ ClaimAspectID=""" & liAspectID & """/>"
  Response.Write GetData(lsDocumentCall, lsStyleSheet, "")
%>

<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>

<!--#include file="includes/incCommonHTMLEndClm.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
