<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.2.1 -->

<%
  Dim lsCurrentPage, liLynxID, lsVehList, VehArray, liVehCounter, lsOptionTag, lsSelected, liVehNumber
  Dim lsChannel
  
  lsCurrentPage = Request("currentFileUploadPage")
  liLynxID = Request("LynxID")
  liVehNumber = Request("VehNumber")
  lsChannel = Request("ch")
  'lsCurrentPage = Request.Cookies("CPSession")("currentFileUploadPage")

  If lsCurrentPage = "frmDocumentUpload" Then
    lsVehList = Request.Cookies("CPSession")(liLynxID & "VehiclesList")
  Else
    lsVehList = Request("VehList")
    Response.Cookies("CPSession")(liLynxID & "VehiclesList") = lsVehList
    'Response.Cookies("CPSession")("VehList") = lsVehList
  End If

  VehArray = Split(lsVehList,",")
  liStop = ubound(VehArray)
%>

<!--#include file="includes/incGetData.asp"-->

<html>
<head>
<title>Estimate File Upload</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript">

  var inUpload_Pressed = false;
  function verifySubmit()
  {
    inUpload_Pressed = true;
    var lsCurrentPage = "<%=lsCurrentPage%>";
    var lsReq = "";
    var liLynxID = "<%=liLynxID%>";
    var lsCurrentPage = "<%=lsCurrentPage%>";
    var liVehNumber = "<%=liVehNumber%>";
    var lsDocPertainsTo;
    //var lsDocPertainsTo = document.getElementById("docPertainsTo").value;
    var lsSupSeqNumber = document.getElementById("supSequenceNumber").value;
    var lsFileType = document.getElementById("docType").value;
    var lsFile = document.getElementById("theFile").value;
    var lsFileExt = lsFile.substring(lsFile.length-3, lsFile.length).toLowerCase();
    var lsFileName = lsFile.substr(lsFile.lastIndexOf("\\")+1);
    document.getElementById("docName").value = lsFileName;

    if (lsCurrentPage == "frmDocumentUpload")
      lsDocPertainsTo = "VEH<%=liVehNumber%>";
    else
      lsDocPertainsTo = document.getElementById("docPertainsTo").value;

    if (document.getElementById("docType").value.length == 0)
      lsReq +=  "* A Document Type must be specified." + "\n" + "\n";
    else if (document.getElementById("docType").value == "Supplement" && document.getElementById("supSequenceNumber").value == "0")
      lsReq +=  "* Please select a Supplement number from the drop-down." + "\n" + "\n";

    if (lsCurrentPage == "frmDocumentUpload" && lsDocPertainsTo == 0)
      lsReq +=  "* A Vehicle to which the document pertains to is not specified." + "\n" + "\n";
    
    if (lsCurrentPage != "frmDocumentUpload" && document.getElementById("docPertainsTo").value.length == 0)
        lsReq +=  "* A Vehicle to which the document pertains to must be specified." + "\n" + "\n";

    if (lsFileExt =="")
      lsReq +=  "* Please specify a file to upload by clicking on the Browse button." + "\n";
    else if (lsFileExt!="pdf" && lsFileExt!="tif" && lsFileExt!="jpg" && lsFileExt!="gif" && lsFileExt!="bmp" && lsFileExt!="doc")
    {
      lsReq +=  "* The file type you are uploading is not allowable." + "\n";
      lsReq +=  "   Please only upload files with extensions of PDF TIF JPG GIF BMP DOC" + "\n";
    }

    if (lsReq.length > 0)
    {
      alert(lsReq);
      inUpload_Pressed = false;
      return;
    }
    else
    {
      showAnimateGif('inline');
      var lsFileUploadParams = liLynxID +"|"+ lsDocPertainsTo +"|"+ lsFileType +"|"+ lsSupSeqNumber +"|"+ lsFileName +"|"+ lsCurrentPage +"|"+ liVehNumber;
      document.cookie = "FileUploadParams=" + lsFileUploadParams;
      document.frmFileUpload.action += "&dt=" + lsFileType;
      document.frmFileUpload.submit();
    }
    inUpload_Pressed = false;
  }

  function ChkSupplement()
  {
    if (document.getElementById("docType").value == "Supplement")
      document.getElementById("supSequenceNumber").style.display = "inline";
    else
    {
      document.getElementById("supSequenceNumber").style.display = "none";
      document.getElementById("supSequenceNumber").value = "0";
    }
  }

  function ClearFileUploadParams()
  {
    document.cookie = "FileUploadParams=; expires=Thu, 01-Jan-70 00:00:01 GMT";
  }

  function initPage()
  {
    document.getElementById("docType").focus();
  }

  function showAnimateGif(state)
  {
    if (document.getElementById('animateGif'))
      document.getElementById('animateGif').style.display = state;
  }

</script>

</head>

<body onLoad="ClearFileUploadParams(); ChkSupplement(); initPage();">

   <p style="font-size:11pt">
<%
  If lsCurrentPage = "frmDocumentUpload" Then
%>
    To upload a document to be attached to LYNX ID <%=liLynxID%>-<%=liVehNumber%>, please follow these three steps:
<%
  Else
%>
    To upload a document to be included with this assignment, please follow these four steps:
<%
  End If
%>
   </p>
    <table width="655px" border="0" cellpadding="2" cellspacing="0">
      <tr>
        <td style="font-size:11pt">1. Select the <span style="FONT-WEIGHT:bold; COLOR:#000099;">Document Type</span>:</td>
        <td nowrap>
          <input type="hidden" name="docName" id="docName">
          <select name="docType" id="docType" onChange="ChkSupplement()">
<%if lsChannel = "TL" then %>
            <option value="" SELECTED></option>
            <option value="Title">Title</option>
            <option value="Vehicle Owner POA">Vehicle Owner - POA</option>
            <option value="Title Copy">Title Copy</option>
            <option value="Letter of Guarantee">Letter of Guarantee</option>
            <option value="Lien Holder POA">Lien Holder - POA </option>
            <option value="Other">Other</option>
<%else %>
            <option value="" SELECTED></option>
            <option value="Estimate">Estimate</option>
            <option value="Supplement">Supplement</option>
            <option value="Photograph">Photograph</option>
            <option value="Letter">Letter</option>
            <option value="Form">Form</option>
            <option value="Other">Other</option>
<%end if%>
          </select>
          <select name="supSequenceNumber" id="supSequenceNumber" style="display:none">
          	<option value="0" SELECTED></option>
          	<option value="1">1</option>
          	<option value="2">2</option>
          	<option value="3">3</option>
          	<option value="4">4</option>
          	<option value="5">5</option>
          	<option value="6">6</option>
          	<option value="7">7</option>
          	<option value="8">8</option>
          	<option value="9">9</option>
          	<option value="10">10</option>
          </select>
        </td>
      </tr>

<%
  If lsCurrentPage <> "frmDocumentUpload" Then
%>
      <tr>
        <td style="font-size:11pt">2. Select the <span style="FONT-WEIGHT:bold; COLOR:#000099;">Vehicle</span> it pertains to:</td>
        <td nowrap>
          <select name="docPertainsTo" id="docPertainsTo" <%If liStop = 2 Then%>DISABLED<%End If%> >
      <%
          If liStop > 2 Then Response.Write "<option value="""" SELECTED></option>"
          
          For liVehCounter = 0 to liStop
            If liStop = 2 Then lsSelected = " SELECTED"
            lsOptionTag = "<option value=""" & VehArray(liVehCounter) & """" & lsSelected & ">" & VehArray(liVehCounter +1) & "</option>"
            liVehCounter = liVehCounter + 1
            Response.Write lsOptionTag
          Next
      %>
          </select>
        </td>
      </tr>
<%
  End If
%>

      <tr>
        <td style="font-size:11pt">
<%
  If lsCurrentPage <> "frmDocumentUpload" Then
%>
          3. Specify the <span style="FONT-WEIGHT:bold; COLOR:#000099;">File</span> to upload <span style="FONT-SIZE:11px">(max size:5 Mb)</span>:
<%
  Else
%>
          2. Specify the <span style="FONT-WEIGHT:bold; COLOR:#000099;">File</span> to upload <span style="FONT-SIZE:11px">(max size:5 Mb)</span>:
<%
  End If
%>
        </td>
        <form name="frmFileUpload" method="post" encType="multipart/form-data" action="frmWaFileUploadAction.asp?ch=<%=lsChannel%>">
        <td nowrap>
          <input type="file" name="theFile" id="theFile" size="40">
        </td>
        </form>
      </tr>
      <tr>
        <td style="font-size:11pt">
<%
  If lsCurrentPage <> "frmDocumentUpload" Then
%>
          4. Submit by clicking the Upload button:&nbsp;&nbsp;&nbsp;
<%
  Else
%>
          3. Submit by clicking the Upload button:&nbsp;&nbsp;&nbsp;
<%
  End If
%>
        </td>
        <td >
          <input type="button" value="Upload" onClick="verifySubmit()">
          <SPAN align="center" id="animateGif" style="display:none;font-weight:bold">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Uploading... Please wait.&nbsp;&nbsp;&nbsp;&nbsp;
            <img src="images/loading.gif" alt="" width="78" height="7" border="0">
          </SPAN>
        </td>
      </tr>
    </table>

<%
  If lsCurrentPage = "frmDocumentUpload" Then
%>
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    	<td width="100%" align="right" valign="top">
      <a href=# onclick="javascript:if(inUpload_Pressed == true) return; else parent.closeMe()" class="mini"><img src="images/btn_cancel.gif" alt="Cancel" width="83" height="31" border="0"></a>&nbsp;&nbsp;
      </td>
    </tr>
  </table>
<%
  End If
%>

</body>

</html>
