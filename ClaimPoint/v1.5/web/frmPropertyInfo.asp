<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.4.2.0 -->

<%
  lsPageName = "frmPropertyInfo.asp"
  lsPageTitle = "Property Details"
  lsPageSubTitle = "Information"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcProperty
  lsHelpPage = "CP_Property.htm"
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incVerifyCurrentProperty.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->

<%
  dim lbOverwriteExisting
  lbOverwriteExisting = false
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspPropertyInfo.asp">

<!--#include file="includes/incHeaderTableWithIcons.asp"-->
<!--#include file="includes/incClaimCommonTop.asp"-->

<%
  Response.Write GetPersistedData(lsPropertyDetailCall, lsPropertyDetailGUID, "PropertyNavBar.xsl", "currentArea=" & lsPageSubTitle, true)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
  Response.Write GetPersistedData(lsClaimInfoCall, lsClaimInfoGUID, "PropertyHeader.xsl", "", true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->
<!--#include file="includes/incClaimDetailShort.asp"-->

<%
  Response.Write GetPersistedData(lsPropertyDetailCall, lsPropertyDetailGUID, "PropertyInfo.xsl", "", true)
%>

<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
