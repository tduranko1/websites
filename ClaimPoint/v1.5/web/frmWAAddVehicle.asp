<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->
<%
Dim  lsShopAssignmentType, IsShopCurrentAssignmentDesc

lsShopAssignmentType= Request("ShopAssignmentType")

 
  If Not lsShopAssignmentType Is Nothing Then
   If lsShopAssignmentType = "ChoiceShop" Then
     lsCurrentAssignmentDesc = "Choice Shop"
   End If

   If lsShopAssignmentType = "ProgramShop" Then
     lsCurrentAssignmentDesc = "Program Shop"
   End If

  End If
   
   IsShopCurrentAssignmentDesc = lsCurrentAssignmentDesc

  'lsCurrentAssignmentDesc="Program Shop"
  lsPageName = "frmWaAddVehicle.asp"
  lsPageTitle = "Web Assignment"
  liPageType = lcAfterInsuranceCompanySelect
  
  Dim liLynxID
  liLynxID = Request.Cookies("CPSession")("currentClaim")
    
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetCurrAssignment.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<%
  on error goto 0
  Dim lsAssignReturn, lsVehicleListCall, lsGUID, lsVehicleList, lsClaimDetail,sIsClaimPointAddRental
  Dim lo1stParty, li1stPartyCount, loClaimNode
  Dim lsInsuredFName, lsInsuredLName, lsInsuredBName
  Dim lsInsuredPhoneArea, lsInsuredPhoneEx, lsInsuredPhoneNum, lsCallerFName, lsCallerLName
  Dim lsInsuredRelation, liInsuredRelationID
  Dim lsClaimNumber, lsLossDate, lsLossState, lsPrevPage, liVehNum, lsLossDescription
  Dim loXML, lsInsConfigXML, ElemList, oNode, lsAssignmentTypeIDList, laAssignmentTypeID, docXML
  Dim lsFldName, lsAlertText, lsInputTag, lsInputRequiredFlag, liTabIdx
  Dim lsInsCoDetailsCall

  lsPrevPage = Request("fromPage")
  liVehNum = Request("vehNum")
sIsClaimPointAddRental = IsClaimPointAddRental()
  
  'We need Elemlist later in the page
  If liCurrAssignmentReserveLineType = 1 Then

    Set loXML = Server.CreateObject("MSXML2.DOMDocument")
    loXML.Async = False
  
    lsInsConfigXML = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
    loXML.LoadXML GetPersistedData(lsInsConfigXML, lsInsCoConfigGUID, "", "", true)
  
    If Not loXML.ParseError = 0 Then
      Response.Write "<b>Error Code:</b> " & loXML.ParseError & "<br>"
  
    Else
      Set ElemList = loXML.SelectNodes("/Root/CustomScripting")
    
    End If

  End If

  lsVehicleListCall = "<VehicleList InsuranceCompanyID=""" & liInsuranceCompanyID & """ LynxID=""" & liLynxID & """/>"
  lsGUID = Request.Cookies("CPSession")("guidArray0")
  
  lsVehicleList = GetPersistedData(lsVehicleListCall, lsGUID, "", "", false)
  
  set loXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  loXML.Async = False
  loXML.LoadXML lsVehicleList
  
  set lo1stParty = loXML.selectNodes("/Root/Vehicle[@ExposureCD='1']")
  li1stPartyCount = lo1stParty.length
  
    lsClaimDetail = GetPersistedData(lsClaimDetailCall, lsGUID, "", "", false)

    loXML.LoadXML lsClaimDetail
    
    set loClaimNode = loXML.selectSingleNode("/Root/Claim")
    if not (loClaimNode is nothing) then
      lsLossDate = loClaimNode.getAttribute("LossDate")
      lsLossState = Server.URLEncode(loClaimNode.getAttribute("LossState"))
      lsLossDescription = loClaimNode.getAttribute("LossDescription")
      if instr(1, lsLossDate, "T") > 0 then
        lsLossDate = mid(lsLossDate, 6, 2) + "/" + mid(lsLossDate, 9, 2) + "/" + mid(lsLossDate, 1, 4)
      end if
    end if
    
    set loClaimNode = loXML.selectSingleNode("/Root/Claim/Insured")
    if not (loClaimNode is nothing) then
      lsInsuredFName = loClaimNode.getAttribute("NameFirst")
      lsInsuredLName = loClaimNode.getAttribute("NameLast")
      lsInsuredBName = loClaimNode.getAttribute("BusinessName")
      lsInsuredPhoneArea = loClaimNode.getAttribute("DayAreaCode")
      lsInsuredPhoneEx = loClaimNode.getAttribute("DayExchangeNumber")
      lsInsuredPhoneNum = loClaimNode.getAttribute("DayUnitNumber")
    end if
    set loClaimNode = loXML.selectSingleNode("/Root/Claim/Caller")
    if not (loClaimNode is nothing) then
      lsCallerFName = loClaimNode.getAttribute("NameFirst")
      lsCallerLName = loClaimNode.getAttribute("NameLast")
      liInsuredRelationID = loClaimNode.getAttribute("InsuredRelationID")

      set loClaimNode = loXML.selectSingleNode("/Root/Reference[@List='CallerRelationToInsured' and @ReferenceID = " & liInsuredRelationID & "]")
      if not (loClaimNode is nothing) then
        lsInsuredRelation = loClaimNode.getAttribute("Name")
      end if
    end if
    set loClaimNode = loXML.selectSingleNode("/Root/Claim/Coverage")
    if not (loClaimNode is nothing) then
      lsClaimNumber = loClaimNode.getAttribute("ClientClaimNumber")
    end if

    
        Dim IsEmailCell
        IsEmailCell = "<Config Path=""ClaimPoint/Customizations/InsuranceCompany[@ID='" & liInsuranceCompanyID & "']/@RepairReferral""/>"
        IsEmailCell = GetData(IsEmailCell, "", "")
        Set docXML = Server.CreateObject("MSXML2.DOMDocument")
        docXML.Async = False
        docXML.loadXML IsEmailCell
        IsEmailCell = docXML.documentElement.text
        IsEmailCell = Lcase(IsEmailCell)
        IsEmailCell = Trim(IsEmailCell)
        IsEmailCell=Replace(IsEmailCell,"<config>","")
        IsEmailCell=Replace(IsEmailCell,"</config>","")

 function IsClaimPointAddRental()
    Dim strServiceURL , getUrl , xmlhttp 
        Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
        strServiceURL = GetWebServiceURL()
        getUrl = strServiceURL & "/GetApplicationVar?VariableName=ClaimPointAddRental_Flag" 
        xmlhttp.Open "GET",getUrl,false
        xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
        xmlhttp.send 
        IsClaimPointAddRental =  xmlhttp.responseXML.text 
     end function
        
     function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "http://dlynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
        case "STG"
        strmexMonikerString = "http://slynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
        case "PRD"
        strmexMonikerString = "http://lynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function
        
%>
<!-- include to check for and display if necessary APD offline/outage messages -->
<!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->
<html>
<head>
    <title>
        <%=IsShopCurrentAssignmentDesc%>
        - Vehicle Info</title>
    <link href="includes/apd_style.css" type="text/css" rel="stylesheet">
    <link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">
<script language="JavaScript" src="includes/incCalendar.js" type="text/javascript"></script>
    <script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "<%= lsPrevPage %>";
  gsVehNum = "<%= liVehNum %>";
  gsVehCount = "";
  lsClaimNumber = unescape("<%=lsClaimNumber%>");
  lsLossDate = "<%=lsLossDate%>";
  lsLossState = "<%=lsLossState%>";
  lsLossDescription = unescape("<%=Escape(lsLossDescription)%>");
  lsInsuredFName = "<%=lsInsuredFName%>";
  lsInsuredLName = "<%=lsInsuredLName%>";
  lsInsuredBName = "<%=lsInsuredBName%>";
  lsInsuredPhoneArea = "<%=lsInsuredPhoneArea%>";
  lsInsuredPhoneEx = "<%=lsInsuredPhoneEx%>";
  lsInsuredPhoneNum = "<%=lsInsuredPhoneNum%>";
  lsCallerFName = "<%=lsCallerFName%>";
  lsCallerLName = "<%=lsCallerLName%>";
  lsCallerRelation = "<%=lsInsuredRelation%>";
  liLynxID = "<%=liLynxID%>";
  parent.liInsuranceCompanyID = "<%=liInsuranceCompanyID%>";
  var gsRetToClaim, gsRetFromClaim, gsEditVehMode;
  var giReserveLineTypeScript = "<%= liCurrAssignmentReserveLineType %>";
  var liShowPromiseToPay = "<%= liShowPromiseToPay %>";
var sIsClaimPointAddRental = "<%=sIsClaimPointAddRental%>";
  
  lsFirstPartyAvail = <%if li1stPartyCount > 0 then response.write "true" else response.write "false" %>;

  function pageInit()
  {
if( <%= liInsuranceCompanyID %> == 184 && sIsClaimPointAddRental == "True"){
                document.getElementById("additionalRentalBlock").style.display = "block";
            }
    if(parent)
      parent.resetTimeOut();
  
    var lsCurrAssignment = "<%=IsShopCurrentAssignmentDesc%>";
    var lsCurrHelpURL;

    if (lsCurrAssignment == "Adverse Subro Desk Review")
      lsCurrHelpURL = "CP_Adverse_Subro_Desk_Review.htm";
    else if (lsCurrAssignment == "Demand Estimate Review")
      lsCurrHelpURL = "CP_Demand_Estimate_Review.htm";
    else if (lsCurrAssignment == "IA Estimate Review")
      lsCurrHelpURL = "CP_IA_Estimate_Review.htm";
    else if (lsCurrAssignment == "Program Shop")
      lsCurrHelpURL = "CP_Program_Shop_Assignment.htm";

    if (parent)
      parent.gsHelpSubTopicPage = lsCurrHelpURL;

    var liVehNum = gsVehNum;
    var lotmpCoverageSelID;
    var lotmpExposureSelID;
    var lsFirstPartyAvail;
  
    // 16Mar2012 - TVD - Elephant
	if ( <%=liInsuranceCompanyID %> == 374) {
		if ((document.getElementById("rbTypeAuthorized").checked) || (document.getElementById("rbTypeEstimate").checked)) {
		}
		else {
			document.getElementById("rbTypeAuthorized").checked = 1;
			setAdditionalInfo();
		}
	}
	else {
	}
/*
	try {
		if ((document.getElementById("rbTypeAuthorized").checked) || (document.getElementById("rbTypeEstimate").checked)) {
		}
		else {
			document.getElementById("rbTypeAuthorized").checked = 1;
			setAdditionalInfo();
		}
	}
	catch(err)
	{
	}
*/
	
    if (gsPrevPage == "retFromClaim")
    {
      gsRetToClaim = "false";
      gsRetFromClaim = "true";
      gsEditVehMode = "true";
      gsVehNum = '1';
      if(parent)
        parent.getVehicleInfoData(gsVehNum);
      lotmpCoverageSelID = document.getElementById("txtCoverageProfileUiCD").value;
      lotmpExposureSelID = document.getElementById("txtExposureCD").value;
      updParty(lotmpExposureSelID);
      document.getElementById("txtCoverageProfileUiCD").value = lotmpCoverageSelID;
    }
    else if (gsPrevPage == "ClaimSummary" || gsPrevPage == "ShopSelection")
    {
      document.getElementById("txtAssignmentTypeID").value = "<%=liCurrentAssignmentId%>";
      gsRetToClaim = "false";
      gsRetFromClaim = "false";
      gsEditVehMode = "true";
  
      if(gsPrevPage == "ClaimSummary" && document.getElementById("txtPromiseToPayFlag"))
        document.getElementById("txtPromiseToPayFlag").checked = true;
        
      if (liVehNum == "")
        liVehNum = 1;
      if(parent)
        parent.getVehicleInfoData(liVehNum);
  
      if (document.getElementById("txtRentalAuthorized").value == "Yes")
        document.getElementById("txtcb_Rental").checked = true;
      else
        document.getElementById("txtcb_Rental").checked = false;
      
      
      lotmpCoverageSelID = document.getElementById("txtCoverageProfileUiCD").value;
      lotmpExposureSelID = document.getElementById("txtExposureCD").value;
  
      if (lotmpExposureSelID != 1)
      {
        if(parent)
        {
          if (lsFirstPartyAvail == false)
            updateSelect('selParty',0);
        }
        else
          return;
        
        
        updRentalFldsState();
      }
  
      document.getElementById("txtExposureCD").value = lotmpExposureSelID;
      updParty(lotmpExposureSelID);
      document.getElementById("txtCoverageProfileUiCD").value = lotmpCoverageSelID;
    }
    else if (gsPrevPage == "ClaimSummary_addNew")
    {
      var lsFirstPartyAvail;
      gsRetToClaim = "false";
      gsRetFromClaim = "false";
      gsEditVehMode = "false";

      gsVehNum = parent.getLastVehNum();
      var loDCIFrame = parent.getdcIFrameDoc();
      if (loDCIFrame){
        for (var i = 1; i <= gsVehNum; i++) {
          var loExposureCD = loDCIFrame.getElementById("txtExposureCD_" + i);
          if (loExposureCD) {
            if (loExposureCD.value == "1") {
              lsFirstPartyAvail = true;
              //first party exists. so remove it.
              var loSelParty = document.getElementById("txtExposureCD");
              if (loSelParty) {
                for (var x=0; x < loSelParty.options.length; x++){
                  if (loSelParty.options[x].innerHTML == "1st Party"){
                    loSelParty.options.remove(x);
                    break;
                  }
                }
              }
            }
          }
        }
      }
      document.getElementById("txtAssignmentTypeID").value = "<%=liCurrentAssignmentId%>";

      if (lsFirstPartyAvail == false)
        updateSelect('selParty',0);

      updRentalFldsState();
      updParty("");
    }
    else
    {
      gsRetToClaim = "false";
      gsRetFromClaim = "false";
      gsEditVehMode = "false";

      document.getElementById("txtAssignmentTypeID").value = "<%=liCurrentAssignmentId%>";

      if (lsFirstPartyAvail == false)
        updateSelect('selParty',0);

      updParty("");
      updRentalFldsState();
    }
    
    //update the DataContainer with the intial values
    if (parent) {
      parent.initClaimData(liLynxID, lsClaimNumber, lsLossDate, lsLossState, lsLossDescription, lsInsuredFName, 
                            lsInsuredLName, lsInsuredBName, lsInsuredPhoneArea, lsInsuredPhoneEx, 
                            lsInsuredPhoneNum, lsCallerFName, lsCallerLName, lsCallerRelation)      
    }
    
    //update the claim information
    document.getElementById("txtCoverageClaimNumber").innerHTML = lsClaimNumber;
    document.getElementById("txtLossDate").innerHTML = lsLossDate;
    document.getElementById("txtLossAddressState").innerHTML = lsLossState;
    document.getElementById("txtLossDescription").value = lsLossDescription;
    document.getElementById("txtCallerName").innerHTML = lsCallerFName + " " + lsCallerLName;
    //document.getElementById("txtCallerNameLast").innerHTML = lsCallerLName;
    document.getElementById("txtCallerRelationToInsuredIDDescription").innerHTML = (lsCallerRelation == "" ? " " : lsCallerRelation) ;
    document.getElementById("txtInsuredName").innerHTML = lsInsuredFName + " " + lsInsuredLName;
    //document.getElementById("txtInsuredNameLast").innerHTML = lsInsuredLName;
    //document.getElementById("txtInsuredPhoneSumm").innerHTML = "(" + lsInsuredPhoneArea + ") " + lsInsuredPhoneEx + "-" + lsInsuredPhoneNum;
    document.getElementById("txtInsuredBusinessName").innerHTML = (lsInsuredBName == "" ? "&nbsp;" : lsInsuredBName);
    
  }

function setRental() 
    {
        if (document.getElementById("txtAssignmentTypeID").value == 16)
        {
            var trRow = document.getElementById("trRental");
            if (trRow.id != null) 
            {
                trRow.style.display = "none";
            }
        }

    }

    //13Feb2012 - TVD - Elephant Changes (Begin)-->
    function setDisableEstimate() {
        if (document.getElementById("divAdditionalInfo")) {
            if (document.getElementById("txtDrivable").value == 1) {
            }
            else {
                if (document.getElementById("rbTypeEstimate").checked == true ) {
                    document.getElementById("txtDrivable").value = 1
                    alert("Non-drivable vehicle cannot be selected when assignment is to 'Secure Estimate Only'");
                }
            }
        }
    }
    
    function setAdditionalInfo() {
        if (document.getElementById("divAdditionalInfo")) {
            if (document.getElementById("rbTypeEstimate").checked) {
                //Clear the Drivable answer
                document.getElementById("txtDrivable").value = 1
            }
            else {
            }
            var additionalInfo = document.getElementById("rbTypeEstimate").checked ? "E" : "A";
            document.getElementById("txtSourceApplicationPassthruDataVeh").value = additionalInfo;
            //alert(document.getElementById("txtSourceApplicationPassthruDataVeh").value);
        }
    }

  function goCancel()
  {
    if (parent)
    {
      if(gsPrevPage == "ShopSelection" || gsPrevPage == "ClaimInfo")
      {
        var lbConfirmCancel = window.confirm("Stop entering this assignment and return to My Claims Desktop?");
        if (lbConfirmCancel)
          top.location = "frmMyClaimsDesktop.asp";
      }
      else
      {
        gsVehCount = parent.getVehCount();
        parent.document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=VehicleInfo&vehCount=" + gsVehCount;
      }
    }
  }


  function updVehicleData(RTC, vehNum)
  {
    if (liShowPromiseToPay == 1 && document.getElementById("txtPromiseToPayFlag").checked == false)
    {
      alert("The Promise to Pay must be accepted in order to submit an assignment to LYNX Services.\n" +
            "Please see your supervisor if you have questions.");
      return;
    }
    
    var liVehNum = vehNum;
    var lsAddNewVeh = "false";
    var lsVehCount = parent.getVehCount();
  
    if (RTC == 1)
    {
      gsRetToClaim = "true";
      if (lsVehCount.charAt(0) == 0)
        lsAddNewVeh = "true";
    }
    else if (gsPrevPage == "ClaimSummary")
    {
      lsAddNewVeh = "false";
    }
    else if ((document.getElementById("txtAssignmentTypeID").value != 4) && (document.getElementById("txtAssignmentTypeID").value != 16) && (document.getElementById("txtAssignmentTypeID").value != 17) && ((gsPrevPage != "ClaimSummary_addNew") || (gsPrevPage != "ClaimInfo")))
    {
      if(parent)
      {
        if (lsVehCount.charAt(0) != 0)
          parent.clrShopValues(liVehNum); //flag to clear shop id and comments if the not SP

        if (lsVehCount.charAt(0) == 0 || gsPrevPage == "ClaimSummary_addNew")
          lsAddNewVeh = "true";
        else
          lsAddNewVeh = "false";
      }
    }
    else if (gsPrevPage == "ClaimSummary_addNew")
    {
      lsAddNewVeh = "true";
    }

    if (gsVehNum == "")
      gsVehNum = 1;

    if(parent)
      parent.tsVehicleData(gsRetToClaim, gsRetFromClaim, gsEditVehMode, lsAddNewVeh, gsVehNum);
  }


  function updParty(selIndex)
  {
    if (selIndex == "")
    {
      updateSelect('selCoverage',0);
    }
    else if (selIndex == "1")
    {
      updateSelect('selCoverage',1);
      if(parent)
        parent.partySelChange(1);
    }
    else if (selIndex == "3")
    {
      updateSelect('selCoverage',2);
      if(parent)
        parent.partySelChange(3);
    }

    var selObj = document.getElementById('txtCoverageProfileUiCD');
    updCovReqFlds(selObj.options[selObj.selectedIndex].value);

    updRentalFldsState();
  }


  function updCovReqFlds(selIndex)
  {
    var aVal = new Array("", "")
    if (selIndex != "") 
      var aVal = selIndex.split("|");

    document.getElementById("txtCoverageProfileCD").value = aVal[0];
    document.getElementById("txtClientCoverageTypeUiID").value = aVal[1];
    var oSel = document.getElementById("txtCoverageProfileUiCD");
    document.getElementById("txtClientCoverageTypeDesc").value = oSel.options[oSel.selectedIndex].text;
    
    if (document.getElementById("txtAssignmentTypeID").value == 16) 
    {
        CovReqFldsDisplay("txtDeductibleAmt", "hidden");
        CovReqFldsDisplay("txtLimitAmt", "hidden");
    }
    else 
    {
        selIndex = aVal[0];
        if (selIndex == "")
        {
          CovReqFldsDisplay("txtDeductibleAmt", "hidden");
          CovReqFldsDisplay("txtLimitAmt", "hidden");
        }
        else if (selIndex == "COLL" || selIndex == "COMP")
        {
          CovReqFldsDisplay("txtDeductibleAmt", "visible");
          CovReqFldsDisplay("txtLimitAmt", "hidden");
        }
        else if (selIndex == "LIAB")
        {
          CovReqFldsDisplay("txtDeductibleAmt", "hidden");
          CovReqFldsDisplay("txtLimitAmt", "visible");
        }
        else if (selIndex == "UIM" || selIndex == "UM")
        {
          CovReqFldsDisplay("txtDeductibleAmt", "visible");
          CovReqFldsDisplay("txtLimitAmt", "visible");
        }
    }

  }


  function CovReqFldsDisplay(objID, state)
  {
    document.getElementById(objID+"Req").style.visibility = state;
    if (state == "visible")
      document.getElementById(objID).required = "true";
    else
      document.getElementById(objID).required = "false";
  }


  function updRentalFldsState()
  {
    var cbRental = document.getElementById("txtcb_Rental").checked;

	//13Feb2012 - TVD - Elephant Changes-
    if (document.getElementById("divAdditionalInfo")) {
        if ((document.getElementById("rbTypeEstimate").checked == true) && (document.getElementById("txtcb_Rental").checked == true)) {
            //Throw a message if you try to active rentals.
            alert("Rentals cannot be selected when assignment is to 'Secure Estimate Only'");
            document.getElementById("txtcb_Rental").checked = false;
        }
        else {
        }
    }

    loExposureSelID = document.getElementById("txtExposureCD").value;
    if (cbRental == true)
    {
      document.getElementById("txtRentalAuthorized").value = "Yes";
      if (loExposureSelID == "1")
      {
        document.getElementById("txtRentalDays").disabled = false;
        document.getElementById("txtRentalDays").style.backgroundColor = "";
        document.getElementById("txtRentalDayAmount").disabled = false;
        document.getElementById("txtRentalDayAmount").style.backgroundColor = "";
        document.getElementById("txtRentalMaxAmount").disabled = false;
        document.getElementById("txtRentalMaxAmount").style.backgroundColor = "";
        document.getElementById("txtRentalDaysAuthorized").disabled = false;
        document.getElementById("txtRentalDaysAuthorized").style.backgroundColor = "";
        document.getElementById("txtRentalInstructions").disabled = false;
        document.getElementById("txtRentalInstructions").style.backgroundColor = "";
                        document.getElementById("txtcb_Rental_additional").disabled = false;
                        document.getElementById("txtcb_Rental_additional").style.backgroundColor = "";
      }
      else if (loExposureSelID == "3")
      {
        document.getElementById("txtRentalDays").disabled = true;
        document.getElementById("txtRentalDays").style.backgroundColor = "InactiveBorder";
        document.getElementById("txtRentalDays").value = "";
        document.getElementById("txtRentalDayAmount").disabled = true;
        document.getElementById("txtRentalDayAmount").style.backgroundColor = "InactiveBorder";
        document.getElementById("txtRentalDayAmount").value = "";
        document.getElementById("txtRentalMaxAmount").disabled = true;
        document.getElementById("txtRentalMaxAmount").style.backgroundColor = "InactiveBorder";
        document.getElementById("txtRentalMaxAmount").value = "";
        document.getElementById("txtRentalDaysAuthorized").disabled = false;
        document.getElementById("txtRentalDaysAuthorized").style.backgroundColor = "";
        document.getElementById("txtRentalInstructions").disabled = false;
        document.getElementById("txtRentalInstructions").style.backgroundColor = "";
                        document.getElementById("txtcb_Rental_additional").disabled = false;
                        document.getElementById("txtcb_Rental_additional").style.backgroundColor = "";
      }
      else if (loExposureSelID == "")
      {
        document.getElementById("txtRentalDays").disabled = true;
        document.getElementById("txtRentalDays").style.backgroundColor = "InactiveBorder";
        document.getElementById("txtRentalDays").value = "";
        document.getElementById("txtRentalDayAmount").disabled = true;
        document.getElementById("txtRentalDayAmount").style.backgroundColor = "InactiveBorder";
        document.getElementById("txtRentalDayAmount").value = "";
        document.getElementById("txtRentalMaxAmount").disabled = true;
        document.getElementById("txtRentalMaxAmount").style.backgroundColor = "InactiveBorder";
        document.getElementById("txtRentalMaxAmount").value = "";
        document.getElementById("txtRentalDaysAuthorized").disabled = true;
        document.getElementById("txtRentalDaysAuthorized").style.backgroundColor = "InactiveBorder";
        document.getElementById("txtRentalDaysAuthorized").value = "";
        document.getElementById("txtRentalInstructions").disabled = true;
        document.getElementById("txtRentalInstructions").style.backgroundColor = "InactiveBorder";
        document.getElementById("txtRentalInstructions").value = "";
                        document.getElementById("txtcb_Rental_additional").style.backgroundColor = "InactiveBorder";
      }
    }
    else if (cbRental == false)
    {
      document.getElementById("txtRentalAuthorized").value = "No";

      document.getElementById("txtRentalDays").disabled = true;
      document.getElementById("txtRentalDays").style.backgroundColor = "InactiveBorder";
      document.getElementById("txtRentalDays").value = "";
      document.getElementById("txtRentalDayAmount").disabled = true;
      document.getElementById("txtRentalDayAmount").style.backgroundColor = "InactiveBorder";
      document.getElementById("txtRentalDayAmount").value = "";
      document.getElementById("txtRentalMaxAmount").disabled = true;
      document.getElementById("txtRentalMaxAmount").style.backgroundColor = "InactiveBorder";
      document.getElementById("txtRentalMaxAmount").value = "";
      document.getElementById("txtRentalDaysAuthorized").disabled = true;
      document.getElementById("txtRentalDaysAuthorized").style.backgroundColor = "InactiveBorder";
      document.getElementById("txtRentalDaysAuthorized").value = "";
      document.getElementById("txtRentalInstructions").disabled = true;
      document.getElementById("txtRentalInstructions").style.backgroundColor = "InactiveBorder";
      document.getElementById("txtRentalInstructions").value = "";
    //Additional Rental
                    document.getElementById("txtcb_Rental_additional").style.backgroundColor = "InactiveBorder";
                    document.getElementById("txtcb_Rental_additional").disabled = true;
                    document.getElementById("tblRentalAdditional").style.display = "none";
                    document.getElementById("txtcb_Rental_additional").checked = false;
                    document.getElementById("txtRentalVendor").value = "";
                    document.getElementById("txtRentalResConf").value = "";
                    document.getElementById("txtRentalVehicleClass").value = "";
                    document.getElementById("txtRentalRateType").value = "";
                    document.getElementById("txtRentalRate").value = "";
                    document.getElementById("txtRentalAuthPickup").value = "";
                    document.getElementById("txtRentalTaxRate").value = "";
                    document.getElementById("txtRentalPhoneAC").value = "";
                    document.getElementById("txtRentalPhoneEN").value = "";
                    document.getElementById("txtRentalPhoneUN").value = "";
                    document.getElementById("txtRentalAddress").value = "";
                    document.getElementById("txtRentalCity").value = "";
                    document.getElementById("txtRentalState").value = "";
                    document.getElementById("txtRentalZip").value = "";
                }
            }
        

          function updAdditionalRentalFldsState(){
            var cbAddRental = document.getElementById("txtcb_Rental_additional").checked;
            if(cbAddRental == true){
                document.getElementById("tblRentalAdditional").style.display = "block";
            }
            else{
                document.getElementById("tblRentalAdditional").style.display = "none";
                document.getElementById("txtRentalVendor").value = "";
                document.getElementById("txtRentalResConf").value = "";
                document.getElementById("txtRentalVehicleClass").value = "";
                document.getElementById("txtRentalRateType").value = "";
                document.getElementById("txtRentalRate").value = "";
                document.getElementById("txtRentalAuthPickup").value = "";
                document.getElementById("txtRentalTaxRate").value = "";
                document.getElementById("txtRentalPhoneAC").value = "";
                document.getElementById("txtRentalPhoneEN").value = "";
                document.getElementById("txtRentalPhoneUN").value = "";
                document.getElementById("txtRentalAddress").value = "";
                document.getElementById("txtRentalCity").value = "";
                document.getElementById("txtRentalState").value = "";
                document.getElementById("txtRentalZip").value = "";
            }
        }

  var laryParty = new Array(1);
  laryParty[0] = new Array("3rd Party","3");
  
  var laryCoverage = new Array(3);
  
  function updateSelect(sel, n)
  {
    var loSel;
    var laryTmp;
    if (sel == "selParty")
    {
      loSel = document.getElementById('txtExposureCD');
      laryTmp = laryParty[n];
    }
    else if (sel == "selCoverage")
    {
      loSel = document.getElementById('txtCoverageProfileUiCD');
      laryTmp = laryCoverage[n];
    }
    var liCurr = loSel.options.length;
    for (var j=liCurr; j>0; j--)
      loSel.options[j] = null;
    for (var i=0; i<laryTmp.length; i++)
    {
      loSel.options[loSel.options.length] = new Option(laryTmp[i], laryTmp[i+1]);
      i++;
    }
    if (loSel.id == "txtCoverageProfileUiCD" && loSel.options.length == 2) {//the first item is blank.
      // auto select the only item in the coverage.
      loSel.selectedIndex = 1;
    }
  }


  function updContactName()
  {
    var lsSelParty = document.getElementById('txtExposureCD').value;
    if (lsSelParty == 3 && document.getElementById('txtContactNameFirst').value == "" && document.getElementById('txtContactNameLast').value == "")
    {
      document.getElementById('txtContactNameFirst').value = document.getElementById('txtOwnerNameFirst').value;
      document.getElementById('txtContactNameLast').value = document.getElementById('txtOwnerNameLast').value;
    }
  }


  function updContactPhone()
  {
    var lsSelParty = document.getElementById('txtExposureCD').value;
    if (lsSelParty == 3 && document.getElementById('txtContactPhoneAC').value == "" && document.getElementById('txtContactPhoneEN').value == "" && document.getElementById('txtContactPhoneUN').value == "")
    {
      document.getElementById('txtContactPhoneAC').value = document.getElementById('txtOwnerPhoneAC').value;
      document.getElementById('txtContactPhoneEN').value = document.getElementById('txtOwnerPhoneEN').value;
      document.getElementById('txtContactPhoneUN').value = document.getElementById('txtOwnerPhoneUN').value;
    }
  }


  function getPriDamage()
  {
    var laryChkBxs = document.frmVehicleInfo.cb_priDamage;
    var liPrimaryCB;
    var cbLength = laryChkBxs.length;
    for (var i=0; i<cbLength; i++)
    {
      if (laryChkBxs[i].checked == true)
        liPrimaryCB = laryChkBxs[i].value;
    }
    return liPrimaryCB;
  }


  function getSecDamage()
  {
    var laryChkBxs = document.frmVehicleInfo.cb_secDamage;
    var lsSecondaryCB = "";
    var cbLength = laryChkBxs.length;
    for (var i=0; i<cbLength; i++)
    {
      if (laryChkBxs[i].checked == true)
      {
        if (lsSecondaryCB != "")
          lsSecondaryCB += "," + laryChkBxs[i].value;
        else
          lsSecondaryCB += laryChkBxs[i].value;
      }
    }
    return lsSecondaryCB;
  }


  function getContactBestPhone()
  {
    var laryChkBxs = document.frmVehicleInfo.cb_ContactBestPhoneCD;
    var lsContactBestPhoneCB = "";
    var cbLength = laryChkBxs.length;
    for (var i=0; i<cbLength; i++)
    {
      if (laryChkBxs[i].checked == true)
        lsContactBestPhoneCB = laryChkBxs[i].value;
    }
    return lsContactBestPhoneCB;
  }


  function setPrimaryCB(cbObj)
  {
    var loTbl = document.getElementById("tblDamageCb");
    var laryChkBxs = loTbl.getElementsByTagName("INPUT");
    var cbLength = laryChkBxs.length;
    for (var i=0; i<cbLength; i++)
    {
      if (laryChkBxs[i] == cbObj)
      {
        if (laryChkBxs[i+1].checked == true)
          laryChkBxs[i+1].checked = false;
        laryChkBxs[i].checked = true;
      }
      else if (laryChkBxs[i].name == "cb_priDamage" && laryChkBxs[i].checked == true)
      {
        laryChkBxs[i].checked = false;
        if (laryChkBxs[i+1].value != 29 && document.getElementById("cb_secDamage29").checked == false) //if No Damge is uncheked
          laryChkBxs[i+1].checked = true;
      }
    }
  }
  
  
  function chkPrimaryCB(cbObj)
  {
    var loCurrObj = cbObj;
    var lbNextPriCBState = loCurrObj.parentElement.previousSibling.firstChild.checked;
  
    if (lbNextPriCBState == true)
    {
      loCurrObj.checked = false;
      return;
    }
    else if (loCurrObj.value == 29 && lbNextPriCBState == false)
    {
      if (loCurrObj.checked == true)
      {
        resetSecondaryCB(0);
        return;
      }
    }
    else if (loCurrObj.value != 29 && lbNextPriCBState == false)
    {
      resetSecondaryCB(1);
      return;
    }
  }
  
  
  function resetSecondaryCB(dis)
  {
    var loTbl = document.getElementById("tblDamageCb");
    var laryChkBxs = loTbl.getElementsByTagName("INPUT");
    var liCBLength = laryChkBxs.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (laryChkBxs[i].name == "cb_secDamage" && laryChkBxs[i].value != 29 && dis == 0)
        laryChkBxs[i].checked = false;
      else if (laryChkBxs[i].name == "cb_secDamage" && laryChkBxs[i].value == 29 && dis == 1)
        laryChkBxs[i].checked = false;
    }
  }
  
  
  var gPrefFlag
  function setContactCB(cbObj)
  {
    var loTbl = document.getElementById("tblContactCb");
    var laryChkBxs = loTbl.getElementsByTagName("INPUT");
    var cbLength = laryChkBxs.length;
    for (var j=0; j<cbLength; j++)
    {
      if (laryChkBxs[j].type == "checkbox")
      {
        if (laryChkBxs[j] == cbObj)
        {
          if (gPrefFlag)
          {
            laryChkBxs[j].checked = false;
            break;
          }
        }
        else
          laryChkBxs[j].checked = false;
      }
    }
  }

  // glsd451 Changes - Adding new controls as per andrew's requirement(Set Default Values for DropDown PrefMethodUpd)

        function SetPrefMethodUpd(dropdownlist) {
            var IsRepairEmailCell = "<%= IsEmailCell %>";
            IsRepairEmailCell = IsRepairEmailCell.replace("<config>", "");
            IsRepairEmailCell = IsRepairEmailCell.replace("</config>", "");
            IsRepairEmailCell = IsRepairEmailCell.toLowerCase();
            document.getElementById("txtConfigRepairReferral").value = IsRepairEmailCell;

            if ((document.getElementById("txtAssignmentTypeID").value == 4 || document.getElementById("txtAssignmentTypeID").value == 16) && IsRepairEmailCell == "true") {
                var Pref = document.getElementById(dropdownlist);
                for (i = 0; i < Pref.length; i++) {
                    if (Pref.value == "") {
                        if (Pref.options[i].value == "NU")
                            Pref.selectedIndex = i;
                    }
                }
            }
        }

        function CallRequired(UpdStatus) {
            document.getElementById("lblEmailReqiured").style.display = 'none';
            document.getElementById("lblCellRequired").style.display = 'none';
            document.getElementById("lblCellCarrierRequired").style.display = 'none';

            if (UpdStatus.value == "EM") {
                document.getElementById("lblEmail").style.display = 'none';
                document.getElementById("lblEmailReqiured").style.display = 'block';
                document.getElementById("lblCell").style.display = 'block';
                document.getElementById("lblCellCarrier").style.display = 'block';
            }
            else if (UpdStatus.value == "CE") {
                document.getElementById("lblCell").style.display = 'none';
                document.getElementById("lblCellCarrier").style.display = 'none';
                document.getElementById("lblCellRequired").style.display = 'block';
                document.getElementById("lblCellCarrierRequired").style.display = 'block';
                document.getElementById("lblEmail").style.display = 'block';

            }
            else if (UpdStatus.value == "NU") {
                document.getElementById("lblEmail").style.display = 'block';
                document.getElementById("lblCell").style.display = 'block';
                document.getElementById("lblCellCarrier").style.display = 'block';
            }
        }
        function UpdatesRequired() {
            var Updates;
            var IsRepairEmailCell = "<%= IsEmailCell %>";
            IsRepairEmailCell = IsRepairEmailCell.replace("<config>", "");
            IsRepairEmailCell = IsRepairEmailCell.replace("</config>", "");
            IsRepairEmailCell = IsRepairEmailCell.toLowerCase();
            Updates = document.getElementById("txtPrefMethodUpd");

            if (IsRepairEmailCell == "true") {
                if (Updates.value == "EM") {
                    document.getElementById("lblEmail").style.display = 'none';
                    document.getElementById("lblEmailReqiured").style.display = 'block';
                    document.getElementById("lblCell").style.display = 'block';
                    document.getElementById("lblCellCarrier").style.display = 'block';
                }
                else if (Updates.value == "CE") {
                    document.getElementById("lblCell").style.display = 'none';
                    document.getElementById("lblCellCarrier").style.display = 'none';
                    document.getElementById("lblCellRequired").style.display = 'block';
                    document.getElementById("lblCellCarrierRequired").style.display = 'block';
                    document.getElementById("lblEmail").style.display = 'block';

                }
                else if (Updates.value == "NU") {
                    document.getElementById("lblEmail").style.display = 'block';
                    document.getElementById("lblCell").style.display = 'block';
                    document.getElementById("lblCellCarrier").style.display = 'block';
                }
            }

        }
  
    </script>
</head>
<body onload="resizeWaIframe(0); pageInit(); parent.hideMsg(); setRental(); SetPrefMethodUpd('txtPrefMethodUpd'); UpdatesRequired();">
    <div id="divContainer">
        <table id="tblClaimInfoBlock" width="600" border="0" cellpadding="0" cellspacing="0"
            style="border: 1px solid #000099; border-collapse: collapse;">
            <tr>
                <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                        border-bottom: 1px solid #000099;">
                        <tr bgcolor="#E5E5F5">
                            <td style="color: #000099; font-size: 10pt; font-weight: bold; height: 24px;">
                                &nbsp;Claim Information
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" style="padding: 0px 10px 10px 10px;">
                    <table name="ClaimInfoTable" id="ClaimInfoTable" border="0" cellpadding="0" cellspacing="0"
                        style="border-collapse: collapse;">
                        <colgroup>
                            <col width="205px" />
                            <col width="30px" />
                            <col width="195px" />
                            <col width="30px" />
                            <col width="120px" />
                            <col width="10px" />
                        </colgroup>
                        <tr>
                            <td nowrap class="legendSumm">
                                Claim Assigned by:
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap class="legendSumm">
                                Relation to Insured:
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                        </tr>
                        <tr style="height: 18px">
                            <td nowrap id="txtCallerName" class="inputView">
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap id="txtCallerRelationToInsuredIDDescription" class="inputView">
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td nowrap class="legendSumm">
                                Claim Number:
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap class="legendSumm">
                                Loss Date:
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap class="legendSumm">
                                Loss State:
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                        </tr>
                        <tr style="height: 18px">
                            <td nowrap id="txtCoverageClaimNumber" class="inputView">
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap id="txtLossDate" class="inputView">
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap id="txtLossAddressState" class="inputView">
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td nowrap colspan="6" class="legendSumm">
                                Description of Loss:
                            </td>
                        </tr>
                        <tr>
                            <td nowrap colspan="5">
                <textarea name="LossDescription" id="txtLossDescription" wrap="physical" class="inputView" readonly="true" style="width:100%; height:50px;"></textarea>
              </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td nowrap class="legendSumm">
                                Insured Name:
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap colspan="3" class="legendSumm">
                                Insured Business Name:
                            </td>
                        </tr>
                        <tr style="height: 18px">
                            <td nowrap id="txtInsuredName" class="inputView">
                            </td>
                            <td nowrap>
                                &nbsp;
                            </td>
                            <td nowrap colspan="3" id="txtInsuredBusinessName" class="inputView">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <form name="frmVehicleInfo">
        <input value="" type="hidden" name="AssignmentTypeID" id="txtAssignmentTypeID">
        <input value="" type="hidden" name="CoverageProfileCD" id="txtCoverageProfileCD">
        <input value="No" type="hidden" name="RentalAuthorized" id="txtRentalAuthorized">
        <input value="" type="hidden" name="ConfigRepairReferral" id="txtConfigRepairReferral" />
        <!-- input for custom scripting -->
        <input type="hidden" name="SourceApplicationPassThruData" id="txtSourceApplicationPassThruData"
            inputtag="SourceApplicationPassThruData" value="">
        <table width="600" border="0" cellpadding="10" cellspacing="0" style="border: 1px solid #000099;
            border-collapse: collapse">
            <tr>
                <td nowrap valign="top">
                    <div style="padding: 0px 0px 5px 10px;">
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                            table-layout: fixed;">
                            <colgroup>
                                <col width="100px" />
                                <col width="220px" />
                                <col width="130px" />
                                <col width="120px" />
                            </colgroup>
                            <tr>
                                <td nowrap class="legend">
                                    Party: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td nowrap class="legend" width="250">
                                    Coverage: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td nowrap class="legend">
                                    &nbsp;&nbsp;&nbsp;Deductible: <span id="txtDeductibleAmtReq" style="font-size: 11pt;
                                        visibility: hidden;">*</span>
                                </td>
                                <td nowrap class="legend">
                                    &nbsp;&nbsp;&nbsp;Limit: <span id="txtLimitAmtReq" style="font-size: 11pt; visibility: hidden;">
                                        *</span>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                    <select name="ExposureCD" id="txtExposureCD" onchange="updParty(this.options[this.selectedIndex].value)"
                                        required="true">
                                        <option value=""></option>
                                        <%if li1stPartyCount = 0 then%>
                                        <option value="1">1st Party</option>
                                        <%end if%>
                                        <option value="3">3rd Party</option>
                                    </select>
                                </td>
                                <td nowrap>
                                    <select name="CoverageProfileUiCD" id="txtCoverageProfileUiCD" onchange="updCovReqFlds(this.options[this.selectedIndex].value)"
                                        required="true">
                                        <option value="" selected></option>
                                        <%
  lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & server.urlencode(liInsuranceCompanyID) & """/>"

  Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaCoverageTypes.xsl", "", true)
                                        %>
                                    </select>
                                    <input type="hidden" name="ClientCoverageTypeUiID" id="txtClientCoverageTypeUiID" />
                                    <input type="hidden" name="ClientCoverageTypeDesc" id="txtClientCoverageTypeDesc" />
                                </td>
                                <td nowrap>
                                    $<input name="DeductibleAmt" id="txtDeductibleAmt" type="text" onblur="FormatCurrencyObj(this)"
                                        onkeypress="return numbersOnly(this,event,1)" value="" size="12" maxlength="6"
                                        required="false">
                                </td>
                                <td nowrap>
                                    $<input name="LimitAmt" id="txtLimitAmt" type="text" onblur="FormatCurrencyObj(this)"
                                        onkeypress="return numbersOnly(this,event,1)" value="" size="12" maxlength="6"
                                        required="false">
                                </td>
                            </tr>
                            <!- 13Jan2012 - TVD - Elephant Changes (Begin)-->
                        </table>
                        <br />
                        <% if liInsuranceCompanyID = 374 then 'Elephant Only %>
                        <br />
                        <div id="divAdditionalInfo" style="width: 590px; border: 1px solid #C0C0C0; padding: 0px 0px 5px 10px;">
                            <div style="position: relative; top: -10px; width: 95px; font-size: 9pt; font-weight: bold;
                                background-color: #E7E7F7; color: #000099; border: 1px solid #CCCCCC;">
                                &nbsp;&nbsp;Additional Info
                            </div>
                            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                                table-layout: fixed;">
                                <tr>
                                    <td class="legendnopad">
                                        <input type="radio" id="rbTypeAuthorized" name="AuthType" value="AuthorizedRepair"
                                            onclick="return setAdditionalInfo(this)">Authorized to Proceed with Repairs
                                    </td>
                                </tr>
                                <tr>
                                    <td class="legendnopad">
                                        <input type="radio" id="rbTypeEstimate" name="AuthType" value="EstimateOnly" onclick="return setAdditionalInfo(this)">Secure
                                        Estimate Only
                                        <input type="hidden" id="txtSourceApplicationPassthruDataVeh" name="txtSourceApplicationPassthruDataVeh" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <% else %>
                        <input type="hidden" id="txtSourceApplicationPassthruDataVeh" name="txtSourceApplicationPassthruDataVeh"
                            value="" />
                        <% end if %>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                            table-layout: fixed;">
                            <!- 13Jan2012 - TVD - Elephant Changes (End) -->
                            <% If liShowPromiseToPay = 1 Then %>
                            <tr>
                                <td height="10" colspan="4">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="background-color: #F8F8F8; border: 1px solid #C0C0C0; font-weight: bold;
                                    width: 100%; padding-left: 4px; padding-bottom: 4px;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td nowrap class="legendnopad" colspan="2">
                                                Promise to Pay: <span style="font-size: 11pt;">*</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" nowrap>
                                                <input type="checkbox" name="PromiseToPayFlag" id="txtPromiseToPayFlag" value=""
                                                    required="true">
                                                <span style="font-weight: bold;">&nbsp;I AGREE&nbsp;&nbsp;&nbsp;</span>
                                            </td>
                                            <td>
                                                <img border="0" src="images/spacer.gif" width="1" height="5"><br>
                                                <span style="color: #000000; font-weight: normal; font-size: 8pt">
                                                    <%= lsPromiseToPayStatement %>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <% End If %>
                            <%
  If liCurrAssignmentReserveLineType = 1 Then
    ' Reset pointer to before first node
    ElemList.Reset
    Set oNode = ElemList.nextNode
                            %>
                            <tr>
                                <td height="10" colspan="4">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" nowrap style="background-color: #F8F8F8; border: 1px solid #C0C0C0;
                                    font-weight: bold; width: 100%; padding-left: 4px; padding-bottom: 4px;">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <%
    For each oNode in ElemList
  
      lsAssignmentTypeIDList = oNode.getAttribute("AssignmentTypeIDList")
      laAssignmentTypeID = split(lsAssignmentTypeIDList, "|")
  
      liStop = ubound(laAssignmentTypeID)
      For liCounter = 0 to liStop
  
        If liCurrentAssignmentId = laAssignmentTypeID(liCounter) _
            and (oNode.getAttribute("InputResponseName") = "LineType" or oNode.getAttribute("InputResponseName") = "Reserve") Then
        
          lsFldName = oNode.getAttribute("InputResponseName")
          lsAlertText = oNode.getAttribute("ScriptText")
          lsInputTag = oNode.getAttribute("InputTag")
          lsInputRequiredFlag = oNode.getAttribute("InputRequiredFlag")

                                            %>
                                            <td nowrap>
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="legendnopad">
                                                            <%= lsAlertText %>&nbsp;<% If lsInputRequiredFlag = 1 Then %>*
                                                            <% End If %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" nowrap="">
                                                            <input type="input" name="<%= lsFldName %>" id="txt<%= lsFldName %>" inputtag="<%= lsInputTag %>"
                                                                onkeypress="return numbersOnly(this,event,1)" value="" size="2" maxlength="2"
                                                                opt="no">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%
        End If
      
      Next
    Next
                                            %>
                                            <td width="50%">
                                            </td>
                                        </tr>
                                    </table>
                            </tr>
                            <%
  End If
                            %>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td nowrap valign="top">
                    <div style="width: 590px; border: 1px solid #C0C0C0; padding: 0px 0px 5px 10px;">
                        <div style="position: relative; top: -10px; width: 100px; font-size: 9pt; font-weight: bold;
                            background-color: #E7E7F7; color: #000099; border: 1px solid #CCCCCC;">
                            &nbsp;&nbsp;Vehicle Owner
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                            table-layout: fixed;">
                            <colgroup>
                                <col width="125px" />
                                <col width="240px" />
                                <col width="200px" />
                            </colgroup>
                            <tr>
                                <td nowrap class="legendnopad">
                                    First Name: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td nowrap class="legendnopad">
                                    Last Name: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td nowrap class="legendnopad">
                                    Phone Number: <span style="color: #000000; font-weight: normal; font-size: 7pt">(numbers
                                        only)</span>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                    <input name="OwnerNameFirst" id="txtOwnerNameFirst" type="text" onblur="ldTrim(this)"
                                        size="15" maxlength="50" required="true">
                                </td>
                                <td nowrap>
                                    <input name="OwnerNameLast" id="txtOwnerNameLast" type="text" onblur="ldTrim(this); updContactName()"
                                        size="23" maxlength="50" required="true">
                                </td>
                                <td nowrap>
                                    <input name="OwnerPhoneAC" id="txtOwnerPhoneAC" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3">
                                    <input name="OwnerPhoneEN" id="txtOwnerPhoneEN" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3">
                                    <input name="OwnerPhoneUN" id="txtOwnerPhoneUN" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 4, event);" onblur="updContactPhone()" size="4"
                                        maxlength="4">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" nowrap class="legend">
                                    Business Name: <span style="font-size: 11pt;">*</span> <span style="color: #000000;
                                        font-weight: normal; font-size: 8pt">(If Insured/Claimant is a business)</span>
                                </td>
                                <td nowrap>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" nowrap>
                                    <input name="OwnerBusinessName" id="txtOwnerBusinessName" type="text" onblur="ldTrim(this)"
                                        size="40" maxlength="50">
                                </td>
                                <td nowrap>
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td nowrap valign="top">
                    <div style="margin-top: 18px; border: 1px solid #C0C0C0; padding: 0px 0px 5px 5px;">
                        <div style="position: relative; top: -10px; width: 110px; font-size: 9pt; font-weight: bold;
                            background-color: #E7E7F7; color: #000099; border: 1px solid #CCCCCC;">
                            &nbsp;&nbsp;Vehicle Contact
                        </div>
                        <table id="tblContactCb" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                            table-layout: fixed;">
                            <colgroup>
                                <col width="125px" />
                                <col width="160px" />
                                <col width="80px" />
                                <col width="200px" />
                            </colgroup>
                            <tr>
                                <td colspan="2">
                                </td>
                                <td valign="bottom" align="center" class="legendnopad">
                                    Preferred
                                </td>
                                <td valign="bottom" nowrap class="legendnopad">
                                    Phone Number: <span style="font-size: 11pt;">*</span> <span style="color: #000000;
                                        font-weight: normal; font-size: 8pt">(One is required)</span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="bottom" nowrap class="legendnopad">
                                    First Name: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td valign="bottom" nowrap class="legendnopad">
                                    Last Name: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td valign="bottom" align="center" class="legendnopad">
                                    Contact
                                </td>
                                <td valign="bottom" nowrap class="legendnopad">
                                    Day: <span style="color: #000000; font-weight: normal; font-size: 7pt">(numbers only)</span>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                    <input name="ContactNameFirst" id="txtContactNameFirst" type="text" onblur="ldTrim(this)"
                                        size="15" maxlength="50" required="true">
                                </td>
                                <td nowrap>
                                    <input name="ContactNameLast" id="txtContactNameLast" type="text" onblur="ldTrim(this)"
                                        size="23" maxlength="50" required="true">
                                </td>
                                <td align="center" nowrap>
                                    <input name="cb_ContactBestPhoneCD" type="checkbox" onfocus="javascript:gPrefFlag=this.checked"
                                        onclick="setContactCB(this)" value="D">
                                </td>
                                <td nowrap>
                                    <input name="ContactPhoneAC" id="txtContactPhoneAC" type="text" value="" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3" required="true">
                                    <input name="ContactPhoneEN" id="txtContactPhoneEN" type="text" value="" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3" required="true">
                                    <input name="ContactPhoneUN" id="txtContactPhoneUN" type="text" value="" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 4, event);" size="4" maxlength="4" required="true">
                                </td>
                            </tr>
                             <!-- glsd451 Changes - Adding new controls as per andrew's requirement(CellPhoneCarrier,ContactBestPhoneCD,Cell)-->
                            <tr>
                                <td class="legendnopad" colspan="2">
                                    <%if (liCurrentAssignmentId = 4 or liCurrentAssignmentId = 16) then%>
                                    <%if IsEmailCell = "true" then%>
                                    <label id="lblEmail">
                                        Email:</label>
                                    <label id="lblEmailReqiured" style="display: none;">
                                        Email: *</label>
                                    <% End if %>
                                    <% End If  %>
                                </td>
                                <td>
                                </td>
                                <td nowrap class="legendnopad">
                                    Night: <span style="color: #000000; font-weight: normal; font-size: 7pt">(numbers only)</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <%if (liCurrentAssignmentId = 4 or liCurrentAssignmentId = 16) then%>
                                    <%if IsEmailCell = "true" then%>
                                    <input name="ContactEmailAddress" id="txtContactEmailAddress" type="text" value=""
                                        onblur="ldTrim(this)" />
                                    <% End If  %>
                                    <% End if %>
                                </td>
                                <td align="center" nowrap>
                                    <input name="cb_ContactBestPhoneCD" type="checkbox" onfocus="javascript:gPrefFlag=this.checked"
                                        onclick="setContactCB(this)" value="N">
                                </td>
                                <td nowrap>
                                    <input name="ContactNightPhoneAC" id="txtContactNightPhoneAC" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3">
                                    <input name="ContactNightPhoneEN" id="txtContactNightPhoneEN" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3">
                                    <input name="ContactNightPhoneUN" id="txtContactNightPhoneUN" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 4, event);" size="4" maxlength="4">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="legendnopad">
                                    <%if (liCurrentAssignmentId = 4 or liCurrentAssignmentId = 16) then%>
                                    <%if IsEmailCell = "true" then%>
                                    Preferred method of status updates:
                                    <% End If  %>
                                    <% End If  %>
                                </td>
                                <td>
                                </td>
                                <td nowrap class="legendnopad">
                                    Alternate: <span style="color: #000000; font-weight: normal; font-size: 7pt">(numbers
                                        only)</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <%if (liCurrentAssignmentId = 4 or liCurrentAssignmentId = 16) then%>
                                    <%if IsEmailCell = "true" then%>
                                    <select name="PrefMethodUpd" id="txtPrefMethodUpd" onchange="CallRequired(this);">
                                        <option value="NU" selected="true">No Updates</option>
                                        <option value="EM">E-mail</option>
                                        <option value="CE">Cell</option>
                                    </select>
                                    <% End If  %>
                                    <% End If %>
                                </td>
                                <td align="center" nowrap>
                                    <input name="cb_ContactBestPhoneCD" type="checkbox" onfocus="javascript:gPrefFlag=this.checked"
                                        onclick="setContactCB(this)" value="A">
                                </td>
                                <td nowrap>
                                    <input name="ContactAltPhoneAC" id="txtContactAltPhoneAC" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3">
                                    <input name="ContactAltPhoneEN" id="txtContactAltPhoneEN" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3">
                                    <% if liCurrentAssignmentId = 16 then%>
                                    <input name="ContactAltPhoneUN" id="txtContactAltPhoneUN" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        size="4" maxlength="4">
                                    <%Else %>
                                    <input name="ContactAltPhoneUN" id="txtContactAltPhoneUN" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 4, event);" size="4" maxlength="4">
                                    <%End If %>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="legendnopad">
                                    <%if (liCurrentAssignmentId = 4 or liCurrentAssignmentId = 16) then%>
                                    <%if IsEmailCell = "true" then%>
                                    <label id="lblCellCarrier">
                                        Cell phone carrier: <span style="color: #000000; font-weight: normal; font-size: 7pt">
                                            (numbers only)</span>
                                    </label>
                                    <label id="lblCellCarrierRequired" style="display: none;">
                                        Cell phone carrier: * <span style="color: #000000; font-weight: normal; font-size: 7pt">
                                            (numbers only)</span>
                                    </label>
                                    <% End if %>
                                    <% End If %>
                                </td>
                                <td>
                                </td>
                                <td class="legendnopad">
                                    <label id="lblCell">
                                        Cell: <span style="color: #000000; font-weight: normal; font-size: 7pt">(numbers only)</span>
                                    </label>
                                    <label id="lblCellRequired" style="display: none;">
                                        Cell: * <span style="color: #000000; font-weight: normal; font-size: 7pt">(numbers only)</span>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <%if (liCurrentAssignmentId = 4 or liCurrentAssignmentId = 16) then%>
                                    <%if IsEmailCell = "true" then%>
                                    <select name="CellPhoneCarrier" id="txtCellPhoneCarrier">
                                        <option value=""></option>
                                        <option value="AT">AT&T</option>
                                        <option value="SP">Sprint</option>
                                        <option value="TM">T-Mobile</option>
                                        <option value="VW">Verizon Wireless</option>
                                        <option value="OT">Other</option>
                                    </select>
                                    <% End If  %>
                                    <% End If  %>
                                </td>
                                <td align="center">
                                    <input name="cb_ContactBestPhoneCD" type="checkbox" onfocus="javascript:gPrefFlag=this.checked"
                                        onclick="setContactCB(this)" value="C">
                                </td>
                                <td>
                                    <input name="CellAreaCode" id="txtCellAreaCode" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3">
                                    <input name="CellExchangeNumber" id="txtCellExchangeNumber" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3">
                                    <% if liCurrentAssignmentId = 16 then%>
                                    <input name="CellUnitNumber" id="txtCellUnitNumber" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        size="4" maxlength="4">
                                    <%Else %>
                                    <input name="CellUnitNumber" id="txtCellUnitNumber" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        onkeyup="return autoTab(this, 4, event);" size="4" maxlength="4">
                                    <%End If %>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr id="trRental">
                <td nowrap valign="top">
                    <div style="width: 590px; border: 1px solid #C0C0C0; padding: 0px 0px 5px 10px;">
                            <div>
                                <div style="position: relative; top: -10px; width: 75px; font-size: 9pt; font-weight: bold; background-color: #E7E7F7; color: #000099; border: 1px solid #CCCCCC;">
                            &nbsp;&nbsp;Rental &nbsp;<input name="cb_Rental" id="txtcb_Rental" type="checkbox"
                                onclick="updRentalFldsState()" title="Click if Rental is authorized">&nbsp;&nbsp;
                        </div>
                        <table id="tblRental" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse table-layout:fixed;">
                            <colgroup>
                                <col width="125px" />
                                <col width="180px" />
                                <col width="260px" />
                            </colgroup>
                            <tr>
                                <td nowrap class="legendnopad">
                                    Rental Days:
                                </td>
                                <td nowrap class="legendnopad">
                                    &nbsp;&nbsp;&nbsp;Rental Daily Maximum:
                                </td>
                                <td nowrap class="legendnopad">
                                    &nbsp;&nbsp;&nbsp;Rental Maximum:
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                    <input name="RentalDays" id="txtRentalDays" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        value="" size="4" maxlength="3">
                                </td>
                                <td nowrap>
                                    $<input name="RentalDayAmount" id="txtRentalDayAmount" type="text" onblur="FormatCurrencyObj(this)"
                                        onkeypress="return numbersOnly(this,event,1)" value="" size="12" maxlength="6">
                                </td>
                                <td nowrap>
                                    $<input name="RentalMaxAmount" id="txtRentalMaxAmount" type="text" onblur="FormatCurrencyObj(this)"
                                        onkeypress="return numbersOnly(this,event,1)" value="" size="12" maxlength="6">
                                </td>
                            </tr>
                            <tr>
                                <td nowrap class="legend">
                                    Days Authorized:
                                </td>
                                <td colspan="2" nowrap class="legend">
                                    Instructions: <span id="commFldMsg" style="color: #000000; font-weight: normal; font-size: 7pt">
                                        (max.:250 characters)</span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" nowrap>
                                    <input name="RentalDaysAuthorized" id="txtRentalDaysAuthorized" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        value="" size="4" maxlength="3">
                                </td>
                                <td colspan="2" nowrap>
                                    <textarea name="RentalInstructions" id="txtRentalInstructions" wrap="physical" style="width: 100%;
                                        height: 40px;" onkeydown="CheckInputLength(this, 250)"></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                            <br />
                            <div id="additionalRentalBlock" style="display:none;width:560px; border: 1px solid #C0C0C0; padding: 0px 0px 5px 10px;">
                                <div style="position: relative; top: -10px; width: 170px; font-size: 9pt; font-weight: bold; background-color: #E7E7F7; color: #000099; border: 1px solid #CCCCCC;">
                                    &nbsp;&nbsp;Additional Rental Info. &nbsp;<input name="cb_Rental_additional" id="txtcb_Rental_additional" type="checkbox"
                                        onclick="updAdditionalRentalFldsState()" title="Click if Additional Rental is authorized">&nbsp;&nbsp;
                                </div>
                                <table id="tblRentalAdditional" border="0" cellpadding="0" cellspacing="0" style="display: none; border-collapse: collapse table-layout:fixed; width: 100%">
   <tr>
                                        <td nowrap class="legendnopad">Rental Vendor: *
                                        </td>
                                        <td nowrap class="legendnopad">Res/Conf: *
                                        </td>
                                        <td nowrap class="legendnopad">Vehicle Class: *
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap>
                                            <select class="txtlegendnopad" name="RentalVendor" id="txtRentalVendor" required="true">
                                                <option value=""></option>
                                                <option value="Enterprise">Enterprise</option>
                                                <option value="Hertz">Hertz</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            <input value="" type="hidden" name="RentalVehicleClass" id="hidRentalVendor">
                                        </td>
                                        <td nowrap>
                                            <input class="txtlegendnopad" name="RentalResConf" id="txtRentalResConf" type="text" value="" maxlength="15" required="true" />
                                        </td>
                                        <td nowrap>
                                            <select class="txtlegendnopad" name="RentalVehicleClass" id="txtRentalVehicleClass" required="true">
                                                <option value=""></option>
                                                <option value="Compact">Compact</option>
                                                <option value="Economy">Economy</option>
                                                <option value="Full Size">Full Size</option>
                                                <option value="Intermediate">Intermediate</option>
                                                <option value="Large Pickup">Large Pickup</option>
                                                <option value="Luxury">Luxury</option>
                                                <option value="Midsize SUV">Midsize SUV</option>
                                                <option value="Mini Van">Mini Van</option>
                                                <option value="Premium">Premium</option>
                                                <option value="Premium SUV">Premium SUV</option>
                                                <option value="Standard">Standard</option>
                                                <option value="SUV">SUV</option>
                                            </select>
                                            <input value="" type="hidden" name="RentalVehicleClass" id="hidRentalVehicleClass">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td nowrap class="legendnopad">Rate Type: *
                                        </td>
                                        <td nowrap class="legendnopad">Rate: *
                                        </td>
                                        <td nowrap class="legendnopad">Auth Pickup: *
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap>
                                            <select class="txtlegendnopad" name="RentalRateType" id="txtRentalRateType" required="true">
                                                <option value=""></option>
                                                <option value="Daily">Daily</option>
                                                <option value="Weekly">Weekly</option>
                                            </select>
                                            <input value="" type="hidden" name="RentalRateType" id="hidRentalRateType">
                                        </td>
                                        <td nowrap>
                                            <input class="txtlegendnopad" name="RentalRate" id="txtRentalRate" type="text" value="" onblur="FormatCurrencyObj(this)"
                                                onkeypress="return numbersOnly(this,event,1)" required="true" />
                                        </td>
                                        <td nowrap>
                                            <input class="txtlegendnopad" name="RentalAuthPickup" id="txtRentalAuthPickup" type="text" value="" maxlength="10" required="true" />
                                            <a href="javascript:void(0)" onclick="showCalIframe(this, 'txtRentalAuthPickup',true)" id="calCall" tabindex="-1">
                                                <img src="images/calendar.gif" alt="" width="24" height="17" border="0" title="Click to select date from calendar">
                                            </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td nowrap class="legendnopad">Rental Tax Rate: 
                                        </td>
                                        <td nowrap class="legendnopad">Rental Phone: *
                                        </td>
                                        <td nowrap class="legendnopad">Rental Address: 
                                        </td>

                                    </tr>
                                    <tr>
                                        <td nowrap><input name="RentalTaxRate" id="txtRentalTaxRate" type="text" value="" onblur="FormatCurrencyObj(this)"
                                            onkeypress="return numbersOnly(this,event,1)" value="" size="12" maxlength="6" />
                                        </td>
                                        <td nowrap>
                                            <input name="RentalPhoneAC" id="txtRentalPhoneAC" type="text" value="" onkeypress="return numbersOnly(this,event,0)"
                                                onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3" required="true">
                                            <input name="RentalPhoneEN" id="txtRentalPhoneEN" type="text" value="" onkeypress="return numbersOnly(this,event,0)"
                                                onkeyup="return autoTab(this, 3, event);" size="3" maxlength="3" required="true">
                                            <input name="RentalPhoneUN" id="txtRentalPhoneUN" type="text" value="" onkeypress="return numbersOnly(this,event,0)"
                                                onkeyup="return autoTab(this, 4, event);" size="4" maxlength="4" required="true">
                                        </td>
                                        <td nowrap>
                                            <input class="txtlegendnopad" name="RentalAddress" id="txtRentalAddress" type="text" value="" maxlength="100" />
                                        </td>

                                    </tr>

                                    <tr>
                                        <td nowrap class="legendnopad">Rental City: 
                                        </td>
                                        <td nowrap class="legendnopad">Rental State: 
                                        </td>
                                        <td nowrap class="legendnopad">Rental Zip: 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap>
                                            <input class="txtlegendnopad" name="RentalCity" id="txtRentalCity" type="text" value="" required="true" maxlength="100" />
                                        </td>
                                        <td nowrap>

                                            <%
                   lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & server.urlencode(liInsuranceCompanyID) & """/>"
                
                   Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaStateList.xsl", "ObjName=RentalState" & lcDelimiter & "tabIndexNo=44", true)
                                            %>
 <input value="" type="hidden" name="RentalState" id="hidRentalState">
                                        </td>

                                        <td nowrap>
                                            <input class="txtlegendnopad" name="RentalZip" id="txtRentalZip" type="text" value="" maxlength="5" required="true" onkeypress="return numbersOnly(this,event,1)" />
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>


                </td>
            </tr>
            <tr>
                <td nowrap valign="top">
                    <div style="width: 590px; border: 1px solid #C0C0C0; padding: 0px 0px 5px 10px;">
                        <div style="position: relative; top: -10px; width: 60px; font-size: 9pt; font-weight: bold;
                            background-color: #E7E7F7; color: #000099; border: 1px solid #CCCCCC;">
                            &nbsp;&nbsp;Vehicle
                        </div>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                            table-layout: fixed;">
                            <colgroup>
                                <col width="90px" />
                                <col width="110px" />
                                <col width="110px" />
                                <col width="85px" />
                                <col width="170px" />
                            </colgroup>
                            <tr>
                                <td nowrap class="legendnopad">
                                    Year: *<span style="color: #000000; font-weight: normal; font-size: 7pt">(YYYY)</span>
                                </td>
                                <td nowrap class="legendnopad">
                                    Make: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td nowrap class="legendnopad">
                                    Model: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td nowrap class="legendnopad">
                                    Drivable: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td nowrap class="legend">
                                    Odometer:
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                    <input name="VehicleYear" id="txtVehicleYear" type="text" onkeypress="return numbersOnly(this,event,0)"
                                        value="" size="4" maxlength="4" required="true">
                                </td>
                                <td nowrap>
                                    <input name="Make" id="txtMake" type="text" value="" onblur="ldTrim(this)" size="10"
                                        maxlength="15" required="true">
                                </td>
                                <td nowrap>
                                    <input name="Model" id="txtModel" type="text" value="" onblur="ldTrim(this)" size="10"
                                        maxlength="15" required="true">
                                </td>
                                <td nowrap>
                                    <select name="Drivable" id="txtDrivable" required="true" onchange="return setDisableEstimate(this)">
                                        <option value="" selected></option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                                <td>
                                    <input name="Mileage" id="txtMileage" type="text" size="8" maxlength="7" onkeypress="return numbersOnly(this,event,1)">
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                            table-layout: fixed;">
                            <colgroup>
                                <col width="125px" />
                                <col width="440px" />
                            </colgroup>
                            <tr>
                                <td nowrap class="legend">
                                    License Plate:
                                    <%if liCurrentAssignmentId <> 16 then%>*<%end if %>
                                </td>
                                <td nowrap class="legend">
                                    VIN:
                                    <%if liCurrentAssignmentId <> 16 then%>* <span style="color: #000000; font-weight: normal;
                                        font-size: 8pt">(License Plate or at least the last 6 digits of the VIN is required)</span><%end if %>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                    <input name="LicensePlateNumber" id="txtLicensePlateNumber" type="text" value=""
                                        onblur="ldTrim(this)" size="10" maxlength="10">
                                </td>
                                <td nowrap>
                                    <input name="VIN" id="txtVIN" type="text" value="" onblur="ldTrim(this)" size="20"
                                        maxlength="17">
                                </td>
                            </tr>
                        </table>
                        <table id="tblDamageCb" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                            table-layout: fixed;">
                            <colgroup>
                                <col width="240px" />
                                <col width="230px" />
                                <col width="10px" />
                                <col width="94px" />
                            </colgroup>
                            <tr>
                                <td nowrap class="legend">
                                    Primary Damages: <span style="font-size: 11pt;">*</span>
                                </td>
                                <td nowrap class="legend">
                                    Secondary Damages:
                                </td>
                                <td nowrap>
                                    &nbsp;
                                </td>
                                <td nowrap>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #000000; font-weight: normal; font-size: 8pt">
                                    (Only one primary point is allowed)
                                </td>
                                <td style="color: #000000; font-weight: normal; font-size: 8pt">
                                    (Multiple secondary points are allowed)
                                </td>
                                <td nowrap>
                                    &nbsp;
                                </td>
                                <td nowrap>
                                    &nbsp;
                                </td>
                            </tr>
                            <%
  lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & server.urlencode(liInsuranceCompanyID) & """/>"

  Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaVehImpactPoints.xsl", "", true)
                            %>
                        </table>
                        <br>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;
                            table-layout: fixed;">
                            <colgroup>
                                <col width="570px" />
                            </colgroup>
                            <tr>
                                <td nowrap class="legendnopad">
                                    Special Intructions/Additional Damage Information/Comments to Shop: <span id="commFldMsgRemarks"
                                        style="color: #000000; font-weight: normal; font-size: 7pt">(max.:250 characters)</span>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                    <textarea name="ShopRemarks" id="txtShopRemarks" wrap="physical" style="width: 100%;
                                        height: 52px;" onkeydown="CheckInputLength(this, 250, 'commFldMsgRemarks')"></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <p class="bodyBlue">
            All fields marked with an asterisk (*) are required.</p>
        <table border="0" cellpadding="0" cellspacing="0" style="align: right;">
            <colgroup>
                <col width="400px" />
                <col width="100px" />
                <col width="90px" />
            </colgroup>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td valign="top" nowrap>
                    <a href="javascript:goCancel()" <% If lsPrevPage = "ClaimSummary" Then %> title="Cancel and Return to Summary"
                        onmouseover="window.status='Cancel and Return to Summary'; return true" onmouseout="window.status=''; return true"
                        <% Else %> title="Return to My Claims Desktop" onmouseover="window.status='Return to My Claims Desktop'; return true"
                        onmouseout="window.status=''; return true" <% End If %>>
                        <img src="images/btn_cancel.gif" border="0" width="83" height="31">
                    </a>
                </td>
                <td valign="top" nowrap>
                    <a href="javascript:updVehicleData(0, '<%= liVehNum %>')" <% If lsPrevPage = "ClaimSummary" Then %>
                        title="Save and Return to Summary Page" onmouseover="window.status='Save and Return to Summary Page'; return true"
                        <% Else %> title="To Summary Page" onmouseover="window.status='To Summary Page'; return true"
                        <% End If %> onmouseout="window.status=''; return true">
                        <% If lsPrevPage = "ClaimSummary" Then %>
                        <img src="images/btn_ok.gif" border="0" width="52" height="31">
                        <% Else %>
                        <img src="images/btn_next.gif" border="0" width="83" height="31">
                        <% End If %>
                    </a>
                </td>
            </tr>
        </table>
        </form>
    </div>
 <iframe id="CalFrame" name="CalFrame" src="blank.asp" style="border: 2px solid #404040; left: -500px; top: 0px; display: none; position: absolute; width: 170; height: 160; z-index: 100; filter: progid:DXImageTransform.Microsoft.Shadow(color='#666666', Direction=135, Strength=3);" marginheight="0" marginwidth="0" noresize frameborder="0" scrolling="NO"></iframe>
</body>
</html>