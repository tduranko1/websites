<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!-- v1.5.0.0 -->

<html>
<head>
</head>
<body>
<%
  lsPageName = "rspUserDetailUpd.asp"
%>

<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incMail.asp"-->

<%
Function UnEscapeString(strIn)
  Dim strOut, i
  strOut = strIn
  For i = 1 To Len(strOut)
    If Mid(strOut, i, 1) = "%" Then
      If Mid(strOut, i + 1, 1) = "u" Then
        'Convert unicode character.
        strOut = Left(strOut, i - 1) _
                  & ChrW(CInt("&H" & Mid(strOut, i + 2, 4))) _
                  & Right(strOut, Len(strOut) - i - 5)

      Else
        'Convert standard byte character.
        strOut = Left(strOut, i - 1) _
                  & Chr(CInt("&H" & Mid(strOut, i + 1, 2))) _
                  & Right(strOut, Len(strOut) - i - 2)

      End If
    End If
  Next
  UnEscapeString = strOut
End Function

function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function

Function AddNewAwareUser(sUserName, sUserAdminEmail)

  Dim loAware, liADUserID, sPassword, lsRequestID, liReqResult, sBody, sSubject, lsErrCode, liAcceptRequest
  Dim sAPDSrv, sAPDEnv, lbUserHasAccess, sADGroupName, liRequest, liAddedToGroup, liRequestAccepted, lsErrorCode
  
  If Instr(1,strServerName, "DEV") > 0 Then	'Development
  		sAPDSrv = "http://apdlynxservices.pgw.local/apd"
      sAPDEnv = " (Development environment)"
  
  ElseIf Instr(1,strServerName, "STG") > 0 or Instr(1,strServerName, "TRN") > 0 Then		'Staging or Lynx Training server
  		sAPDSrv = "http://sapd.lynxservices.com/apd"
      sAPDEnv = " (Staging environment)"
  
  ElseIf Instr(1,strServerName, "PRD") > 0 Or (uCASE(Left(strServerName,3)) = "BUY") Then	'Production
  		sAPDSrv = "https://apd.lynxservices.com/apd"
      sAPDEnv = ""
  
  Else	'default
  		sAPDSrv = "https://apd.lynxservices.com/apd"
      sAPDEnv = ""
  
  End If

  sSubject = "ClaimPoint from LYNX Services Registration"

  'set loAware = Server.CreateObject("eTemGlobalReg.clsRegistration")
  Set loAware = Server.CreateObject("AWARE.clsAWARE")
  
  'check if the user exists in Aware
  liADUserID = loAware.GetUserIDFromEmail(sUserName)

  ' User does not exist in AWARE
  If liADUserID = "" Then
  
    'Create a new Password
    Randomize
    sPassword = "Lynx" & Int(90000 * Rnd + 10000)
    
    'Create a unique ID
    Randomize
    lsRequestID = "CP" & Int(90000 * Rnd + 10000)
    
    liReqResult  =	loAware.AddNewUser(liLynxAppID, sUserName, lsRequestID, "", "", "", "", sPassword, lsErrCode)

      If liReqResult = 0 Then 'Successfully added new user
      
        sBody = "Welcome to the ClaimPoint from LYNX Services website " & sAPDSrv & sAPDEnv & "." & vbcrlf & vbcrlf _
              & "Please keep this e-mail for future reference. It contains important information regarding your Login ID and password." & vbcrlf & vbcrlf _
              & "You are now registered to login to the ClaimPoint website." & vbcrlf _
              & "Login ID: " & sUserName  & vbcrlf _
              & "Password: " & sPassword  & vbcrlf & vbcrlf _
              & "Please remember that the password is case sensitive and must be entered exactly as shown above." & vbcrlf & vbcrlf _
              & "Thank you." & vbcrlf & vbcrlf _
              & "LYNX Services Team" & vbcrlf & vbcrlf _
              & "You have been given access to ClaimPoint by " & lsUserNameFirst & " " & lsUserNameLast & " (" & lsUserEmail & ")."

        'Sending an e-mail (From, To, Subject, Body)
        loAware.SendEmail sUserAdminEmail, sUserName, sSubject, sBody

        AddNewAwareUser = "created"
      
      ElseIf liReqResult = -2 Then
        liAcceptRequest = loAware.AcceptRequest(lsErrCode, lsErrCode, false, true, "", true, "")
        
        '1 is success
        If liAcceptRequest = 1 Then
          AddNewAwareUser = "created"

        Else
          AddNewAwareUser = "failed"

        End If
      
      ElseIf liReqResult = -1 Then

        AddNewAwareUser = "failed"
      
      End If

  'User exists in AWARE
  Else
    'check if the user has access to this application
    lbUserHasAccess = loAware.UserHasAccess(liLynxAppID, liADUserID)

    'Request access for the user
    If lbUserHasAccess = false Then
      liRequest = loAware.Request(sUserName, "", "", "", "", "", "", liLynxAppID, lsErrorCode)
      
      If lsErrorCode <> "" Then
        liRequestAccepted = loAware.AcceptRequest(lsErrorCode, lsErrorCode, true, true, "1", true)

      End If
      
    End If

    sADGroupName = "GOFWebLynxAPD"

    'Be sure the user is part of this application
    liAddedToGroup = loAware.AddUserToGroup(sUserName, sADGroupName)

    If lbUserHasAccess = false and liRequestAccepted <> 1 Then
      AddNewAwareUser = "failed"

    ElseIf lbUserHasAccess = false Then
      'Just to tell the user that he needs to use his existing password as he is already created 
      sBody = "Welcome to the ClaimPoint from LYNX Services website " & sAPDSrv & sAPDEnv & "." & vbcrlf & vbcrlf _
            & "You already have a Login account for this website. " & vbcrlf _
            & "Login ID: " & sUserName & vbcrlf & vbcrlf _
            & "Please use your current password to login to the website. If you don't recall your current password, " _
            & "you can have a new one sent to you by clicking the Forgot Password button from the Login screen." & vbcrlf & vbcrlf _
            & "Please remember that the password is case sensitive." & vbcrlf & vbcrlf _
            & "Thank you." & vbcrlf & vbcrlf _
            & "LYNX Services Team" & vbcrlf & vbcrlf _
            & "This e-mail was sent by " & lsUserNameFirst & " " & lsUserNameLast & " (" & lsUserEmail & ")."
  
      'Sending an e-mail
      loAware.SendEmail sUserAdminEmail, sUserName, sSubject, sBody

	  AddNewAwareUser = "exists"
	
	Else
      'User is all set
	  AddNewAwareUser = "success"

    End If

  End If

  Set loAware = Nothing

End Function


Dim lsStrXML, loAPDData, liRetVal, sMode, sRet, loRootNode, lsUserID, loAware
Dim sPassword, sBody, sUserName, sUserAdminEmail, lsAddAwareResult, docXML, lsEmailStrXML
Dim mexMonikerString 
         mexMonikerString = GetWebServiceURL()

Set docXML = Server.CreateObject("MSXML2.DOMDocument")
docXML.Async = False
  
lsEmailStrXML = "<Config Path='ClaimPoint/NewUserAdd/NotificationEmail'/>"
docXML.LoadXML GetData(lsEmailStrXML, "", "")
sUserAdminEmail = docXML.documentElement.text

Set docXML = Nothing

sMode = request("mode")

'sUserAdminEmail = lsUserEmail  'get Admin email from the current user

If request("UserID") = "-1" or sMode = "insert" Then
  lsStrXML =  "<Root Transaction=""1"" CarryResults=""1"">" + _
                  "<UserDetailInsert " + _
                      " OfficeID=""" + request("OfficeID") + """" + _
                      " SupervisorUserID=""""" + _
                      " AssignmentBeginDate=""" + """" + _
                      " AssignmentEndDate=""" + """" + _
                      " ClientUserID=""" + trim(request("ClientUserID")) + """" + _
                      " EmailAddress=""" + trim(request("EmailAddress")) + """" + _
                      " FaxAreaCode=""" + request("FaxAreaCode") + """" + _
                      " FaxExchangeNumber=""" + request("FaxExchangeNumber") + """" + _
                      " FaxExtensionNumber=""" + request("FaxExtensionNumber") + """" + _
                      " FaxUnitNumber=""" + request("FaxUnitNumber") + """" + _
                      " NameFirst=""" + trim(request("NameFirst")) + """" + _
                      " NameLast=""" + trim(request("NameLast")) + """" + _
                      " NameTitle=""""" + _
                      " PhoneAreaCode=""" + request("PhoneAreaCode") + """" + _
                      " PhoneExchangeNumber=""" + request("PhoneExchangeNumber") + """" + _
                      " PhoneExtensionNumber=""" + request("PhoneExtensionNumber") + """" + _
                      " PhoneUnitNumber=""" + request("PhoneUnitNumber") + """" + _
                      " SupervisorFlag=""0""" + _
                      " LicenseAssignStList=""""" + _
                      " RoleList=""" + request("RoleList") + """" + _
                      " ProfileList=""" + request("ProfileList") + """" + _
                      " PermissionList=""" + request("PermissionList") + """" + _
                      " SysLastUserID=""" + request("SysLastUserID") + """" + _
                      " />" + _
                  "<UserApplicationInsert " + _
                      " ApplicationID=""" + request("ApplicationID") + """" + _
                      " LogonId=""" + trim(request("EmailAddress")) + """" + _
                      " PasswordHint=""Password was reset to a random value.""" + _
                      " SysLastUserID=""" + request("SysLastUserID") + """" + _
                      "/>"

  If request("rbUserActive") = 1 Then
    lsStrXML = lsStrXML + "<UserSetStatus " + _
                            " ApplicationID=""" + request("ApplicationID") + """" + _
                            " SysLastUserID=""" + request("SysLastUserID") + """" + _
                            " />"

  End If

  lsStrXML = lsStrXML + "</Root>"

End If

If request("UserID") <> "-1" and sMode = "update" Then
  lsStrXML =  "<Root Transaction=""1"" CarryResults=""0"">" + _
                  "<UserDetailUpdate " + _
                      " ApplicationCD=""" + lsApplicationCD + """" + _
                      " UserID=""" + request("UserID") + """" + _
                      " ApplicationID=""" + request("ApplicationID") + """" + _
                      " OfficeID=""" + request("OfficeID") + """" + _
                      " SupervisorUserID=""" + request("SupervisorUserID") + """" + _
                      " AssignmentBeginDate=""" + request("AssignmentBeginDate") + """" + _
                      " AssignmentEndDate=""" + request("AssignmentEndDate") + """" + _
                      " ClientUserID=""" + trim(request("ClientUserID")) + """" + _
                      " EmailAddress=""" + trim(request("UserEmailAddress")) + """" + _
                      " FaxAreaCode=""" + request("FaxAreaCode") + """" + _
                      " FaxExchangeNumber=""" + request("FaxExchangeNumber") + """" + _
                      " FaxExtensionNumber=""" + request("FaxExtensionNumber") + """" + _
                      " FaxUnitNumber=""" + request("FaxUnitNumber") + """" + _
                      " NameFirst=""" + trim(request("NameFirst")) + """" + _
                      " NameLast=""" + trim(request("NameLast")) + """" + _
                      " NameTitle=""" + request("NameTitle") + """" + _
                      " PhoneAreaCode=""" + request("PhoneAreaCode") + """" + _
                      " PhoneExchangeNumber=""" + request("PhoneExchangeNumber") + """" + _
                      " PhoneExtensionNumber=""" + request("PhoneExtensionNumber") + """" + _
                      " PhoneUnitNumber=""" + request("PhoneUnitNumber") + """" + _
                      " SupervisorFlag=""" + request("SupervisorFlag") + """" + _
                      " LicenseAssignStList=""" + request("LicenseAssignStList") + """" + _
                      " RoleList=""" + request("RoleList") + """" + _
                      " ProfileList=""" + request("ProfileList") + """" + _
                      " PermissionList=""" + request("PermissionList") + """" + _
                      " SysLastUserID=""" + request("SysLastUserID") + """" + _
                      " SysLastUpdatedDate=""" + request("SysLastUpdatedDate") + """" + _
                      " />"

  If request("UserExistsInCP") = "0" Then
    lsStrXML = lsStrXML + "<UserApplicationInsert " + _
                              " UserID=""" + request("UserID") + """" + _
                              " ApplicationID=""" + request("ApplicationID") + """" + _
                              " LogonId=""" + trim(request("EmailAddress")) + """" + _
                              " PasswordHint=""Password was reset to a random value.""" + _
                              " SysLastUserID=""" + request("SysLastUserID") + """" + _
                              "/>"

  Else
    lsStrXML = lsStrXML + "<UserApplicationUpdate " + _
                              " UserID=""" + request("UserID") + """" + _
                              " ApplicationID=""" + request("ApplicationID") + """" + _
                              " LogonId=""" + trim(request("EmailAddress")) + """" + _
                              " PasswordHint=""" + server.urlencode(trim(request("PasswordHint"))) + """" + _
                              " SysLastUserID=""" + request("SysLastUserID") + """" + _
                              " SysLastUpdatedDate=""" + request("AppSysLastUpdatedDate") + """" + _
                              " />"

  End If
  
  If request("rbUserActive") <> request("UserActiveStatus") Then
    lsStrXML = lsStrXML + "<UserSetStatus " + _
                            " UserID=""" + request("UserID") + """" + _
                            " ApplicationID=""" + request("ApplicationID") + """" + _
                            " SysLastUserID=""" + request("SysLastUserID") + """" + _
                            " />"

  End If
  
  lsStrXML = lsStrXML + "</Root>"

End If

If request("UserID") <> "-1" and sMode = "activate" Then
  lsStrXML =  "<UserSetStatus " + _
                  " UserID=""" + request("UserID") + """" + _
                  " ApplicationID=""" + request("ApplicationID") + """" + _
                  " SysLastUserID=""" + request("SysLastUserID") + """" + _
                  " />"         

End If

If sMode = "check" Then
  lsStrXML =  "<UserDetail " + _
                  " EmailAddress=""" + trim(request("EmailAddress")) + """" + _
                  " ApplicationCD=""" + request("ApplicationCD") + """"

  If request("UserType") <> "M" Then
    lsStrXML = lsStrXML + " InsuranceCompanyID=""" + request("InsuranceCompanyID") + """"

  End If

  lsStrXML = lsStrXML + " />"

End If


sUserName	=	UnEscapeString(Request.form("EmailAddress"))

On Error Resume Next

If request("UserID") <> "-1" and sMode="update" Then

  'Set loAPDData = Server.CreateObject("PPGNAAPD.clsAPDData")
Set loAPDData = GetObject(mexMonikerString)

  liRetVal = loAPDData.PutXML(lsStrXML)

  Set loAPDData = Nothing

  'Need to check if the user exists in Aware.
  If liRetVal <> "-1" Then
    lsAddAwareResult = AddNewAwareUser(sUserName, sUserAdminEmail)
  
  End If


%>
  <form id="frmUserDetail" name="frmUserDetail" target="_top" method="post" action="frmUserList.asp">
    <input type="hidden" name="hidUserID" id="hidUserID" value="<%=request("UserID")%>"/>
    <input type="hidden" id="hidUserType" name="hidUserType" value="<%=request("UserType")%>"/>
    <input type="hidden" id="hidUserType" name="M" value="<%=request("UserType")%>"/>
  </form>

  <script language="javascript">
      var lsRet = "<%= liRetVal %>";

      var lsAddADUser = "<%= lsAddAwareResult %>";

      if (lsRet == "1") {
          if (lsAddADUser == "created") {
              alert("User information was updated.\n" +
              "A Login account didn't exist for this user and it was created.\n" +
              "An e-mail was sent to the user with the Login information.")
          }
          else if (lsAddADUser == "failed") {
              alert("User information was updated.\n" +
              "A Login account didn't exist for this user and it could not be created due to an error.\n" +
              "Please try to update the information again to create the Login account.\n" +
              "If you are unable to resolve the problem, please contact LYNX Services support. Thank you.")
          }

          var objForm = document.getElementById("frmUserDetail");
          if (objForm) {
              objForm.submit();
          }
          parent.showMsg("Loading User List. Please wait... ");
      }
      else {
          alert("Could not update user information.\n" +
            "Please try to update the information again.\n" +
            "If you are unable to resolve the issue, please contact LYNX Services support. Thank you.")
          parent.hideMsg();
      }
      parent.bInUpdateProcess = false;
  </script>

<%
End If

If request("UserID") = "-1" and sMode="insert" Then
  'Check if the login ID exist in user table
  Dim lsStrXML2
  lsStrXML2 =  "<UserDetail " + _
                  " EmailAddress=""" + request("EmailAddress") + """" + _
                  " ApplicationCD=""" + request("ApplicationCD") + """" + _
                  " InsuranceCompanyID=""" + request("InsuranceCompanyID") + """" + _
                  " />"         

  Set docXML = Server.CreateObject("MSXML2.DOMDocument")
  docXML.Async = False
  sRet = GetData(lsStrXML2, "", "")

  If sRet = "" Then
    'User exists for a different insurance company

  Else
    docXML.LoadXML sRet
    Set loRootNode = docXML.documentElement
    
    If not (loRootNode is Nothing) Then
      lsUserID = loRootNode.getAttribute("UserID")

      If lsUserID = "-1" Then
        'Add the user in user table
        'Set loAPDData = Server.CreateObject("PPGNAAPD.clsAPDData")
	 Set loAPDData = GetObject(mexMonikerString)
        liRetVal = loAPDData.PutXML(lsStrXML)
        Set loAPDData = Nothing

        'Check if the insert succeeded
        If liRetVal <> "-1" Then
          lsAddAwareResult = AddNewAwareUser(sUserName, sUserAdminEmail)

%>

          <form id="frmUserDetail" name="frmUserDetail" target="_top" method="post" action="frmUserList.asp">
            <input type="hidden" name="hidUserID" id="hidUserID" value="<%=lsUserID%>"/>
            <input type="hidden" id="hidUserType" name="M" value="<%=request("UserType")%>"/>
          </form>

          <script language="javascript">
              var lsRet = "<%= liRetVal %>";
              var lsAddADUser = "<%= lsAddAwareResult %>";

              if (lsRet == "1") {
                  if (lsAddADUser == "failed") {
                      alert("User was created but the Login account could not be created.\n" +
                      "Please open this user's information from the User List screen and click the Save button to created the Login account.\n" +
                      "If you are unable to resolve the problem, please contact LYNX Services support. Thank you.")
                  }
                  if (lsAddADUser == "exists") {
                      alert("User was created and a Login account \(e-mail address\) already existed for this user.\n" +
                      "A login reminder was sent to the user's e-mail address.")
                  }

                  var objForm = document.getElementById("frmUserDetail");
                  if (objForm) {
                      objForm.submit();
                  }
                  parent.showMsg("Loading User List. Please wait... ");
              }
              else {
                  alert("Unable to add user. Please try to add the user again.\n" +
                    "If you are unable to resolve the problem, please contact LYNX Services support. Thank you.")
                  parent.hideMsg();
              }
              parent.bInUpdateProcess = false;
          </script>

<%
  
        End If

      Else
        'problem. the user is linked to another user and this should have navigated the user to update the record.
%>

        <form id="frmUserCheck" name="frmUserCheck" method="post" target="_top" action="frmUserDetail.asp">
          <input type="hidden" id="hidUserID" name="hidUserID" value="<%=lsUserID%>"/>
          <input type="hidden" id="hidUserType" name="hidUserType"/>
        </form>

        <script language="javascript">
            var sUserID = "<%=lsUserID%>";
            if (sUserID != "-1" && sUserID != parent.sUserID) {
                var bRet = false;
                bRet = confirm("User with this Login ID already exists. Would you like to edit the user details?");
                if (bRet == true) {
                    parent.sUserID = sUserID;
                    parent.bUserChecked = true;
                    var objForm = document.getElementById("frmUserCheck");
                    if (objForm) {
                        objForm.hidUserType.value = parent.sUserType;
                        objForm.submit();
                    }
                    parent.showMsg("Loading User Detail. Please wait... ");
                } else {
                    var obj = parent.document.getElementById("txtEmailAddress");
                    if (obj) {
                        obj.focus();
                        obj.select();
                    }
                    parent.bUserChecked = false;
                    if (isNaN(parent.iTimer) == false)
                        window.clearTimeout(parent.iTimer);
                    parent.hideMsg();
                }
            } else {
                parent.bUserChecked = true;
                parent.hideMsg();
            }
            parent.bInUpdateProcess = false;
        </script>

<%

      End If
    End If

    Set loRootNode = Nothing

  End If
    
  Set docXML = Nothing
  
End If

If request("UserID") <> "-1" and sMode = "activate" Then
'  Dim docXML
  Set docXML = Server.CreateObject("MSXML2.DOMDocument")
  docXML.Async = False
  docXML.LoadXML GetData(lsStrXML, "", "")

  Dim lsActive, lsBeginEndDate, loUserNode, lsSysLastUpdatedDate
  Set loUserNode = docXML.documentElement.selectSingleNode("/Root/User")
  If not (loUserNode is Nothing) Then
    lsActive = loUserNode.getAttribute("Active")
    lsBeginEndDate = loUserNode.getAttribute("AccessBeginEndDate")
    lsSysLastUpdatedDate = loUserNode.getAttribute("SysLastUpdatedDate")
  End If

  Set loUserNode = Nothing
  Set docXML = Nothing

%>

<script language="javascript">
    parent.setUserStatus("<%=lsActive%>", "<%=lsBeginEndDate%>", "<%=lsSysLastUpdatedDate%>")
</script>

<%    
End If

If sMode = "check" Then
  Set docXML = Server.CreateObject("MSXML2.DOMDocument")
  docXML.Async = False
  sRet = GetData(lsStrXML, "", "")
  If sRet = "" Then
%>

  <script language="javascript">
      alert("The Login ID you entered is used by another user. Please specify a different e-mail address for the Login ID.");
      var obj = parent.document.getElementById("txtEmailAddress");
      if (obj) {
          obj.focus();
          obj.select();
      }
      parent.bUserChecked = false;
      parent.hideMsg();
  </script>

<%
  Else
    docXML.LoadXML sRet
    Set loRootNode = docXML.documentElement
    
    If not (loRootNode is Nothing) Then
      lsUserID = loRootNode.getAttribute("UserID")
%>

      <form id="frmUserCheck" name="frmUserCheck" method="post" target="_top" action="frmUserDetail.asp">
        <input type="hidden" id="hidUserID" name="hidUserID" value="<%=lsUserID%>"/>
        <input type="hidden" id="hidUserType" name="hidUserType"/>
      </form>

      <script language="javascript">
          var sUserID = "<%=lsUserID%>";
          if (sUserID != "-1" && sUserID != parent.sUserID) {
              var bRet = false;
              bRet = confirm("User with this Login ID already exists. Would you like to edit the user details?");
              if (bRet == true) {
                  parent.sUserID = sUserID;
                  parent.bUserChecked = true;
                  var objForm = document.getElementById("frmUserCheck");
                  if (objForm) {
                      objForm.hidUserType.value = parent.sUserType;
                      objForm.submit();
                  }
                  parent.showMsg("Loading User Detail. Please wait... ");
              } else {
                  var obj = parent.document.getElementById("txtEmailAddress");
                  if (obj) {
                      obj.focus();
                      obj.select();
                  }
                  parent.bUserChecked = false;
                  if (isNaN(parent.iTimer) == false)
                      window.clearTimeout(parent.iTimer);
                  parent.hideMsg();
              }
          } else {
              parent.bUserChecked = true;
              parent.hideMsg();
          }
          parent.bInUpdateProcess = false;
      </script>

<%
    End If
  
    Set loRootNode = Nothing
  
  End If

Set docXML = Nothing

End If
%>

</body>
</html>