<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!-- v1.4.5.0 -->

<%      Dim IsEmailCell
        IsEmailCell = "<Config Path=""ClaimPoint/Customizations/InsuranceCompany[@ID='" & liInsuranceCompanyID & "']/@RepairReferral""/>"
        IsEmailCell = GetData(IsEmailCell, "", "")
        docXML.Async = False
        docXML.loadXML IsEmailCell
        IsEmailCell = docXML.documentElement.text
        IsEmailCell = Lcase(IsEmailCell)
        IsEmailCell = Trim(IsEmailCell)
        IsEmailCell= Replace(IsEmailCell,"<config>","")
        IsEmailCell= Replace(IsEmailCell,"</config>","")
%>   


<html>
<head>
<title>WADataContainer</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script language="JavaScript" type="text/JavaScript">


    function pageInit() {
        if (parent.gbEmergencyLiabilityScript == 1) {
            var loNewCell;
            var lsNewElm = "<input name='SourceApplicationPassThruData' id='txtSourceApplicationPassThruData' type='text'>";
            var loTbl = document.getElementById("tblClaim");
            if (document.all) {
                loNewCell = loTbl.rows[0].insertCell();
            }
            else if (document.createElement) {
                loNewCell = document.createElement("TD");
                var loMyRrow = loTbl.getElementsByTagName("tr").item(0);
                loMyRrow.appendChild(loNewCell);
            }
            loNewCell.innerHTML = lsNewElm;
        }
    }

    function GetConfigValue() {

        var IsRepairEmailCell = "<%= IsEmailCell %>";

        IsRepairEmailCell = IsRepairEmailCell.replace("<config>", "");
        IsRepairEmailCell = IsRepairEmailCell.replace("</config>", "");
        IsRepairEmailCell = IsRepairEmailCell.toLowerCase();

        document.getElementById("txtRepairRefrral").value = IsRepairEmailCell;
    }

    function updClaimValues() {
        if (document.getElementById('txtInsuredPhoneAC').value != "") {
            document.getElementById('txtInsuredPhone').value = document.getElementById('txtInsuredPhoneAC').value + document.getElementById('txtInsuredPhoneEN').value + document.getElementById('txtInsuredPhoneUN').value;
            document.getElementById('txtInsuredPhoneSumm').value = document.getElementById('txtInsuredPhoneAC').value + "-" + document.getElementById('txtInsuredPhoneEN').value + "-" + document.getElementById('txtInsuredPhoneUN').value;
        }
    }


    function updVehValues(vehNum) {
        var liVehNum = vehNum;
        if (document.getElementById("txtOwnerPhoneAC_" + liVehNum).value != "") {
            document.getElementById("txtOwnerPhone_" + liVehNum).value = document.getElementById("txtOwnerPhoneAC_" + liVehNum).value + document.getElementById("txtOwnerPhoneEN_" + liVehNum).value + document.getElementById("txtOwnerPhoneUN_" + liVehNum).value;
            document.getElementById("txtOwnerPhoneSumm_" + liVehNum).value = document.getElementById("txtOwnerPhoneAC_" + liVehNum).value + "-" + document.getElementById("txtOwnerPhoneEN_" + liVehNum).value + "-" + document.getElementById("txtOwnerPhoneUN_" + liVehNum).value;
        }

        document.getElementById("txtContactName_" + liVehNum).value = document.getElementById("txtContactNameFirst_" + liVehNum).value + " " + document.getElementById("txtContactNameLast_" + liVehNum).value;

        if (document.getElementById("txtContactPhoneAC_" + liVehNum).value != "") {
            document.getElementById("txtContactPhone_" + liVehNum).value = document.getElementById("txtContactPhoneAC_" + liVehNum).value + document.getElementById("txtContactPhoneEN_" + liVehNum).value + document.getElementById("txtContactPhoneUN_" + liVehNum).value;
            document.getElementById("txtContactPhoneSumm_" + liVehNum).value = document.getElementById("txtContactPhoneAC_" + liVehNum).value + "-" + document.getElementById("txtContactPhoneEN_" + liVehNum).value + "-" + document.getElementById("txtContactPhoneUN_" + liVehNum).value;
        }

        if (document.getElementById("txtContactNightPhoneAC_" + liVehNum).value != "") {
            document.getElementById("txtContactNightPhoneSumm_" + liVehNum).value = document.getElementById("txtContactNightPhoneAC_" + liVehNum).value + "-" + document.getElementById("txtContactNightPhoneEN_" + liVehNum).value + "-" + document.getElementById("txtContactNightPhoneUN_" + liVehNum).value;
            document.getElementById("txtContactNightPhone_" + liVehNum).value = document.getElementById("txtContactNightPhoneAC_" + liVehNum).value + document.getElementById("txtContactNightPhoneEN_" + liVehNum).value + document.getElementById("txtContactNightPhoneUN_" + liVehNum).value;
        }

        if (document.getElementById("txtContactAltPhoneAC_" + liVehNum).value != "") {
            document.getElementById("txtContactAltPhoneSumm_" + liVehNum).value = document.getElementById("txtContactAltPhoneAC_" + liVehNum).value + "-" + document.getElementById("txtContactAltPhoneEN_" + liVehNum).value + "-" + document.getElementById("txtContactAltPhoneUN_" + liVehNum).value;
            document.getElementById("txtContactAltPhone_" + liVehNum).value = document.getElementById("txtContactAltPhoneAC_" + liVehNum).value + document.getElementById("txtContactAltPhoneEN_" + liVehNum).value + document.getElementById("txtContactAltPhoneUN_" + liVehNum).value;
        }

        if (document.getElementById("txtCellAreaCode_" + liVehNum).value != "") {

            document.getElementById("txtContactCellPhoneSumm_" + liVehNum).value = document.getElementById("txtCellAreaCode_" + liVehNum).value + "-" + document.getElementById("txtCellExchangeNumber_" + liVehNum).value + "-" + document.getElementById("txtCellUnitNumber_" + liVehNum).value;
            document.getElementById("txtContactCellPhone_" + liVehNum).value = document.getElementById("txtCellAreaCode_" + liVehNum).value + document.getElementById("txtCellExchangeNumber_" + liVehNum).value + document.getElementById("txtCellUnitNumber_" + liVehNum).value;
        }

        document.getElementById("txtImpactLocations_" + liVehNum).value = document.getElementById("txtPrimaryDamage_" + liVehNum).value + "," + document.getElementById("txtSecondaryDamage_" + liVehNum).value;
    }


    function UpdContactPhone(vehNum) {
        var liVehNum = vehNum;
        if (document.getElementById("txtContactPhoneAC_" + liVehNum).value != "") {
            document.getElementById("txtOwnerPhoneAC_" + liVehNum).value = document.getElementById("txtContactPhoneAC_" + liVehNum).value;
            document.getElementById("txtOwnerPhoneEN_" + liVehNum).value = document.getElementById("txtContactPhoneEN_" + liVehNum).value;
            document.getElementById("txtOwnerPhoneUN_" + liVehNum).value = document.getElementById("txtContactPhoneUN_" + liVehNum).value;
        }
        else if (document.getElementById("txtContactNightPhoneAC_" + liVehNum).value != "") {
            document.getElementById("txtOwnerPhoneAC_" + liVehNum).value = document.getElementById("txtContactNightPhoneAC_" + liVehNum).value;
            document.getElementById("txtOwnerPhoneEN_" + liVehNum).value = document.getElementById("txtContactNightPhoneEN_" + liVehNum).value;
            document.getElementById("txtOwnerPhoneUN_" + liVehNum).value = document.getElementById("txtContactNightPhoneUN_" + liVehNum).value;
        }
        else {
            document.getElementById("txtOwnerPhoneAC_" + liVehNum).value = document.getElementById("txtContactAltPhoneAC_" + liVehNum).value;
            document.getElementById("txtOwnerPhoneEN_" + liVehNum).value = document.getElementById("txtContactAltPhoneEN_" + liVehNum).value;
            document.getElementById("txtOwnerPhoneUN_" + liVehNum).value = document.getElementById("txtContactAltPhoneUN_" + liVehNum).value;
        }
        document.getElementById("txtOwnerPhone_" + liVehNum).value = document.getElementById("txtOwnerPhoneAC_" + liVehNum).value + document.getElementById("txtOwnerPhoneEN_" + liVehNum).value + document.getElementById("txtOwnerPhoneUN_" + liVehNum).value;
        document.getElementById("txtOwnerPhoneSumm_" + liVehNum).value = document.getElementById("txtOwnerPhoneAC_" + liVehNum).value + "-" + document.getElementById("txtOwnerPhoneEN_" + liVehNum).value + "-" + document.getElementById("txtOwnerPhoneUN_" + liVehNum).value;
    }


    function insNewVeh(vehNum, isFirstparty, isProgramShop, bReserveLineTypeScript) {

        var liVehNum = vehNum;
        var liAssignmentTypeID = "";
        var sHtml = "";

        //  	if (isProgramShop == true)
        //    {
        liAssignmentTypeID = 16;
        //    }

        var loTbl = document.getElementById("tblVehicleBody");
        if (document.all) {
            var lcolTblRows = loTbl.rows;
            var loNewRow = loTbl.insertRow(-1);
            var loNewCell = loNewRow.insertCell();
            loNewRow.vehNum = liVehNum;

            if (isFirstparty == true)
                loNewRow.firstParty = "true";
            else
                loNewRow.firstParty = "false";

            if (isProgramShop == true)
                loNewRow.programShop = "true";
            else
                loNewRow.programShop = "false";
        }
        else if (document.createElement) {
            var loNewRow = document.createElement("TR");
            var loNewCell = document.createElement("TD");
            loNewRow.appendChild(loNewCell);
            loTbl.appendChild(loNewRow);
            loNewRow.setAttribute("vehNum", liVehNum);

            if (isFirstparty == true)
                loNewRow.setAttribute("firstParty", "true");
            else
                loNewRow.setAttribute("firstParty", "false");

            if (isProgramShop == true)
                loNewRow.setAttribute("programShop", "true");
            else
                loNewRow.setAttribute("programShop", "false");
        }
        sHtml = '<input name=AssignmentTypeID id=txtAssignmentTypeID_' + liVehNum + ' type=text value=' + liAssignmentTypeID + '><input name=ExposureCD id=txtExposureCD_' + liVehNum + ' type=text><input name=CoverageProfileCD id=txtCoverageProfileCD_' + liVehNum + ' type=text><input name=OwnerNameFirst id=txtOwnerNameFirst_' + liVehNum + ' type=text><input name=OwnerNameLast id=txtOwnerNameLast_' + liVehNum + ' type=text><input name=OwnerPhoneAC id=txtOwnerPhoneAC_' + liVehNum + ' type=text opt=yes><input name=OwnerPhoneEN id=txtOwnerPhoneEN_' + liVehNum + ' type=text opt=yes><input name=OwnerPhoneUN id=txtOwnerPhoneUN_' + liVehNum + ' type=text opt=yes><input name=OwnerBusinessName id=txtOwnerBusinessName_' + liVehNum + ' type=text><input name=ContactNameFirst id=txtContactNameFirst_' + liVehNum + ' type=text><input name=ContactNameLast id=txtContactNameLast_' + liVehNum + ' type=text><input name=ContactPhoneAC id=txtContactPhoneAC_' + liVehNum + ' type=text opt=yes><input name=ContactPhoneEN id=txtContactPhoneEN_' + liVehNum + ' type=text opt=yes><input name=ContactPhoneUN id=txtContactPhoneUN_' + liVehNum + ' type=text opt=yes><input name=ContactNightPhoneAC id=txtContactNightPhoneAC_' + liVehNum + ' type=text opt=yes><input name=ContactNightPhoneEN id=txtContactNightPhoneEN_' + liVehNum + ' type=text opt=yes><input name=ContactNightPhoneUN id=txtContactNightPhoneUN_' + liVehNum + ' type=text opt=yes><input name=ContactAltPhoneAC id=txtContactAltPhoneAC_' + liVehNum + ' type=text opt=yes><input name=ContactAltPhoneEN id=txtContactAltPhoneEN_' + liVehNum + ' type=text opt=yes><input name=ContactAltPhoneUN id=txtContactAltPhoneUN_' + liVehNum + ' type=text opt=yes><input name=ContactBestPhoneCD id=txtContactBestPhoneCD_' + liVehNum + ' type=text><input name=VIN id=txtVIN_' + liVehNum + ' type=text><input name=LicensePlateNumber id=txtLicensePlateNumber_' + liVehNum + ' type=text><input name=VehicleYear id=txtVehicleYear_' + liVehNum + ' type=text><input name=Make id=txtMake_' + liVehNum + ' type=text><input name=Model id=txtModel_' + liVehNum + ' type=text><input name=Drivable id=txtDrivable_' + liVehNum + ' type=text><input name=PrimaryDamage id=txtPrimaryDamage_' + liVehNum + ' type=text><input name=SecondaryDamage id=txtSecondaryDamage_' + liVehNum + ' type=text><input name=ImpactLocations id=txtImpactLocations_' + liVehNum + ' type=text><input name=ShopLocationID id=txtShopLocationID_' + liVehNum + ' type=text><input name=ShopSearchLogID id=txtShopSearchLogID_' + liVehNum + ' type=text><input name=SelectedShopRank id=txtSelectedShopRank_' + liVehNum + ' type=text><input name=SelectedShopScore id=txtSelectedShopScore_' + liVehNum + ' type=text><textarea name=ShopRemarks id=txtShopRemarks_' + liVehNum + '></textarea><input name=OwnerName id=txtOwnerName_' + liVehNum + ' type=text opt=yes><input name=OwnerPhone id=txtOwnerPhone_' + liVehNum + ' type=text><input name=ContactName id=txtContactName_' + liVehNum + ' type=text opt=yes><input name=ContactPhone id=txtContactPhone_' + liVehNum + ' type=text><input name=ContactNightPhone id=txtContactNightPhone_' + liVehNum + ' type=text><input name=ContactAltPhone id=txtContactAltPhone_' + liVehNum + ' type=text><input name=OwnerPhoneSumm id=txtOwnerPhoneSumm_' + liVehNum + ' type=text><input name=ContactPhoneSumm id=txtContactPhoneSumm_' + liVehNum + ' type=text><input name=ContactNightPhoneSumm id=txtContactNightPhoneSumm_' + liVehNum + ' type=text><input name=ContactAltPhoneSumm id=txtContactAltPhoneSumm_' + liVehNum + ' type=text><input name=ShopName id=txtShopName_' + liVehNum + ' type=text><input name=ShopAddress1 id=txtShopAddress1_' + liVehNum + ' type=text><input name=ShopAddress2 id=txtShopAddress2_' + liVehNum + ' type=text><input name=ShopCity id=txtShopCity_' + liVehNum + ' type=text><input name=ShopState id=txtShopState_' + liVehNum + ' type=text><input name=ShopZip id=txtShopZip_' + liVehNum + ' type=text><input name=ShopPhone id=txtShopPhone_' + liVehNum + ' type=text><input name=ShopFax id=txtShopFax_' + liVehNum + ' type=text><input name=ShopMFopen id=txtShopMFopen_' + liVehNum + ' type=text opt=yes><input name=ShopMFClosed id=txtShopMFClosed_' + liVehNum + ' type=text opt=yes><input name=ShopSatOpen id=txtShopSatOpen_' + liVehNum + ' type=text opt=yes><input name=ShopSatClosed id=txtShopSatClosed_' + liVehNum + ' type=text opt=yes><textarea name=DrivingDirections id=txtDrivingDirections_' + liVehNum + '></textarea><input name=RentalAuthorized_ id=txtRentalAuthorized_' + liVehNum + ' type=text><input name=RentalDays id=txtRentalDays_' + liVehNum + ' type=text><input name=RentalDayAmount id=txtRentalDayAmount_' + liVehNum + ' type=text><input name=RentalMaxAmount id=txtRentalMaxAmount_' + liVehNum + ' type=text><input name=RentalDaysAuthorized id=txtRentalDaysAuthorized_' + liVehNum + ' type=text><textarea name=RentalInstructions id=txtRentalInstructions_' + liVehNum + '></textarea><input name=DeductibleAmt id=txtDeductibleAmt_' + liVehNum + ' type=text><input name=LimitAmt id=txtLimitAmt_' + liVehNum + ' type=text><input name=CoverageProfileUiCD id=txtCoverageProfileUiCD_' + liVehNum + ' type=text><input name=PrimaryDamageDescription id=txtPrimaryDamageDescription_' + liVehNum + ' type=text><input name=SecondaryDamageDescription id=txtSecondaryDamageDescription_' + liVehNum + ' type=text><input name=AssignmentTypeIDDescription id=txtAssignmentTypeIDDescription_' + liVehNum + ' type=text><input name=Mileage id=txtMileage_' + liVehNum + ' type=text><input name=ClientCoverageTypeID id=txtClientCoverageTypeUiID_' + liVehNum + ' type=text><input name=ClientCoverageTypeDesc id=txtClientCoverageTypeDesc_' + liVehNum + ' type=text><input name=VehicleNumber id=txtVehicleNumber_' + liVehNum + ' type=text ><input name=ShowScriptingMemoFlag id=txtShowScriptingMemoFlag_' + liVehNum + ' value=0 type=text><input name=CellPhoneCarrier id=txtCellPhoneCarrier_' + liVehNum + ' type=text><input name=ContactEmailAddress id=txtContactEmailAddress_' + liVehNum + ' type=text><input name=PrefMethodUpd id=txtPrefMethodUpd_' + liVehNum + ' type=text><input name=ContactCellPhone id=txtContactCellPhone_' + liVehNum + ' type=text><input name=CellAreaCode id=txtCellAreaCode_' + liVehNum + ' type=text opt=yes><input name=CellExchangeNumber id=txtCellExchangeNumber_' + liVehNum + ' type=text opt=yes><input name=CellUnitNumber id=txtCellUnitNumber_' + liVehNum + ' type=text opt=yes><input name=ContactCellPhoneSumm id=txtContactCellPhoneSumm_' + liVehNum + ' type=text>';

        if (parent.gbEmergencyLiabilityScript == 1 || parent.gbReserveLineTypeScript == 1) {
            sHtml += '<input name=SourceApplicationPassThruData id=txtSourceApplicationPassThruData_' + liVehNum + ' type=text><textarea name=CustomScriptingMemo id=txtCustomScriptingMemo_' + liVehNum + '></textarea>';
        }

        if (parent.gbReserveLineTypeScript == 1) {
            sHtml += '<input name=LineType id=txtLineType_' + liVehNum + ' type=text opt=yes><input name=Reserve id=txtReserve_' + liVehNum + ' type=text opt=yes>';
        }
        sHtml += '<input name=SourceApplicationPassthruDataVeh id=txtSourceApplicationPassthruDataVeh_' + liVehNum + ' type=text>';
        loNewCell.innerHTML = sHtml;
    }

    /*
    [0] <input name=AssignmentTypeID id=txtAssignmentTypeID_'+liVehNum+' type=text>
    [1] <input name=ExposureCD id=txtExposureCD_'+liVehNum+' type=text>
    [2] <input name=CoverageProfileCD id=txtCoverageProfileCD_'+liVehNum+' type=text>
    [3] <input name=OwnerNameFirst id=txtOwnerNameFirst_'+liVehNum+' type=text>
    [4] <input name=OwnerNameLast id=txtOwnerNameLast_'+liVehNum+' type=text>
    [5] <input name=OwnerPhoneAC id=txtOwnerPhoneAC_'+liVehNum+' type=text opt=yes>
    [6] <input name=OwnerPhoneEN id=txtOwnerPhoneEN_'+liVehNum+' type=text opt=yes>
    [7] <input name=OwnerPhoneUN id=txtOwnerPhoneUN_'+liVehNum+' type=text opt=yes>
    [8] <input name=OwnerBusinessName id=txtOwnerBusinessName_'+liVehNum+' type=text>
    [9] <input name=ContactNameFirst id=txtContactNameFirst_'+liVehNum+' type=text>
    [10] <input name=ContactNameLast id=txtContactNameLast_'+liVehNum+' type=text>
    [11] <input name=ContactPhoneAC id=txtContactPhoneAC_'+liVehNum+' type=text opt=yes>
    [12] <input name=ContactPhoneEN id=txtContactPhoneEN_'+liVehNum+' type=text opt=yes>
    [13] <input name=ContactPhoneUN id=txtContactPhoneUN_'+liVehNum+' type=text opt=yes>
    [14] <input name=ContactNightPhoneAC id=txtContactNightPhoneAC_'+liVehNum+' type=text opt=yes>
    [15] <input name=ContactNightPhoneEN id=txtContactNightPhoneEN_'+liVehNum+' type=text opt=yes>
    [16] <input name=ContactNightPhoneUN id=txtContactNightPhoneUN_'+liVehNum+' type=text opt=yes>
    [17] <input name=ContactAltPhoneAC id=txtContactAltPhoneAC_'+liVehNum+' type=text opt=yes>
    [18] <input name=ContactAltPhoneEN id=txtContactAltPhoneEN_'+liVehNum+' type=text opt=yes>
    [19] <input name=ContactAltPhoneUN id=txtContactAltPhoneUN_'+liVehNum+' type=text opt=yes>
    [20] <input name=ContactBestPhoneCD id=txtContactBestPhoneCD_'+liVehNum+' type=text>
    [21] <input name=VIN id=txtVIN_'+liVehNum+' type=text>
    [22] <input name=LicensePlateNumber id=txtLicensePlateNumber_'+liVehNum+' type=text>
    [23] <input name=VehicleYear id=txtVehicleYear_'+liVehNum+' type=text>
    [24] <input name=Make id=txtMake_'+liVehNum+' type=text>
    [25] <input name=Model id=txtModel_'+liVehNum+' type=text>
    [26] <input name=Drivable id=txtDrivable_'+liVehNum+' type=text>
    [27] <input name=PrimaryDamage id=txtPrimaryDamage_'+liVehNum+' type=text>
    [28] <input name=SecondaryDamage id=txtSecondaryDamage_'+liVehNum+' type=text>
    [29] <input name=ImpactLocations id=ImpactLocations_'+liVehNum+' type=text>
    [30] <input name=ShopLocationID id=txtShopLocationID_'+liVehNum+' type=text>
    [31] <input name=ShopSearchLogID id=txtShopSearchLogID_'+liVehNum+' type=text>
    [32] <input name=SelectedShopRank id=txtSelectedShopRank_'+liVehNum+' type=text>
    [33] <input name=SelectedShopScore id=txtSelectedShopScore_'+liVehNum+' type=text>
    [34] <textarea name=ShopRemarks id=txtShopRemarks_'+liVehNum+'><textarea>
    [35] <input name=OwnerName id=txtOwnerName_'+liVehNum+' type=text opt=yes>
    [36] <input name=OwnerPhone id=txtOwnerPhone_'+liVehNum+' type=text>
    [37] <input name=ContactName id=txtContactName_'+liVehNum+' type=text opt=yes>
    [38] <input name=ContactPhone id=txtContactPhone_'+liVehNum+' type=text>
    [39] <input name=ContactNightPhone id=txtContactNightPhone_'+liVehNum+' type=text>
    [40] <input name=ContactAltPhone id=txtContactAltPhone_'+liVehNum+' type=text>
    [41] <input name=OwnerPhoneSumm id=txtOwnerPhoneSumm_'+liVehNum+' type=text>
    [42] <input name=ContactPhoneSumm id=txtContactPhoneSumm_'+liVehNum+' type=text>
    [43] <input name=ContactNightPhoneSumm id=txtContactNightPhoneSumm_'+liVehNum+' type=text>
    [44] <input name=ContactAltPhoneSumm id=txtContactAltPhoneSumm_'+liVehNum+' type=text>
    [45] <input name=ShopName id=txtShopName_'+liVehNum+' type=text>
    [46] <input name=ShopAddress1 id=txtShopAddress1_'+liVehNum+' type=text>
    [47] <input name=ShopAddress2 id=txtShopAddress2_'+liVehNum+' type=text>
    [48] <input name=ShopCity id=txtShopCity_'+liVehNum+' type=text>
    [49] <input name=ShopState id=txtShopState_'+liVehNum+' type=text>
    [50] <input name=ShopZip id=txtShopZip_'+liVehNum+' type=text>
    [51] <input name=ShopPhone id=txtShopPhone_'+liVehNum+' type=text>
    [52] <input name=ShopFax id=txtShopFax_'+liVehNum+' type=text>
    [53] <input name=ShopMFopen id=txtShopMFopen_'+liVehNum+' type=text opt=yes>
    [54] <input name=ShopMFClosed id=txtShopMFClosed_'+liVehNum+' type=text opt=yes>
    [55] <input name=ShopSatOpen id=txtShopSatOpen_'+liVehNum+' type=text opt=yes>
    [56] <input name=ShopSatClosed id=txtShopSatClosed_'+liVehNum+' type=text opt=yes>
    [57] <textarea name=DrivingDirections id=txtDrivingDirections_'+liVehNum+'><textarea>
    [58] <input name=RentalAuthorized_ id=txtRentalAuthorized_'+liVehNum+' type=text>
    [59] <input name=RentalDays id=txtRentalDays_'+liVehNum+' type=text>
    [60] <input name=RentalDayAmount id=txtRentalDayAmount_'+liVehNum+' type=text>
    [61] <input name=RentalMaxAmount id=txtRentalMaxAmount_'+liVehNum+' type=text>
    [62] <input name=RentalDaysAuthorized id=txtRentalDaysAuthorized_'+liVehNum+' type=text>
    [63] <textarea name=RentalInstructions id=txtRentalInstructions_'+liVehNum+'></textarea>
    [64] <input name=DeductibleAmt id=txtDeductibleAmt_'+liVehNum+' type=text>
    [65] <input name=LimitAmt id=txtLimitAmt_'+liVehNum+' type=text>
    [66] <input name=CoverageProfileUiCD id=txtCoverageProfileUiCD_'+liVehNum+' type=text>
    [67] <input name=PrimaryDamageDescription id=txtPrimaryDamageDescription_'+liVehNum+' type=text>
    [68] <input name=SecondaryDamageDescription id=txtSecondaryDamageDescription_'+liVehNum+' type=text>
    [69] <input name=AssignmentTypeIDDescription id=txtAssignmentTypeIDDescription_'+liVehNum+' type=text>
    [70] <input name=Mileage id=txtMileage_'+liVehNum+' type=text>
    [71] <input name=ClientCoverageTypeID id=txtClientCoverageTypeUiID_'+liVehNum+' type=text>
    [72] <input name=ClientCoverageTypeDesc id=txtClientCoverageTypeDesc_'+liVehNum+' type=text>
    [73] <input name=VehicleNumber id=txtVehicleNumber_'+liVehNum+' type=text >
    [74] <input name=ShowScriptingMemoFlag id=txtShowScriptingMemoFlag_'+liVehNum+' value=0 type=text>
    [75] <input name=SourceApplicationPassThruData id=txtSourceApplicationPassThruData_'+liVehNum+' type=text>
    [76] <textarea name=CustomScriptingMemo id=txtCustomScriptingMemo_'+liVehNum+'></textarea>
    [77] <input name=LineType id=txtLineType_'+liVehNum+' type=text opt=yes>
    [78] <input name=Reserve id=txtReserve_'+liVehNum+' type=text opt=yes>
    [79] <input name=SourceApplicationPassthruDataVeh id=txtSourceApplicationPassthruDataVeh_'+liVehNum+' type=text opt=yes>
    [80] <input name=CellPhoneCarrier id=txtCellPhoneCarrier_' + liVehNum + ' type=text>
    [81] <input name=ContactEmailAddress id=txtContactEmailAddress_' + liVehNum + ' type=text> 
    [82] <input name=PrefMethodUpd id=txtPrefMethodUpd_' + liVehNum + ' type=text>
    [83] <input name=ContactCellPhone id=txtContactCellPhone_' + liVehNum + ' type=text>
    [84] <input name=CellAreaCode id=txtCellAreaCode_' + liVehNum + ' type=text opt=yes>
    [85] <input name=CellExchangeNumber id=txtCellExchangeNumber_' + liVehNum + ' type=text opt=yes>
    [86] <input name=CellUnitNumber id=txtCellUnitNumber_' + liVehNum + ' type=text opt=yes>
    */
  
</script>

</head>

<body onload = "GetConfigValue();">

<form name="tblClaimInfo">

  <table name="tblClaim" id="tblClaim">
    <tr>
    	<td>
      	<input name="NoticeMethodID" id="txtNoticeMethodID" type="text" value="5">
      	<input name="DataSource" id="txtDataSource" type="text" value="Claim Point Web Assignment">
      	<input name="FNOLUserID" id="txtFNOLUserID" type="text">
      	<input name="InsuranceCompanyID" id="txtInsuranceCompanyID" type="text">
      	<input name="AssignmentAtSelectionFlag" id="txtAssignmentAtSelectionFlag" type="text">
      	<input name="DemoFlag" id="txtDemoFlag" type="text">
      	<input name="AssignmentDescription" id="txtAssignmentDescription" type="text">
      	<input name="CarrierOfficeName" id="txtCarrierOfficeName" type="text">
        <input name="CarrierUser" id="txtCarrierUser" type="text" opt="yes">
        <input name="CarrierRepNameFirst" id="txtCarrierRepNameFirst" type="text">
        <input name="CarrierRepNameLast" id="txtCarrierRepNameLast" type="text">
        <input name="CarrierRepUserID" id="txtCarrierRepUserID" type="text" value="">
        <input name="CarrierRepPhoneDay" id="txtCarrierRepPhoneDay" type="text">
        <input name="CarrierRepEmailAddress" id="txtCarrierRepEmailAddress" type="text">
        <input name="CarrierName" id="txtCarrierName" type="text">
      	<input name="CoverageClaimNumber" id="txtCoverageClaimNumber" type="text">
      	<input name="LossDate" id="txtLossDate" type="text">
      	<input name="LossAddressState" id="txtLossAddressState" type="text">
        <textarea name="LossDescription" id="txtLossDescription"></textarea>
      	<input name="CallerNameFirst" id="txtCallerNameFirst" type="text">
      	<input name="CallerNameLast" id="txtCallerNameLast" type="text">
      	<input name="CallerRelationToInsuredID" id="txtCallerRelationToInsuredID" type="text">
      	<input name="CallerRelationToInsuredIDDescription" id="txtCallerRelationToInsuredIDDescription" type="text">
      	<input name="InsuredNameFirst" id="txtInsuredNameFirst" type="text">
      	<input name="InsuredNameLast" id="txtInsuredNameLast" type="text">
      	<input name="InsuredPhone" id="txtInsuredPhone" type="text">
      	<input name="InsuredBusinessName" id="txtInsuredBusinessName" type="text">
      	<input name="CollisionDeductibleAmt" id="txtCollisionDeductibleAmt" type="text">
      	<input name="CollisionLimitAmt" id="txtCollisionLimitAmt" type="text">
      	<input name="ComprehensiveDeductibleAmt" id="txtComprehensiveDeductibleAmt" type="text">
      	<input name="ComprehensiveLimitAmt" id="txtComprehensiveLimitAmt" type="text">
      	<input name="LiabilityDeductibleAmt" id="txtLiabilityDeductibleAmt" type="text">
      	<input name="LiabilityLimitAmt" id="txtLiabilityLimitAmt" type="text">
      	<input name="UnderInsuredDeductibleAmt" id="txtUnderInsuredDeductibleAmt" type="text">
      	<input name="UnderInsuredLimitAmt" id="txtUnderInsuredLimitAmt" type="text">
      	<input name="UnInsuredDeductibleAmt" id="txtUnInsuredDeductibleAmt" type="text">
      	<input name="UnInsuredLimitAmt" id="txtUnInsuredLimitAmt" type="text">
      	<textarea name="Remarks" id="txtRemarks"></textarea>
      	<input name="TimeStarted" id="txtTimeStarted" type="text">
      	<input name="TimeFinished" id="txtTimeFinished" type="text">
      	<input name="IntakeStartSeconds" id="txtIntakeStartSeconds" type="text">
      	<input name="IntakeEndSeconds" id="txtIntakeEndSeconds" type="text">
      	<input name="IntakeSeconds" id="txtIntakeSeconds" type="text">
      	<input name="InsuredName" id="txtInsuredName" type="text">
      	<input name="InsuredPhoneSumm" id="txtInsuredPhoneSumm" type="text">
      	<input name="InsuredPhoneAC" id="txtInsuredPhoneAC" type="text" opt="yes">
      	<input name="InsuredPhoneEN" id="txtInsuredPhoneEN" type="text" opt="yes">
      	<input name="InsuredPhoneUN" id="txtInsuredPhoneUN" type="text" opt="yes">
      	<input name="ClaimSubmitted" id="txtClaimSubmitted" type="text" value="false" opt="yes">
      	<input name="RentalAuthorized" id="txtRentalAuthorized" type="text">
      	<input name="RentalDays" id="txtRentalDays" type="text">
      	<input name="RentalDayAmount" id="txtRentalDayAmount" type="text">
      	<input name="RentalMaxAmount" id="txtRentalMaxAmount" type="text">
        <input name=RentalDaysAuthorized id=txtRentalDaysAuthorized type=text>
        <textarea name=RentalInstructions id=txtRentalInstructions></textarea>
        <input name=Mileage id=txtMileage type=text>
      	<input name="LiabilityText" id="txtLiabilityText" type="text" opt="yes">
      	<input name="LiabilityFlag" id="txtLiabilityFlag" type="text" opt="yes">
      	<input name="EmergencyText" id="txtEmergencyText" type="text" opt="yes">
      	<input name="EmergencyFlag" id="txtEmergencyFlag" type="text" opt="yes">
        <textarea name="CustomScriptingMemo" id="txtCustomScriptingMemo" opt="yes"></textarea>
        <input name="SourceApplicationPassThruData" id="txtSourceApplicationPassThruData" type="text" opt="yes">
        <input name="RepairReferral" id="txtRepairRefrral" type="text" />
      </td>
    </tr>
  </table>

</form>

<form name="tblVehicleInfo">

  <table name="tblVehicle" id="tblVehicle">
    <tbody id="tblVehicleBody">
      <!-- Vehicle TDs go here -->  
    </tbody>
  </table>

</form>

</body>
</html>
