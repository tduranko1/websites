<!--#include file="includes/incCommonTop.asp"-->

<%
lsPageName = "frmDisplayDocument.asp"
liPageType = lcAfterInsuranceCompanySelect
%>

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->

<%
dim lsDocument
dim lsDocType
dim liPdPosn
dim lsArchived
dim lsXMLCall
dim lsFormat
lsDocument = request("D")
lsArchived = request("A")




if len(lsDocument) > 0 then
	lsDocType = ""
	liPdPosn = instr(1, lsDocument, ".")
	
	if liPdPosn > 0 then lsDocType = lcase(mid(lsDocument, liPdPosn))
	lsXMLCall = "<Document ImageLocation=""" & lsDocument & """ Archived=""" & lsArchived & """/>"

	lsStyleSheet = "ViewXML.xsl"
	
	if lsDocType = ".xml" then
		Response.ContentType = "text/HTML"
		Response.Write "<HTML><HEAD></HEAD><BODY>"
		Response.Write loAPD.GetAPDDocumentXML(lsXMLCall, lsStyleSheet)
		Response.Write "</BODY></HTML>"
	else
		select case lsDocType
			case ".jpg", ".jpeg"
				lsFormat="image/jpeg"
			case ".tif", ".tiff"
				lsFormat="image/tiff"
			case ".gif"
				lsFormat="image/gif"
			case ".bmp"
				lsFormat="image/bmp"
			case ".pdf"
				lsFormat="application/pdf"
			case ".doc"
				lsFormat="application/msword"
		end select	

		'response.binarywrite loAPD.GetAPDDocument(lsXMLCall) 
		 response.redirect("frmDisplayDocument.aspx?doc="&lsDocument&"&format="&lsFormat&"&Arc="&lsArchived)
	end if
end if
%>

<!--#include file="includes/incCommonBottom.asp"-->

