<!--#include file="includes/incCommonTop.asp"-->
<%
  lsPageName = "frmWaIAAssignmentPrint.asp"
  lsPageTitle = "IA Assignment Print Layout"
%>

<%
  Dim liLynxID, lsGUID, lsGetWebAssignmentCall, lsUserDetailCall, liCarrierRep, liInsCoLynxContactPhone
  
  liLynxID = Request("LynxID")
  lsGUID = Request.Cookies("CPSession")("guidArray0")

  liCarrierRep = Request.Cookies("CPSession")("CarrierRep")
  liInsCoLynxContactPhone = Request.Cookies("CPSession")("InsCoLynxContactPhone")
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

<html>
<head>
<title>Assignment Report</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" type="text/JavaScript">

  function ResizeMe()
  {
  	if(parent)
  		var objIFrame = parent.document.getElementById('prtFrame');
  	else
  		return false;
  
  	var objContainer = document.getElementById("divContainer")
  
  	if(objContainer)
  	{
  		objIFrame.style.height = objContainer.offsetHeight + 60 ; // resize iframe according to the height of its contents
  		top.window.scrollTo(0, 0);
  	}
  	else
  		objIFrame.height = 400; // default size
  } 


</script>


</head>

<body onLoad="ResizeMe();">

<div id="divContainer">

<%
  lsGetWebAssignmentCall = "<GetWebAssignmentXML LynxID=""" & server.urlencode(liLynxID) & """/>"

  Response.Write GetPersistedData(lsGetWebAssignmentCall, lsGUID, "WaIAAssignmentPrintLayout.xsl", "LynxID=" & liLynxID & lcDelimiter & "CarrierRep=" & liCarrierRep & lcDelimiter & "LynxContactPhone=" & liInsCoLynxContactPhone, false)
%>

</div>

</body>
</html>
