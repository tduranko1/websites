<%@ Language=VBScript %>
<%
  Function ClearCookies(sCookie)
  Dim x,y
    If Request.Cookies(sCookie).HasKeys Then
      For Each y in Request.Cookies(sCookie)
        Response.Cookies(sCookie)(y)= ""
        Response.Cookies(sCookie).Expires = Now()-1
      Next
    Else
      Response.Cookies(sCookie) = ""
      Response.Cookies(sCookie).Expires = Now()-1
    End If
  End Function

  ClearCookies("CPSession")
  ClearCookies("FileUploadParams")

  If Request("NL") = "1" Then
    Response.Redirect "frmLogin.asp"
  Else
    Response.Redirect "frmLogin.asp"
  End If
%>
