<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incMail.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
  Dim liUpdateResult, lsShopEmail, liShopQualified
  Dim liShopID, lsShopName, lsShopAddress1, lsShopAddress2, lsShopCity, lsShopState, liShopZip, lsShopContact
  Dim lsShopContactPhone, lsShopFax, lsShopEstPkg, docXMLemail, lsEmailStrXML, lsEmailRetVal
  
  liUpdateResult = Request.Form("updateResult")
  lsShopEmail = Request.Form("ShopEmail")
  liShopQualified = Request("ShopQualified")
  liShopID = Request.Form("ShopID")
  lsShopName = Request.Form("ShopName")
  lsShopAddress1 = Request.Form("ShopAddress1")
  lsShopAddress2 = Request.Form("ShopAddress2")
  lsShopCity = Request.Form("ShopCity")
  lsShopState = Request.Form("ShopState")
  liShopZip = Request.Form("ShopZip")
  lsShopContact = request("ShopContact")
  lsShopContactPhone = request("ShopContactPhone")
  lsShopFax = request("ShopFax")
  lsShopEstPkg = request("ShopEstPkg")

  Set docXMLemail = Server.CreateObject("MSXML2.DOMDocument")
  docXMLemail.Async = False
  
  lsEmailStrXML = "<Config Path='ClaimPoint/ShopSignUp/NotificationEmail'/>"
  docXMLemail.LoadXML GetData(lsEmailStrXML, "", "")
  lsEmailRetVal = docXMLemail.documentElement.text
  
  Set docXMLemail = Nothing
%>


<html>
<head>
<title>LYNXSelect Program Registration</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">

<script language="JavaScript" type="text/JavaScript">

function cancelData()
{
  top.location = 'frmCFShopLogin.asp';
}


// Popup About page with application details
function callAbout()
{
  if (window.event.ctrlKey)
  {
    var lsDimensions = "dialogHeight:300px; dialogWidth:380px; "
    var lsSettings = "scroll:no; resizable:yes; status:no; help:no; center:yes; unadorned:yes;"
    var lsQuery = "frmAbout.asp";
    window.showModalDialog( lsQuery, window, lsDimensions + lsSettings );
  }
}

</script>

</head>

<BODY LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 background="./images/page_bg.jpg">

<div align="left">

	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
		<tr>
			<td width="169" align="left" valign="top">
  			<IMG SRC="images/logo_corner.jpg" ALT="" width="169" height="202">
			</td>
			<td align="left" valign="top">

        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" background="images/header_bg.jpg" height="132">
        	<tr>
        		<td height="50">&nbsp;</td>
        		<td align="right" valign="top" height="50">&nbsp;</td>
        	</tr>
        	<tr>
        		<td colspan=2 valign="top">&nbsp;</td>
        	</tr>
        	<tr>
        		<td align="left" valign="bottom" nowrap width="50%">
              <p class="header">
              LYNXSelect<sup style="font-size: 9px;">TM</sup> Registration for 
              <span id="spanlsPageTitle" lang="en-us"><em>CertifiedFirst </em><sup style="font-size: 9px;">TM</sup> Network</span>
            </td>
        		<td align="right" valign="bottom" width="50%">&nbsp;</td>
        	</tr>
        </table>
        <br>

        <%
          If liShopQualified = "1" Then
        %>

          <br/><br/>
          
          <p class="login">
          Your shop has been enrolled into the LYNXSelect Collision Repair Program.
          </p>

        <%
          ElseIf liUpdateResult = "1" Then
          
            Dim sToEmail, sFromEmail, sSubject, sBody, sCPSrv, sEnv, sLogonUser

            If Instr(1,strServerName, "DEV") > 0 Then	'Development
              sCPSrv = "http://sgofetemdev1/apd"
              sEnv = "Dev"
            ElseIf Instr(1,strServerName, "STG") > 0 Then		'Staging
              sCPSrv = "https://stgbuyat.ppg.com/apd"
              sEnv = "Stg"
            ElseIf Instr(1,strServerName, "PRD") > 0 Or (uCASE(Left(strServerName,3)) = "BUY") Then	'Production
              sCPSrv = "https://apd.lynxservices.com/apd"
              sEnv = "Prd"
            Else	'default
              sCPSrv = "https://apd.lynxservices.com/apd"
              sEnv = "Dev"
            End if

            If sEnv = "Dev" or sEnv = "Stg" Then
              If instr(lsShopEmail,"@lynxservices.com") > 0 or instr(lsShopEmail,"@ppg.com") > 0 Then
                sToEmail = lsShopEmail
              Else
                sToEmail = ""
              End If
            ElseIf sEnv = "Prd" Then
              sToEmail = lsShopEmail
            End If
            
            sFromEmail = "LynxSelect@lynxservices.com"
            sSubject = "Thanks for enrolling in the LYNXSelect Collision Repair Shop Program" 
            sBody = "<style>body{font-family:Georgia,serif,arial;font-size:11pt;}</style>"_
                    &"<p><img src='" & sCPSrv & "/images/LYNXSelect_ logo.jpg'/></p>"_
                    &"<p>Welcome to the LYNXSelect<sup>sm</sup> Collision Repair Program!</p>"_
                    &"<p>Your application to become part of this exciting program has been processed and approved. Please print the <strong>Background Check Instructions</strong>, complete the authorization form and submit at your earliest convenience.</p>"_
                    &"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & sCPSrv & "/pdf/LYNXSelect Background Check Instructions.pdf'><img src='" & sCPSrv & "/images/pdfdoc.gif' width='32' height='32' hspace='6' border='0' align='middle'/></a><br>"_
                    &"<font size='-2'>Background Check Instructions</font>"_
                    &"<p>LYNX Services now provides auto physical damage claim management services for insurance companies. Participating LYNXSelect repair shops will have the opportunity to receive new customer referrals from this business relationship.</p>"_
                    &"<p><u>Assignment Process</u>: Complete assignment information will be faxed to you in addition to any electronic transmission. There are a number of steps that should be taken to help ensure the success of our program when contacting the policyholder to schedule the appointment:<br>"_
                    &"<ul><li>Be certain to <strong>call within 24 hours</strong> of receiving the assignment from LYNX Services.</li>"_
                    &"<li><strong>Identify</strong> yourself by <strong>your shop name</strong>.</li>"_
                    &"<li><strong>Provide excellent service</strong> and adhere to the LYNXSelect program requirements.</li></ul></p>"_
                    &"<p>Please expedite your payment processing by electronically submitting final estimates and related repair documents to LYNX Services promptly.</p>"_
                    &"<p><u>ADP and CCC designated users</u>: all new assignments will be transmitted to you through ADP's Shoplink or CCC's Eznet and you will also receive a fax copy of the assignment.</p>"_
                    &"<p><u>Mitchell designated users</u>: new assignments will be sent to you by fax only at this time. We are currently working with Mitchell to provide you with electronic connectivity in the future.</p>"_
                    &"<p>A sample copy of a LYNXSelect assignment is enclosed for your review. You will note that this assignment may differ from others you receive in that the second page contains specific handling instructions for the particular claim that may be unique to the insurance company.</p>"_
                    &"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & sCPSrv & "/pdf/LYNXSelect Shop Assignment Sample.pdf'><img src='" & sCPSrv & "/images/pdfdoc.gif' width='32' height='32' hspace='6' border='0' align='middle'/></a><br>"_
                    &"<font size='-2'>Shop Assignment Sample</font>"_
                    &"<p>LYNX Services is committed to processing payment requests and mailing a check to you within 15 business days after receiving your final estimate. We appreciate your understanding as we develop an Electronic Funds Transfer payment option. If you have a payment status inquiry, you may call (239)479-5800.</p>"_
                    &"<p>We hope you are as excited as we are about this new business opportunity. If you have any questions regarding the LYNXSelect Collision Repair program, please contact us via email at lynxselect@lynxservices.com or call our Participant Services Representatives at 239-479-6043.</p>"_
                    &"<p>Sincerely,</p>"_
                    &"<p>Paul R. McFarland<br>"_
                    &"Director, Programs Administration<br>"_
                    &"LYNX Services</p>"_
                    &"<p></p>"_
                    &"<p>Note: You will need a PDF Viewer to view the PDF files in this e-mail. If you don't have a viewer, you can download it from the Adobe website by clicking on this logo. <a href='http://www.adobe.com/products/acrobat/readstep2.html'><img src='http://www.adobe.com/images/get_adobe_reader.gif' border='0' alt='Download PDF Viewer'></a></p>"

            If sToEmail <> "" Then
              SendMail sToEmail, sFromEmail, sSubject, sBody , "" , ""
            End If
            
            If sEnv = "Dev" or sEnv = "Stg" Then
              If instr(lsShopEmail,"@lynxservices.com") > 0 or instr(lsShopEmail,"@ppg.com") > 0 Then
                sToEmail = lsShopEmail
              Else
                sToEmail = ""
              End If
            ElseIf sEnv = "Prd" Then
              sToEmail = lsEmailRetVal
            End If

            sFromEmail = "LynxSelect@lynxservices.com"
            sSubject = "CertifiedFirst ID# " & liShopID & " Enrollment Submitted" 
            sBody = "<style>body{font-family:Georgia,serif,arial;font-size:11pt;}</style>"_
                    &"<p>The following CertifiedFirst shop has submitted their enrollment.</p>"_
                    &"<p><strong>Submitted on:</strong> " & Now() &"</p>"_
                    &"<p><strong>CertifiedFirst ID:</strong> " & liShopID &"</p>"_
                    &"<p><strong>Shop Name:</strong> " & lsShopName &"<br>"_
                    &"<strong>Shop Address:</strong> " & lsShopAddress1 & "&nbsp;&nbsp;" & lsShopAddress2 &"<br>"_
                    &"<strong>City:</strong> " & lsShopCity & " &nbsp;&nbsp;<strong>State:</strong> " & lsShopState & " &nbsp;&nbsp;<strong>Zip:</strong> " & liShopZip &"</p>"_
                    &"<p><strong>Shop Contact:</strong> " & lsShopContact &"<br>"_
                    &"<strong>Phone:</strong> " & lsShopContactPhone &"<br>"_
                    &"<strong>Fax:</strong> " & lsShopFax &"</p>"_
                    &"<p><strong>Preferred Estimate Package:</strong> " & lsShopEstPkg &"</p>"

            If sToEmail <> "" Then
              SendMail sToEmail, sFromEmail, sSubject, sBody , "" , ""
            End If
        %>

          <br/><br/>
          
          <p class="login">
          Your enrollment into the LYNXSelect Collision Repair Program has been successfully submitted.
          </p>
          <p class="login">
          An email acknowledgement and request for some additional information has been sent by the LYNXSelect Program Administrator.
          Please check your email as soon as possible.
          </p>
          <p class="login">
          Thank you for your time and interest in LYNXSelect.
          We look forward to teaming with the <em>CertifiedFirst</em> Network program, and to exciting future opportunities.
          </p>
          <p class="login">
          Return to the <a href="frmCFShopLogin.asp" target="_top">Login</a> screen.
          </p>

        <%
          Else
        %>

          <table>
            <tr>
              <td nowrap="">
                <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
              </td>
              <td colspan="4" nowrap="" style="color:#FF0000; font-size:11pt; font-weight:bold;">
                Submit Error!
              </td>
            </tr>
            <tr>
              <td nowrap=""> </td>
              <td nowrap=""><br/>
              <p class="login">
              There has been a problem sending your <em>CertifiedFirst</em> Network shop information to LYNX Services.
              </p>
              <p class="login">
              <a href="javascript:history.go(-2)">Return</a> to the form and resubmit it or
              return to the <a href="frmCFShopLogin.asp" target="_top">Login</a> screen.
              </p>
              </td>
            </tr>
          </table>
        
        <%
          End If
        %>
  
          <br>
          <p class="login">
          For questions regarding the LYNXSelect Program or this enrollment process, please email us at <a href="mailto:lynxselect@lynxservices.com?subject=Questions regarding the LYNXSelect Program">lynxselect@lynxservices.com</a>.
          </p>
          <p class="login">
          For questions regarding the <em>CertifiedFirst</em> Network Program, please call 866-CERT-1ST.
          </p>

		</td>
	</tr>
</table>

</div>

<br>
<br>
<br>

<!--#include file="includes/incFooter.asp"-->

</body>
</html>
