<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->

<%
Dim lsBrowser
lsPageName = "getReport.asp"
liPageType = lcAfterInsuranceCompanySelect
lsBrowser = Request.ServerVariables("HTTP_USER_AGENT")


if liViewReport <> 1 then
  response.write "You don't have permission to view Reports. Please contact your supervisor."
  response.end
end if

%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->

  <html>
  <head>
    <title><%=Request("friendlyName")%></title>
        <style>
            .toolbarButton {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: buttonface;
              border:0px;
            }
            .toolbarButtonOver {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: buttonface;
              border-top:1px solid #FFFFFF;
              border-left:1px solid #FFFFFF;
              border-bottom:1px solid #696969;
              border-right:1px solid #696969;
            }
        
            .toolbarButtonOut {
              width: 24px;
              height: 24px;
              cursor:arrow;
              background-color: buttonface;
              border:0px;
            }
        </style>
    <script language="javascript">
      var zoomVal = 100;
      var sFriendlyName = "<%=Request("friendlyName")%>";
      var pdf1 = null;
      var sFormActionView = "frmGetReportPDF.asp?mode=view";
      var sFormActionSave = "frmGetReportPDF.asp?mode=save";
      var bReportPDFLoading = false;
      function pageInit(){
        try {
          bReportPDFLoading = false;
// HQCL-3898 - view report on separate window issue fix - start
         // if (window == window.top)
           // btnClose.style.display = "inline";
// HQCL-3898 - view report on separate window issue fix - end
          document.getElementById("frmGetReport").action = sFormActionView;
          document.getElementById("frmGetReport").submit();
          bReportPDFLoading = true;
        } catch (e) {
        }
      }
      
      function SaveLocal(){
        try {
          //if (pdf1) {
            bReportPDFLoading = false;
            btnSaveLocal.disabled = true;
            var sFileName = sFriendlyName; //parent.sReportFriendlyName;
            frmGetReport.action = sFormActionSave;
            frmGetReport.submit();
            frmGetReport.action = sFormActionView;
            bReportPDFLoading = true;
          //}
        } catch (e) {}
        finally {
          btnSaveLocal.disabled = false;
        }
      }
      
      function printDoc() {
        try {
          if (pdf1)
            pdf1.Print();
        } catch (e) {
        }
      }
      function zoomIn(){
        if (pdf1)
          if (zoomVal < 600) {
              zoomVal += 50;
              pdf1.setZoom(zoomVal);
              pdf1.focus();
          }
      }

      function zoomOut(){
        if (pdf1)
          if (zoomVal > 20) { 
              zoomVal -= 50;
              pdf1.setZoom(zoomVal);
              pdf1.focus();
          }
      }
      function tb_onmouseover(obj){
          obj.className = "toolbarButtonOver";
      }
      
      function tb_onmouseout(obj){
          obj.className = "toolbarButtonOut";
      }
      function goToPage(val){
        if (pdf1) {
          switch(val){
              case 1:
                  pdf1.gotoFirstPage();
                  break;
              case 2:
                  pdf1.gotoPreviousPage();
                  break;
              case 3:
                  pdf1.gotoNextPage();
                  break;
              case 4:
                  pdf1.gotoLastPage();
                  break;
          }
          pdf1.focus();
        }
      }
      
      function reportReady(){
        try {
          if (iFrmReportPDF.frameElement.readyState == "complete" && bReportPDFLoading == true) {
            pdf1 = iFrmReportPDF.document.body.firstChild;
            pdf1.setShowToolbar(false);
          }
        } catch (e) {
        }
      }
      
    </script>
  </head>
  <body style="border:0px solid #C0C0C0;margin:0px;padding:0px;overflow:hidden" onload="pageInit()" oncontextmenu="return false">
    <table border="0" cellspacing="0" cellpadding="0" style="width:100%;height:100%">
      <%if instr(lsBrowser, "MSIE") > 0 then%>
      <tr style="height:21px;">
        <td style="background-color:buttonface">
          <table border="0" cellspacing="0" cellpadding="0" style="width:100%">
            <colgroup>
              <col width="*"/>
              <col width="21px" style="align:right"/>
            </colgroup>
            <tr>
              <td>
                <button hidefocus="true" id="btnSaveLocal" tabIndex="-1" tabStop="false" onclick="SaveLocal()" title="Save a local copy" class="toolbarButton" onmouseover="tb_onmouseover(this);" onmouseout="tb_onmouseout(this);"><img src="images/SaveCopy.gif" width="17" height="14" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"/></button>
<%if Request("reportFormat") = "PDF" then%>
                <button hidefocus="true" id="btnPrint" tabIndex="-1" tabStop="false" onclick="printDoc()" title="Print Document" class="toolbarButton" onmouseover="tb_onmouseover(this);" onmouseout="tb_onmouseout(this);"><img src="images/print.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"></button>
                <img src="images/spacer.gif" width="4px"/>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomIn()" title="Zoom In" class="toolbarButton" onmouseover="tb_onmouseover(this);" onmouseout="tb_onmouseout(this);"><img src="images/zoomin.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"></button>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="zoomOut()" title="Zoom Out" class="toolbarButton" onmouseover="tb_onmouseover(this);" onmouseout="tb_onmouseout(this);"><img src="images/zoomout.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"></button>
                <img src="images/spacer.gif" width="4px"/>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(1)" title="First Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&lt;&lt;</button>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(2)" title="Previous Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&lt;</button>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(3)" title="Next Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&gt;</button>
                <button hidefocus="true" tabIndex="-1" tabStop="false" onclick="goToPage(4)" title="Last Page" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)">&gt;&gt;</button>
<%end if%>
              </td>
              <td>
                <button id="btnClose" hidefocus="true" tabIndex="-1" tabStop="false" onclick="window.close()" title="Close Window" class="toolbarButton" onmouseover="tb_onmouseover(this)" onmouseout="tb_onmouseout(this)" style="display:none"><img src="images/close.gif"/ width="18" height="18" border="0" style="FILTER:alpha(opacity=60); cursor:hand;"></button>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <% end if %>
      <tr>
        <td>
          <iframe src="frmReportLoading.asp" id="iFrmReportPDF" name="iFrmReportPDF" onreadyStateChange="reportReady()" style="width:100%;height:100%;border:0px;">
          </iframe>
        </td>
      </tr>
    </table>
  <form name="frmGetReport" id="frmGetReport" method="post" target="iFrmReportPDF">
    <input type="hidden" name="reportFileName" id="reportFileName" value="<%=Request("reportFileName")%>"/>
    <input type="hidden" name="storedProc" id="storedProc" value="<%=Request("storedProc")%>"/>
    <input type="hidden" name="reportParams" id="reportParams" value="<%=Request("reportParams")%>"/>
    <input type="hidden" name="staticParams" id="staticParams" value="<%=Request("staticParams")%>"/>
    <input type="hidden" name="friendlyName" id="friendlyName" value="<%=Request("friendlyName")%>"/>
    <input type="hidden" name="reportFormat" id="reportFormat" value="<%=Request("reportFormat")%>"/>
  </form>
  </body>
  </html>
