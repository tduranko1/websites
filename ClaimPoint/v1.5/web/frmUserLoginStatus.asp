<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incCryptTools.asp"-->

<%
  lsPageName = "frmUserLoginStatus.asp"

  Dim loAware, iResult, sADGroupName, bLockedOut, sUserName, lsAccountStatus, liUnlockStatus, lsAccountUnlockStatus

  sUserName = Request("UserName")

  sADGroupName = "GOFWebLynxAPD"

  Set loAware = Server.CreateObject("AWARE.clsAWARE")

  If Request.Form("realSubmit") = "fromButtonCheck" Then
    bLockedOut = loAware.IsAccountLockedOut(sUserName)

    If bLockedOut = true Then
      lsAccountStatus = "locked"
    Else
      lsAccountStatus = "unlocked"
    End If

  ElseIf Request.Form("realSubmit") = "fromButtonUnlock" Then
    lsUserEmail = fDecrypt("LYNXAPDKEY", Request.Cookies("CPSession")("authUser"))
    liUnlockStatus = loAware.UnlockAccount(sUserName,lsUserEmail)

    If liUnlockStatus = 2 Then
      lsAccountUnlockStatus = "Account has been unlocked."
    ElseIf liUnlockStatus = 1 Then
      lsAccountUnlockStatus = "Account not found. Please report it to your supervisor."
    Else
      lsAccountUnlockStatus = "Error unlocking account. Please report it to your supervisor."
    End If

  End If

  Set loAware = Nothing

%>

<html>
  <head>
  </head>
  <body leftmargin="0">

<p style="FONT-FAMILY:Arial,sans serif; FONT-SIZE:10pt;">


<%
  If Request.Form("realSubmit") = "fromButtonCheck" Then
%>

    <form NAME="frmLogin" METHOD="post" ACTION="frmUserLoginStatus.asp">
    
    Account is <%= lsAccountStatus %>. 

<%
    If bLockedOut = true Then
%>

    	<input type="hidden" name="realSubmit" value="fromButtonUnlock">
    	<input type="hidden" name="UserName" value="<%= sUserName %>">
      <input type="submit" name="check" id="check" value="Unlock account">
    </form>
<%
    End If

  ElseIf Request.Form("realSubmit") = "fromButtonUnlock" Then
%>

  <%= lsAccountUnlockStatus %>

<%
  ElseIf bLockedOut = "" Then
%>

  <form NAME="frmLogin" METHOD="post" ACTION="frmUserLoginStatus.asp">
  	<input type="hidden" name="realSubmit" value="fromButtonCheck">
  	<input type="hidden" name="UserName" value="<%= sUserName %>">
    <input type="submit" name="check" id="check" value="Check login status">
  </form>

<%
  End If
%>

</p>

  </body>
</html>