<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->

<%
  lsPageName = "frmMyClaimsDesktop.asp"
  lsPageTitle = "My Claims Desktop"
  liPageType = lcAfterInsuranceCompanySelect
  bGridHover = True
  lsHelpPage = "CP_My_Claims_Desktop.htm"

  Response.Cookies("CPSession")("currentClaim") = ""
  Response.Cookies("CPSession")("currentClaim").Expires = Now()-1
  Response.Cookies("CPSession")("CurrAssignmentDescription") = ""
  Response.Cookies("CPSession")("CurrAssignmentDescription").Expires = Now()-1
  Response.Cookies("CPSession")("CurrAssignmentId") = ""
  Response.Cookies("CPSession")("CurrAssignmentId").Expires = Now()-1

  ' 19Jan2012 - TVD - Elephant Custom Scripting
  Response.Cookies("CPSession")("CurrAuthType") = ""
  Response.Cookies("CPSession")("CurrAuthType").Expires = Now()-1

  Response.Cookies("CPSession")("CurrAssignmentReserveLineType") = ""
  Response.Cookies("CPSession")("CurrAssignmentReserveLineType").Expires = Now()-1
  Response.Cookies("CPSession")("CurrAssignmentEmergencyLiability") = ""
  Response.Cookies("CPSession")("CurrAssignmentEmergencyLiability").Expires = Now()-1
  Response.Cookies("CPSession")("CurrAssignmentSB511") = ""
  Response.Cookies("CPSession")("CurrAssignmentSB511").Expires = Now()-1

  Response.Cookies("CPSession")("currentFileUploadPage") = ""
  Response.Cookies("CPSession")("currentFileUploadPage").Expires = Now()-1
  Response.Cookies("CPSession")("APDClaim") = ""
  Response.Cookies("CPSession")("APDClaim").Expires = Now()-1
  Response.Cookies("FileUploadParams") = ""
  Response.Cookies("FileUploadParams").Expires = Now()-1
%>

<!--#include file="includes/incCPCryptTool.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->

<%
  Dim lsInsCoDetailsCall, lsCarrierOfficesListCall, lsPrevSearchField, lsPrevSearchValue
  Dim strAwareMessage, lsFirstname, lsLastName, lsInsuranceCompanyID

  lsPrevSearchField = Request.Cookies("CPSession")("lastSearchField")
  lsPrevSearchValue = Request.Cookies("CPSession")("lastSearchValue")
  strAwareMessage = Request.Cookies("CPSession")("AwareMessage")
  Response.Cookies("CPSession")("AwareMessage") = ""
  lsFirstname = lsUserNameFirst
  lsLastName = lsUserNameLast
  lsInsuranceCompanyID = Request.Cookies("CPSession")("InsuranceCompanyID")
%>


<script language="vbscript">
Function Encryption(ByVal Message, ByVal Password)
        Dim MessageLength
        Dim PasswordLength
        Dim Index
        Dim Ascii
        Dim Key
        Dim NewAscii
        Dim Encrypted

        If Message = "" Or Password = "" Then
            Encrypt = Message
            Exit Function
        End If

        ' Get lengths of message and password
        MessageLength = Len(Message)
        PasswordLength = Len(Password)

        ' loop through each character in message
        For Index = 1 To MessageLength
            ' Get ascii value of message character
            Ascii = Asc(Mid(Message, Index, 1))

            ' Get ascii value of key at correct position
            Key = Asc(Mid(Password, (Index Mod PasswordLength + 1), 1))

            ' Add key to ascii
            NewAscii = Ascii + Key

            ' Working with bytes, loop to low numbers if below zero
            If NewAscii > 255 Then NewAscii = NewAscii - 255

            ' apend ascii as a double hex character to encrypted message
            Encrypted = Encrypted & Right("0" & Hex(NewAscii), 2)
        Next

        ' Return the encrypted message	
        Encryption = Encrypted
    End Function

</script>
<!--17Aug2021 - TVD v1.0 - Chrome related issue fix-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<script language="JavaScript">

  var strAwareMessage = "<%=replace(strAwareMessage, """", "")%>";
  
  if  (strAwareMessage != ""){
      var strDays = strAwareMessage.indexOf(" ") != -1 ? strAwareMessage.split(" ")[0] : "";
      var answer = confirm("Your password will expire in " + strDays + " day(s). Would you like to change it now?");
      if (answer) {
         top.location = "frmChangePassword.asp";
      }
  }

  function doShopSearch()
  {
    document.getElementById("shopSrhFrame").style.height = "0";
    document.getElementById("shopSrhFrame").style.width = "600";
    document.frmShopSearchForm.submit();
    document.getElementById('shopSrhFrame').style.display = "inline";
  }

  
  function doClaimView()
  {
    var liClaimViewLevel = <%=liClaimViewLevel%>;
    var liConstClaimViewLevel = <%=liClaimViewLevel_All%>;
    var liConstClaimViewLevel_Own = <%=liClaimViewLevel_Own%>;
    var liConstClaimViewLevel_Office = <%=liClaimViewLevel_Office%>;
    var liCarrierRep = <%=liCarrierRep%>;
    var lsSelClaimTypeValue = document.getElementById('AssignmentType');
    var lsFName = '<%= Replace(lsFirstname,"'","\'")%>';
    var lsLName = '<%= Replace(lsLastName,"'","\'")%>'; 
    var lsStatus = document.getElementById('txtSelectionCode');
    var lsDays = document.getElementById('txtDays');
    var lsOffice = document.getElementById('txtOfficeID');
    var lsInsCompanyID = <%=lsInsuranceCompanyID %>;
    var lsUser = document.getElementById('txtUserID'); 
   
    var iframe,iwin,selectedByUser,Office,User,Status,Days;

    if ((lsStatus != null && lsStatus != undefined) && (lsDays != null && lsDays != undefined))
    {
        Status = lsStatus.options[lsStatus.selectedIndex].text;
        Days = lsDays.options[lsDays.selectedIndex].text;
    }
  
    if(liClaimViewLevel == liConstClaimViewLevel)
    {
        if(liCarrierRep == 0)
        {
            iframe = window.parent.parent.document.getElementById('ifrmCarrierUsersList');
            iwin= iframe.contentWindow || iframe.contentDocument.defaultView;
            selectedByUser  = iwin.document.getElementById('txtCarrierUser');
            User = selectedByUser.options[selectedByUser.selectedIndex].text;
            Office = lsOffice.options[lsOffice.selectedIndex].text;
        }
        else
        {
            Office = lsOffice.value;
            User = lsUser.value;
        }
    }
    else if(liClaimViewLevel == liConstClaimViewLevel_Office)
    {
        Office = lsOffice.value;
        User = lsUser.value;
    }
    else
    {
        if(liClaimViewLevel == liConstClaimViewLevel_Own)
        {
            Office = lsOffice.value;
            User = lsUser.value;
        }
    }
    
    var QuerString = "Status=" + Status + "&Office=" + Office + "&User=" + User + "&NoOfDays=" + Days + "&InsuranceCompanyID=" + lsInsCompanyID;
//alert(QuerString);
    //var Encrypted = Encryption(QuerString,"CPWEMROOF"); previous code
    //17Aug2021 - TVD v1.0 - Chrome related issue fix
    var Encrypted = CryptoJS.AES.encrypt(QuerString,"CPWEMROOF");
    // alert(QuerString);
     //  alert(Encrypted);
     //   alert(Decryption(Encrypted,"CPWEMROOF"));

    if (liClaimViewLevel == liConstClaimViewLevel && document.getElementById('txtUserID').value == "")
    {
      alert("Please select a user from the drop-down list.");
      return;
    }   
    else
    {
   // alert("Roofing Else");
        if (lsSelClaimTypeValue != null && lsSelClaimTypeValue != undefined)
        {
	//alert("test");
         if (lsSelClaimTypeValue.value == "Roofing" || lsSelClaimTypeValue.value == "WEM")
             {
		//alert("test");
                //showMsg("Searching Property Claims...");
              //document.getElementById("msgDiv").style.display = "inline";
              //alert("Enter into the Roofing Frame Part");
              if (lsSelClaimTypeValue.value == "Roofing")
              {
              //alert("Roofing");
              document.getElementById("claimViewePropertyFrame").src="../roofing/SearchClaimFromView.aspx?Info="+Encrypted; 
              }
              if (lsSelClaimTypeValue.value == "WEM")
              {
              //alert("WEM");
               document.getElementById("claimViewePropertyFrame").src="../eProperty/SearchClaimFromView.aspx?Info="+Encrypted; 
              }
               // document.getElementById("claimViewePropertyFrame").src="../eproperty/testpage.aspx";      
                //alert(document.getElementById("claimViewePropertyFrame").src);             
              document.getElementById("claimViewePropertyFrame").style.height = "500px";
              document.getElementById("claimViewePropertyFrame").style.width = "700px";      
              document.getElementById('claimViewePropertyFrame').style.display = "inline";
              document.getElementById('shopSrhFrame').style.display = "none";
              document.getElementById('claimSrhFrame').style.display = "none";
              document.getElementById('claimViewFrame').style.display = "none";
              document.getElementById('SecondaryTowingFrame').style.display = "none";
               //document.getElementById("msgDiv").style.display = "none";
            }                  
             else
             {     
		//alert("test");        
                  showMsg("Retrieving Claims...");
                  document.getElementById("msgDiv").style.display = "inline";		
                  document.getElementById("claimViewFrame").style.height = "0";
                  document.getElementById("claimViewFrame").style.width = "600";
  
                  document.frmViewForm.submit();
                  document.getElementById('claimViewFrame').style.display = "inline";
                  document.getElementById('claimViewePropertyFrame').style.display = "none";
             }     
        }
        else
        {
		  //alert("test");
                  showMsg("Retrieving Claims...");
                  document.getElementById("msgDiv").style.display = "inline";
                  document.getElementById("claimViewFrame").style.height = "0";
                  document.getElementById("claimViewFrame").style.width = "600";
  
                  document.frmViewForm.submit();
                  document.getElementById('claimViewFrame').style.display = "inline";
                  document.getElementById('claimViewePropertyFrame').style.display = "none";
        }
       
    }
  }


  function doClaimSearch()
  {
    var lsClaimNumber = document.getElementById('txtClaimNumber').value;
    var liLynxID = document.getElementById('txtLynxID').value;
    var lsInsuredNameLast = document.getElementById('txtInsuredNameLast').value;

    if (lsClaimNumber.length == 0 && liLynxID.length == 0 && lsInsuredNameLast.length == 0)
    {
      alert("Please enter a search criteria in order to search for claims.");
      return;
    }
    else
    {
      showMsg("Searching Claims...");
      document.getElementById("msgDiv").style.display = "inline";
      document.getElementById("claimSrhFrame").style.height = "0";
      document.getElementById("claimSrhFrame").style.width = "600";

      document.frmSearchForm.submit();
      document.getElementById('claimSrhFrame').style.display = "inline";
      document.getElementById('shopSrhFrameProperty').style.display = "none";
    }
  }

  function doClaimSearchProperty(liInsuranceCompanyID)
  {
    var lsClaimNumber = document.getElementById('txtClaimNumber').value;
    var liLynxID = document.getElementById('txtLynxID').value;
    var lsInsuredNameLast = document.getElementById('txtInsuredNameLast').value;
        
    var lsFName = '<%= Replace(lsFirstname,"'","\'")%>';
    var lsLName = '<%= Replace(lsLastName,"'","\'")%>';
   
    if (lsClaimNumber.length == 0 && liLynxID.length == 0 && lsInsuredNameLast.length == 0)
    {
      alert("Please enter a search criteria in order to search for claims.");
      return;
    }
    else
    {
      showMsg("Searching Property Claims...");
      document.getElementById("msgDiv").style.display = "inline";

      if(liInsuranceCompanyID=="184")
      {      
        var ClaimType = document.getElementById('ClaimType').value;       
        if(ClaimType=="Roofing")
            document.getElementById("shopSrhFrameProperty").src="../Roofing/searchclaim.aspx?ClaimID=" +liLynxID + "&ClaimNo=" +lsClaimNumber+ "&LastName="+lsInsuredNameLast + "&Insc=" + liInsuranceCompanyID + "&FName=" + lsFName + "&LName=" + lsLName+"&ClaimType="+ClaimType;        
        else
            document.getElementById("shopSrhFrameProperty").src="../eproperty/searchclaim.aspx?ClaimID=" +liLynxID + "&ClaimNo=" +lsClaimNumber+ "&LastName="+lsInsuredNameLast + "&Insc=" + liInsuranceCompanyID + "&FName=" + lsFName + "&LName=" + lsLName+"&ClaimType="+ClaimType;        
      }
      else
        document.getElementById("shopSrhFrameProperty").src="../eproperty/searchclaim.aspx?ClaimID=" +liLynxID + "&ClaimNo=" +lsClaimNumber+ "&LastName="+lsInsuredNameLast + "&Insc=" + liInsuranceCompanyID + "&FName=" + lsFName + "&LName=" + lsLName+"&ClaimType=WEM";
        
      document.getElementById("shopSrhFrameProperty").style.height = "100%";
      document.getElementById("shopSrhFrameProperty").style.width = "100%";      
      document.getElementById('shopSrhFrameProperty').style.display = "inline";
      document.getElementById('shopSrhFrame').style.display = "none";
      document.getElementById('claimSrhFrame').style.display = "none";
       document.getElementById("msgDiv").style.display = "none";
    }
  }

  function viewResultsReady()
  {
    try
    {
      if (claimViewFrame.frameElement.readyState == "complete")
      {
        var objMsg = document.getElementById("msgDiv");
        if (objMsg)
          objMsg.style.display = "none";
      }
    }
    catch (e) {}
  }

  function srhResultsReady()
  {
    try
    {
      if (claimSrhFrame.frameElement.readyState == "complete")
      {
        var objMsg = document.getElementById("msgDiv");
        if (objMsg)
          objMsg.style.display = "none";
      }
    }
    catch (e) {}
  }

  function showMsg(str)
  {
    window.scrollTo(0,0);
    var objMsg = document.getElementById("msgDiv");
    if (objMsg)
    {
      var objMsgTxt = document.getElementById("msgTxt");
      if (objMsgTxt)
        objMsgTxt.innerHTML = str;
      objMsg.style.display = "inline";
    }
  }

  function hideMsg()
  {
    var objMsg = document.getElementById("msgDiv");
    if (objMsg)
    {
      objMsg.style.display = "none";
    }
  }

  function updUserIDValue()
  {
    document.getElementById('txtUserID').value = window.frames['ifrmCarrierUsersList'].document.getElementById('txtCarrierUser').value;
  }

  function showGoBtn(state)
  {
    if (state == "show")
    {
      gifState = "none";
      btnState = "inline"  
    }
    else
    {
      gifState = "inline";
      btnState = "none"  
    }
    
    if (document.getElementById('animateGif'))
      document.getElementById('animateGif').style.display = gifState;

    if (document.getElementById('ifrmCarrierUsersList'))
      document.getElementById('ifrmCarrierUsersList').style.display = btnState;

    if (document.getElementById('btnGo'))
      document.getElementById('btnGo').style.display = btnState;
  }

   function clickAssignmentCombined(Atype, Adesc,Combined)
  {
  if(Combined == "B")
     document.cookie = "CombinedType=B";
  else if(Combined == "C")
     document.cookie = "CombinedType=C";

      clickAssignment(Atype, Adesc);
  }

  function clickAssignment(Atype, Adesc)
  {
    var liAssignType = Atype;
    var lsAssignDesc = Adesc;
    
    if (liAssignType == <%=liAssignmentType_PSA%>) //Program Shop Assignment
    {
      document.getElementById('txtFrameSrc').value = "ProgramShop";
      document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
      document.getElementById('txtAssignmentId').value = liAssignType;
    }
    else if (liAssignType == <%=liAssignmentType_RRP%>) 
    {
      document.getElementById('txtFrameSrc').value = "RepairReferral";
      document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
      document.getElementById('txtAssignmentId').value = liAssignType;
    }
    else if (lsAssignDesc == 'SecondaryTowing') //Secondary Assignment
    {
      document.getElementById('txtFrameSrc').value = "SecondaryTowing";
      document.getElementById('txtAssignmentDesc').value = "Secondary Towing";
      document.getElementById('txtAssignmentId').value = 5;
    }
    else if (liAssignType == <%=liAssignmentType_ASDA%> || liAssignType == <%=liAssignmentType_DEA%> || liAssignType == <%=liAssignmentType_IAEA%> || liAssignType == <%=liAssignmentType_NPESA%>) //Audit
    {
      document.getElementById('txtFrameSrc').value = "DeskAudit";
      document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
      document.getElementById('txtAssignmentId').value = liAssignType;
    }
    else if (liAssignType == <%=liAssignmentType_ASDR%> || liAssignType == <%=liAssignmentType_DER%> || liAssignType ==  <%=liAssignmentType_IAER%> || liAssignType ==  <%=liAssignmentType_DRSER%> || liAssignType ==  <%=liAssignmentType_SER%>) //Desk Review
    {
      document.getElementById('txtFrameSrc').value = "DeskReview";
      document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
      document.getElementById('txtAssignmentId').value = liAssignType;
    }
    else if (liAssignType == <%=liAssignmentType_IAFA%> || liAssignType == <%=liAssignmentType_IAPO%> || liAssignType == <%=liAssignmentType_IAACV%>) //IA Assignment
    {
      document.getElementById('txtFrameSrc').value = "IA";
      document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
      document.getElementById('txtAssignmentId').value = liAssignType;
    }
    else if (liAssignType == "<%=liAssignmentType_TL%>") {
      document.getElementById('txtFrameSrc').value = "TotalLoss";
      document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
      document.getElementById('txtAssignmentId').value = liAssignType;
    }
    else if (lsAssignDesc == "Roofing") {   
      document.getElementById('txtFrameSrc').value = "Roofing";
      document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
      document.getElementById('txtAssignmentId').value = liAssignType;      
    } 
     else if (liAssignType == "<%=liAssignmentType_CSA%>") {
      document.getElementById('txtFrameSrc').value = "ChoiceShop";
      document.getElementById('txtAssignmentDesc').value = lsAssignDesc;
      document.getElementById('txtAssignmentId').value = liAssignType;
    }
    else
      return;
    
    document.frmAssignment.submit();   
  }

  
  function carrierOfficesSelect()
  {
    showGoBtn("hide");
    var liClaimViewLevel = <%=liClaimViewLevel%>;
    var loSel = document.getElementById('txtOfficeID');
    var liOffice = loSel.options[loSel.selectedIndex].value;
    var liUserID = "0";
    document.getElementById('txtUserID').value = liUserID;

    document.getElementById('ifrmCarrierUsersList').src = "frmCarrierUsersList.asp?OfficeID=" + liOffice + "&ClaimViewLevel=" + liClaimViewLevel + "&UserID=" + liUserID;
  }
  

  function setDefaultStatus()
  {
    var loSel = document.getElementById('txtSelectionCode');
    if (loSel.options[loSel.selectedIndex].value == "D")
    {
      var sel = document.getElementById('txtDays');
      var liCurr = sel.options.length;
      for (var j=liCurr; j>0; j--)
        sel.selected = "";
      sel.options[0].selected = "true";
      sel.disabled = "true";
    }
    else
      document.getElementById('txtDays').disabled = "";
  }
  
  
  function showViewTab()
  {
    if (document.getElementById('tabView').className == "tabSel")
      return;

    if (document.getElementById('claimSrhFrame').style.display == "inline")
      document.getElementById('claimSrhFrame').style.display = "none";

    if (document.getElementById('shopSrhFrame') && document.getElementById('shopSrhFrame').style.display == "inline")
      document.getElementById('shopSrhFrame').style.display = "none";

    if (document.getElementById('shopSrhFrameProperty') && document.getElementById('shopSrhFrameProperty').style.display == "inline")
      document.getElementById('shopSrhFrameProperty').style.display = "none";

    document.getElementById('claimViewFrame').style.display = "inline";
    gsHelpSubTopicPage = "CP_View_Tab.htm";

    if (document.getElementById('searchDiv'))
    {
      document.getElementById('tabSearch').className = "tabDesel";
      document.getElementById('searchDiv').style.display = "none";
    }
    if (document.getElementById('assignmentDiv'))
    {
      document.getElementById('tabAssignment').className = "tabDesel";
      document.getElementById('assignmentDiv').style.display = "none";
    }
    if (document.getElementById('viewDiv'))
    {
      document.getElementById('tabView').className = "tabSel";
      document.getElementById('tabView').style.color = "#000099";
      document.getElementById('viewDiv').style.display = "inline";
    }
  }
  
  
  function showSearchTab()
  {
    if (document.getElementById('tabSearch').className == "tabSel")
      return;
    
    if (document.getElementById('claimViewFrame') && document.getElementById('claimViewFrame').style.display == "inline")
      document.getElementById('claimViewFrame').style.display = "none";

    if (document.getElementById('shopSrhFrame') && document.getElementById('shopSrhFrame').style.display == "inline")
      document.getElementById('shopSrhFrame').style.display = "none";

    if (document.getElementById('claimViewePropertyFrame') && document.getElementById('claimViewePropertyFrame').style.display == "inline")
      document.getElementById('claimViewePropertyFrame').style.display = "none";

    if (document.getElementById('shopSrhFrameProperty') && document.getElementById('shopSrhFrameProperty').style.display == "inline")
      document.getElementById('shopSrhFrameProperty').style.display = "none";

    document.getElementById('claimSrhFrame').style.display = "inline";
    gsHelpSubTopicPage = "CP_Search_Tab.htm";

    if (document.getElementById('viewDiv'))
    {
      document.getElementById('tabView').className = "tabDesel";
      document.getElementById('viewDiv').style.display = "none";
    }
    
    if (document.getElementById('assignmentDiv'))
    {
      document.getElementById('tabAssignment').className = "tabDesel";
      document.getElementById('assignmentDiv').style.display = "none";
    }
    
    if (document.getElementById('searchDiv'))
    {
      document.getElementById('tabSearch').className = "tabSel";
      document.getElementById('tabSearch').style.color = "#000099";
      document.getElementById('searchDiv').style.display = "inline";
    }
  }
  
  
  function showAssignmentTab()
  {
    if (document.getElementById('tabAssignment').className == "tabSel")
      return;
    
    if (document.getElementById('claimSrhFrame').style.display == "inline")
      document.getElementById('claimSrhFrame').style.display = "none";
    
    if (document.getElementById('claimViewFrame') && document.getElementById('claimViewFrame').style.display == "inline")
      document.getElementById('claimViewFrame').style.display = "none";

    if (document.getElementById('claimViewePropertyFrame') && document.getElementById('claimViewePropertyFrame').style.display == "inline")
      document.getElementById('claimViewePropertyFrame').style.display = "none";

    if (document.getElementById('shopSrhFrameProperty') && document.getElementById('shopSrhFrameProperty').style.display == "inline")
      document.getElementById('shopSrhFrameProperty').style.display = "none";

    gsHelpSubTopicPage = "CP_Assignment_Tab.htm";

    if (document.getElementById('viewDiv'))
    {
      document.getElementById('tabView').className = "tabDesel";
      document.getElementById('viewDiv').style.display = "none";
    }
    
    if (document.getElementById('searchDiv'))
    {
      document.getElementById('tabSearch').className = "tabDesel";
      document.getElementById('searchDiv').style.display = "none";
    }
    
    if (document.getElementById('assignmentDiv'))
    {
      document.getElementById('tabAssignment').className = "tabSel";
      document.getElementById('tabAssignment').style.color = "#000099";
      document.getElementById('assignmentDiv').style.display = "inline";
    }
  }

  function showShopSearchTab()
  {
    if (document.getElementById('tabAssignment').className == "tabSel")
      return;
    
    if (document.getElementById('claimSrhFrame').style.display == "inline")
      document.getElementById('claimSrhFrame').style.display = "none";
    
    if (document.getElementById('claimViewFrame') && document.getElementById('claimViewFrame').style.display == "inline")
      document.getElementById('claimViewFrame').style.display = "none";

    if (document.getElementById('claimViewePropertyFrame') && document.getElementById('claimViewePropertyFrame').style.display == "inline")
      document.getElementById('claimViewePropertyFrame').style.display = "none";

    gsHelpSubTopicPage = "";

    if (document.getElementById('viewDiv'))
    {
      document.getElementById('tabView').className = "tabDesel";
      document.getElementById('viewDiv').style.display = "none";
    }
    
    if (document.getElementById('searchDiv'))
    {
      document.getElementById('tabSearch').className = "tabDesel";
      document.getElementById('searchDiv').style.display = "none";
    }
    
    if (document.getElementById('assignmentDiv'))
    {
      document.getElementById('tabAssignment').className = "tabSel";
      document.getElementById('tabAssignment').style.color = "#000099";
      document.getElementById('assignmentDiv').style.display = "inline";
    }

    if (document.getElementById('shopSrhFrame').style.height == "")
      doShopSearch();
    else
      document.getElementById('shopSrhFrame').style.display = "inline";
  }

  function HLTabOn(obj)
  {
    if (obj.className == "tabSel")
      return;
    obj.style.color = "#FF0000";
  }

  function HLTabOff(obj)
  {
    if (obj.className == "tabSel")
      return;
    obj.style.color = "#000099";
  }

</script>
<style type="text/css">   

    
     .aWaterMitigation
{
    background-image: url('images/btn_SC_WaterRemediation_Update.gif');
    width: 130px;
    height: 28px;
    background-position: left top;
    background-repeat: no-repeat;
    border: 0;
    display:inline-block;
}

    .aWaterMitigation:hover
    {
        background-image: url('images/btn_SC_WaterRemediation_hover_Update.gif');
        width: 130px;
        height: 28px;
        background-position: left top;
        background-repeat: no-repeat;
        border: 0;
    }
    
  .aManagedRoofing
    {
        background-image: url('images/btn_SC_ManagedRoofing.gif');
        width: 130px;
        height: 28px;
        background-position: left top;
        background-repeat: no-repeat;
        border: 0;
        display:inline-block;
    }
    .aManagedRoofing:hover
    {
        background-image: url('images/btn_SC_ManagedRoofing_hover.gif');
        width: 130px;
        height: 28px;
        background-position: left top;
        background-repeat: no-repeat;
        border: 0;        
    }
   
   
</style>
<br>

<DIV style="position:relative; top:-4px; left:20px;">

  <% If liClaimRead = 1 Then %>
    <% If liCarrierRep = 1 or liCarrierRep = 0 and liClaimViewLevel = liClaimViewLevel_All Then %>
        <SPAN id="tabView"
              class="tabSel"
              onClick="showViewTab()"
              title="View Claims"
              onMouseOver="HLTabOn(this); window.status='View Claims'; return true"
              onMouseOut="HLTabOff(this); window.status=''; return true">
          View
        </SPAN>
        &nbsp;
    <% End If %>
  <% End If %>
  <% If liClaimRead = 1 Then %>
        <SPAN id="tabSearch"
              class="tabDesel"
              onClick="showSearchTab()"
              title="Search Claims"
              onMouseOver="HLTabOn(this); window.status='Search Claims'; return true"
              onMouseOut="HLTabOff(this); window.status=''; return true">
          Search
        </SPAN>
        &nbsp;
  <% End If %>
  <% If liClaimCreate = 1 Then %>
    <% If UCase(lsClientOfficeId) = "FCS" Then %>
        <SPAN id="tabAssignment"
              class="tabDesel"
              onClick="showShopSearchTab()"
              title="Search for a Program Shop"
              onMouseOver="HLTabOn(this); window.status='Search for a Program Shop'; return true"
              onMouseOut="HLTabOff(this); window.status=''; return true">
          Shop Search
        </SPAN>
    <% Else %>
        <SPAN id="tabAssignment"
              class="tabDesel"
              onClick="showAssignmentTab()"
              title="Start an Assignment"
              onMouseOver="HLTabOn(this); window.status='Start an Assignment'; return true"
              onMouseOut="HLTabOff(this); window.status=''; return true">
          Assignment
        </SPAN>
    <% End If %>
  <% End If %>

</DIV>
  
<% If liClaimRead = 1 Then %>
  <% If liCarrierRep = 1 or liCarrierRep = 0 and liClaimViewLevel = liClaimViewLevel_All Then %>

    <DIV id="viewDiv">

      <table width="600" border="0" cellspacing="0" cellpadding="0" style="padding:2px; border:1px solid #000099; z-index:2">

        <form name="frmViewForm" id="frmViewForm" method="POST" action="frmCarrierRepDesktop.asp" target="claimViewFrame">

         <tr>
          <td colspan="5"><img border="0" src="images/spacer.gif" width="1" height="4"></td>
        </tr>
         <tr>
          <td nowrap style="width:110px"><p class="bodyBlue">Status:</td>
          <td height="32" nowrap>
            <select name="SelectionCode" id="txtSelectionCode" onChange="setDefaultStatus()">
              <option value="D" SELECTED>Any/All Open</option>
              <option value="O">Open</option>
              <option value="CL">Closed</option>
              <option value="CA">Cancelled</option>
              <option value="V">Voided</option>
              <option value="A">All</option>
            </select>
          </td>
          <td nowrap><p class="bodyBlue">No. of Days:</td>
          <td width="100%" nowrap>
            <select name="Days" id="txtDays">
              <option value="30"SELECTED>30</option>
              <option value="60">60</option>
              <option value="90">90</option>
              <option value="120">120</option>
            </select>
          </td>
          <td></td>
        </tr>
    
    <% If liClaimViewLevel = liClaimViewLevel_Own or liClaimViewLevel = liClaimViewLevel_Office Then %>
    
        <input type="hidden" name="OfficeID" id="txtOfficeID" value="<%=liOfficeID%>">
    
    <% End If %>
    
    <% If liClaimViewLevel = liClaimViewLevel_Own Then %>
    
        <tr>
          <td nowrap>&nbsp;</td>
          <td height="34">&nbsp;</td>
          <td nowrap>&nbsp;</td>
          <td nowrap>&nbsp;</td>
    
    <% Else %>
    
        <tr>
          
      <% If liClaimViewLevel = liClaimViewLevel_All Then %>
    
          <td nowrap><p class="bodyBlue">Office:</td>
          <td height="34" nowrap>
        <%
          lsCarrierOfficesListCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
        
          Response.Write GetPersistedData(lsCarrierOfficesListCall, lsInsCoConfigGUID, "CarrierOfficesList.xsl", "OfficeID=" & liOfficeID, true)
        %>
            <img border="0" src="images/spacer.gif" width="10" height="1">
          </td>
    
      <% Else %>
          
          <td nowrap>&nbsp;</td>
          <td height="34">&nbsp;</td>
    
      <% End If %>
    
      <% If liClaimViewLevel = liClaimViewLevel_Office or liClaimViewLevel = liClaimViewLevel_All Then %>
      
          <td nowrap><p class="bodyBlue">User:</td>
          <td width="100%" nowrap>
            
            <SPAN id="animateGif" style="font-weight:bold; color:#008B8B;">
              Retrieving List...&nbsp;&nbsp;&nbsp;
              <img src="images/loading.gif" alt="" width="78" height="7" border="0">
            </SPAN>
            
            <IFRAME SRC="frmCarrierUsersList.asp?OfficeID=<%=liOfficeID%>&ClaimViewLevel=<%=liClaimViewLevel%>&UserID=<%=liUserID%>" NAME="ifrmCarrierUsersList" ID="ifrmCarrierUsersList" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="width:100%; height:24px; border:0px; display:none">
            </IFRAME>
          </td>
          <td>&nbsp;</td>
      <% Else %>
    
          <td nowrap>&nbsp;</td>
          <td colspan="2" width="100%">&nbsp;</td>
    
      <% End If %>
    
    <% End If %>
    
        </tr>        
        <tr>
      <%   Dim IsWaterMitigationFeature
 	       Dim docXML                  
                  Set docXML = Server.CreateObject("MSXML2.DOMDocument") 
	IsWaterMitigationFeature = "<Config Path=""ClaimPoint/Customizations/InsuranceCompany[@ID='" & liInsuranceCompanyID & "']/@WaterMitigation""/>"
		IsWaterMitigationFeature = GetData(IsWaterMitigationFeature, "", "")   
         
                  docXML.Async = False
                  docXML.loadXML IsWaterMitigationFeature 
                  IsWaterMitigationFeature = docXML.documentElement.text    
      
      Dim IsRoofingFeature	
      Dim docRoofingXML     
      Set docRoofingXML = Server.CreateObject("MSXML2.DOMDocument")       
  	IsRoofingFeature = "<Config Path=""ClaimPoint/Customizations/InsuranceCompany[@ID='" & liInsuranceCompanyID & "']/@Roofing""/>"	 
                  IsRoofingFeature = GetData(IsRoofingFeature, "", "")         
                            
                  docRoofingXML.Async = False
                  docRoofingXML.loadXML IsRoofingFeature
                  IsRoofingFeature = docRoofingXML.documentElement.text      
         
                 %>
            <td><p class="bodyBlue">Claim Type:</td>  
            <%  if Lcase(IsRoofingFeature) = "true" And Lcase(IsWaterMitigationFeature) = "true" then %>  
            <% if liInsuranceCompanyID = 184 then %>
            <td colspan="2">
             <select name="AssignmentType" id="Select1">           
                      <option value=""  SELECTED>* All *</option>    
                      <option value="6">Adverse Subro Desk Audit</option>
                      <option value="7">Demand Estimate Audit</option>
                      <option value="8">IA Estimate Audit</option>
                      <option value="4">Program Shop Assignment</option> 
                      <option value="17">Choice Shop Assignment</option>      
                      <option value="Roofing">Managed Roofing Service</option>
                      <option value="WEM">WEM</option>
                  </select>
            </td>
               <% Else %>
            <td colspan="2">
             <select name="AssignmentType" id="AssignmentType">           
                      <option value=""  SELECTED>* All *</option>    
                      <option value="6">Adverse Subro Desk Audit</option>
                      <option value="7">Demand Estimate Audit</option>
                      <option value="8">IA Estimate Audit</option>
                      <option value="4">Program Shop Assignment</option>       
                      <option value="Roofing">Managed Roofing Service</option>
                      <option value="WEM">WEM</option>
                  </select>
            </td>
            <% End If %> 
            <%Elseif Lcase(IsRoofingFeature) = "true" then %>  
            <td colspan="2">
             <select name="AssignmentType" id="AssignmentType">           
                      <option value=""  SELECTED>* All *</option>    
                      <option value="6">Adverse Subro Desk Audit</option>
                      <option value="7">Demand Estimate Audit</option>
                      <option value="8">IA Estimate Audit</option>
                      <option value="4">Program Shop Assignment</option>                          
                       <option value="Roofing">Managed Roofing Service</option>
                  </select>
            </td>
            <%Elseif Lcase(IsWaterMitigationFeature) = "true" then%>
                    <% if liInsuranceCompanyID = 259 then %>
                    <td colspan="2">
                     <select name="AssignmentType" id="AssignmentType">           
                              <option value=""  SELECTED>* All *</option>    
                              <option value="6">Adverse Subro Desk Audit</option>
                              <option value="7">Demand Estimate Audit</option>
                              <option value="5">IA Estimate Review</option>
                              <option value="4">Program Shop Assignment</option>                          
                              <option value="WEM">WEM</option>
                          </select>
                    </td>    
                    <% Else %>
                    <td colspan="2">
                     <select name="AssignmentType" id="AssignmentType">           
                              <option value=""  SELECTED>* All *</option>    
                              <option value="6">Adverse Subro Desk Audit</option>
                              <option value="7">Demand Estimate Audit</option>
                              <option value="8">IA Estimate Audit</option>
                              <option value="4">Program Shop Assignment</option>                          
                              <option value="WEM">WEM</option>
                          </select>
                    </td>
                    <% End If %>
            <%Else %>
            <td colspan="2">
        <%                  
          lsCarrierOfficesListCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
                   
          Response.Write GetPersistedData(lsCarrierOfficesListCall, lsInsCoConfigGUID, "ClaimTypes.xsl", "OfficeID=" & liOfficeID, true)%>          
       </td>      
            <%End If %>
    
          <td nowrap align="right">
            <a href="JavaScript:doClaimView()"
               title="Retrieve Claims"
               onMouseOver="window.status='Retrieve Claims'; return true"
               onMouseOut="window.status=''; return true">
              <img id="btnGo" border="0" src="images/btn_go.gif" width="52" height="31" style="display:none;">
            </a>
          </td>
          <td>&nbsp;</td>
        </tr>
            
      <% If liCarrierRep = 0 Then %>
            <input type="hidden" name="UserID" id="txtUserID" value="0">
      <% Else %>
            <input type="hidden" name="UserID" id="txtUserID" value="<%=liUserID%>">
      <% End If %>

        </form>

      </table>

    </DIV>

  <% End If %>
<% End If %>

<% If liClaimRead = 1 Then %>

  <DIV id="searchDiv" style="display:none">

    <table width="730" border="0" cellspacing="0" cellpadding="0" style="padding:2px; border:1px solid #000099; z-index:2">

      <form name="frmSearchForm" id="frmSearchForm" method="POST" action="frmClaimSearchResults.asp" target="claimSrhFrame">
        <input type="hidden" name="hidSearchField" id="hidSearchField">
        <input type="hidden" name="hidSearchValue" id="hidSearchValue">

      <tr>
        <td colspan="5"><img border="0" src="images/spacer.gif" width="1" height="4"></td>
      </tr>
      <tr>
        <td nowrap><p class="bodyBlue">Claim Number:</td>
        <td height="32">
          <input type="text" name="ClaimNumber" id="txtClaimNumber" size="30" maxlength="30" onKeyPress="JavaScript:if (event.keyCode == 13) {ldTrim(this); doClaimSearch()}" onBlur="ldTrim(this)">
        </td>
        <td nowrap><p class="bodyBlue">LYNX ID:</td>
        <td width="100%">
          <input type="text" name="LynxID" id="txtLynxID" size="28" maxlength="28" onKeyPress="JavaScript:if (event.keyCode == 13){ldTrim(this); doClaimSearch()}" onBlur="ldTrim(this)">
        </td>
      </tr>
      <tr>
        <td colspan="4" nowrap height="34">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="450">
                <p class="bodyBlue">Insured/Claimant Last Name:&nbsp;
                  <input type="text" name="InsuredNameLast" id="txtInsuredNameLast" size="30" maxlength="30" onKeyPress="JavaScript:if (event.keyCode == 13) {ldTrim(this); doClaimSearch()}" onBlur="ldTrim(this)">
                </p>
              </td>
              <% if liInsuranceCompanyID = 184 then %>
              <td><p class="bodyBlue">Claim Type:&nbsp;              
                  <select name="ClaimType" id="ClaimType">                      
                      <option value="Roofing">Roofing</option>
                      <option value="WEM" SELECTED>WEM</option>
                  </select></p>
              </td>  
              <%Else %>
              <td></td>
              <% end if %>              

               <%  if Lcase(IsRoofingFeature) = "true" Or Lcase(IsWaterMitigationFeature) = "true" then %> 
               <td align="center">
                <a href="JavaScript:doClaimSearch()" style="vertical-align:baseline;"
                   title="Search Claims"
                   onMouseOver="window.status='Search Claims'; return true"
                   onMouseOut="window.status=''; return true">
                  <img border="0" src="images/btn_Search_APD_Update.gif" width="102" height="31">
                </a>
              </td>
              <%Else %>
              <td align="right">
                <a href="JavaScript:doClaimSearch()"
                   title="Search Claims"
                   onMouseOver="window.status='Search Claims'; return true"
                   onMouseOut="window.status=''; return true">
                  <img border="0" src="images/btn_go.gif" width="52" height="31">
                </a>
              </td>
              <%End if %>
            </tr>

            <% if liInsuranceCompanyID = 184 or liInsuranceCompanyID = 259 then %>
            <tr>
                <td>
                
                <br />
                <br />
                </td> 
                <td>
                <br />
                </td>
                <td align="right">
                <a href="JavaScript:doClaimSearchProperty(<%=liInsuranceCompanyID %>)"
                   title="Search Claims"
                   onMouseOver="window.status='Search Property Claims'; return true"
                   onMouseOut="window.status=''; return true">
                  <img border="0" src="images/btn_Property.gif" width="140" height="31">
                </a>
                </td>
            </tr>
            <% end if %>
          </table>

        </td>
      </tr>        
	  
      </form>

    </table>
  
  </DIV>

<% End If %>

<% If liClaimCreate = 1 Then %>

  <DIV id="assignmentDiv" style="display:none;">

    <table width="600" border="0" cellspacing="0" cellpadding="0" style="padding:2px; border:1px solid #000099; z-index:2">

    <%
      If UCase(lsClientOfficeId) <> "FCS" Then
    %>

      <tr>
        <td colspan="4" height="30" nowrap>
          <p class="login">&nbsp;&nbsp;Please select the claim assignment type to submit from the options below:</p>
        </td>
      </tr>

    <%
      End If
    %>

      <tr>

    <%
      If UCase(lsClientOfficeId) = "FCS" Then
    %>

      <form name="frmShopSearchForm" id="frmShopSearchForm" method="POST" action="frmFCSShopSearch.asp" target="shopSrhFrame">
      </form>

        <td colspan="4" height="66" valign="top">
          Please select the type of search desired by marking the appropriate radio button.<br>
          <li><strong>Search by Distance</strong> will return results based on a radius from a central location.<br></li>
          <li><strong>Search by Name</strong> results are based on a likely match of your query.<br></li>

    <%
      Else
    %>

        <td colspan="4" nowrap height="198" valign="Top">
    <%
        lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
      
        Dim lsAssignReturn
        lsAssignReturn = GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "AssignmentTypes.xsl", "CarrierRep=" & liCarrierRep & lcDelimiter & "OfficeID=" & liOfficeID & lcDelimiter & "ClaimViewLevel=" & liClaimViewLevel & lcDelimiter & "ClaimViewLevel_All=" & liClaimViewLevel_All, true)
        
        If lsAssignReturn <> "-1" Then
          Response.write lsAssignReturn
          msgbox(lsAssignReturn)

	  'response.write liInsuranceCompanyID  

	'This code written by Mahes
 	 Dim lsQuerystring,strEncrypted

	liUserID = Request.Cookies("CPSession")("UserID")
        lsUserEmail = Request.Cookies("CPSession")("UserEmail")
        liOfficeID = Request.Cookies("CPSession")("OfficeID")
        lsUserNameFirst = Request.Cookies("CPSession")("NameFirst")
        lsUserNameLast = Request.Cookies("CPSession")("NameLast")
        lsPhoneAreaCode = Request.Cookies("CPSession")("PhoneAreaCode")
        lsPhoneExchangeNumber = Request.Cookies("CPSession")("PhoneExchangeNumber")
        lsPhoneUnitNumber = Request.Cookies("CPSession")("PhoneUnitNumber")
        lsPhoneExtensionNumber = Request.Cookies("CPSession")("PhoneExtensionNumber")
        liInsuranceCompanyID = Request.Cookies("CPSession")("InsuranceCompanyID")
        lsInsuranceCompanyName = Request.Cookies("CPSession")("InsuranceCompanyName")
 	
        lsQuerystring="UserID=" & liUserID & "&UFname=" & lsUserNameFirst & "&ULname=" & lsUserNameLast & "&OfficeId=" & liOfficeID & "&UEmail=" & lsUserEmail & "&PhoneAC=" & lsPhoneAreaCode & "&PhoneEN=" & lsPhoneExchangeNumber & "&PhoneUN=" & lsPhoneUnitNumber & "&PhoneExn=" & lsPhoneExtensionNumber & "&InsCompID=" & liInsuranceCompanyID & "&InsCompName=" & lsInsuranceCompanyName
       
	strEncrypted=Encrypt(lsQuerystring,"CPWEMROOF")        

        
          if Lcase(IsWaterMitigationFeature) = "true" then
           ''response.Write liInsuranceCompanyID
           ''Add Property claim submission button

            %>
                <br />&nbsp;&nbsp;
                <a href="/eProperty/Default.aspx?info=<% =strEncrypted %>" target="_blank"
                   title="Enter Water Mitigation Claim" 
                   onMouseOut="window.status=''; return true" style="text-decoration:none" class="aWaterMitigation">
                  <img border="0" alt="" src="images/Blank.gif" width="130" height="28"/>
                </a>
                 <%
          end if%>
        
          <% if  LCase(IsRoofingFeature) = "true" then
           
            %>	
    
                &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;                 
                <a href="javascript:clickAssignment('<% =strEncrypted %>','Roofing');" target="_self"
                   title="Enter Roofing Claim" style="text-decoration:none" class="aManagedRoofing">                 
			        <img border="0" alt="" src="images/Blank.gif" width="130px" height="28px"/>
                </a>               
               <%
          end if %>
          

       <%   if liInsuranceCompanyID = 374 then
           'Add Towing submission button

            %>
                </td>
              <%
          end if

		  
        Else
          Response.write "Error! Unable to get assignment types."
        End If

      End If
    %>
        </td>      
      </tr>        
    </table>

  </DIV>

<% End If %>

<br>

<IFRAME SRC="blank.asp" NAME="claimViewFrame" ID="claimViewFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="display:none; width:100%; border:0px;"></IFRAME>
<IFRAME SRC="blank.asp" NAME="claimSrhFrame" ID="claimSrhFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="display:none; width:100%; border:0px;"></IFRAME>
<IFRAME SRC="blank.asp" NAME="shopSrhFrame" ID="shopSrhFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="display:none; width:100%; border:0px;"></IFRAME>
<IFRAME SRC="blank.asp" NAME="shopSrhFrameProperty" ID="shopSrhFrameProperty" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="display:none; width:600; border:0px;" scrolling="yes"></IFRAME>
<IFRAME SRC="blank.asp" NAME="secondaryTowingFrame" ID="secondaryTowingFrame"  MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" WIDTH="800px" HEIGHT="1800px" ></IFRAME>
<IFRAME SRC="blank.asp" NAME="claimViewePropertyFrame" ID="claimViewePropertyFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="display:none; width:600; border:0px;" scrolling="yes"></IFRAME>

  <form name="frmAssignment" method="post" action="frmWaMain.asp">
    <input name="FrameSrc" id="txtFrameSrc" type="hidden" value="">
    <input name="AssignmentDesc" id="txtAssignmentDesc" type="hidden" value="">
    <input name="AssignmentId" id="txtAssignmentId" type="hidden" value="">
  </form>

  <div name="msgDiv" id="msgDiv" style="position:absolute;top: 300px; left:200px;width: 100%;height: 100%;z-index:99;display:none;">
    <table width="220" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #808080; background-color:#EEEEEE; border-collapse:collapse;">
      <tr>
        <td height="30" id="msgTxt" name="msgTxt" style="text-align:center; font-size:12px; font-weight:bold; color:#000080;"></td>
      </tr>
      <tr>
        <td height="20" style="text-align:center"><img src="images/loading_anim.gif" alt="" width="94" height="17" border="0"></td>
      </tr>
      <tr>
        <td height="10">&nbsp;</td>
      </tr>
    </table>
  </div>

  <script language="JavaScript" for="window">
<% If liCarrierRep = 0 and liClaimViewLevel <> liClaimViewLevel_All Then %>
    showSearchTab();
<% Else %>
  <% If liCarrierRep = 1 and UCase(lsClientOfficeId) <> "FCS" Then %>
    doClaimView();
  <% Else %>
    setDefaultStatus();
  <% End If %>
<% End If %>

<% If liClaimViewLevel = liClaimViewLevel_Own Then %>
  if (document.getElementById('btnGo'))
    document.getElementById('btnGo').style.display = "inline";
<% End If %>

  </script>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->