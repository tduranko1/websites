<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->

<%
  Dim liLynxID, liAspectID, lsAssignmentType, liVehNumber,sIsClaimPointAddRental

  liInsuranceCompanyID = Request.Cookies("CPSession")("InsuranceCompanyID")
  
  liLynxID = Request("LynxID")
  liVehNumber = Request("VehNumber")
  liAspectID = Request("AspectID")
  lsAssignmentType = Request("AssignmentType")

  lsPageName = "frmVehicleInfo.asp"
  lsPageTitle = "Vehicle Details"
  lsPageSubTitle = "Information"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcVehicle
  lsHelpPage = "CP_Vehicle_Detail.htm"

sIsClaimPointAddRental = IsClaimPointAddRental()

 function IsClaimPointAddRental()
    Dim strServiceURL , getUrl , xmlhttp 
        Set xmlhttp = server.Createobject("MSXML2.XMLHTTP")
        strServiceURL = GetWebServiceURL()
        getUrl = strServiceURL & "/GetApplicationVar?VariableName=ClaimPointAddRental_Flag" 
        xmlhttp.Open "GET",getUrl,false
        xmlhttp.setRequestHeader "Content-Type","application/x-www-form-urlencoded"
        xmlhttp.send 
        IsClaimPointAddRental =  xmlhttp.responseXML.text 
     end function
        
     function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "http://dlynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
        case "STG"
        strmexMonikerString = "http://slynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
        case "PRD"
        strmexMonikerString = "http://lynxdataservice.pgw.local/PGWAPDFoundation/APDService.asmx"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incVerifyCurrentVehicle.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStartClm.asp"-->
<%
dim lbOverwriteExisting
lbOverwriteExisting = false
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspVehicleInfo.asp">
<!--#include file="includes/incHeaderTableWithIcons.asp"-->

<!--#include file="includes/incClaimCommonTop.asp"-->

<%
  Dim lsVehicleAssignmentCall, liAssignmentID

  lsVehicleAssignmentCall = "<VehicleAssignment InsuranceCompanyID=""" & liInsuranceCompanyID & """  LynxID=""" & liLynxID & """  ClaimAspectID=""" & liVehicleID & """/>"
  
  set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML GetData(lsVehicleAssignmentCall, "", "")
  
  If Not docXML.ParseError = 0 Then
    'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
    'Response.Write "<b>Error Description:</b> " & docXML.ParseError.reason & "<br>"
    'Response.Write "<b>Error File Position:</b> " & docXML.ParseError.filepos & "<br>"
    'Response.Write "<b>Error Line:</b> " & docXML.ParseError.line & "<br>"
    'Response.Write "<b>Error Line Position:</b> " & docXML.ParseError.linepos & "<br>"
    'Response.Write "<b>Error Source Text:</b> " & docXML.ParseError.srcText & "<br>"
  Else
    Set Node = docXML.documentElement.selectSingleNode("Assignment") 
    liAssignmentID = Node.getAttribute("AssignmentID")
    Set Node = Nothing
  End If
  
  Set docXML = Nothing
%>

<%
Response.Write GetPersistedData(lsVehicleDetailCall, lsVehicleDetailGUID, "VehicleNavBar.xsl", "currentArea=" & lsPageSubTitle & lcDelimiter & "AssignmentID=" & liAssignmentID & lcDelimiter & "AssignmentType=" & lsAssignmentType & lcDelimiter & "AspectID=" & liAspectID & lcDelimiter & "LynxID=" & liLynxID & lcDelimiter & "VehNumber=" & liVehNumber, false)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
  Response.Write GetPersistedData(lsVehicleDetailCall, lsVehicleDetailGUID, "VehicleHeader.xsl", "AspectID=" & liAspectID, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

<%
Response.Write GetPersistedData(lsVehicleDetailCall, lsVehicleDetailGUID, "VehicleInfo.xsl", "", true)
%>
<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>
<form name="frmNewVehicle" id="frmNewVehicle" method="POST" action="rspClaimAddVehicle.asp" >
  <input type="hidden" name="hidLynxID" id="hidLynxID" value="<%=liLynxID%>"/>
</form>

<script>
var sIsClaimPointAddRental = "<%=sIsClaimPointAddRental%>";
    if( <%= liInsuranceCompanyID %> == 184 && sIsClaimPointAddRental == "True"){
        if(document.getElementById("rentalMgtLink")){
            document.getElementById("rentalMgtLink").style.display = "block";
        }
   }

function ShowRentalMgt() {
        var sDimensions = "dialogHeight:600px; dialogWidth:1100px; ";
        var sSettings = "center:Yes; help:No; resizable:No; status:No; unadorned:Yes;";
        var sUrl = "RentalCoverage.aspx";
        var sQuery = "?LynxID="+<%=liLynxID%>+"&InscCompID="+<%=liInsuranceCompanyID%>+"&ClaimAspectID="+<%=liAspectID%>

        var strRet = window.showModalDialog(sUrl+sQuery, "", sDimensions + sSettings);
           }
</script>

<!--#include file="includes/incCommonHTMLEndClm.asp"-->

<!--#include file="includes/incCommonBottom.asp"-->