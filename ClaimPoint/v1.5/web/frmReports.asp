<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<%
  lsPageName = "frmReports.asp"
  lsPageTitle = "Reports"
  liPageType = lcAfterInsuranceCompanySelect
  lsHelpPage = "CP_Generating_Reports.htm"

  Response.Cookies("CPSession")("currentClaim") = ""
  Response.Cookies("CPSession")("currentClaim").Expires = Now()-1
  Response.Cookies("CPSession")("APDClaim") = ""
  Response.Cookies("CPSession")("APDClaim").Expires = Now()-1

%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->
<%
  Dim  lsGUID, lsReportsListCall
  lsGUID = Request.Cookies("CPSession")("guidArray0")

  if liViewReport = 1 then

    if liClaimViewLevel = liClaimViewLevel_All or liClaimViewLevel = liClaimViewLevel_Office then
      ' User has View All privilege. Send the Insurance company id so that all reports attached to the insurance is visible
      lsReportsListCall = "<ReportsList UserID=""" + liUserID + """ InsuranceCompanyID=""" + liInsuranceCompanyID + """/>"
    else
      ' User has View Own / View Office privilege. Do not send Insurance company id so that the data will be returned
      ' for the office the user belongs to.
      lsReportsListCall = "<ReportsList UserID=""" + liUserID + """/>"
    end if
    response.write "<pre>" & lsReportsListCall & "</pre>"
    Dim lsReportsList
    lsReportsList = GetPersistedData(lsReportsListCall, lsGUID, "Reports.xsl", "", true)
  
    response.write lsReportsList
  else
    response.write "You don't have permission to view Reports. Please contact your supervisor."
  end if
%>
<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
