<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetCurrAssignment.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!-- v1.5.0.2 -->


<%
 lsPageName = "frmWaShopSelectionAdv.asp"
  lsPageTitle = lsCurrentAssignmentDesc & " Assignment - Shop Search"
  liPageType = lcAssignmentPage
%>

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<%
  Dim lsCity, lsCState, liZipCode, lsShopName, lsShopNameCity, lsShopState, lsShopTypeCD, liMaxShops, lsVehDesc, liVehNum, lsAddVehicle
  Dim lsGUID, lsSearchType, lsShopSearchCall, lsFromPage, liAddVehFromProgShop, liRetToSummary, lsRetVal, lsShopSearchByCityStateCall
  Dim docXML, oNodes, oNode, lsRetCity, lsRetState, lsRetPrimaryZip, lsCityListHTML, liCitiesReturned, lbCitySelected, liCitySelectedZip
  
  

  lsCity = Request("City")
  lsCState = Request("CState")
  liZipCode = Request("ZipCode")
  lsShopName = Request("ShopName")
  lsShopNameCity = Request("ShopNameCity")
  lsShopState = Request("ShopState")
  lsVehDesc = Request("vehDesc")
  lsSearchType = Request("searchType")
  lsShopTypeCD = "P"
  lsGUID = Request.Cookies("CPSession")("guidArray0")
  lsFromPage = Request("fromPage")
  lbCitySelected = Request("CitySelected")
  liCitySelectedZip = Request("CitySelectedZip")
  lsAddVehicle = Request("AddVehicle")

  

  %>

  

  <%
  If lbCitySelected = "true" Then
    liCitiesReturned = 1
    liZipCode = liCitySelectedZip
  End If

  If lsFromPage = "frmClaimSearch" or lsFromPage = "frmAddVehicle" Then
    liVehNum = 1
    liAddVehFromProgShop = 1

  ElseIf lsFromPage = "ShopSelectionAdv" and lsSearchType = "CS" and lbCitySelected <> "true" Then
    liVehNum = Request("vehNum")
    liAddVehFromProgShop = Request("addVeh")

    lsShopSearchByCityStateCall = "<ShopSearchByCityState ShopCity=""" & server.urlencode(lsCity) & """ ShopState=""" & lsCState & """ />"
    lsRetVal = GetData(lsShopSearchByCityStateCall, "", "")

    Set docXML = Server.CreateObject("MSXML2.DOMDocument")
    docXML.Async = False
    docXML.LoadXML lsRetVal

    If Not docXML.ParseError = 0 Then
      Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"

    Else
      Set oNodes = docXML.documentElement.selectNodes("City")
      liCitiesReturned = oNodes.length

      If liCitiesReturned = 1 Then
        Set oNode = docXML.documentElement.selectSingleNode("City")
        liZipCode = oNode.getAttribute("PrimaryZip")
        Set oNode = Nothing

      End If
    
    End If

  Else
    liVehNum = Request("vehNum")
    liAddVehFromProgShop = Request("addVeh")

  End If

  If lsFromPage = "ClaimSummary" Then
    liRetToSummary = 1
    liAddVehFromProgShop = 0

  ElseIf lsFromPage = "ShopSelectionAdv" Then
    liRetToSummary = Request("retToSummary")

  Else
    liRetToSummary = 0
  End If

  If lsFromPage = "ShopSelectionAdv" Then
    Response.Cookies("CPSession")("srhZipCode") = liZipCode

  Else
    liZipCode = Request.Cookies("CPSession")("srhZipCode")
  End If

%>

<!--#include file="includes/incSecurityCheck.asp"-->

<html>
<head>
<title><%=lsCurrentAssignmentDesc%> - Shop Search</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/incWaDatachk.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  var gsPrevPage = "<%=lsFromPage%>";
  var gsVehNum = "<%=liVehNum%>";
  gsAddVeh = <%=liAddVehFromProgShop%>;
  var gsRetToSummary = "<%=liRetToSummary%>";
  var gsVehCount = "";
  var gsAddVehicle = "<%=lsAddVehicle%>";
  
   
  function pageInit()
  {
  
 
	if(parent)
         //parent.resetTimeOut();
  
    if (parent)
      parent.gsHelpSubTopicPage = "CP_Program_Shop_Assignment.htm";
    
    var liVehNum = gsVehNum;
    if (gsPrevPage == "ClaimSummary")
      document.getElementById("txtSrhShopCState").value = "<%= lsShopState %>";
    else
      document.getElementById("txtSrhShopCState").value = "<%= lsCState %>";
    
    document.getElementById("txtSrhShopState").value = "<%= lsShopState %>";
        
        
      
    if (parent && document.getElementById("txtShopRemarks"))
        parent.getShopComments(liVehNum);
  
    var lsSearchType = "<%= lsSearchType %>";
    if (lsSearchType == "CS" )
      document.getElementById('txtSrhShopCity').focus();
    else if (lsSearchType == "SN" )
      document.getElementById('txtSrhShopName').focus();
    else
      document.getElementById('txtSrhShopZipCode').focus();
    
    setActiveSearch();
  }
  
  
  function setActiveSearch()
  {
    var lsSearchType;
    var lcolElmsRD = document.getElementsByName("searchType");
    var liRdLength = lcolElmsRD.length;
    for (var i=0; i<liRdLength; i++)
    {
      if (lcolElmsRD.item(i).checked == true)
      {
        lsSearchType = lcolElmsRD.item(i).value;

        if (lsSearchType == "CS")
        {
          document.getElementById("txtSrhShopCity").disabled = false;
          document.getElementById("txtSrhShopCState").disabled = false;
          document.getElementById("txtSrhShopZipCode").disabled = true;
          document.getElementById("txtSrhShopName").disabled = true;
          document.getElementById("txtSrhShopNameCity").disabled = true;
          document.getElementById("txtSrhShopState").disabled = true;
          document.getElementById("reqStateCity").innerText = "*";
          document.getElementById("reqStateName").innerText = "";
          document.getElementById("reqMark").style.display = "inline";
        }
        else if (lsSearchType == "ZC")
        {
          document.getElementById("txtSrhShopZipCode").disabled = false;
          document.getElementById("txtSrhShopCity").disabled = true;
          document.getElementById("txtSrhShopCState").disabled = true;
          document.getElementById("txtSrhShopName").disabled = true;
          document.getElementById("txtSrhShopNameCity").disabled = true;
          document.getElementById("txtSrhShopState").disabled = true;
          document.getElementById("reqStateCity").innerText = "";
          document.getElementById("reqStateName").innerText = "";
          document.getElementById("reqMark").style.display = "none";
        }
        else if (lsSearchType == "SN")
        {
          document.getElementById("txtSrhShopName").disabled = false;
          document.getElementById("txtSrhShopNameCity").disabled = false;
          document.getElementById("txtSrhShopState").disabled = false;
          document.getElementById("txtSrhShopCity").disabled = true;
          document.getElementById("txtSrhShopCState").disabled = true;
          document.getElementById("txtSrhShopZipCode").disabled = true;
          document.getElementById("reqStateCity").innerText = "";
          document.getElementById("reqStateName").innerText = "*";
          document.getElementById("reqMark").style.display = "inline";
        }
        return;
      }
    }
  }

  function setSelectedShop(cbObj)
  {
    var lcolElmsCB = document.getElementsByName("cb_SelectShop");
    var liCBLength = lcolElmsCB.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i) == cbObj)
        lcolElmsCB.item(i).checked = true;
      else
        lcolElmsCB.item(i).checked = false;
    }
  }
  
  
  function selectShop()
  {

  
    var laryShopInfo;
    var liShopLocationID;
    var liShopSearchLogID;
    var liSelectedShopRank;
    var liSelectedShopScore;
    var lsShopChecked = "false";
    var lcolElmsCB = document.getElementsByName("cb_SelectShop");
    var liCBLength = lcolElmsCB.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i).checked == true)
      {
        laryShopInfo = lcolElmsCB.item(i).value.split("|");
        liShopLocationID = laryShopInfo[0];
        liShopSearchLogID = laryShopInfo[1];
        liSelectedShopRank = laryShopInfo[2];
        liSelectedShopScore = laryShopInfo[3];
        lsShopChecked = "true";
      }
    }
 
    if (lsShopChecked == "false")
    {
      alert("Please selected a shop before continuing.");
      return;
    }
    else
    {
      var lsShopComments = document.getElementById("txtShopRemarks").value;
      var liTxtAreaLength = lsShopComments.length;
      if (liTxtAreaLength > 250)
      {
        alert("* Only 250 characters are allowed for Comments to Repair Shop.\n" +
              "    You have typed "+ liTxtAreaLength +" characters.");
        return;
      }
   
      if(parent && gsAddVeh == 1)
      {
     
  //   alert(parent.getLastVehNum());
        var lastVehNum = parent.getLastVehNum();
         
        if (lastVehNum == 0)
          parent.createNewVeh();
      }
      
      if(parent)
        parent.updShopSelection(gsVehNum, liShopLocationID, lsShopComments, gsRetToSummary, liShopSearchLogID, liSelectedShopRank, liSelectedShopScore);
      else
        return false;
    }
  }
  
  
  function setSelectedCity(cbObj)
  {
    var lcolElmsCB = document.getElementsByName("cb_SelectCity");
    var liCBLength = lcolElmsCB.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i) == cbObj)
        lcolElmsCB.item(i).checked = true;
      else
        lcolElmsCB.item(i).checked = false;
    }
  }

  function selectCity()
  {

  
    var liZipCode = "";
    var lsCityChecked = "false";
    var lcolElmsCB = document.getElementsByName("cb_SelectCity");
    var liCBLength = lcolElmsCB.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i).checked == true)
      {
        liZipCode = lcolElmsCB.item(i).value;
        lsCityChecked = "true";
      }
    }
  
    if (lsCityChecked == "false")
    {
      alert("Please selected a city before continuing.");
      return;
    }
    else
    {
      document.getElementById("txtCitySelected").value = "true";
      document.getElementById("txtCitySelectedZip").value = liZipCode;
      document.getElementById("animateGif2").style.display = "inline";
      document.frmShopSrh.submit();
    }
  }

  function searchShop()
  {
    var lsSearchType;
    var lsReq = "";
    var lsMsg = "";
    
    var lcolElmsRD = document.getElementsByName("searchType");
    var liRdLength = lcolElmsRD.length;
    for (var i=0; i<liRdLength; i++)
    {
      if (lcolElmsRD.item(i).checked == true)
        lsSearchType = lcolElmsRD.item(i).value;
    }
    
    if (lsSearchType == "CS")
    {
      var loSrhShopCity = document.getElementById("txtSrhShopCity");
      var loSrhShopCState = document.getElementById("txtSrhShopCState");
      if (loSrhShopCity.value == "" || loSrhShopCState.value == "")
        lsReq +=  "    " + "City and State are required for a City and State type of search." + "\n";
    }


    else if (lsSearchType == "ZC")
    {
   
      var loSrhShopZipCode = document.getElementById("txtSrhShopZipCode");
      if (loSrhShopZipCode.value == "")
        lsReq +=  "    " + "Zip Code value is required for a Zip Code type of search." + "\n";
      else if (loSrhShopZipCode.value != ""  && loSrhShopZipCode.value.length !=5)
        lsReq +=  "    " + "The Zip Code field requires a 5-digit input." + "\n";
    }
    else if (lsSearchType == "SN")
    {
      var loSrhShopName = document.getElementById("txtSrhShopName");
      ldTrim(loSrhShopName); //remove L/T spaces
      var loSrhShopNameCity = document.getElementById("txtSrhShopNameCity");
      ldTrim(loSrhShopNameCity); //remove L/T spaces
      var loSrhShopState = document.getElementById("txtSrhShopState");
      if ((loSrhShopName.value != "" || loSrhShopNameCity.value != "") && loSrhShopState.value == "")
        lsReq +=  "    " + "State is required for a Shop Name type of search." + "\n";
      else if (loSrhShopName.value == "" && loSrhShopNameCity.value == "" && loSrhShopState.value == "")
        lsReq +=  "    " + "Shop Name or City and State fields are required for this search." + "\n";
    }
    else
    {
      alert("Please select the search desired by selecting the appropriate radio button.");
      return;
    }
  
    if (lsReq.length > 0)
    {
      lsMsg += "* The following fields are required:\n";
      lsMsg += lsReq;
      alert(lsMsg);
    }
    else
    {
      document.getElementById("animateGif").style.display = "inline";
      document.frmShopSrh.submit();
      
    }
  }
  
  
  function chkEnterKey(event) 
  {
    if ( event.keyCode == 13 )
      searchShop();
  }
  
  
  function numbersOnly(obj,evt,decPntKey)
  {
    if (obj.value.length > obj.maxLength) return;
    var bOK = false;
    evt = (evt) ? evt : ((event) ? event : null);
    if (evt)
    {
      if ( evt.keyCode == 13 )
        searchShop();
      else
      {
        var charCode = (evt.charCode || evt.charCode == 0) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : evt.which);
        if (!(charCode > 13 && (charCode < 48 || charCode > 57))) bOK = true; // normal numbers
        if (decPntKey == 1 && charCode == 46) bOK = true; // allow decimals '.'
    
        if (bOK != true)
        {
          if (evt.returnValue) evt.returnValue = false;
          else if (evt.preventDefault) evt.preventDefault();
          else return false;
        }
      }
    }
  }
  
  
  function alphaOnly(obj,evt)
  {
    if (obj.value.length > obj.maxLength) return;
    var bOK = false;
    evt = (evt) ? evt : ((event) ? event : null);
    if (evt)
    {
      if ( evt.keyCode == 13 )
        searchShop();
      else
      {
        var charCode = (evt.charCode || evt.charCode == 0) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : evt.which);
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) bOK = true; // normal letters
    
        if (bOK != true)
        {
          if (evt.returnValue) evt.returnValue = false;
          else if (evt.preventDefault) evt.preventDefault();
          else return false;
        }
      }
    }
  }
  
  
  function goCancel()
  {
    if (parent)
    {
      if(gsRetToSummary == 1)
      {
        gsVehCount = parent.getVehCount();
        if (parent.lsPageName && parent.lsPageName == "frmWaMainAddVehicle.asp")
          parent.document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=VehicleInfo&vehCount=" + gsVehCount;
        else
          parent.document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ShopSelection&vehCount="+gsVehCount;
      }
      else
        top.location = "frmMyClaimsDesktop.asp";
    }
  }
  
</script>

</head>

<body onLoad="resizeWaIframe(0); pageInit(); parent.hideMsg();">

<DIV ID="divToolTip"></DIV>

<SCRIPT TYPE="text/javascript">
<!--

    Xoffset = -20;
    Yoffset = 20;

    var ttLayer;
    var yDist = -1000;

    var ns6 = document.getElementById && !document.all;
    var ie4 = document.all;

    if (ns6)
        ttLayer = document.getElementById("divToolTip").style;
    else if (ie4)
        ttLayer = document.all.divToolTip.style;

    ttLayer.visibility = "visible";
    ttLayer.display = "none";
    document.onmousemove = get_mouseXY;

    function popDiv(elID) {
        var msg = document.getElementById(elID).value;
        var content = "<TABLE WIDTH=300 BORDER=0 BORDERCOLOR=blue CELLPADDING=2 CELLSPACING=0 style='border:1px solid #000080; background-color:#E6E6FA;'>";
        content += "<TR><TD style='font-family:Arial,sans-serif;  font-weight:normal;  font-size:9pt;'>" + msg + "</TD></TR></TABLE>";
        yDist = Yoffset;

        if (ns6) {
            document.getElementById("divToolTip").innerHTML = content;
            ttLayer.display = '';
        }
        if (ie4) {
            document.all("divToolTip").innerHTML = content;
            ttLayer.display = '';
        }
    }

    function get_mouseXY(e) {
        var x = ns6 ? e.pageX : event.x + document.body.scrollLeft;
        ttLayer.left = x + Xoffset;
        var y = ns6 ? e.pageY : event.y + document.body.scrollTop;
        ttLayer.top = y + yDist;
    }

    function hideDiv() {
        yDist = -1000;
        ttLayer.display = "none"
    }

//-->
</SCRIPT>

<div id="divContainer">

  <form name="frmShopSrh" method="post" action="frmWaShopSelectionAdv.asp">
    <input name="vehDesc" type="hidden" value="<%=lsVehDesc%>">
    <input name="vehNum" type="hidden" value="<%=liVehNum%>">
    <input name="fromPage" type="hidden" value="ShopSelectionAdv">
    <input name="addVeh" type="hidden" value="<%=liAddVehFromProgShop%>">
    <input name="CitySelected" id="txtCitySelected" type="hidden" value="false">
    <input name="CitySelectedZip" id="txtCitySelectedZip" type="hidden">
    <input name="retToSummary" type="hidden" value="<%=liRetToSummary%>">
    <%if lsAddVehicle <> "" then%>
      <input name="AddVehicle" id="txtAddVehicle" type="hidden" value="1">
    <%end if%>

  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000099" style="border-collapse:collapse">
    <tr>
      <td style="font-size:10pt">
        Please select the type of search desired by marking the appropriate radio button.<br>
        <li><strong>Search by Distance</strong> will return results based on a radius from a central location.<br></li>
        <li><strong>Search by Name</strong> results are based on a likely match of your query.<br></li>
      </td>
    </tr>
  </table>

  <br>

  <table id="tblSearch" width="600" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
    <tr style="height: 20px;" bgcolor="#E5E5F5">
      <td colspan="5" style="border:1px solid #000099; font-size:10pt; font-weight:bold;">&nbsp;
        <span style="color:#000099; font-size:10pt; font-weight:bold;">Search by Distance</span>
      </td>
    </tr>
    <tr valign="middle" style="height: 30px;">
      <td nowrap class="box" style="border-left:1px solid #000099;">
        <input name="searchType" type="radio" value="ZC" onclick="setActiveSearch()"
          <%
            If Request("searchType") = "ZC" or  Request("searchType") = "" Then
            Response.Write " checked=""checked"""
            End If
          %>
        />
      </td>
      <td nowrap style="font-size: 10pt; font-weight:bold;" class="box" onclick="document.frmShopSrh.searchType[0].click();" onmouseover="style.cursor='hand'; title='Select Zip Code search';" onmouseout="style.cursor='default';">
        Zip Code:
      </td>
      <td nowrap class="box">
        <input name="ZipCode" id="txtSrhShopZipCode" type="text" onKeyPress="return numbersOnly(this,event,0)" value="<%=liZipCode%>" size="6" maxlength="5" AUTOCOMPLETE="OFF">
      </td>
      <td colspan="2" class="box" style="border-right:1px solid #000099;">&nbsp;</td>
    </tr>
    <tr valign="middle" style="height: 30px;">
      <td nowrap class="box" style="border-left:1px solid #000099;">
        <input name="searchType" type="radio" value="CS" onclick="setActiveSearch()"

          <%
            If Request("searchType") = "CS" Then
            Response.Write " checked=""checked"""
            End If
          %>

        />
      </td>
      <td nowrap style="font-size: 10pt; font-weight:bold;" class="box" onclick="document.frmShopSrh.searchType[1].click();" onmouseover="style.cursor='hand'; title='Select City search';" onmouseout="style.cursor='default';">
        City:
      </td>
      <td nowrap class="box">
        <input name="City" id="txtSrhShopCity" type="text" value="<%=lsCity%>" onkeypress="chkEnterKey( event );" size="28" maxlength="40" AUTOCOMPLETE="OFF">
      </td>
      <td nowrap style="font-size: 10pt; font-weight:bold;" class="box">
        State:
      </td>
      <td nowrap class="box" style="border-right:1px solid #000099;">
        <select name="CState" id="txtSrhShopCState">
          <option value=""></option>
          <option value="AK">Alaska</option>
          <option value="AL">Alabama</option>
          <option value="AR">Arkansas</option>
          <option value="AZ">Arizona</option>
          <option value="CA">California</option>
          <option value="CO">Colorado</option>
          <option value="CT">Connecticut</option>
          <option value="DC">District of Columbia</option>
          <option value="DE">Delaware</option>
          <option value="FL">Florida</option>
          <option value="GA">Georgia</option>
          <option value="HI">Hawaii</option>
          <option value="IA">Iowa</option>
          <option value="ID">Idaho</option>
          <option value="IL">Illinois</option>
          <option value="IN">Indiana</option>
          <option value="KS">Kansas</option>
          <option value="KY">Kentucky</option>
          <option value="LA">Louisiana</option>
          <option value="MD">Maryland</option>
          <option value="ME">Maine</option>
          <option value="MI">Michigan</option>
          <option value="MN">Minnesota</option>
          <option value="MO">Missouri</option>
          <option value="MS">Mississippi</option>
          <option value="MT">Montana</option>
          <option value="NE">Nebraska</option>
          <option value="NV">Nevada</option>
          <option value="NH">New Hampshire</option>
          <option value="NJ">New Jersey</option>
          <option value="NM">New Mexico</option>
          <option value="NY">New York</option>
          <option value="NC">North Carolina</option>
          <option value="ND">North Dakota</option>
          <option value="OH">Ohio</option>
          <option value="OK">Oklahoma</option>
          <option value="OR">Oregon</option>
          <option value="PA">Pennsylvania</option>
          <option value="RI">Rhode Island</option>
          <option value="SC">South Carolina</option>
          <option value="SD">South Dakota</option>
          <option value="TN">Tennessee</option>
          <option value="TX">Texas</option>
          <option value="UT">Utah</option>
          <option value="VA">Virginia</option>
          <option value="VT">Vermont</option>
          <option value="WA">Washington</option>
          <option value="WI">Wisconsin</option>
          <option value="WV">West Virginia</option>
          <option value="WY">Wyoming</option>
        </select>
        <span id="reqStateCity" style="width:10px; font-size:11pt; color:#000099; display:inline"></span>
      </td>
    </tr>
  </table>

  <table id="tblSearch" width="600" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
    <tr style="height:20px;">
      <td colspan="8">&nbsp;</td>
    </tr>
    <tr style="height: 20px;" bgcolor="#E5E5F5">
      <td colspan="8" style="border:1px solid #000099; color:#000099; font-size:10pt; font-weight:bold;">&nbsp;
        <span style="color:#000099; font-size:10pt; font-weight:bold;">Search by Name</span>
      </td>
    </tr>
    <tr valign="middle" style="height: 30px;">
      <td nowrap class="box" style="border-left:1px solid #000099;">
        <input name="searchType" type="radio" value="SN" onclick="setActiveSearch()"

          <%
            If Request("searchType") = "SN" Then
            Response.Write " checked=""checked"""
            End If
          %>

        />
      </td>
      <td nowrap style="font-size: 10pt; font-weight:bold;" class="box" onclick="document.frmShopSrh.searchType[2].click();" onmouseover="style.cursor='hand'; title='Select Name search';" onmouseout="style.cursor='default';">
        Shop Name:
      </td>
      <td nowrap class="box">
        <input name="ShopName" id="txtSrhShopName" type="text" value="<%=lsShopName%>" onkeypress="chkEnterKey( event );" size="20" maxlength="40" AUTOCOMPLETE="OFF">
      </td>
      <td nowrap style="font-size: 10pt; font-weight:bold;" class="box">
        City:
      </td>
      <td nowrap class="box">
        <input name="ShopNameCity" id="txtSrhShopNameCity" type="text" value="<%=lsShopNameCity%>" onkeypress="chkEnterKey( event );" size="20" maxlength="40" AUTOCOMPLETE="OFF">
      </td>
      <td nowrap style="font-size: 10pt; font-weight:bold;" class="box">
        State:
      </td>
      <td nowrap class="box" style="border-right:2px solid #000099;">
        <select name="ShopState" id="txtSrhShopState">
          <option value=""></option>
          <option value="AK">Alaska</option>
          <option value="AL">Alabama</option>
          <option value="AR">Arkansas</option>
          <option value="AZ">Arizona</option>
          <option value="CA">California</option>
          <option value="CO">Colorado</option>
          <option value="CT">Connecticut</option>
          <option value="DC">District of Columbia</option>
          <option value="DE">Delaware</option>
          <option value="FL">Florida</option>
          <option value="GA">Georgia</option>
          <option value="HI">Hawaii</option>
          <option value="IA">Iowa</option>
          <option value="ID">Idaho</option>
          <option value="IL">Illinois</option>
          <option value="IN">Indiana</option>
          <option value="KS">Kansas</option>
          <option value="KY">Kentucky</option>
          <option value="LA">Louisiana</option>
          <option value="MD">Maryland</option>
          <option value="ME">Maine</option>
          <option value="MI">Michigan</option>
          <option value="MN">Minnesota</option>
          <option value="MO">Missouri</option>
          <option value="MS">Mississippi</option>
          <option value="MT">Montana</option>
          <option value="NE">Nebraska</option>
          <option value="NV">Nevada</option>
          <option value="NH">New Hampshire</option>
          <option value="NJ">New Jersey</option>
          <option value="NM">New Mexico</option>
          <option value="NY">New York</option>
          <option value="NC">North Carolina</option>
          <option value="ND">North Dakota</option>
          <option value="OH">Ohio</option>
          <option value="OK">Oklahoma</option>
          <option value="OR">Oregon</option>
          <option value="PA">Pennsylvania</option>
          <option value="RI">Rhode Island</option>
          <option value="SC">South Carolina</option>
          <option value="SD">South Dakota</option>
          <option value="TN">Tennessee</option>
          <option value="TX">Texas</option>
          <option value="UT">Utah</option>
          <option value="VA">Virginia</option>
          <option value="VT">Vermont</option>
          <option value="WA">Washington</option>
          <option value="WI">Wisconsin</option>
          <option value="WV">West Virginia</option>
          <option value="WY">Wyoming</option>
        </select>
        <span id="reqStateName" style="width:10px; font-size:11pt; color:#000099; display:inline"></span>
      </td>
    </tr>

  </form>

  </table>

  <table width="600" border="0" cellpadding="0" cellspacing="0" style="align:right;">
    <tr>
      <td nowrap>
        <p class="bodyBlue"><span id="reqMark" style="display:none">* Required</span></p>
      </td>
      <td width="100%" align="center">
        <span id="animateGif" style="font-size:12px; font-weight:bold; color:#000080; display:none;">
          Searching for Shops...&nbsp;&nbsp;&nbsp;
          <img src="images/loading.gif" alt="" width="78" height="7" border="0">
        </SPAN>
      </td>
      <td align="right" valign="top" nowrap>
        <a href="javascript:goCancel()"
           title="Return to <% If liRetToSummary = 1 Then %> Summary <% Else %> My Claims Desktop <% End If %>"
           onMouseOver="window.status='Return to <% If liRetToSummary = 1 Then %> Summary <% Else %> My Claims Desktop <% End If %>'; return true"
           onMouseOut="window.status=''; return true">
          <img src='images/btn_cancel.gif' border='0' WIDTH='83' HEIGHT='31'>
        </a>
      </td>
      <td align="right" valign="top" nowrap><img border="0" src="images/spacer.gif" WIDTH="10" HEIGHT="1"></td>
      <td align="right" valign="top" nowrap>
        <a href="javascript:searchShop()"
           title="Search for Shops"
           onMouseOver="window.status='Search for Shops'; return true"
           onMouseOut="window.status=''; return true">
          <img border="0" src="images/btn_search_green.gif" WIDTH="83" HEIGHT="31">
        </a>
      </td>
    </tr>
  </table>
  <br/>

<% If lsSearchType = "CS" and liCitiesReturned > 1 Then %>
  <br/>

  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000099" style="border-collapse:collapse">
    <tr>
      <td valign="top"><img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" /></td>
      <td style="font-size:10pt">
        Your City/State query returned multiple results. Please select the city that best matches your search by checking
        the appropriate checkbox and clicking <strong>Next</strong> button below.
      </td>
    </tr>
  </table>
  
  <br/>
  
  <table width="500" border="1" cellpadding="0" cellspacing="0" bordercolor="#000099" style="border-collapse:collapse">
    <colgroup>
      <col width="30px"/>
      <col width="360px"/>
      <col width="100px"/>
    </colgroup>

    <tr bgcolor="#E5E5F5">
      <td colspan="2" align="center" style="color:#000099; font-size:10pt; font-weight:bold; padding:4px;">City</td>
      <td align="center" style="color:#000099; font-size:10pt; font-weight:bold; padding:4px;">State</td>
    </tr>

    <%
      For Each oNode in oNodes

        lsRetCity = oNode.getAttribute("City")
        lsRetState = oNode.getAttribute("State")
        lsRetPrimaryZip = oNode.getAttribute("PrimaryZip")

        lsCityListHTML = lsCityListHTML & _
                         "<tr style='padding:4px; border: 1px solid #000099;'>" & _
                         "  <td align='center' class='cellGrid' style='border-left: 1px solid #000099;' nowrap=''><input name='cb_SelectCity' type='checkbox' onClick='setSelectedCity(this)' value='" & lsRetPrimaryZip & "'></td>" & _
                         "  <td class='cellGrid'>" & lsRetCity & "</td>" & _
                         "  <td class='cellGrid' align='center' colspan='2'>" & lsRetState & "</td>" & _
                         "</tr>"

      Next

      Response.write lsCityListHTML

      Set oNodes = Nothing
      Set docXML = Nothing
    %>

  </table>
  
  <table width="500" border="0" cellpadding="0" cellspacing="0" style="align:right;">
    <tr>
      <td width="100%" align="center">
        <span id="animateGif2" style="font-size:12px; font-weight:bold; color:#000080; display:none;">
          Searching for Shops...&nbsp;&nbsp;&nbsp;
          <img src="images/loading.gif" alt="" width="78" height="7" border="0">
        </SPAN>
      </td>
      <td align="right" valign="top" nowrap>
      
        <a href="javascript:selectCity()"
           title="Search for Shops in the City selected"
           onMouseOver="window.status='Search for Shops in the City selected'; return true"
           onMouseOut="window.status=''; return true">
          <img src='images/btn_next.gif' border='0' WIDTH='83' HEIGHT='31'>
        </a>
      </td>
    </tr>
  </table>

<% ElseIf lsSearchType = "CS" and liCitiesReturned = 0 Then %>
  <br/>

  <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000099" style="border-collapse:collapse">
    <tr>
      <td valign="top"><img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" /></td>
       <td style="font-size: 10pt; color: #FF0000; font-weight: bold;">
        Your City/State query didn't return any results. Please redefine your search.
      </td>
    </tr>
  </table>

<% Else %>
<br />
  <%
    If lsFromPage = "ShopSelectionAdv" Then
      If lsSearchType = "ZC" Then

        liMaxShops= "20"
        lsShopSearchCall = "<ShopSearchByDistance Zip=""" & liZipCode & _
                           """ InsuranceCompanyID=""" & liInsuranceCompanyID & _
                           """ ShopTypeCode=""" & lsShopTypeCD & """ MaxShops=""" & liMaxShops & """ UserID=""" & liUserID & """/>"
        Response.Write GetPersistedData(lsShopSearchCall, lsGUID, "WaShopSelection.xsl", "VehDesc=" & lsVehDesc & lcDelimiter & "PageTitle=" & lsPageTitle & lcDelimiter & "AddVehicle=" & lsAddVehicle, false)
    
      Elseif lsSearchType = "CS" and liCitiesReturned = 1  Then

        liMaxShops= "20"
        lsShopSearchCall = "<ShopSearchByDistance Zip=""" & liZipCode & _
                           """ InsuranceCompanyID=""" & liInsuranceCompanyID & _
                           """ ShopTypeCode=""" & lsShopTypeCD & """ MaxShops=""" & liMaxShops & """ UserID=""" & liUserID & """/>"
        Response.Write GetPersistedData(lsShopSearchCall, lsGUID, "WaShopSelection.xsl", "VehDesc=" & lsVehDesc & lcDelimiter & "PageTitle=" & lsPageTitle & lcDelimiter & "AddVehicle=" & lsAddVehicle, false)
    
      Elseif lsSearchType = "SN" Then
        liMaxShops= "40"
        lsShopSearchCall = "<ShopSearchByName ShopCity=""" & server.urlencode(lsShopNameCity) & _
                           """ ShopName=""" & server.urlencode(lsShopName) & _
                           """ ShopState=""" & lsShopState &_
                           """ InsuranceCompanyID=""" & liInsuranceCompanyID & _
                           """ ShopTypeCode=""" & lsShopTypeCD & """ MaxShops=""" & liMaxShops & """ UserID=""" & liUserID & """/>"
        Response.Write GetPersistedData(lsShopSearchCall, lsGUID, "WaShopSelection.xsl", "VehDesc=" & lsVehDesc & lcDelimiter & "PageTitle=" & lsPageTitle & lcDelimiter & "AddVehicle=" & lsAddVehicle, false)
    
      End If

    End If
  %>

<% End If %>

</div>

</body>
</html>