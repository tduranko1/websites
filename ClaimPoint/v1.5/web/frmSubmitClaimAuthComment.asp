<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!-- v1.4.3.0 -->

<%
  Dim liLynxID, lsClaimNum, lsLossDate, lsGUID, lsUserClaimDetailCall
  Dim lsCRName, lsCRPhone, lsCRPhoneExt, lsCREmail
  Dim docXML, Node, lsRepEmailStrXML, sRepEmail
  
  liLynxID = Request("LynxID")
  lsClaimNum = Request("ClaimNumber")
  lsLossDate = Request("LossDate")

  lsGUID = Request.Cookies("CPSession")("guidArray0")
  
  lsUserClaimDetailCall = "<UserClaimDetail LynxID=""" & server.urlencode(liLynxID) & """ />"
  
  set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML GetData(lsUserClaimDetailCall, "", "")
  
  If Not docXML.ParseError = 0 Then
    'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
    'Response.Write "<b>Error Description:</b> " & docXML.ParseError.reason & "<br>"
    'Response.Write "<b>Error File Position:</b> " & docXML.ParseError.filepos & "<br>"
    'Response.Write "<b>Error Line:</b> " & docXML.ParseError.line & "<br>"
    'Response.Write "<b>Error Line Position:</b> " & docXML.ParseError.linepos & "<br>"
    'Response.Write "<b>Error Source Text:</b> " & docXML.ParseError.srcText & "<br>"
  Else
    Set Node = docXML.documentElement.selectSingleNode("Claim") 
  
    lsCRName = Node.getAttribute("OwnerUserNameFirst") & " " & Node.getAttribute("OwnerUserNameLast")
    lsCRPhone = Node.getAttribute("OwnerUserPhoneAreaCode") & "-" & Node.getAttribute("OwnerUserPhoneExchangeNumber") & "-" & Node.getAttribute("OwnerUserPhoneUnitNumber")
    lsCRPhoneExt = Node.getAttribute("OwnerUserExtensionNumber")
    lsCREmail = Node.getAttribute("OwnerUserEmail")
    Set Node = Nothing
  End If
  
  lsRepEmailStrXML = "<Config Path='ClaimPoint/ClaimCommentMailbox/NotificationEmail'/>"
  docXML.LoadXML GetData(lsRepEmailStrXML, "", "")
  sRepEmail = docXML.documentElement.text

  Set docXML = Nothing

%>
<!DOCTYPE html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Submit comment</TITLE>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">

<SCRIPT language="JavaScript">function goReset()
{
	JavaScript:document.frmMainForm.txtComment.value = "";
}

function goSubmit()
{
	var lsReq = "";
  var lsMsg = "";

	//clear message holder
  document.getElementById("DisplayMessage").innerHTML = "";

  if (document.getElementById("SendToClaimOwner").value.length == 0)
		lsReq +=  "    " + "Select an e-mail." + "\n";
  if (document.getElementById("txtComment").value.length == 0)
		lsReq +=  "    " + "Enter a comment." + "\n";

	if (lsReq.length > 0)
	{
		lsMsg += "* The following field(s) are required:\n";
		lsMsg += lsReq;
    alert(lsMsg);
    return;
	}
  else
    document.frmMainForm.submit();
}
</SCRIPT>

</HEAD>
<BODY>

<p class="headerRed">Submit Comments</p>

<FORM name="frmMainForm" id="frmMainForm" method="POST" target="IFrmSubmitClaimAuthComment" action="rspSubmitClaimAuthComment.asp">
  <input type="hidden" id="LynxID" name="LynxID" value="<%= liLynxID %>"/>

  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
    	<td valign="top"><P class="bodyBlue">LYNX ID:</P></td>
    	<td class="body"><%= liLynxID %></td>
    	<td width="20"></td>
    	<td nowrap=""><P class="bodyBlue">LYNX Rep.:</P></td>
    	<td class="body"><%= lsCRName %></td>
    </tr>
    <tr>
    	<td valign="top"><P class="bodyBlue">Claim Number:</P></td>
    	<td class="body"><%= lsClaimNum %></td>
    	<td width="20"></td>
    	<td><P class="bodyBlue">Phone:</P></td>
    	<td class="body"><%= lsCRPhone %></td>
    </tr>
    <tr>
    	<td valign="top"><P class="bodyBlue">Date of Loss:</P></td>
    	<td class="body"><%= lsLossDate %></td>
    	<td width="20"></td>
    	<td><P class="bodyBlue">E-mail:</P></td>
    	<td class="body">
        <!-- The COM looks for SendToClaimOwner to see wether send it to claim owner of generic mailbox -->
        <select name="SendToClaimOwner" id="SendToClaimOwner">
        	<option value="" SELECTED>Please choose one</option>
        	<option value="yes">Assigned Claims Rep</option>
        	<option value="no">Comments mailbox</option>
        </select>
      </td>
    </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
  	<tr>
  		<td colspan="5" nowrap>
        <br>
        <P class="bodyBlue">Comment:<br>
  			<TEXTAREA rows="10" id="txtComment" name="txtComment" style="width:99%"></TEXTAREA>
  		</td>
  	</tr>
  	<tr>
  		<td colspan="4">
        <span id="DisplayMessage" style="color: #FF0000; font-size: 11px; font-weight: bold;"></span>
  		</td>
  		<td align="center" width="200" nowrap>
              <a href="Javascript:goSubmit();"
                 title="Click to Submit"
                 onMouseOver="window.status='Click to Submit'; return true"
                 onMouseOut="window.status=''; return true">
                <IMG border="0" src="images/btn_submit.gif" border="0" width="83" height="31"></a>
  		</td>
  	</tr>
  </table>

</FORM>

  <iframe src="blank.asp" id="IFrmSubmitClaimAuthComment" name="IFrmSubmitClaimAuthComment" style="height:0px;width:0px;display:none" frameBorder="0" />

</BODY>
</HTML>

<!--#include file="includes/incCommonBottom.asp"-->
