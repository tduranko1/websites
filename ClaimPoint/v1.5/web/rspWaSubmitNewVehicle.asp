
<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->

<%
  lsPageName = "rspWASubmitNewVehicle.asp"
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
  Dim loStrXML, lsPrintLayoutPage, lsStrXML, lsRetVal, liRetLynxID , liCurrentEnvironment
  Dim docXML, oNodes, oNode, sVehList
  Dim sDocLocation, sVehListXML, sTemp
  
  lsStrXML = request("lstrXML")
  lsPrintLayoutPage = request("PrintLayoutPage")
  
  On Error Resume Next

  'lsRetVal = GetData(lsStrXML, "", "")

	lsRetVal = CallSoapWCF(lsStrXML)


  'check for any error return value
  Set docXML = CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML lsRetVal
  
  If Not docXML.ParseError = 0 Then
    Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
  Else

    If docXML.documentElement.nodeName = "Root" Then
      sDocLocation = docXML.documentElement.getAttribute("WebAssignmentDocument")
      liRetLynxID = docXML.documentElement.getAttribute("LynxID")
      Set oNodes = docXML.documentElement.selectNodes("Vehicle")
  
      For Each oNode in oNodes
        If sVehList <> "" Then
          sVehList = sVehList & ","
        End If
        sVehList = sVehList & "VEH" & oNode.getAttribute("VehicleNumber") & ","
        sVehList = sVehList & oNode.getAttribute("VehicleYear") & " " & oNode.getAttribute("Make") & " " & oNode.getAttribute("Model")
      Next

    Else
      'get the returned error msg and stuff it in the global message holder
      lsExternalEventMsg = docXML.documentElement.getAttribute("Description")
    End If

  End If
      
  Set oNodes = Nothing
  Set docXML = Nothing


function GetSoapURL()

Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    liCurrentEnvironment = GetEnvironment
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"       
        case "STG"
        strmexMonikerString = "http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"        
        case "PRD"
        strmexMonikerString = "http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"       
    End Select
    
   GetSoapURL = strmexMonikerString


end function

function CallSoapWCF(claimXML)

'/* Call the web service */
Dim xmlHttp, xmlResponse, postUrl
Dim dataToSend
Dim sXmlCall
Dim docXML
Dim NodeList
Dim strXmlArray
Dim strReturnXML, strResult

sXMLCall = claimXML
Set xmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP")
postUrl = GetSoapURL()
dataToSend = "" & _
                "<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">" & _   
                   "<soapenv:Body>" & _
                   "<ClaimSubmissionGetXML xmlns=""http://LynxAPDComponentServices""><strParamXML>" & Server.HTMLEncode(sXmlCall) & "</strParamXML></ClaimSubmissionGetXML>" &_                                 
                   "</soapenv:Body>" & _
                "</soapenv:Envelope>"

xmlHttp.setTimeouts 60000,60000,120000,120000
Call xmlHttp.Open("POST", postUrl, False, "", "")
xmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
xmlHttp.setRequestHeader "SOAPAction", "http://LynxAPDComponentServices/IEcadAccessor/ClaimSubmissionGetXML"
xmlHttp.setRequestHeader "Content-length", len(dataToSend)
xmlHttp.send dataToSend

Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
docXML.Async = False
docXML.LoadXML HTMLDecode(xmlHttp.responseText)
strReturnXML = docXML.xml

 strResult = Split(strReturnXML,"<ClaimSubmissionGetXMLResult>")(1)

xmlResponse = Split(strResult, "</ClaimSubmissionGetXMLResult>")(0)

CallSoapWCF = xmlResponse

end function

Function HtmlDecode(htmlString)
                Dim returnValue
                
                returnValue = htmlString
                returnValue = Replace(returnValue, "&lt;", "<")
                returnValue = Replace(returnValue, "&gt;", ">")
                returnValue = Replace(returnValue, "&quot;", """")
                returnValue = Replace(returnValue, "&amp;", "&")
                HtmlDecode = returnValue
End Function

%>

<script language="JavaScript" type="text/JavaScript">

  var liLynxID = "<%=liRetLynxID%>";
  var lsVehList = "<%=sVehList%>";
  var lsDocLoc = "<%=replace(sDocLocation, "\", "\\")%>";
  var lbSubmitSuccess;
  debugger;
  if (liLynxID != "")
    lbSubmitSuccess = "1";
  else
    lbSubmitSuccess = "0";
    
  if(parent)
  {
    parent.document.getElementById("uiFrame").src = "frmWaSubmitNewVehicle.asp?fromPage=rspWASubmitNewVehicle&LynxID=" + liLynxID + "&SubmitSuccess=" + lbSubmitSuccess + "&VehList=" + lsVehList + "&PrintLayoutPage=<%=lsPrintLayoutPage%>&D=" + lsDocLoc +"&Env=<%=liCurrentEnvironment%>";
  }

</script>
