<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!-- v1.4.5.0 -->

<%
  Dim lsCurrentAssignmentDesc, lsPrevPage
  lsCurrentAssignmentDesc = Request.Cookies("CPSession")("CurrAssignmentDescription")
  lsPrevPage = Request("fromPage")

  lsPageName = "frmWaTLAssignment.asp"
  lsPageTitle = lsCurrentAssignmentDesc & " - Claim Info"
  liPageType = lcAssignmentPage

  Dim lsInsCoDetailsCall, lsCarrierUsersListCall
  
  lsInsCoConfigGUID = Request.Cookies("CPSession")("guidArray5")
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<html>
<head>
<title><%=lsCurrentAssignmentDesc%> - Claim Info</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incTLStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/incCalendar.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery.maskedinput-1.2.2.min.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery.selectboxes.min.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery.simplemodal-1.3.5.min.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery.tools.min.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/inctlscript.js" type="text/javascript"></script>

</head>

<body tabIndex="-1">

<div id="divContainer">

   <table border="0" cellpadding="4" cellspacing="1" style="boder-collapse:collapse">
      <tr>
         <td id="tdBasicStep" class="breadcrumb_item" onclick="javascript:gotoStep(0)"><img id="validation_step_0" src="images/spacer.gif" class="validationIcon"/>&nbsp;Basic</td>
         <td id="tdInsuredStep" class="breadcrumb_item" onclick="javascript:gotoStep(1)"><img id="validation_step_1" src="images/spacer.gif" class="validationIcon"/>&nbsp;Insured</td>
         <td id="tdVehicleStep" class="breadcrumb_item" onclick="javascript:gotoStep(2)"><img id="validation_step_2" src="images/spacer.gif" class="validationIcon"/>&nbsp;Vehicle</td>
         <td id="tdSettlementStep" class="breadcrumb_item" onclick="javascript:gotoStep(3)"><img id="validation_step_3" src="images/spacer.gif" class="validationIcon"/>&nbsp;Settlement</td>
         <td id="tdSummaryStep" class="breadcrumb_item" onclick="javascript:gotoStep(4)"><img id="validation_step_4" src="images/spacer.gif" class="validationIcon"/>&nbsp;Summary</td>
         <td id="tdSubmitStep" class="breadcrumb_item"><img id="validation_step_5" src="images/spacer.gif" class="validationIcon"/>&nbsp;Submit</td>
      </tr>
   </table>
   <div id="divBasic" class="divStep">
      <table border="0" cellpadding="0" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="300px"/>
            <col width="300px"/>
         </colgroup>
         <tr>
            <td><div class="tabHeader">Basic Claim Information</div></td>
            <td><div class="tabHeader required">All fields marked with an asterisk (*) are required</div></td>
         </tr>
         <tr>
            <td class="legend">Claim Assigned by: *</td>
            <td class="legend">Assigner Relation to Insured: *</td>
         </tr>
         <tr>
            <td>
               <select id="selAssignedBy">
                  <option value=""></option>
               </select>
            </td>
            <td>
               <select id="selAssignorRelation">
                  <option value=""></option>
               </select>
            </td>
         </tr>
         <tr>
            <td class="legend">Claim Number: *</td>
            <td></td>
         </tr>
         <tr>
            <td>
               <input id="txtClaimNumber" size="15" maxLength="30"></input>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">Loss Date: *</td>
            <td class="legend">Loss State: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtLossDate" size="10" maxlength="10" onBlur="formatDateObject(this)"></input>
               <a href="javascript:void(0)" onclick="javascript:showCalIframe(this, 'txtLossDate')" id="calCall" tabIndex="-1"><img src="images/calendar.gif" alt="" width="24" height="17" border="0" title="Click to select date from calendar"></a>
            </td>
            <td>
               <select id="selLossState">
                  <option value=""></option>
               </select>
            </td>
         </tr>
         <tr>
            <td class="legend">Party: *</td>
            <td class="legend">Coverage:</td>
         </tr>
         <tr>
            <td>
               <select id="selPartyCD">
                  <option value=""></option>
               </select>
            </td>
            <td>
               <select id="selCoverage">
                  <option value=""></option>
               </select>
            </td>
         </tr>
      </table>
   </div>
   <div id="divInsured" class="divStep">
      <table border="0" cellpadding="0" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="300px"/>
            <col width="300px"/>
         </colgroup>
         <tr>
            <td><div class="tabHeader">Insured Information</div></td>
            <td><div class="tabHeader required">All fields marked with an asterisk (*) are required</div></td>
         </tr>
         <tr>
            <td class="legend">First Name: *</td>
            <td class="legend">Last Name: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtInsuredFirstName" size="40" maxLength="50"></input>
            </td>
            <td>
               <input id="txtInsuredLastName" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend" colspan="2">Business Name: *</td>
         </tr>
         <tr>
            <td colspan="2">
               <input id="txtInsuredBusinessName" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Address: *</td>
            <td class="legend">Address 2:</td>
         </tr>
         <tr>
            <td>
               <input id="txtInsuredAddress1" size="40" maxLength="50"></input>
            </td>
            <td>
               <input id="txtInsuredAddress2" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Zip: *</td>
            <td></td>
         </tr>
         <tr>
            <td>
               <input id="txtInsuredAddressZip" size="5" maxLength="5"></input>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">City: *</td>
            <td class="legend">State: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtInsuredAddressCity" size="30" maxLength="30"></input>
            </td>
            <td>
               <select id="selInsuredAddressState">
                  <option value=""></option>
               </select>
            </td>
         </tr>
      </table>
   </div>
   <div id="divVehicle" class="divStep">
      <table border="0" cellpadding="0" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="300px"/>
            <col width="300px"/>
         </colgroup>
         <tr>
            <td><div class="tabHeader">Vehicle Owner Information</div></td>
            <td><div class="tabHeader required">All fields marked with an asterisk (*) are required</div></td>
         </tr>
         <tr>
            <td class="legend">First Name: *</td>
            <td class="legend">Last Name: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtOwnerFirstName" size="40" maxLength="50"></input>
            </td>
            <td>
               <input id="txtOwnerLastName" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Business Name: *</td>
            <td></td>
         </tr>
         <tr>
            <td>
               <input id="txtOwnerBusinessName" size="40" maxLength="50"></input>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">Address: *</td>
            <td class="legend">Address 2:</td>
         </tr>
         <tr>
            <td>
               <input id="txtOwnerAddress1" size="40" maxLength="50"></input>
            </td>
            <td>
               <input id="txtOwnerAddress2" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Zip: *</td>
            <td></td>
         </tr>
         <tr>
            <td>
               <input id="txtOwnerAddressZip" size="5" maxLength="5"></input>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">City: *</td>
            <td class="legend">State: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtOwnerAddressCity" size="30" maxLength="30"></input>
            </td>
            <td>
               <select id="selOwnerAddressState">
                  <option value=""></option>
               </select>
            </td>
         </tr>
         <tr>
            <td class="legend">Primary Phone: *</td>
            <td class="legend">Secondary Phone:</td>
         </tr>
         <tr>
            <td>
               <input id="txtOwnerPhoneDay" size="25" maxLength="25"></input>
            </td>
            <td>
               <input id="txtOwnerPhoneAlt" size="25" maxLength="25"></input>
            </td>
         </tr>
         <tr>
            <td colspan="2"><div class="tabHeader">Vehicle Information</div></td>
         </tr>
         <tr>
            <td class="legend">Year: *</td>
            <td class="legend">Make: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtVehicleYear" size="4" maxLength="4"></input>
            </td>
            <td>
               <input id="txtVehicleMake" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Model: *</td>
            <td class="legend">VIN: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtVehicleModel" size="40" maxLength="50"></input>
            </td>
            <td>
               <input id="txtVehicleVIN" size="17" maxLength="17"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">License Plate: *</td>
            <td class="legend">License State: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtLicensePlate" size="10" maxLength="10"></input>
            </td>
            <td>
               <select id="selLicenseState">
                  <option value=""></option>
               </select>
            </td>
         </tr>
         <tr>
            <td colspan="2"><div class="tabHeader">Title Information</div></td>
         </tr>
         <tr>
            <td class="legend" colspan="2">Title Name(s): *</td>
         </tr>
         <tr>
            <td colspan="2">
               <input id="txtTitleNames" size="50" maxLength="100" title="Enter the Name(s) exactly as it appears on the Vehicle�s Title"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">State: *</td>
            <td class="legend">Was the Title Requested?:</td>
         </tr>
         <tr>
            <td>
               <select id="selTitleState">
                  <option value=""></option>
               </select>
            </td>
            <td>
               <select id="selTitleStatus">
                  <option value=""></option>
                  <option value="Requested">Requested</option>
               </select>
            </td>
         </tr>
      </table>
   </div>
   <div id="divSettlement" class="divStep">
      <table border="0" cellpadding="0" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="300px"/>
            <col width="300px"/>
         </colgroup>
         <tr>
            <td><div class="tabHeader">Settlement Information</div></td>
            <td><div class="tabHeader required">All fields marked with an asterisk (*) are required</div></td>
         </tr>
         <tr>
            <td class="legend">Settlement Date: *</td>
            <td class="legend">Settlement Amount: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtSettlementDate" size="10" maxLength="10" onBlur="formatDateObject(this)"></input>
               <a href="javascript:void(0)" onclick="javascript:showCalIframe(this, 'txtSettlementDate')" id="calCall" tabIndex="-1"><img src="images/calendar.gif" alt="" width="24" height="17" border="0" title="Click to select date from calendar"></a>
            </td>
            <td>
               <input id="txtSettlementAmount" size="20" maxLength="15" onBlur="formatCurrencyObject(this)"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Advance Amount: *</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td>
               <input id="txtAdvanceAmount" size="20" maxLength="15" onBlur="formatCurrencyObject(this)"></input>
            </td>
            <td>
            </td>
         </tr>
         <tr>
            <td><div class="tabHeader"><input type="checkbox" id="chkLienHolder" onclick="chkLienHolder_click()">Lien Holder Information</div></td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">Name: *</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td colspan="2">
               <input id="txtLienHolderName" size="75" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Address: *</td>
            <td class="legend">Address 2:</td>
         </tr>
         <tr>
            <td>
               <input id="txtLHAddress1" size="40" maxLength="50"></input>
            </td>
            <td>
               <input id="txtLHAddress2" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Zip: *</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td>
               <input id="txtLHAddressZip" size="5" maxLength="5"></input>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">City: *</td>
            <td class="legend">State: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtLHAddressCity" size="30" maxLength="30"></input>
            </td>
            <td>
               <select id="selLHAddressState">
                  <option value=""></option>
               </select>
            </td>
         </tr>
         <tr>
            <td class="legend">Phone: *</td>
            <td class="legend">Fax:</td>
         </tr>
         <tr>
            <td>
               <input id="txtLHPhone" size="25" maxLength="25"></input>
            </td>
            <td>
               <input id="txtLHFax" size="15" maxLength="15"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Email Address:</td>
            <td class="legend">Contact Name: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtLHEmailAddress" size="40" maxLength="50"></input>
            </td>
            <td>
               <input id="txtLHContactName" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Payoff Amount: *</td>
            <td class="legend">Expiration Date: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtLHPayoffAmount" size="20" maxLength="15" onBlur="formatCurrencyObject(this);populateLoGAmount();formatCurrencyObject(txtLoGAmount)"></input>
            </td>
            <td>
               <input id="txtLHPayoffExpDate" size="10" maxLength="10" onBlur="formatDateObject(this)"></input>
               <a href="javascript:void(0)" onclick="javascript:showCalIframe(this, 'txtLHPayoffExpDate', 1)" id="calCall" tabIndex="-1"><img src="images/calendar.gif" alt="" width="24" height="17" border="0" title="Click to select date from calendar"></a>
            </td>
         </tr>
         <tr>
            <td class="legend">Letter of Guarantee Amount: *</td>
            <td class="legend">Account Number: *</td>
         </tr>
         <tr>
            <td>
               <input id="txtLoGAmount" size="20" maxLength="15" onBlur="formatCurrencyObject(this)"></input>
            </td>
            <td>
               <input id="txtLHAccountNumber" size="30" maxLength="75"></input>
            </td>
         </tr>
         <tr>
            <td><div class="tabHeader">Salvage Information</div></td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">Name: *</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td colspan="2">
               <input id="txtSalvageName" size="75" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Address:</td>
            <td class="legend">Address 2:</td>
         </tr>
         <tr>
            <td>
               <input id="txtSalvageAddress1" size="40" maxLength="50"></input>
            </td>
            <td>
               <input id="txtSalvageAddress2" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Zip:</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td>
               <input id="txtSalvageAddressZip" size="5" maxLength="5"></input>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">City:</td>
            <td class="legend">State:</td>
         </tr>
         <tr>
            <td>
               <input id="txtSalvageAddressCity" size="30" maxLength="30"></input>
            </td>
            <td>
               <select id="selSalvageAddressState">
                  <option value=""></option>
               </select>
            </td>
         </tr>
         <tr>
            <td class="legend">Phone:</td>
            <td class="legend">Fax:</td>
         </tr>
         <tr>
            <td>
               <input id="txtSalvagePhone" size="25" maxLength="25"></input>
            </td>
            <td>
               <input id="txtSalvageFax" size="15" maxLength="15"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Email Address:</td>
            <td class="legend">Contact Name:</td>
         </tr>
         <tr>
            <td>
               <input id="txtSalvageEmailAddress" size="40" maxLength="50"></input>
            </td>
            <td>
               <input id="txtSalvageContactName" size="40" maxLength="50"></input>
            </td>
         </tr>
         <tr>
            <td class="legend">Control Number: *</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td>
               <input id="txtSalvageControlNumber" size="30" maxLength="75"></input>
            </td>
            <td></td>
         </tr>
         <tr>
            <td colspan="2" class="legend">Comments: (500 characters max)</td>
         </tr>
         <tr>
            <td colspan="2">
               <textarea id="txtComments" rows="10" cols="50" onkeypress="return (this.value.length <= 500)"></textarea>
            </td>
         </tr>
      </table>
   </div>
   <div id="divSummary" class="divStep">
      <table border="0" cellpadding="0" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="300px"/>
            <col width="300px"/>
         </colgroup>
         <tr>
            <td><div class="tabHeader">Summary</div></td>
            <td><div class="tabHeader required"></div></td>
         </tr>
         <tr>
            <td><div class="tabHeader">Basic Claim Information</div></td>
            <td><div class="tabHeader required">[&nbsp;<a href="javascript:editStep(0)" onmouseover="window.status = 'Edit Basic Claim Information';return true;" onmouseout="window.status='';return true;">Edit</a>&nbsp;]</div></td>
         </tr>
         <tr>
            <td class="legend">Claim Assigned By:</td>
            <td class="legend">Assigner relation to Insured:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_selAssignedBy"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_selAssignorRelation"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Claim Number:</td>
            <td></td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtClaimNumber"></div>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">Loss Date:</td>
            <td class="legend">Loss State:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtLossDate"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_selLossState"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Party:</td>
            <td class="legend">Coverage:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_selPartyCD"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_selCoverage"></div>
            </td>
         </tr>
      </table>   
      <table border="0" cellpadding="0" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="300px"/>
            <col width="300px"/>
         </colgroup>
         <tr>
            <td><div class="tabHeader">Insured Information</div></td>
            <td><div class="tabHeader required">[&nbsp;<a href="javascript:editStep(1)" onmouseover="window.status = 'Edit Insured Information';return true;" onmouseout="window.status='';return true;">Edit</a>&nbsp;]</div></td>
         </tr>
         <tr>
            <td class="legend">First Name:</td>
            <td class="legend">Last Name:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtInsuredFirstName"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtInsuredLastName"></div>
            </td>
         </tr>
         <tr>
            <td class="legend" colspan="2">Business Name:</td>
         </tr>
         <tr>
            <td colspan="2">
               <div class="dataSummary" id="divSummary_txtInsuredBusinessName"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Address</td>
            <td class="legend">Address 2:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtInsuredAddress1"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtInsuredAddress2"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Zip:</td>
            <td></td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtInsuredAddressZip"></div>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">City:</td>
            <td class="legend">State:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtInsuredAddressCity"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_selInsuredAddressState"></div>
            </td>
         </tr>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="300px"/>
            <col width="300px"/>
         </colgroup>
         <tr>
            <td><div class="tabHeader">Vehicle Owner Information</div></td>
            <td><div class="tabHeader required">[&nbsp;<a href="javascript:editStep(2)" onmouseover="window.status = 'Edit Vehicle Owner Information';return true;" onmouseout="window.status='';return true;">Edit</a>&nbsp;]</div></td>
         </tr>
         <tr>
            <td class="legend">First Name:</td>
            <td class="legend">Last Name:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtOwnerFirstName"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtOwnerLastName"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Business Name:</td>
            <td></td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtOwnerBusinessName"></div>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">Address</td>
            <td class="legend">Address 2:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtOwnerAddress1"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtOwnerAddress2"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Zip:</td>
            <td></td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtOwnerAddressZip"></div>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">City:</td>
            <td class="legend">State:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtOwnerAddressCity"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_selOwnerAddressState"></div>
            </td>
         </tr>
         <tr>
            <td><div class="tabHeader">Vehicle Information</div></td>
            <td><div class="tabHeader required">[&nbsp;<a href="javascript:editStep(2)" onmouseover="window.status = 'Edit Vehicle Information';return true;" onmouseout="window.status='';return true;">Edit</a>&nbsp;]</div></td>
         </tr>
         <tr>
            <td class="legend">Year:</td>
            <td class="legend">Make:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtVehicleYear"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtVehicleMake"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Model:</td>
            <td class="legend">VIN:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtVehicleModel"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtVehicleVIN"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">License Plate:</td>
            <td class="legend">License State:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtLicensePlate"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_selLicenseState"></div>
            </td>
         </tr>
         <tr>
            <td><div class="tabHeader">Title Information</div></td>
            <td><div class="tabHeader required">[&nbsp;<a href="javascript:editStep(2)" onmouseover="window.status = 'Edit Title Information';return true;" onmouseout="window.status='';return true;">Edit</a>&nbsp;]</div></td>
         </tr>
         <tr>
            <td class="legend" colspan="2">Title Name(s):</td>
         </tr>
         <tr>
            <td colspan="2">
               <div class="dataSummary" id="divSummary_txtTitleNames"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">State:</td>
            <td class="legend">Was the Title Requested?:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_selTitleState"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_selTitleStatus"></div>
            </td>
         </tr>
      </table>
      <table border="0" cellpadding="0" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="300px"/>
            <col width="300px"/>
         </colgroup>
         <tr>
            <td><div class="tabHeader">Settlement Information</div></td>
            <td><div class="tabHeader required">[&nbsp;<a href="javascript:editStep(3)" onmouseover="window.status = 'Edit Settlement Information';return true;" onmouseout="window.status='';return true;">Edit</a>&nbsp;]</div></td>
         </tr>
         <tr>
            <td class="legend">Settlement Date:</td>
            <td class="legend">Settlement Amount:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtSettlementDate"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtSettlementAmount"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Advance Amount:</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtAdvanceAmount"></div>
            </td>
            <td></td>
         </tr>
         <tr>
            <td><div class="tabHeader">Lien Holder Information</div></td>
            <td><div class="tabHeader required">[&nbsp;<a href="javascript:editStep(3)" onmouseover="window.status = 'Edit Lien Holder Information';return true;" onmouseout="window.status='';return true;">Edit</a>&nbsp;]</div></td>
         </tr>
         <tr>
            <td class="legend">Name:</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td colspan="2">
               <div class="dataSummary" id="divSummary_txtLienHolderName"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Address</td>
            <td class="legend">Address 2:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtLHAddress1"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtLHAddress2"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Zip:</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtLHAddressZip"></div>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">City:</td>
            <td class="legend">State:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtLHAddressCity"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_selLHAddressState"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Phone:</td>
            <td class="legend">Fax:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtLHPhone"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtLHFax"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Email Address:</td>
            <td class="legend">Contact Name:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtLHEmailAddress"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtLHContactName"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Payoff Amount:</td>
            <td class="legend">Expiration Date:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtLHPayoffAmount"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtLHPayoffExpDate"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Letter of Guarantee Amount:</td>
            <td class="legend">Account Number:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtLoGAmount"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtLHAccountNumber"></div>
            </td>
         </tr>
         <tr>
            <td><div class="tabHeader">Salvage Vendor Information</div></td>
            <td><div class="tabHeader required">[&nbsp;<a href="javascript:editStep(3)" onmouseover="window.status = 'Edit Salvage Information';return true;" onmouseout="window.status='';return true;">Edit</a>&nbsp;]</div></td>
         </tr>
         <tr>
            <td class="legend">Name:</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td colspan="2">
               <div class="dataSummary" id="divSummary_txtSalvageName"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Address</td>
            <td class="legend">Address 2:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtSalvageAddress1"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtSalvageAddress2"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Zip:</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtSalvageAddressZip"></div>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend">City:</td>
            <td class="legend">State:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtSalvageAddressCity"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_selSalvageAddressState"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Phone:</td>
            <td class="legend">Fax:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtSalvagePhone"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtSalvageFax"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Email Address:</td>
            <td class="legend">Contact Name:</td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtSalvageEmailAddress"></div>
            </td>
            <td>
               <div class="dataSummary" id="divSummary_txtSalvageContactName"></div>
            </td>
         </tr>
         <tr>
            <td class="legend">Control Number:</td>
            <td class="legend"></td>
         </tr>
         <tr>
            <td>
               <div class="dataSummary" id="divSummary_txtSalvageControlNumber"></div>
            </td>
            <td></td>
         </tr>
         <tr>
            <td class="legend" colspan="2">Comments:</td>
         </tr>
         <tr>
            <td colspan="2">
               <textarea rows="10" cols="50" id="divSummary_txtComments" readonly="readonly" style="border:1px solid #C0C0C0"></textarea>
            </td>
         </tr>
      </table>
   </div>
   <div id="divSubmit" class="divStep" style="width:750px;">
      <table border="0" cellpadding="0" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed">
         <colgroup>
            <col width="300px"/>
            <col width="450px"/>
         </colgroup>
         <tr>
            <td colspan="2">
                <div id="divSubmitDebug" class="hidden">
                  <textarea id="txtSubmitWA" rows="10" cols="70">
                  </textarea>
                </div>
                <div id="divSubmitSuccess" class="hidden">
                    <table width="750" border="0" cellpadding="10" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse">
                      <tr>
                        <td nowrap="">
                  
                         <P style="font-size:11pt">
                         Your claim has been assigned the following LYNX ID.
                  
                          <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td colspan="2" nowrap>&nbsp;</td>
                            </tr>
                            <tr>
                              <td nowrap class="legendnopad" style="font-size:14pt">LYNX ID:</td>
                              <td nowrap style="font-size:14pt"><div id="txtLynxID"></div></td>
                            </tr>
                            <tr>
                              <td nowrap class="legendnopad" style="font-size:11pt">Submitted:&nbsp;</td>
                              <td nowrap style="font-size:11pt"><span id="txtSubmitDate"></span></td>
                            </tr>
                          </table>
                  
                         </P>
                  
                        </td>
                      </tr>
                    </table>
                     <IFRAME SRC="blank.asp" NAME="uploadFrame" ID="uploadFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="width:700px; height:140px;"></IFRAME>
                       <table width="600" border="0" cellpadding="0" cellspacing="0">
                         <tr>
                           <td width="100%" align="right">
                             <a href="javascript:rtnMyClmDsktp()"
                                title="Return to My Claims Desktop"
                                onMouseOver="window.status='Return to My Claims Desktop'; return true"
                                onMouseOut="window.status=''; return true"> <img src="images/prev_button.gif" width="19" height="19" border="0" aligqn="top"><strong>Return</strong></a>
                              to My Claims Desktop.
                           </td>
                         </tr>
                       </table>
                     
                       <br>
                     
                     
                        <div id="divShowWADocument" class="hidden">
                           <P style="font-size:9pt">
                             To print a copy for your records click on the <img src="images/pdfprint.gif"/> print button.<br>
                             To save a local copy click on the <img src="images/pdfsave.gif"/> save button.
                           </P>
                     
                           <IFRAME SRC="blank.asp" NAME="prtFrame" ID="prtFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="width:708px; height:500px;border:1px solid #808080; padding:0px; background:transparent;"></IFRAME>
                     
                     
                           <br>
                        </div>
                        <div id="divShowWADocumentError" class="hidden">
                          <table border=0>
                            <tr>
                              <td valign=top>
                                <img border=0 src=images/warn_img.gif WIDTH=16 HEIGHT=16 hspace=6>
                              </td>
                              <td valign=top style='font-size:10pt; font-weight:bold;'>
                                Your claim has been submitted successfully but the Assignment Report cannot be displayed at this time.
                                A copy of the report will be sent to you via e-mail shortly and will be available in the Documents screen for this claim.
                                Sorry for the inconvenience.
                              </td>
                            </tr>
                          </table>
                        </div>
                </div>
                <div id="divSubmitFailure" class="hidden">
                    <table width="600" border="0" cellpadding="10" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse">
                      <tr>
                        <td nowrap="">
                          <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
                        </td>
                        <td width="100%" nowrap="" class="legendnopad" style="color:#FF0000; font-size:11pt; font-weight:bold;">
                          Your claim has not been submitted due to an error.
                        </td>
                      </tr>
                      <tr>
                        <td nowrap="">&#160;</td>
                        <td width="100%" height="30" nowrap="" class="legendnopad" style="font-size:10pt;">
                          Please
                          <a href="javascript:submitClaim()"
                             title="Re-submit claim"
                             onMouseOver="window.status='Re-submit claim'; return true"
                             onMouseOut="window.status=''; return true"><strong>re-submit</strong></a>
                          your claim.
                        </td>
                      </tr>
                    </table>
                  </div>
            </td>
         </tr>
      </table> 
      <form id="frmAssignment">
        <input id="lstrXML" name="lstrXML" type="hidden" />
      </form>  
   </div>
   
   
   <table border="0" cellpadding="4" cellspacing="0" style="boder-collapse:collapse;table-layout:fixed;margin:15 0 0 0">
      <colgroup>
         <col width="330px"/>
         <col width="90px"/>
         <col width="90px"/>
         <col width="90px"/>
      </colgroup>
      <tr>
         <td><a id="imgCancel" href="javascript:goCancel()" tabindex="600"><img src="images/btn_cancel.gif"/></a></td>
         <td><a id="imgBack" href="javascript:prevStep()" tabindex="601"><img src="images/btn_back.gif"/>
         <a id="imgEditCancel" href="javascript:editCancel()" class="hidden" tabindex="602"><img src="images/btn_cancel.gif"/></a></td>
         <td><a id="imgNext" href="javascript:nextStep()" tabindex="603"><img src="images/btn_next.gif"/></a>
         <a id="imgEditOK" href="javascript:editSave()" class="hidden" tabindex="604"><img src="images/btn_ok.gif"/></a></td>
         <td><a id="imgSubmit" href="javascript:submitClaim()" class="hidden" tabindex="605"><img src="images/btn_submit.gif"/></a></td>
      </tr>
   </table>
</div>
<iframe ID="CalFrame" NAME="CalFrame" SRC="blank.asp" STYLE="border:2px solid #404040; left:-500px; top:0px; display:none; position:absolute; width:170; height:186; z-index:100; filter: progid:DXImageTransform.Microsoft.Shadow(color='#666666', Direction=135, Strength=3);" MARGINHEIGHT=0 MARGINWIDTH=0 NORESIZE FRAMEBORDER=0 SCROLLING=NO></iframe>
<div id="progress" style="display:none">
<div id="progressMessage" style="font-size:12px;text-align:center;padding:5 5 10 5">Initializing new claim. Please wait...</div>
<center><img src="images/loading.gif"/></center>
</div>
<script language="javascript">
    $(document).ready(function() {
        showMessage("Initializing New Claim. Please wait.");
        $.ajax({
            type: "GET",
            url: "frmGetTLReference.asp",
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("An error occured while initializing claim.");
                hideMessage();
            },
            success: function(xml) {
                //init assigned by
                $(xml).find("OfficeUser").each(function() {
                    $("#selAssignedBy").addOption($(this).attr("userid"), $(this).attr("namefirst") + " " + $(this).attr("namelast"));
                });
                $("#selAssignedBy").selectOptions("");
                //init relation
                $(xml).find("Reference[list|=Relation]").each(function() {
                    $("#selAssignorRelation").addOption($(this).attr("value"), $(this).attr("name"));
                });
                $("#selAssignorRelation").selectOptions("14").attr("disabled", "true");
                //init party
                $(xml).find("Reference[list|=Party]").each(function() {
                    $("#selPartyCD").addOption($(this).attr("value"), $(this).attr("name"));
                });
                $("#selPartyCD").selectOptions("");
                //init covearage
                $(xml).find("Reference[list|=Coverage]").each(function() {
                    $("#selCoverage").addOption($(this).attr("value"), $(this).attr("name"));
                });
                $("#selCoverage").selectOptions("");
                //init states
                $(xml).find("Reference[list|=State]").each(function() {
                    $("#selLossState").addOption($(this).attr("value"), $(this).attr("name"));
                });
                $("#selLossState").selectOptions("");
                $("#selLossState").copyOptions("#selInsuredAddressState", "all");
                $("#selInsuredAddressState").selectOptions("");
                $("#selLossState").copyOptions("#selOwnerAddressState", "all");
                $("#selOwnerAddressState").selectOptions("");
                $("#selLossState").copyOptions("#selLicenseState", "all");
                $("#selLicenseState").selectOptions("");
                $("#selLossState").copyOptions("#selTitleState", "all");
                $("#selTitleState").selectOptions("");
                $("#selLossState").copyOptions("#selLHAddressState", "all");
                $("#selLHAddressState").selectOptions("");
                $("#selLossState").copyOptions("#selSalvageAddressState", "all");
                $("#selSalvageAddressState").selectOptions("");
                $(xml).find("Insurance").each(function() {
                    Insurance.id = $(this).attr("id");
                    Insurance.name = $(this).attr("name");
                });
                $(xml).find("CarrierRep").each(function() {
                    CarrierRep.Office = $(this).attr("officename");
                    CarrierRep.FirstName = $(this).attr("namefirst");
                    CarrierRep.LastName = $(this).attr("namelast");
                    CarrierRep.Phone = $(this).attr("phone");
                    CarrierRep.Email = $(this).attr("email");
                    CarrierRep.id = $(this).attr("userid");
                    CarrierRep.InsuranceName = $(this).attr("insurancecompanyname");
                });
                dataXML = xml;
                showStep(0);
                hideMessage();
                chkLienHolder_click();
            }
        });
        pageInit();
        document.onclick = hideCalFrame;
        parent.hideMsg();

        try {
            if (parent.outer) {
                parent.outer.scrollIntoView(true);
            }
        } catch (e) { }
    });
</script>

</body>
</html>
