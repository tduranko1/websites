
<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCryptTools.asp"-->

<%
  Dim lsUser, lsUserName, laUserName, lsSrvEnv, lsResultXML, lsGUID, lsStrXML, lsVerRetVal, docXML
  
  lsGUID = Request.Cookies("CPSession")("guidArray0")
  
  lsUser = fDecrypt("LYNXAPDKEY", Request.Cookies("CPSession")("authUser"))
  
  If lsUser = "" Then
    lsUserName = "Not Logged In"
  Else
  	lsUserName = lsUserNameFirst & " " & lsUserNameLast
  End If
  
  If Instr(1,strServerName, "DEV") > 0 Then	''' Development
  		lsSrvEnv = "Development"
  ElseIf Instr(1,strServerName, "STG") > 0 Then		''' Staging
  		lsSrvEnv = "Staging"
  ElseIf Instr(1,strServerName, "PRD") > 0 Or (uCASE(Left(strServerName,3)) = "BUY") Then	''' Production
  		lsSrvEnv = "Production"
  Else	'default
  		lsSrvEnv = strServerName
  End if

  set docXML = Server.CreateObject("MSXML2.DOMDocument")
  docXML.Async = False
  
  lsStrXML = "<Config Path='ClaimPoint/Version' />"
  docXML.LoadXML GetData(lsStrXML, "", "")
  lsVerRetVal = docXML.documentElement.text
  
  lsResultXML = GetPersistedData("<Status />", lsGUID, "", "", true)

  Set docXML = Nothing
%>

<HTML>
<HEAD>
<TITLE>About</TITLE>
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript">

  function getXML()
  {
    var lsXML = document.getElementById("statusXML").innerHTML;
    if (lsXML != "-1")
    {
      var objXML = new ActiveXObject("Microsoft.XMLDOM");
      objXML.loadXML(lsXML);
      var rootNode = objXML.documentElement.selectSingleNode("/ROOT");
      document.getElementById("apdVersion").innerHTML = rootNode.selectSingleNode("CONFIG/@version").nodeValue;
      document.getElementById("dbVersion").innerHTML = rootNode.selectSingleNode("DATABASE/@version").nodeValue;
    }
  }

</script>

</HEAD>
<BODY onLoad="getXML()">

  <span id="statusXML" style="display:none"><%=lsResultXML%></span>

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
  <TR>
  	<TD><IMG SRC="images/modal_TL.gif" ALT="" width="90" height="45"></TD>
  	<TD width="100%" align="right" valign="top">
    <a href=# onclick="javascript:window.close()" class="mini">Close</a>&nbsp;&nbsp;
    </TD>
  </TR>
</TABLE>

	<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
		<TR>
			<TD width="10%"><IMG SRC="images/modal_ML.gif" ALT="" width="70" height="65"></TD>
			<TD width="100%" background="images/modal_MC.gif" valign="top">
        <br>
        <div align="center"><p class="bodyBlue">Application Environment Info</p></div>
      </TD>
			<TD width="10%"><IMG SRC="images/modal_MR.gif" ALT="" width="70" height="65"></TD>
		</TR>
	</TABLE>
  
  <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="center">
    <TR>
      <TD><p class="bodyBlue">Application:</TD>
      <TD>ClaimPoint</TD>
    </TR>
    <TR>
      <TD><p class="bodyBlue">Current User:</TD>
      <TD>
        <%=lsUserName%>
      </TD>
    </TR>
    <TR>
      <TD><p class="bodyBlue">Environment:</TD>
      <TD>
        <%=lsSrvEnv%>
      </TD>
    </TR>
    <TR>
      <TD><p class="bodyBlue">ClaimPoint Version:</TD>
      <TD><%= lsVerRetVal %></TD>
    </TR>
    <TR>
      <TD><p class="bodyBlue">APD Version:</p></TD>
      <TD id="apdVersion"></TD>
    </TR>
    <TR>
      <TD><p class="bodyBlue">Database Version:</p></TD>
      <TD id="dbVersion"></TD>
    </TR>
  </TABLE>

</body>
</html>
