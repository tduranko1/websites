<!-- v1.5.0.0 -->

<%@ Language=VBScript %>
<%
  'Response.Cookies("CPSession")("currentClaim") = request("hidLynxID")
  'Response.Cookies("CPSession")("currentClaimAssignment") = request("hidAssignmentType")
  'Response.Cookies("CPSession")("currentVehiclesList") = request("hidVehiclesList")

  Dim liLynxID, liAspectID, lsAssignmentType, liVehNumber
  
  liLynxID = Request("hidLynxID")
  liAspectID = Request("hidAspectID")
  liVehNumber = Request("hidVehNumber")
  lsAssignmentType = Request("hidAssignmentType")
  Response.Cookies("CPSession")(liLynxID & "VehiclesList") = request("hidVehiclesList")
  Response.Cookies("CPSession")(liLynxID & "ClaimAssignment") = lsAssignmentType
  
  'Generate new 1-4 GUIDS since we just selected another claim
  'ClaimDetailGUID = guidArray1
  'VehicleDetailGUID = guidArray2
  'PropertyDetailGUID = guidArray3
  'ClaimInfoGUID = guidArray4

  Dim loSession, laGUIDs, liGUIDs, liCounter

  Set loSession = Server.CreateObject("APD.clsAppData")

  liGUIDs = 5
  laGUIDs = loSession.GenerateXGuids(liGUIDs)
  liGUIDs = liGUIDs - 1
  
  For liCounter = 1 to liGUIDs
    Response.Cookies("CPSession")("guidArray" & liCounter & liLynxID) = laGUIDs(liCounter)
  Next

  Set loSession = Nothing
%>

<script language="JavaScript">

  var liLynxID = "<%=liLynxID%>";
  var liAspectID = "<%=liAspectID%>";
  var liVehNumber = "<%=liVehNumber%>";
  
  if (liLynxID != "" || liLynxID != 0 || liAspectID != "" || liAspectID != 0)
  {
    var lsPath = "frmClaimInfo.asp?LynxID=" + liLynxID + "&AspectID=" + liAspectID + "&AssignmentType=" + "<%=lsAssignmentType%>" + "&VehNumber=" + liVehNumber;
    var lsWinName = "clm" + liLynxID + "_" + liVehNumber;
    var lsClaimWindow = window.open(lsPath, lsWinName, 'width=740,height=600,toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no');
    lsClaimWindow.focus();
  }
  else
    top.alert("Invalid claim number. Please retry.")

</script>
