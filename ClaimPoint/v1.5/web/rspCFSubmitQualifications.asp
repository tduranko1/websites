<!--#include file="includes/incCommonTop.asp"-->

<%
  lsPageName = "rspCFSubmitQulifications.asp"

  Dim liCertifiedFirstID, liAddressZip, loStrXML, lsStrXML, liRetVal
  Dim mexMonikerString 
         mexMonikerString = GetWebServiceURL()

  lsStrXML = request("lstrXML")
  liCertifiedFirstID = request("CertifiedFirstID")
  liAddressZip = request("AddressZip")
  
  On Error Resume Next
  
    'Set loStrXML = Server.CreateObject("PPGNAAPD.clsAPDData")
    Set loStrXML = GetObject(mexMonikerString)
    liRetVal = loStrXML.PutXML(lsStrXML)
  
    Set loStrXML = Nothing

function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function
  
%>

<script language="JavaScript" type="text/JavaScript">

  var liUpdateResult = <%=liRetVal%>;
  
  if (liUpdateResult > 0)
    top.location = 'frmCFShopInfo.asp?CertifiedFirstID=<%=liCertifiedFirstID%>&AddressZip=<%=liAddressZip%>';
  else
  {
    alert("Unable to submit your request. Please try again. Thank You.");
    history.go(-1);
  }

</script>