<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
	dim userName
	dim docPath
	dim isThumbNail
	dim liPdPosn


	userName = trim(Request("u"))
	docPath = trim(Request("d"))
	isThumbNail = trim(Request("t"))
   
   if isThumbNail <> "T" then isThumbNail = "F"

	liPdPosn = instr(1, docPath, ".")

	if liPdPosn > 0 then lsDocType = lcase(mid(docPath, liPdPosn))

    'check if all values are passed in from XML
     if userName = "" then
         response.write "<Root><Error code=""-1"" description=""Empty LogonID""></Root>"
     elseif Trim(docPath) = "" then
         response.write "<Root><Error code=""-1"" description=""Empty Document Path""></Root>"
     elseif Trim(isThumbNail) = "" then
         response.write "<Root><Error code=""-1"" description=""Empty ThumbNail Field""></Root>"

     else ' processing block
		Dim lsRequestXML
      
		if Trim(isThumbNail) = "T" and lsDocType = ".jpg" then
         Dim sXmlCall, oAPDData, mexMonikerString 
         mexMonikerString = GetWebServiceURL()

			'Set oAPDData = Server.CreateObject("PPGNAAPD.clsAPDData")
            Set oAPDData = GetObject(mexMonikerString)
			lsRequestXML = "<Thumbnail ImageLocation=""" + docPath + """ />"
			Response.ContentType="image/jpeg"
			Response.BinaryWrite oAPDData.GetDocument( lsRequestXML )

			Set oAPDData = nothing
		else

			dim lsDocType
			lsRequestXML = "<Document ImageLocation=""" & docPath & """/>"
			lsStyleSheet = "ViewXML.xsl" 

			if lsDocType = ".xml" then
				Response.ContentType = "text/HTML"
				Response.Write "<HTML><HEAD></HEAD><BODY>"
				Response.Write loAPD.GetAPDDocumentXML(lsRequestXML, lsStyleSheet)
				Response.Write "</BODY></HTML>"
			else
				select case lsDocType
					case ".jpg", ".jpeg"
						response.contenttype="image/jpeg"
					case ".tif", ".tiff"
						response.contenttype="image/tiff"
					case ".gif"
						response.contenttype="image/gif"
					case ".bmp"
						response.contenttype="image/bmp"
					case ".pdf"
						response.contenttype="application/pdf"
					case ".doc"
						response.contenttype="application/msword"
				end select
				response.binarywrite loAPD.GetAPDDocument(lsRequestXML)
			end if

		end if ' end of if thumbnaill block

     end if 'end if of processing block

function GetWebServiceURL()
    Dim strmexMonikerString
    Dim objFSO, ts, GetEnvironment       
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    Set ts = objFSO.OpenTextFile("C:\PGW\Environment\Config.txt")
    GetEnvironment = ts.ReadAll
    
    Select case GetEnvironment
        case "DEV"
        strmexMonikerString = "service:mexAddress=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://dlynxdataservice.pgw.local/LynxAPDComponentServices/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "STG"
        strmexMonikerString = "service:mexAddress=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://stglynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
        case "PRD"
        strmexMonikerString = "service:mexAddress=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc/mex"
        strmexMonikerString = strmexMonikerString + ", address=http://lynxdataserviceapd.pgw.local/LynxAPDComponent/EcadAccessor.svc"
        strmexMonikerString = strmexMonikerString + ", binding=BasicHttpBinding_IEcadAccessor, bindingNamespace='http://LynxAPDComponentServices'"
        strmexMonikerString = strmexMonikerString + ", contract=IEcadAccessor, contractNamespace='http://LynxAPDComponentServices'"
    End Select
    
   GetWebServiceURL = strmexMonikerString
end function

%>
