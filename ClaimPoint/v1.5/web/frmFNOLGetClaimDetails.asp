<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!-- v1.5.0.0 -->

<%
  Dim lsFNOLSearchCall, lsFNOLSearchRet, lsGUID
  Dim loXML, loVehicles, loVehicle, lbClaimNumberExists, loRoot, liLynxID
  Dim lsClaimNumber, lsLynxID, lsLastName, lsDateOfLoss, lsSearchType
  Dim lsCurrentAssignmentTypeID, lsCurrentAssignmentTypeDesc
  
  liOfficeID = Request.QueryString("officeid")
  lsLynxID = Request("txtGetLynxID")

  if lsLynxID <> "" then
     if isNumeric(lsLynxID) = false then
         writeError "LYNX id must be a numeric value. Please search again."
     end if
  else
      writeError "Missing LYNX id. System error."
  end if
  
  
'  response.write "lsClaimNumber = " & lsClaimNumber & "<br/>"
'  response.write "lsLynxID = " & lsLynxID & "<br/>"
'  response.write "lsLastName = " & lsLastName & "<br/>"
'  response.write "lsDateOfLoss = " & lsDateOfLoss & "<br/>"
'  response.write "lsSearchType = " & lsSearchType & "<br/>"
'response.end
  
  lsGUID = Request.Cookies("CPSession")("guidArray0")
'  lsCurrentAssignmentTypeID = Request.Cookies("CPSession")("CurrAssignmentId")
'  lsCurrentAssignmentTypeDesc = Request.Cookies("CPSession")("CurrAssignmentDescription")
  
  'lsFNOLSearchCall = "<ClaimNumberCheck ClientClaimNumber=""" + lsClaimNumber + """ InsuranceCompanyID=""" + liInsuranceCompanyID + """/>"
  lsFNOLSearchCall = "<FNOLService><RetrieveFNOL clientid=""" & liInsuranceCompanyID & """ lynxid=""" & lsLynxID & """ officeid=""" & liOfficeID & """ application=""APD"" /></FNOLService>"  
  lsFNOLSearchRet =  GetPersistedData(lsFNOLSearchCall, lsGUID, "", "", false)
  
'  response.write lsFNOLSearchRet
  
  sub writeError(strMessage)
      if strMessage <> "" then
%>
      <script language="javascript">
         alert("<%=strMessage%>");
         if (parent.document.getElementById("btnSearch"))
            parent.document.getElementById("btnSearch").disabled = false;
      </script>
<%    end if
      response.end
  end sub
%>
<script language="javascript">
   if (typeof(parent.showFNOLData) == "function"){
      parent.showFNOLData('<%=replace(replace(lsFNOLSearchRet, "'", "\'"), vbcrlf, "")%>')
   }
</script>
<!--#include file="includes/incCommonBottom.asp"-->
