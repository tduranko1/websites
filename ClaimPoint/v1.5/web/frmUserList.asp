<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<%
  Dim lsGUID, lsUserListCall, liSecLevel, lsUserType

  lsPageName = "frmUserList.asp"
  lsHelpPage = "CP_User_Management.htm"
  lsUserType = Request("M")
  
  if (liOfficeID = "0" or liOfficeID = "") and lsUserType="M" then
  	lsPageTitle = "Lynx Employees"
  	liSecLevel = 1
  'elseif lsUserType="P" then
  '	lsPageTitle = "Lynx Employee User List"
  '	liSecLevel = 2
  else
    lsUserType= "U"
  	lsPageTitle = "Users for " & "<Insurance Company>"
  	liSecLevel = 3
  end if

  liPageType = lcAfterInsuranceCompanySelect
  lsPageTitle = Replace(lsPageTitle,"<Insurance Company>", lsInsuranceCompanyName)
  bModal = false

  Response.Cookies("CPSession")("currentClaim") = ""
  Response.Cookies("CPSession")("currentClaim").Expires = Now()-1
  Response.Cookies("CPSession")("APDClaim") = ""
  Response.Cookies("CPSession")("APDClaim").Expires = Now()-1

  lsGUID = Request.Cookies("CPSession")("guidArray0")
%>
<!--#include file="includes/incGetData.asp"-->

<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->
<link href="includes/incGridSort.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="includes/incGridSort.js"></script>
<script type="text/javascript" src="includes/incGridHover.js"></script>
<script language="javascript">
  var sUserType = "<%=lsUserType%>";
  var sInsuranceCompany = "<%=lsInsuranceCompanyName%>";
  var sInsuranceCompanyID = "<%=liInsuranceCompanyID%>";
  
  function repaintTable(oTbl) {
    //reset the table color
    var obj = oTbl.tBodies[0];
    if (obj){
      var oRows = obj.rows;
      var oDisplayedRows = new Array();
      for (var i = 0; i < oRows.length; i++) {
        oRows[i].style.background = "#FFFFFF";
        if (oRows[i].style.display != "none" && oRows[i].style.visibility != "hidden")
          oDisplayedRows.push(oRows[i]);
      }
      for (var i = 0; i < oDisplayedRows.length; i+=2)
        oDisplayedRows[i].style.background = "#DFEFFF";
    }
  }
</script>
<%

  if liManageUsers = 1 then

    if liSecLevel = 1 then
      lsUserListCall = "<UserList ApplicationCD=""" + lsApplicationCD + """ />"
    elseif liSecLevel = 3 then
      lsUserListCall = "<UserList ApplicationCD=""" + lsApplicationCD + """ InsuranceCompanyID=""" + liInsuranceCompanyID + """/>"
    end if
    
    Dim lsUserList
    lsUserList = GetPersistedData(lsUserListCall, lsGUID, "UserList.xsl", "UserType=" & lsUserType, false)
    response.write lsUserList
  else
    response.write "You don't have permission to manage users. Please contact your supervisor."
  end if
%>

<script type="text/javascript">
  if (document.getElementById("userList"))
  {
    var SrtUsrTbl = new GridSort(document.getElementById("userList"), ["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"]);
    SrtUsrTbl.sort(0, false);
    repaintTable(document.getElementById("userList"));

    var SrtUsrTbl = new GridSort(document.getElementById("InactiveList"), ["CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString", "CaseInsensitiveString"]);
    SrtUsrTbl.sort(0, false);
    repaintTable(document.getElementById("InactiveList"));
  }
</script>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
