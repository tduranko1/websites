<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!-- v1.5.0.0 -->

<%
  lsPageName = "frmWaMainAddVehicle.asp"
  lsPageTitle = "Web Assignment"
  liPageType = lcAfterInsuranceCompanySelect
  
  Dim lsCurrentAssignmentDesc, liCurrentAssignmentId,lsCombined
  Dim lsUIFrameSrc, lsDCFrameSrc, lsAssignmentType, liClaimRepUserID, liAddVehicle
  Dim lsInsCoDetailsCall, lsInsuranceCompanyConfig, lsAssignmentAtSelectionFlag, loRoot, loXML
  Dim lsInsConfigXML, ElemList, lsAssignmentTypeIDList, laAssignmentTypeID, oNode, liLynxID

  Dim  lsShopAssignmentType

   lsShopAssignmentType= Request("ShopAssignmentType")


   If Not lsShopAssignmentType Is Nothing Then
     If lsShopAssignmentType = "ChoiceShop" Then
         lsCurrentAssignmentDesc = "Choice Shop"
      End If

     If lsShopAssignmentType = "ProgramShop" Then
         lsCurrentAssignmentDesc = "Program Shop"
      End If
   
   End If

     
   If lsShopAssignmentType Is Nothing Then
      'For Combined cookie to set title
    	lsCombined = Request.Cookies("CombinedType")
	    If Not lsCombined Is Nothing Then
   		   If lsCombined = "C" Then
     			lsCurrentAssignmentDesc = "Choice Shop"
   		   ElseIf lsCombined = "B" Then
     			lsCurrentAssignmentDesc = "DRP/Choice"
   		   Else 
      			lsCurrentAssignmentDesc = Request.Form("AssignmentDesc")
   		End If
	Else
		lsCurrentAssignmentDesc = Request.Form("AssignmentDesc")
  	End If

    End If



  liLynxID = Request("LynxID")
 'lsCurrentAssignmentDesc = Request.Form("AssignmentDesc")
  liCurrentAssignmentId = Request.Form("AssignmentId")
  lsAssignmentType = Request.Form("FrameSrc")
  liAddVehicle = 0

  if (lsCurrentAssignmentDesc <> "") then lsPageTitle = lsCurrentAssignmentDesc & " " & lsPageTitle

   if (lsCurrentAssignmentDesc = "Roofing") Then lsPageTitle = "Participant Contractor-Contractor Search"

  Response.Cookies("CPSession")("CurrAssignmentDescription") = lsCurrentAssignmentDesc
  Response.Cookies("CPSession")("CurrAssignmentId") = liCurrentAssignmentId

  'If the Ins Co Config shows that these flags are 1 then check if they should be turned on for the current assignment chosen.
  If liEmergencyLiabilityScript = 1 or liReserveLineTypeScript = 1 or liSB511Script = 1 Then

    Set loXML = Server.CreateObject("MSXML2.DOMDocument")
    loXML.Async = False

    lsInsConfigXML = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
    loXML.LoadXML GetPersistedData(lsInsConfigXML, lsInsCoConfigGUID, "", "", false)
  
    If Not loXML.ParseError = 0 Then
      Response.Write "<b>Error Code:</b> " & loXML.ParseError & "<br>"
  
    Else
  
      Set ElemList = loXML.SelectNodes("/Root/CustomScripting")

      If ElemList.length > 0 Then
        For each oNode in ElemList
      
          lsAssignmentTypeIDList = oNode.getAttribute("AssignmentTypeIDList")
          laAssignmentTypeID = split(lsAssignmentTypeIDList, "|")

          If PositionInArray(laAssignmentTypeID, liCurrentAssignmentId, vbBinaryCompare) < 0 _
                               and (LCASE(oNode.getAttribute("InputResponseName")) = "liability" _ 
                               or LCASE(oNode.getAttribute("InputResponseName")) = "emergency") Then
            liEmergencyLiabilityScript = 0
          End If
            
          If PositionInArray(laAssignmentTypeID, liCurrentAssignmentId, vbBinaryCompare) < 0 _
                               and LCASE(oNode.getAttribute("ConditionValue")) = "ca" Then
            liSB511Script = 0
          End If

          If PositionInArray(laAssignmentTypeID, liCurrentAssignmentId, vbBinaryCompare) < 0 _
                               and (LCase(oNode.getAttribute("InputResponseName")) = "linetype" _ 
                               or LCase(oNode.getAttribute("InputResponseName")) = "reserve") Then
            liReserveLineTypeScript = 0
          End If

        Next
    
      End If
  
    End If

  End If

  Response.Cookies("CPSession")("CurrAssignmentEmergencyLiability") = liEmergencyLiabilityScript
  Response.Cookies("CPSession")("CurrAssignmentSB511") = liSB511Script
  Response.Cookies("CPSession")("CurrAssignmentReserveLineType") = liReserveLineTypeScript

  'Return the position strLookingFor is found in aMyArray
  'if strLookingFor is not found -1 is returned
  'aMyArray = the string array that we are searching
  'strLookingFor = the string that we are looking for in the array aMyArray
  'compare = case-sensitive search (vbBinaryCompare) or a case-insensitive search (vbTextCompare)
  'ex. Response.Write PositionInArray(aSomeArray, "bar", vbBinaryCompare)
  'ex. Response.Write PositionInArray(aSomeArray, "Cool!", vbTextCompare) 
  Function PositionInArray(aMyArray, strLookingFor, compare)
  
    Dim iUpper, iLoop
    iUpper = UBound(aMyArray)
  
    For iLoop = LBound(aMyArray) to iUpper
      If compare = vbTextCompare then
        If CStr(UCase(aMyArray(iLoop))) = CStr(UCase(strLookingFor)) then
          PositionInArray = iLoop
          Exit Function
        End If
      Else
        If CStr(aMyArray(iLoop)) = CStr(strLookingFor) then
          PositionInArray = iLoop
          Exit Function
        End If
      End If
    Next
  
    'Didn't find the element in the array...
    PositionInArray = -1
  
  End Function

  if liLynxID <> "" then
    Response.Cookies("CPSession")("currentClaim") = liLynxID
  end if

  If lsAssignmentType = "ProgramShop" Then
    if Request.Form("ShopLocationID") <> "" then
      'shop was selected previously. Set the UIFrame to blank so that the code below will add a vehicle and set
      '  its shop to the selected one and display the vehicle details.
      lsUIFrameSrc = "blank.asp"
    else
      'shop was not selected previously. so do the shop selection now.
      lsUIFrameSrc = "frmWaShopSelectionAdv.asp?fromPage=frmClaimSearch"
    end if
    lsDCFrameSrc = "frmWaDataContainer.asp"
    liAddVehicle = 1
    ElseIf lsAssignmentType = "ChoiceShop" Then
    if Request.Form("ShopLocationID") <> "" then
      'shop was selected previously. Set the UIFrame to blank so that the code below will add a vehicle and set
      '  its shop to the selected one and display the vehicle details.
      lsUIFrameSrc = "blank.asp"
    else
      'shop was not selected previously. so do the shop selection now.
      lsUIFrameSrc = "frmWaChoiceShopSelectionAdv.asp?fromPage=frmClaimSearch"
    end if
    lsDCFrameSrc = "frmWaChoiceDataContainer.asp"
    liAddVehicle = 1
  ElseIf lsAssignmentType = "RepairReferral" Then
    if Request.Form("ShopLocationID") <> "" then
      'shop was selected previously. Set the UIFrame to blank so that the code below will add a vehicle and set
      '  its shop to the selected one and display the vehicle details.
      lsUIFrameSrc = "blank.asp"
    else
      'shop was not selected previously. so do the shop selection now.
      lsUIFrameSrc = "frmWaRRPShopSelectionAdv.asp?fromPage=frmClaimSearch"
    end if
    lsDCFrameSrc = "frmWaRRPDataContainer.asp"
    liAddVehicle = 1
  ElseIf lsAssignmentType = "DeskAudit" Then
    lsUIFrameSrc = "frmWaDAAddvehicle.asp?fromPage=frmAddVehicle"
    lsDCFrameSrc = "frmWaDeskAuditDataContainer.asp"

  ElseIf lsAssignmentType = "DeskReview" Then
    lsUIFrameSrc = "frmWaDAAddvehicle.asp?fromPage=frmAddVehicle"
    lsDCFrameSrc = "frmWaDeskAuditDataContainer.asp"

  ElseIf lsAssignmentType = "IA" Then
    lsUIFrameSrc = "frmWaIAAddVehicle.asp?fromPage=frmAddVehicle"
    lsDCFrameSrc = "frmWaIAassignmentDataContainer.asp"

  Else
    lsUIFrameSrc = "frmWaAddVehicle.asp"
    lsDCFrameSrc = "frmWaDataContainer.asp"
  End If

  Response.Cookies("APDSrh")("srhZipCode") = ""
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>

<%
  If lsAssignmentType = "DeskAudit" or lsAssignmentType = "DeskReview" Then
%>
  <script language="JavaScript" src="includes/incWaDeskAuditDatachk.js" type="text/javascript"></script>
<%
  ElseIf lsAssignmentType = "IA" Then
%>
  <script language="JavaScript" src="includes/incWaIAAssignmentDatachk.js" type="text/javascript"></script>
<%
  Else
%>
  <script language="JavaScript" src="includes/incWaDatachk.js" type="text/javascript"></script>
<%
  End If
%>

<script language="JavaScript">

  gbEmergencyLiabilityScript = <%= liEmergencyLiabilityScript %>;
  gbReserveLineTypeScript = <%= liReserveLineTypeScript %>;

  var lsPageName = "<%=lsPageName%>";
  var lsClaimNumber;
  var lsLossDate;
  var lsLossState;
  var lsLossDescription;
  var lsInsuredFName;
  var lsInsuredLName;
  var lsInsuredBName;
  var lsInsuredPhoneArea;
  var lsInsuredPhoneEx;
  var lsInsuredPhoneNum;
  var lsCallerFName;
  var lsCallerLName;
  var lsCallerRelation;
  var liUserID = "<%=liUserID%>";
  var liLynxID = "<%=liLynxID%>";
  var liInsuranceCompanyID;
  lsCarrierRepNameFirst = "<%=lsUserNameFirst%>";
  lsCarrierRepNameLast = "<%=lsUserNameLast%>";
  lsCarrierRepPhoneDay = "<%=lsPhoneAreaCode%>-<%=lsPhoneExchangeNumber%>-<%=lsPhoneUnitNumber%>";
  lsCarrierRepEmailAddress = "<%=lsUserEmail%>";
  lsCarrierOfficeName = "<%=lsOfficeName%>";
  lsCarrierName = "<%=lsInsuranceCompanyName%>";
  lsAssignmentDesc = "<%=lsCurrentAssignmentDesc%>";
  gsAddVeh = <%=liAddVehicle%>;

<%
  lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & server.urlencode(liInsuranceCompanyID) & """/>"
  
  lsInsuranceCompanyConfig =  GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "", "", true)
  
    set loXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
    loXML.Async = False
    loXML.LoadXML lsInsuranceCompanyConfig
    
    set loRoot = loXML.selectSingleNode("/Root")
    if not loRoot is nothing then
      lsAssignmentAtSelectionFlag = loRoot.getAttribute("AssignmentAtSelectionFlag")
    end if
    
%>

  lsAssignmentAtSelectionFlag = "<%=lsAssignmentAtSelectionFlag%>";

  
<%
    dim lsVehicleListCall, lsGUID, lsVehicleList, loVehicles, loVehicle, lsVehicleList2
    
    lsVehicleListCall = "<VehicleAllList InsuranceCompanyID=""" & liInsuranceCompanyID & """ LynxID=""" & liLynxID & """/>"
    lsGUID = Request.Cookies("CPSession")("guidArray0")
    
    lsVehicleList = GetPersistedData(lsVehicleListCall, lsGUID, "", "", false)

    set loXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
    loXML.Async = False
    loXML.LoadXML lsVehicleList
    
    set loVehicles = loXML.selectNodes("/Root/Vehicle")
    lsVehicleList2 = ""
    for each loVehicle in loVehicles
      lsVehicleList2 = lsVehicleList2 & """" & loVehicle.getAttribute("VehicleNumber") & "|" & loVehicle.getAttribute("ExposureCD") & """" & ","
    next
    
    lsVehicleList2 = left(lsVehicleList2, len(lsVehicleList2) - 1)
    
%>
  //an array of vehicle list vehicleNum|ExposureCD
  var laVehicleList = new Array(<%=lsVehicleList2%>);
var  newVehNum = parseInt(laVehicleList[laVehicleList.length - 1].split("|")[0], 10) + 1;
  

  function hideMsg()
  {
    var objMsg = document.getElementById("msgDiv");
    if (objMsg)
    {
      objMsg.style.display = "none";
    }
  }

  function showMsg()
  {
    window.scrollTo(0,0);
    var objMsg = document.getElementById("msgDiv");
    if (objMsg)
    {
      objMsg.style.display = "inline";
    }
  }

  function chkBeforeUnloadWindow()
  {
    // alert(window.frames.uiFrame.document.activeElement);
    // alert(document.activeElement.tagName);
    var lbShowWarning = true;
    var loObj = window.frames.uiFrame.document.activeElement;

    // if (document.activeElement.tagName == "A" || (loObj == "javascript:goCancel()") || (loObj == "javascript:rtnMyClmDsktp()"))
    //if (loObj == "javascript:goCancel()" || loObj == "javascript:rtnMyClmDsktp()")

    if (loObj == "javascript:goCancel()" || loObj == "javascript:rtnMyClmDsktp()" || loObj == "javascript:selectShop()")
      lbShowWarning = false;
      
    if (lbShowWarning == true)
    {
      event.returnValue = "Please use the navigation buttons (icons) on the page to navigate while entering an assignment.\n"+
                          "If you use the browser buttons to navigate, the data entered may be lost or invalid.\n"+
                          "If you wish to navigate to link clicked, press the OK button.";
    }
  }
  
  function initClaimData(sClaimID, sClaimNumber, sLossDate, sLossState, sLossDescription, sInsuredFName, 
                         sInsuredLName, sInsuredBName, sInsuredPhoneArea, sInsuredPhoneEx, 
                         sInsuredPhoneNum, sCallerFName, sCallerLName, sCallerRelation) {
    liLynxID = sClaimID; 
    lsClaimNumber = sClaimNumber;
    lsLossDate = sLossDate;
    lsLossState = sLossState;
    lsInsuredFName = sInsuredFName;
    lsInsuredLName = sInsuredLName;
    lsLossDescription = sLossDescription;
    lsInsuredBName = sInsuredBName;
    lsInsuredPhoneArea  = sInsuredPhoneArea;
    lsInsuredPhoneEx = sInsuredPhoneEx;
    lsInsuredPhoneNum = sInsuredPhoneNum;
    lsCallerFName = sCallerFName;
    lsCallerLName = sCallerLName;
    lsCallerRelation = sCallerRelation;    

    var loDcIFrameDoc = parent.getdcIFrameDoc();
    if (loDcIFrameDoc) {
      if (loDcIFrameDoc.getElementById("txtInsuredNameFirst"))
        loDcIFrameDoc.getElementById("txtInsuredNameFirst").value = lsInsuredFName;
      if (loDcIFrameDoc.getElementById("txtInsuredNameLast"))
        loDcIFrameDoc.getElementById("txtInsuredNameLast").value = lsInsuredLName;
      if (loDcIFrameDoc.getElementById("txtInsuredPhoneAC"))
        loDcIFrameDoc.getElementById("txtInsuredPhoneAC").value = lsInsuredPhoneArea;
      if (loDcIFrameDoc.getElementById("txtInsuredPhoneEN"))
        loDcIFrameDoc.getElementById("txtInsuredPhoneEN").value = lsInsuredPhoneEx;
      if (loDcIFrameDoc.getElementById("txtInsuredPhoneUN"))
        loDcIFrameDoc.getElementById("txtInsuredPhoneUN").value = lsInsuredPhoneNum;
      if (loDcIFrameDoc.getElementById("txtInsuredBusinessName"))
        loDcIFrameDoc.getElementById("txtInsuredBusinessName").value = lsInsuredBName;
      if (loDcIFrameDoc.getElementById("txtFNOLUserID"))
        loDcIFrameDoc.getElementById("txtFNOLUserID").value = liUserID;
    }
  }
  
  function initPSAddVehicle(){
      if(gsAddVeh == 1)
      {
        
        if (typeof(window.frames["dataContIfr"].insNewVeh) != "function") {
          window.setTimeout("initPSAddVehicle()", 250);
          return;
        }
        var lastVehNum = getLastVehNum();

        if (lastVehNum == 0){
          createNewVeh();
        }
        lastVehNum = getLastVehNum();
        
        var oDC = getdcIFrameDoc();
        if (oDC) {
          oDC.getElementById("txtShopLocationID_"+lastVehNum).value = "<%=Request.Form("ShopLocationID")%>";
          oDC.getElementById("txtShopSearchLogID_"+lastVehNum).value = "<%=Request.Form("ShopSearchLogID")%>";
          oDC.getElementById("txtSelectedShopRank_"+lastVehNum).value = "<%=Request.Form("SelectedShopRank")%>";
          oDC.getElementById("txtSelectedShopScore_"+lastVehNum).value = "<%=Request.Form("SelectedShopScore")%>";
          oDC.getElementById("txtShopRemarks_"+lastVehNum).value = "<%=Replace(Request.Form("ShopRemarks"), """", "\""")%>";
          oDC.getElementById("txtShopName_"+lastVehNum).value = "<%=Replace(Request.Form("ShopName"), """", "\""")%>";
          oDC.getElementById("txtShopAddress1_"+lastVehNum).value = "<%=Replace(Request.Form("ShopAddress1"), """", "\""")%>";
          oDC.getElementById("txtShopAddress2_"+lastVehNum).value = "<%=Replace(Request.Form("ShopAddress2"), """", "\""")%>";
          oDC.getElementById("txtShopCity_"+lastVehNum).value = "<%=Replace(Request.Form("ShopCity"), """", "\""")%>";
          oDC.getElementById("txtShopState_"+lastVehNum).value = "<%=Request.Form("ShopState")%>";
          oDC.getElementById("txtShopZip_"+lastVehNum).value = "<%=Request.Form("ShopZip")%>";
          oDC.getElementById("txtShopPhone_"+lastVehNum).value = "<%=Request.Form("ShopPhone")%>";
          oDC.getElementById("txtShopFax_"+lastVehNum).value = "<%=Request.Form("ShopFax")%>";
          oDC.getElementById("txtShopMFopen_"+lastVehNum).value = "<%=Request.Form("ShopMFopen")%>";
          oDC.getElementById("txtShopMFClosed_"+lastVehNum).value = "<%=Request.Form("ShopMFClosed")%>";
          oDC.getElementById("txtShopSatOpen_"+lastVehNum).value = "<%=Request.Form("ShopSatOpen")%>";
          oDC.getElementById("txtShopSatClosed_"+lastVehNum).value = "<%=Request.Form("ShopSatClosed")%>";
          
          document.getElementById("uiFrame").src = "frmWaAddVehicle.asp?fromPage=ShopSelection";
        }
      }
  }
  
</script>

<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->

  <DIV id="msgDiv" name="msgDiv" style="position:relative; top:60px; left:200px; display:inline;">

    <table width="220" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
      <tr>
        <td height="20" id="msgTxt" name="msgTxt" style="text-align:center; font-size:12px; font-weight:bold; color:#000080;">Loading... Please Wait.</td>
      </tr>
      <tr>
        <td height="20" style="text-align:center"><img src="images/loading_anim.gif" alt="" width="94" height="17" border="0"></td>
      </tr>
    </table>
    <br>
    <img src="images/spacer.gif" border="0" WIDTH="1" HEIGHT="220">
    <br>
<input type="hidden" name="NextVehNum" id="txtNextVehNum" value="" style="visibility: hidden;">
  </DIV>
<script>
document.getElementById("txtNextVehNum").value = newVehNum;
</script>
<IFRAME SRC="<%=lsUIFrameSrc%>" NAME="uiFrame" ID="uiFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="width:100%; height:1000px; border:1px; background:transparent;"></IFRAME>
<IFRAME SRC="<%=lsDCFrameSrc%>" NAME="dataContIfr" ID="dataContIfr" WIDTH="0" HEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="overflow:auto;visibility1:hidden;"></IFRAME>
  <%if (lsAssignmentType = "ProgramShop" or lsAssignmentType = "RepairReferral" or lsAssignmentType = "ChoiceShop") and liAddVehicle = 1 and Request.Form("ShopLocationID") <> "" then%>
    <script language="javascript">
        initPSAddVehicle();
    </script>
  <%end if%>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->