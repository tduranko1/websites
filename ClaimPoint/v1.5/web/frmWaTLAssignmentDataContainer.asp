<html>
<head>
<title>WaIAAssignmentDataContainer</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script language="JavaScript" type="text/JavaScript">

  function updClaimValues()
  {
    if (document.getElementById('txtOwnerPhoneAC').value != "")
    {
    	document.getElementById('txtOwnerPhoneSumm').value = document.getElementById('txtOwnerPhoneAC').value +"-"+ document.getElementById('txtOwnerPhoneEN').value +"-"+ document.getElementById('txtOwnerPhoneUN').value;
    	document.getElementById('txtOwnerPhone').value = document.getElementById('txtOwnerPhoneAC').value + document.getElementById('txtOwnerPhoneEN').value + document.getElementById('txtOwnerPhoneUN').value;
    }
    else
    {
    	document.getElementById('txtOwnerPhoneSumm').value = "";
    	document.getElementById('txtOwnerPhone').value = "";
    }

    if (document.getElementById('txtOwnerNightPhoneAC').value != "")
    {
    	document.getElementById('txtOwnerNightPhoneSumm').value = document.getElementById('txtOwnerNightPhoneAC').value +"-"+ document.getElementById('txtOwnerNightPhoneEN').value +"-"+ document.getElementById('txtOwnerNightPhoneUN').value;
    	document.getElementById('txtOwnerNightPhone').value = document.getElementById('txtOwnerNightPhoneAC').value + document.getElementById('txtOwnerNightPhoneEN').value + document.getElementById('txtOwnerNightPhoneUN').value;
    }
    else
    {
    	document.getElementById('txtOwnerNightPhoneSumm').value = "";
    	document.getElementById('txtOwnerNightPhone').value = "";
    }

    if (document.getElementById('txtOwnerAltPhoneAC').value != "")
    {
    	document.getElementById('txtOwnerAltPhoneSumm').value = document.getElementById('txtOwnerAltPhoneAC').value +"-"+ document.getElementById('txtOwnerAltPhoneEN').value +"-"+ document.getElementById('txtOwnerAltPhoneUN').value;
    	document.getElementById('txtOwnerAltPhone').value = document.getElementById('txtOwnerAltPhoneAC').value + document.getElementById('txtOwnerAltPhoneEN').value + document.getElementById('txtOwnerAltPhoneUN').value;
    }
    else
    {
    	document.getElementById('txtOwnerAltPhoneSumm').value = "";
    	document.getElementById('txtOwnerAltPhone').value = "";
    }

    if (document.getElementById('txtLocationAreaCode').value != "")
    {
    	document.getElementById('txtLocationPhoneSumm').value = document.getElementById('txtLocationAreaCode').value +"-"+ document.getElementById('txtLocationExchangeNumber').value +"-"+ document.getElementById('txtLocationUnitNumber').value;
    	document.getElementById('txtLocationPhone').value = document.getElementById('txtLocationAreaCode').value + document.getElementById('txtLocationExchangeNumber').value + document.getElementById('txtLocationUnitNumber').value;
    }
    else
    {
    	document.getElementById('txtLocationPhoneSumm').value = "";
    	document.getElementById('txtLocationPhone').value = "";
    }
  }
  
</script>

</head>

<body>

<form name="tblIAAssignmentInfo">

  <table name="tblClaim" id="tblClaim">
    <tr>
    	<td>
      	<input name="NoticeMethodID" id="txtNoticeMethodID" type="text" value="5">
      	<input name="DataSource" id="txtDataSource" type="text" value="Claim Point Web Assignment">
      	<input name="FNOLUserID" id="txtFNOLUserID" type="text">
      	<input name="InsuranceCompanyID" id="txtInsuranceCompanyID" type="text">
      	<input name="AssignmentAtSelectionFlag" id="txtAssignmentAtSelectionFlag" type="text">
      	<input name="DemoFlag" id="txtDemoFlag" type="text">
      	<input name="AssignmentDescription" id="txtAssignmentDescription" type="text">
        <input name="CarrierUser" id="txtCarrierUser" type="text" opt="yes">
      	<input name="CarrierOfficeName" id="txtCarrierOfficeName" type="text">
        <input name="CarrierRepNameFirst" id="txtCarrierRepNameFirst" type="text">
        <input name="CarrierRepNameLast" id="txtCarrierRepNameLast" type="text">
        <input name="CarrierRepUserID" id="txtCarrierRepUserID" type="text" value="">
        <input name="CarrierRepPhoneDay" id="txtCarrierRepPhoneDay" type="text">
        <input name="CarrierRepEmailAddress" id="txtCarrierRepEmailAddress" type="text">
        <input name="CarrierName" id="txtCarrierName" type="text">
      	<input name="CoverageClaimNumber" id="txtCoverageClaimNumber" type="text">
      	<input name="LossDate" id="txtLossDate" type="text">
      	<input name="LossAddressState" id="txtLossAddressState" type="text">
      	<input name="CallerNameFirst" id="txtCallerNameFirst" type="text">
      	<input name="CallerNameLast" id="txtCallerNameLast" type="text">
      	<input name="CallerRelationToInsuredID" id="txtCallerRelationToInsuredID" type="text">
      	<input name="CallerRelationToInsuredIDDescription" id="txtCallerRelationToInsuredIDDescription" type="text">
      	<input name="InsuredNameFirst" id="txtInsuredNameFirst" type="text">
      	<input name="InsuredNameLast" id="txtInsuredNameLast" type="text">
      	<input name="InsuredBusinessName" id="txtInsuredBusinessName" type="text">
      	<input name="CollisionDeductibleAmt" id="txtCollisionDeductibleAmt" type="text">
      	<input name="CollisionLimitAmt" id="txtCollisionLimitAmt" type="text">
      	<input name="ComprehensiveDeductibleAmt" id="txtComprehensiveDeductibleAmt" type="text">
      	<input name="ComprehensiveLimitAmt" id="txtComprehensiveLimitAmt" type="text">
      	<input name="LiabilityDeductibleAmt" id="txtLiabilityDeductibleAmt" type="text">
      	<input name="LiabilityLimitAmt" id="txtLiabilityLimitAmt" type="text">
      	<input name="UnderInsuredDeductibleAmt" id="txtUnderInsuredDeductibleAmt" type="text">
      	<input name="UnderInsuredLimitAmt" id="txtUnderInsuredLimitAmt" type="text">
      	<input name="UnInsuredDeductibleAmt" id="txtUnInsuredDeductibleAmt" type="text">
      	<input name="UnInsuredLimitAmt" id="txtUnInsuredLimitAmt" type="text">
      	<input name="TimeStarted" id="txtTimeStarted" type="text">
      	<input name="TimeFinished" id="txtTimeFinished" type="text">
      	<input name="IntakeStartSeconds" id="txtIntakeStartSeconds" type="text">
      	<input name="IntakeEndSeconds" id="txtIntakeEndSeconds" type="text">
      	<input name="IntakeSeconds" id="txtIntakeSeconds" type="text">
      	<input name="InsuredName" id="txtInsuredName" type="text">
      	<input name="ClaimSubmitted" id="txtClaimSubmitted" type="text" value="false" opt="yes">
        <textarea name="Remarks" id="txtDescLossDamage"></textarea>
      </td>
    </tr>
  </table>

  <table name="tblVehicle" id="tblVehicle">
    <tr>
    	<td programShop="false">
        <input name="AssignmentTypeID" id="txtAssignmentTypeID" type="text">
        <input name="AssignmentTypeIDDescription" id="txtAssignmentTypeIDDescription" type="text">
        <input name="ExposureCD" id="txtExposureCD" type="text">
        <input name="CoverageProfileCD" id="txtCoverageProfileCD" type="text">
        <input name=DeductibleAmt id=txtDeductibleAmt type=text>
        <input name=LimitAmt id=txtLimitAmt type=text>
        <input name=CoverageProfileUiCD id=txtCoverageProfileUiCD type=text>
        <input name="OwnerNameFirst" id="txtOwnerNameFirst" type="text">
        <input name="OwnerNameLast" id="txtOwnerNameLast" type="text">
        <input name="OwnerBusinessName" id="txtOwnerBusinessName" type="text">
        <input name="OwnerPhoneAC" id="txtOwnerPhoneAC" type="text">
        <input name="OwnerPhoneEN" id="txtOwnerPhoneEN" type="text">
        <input name="OwnerPhoneUN" id="txtOwnerPhoneUN" type="text">
        <input name="OwnerPhone" id="txtOwnerPhone" type="text">
        <input name="OwnerNightPhoneAC" id="txtOwnerNightPhoneAC" type="text">
        <input name="OwnerNightPhoneEN" id="txtOwnerNightPhoneEN" type="text">
        <input name="OwnerNightPhoneUN" id="txtOwnerNightPhoneUN" type="text">
        <input name="OwnerNightPhone" id="txtOwnerNightPhone" type="text">
        <input name="OwnerAltPhoneAC" id="txtOwnerAltPhoneAC" type="text">
        <input name="OwnerAltPhoneEN" id="txtOwnerAltPhoneEN" type="text">
        <input name="OwnerAltPhoneUN" id="txtOwnerAltPhoneUN" type="text">
        <input name="OwnerAltPhone" id="txtOwnerAltPhone" type="text">
        <input name="OwnerBestPhoneCD" id="txtOwnerBestPhoneCD" type="text">
        <input name="OwnerPhoneSumm" id="txtOwnerPhoneSumm" type="text">
        <input name="OwnerNightPhoneSumm" id="txtOwnerNightPhoneSumm" type="text">
        <input name="OwnerAltPhoneSumm" id="txtOwnerAltPhoneSumm" type="text">
        <input name="VehicleYear" id="txtVehicleYear" type="text">
        <input name="Make" id="txtMake" type="text">
        <input name="Model" id="txtModel" type="text">
        <input name="LicensePlateNumber" id="txtLicensePlateNumber" type="text">
        <input name="VIN" id="txtVIN" type="text">
        <input name="Mileage" id="txtMileage" type="text">
        <input name="Color" id="txtColor" type="text">
        <input name="Drivable" id="txtDrivable" type="text" value="1">
        <input name="LocationName" id="txtLocationName" type=text>
        <input name="LocationAddress1" id="txtLocationAddress1" type=text>
        <input name="LocationAddressCity" id="txtLocationAddressCity" type=text>
        <input name="LocationAddressState" id="txtLocationAddressState" type=text>
        <input name="LocationAddressZip" id="txtLocationAddressZip" type=text>
        <input name="LocationAreaCode" id="txtLocationAreaCode" type=text>
        <input name="LocationExchangeNumber" id="txtLocationExchangeNumber" type=text>
        <input name="LocationUnitNumber" id="txtLocationUnitNumber" type=text>
        <input name="LocationPhone" id="txtLocationPhone" type="text">
        <input name="LocationPhoneSumm" id="txtLocationPhoneSumm" type="text">
        <textarea name="Remarks" id="txtRemarksToIA"></textarea>
        <input name=ClientCoverageTypeID id=txtClientCoverageTypeUiID type=text>
        <input name=ClientCoverageTypeDesc id=txtClientCoverageTypeDesc type=text>
      </td>
    </tr>
  </table>

</form>

</body>
</html>
