<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->

<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
  lsPageName = "rspSubmitComment.asp"
  
  Dim lsStrXML, loAPDData, docXML, lsEmailStrXML, liNoteRetVal, liWorkflowRetVal, lbSubmitStatus
  Dim lsRedirectURL, liLynxID, liAssignmentType, liAspectID, liStatusID, liVehNumber
  
  liLynxID = Request("hidLynxID")
  liAspectID = request("hidAspectID")
  liAssignmentType = Request("hidAssignmentType")
  liStatusID = request("hidStatusID")
  liVehNumber = request("hidVehNumber")
  
  lsStrXML =  "<NoteInsDetail " + _
                  " ClaimAspectID=""" + liAspectID + """" + _
                  " NoteTypeID=""" + Request("hidNoteTypeID") + """" + _
                  " StatusID=""" + liStatusID + """" + _
                  " Note=""" + server.urlencode(Request("hidNote")) + """" + _
                  " UserID=""" + Request("hidUserID") + """" + _
                  " />"
  
  On Error Resume Next
  Set loAPDData = Server.CreateObject("PPGNAAPD.clsAPDData")
  liNoteRetVal = loAPDData.PutXML(lsStrXML)
  
  If liNoteRetVal = 1 Then
  
    lsStrXML =  "<WorkflowNotifyEvent " + _
                    " EventID=""" + Request("hidEventID") + """" + _
                    " ClaimAspectID=""" + liAspectID + """" + _
                    " LynxID=""" + liLynxID + """" + _
                    " UserID=""" + Request("hidUserID") + """" + _
                    " />"

    On Error Resume Next
    Set loAPDData = Server.CreateObject("PPGNAAPD.clsAPDData")
    liWorkflowRetVal = loAPDData.PutXML(lsStrXML)

    If liWorkflowRetVal = 1 Then
      lbSubmitStatus = 1
    Else
      lbSubmitStatus = 0
    End If
  
  Else
    lbSubmitStatus = 0
  End If

  Set loAPDData = Nothing

  lsRedirectURL = "frmSubmitComment.asp?FromPage=rsp&SubmitStatus=" & lbSubmitStatus & "&Note=" & server.urlencode(Request("hidNote")) & "&LynxID=" & liLynxID & "&AssignmentType=" & liAssignmentType & "&AspectID=" & liAspectID & "&StatusID=" & liStatusID & "&VehNumber=" & liVehNumber
%>

<script language="JavaScript" type="text/JavaScript">

    parent.location = "<%= lsRedirectURL %>";
      
</script>

<!--#include file="includes/incCommonBottom.asp"-->
