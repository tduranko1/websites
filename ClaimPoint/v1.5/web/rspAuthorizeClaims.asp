<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!-- v1.4.3.0 -->

<%
  lsPageName = "rspAuthorizeClaims.asp"

  Dim lsStrXML, loAPDData, lsRetVal, docXML, oNode, oNodes, sAlertMsg
  
  lsStrXML =  "<AuthorizationListUpdate" + _
                  " AuthorizedClaimAspectIDs=""" + request("AuthorizedClaimAspectIDs") + """" + _
                  " UserID=""" + request("UserID") + """" + _
                  " />"
  
  On Error Resume Next
  
  Set loAPDData = Server.CreateObject("PPGNAAPD.clsAPDData")
  
  lsRetVal = GetData(lsStrXML, "", "")
  
  Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML lsRetVal
  
  If Not docXML.ParseError = 0 Then
    Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
  Else

    If lcase(docXML.documentElement.nodeName) = "root" Then

      Set oNodes = docXML.documentElement.selectNodes("Claim")
    
      For Each oNode in oNodes
        
        If sAlertMsg <> "" Then
          sAlertMsg = sAlertMsg & "\n"
        End If

        If lcase(oNode.getAttribute("AlreadyApproved")) = "true" Then
          sAlertMsg = sAlertMsg & "LYNX ID " & oNode.getAttribute("LynxID") & " - " & oNode.getAttribute("VehicleNumber") & " approved by " & oNode.getAttribute("AuthorizingUserNameFirst") & " " & oNode.getAttribute("AuthorizingUserNameLast")
        End If
        
      Next
  
      Set oNodes = Nothing

    End If

  End If

  Set loAPDData = Nothing

%>

<script language="JavaScript" type="text/JavaScript">
  
  var sApprovedMsg = "<%=sAlertMsg%>";
  
  if (sApprovedMsg != "")
    alert("Please Note: Claim(s) previously approved:\n\n" + sApprovedMsg + "\n\n" + "Click the OK button to reload the page.");

  parent.document.location.href = "frmAuthPayments.asp";
      
</script>
