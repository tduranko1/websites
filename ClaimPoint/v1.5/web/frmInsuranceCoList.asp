<!--#include file="includes/incCommonTop.asp"-->
<%
lsPageName = "frmInsuranceCoList.asp"
lsPageTitle = "Insurance Companies List"
liPageType = lcBeforeInsuranceCompanySelect
bmodal=true
'add code here to show the message in the status bar and do paging
if Request("Msg")= 2 then
	lsMessage = "Insurance companies with users cannot be deleted."
elseif	Request("Msg")= 1 then
	lsMessage = "Action successful."
elseif	Request("Msg")= 3 then
	lsMessage = "Unknown Error"
end if


%>
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incCommonHTMLStart.asp"-->
<%

dim laReturnVal, liPages, lsCurrentPage, lsPageSize
dim laInsuranceCompanies
dim lsInsuranceCompanyName
dim liInsuranceCompanyID
dim liInsuranceLynxCompanyID
dim lbIsBlue, lsColor, lsClass

lsCurrentPage = Request("P")
if lsCurrentPage = "" then lsCurrentPage = 1
lsPageSize = 10

laReturnVal = loAPD.GetData("InsuranceCompanies", "", false, "", true, lsCurrentPage, lsPageSize)

liPages = -1
if isArray(laReturnVal) then
	laInsuranceCompanies = laReturnVal(0)
	liPages = laReturnVal(1)
	'Response.Write "liPages=" & liPages
end if
%>
<script language="JavaScript">
	function setInsuranceCompanyID(piInsuranceCompanyID)
	{
		var confirmResult = window.confirm("Delete this Insurance Company?");
		if (confirmResult) {
			document.frmMainForm.hidInsuranceCompanyID.value = piInsuranceCompanyID;
			document.frmMainForm.submit();
		}
	}
</script>

<form name="frmMainForm" id="frmMainForm" method="POST" action="p_delete_InsuranceCo.asp">
<input type="hidden" name="hidInsuranceCompanyID" id="hidInsuranceCompanyID">

			<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse" bordercolor="#00099">
				<tr>
					<td width="8" valign="top" background="images/table_CL_blue.gif" height="8">
					<img border="0" src="images/table_TL_blue.gif" width="8" height="8"></td>
					<td background="images/table_TC_blue.gif" height="8"><img border="0" src="images/spacer.gif" width="25" height="8"></td>
					<td background="images/table_TC_blue.gif" colspan="2" height="8"><img border="0" src="images/spacer.gif" width="25" height="8"></td>
					<td width="8" background="images/table_CR_blue.gif" valign="top" height="8">
					<img border="0" src="images/table_TR_blue.gif" width="8" height="8"></td>
				</tr>
				
				<tr>
					<td width="8" background="images/table_CL_blue.gif" class="cell">&nbsp;</td>
					<td background="images/lt_blue.gif" class="cell" nowrap>
						<p class="bodyBlue">Insurance Company:</td>
					<td background="images/lt_blue.gif" class="cell" align="center">
						<p class="formCaption">ID #</td>
					<td background="images/lt_blue.gif" class="cell" width="100" align="right">
						<a href="frmInsuranceCoDetail.asp">
						<img border="0" src="images/btn_add_new.gif" width="83" height="31"></a></td>
					<td width="8" background="images/table_CR_blue.gif" class="cell">&nbsp;</td>
				</tr>
			<%
				lbIsBlue = true
				lsColor = "blue"
				if isArray(laInsuranceCompanies) then
					lbIsBlue = false
					lsColor = "white"
					liStop = ubound(laInsuranceCompanies, 2)
			%>		
					
			<%		
					for liCounter = 0 to liStop
						lsInsuranceCompanyName = laInsuranceCompanies(0, liCounter)
						liInsuranceLynxCompanyID = laInsuranceCompanies(2, liCounter)
						liInsuranceCompanyID = laInsuranceCompanies(1, liCounter)
						if lbIsBlue then lsColor = "blue" else lsColor = "white"
						if liCounter < liStop then lsClass = "class=""cell""" else lsClass = ""
			%>	
						<tr>
							<td width="8" background="images/table_CL_<%=lsColor%>.gif" valign="bottom" <%=lsClass%>>&nbsp;</td>
							<td <%if lbIsBlue then%> background="images/lt_blue.gif" <%end if%> <%=lsClass%> nowrap>
							<p class="body"><%=lsInsuranceCompanyName%></td>
							<td <%if lbIsBlue then%> background="images/lt_blue.gif" <%end if%> <%=lsClass%> align="center"><font class="body"><%=liInsuranceLynxCompanyID%></font></td>
							<td <%if lbIsBlue then%> background="images/lt_blue.gif" <%end if%> <%=lsClass%> width="100" align="right">
								<a href="JavaScript:setInsuranceCompanyID(<%=liInsuranceCompanyID%>)">
									<img border="0" src="images/btn_delete_<%=lsColor%>.gif" width="83" height="31"></a></td>
							<td width="8" background="images/table_CR_<%=lsColor%>.gif" <%if lsColor = "white" then%>valign="bottom"<%end if%>  <%=lsClass%>>&nbsp;</td>
						</tr>
			<%
						lbIsBlue = not lbIsBlue
					next
					lbIsBlue = not lbIsBlue
				else
					lsColor = "white"
					lbIsBlue = not lbIsBlue
			%>		
					<tr>
						<td width="8" background="images/table_CL_white.gif">&nbsp;</td>
						<td background="images/lt_white.gif" nowrap><p class="bodyblue">No Insurance Companies Configured.</td>
						<td background="images/lt_white.gif" align="center">&nbsp;</td>
						<td background="images/lt_white.gif" width="100" align="right">&nbsp;</td>
						<td width="8" background="images/table_CR_white.gif">&nbsp;</td>
					</tr>
				<%end if%>
				<tr>
					<td width="8" background="images/table_CL_<%=lsColor%>.gif" valign="bottom" height="8">
					<img border="0" src="images/table_BL_<%=lsColor%>.gif" width="8" height="8"></td>
					<td class="cell" height="8" <%if lbIsBlue then%>background="images/lt_blue.gif"<%end if%> ><img border="0" src="images/spacer.gif" width="25" height="8"></td>
					<td class="cell" colspan="2"<%if lbIsBlue then%>height="8" background="images/lt_blue.gif"<%end if%> ><img border="0" src="images/spacer.gif" width="25" height="8"></td>
					<td width="8" background="images/table_CR_<%=lsColor%>.gif" valign="bottom" height="8">
					<img border="0" src="images/table_BR_<%=lsColor%>.gif" width="8" height="8"></td>
				</tr>
			</table>
			<table>
				<tr>
					<%
					lsCurrentPage = cint(lsCurrentPage)
					for liCounter = 1 to liPages
						Response.Write "<td class=""cell"">"
						
						if liCounter <> lsCurrentPage then
							Response.Write "<a href=""" & lsPageName & "?P=" & liCounter & """>" & liCounter & "</a>"
						else
							Response.Write "<b>" & liCounter & "</b>"
						end if
						
						Response.Write "</td>"
					next
					%>
				</tr>
			</table>
		</td>
	</tr>		
</table>	
</form>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
