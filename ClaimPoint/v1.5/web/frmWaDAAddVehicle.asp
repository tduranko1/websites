<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetCurrAssignment.asp"-->
<!-- v1.5.0.0 -->

<%
  Dim lsGUID, lsInsCoDetailsCall, lsCarrierUsersListCall, lsPrevPage, liVehNum
  Dim loXML, lsInsConfigXML, ElemList, ElmLength, oNode, lsAssignmentTypeIDList, laAssignmentTypeID
  Dim lsRdName, lsFldName, lsAlertText, lsInputTag, lsInputRequiredFlag, liTabIdx
  Dim lsAssignReturn, lsVehicleListCall, lsVehicleList, lsClaimDetail
  Dim lo1stParty, li1stPartyCount, loClaimNode, lsInsuredFName, lsInsuredLName, lsInsuredBName
  Dim lsInsuredPhoneArea, lsInsuredPhoneEx, lsInsuredPhoneNum, lsCallerFName, lsCallerLName, lsLossDescription
  Dim lsInsuredRelation, liInsuredRelationID, lsClaimNumber, lsLossDate, lsLossState, liLynxID, lsClaimDetailCall
  Dim lsLossStates
  
  lsPrevPage = Request("fromPage")
  liVehNum = Request("vehNum")

  lsPageName = "frmWaDeskAudit.asp"
  lsPageTitle = lsCurrentAssignmentDesc & " Assignment - Claim Info"
  liPageType = lcAssignmentPage
  
  lsGUID = Request.Cookies("CPSession")("guidArray0")

  Set loXML = Server.CreateObject("MSXML2.DOMDocument")
  loXML.Async = False
  
  'We need Elemlist later in the page
  If liCurrAssignmentReserveLineType = 1 Then

    lsInsConfigXML = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
    loXML.LoadXML GetPersistedData(lsInsConfigXML, lsInsCoConfigGUID, "", "", true)
  
    If Not loXML.ParseError = 0 Then
      Response.Write "<b>Error Code:</b> " & loXML.ParseError & "<br>"
  
    Else
      Set ElemList = loXML.SelectNodes("/Root/CustomScripting")
      ElmLength = ElemList.Length
      
    End If

  End If

  on error goto 0

  liLynxID = Request.Cookies("CPSession")("currentClaim")

  lsVehicleListCall = "<VehicleList InsuranceCompanyID=""" & liInsuranceCompanyID & """ LynxID=""" & liLynxID & """/>"
  lsVehicleList = GetPersistedData(lsVehicleListCall, lsGUID, "", "", false)
  
  loXML.LoadXML lsVehicleList
  
  set lo1stParty = loXML.selectNodes("/Root/Vehicle[@ExposureCD='1']")
  li1stPartyCount = lo1stParty.length
  
    lsClaimDetailCall = "<ClaimDetail InsuranceCompanyID=""" & liInsuranceCompanyID & """ LynxID=""" & liLynxID & """/>"
    lsClaimDetail = GetPersistedData(lsClaimDetailCall, lsGUID, "", "", false)
    
    loXML.LoadXML lsClaimDetail
    
    set loClaimNode = loXML.selectSingleNode("/Root/Claim")
    if not (loClaimNode is nothing) then
      lsLossDate = loClaimNode.getAttribute("LossDate")
      lsLossState = Replace(loClaimNode.getAttribute("LossState"), """", "/""")
      if instr(1, lsLossDate, "T") > 0 then
        lsLossDate = mid(lsLossDate, 6, 2) + "/" + mid(lsLossDate, 9, 2) + "/" + mid(lsLossDate, 1, 4)
      end if
      lsLossDescription = loClaimNode.getAttribute("LossDescription")
    end if
    
    set loClaimNode = loXML.selectSingleNode("/Root/Claim/Insured")
    if not (loClaimNode is nothing) then
      lsInsuredFName = Replace(loClaimNode.getAttribute("NameFirst"), """", "/""")
      lsInsuredLName = Replace(loClaimNode.getAttribute("NameLast"), """", "/""")
      lsInsuredBName = Replace(loClaimNode.getAttribute("BusinessName"), """", "/""")
      lsInsuredPhoneArea = loClaimNode.getAttribute("DayAreaCode")
      lsInsuredPhoneEx = loClaimNode.getAttribute("DayExchangeNumber")
      lsInsuredPhoneNum = loClaimNode.getAttribute("DayUnitNumber")
    end if
    set loClaimNode = loXML.selectSingleNode("/Root/Claim/Caller")
    if not (loClaimNode is nothing) then
      lsCallerFName = Replace(loClaimNode.getAttribute("NameFirst"), """", "/""")
      lsCallerLName = Replace(loClaimNode.getAttribute("NameLast"), """", "/""")
      liInsuredRelationID = loClaimNode.getAttribute("InsuredRelationID")
      set loClaimNode = loXML.selectSingleNode("/Root/Reference[@List='CallerRelationToInsured' and @ReferenceID = " & liInsuredRelationID & "]")
      if not (loClaimNode is nothing) then
        lsInsuredRelation = loClaimNode.getAttribute("Name")
      end if
    end if
    set loClaimNode = loXML.selectSingleNode("/Root/Claim/Coverage")
    if not (loClaimNode is nothing) then
      lsClaimNumber = loClaimNode.getAttribute("ClientClaimNumber")
    end if
    
%>
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<html>
<head>
<title><%=lsCurrentAssignmentDesc%> - Claim Info</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="includes/jquery.autocomplete.css" />

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery-1.3.2.min.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery.selectboxes.min.js" type="text/javascript"></script>

<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "<%= lsPrevPage %>";
  gsVehCount = "";
  gsVehNum = "<%= liVehNum %>";
  lsClaimNumber = "<%=lsClaimNumber%>";
  lsLossDate = "<%=lsLossDate%>";
  lsLossState = "<%=lsLossState%>";
  lsLossDescription = "<%=replace(lsLossDescription, vbcrlf, "\n")%>";
  lsInsuredFName = "<%=lsInsuredFName%>";
  lsInsuredLName = "<%=lsInsuredLName%>";
  lsInsuredBName = "<%=lsInsuredBName%>";
  lsInsuredPhoneArea = "<%=lsInsuredPhoneArea%>";
  lsInsuredPhoneEx = "<%=lsInsuredPhoneEx%>";
  lsInsuredPhoneNum = "<%=lsInsuredPhoneNum%>";
  lsCallerFName = "<%=lsCallerFName%>";
  lsCallerLName = "<%=lsCallerLName%>";
  lsCallerRelation = "<%=lsInsuredRelation%>";
  liLynxID = "<%=liLynxID%>";
  parent.liInsuranceCompanyID = "<%=liInsuranceCompanyID%>";  
  var liDisableAmicaFields = "<%= liDisableAmicaFields %>";
  var liLaborRatesSetupFlag = "<%= liLaborRatesSetupFlag %>";
  var liRequireFirstPartyDeductible = "<%= liRequireFirstPartyDeductible %>";
  var lsCityData = null;
  var lbZipCodeResolved = false;
  var oCitiesCache = null;
  var strCityCache = "";
  var lbDoNext = false;
  var lbCancel = false;
  var strOldCity = "";
  var lbDataValidated = false;

  if (liDisableAmicaFields == 1)
    parent.gbDisableAmicaFields = 1;

  if (liLaborRatesSetupFlag == "1")
   parent.gbLaborRatesSetupFlag = liLaborRatesSetupFlag;
  
            <%
              lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
            
              Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaClaimNumberValidation.xsl", "OfficeID=" & liOfficeID, true)
            %>
  
  function pageInit()
  {
    if(parent)
      parent.resetTimeOut();

    var lsCurrAssignment = "<%=lsCurrentAssignmentDesc%>";
    var lsCurrHelpURL;

    if (lsCurrAssignment == "Adverse Subro Desk Audit")
      lsCurrHelpURL = "CP_Adverse_Subro_Desk_Audit.htm";
    else if (lsCurrAssignment == "Demand Estimate Audit")
      lsCurrHelpURL = "CP_Demand_Estimate_Audit.htm";
    else if (lsCurrAssignment == "IA Estimate Audit")
      lsCurrHelpURL = "CP_IA_Estimate_Audit.htm";

    if (parent)
      parent.gsHelpSubTopicPage = lsCurrHelpURL;
    
    //preselect the caller
    var lsCallerName = lsCallerLName + ", " + lsCallerFName;
    var lsCallerName2 = lsCallerFName + ", " + lsCallerLName; //this is to support the old claims where the lName and FName were swapped.

    var liUserType = <%=liCarrierRep%>;
    var liAllowCarrierRepSelectionFlag = <%=liAllowCarrierRepSelectionFlag%>;

    if (gsPrevPage == "ClaimSummary")
    {
      if (parent)
      {
        setTimeout("parent.getClaimInfoData()",200);
        //parent.getClaimInfoData();
  
        var lotmpCoverageSelID = document.getElementById("txtCoverageProfileUiCD").value;
        var lotmpExposureSelID = document.getElementById("txtExposureCD").value;
        document.getElementById("txtExposureCD").value = lotmpExposureSelID;
        updParty(lotmpExposureSelID);
        document.getElementById("txtCoverageProfileUiCD").value = lotmpCoverageSelID;

          
        var dcFrame = parent.getdcIFrameDoc();
        lsCityData = dcFrame.getElementById("txtRepairLocationCity").value + "|" +
                     dcFrame.getElementById("txtRepairLocationState").value + "|" +
                     dcFrame.getElementById("txtRepairLocationCounty").value;
        $("#txtRepairZipCode").val(dcFrame.getElementById("txtRepairZipCode").value);
      }
    }
    else
    {
      document.getElementById("txtTimeStarted").value = getCurrentDT();
      document.getElementById("txtIntakeStartSeconds").value = getCurrentTimeSec();
      updParty("");
    }
    if (parent) {
      parent.initClaimData(liLynxID, lsClaimNumber, lsLossDate, lsLossState, lsLossDescription, lsInsuredFName, 
                            lsInsuredLName, lsInsuredBName, lsInsuredPhoneArea, lsInsuredPhoneEx, 
                            lsInsuredPhoneNum, lsCallerFName, lsCallerLName, lsCallerRelation)      

    //update the claim information
    document.getElementById("txtCoverageClaimNumber").innerHTML = lsClaimNumber;
    document.getElementById("txtLossDate").innerHTML = lsLossDate;
    document.getElementById("txtLossAddressState").value = lsLossState;
    document.getElementById("txtLossAddressState").disabled = true;
    document.getElementById("txtLossDescription").innerHTML = lsLossDescription;
    document.getElementById("txtCallerName").innerHTML = lsCallerFName + " " + lsCallerLName;
    //document.getElementById("txtCallerNameLast").innerHTML = lsCallerLName;
    document.getElementById("txtCallerRelationToInsuredIDDescription").innerHTML = (lsCallerRelation == "" ? " " : lsCallerRelation) ;
    document.getElementById("txtInsuredName").innerHTML = lsInsuredFName + " " + lsInsuredLName;
    document.getElementById("txtInsuredNameFirst").value = lsInsuredFName;
    document.getElementById("txtInsuredNameLast").value = lsInsuredLName;
    //document.getElementById("txtInsuredPhoneSumm").innerHTML = "(" + lsInsuredPhoneArea + ") " + lsInsuredPhoneEx + "-" + lsInsuredPhoneNum;
    document.getElementById("txtInsuredBusinessName").innerHTML = lsInsuredBName;
    document.getElementById("txtInsuredBusinessName").value = lsInsuredBName;
    }

    //copyOptions("txtLossAddressState", "txtInsuredAddressState");
    $("#txtLossAddressState").copyOptions("#txtInsuredAddressState", "all");
    $("#txtInsuredAddressState").val("");

    $("#txtRepairZipCode").bind("blur", function(e){
      if ($("#txtRepairZipCode").val() != ""){
         lbZipCodeResolved = false;
         $.get("ziplookup.asp?z=" + $("#txtRepairZipCode").val(), function(data){
            if (data != "" && data != "||"){
               zipResolve(data);
            } else {
               if (lbCancel == false)
                  alert("Zip Code could not be resolved. Please type a valid zip code, or enter the City, State, and County.");
               //$("#txtRepairZipCode").val("");
            }
         });
      }
    });
    $("#txtRepairLocationCity").bind("blur", function(e){
      if ($("#txtRepairLocationCity").val() != ""){
         lbZipCodeResolved = false;
         var city = $("#txtRepairLocationCity").val();
         city = jQuery.trim(city).toUpperCase();
         if (city == strOldCity) return;
         $("#txtRepairLocationCity").val(city);
         $.get("csclookup.asp?cty=" + city, function(data){
            oCitiesCache = null;
            if (data != ""){
               var matches = data.split("\n");
               var strAdded = "";
               var strState = "";
               var objSource = document.getElementById("txtLossAddressState");
               var objDestination = document.getElementById("txtRepairLocationState");
               objDestination.innerHTML = "";
               var objCounty = document.getElementById("txtRepairLocationCounty");
               objCounty.innerHTML = "";
               var data2;
               var oOption = document.createElement("OPTION");
               objDestination.options.add(oOption);
               oOption.innerHTML = "-- Select State --";
               oOption.value = "";

               var oOption = document.createElement("OPTION");
               objCounty.options.add(oOption);
               oOption.innerHTML = "-- Select County --";
               oOption.value = "";

               for (var i = 0; i < matches.length; i++){
                  data2 = matches[i].split("|");
                  var strCity = data2[0];
                  var strState = data2[1];
                  if (strCity == city && strAdded.indexOf(strState + "|") == -1){
                     for (j = 0; j < objSource.options.length; j++){
                        if (objSource.options[j].value == strState) {
                           var oOption = document.createElement("OPTION");
                           objDestination.options.add(oOption);
                           oOption.innerHTML = objSource.options[j].innerHTML;
                           oOption.value = objSource.options[j].value;
                           strAdded += strState + "|";
                        }
                     }
                  }
               }
               oCitiesCache = matches;
               $("#txtRepairLocationState").sortOptions();
               
               if (objDestination.options.length == 2) { 
                  objDestination.selectedIndex = 1;
                  $("#txtRepairLocationState").trigger("change");
               } else {
                  if (lsCityData == null) {
                     $("txtRepairLocationState").val("");
                  } else {
                     $("#txtRepairLocationState").val(lsCityData[1]); 
                     $("#txtRepairLocationState").trigger("change");
                  }
               }
               lbZipCodeResolved = true;
               strOldCity = city;
               if (lbDoNext == true){
                  setTimeout("SaveVehInfo()", 100);
               }
            } else {
               if (lbCancel == false){
                  alert("City could not be resolved. Please ener a valid City.");
                  $("#txtRepairLocationCity").focus();
               }
            }
         });
      }
    });
    
    $("#txtRepairLocationState").bind("change", function(e){
      if (oCitiesCache != null){ 
         var data;
         var city = $("#txtRepairLocationCity").val().toUpperCase();
         var state = $("#txtRepairLocationState").val();
         var strAdded = "";
         var objDestination = document.getElementById("txtRepairLocationCounty");
         objDestination.innerHTML = "";
         var oOption = document.createElement("OPTION");
         objDestination.options.add(oOption);
         oOption.innerHTML = "-- Select County --";
         oOption.value = "";
         
         objDestination.disabled = false;

         for (var i = 0; i < oCitiesCache.length; i++){
            data = oCitiesCache[i].split("|");
            if (data.length == 3){
               data[2] = data[2].substr(0, data[2].length - 1);
               if (data[0] == city && data[1] == state && strAdded.indexOf(data[2] + "|") == -1){
                  try {
                  var oOption = document.createElement("OPTION");
                  objDestination.options.add(oOption);
                  oOption.innerHTML = data[2];
                  oOption.value = data[2];
                  strAdded += data[2] + "|";
                  } catch (e) {}
               }
            }
         }
         $("#txtRepairLocationCounty").sortOptions();
         if (objDestination.options.length == 2) { 
            objDestination.selectedIndex = 1;
            objDestination.disabled = true;
         } else {
            if (lsCityData == null)
               $("#txtRepairLocationCounty").val("");
            else
               $("#txtRepairLocationCounty").val(lsCityData[2]);
         }
         if (lbDoNext == true){
            setTimeout("SaveVehInfo()", 100);
         }
      }
    });
    if (gsPrevPage == "ClaimSummary") {
       zipResolve(lsCityData);
       $("#txtRepairLocationCity").trigger("blur");
    }
    $("#txtInsuredAddressZip").bind("blur", function(e){
      if ($("#txtInsuredAddressZip").val() != ""){
         $.get("ziplookup.asp?z=" + $("#txtInsuredAddressZip").val(), function(data){
            if (data != "" && data != "||"){
               data = data.split("|");
               $("#txtInsuredAddressCity").val(data[0]);
               $("#txtInsuredAddressState").val(data[1]);
            } else {
            }
         });
      }
    });
  }
  
   function zipResolve(data){
      if (data != "" && data != "||"){
         lsCityData = data.split("|");
         $("#txtRepairLocationCity").val(lsCityData[0]); 
         $("#txtLossAddressState").copyOptions("#txtRepairLocationState", "all");
         document.getElementById("txtRepairLocationState").options[0].innerHTML = "-- Select State --";
         $("#txtRepairLocationState").val(lsCityData[1]); 
         document.getElementById("txtRepairLocationCounty").innerHTML = "";
         $("#txtRepairLocationCounty").addOption("", "-- Select County --");
         $("#txtRepairLocationCounty").addOption(lsCityData[2], lsCityData[2]);
         $("#txtRepairLocationCounty").val(lsCityData[2]);
         lbZipCodeResolved = true;
         if (lbDoNext == true){
            setTimeout("SaveVehInfo()", 100);
         }
      }
   }
   
   function resetCity(){   
      lsCityData = null;
      lbZipCodeResolved = false;
      $("#txtRepairLocationCity").val(""); 
      document.getElementById("txtRepairLocationState").selectedIndex = -1;
      document.getElementById("txtRepairLocationCounty").selectedIndex = -1;
   }

  function setRadioByValue(radioButton, val)
  {
    if (radioButton == "Liability")
      var radGroup = document.forms[0].Liability;
    if (radioButton == "Emergency")
      var radGroup = document.forms[0].Emergency;
    for (var x = 0; x < radGroup.length; x++)
    {
      if (radGroup[x].value == val)
        radGroup[x].checked = true;
    }
  }
  
  
  function SaveVehInfo()
  {
    lbDoNext = false;
    lbDataValidated = false;

    var zip = jQuery.trim($("#txtRepairZipCode").val());
    var city = jQuery.trim($("#txtRepairLocationCity").val());
    var state = jQuery.trim($("#txtRepairLocationState").val());
    var county = jQuery.trim($("#txtRepairLocationCounty").val());
    
    //alert("|" + zip + "|,| " + city + "|, |" + state + "|");
    
    if (zip == "" &&
        city == "" &&
         state == ""){
       alert("Please enter a Repair Location Zip code or Repair Location City and State.");
       return;
    }
    
    if (zip != "" && lbZipCodeResolved == false){
      //setTimeout("chkClaimNumValidity()", 100);
      //alert("Please wait for the zip code to verified.");
      lbDoNext = true;
      return;
    }
    
    if (city != "" &&
         state == "" &&
         lbZipCodeResolved == false){
         //alert("Please wait for the Repair Location City to be verified.");
         lbDoNext = true;
         return;
         //setTimeout("chkClaimNumValidity()", 100);
    }

    if (city != "" &&
         state == "" &&
         lbZipCodeResolved == true){
         alert("Please select the Repair Location State.");
         lbDoNext = true;
         return;
         //setTimeout("chkClaimNumValidity()", 100);
    }
    
    if (city != "" &&
         county == "" &&
         lbZipCodeResolved == true){
         alert("Please select the Repair Location County.");
         return;
         //setTimeout("chkClaimNumValidity()", 100);
    }
    
    lbReturn = true;
    
    if (lbReturn == true) {
       if (liLaborRatesSetupFlag == "1"){
         getLaborRates();
         lbDataValidated = true;
       } else 
         doNext();
    } else
      return;
  }
  
  function doNext() {    
    parent.tsDeskAudit(gsPrevPage);
  }

  
  function getLaborRates(){
    var city = jQuery.trim($("#txtRepairLocationCity").val());
    var state = jQuery.trim($("#txtRepairLocationState").val());
    var county = jQuery.trim($("#txtRepairLocationCounty").val());
      $.get("laborrates.asp?c=" + city + "&s=" + state + "&cn=" + county, function(data){
         if (data != ""){
            var obj = eval(data);
            if (obj){
               $("#txtBodyLabor").val(obj.BodyRate);
               $("#txtLaborTax").val(obj.LaborTax);
               $("#txtRefinishLabor").val(obj.RefinishRate);
               $("#txtPartsTax").val(obj.PartsTax);
               $("#txtMaterialsLabor").val(obj.MaterialRate);
               $("#txtMaterialsTax").val(obj.MaterialsTax);
               $("#txtFrameLabor").val(obj.FrameRate);
               $("#txtMechanicalLabor").val(obj.MechRate);
               if (lbDataValidated == true) doNext();
            }
         }
      });
  }
  
  function updParty(selIndex)
  {
    if (selIndex == "")
    {
      updateSelect('selCoverage',0);
    }
    else if (selIndex == "1")
    {
      updateSelect('selCoverage',1);
       partySelChange(1);
    }
    else if (selIndex == "3")
    {
      updateSelect('selCoverage',2);
      partySelChange(3);
    }

    var selObj = document.getElementById('txtCoverageProfileUiCD');
    updCovReqFlds(selObj.options[selObj.selectedIndex].value, false);
  }
  
  function updCovReqFlds(selIndex, bUpdCovFlds)
  {
    var oWarnParty = document.getElementById("warnParty");
    if (bUpdCovFlds == true && document.getElementById("txtExposureCD").value == "")
    {
      oWarnParty.style.display = "inline";
      document.getElementById("txtCoverageProfileUiCD").selectedIndex = 0;
      setTimeout("document.getElementById('warnParty').style.display = 'none'", 3000)
      return;
    }
    else
    {
      oWarnParty.style.display = "none";
    }

    var aVal = new Array("", "")
    if (selIndex != "") 
      var aVal = selIndex.split("|");

    document.getElementById("txtCoverageProfileCD").value = aVal[0];
    document.getElementById("txtClientCoverageTypeUiID").value = aVal[1];
    var oSel = document.getElementById("txtCoverageProfileUiCD");
    document.getElementById("txtClientCoverageTypeDesc").value = oSel.options[oSel.selectedIndex].text;
  }

  var laryParty = new Array(1);
  laryParty[0] = new Array("3rd Party","3");
  
  var laryCoverage = new Array(3);
  laryCoverage[0] = new Array("Collision","COLL","Comprehensive","COMP","Liability","LIAB","Underinsured","UIM","Uninsured","UM");
  laryCoverage[1] = new Array("Collision","COLL","Comprehensive","COMP","Underinsured","UIM","Uninsured","UM");
  laryCoverage[2] = new Array("Liability","LIAB");

  function updateSelect(sel, n)
  {
    var loSel;
    var laryTmp;
    if (sel == "selParty")
    {
      loSel = document.getElementById('txtExposureCD');
      laryTmp = laryParty[n];
    }
    else if (sel == "selCoverage")
    {
      loSel = document.getElementById('txtCoverageProfileUiCD');
      laryTmp = laryCoverage[n];
    }
    var liCurr = loSel.options.length;
    for (var j=liCurr; j>0; j--)
      loSel.options[j] = null;
    for (var i=0; i<laryTmp.length; i++)
    {
      loSel.options[loSel.options.length] = new Option(laryTmp[i], laryTmp[i+1]);
      i++;
    }
    if (loSel.id == "txtCoverageProfileUiCD" && loSel.options.length == 2) {//the first item is blank.
      // auto select the only item in the coverage.
      loSel.selectedIndex = 1;
    }
  }
  

  function updDeductible(selIndex)
  {
    if (selIndex == "LIAB")
    {
      document.getElementById('txtDeductibleAmt').value = "";
      document.getElementById('txtDeductibleAmt').disabled = "true";
      document.getElementById('txtDeductibleAmt').style.background = "ButtonHighlight";
    }
    else
    {
      document.getElementById('txtDeductibleAmt').disabled = "";
      document.getElementById('txtDeductibleAmt').style.background = "White";
    }
  }
  
  
  function formatLossDate() 
  {
    if (document.getElementById('txtLossDate').value.length != 0)
    {
      var lsDate = document.getElementById('txtLossDate').value;
      var lsMonth, lsDay, lsYear;
      if (lsDate.indexOf("/") != -1) {
        var laryDate = lsDate.split("/");
        lsMonth = laryDate[0];
        lsDay = laryDate[1];
        lsYear = laryDate[2];
      } else {
        //try to format with regular expression.
        lsYear = lsDate.substr(lsDate.length - 4, 4);
        var lsReminder = lsDate.substr(0, lsDate.length - 4);
        switch (lsReminder.length) {
          case 4:
            lsMonth = lsReminder.substr(0, 2);
            lsDay = lsReminder.substr(2, 2);
            break;
          case 3:
            if (parseInt(lsReminder.substr(0, 2), 10) > 12) {
              lsMonth = lsReminder.substr(0, 1);
              lsDay = lsReminder.substr(1, 2)
            } else {
              lsMonth = lsReminder.substr(0, 2);
              lsDay = lsReminder.substr(2, 1)              
            }
            break;
          case 2:
            lsMonth = lsReminder.substr(0, 1);
            lsDay = lsReminder.substr(1, 1);
            break;
        }
      }
  
      var lbBadDate = false;
      if (lsMonth == undefined || lsMonth < 1 || lsMonth > 12)
        lbBadDate = true;
      if (lsDay == undefined || lsDay < 1 || lsDay > 31)
        lbBadDate = true;
      if (lsYear == undefined || lsYear < 1)
        lbBadDate = true;
      if (lbBadDate == true)
      {
        alert("Please enter the date as MM/DD/YYYY or M/D/YY or MMDDYYYY.");
        document.getElementById('txtLossDate').focus();
        return;
      }
  
      if (lsMonth < 10 && lsMonth.charAt(0) != 0)
        lsMonth = "0"+lsMonth;

      if (lsDay < 10 && lsDay.charAt(0) != 0)
        lsDay = "0"+lsDay;

      var lsMydate = new Date();
      if (lsYear == undefined || lsYear == "" || (lsYear >= 100 && lsYear < 1900))
      {
        lsYear = lsMydate.getFullYear();
      }
      else if (lsYear >= 0 && lsYear < 100)
      {
        lsYear = Number(lsYear) + 2000;
      }
      else if ((lsYear >= 1900 && lsYear < 2000) || (lsYear > 2000))
      {
        lsYear = Number(lsYear);
      }
      else
      {
        lsYear = lsMydate.getFullYear();
      }

      document.getElementById('txtLossDate').value = lsMonth + "/" + lsDay + "/" + lsYear;
    }
  }
  
function partySelChange(party)
{
  if (party == 1)
  {
    if (document.getElementById("txtOwnerNameFirst").value == "")
      document.getElementById("txtOwnerNameFirst").value = document.getElementById("txtInsuredNameFirst").value;
    
    if (document.getElementById("txtOwnerNameLast").value == "")
      document.getElementById("txtOwnerNameLast").value = document.getElementById("txtInsuredNameLast").value;
    
    if (document.getElementById("txtOwnerBusinessName").value == "")
      document.getElementById("txtOwnerBusinessName").value = document.getElementById("txtInsuredBusinessName").value;
  }
  else if (party == 3)
  {
    if ((document.getElementById("txtOwnerNameFirst").value == document.getElementById("txtInsuredNameFirst").value) && (document.getElementById("txtOwnerNameLast").value == document.getElementById("txtInsuredNameLast").value))
    {
      document.getElementById("txtOwnerNameFirst").value = "";
      document.getElementById("txtOwnerNameLast").value = "";
    }
    
    if (document.getElementById("txtOwnerBusinessName").value == document.getElementById("txtInsuredBusinessName").value)
      document.getElementById("txtOwnerBusinessName").value = "";
  }
}

  
  function goCancel()
  {
    if (parent)
    {
      lbCancel = true;
      if (gsPrevPage == "ClaimSummary")
      {
        parent.document.getElementById("uiFrame").src = "frmWaDASummaryAddVeh.asp?fromPage=DeskAudit&vehCount=0,0";
        //frmWaDASummaryAddVeh
      }
      else
      {
        var lbConfirmCancel = window.confirm("Stop entering this assignment and return to My Claims Desktop?");
        if (lbConfirmCancel)
          top.location = "frmMyClaimsDesktop.asp";
      }
    }
  }


  function ClaimNumberFormat(objInput) {
    var bValid = false;
    var obj = document.getElementById("txtCarrierRepOfficeID");
    if (obj) {
      if (obj.value > "0")
        var bValid = FormatClaimNumber(objInput, obj.value, larrOfficeID, larrClaimNumberFormatJS, larrClaimNumberValidJS, larrClaimNumberMsgText, true);
    }
    return bValid;
  }


</script>

</head>

<body onload="resizeWaIframe(0); pageInit(); parent.hideMsg();" tabIndex="-1">

<div id="divContainer">

  <form name="frmClaimInfo">
    <input id="txtTimeStarted" type="hidden">
    <input id="txtIntakeStartSeconds" type="hidden">
    <input id="txtInsuranceCompanyID" type="hidden" value="<%=liInsuranceCompanyID%>" >
    <input id="txtCarrierRepUserID" type="hidden" value="<%=liUserID%>" >
    <input id="txtCarrierRepOfficeID" type="hidden" value="<%=liOfficeID%>" opt="yes">
    <input id="txtCarrierRepNameFirst" type="hidden" value="<%=lsUserNameFirst%>" >
    <input id="txtCarrierRepNameLast" type="hidden" value="<%=lsUserNameLast%>">
    <input id="txtCarrierRepPhoneDay" type="hidden"  value="<%=lsPhoneAreaCode%>-<%=lsPhoneExchangeNumber%>-<%=lsPhoneUnitNumber%>" >
    <input id="txtCarrierRepEmailAddress" type="hidden" value="<%=lsUserEmail%>" >
    <input id="txtCarrierOfficeName" type="hidden" value="<%=lsOfficeName%>" >
    <input id="txtCarrierName" type="hidden" value="<%=lsInsuranceCompanyName%>" >
    <input id="txtAssignmentTypeID" type="hidden" value="<%=liCurrentAssignmentId%>">
    <input id="txtAssignmentDescription" type="hidden" value="<%=lsCurrentAssignmentDesc%>" >
    <input id="txtFNOLUserID" type="hidden" value="<%=liUserID%>" >
    <input id="txtCoverageProfileCD" type="hidden" value="">
    <input id="txtLaborRatesSetupFlag" type="hidden" value="<%=liLaborRatesSetupFlag%>" >
  <!-- input for custom scripting -->
  <%
    If liCurrAssignmentEmergencyLiability = 1 Then
  %>
      <input type="hidden" name="SourceApplicationPassThruDataClm" id="txtSourceApplicationPassThruDataClm" inputTag="SourceApplicationPassThruData" value="">
  <%
    ElseIf liCurrAssignmentReserveLineType = 1 Then
  %>
      <input type="hidden" name="SourceApplicationPassThruData" id="txtSourceApplicationPassThruData" inputTag="SourceApplicationPassThruData" value="">
  <%
    End If
  %>

  
    <table width="600" border="0" cellpadding="10" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse">
      <tr>
        <td nowrap valign="top">

          <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
            <DIV style="position:relative; top:-10px; width:100px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
              &nbsp;&nbsp;Claim Information&nbsp;&nbsp;
            </DIV>
  
            <table name="ClaimInfoTable" id="ClaimInfoTable" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
              <colgroup>
                <col width="205px"/>
                <col width="30px"/>
                <col width="195px"/>
                <col width="30px"/>
                <col width="108px"/>
                <col width="10px"/>
              </colgroup>
              <tr>
                <td nowrap class="legendnopad">Claim Assigned by:</td>
                <td nowrap>&nbsp;</td>
                <td nowrap class="legendnopad">Relation to Insured:</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
              </tr>
              <tr style="height:18px">
                <td nowrap id="txtCallerName" class="inputView"></td>
                <td nowrap>&nbsp;</td>
                <td nowrap id="txtCallerRelationToInsuredIDDescription" class="inputView" ></td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
              </tr>
              <tr>
                <td nowrap class="legendSumm">Claim Number:</td>
                <td nowrap>&nbsp;</td>
                <td nowrap class="legendSumm">Loss Date:</td>
                <td nowrap>&nbsp;</td>
                <td nowrap class="legendSumm">Loss State:</td>
                <td nowrap>&nbsp;</td>
              </tr>
              <tr style="height:18px">
                <td nowrap id="txtCoverageClaimNumber" class="inputView"></td>
                <td nowrap>&nbsp;</td>
                <td nowrap id="txtLossDate"  class="inputView"></td>
                <td nowrap>&nbsp;</td>
                <td nowrap id1="txtLossAddressState" class="inputView">
            <%
              lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
            
              lsLossStates = GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaLossState.xsl", "OfficeID=" & liOfficeID & lcDelimiter & "tabIndexNo=7" & lcDelimiter & "ShowAllStates=1", true)
              
              response.write lsLossStates
            %>
                </td>
                <td nowrap>&nbsp;</td>
              </tr>
              <tr>
                <td nowrap colspan="5" class="legendSumm">Description of Loss:</td>
                <td nowrap>&nbsp;</td>
              </tr>
              <tr>
                <td nowrap colspan="5" id="txtLossDescription" class="inputView"></td>
                <td nowrap>&nbsp;</td>
              </tr>
              <tr>
                <td nowrap class="legendSumm">Insured Name:</td>
                <td nowrap>&nbsp;</td>
                <td nowrap colspan="3" class="legendSumm">Insured Business Name:</td>
                <td nowrap>&nbsp;</td>
              </tr>
              <tr style="height:18px">
                <td nowrap id="txtInsuredName" class="inputView"></td>
                <td nowrap>&nbsp;</td>
                <td nowrap colspan="3" id="txtInsuredBusinessName" class="inputView"></td>
                <td nowrap>&nbsp;</td>
              </tr>
            <%if liDisableInsuredAddress <> "1" and li1stPartyCount = 0 then%>
            <tr>
              <td nowrap colspan="5" class="legend">
                Insured Address: * 
              </td>
            </tr>
            <tr>
              <td nowrap>
                <input name="InsuredAddress1" id="txtInsuredAddress1" type="text" value="" onBlur="ldTrim(this)" size="35" maxlength="50" tabIndex="12">
              </td>
              <td nowrap>&nbsp;</td>
              <td nowrap colspan="3">
                <input name="InsuredAddress2" id="txtInsuredAddress2" type="text" value="" onBlur="ldTrim(this)" size="48" maxlength="50" tabIndex="13">
              </td>
            </tr>
            <tr>
              <td nowrap class="legend">
                Zip: *
              </td>
              <td></td>
              <td nowrap class="legend">
                City: *
              </td>
              <td nowrap>&nbsp;</td>
              <td nowrap class="legend">
                State: *
              </td>
            <tr>
              <td nowrap>
                <input name="InsuredAddressZip" id="txtInsuredAddressZip" type="text" value="" onBlur="ldTrim(this)" size="5" maxlength="5" tabIndex="14">
              </td>
              <td></td>
              <td nowrap>
                <input name="InsuredAddressCity" id="txtInsuredAddressCity" type="text" value="" onBlur="ldTrim(this)" size="21" maxlength="30" tabIndex="15">
              </td>
              <td nowrap>&nbsp;</td>
              <td nowrap>
                <select name="InsuredAddressState" id="txtInsuredAddressState" tabIndex="16">
                </select>
              </td>
            </tr>
            <%end if%>
            </table>

<input name="InsuredNameFirst" id="txtInsuredNameFirst" type="hidden">
<input name="InsuredNameLast" id="txtInsuredNameLast" type="hidden">
<input name="InsuredNameLast" id="txtInsuredBusinessName" type="hidden">

          </DIV>

        </td>
      </tr>
      <tr>
        <td nowrap valign="top">
        
          <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
            <DIV style="position:relative; top:-10px; width:70px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
              &nbsp;&nbsp;Loss information&nbsp;&nbsp;
            </DIV>

            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
              <colgroup>
                <col width="100px"/>
                <col width="225px"/>
                <col width="130px"/>
                <col width="114px"/>
              </colgroup>
              <tr>
                <td nowrap class="legendnopad">Party: <span style="font-size:11pt;">*</span></span></td>
                <td nowrap class="legendnopad" width="250">Coverage: <span style="font-size:11pt;">*</span>
                <span id="warnParty" style="color:#FF0000; display:none; font-size:11px; font-weight:bold;"> [Please select a Party first]</span></td>
                <td nowrap class="legendnopad">&nbsp;&nbsp;&nbsp;Deductible:</td>
                <td nowrap class="legendnopad"></td>
              </tr>
              <tr>
                <td nowrap>
                  <select name="ExposureCD" id="txtExposureCD" onChange="updParty(this.options[this.selectedIndex].value)" required="true" tabIndex="17">
                    <option value=""></option>
                    <%if li1stPartyCount = 0 then%>
                    <option value="1">1st Party</option>
                    <%end if%>
                    <option value="3">3rd Party</option>
                  </select>
                </td>
                <td nowrap>
                  <select name="CoverageProfileUiCD" id="txtCoverageProfileUiCD" onChange="updCovReqFlds(this.options[this.selectedIndex].value, true)" required="true" tabIndex="18">
                    <option value="" selected></option>
            <%
              lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
            
              Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaCoverageTypes.xsl", "", true)
              
            %>

                  </select>
                  <input type="hidden" name="ClientCoverageTypeUiID" id="txtClientCoverageTypeUiID"/>
                  <input type="hidden" name="ClientCoverageTypeDesc" id="txtClientCoverageTypeDesc"/>
                </td>
                <td nowrap>
                  $<input name="DeductibleAmt" id="txtDeductibleAmt" type="text" onBlur="FormatCurrencyObj(this)" onKeyPress="return numbersOnly(this,event,1)" value="" size="13" maxlength="6" tabIndex="19">
                </td>
                <td nowrap>
                  <input name="LimitAmt" id="txtLimitAmt" type="hidden" value="" tabIndex="20">
                </td>
              </tr>


<%
  If liCurrAssignmentReserveLineType = 1 Then
    ' Reset pointer to before first node
    ElemList.reset
    Set oNode = ElemList.nextNode
    liTabIdx = 20

%>
            <tr>
              <td height="10" colspan="4"></td>
            </tr>
            <tr>
              <td colspan="4" nowrap style="background-color:#F8F8F8; border:1px solid #C0C0C0; font-weight:bold; width:100%; padding-left:4px; padding-bottom:4px;">
              <table width="100%" cellspacing="0" cellpadding="0">
              <tr>

<%
    For each oNode in ElemList

      lsAssignmentTypeIDList = oNode.getAttribute("AssignmentTypeIDList")
      laAssignmentTypeID = split(lsAssignmentTypeIDList, "|")

      liStop = ubound(laAssignmentTypeID)
      For liCounter = 0 to liStop
  
        If liCurrentAssignmentId = laAssignmentTypeID(liCounter) _
            and (oNode.getAttribute("InputResponseName") = "LineType" or oNode.getAttribute("InputResponseName") = "Reserve") Then
        
          lsFldName = oNode.getAttribute("InputResponseName")
          lsAlertText = oNode.getAttribute("ScriptText")
          lsInputTag = oNode.getAttribute("InputTag")
          lsInputRequiredFlag = oNode.getAttribute("InputRequiredFlag")

%>


              <td nowrap>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="legendnopad">
                      <%= lsAlertText %>&nbsp;<% If lsInputRequiredFlag = 1 Then %>* <% End If %>
                    </td>
                  </tr>
                  <tr>
                    <td valign="top" nowrap="">
                      <% liTabIdx = liTabIdx+1 %>
                      <input type="input" name="<%= lsFldName %>" id="txt<%= lsFldName %>" inputTag="<%= lsInputTag %>" onKeyPress="return numbersOnly(this,event,1)" tabIndex="<%= liTabIdx %>" value="" size="2" maxlength="2" opt="no">
                    </td>
                  </tr>
                </table>
              </td>


<%
        End If
      
      Next
    Next
%>
              <td width="50%">
              </td>
            </tr>
            </table>
            
            </tr>
<%
  End If
%>

               <tr>
            </table>
  
          </DIV>

        </td>
      </tr>
      <tr>
        <td nowrap valign="top">
        
          <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
            <DIV style="position:relative; top:-10px; width:60px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
              &nbsp;&nbsp;Vehicle Information&nbsp;&nbsp;
            </DIV>

            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
              <colgroup>
                <col width="130px"/>
                <col width="165px"/>
                <col width="270px"/>
              </colgroup>
              <tr>
                <td nowrap class="legendnopad">Owner First Name: <span style="font-size:11pt;">*</span></td>
                <td nowrap class="legendnopad">Owner Last Name: <span style="font-size:11pt;">*</span></td>
                <td nowrap class="legendnopad">Owner Business Name: <span style="font-size:11pt;">*</span>
                  <span style="color:#000000; font-weight:normal; font-size:8pt">(If owner is a business)</span>
                </td>
              </tr>
              <tr>
                <td nowrap>
                  <input name="OwnerNameFirst" id="txtOwnerNameFirst" type="text" onBlur="ldTrim(this)" size="15" maxlength="50" required="true" tabIndex="25">
                </td>
                <td nowrap>
                  <input name="OwnerNameLast" id="txtOwnerNameLast" type="text" onBlur="ldTrim(this);" size="23" maxlength="50" required="true" tabIndex="26">
                </td>
                <td nowrap>
                  <input name="OwnerBusinessName" id="txtOwnerBusinessName" type="text" onBlur="ldTrim(this)" size="40" maxlength="50" tabIndex="27">
                </td>
              </tr>
              <tr>
                <td colspan="3" nowrap>
  
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
                    <colgroup>
                      <col width="80px"/>
                      <col width="96px"/>
                      <col width="96px"/>
                      <col width="4px"/>
                      <col width="146px"/>
                      <col width="160px"/>
                    </colgroup>
                    <tr>
                      <td nowrap class="legend">
                        Year: *<span style="color:#000000; font-weight:normal; font-size:7pt">(YYYY)</span>
                      </td>
                      <td nowrap class="legend">Make: <span style="font-size:11pt;">*</span></td>
                      <td nowrap class="legend">Model: <span style="font-size:11pt;">*</span></td>
                      <td nowrap class="legend"></td>
                      <td nowrap class="legend"><% If liDisableAmicaFields = 0 Then %>VIN:<% End If %></td>
                      <td nowrap class="legend"><% If liDisableAmicaFields = 0 Then %>Odometer:<% End If %></td>
                    </tr>
                    <tr>
                      <td nowrap>
                        <input name="VehicleYear" id="txtVehicleYear" type="text" onKeyPress="return numbersOnly(this,event,0)" value="" size="4" maxlength="4" required="true" tabIndex="28">
                      </td>
                      <td nowrap>
                        <input name="Make" id="txtMake" type="text" value="" onBlur="ldTrim(this)" size="10" maxlength="15" required="true" tabIndex="29">
                      </td>
                      <td nowrap>
                        <input name="Model" id="txtModel" type="text" value="" onBlur="ldTrim(this)" size="10" maxlength="15" required="true" tabIndex="30">
                      </td>
                      <td nowrap>
                        <input name="Drivable" id="txtDrivable" type="hidden" value="1" tabIndex="31">
                      </td>
                      <td nowrap>
                        <input name="VIN" id="txtVIN" type="<% If liDisableAmicaFields = 0 Then %>text<% Else %>hidden<% End If %>" value="" onBlur="ldTrim(this)" size="20" maxlength="17" tabIndex="32">
                      </td>
                      <td nowrap>
                        <input name="Mileage" id="txtMileage" type="<% If liDisableAmicaFields = 0 Then %>text<% Else %>hidden<% End If %>" onKeyPress="return numbersOnly(this,event,0)" value="" size="10" maxlength="7" required="true" tabIndex="33">
                      </td>
                    </tr>
                  </table>
  
                </td>
              </tr>
              <tr>
                <td colspan="3" valign="top" nowrap>
  
                  <table id="tblDamageCb" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
                    <colgroup>
                      <col width="240px"/>
                      <col width="230px"/>
                      <col width="10px"/>
                      <col width="94px"/>
                    </colgroup>
                    <tr>
                      <td nowrap class="legend">Primary Damages:</td>
                      <td nowrap class="legend">Secondary Damages:</td>
                      <td nowrap>&nbsp;</td>
                      <td nowrap>&nbsp;</td>
                    </tr>
                    <tr>
                      <td style="color:#000000; font-weight:normal; font-size:8pt">
                        (Only one primary point is allowed)
                      </td>
                      <td style="color:#000000; font-weight:normal; font-size:8pt">
                        (Multiple secondary points are allowed)
                      </td>
                      <td nowrap>&nbsp;</td>
                      <td nowrap>&nbsp;</td>
                    </tr>
  
            <%
              lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
            
              Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaVehImpactPoints.xsl", "tabIndexNo=34", true)
            %>

                  </table>
  
                </td>
              </tr>
            </table>
            
            <br>
  
            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
              <colgroup>
                <col width="568px"/>
              </colgroup>
              <tr>
                <td nowrap class="legendnopad">
                  Special Intructions/Additional Damage Information:
                  <span id="commFldMsgRemarks" style="color:#000000; font-weight:normal; font-size:7pt">(max.:500 characters)</span>
                </td>
              </tr>
              <tr>
                <td nowrap>
                  <textarea name="Remarks" id="txtRemarks" wrap="physical" style="width:100%; height:52px;" onKeyDown="CheckInputLength(this, 500, 'commFldMsgRemarks')" tabIndex="35"></textarea>
                </td>
              </tr>
            </table>
  
          </DIV>

        </td>
      </tr>
    <tr>
      <td nowrap valign="top">
          
        <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
          <DIV style="position:relative; top:-10px; width:60px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
            &nbsp;&nbsp;Repair Location Information&nbsp;&nbsp;
          </DIV>

          <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
            <colgroup>
               <col width="75px"/>
               <col width="25px"/>
               <col width="150px"/>
               <col width="150px"/>
               <col width="150px"/>
            </colgroup>
            <tr>
               <td class="legendnopad">Zip Code: *</td>
               <td></td>
               <td class="legendnopad">City: *</td>
               <td class="legendnopad">State: *</td>
               <td class="legendnopad">County:</td>
            </tr>
            <tr>
               <td>
                  <input name="rprZipCode" id="txtRepairZipCode" onKeyPress="resetCity();return numbersOnly(this,event,1);" size="6" maxlength="5" tabIndex="36"/>
               </td>
               <td class="legendnopad">-or-</td>
               <td>
                  <input name="RepairLocationCity" id="txtRepairLocationCity" size="20" maxlength="50" tabIndex="37"/>
               </td>
               <td>
                  <select name="RepairLocationState" id="txtRepairLocationState" value="" tabIndex="38">
                     <option value="">-- Select State --</option>
                  </select>
               </td>
               <td>
                  <!-- <input name="RepairLocationCounty" id="txtRepairLocationCounty" size="20" maxlength="50" tabIndex="39"/> -->
                  <select name="RepairLocationCounty" id="txtRepairLocationCounty" value="" tabIndex="39">
                     <option value="">-- Select County --</option>
                  </select>
                  
               </td>
            </tr>
          </table>
        </DIV>
      </td>
      
    </tr>

<% If liLaborRatesSetupFlag = 0 Then %>
    <tr>
<% Else %>
    <tr style="display:none">
<% End If %>
        <td nowrap valign="top">
        
          <DIV style="width:590px; margin-top:10px; border:1px solid #C0C0C0; padding:0px 0px 5px 10px;">
            <DIV style="position:relative; top:-10px; width:60px; font-size:9pt; font-weight:bold; background-color:#E7E7F7; color:#000099; border:1px solid #CCCCCC;">
              &nbsp;&nbsp;Guidelines&nbsp;&nbsp;
            </DIV>
  
            <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
              <colgroup>
                <col width="75px"/>
                <col width="120px"/>
                <col width="35px"/>
                <col width="70px"/>
                <col width="90px"/>
                <col width="30px"/>
                <col width="50px"/>
                <col width="90px"/>
              </colgroup>
              <tr>
                <td nowrap colspan="2" class="legendnopad">Labor Rates</td>
                <td width="40" nowrap>&nbsp;</td>
                <td nowrap colspan="2" class="legendnopad">Tax Info</td>
                <td width="40" nowrap>&nbsp;</td>
                <td nowrap colspan="2" class="legendnopad">Discounts</td>
              </tr>
              <tr>
                <td nowrap class="legendnopad">Body:</td>
                <td nowrap>
                  $
                  <input name="BodyLabor" id="txtBodyLabor" type="text" value="" onBlur="FormatCurrencyObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="10" maxlength="6" tabIndex="60">
                  /hr.
                </td>
                <td width="30" nowrap>&nbsp;</td>
                <td nowrap class="legendnopad">Labor:</td>
                <td nowrap>
                  <input name="LaborTax" id="txtLaborTax" type="text" value="" onBlur="FormatPercentageObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="6" maxlength="7" tabIndex="65">
                  %
                </td>
                <td width="30" nowrap>&nbsp;</td>
                <td nowrap class="legendnopad">Parts:</td>
                <td nowrap>
                  <input name="PartsDiscount" id="txtPartsDiscount" type="text" value="" onBlur="FormatPercentageObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="6" maxlength="7" tabIndex="68">
                  %
                </td>
              </tr>
              <tr>
                <td nowrap class="legendnopad">Refinish:</td>
                <td nowrap>
                  $
                  <input name="RefinishLabor" id="txtRefinishLabor" type="text" value="" onBlur="FormatCurrencyObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="10" maxlength="6" tabIndex="61">
                  /hr.
                </td>
                <td width="30" nowrap>&nbsp;</td>
                <td nowrap class="legendnopad">Parts:</td>
                <td nowrap>
                  <input name="PartsTax" id="txtPartsTax" type="text" value="" onBlur="FormatPercentageObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="6" maxlength="7" tabIndex="66">
                  %
                </td>
                <td width="30" nowrap>&nbsp;</td>
                <td nowrap class="legendnopad">Glass:&nbsp;</td>
                <td nowrap>
                  <input name="GlassDiscount" id="txtGlassDiscount" type="text" value="" onBlur="FormatPercentageObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="6" maxlength="7" tabIndex="69">
                  %
                </td>
              </tr>
              <tr>
                <td nowrap class="legendnopad">Materials:</td>
                <td nowrap>
                  $
                  <input name="MaterialsLabor" id="txtMaterialsLabor" type="text" value="" onBlur="FormatCurrencyObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="10" maxlength="6" tabIndex="62">
                  /hr.
                </td>
                <td width="30" nowrap>&nbsp;</td>
                <td nowrap class="legendnopad">Materials:&nbsp;</td>
                <td nowrap>
                  <input name="MaterialsTax" id="txtMaterialsTax" type="text" value="" onBlur="FormatPercentageObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="6" maxlength="7" tabIndex="67">
                  %
                </td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
              </tr>
              <tr>
                <td nowrap class="legendnopad">Frame:</td>
                <td nowrap>
                  $
                  <input name="FrameLabor" id="txtFrameLabor" type="text" value="" onBlur="FormatCurrencyObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="10" maxlength="6" tabIndex="63">
                  /hr.
                </td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
              </tr>
              <tr>
                <td nowrap class="legendnopad">Mechanical:&nbsp;</td>
                <td nowrap>
                  $
                  <input name="MechanicalLabor" id="txtMechanicalLabor" type="text" value="" onBlur="FormatCurrencyObj(this)" onKeyPress="return numbersOnly(this,event,1)" size="10" maxlength="6" tabIndex="64">
                  /hr.
                </td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
                <td nowrap>&nbsp;</td>
              </tr>
            </table>
  
          </DIV>

        </td>
      </tr>
    </table>

    <p class="bodyBlue"> All fields marked with an asterisk (*) are required.</p>

    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; table-layout:fixed;">
      <colgroup>
        <col width="400px"/>
        <col width="100px"/>
        <col width="90px"/>
      </colgroup>
      <tr>
        <td>&nbsp;</td>
        <td id="cancelImgSrc" valign="top" nowrap>
          <a href='javascript:goCancel()' tabIndex='80'
             title="<% If lsPrevPage = "ClaimSummary" Then %> Cancel and Return to Summary Page <% Else %> Return to My Claims Desktop <% End If %>"
             onMouseOver="window.status='<% If lsPrevPage = "ClaimSummary" Then %> Cancel and Return to Summary Page <% Else %> Return to My Claims Desktop <% End If %>'; return true"
             onMouseOut="window.status=''; return true">
             <% If lsPrevPage = "ClaimSummary" Then %>
              <img src='images/btn_return.gif' border='0' WIDTH='83' HEIGHT='31'>
             <% Else %>
              <img src='images/btn_cancel.gif' border='0' WIDTH='83' HEIGHT='31'>
             <% End If %>
          </a>
        </td>
        <td id="nextImgSrc" valign="top" nowrap>
          <a href="javascript:SaveVehInfo()" tabIndex='81'
          <% If lsPrevPage = "ClaimSummary" Then %>
             title="Save and Return to Summary Page"
             onMouseOver="window.status='Save and Return to Summary Page'; return true"
          <% Else %>
             title="To Summary Page"
             onMouseOver="window.status='To Summary Page'; return true"
          <% End If %>
             onMouseOut="window.status=''; return true">

          <% If lsPrevPage = "ClaimSummary" Then %>
            <img src="images/btn_ok.gif" border="0" WIDTH="52" HEIGHT="31">
          <% Else %>
            <img src="images/btn_next.gif" border="0" WIDTH="83" HEIGHT="31">
          <% End If %>
          </a>
        </td>
      </tr>
    </table>

  </form>

</div>

</body>
</html>

<%
  Set oNode = Nothing
  Set ElemList = Nothing
  Set loXML = Nothing
%>

