<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!-- v1.4.5.0 -->

<%
  Dim liInsuranceCompanyID, liInsuranceCompanyName, lsInsCoLynxContactPhone, liDocumentUploadFlag, lsInsCoConfigCall, docXML
  Dim lsStrXMLpMsg, liShowPromiseToPay, liReserveLineTypeScript, ElemList, oNode, liSB511Script
  Dim liEmergencyLiabilityScript, lsDisableAmicaFields, liDisableAmicaFields, liLaborRatesSetupFlag
  Dim lbRequireFirstPartyDeductible, lsRequireFirstPartyDeductible, lsDisableInsuredAddress, liDisableInsuredAddress
  Dim lsDisablePolicyNumber, liDisablePolicyNumber
 
  liInsuranceCompanyID = Request("hidInsuranceCompanyID")
  liInsuranceCompanyName = Request("hidInsuranceCompanyName")

  If Request("fromPage") = "Login" and liInsuranceCompanyID = "" Then
    Response.Redirect "frmLogout.asp?NL=1"

  ElseIf Request("fromPage") = "InsuranceCoSelect" Then
    'Generate new GUID since we just changed to another carrier 
  	Dim loSession, laGUIDs
  	Set loSession = Server.CreateObject("APD.clsAppData")
  	laGUIDs = loSession.GenerateXGuids(1)
		Response.Cookies("CPSession")("guidArray5") = laGUIDs(0)
  	Set loSession = Nothing

  End If
  
  lsInsCoConfigGUID = Request.Cookies("CPSession")("guidArray5")
  lsInsCoConfigCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """ />"
  liShowPromiseToPay = 0
  liReserveLineTypeScript = 0
  liEmergencyLiabilityScript = 0
  liSB511Script = 0
  liDisableAmicaFields = 0
  

  Set docXML = Server.CreateObject("MSXML2.DOMDocument")
  docXML.Async = False

  'Disable Desk Audit fields
  'lsStrXMLpMsg = "<Config Path='ClaimPoint/DisableDeskAuditFields/InsuranceCompany[@ID=" & liInsuranceCompanyID & "]' />"
  lsStrXMLpMsg = "<Config Path='ClaimPoint/Customizations/InsuranceCompany[@ID=" & liInsuranceCompanyID & "]/@DisableDeskAuditFields' />"
  docXML.LoadXML GetData(lsStrXMLpMsg, "", "")
  lsDisableAmicaFields = lcase(docXML.documentElement.text)
  If lsDisableAmicaFields = "true" Then
    liDisableAmicaFields = 1
  End If
  
  'Disable Desk Audit fields
'  lbRequireFirstPartyDeductible = 0
'  lsStrXMLpMsg = "<Config Path='ClaimPoint/DisableDeskAuditFields/InsuranceCompany[@ID=" & liInsuranceCompanyID & "]/@Require1stPartyDeductible' />"
'  docXML.LoadXML GetData(lsStrXMLpMsg, "", "")
'  lsRequireFirstPartyDeductible = lcase(docXML.documentElement.text)
'  If lsRequireFirstPartyDeductible = "true" Then
'    lbRequireFirstPartyDeductible = 1
'  End If

  'Disable Insured fields
  'lsStrXMLpMsg = "<Config Path='ClaimPoint/DisableDeskAuditFields/InsuranceCompany[@ID=" & liInsuranceCompanyID & "]' />"
  lsStrXMLpMsg = "<Config Path='ClaimPoint/Customizations/InsuranceCompany[@ID=" & liInsuranceCompanyID & "]/@DisableInsuredAddress' />"
  docXML.LoadXML GetData(lsStrXMLpMsg, "", "")
  lsDisableInsuredAddress = lcase(docXML.documentElement.text)
  If lsDisableInsuredAddress = "true" Then
    liDisableInsuredAddress = 1
  else 
    liDisableInsuredAddress = 0
  End If
  
  'Disable Policy Number fields
  'lsStrXMLpMsg = "<Config Path='ClaimPoint/DisableDeskAuditFields/InsuranceCompany[@ID=" & liInsuranceCompanyID & "]' />"
  lsStrXMLpMsg = "<Config Path='ClaimPoint/Customizations/InsuranceCompany[@ID=" & liInsuranceCompanyID & "]/@DisablePolicyNumber' />"
  docXML.LoadXML GetData(lsStrXMLpMsg, "", "")
  lsDisablePolicyNumber = lcase(docXML.documentElement.text)
  If lsDisablePolicyNumber = "true" Then
    liDisablePolicyNumber = 1
  else 
    liDisablePolicyNumber = 0
  End If

  docXML.LoadXML GetPersistedData(lsInsCoConfigCall, lsInsCoConfigGUID, "", "", false)
  
  If Not docXML.ParseError = 0 Then
    Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
  Else
    lsInsCoLynxContactPhone = docXML.documentElement.getAttribute("LynxContactPhone")
    liDocumentUploadFlag = docXML.documentElement.getAttribute("ClaimPointDocumentUploadFlag")
    liLaborRatesSetupFlag = docXML.documentElement.getAttribute("LaborRatesSetupFlag")

    Set ElemList = docXML.SelectNodes("/Root/CustomScripting")
  
    If ElemList.length > 0 Then
  
      For each oNode in ElemList
    
        'Line Type and Reserve Statement
        If LCASE(oNode.getAttribute("InputResponseName")) = "linetype" _ 
           or LCASE(oNode.getAttribute("InputResponseName")) = "reserve" Then
          liReserveLineTypeScript = 1
        End If

        '19Jan2012 - TVD - AuthType for Elephant custom scripting
        If LCASE(oNode.getAttribute("InputResponseName")) = "AuthType" Then
          liAuthTypeScript = 1
        End If

        'Liability and Emergency Statement
        If LCASE(oNode.getAttribute("InputResponseName")) = "liability" _ 
           or LCASE(oNode.getAttribute("InputResponseName")) = "emergency" Then
          liEmergencyLiabilityScript = 1
        End If

        'Promise To Pay Statement
        If LCASE(oNode.getAttribute("InputResponseName")) = "promisetopay" Then
          liShowPromiseToPay = 1
        End If

        'SB511 Statement for InsCo ID = 99
        If liInsuranceCompanyID = 99 and oNode.getAttribute("ConditionValue") = "CA" Then
            liSB511Script = 1
        End If

      Next
    End If

    Set ElemList = Nothing

  End If
  
  Set docXML = Nothing

  Response.Cookies("CPSession")("InsuranceCompanyID") = liInsuranceCompanyID
  Response.Cookies("CPSession")("InsuranceCompanyName") = liInsuranceCompanyName
  Response.Cookies("CPSession")("InsCoLynxContactPhone") = lsInsCoLynxContactPhone
  Response.Cookies("CPSession")("DocumentUploadFlag") = liDocumentUploadFlag
  Response.Cookies("CPSession")("ShowPromiseToPay") = liShowPromiseToPay
  Response.Cookies("CPSession")("ReserveLineTypeScript") = liReserveLineTypeScript
  Response.Cookies("CPSession")("EmergencyLiabilityScript") = liEmergencyLiabilityScript
  Response.Cookies("CPSession")("SB511Script") = liSB511Script
  Response.Cookies("CPSession")("DisableAmicaFields") = liDisableAmicaFields
  Response.Cookies("CPSession")("LaborRatesSetupFlag") = liLaborRatesSetupFlag
'  Response.Cookies("CPSession")("RequireFirstPartyDeductible") = lbRequireFirstPartyDeductible
  Response.Cookies("CPSession")("DisableInsuredAddress") = liDisableInsuredAddress
  Response.Cookies("CPSession")("DisablePolicyNumber") = liDisablePolicyNumber

  
  Response.Redirect "frmMyClaimsDesktop.asp"
%>
