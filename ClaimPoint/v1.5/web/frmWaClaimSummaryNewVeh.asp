<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetCurrAssignment.asp"-->
<!-- v1.4.5.0 -->

<%

Dim  lsShopAssignmentType, IsAssignmentDesc

lsShopAssignmentType= Request("ShopAssignmentType")

  'Response.Write lsShopAssignmentType
  If Not lsShopAssignmentType Is Nothing Then
   If lsShopAssignmentType = "ChoiceShop" Then
     lsCurrentAssignmentDesc = "Choice Shop"
   End If

   If lsShopAssignmentType = "ProgramShop" Then
     lsCurrentAssignmentDesc = "Program Shop"
   End If
   
   IsAssignmentDesc = lsCurrentAssignmentDesc

  End If
 

  'lsCurrentAssignmentDesc="Program Shop"

  'lsCurrentAssignmentDesc="Program Shop"
  lsPageName = "frmWaClaimSummaryNewVeh.asp"
  lsPageTitle = lsCurrentAssignmentDesc & " Assignment - Summary"
  liPageType = lcAssignmentPage
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<% Dim IsEmailCell
        IsEmailCell = "<Config Path=""ClaimPoint/Customizations/InsuranceCompany[@ID='" & liInsuranceCompanyID & "']/@RepairReferral""/>"

        IsEmailCell = GetData(IsEmailCell, "", "")
        docXML.Async = False
        docXML.loadXML IsEmailCell
        IsEmailCell = docXML.documentElement.text
        IsEmailCell = Lcase(IsEmailCell) %>

<html>
<head>
<title><%=lsCurrentAssignmentDesc%> - Summary</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "";
  gsVehNum = "";
  gsVehCount = "<%=Request("vehCount")%>";
  giReserveLineTypeScript = <%= liCurrAssignmentReserveLineType %>;
  var lsCurrAssignment = "<%=lsCurrentAssignmentDesc%>";

  function pageInit()
  {
     if(parent)
      parent.resetTimeOut();
  
    var lsCurrHelpURL;
   
    if (lsCurrAssignment == "Adverse Subro Desk Review")
      lsCurrHelpURL = "CP_Adverse_Subro_Desk_Review.htm";
    else if (lsCurrAssignment == "Demand Estimate Review")
      lsCurrHelpURL = "CP_Demand_Estimate_Review.htm";
    else if (lsCurrAssignment == "IA Estimate Review")
      lsCurrHelpURL = "CP_IA_Estimate_Review.htm";
    else if (lsCurrAssignment == "Program Shop")
      lsCurrHelpURL = "CP_Program_Shop_Assignment.htm";
    else if (lsCurrAssignment == "Choice Shop")
      lsCurrHelpURL = "CP_Program_Shop_Assignment.htm";

    if (parent)
      parent.gsHelpSubTopicPage = lsCurrHelpURL;

    addVehicleDiv(gsVehCount);

     if(parent)
      parent.getClaimSummaryData(gsVehCount);

    //update the claim information
    document.getElementById("txtCoverageClaimNumber").innerHTML = parent.lsClaimNumber;
    document.getElementById("txtLossDate").innerHTML = parent.lsLossDate;
    document.getElementById("txtLossAddressState").innerHTML = parent.lsLossState;
    document.getElementById("txtLossDescription").value = parent.lsLossDescription;
    document.getElementById("txtCallerName").innerHTML = parent.lsCallerFName + " " + parent.lsCallerLName;
    //document.getElementById("txtCallerNameLast").innerHTML = lsCallerLName;
    document.getElementById("txtCallerRelationToInsuredIDDescription").innerHTML = (parent.lsCallerRelation == "" ? " " : parent.lsCallerRelation) ;
    document.getElementById("txtInsuredName").innerHTML = parent.lsInsuredFName + " " + parent.lsInsuredLName;
    //document.getElementById("txtInsuredNameLast").innerHTML = lsInsuredLName;
    //document.getElementById("txtInsuredPhoneSumm").innerHTML = "(" + parent.lsInsuredPhoneArea + ") " + parent.lsInsuredPhoneEx + "-" + parent.lsInsuredPhoneNum;
    document.getElementById("txtInsuredBusinessName").innerHTML = (parent.lsInsuredBName == "" ? "&nbsp;" : parent.lsInsuredBName);

    var IsDRPChoice =  "<%= lsShopAssignmentType %>"; 
        //alert(IsDRPChoice);
      if(IsDRPChoice != ""){
         document.getElementById("AddNewVehDiv").style.display = "none";
          }
      else{
         document.getElementById("AddNewVehDiv").style.display = "block";
         }
  }
  
  function addVehicleDiv(gsVehCount)
  {

      if(parent)
    {
      var liVehNum;
      var liDispOrder = 0;
      var liCurrRow;
      var liVehSummDispType;
      var loNewDiv;
      var sHtml;
      var laryVehCount = gsVehCount.split(",");
      var liAryLength = laryVehCount.length;
      var loVehInsDiv = document.getElementById("vehTblHolder");
      var IsRepairEmailCell = "<%= IsEmailCell %>"
      IsRepairEmailCell = IsRepairEmailCell.replace("<config>", "");
      IsRepairEmailCell = IsRepairEmailCell.replace("</config>", "");
      IsRepairEmailCell = IsRepairEmailCell.toLowerCase();
      
   
      loVehInsDiv.innerHTML = "";  
      for (x=2; x<liAryLength; x++)
      {
        if (laryVehCount[x] != "")
        {
          liVehNum = laryVehCount[x];
          liDispOrder++;
          liCurrRow = x-2;
          loNewDiv = document.createElement("DIV");
          loVehInsDiv.appendChild(loNewDiv);
          liVehSummDispType = parent.getVehSummType(liCurrRow);
          
          if (liVehSummDispType == 1) //"+liDispOrder+"
          {
            sHtml = "<br><br><table width=600 border=10 cellpadding=0 cellspacing=0 style='border:1px solid #000099; border-collapse:collapse'><tr><td><table width=100% border=0 cellpadding=0 cellspacing=0 style='border-collapse:collapse; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>&nbsp;New Vehicle Information</td><td align=right><a href=javascript:deleteVehicle("+liVehNum+") title='Delete this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_delete_blue.gif WIDTH=83 HEIGHT=31></a><img border=0 src=images/spacer.gif WIDTH=10 HEIGHT=1><a href=javascript:editVehicle("+liVehNum+") title='Edit this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_edit_blue.gif WIDTH=83 HEIGHT=31></a></td></tr></table></td></tr><tr><td valign=top style='padding: 0px 10px 10px 10px;'><table name=VehicleInfoTable id=VehicleInfoTable_"+liVehNum+" border=0 cellpadding=0 cellspacing=0><tr><td width=152 nowrap class=legendSumm>Party:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Coverage:</td><td width=10 nowrap></td><td width=160 nowrap class=legendSumm>Deductible:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Limit:</td></tr><tr><td nowrap><input type=text class=inputView id=txtExposureCD_"+liVehNum+" size=9 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtClientCoverageTypeDesc_"+liVehNum+" size=17 readonly=true></td><td nowrap></td><td nowrap>$<input type=text class=inputView id=txtDeductibleAmt_"+liVehNum+" size=16 readonly=true></td><td nowrap></td><td nowrap>$<input type=text class=inputView id=txtLimitAmt_"+liVehNum+" size=16 readonly=true></td></tr>";
            
            if (giReserveLineTypeScript == 1)
              sHtml += "<TR><TD nowrap class=legendSumm>	Line Type:</TD><TD nowrap></TD><TD nowrap class=legendSumm>	Reserve Number:</TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR><TR><TD nowrap><INPUT type=text class=inputView id=txtLineType_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap><INPUT type=text class=inputView id=txtReserve_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR>";

        //glsd451 Changes - Adding new controls as per andrew's requirement 
           if((lsCurrAssignment == "Program Shop" || lsCurrAssignment == "Repair Referral" || lsCurrAssignment == "Choice Shop") && IsRepairEmailCell == "true")
           {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr>    <tr><td nowrap class=legendSumm colspan=3>Email: </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap class=legendSumm>Cell phone carrier:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactEmailAddress_"+liVehNum+" readonly=true> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtCellPhoneCarrier_"+liVehNum+" readonly=true> </td></tr><tr><td nowrap class=legendSumm colspan=3>Preferred method of status updates:</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtPrefMethodUpd_"+liVehNum+"  readonly=true></td><td nowrap> </td><td nowrap></td><td nowrap> </td><td nowrap> </td></tr>"           
            }
            else
            {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr>"
            }
            sHtml += "<tr id='trRental1'><td width=152 nowrap class=legendSumm>Apply Rental:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Days:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Rental Daily Maximum:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Rental Maximum:</td></tr>"
            sHtml += "<tr id='trRental2'><td nowrap><input type=text class=inputView id=txtRentalAuthorized_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalDays_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalDayAmount_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalMaxAmount_"+liVehNum+" size=16 readonly=true></td></tr>"
            sHtml += "<tr id='trRental3'><td nowrap class=legendSumm>Rental Days Auth.:</td><td nowrap> </td><td nowrap colspan=3 class=legendSumm>Rental Instructions:</td><td nowrap> </td><td nowrap> </td></tr>"
            sHtml += "<tr id='trRental4'><td nowrap valign=top><input type=text class=inputView id=txtRentalDaysAuthorized_"+liVehNum+" style='text-align:right' size=3 readonly=true></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea wrap='physical' class=inputView id=txtRentalInstructions_"+liVehNum+" readonly=true style='width:100%; height:50px;'></textarea></td></tr>"

               //Additional Rental Info
            sHtml += "<tr><td colspan='7'><table id=tblAdditionalRental_"+liVehNum+" style='border:1px solid #000099; margin-top:10px;'><tr><td colspan='7'><table cellpadding=5 cellspacing=0 style='border-collapse:collapse; width:100%; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>Additional Rental Info : </td></tr></table></td></tr><td style='padding:0px 10px 10px 10px;'><table>"
            sHtml += "<tr id='trRental5'><td width=150 nowrap class=legendSumm>Rental Vendor:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Res/Conf:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Vehicle Class:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rate Type:</td></tr>"
            sHtml += "<tr id='trRental6'><td nowrap><input type=text class=inputView id=hidRentalVendor_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalResConf_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalVehicleClass_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalRateType_"+liVehNum+" size=15 readonly=true></td></tr>"

            sHtml += "<tr id='trRental7'><td width=150 nowrap class=legendSumm>Rate:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm> Auth Pickup: </td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Tax Rate:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Phone:</td></tr>"
            sHtml += "<tr id='trRental8'><td nowrap>$<input type=text class=inputView id=txtRentalRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalAuthPickup_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalTaxRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalPhoneAC_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneEN_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneUN_"+liVehNum+" size=7 readonly=true></td></tr>"

            sHtml += "<tr id='trRental9'><td width=150 nowrap class=legendSumm>Rental Address: </td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Rental City:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental State</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Zip:</td></tr>"
            sHtml += "<tr id='trRental10'><td nowrap><input type=text class=inputView id=txtRentalAddress_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalCity_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalState_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalZip_"+liVehNum+" size=15 readonly=true></td></tr>"
            sHtml +="</table></td></tr></table></td></tr>"

            sHtml += "<tr><td colspan=2 nowrap class=legendSumm>Vehicle VIN:</td><td colspan=4 nowrap class=legendSumm>Vehicle YMM:</td><td> </td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtVIN_"+liVehNum+" size=24 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtVehicleYear_"+liVehNum+" size=5 readonly=true> <input type=text class=inputView id=txtMake_"+liVehNum+" size=15 readonly=true> <input type=text class=inputView id=txtModel_"+liVehNum+" size=15 readonly=true></td><td nowrap></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle License Plate:</td><td colspan=1 nowrap class=legendSumm>Vehicle Drivable:</td><td nowrap> </td><td colspan=3 nowrap class=legendSumm>Odometer:</td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtLicensePlateNumber_"+liVehNum+" size=24 readonly=true></td><td nowrap colspan=2><input type=text class=inputView id=txtDrivable_"+liVehNum+" size=5 readonly=true></td><td nowrap colspan=3><input type=text class=inputView colspan=4 id=txtMileage_"+liVehNum+" size=8 readonly=true></td></tr><tr><td nowrap class=legendSumm>Primary Damage:</td><td nowrap> </td><td colspan=5 nowrap class=legendSumm>Secondary Damages:</td></tr><tr><td nowrap valign=top><textarea wrap='physical' class=inputView id=txtPrimaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtSecondaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr></table></tr></table>"
          }
            //sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap></td><td nowrap></td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true><input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap></td><td nowrap></td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap></td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap></td><td nowrap></td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap></td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr> <tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true><input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap></td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr>   <tr><td nowrap class=legendSumm colspan=3>Email: </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap class=legendSumm>Cell phone carrier:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactEmailAddress_"+liVehNum+" readonly=true> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtCellPhoneCarrier_"+liVehNum+" readonly=true> </td></tr><tr><td nowrap class=legendSumm colspan=3>Preferred method of status updates:</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtPrefMethodUpd_"+liVehNum+"  readonly=true></td><td nowrap> </td><td nowrap></td><td nowrap> </td><td nowrap> </td></tr> <tr><td width=152 nowrap class=legendSumm>Apply Rental:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Days:</td><td width=10 nowrap></td><td width=160 nowrap class=legendSumm>Rental Daily Maximum:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Maximum:</td></tr><tr><td nowrap><input type=text class=inputView id=txtRentalAuthorized_"+liVehNum+" size=3 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtRentalDays_"+liVehNum+" size=3 readonly=true></td><td nowrap></td><td nowrap>$<input type=text class=inputView id=txtRentalDayAmount_"+liVehNum+" size=16 readonly=true></td><td nowrap></td><td nowrap>$<input type=text class=inputView id=txtRentalMaxAmount_"+liVehNum+" size=16 readonly=true></td></tr><tr><td nowrap class=legendSumm>Rental Days Auth.:</td><td nowrap></td><td nowrap colspan=3 class=legendSumm>Rental Instructions:</td><td nowrap></td><td nowrap></td></tr><tr><td nowrap valign=top><input type=text class=inputView id=txtRentalDaysAuthorized_"+liVehNum+" style='text-align:right' size=3 readonly=true></td><td nowrap></td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtRentalInstructions_"+liVehNum+" readonly=true style='width:100%; height:50px;'></textarea></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle VIN:</td><td colspan=4 nowrap class=legendSumm>Vehicle YMM:</td><td></td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtVIN_"+liVehNum+" size=24 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtVehicleYear_"+liVehNum+" size=5 readonly=true><input type=text class=inputView id=txtMake_"+liVehNum+" size=15 readonly=true><input type=text class=inputView id=txtModel_"+liVehNum+" size=15 readonly=true></td><td nowrap></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle License Plate:</td><td colspan=1 nowrap class=legendSumm>Vehicle Drivable:</td><td colspan=3 nowrap class=legendSumm>Odometer:</td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtLicensePlateNumber_"+liVehNum+" size=24 readonly=true></td><td nowrap><input type=text class=inputView id=txtDrivable_"+liVehNum+" size=5 readonly=true></td><td nowrap><input type=text class=inputView colspan=4 id=txtMileage_"+liVehNum+" size=8 readonly=true></td></tr><tr><td nowrap class=legendSumm>Primary Damage:</td><td nowrap></td><td colspan=5 nowrap class=legendSumm>Secondary Damages:</td></tr><tr><td nowrap valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtPrimaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td><td nowrap></td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtSecondaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr></table></td></tr></table>";
          
          else if (liVehSummDispType == 2)
          {
            sHtml = "<br><br><table width=600 border=10 cellpadding=0 cellspacing=0 style='border:1px solid #000099; border-collapse:collapse'><tr><td><table width=100% border=0 cellpadding=0 cellspacing=0 style='border-collapse:collapse; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>&nbsp;New Vehicle Information</td><td align=right><a href=javascript:deleteVehicle("+liVehNum+") title='Delete this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_delete_blue.gif WIDTH=83 HEIGHT=31></a><img border=0 src=images/spacer.gif WIDTH=10 HEIGHT=1><a href=javascript:editVehicle("+liVehNum+") title='Edit this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_edit_blue.gif WIDTH=83 HEIGHT=31></a></td></tr></table></td></tr><tr><td valign=top style='padding: 0px 10px 10px 10px;'><table name=VehicleInfoTable id=VehicleInfoTable_"+liVehNum+" border=0 cellpadding=0 cellspacing=0><tr><td width=152 nowrap class=legendSumm>Party:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Coverage:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Deductible:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Limit:</td></tr><tr><td nowrap><input type=text class=inputView id=txtExposureCD_"+liVehNum+" size=9 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtClientCoverageTypeDesc_"+liVehNum+" size=17 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtDeductibleAmt_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtLimitAmt_"+liVehNum+" size=16 readonly=true></td></tr>";

            if (giReserveLineTypeScript == 1)
              sHtml += "<TR><TD nowrap class=legendSumm>	Line Type:</TD><TD nowrap></TD><TD nowrap class=legendSumm>	Reserve Number:</TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR><TR><TD nowrap><INPUT type=text class=inputView id=txtLineType_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap><INPUT type=text class=inputView id=txtReserve_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR>";


              //glsd451 Changes - Adding new controls as per andrew's requirement 

            if((lsCurrAssignment == "Program Shop" || lsCurrAssignment == "Repair Referral" || lsCurrAssignment == "Choice Shop") && IsRepairEmailCell == "true")
           {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap class=legendSumm colspan=3>Email: </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap class=legendSumm>Cell phone carrier:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactEmailAddress_"+liVehNum+" readonly=true> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtCellPhoneCarrier_"+liVehNum+" readonly=true> </td></tr><tr><td nowrap class=legendSumm colspan=3>Preferred method of status updates:</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtPrefMethodUpd_"+liVehNum+"  readonly=true></td><td nowrap> </td><td nowrap></td><td nowrap> </td><td nowrap> </td></tr>"           
            }
            else
            {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr>"
            }
            sHtml += "<tr id='trRental1'><td width=152 nowrap class=legendSumm>Apply Rental:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Days:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Rental Daily Maximum:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Rental Maximum:</td></tr>"
            sHtml += "<tr id='trRental2'><td nowrap><input type=text class=inputView id=txtRentalAuthorized_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalDays_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalDayAmount_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalMaxAmount_"+liVehNum+" size=16 readonly=true></td></tr>"
            sHtml += "<tr id='trRental3'><td nowrap class=legendSumm>Rental Days Auth.:</td><td nowrap> </td><td nowrap colspan=3 class=legendSumm>Rental Instructions:</td><td nowrap> </td><td nowrap> </td></tr>"
            sHtml += "<tr id='trRental4'><td nowrap valign=top><input type=text class=inputView id=txtRentalDaysAuthorized_"+liVehNum+" style='text-align:right' size=3 readonly=true></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtRentalInstructions_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr>"

               //Additional Rental Info
            sHtml += "<tr><td colspan='7'><table id=tblAdditionalRental_"+liVehNum+" style='border:1px solid #000099; margin-top:10px;'><tr><td colspan='7'><table cellpadding=5 cellspacing=0 style='border-collapse:collapse; width:100%; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>Additional Rental Info : </td></tr></table></td></tr><td style='padding:0px 10px 10px 10px;'><table>"
            sHtml += "<tr id='trRental5'><td width=150 nowrap class=legendSumm>Rental Vendor:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Res/Conf:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Vehicle Class:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rate Type:</td></tr>"
            sHtml += "<tr id='trRental6'><td nowrap><input type=text class=inputView id=hidRentalVendor_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalResConf_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalVehicleClass_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalRateType_"+liVehNum+" size=15 readonly=true></td></tr>"

            sHtml += "<tr id='trRental7'><td width=150 nowrap class=legendSumm>Rate:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm> Auth Pickup: </td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Tax Rate:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Phone:</td></tr>"
            sHtml += "<tr id='trRental8'><td nowrap>$<input type=text class=inputView id=txtRentalRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalAuthPickup_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalTaxRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalPhoneAC_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneEN_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneUN_"+liVehNum+" size=7 readonly=true></td></tr>"

            sHtml += "<tr id='trRental9'><td width=150 nowrap class=legendSumm>Rental Address: </td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Rental City:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental State</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Zip:</td></tr>"
            sHtml += "<tr id='trRental10'><td nowrap><input type=text class=inputView id=txtRentalAddress_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalCity_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalState_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalZip_"+liVehNum+" size=15 readonly=true></td></tr>"
            sHtml +="</table></td></tr></table></td></tr>"

            sHtml += "<tr><td colspan=2 nowrap class=legendSumm>Vehicle VIN:</td><td colspan=4 nowrap class=legendSumm>Vehicle YMM:</td><td> </td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtVIN_"+liVehNum+" size=24 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtVehicleYear_"+liVehNum+" size=5 readonly=true> <input type=text class=inputView id=txtMake_"+liVehNum+" size=15 readonly=true> <input type=text class=inputView id=txtModel_"+liVehNum+" size=15 readonly=true></td><td nowrap></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle License Plate:</td><td colspan=1 nowrap class=legendSumm>Vehicle Drivable:</td><td colspan=3 nowrap class=legendSumm>Odometer:</td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtLicensePlateNumber_"+liVehNum+" size=24 readonly=true></td><td nowrap><input type=text class=inputView id=txtDrivable_"+liVehNum+" size=5 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtMileage_"+liVehNum+" size=8 readonly=true></td></tr><tr><td nowrap class=legendSumm>Primary Damage:</td><td nowrap> </td><td colspan=5 nowrap class=legendSumm>Secondary Damages:</td></tr><tr><td nowrap valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtPrimaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtSecondaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr><tr><td nowrap class=legendSumm>Shop Selected:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td colspan=5 valign=top style='color:#FF0000;#font-size:10pt;font-weight:bold;'><img border=0 src=images/warn_img.gif WIDTH=16 HEIGHT=16 hspace=6 align=bottom>No shop selected. Please select a shop for this vehicle.</td><td nowrap> </td><td nowrap><a href='javascript:selectShop(" + liVehNum.toString() + "," + liDispOrder.toString() + ",\"" + lsCurrAssignment.toString() + "\")' title='Select a shop' style='cursor:pointer;cursor:hand;'><img border=0 src=images/btn_select_shop.gif WIDTH=104 HEIGHT=31></a></td></tr></table></tr></table>"
           // sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr>   <tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr>  <tr><td nowrap class=legendSumm colspan=3>Email: </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap class=legendSumm>Cell phone carrier:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactEmailAddress_"+liVehNum+" readonly=true> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtCellPhoneCarrier_"+liVehNum+" readonly=true> </td></tr><tr><td nowrap class=legendSumm colspan=3>Preferred method of status updates:</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtPrefMethodUpd_"+liVehNum+"  readonly=true></td><td nowrap> </td><td nowrap></td><td nowrap> </td><td nowrap> </td></tr>  <tr><td width=152 nowrap class=legendSumm>Apply Rental:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Days:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Rental Daily Maximum:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Rental Maximum:</td></tr><tr><td nowrap><input type=text class=inputView id=txtRentalAuthorized_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalDays_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalDayAmount_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalMaxAmount_"+liVehNum+" size=16 readonly=true></td></tr><tr><td nowrap class=legendSumm>Rental Days Auth.:</td><td nowrap> </td><td nowrap colspan=3 class=legendSumm>Rental Instructions:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap valign=top><input type=text class=inputView id=txtRentalDaysAuthorized_"+liVehNum+" style='text-align:right' size=3 readonly=true></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtRentalInstructions_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle VIN:</td><td colspan=4 nowrap class=legendSumm>Vehicle YMM:</td><td> </td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtVIN_"+liVehNum+" size=24 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtVehicleYear_"+liVehNum+" size=5 readonly=true> <input type=text class=inputView id=txtMake_"+liVehNum+" size=15 readonly=true> <input type=text class=inputView id=txtModel_"+liVehNum+" size=15 readonly=true></td><td nowrap></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle License Plate:</td><td colspan=1 nowrap class=legendSumm>Vehicle Drivable:</td><td colspan=3 nowrap class=legendSumm>Odometer:</td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtLicensePlateNumber_"+liVehNum+" size=24 readonly=true></td><td nowrap><input type=text class=inputView id=txtDrivable_"+liVehNum+" size=5 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtMileage_"+liVehNum+" size=8 readonly=true></td></tr><tr><td nowrap class=legendSumm>Primary Damage:</td><td nowrap> </td><td colspan=5 nowrap class=legendSumm>Secondary Damages:</td></tr><tr><td nowrap valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtPrimaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtSecondaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr><tr><td nowrap class=legendSumm>Shop Selected:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td colspan=5 valign=top style='color:#FF0000;#font-size:10pt;font-weight:bold;'><img border=0 src=images/warn_img.gif WIDTH=16 HEIGHT=16 hspace=6 align=bottom>No shop selected. Please select a shop for this vehicle.</td><td nowrap> </td><td nowrap><a href='javascript:selectShop(" + liVehNum.toString() + "," + liDispOrder.toString() + ",\"" + lsCurrAssignment.toString() + "\")' title='Select a shop' style='cursor:pointer;cursor:hand;'><img border=0 src=images/btn_select_shop.gif WIDTH=104 HEIGHT=31></a></td></tr></table></tr></table>";
          }
          
          else if (liVehSummDispType == 3)
          {
          
            sHtml = "<br><br><table width=600 border=10 cellpadding=0 cellspacing=0 style='border:1px solid #000099; border-collapse:collapse'><tr><td><table width=100% border=0 cellpadding=0 cellspacing=0 style='border-collapse:collapse; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>&nbsp;New Vehicle Information</td><td align=right><a href=javascript:deleteVehicle("+liVehNum+") title='Delete this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_delete_blue.gif WIDTH=83 HEIGHT=31></a><img border=0 src=images/spacer.gif WIDTH=10 HEIGHT=1><a href=javascript:editVehicle("+liVehNum+") title='Edit this vehicle' name='' onMouseOver='window.status=this.title; return true;' onMouseOut='window.status=this.name; return true;' ><img border=0 src=images/btn_edit_blue.gif WIDTH=83 HEIGHT=31></a></td></tr></table></td></tr><tr><td valign=top style='padding: 0px 10px 10px 10px;'><table name=VehicleInfoTable id=VehicleInfoTable_"+liVehNum+" border=0 cellpadding=0 cellspacing=0><tr><td width=152 nowrap class=legendSumm>Party:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Coverage:</td><td width=10 nowrap></td><td width=160 nowrap class=legendSumm>Deductible:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Limit:</td></tr><tr><td nowrap><input type=text class=inputView id=txtExposureCD_"+liVehNum+" size=9 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtClientCoverageTypeDesc_"+liVehNum+" size=17 readonly=true></td><td nowrap></td><td nowrap>$<input type=text class=inputView id=txtDeductibleAmt_"+liVehNum+" size=16 readonly=true></td><td nowrap></td><td nowrap>$<input type=text class=inputView id=txtLimitAmt_"+liVehNum+" size=16 readonly=true></td></tr>";

            if (giReserveLineTypeScript == 1)
              sHtml += "<TR><TD nowrap class=legendSumm>	Line Type:</TD><TD nowrap></TD><TD nowrap class=legendSumm>	Reserve Number:</TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR><TR><TD nowrap><INPUT type=text class=inputView id=txtLineType_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap><INPUT type=text class=inputView id=txtReserve_"+liVehNum+" size=4 readonly=true></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD><TD nowrap></TD></TR>";

           // glsd451 Changes - Adding new controls as per andrew's requirement 

             if((lsCurrAssignment == "Program Shop" || lsCurrAssignment == "Repair Referral" || lsCurrAssignment == "Choice Shop") && IsRepairEmailCell == "true")
           {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap class=legendSumm colspan=3>Email: </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap class=legendSumm>Cell phone carrier:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactEmailAddress_"+liVehNum+" readonly=true> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtCellPhoneCarrier_"+liVehNum+" readonly=true> </td></tr><tr><td nowrap class=legendSumm colspan=3>Preferred method of status updates:</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtPrefMethodUpd_"+liVehNum+"  readonly=true></td><td nowrap> </td><td nowrap></td><td nowrap> </td><td nowrap> </td></tr>"           
            }
            else
            {
            sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap> </td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true> <input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr>"
            }
            sHtml += "<tr id='trRental1'><td width=152 nowrap class=legendSumm>Apply Rental:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Days:</td><td width=10 nowrap> </td><td width=160 nowrap class=legendSumm>Rental Daily Maximum:</td><td width=10 nowrap> </td><td width=100 nowrap class=legendSumm>Rental Maximum:</td></tr>"
            sHtml += "<tr id='trRental2'><td nowrap><input type=text class=inputView id=txtRentalAuthorized_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalDays_"+liVehNum+" size=3 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalDayAmount_"+liVehNum+" size=16 readonly=true></td><td nowrap> </td><td nowrap>$<input type=text class=inputView id=txtRentalMaxAmount_"+liVehNum+" size=16 readonly=true></td></tr>"
            sHtml += "<tr id='trRental3'><td nowrap class=legendSumm>Rental Days Auth.:</td><td nowrap> </td><td nowrap colspan=3 class=legendSumm>Rental Instructions:</td><td nowrap> </td><td nowrap> </td></tr>"
            sHtml += "<tr id='trRental4'><td nowrap valign=top><input type=text class=inputView id=txtRentalDaysAuthorized_"+liVehNum+" style='text-align:right' size=3 readonly=true></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtRentalInstructions_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr>"

               //Additional Rental Info
            sHtml += "<tr><td colspan='7'><table id=tblAdditionalRental_"+liVehNum+" style='border:1px solid #000099; margin-top:10px;'><tr><td colspan='7'><table cellpadding=5 cellspacing=0 style='border-collapse:collapse; width:100%; border-bottom:1px solid #000099;'><tr bgcolor='#E5E5F5'><td style='color:#000099; font-size:10pt; font-weight:bold;'>Additional Rental Info : </td></tr></table></td></tr><td style='padding:0px 10px 10px 10px;'><table>"
            sHtml += "<tr id='trRental5'><td width=150 nowrap class=legendSumm>Rental Vendor:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Res/Conf:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Vehicle Class:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rate Type:</td></tr>"
            sHtml += "<tr id='trRental6'><td nowrap><input type=text class=inputView id=hidRentalVendor_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalResConf_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalVehicleClass_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalRateType_"+liVehNum+" size=15 readonly=true></td></tr>"

            sHtml += "<tr id='trRental7'><td width=150 nowrap class=legendSumm>Rate:</td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm> Auth Pickup: </td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Tax Rate:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Phone:</td></tr>"
            sHtml += "<tr id='trRental8'><td nowrap>$<input type=text class=inputView id=txtRentalRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalAuthPickup_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalTaxRate_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalPhoneAC_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneEN_"+liVehNum+" size=4 readonly=true><input type=text class=inputView id=txtRentalPhoneUN_"+liVehNum+" size=7 readonly=true></td></tr>"

            sHtml += "<tr id='trRental9'><td width=150 nowrap class=legendSumm>Rental Address: </td><td width=10 nowrap></td><td width=150 nowrap class=legendSumm>Rental City:</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental State</td><td width=10 nowrap> </td><td width=150 nowrap class=legendSumm>Rental Zip:</td></tr>"
            sHtml += "<tr id='trRental10'><td nowrap><input type=text class=inputView id=txtRentalAddress_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalCity_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=hidRentalState_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtRentalZip_"+liVehNum+" size=15 readonly=true></td></tr>"
            sHtml +="</table></td></tr></table></td></tr>"

            sHtml += "<tr><td colspan=2 nowrap class=legendSumm>Vehicle VIN:</td><td colspan=4 nowrap class=legendSumm>Vehicle YMM:</td><td> </td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtVIN_"+liVehNum+" size=24 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtVehicleYear_"+liVehNum+" size=5 readonly=true> <input type=text class=inputView id=txtMake_"+liVehNum+" size=15 readonly=true> <input type=text class=inputView id=txtModel_"+liVehNum+" size=15 readonly=true></td><td nowrap></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle License Plate:</td><td colspan=1 nowrap class=legendSumm>Vehicle Drivable:</td><td colspan=3 nowrap class=legendSumm>Odometer:</td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtLicensePlateNumber_"+liVehNum+" size=24 readonly=true></td><td nowrap><input type=text class=inputView id=txtDrivable_"+liVehNum+" size=5 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtMileage_"+liVehNum+" size=8 readonly=true></td></tr><tr><td nowrap class=legendSumm>Primary Damage:</td><td nowrap> </td><td colspan=5 nowrap class=legendSumm>Secondary Damages:</td></tr><tr><td nowrap valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtPrimaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td><td nowrap> </td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtSecondaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr><tr><td nowrap class=legendSumm>Shop Selected:</td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td><td nowrap> </td></tr><tr><td colspan=5 valign=top><input type=text class=inputView id=txtShopName_"+liVehNum+" size=45 style='width:380px' readonly=true></td><td nowrap> </td><td nowrap valign=top rowspan=2><a href='javascript:selectShop(" + liVehNum.toString() + "," + liDispOrder.toString() + ",\"" + lsCurrAssignment.toString() + "\")' title='Select a shop' style='cursor:pointer;cursor:hand;'><img border=0 src=images/btn_select_shop.gif WIDTH=104 HEIGHT=31></a></td></tr><tr><td colspan=3 class=legendSumm nowrap>Shop Address:</td><td nowrap> </td><td nowrap class=legendSumm>Shop Phone:</td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtShopAddress1_"+liVehNum+" size=40 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtShopPhone_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td colspan=3 nowrap><input type=text class=inputView id=txtShopAddress2_"+liVehNum+" size=40 readonly=true></td><td nowrap> </td><td nowrap class=legendSumm>Shop Fax:</td><td nowrap> </td><td nowrap> </td></tr><tr><td colspan=3 nowrap><input type=text class=inputView id=txtShopCity_"+liVehNum+" size=25 readonly=true> <input type=text class=inputView id=txtShopState_"+liVehNum+" size=2 readonly=true> <input type=text class=inputView id=txtShopZip_"+liVehNum+" size=5 readonly=true></td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtShopFax_"+liVehNum+" size=15 readonly=true></td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=7 class=legendSumm>Shop Directions:</td></tr><tr><td colspan=7 valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtDrivingDirections_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td></tr><tr><td nowrap colspan=7 class=legendSumm>Comments to Shop:</td></tr><tr><td colspan=5 valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtShopRemarks_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td><td nowrap> </td><td nowrap valign=top><a href=javascript:editShopComments("+liVehNum+") title='Edit comments to shop' style='cursor:pointer;cursor:hand;'><img border=0 src=images/btn_edit_comments.gif WIDTH=120 HEIGHT=31></a></td></tr></table></td></tr></table>"

            //sHtml += "<tr><td nowrap class=legendSumm>Owner Name:</td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap class=legendSumm>Owner Phone:</td><td nowrap></td><td nowrap></td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtOwnerNameFirst_"+liVehNum+" size=16 readonly=true><input type=text class=inputView id=txtOwnerNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtOwnerPhoneSumm_"+liVehNum+" size=15 readonly=true></td><td nowrap></td><td nowrap></td></tr><tr><td nowrap class=legendSumm>Owner Business Name:</td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap></td></tr><tr><td nowrap colspan=5><input name=OwnerBusinessName id=txtOwnerBusinessName_"+liVehNum+" type=text class=inputView size=50 readonly=true></td><td nowrap></td><td nowrap></td></tr><tr><td nowrap class=legendSumm>Contact Name:</td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap class=legendSumm>Contact Phone:</td><td nowrap></td><td nowrap class=legendSumm>Pref. Contact Phone:</td></tr>  <tr><td nowrap colspan=3><input type=text class=inputView id=txtContactNameFirst_"+liVehNum+" size=16 readonly=true><input type=text class=inputView id=txtContactNameLast_"+liVehNum+" size=21 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtContactPhoneSumm_"+liVehNum+" size=15 readonly=true> (Day)</td><td nowrap></td><td nowrap><input type=text class=inputView id=txtContactBestPhoneCD_"+liVehNum+" size=10 readonly=true></td></tr>  <tr><td nowrap class=legendSumm colspan=3>Email: </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactNightPhoneSumm_"+liVehNum+" size=15 readonly=true> (Night)</td><td nowrap> </td><td nowrap class=legendSumm>Cell phone carrier:</td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtContactEmailAddress_"+liVehNum+" readonly=true> </td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactAltPhoneSumm_"+liVehNum+" size=15 readonly=true> (Alt.)</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtCellPhoneCarrier_"+liVehNum+" readonly=true> </td></tr><tr><td nowrap class=legendSumm colspan=3>Preferred method of status updates:</td><td nowrap> </td><td nowrap><input type=text class=inputView id=txtContactCellPhoneSumm_"+liVehNum+" size=15 readonly=true> (Cell)</td><td nowrap> </td><td nowrap> </td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtPrefMethodUpd_"+liVehNum+"  readonly=true></td><td nowrap> </td><td nowrap></td><td nowrap> </td><td nowrap> </td></tr>  <tr><td width=152 nowrap class=legendSumm>Apply Rental:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Days:</td><td width=10 nowrap></td><td width=160 nowrap class=legendSumm>Rental Daily Maximum:</td><td width=10 nowrap></td><td width=100 nowrap class=legendSumm>Rental Maximum:</td></tr><tr><td nowrap><input type=text class=inputView id=txtRentalAuthorized_"+liVehNum+" size=3 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtRentalDays_"+liVehNum+" size=3 readonly=true></td><td nowrap></td><td nowrap>$<input type=text class=inputView id=txtRentalDayAmount_"+liVehNum+" size=16 readonly=true></td><td nowrap></td><td nowrap>$<input type=text class=inputView id=txtRentalMaxAmount_"+liVehNum+" size=16 readonly=true></td></tr><tr><td nowrap class=legendSumm>Rental Days Auth.:</td><td nowrap></td><td nowrap colspan=3 class=legendSumm>Rental Instructions:</td><td nowrap></td><td nowrap></td></tr><tr><td nowrap valign=top><input type=text class=inputView id=txtRentalDaysAuthorized_"+liVehNum+" style='text-align:right' size=3 readonly=true></td><td nowrap></td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtRentalInstructions_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle VIN:</td><td colspan=4 nowrap class=legendSumm>Vehicle YMM:</td><td></td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtVIN_"+liVehNum+" size=24 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtVehicleYear_"+liVehNum+" size=5 readonly=true><input type=text class=inputView id=txtMake_"+liVehNum+" size=15 readonly=true><input type=text class=inputView id=txtModel_"+liVehNum+" size=15 readonly=true></td><td nowrap></td></tr><tr><td colspan=2 nowrap class=legendSumm>Vehicle License Plate:</td><td colspan=1 nowrap class=legendSumm>Vehicle Drivable:</td><td colspan=3 nowrap class=legendSumm>Odometer:</td></tr><tr><td colspan=2 nowrap><input type=text class=inputView id=txtLicensePlateNumber_"+liVehNum+" size=24 readonly=true></td><td nowrap><input type=text class=inputView id=txtDrivable_"+liVehNum+" size=5 readonly=true></td><td colspan=4 nowrap><input type=text class=inputView id=txtMileage_"+liVehNum+" size=8 readonly=true></td></tr><tr><td nowrap class=legendSumm>Primary Damage:</td><td nowrap></td><td colspan=5 nowrap class=legendSumm>Secondary Damages:</td></tr><tr><td nowrap valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtPrimaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td><td nowrap></td><td colspan=5 nowrap valign=top><textarea cols='78' rows='4' wrap='physical' class=inputView id=txtSecondaryDamageDescription_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td></tr><tr><td nowrap class=legendSumm>Shop Selected:</td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap></td><td nowrap></td></tr><tr><td colspan=5 valign=top><input type=text class=inputView id=txtShopName_"+liVehNum+" size=45 style='width:380px' readonly=true></td><td nowrap></td><td nowrap valign=top rowspan=2><a href=javascript:selectShop("+liVehNum+","+liDispOrder+") title='Select a shop' style='cursor:pointer;cursor:hand;'><img border=0 src=images/btn_select_shop.gif WIDTH=104 HEIGHT=31></a></td></tr><tr><td colspan=3 class=legendSumm nowrap>Shop Address:</td><td nowrap></td><td nowrap class=legendSumm>Shop Phone:</td><td nowrap></td></tr><tr><td nowrap colspan=3><input type=text class=inputView id=txtShopAddress1_"+liVehNum+" size=40 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtShopPhone_"+liVehNum+" size=15 readonly=true></td><td nowrap></td><td nowrap></td></tr><tr><td colspan=3 nowrap><input type=text class=inputView id=txtShopAddress2_"+liVehNum+" size=40 readonly=true></td><td nowrap></td><td nowrap class=legendSumm>Shop Fax:</td><td nowrap></td><td nowrap></td></tr><tr><td colspan=3 nowrap><input type=text class=inputView id=txtShopCity_"+liVehNum+" size=25 readonly=true><input type=text class=inputView id=txtShopState_"+liVehNum+" size=2 readonly=true><input type=text class=inputView id=txtShopZip_"+liVehNum+" size=5 readonly=true></td><td nowrap></td><td nowrap><input type=text class=inputView id=txtShopFax_"+liVehNum+" size=15 readonly=true></td><td nowrap></td><td nowrap></td></tr><tr><td nowrap colspan=7 class=legendSumm>Shop Directions:</td></tr><tr><td colspan=7 valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtDrivingDirections_"+liVehNum+" readonly=true style='width:100%;height:35px;'></textarea></td></tr><tr><td nowrap colspan=7 class=legendSumm>Comments to Shop:</td></tr><tr><td colspan=5 valign=top><textarea cols='' rows='' wrap='physical' class=inputView id=txtShopRemarks_"+liVehNum+" readonly=true style='width:100%;height:50px;'></textarea></td><td nowrap></td><td nowrap valign=top><a href=javascript:editShopComments("+liVehNum+") title='Edit comments to shop' style='cursor:pointer;cursor:hand;'><img border=0 src=images/btn_edit_comments.gif WIDTH=120 HEIGHT=31></a></td></tr></table></tr></table>";
          }
          else if (liVehSummDispType == 0)
            sHtml = "";
          
          loNewDiv.innerHTML = sHtml;
        }
      }
    }
  }
  
  
  function addNewVehicle()
  {
  var liLastvehNumber = parent.getLastVehNum();
  var SelectedShop = document.getElementById("txtShopName_" + liLastvehNumber).value;

  if(SelectedShop == "" || SelectedShop == undefined || SelectedShop == null)
  { 
     alert("Please select the shop for vehicle "+liLastvehNumber +", before add the new vehicle");
     document.getElementById("txtShopName_" + liLastvehNumber).focus();
     return;
  }

    if(parent)
    {
        var liVehNum = parent.getLastVehNum() + 1;
        parent.document.getElementById("uiFrame").src = "frmWaAddVehicle.asp?fromPage=ClaimSummary_addNew&vehNum=" + liVehNum;
    }
    else
      return false;
  }
  
  
  function submitClaim()
  {

  var liLastvehNumber = parent.getLastVehNum();
  var SelectedShop = document.getElementById("txtShopName_" + liLastvehNumber).value;

  if(SelectedShop == "" || SelectedShop == undefined || SelectedShop == null)
  { 
     alert("Please select the shop for vehicle "+liLastvehNumber +", before submit the claim");
     document.getElementById("txtShopName_" + liLastvehNumber).focus();
     return;
  }

    if(parent)
    {
      var lsWasSubmitted = parent.chkClaimSubmitted();
      if (lsWasSubmitted == "false")
      {
        var lsShopReq = parent.chkReqSP();
        if (lsShopReq == "")
        {
        var AssignmentDesc = "<%=IsAssignmentDesc %>";

        if(AssignmentDesc == "Program Shop")
            parent.document.getElementById("uiFrame").src = "frmWaSubmitNewVehicle.asp?fromPage=ClaimSummary&fromAssignmentType=ProgramShop";
        else if(AssignmentDesc == "Choice Shop")
            parent.document.getElementById("uiFrame").src = "frmWaSubmitNewVehicle.asp?fromPage=ClaimSummary&fromAssignmentType=ChoiceShop";
        else
           parent.document.getElementById("uiFrame").src = "frmWaSubmitNewVehicle.asp?fromPage=ClaimSummary";

        }
        else
        {
          alert(lsShopReq);
          return;
        }
      }
      else
      {
        alert("Unable to submit claim. This claim has already been submitted successfully.");
        return;
      }
    }
    else
      return false;
  }
  
  
  function editClaim()
  {
    if(parent)
    {
      dspMsg();
      parent.document.getElementById("uiFrame").src = "frmWaClaimInfo.asp?fromPage=ClaimSummary";
    }
    else
      return false;
  }
  
  
  function editVehicle(vehNum)
  {
    var liVehNum = vehNum;
    if(parent)
    {
      dspMsg();
      parent.document.getElementById("uiFrame").src = "frmWaAddVehicle.asp?fromPage=ClaimSummary&vehNum=" + liVehNum;
    }
    else
      return false;
  }
  
  
  function dspMsg()
  {
    var objIFrame = parent.document.getElementById('uiFrame');
    objIFrame.style.height = "0";
    parent.showMsg();
  }

  
  function deleteVehicle(vehNum)
  {
    if(parent)
    {
      var liVehNum = vehNum;
      var lbDelVehQ = window.confirm("Vehicle " + liVehNum + " will be deleted. Are you sure you want to delete this vehicle?");
      if (lbDelVehQ == true)
        parent.delVehicle(liVehNum, "ClaimSummary_addNew");
      else return;
    }
  }
  
  
  function selectShop(vehNum, DispOrder,AssignmentType)
  {
    var liVehNum = vehNum;
    var prevPage = "frmWaClaimSummaryNewVeh";
    var liDispOrder = DispOrder;
    var liAssignmentType = AssignmentType;

    var CurrentAssignmentType ="<%= lsShopAssignmentType %>";
    
    if(CurrentAssignmentType != "" && CurrentAssignmentType != undefined && CurrentAssignmentType != null)
      if(CurrentAssignmentType == "ProgramShop")
         liAssignmentType = "Choice Shop";


    if(parent)
      parent.getShopSelectionData(liVehNum, liDispOrder,liAssignmentType, prevPage);
    else
      return false;
  }
  
  
  function editShopComments(vehNum)
  {
    var liVehNum = vehNum;
    if(parent)
      parent.document.getElementById("uiFrame").src = "frmWaShopComments.asp?fromPage=ClaimSummary_addNew&vehNum=" + liVehNum;
    else
      return false;
  }
  
  
  function goCancel()
  {
    if (parent)
    {
      var lbConfirmCancel = window.confirm("Stop entering this assignment and return?");
      if (lbConfirmCancel)
      {
        top.location = "frmMyClaimsDesktop.asp";
      }
    }
  }


</script>

</head>
<body onload="resizeWaIframe(gsVehCount); pageInit(); parent.hideMsg();">

<div id="divContainer">

  <form name="frmSummaryInfo">

    <table id="tblClaimInfoBlock" width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse;">
      <tr>
        <td>

          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-bottom: 1px solid #000099;">
            <tr bgcolor="#E5E5F5"">
              <td style="color:#000099; font-size:10pt; font-weight:bold; height:24px;">
                &nbsp;Claim Information
              </td>
            </tr>
          </table>

        </td>
      </tr>

      <tr>
        <td valign="top" style="padding: 0px 10px 10px 10px;">

          <table name="ClaimInfoTable" id="ClaimInfoTable" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
            <colgroup>
              <col width="205px"/>
              <col width="26px"/>
              <col width="195px"/>
              <col width="26px"/>
              <col width="120px"/>
              <col width="4px"/>
            </colgroup>
            <tr>
              <td nowrap class="legendSumm">Claim Assigned by:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap class="legendSumm">Relation to Insured:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap id="txtCallerName" class="inputView"></td>
              <td nowrap>&nbsp;</td>
              <td nowrap id="txtCallerRelationToInsuredIDDescription" class="inputView" ></td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Claim Number:</td>
              <td>&nbsp;</td>
              <td class="legendSumm">Loss Date:</td>
              <td>&nbsp;</td>
              <td class="legendSumm">Loss State:</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap id="txtCoverageClaimNumber" class="inputView"></td>
              <td nowrap>&nbsp;</td>
              <td nowrap id="txtLossDate"  class="inputView"></td>
              <td nowrap>&nbsp;</td>
              <td nowrap id="txtLossAddressState" class="inputView"></td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap colspan="6" class="legendSumm">Description of Loss:</td>
            </tr>
            <tr>
<td nowrap colspan="5">
                <textarea name="LossDescription" id="txtLossDescription" wrap="physical" class="inputView" readonly="true" style="width:100%; height:50px;"></textarea>
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Insured Name:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap colspan="3" class="legendSumm">Insured Business Name:</td>
            </tr>
            <tr>
              <td nowrap id="txtInsuredName" class="inputView"></td>
              <td nowrap>&nbsp;</td>
              <td nowrap colspan="3" id="txtInsuredBusinessName" class="inputView"></td>
              <td colspan="6" id="txtRemarks" style="display:none"></td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <DIV id="vehTblHolder"><br><SPAN style="font-size:11px; font-weight:bold">Loading Vehicle Data...</SPAN></DIV>

    <table width="600px" id="lastTbl" border="0" cellpadding="0" cellspacing="0">
      <colgroup>
        <col width="390px"/>
        <col width="100px"/>
        <col width="110px"/>
      </colgroup>
      <tr>
        <td colspan="3" nowrap>
          <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="10">
        </td>
      </tr>
       <tr>
      <td width="100%">
      <!-- Add veh button div and table starts here -->
      <div id="AddNewVehDiv">
      <table border="0" cellpadding="0" cellspacing="0" width="200%">
      <tr>
        <td colspan="2" valign="middle" class="legendnopad" style="font-size:14px;">
          Add Another Vehicle
        </td>
        <td nowrap>
          <a href="javascript:addNewVehicle()"
             title="Add another vehicle"
             onMouseOver="window.status='Add another vehicle'; return true"
             onMouseOut="window.status=''; return true">
            <img border="0" src="images/btn_AddVehicle.gif" WIDTH="104" HEIGHT="28">
          </a>
        </td>
      </tr>
      <tr>
        <td colspan="3" class="legendnopad">
          <span style="color:#000000; font-weight:normal; font-size:10pt">
          If you would like to add another vehicle to this claim, please press the "Add Vehicle" button.
          </span>
        </td>
      </tr>
      </table>
      </div> 

       <!-- Add veh button div and table ends here -->

      </td>
       </tr>
      <tr>
        <td colspan="3" nowrap>
          <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="10">
        </td>
      </tr>
      <tr>
        <td valign="middle" class="legendnopad" style="font-size:14px;">
          Submit Claim to LYNX Services
        </td>
        <td valign="top" nowrap>
          <a href='javascript:goCancel()'
             title='Cancel this assignment and return to My Claims Desktop'
             onMouseOver="window.status='Cancel this assignment and return to My Claims Desktop'; return true"
             onMouseOut="window.status=''; return true">
            <img src='images/btn_cancel.gif' border='0' WIDTH='83' HEIGHT='31'>
          </a>
        </td>
        <td nowrap>
          <a href="javascript:submitClaim()"
             title="Submit Claim to LYNX"
             onMouseOver="window.status='Submit Claim to LYNX'; return true"
             onMouseOut="window.status=''; return true">
            <img border="0" src="images/btn_submit_green.gif" WIDTH="83" HEIGHT="31">
          </a>
        </td>
      </tr>
      <tr>
        <td colspan="3" class="legendnopad">
          <span style="color:#000000; font-weight:normal; font-size:10pt">
          If you have entered all the vehicles that pertain to this claim, please review the information for accuracy
          and make any necessary changes by pressing the "EDIT" button. When you are done, please press the "SUBMIT" button
          to send the Claim information to LYNX Services.
          </span>
        </td>
      </tr>
    </table>

  </form>

</div>

</body>
</html>