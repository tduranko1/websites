<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetCurrAssignment.asp"-->
<!-- v1.4.5.0 -->

<%
  Dim lsInsCoDetailsCall, lsCarrierUsersListCall, lsPrevPage
  Dim lsInsConfigXML, loXML, ElemList, oNode, lsAssignmentTypeIDList, laAssignmentTypeID

  lsPrevPage = Request("fromPage")
  
  lsPageName = "frmFNOLSearch.asp"
  lsPageTitle =  lsCurrentAssignmentDesc & " FNOL Search"
  liPageType = lcAssignmentPage
  liOfficeID = Request.QueryString("officeid")

'  'Create Elemlist if either of these variables = 1
'  'We need Elemlist later in the page
'  If liCurrAssignmentEmergencyLiability = 1 or liCurrAssignmentSB511 = 1 Then

'    Set loXML = Server.CreateObject("MSXML2.DOMDocument")
'    loXML.Async = False
  
'    lsInsConfigXML = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
'    loXML.LoadXML GetPersistedData(lsInsConfigXML, lsInsCoConfigGUID, "", "", false)
  
'    If Not loXML.ParseError = 0 Then
'      Response.Write "<b>Error Code:</b> " & loXML.ParseError & "<br>"
'  
'    Else
'      Set ElemList = loXML.SelectNodes("/Root/CustomScripting")
'    
'    End If

'  End If
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<html>
<head>
<title>FNOL Search</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/incCalendar.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "<%= lsPrevPage %>";
  gsVehCount = "";

<%
'  lsInsCoDetailsCall = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"

'  Response.Write GetPersistedData(lsInsCoDetailsCall, lsInsCoConfigGUID, "WaClaimNumberValidation.xsl", "OfficeID=" & liOfficeID, true)
%>
  var iLynxID, iVehicleID, strClaimNumber;
  var objSelectedRow = null;
  var objSelectedVehRow = null;
  var objRet = {
                  claimxml: null,
                  claimnumber: null,
                  vehicleid: null
               }
  
  function doSearch(){
      if (document.getElementById("rbSearchLynxId").checked == true){
         document.getElementById("txtClaimNumber").value = "";
         document.getElementById("txtLastName").value = "";
         document.getElementById("txtDOL").value = "";
         
         ldTrim(document.getElementById("txtLynxID"));
         var strLynxID = document.getElementById("txtLynxID").value;
         if (strLynxID == "") {
            alert("Enter a LYNX id you would like to search.");
            document.getElementById("txtLynxID").select();
            event.returnValue = false;
            return false;
         }
      } else if (document.getElementById("rbSearchClaimNumber").checked == true) {
         document.getElementById("txtLynxID").value = "";
         document.getElementById("txtLastName").value = "";
         document.getElementById("txtDOL").value = "";
         
         ldTrim(document.getElementById("txtClaimNumber"));
         var strClaimNumber = document.getElementById("txtClaimNumber").value;
         if (strClaimNumber == ""){
            alert("Enter the claim number you would like to search.");
            document.getElementById("txtClaimNumber").select();
            event.returnValue = false;
            return false;
         }
      } else {
         document.getElementById("txtLynxID").value = "";
         document.getElementById("txtClaimNumber").value = "";

         ldTrim(document.getElementById("txtLastName"));
         ldTrim(document.getElementById("txtDOL"));
         
         var strLastName = document.getElementById("txtLastName").value;
         var strDOL = document.getElementById("txtDOL").value;
         
         if (strLastName == "" || strDOL == ""){
            alert("Enter the Insured Last Name and Date of Loss.");
            document.getElementById("txtLastName").select();
            event.returnValue = false;
            return false;
         }
         
         if (strDOL.split("/").length != 3){
            event.returnValue = false;
            return false;
         }
      }
      
      document.getElementById("btnSearch").disabled = true;

      var objProgress = document.getElementById("divProgress");
      if (objProgress){
         objProgress.style.display = "inline";
      }
      
      frmFNOLSearch.submit();
  }
  
  function doClose(){
      window.returnValue = objRet;
      window.close();
  }
  
  function formatLossDate() 
  {
    var obj = event.srcElement;
    //if (document.getElementById('txtDOL').value.length != 0)
    if (obj.value.length != 0)
    {
      var lsDate = obj.value;
      var lsMonth, lsDay, lsYear;
      if (lsDate.indexOf("/") != -1) {
        var laryDate = lsDate.split("/");
        lsMonth = laryDate[0];
        lsDay = laryDate[1];
        lsYear = laryDate[2];
      } else {
        //try to format with regular expression.
        lsYear = lsDate.substr(lsDate.length - 4, 4);
        var lsReminder = lsDate.substr(0, lsDate.length - 4);
        switch (lsReminder.length) {
          case 4:
            lsMonth = lsReminder.substr(0, 2);
            lsDay = lsReminder.substr(2, 2);
            break;
          case 3:
            if (parseInt(lsReminder.substr(0, 2), 10) > 12) {
              lsMonth = lsReminder.substr(0, 1);
              lsDay = lsReminder.substr(1, 2)
            } else {
              lsMonth = lsReminder.substr(0, 2);
              lsDay = lsReminder.substr(2, 1)              
            }
            break;
          case 2:
            lsMonth = lsReminder.substr(0, 1);
            lsDay = lsReminder.substr(1, 1);
            break;
        }
      }
  
      var lbBadDate = false;
      if (lsMonth == undefined || lsMonth < 1 || lsMonth > 12)
        lbBadDate = true;
      if (lsDay == undefined || lsDay < 1 || lsDay > 31)
        lbBadDate = true;
      if (lsYear == undefined || lsYear < 1)
        lbBadDate = true;
      if (lbBadDate == true)
      {
        alert("Please enter the date as MM/DD/YYYY or M/D/YY or MMDDYYYY.");
        obj.select();
        event.returnValue = false;
        return false;
      }
  
      if (lsMonth < 10 && lsMonth.charAt(0) != 0)
        lsMonth = "0"+lsMonth;

      if (lsDay < 10 && lsDay.charAt(0) != 0)
        lsDay = "0"+lsDay;

      var lsMydate = new Date();
      if (lsYear == undefined || lsYear == "" || (lsYear >= 100 && lsYear < 1900))
      {
        lsYear = lsMydate.getFullYear();
      }
      else if (lsYear >= 0 && lsYear < 100)
      {
        lsYear = Number(lsYear) + 2000;
      }
      else if ((lsYear >= 1900 && lsYear < 2000) || (lsYear > 2000))
      {
        lsYear = Number(lsYear);
      }
      else
      {
        lsYear = lsMydate.getFullYear();
      }

      obj.value = lsMonth + "/" + lsDay + "/" + lsYear;
    }
  }
  
  function showSearchResult(strSearchResultXML){
      document.getElementById("btnSearch").disabled = false;
      var objProgress = document.getElementById("divProgress");
      if (objProgress){
         objProgress.style.display = "none";
      }
      var iRecords = 0;
      if (strSearchResultXML == ""){
         strSearchResultXML = "<SearchFNOLResult records=\"0\"/>"
         document.getElementById("tblSearchResult").style.display = "none";
         document.getElementById("divNoRecords").style.display = "inline";
      } else {
       document.getElementById("xmlSearchResult").loadXML(strSearchResultXML);
      iRecords = document.getElementById("xmlSearchResult").documentElement.getAttribute("records");
      document.getElementById("xmlSearchResult").documentElement.removeAttribute("records");
      document.getElementById("xmlSearchResult").loadXML(document.getElementById("xmlSearchResult").xml);
    }
      

      document.getElementById("divSearchContainer").style.display = "none";
      document.getElementById("divSearchResult").style.display = "inline";
      
      if (isNaN(parseInt(iRecords, 10)) == true || parseInt(iRecords, 10) == 0){
         document.getElementById("tblSearchResult").style.display = "none";
         document.getElementById("divNoRecords").style.display = "inline";
         alert("No LYNX FNOL Records match your search criteria");
      } else {
         document.getElementById("tblSearchResult").style.display = "inline";
         document.getElementById("divNoRecords").style.display = "none";
         
         if (parseInt(iRecords, 10) > 0){
            window.setTimeout("autoSelectClaim(" + parseInt(iRecords, 10) + ")", 200);
         }
      }
  }

  function autoSelectClaim(iRecords){
      var oTbl = document.getElementById("tblSearchResult");
      if (oTbl){
         if (oTbl.rows.length == iRecords){
            oTbl.rows[0].fireEvent("onclick");
         } else {
            window.setTimeout("autoSelectClaim(" + iRecords + ")", 200);
         }
      }
  }
  
  function newSearch(){
      document.getElementById("divSearchContainer").style.display = "inline";
      document.getElementById("divSearchResult").style.display = "none";
      document.getElementById("divVehicles").style.display = "none";
      document.getElementById("xmlSearchResult").loadXML("<Root/>");
      document.getElementById("xmlFNOLClaimData").loadXML("<Root/>");
      document.getElementById("xmlVehicles").loadXML("<Root/>");
  }
  
  function selectSearchBy(strType){
      switch (strType) {
         case 1:
            document.getElementById("txtLynxID").disabled = false;
            document.getElementById("txtLynxID").select();
            
            document.getElementById("txtClaimNumber").disabled = true;
            document.getElementById("txtLastName").disabled = true;
            document.getElementById("txtDOL").disabled = true;
            document.getElementById("btnCal").disabled = true;
            break;
         case 2:
            document.getElementById("txtLynxID").disabled = true;
            document.getElementById("txtClaimNumber").disabled = false;
            document.getElementById("txtClaimNumber").select();
            
            document.getElementById("txtLastName").disabled = true;
            document.getElementById("txtDOL").disabled = true;
            document.getElementById("btnCal").disabled = true;
            break;
         case 3:
            document.getElementById("txtLynxID").disabled = true;
            document.getElementById("txtClaimNumber").disabled = true;
            
            document.getElementById("txtLastName").disabled = false;
            document.getElementById("txtLastName").select();
            document.getElementById("txtDOL").disabled = false;
            document.getElementById("btnCal").disabled = false;
         break;
      }
  }
  
  function initSearch(){
      selectSearchBy(1);
      var objProgress = document.getElementById("divProgress");
      if (objProgress){
         objProgress.style.left = (document.body.offsetWidth - objProgress.offsetWidth) / 2;
         objProgress.style.top = (document.body.offsetHeight - objProgress.offsetHeight) / 2;
         objProgress.style.visibility = "visible";
         objProgress.style.display = "none"
      }
  }
  
  function selectRecord(objRow){
      if (objSelectedRow) {
         objSelectedRow.style.backgroundColor = "#FFFFFF";
      }
      
      if (objRow) {
         objSelectedRow = objRow;
         objSelectedRow.style.backgroundColor = "#00BFFF";
         iLynxID = objSelectedRow.cells[0].innerText;
         strClaimNumber = objSelectedRow.cells[1].innerText;
         document.getElementById("txtGetLynxID").value = iLynxID;
         document.getElementById("btnSelectLynxID").disabled = false;
      }
  }
  
  function getLynxIDDetails(){
      if (document.getElementById("txtGetLynxID").value == ""){
         alert("Please select a claim and then click on the 'Next' button.");
      }
      
      var objProgress = document.getElementById("divProgress");
      if (objProgress){
         objProgress.style.display = "inline";
      }

      document.getElementById("frmFNOLGetClaim").submit();
  }
  
  function showFNOLData(strClaimXML){
      var objProgress = document.getElementById("divProgress");
      if (objProgress){
         objProgress.style.display = "none";
      }
      if (strClaimXML == "") strClaimXML = "<Root/>";
      var objClaim = document.getElementById("xmlFNOLClaimData");
      if (objClaim){
         objClaim.loadXML(strClaimXML);
         var objVehicles = objClaim.selectNodes("/Root/Claim/Vehicle");
         var objVehicleXML = document.getElementById("xmlVehicles");
         objVehicleXML.loadXML("<Root/>");
         var iRecords = objVehicles.length;
         for (var i = 0; i < objVehicles.length; i++){
            var oVeh = objVehicleXML.createElement("Vehicle");
            oVeh.setAttribute("id", objVehicles[i].getAttribute("id"));
            oVeh.setAttribute("party", (objVehicles[i].getAttribute("exposurecd") == "1" ? "1st" : "3rd"));
            oVeh.setAttribute("owner", "");
            oVeh.setAttribute("year", objVehicles[i].getAttribute("year"));
            oVeh.setAttribute("make", objVehicles[i].getAttribute("make"));
            oVeh.setAttribute("model", objVehicles[i].getAttribute("model"));
            
            var oOwner = objVehicles[i].selectSingleNode("Owner");
            if (oOwner) {
               var strName = oOwner.getAttribute("NameFirst") + " " + oOwner.getAttribute("NameLast");
               oVeh.setAttribute("owner", strName);
            }
            objVehicleXML.documentElement.appendChild(oVeh);
            
            //alert(iRecords);
            
            objVehicleXML.loadXML(objVehicleXML.xml);
         }
         
         document.getElementById("divVehicles").style.display = "inline";
         if (isNaN(parseInt(iRecords, 10)) == true || parseInt(iRecords, 10) == 0){
            document.getElementById("tblVehicleList").style.display = "none";
            document.getElementById("divNoVehRecords").style.display = "inline";
            //alert("No LYNX FNOL Records match your search criteria");
         } else {
            document.getElementById("tblVehicleList").style.display = "inline";
            document.getElementById("divNoVehRecords").style.display = "none";
            
            if (iRecords > 0) {
               window.setTimeout("autoSelectVehicle(" + iRecords + ")", 200);
            }
         }
         //alert(objVehicleXML.xml);
         divSearchResult.style.display = "none";
      }
  }
  
  function autoSelectVehicle(iRecords){
      var oTbl = document.getElementById("tblVehicleList");
      if (oTbl){
         if (oTbl.rows.length == iRecords){
            oTbl.rows[0].fireEvent("onclick");
         } else {
            window.setTimeout("autoSelectVehicle(" + iRecords + ")", 200);
         }
      }
  }

  function selectVehRecord(objRow){
      if (objSelectedVehRow) {
         objSelectedVehRow.style.backgroundColor = "#FFFFFF";
      }
      
      if (objRow) {
         objSelectedVehRow = objRow;
         objSelectedVehRow.style.backgroundColor = "#00BFFF";
         iVehicleID = objSelectedVehRow.cells[0].innerText;
         document.getElementById("btnSelectVehicle").disabled = false;
      }
  }
  
  function selectVehicle(){
      //alert(iVehicleID);
      //document.getElementById("txtOfficeID").value = liOfficeID
      objRet.claimxml = xmlFNOLClaimData.xml;
      objRet.claimnumber = strClaimNumber;
      objRet.vehicleid = iVehicleID;
      window.returnValue = objRet;
      window.close();
  }
  
  function showSearchData(){
      document.getElementById("divVehicles").style.display = "none";
      document.getElementById("divSearchResult").style.display = "inline";
  }
  
  function showCalendar(){
    var lsDimensions = "dialogHeight:225px; dialogWidth:177px; "
    var lsSettings = "scroll:no; resizable:no; status:no; help:no; center:yes; unadorned:yes;"
    var lsQuery = "frmCalendar.asp?DateElmId=" + document.getElementById("txtDOL").value + "&AllowFutureDates=0";
    var strRet = window.showModalDialog( lsQuery, window, lsDimensions + lsSettings );
    if (strRet != undefined && strRet.indexOf("/") != -1){
      document.getElementById("txtDOL").value = strRet;
    }
  }

  document.onclick = hideCalFrame;
</script>

</head>

<body onload="initSearch()" tabIndex="-1" style="padding:10px">
<div id="divSearchContainer">
  <form name="frmFNOLSearch" action="frmFNOLSearchResult.asp?officeid=<%=liOfficeID %>" method="post" target="frmSearch" onsubmit="doSearch()">
    <input id="txtInsuranceCompanyID" type="hidden" value="<%=liInsuranceCompanyID%>" ></input>
    <input id="txtOfficeID" type="hidden" value="<%=liOfficeID%>" />
    <input id="txtSearchType" type="hidden" value=""></input>

    <table border="0" cellpadding="2" cellspacing="0" style="table-layout:fixed">
      <colgroup>
         <col width="32px"/>
      	<col width="118px"/>
      	<col width="160px"/>
      	<col width="80px"/>
      	<col width="87px"/>
      	<col width="30px"/>
      	<col width="75px"/>
      </colgroup>
      <tr>
         <td colspan="7" style="font-weight:bold">Enter a LYNX id or Claim Number or Insured Last Name with Date of Loss</td>
      </tr>
      <tr>
         <td><input type="radio" id="rbSearchLynxId" name="rbSearchBy" checked="true" onclick="selectSearchBy(1)"></td>
      	<td><label for="rbSearchLynxId">LYNX id:</label></td>
      	<td colspan="5"><input type="text" size="10" maxlength="10" id="txtLynxID" name="txtLynxID"></input></td>
      	<!--<td colspan="4">
      		<button onclick="doIDSearch()">Search</button>&nbsp;
      	</td>-->
      </tr>
      <tr>
         <td><input type="radio" id="rbSearchClaimNumber" name="rbSearchBy" onclick="selectSearchBy(2)"></td>
      	<td><label for="rbSearchClaimNumber">Claim Number:</label></td>
      	<td colspan="5"><input type="text" size="20" maxlength="30" id="txtClaimNumber" name="txtClaimNumber"></input></td>
      	<!--<td colspan="4">
      		<button onclick="doClmNumberSearch()">Search</button>&nbsp;
      	</td>-->
      </tr>
      <tr>
         <td><input type="radio" id="rbSearchLastName" name="rbSearchBy" onclick="selectSearchBy(3)"></td>
      	<td><label for="rbSearchLastName">Insured Last Name:</label></td>
      	<td><input type="text" size="20" maxlength="30" id="txtLastName" name="txtLastName"></input></td>
      	<td>Date of Loss:</td>
      	<td>
      		<input type="text" size="10" maxlength="10" id="txtDOL" name="txtDOL" onBlur="formatLossDate()"></input>
      	</td>
      	<td colspan="2">
      		<button id="btnCal" tabIndex="-1" onclick="showCalendar()" style="border:0px;margin:0px;padding:0px;background-color:transparent"><img src="images/calendar.gif" alt="" width="24" height="17" border="0" title="Click to select date from calendar"></button>
      	</td>
      	<!--<td>
      		<button onclick="doNameSearch()">Search</button>&nbsp;
      	</td>-->
      </tr>
      <tr>
      	<td colspan="7">Partial Claim Number or Last Name entries are permitted.<br/></td>
      </tr>
      <tr>
      	<td colspan="7"><input id="btnSearch" type="submit" value="Search"/>&nbsp;<button onclick="doClose()">Cancel</button></td>
      </tr>
    </table>
  </form>

  
</div>

<div id="divSearchResult" style="display:none">
   <div style="font-size:14px">Select a claim:</div>
   <table border="1" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
   	<colspan>
   		<col width="75px"/>
   		<col width="120px"/>
   		<col width="100px"/>
   		<col width="240px"/>
   	</colspan>
      <tr>
         <td style="font-weight:bold;background-color:silver;text-align:center">LYNX ID</td>
         <td style="font-weight:bold;background-color:silver;text-align:center">Claim Number</td>
         <td style="font-weight:bold;background-color:silver;text-align:center">Date of Loss</td>
         <td style="font-weight:bold;background-color:silver;text-align:center">Insured Name</td>
      </tr>
  </table>
   <div style="height:110px;overflow:auto;border:0px;border-bottom:1px solid silver;margin:0px;margin-bottom:5px;">
   <table id="tblSearchResult" border="1" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed" datasrc="#xmlSearchResult">
   	<colspan>
   		<col width="75px"/>
   		<col width="120px"/>
   		<col width="100px"/>
   		<col width="240px"/>
   	</colspan>
      <tr onclick="selectRecord(this)" ondblclick="selectRecord(this);getLynxIDDetails();">
         <td><div class="data" datafld="lynxid"/></td>
         <td><div class="data" datafld="claimnumber"/></td>
         <td><div class="data" datafld="dateofloss"/></td>
         <td><div class="data" datafld="insuredname"/></td>
      </tr>
   </table>
   <div id="divNoRecords" style="height:60px;padding:15px;color:#FF0000;text-align:center;display:none;font-size:11px;font-weight:bold;width:100%">No LYNX FNOL Records match your search criteria.</div>
   </div>

   <table border="0" cellpadding="2" cellspacing="0" style="width:100%">
      <tr>
         <td>
            <button id="btnSelectLynxID" onclick="getLynxIDDetails()" disabled="true">Next</button>&nbsp;
            <button onclick="doClose()">Cancel</button>&nbsp;&nbsp;
         </td>
         <td style="text-align:right">
            <button onclick="newSearch()">New Search</button>   
         </td>
      </tr>
   </table>
</div>
<div id="divVehicles" style="display:none">
   <div style="font-size:14px">Select a vehicle:</div>
   <table border="1" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed">
   	<colspan>
    		<col width="50px"/>
   		<col width="175px"/>
   		<col width="100px"/>
   		<col width="100px"/>
   		<col width="100px"/>
   	</colspan>
      <tr>
         <td style="font-weight:bold;background-color:silver;text-align:center">Party</td>
         <td style="font-weight:bold;background-color:silver;text-align:center">Owner</td>
         <td style="font-weight:bold;background-color:silver;text-align:center">Year</td>
         <td style="font-weight:bold;background-color:silver;text-align:center">Make</td>
         <td style="font-weight:bold;background-color:silver;text-align:center">Model</td>
      </tr>
  </table>
   <div style="height:110px;overflow:auto;border:0px;border-bottom:1px solid silver;margin:0px;margin-bottom:5px;">
   <table id="tblVehicleList" border="1" cellpadding="2" cellspacing="0" style="border-collapse:collapse;table-layout:fixed" datasrc="#xmlVehicles">
   	<colspan>
    		<col width="0px" style="display:none"/>
         <col width="50px"/>
   		<col width="175px"/>
   		<col width="100px"/>
   		<col width="100px"/>
   		<col width="100px"/>
   	</colspan>
      <tr onclick="selectVehRecord(this)" ondblclick="selectVehRecord(this); selectVehicle();">
         <td><div class="data" datafld="id"/></td>
         <td><div class="data" datafld="party"/></td>
         <td><div class="data" datafld="Owner"/></td>
         <td><div class="data" datafld="Year"/></td>
         <td><div class="data" datafld="Make"/></td>
         <td><div class="data" datafld="Model"/></td>
      </tr>
   </table>
   <div id="divNoVehRecords" style="height:60px;padding:15px;color:#FF0000;text-align:center;display:none;font-size:11px;font-weight:bold;width:100%">No vehicles found.</div>
   </div>

   <table border="0" cellpadding="2" cellspacing="0" style="width:100%">
      <tr>
         <td>
            <button id="btnSelectVehicle" onclick="selectVehicle()" disabled="true">Select</button>&nbsp;
            <button onclick="doClose()">Cancel</button>&nbsp;&nbsp;
         </td>
         <td style="text-align:right">
            <button id="btnBack" onclick="showSearchData()">Go Back</button>&nbsp;
            <button onclick="newSearch()">New Search</button>   
         </td>
      </tr>
   </table>
</div>
<form name="frmFNOLGetClaim" action="frmFNOLGetClaimDetails.asp?officeID=<%=liOfficeID %>" method="post" target="frmSearch">
  <input id="txtGetLynxID" name="txtGetLynxID" type="hidden" value=""/>
 </form>
<iframe ID="CalFrame" NAME="CalFrame" SRC="blank.asp" STYLE="border:2px solid #404040; left:-500px; top:0px; display:none; position:absolute; width:170; height:186; z-index:100; filter: progid:DXImageTransform.Microsoft.Shadow(color='#666666', Direction=135, Strength=3);" MARGINHEIGHT=0 MARGINWIDTH=0 NORESIZE FRAMEBORDER=0 SCROLLING=NO></iframe>

<iframe name="frmSearch" id="frmSearch" style="height:1px;width:3px;visibility:hidden" src="blank.asp">
</iframe>

<xml id="xmlSearchResult"/>
<xml id="xmlFNOLClaimData"/>
<xml id="xmlVehicles"><Root/></xml>

<div id="divProgress" style="position:absolute;top:10px; left:10px;width:200px;height:75px;background-color:#FFFACD;padding:5px;border:3px double #C0C0C0;visibility:hidden">
   <table border="0" cellpadding="2" cellspacing="0" style="width:100%;height:100%;border:0px solid #c0c0c0;border-collapse:collapse">
      <tr>
         <td style="text-align:center">Please wait...</td>
      </tr>
      <tr>
         <td style="text-align:center">
            <img src="images/loading_anim.gif"/>
         </td>
      </tr>
   </table>
</div>
</body>
</html>

<%
  Set oNode = Nothing
  Set ElemList = Nothing
  Set loXML = Nothing
%>
