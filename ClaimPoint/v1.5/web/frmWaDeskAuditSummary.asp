<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetCurrAssignment.asp"-->
<!-- v1.4.5.0 -->

<%
  lsPageName = "frmWaDeskAuditSummary.asp"
  lsPageTitle = lsCurrentAssignmentDesc & " Assignment - Summary"
  liPageType = lcAssignmentPage
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<html>
<head>
<title><%=lsCurrentAssignmentDesc%> - Summary</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "";
  gsVehNum = "";
  gsVehCount = "<%=Request("vehCount")%>";
  
  function pageInit()
  {
     if(parent)
      parent.resetTimeOut();
  
    var lsCurrAssignment = "<%=lsCurrentAssignmentDesc%>";
    var lsCurrHelpURL;

    if (lsCurrAssignment == "Adverse Subro Desk Audit")
      lsCurrHelpURL = "CP_Adverse_Subro_Desk_Audit.htm";
    else if (lsCurrAssignment == "Demand Estimate Audit")
      lsCurrHelpURL = "CP_Demand_Estimate_Audit.htm";
    else if (lsCurrAssignment == "IA Estimate Audit")
      lsCurrHelpURL = "CP_IA_Estimate_Audit.htm";

    if (parent)
      parent.gsHelpSubTopicPage = lsCurrHelpURL;
    
     if(parent)
      parent.getClaimSummaryData();
  }
  
  
  function submitClaim()
  {
    if(parent)
    {
      var lsWasSubmitted = parent.chkClaimSubmitted();
      if (lsWasSubmitted == "false")
      {
        parent.document.getElementById("uiFrame").src = "frmWaSubmitAssignment.asp?fromPage=DeskAuditSummary";
      }
      else
      {
        alert("Unable to submit claim. This desk audit has already been submitted successfully.");
        return;
      }
    }
    else
      return false;
  }
  
  
  function editDeskAudit()
  {
    if(parent)
    {
      dspMsg();
      parent.document.getElementById("uiFrame").src = "frmWaDeskAudit.asp?fromPage=ClaimSummary";
    }
    else
      return false;
  }
  
  function dspMsg()
  {
    var objIFrame = parent.document.getElementById('uiFrame');
    objIFrame.style.height = "0";
    parent.showMsg();
  }
  
  function goCancel()
  {
    if (parent)
    {
      var lbConfirmCancel = window.confirm("Stop entering this assignment and return?");
      if (lbConfirmCancel)
      {
        top.location = "frmMyClaimsDesktop.asp";
      }
    }
  }


</script>

</head>
<body onload="resizeWaIframe(gsVehCount); pageInit(); parent.hideMsg();">

<div id="divContainer">

  <form name="frmSummaryInfo">

    <table id="tblClaimInfoBlock" width="600" border="0" cellpadding="0" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse;">
      <tr>
        <td>

          <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; border-bottom: 1px solid #000099;">
            <tr bgcolor="#E5E5F5"">
              <td style="color: #000099; font-size: 10pt; font-weight: bold;">
                &nbsp;Claim Information
              </td>
              <td align="right">
              <a href="javascript:editDeskAudit()"
                 title="Edit desk audit information"
                 onMouseOver="window.status='Edit desk audit information'; return true"
                 onMouseOut="window.status=''; return true">
                 <img border="0" src="images/btn_edit_blue.gif" WIDTH="83" HEIGHT="31">
               </a>
              </td>
            </tr>
          </table>

        </td>
      </tr>
      <tr>
        <td valign="top" style="padding: 0px 10px 10px 10px;">

          <table name="DeskAuditInfoTable" id="DeskAuditInfoTable" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;">
            <colgroup>
              <col width="155px"/>
              <col width="135px"/>
              <col width="135px"/>
              <col width="135px"/>
            </colgroup>
            <tr>
              <td nowrap class="legendSumm">Claim Assigned by:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap class="legendSumm">Relation to Insured:</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap colspan="2">
                <input name="CallerNameFirst" id="txtCallerNameFirst" type="text" class="inputView" value="" size="16" readonly="true">
                <input name="CallerNameLast" id="txtCallerNameLast" type="text" class="inputView" value="" size="21" readonly="true">
              </td>
              <td nowrap colspan="2">
                <input name="CallerRelationToInsuredIDDescription" id="txtCallerRelationToInsuredIDDescription" type="text" class="inputView" value="" size="22" readonly="true">
              </td>
            </tr>
            <tr>
              <td class="legendSumm">Claim Number:</td>
              <td class="legendSumm">Loss Date:</td>
              <td class="legendSumm">Loss State:</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="CoverageClaimNumber" id="txtCoverageClaimNumber" type="text" class="inputView" value="" size="20" readonly="true">
              </td>
              <td nowrap>
                <input name="LossDate" id="txtLossDate" type="text" class="inputView" value="" size="12" readonly="true">
              </td>
              <td nowrap>
                <input name="LossAddressState" id="txtLossAddressState" type="text" class="inputView" value="" size="4" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
        <% If liDisablePolicyNumber = 0 Then %>
            <tr>
              <td nowrap class="legend" colspan="5" >Policy Number:</td>
            </tr>
            <tr>
               <td colspan="5" nowrap>
                  <input name="PolicyNumber" id="txtPolicyNumber" type="text" class="inputView"  value="" size="15" readonly="true">
               </td>
            </tr>
        <% End If %>
            <tr>
              <td nowrap class="legendSumm">Insured Name:</td>
              <td nowrap>&nbsp;</td>
              <td colspan="2" nowrap class="legendSumm"><% If liDisableAmicaFields = 0 Then %>Insured Business Name:<% End If %></td>
            </tr>
            <tr>
              <td nowrap colspan="2">
                <input name="InsuredNameFirst" id="txtInsuredNameFirst" type="text" class="inputView" value="" size="16" readonly="true">
                <input name="InsuredNameLast" id="txtInsuredNameLast" type="text" class="inputView" value="" size="21" readonly="true">
              </td>
              <td nowrap colspan="2">
              <% If liDisableAmicaFields = 0 Then %>
                <input name="InsuredBusinessName" id="txtInsuredBusinessName" type="text" class="inputView" value="" size="50" readonly="true">
              <% End If %>
              </td>
            </tr>
         <% if liDisableInsuredAddress <> "1" then %>
            <tr>
               <td colspan="4" class="legendSumm">Insured Address:</td>
            </tr>
            <tr>
               <td nowrap colspan="2">
                <input name="InsuredAddress1" id="txtInsuredAddress1" type="text" class="inputView" value="" size="44" readonly="true">
               </td>
               <td nowrap colspan="2">
                <input name="InsuredAddress2" id="txtInsuredAddress2" type="text" class="inputView" value="" size="50" readonly="true">
               </td>
            </tr>
            <tr>
               <td class="legendSumm">Zip:</td>
               <td class="legendSumm" colspan="2">City:</td>
               <td class="legendSumm">State:</td>
            </tr>
            <tr>
               <td nowrap>
                <input name="InsuredAddressZip" id="txtInsuredAddressZip" type="text" class="inputView" value="" size="5" readonly="true">
               </td>
               <td nowrap colspan="2">
                <input name="InsuredAddressCity" id="txtInsuredAddressCity" type="text" class="inputView" value="" size="20" readonly="true">
               </td>
               <td nowrap>
                <input name="InsuredAddressState" id="txtInsuredAddressState" type="text" class="inputView" value="" size="30" readonly="true">
               </td>
            </tr>
         <% end if %>

<%
  If liCurrAssignmentEmergencyLiability = 1 Then
%>

            <tr>
              <td colspan="4" class="legendSumm">

                <table width="100%" border="0" cellpadding="2" cellspacing="0">
                  <tr valign="top">
                    <td class="legendnopad">Liability:</td>
                    <td id="LiabilityText"></td>
                    <td nowrap>
                      <input name="LiabilityFlag" id="txtLiabilityFlag" type="text" class="inputView" value="" size="2" readonly="true">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td class="legendnopad">Emergency:</td>
                    <td id="EmergencyText"></td>
                    <td nowrap>
                      <input name="EmergencyFlag" id="txtEmergencyFlag" type="text" class="inputView" value="" size="2" readonly="true">
                    </td>
                  </tr>
                </table>

              </td>
            </tr>
<%
  End If
%>

            <tr>
              <td nowrap class="legendSumm">Party:</td>
              <td nowrap class="legendSumm">Coverage:</td>
              <td nowrap class="legendSumm">Deductible:</td>
              <td nowrap class="legendSumm"></td>
            </tr>
            <tr>
              <td nowrap>
                <input name="ExposureCD" id="txtExposureCD" type="text" class="inputView" value="" size="5" readonly="true">
              </td>
              <td nowrap>
                <input name="CoverageProfileUiCD" id="txtClientCoverageTypeDesc" type="text" class="inputView" value="" size="13" readonly="true">
              </td>
              <td nowrap>
                $<input name="DeductibleAmt" id="txtDeductibleAmt" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>
              </td>
            </tr>

<%
  If liCurrAssignmentReserveLineType = 1 Then
%>


            <tr>
              <td nowrap class="legendSumm">Line Type:</td>
              <td nowrap class="legendSumm">Reseve Number:</td>
              <td nowrap class="legendSumm"></td>
              <td nowrap class="legendSumm"></td>
            </tr>
            <tr>
              <td nowrap>
                <input id="txtLineType" type="text" class="inputView" value="" size="3" readonly="true">
              </td>
              <td nowrap>
                <input id="txtReserve" type="text" class="inputView" value="" size="3" readonly="true">
              </td>
              <td nowrap></td>
              <td nowrap></td>
            </tr>

<%
  End If
%>
            <tr>
              <td nowrap colspan="4" class="legendSumm">Description of Loss:</td>
            </tr>
            <tr>
              <td nowrap colspan="4">
                <textarea name="LossDescription" id="txtLossDescription" wrap="physical" class="inputView" readonly="true" style="width:100%; height:50px;"></textarea>
              </td>
            </tr>
            <tr>
              <td colspan="2" class="legendSumm">Owner Name:</td>
              <td colspan="2" class="legendSumm">Owner Business Name:</td>
            </tr>
            <tr>
              <td nowrap colspan="2">
                <input name="OwnerNameFirst" id="txtOwnerNameFirst" type="text" class="inputView" value="" size="16" readonly="true">
                <input name="OwnerNameLast" id="txtOwnerNameLast" type="text" class="inputView" value="" size="21" readonly="true">
              </td>
              <td nowrap colspan="2">
                <input name="OwnerBusinessName" id="txtOwnerBusinessName" type="text" class="inputView" value="" size="50" readonly="true">
              </td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Vehicle Year:</td>
              <td nowrap class="legendSumm">Vehicle Make:</td>
              <td nowrap class="legendSumm">Vehicle Model:</td>
              <td nowrap class="legendSumm"></td>
            </tr>
            <tr>
              <td nowrap>
                <input name="VehicleYear" id="txtVehicleYear" type="text" class="inputView" value="" size="3" readonly="true">
              </td>
              <td nowrap>
                <input name="Make" id="txtMake" type="text" class="inputView" value="" size="12" readonly="true">
              </td>
              <td nowrap>
                <input name="Model" id="txtModel" type="text" class="inputView" value="" size="16" readonly="true">
              </td>
              <td nowrap>
              </td>
            </tr>

      <% If liDisableAmicaFields = 0 Then %>
            <tr>
              <td nowrap class="legendSumm">Vehicle Vin:</td>
              <td nowrap class="legendSumm">Odometer:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="VIN" id="txtVIN" type="text" class="inputView" value="" size="18" readonly="true">
              </td>
              <td nowrap>
                <input name="Mileage" id="txtMileage" type="text" class="inputView" value="" size="10" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
      <% End If %>

            <tr>
              <td nowrap class="legendSumm">Primary Damage:</td>
              <td colspan="3" nowrap class="legendSumm">Secondary Damages:</td>
            </tr>
            <tr>
              <td nowrap valign="top">
                <TEXTAREA cols="" rows="" wrap="physical" class="inputView" id="txtPrimaryDamageDescription" readonly="true" style="width:95%;height:35px;"></TEXTAREA>
              </td>
              <td colspan="3" nowrap>
                <TEXTAREA cols="78" rows="4" wrap="physical" class="inputView" id="txtSecondaryDamageDescription" readonly="true" style="width:100%;height:50px;"></TEXTAREA>
              </td>
            </tr>
            <tr>
              <td nowrap colspan="4" class="legendSumm"> Special Intructions/Additional Damage Information:</td>
            </tr>
            <tr>
              <td nowrap colspan="4">
                <textarea name="Remarks" id="txtRemarks" cols="110" rows="4" wrap="physical" class="inputView" readonly="true" style="width:100%; height:50px;"></textarea>
              </td>
            </tr>
            <tr>
              <td nowrap colspan="4" class="legendSumm"> Repair Location Information:</td>
            </tr>
            <%if liInsuranceCompanyID = 176 then %>
            <tr>
               <td class="legendSumm" colspan="4">Shop Name:</td>
            </tr>
            <tr>
               <td colspan="4">
                  <input name="RepairShopName" id="txtRepairShopName" type="text" class="inputView" value="" size="100" readonly="true">
               </td>
            </tr>
            <tr>
               <td class="legendnopad" colspan="4">Address:</td>
            </tr>
            <tr>
               <td colspan="4">
                  <input name="RepairShopAddress" id="txtRepairShopAddress" type="text" class="inputView" value="" size="100" readonly="true">
               </td>
            </tr>
            <%end if %>
            <tr>
               <td nowrap class="legendSumm">Zip Code:</td>
               <td nowrap class="legendSumm">City:</td>
               <td nowrap class="legendSumm">State:</td>
               <td nowrap class="legendSumm">County:</td>
            </tr>
            <tr>
               <td><input name="RepairZipCode" id="txtRepairZipCode" type="text" class="inputView" value="" size="18" readonly="true"></td>
               <td><input name="RepairLocationCity" id="txtRepairLocationCity" type="text" class="inputView" value="" size="20" readonly="true"></td>
               <td><input name="RepairLocationState" id="txtRepairLocationState" type="text" class="inputView" value="" size="20" readonly="true"></td>
               <td><input name="RepairLocationCounty" id="txtRepairLocationCounty" type="text" class="inputView" value="" size="20" readonly="true"></td>
            </tr>
            <%if liInsuranceCompanyID = 176 then %>
            <tr>
               <td class="legendSumm" colspan="2">Phone:</td>
               <td class="legendSumm" colspan="2">Fax:</td>
            </tr>
            <tr>
               <td colspan="2">
                  <input name="RepairShopPhone" id="txtRepairShopPhone" type="text" class="inputView" value="" size="25" readonly="true">
               </td>
               <td colspan="2">
                  <input name="RepairShopFax" id="txtRepairShopFax" type="text" class="inputView" value="" size="25" readonly="true">
               </td>
            </tr>
            <tr>
               <td class="legendSumm" colspan="2">Email:</td>
               <td class="legendSumm" colspan="2">Inspection Date:</td>
            </tr>
            <tr>
               <td colspan="2">
                  <input name="RepairShopEmailAddress" id="txtRepairShopEmailAddress" type="text" class="inputView" value="" size="30" readonly="true">
               </td>
               <td colspan="2">
                  <input name="RepairInspectionDate" id="txtRepairInspectionDate" type="text" class="inputView" value="" size="30" readonly="true">
               </td>
            </tr>
            <%end if %>

        <% If liLaborRatesSetupFlag = 0 Then %>
            <tr>
              <td nowrap class="legendSumm">Body Labor:</td>
              <td nowrap class="legendSumm">Labor Tax:</td>
              <td nowrap class="legendSumm">Parts Discount:</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="BodyLabor" id="txtBodyLabor" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>
                <input name="LaborTax" id="txtLaborTax" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>
                <input name="PartsDiscount" id="txtPartsDiscount" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Refinish Labor:</td>
              <td nowrap class="legendSumm">Parts Tax:</td>
              <td nowrap class="legendSumm">Glass Discount:</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="RefinishLabor" id="txtRefinishLabor" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>
                <input name="PartsTax" id="txtPartsTax" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>
                <input name="GlassDiscount" id="txtGlassDiscount" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Materials Labor:</td>
              <td nowrap class="legendSumm">Materials Tax:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="MaterialsLabor" id="txtMaterialsLabor" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>
                <input name="MaterialsTax" id="txtMaterialsTax" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Frame Labor:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="FrameLabor" id="txtFrameLabor" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap class="legendSumm">Mechanical Labor:</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
            <tr>
              <td nowrap>
                <input name="MechanicalLabor" id="txtMechanicalLabor" type="text" class="inputView" value="" style="text-align:right" size="10" readonly="true">
              </td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
              <td nowrap>&nbsp;</td>
            </tr>
      <% End If %>

          </table>

        </td>
      </tr>
    </table>

    <table width="600px" id="lastTbl" border="0" cellpadding="0" cellspacing="0">
      <colgroup>
        <col width="400px"/>
        <col width="100px"/>
        <col width="90px"/>
      </colgroup>
      <tr>
        <td colspan="3" nowrap>
          <img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="10">
        </td>
      </tr>
      <tr>
        <td valign="middle" class="legendnopad" style="font-size:14px;">
          Submit Desk Audit to LYNX Services
        </td>
        <td valign="top" nowrap>
          <a href='javascript:goCancel()'
             title='Cancel this assignment and return to My Claims Desktop'
             onMouseOver="window.status='Cancel this assignment and return to My Claims Desktop'; return true"
             onMouseOut="window.status=''; return true">
            <img src='images/btn_cancel.gif' border='0' WIDTH='83' HEIGHT='31'>
          </a>
        </td>
        <td nowrap>
          <a href="javascript:submitClaim()"
             title="Submit Desk Audit to LYNX"
             onMouseOver="window.status='Submit Desk Audit to LYNX'; return true"
             onMouseOut="window.status=''; return true">
            <img border="0" src="images/btn_submit_green.gif" WIDTH="83" HEIGHT="31">
          </a>
        </td>
      </tr>
      <tr>
        <td colspan="3" class="legendnopad">
          <span style="color:#000000; font-weight:normal; font-size:10pt">
          Please review the information for accuracy
          and make any necessary changes by pressing the "EDIT" button. When you are done, please press the "SUBMIT" button
          to send the information to LYNX Services and receive a LYNX ID.
          </span>
        </td>
      </tr>
    </table>

  </form>

</div>

</body>
</html>
