<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incGetCurrAssignment.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!-- v1.5.0.2 -->
<%
 lsPageName = "frmWaChoiceShopSelectionAdv.asp"
  lsPageTitle = lsCurrentAssignmentDesc & " Assignment - Shop Search"
  liPageType = lcAssignmentPage

  If Not lsCurrentAssignmentDesc Is Nothing Then
   If lsCurrentAssignmentDesc = "Choice Shop" Then
     lsCurrentAssignmentDesc = "Choice Shop"
   Else 
     lsCurrentAssignmentDesc = "DRP/Choice"
   End If
  End If


%>
<!-- include to check for and display if necessary APD offline/outage messages -->
<!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->
<%
  Dim lsCity, lsCState, liZipCode, lsShopName, lsShopNameCity, lsShopState, lsShopTypeCD, liMaxShops, lsVehDesc, liVehNum, lsAddVehicle ,lsDistance
  Dim lsGUID, lsSearchType, lsShopSearchCall, lsFromPage, liAddVehFromProgShop, liRetToSummary, lsRetVal, lsShopSearchByCityStateCall
  Dim docXML, oNodes, oNode, lsRetCity, lsRetState, lsRetPrimaryZip, lsCityListHTML, liCitiesReturned, lbCitySelected, liCitySelectedZip
  Dim lsNewShopName, lsNewShopAddress1, lsNewShopAddress2, lsNewShopCity, lsNewShopState, lsNewShopZip, lsNewPhoneAreaCode, lsNewPhoneExchangeNumber, lsNewPhoneUnitNumber,lsNewShopSearchCall
 
 'Choice shop purpose added by glsd451
   lsShopName = Request("SearchShopName")

 'Combined Shop
 lsNewShopName = Request("NewShopName")
 lsNewShopAddress1 = Request("NewShopAddress1")
 lsNewShopAddress2 = Request("NewShopAddress2")
 lsNewShopCity = Request("NewShopCity")
 lsNewShopState = Request("NewShopState")
 lsNewShopZip = Request("NewShopZipcode")
 lsNewPhoneAreaCode = Request("NewShopPhoneAreaCode")
 lsNewPhoneExchangeNumber = Request("NewShopPhoneExchangeNumber")
 lsNewPhoneUnitNumber = Request("NewShopPhoneUnitNumber")

 'Choice shop purpose added by glsd451
   'lsShopName = Request("ShopName")
   lsDistance = Request("distance")
  liZipCode = Request("ZipCode")
  lsVehDesc = Request("vehDesc")
  lsSearchType = Request("searchType")

  lsShopTypeCD = Request.Cookies("CombinedType")
  lsGUID = Request.Cookies("CPSession")("guidArray0")
  lsFromPage = Request("fromPage")
  lbCitySelected = Request("CitySelected")
  liCitySelectedZip = Request("CitySelectedZip")
  lsAddVehicle = Request("AddVehicle")

  
  
%>
<%
  If lbCitySelected = "true" Then
    liCitiesReturned = 1
    liZipCode = liCitySelectedZip
  End If

  If lsFromPage = "frmClaimSearch" or lsFromPage = "frmAddVehicle" Then
    liVehNum = 1
    liAddVehFromProgShop = 1

  ElseIf lsFromPage = "ShopSelectionAdv" and lsSearchType = "CS" and lbCitySelected <> "true" Then
    liVehNum = Request("vehNum")
    liAddVehFromProgShop = Request("addVeh")

       Set docXML = Server.CreateObject("MSXML2.DOMDocument")
    docXML.Async = False
    docXML.LoadXML lsRetVal

   
  Else
    liVehNum = Request("vehNum")
    liAddVehFromProgShop = Request("addVeh")

  End If

  If lsFromPage = "ClaimSummary" Then
    liRetToSummary = 1
    liAddVehFromProgShop = 0

  ElseIf lsFromPage = "ShopSelectionAdv" Then
    liRetToSummary = Request("retToSummary")

  Else
    liRetToSummary = 0
  End If

 If lsDistance Is Nothing Then
   If lsDistance = "" Then
      lsDistance  = 50
   End if
 End if

  If lsFromPage = "ShopSelectionAdv" Then
    Response.Cookies("CPSession")("srhZipCode") = liZipCode
    Response.Cookies("CPSession")("srhDistance") = lsDistance
    
  Else
    liZipCode = Request.Cookies("CPSession")("srhZipCode")
    lsDistance = Request.Cookies("CPSession")("srhDistance")
  End If

  If lsDistance Is Nothing Then
    If lsDistance = "" Then
      lsDistance  = 50
     End if
  End if

  'lsDistance  = 50
  'Response.write lsDistance
  'Response.End
  


%>
<!--#include file="includes/incSecurityCheck.asp"-->
<html>
<head>
    <title><%=lsCurrentAssignmentDesc%> - Shop Search</title>
    <!--<title>DRP/Choice - Shop Search</title>-->
    <link href="includes/apd_style.css" type="text/css" rel="stylesheet">
    <link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">
    <script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
    <script language="JavaScript" src="includes/incWaDatachk.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/JavaScript">
  
  var gsPrevPage = "<%=lsFromPage%>";
  var gsVehNum = "<%=liVehNum%>";
  var gsAddVeh = <%=liAddVehFromProgShop%>;
  var gsRetToSummary = "<%=liRetToSummary%>";
  var gsVehCount = "";
  var gsAddVehicle = "<%=lsAddVehicle%>";
  
  

  function pageInit()
  {
  
 
	if(parent)
         //parent.resetTimeOut();
  
    if (parent)
      parent.gsHelpSubTopicPage = "CP_Program_Shop_Assignment.htm";
    
    var liVehNum = gsVehNum;
    //if (gsPrevPage == "ClaimSummary")
      //document.getElementById("txtSrhShopCState").value = "<%= lsShopState %>";
   document.getElementById("txtShopState").value = "<%= lsNewShopState %>";
    
    //setActiveSearch();
  }
  
  
//  function setActiveSearch()
//  {
//    var lsSearchType;
//    var lcolElmsRD = document.getElementsByName("searchType");
//    var liRdLength = lcolElmsRD.length;
//    for (var i=0; i<liRdLength; i++)
//    {
//      if (lcolElmsRD.item(i).checked == true)
//      {
//        lsSearchType = lcolElmsRD.item(i).value;

//      
//        if (lsSearchType == "ZC")
//        {
//          document.getElementById("txtSrhShopZipCode").disabled = false;
//          document.getElementById("txtSrhShopName").disabled = false;

//        }
//       
//        return;
//      }
//    }
//  }

  function setSelectedShop(cbObj)
  {
    var lcolElmsCB = document.getElementsByName("cb_SelectShop");
    var liCBLength = lcolElmsCB.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i) == cbObj)
        lcolElmsCB.item(i).checked = true;
      else
        lcolElmsCB.item(i).checked = false;
    }
  }
  
  
  function selectShop()
  {
    var laryShopInfo;
    var liSelectedChoiceShopUID;
    var liShopLocationID;
    var liShopSource;
    var liShopSearchLogID;
    var liSelectedShopRank;
    var liSelectedShopScore;
    
    var lsShopChecked = "false";
    var lcolElmsCB = document.getElementsByName("cb_SelectShop");
    var liCBLength = lcolElmsCB.length;
    var newshop=false;
    
    
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i).checked == true)
      {
        lsShopChecked = "true";
        laryShopInfo = lcolElmsCB.item(i).value.split("|");

        liShopSource = laryShopInfo[0];
        if(liShopSource.toUpperCase() == "HQ")
        {
           liSelectedShopLocationID = getChoiceshoplocationid(laryShopInfo,newshop);
           //alert("liSelectedShopLocationID : " + liSelectedShopLocationID);
           if(liSelectedShopLocationID == undefined)
           {
              alert("Invalid shop so please contact the lynx administrator");
              window.location.reload();
           }
          liSelectedChoiceShopUID = laryShopInfo[1];
        }
        else if(liShopSource.toUpperCase() == "HQNEW")
        {
          liSelectedShopLocationID =  laryShopInfo[1];
          if(liSelectedShopLocationID == undefined)
           {
              alert("Invalid shop so please contact the lynx administrator");
              window.location.reload();
           }
           liSelectedChoiceShopUID = liSelectedShopLocationID;
        }
        else
        {
            liShopLocationID = laryShopInfo[1];
            liShopSearchLogID = laryShopInfo[2];
            liSelectedShopRank = laryShopInfo[3];
            liSelectedShopScore = laryShopInfo[4];
            lsShopChecked = "true";
        }
      }
    }
  
    if (lsShopChecked == "false")
    {
      alert("Please selected a shop before continuing.");
      return;
    }
    else
    {
      var lsShopComments = document.getElementById("txtShopRemarks").value;
      var liTxtAreaLength = lsShopComments.length;
      if (liTxtAreaLength > 250)
      {
        alert("* Only 250 characters are allowed for Comments to Repair Shop.\n" +
              "    You have typed "+ liTxtAreaLength +" characters.");
        return;
      }
   
      if(parent && gsAddVeh == 1)
      {
     
  //   alert(parent.getLastVehNum());
        var lastVehNum = parent.getLastVehNum();
         
        if (lastVehNum == 0){
          parent.createCSNewVeh(liShopSource);
         }
      }
      if(parent)
      {
      if(liShopSource.toUpperCase() == "LYNX")
         parent.updProgramShopSelection(gsVehNum, liShopLocationID, lsShopComments, gsRetToSummary, liShopSearchLogID, liSelectedShopRank, liSelectedShopScore);   
      else
         parent.updChoiceShopSelection(gsVehNum, liSelectedChoiceShopUID, lsShopComments,liSelectedShopLocationID, gsRetToSummary);
        }
      else
      {
        return false;
        }
    }
  }
  function SaveChoiceShop(newShopResult)
  {
  // alert("Entered : SaveChoiceShop");
    var ShopName,Address1,Address2,city,state,Zipcode,FedTaxID,Email,PhoneArea,PhoneExchange,PhoneUnit,FaxArea,FaxExchange,FaxUnit,Reason;
    var ShopSource = "HQ";
    var liArrNewShopResult = newShopResult.split("|");
//    ShopName  = document.getElementById("txtShopName").value
//    Address1 =  document.getElementById("txtShopAddress1").value
//    Address2 =  document.getElementById("txtShopAddress2").value
//    city =  document.getElementById("txtShopCity").value
//    state = "<%= lsNewShopState %>"; 
//    //state = document.getElementById("txtShopState").value
//    Zipcode =  document.getElementById("txtShopZipcode").value
//    FedTaxID =  document.getElementById("txtShopFedTaxId").value 
//    Email =  document.getElementById("txtShopEmailAddress").value
//    PhoneArea =  document.getElementById("txtShopPhoneAreaCode").value
//    PhoneExchange =  document.getElementById("txtShopPhoneExchangeNumber").value
//    PhoneUnit =  document.getElementById("txtShopPhoneUnitNumber").value
//    FaxArea =  document.getElementById("txtShopFaxAreaCode").value
//    FaxExchange =  document.getElementById("txtShopFaxExchangeNumber").value
//    FaxUnit =  document.getElementById("txtShopFaxUnitNumber").value

    var laryShopInfo = new Array();


    ShopName  = liArrNewShopResult[0];
    Address1 =  liArrNewShopResult[1];
    Address2 =  liArrNewShopResult[2];
    city =  liArrNewShopResult[3];;
    state = liArrNewShopResult[4]; 
    //state = document.getElementById("txtShopState").value
    Zipcode =  liArrNewShopResult[5];
    FedTaxID =  document.getElementById("txtShopFedTaxId").value;
    Email =  document.getElementById("txtShopEmailAddress").value;
    PhoneArea = liArrNewShopResult[6];
    PhoneExchange =  liArrNewShopResult[7];
    PhoneUnit =  liArrNewShopResult[8];
    FaxArea =  document.getElementById("txtShopFaxAreaCode").value;
    FaxExchange =  document.getElementById("txtShopFaxExchangeNumber").value;
    FaxUnit =  document.getElementById("txtShopFaxUnitNumber").value;

    var RsMsg = "";
    var RsReq = "";
    var RqZipcode = document.getElementById("txtShopZipcode").value;
    var RqAddress = document.getElementById("txtShopAddress1").value;
    var RqCity = document.getElementById("txtShopCity").value;
    var RqState = "<%= lsNewShopState %>";

    if(Zipcode.toUpperCase() != RqZipcode.toUpperCase())
      RsReq += "    " + "Zipcode : " + RqZipcode +" to "+ Zipcode +"\n";

    if(Address1.toUpperCase() != RqAddress.toUpperCase())
      RsReq += "    " + "Address1 : " + RqAddress +" to "+ Address1 +"\n";

    if(city.toUpperCase() != RqCity.toUpperCase())
      RsReq += "    " + "City : " + RqCity +" to "+ city +"\n";

     if(state.toUpperCase() != RqState.toUpperCase())
      RsReq += "    " + "State : " + RqState +" to "+ state +"\n";

      
       if (RsReq.length > 0) {
        RsMsg += "* The following fields are mismatched from Address Standardized Service, so these fileds will be changed as per Address Standardized Service:\n";
        RsMsg += RsReq;
        RsMsg  = RsMsg + "Do you want to proceed?";

        MismatchedNewShop();
        var strConfirm = confirm(RsMsg);

            if(!strConfirm){
               MismatchedNewShop();
                return;
              }
        }
        
      



    
  //  alert("ShopName : " + ShopName);
  //  alert("Address1 : " + Address1);
  //  alert("Address2 : " + Address2);
  //  alert("city : " + city);
  //  alert("state : " + state);
  //  alert("Zipcode : " + Zipcode);
 //   alert("PhoneArea : " + PhoneArea);
  //  alert("PhoneExchange : " + PhoneExchange);
   // alert("PhoneUnit : " + PhoneUnit);
    
    var ShopUID,ShopLicenceNo,ShopLink,ShopPartnerID,ShopContactFirstName,ShopContactLastName,ShopDistance,PhoneNumber,FaxNumber;
    //PhoneNumber = PhoneArea.concat(PhoneExchange,PhoneUnit);
    PhoneNumber = PhoneArea  + PhoneExchange + PhoneUnit;
    FaxNumber = FaxArea + FaxExchange + FaxUnit;
    ShopUID = "";
    ShopLicenceNo = ""
    ShopLink = "";
    ShopPartnerID = "";
    ShopContactFirstName = ""
    ShopContactLastName = "";
    ShopDistance = "";
    var laryShopInfo = new Array();
    laryShopInfo = [ShopUID,ShopName,Address1,Address2,city,state,Zipcode,PhoneNumber,FaxNumber,Email,FedTaxID,ShopLicenceNo,ShopLink,ShopPartnerID,ShopContactFirstName,ShopContactLastName,ShopDistance];
   
   
    var lsMsg = "";
    var lsReq = ""; 
    if (myTrim(ShopName) == "")
        lsReq += "    " + "ShopName" + "\n";
    if (myTrim(Address1) == "")
        lsReq += "    " + "Address1" + "\n";  
    if (myTrim(city) == "")
        lsReq += "    " + "City" + "\n";
    if (myTrim(state) == "")
        lsReq += "    " + "State" + "\n";
    if (myTrim(Zipcode) == "")
        lsReq += "    " + "Zipcode" + "\n";
    if (Zipcode != "" && Zipcode.length != 5)
        lsReq += "    " + "Zipcode is Invalid" + "\n" + "    " + "Zipcode must be be a formated as: nnnnn " + "\n";
    if (myTrim(PhoneNumber) == "")
        lsReq += "    " + "PhoneNumber" + "\n";
    if (PhoneNumber != "" && PhoneNumber.length != 10)
        lsReq += "    " + "Phone Number is invalid" + "\n" + "    " + "Phone Number must be a formated as: 999 999 9999" + "\n";
   if (lsReq.length > 0) {
        lsMsg += "* The following fields are required:\n";
        lsMsg += lsReq;
        alert(lsMsg);
    }
   
    if(myTrim(ShopName) != "" && myTrim(Address1) !="" && myTrim(city) !="" && myTrim(state) != "" && myTrim(Zipcode) != "" && myTrim(PhoneNumber) != "" && PhoneNumber.length == 10 && Zipcode.length == 5)
    {
    var newshop=true;
    var liSelectedShopLocationID = getChoiceshoplocationid(laryShopInfo,newshop);

    // Form reference:
   // var theForm = document.forms['NewShopInfoForm'];

    // Add data:
  /*  addHidden('ShopName', ShopName , 'txtShopName_'+liSelectedShopLocationID);
     addHidden( 'ShopAddress1', Address1 , 'txtShopAddress1_'+liSelectedShopLocationID);
      addHidden( 'ShopAddress1', Address2 , 'txtShopAddress2_'+liSelectedShopLocationID);
       addHidden( 'ShopCity', city , 'txtShopCity_'+liSelectedShopLocationID);
        addHidden( 'ShopState', state , 'txtShopState_'+liSelectedShopLocationID);
         addHidden( 'ShopZip', Zipcode , 'txtShopZip_'+liSelectedShopLocationID);
          addHidden( 'ShopPhone', PhoneNumber , 'txtShopPhone_'+liSelectedShopLocationID);
           addHidden('ShopFax', FaxNumber , 'txtShopFax_'+liSelectedShopLocationID);*/

    // Submit the form:
    //theForm.submit();

    
    var lsShopComments = document.getElementById("txtNewShopRemarks").value;
      var liTxtAreaLength = lsShopComments.length;
      if (liTxtAreaLength > 250)
      {
        alert("* Only 250 characters are allowed for Comments to Repair Shop.\n" +
              "    You have typed "+ liTxtAreaLength +" characters.");
        return;
      }
   
      if(parent && gsAddVeh == 1)
      {
     
  //   alert(parent.getLastVehNum());
        var lastVehNum = parent.getLastVehNum();
         
        if (lastVehNum == 0)
          parent.createCSNewVeh(ShopSource);
      }
      if(parent)
      {
        parent.updChoiceNewShopSelection(gsVehNum, lsShopComments,liSelectedShopLocationID, gsRetToSummary,laryShopInfo);
        }
      else
      {
        return false;
        }

        }
    else
    {
    document.getElementById("AddChoiceShop").style.display = "block";
    document.getElementById("MismatchedNewShopID").style.display = "block";
    parent.document.getElementById("uiFrame").style.height = "850px";
    document.getElementById("txtShopName").focus();
    //document.getElementById("txtShopName").focus();
    }
    
  }

  function addHidden(key, value,id) {
    // Create a hidden input element, and append it to the form:
    var input = document.createElement('input');
    input.type = 'hidden';
    input.name = key;
    input.id = id;
    input.value = value;
    //theForm.appendChild(input);
}

  function MismatchedNewShop(){
    document.getElementById("AddChoiceShop").style.display = "block";
    document.getElementById("MismatchedNewShopID").style.display = "block";
    parent.document.getElementById("uiFrame").style.height = "850px";
    document.getElementById("txtShopName").focus();
  }
  function LYNXDisableShop(){
    document.getElementById("AddChoiceShop").style.display = "block";
    document.getElementById("LYNXDisabledShopID").style.display = "block";
    parent.document.getElementById("uiFrame").style.height = "850px";
    document.getElementById("txtShopName").focus();
  }
  function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
    }
    function CheckSimilarMatchShop()
    { 
    var ShopName,Address1,Address2,city,state,Zipcode,FedTaxID,Email,PhoneArea,PhoneExchange,PhoneUnit,FaxArea,FaxExchange,FaxUnit,PhoneNumber;

    ShopName =  document.getElementById("txtShopName").value
    Address1 =  document.getElementById("txtShopAddress1").value
    Address2 =  document.getElementById("txtShopAddress2").value
    city =  document.getElementById("txtShopCity").value
    //state = "<%= lsNewShopState %>"; 
    state = document.getElementById("txtShopState").value
    Zipcode =  document.getElementById("txtShopZipcode").value
    FedTaxID =  document.getElementById("txtShopFedTaxId").value 
    Email =  document.getElementById("txtShopEmailAddress").value
    PhoneArea =  document.getElementById("txtShopPhoneAreaCode").value
    PhoneExchange =  document.getElementById("txtShopPhoneExchangeNumber").value
    PhoneUnit =  document.getElementById("txtShopPhoneUnitNumber").value
    FaxArea =  document.getElementById("txtShopFaxAreaCode").value
    FaxExchange =  document.getElementById("txtShopFaxExchangeNumber").value
    FaxUnit =  document.getElementById("txtShopFaxUnitNumber").value
    
    PhoneNumber = PhoneArea + PhoneExchange + PhoneUnit;
   
    var lsMsg = "";
    var lsReq = ""; 


    if (myTrim(ShopName) == "")
        lsReq += "    " + "ShopName" + "\n";
    if (myTrim(Address1) == "")
        lsReq += "    " + "Address1" + "\n";  
    if  (myTrim(city) == "")
        lsReq += "    " + "City" + "\n";
    if  (myTrim(state) == "")
        lsReq += "    " + "State" + "\n";
    if (myTrim(Zipcode) == "")
        lsReq += "    " + "Zipcode" + "\n";
    if (Zipcode != "" && Zipcode.length != 5)
        lsReq += "    " + "Zipcode is Invalid" + "\n" + "    " + "Zipcode must be be a formated as: nnnnn " + "\n";
    if (myTrim(PhoneNumber) == "")
        lsReq += "    " + "PhoneNumber" + "\n";
    if (PhoneNumber != "" && PhoneNumber.length != 10)
        lsReq += "    " + "Phone Number is invalid" + "\n" + "    " + "Phone Number must be a formated as: 999 999 9999" + "\n";
   if (lsReq.length > 0) {
        lsMsg += "* The following fields are required:\n";
        lsMsg += lsReq;
        alert(lsMsg);
    }
    if (myTrim(ShopName) != "" && myTrim(Address1) !="" && myTrim(city) !="" && myTrim(state) != "" && myTrim(Zipcode) != "" && myTrim(PhoneNumber) != "" && PhoneNumber.length == 10 && Zipcode.length == 5)
    {
         document.getElementById("animateGif").style.display = "inline";
         document.frmAddNewShop.submit();
    }
 }

  function getChoiceshoplocationid(laryShopInfo,isNewShopFlag)
  {
    try {
            var strPathWay, liSelectedShopName,liSelectedShopAddress1,liSelectedShopAddress2,liSelectedShopCity;
            var liSelectedShopstateCD,liSelectedZipCode,liSelectedShopPhoneNo,liSelectedShopFaxNo,liSelectedShopEmailAddress;
            var liSelectedShopFedralTaxID,liSelectedShopLicenceNo,liSelectedShopLink,liSelectedShopPartnerID,liSelectedShopContactFirstName;
            var liSelectedShopContactLastName,liSelectedShopDistance,liSelectedShopLocationID;
            var liFaxAreaCode,liFaxExchangeNumber,liFaxUnitNumber;
            var liPhoneAreaCode,liPhoneExchangeNumber,liPhoneUnitNumber;
              
          if(isNewShopFlag){
            liSelectedShopUID =  myTrim(laryShopInfo[0]);
            liSelectedShopName = myTrim(laryShopInfo[1]);
            liSelectedShopAddress1 = myTrim(laryShopInfo[2]);
            liSelectedShopAddress2= myTrim(laryShopInfo[3]);
            liSelectedShopCity = myTrim(laryShopInfo[4]);
            liSelectedShopstateCD = myTrim(laryShopInfo[5]);
            liSelectedZipCode = myTrim(laryShopInfo[6]);
            liSelectedShopPhoneNo = myTrim(laryShopInfo[7]);
            liSelectedShopFaxNo = myTrim(laryShopInfo[8]);
            liSelectedShopEmailAddress = myTrim(laryShopInfo[9]);
            liSelectedShopFedralTaxID = myTrim(laryShopInfo[10]);
            liSelectedShopLicenceNo = myTrim(laryShopInfo[11]);
            liSelectedShopLink = myTrim(laryShopInfo[12]);
            liSelectedShopPartnerID = myTrim(laryShopInfo[13]);
            liSelectedShopContactFirstName = myTrim(laryShopInfo[14]);
            liSelectedShopContactLastName = myTrim(laryShopInfo[15]);
            liSelectedShopDistance = myTrim(laryShopInfo[16]);
            }
            else{
            liSelectedShopUID =  myTrim(laryShopInfo[1]);
            liSelectedShopName = myTrim(laryShopInfo[2]);
            liSelectedShopAddress1 = myTrim(laryShopInfo[3]);
            liSelectedShopAddress2= myTrim(laryShopInfo[4]);
            liSelectedShopCity = myTrim(laryShopInfo[5]);
            liSelectedShopstateCD = myTrim(laryShopInfo[6]);
            liSelectedZipCode = myTrim(laryShopInfo[7]);
            liSelectedShopPhoneNo = myTrim(laryShopInfo[8]);
            liSelectedShopFaxNo = "";
            liSelectedShopEmailAddress = "";
            liSelectedShopFedralTaxID = myTrim(laryShopInfo[11]);
            liSelectedShopLicenceNo = "";
            liSelectedShopLink = "";
            liSelectedShopPartnerID = "";
            liSelectedShopContactFirstName = "";
            liSelectedShopContactLastName = "";
            liSelectedShopDistance = myTrim(laryShopInfo[17]);
            }
              
               //Spcial char replace 
               liSelectedShopName = liSelectedShopName.replace(/&/g, "&amp;");
               liSelectedShopName = liSelectedShopName.replace(/>/g, "&gt;");
               liSelectedShopName = liSelectedShopName.replace(/</g, "&lt;");
               liSelectedShopName = liSelectedShopName.replace(/"/g, "&quot;");
               

            if (liSelectedShopPhoneNo != "")
            {
                liSelectedShopPhoneNo = liSelectedShopPhoneNo.replace("+1", "");
                liSelectedShopPhoneNo = liSelectedShopPhoneNo.replace(/-/g , "");
 
                liPhoneAreaCode = liSelectedShopPhoneNo.substring(0, 3);
                liPhoneExchangeNumber = liSelectedShopPhoneNo.substring(3, 6);
                liPhoneUnitNumber = liSelectedShopPhoneNo.substring(6, 10);
            }
            else
            {
                liPhoneAreaCode = "";
                liPhoneExchangeNumber = "";
                liPhoneUnitNumber = "";
            }


               if (liSelectedShopFaxNo != "")
            {
                liSelectedShopFaxNo = liSelectedShopFaxNo.replace("+1", "");
                liSelectedShopFaxNo = liSelectedShopFaxNo.replace(/-/g , "");

                liFaxAreaCode = liSelectedShopFaxNo.substring(0, 3);
                liFaxExchangeNumber = liSelectedShopFaxNo.substring(3, 6);
                liFaxUnitNumber = liSelectedShopFaxNo.substring(6, 10);
            }
            else
            {
                liFaxAreaCode = "";
                liFaxExchangeNumber = "";
                liFaxUnitNumber = "";
            }
            
        
                 
            var dataToSend = "" +
                        "      <bNewShopFlag>" + isNewShopFlag + "</bNewShopFlag> " +
                        "      <sHQBusinessTypeCD>P</sHQBusinessTypeCD> " +
                        "      <sHQAddress1>" + liSelectedShopAddress1 + "</sHQAddress1> " +
                        "      <sHQAddress2>"+ liSelectedShopAddress2 +"</sHQAddress2> " +
                        "      <sHQAddressCity>"+ liSelectedShopCity +"</sHQAddressCity> " +
                        "      <sHQAddressState>"+ liSelectedShopstateCD +"</sHQAddressState> " +
                        "      <sHQAddressZip>"+ liSelectedZipCode +"</sHQAddressZip> " +
                        "      <sHQEmailAddress>"+ liSelectedShopEmailAddress +"</sHQEmailAddress> " +
                        "      <bHQEnabledFlag>true</bHQEnabledFlag> " +
                        "      <sHQFaxAreaCode>"+liFaxAreaCode+"</sHQFaxAreaCode> " +
                        "      <sHQFaxExchangeNumber>"+liFaxExchangeNumber+"</sHQFaxExchangeNumber> " +
                        "      <sHQFaxUnitNumber>"+liFaxUnitNumber+"</sHQFaxUnitNumber> " +
                        "      <sHQFedTaxId>"+liSelectedShopFedralTaxID+"</sHQFedTaxId> " +
                        "      <sHQName>"+ liSelectedShopName +"</sHQName> " +
                        "      <sHQPhoneAreaCode>"+ liPhoneAreaCode +"</sHQPhoneAreaCode> " +
                        "      <sHQPhoneExchangeNumber>"+liPhoneExchangeNumber+"</sHQPhoneExchangeNumber> " +
                        "      <sHQPhoneUnitNumber>"+liPhoneUnitNumber+"</sHQPhoneUnitNumber> " +
                        "      <iHQSysLastUserID>0</iHQSysLastUserID> " +
                        "      <sHQApplicationCD>APD</sHQApplicationCD> " +
                        "      <sHQShopUID>"+liSelectedShopUID+"</sHQShopUID> " +
                        "      <sHQShopPartnerID>"+liSelectedShopPartnerID+"</sHQShopPartnerID> "
           
            

            strPathWay = HttpPost(dataToSend)
            
            return strPathWay;


        } catch (e) {
            strPathWay= "<Root returnCode=\"1\"><error><user>An internal error has occured while saving the data.</user><advanced><![CDATA[" + escape(e.message) + "]]></advanced></error></Root>";
          
        }
  }

  function createCORSRequest(method, url) {
    try {
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
            xhr.open(method, url, true);
            xhr.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
           } else if (typeof XDomainRequest != "undefined") {
            xhr = new XDomainRequest();
            xhr.open(method, url);
        } else {
            xhr = null;
        }
        return xhr;
    } catch (e) {
        console.error(e);
    }
    
}



 function HttpPost(strDataToSend) {
    try {
    var dataToSend,strReturnvalue;
    //strReturnvalue = createCORSRequest("POST", "rspHQShoplocation.asp");
    var objXMLHTTP = GetXmlHttpObject();

   objXMLHTTP.open("POST", "rspHQShoplocation.asp", false);
   objXMLHTTP.setRequestHeader("Content-Type", "text/xml; charset=utf-8");
    objXMLHTTP.send(strDataToSend);
 
     strReturnvalue=objXMLHTTP.responseXML.getElementsByTagName('ChoiceShopProcessResult')[0].firstChild.nodeValue

     
     return strReturnvalue;
    }catch (e) {
        console.error(e);
    }
}

function GetXmlHttpObject(){
    var xmlHttp=null;
    try
    {
      // Firefox, Opera 8.0+, Safari
      xmlHttp=new XMLHttpRequest();
    }
    catch (e)
    {
      // Internet Explorer
        try
        {
            xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e)
        {
            xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return xmlHttp;
}
    
   function setSelectedCity(cbObj)
  {
    var lcolElmsCB = document.getElementsByName("cb_SelectCity");
    var liCBLength = lcolElmsCB.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i) == cbObj)
        lcolElmsCB.item(i).checked = true;
      else
        lcolElmsCB.item(i).checked = false;
    }
  }

  function selectCity()
  {

    var liZipCode = "";
    var lsCityChecked = "false";
    var lcolElmsCB = document.getElementsByName("cb_SelectCity");
    var liCBLength = lcolElmsCB.length;
    for (var i=0; i<liCBLength; i++)
    {
      if (lcolElmsCB.item(i).checked == true)
      {
        liZipCode = lcolElmsCB.item(i).value;
        lsCityChecked = "true";
      }
    }
  
    if (lsCityChecked == "false")
    {
      alert("Please selected a city before continuing.");
      return;
    }
    else
    {
      document.getElementById("txtCitySelected").value = "true";
      document.getElementById("txtCitySelectedZip").value = liZipCode;
      document.getElementById("animateGif2").style.display = "inline";
      document.frmShopSrh.submit();
    }
  }


  //Glsd451 works
  function AddChoiceShop(displayflag)
  {
   
   if(displayflag == 1)
      document.getElementById("ChoiceShopSelection").style.display = "none";

   document.getElementById("AddChoiceShop").style.display = "block";
   parent.document.getElementById("uiFrame").style.height = "850px";
   document.getElementById("txtShopName").focus();
   
   }
   function SaveCancel()
   {
    window.history.back();
   }
   var defaultText = "50";
    function WaterMark(txt, evt)
    {
        if(txt.value.length == 0 && evt.type == "blur")
        {
            txt.style.color = "gray";
            txt.value = defaultText;
        }
        if(txt.value == defaultText && evt.type == "focus")
        {
            txt.style.color = "black";
            txt.value="";
        }
    }
  function searchShop()
  {
    var lsSearchType;
    var lsReq = "";
    var lsMsg = "";
      
    var loSrhShopZipCode = document.getElementById("txtSrhShopZipCode");
    if (loSrhShopZipCode.value == "")
    lsReq +=  "    " + "Zip Code value is required for a Zip Code type of search." + "\n";
    else if (loSrhShopZipCode.value != ""  && loSrhShopZipCode.value.length !=5)
    lsReq +=  "    " + "The Zip Code field requires a 5-digit input." + "\n";
   
  
    if (lsReq.length > 0)
    {
      lsMsg += "* The following fields are required:\n";
      lsMsg += lsReq;
      alert(lsMsg);
    }
    else
    {
      document.getElementById("animateGif").style.display = "inline";
      document.frmShopSrh.submit();
      
    }
  }
 

  function chkEnterKey(event) 
  {
    if ( event.keyCode == 13 )
      searchShop();
  }
  
  
  function numbersOnly(obj,evt,decPntKey)
  {
    if (obj.value.length > obj.maxLength) return;
    var bOK = false;
    evt = (evt) ? evt : ((event) ? event : null);
    if (evt)
    {
      if ( evt.keyCode == 13 )
        searchShop();
      else
      {
        var charCode = (evt.charCode || evt.charCode == 0) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : evt.which);
        if (!(charCode > 13 && (charCode < 48 || charCode > 57))) bOK = true; // normal numbers
        if (decPntKey == 1 && charCode == 46) bOK = true; // allow decimals '.'
    
        if (bOK != true)
        {
          if (evt.returnValue) evt.returnValue = false;
          else if (evt.preventDefault) evt.preventDefault();
          else return false;
        }
      }
    }
  }
  
  
  function alphaOnly(obj,evt)
  {
    if (obj.value.length > obj.maxLength) return;
    var bOK = false;
    evt = (evt) ? evt : ((event) ? event : null);
    if (evt)
    {
      if ( evt.keyCode == 13 )
        searchShop();
      else
      {
        var charCode = (evt.charCode || evt.charCode == 0) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : evt.which);
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) bOK = true; // normal letters
    
        if (bOK != true)
        {
          if (evt.returnValue) evt.returnValue = false;
          else if (evt.preventDefault) evt.preventDefault();
          else return false;
        }
      }
    }
  }
  
  
  function goCancel()
  {
    if (parent)
    {
      if(gsRetToSummary == 1)
      { 
        gsVehCount = parent.getVehCount();
        if (parent.lsPageName && parent.lsPageName == "frmWaMainAddVehicle.asp")
          parent.document.getElementById("uiFrame").src = "frmWaClaimSummaryNewVeh.asp?fromPage=VehicleInfo&vehCount=" + gsVehCount;
        else
          parent.document.getElementById("uiFrame").src = "frmWaClaimSummary.asp?fromPage=ShopSelection&vehCount="+gsVehCount;
      }
      else
        top.location = "frmMyClaimsDesktop.asp";
    }
  }
  
    </script>
    <style type="text/css">
        .txtbox_shop
        {
            border: 1px solid #D8D8D8;
            padding: 2px;
            height: 20px;
            color: #000;
        }
        .shop_tbl td
        {
            color: #000099;
        }
        #txtdistance
        {
            color: Gray;
        }
    </style>
</head>
<body onload="resizeWaIframe(0); pageInit(); parent.hideMsg();">
    <div id="divToolTip">
    </div>
    <script type="text/javascript">


        Xoffset = -20;
        Yoffset = 20;

        var ttLayer;
        var yDist = -1000;

        var ns6 = document.getElementById && !document.all;
        var ie4 = document.all;

        if (ns6)
            ttLayer = document.getElementById("divToolTip").style;
        else if (ie4)
            ttLayer = document.all.divToolTip.style;

        ttLayer.visibility = "visible";
        ttLayer.display = "none";
        document.onmousemove = get_mouseXY;

        function popDiv(elID) {
            var msg = document.getElementById(elID).value;
            var content = "<TABLE WIDTH=300 BORDER=0 BORDERCOLOR=blue CELLPADDING=2 CELLSPACING=0 style='border:1px solid #000080; background-color:#E6E6FA;'>";
            content += "<TR><TD style='font-family:Arial,sans-serif;  font-weight:normal;  font-size:9pt;'>" + msg + "</TD></TR></TABLE>";
            yDist = Yoffset;

            if (ns6) {
                document.getElementById("divToolTip").innerHTML = content;
                ttLayer.display = '';
            }
            if (ie4) {
                document.all("divToolTip").innerHTML = content;
                ttLayer.display = '';
            }
        }

        function get_mouseXY(e) {
            var x = ns6 ? e.pageX : event.x + document.body.scrollLeft;
            ttLayer.left = x + Xoffset;
            var y = ns6 ? e.pageY : event.y + document.body.scrollTop;
            ttLayer.top = y + yDist;
        }

        function hideDiv() {
            yDist = -1000;
            ttLayer.display = "none"
        }


    </script>
    <div id="divContainer">
        <form name="frmShopSrh" method="post" action="frmWaChoiceShopSelectionAdv.asp">
        <input name="vehDesc" type="hidden" value="<%=lsVehDesc%>">
        <input name="vehNum" type="hidden" value="<%=liVehNum%>">
        <input name="fromPage" type="hidden" value="ShopSelectionAdv">
        <input name="addVeh" type="hidden" value="<%=liAddVehFromProgShop%>">
        <input name="CitySelected" id="txtCitySelected" type="hidden" value="false">
        <input name="CitySelectedZip" id="txtCitySelectedZip" type="hidden">
        <input name="retToSummary" type="hidden" value="<%=liRetToSummary%>">
        <%if lsAddVehicle <> "" then%>
        <input name="AddVehicle" id="txtAddVehicle" type="hidden" value="1">
        <%end if%>
        <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000099"
            style="border-collapse: collapse">
            <tr>
                <td style="font-size: 10pt">
                    <li><strong>Search by Distance</strong> will return results based on a radius from a
                        central location.<br>
                    </li>
                    <li><strong>Search by Shop Name</strong> results are based on a likely match of your
                        query.<br>
                    </li>
                </td>
            </tr>
        </table>
        <br>
        <table id="tblSearch" width="695" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
            <tr style="height: 20px;" bgcolor="#E5E5F5">
                <td colspan="5" style="border: 1px solid #000099; font-size: 10pt; font-weight: bold;">
                    &nbsp; <span style="color: #000099; font-size: 10pt; font-weight: bold;">Search by Distance</span>
                </td>
            </tr>
            <tr valign="middle" style="height: 30px;">
                <td style="border-left: 1px solid #000099; border-bottom: 1px solid #000099;">
                </td>
                <td nowrap style="font-size: 10pt; font-weight: bold;" width="13%" class="box" onclick="document.frmShopSrh.searchType[0].click();"
                    onmouseover="style.cursor='hand'; title='Select Zip Code search';" onmouseout="style.cursor='default';">
                    &nbsp; Zip Code: <span style="color: #000099; font-size: 11pt">*</span>
                </td>
                <td nowrap class="box">
                    <input name="ZipCode" id="txtSrhShopZipCode" type="text" onkeypress="return numbersOnly(this,event,0)"
                        value="<%=liZipCode%>" size="6" maxlength="5" autocomplete="OFF">
                </td>
                <td nowrap style="font-size: 10pt; font-weight: bold;" width="13%" class="box">
                    Shop Name:
                </td>
                <td nowrap class="box" style="border-right: 1px solid #000099;">
                    <input name="SearchShopName" id="txtSrhShopName" type="text" value="<%=lsShopName%>"
                        size="20" maxlength="40" autocomplete="OFF">
                </td>
            </tr>
            <tr valign="middle" style="height: 30px;">
                <td style="border-left: 1px solid #000099; border-bottom: 1px solid #000099;">
                </td>
                <td style="border-bottom: 1px solid #000099; font-size: 10pt; cursor: default; font-weight: bold;">
                    &nbsp; Distance:
                </td>
                <td style="border-bottom: 1px solid #000099;">
                    <input name="distance" id="txtdistance" type="text" onkeypress="return numbersOnly(this,event,0)"
                        value="<%=lsDistance%>" size="3" maxlength="5" autocomplete="OFF" onblur="WaterMark(this, event);"
                        onfocus="WaterMark(this, event);">
                </td>
                <td style="border-bottom: 1px solid #000099;">
                </td>
                <td style="border-bottom: 1px solid #000099; border-right: 1px solid #000099;">
                </td>
            </tr>
        </form>
        </table>
        <table width="695" border="0" cellpadding="0" cellspacing="0" style="align: right;">
            <tr>
                <td nowrap>
                    <p class="bodyBlue">
                        <span id="reqMark" style="display: none">* Required</span></p>
                </td>
                <td width="100%" align="center">
                    <span id="animateGif" style="font-size: 12px; font-weight: bold; color: #000080;
                        display: none;">Searching for Shops...&nbsp;&nbsp;&nbsp;
                        <img src="images/loading.gif" alt="" width="78" height="7" border="0">
                    </span>
                </td>
                <td align="right" valign="top" nowrap>
                    <img border="0" src="images/spacer.gif" width="10" height="1">
                </td>
                <td align="right" valign="top" nowrap>
                    <a href="javascript:goCancel()" title="Return to <% If liRetToSummary = 1 Then %> Summary <% Else %> My Claims Desktop <% End If %>"
                        onmouseover="window.status='Return to <% If liRetToSummary = 1 Then %> Summary <% Else %> My Claims Desktop <% End If %>'; return true"
                        onmouseout="window.status=''; return true">
                        <img src='images/btn_cancel.gif' border='0' width='83' height='31'>
                    </a>
                </td>
                <td align="right" valign="top" nowrap>
                    <img border="0" src="images/spacer.gif" width="10" height="1">
                </td>
                <td align="right" valign="top" nowrap>
                    <a href="javascript:searchShop()" title="Search for Shops" onmouseover="window.status='Search for Shops'; return true"
                        onmouseout="window.status=''; return true">
                        <img border="0" src="images/btn_search_green.gif" width="83" height="31">
                    </a>
                </td>
            </tr>
        </table>
        <br />
        <div id="AddChoiceShop" style="border-collapse: collapse; display: none;">
            <form name="frmAddNewShop" method="post" action="frmWaChoiceShopSelectionAdv.asp">
            <input name="vehDesc" type="hidden" value="<%=lsVehDesc%>">
            <input name="vehNum" type="hidden" value="<%=liVehNum%>">
            <input name="fromPage" type="hidden" value="ShopSelectionAdv">
            <input name="addVeh" type="hidden" value="<%=liAddVehFromProgShop%>">
            <input name="CitySelected" id="Hidden1" type="hidden" value="false">
            <input name="CitySelectedZip" id="Hidden2" type="hidden">
            <input name="retToSummary" type="hidden" value="<%=liRetToSummary%>">
            <div id="MismatchedNewShopID" style="border-collapse: collapse; display: none;">
            <table>
             <tr>
              <td valign="top">
                <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
              </td>
              <td width="100%" nowrap="" class="legendnopad" style="font-size: 10pt; color:#FF0000; font-weight:bold;">
                An Invalid address has been entered. Please check the address again or enter a valid address.
              </td>
            </tr>
            </table>
            </div>
            <div id="LYNXDisabledShopID" style="border-collapse: collapse; display: none;">
            <table>
             <tr>
              <td valign="top">
                <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
              </td>
              <td width="100%" nowrap="" class="legendnopad" style="font-size: 10pt; color:#FF0000; font-weight:bold;">
                This shop does not qualify for a LYNX DRP or Choice assignment, and has been designated to require alternate
              </td>
            </tr>
            <tr>
            <td>
            </td>
            <td width="100%" nowrap="" class="legendnopad" style="font-size: 10pt; color:#FF0000; font-weight:bold;">
                claim handling such as field inspection, etc. Please follow carrier's internal claim processing protocol.
              </td>
            </tr>
            </table>
            </div>
            <div style="width: 650px; border: 1px solid #000099; padding: 30px 15px 15px 15px;">
                <div style="border-bottom: #c0c0c0 1px solid; border-left: #c0c0c0 1px solid; padding-bottom: 5px;
                    margin-top: 10px; padding-left: 10px; width: 610px; padding-right: 0px; border-top: #c0c0c0 1px solid;
                    border-right: #c0c0c0 1px solid; padding-top: 0px">
                    <div style="border-bottom: #cccccc 1px solid; position: relative; border-left: #cccccc 1px solid;
                        background-color: #e7e7f7; width: 140px; color: #000099; font-size: 9pt; border-top: #cccccc 1px solid;
                        top: -10px; font-weight: bold; border-right: #cccccc 1px solid">
                        &nbsp;&nbsp;Add New Choice Shop
                    </div>
                    <table width="620" border="0" cellpadding="2" cellspacing="2" bordercolor="#000099"
                        class="shop_tbl" style="border-collapse: collapse;">
                        <tr>
                            <td nowrap style="font-size: 10pt; font-weight: bold;" colspan="2">
                                Name:<span style="color: #000099; font-size: 11pt">*</span><br />
                                <input name="NewShopName" id="txtShopName" type="text" onkeypress="chkEnterKey(event);"
                                    size="20" maxlength="40" value="<%=lsNewShopName%>" autocomplete="OFF" class="txtbox_shop"
                                    tabindex="1" style="width: 250px;">
                            </td>
                        </tr>
                        <tr>
                            <td nowrap style="font-size: 10pt; font-weight: bold;">
                                Address1:<span style="color: #000099; font-size: 11pt">*</span><br />
                                <input name="NewShopAddress1" id="txtShopAddress1" type="text" onkeypress="chkEnterKey(event);"
                                    size="20" maxlength="40" value="<%=lsNewShopAddress1%>" autocomplete="OFF" tabindex="2"
                                    style="width: 250px;" class="txtbox_shop">
                            </td>
                        </tr>
                        <tr>
                            <td nowrap style="font-size: 10pt; font-weight: bold;">
                                Address2:<br />
                                <input name="NewShopAddress2" id="txtShopAddress2" type="text" onkeypress="chkEnterKey(event);"
                                    size="20" maxlength="40" value="<%=lsNewShopAddress2%>" autocomplete="OFF" tabindex="3"
                                    style="width: 250px;" class="txtbox_shop">
                            </td>
                        </tr>
                        <tr>
                            <td nowrap style="font-size: 10pt; font-weight: bold;">
                                City:<span style="color: #000099; font-size: 11pt">*</span><br />
                                <input name="NewShopCity" id="txtShopCity" type="text" onkeypress="chkEnterKey(event);"
                                    size="20" maxlength="40" value="<%=lsNewShopCity%>" autocomplete="OFF" class="txtbox_shop"
                                    tabindex="4" style="width: 150px;">
                            </td>
                        </tr>
                        <tr>
                            <td nowrap style="font-size: 10pt; font-weight: bold;">
                                State:<span style="color: #000099; font-size: 11pt">*</span><br />
                                <select name="NewShopState" id="txtShopState" tabindex="5">
                                    <option value=""></option>
                                    <option value="AK">Alaska</option>
                                    <option value="AL">Alabama</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DC">District of Columbia</option>
                                    <option value="DE">Delaware</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="IA">Iowa</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="MD">Maryland</option>
                                    <option value="ME">Maine</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MT">Montana</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NY">New York</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VA">Virginia</option>
                                    <option value="VT">Vermont</option>
                                    <option value="WA">Washington</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WY">Wyoming</option>
                                </select>
                            </td>
                            <td nowrap style="font-size: 10pt; font-weight: bold;">
                                Zipcode: <span style="color: #000099; font-size: 11pt">*</span><br />
                                <input name="NewShopZipcode" id="txtShopZipcode" type="text" onkeypress="return numbersOnly(this,event,0)"
                                    size="20" maxlength="6" value="<%=lsNewShopZip%>" autocomplete="OFF" class="txtbox_shop"
                                    tabindex="6" style="width: 150px;">
                            </td>
                        </tr>
                        <tr>
                            <td nowrap style="font-size: 10pt; font-weight: bold;">
                                FedTaxId:<br />
                                <input name="NewShopFedTaxId" id="txtShopFedTaxId" type="text" onkeypress="chkEnterKey(event);"
                                    size="20" maxlength="40" autocomplete="OFF" class="txtbox_shop" tabindex="7"
                                    style="width: 165px;">
                            </td>
                            <td nowrap style="font-size: 10pt; font-weight: bold;">
                                EmailAddress:<br />
                                <input name="NewShopEmailAddress" id="txtShopEmailAddress" type="text" onkeypress="chkEnterKey(event);"
                                    size="20" maxlength="40" autocomplete="OFF" class="txtbox_shop" tabindex="8"
                                    style="width: 200px;">
                            </td>
                        </tr>
                        <tr>
                            <td nowrap style="font-size: 10pt; font-weight: bold; width: 275px;">
                                Phone Number: <span style="font-size: 11px;">(Number Only)*</span><br />
                                <table border="0" cellpadding="1" cellspacing="1" width="200px">
                                    <tr>
                                        <td>
                                            <input name="NewShopPhoneAreaCode" id="txtShopPhoneAreaCode" type="text" size="3"
                                                onkeypress="return numbersOnly(this,event,0)" onkeyup="movetoNext(this, 'txtShopPhoneExchangeNumber')"
                                                maxlength="3" autocomplete="OFF" class="txtbox_shop" value="<%=lsNewPhoneAreaCode%>"
                                                tabindex="9" style="width: 50px;">
                                        </td>
                                        <td>
                                            <input name="NewShopPhoneExchangeNumber" id="txtShopPhoneExchangeNumber" type="text"
                                                onkeypress="return numbersOnly(this,event,0)" onkeyup="movetoNext(this, 'txtShopPhoneUnitNumber')"
                                                size="3" maxlength="3" value="<%=lsNewPhoneExchangeNumber%>" autocomplete="OFF"
                                                class="txtbox_shop" value="" tabindex="10" style="width: 50px;">
                                        </td>
                                        <td>
                                            <input name="NewShopPhoneUnitNumber" id="txtShopPhoneUnitNumber" type="text" size="4"
                                                onkeypress="return numbersOnly(this,event,0)" onkeyup="movetoNext(this, 'txtShopFaxAreaCode')"
                                                maxlength="4" value="<%=lsNewPhoneUnitNumber%>" autocomplete="OFF" class="txtbox_shop"
                                                value="" tabindex="11" style="width: 60px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td nowrap style="font-size: 10pt; font-weight: bold;">
                                Fax Number: <span style="font-size: 11px;">(Number Only)</span><br />
                                <table border="0" cellpadding="1" cellspacing="1" width="200px">
                                    <tr>
                                        <td>
                                            <input name="NewShopFaxAreaCode" id="txtShopFaxAreaCode" type="text" size="3" maxlength="3"
                                                autocomplete="OFF" onkeypress="return numbersOnly(this,event,0)" onkeyup="movetoNext(this, 'txtShopFaxExchangeNumber')"
                                                class="txtbox_shop" value="" tabindex="12" style="width: 50px;">
                                        </td>
                                        <td>
                                            <input name="NewShopFaxExchangeNumber" id="txtShopFaxExchangeNumber" type="text"
                                                size="3" onkeypress="return numbersOnly(this,event,0)" onkeyup="movetoNext(this, 'txtShopFaxUnitNumber')"
                                                maxlength="3" autocomplete="OFF" class="txtbox_shop" value="" tabindex="13" style="width: 50px;">
                                        </td>
                                        <td>
                                            <input name="NewShopFaxUnitNumber" id="txtShopFaxUnitNumber" type="text" size="4"
                                                onkeypress="movetoNext(this, 'txtNewShopRemarks')" onkeyup="return autoTab(this, 4, event);"
                                                maxlength="4" autocomplete="OFF" class="txtbox_shop" value="" tabindex="14" style="width: 60px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
            </form>
            </table>
            <br />
            <br />
            <table width="650" border="1" cellpadding="0" cellspacing="0" bordercolor="#000099"
                style="border-collapse: collapse">
                <tr bgcolor="#E5E5F5">
                    <td style="padding: 8px;">
                        <span style="color: #000099; font-size: 10pt; font-weight: bold;">Comments to Repair
                            Shop:</span> <span id="commFldMsg" style="color: #000000; font-weight: normal; font-size: 7pt">
                                (max.:250 characters)</span>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 10px;">
                        <textarea name="NewShopRemarks" id="txtNewShopRemarks" cols="110" rows="5" wrap="physical"
                            tabindex="15" style="width: 100%; height: 60px;" onkeydown="CheckInputLength(this, 250)"></textarea>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <table width="695" border="0" cellpadding="0" cellspacing="0" style="align: right;">
        <tr>
            <td align="right" valign="top" nowrap>
                <a href="javascript:SaveCancel()" title="Cancel the save" onmouseover="window.status='Cancel'; return true"
                    onmouseout="window.status=''; return true">
                    <img border="0" src="images/btn_cancel.gif" width="83" height="31">
                </a><a href="javascript:CheckSimilarMatchShop()" title="Save the Shop" onmouseover="window.status='Save the Shop'; return true"
                    onmouseout="window.status=''; return true">
                    <img border='0' src='images/btn_Save_Choice_Shop.gif' width='83' height='31'>
                </a>
            </td>
        </tr>
    </table>
    </div>
    <br />
    <!--<% If lsSearchType = "CS" and liCitiesReturned > 1 Then %>-->
    <br />
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000099"
        style="border-collapse: collapse; display: none;">
        <tr>
            <td valign="top">
                <img border="0" src="images/warn_img.gif" width="16" height="16" hspace="6" />
            </td>
            <td style="font-size: 10pt">
                Your City/State query returned multiple results. Please select the city that best
                matches your search by checking the appropriate checkbox and clicking <strong>Next</strong>
                button below.
            </td>
        </tr>
    </table>
    <table width="500" border="1" cellpadding="0" cellspacing="0" bordercolor="#000099"
        style="border-collapse: collapse; display: none;">
        <colgroup>
            <col width="30px" />
            <col width="360px" />
            <col width="100px" />
        </colgroup>
        <tr bgcolor="#E5E5F5">
            <td colspan="2" align="center" style="color: #000099; font-size: 10pt; font-weight: bold;
                padding: 4px;">
                City
            </td>
            <td align="center" style="color: #000099; font-size: 10pt; font-weight: bold; padding: 4px;">
                State
            </td>
        </tr>
        <%
      For Each oNode in oNodes

        lsRetCity = oNode.getAttribute("City")
        lsRetState = oNode.getAttribute("State")
        lsRetPrimaryZip = oNode.getAttribute("PrimaryZip")

        lsCityListHTML = lsCityListHTML & _
                         "<tr style='padding:4px; border: 1px solid #000099;'>" & _
                         "  <td align='center' class='cellGrid' style='border-left: 1px solid #000099;' nowrap=''><input name='cb_SelectCity' type='checkbox' onClick='setSelectedCity(this)' value='" & lsRetPrimaryZip & "'></td>" & _
                         "  <td class='cellGrid'>" & lsRetCity & "</td>" & _
                         "  <td class='cellGrid' align='center' colspan='2'>" & lsRetState & "</td>" & _
                         "</tr>"

      Next

      Response.write lsCityListHTML

      Set oNodes = Nothing
      Set docXML = Nothing
        %>
    </table>
    <table width="500" border="0" cellpadding="0" cellspacing="0" style="align: right;">
        <tr>
            <td align="right" valign="top" nowrap>
                <a href="javascript:AddChoiceShop()" title="Add Choice Shop" onmouseover="window.status='Add Choice Shop'; return true"
                    onmouseout="window.status=''; return true">
                    <img border="0" src="images/btn_addChoiceShop.gif" width="83" height="31">
                </a>
            </td>
            <td width="100%" align="center">
                <span id="animateGif2" style="font-size: 12px; font-weight: bold; color: #000080;
                    display: none;">Searching for Shops...&nbsp;&nbsp;&nbsp;
                    <img src="images/loading.gif" alt="" width="78" height="7" border="0">
                </span>
            </td>
            <td align="right" valign="top" nowrap>
                <a href="javascript:selectCity()" title="Search for Shops in the City selected" onmouseover="window.status='Search for Shops in the City selected'; return true"
                    onmouseout="window.status=''; return true">
                    <img src='images/btn_next.gif' border='0' width='83' height='31'>
                </a>
            </td>
        </tr>
    </table>
    <% ElseIf lsSearchType = "CS" and liCitiesReturned = 0 Then %>
    <br />
    <table width="600" border="0" cellpadding="0" cellspacing="0" bordercolor="#000099"
        style="border-collapse: collapse; display: none;">
        <tr>
            <td valign="top">
                <img border="0" src="images/warn_img.gif" width="16" height="16" hspace="6" />
            </td>
            <td style="font-size: 10pt; color: #FF0000; font-weight: bold;">
                Your City/State query didn't return any results. Please redefine your search.
            </td>
        </tr>
    </table>
    <% Else %>
    <br />
    <%
        If lsNewShopName <> "" Then
          ' Dim listbox
          ' Set listbox = document.getElementById("txtShopState")
           'ShopState = "PA"
           lsNewShopSearchCall = "<ShopSimilarMatch ShopName=""" & server.urlencode(lsNewShopName) & """ ShopAddress1=""" & server.urlencode(lsNewShopAddress1) & """ ShopAddress2=""" & server.urlencode(lsNewShopAddress2) & _
                           """ ShopCity=""" & lsNewShopCity & """ ShopState=""" & lsNewShopState & """ ShopZip=""" & lsNewShopZip & _
                           """ PhoneAreaCode=""" & lsNewPhoneAreaCode & """ PhoneExchangeNumber=""" & lsNewPhoneExchangeNumber & """ PhoneUnitNumber=""" & lsNewPhoneUnitNumber & """ ShopTypeCode=""" & lsShopTypeCD & """ InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
            Response.Write GetPersistedData(lsNewShopSearchCall, lsGUID, "WaSimilarMatchShop.xsl", "VehDesc=" & lsVehDesc & lcDelimiter & "PageTitle=" & lsPageTitle & lcDelimiter & "AddVehicle=" & lsAddVehicle, false) 
       Else

          lsDistance  = txtdistance.value
         If lsFromPage = "ShopSelectionAdv" Then
           liMaxShops= "20"
              lsShopSearchCall = "<ShopCombinedSearchByDistance Zip=""" & liZipCode & _
                           """ InsuranceCompanyID=""" & liInsuranceCompanyID & _
                          """ ShopTypeCode=""" & lsShopTypeCD & """ MaxShops=""" & liMaxShops & """ UserID=""" & liUserID & """ ShopName=""" & server.urlencode(lsShopName) & """ TargetDistance=""" & lsDistance & """/>"
             
             'Response.Write lsShopSearchCall
             Response.Write GetPersistedData(lsShopSearchCall, lsGUID, "WaCombinedShopSelection.xsl", "VehDesc=" & lsVehDesc & lcDelimiter & "PageTitle=" & lsPageTitle & lcDelimiter & "AddVehicle=" & lsAddVehicle, false)

          End If 
       End If 
             
           
           
    %>
    <% End If %>
    </div>
</body>
</html>
