<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.4.6.0 -->

<%
  lsPageName = "frmWitnessInfo.asp"
  lsPageTitle = "Claim Details"
  lsPageSubTitle = "Witness"
  lsStyleSheet = "WitnessInfoAlt.xsl"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcClaim
  lsHelpPage = "CP_Witness_Information.htm"
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStartClm.asp"-->
<%
dim lsWitness 
dim laWitness
lsWitness = Trim(Request("W"))

if lsWitness = "" or lsWitness="0" then Response.Redirect "frmClaimInfo.asp"
if right(lsWitness, 1) = "," then lsWitness = left(lsWitness, len(lsWitness) - 1)

laWitness = split(lsWitness, ",")

dim lbOverwriteExisting
lbOverwriteExisting = false
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspWitnessInfo.asp">
<!--#include file="includes/incHeaderTableWithIcons.asp"-->

<!--#include file="includes/incClaimCommonTop.asp"-->

<%
Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "NavBar.xsl", "currentArea=" & lsPageSubTitle, true)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
Response.Write GetPersistedData(lsClaimDetailCall, lsClaimDetailGUID, "ClaimHeader.xsl", "pageTitle=" & lsPageSubTitle, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

<%

liStop = ubound(laWitness)

for liCounter = 0 to liStop
	lsWitness = laWitness(liCounter)
	if len(lsWitness) > 0 and lsWitness <> "0" then
		Response.Write GetData("<ClaimWitness InsuranceCompanyID=""" & liInsuranceCompanyID & """ InvolvedID=""" & lsWitness & """/>", lsStyleSheet, "")
		if liCounter < liStop then
		%> 
			<!--#include file="includes/incMultipleCallSeparator.asp"-->
		<%
		end if
	end if
next

%>

<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>

<!--#include file="includes/incCommonHTMLEndClm.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
