<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/IncAuthenticationTools.asp"-->
<!--#include file="includes/incCryptTools.asp"-->
<!-- v1.5.0.0 -->

<%
  lsPageName = "frmChangePassword.asp"
  lsPageTitle = "Change Password"
  liPageType = lcAuthSection

  Dim sEmailAddress, sOldPassword, sNewPassword, sPasswordHint, lsResult, sRememberUser, lsRedirPage, lsUserSessionCall
  Dim docXML, NodeUser, InsuranceCompanyID, InsuranceCompanyName, liCarrierRep, NodeClaimPointPermission, lsInsCoListCall
  Dim NodeInsurance, sUserAuthenticateResult
  Dim strAwareResult, strAwareMessage

  If Request.Form("realSubmit") = "fromButton" Then
    sEmailAddress  = Request.Form("txtUserName")
    sOldPassword  = Request.Form("txtOldPassword")
    sNewPassword  = Request.Form("txtNewPassword")
    sPasswordHint  = server.urlencode(Request.Form("txtPasswordHint"))

    lsResult = ChangePassword(sEmailAddress,sOldPassword,sNewPassword,sPasswordHint)

    lsMessage = lsResult

    If lsResult = "Successful" Then
      'blank so that nothing gets displayed
      lsMessage = ""

      'Save new password if user has Remember Me turned on
      sRememberUser = Request.Cookies("APDLogin")("txtRemember")

      If sRememberUser = "on" Then
        Response.Cookies("APDLogin")("txtUserName") = fCrypt("LYNXAPDKEY", sEmailAddress)
        Response.Cookies("APDLogin")("txtPassword") = fCrypt("LYNXAPDKEY", sNewPassword)
        Response.Cookies("APDLogin")("txtRemember") = sRememberUser
        Response.Cookies("APDLogin").Expires = Now()+60

      Else
        Response.Cookies("APDLogin")("txtUserName") = ""
        Response.Cookies("APDLogin")("txtPassword") = ""
        Response.Cookies("APDLogin")("txtRemember") = ""
        Response.Cookies("APDLogin").Expires = Now()-1

      End If

      sUserAuthenticateResult = UserAuthenticate(sEmailAddress,sNewPassword)

       if Instr(1, sUserAuthenticateResult, "|") > 0 then
         strAwareResult = mid(sUserAuthenticateResult, 1, Instr(1, sUserAuthenticateResult, "|") - 1)
         strAwareMessage = mid(sUserAuthenticateResult, Instr(1, sUserAuthenticateResult, "|") + 1)
         sUserAuthenticateResult = strAwareResult
       end if

      If sUserAuthenticateResult = "Success" Then
        Response.Cookies("CPSession")("authUser") = fCrypt("LYNXAPDKEY", sEmailAddress)

        lsUserSessionCall = "<UserSessionDetail Login=""" & server.urlencode(sEmailAddress) & """ ApplicationCD=""" & lsApplicationCD & """ />"

        Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
        docXML.Async = False
        docXML.LoadXML GetData(lsUserSessionCall, "", "")

        If Not docXML.ParseError = 0 Then
          'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
        Else

          Set NodeUser = docXML.documentElement.selectSingleNode("User")
          Response.Cookies("CPSession")("UserID") = NodeUser.getAttribute("UserID")
          Response.Cookies("CPSession")("UserEmail") = sEmailAddress
          Response.Cookies("CPSession")("OfficeID") = NodeUser.getAttribute("OfficeID")
          Response.Cookies("CPSession")("OfficeName") = NodeUser.getAttribute("OfficeName")
          Response.Cookies("CPSession")("ClientOfficeId") = NodeUser.getAttribute("ClientOfficeID")
          Response.Cookies("CPSession")("NameFirst") = NodeUser.getAttribute("NameFirst")
          Response.Cookies("CPSession")("NameLast") = NodeUser.getAttribute("NameLast")
          Response.Cookies("CPSession")("PhoneAreaCode") = NodeUser.getAttribute("PhoneAreaCode")
          Response.Cookies("CPSession")("PhoneExchangeNumber") = NodeUser.getAttribute("PhoneExchangeNumber")
          Response.Cookies("CPSession")("PhoneUnitNumber") = NodeUser.getAttribute("PhoneUnitNumber")
          Response.Cookies("CPSession")("PhoneExtensionNumber") = NodeUser.getAttribute("PhoneExtensionNumber")

          InsuranceCompanyID = NodeUser.getAttribute("InsuranceCompanyID")
          InsuranceCompanyName = Server.URLEncode(NodeUser.getAttribute("InsuranceCompanyName"))

          If NodeUser.getAttribute("OfficeID") > "0" Then
            liCarrierRep = 1 'Carrier Rep User
          ElseIf NodeUser.getAttribute("OfficeID") = "0" and NodeUser.getAttribute("InsuranceCompanyID") = "0" Then
            liCarrierRep = 0 'LYNX Employee User
          End If
          Response.Cookies("CPSession")("CarrierRep") = liCarrierRep

          Set NodeClaimPointPermission = docXML.documentElement.selectSingleNode("User/ClaimPointPermission")
          Response.Cookies("CPSession")("ClaimRead") = NodeClaimPointPermission.getAttribute("ClaimRead")
          Response.Cookies("CPSession")("ClaimCreate") = NodeClaimPointPermission.getAttribute("ClaimCreate")
          Response.Cookies("CPSession")("ViewReport") = NodeClaimPointPermission.getAttribute("ViewReport")
          Response.Cookies("CPSession")("ManageUsers") = NodeClaimPointPermission.getAttribute("ManageUsers")
          Response.Cookies("CPSession")("AuthorizePayments") = NodeClaimPointPermission.getAttribute("AuthorizePayments")
          Response.Cookies("CPSession")("PaymentAuthorizationLevel") = NodeClaimPointPermission.getAttribute("PaymentAuthorizationLevel")
          Response.Cookies("CPSession")("ClaimViewLevel") = NodeClaimPointPermission.getAttribute("ClaimViewLevel")

          Set NodeClaimPointPermission = Nothing
          Set NodeUser = Nothing

        End If

        If liCarrierRep = 0 Then
          lsInsCoListCall = "<InsuranceCompanyList/>"
          docXML.LoadXML GetData(lsInsCoListCall, "", "")

          If Not docXML.ParseError = 0 Then
            'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
          Else
            Set NodeInsurance = docXML.documentElement.selectSingleNode("InsuranceCompany[@DemoFlag='1']")
            InsuranceCompanyID = NodeInsurance.getAttribute("InsuranceCompanyID")
            InsuranceCompanyName = Server.URLEncode(NodeInsurance.getAttribute("Name"))
            Set NodeInsurance = Nothing
          End If

        End If

        lsRedirPage = "rspInsuranceCoSelect.asp?hidInsuranceCompanyID=" & InsuranceCompanyID & "&hidInsuranceCompanyName=" & InsuranceCompanyName

      Else
        lsMessage = sUserAuthenticateResult
        Response.Redirect "frmLogin.asp?fromPage=ChangePassword&ErrorMessage=" & lsMessage

      End If

    End If

  Else
    sRememberUser = Request.Cookies("APDLogin")("txtRemember")

    If sRememberUser = "on" Then
      sEmailAddress = fDecrypt("LYNXAPDKEY", Request.Cookies("APDLogin")("txtUserName"))
      sOldPassword = fDecrypt("LYNXAPDKEY", Request.Cookies("APDLogin")("txtPassword"))
    End If

  End If
%>

<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->
<style>
   li {
   }
</style>
<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script LANGUAGE="javascript">
  <!--

    function Validate() {
        var queryString = window.location.search;
        if (checkvulnerability(queryString)) {
            alert("Change password was not successful.\nPlease try again.");
            return;
        }
    var lsLoginID = document.getElementById("txtUserName").value;
    lsLoginID = lsLoginID.split(' ').join('');
    document.getElementById("txtUserName").value = lsLoginID;

    if (isEmail(lsLoginID) == false)
    {
      alert("Invalid e-mail address specified for Login ID. Please enter it in the following format: name@domain.nn \n" +
            "The following characters are not valid for the name part of the e-mail address. \n" +
            "     > greater than \n" +
            "     < less than \n" +
            "     ( open parentheses \n" +
            "     ) close parentheses \n" +
            "     [ open brackets \n" +
            "     ] close brackets \n" +
            "     \\ back slash \n" +
            "     % percent \n" +
            "     & ampersand \n" +
            "     + plus \n" +
            "     . period (see note below)\n" +
            "     , comma \n" +
            "     ; semicolon \n" +
            "     : colon \n" +
            "       whitespace character (such as a space or tab) \n" +
            "     @ at sign (as part of the name) \n" +
            "     \" double quote \n" +
            "Note: If there are any periods before the @ sign they must be followed by one or more valid characters. \n" +
            "A period cannot be the first character and there cannot be consecutive periods before the @ sign.");

      document.getElementById("txtUsername").focus();
      return false;
    }

    if (document.getElementById("txtOldPassword").value == "")
    {
      alert("Please enter your old password.")
      document.getElementById("txtOldPassword").focus();
      return;
    }

    var iPswdLen;
    iPswdLen = document.getElementById("txtNewPassword").value.length;
    var rePW = /[A-Za-z]+[0-9]+/;//regular expression looking for at least one alpha and at least one number -- abcde1f
    var rePWalso = /[0-9]+[A-Za-z]+/; // alternate expression which starts with a number 1234fdf
    var strAllow = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^*_():";
    var strPwd = document.getElementById("txtNewPassword").value;
    var invalidChars = "";
    var iMaxInvalidChars = 10;
    var strChar = "";
    for (var i = 0; i < iPswdLen && iMaxInvalidChars > 0; i++) {
       strChar = strPwd.substr(i, 1);
       if (strAllow.indexOf(strChar) == -1) {
          invalidChars += (strChar == " " ? "[SPACE]" : strChar) + "\n";
          iMaxInvalidChars--;
       }
    }
    if (invalidChars != "") {
       alert("New password contains the following invalid characters:\n" + invalidChars + "\nPassword must meet the above mentioned requirements.");
       document.getElementById("txtNewPassword").value = "";
       document.getElementById("txtConfirmPassword").value = "";
       document.getElementById("txtNewPassword").focus();
       return;
    }

    if (iPswdLen < 7 || iPswdLen > 15) {
       alert("New password must be at least 7 and no more than 15 characters in length.");
       document.getElementById("txtNewPassword").value = "";
       document.getElementById("txtConfirmPassword").value = "";
       document.getElementById("txtNewPassword").focus();
       return;
    }

    /*if ((iPswdLen > 6)&&(iPswdLen < 19))
    {
      if ((!rePW.test(document.getElementById("txtNewPassword").value)) && (!rePWalso.test(document.getElementById("txtNewPassword").value)))
      {
        alert("New password must be atleast 7 characters, must contain at least one number and at least one letter and meet the above requirements.");
        document.getElementById("txtNewPassword").focus()
        return;
      }
    }
    else
    {
      alert("New password must be atleast 7 characters, must contain at least one number and at least one letter and meet the above requirements.");
      document.getElementById("txtNewPassword").focus()
      return;
    }*/

    if (document.getElementById("txtNewPassword").value != document.getElementById("txtConfirmPassword").value)
    {
       alert("The New Password does not match the Confirm New Password.");
      document.getElementById("txtConfirmPassword").focus()
      return;
    }

    if (document.getElementById("txtPasswordHint").value == "")
    {
      alert("Please enter a password hint for your new password.");
      document.getElementById("txtPasswordHint").focus()
      return;
    }

    document.frmChgPswd.submit();
  }

  function isEmail(strEmail)
  {
    var re = /^(([^<>()[\]\\%&+.,;:\s@\"]+(\.[^<>()[\]\\%&+.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(strEmail);
  }

  function onEnterKey(event)
  {
    if ( event.keyCode == 13 )
      Validate();
  }

  function Continue()
  {
    top.location = '<%= lsRedirPage %>';
  }

  function reset_onclick()
  {
    document.getElementById("txtUsername").value  = "";
    document.getElementById("txtOldPassword").value  = "";
    document.getElementById("txtNewPassword").value  = "";
    document.getElementById("txtConfirmPassword").value  = "";
    document.getElementById("txtPasswordHint").value  = "";
  }
  //-->
</script>

<%
  if Request.Form("realSubmit") = "fromButton" and lsResult = "Successful" then
%>

  <br><br>

  <p class="login">Your password has been changed successfully.</p>

  <br>
  <br>

  <div style="width: 300px; text-align: right;">
    <a href="Javascript:Continue();"
       title="Click to Continue"
       onMouseOver="window.status='Click to Continue'; return true"
       onMouseOut="window.status=''; return true">
      <img border="0" src="images/btn_continue.gif" border="0" WIDTH="83" HEIGHT="31">
    </a>
  </div>

<%
  else
%>

<form NAME="frmChgPswd" METHOD="post" ACTION="frmChangePassword.asp">
  <input type="hidden" name="realSubmit" value="fromButton">

  <div style="width:590px">
    <p class="login" style="border:1px solid #C0C0C0;padding:10px;">
      <b>Effective September 28, 2010, LYNX Services implemented a password expiration policy.</b><br /><br />
      Password for your login to this website will automatically expire after 180 days if not changed by you within that time frame. 
      When you attempt to login with an expired password you will be directed to the Forgot Password page.
    </p>
    <p class="login">
      Please enter your login ID, your old password, a new password and a password hint and click the Submit button to set your new password.
    </p>
    <p class="login">
      <!-- Your new password must be between 6 and 10 characters and must contain at least one number and at least one letter. -->
      Your new password must meet the following requirements:
      <ul>
         <!--<li>The password cannot contain all or part of the user�s name or user id</li>-->
         <li>The password must be at least seven and no more than fifteen characters in length</li>
         <li>The password must contain characters from three of the following four categories: </li>
            <ul>
               <li>Uppercase characters (A through Z) </li>
               <li>Lowercase characters (a through z) </li>
               <li>Numbers (0 through 9) </li>
               <li>Non-alphanumeric characters - allowed: ! @ # $ % ^ ( ) * _ :</li>
               <!--<li>Non-alphanumeric characters (e.g: !, @, #, $, %). Do not use the backslash (\) or dash (-) characters in your password. </li>-->
            </ul>
         <li>Disallow at least 5 prior Passwords.</li>
      </ul>

    </p>
    <p class="login">
      Password hint: You can be reminded of your password in case you forget it by clicking on the "Password Hint" link in the login screen.
    </p>
  </div>

  <br>

  <table width="600" border="0" cellspacing="0" cellpadding="6" style="border-collapse:collapse; border:1px solid #000099;">
    <colgroup>
      <col width="260px"/>
      <col width="340px"/>
    </colgroup>
    <tr bgcolor="#E5E5F5">
      <td class="box">
        <p class="bodyBlue">Login ID: *
        <span style="font-size: 11px; font-weight: normal; color: #000000;">(User's valid e-mail address)</span></p>
      </td>
      <td class="box">
        <input type="text" name="txtUserName" id="txtUserName" value="<%=sEmailAddress%>" size="40" onkeypress="onEnterKey(event);" onBlur="ldTrim(this)"></td>
    </tr>
    <tr>
      <td class="box">
        <p class="bodyBlue">Old Password: *
      </td>
      <td class="box">
        <input type="password" name="txtOldPassword" id="txtOldPassword" value="<%=sOldPassword%>" size="40" onkeypress="onEnterKey(event);" onBlur="ldTrim(this)"></td>
    </tr>
    <tr bgcolor="#E5E5F5">
      <td class="box">
        <p class="bodyBlue">New Password: *
      </td>
      <td class="box">
        <input type="password" name="txtNewPassword" id="txtNewPassword" size="40" onkeypress="onEnterKey(event);" onBlur="ldTrim(this)"></td>
    </tr>
    <tr>
      <td class="box">
        <p class="bodyBlue">Confirm New Password: *
      </td>
      <td class="box">
        <input type="password" name="txtConfirmPassword" id="txtConfirmPassword" size="40" onkeypress="onEnterKey(event);" onBlur="ldTrim(this)"></td>
    </tr>
    <tr bgcolor="#E5E5F5">
      <td class="box">
        <p class="bodyBlue">New Password Hint: *
      </td>
      <td class="box">
        <input type="text" name="txtPasswordHint" id="txtPasswordHint" size="40" onkeypress="onEnterKey(event);" onBlur="ldTrim(this)">
      </td>
    </tr>
  </table>

  <table width="600" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;">
    <colgroup>
      <col width="500px"/>
      <col width="100px"/>
    </colgroup>
    <tr>
      <td>
         <p class="bodyBlue">* Required fields</p>
      </td>
      <td>
        <a href="Javascript:Validate();"
           title="Click to Submit"
           onMouseOver="window.status='Click to Submit'; return true"
           onMouseOut="window.status=''; return true">
          <img border="0" src="images/btn_submit.gif" border="0" WIDTH="83" HEIGHT="31">
        </a>
      </td>
    </tr>
  </table>

</form>

<%
  end if
%>

<script language="JavaScript" for="window" event="onload">
 if (document.getElementById("txtNewPassword"))
 {
    var sRememberUser = '<%=sRememberUser%>';

    if (sRememberUser == "on")
       document.getElementById("txtNewPassword").focus();
 }
</script>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
