<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!-- v1.4.4.0 -->

<%
'********************************
'from global.asa...didn't work there in staging,
'so we'll try here.  everybody has to log in, so
'it's no big deal where the code actually resides.

If Request.Cookies("CPSession")("guidArray0") = "" Then
  Dim loSession, laGUIDs, liGUIDs
  
  Set loSession = Server.CreateObject("APD.clsAppData")
  
  liGUIDs = 6
  
  laGUIDs = loSession.GenerateXGuids(liGUIDs)
  
  liGUIDs = liGUIDs - 1
  
  For liCounter = 0 to liGUIDs
    Response.Cookies("CPSession")("guidArray" & liCounter) = laGUIDs(liCounter)
  Next
  
  Set loSession = Nothing
End If
'end segment from global.asa
'********************************  

  Dim liCertifiedFirstID, liAddressZip, lsGUID, docXML, lsResultXML, lsShopLoginCall, ShopLoadID, liPreviousVisited
  
  If Request.Form("realSubmit") = "fromButton" Then
    liCertifiedFirstID = Request.Form("CertifiedFirstID")
    liAddressZip  = Request.Form("AddressZip")
      
    lsShopLoginCall = "<CFShopLogin CertifiedFirstID=""" & server.urlencode(liCertifiedFirstID) & """ AddressZip=""" & server.urlencode(liAddressZip) & """/>"
    
    Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
    docXML.Async = False
    docXML.LoadXML GetData(lsShopLoginCall, "", "")
    
    If Not docXML.ParseError = 0 Then
      'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
    Else
      ShopLoadID = docXML.documentElement.getAttribute("ShopLoadID")
      liPreviousVisited = docXML.documentElement.getAttribute("PreviousVisitToSiteFlag")
    End If
  
    If ShopLoadID = "-1" Then
      Response.Redirect "frmCFShopResults.asp?ShopQualified=1"
    ElseIf ShopLoadID > 0 Then
      Response.Redirect "frmCFShopInfo.asp?CertifiedFirstID=" & liCertifiedFirstID & "&AddressZip=" & liAddressZip & "&PreviousVisited=" & liPreviousVisited
    ElseIf ShopLoadID = 0 Then
      lsMessage = "Invalid CertifiedFirst Network ID and/or Shop Zip Code entered."
    Else
      lsMessage = "Unable to login. Please contact us at the phone number/email below. Thanks"
    End If
    
    Set docXML = Nothing

  End If

%>

<html>
<head>
<title>LYNXSelect Program Registration</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incBrowserSniffer.js" type="text/javascript"></script>
<SCRIPT language="JavaScript" src="includes/incCFShopInfo.js"></SCRIPT>

<script language="JavaScript" type="text/JavaScript">

function tstBrowser()
{
  if (is_ie5_5up == false && is_nav7up == false && is_moz_ver < 1.4)
  {
    alert("This site requires Internet Explorer v.5.5 or higher or Netscape v.7.1 or higher or Mozilla v1.4  or higher.");
    return false;
  }
  else
  {
    document.getElementById("txtCertifiedFirstID").focus();
  }
}


function validateLogin()
{
  var lbIsValid = true;
  var lsMsg = "";
  var lsReq = "";

  if (tstBrowser() == false)
  {
    lbIsValid = false;
    return;
  }

  if (document.getElementById("txtCertifiedFirstID").value.length == 0)
    lsReq +=  "    " + "CertifiedFirst ID" + "\n";
  if (document.getElementById("txtAddressZip").value.length == 0)
    lsReq +=  "    " + "Shop Zip Code " + "\n";

  if (lsReq.length > 0)
  {
    lsMsg += "* The following fields are required:\n";
    lsMsg += lsReq;
  }

  if (lsMsg.length > 0) // display errors if any
  {
    alert(lsMsg);
    lbIsValid = false;
  }

  if (lbIsValid)
    document.frmLogin.submit();
}


function reset_onclick() 
{
  document.frmLogin.txtCertifiedFirstID.value  = "";
  document.frmLogin.txtAddressZip.value = ""; 
}


function pswd_onclick(event) 
{
  if ( event.keyCode == 13 )
    validateLogin();
}


// Popup About page with application details
function callAbout()
{
  if (window.event.ctrlKey)
  {
    var lsDimensions = "dialogHeight:300px; dialogWidth:380px; "
    var lsSettings = "scroll:no; resizable:yes; status:no; help:no; center:yes; unadorned:yes;"
    var lsQuery = "frmAbout.asp";
    window.showModalDialog( lsQuery, window, lsDimensions + lsSettings );
  }
}

</script>

</head>

<BODY LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 background="./images/page_bg.jpg" onLoad="tstBrowser()">

  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
    <tr>
      <td width="169" align="left" valign="top">
        <IMG SRC="images/logo_corner.jpg" ALT="" width="169" height="202">
      </td>
      <td align="left" valign="top">

        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" background="images/header_bg.jpg" height="132">
          <tr>
            <td height="50">&nbsp;</td>
            <td align="right" valign="top" height="50">&nbsp;</td>
          </tr>
          <tr>
            <td colspan=2 valign="top">&nbsp;<span class="errorMessage" style="font-size:14px"><%=lsMessage%></span></td>
          </tr>
          <tr>
            <td align="left" valign="bottom" nowrap width="50%">
              <p class="header">
              LYNXSelect<sup style="font-size: 9px;">TM</sup> Registration for 
              <span id="spanlsPageTitle" lang="en-us"><em>CertifiedFirst </em><sup style="font-size: 9px;">TM</sup> Network</span>
            </td>
            <td align="right" valign="bottom" width="50%">&nbsp;</td>
          </tr>
        </table>
        <br>
        <table border="0" cellpadding="0" cellspacing="0" width="600">
          <tr>
            <td valign="bottom">
              <div style="border:2px solid #999999; width:86%; padding:6px; ">
                <p class="login">
                Welcome to LYNXSelect Registration website.  All <em>CertifiedFirst</em> Network shops are invited to apply for participation
                in the LYNXSelect Collision Repair Program. Shops that meet the program�s criteria become part of a national group
                of shops available for work.
                </p>
              </div>
            </td>
            <td width="200" valign="top" align="center" nowrap>
              <img src="images/LYNXSelect_ logo.jpg" alt="" width="160" height="48" border="0">
              <br><br>
              <!-- <img src="images/spacer.gif" border="0" width="10" height="40"> -->
              <img src="images/CertifiedFirst_logo.jpg" alt="" width="150" height="51" border="0">
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <img src="images/spacer.gif" border="0" width="1" height="20">
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <p class="login">
                We�ve streamlined the registration process for CertifiedFirst Network shops.
                Please input your <em>CertifiedFirst</em> Network ID and your shop location�s zip code. Then click, <strong>Login</strong>.
              </p>
            </td>
          </tr>
        </table>
        
        <form name="frmLogin" action="frmCFShopLogin.asp" method="post">
        <input type="hidden" name="realSubmit" value="fromButton">
          
        <br>
        <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse" bordercolor="#00099">
          <tr>
            <td width="8" height="8" valign="top"><img border="0" src="images/table_TL_blue.gif" WIDTH="10" HEIGHT="8"></td>
            <td width="210" background="images/table_TC_blue.gif"><img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="1"></td>
            <td width="374" background="images/table_TC_blue.gif"><img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="1"></td>
            <td width="8" height="8" valign="top"><img border="0" src="images/table_TR_blue.gif" WIDTH="8" HEIGHT="8"></td>
          </tr>
          <tr>
            <td background="images/table_CL_blue.gif" class="cell">&nbsp;</td>
            <td background="images/lt_blue.gif" height="34" class="cell" nowrap>
              <p class="bodyBlue" style="margin-left:80px;"><em>CertifiedFirst</em> Network ID:</p>
            </td>
            <td background="images/lt_blue.gif" class="cell">
              <input type="text" id="txtCertifiedFirstID" name="CertifiedFirstID" value="" size="20" maxlength="20" onKeyPress="return numbersOnly(this,event,0)">
            </td>
            <td background="images/table_CR_blue.gif" class="cell">&nbsp;</td>
          </tr>
          <tr>
            <td background="images/table_CL_white.gif" valign="bottom">&nbsp;</td>
            <td height="34" nowrap>
              <p class="bodyBlue" style="margin-left:80px;">Shop Zip Code:</p>
            </td>
            <td>
              <input type="text" id="txtAddressZip" name="AddressZip" value="" size="8" maxlength="5" onKeyPress=" pswd_onclick( event ); return numbersOnly(this,event,0);">
            </td>
            <td background="images/table_CR_white.gif" valign="bottom">&nbsp;</td>
          </tr>
          <tr>
            <td background="images/table_CL_white.gif" valign="bottom"><img border="0" src="images/table_BL_white.gif" WIDTH="8" HEIGHT="8"></td>
            <td class="cell"><img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="1"></td>
            <td class="cell"><img border="0" src="images/spacer.gif" WIDTH="1" HEIGHT="1"></td>
            <td background="images/table_CR_white.gif" valign="bottom"><img border="0" src="images/table_BR_white.gif" WIDTH="8" HEIGHT="8"></td>
          </tr>
        </table>
      
        <table width="600" border="0" cellpadding="0" cellspacing="0" style="align:right;">
          <tr>
            <td width="100%">&nbsp;</td>
            <td align="right" valign="top" nowrap>
              <a href="javascript:reset_onclick()" title="Reset" style="cursor:pointer; cursor:hand;"><img border="0" src="images/btn_reset.gif" WIDTH="83" HEIGHT="31"></a>
            </td>
            <td align="right" valign="top" nowrap>
             <img border="0" src="images/spacer.gif" WIDTH="10" HEIGHT="1">
            </td>
            <td id="nextImgSrc" align="right" valign="top" nowrap>
              <a href="javascript:validateLogin()" title="Next" style="cursor:pointer; cursor:hand;"><img border="0" src="images/btn_login.gif" WIDTH="83" HEIGHT="31"></a>
            </td>
          </tr>
        </table>
        
        </form>

        <br>
        <p class="login">
        For questions regarding the LYNXSelect Program or this enrollment process, please email us at <a href="mailto:lynxselect@lynxservices.com?subject=Questions regarding the LYNXSelect Program">lynxselect@lynxservices.com</a>.
        </p>
        <p class="login">
        For questions regarding the <em>CertifiedFirst</em> Network Program, please call 866-CERT-1ST.
        </p>

      </td>
    </tr>
  </table>

<br>
<br>
<br>

<!--#include file="includes/incFooter.asp"-->

</body>
</html>
