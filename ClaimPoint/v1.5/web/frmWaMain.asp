<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incCPCryptTool.asp"-->
<!-- v1.4.5.0 -->

<%
  lsPageName = "frmWaMain.asp"
  lsPageTitle = "Web Assignment"
  liPageType = lcAfterInsuranceCompanySelect
  
  Dim lsUIFrameSrc, lsDCFrameSrc, lsAssignmentType,lsCombined
  Dim lsCurrentAssignmentDesc, liCurrentAssignmentId, lsInsCoDetailsCall, lsCarrierUsersListCall, lsPrevPage
  Dim lsInsConfigXML, loXML, ElemList, oNode, lsAssignmentTypeIDList, laAssignmentTypeID
  Dim  lsShopAssignmentType

   lsShopAssignmentType= Request("ShopAssignmentType")

  'Response.Write lsShopAssignmentType
   If Not lsShopAssignmentType Is Nothing Then
     If lsShopAssignmentType = "ChoiceShop" Then
         lsCurrentAssignmentDesc = "Choice Shop"
      End If

     If lsShopAssignmentType = "ProgramShop" Then
         lsCurrentAssignmentDesc = "Program Shop"
      End If
   
   End If

   If lsShopAssignmentType Is Nothing Then
      'For Combined cookie to set title
    	lsCombined = Request.Cookies("CombinedType")
	    If Not lsCombined Is Nothing Then
   		   If lsCombined = "C" Then
     			lsCurrentAssignmentDesc = "Choice Shop"
   		   ElseIf lsCombined = "B" Then
     			lsCurrentAssignmentDesc = "DRP/Choice"
   		   Else 
      			lsCurrentAssignmentDesc = Request.Form("AssignmentDesc")
   		End If
	Else
		lsCurrentAssignmentDesc = Request.Form("AssignmentDesc")
  	End If

    End If



 
  liCurrentAssignmentId = Request.Form("AssignmentId")
  lsAssignmentType = Request.Form("FrameSrc")
  
  if (lsCurrentAssignmentDesc <> "") then lsPageTitle = lsCurrentAssignmentDesc & " " & lsPageTitle

  if (lsCurrentAssignmentDesc = "Roofing") Then lsPageTitle = "Participant Contractor-Contractor Search"

  Response.Cookies("CPSession")("CurrAssignmentDescription") = lsCurrentAssignmentDesc
  Response.Cookies("CPSession")("CurrAssignmentId") = liCurrentAssignmentId

  'If the Ins Co Config shows that these flags are 1 then check if they should be turned on for the current assignment chosen.
  If liEmergencyLiabilityScript = 1 or liReserveLineTypeScript = 1 or liSB511Script = 1 Then

    Set loXML = Server.CreateObject("MSXML2.DOMDocument")
    loXML.Async = False
  
    lsInsConfigXML = "<InsuranceCompanyConfig InsuranceCompanyID=""" & liInsuranceCompanyID & """/>"
    loXML.LoadXML GetPersistedData(lsInsConfigXML, lsInsCoConfigGUID, "", "", false)
  
    If Not loXML.ParseError = 0 Then
      Response.Write "<b>Error Code:</b> " & loXML.ParseError & "<br>"
  
    Else
  
      Set ElemList = loXML.SelectNodes("/Root/CustomScripting")
    
      If ElemList.length > 0 Then
        For each oNode in ElemList
      
          lsAssignmentTypeIDList = oNode.getAttribute("AssignmentTypeIDList")
          laAssignmentTypeID = split(lsAssignmentTypeIDList, "|")

          If PositionInArray(laAssignmentTypeID, liCurrentAssignmentId, vbBinaryCompare) < 0 _
                               and (LCASE(oNode.getAttribute("InputResponseName")) = "liability" _ 
                               or LCASE(oNode.getAttribute("InputResponseName")) = "emergency") Then
            liEmergencyLiabilityScript = 0
          End If
            
          If PositionInArray(laAssignmentTypeID, liCurrentAssignmentId, vbBinaryCompare) < 0 _
                               and LCASE(oNode.getAttribute("ConditionValue")) = "ca" Then
            liSB511Script = 0
          End If

          If PositionInArray(laAssignmentTypeID, liCurrentAssignmentId, vbBinaryCompare) < 0 _
                               and (LCase(oNode.getAttribute("InputResponseName")) = "linetype" _ 
                               or LCase(oNode.getAttribute("InputResponseName")) = "reserve") Then
            liReserveLineTypeScript = 0
          End If

        Next
    
      End If
  
    End If

  End If


  Response.Cookies("CPSession")("CurrAssignmentEmergencyLiability") = liEmergencyLiabilityScript
  Response.Cookies("CPSession")("CurrAssignmentSB511") = liSB511Script
  Response.Cookies("CPSession")("CurrAssignmentReserveLineType") = liReserveLineTypeScript

  'Return the position strLookingFor is found in aMyArray
  'if strLookingFor is not found -1 is returned
  'aMyArray = the string array that we are searching
  'strLookingFor = the string that we are looking for in the array aMyArray
  'compare = case-sensitive search (vbBinaryCompare) or a case-insensitive search (vbTextCompare)
  'ex. Response.Write PositionInArray(aSomeArray, "bar", vbBinaryCompare)
  'ex. Response.Write PositionInArray(aSomeArray, "Cool!", vbTextCompare) 
  Function PositionInArray(aMyArray, strLookingFor, compare)
  
    Dim iUpper, iLoop
    iUpper = UBound(aMyArray)
  
    For iLoop = LBound(aMyArray) to iUpper
      If compare = vbTextCompare then
        If CStr(UCase(aMyArray(iLoop))) = CStr(UCase(strLookingFor)) then
          PositionInArray = iLoop
          Exit Function
        End If
      Else
        If CStr(aMyArray(iLoop)) = CStr(strLookingFor) then
          PositionInArray = iLoop
          Exit Function
        End If
      End If
    Next
  
    'Didn't find the element in the array...
    PositionInArray = -1
  
  End Function


  Dim lsQuerystring,strEncrypted

	liUserID = Request.Cookies("CPSession")("UserID")
        lsUserEmail = Request.Cookies("CPSession")("UserEmail")
        liOfficeID = Request.Cookies("CPSession")("OfficeID")
        lsUserNameFirst = Request.Cookies("CPSession")("NameFirst")
        lsUserNameLast = Request.Cookies("CPSession")("NameLast")
        lsPhoneAreaCode = Request.Cookies("CPSession")("PhoneAreaCode")
        lsPhoneExchangeNumber = Request.Cookies("CPSession")("PhoneExchangeNumber")
        lsPhoneUnitNumber = Request.Cookies("CPSession")("PhoneUnitNumber")
        lsPhoneExtensionNumber = Request.Cookies("CPSession")("PhoneExtensionNumber")
        liInsuranceCompanyID = Request.Cookies("CPSession")("InsuranceCompanyID")
        lsInsuranceCompanyName = Request.Cookies("CPSession")("InsuranceCompanyName")
 	
        lsQuerystring="UserID=" & liUserID & "&UFname=" & lsUserNameFirst & "&ULname=" & lsUserNameLast & "&OfficeId=" & liOfficeID & "&UEmail=" & lsUserEmail & "&PhoneAC=" & lsPhoneAreaCode & "&PhoneEN=" & lsPhoneExchangeNumber & "&PhoneUN=" & lsPhoneUnitNumber & "&PhoneExn=" & lsPhoneExtensionNumber & "&InsCompID=" & liInsuranceCompanyID & "&InsCompName=" & lsInsuranceCompanyName
       
	strEncrypted=Encrypt(lsQuerystring,"CPWEMROOF")    

  If lsAssignmentType = "ProgramShop" Then
    lsUIFrameSrc = "frmWaShopSelectionAdv.asp?fromPage=frmClaimSearch"
    lsDCFrameSrc = "frmWaDataContainer.asp"

  ElseIf lsAssignmentType = "ChoiceShop" Then
    lsUIFrameSrc = "frmWaChoiceShopSelectionAdv.asp?fromPage=frmClaimSearch"
    lsDCFrameSrc = "frmWaChoiceDataContainer.asp" 
    
  ElseIf lsAssignmentType = "RepairReferral" Then
    lsUIFrameSrc = "frmWaRRPShopSelectionAdv.asp?fromPage=frmClaimSearch"
    lsDCFrameSrc = "frmWaRRPDataContainer.asp"

  ElseIf lsAssignmentType = "DeskAudit" Then
    lsUIFrameSrc = "frmWaDeskAudit.asp"
    lsDCFrameSrc = "frmWaDeskAuditDataContainer.asp"

  ElseIf lsAssignmentType = "DeskReview" Then
    lsUIFrameSrc = "frmWaDeskAudit.asp"
    lsDCFrameSrc = "frmWaDeskAuditDataContainer.asp"

  ElseIf lsAssignmentType = "TotalLoss" Then
    lsUIFrameSrc = "frmWaTLassignment.asp"
    lsDCFrameSrc = "frmWaTLassignmentDataContainer.asp"

  ElseIf lsAssignmentType = "IA" Then
    lsUIFrameSrc = "frmWaIAassignment.asp"
    lsDCFrameSrc = "frmWaIAassignmentDataContainer.asp"

  ElseIf lsAssignmentType = "SecondaryTowing" Then
    lsUIFrameSrc = "frmSecondaryTowing.aspx?Insc=" & liInsuranceCompanyID & "&Analyst=" & liUserID
    lsDCFrameSrc = "frmWaSTDataContainer.asp"
  ElseIf lsAssignmentType = "Roofing" Then
    lsUIFrameSrc = "../Roofing/ContractorSearch.aspx?info=" & strEncrypted 
    lsDCFrameSrc = ""
  Else
    lsUIFrameSrc = "frmWaClaimInfo.asp"
    lsDCFrameSrc = "frmWaDataContainer.asp"
  End If

  Response.Cookies("CPSession")("srhZipCode") = ""
  Response.Cookies("CPSession")("srhDistance") = ""
  
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->

<script language="JavaScript" src="includes/jquery-1.4.2.min.js" type="text/javascript"></script>
<script language="JavaScript" src="includes/jquery.simplemodal-1.3.5.min.js" type="text/javascript"></script>

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<%
  If lsAssignmentType = "DeskAudit" or lsAssignmentType = "DeskReview" Then
%>
  <script language="JavaScript" src="includes/incWaDeskAuditDatachk.js" type="text/javascript"></script>
<%
  ElseIf lsAssignmentType = "IA" Then
%>
  <script language="JavaScript" src="includes/incWaIAAssignmentDatachk.js" type="text/javascript"></script>
<%
  Else
%>
  <script language="JavaScript" src="includes/incWaDatachk.js" type="text/javascript"></script>
<%
  End If
%>

<script language="JavaScript">

  var objFNOLSelection = null;
  gbEmergencyLiabilityScript = <%= liEmergencyLiabilityScript %>;
  gbReserveLineTypeScript = <%= liReserveLineTypeScript %>;
  var liLaborRatesSetupFlag = "<%= liLaborRatesSetupFlag %>";

  var lsAssignmentType = "<%=lsAssignmentType%>";
  
  function hideMsg()
  {
    var objMsg = document.getElementById("msgDiv");
    if (objMsg)
    {
      objMsg.style.display = "none";
    }
  }

  function showMsg()
  {
    window.scrollTo(0,0);
    var objMsg = document.getElementById("msgDiv");
    if (objMsg)
    {
      objMsg.style.display = "inline";
    }
  }

  function chkBeforeUnloadWindow()
  {
    //alert(window.frames.uiFrame.document.activeElement);
    // alert(document.activeElement.tagName);
    var lbShowWarning = true;
    var loObj = window.frames.uiFrame.document.activeElement;

    // if (document.activeElement.tagName == "A" || (loObj == "javascript:goCancel()") || (loObj == "javascript:rtnMyClmDsktp()"))
    //if (loObj == "javascript:goCancel()" || loObj == "javascript:rtnMyClmDsktp()")
    if ((loObj == "javascript:goCancel()") || (loObj == "javascript:rtnMyClmDsktp()") || (loObj == "javascript:selectShop()") || (chkClaimSubmitted() == "true"))
      lbShowWarning = false;
    
    //alert(chkClaimSubmitted() + ", " + lbShowWarning);
    if (lbShowWarning == true)
    {
      event.returnValue = "Please use the navigation buttons (icons) on the page to navigate while entering an assignment.\n"+
                          "If you use the browser buttons to navigate, the data entered may be lost or invalid.\n"+
                          "If you wish to navigate to link clicked, press the OK button.";
    }
  }
  
  function setFNOLData(objFNOL){
      if (objFNOL){
         objFNOLSelection = objFNOL;
         if (typeof(objFNOLSelection.claimxml) == "string" && objFNOLSelection.claimxml != ""){
            xmlFNOLData.loadXML(objFNOLSelection.claimxml);
            
            if (xmlFNOLData.parseError.errorCode != 0){
               xmlFNOLData.loadXML("<Root/>");
               objFNOLSelection = null;
            }
         } else {
            objFNOLSelection = null;
            xmlFNOLData.loadXML("<Root/>");
         }
      } else {
         objFNOLSelection = null;
		 //-------------------------------//
		 // 02Dec2013 - TVD  			  //
		 // Remove the line below because //
		 // it caused errors in IE11.     //
		 // The xmlFNOLData never gets set//
		 // for this to even work.        //
		 //-------------------------------//
         //xmlFNOLData.loadXML("<Root/>");
      }
  }
  
  function getFNOLClaimNumber(){
      if (objFNOLSelection) {
         return objFNOLSelection.claimnumber;
      }
      return "";
  }

  function getFNOLVehicleID(){
      if (objFNOLSelection) {
         return objFNOLSelection.vehicleid;
      }
      return "";
  }
  
  function showModalMessage(strMessage) {
    if (strMessage == "")
        strMessage = "Please wait...";
    $("#progressMessage").text(strMessage);
    //$("#progress").modal({ minHeight: 75, minWidth: 300, escClose: false, position: ["10%", "20%"] });
    $("#progress").modal({ minHeight: 75, minWidth: 300, escClose: false });
  }
  
  function hideModalMessage(){
    $.modal.close();
  }
</script>

<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->
    <div id="progress" style="display:none">
    <div id="progressMessage" style="font-size:12px;text-align:center;padding:5 5 10 5">Please wait...</div>
    <center><img src="images/loading.gif"/></center>
    </div>
  <DIV id="msgDiv" name="msgDiv" style="position:relative; top:60px; left:200px; display:inline;">

    <div name="msgDiv" id="msgDiv" style="position:relative; left:200px;display:none;">
      <table width="220" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #808080; background-color:#EEEEEE; border-collapse:collapse;">
        <tr>
          <td height="30" id="msgTxt" name="msgTxt" style="text-align:center; font-size:12px; font-weight:bold; color:#000080;">Loading... Please Wait.</td>
        </tr>
        <tr>
          <td height="20" style="text-align:center"><img src="images/loading_anim.gif" alt="" width="94" height="17" border="0"></td>
        </tr>
        <tr>
          <td height="10">&nbsp;</td>
        </tr>
      </table>
    </div>
    <br>
    <img src="images/spacer.gif" border="0" WIDTH="1" >
    <br>

  </DIV>

<IFRAME SRC="<%=lsUIFrameSrc%>" NAME="uiFrame" ID="uiFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="width:100%; height:1750px; border:0px; background:transparent; "></IFRAME>
<IFRAME SRC="<%=lsDCFrameSrc%>" NAME="dataContIfr" ID="dataContIfr" WIDTH="0" HEIGHT="0" SCROLLING="no"  FRAMEBORDER="0" STYLE="visibility:hidden;"></IFRAME>
<!--<IFRAME SRC="<%=lsDCFrameSrc%>" NAME="dataContIfr" ID="dataContIfr" WIDTH="500" HEIGHT="500" SCROLLING="yes"  FRAMEBORDER="0" STYLE="visibility:visible;"></IFRAME>-->
<%
	If lsAssignmentType = "Roofing" then
		response.write "<script language='javascript'>hideMsg();</script>"
	End If
%>

<%
	If lsAssignmentType = "SecondaryTowing" then
		response.write "<script language='javascript'>hideMsg();</script>"
	End If
%>

<xml id="xmlFNOLData">
   <Root/>
</xml>

<!--#include file="includes/incCommonHTMLEnd.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
