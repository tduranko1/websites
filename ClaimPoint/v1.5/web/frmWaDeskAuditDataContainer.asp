<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!-- v1.4.5.0 -->

<html>
<head>
<title>WADeskAuditDataContainer</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script language="JavaScript" type="text/JavaScript">

  function pageInit()
  {
    if (parent.gbEmergencyLiabilityScript == 1 || parent.gbReserveLineTypeScript == 1)
      insTdCell(document.getElementById("tblVehicle"))
  }

  function insTdCell(oTbl)
  {
  	var loNewCell;
    var lsNewElm = "<input name='SourceApplicationPassThruData' id='txtSourceApplicationPassThruData' type='text'><textarea name='CustomScriptingMemo' id='txtCustomScriptingMemo'></textarea>";

  	if (document.all)
  	{
  	  loNewCell = oTbl.rows[0].insertCell();
  	}
  	else if (document.createElement)
  	{
  		loNewCell = document.createElement("TD");
      var loMyRrow = oTbl.getElementsByTagName("tr").item(0);
  		loMyRrow.appendChild(loNewCell);
    }
    loNewCell.innerHTML = lsNewElm;
  }


  function updImpactValues()
  {
  	document.getElementById("txtImpactLocations").value = document.getElementById("txtPrimaryDamage").value + "," + document.getElementById("txtSecondaryDamage").value;
  }
  
</script>

</head>

<body onload="pageInit()">

<form name="tblDeskAuditInfo">

  <table name="tblClaim" id="tblClaim">
    <tr>
    	<td>
      	<input name="NoticeMethodID" id="txtNoticeMethodID" type="text" value="5">
      	<input name="DataSource" id="txtDataSource" type="text" value="Claim Point Web Assignment">
      	<input name="FNOLUserID" id="txtFNOLUserID" type="text">
      	<input name="InsuranceCompanyID" id="txtInsuranceCompanyID" type="text">
      	<input name="AssignmentDescription" id="txtAssignmentDescription" type="text">
      	<input name="AssignmentAtSelectionFlag" id="txtAssignmentAtSelectionFlag" type="text">
      	<input name="DemoFlag" id="txtDemoFlag" type="text">
        <input name="CarrierUser" id="txtCarrierUser" type="text" opt="yes">
      	<input name="CarrierOfficeName" id="txtCarrierOfficeName" type="text">
        <input name="CarrierRepNameFirst" id="txtCarrierRepNameFirst" type="text">
        <input name="CarrierRepNameLast" id="txtCarrierRepNameLast" type="text">
        <input name="CarrierRepUserID" id="txtCarrierRepUserID" type="text" value="">
        <input name="CarrierRepPhoneDay" id="txtCarrierRepPhoneDay" type="text">
        <input name="CarrierRepEmailAddress" id="txtCarrierRepEmailAddress" type="text">
        <input name="CarrierName" id="txtCarrierName" type="text">
      	<input name="CoverageClaimNumber" id="txtCoverageClaimNumber" type="text">
      	<input name="LossDate" id="txtLossDate" type="text">
      	<input name="LossAddressState" id="txtLossAddressState" type="text">
      	<input name="CallerNameFirst" id="txtCallerNameFirst" type="text">
      	<input name="CallerNameLast" id="txtCallerNameLast" type="text">
      	<input name="CallerRelationToInsuredID" id="txtCallerRelationToInsuredID" type="text">
      	<input name="CallerRelationToInsuredIDDescription" id="txtCallerRelationToInsuredIDDescription" type="text">
      	<input name="InsuredNameFirst" id="txtInsuredNameFirst" type="text">
      	<input name="InsuredNameLast" id="txtInsuredNameLast" type="text">
      	<input name="InsuredBusinessName" id="txtInsuredBusinessName" type="text">
         <input name="InsuredAddress1" id="txtInsuredAddress1" type="text">
         <input name="InsuredAddress2" id="txtInsuredAddress2" type="text">
         <input name="InsuredAddressCity" id="txtInsuredAddressCity" type="text">
         <input name="InsuredAddressState" id="txtInsuredAddressState" type="text">
         <input name="InsuredAddressZip" id="txtInsuredAddressZip" type="text">
      	<input name="CollisionDeductibleAmt" id="txtCollisionDeductibleAmt" type="text">
      	<input name="CollisionLimitAmt" id="txtCollisionLimitAmt" type="text">
      	<input name="ComprehensiveDeductibleAmt" id="txtComprehensiveDeductibleAmt" type="text">
      	<input name="ComprehensiveLimitAmt" id="txtComprehensiveLimitAmt" type="text">
      	<input name="LiabilityDeductibleAmt" id="txtLiabilityDeductibleAmt" type="text">
      	<input name="LiabilityLimitAmt" id="txtLiabilityLimitAmt" type="text">
      	<input name="UnderInsuredDeductibleAmt" id="txtUnderInsuredDeductibleAmt" type="text">
      	<input name="UnderInsuredLimitAmt" id="txtUnderInsuredLimitAmt" type="text">
      	<input name="UnInsuredDeductibleAmt" id="txtUnInsuredDeductibleAmt" type="text">
      	<input name="UnInsuredLimitAmt" id="txtUnInsuredLimitAmt" type="text">
      	<input name="TimeStarted" id="txtTimeStarted" type="text">
      	<input name="TimeFinished" id="txtTimeFinished" type="text">
      	<input name="IntakeStartSeconds" id="txtIntakeStartSeconds" type="text">
      	<input name="IntakeEndSeconds" id="txtIntakeEndSeconds" type="text">
      	<input name="IntakeSeconds" id="txtIntakeSeconds" type="text">
      	<input name="InsuredName" id="txtInsuredName" type="text">
      	<input name="ClaimSubmitted" id="txtClaimSubmitted" type="text" value="false" opt="yes">
        <textarea name="LossDescription" id="txtLossDescription"></textarea>
      	<input name="LiabilityText" id="txtLiabilityText" type="text" opt="yes">
      	<input name="LiabilityFlag" id="txtLiabilityFlag" type="text" opt="yes">
      	<input name="EmergencyText" id="txtEmergencyText" type="text" opt="yes">
      	<input name="EmergencyFlag" id="txtEmergencyFlag" type="text" opt="yes">
        <textarea name="CustomScriptingMemoClm" id="txtCustomScriptingMemoClm" opt="yes"></textarea>
        <input name="SourceApplicationPassThruDataClm" id="txtSourceApplicationPassThruDataClm" type="text" opt="yes">
        <input name="LaborRatesSetupFlag" id="txtLaborRatesSetupFlag" type="text" value="<%=liLaborRatesSetupFlag%>">
        <input name="PolicyNumber" id="txtPolicyNumber" type="text" value="">
      </td>
    </tr>
  </table>

  <table name="tblVehicle" id="tblVehicle">
    <tr>
    	<td programShop="false">
        <input name="AssignmentTypeID" id="txtAssignmentTypeID" type="text">
        <input name="ExposureCD" id="txtExposureCD" type="text">
        <input name="CoverageProfileCD" id="txtCoverageProfileCD" type="text">
        <input name="OwnerNameFirst" id="txtOwnerNameFirst" type="text">
        <input name="OwnerNameLast" id="txtOwnerNameLast" type="text">
        <input name="OwnerBusinessName" id="txtOwnerBusinessName" type="text">
        <input name="VIN" id="txtVIN" type="text">
        <input name="Mileage" id="txtMileage" type="text">
        <input name="LicensePlateNumber" id="txtLicensePlateNumber" type="text">
        <input name="VehicleYear" id="txtVehicleYear" type="text">
        <input name="Make" id="txtMake" type="text">
        <input name="Model" id="txtModel" type="text">
        <input name="Drivable" id="txtDrivable" type="text">
        <input name="PrimaryDamage" id="txtPrimaryDamage" type="text">
        <input name="SecondaryDamage" id="txtSecondaryDamage" type="text">
        <input name="PrimaryDamageDescription" id="txtPrimaryDamageDescription" type="text">
        <input name="SecondaryDamageDescription" id="txtSecondaryDamageDescription" type="text">
        <input name="ImpactLocations" id="txtImpactLocations" type="text">
        <textarea name="Remarks" id="txtRemarks"></textarea>
        <input name=DeductibleAmt id=txtDeductibleAmt type=text>
        <input name=LimitAmt id=txtLimitAmt type=text>
        <input name=CoverageProfileUiCD id=txtCoverageProfileUiCD type=text>
        <input name=ClientCoverageTypeID id=txtClientCoverageTypeUiID type=text>
        <input name=ClientCoverageTypeDesc id=txtClientCoverageTypeDesc type=text>
        <input name=VehicleNumber id=txtVehicleNumber type=text>
      	<input name="LineType" id="txtLineType" type="text" opt="yes">
      	<input name="Reserve" id="txtReserve" type="text" opt="yes">
        <input name=ShowScriptingMemoFlag id=txtShowScriptingMemoFlag value=0 type=text>
         <input name="RepairZipCode" id="txtRepairZipCode" type="text">
         <input name="RepairLocationCity" id="txtRepairLocationCity" type="text">
         <input name="RepairLocationState" id="txtRepairLocationState" type="text">
         <input name="RepairLocationCounty" id="txtRepairLocationCounty" type="text">
         <input name="RepairShopName" id="txtRepairShopName" type="text">
         <input name="RepairShopAddress" id="txtRepairShopAddress" type="text">
         <input name="RepairShopPhone" id="txtRepairShopPhone" type="text">
         <input name="RepairShopFax" id="txtRepairShopFax" type="text">
         <input name="RepairShopEmailAddress" id="txtRepairShopEmailAddress" type="text">
         <input name="RepairInspectionDate" id="txtRepairInspectionDate" type="text">
         <input name="ShopRemarks" id="txtShopRemarks" type="text" >
      </td>
    </tr>
  </table>

  <table name="tblGuidelines" id="tblGuidelines">
    <tr>
    	<td>
      	<input name="BodyLabor" id="txtBodyLabor" type="text">
      	<input name="RefinishLabor" id="txtRefinishLabor" type="text">
      	<input name="MaterialsLabor" id="txtMaterialsLabor" type="text">
      	<input name="FrameLabor" id="txtFrameLabor" type="text">
      	<input name="MechanicalLabor" id="txtMechanicalLabor" type="text">
      	<input name="LaborTax" id="txtLaborTax" type="text">
      	<input name="PartsTax" id="txtPartsTax" type="text">
      	<input name="MaterialsTax" id="txtMaterialsTax" type="text">
      	<input name="PartsDiscount" id="txtPartsDiscount" type="text">
      	<input name="GlassDiscount" id="txtGlassDiscount" type="text">
      </td>
    </tr>
  </table>
</form>


</body>
</html>
