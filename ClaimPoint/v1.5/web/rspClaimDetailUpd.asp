<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetData.asp"-->

<%
  lsPageName = "rspClaimDetailUpd.asp"
  
  Dim lsStrXML, loAPDData, liRetVal, sMode, sRet, loRootNode, lsUserID, loAware
  Dim sPassword, sBody, sUserName, sUserAdminEmail, lsAddAwareResult, docXML, lsEmailStrXML
  
  lsStrXML =  "<ClaimDetailMinimalUpdate " + _
                  " ClaimAspectID=""" + request("ClaimAspectID") + """" + _
                  " ClientClaimNumber=""" + server.urlencode(request("ClientClaimNumber")) + """" + _
                  " LossDate=""" + server.urlencode(request("LossDate")) + """" + _
                  " LossState=""" + request("LossState") + """" + _
                  " ClientCoverageTypeID=""" + request("ClientCoverageTypeID") + """" + _
                  " UserID=""" + request("UserID") + """" + _
                  " />"
  
  On Error Resume Next
  
  Set loAPDData = Server.CreateObject("PPGNAAPD.clsAPDData")
  
  liRetVal = loAPDData.PutXML(lsStrXML)
  
  Set loAPDData = Nothing
%>

<script language="JavaScript" type="text/JavaScript">

  var lsRet = "<%= liRetVal %>";
  
  parent.ClaimUpdResults(lsRet);
      
</script>
