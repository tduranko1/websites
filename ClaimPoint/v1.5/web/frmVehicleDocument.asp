<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.4.6.1 -->

<%
  Dim lsMode, lsXMLCall, lsDocType, liLynxID, lsAssignmenType, liFirstVehicle, liAssignmentID
  
  lsMode = request("M")
  liLynxID = Request("LynxID")
  lsAssignmenType = Request("AssignmenType")
  liFirstVehicle = request("FirstVehicle")
  liAssignmentID = request("AssignmentID")

  lsPageSubTitle = "Estimates and Documents"
  lsStyleSheet = "DocumentListAlt.xsl"
  lsDocType = "A"
  
  if lsMode = "E" then 
  	lsStyleSheet = "EstimateListAlt.xsl"
  	lsPageSubTitle = "Estimates"
  	lsDocType = "E"
  end if
  
  lsPageName = "frmVehicleDocument.asp"
  lsPageTitle = "Claim Details"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcVehicle
  lsHelpPage = "CP_Documents.htm"
  bGridSort = True
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incVerifyCurrentVehicle.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStartClm.asp"-->
<%
dim lbOverwriteExisting
lbOverwriteExisting = false

lsXMLCall = "<DocumentList InsuranceCompanyID=""" & liInsuranceCompanyID & """ LynxID=""" & liClaimID & """ ClaimAspectID=""" & liVehicleID & """ DocumentClassCD=""" & lsDocType & """/>"
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspVehicleInfo.asp">
<!--#include file="includes/incGoDocument.asp"-->
<!--#include file="includes/incHeaderTableWithIcons.asp"-->

<!--#include file="includes/incClaimCommonTop.asp"-->

<%
Response.Write GetPersistedData(lsVehicleDetailCall, lsVehicleDetailGUID, "VehicleNavBar.xsl", "currentArea=" & lsPageSubTitle & lcDelimiter & "AssignmentID=" & liAssignmentID & lcDelimiter & "AssignmenType=" & lsAssignmenType & lcDelimiter & "FirstVehicle=" & liFirstVehicle & lcDelimiter & "LynxID=" & liLynxID, true)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
Response.Write GetPersistedData(lsClaimInfoCall, lsClaimInfoGUID, "VehicleHeader.xsl", "claimNumber=" & liClaimID & lcDelimiter & "pageTitle=" & lsPageSubTitle & lcDelimiter & "currentVehicle=" & liVehicleID & lcDelimiter & "ClientOfficeId=" & UCase(lsClientOfficeId), true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

<%
Response.Write GetData(lsXMLCall, lsStyleSheet, "")
%>
<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>

<form name="frmNewVehicle" id="frmNewVehicle" method="POST" action="rspClaimAddVehicle.asp" >
  <input type="hidden" name="hidLynxID" id="hidLynxID" value="<%=liClaimID%>"/>
</form>

<!--#include file="includes/incCommonHTMLEndClm.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
