<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/IncAuthenticationTools.asp"-->
<!--#include file="includes/incCryptTools.asp"-->
<!-- v1.4.2.0 -->

<%
  lsPageName = "frmForgotPassword.asp"
  lsPageTitle = "Forgot Password"
  liPageType = lcAuthSection
  
  Dim sEmailAddress, lsResult, sRememberUser

  If Request.Form("realSubmit") = "fromButton" Then
    sEmailAddress  = Request.Form("UserName")
    lsResult = ForgotPassword(sEmailAddress)
    lsMessage  = lsResult

  Else
    sRememberUser = Request.Cookies("APDLogin")("txtRemember")

    If sRememberUser = "on" Then
      sEmailAddress = fDecrypt("LYNXAPDKEY", Request.Cookies("APDLogin")("txtUserName"))
    End If

  End If
%>

<!--#include file="includes/incCommonHTMLStart.asp"-->
<!--#include file="includes/incHeaderTableNoIcons.asp"-->
<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script LANGUAGE="javascript">
  <!--
  function Validate() {
    var queryString = window.location.search;
    if (checkvulnerability(queryString)) {
        alert("Password reset was not successful.\nPlease try again.");
		return;
	}
    var lsLoginID = document.getElementById("txtUserName").value;
    lsLoginID = lsLoginID.split(' ').join('');
    document.getElementById("txtUserName").value = lsLoginID;
    
    if (isEmail(lsLoginID) == false)
    {
      alert("Invalid e-mail address specified for Login ID. Please enter it in the following format: name@domain.nn \n" +
            "The following characters are not valid for the name part of the e-mail address. \n" +
            "     > greater than \n" +
            "     < less than \n" +
            "     ( open parentheses \n" +
            "     ) close parentheses \n" +
            "     [ open brackets \n" +
            "     ] close brackets \n" +
            "     \\ back slash \n" +
            "     % percent \n" +
            "     & ampersand \n" +
            "     + plus \n" +
            "     . period (see note below)\n" +
            "     , comma \n" +
            "     ; semicolon \n" +
            "     : colon \n" +
            "       whitespace character (such as a space or tab) \n" +
            "     @ at sign (as part of the name) \n" +
            "     \" double quote \n" +
            "Note: If there are any periods before the @ sign they must be followed by one or more valid characters. \n" +
            "A period cannot be the first character and there cannot be consecutive periods before the @ sign.");

      document.getElementById("txtUserName").focus()
      return false;    
    }

    document.frmforgotPswd.submit();
  }
  
  function isEmail(strEmail)
  {
    var re = /^(([^<>()[\]\\%&+.,;:\s@\"]+(\.[^<>()[\]\\%&+.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(strEmail);
  }

  function reset_onclick() 
  {
    document.getElementById("txtUsername").value = ""
  }
 
  //-->
</script>

<form NAME="frmforgotPswd" METHOD="post" ACTION="frmforgotPassword.asp">
  <input type="hidden" name="realSubmit" value="fromButton">

  <div style="width:590px">
    <p class="login" style="border:1px solid #C0C0C0;padding:10px;">
      <b>Effective September 28, 2010, LYNX Services implemented a password expiration policy.</b><br /><br />
      Password for your login to this website will automatically expire after 180 days if not changed by you within that time frame. 
      When you attempt to login with an expired password you will be directed to the Forgot Password page.
    </p>
    <p class="login">
      Please enter your login ID and click the Submit button to have the system reset your password. Your new password will be sent to your e-mail address.
    </p>
  </div>

  <br><br>

  <table width="600" border="0" cellspacing="0" cellpadding="6" style="border-collapse:collapse; border:1px solid #000099;">
    <colgroup>
      <col width="240px"/>
      <col width="360px"/>
    </colgroup>
    <tr bgcolor="#E5E5F5">
      <td class="box">
        <p class="bodyBlue">Login ID:
        <span style="font-size: 11px; font-weight: normal; color: #000000;">(User's valid e-mail address)</span></p>
      </td>
      <td class="box">
        <input type="text" name="UserName" id="txtUserName" value="<%=sEmailAddress%>" size="40" onBlur="ldTrim(this)">
      </td>
    </tr>
  </table>

  <div style="width:590px; text-align: right;">
    <a href="Javascript:Validate();"
       title="Click to Submit"
       onMouseOver="window.status='Click to Submit'; return true"
       onMouseOut="window.status=''; return true">
      <img border="0" src="images/btn_submit.gif" border="0" WIDTH="83" HEIGHT="31">
    </a>
  <div style="width:590px">

</form>      

<script language="JavaScript">
  document.getElementById("txtUserName").focus();   
</script>  

<!--#include file="includes/incCommonHTMLEnd.asp"-->
