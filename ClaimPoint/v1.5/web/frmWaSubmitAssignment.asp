<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incAppConstants.asp"-->
<!--#include file="includes/incGetUserSession.asp"-->
<!-- v1.5.0.0 -->

<%
  Dim lsCurrentAssignment, liCurrentAssignmentId, lsPrevPage, liLynxID, lbWasSubmitted, lsVehList, lsPrintLayoutPage, lsCurrentEnvironment
  Dim lsDocLocation, lsShopAssignmentType

  lsCurrentAssignment = Request.Cookies("CPSession")("CurrAssignmentDescription")
  liCurrentAssignmentId =  Request.Cookies("CPSession")("CurrAssignmentId")

  lsShopAssignmentType=Request.Cookies("ShopAssignmentType")

  'Response.Write "lsShopAssignmentType : "
  'Response.Write lsShopAssignmentType


  If Not lsShopAssignmentType Is Nothing Then
   If lsShopAssignmentType = "ChoiceShop" Then
     lsCurrentAssignmentDesc = "Choice Shop"
   End If

   If lsShopAssignmentType = "ProgramShop" Then
     lsCurrentAssignmentDesc = "Program Shop"
   End If

  End If

  lsPageName = "frmWaSubmitAssignment.asp"
  lsPageTitle = "Submit " & lsCurrentAssignment & " Assignment"
  
  lsPrevPage = Request("fromPage")
  liPageType = lcAssignmentPage
  
  If lsPrevPage = "rspWASubmitAssignment" Then
    liLynxID = Request("LynxID")
    lbWasSubmitted = Request("SubmitSuccess")
    lsVehList = Request("VehList")
    lsPrintLayoutPage = Request("PrintLayoutPage")
    lsDocLocation = Request("D")
lsCurrentEnvironment = Request("Env")
  End If
%>

<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incGetData.asp"-->

  <!-- include to check for and display if necessary APD offline/outage messages -->
  <!-- this message will display under the page title -->
<!--#include file="includes/incAPDOutageCheck.asp"-->

<html>
<head>
<title>Submit <%=lsCurrentAssignment%></title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<link href="includes/incWaStyle.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incWaUtilities.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">

  gsPrevPage = "<%=Request("fromPage")%>";
  gsVehNum = "";
  gsVehCount = "";
  var gsSubmitDate;
  
  function pageInit()
  {
    if(parent)
      parent.resetTimeOut();
  
    if (gsPrevPage == "ClaimSummary" || gsPrevPage == "DeskAuditSummary" || gsPrevPage == "IAAssignmentSummary")
    {
      var lsWasSubmitted = parent.chkClaimSubmitted();
      
      if (lsWasSubmitted == "false")
      {
        var lsChkReqParams = parent.chkReqParams();
        
        var laReqParams = lsChkReqParams.split("|");
        if (laReqParams[0] == "false")
        {
          document.getElementById("msgDiv").style.display = "none";
          var lsReqParamsError1 = "";
          var lsReqParamsError2 = "";
          var lsReqParamsError3 = "";
          
          if (laReqParams[1] != "")
            lsReqParamsError1 = "<tr><td> </td>" +
                                "<td><span style='font-size:14px; font-weight:bold'>" + laReqParams[1] +
                                "</span> - This may require that you logout and login again to ClaimPoint.</td></tr>";
            
          if (laReqParams[2] != "")
            lsReqParamsError2 = "<tr><td> </td>" +
                                "<td><span style='font-size:14px; font-weight:bold'>" + laReqParams[2] +
                                "</span> - This may require that you return to the MyClaimDesktop and re-enter the assignment.</td></tr>";
            
          if (laReqParams[3] != "")
            lsReqParamsError3 = "<tr><td> </td>" +
                                "<td><span style='font-size:14px; font-weight:bold'>" + laReqParams[3] +
                                "</span> - Review and correct the coverage type.</td></tr>";
            
          var lsReqParamsError =  "<br><br>" +
                                  "<table border=0><tr><td valign=top><img border=0 src=images/warn_img.gif WIDTH=16 HEIGHT=16 hspace=6></td>" +
                                  "<td valign=top style='color:#FF0000; font-size:11pt; font-weight:bold;'>" +
                                  "The assigment cannot be sent becauce essential data is invalid or missing:<br><br>" +
                                  "</td></tr>" +
                                  lsReqParamsError1 + lsReqParamsError2 + lsReqParamsError3 +
                                  "<tr><td> </td>" +
                                  "<td><br><span style='font-size:14px; font-weight:bold'>" +
                                  "<a href='javascript:history.go(-1)' title='Return to Summary Screen'; return true'>Click here</a> to return to the Summary screen and review the information." +
                                  "</span><br><br>If the problem persists, please contact the LYNX Services helpdesk. Thank you.</td></tr></table>";
  
          document.getElementById("errorDiv").innerHTML = lsReqParamsError;
          return;
        }
        else
        {
          exeXML();
        }
      }  
      else
      {
        alert("Unable to submit claim. This claim has already been submitted successfully.");
        top.location = "frmMyClaimsDesktop.asp";
      }
    }
    else if (gsPrevPage == "rspWASubmitAssignment")
    {
       if(parent)
      {
        var liLynxID = "<%=liLynxID%>";
        if (liLynxID > 0)
        {
          document.getElementById("txtSubmitDate").innerHTML = parent.getSubmitDate();
          parent.markClaimSubmitted('true');
        }
      }
    }
    updAdditionalRentalManagementInfo();
  }
  
  
  function exeXML()
  {
     if(parent)
    {
      var lsXML = parent.getXMLstr();
      if (lsXML != "")
      {
        document.getElementById("txtlstrXML").value = lsXML;
        if (document.getElementById("txtlstrXML").value != "")
         document.frmSubmitAssignment.submit();
      }
    }
  }

  
  function printWindow()
  {
    var loiFrm = frames["prtFrame"];
    if (loiFrm)
    {
      loiFrm.focus(); //IE requirement
      loiFrm.print();
    }
  }
  
  
  function rtnMyClmDsktp()
  {
    top.location = "frmMyClaimsDesktop.asp";
  }
  
  
  function reSubmitClaim()
  {
    if(parent)
    {
      var lsWasSubmitted = parent.chkClaimSubmitted();
      if (lsWasSubmitted == "false")
      {
        parent.document.getElementById("uiFrame").src = "frmWaSubmitAssignment.asp?fromPage=ClaimSummary";
      }
      else
      {
        alert("Unable to submit claim. This claim has already been submitted successfully.");
        return;
      }
    }
    else
      return;
  }

function updAdditionalRentalManagementInfo(){
            debugger;
            var liVehNum = "1";
docUiContainer =  parent.getdcIFrameDoc();
 var liNewVehNum;
            if(docUiContainer.getElementById("txtExposureCD_" + liVehNum).value == "3")
                liNewVehNum = "2";
            else
                liNewVehNum = "1";
            var iInscCompanyID = <%= liInsuranceCompanyID %>;
            var strDataToSend = "";
            
            var lsWasSubmitted = parent.chkClaimSubmitted();
            if (lsWasSubmitted == "true")
            {
  		var strEnvironment= "<%=lsCurrentEnvironment%>";
                var strServiceURL = ""
                if(docUiContainer && docUiContainer.getElementById("txtIsRentalExistFlag_" + liVehNum).value == "0"){
                    if("<%=liLynxID%>" != "" && docUiContainer.getElementById("hidRentalVendor_" + liVehNum).value != ""){
                        strDataToSend = "LynxID="+ "<%=liLynxID%>";
                        strDataToSend += "&RentalAgency="+ docUiContainer.getElementById("hidRentalVendor_" + liVehNum).value;
                        strDataToSend += "&VehicleNumber=" + liNewVehNum;
                        strDataToSend += "&InsuranceCompanyID=" + iInscCompanyID;
                        strDataToSend += "&RentalResConf=" + docUiContainer.getElementById("txtRentalResConf_" + liVehNum).value;
                        strDataToSend += "&RentalVehicleClass=" + docUiContainer.getElementById("hidRentalVehicleClass_" + liVehNum).value;
                        strDataToSend += "&RentalRateType=" + docUiContainer.getElementById("hidRentalRateType_" + liVehNum).value;
                        strDataToSend += "&RentalRate=" + docUiContainer.getElementById("txtRentalRate_" + liVehNum).value;
                        strDataToSend += "&RentalAuthPickup=" + docUiContainer.getElementById("txtRentalAuthPickup_" + liVehNum).value;
                        strDataToSend += "&RentalTaxRate=" + docUiContainer.getElementById("txtRentalTaxRate_" + liVehNum).value;
                        strDataToSend += "&RentalPhone=" + docUiContainer.getElementById("txtRentalPhoneAC_" + liVehNum).value +"-"+ docUiContainer.getElementById("txtRentalPhoneEN_" + liVehNum).value +"-"+docUiContainer.getElementById("txtRentalPhoneUN_" + liVehNum).value;
                        strDataToSend += "&RentalAddress=" + docUiContainer.getElementById("txtRentalAddress_" + liVehNum).value;
                        strDataToSend += "&RentalCity=" + docUiContainer.getElementById("txtRentalCity_" + liVehNum).value;
                        strDataToSend += "&RentalState=" + docUiContainer.getElementById("hidRentalState_" + liVehNum).value;
                        strDataToSend += "&RentalZip=" + docUiContainer.getElementById("txtRentalZip_" + liVehNum).value;
                        if(strEnvironment == "DEV")
                            strServiceURL = "https://dapd.lynxservices.com/PGWClaimPointFoundation/APDService.asmx"
                        else if(strEnvironment == "STG")
                            strServiceURL = "https://sapd.lynxservices.com/PGWClaimPointFoundation/APDService.asmx"
                        else if(strEnvironment == "PRD")
                            strServiceURL = "https://apd.lynxservices.com/PGWClaimPointFoundation/APDService.asmx"
                            if(strServiceURL != ""){
			postURL = strServiceURL + "/RentalMgt";
            var oReq = new ActiveXObject("MSXML2.XMLHTTP");
			oReq.open("POST", postURL , false);
			oReq.setRequestHeader("content-type", "application/x-www-form-urlencoded");
            oReq.send(strDataToSend);
			}
                        docUiContainer.getElementById("txtIsRentalExistFlag_" + liVehNum).value = "1"; 
                    }
                }
            }
        }
  
</script>

</head>

<body onLoad="resizeWaIframe(0); pageInit();">

<div id="divContainer">

  <DIV id="errorDiv"></DIV>

  <form name="frmSubmitAssignment" method="post" action="rspWaSubmitAssignment.asp">

<%
  If lsPrevPage = "ClaimSummary" or lsPrevPage = "DeskAuditSummary" or lsPrevPage = "IAAssignmentSummary" Then
%>
    <TEXTAREA COLS="" ROWS="" NAME="lstrXML" ID="txtlstrXML" READONLY STYLE="visibility: hidden;"></TEXTAREA>

  <%
    If lsPrevPage = "DeskAuditSummary" Then
  %>
      <input name="PrintLayoutPage" type="hidden" value="DeskAuditPrint">
  <%
    ElseIf lsPrevPage = "IAAssignmentSummary" Then
  %>
      <input name="PrintLayoutPage" type="hidden" value="IAAssignmentPrint">
  <%
    Else
  %>
      <input name="PrintLayoutPage" type="hidden" value="Print">
  <%
    End If
  %>

  <div name="msgDiv" id="msgDiv" style="position:relative; top:60px; left:200px;display:inline;">

    <table width="220" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #808080; background-color:#EEEEEE; border-collapse:collapse;">
      <tr>
        <td height="20" id="msgTxt" name="msgTxt" style="text-align:center; font-size:12px; font-weight:bold; color:#000080;">Submitting Claim... Please Wait.</td>
      </tr>
      <tr>
        <td height="20" style="text-align:center"><img src="images/loading_anim.gif" alt="" width="94" height="17" border="0"></td>
      </tr>
      <tr>
        <td height="10">&nbsp;</td>
      </tr>
    </table>

  </div>

<%
  ElseIf lsPrevPage = "rspWASubmitAssignment" Then
  
    If lbWasSubmitted = "1" Then
%>

  <br>

  <table width="600" border="0" cellpadding="10" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse">
    <tr>
      <td nowrap="">

       <P style="font-size:11pt">
       Your claim has been assigned the following LYNX ID.

        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td colspan="2" nowrap>&nbsp;</td>
          </tr>
          <tr>
            <td nowrap class="legendnopad" style="font-size:14pt">LYNX ID:</td>
            <td nowrap style="font-size:14pt"><%=liLynxID%></td>
          </tr>
          <tr>
            <td nowrap class="legendnopad" style="font-size:11pt">Submitted:&nbsp;</td>
            <td nowrap style="font-size:11pt"><span id="txtSubmitDate"></span></td>
          </tr>
        </table>

       </P>

      </td>
    </tr>
  </table>

  <br>

  <%
      If liDocumentUploadFlag = "1" Then
  %>

    <IFRAME SRC="frmWaFileUploadPrompt.asp?LynxID=<%=liLynxID%>&VehList=<%=lsVehList%>" NAME="uploadFrame" ID="uploadFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="width:640px; height:140px;"></IFRAME>

<%
      End If

    Else
%>

  <br>

  <table width="600" border="0" cellpadding="10" cellspacing="0" style="border:1px solid #000099; border-collapse:collapse">
    <tr>
      <td nowrap="">
        <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
      </td>
      <td width="100%" nowrap="" class="legendnopad" style="color:#FF0000; font-size:11pt; font-weight:bold;">
        Your claim has not been submitted due to an error.
      </td>
    </tr>
    <tr>
      <td nowrap="">&#160;</td>
      <td width="100%" height="30" nowrap="" class="legendnopad" style="font-size:10pt;">
        Please
        <a href="javascript:reSubmitClaim()"
           title="Re-submit claim"
           onMouseOver="window.status='Re-submit claim'; return true"
           onMouseOut="window.status=''; return true"><strong>re-submit</strong></a>
        your claim.
      </td>
    </tr>
  </table>

  <br>

<%
    End If

  End If

  If lsPrevPage = "rspWASubmitAssignment" Then
  
    If lbWasSubmitted = "1" Then
%>

  <table width="600" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="100%" align="right">
        <a href="javascript:rtnMyClmDsktp()"
           title="Return to My Claims Desktop"
           onMouseOver="window.status='Return to My Claims Desktop'; return true"
           onMouseOut="window.status=''; return true">
          <img src="images/prev_button.gif" width="19" height="19" border="0" align="top"></a>

        <a href="javascript:rtnMyClmDsktp()"
           title="Return to My Claims Desktop"
           onMouseOver="window.status='Return to My Claims Desktop'; return true"
           onMouseOut="window.status=''; return true"><strong>Return</strong></a>
         to My Claims Desktop.
      </td>
    </tr>
  </table>

  <br>

    <%
      If lsDocLocation <> "" Then
    %>

      <P style="font-size:9pt">
        To print a copy for your records click on the <img src="images/pdfprint.gif"/> print button.<br>
        To save a local copy click on the <img src="images/pdfsave.gif"/> save button.
      </P>

      <IFRAME SRC="frmDisplayDocument.asp?D=<%=lsDocLocation%>" NAME="prtFrame" ID="prtFrame" MARGINWIDTH="0" MARGINHEIGHT="0" SCROLLING="no" FRAMEBORDER="0" STYLE="width:708px; height:500px;border:1px solid #808080; padding:0px; background:transparent;"></IFRAME>

    <%
      Else
    %>

        <br>
    
        <table border=0>
          <tr>
            <td valign=top>
              <img border=0 src=images/warn_img.gif WIDTH=16 HEIGHT=16 hspace=6>
            </td>
            <td valign=top style='font-size:10pt; font-weight:bold;'>
              Your claim has been submitted successfully but the Assignment Report cannot be displayed at this time.
              A copy of the report will be sent to you via e-mail shortly and will be available in the Documents screen for this claim.
              Sorry for the inconvenience.
            </td>
          </tr>
        </table>
    
    <%
      End If

    Else
  %>
    
  <table width="600" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="100%" align="right">
        <a href="javascript:rtnMyClmDsktp()"
           title="Return to My Claims Desktop"
           onMouseOver="window.status='Return to My Claims Desktop'; return true"
           onMouseOut="window.status=''; return true">
          <strong>Return</strong>
        </a> to My Claims Desktop. 
      </td>
    </tr>
  </table>

<%
    End If
  
  End If
%>

  </form>

</div>

</body>
</html>

