<%@ Language=VBScript %>

<%
  ' v1.5.2.1
  Option Explicit

  Dim lSizeLimit, liUserID, lsCurrentPage, lBytes, oApd, liLynxID, lsVehList, sPertainsTo, sDocumentType, sSupplementNumber, sDocumentName
  Dim sFileUploadParams, aryFileUploadParams, sUploadStatus, liVehNumber, lsChannel
  lsChannel = Request("ch")

  Server.ScriptTimeout = 300 'override the server timeout to 5 minutes to allow a 5 Mb file upload

  On Error Resume Next
  sDocumentType = Request("dt")
  
  lSizeLimit = 5120000 '5Mb File Limit
  liUserID = Request.Cookies("CPSession")("UserID")
  sFileUploadParams = Request.Cookies("FileUploadParams")
  aryFileUploadParams = split(sFileUploadParams,"|")
  
  liLynxID = aryFileUploadParams(0)
  sPertainsTo = aryFileUploadParams(1)
  if (sDocumentType = "") then
   sDocumentType = aryFileUploadParams(2)
  end if
  sSupplementNumber = aryFileUploadParams(3)
  sDocumentName = aryFileUploadParams(4)
  lsCurrentPage = aryFileUploadParams(5)
  liVehNumber = aryFileUploadParams(6)
  lsVehList = Request.Cookies("CPSession")(liLynxID & "VehiclesList")

  lBytes = Request.TotalBytes

  If lBytes > 0 And lBytes < lSizeLimit Then
    ' Instantiate Upload Class
    Set oApd = Server.CreateObject("APD.clsAppData")

    oApd.ProcessUploadedDocuments lBytes, Request.BinaryRead( lBytes ), liLynxID, sPertainsTo, sDocumentType, sSupplementNumber, liUserID

    Set oApd = Nothing
    
    sUploadStatus = "Complete"
  Else
    sUploadStatus = "OverSize"
  End If
    
%>

<html>
<head>
  <title>Estimate File Upload Save</title>
  <link href="includes/apd_style.css" type="text/css" rel="stylesheet">

<script language="JavaScript" src="includes/incBrowserSniffer.js" type="text/javascript"></script>

<script language="JavaScript">

  function initPage()
  {
    var lsCurrentPage = "<%=lsCurrentPage%>";
    var liVehNumber = "<%=liVehNumber%>";
    var lsUploadStatus = "<%=sUploadStatus%>";
    if (lsCurrentPage == "frmDocumentUpload" && lsUploadStatus == "Complete")
    {
      if (is_ie5_5up == true)
        parent.sReturnVal = "1";
      else
      {
        parent.sReturnVal = "1";
        top.opener.myModalWin.returnedValue = "1";
      }
    }
  }

  function reloadFrame()
  {
    if (is_ie5_5up == true)
      parent.document.getElementById("uploadFrame").src = "frmWaFileUploadPrompt.asp?LynxID=<%=liLynxID%>&ch=<%=lsChannel%>&VehList=<%=lsVehList%>&currentFileUploadPage=<%=lsCurrentPage%>&VehNumber=<%=liVehNumber%>";
    else
      history.back();
  }

</script>
  
</head>

<body onLoad="initPage();">

<%
  If sUploadStatus = "OverSize" Then
%>
    <table border="0">
      <tr>
        <td valign="top">
          <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
        </td>
        <td>
          <div style="color:#FF0000; font-size:11pt; font-weight:bold;">5 Mb file size upload limit exceeded. Your file size is <%=Round(lBytes / 1024 / 1000, 1)%> Mb.</div><br>
		  <div style="color:#000000; font-size:10pt; font-weight:bold;">Please remove pages from your document to reduce the file size below 5 Mb.</div>
        </td>
      </tr>
      <tr>
        <td> </td>
        <td><br/>
        <p style="font-size:14px; font-weight:bold">
          Please <a href="javascript:reloadFrame()">click here</a> to return to the upload screen.
        </p>
        </td>
      </tr>
    </table>
<%
  ElseIf sUploadStatus = "Complete" Then
%>
  <p style="font-size:11pt">
    The <span style="FONT-WEIGHT:bold; COLOR:#000099;"><%=sDocumentType%><%If sSupplementNumber > 0 Then Response.Write " " & sSupplementNumber%></span>
    <span style="FONT-STYLE:italic;"><%=sDocumentName%></span> was uploaded successfully.
  </p>
  <p style="font-size:11pt">
    <a href="javascript:reloadFrame()"><strong>Click here</strong></a> to upload another document.
  </p>
  <%
    If lsCurrentPage = "frmDocumentUpload" Then
  %>
  <br><br>
      
  <table width="100%" border="0">
    <tr>
    	<td width="100%" align="right">
        <a href=# onclick="javascript:parent.closeMe()" class="mini"><img src="images/btn_close.gif" alt="Close Window" width="80" height="28" vspace="10" border="0"></a>&nbsp;
      </td>
    </tr>
  </table>
  <%
    End If
  %>

<%
  End If
%>

</body>
</html>
