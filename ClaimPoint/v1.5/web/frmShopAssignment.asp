<!--#include file="includes/incCommonTop.asp"-->
<!-- v1.5.0.0 -->

<%
  Dim liLynxID, lsAssignmentType, liAspectID, liAssignmentID, liVehNumber
  
  liLynxID = Request("LynxID")
  liVehNumber = Request("VehNumber")
  lsAssignmentType = Request("AssignmentType")
  liAspectID = request("AspectID")
  liAssignmentID = request("AssignmentID")

  lsPageName = "frmShopAssignment.asp"
  lsPageTitle = "Claim Details"
  lsPageSubTitle = "Shop Assignment"
  liPageType = lcAfterClaimSearch
  liPageIcon = lcVehicle
  lsHelpPage = "CP_Shop_Information.htm"
%>
<!--#include file="includes/incGetUserSession.asp"-->
<!--#include file="includes/incSecurityCheck.asp"-->
<!--#include file="includes/incVerifyInsuranceCo.asp"-->
<!--#include file="includes/incVerifyCurrentClaim.asp"-->
<!--#include file="includes/incVerifyCurrentVehicle.asp"-->
<!--#include file="includes/incGetData.asp"-->
<!--#include file="includes/incCommonHTMLStartClm.asp"-->
<%
dim lbOverwriteExisting
lbOverwriteExisting = false
%>

<form name="frmMainForm" id="frmMainForm" method="POST" action="rspVehicleInfo.asp">
<!--#include file="includes/incHeaderTableWithIcons.asp"-->

<!--#include file="includes/incClaimCommonTop.asp"-->

<%
  Response.Write GetPersistedData(lsVehicleDetailCall, lsVehicleDetailGUID, "VehicleNavBar.xsl", "currentArea=" & lsPageSubTitle & lcDelimiter & "AssignmentID=" & liAssignmentID & lcDelimiter & "AssignmentType=" & lsAssignmentType & lcDelimiter & "AspectID=" & liAspectID & lcDelimiter & "LynxID=" & liLynxID & lcDelimiter & "VehNumber=" & liVehNumber, true)
%>

<!--#include file="includes/incClaimCommonAfterMenu.asp"-->

<%
  Response.Write GetPersistedData(lsVehicleDetailCall, lsVehicleDetailGUID, "VehicleHeader.xsl", "AspectID=" & liAspectID, true)
%>

<!--#include file="includes/incClaimCommonAfterHeader.asp"-->

<!--#include file="includes/incClaimDetailShort.asp"-->

<%
  Response.Write GetPersistedData(lsVehicleDetailCall, lsVehicleDetailGUID, "VehicleShop.xsl", "", true)
%>

<!--#include file="includes/incClaimCommonBottom.asp"-->

</form>

<form name="frmNewVehicle" id="frmNewVehicle" method="POST" action="rspClaimAddVehicle.asp" >
  <input type="hidden" name="hidLynxID" id="hidLynxID" value="<%=liLynxID%>"/>
</form>

<!--#include file="includes/incCommonHTMLEndClm.asp"-->
<!--#include file="includes/incCommonBottom.asp"-->
