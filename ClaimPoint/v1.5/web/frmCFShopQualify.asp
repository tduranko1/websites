<!--#include file="includes/incCommonTop.asp"-->
<!--#include file="includes/incGetData.asp"-->
<%

  Dim liCertifiedFirstID
  Dim liAddressZip
  Dim lsGUID
  Dim docXML
  Dim lsResultXML
  Dim lsShopInfoCall
  Dim ShopLoadID
  Dim ShopHasSubmitQualify
  
  liCertifiedFirstID = Request("CertifiedFirstID")
  liAddressZip = Request("AddressZip")
  lsGUID = Request.Cookies("CPSession")("guidArray0")
  
  lsShopInfoCall = "<CFShopLogin CertifiedFirstID=""" & server.urlencode(liCertifiedFirstID) & """ AddressZip=""" & server.urlencode(liAddressZip) & """/>"
  
  Set docXML = Server.CreateObject("MSXML2.DOMDocument.4.0")
  docXML.Async = False
  docXML.LoadXML GetData(lsShopInfoCall, "", "")
  
  If Not docXML.ParseError = 0 Then
    'Response.Write "<b>Error Code:</b> " & docXML.ParseError & "<br>"
    'Response.Write "<b>Error Description:</b> " & docXML.ParseError.reason & "<br>"
    'Response.Write "<b>Error File Position:</b> " & docXML.ParseError.filepos & "<br>"
    'Response.Write "<b>Error Line:</b> " & docXML.ParseError.line & "<br>"
    'Response.Write "<b>Error Line Position:</b> " & docXML.ParseError.linepos & "<br>"
    'Response.Write "<b>Error Source Text:</b> " & docXML.ParseError.srcText & "<br>"
  Else
    ShopLoadID = docXML.documentElement.getAttribute("ShopLoadID")
    ShopHasSubmitQualify = docXML.documentElement.getAttribute("QualificationsSubmittedDate")
  End If
  
  Set docXML = Nothing
  
  If ShopLoadID = "-1" Then
  	Response.Redirect "frmCFShopResults.asp?ShopQualified=1"
  ElseIf ShopHasSubmitQualify <> "" Then
  	Response.Redirect "frmCFShopInfo.asp?CertifiedFirstID=" & liCertifiedFirstID & "&AddressZip=" & liAddressZip
  End If

%>

<html>
<head>
<title>LYNXSelect Program Shop Qualification Overview</title>
<link href="includes/apd_style.css" type="text/css" rel="stylesheet">
<SCRIPT language="JavaScript" src="includes/incCFShopSMTAllInfo.js"></SCRIPT>
<SCRIPT language="JavaScript" src="includes/incCFShopInfo.js"></SCRIPT>

<script language="JavaScript" type="text/JavaScript">

function validateQualify()
{
	if (document.getElementById("txtAge").value > 255)
  {
    alert("Number of Years in Business max. value is 255." + "\n" +"Please re-enter this value before continuing.");
    return;
  }

  var lsXML = GetXmlElements();
  if (lsXML != "")
  {
    document.getElementById("txtlstrXML").value = lsXML;
    if (document.getElementById("txtlstrXML").value != "")
      document.frmCFQualifyShopXML.submit();
  }
}


function GetXmlElements()
{
  var lsElms = '<CFShopQualifyUpdate ShopLoadID="<%=ShopLoadID%>">';
  lsElms += "<ShopLoadID>" + "<![CDATA[<%=ShopLoadID%>]]>" + "</ShopLoadID>";
	
  var loDiv = document.getElementById("formDataDiv");
  var lcolElms = loDiv.getElementsByTagName("INPUT");
	var liElmsLength = lcolElms.length;
	for(var x=0; x<liElmsLength; x++)
	{
    if (lcolElms[x].type == "checkbox")
    {
      if (lcolElms[x].checked)
        lsElms += "<" + lcolElms[x].name + ">" + "<![CDATA[1]]>" + "</"+ lcolElms[x].name + ">";
      else
        lsElms += "<" + lcolElms[x].name + ">" + "<![CDATA[0]]>" + "</"+ lcolElms[x].name + ">";
    }
    else if (lcolElms[x].type == "radio")
    {
      lsElms += "<" + lcolElms[x].name + ">" + "<![CDATA[" + getRadioValue(lcolElms[x].name) + "]]>" + "</"+ lcolElms[x].name + ">";
      x++;
    }
    else if (lcolElms[x].type == "text")
    {
      lsElms += "<" + lcolElms[x].name + ">" + "<![CDATA[" + lcolElms[x].value + "]]>" + "</"+ lcolElms[x].name + ">";
    }
  }

  lsElms += "</CFShopQualifyUpdate>";
  return lsElms;
}


function getRadioValue(loName)
{
  var laryRadio = document.frmCFQualify.elements[loName];
  for (var i = 0 ; i < laryRadio.length ; i++)
  {
    if (laryRadio[i].checked)
      return laryRadio[i].value;
  }
  return "";
}


function cancelData()
{
  top.location = 'frmCFShopLogin.asp';
}


// PDF Popup
function showPDF(lsUrl)
{
  day = new Date();
  id = day.getTime();
  var lsQuery = "frmCFShowPDF.asp?pdfURL="+lsUrl;
  eval("page" + id + " = window.open(lsQuery, 'pdfWindow', 'toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=800,height=600');");
}


// Popup About page with application details
function callAbout()
{
  if (window.event.ctrlKey)
  {
    var lsDimensions = "dialogHeight:300px; dialogWidth:380px; "
    var lsSettings = "scroll:no; resizable:yes; status:no; help:no; center:yes; unadorned:yes;"
    var lsQuery = "frmAbout.asp";
    window.showModalDialog( lsQuery, window, lsDimensions + lsSettings );
  }
}

</script>

</head>

<BODY LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0 background="./images/page_bg.jpg" onLoad="InitPage()" >

<div align="left">
	
	<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
		<tr>
			<td width="169" align="left" valign="top">
  			<IMG SRC="images/logo_corner.jpg" ALT="" width="169" height="202">
			</td>
			<td align="left" valign="top">

        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" background="images/header_bg.jpg" height="132">
        	<tr>
        		<td height="50">&nbsp;</td>
        		<td align="right" valign="top" height="50">&nbsp;</td>
        	</tr>
        	<tr>
        		<td colspan=2 valign="top">&nbsp;</td>
        	</tr>
        	<tr>
        		<td align="left" valign="bottom" nowrap width="50%">
              <p class="header">
              LYNXSelect<sup style="font-size: 9px;">TM</sup> Registration for 
              <span id="spanlsPageTitle" lang="en-us"><em>CertifiedFirst </em><sup style="font-size: 9px;">TM</sup> Network</span>
            </td>
        		<td align="right" valign="bottom" width="50%">&nbsp;</td>
        	</tr>
        </table>
        <br>
        <DIV id="formDataDiv">
<%
If ShopLoadID > 0 Then
%>

	<table border="0" cellpadding="0" cellspacing="0" width="600">
		<tr valign="top">
			<td>
        <p class="login" style="font-size:11pt; font-weight:bold;">
          LYNXSelect Shop Qualification Overview
        </p>
			</td>
			<td width="200" rowspan="2" valign="top" align="center" nowrap>
        <img src="images/LYNXSelect_ logo.jpg" alt="" width="160" height="48" border="0">
        <br>
        <br>
        <!-- <img src="images/spacer.gif" border="0" width="10" height="40"> -->
        <img src="images/CertifiedFirst_logo.jpg" alt="" width="150" height="51" border="0">
			</td>
		</tr>
		<tr valign="top">
			<td>
        <br>
        <p class="login">
          LYNX Services is building a program of qualified repair facilities � the LYNXSelect<sup style="font-size: 9px;">TM</sup> Program.
          Shops that participate in the LYNXSelect Program will be eligible to receive referrals for insurance work.
        </p>
        <p class="login">
          In order to participate, interested shops must sign an
          <a href="javascript:showPDF('pdf/LYNXSelect Terms of Agreement.pdf')" title="LYNXSelect Terms of Agreement">agreement</a>
          with LYNX Services and must agree to a
          <a href="javascript:showPDF('pdf/Proudfoot Background Authorization.pdf')" title="Proudfoot Background Authorization">background check</a>
          that will be conducted by a third party.
        </p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
        <img src="images/spacer.gif" border="0" width="1" height="20">
			</td>
		</tr>
	</table>

	<table border="0" cellpadding="0" cellspacing="0" width="600">
		<tr>
			<td>
        <p style="color:#FF0000; font-size:12pt;">
          Please take a few moments to read the following guidelines and check mark all those that your business either meets or exceeds:
        </p>
			</td>
		</tr>
	</table>
  <br>
  
  <form name="frmCFQualify">

	<table border="0" cellpadding="2" cellspacing="0" width="600">
		<tr>
			<td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">
        Customer Service Requirements:
      </p></td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="CustomaryHoursFlag" type="checkbox" value="1"></td>
			<td class="login">
        Provide service during customary business hours, Monday through Friday, from 8:00 AM to 5:00 PM.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="ReceptionistFlag" type="checkbox" value="1"></td>
			<td class="login">
        Employ receptionist/office personnel to greet the customer, handle customer calls, and make appointments during normal business hours.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="MinimumWarrantyFlag" type="checkbox" value="1"></td>
			<td class="login">
        Provide a minimum three-year warranty on repair workmanship and refinish.
      </td>
		</tr>
		<tr>
			<td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="10"></td>
		</tr>

		<tr>
			<td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">
        Business and Regulatory Requirements:
      </p></td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="BusinessRegsFlag" type="checkbox" value="1"></td>
			<td class="login">
        Business must comply with local, state and federal business licenses, permits, and regulations.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="HazMatFlag" type="checkbox" value="1"></td>
			<td class="login">
        Business must comply with all safety and environmental standards when handling or disposing of hazardous materials.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="NoFeloniesFlag" type="checkbox" value="1"></td>
			<td class="login">
        Business owner/s have not been convicted of a felony in the past seven years.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="BusinessStabilityFlag" type="checkbox" value="1"></td>
			<td class="login">
        Business financial stability conditions:<br>
        <li>Business has not petitioned for bankruptcy in the past seven years.</li>
        <li>No adverse credit history or tax liens that could have a potentially adverse impact on the status of the operation.</li>
      </td>
		</tr>
		<tr>
			<td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="10"></td>
		</tr>

		<tr>
			<td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">
        Insurance requirements:
      </p></td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="EmployerLiabilityFlag" type="checkbox" value="1"></td>
			<td class="login">
        Minimum of $100,000 Employer�s Liability insurance.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="CommercialLiabilityFlag" type="checkbox" value="1"></td>
			<td class="login">
        Minimum of $1,000,000 Commercial General Liability Insurance.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="WorkersCompFlag" type="checkbox" value="1"></td>
			<td class="login">
        Workers Compensation Insurance to the statutory limits in the state where they have employees.
      </td>
		</tr>
		<tr>
			<td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="10"></td>
		</tr>

		<tr>
			<td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">
        Repair Equipment:
      </p></td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="FrameDiagnosticEquipFlag" type="checkbox" value="1"></td>
			<td class="login">
        Basic Unibody and Frame diagnostic equipment for dimension verification.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="FrameAnchoringPullingEquipFlag" type="checkbox" value="1"></td>
			<td class="login">
        Unibody and Frame 4-point anchoring and pulling equipment.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="MIGWeldersFlag" type="checkbox" value="1"></td>
			<td class="login">
        MIG welders � 110 and 220 capabilities.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="RefinishCapabilityFlag" type="checkbox" value="1"></td>
			<td class="login">
        Refinish capability for reproducing OEM type finish:<br>
        <li>Pressurized paint booth</li>
        <li>Paint mixing system capable of matching OEM finishes in texture and color</li>
        <li>Paint curing capability</li>
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="MechanicalRepairFlag" type="checkbox" value="1"></td>
			<td class="login">
        Provide mechanical repair services (in-house or sublet):<br>
        <li>Suspension</li>
        <li>Wheel alignment</li>
        <li>Engine and transmission</li>
        <li>Electrical</li>
        <li>Air bag service</li>
        <li>Air conditioning</li>
      </td>
		</tr>
		<tr>
			<td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="10"></td>
		</tr>

		<tr>
			<td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">
        Employee Training:
      </p></td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="CompetentEstimatorsFlag" type="checkbox" value="1"></td>
			<td class="login">
        Shop estimators must have sufficient training to be knowledgeable and proficient with estimating software of choice.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="EmployeeEducationFlag" type="checkbox" value="1"></td>
			<td class="login">
        Provide and document continuing education opportunities for their employees such as I-CAR training, ASE certification, or equivalent training.
      </td>
		</tr>
		<tr>
			<td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="10"></td>
		</tr>

		<tr>
			<td colspan="2"><p class="login" style="font-size:10pt; font-weight:bold;">
        Electronic Estimate and Communication Capability:
      </p></td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="ElectronicEstimatesFlag" type="checkbox" value="1"></td>
			<td class="login">
        Capable of providing an electronic estimate.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="DigitalPhotosFlag" type="checkbox" value="1"></td>
			<td class="login">
        Capable of providing digital photographs.
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="VANInetFlag" type="checkbox" value="1"></td>
			<td class="login">
        Able to accept assignments, and return estimates and digital photographs through electronic connectivity (VAN or Internet connection).
      </td>
		</tr>
		<tr valign="top">
			<td width="40" align="center"><input name="EFTFlag" type="checkbox" value="1"></td>
			<td class="login">
        Agree to accept EFT payments for authorized repair work following the completion of the repairs.
      </td>
		</tr>
		<tr>
			<td colspan="2"><img src="images/spacer.gif" border="0" width="1" height="10"></td>
		</tr>
	</table>

	<table border="0" cellpadding="2" cellspacing="0" width="600">
		<tr>
			<td colspan="2">
        <p class="login" style="font-size:11pt; font-weight:bold;">
          Additional Information
        </p>
      </td>
		</tr>
		<tr>
			<td colspan="2" class="login">
        <br>
        The following information, while not explicit requirements of the LYNXSelect program, helps us understand your business better.
        <br><br>
      </td>
		</tr>
		<tr valign="top">
			<td class="login" nowrap>
        <input type="radio" name="SecureStorageFlag" value="1">Yes
        <input type="radio" name="SecureStorageFlag" value="0">No&nbsp;&nbsp;
      </td>
			<td class="login">
        Shop provides a secure area for vehicles awaiting repairs or under repair.
      </td>
		</tr>
		<tr valign="top">
			<td class="login" nowrap>
        <input type="radio" name="ValidEPALicenseFlag" value="1">Yes
        <input type="radio" name="ValidEPALicenseFlag" value="0">No&nbsp;&nbsp;
      </td>
			<td class="login">
        Shop has a valid EPA License.
      </td>
		</tr>
		<tr valign="top">
			<td class="login" nowrap>
        <input type="radio" name="HydraulicLiftFlag" value="1">Yes
        <input type="radio" name="HydraulicLiftFlag" value="0">No&nbsp;&nbsp;
      </td>
			<td class="login">
        Shop has a hydraulic lift or ability to raise a vehicle to complete a safety inspection and a repair estimate.
      </td>
		</tr>
		<tr valign="top">
			<td class="login" nowrap>
        <input type="radio" name="ICARFlag" value="1">Yes
        <input type="radio" name="ICARFlag" value="0">No&nbsp;&nbsp;
      </td>
			<td class="login">
        At least one technician and one estimator have completed I-CAR Collision Repair 2000 Course (8 parts).
      </td>
		</tr>
		<tr valign="top">
			<td class="login" nowrap>
        <input type="radio" name="RefinishTrainedFlag" value="1">Yes
        <input type="radio" name="RefinishTrainedFlag" value="0">No&nbsp;&nbsp;
      </td>
			<td class="login">
        At least one refinish technician has completed minimum training requirements for refinishing.
      </td>
		</tr>
		<tr valign="top">
			<td class="login" nowrap>
        <input type="radio" name="ASEFlag" value="1">Yes
        <input type="radio" name="ASEFlag" value="0">No&nbsp;&nbsp;
      </td>
			<td class="login">
        At least one technician is ASE certified.
      </td>
		</tr>
		<tr valign="top">
			<td class="login" align="right">
        <input type="text" name="Age" id="txtAge" size="3" maxlength="3">&nbsp;&nbsp;
      </td>
			<td class="login">
        Number of years shop has been in business.
        <span style="color:#000000; font-weight:normal; font-size:7pt">(max. 255)</span>
      </td>
		</tr>
	</table>
  
  </form>

  <br>
  <table width="600" border="0" cellpadding="0" cellspacing="0" style="align:right;">
    <tr>
      <td width="100%">&nbsp;</td>
      <td align="right" nowrap="">
	  		<a href="javascript:cancelData()" title="Cancel" style="cursor:pointer; cursor:hand;"><img border="0" src="images/btn_cancel.gif" WIDTH="83" HEIGHT="31"/></a>
  		</td>
      <td nowrap="">
		   <img border="0" src="images/spacer.gif" WIDTH="20" HEIGHT="1"/>
		  </td>
      <td id="nextImgSrc" align="right" valign="top" nowrap>
  			<a href="javascript:validateQualify()" title="Continue" style="cursor:pointer; cursor:hand;"><img border="0" src="images/btn_continue.gif" WIDTH="83" HEIGHT="31"></a>
  		</td>
    </tr>
  </table>

<%
Else
%>
    <br/><br/>
    <table>
      <tr>
        <td nowrap="">
          <img border="0" src="images/warn_img.gif" WIDTH="16" HEIGHT="16" hspace="6" align="bottom" />
        </td>
        <td colspan="4" nowrap="" style="color:#FF0000; font-size:11pt; font-weight:bold;">
          Invalid Login.
        </td>
      </tr>
      <tr>
        <td nowrap=""> </td>
        <td nowrap=""><br/>
        <p style="font-size:13px">
          The <em>CertifiedFirst</em> Network ID or the Zip Code you entered is invalid.
          Please <a href="frmCFShopLogin.asp" target="_top">Login</a> again.
        </p>
        </td>
      </tr>
    </table>
  
<%
End if
%>
  
        </DIV>
		</td>
	</tr>
</table>

  <form name="frmCFQualifyShopXML" method="post" action="rspCFSubmitQualifications.asp">
    <textarea cols="" rows="" name="lstrXML" id="txtlstrXML" readonly style="visibility: hidden;"></textarea>
    <input type="hidden" name="CertifiedFirstID" value="<%=liCertifiedFirstID%>" style="visibility: hidden;">
    <input type="hidden" name="AddressZip" value="<%=liAddressZip%>" style="visibility: hidden;">
  </form>

</div>

<br>
<!--#include file="includes/incFooter.asp"-->

</body>
</html>
