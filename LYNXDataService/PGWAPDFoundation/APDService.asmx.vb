﻿'--------------------------------------------------------------
' Program: wsPGWAPDFoundation WebServices
' Author:  Thomas Duranko
' Date:    Feb 21, 2012
' Version: 1.0
'          1.1 - 26Oct2016 - TVD - Corrected the Deductible and Amount for UM vs UIM.  
'                                  I had them reversed.
'          1.2 - 25Jun2018 - TVD - Adding new functions for Claim and Vehicle dot net update
'                                  ClaimXP.aspx
'          1.3 - 16May2019 - TVD - Correcting an issue with UI and UIM deductibles.  
'                                  They were mapped backwards.
'          1.4 - 03Feb2020 - TVD - Set the Claimant to 0 on Web Services Assignments.  This
'                                  fixes an invoicing issue when the vehicle is > 0 by making
'                                  sure this value is not null
' 
' Description:  This site provides a number of APD service 
'               requestes via HTTP/XML SOAP requests for ASP.NET 
'               applications.  
'--------------------------------------------------------------
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail

Imports System.Xml
Imports System.IO
Imports System.Xml.Serialization
Imports System.Net

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://service.pgwapdfoundation.com/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class APDService
    Inherits System.Web.Services.WebService
    Dim APDUtilities As New APDUtilities
    Dim Claim As New APDClaim
    Dim Vehicles As New ArrayList
    Dim iInsuranceCompanyID As Integer = 0

    '--------------------------------------------
    ' This function returns is a simple ping test 
    ' to see if the server side service is working.
    '--------------------------------------------
    <WebMethod(Description:="This function returns is a simple ping test to see if the server side service is working.")>
    Public Function Ping() As String
        Try
            'Throw New System.Exception(String.Format("Error: {0}", "WebServices Ready..."))
            Return "Server Side WebServices Ready!!!"
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return "WebServices Failed: " & oExcept.ToString
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClientTowing
    ' This APD function returns rows from the utb_client_towing
    ' database table based on InsuranceCompanyID and AnalystID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns rows from the utb_client_towing database table based on InsuranceCompanyID and AnalystID")>
    Public Function GetClientTowing(ByVal InsuranceCompanyID As Integer, ByVal AnalystUserID As Integer) As List(Of utb_client_towing)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_client_towing
        Dim wsData As New List(Of utb_client_towing)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetClientTowing"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@InsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@InsuranceCompanyID").Value = InsuranceCompanyID

            cmdSelect.Parameters.Add("@AnalystUserID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@AnalystUserID").Value = AnalystUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_client_towing
                    wsClass.TowingID = Reader1.Item("TowingID")
                    wsClass.InsuranceCompanyID = Reader1.Item("InsuranceCompanyID")
                    wsClass.AnalystUserID = Reader1.Item("AnalystUserID")
                    wsClass.CusAddress = Reader1.Item("CusAddress")
                    wsClass.CusAltPhone = Reader1.Item("CusAltPhone")
                    wsClass.CusCity = Reader1.Item("CusCity")
                    wsClass.CusEmail = Reader1.Item("CusEmail")
                    wsClass.CusHomePhone = Reader1.Item("CusHomePhone")
                    wsClass.CusName = Reader1.Item("CusName")
                    wsClass.CusState = Reader1.Item("CusState")
                    wsClass.CusWorkPhone = Reader1.Item("CusWorkPhone")
                    wsClass.CusZip = Reader1.Item("CusZip")
                    wsClass.DesAddress = Reader1.Item("DesAddress")
                    wsClass.DesCity = Reader1.Item("DesCity")
                    wsClass.DesFacilityContactName = Reader1.Item("DesFacilityContactName")
                    wsClass.DesHoursOfOperation = Reader1.Item("DesHoursOfOperation")
                    wsClass.DesPhone = Reader1.Item("DesPhone")
                    wsClass.DesState = Reader1.Item("DesState")
                    wsClass.DesZip = Reader1.Item("DesZip")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.LocAddress = Reader1.Item("LocAddress")
                    wsClass.LocCity = Reader1.Item("LocCity")
                    wsClass.LocFacilityContactName = Reader1.Item("LocFacilityContactName")
                    wsClass.LocHoursOfOperation = Reader1.Item("LocHoursOfOperation")
                    wsClass.LocPhone = Reader1.Item("LocPhone")
                    wsClass.LocState = Reader1.Item("LocState")
                    wsClass.LocZip = Reader1.Item("LocZip")
                    wsClass.VehConditionDescription = Reader1.Item("VehConditionDescription")
                    wsClass.VehLicensePlateNumber = Reader1.Item("VehLicensePlateNumber")
                    wsClass.VehMake = Reader1.Item("VehMake")
                    wsClass.VehModel = Reader1.Item("VehModel")
                    wsClass.VehVINState = Reader1.Item("VehVINState")
                    wsClass.VehYear = Reader1.Item("VehYear")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_client_towing
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_client_towing
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: InsClientTowing
    ' This APD function returns rows from the utb_client_towing
    ' database table based on InsuranceCompanyID and AnalystID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns rows from the utb_client_towing database table based on InsuranceCompanyID and AnalystID")>
    Public Function InsClientTowing(
        ByVal InsuranceCompanyID As Integer _
        , ByVal AnalystUserID As Integer _
        , ByVal ClaimNumber As String _
        , ByVal CusAddress As String _
        , ByVal CusAltPhone As String _
        , ByVal CusCity As String _
        , ByVal CusEmail As String _
        , ByVal CusHomePhone As String _
        , ByVal CusName As String _
        , ByVal CusState As String _
        , ByVal CusWorkPhone As String _
        , ByVal CusZip As String _
        , ByVal DesAddress As String _
        , ByVal DesCity As String _
        , ByVal DesFacilityContactName As String _
        , ByVal DesHoursOfOperation As String _
        , ByVal DesPhone As String _
        , ByVal DesState As String _
        , ByVal DesZip As String _
        , ByVal LynxID As Integer _
        , ByVal LocAddress As String _
        , ByVal LocCity As String _
        , ByVal LocFacilityContactName As String _
        , ByVal LocHoursOfOperation As String _
        , ByVal LocPhone As String _
        , ByVal LocState As String _
        , ByVal LocZip As String _
        , ByVal VehConditionDescription As String _
        , ByVal VehLicensePlateNumber As String _
        , ByVal VehMake As String _
        , ByVal VehModel As String _
        , ByVal VehVINState As String _
        , ByVal VehYear As Integer _
        , ByVal Comments As String _
        , ByVal SysLastUserID As Integer
        )

        Dim Conn As New SqlConnection
        Dim Cmd As New SqlClient.SqlCommand

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            Cmd.CommandType = CommandType.StoredProcedure
            Cmd.CommandText = "uspInsClientTowing"
            Cmd.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            Cmd.Parameters.Add("@InsuranceCompanyID", SqlDbType.Int, 4)
            Cmd.Parameters("@InsuranceCompanyID").Value = InsuranceCompanyID

            Cmd.Parameters.Add("@AnalystUserID", SqlDbType.Int, 4)
            Cmd.Parameters("@AnalystUserID").Value = AnalystUserID

            Cmd.Parameters.Add("@ClaimNumber", SqlDbType.VarChar, 50)
            Cmd.Parameters("@ClaimNumber").Value = ClaimNumber

            Cmd.Parameters.Add("@CusAddress", SqlDbType.VarChar, 50)
            Cmd.Parameters("@CusAddress").Value = CusAddress

            Cmd.Parameters.Add("@CusAltPhone", SqlDbType.VarChar, 15)
            Cmd.Parameters("@CusAltPhone").Value = CusAltPhone

            Cmd.Parameters.Add("@CusCity", SqlDbType.VarChar, 30)
            Cmd.Parameters("@CusCity").Value = CusCity

            Cmd.Parameters.Add("@CusEmail", SqlDbType.VarChar, 50)
            Cmd.Parameters("@CusEmail").Value = CusEmail

            Cmd.Parameters.Add("@CusHomePhone", SqlDbType.VarChar, 15)
            Cmd.Parameters("@CusHomePhone").Value = CusHomePhone

            Cmd.Parameters.Add("@CusName", SqlDbType.VarChar, 100)
            Cmd.Parameters("@CusName").Value = CusName

            Cmd.Parameters.Add("@CusState", SqlDbType.VarChar, 2)
            Cmd.Parameters("@CusState").Value = CusState

            Cmd.Parameters.Add("@CusWorkPhone", SqlDbType.VarChar, 15)
            Cmd.Parameters("@CusWorkPhone").Value = CusWorkPhone

            Cmd.Parameters.Add("@CusZip", SqlDbType.VarChar, 8)
            Cmd.Parameters("@CusZip").Value = CusZip

            Cmd.Parameters.Add("@DesAddress", SqlDbType.VarChar, 50)
            Cmd.Parameters("@DesAddress").Value = DesAddress

            Cmd.Parameters.Add("@DesCity", SqlDbType.VarChar, 30)
            Cmd.Parameters("@DesCity").Value = DesCity

            Cmd.Parameters.Add("@DesFacilityContactName", SqlDbType.VarChar, 100)
            Cmd.Parameters("@DesFacilityContactName").Value = DesFacilityContactName

            Cmd.Parameters.Add("@DesHoursOfOperation", SqlDbType.VarChar, 150)
            Cmd.Parameters("@DesHoursOfOperation").Value = DesHoursOfOperation

            Cmd.Parameters.Add("@DesPhone", SqlDbType.VarChar, 15)
            Cmd.Parameters("@DesPhone").Value = DesPhone

            Cmd.Parameters.Add("@DesState", SqlDbType.VarChar, 2)
            Cmd.Parameters("@DesState").Value = DesState

            Cmd.Parameters.Add("@DesZip", SqlDbType.VarChar, 8)
            Cmd.Parameters("@DesZip").Value = DesZip

            Cmd.Parameters.Add("@LynxID", SqlDbType.Int, 4)
            Cmd.Parameters("@LynxID").Value = LynxID

            Cmd.Parameters.Add("@LocAddress", SqlDbType.VarChar, 50)
            Cmd.Parameters("@LocAddress").Value = LocAddress

            Cmd.Parameters.Add("@LocCity", SqlDbType.VarChar, 30)
            Cmd.Parameters("@LocCity").Value = LocCity

            Cmd.Parameters.Add("@LocFacilityContactName", SqlDbType.VarChar, 100)
            Cmd.Parameters("@LocFacilityContactName").Value = LocFacilityContactName

            Cmd.Parameters.Add("@LocHoursOfOperation", SqlDbType.VarChar, 150)
            Cmd.Parameters("@LocHoursOfOperation").Value = LocHoursOfOperation

            Cmd.Parameters.Add("@LocPhone", SqlDbType.VarChar, 15)
            Cmd.Parameters("@LocPhone").Value = LocPhone

            Cmd.Parameters.Add("@LocState", SqlDbType.VarChar, 2)
            Cmd.Parameters("@LocState").Value = LocState

            Cmd.Parameters.Add("@LocZip", SqlDbType.VarChar, 8)
            Cmd.Parameters("@LocZip").Value = LocZip

            Cmd.Parameters.Add("@VehConditionDescription", SqlDbType.VarChar, 255)
            Cmd.Parameters("@VehConditionDescription").Value = VehConditionDescription

            Cmd.Parameters.Add("@VehLicensePlateNumber", SqlDbType.VarChar, 10)
            Cmd.Parameters("@VehLicensePlateNumber").Value = VehLicensePlateNumber

            Cmd.Parameters.Add("@VehMake", SqlDbType.VarChar, 50)
            Cmd.Parameters("@VehMake").Value = VehMake

            Cmd.Parameters.Add("@VehModel", SqlDbType.VarChar, 50)
            Cmd.Parameters("@VehModel").Value = VehModel

            Cmd.Parameters.Add("@VehVINState", SqlDbType.VarChar, 17)
            Cmd.Parameters("@VehVINState").Value = VehVINState

            Cmd.Parameters.Add("@VehYear", SqlDbType.Int, 4)
            Cmd.Parameters("@VehYear").Value = VehYear

            Cmd.Parameters.Add("@Comments", SqlDbType.VarChar, 1000)
            Cmd.Parameters("@Comments").Value = Comments

            Cmd.Parameters.Add("@SysLastUserID", SqlDbType.Int, 4)
            Cmd.Parameters("@SysLastUserID").Value = SysLastUserID

            '-------------------------------------------
            ' Output parameters from the stored proc
            '-------------------------------------------
            Dim ComOutput As New SqlParameter("@TowingID", SqlDbType.Int, 4)
            ComOutput.Direction = ParameterDirection.Output
            Cmd.Parameters.Add(ComOutput)

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            'Dim sBody As String
            'sBody = InsuranceCompanyID & ", " & AnalystUserID & ", " & ClaimNumber & ", " & CusAddress & ", " & CusAltPhone & ", " & CusCity & ", " & CusEmail & ", " & CusHomePhone & ", " & CusName & ", " & CusState & ", " & CusWorkPhone & ", " & CusZip & ", " & DesAddress & ", " & DesCity & ", " & DesFacilityContactName & ", " & DesHoursOfOperation & ", " & DesPhone & ", " & DesState & ", " & DesZip & ", " & LynxID & ", " & LocAddress & ", " & LocCity & ", " & LocFacilityContactName & ", " & LocHoursOfOperation & ", " & LocPhone & ", " & LocState & ", " & LocZip & ", " & VehConditionDescription & ", " & VehLicensePlateNumber & ", " & VehMake & ", " & VehModel & ", " & VehVINState & ", " & VehYear & ", " & SysLastUserID
            'SendMail("tduranko@pgwglass.com", "tom@pgwglass.com", "", "Debug", sBody, "rocSMTP01.pgw.local")
            Cmd.ExecuteNonQuery()

            'SendMail("tduranko@pgwglass.com", "tom@pgwglass.com", "", "Debug1", Cmd.Parameters("@TowingID").Value, "rocSMTP01.pgw.local")

            If Cmd.Parameters("@TowingID").Value > 0 Then
                Return Cmd.Parameters("@TowingID").Value
            Else
                Throw New System.Exception(String.Format("Error: {0}", "APD WebServices failed to add a towing record (InsClientTowing)..."))
            End If

        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'SendMail("tduranko@pgwglass.com", "tom@pgwglass.com", "", "Debug2", oExcept.ToString, "rocSMTP01.pgw.local")
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return "WebServices Failed: " & oExcept.ToString
        Finally
            Cmd = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: SendMail
    ' This APD function sends emails
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function sends emails")>
    Public Function SendMail(ByVal SendTo As String, ByVal SendFrom As String, ByVal SendBCC As String, ByVal Subject As String,
                             ByVal Body As String, ByVal MailHost As String) As String

        Dim oMailMessage As New MailMessage
        Dim oMail As New System.Net.Mail.SmtpClient
        Dim mailTo As String()

        Try
            'oMail.Credentials = New NetworkCredential("MyuserName", "MyPass")
            If SendFrom = "" Then
                SendFrom = "NoReply@PGWGlass.com"
            End If

            oMailMessage.From = New MailAddress(SendFrom)

            If SendTo.Trim() <> String.Empty Then
                mailTo = SendTo.Split(";"c, ","c)
            End If

            For index As Integer = 0 To mailTo.Length - 1
                oMailMessage.To.Add(New MailAddress(mailTo(index)))
            Next

            If SendBCC <> "" Then
                oMailMessage.Bcc.Add(SendBCC)
            End If
            oMailMessage.Subject = Subject
            oMailMessage.IsBodyHtml = True
            oMailMessage.Body = Body
            oMailMessage.Priority = MailPriority.Normal

            'oMail.Host = MailHost
            oMail.Send(oMailMessage)

            Return "0"

        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return "SendMail WebServices Failed: " & oExcept.ToString
        End Try

    End Function

    '-----------------------------------------------------------
    ' APD: GetNewLynxIDNumber
    ' This APD function gets the next available LynxID number from FNOL
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function gets the next available LynxID number from FNOL")>
    Public Function GetNewLynxIDNumber() As String
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader As SqlClient.SqlDataReader
        Dim sLynxID As String

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "sp_GetRecID"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectStringFNOL")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            ' NONE

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader = cmdSelect.ExecuteReader

            If Reader.HasRows Then
                Reader.Read()
                sLynxID = Reader.Item(0)
            Else
                sLynxID = 0
            End If

            Return sLynxID

        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return "Get LynxID number WebServices failed: " & oExcept.ToString
        Finally
            cmdSelect = Nothing
            Reader.Close()
            Conn.Close()
        End Try

    End Function

    '-----------------------------------------------------------
    ' APD: GetExistingInscCompanies
    ' This APD function returns information related to client
    ' setup from the database table.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns information related to client setup from the database table.")>
    Public Function GetExistingInscCompanies() As List(Of utb_insurance)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_insurance
        Dim wsData As New List(Of utb_insurance)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetClientInfo"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            'cmdSelect.Parameters.Add("@ReturnType", SqlDbType.VarChar, 15)
            'cmdSelect.Parameters("@ReturnType").Value = ReturnType

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_insurance
                    wsClass.InsuranceCompanyID = Reader1.Item("InsuranceCompanyID")
                    wsClass.DeskAuditPreferredCommunicationMethodID = Reader1.Item("DeskAuditPreferredCommunicationMethodID")
                    wsClass.FedTaxId = Reader1.Item("FedTaxId")
                    wsClass.IngresAccountingId = Reader1.Item("IngresAccountingId")
                    wsClass.InvoicingModelPaymentCD = Reader1.Item("InvoicingModelPaymentCD")
                    wsClass.InvoiceMethodCD = Reader1.Item("InvoiceMethodCD")
                    wsClass.LicenseDeterminationCD = Reader1.Item("LicenseDeterminationCD")
                    wsClass.Name = Reader1.Item("Name")
                    wsClass.PhoneAreaCode = Reader1.Item("PhoneAreaCode")
                    wsClass.PhoneExchangeNumber = Reader1.Item("PhoneExchangeNumber")
                    wsClass.PhoneExtensionNumber = Reader1.Item("PhoneExtensionNumber")
                    wsClass.PhoneUnitNumber = Reader1.Item("PhoneUnitNumber")
                    wsClass.ReturnDocDestinationCD = Reader1.Item("ReturnDocDestinationCD")
                    wsClass.ReturnDocPackageTypeCD = Reader1.Item("ReturnDocPackageTypeCD")
                    wsClass.ReturnDocRoutingCD = Reader1.Item("ReturnDocRoutingCD")
                    wsClass.TotalLossValuationWarningPercentage = Reader1.Item("TotalLossValuationWarningPercentage")
                    wsClass.TotalLossWarningPercentage = Reader1.Item("TotalLossWarningPercentage")
                    wsClass.WarrantyPeriodRefinishMinCD = Reader1.Item("WarrantyPeriodRefinishMinCD")
                    wsClass.WarrantyPeriodWorkmanshipMinCD = Reader1.Item("WarrantyPeriodWorkmanshipMinCD")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.Address1 = Reader1.Item("Address1")
                    wsClass.Address2 = Reader1.Item("Address2")
                    wsClass.AddressCity = Reader1.Item("AddressCity")
                    wsClass.AddressState = Reader1.Item("AddressState")
                    wsClass.AddressZip = Reader1.Item("AddressZip")
                    wsClass.AssignmentAtSelectionFlag = Reader1.Item("AssignmentAtSelectionFlag")
                    wsClass.AuthorizeLaborRatesFlag = Reader1.Item("AuthorizeLaborRatesFlag")
                    wsClass.BillingModelCD = Reader1.Item("BillingModelCD")
                    wsClass.BusinessTypeCD = Reader1.Item("BusinessTypeCD")
                    wsClass.CarrierLynxContactPhone = Reader1.Item("CarrierLynxContactPhone")
                    wsClass.CFLogoDisplayFlag = Reader1.Item("CFLogoDisplayFlag")
                    wsClass.ClaimPointCarrierRepSelFlag = Reader1.Item("ClaimPointCarrierRepSelFlag")
                    wsClass.ClaimPointDocumentUploadFlag = Reader1.Item("ClaimPointDocumentUploadFlag")
                    wsClass.ClientAccessFlag = Reader1.Item("ClientAccessFlag")
                    wsClass.DemoFlag = Reader1.Item("DemoFlag")
                    wsClass.DeskAuditCompanyCD = Reader1.Item("DeskAuditCompanyCD")
                    wsClass.DeskReviewLicenseReqFlag = Reader1.Item("DeskReviewLicenseReqFlag")
                    wsClass.EarlyBillFlag = Reader1.Item("EarlyBillFlag")
                    wsClass.EarlyBillStartDate = Reader1.Item("EarlyBillStartDate")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.FaxAreaCode = Reader1.Item("FaxAreaCode")
                    wsClass.FaxExchangeNumber = Reader1.Item("FaxExchangeNumber")
                    wsClass.FaxExtensionNumber = Reader1.Item("FaxExtensionNumber")
                    wsClass.FaxUnitNumber = Reader1.Item("FaxUnitNumber")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")

                    wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_insurance
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_insurance
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetExistingInscCompaniesSimple
    ' This APD function returns simple information related to client
    ' setup from the database table.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns simple information related to client setup from the database table.")>
    Public Function GetExistingInscCompaniesSimple() As List(Of utb_insurance_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_insurance_simple
        Dim wsData As New List(Of utb_insurance_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetClientInfo"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            'cmdSelect.Parameters.Add("@ReturnType", SqlDbType.VarChar, 15)
            'cmdSelect.Parameters("@ReturnType").Value = ReturnType

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_insurance_simple
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.InsuranceCompanyID = Reader1.Item("InsuranceCompanyID")
                    wsClass.IngresAccountingId = Reader1.Item("IngresAccountingId")
                    wsClass.Name = Reader1.Item("Name")
                    wsClass.Address1 = Reader1.Item("Address1")
                    wsClass.Address2 = Reader1.Item("Address2")
                    wsClass.AddressCity = Reader1.Item("AddressCity")
                    wsClass.AddressState = Reader1.Item("AddressState")
                    wsClass.AddressZip = Reader1.Item("AddressZip")

                    If Trim(Reader1.Item("PhoneAreaCode")) <> "" Then
                        wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    End If
                    If Trim(Reader1.Item("FaxAreaCode")) <> "" Then
                        wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    End If

                    '----------------------------------------
                    ' Remove default ones
                    '----------------------------------------
                    'wsClass.TotalLossValuationWarningPercentage.Remove(0)
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_insurance_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_insurance_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetExistingInscCompaniesDetailed
    ' This APD function returns detailed information related to client
    ' setup from the database table by InsuranceCompanyID.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns detailed information related to client setup from the database table by InsuranceCompanyID")>
    Public Function GetExistingInscCompaniesDetailed(ByVal InsuranceCompanyID As Integer) As List(Of utb_insurance)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_insurance
        Dim wsData As New List(Of utb_insurance)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetClientInfo"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_insurance
                    wsClass.InsuranceCompanyID = Reader1.Item("InsuranceCompanyID")
                    wsClass.DeskAuditPreferredCommunicationMethodID = Reader1.Item("DeskAuditPreferredCommunicationMethodID")
                    wsClass.FedTaxId = Reader1.Item("FedTaxId")
                    wsClass.IngresAccountingId = Reader1.Item("IngresAccountingId")
                    wsClass.InvoicingModelPaymentCD = Reader1.Item("InvoicingModelPaymentCD")
                    wsClass.InvoiceMethodCD = Reader1.Item("InvoiceMethodCD")
                    wsClass.LicenseDeterminationCD = Reader1.Item("LicenseDeterminationCD")
                    wsClass.Name = Reader1.Item("Name")
                    wsClass.PhoneAreaCode = Reader1.Item("PhoneAreaCode")
                    wsClass.PhoneExchangeNumber = Reader1.Item("PhoneExchangeNumber")
                    wsClass.PhoneExtensionNumber = Reader1.Item("PhoneExtensionNumber")
                    wsClass.PhoneUnitNumber = Reader1.Item("PhoneUnitNumber")
                    wsClass.ReturnDocDestinationCD = Reader1.Item("ReturnDocDestinationCD")
                    wsClass.ReturnDocPackageTypeCD = Reader1.Item("ReturnDocPackageTypeCD")
                    wsClass.ReturnDocRoutingCD = Reader1.Item("ReturnDocRoutingCD")
                    wsClass.TotalLossValuationWarningPercentage = Reader1.Item("TotalLossValuationWarningPercentage")
                    wsClass.TotalLossWarningPercentage = Reader1.Item("TotalLossWarningPercentage")
                    wsClass.WarrantyPeriodRefinishMinCD = Reader1.Item("WarrantyPeriodRefinishMinCD")
                    wsClass.WarrantyPeriodWorkmanshipMinCD = Reader1.Item("WarrantyPeriodWorkmanshipMinCD")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.Address1 = Reader1.Item("Address1")
                    wsClass.Address2 = Reader1.Item("Address2")
                    wsClass.AddressCity = Reader1.Item("AddressCity")
                    wsClass.AddressState = Reader1.Item("AddressState")
                    wsClass.AddressZip = Reader1.Item("AddressZip")
                    wsClass.AssignmentAtSelectionFlag = Reader1.Item("AssignmentAtSelectionFlag")
                    wsClass.AuthorizeLaborRatesFlag = Reader1.Item("AuthorizeLaborRatesFlag")
                    wsClass.BillingModelCD = Reader1.Item("BillingModelCD")
                    wsClass.BusinessTypeCD = Reader1.Item("BusinessTypeCD")
                    wsClass.CarrierLynxContactPhone = Reader1.Item("CarrierLynxContactPhone")
                    wsClass.CFLogoDisplayFlag = Reader1.Item("CFLogoDisplayFlag")
                    wsClass.ClaimPointCarrierRepSelFlag = Reader1.Item("ClaimPointCarrierRepSelFlag")
                    wsClass.ClaimPointDocumentUploadFlag = Reader1.Item("ClaimPointDocumentUploadFlag")
                    wsClass.ClientAccessFlag = Reader1.Item("ClientAccessFlag")
                    wsClass.DemoFlag = Reader1.Item("DemoFlag")
                    wsClass.DeskAuditCompanyCD = Reader1.Item("DeskAuditCompanyCD")
                    wsClass.DeskReviewLicenseReqFlag = Reader1.Item("DeskReviewLicenseReqFlag")
                    wsClass.EarlyBillFlag = Reader1.Item("EarlyBillFlag")
                    wsClass.EarlyBillStartDate = Reader1.Item("EarlyBillStartDate")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.FaxAreaCode = Reader1.Item("FaxAreaCode")
                    wsClass.FaxExchangeNumber = Reader1.Item("FaxExchangeNumber")
                    wsClass.FaxExtensionNumber = Reader1.Item("FaxExtensionNumber")
                    wsClass.FaxUnitNumber = Reader1.Item("FaxUnitNumber")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")

                    wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_insurance
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_insurance
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetOfficeSimple
    ' This APD function returns simple information related to client
    ' office setup from the database table by InsuranceCompanyID.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns simple information related to client office setup from the database table by InsuranceCompanyID")>
    Public Function GetOfficeSimple(ByVal InsuranceCompanyID As Integer) As List(Of utb_office_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_office_simple
        Dim wsData As New List(Of utb_office_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetOfficeInfo"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_office_simple
                    wsClass.OfficeID = Reader1.Item("OfficeID")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.OfficeName = Reader1.Item("OfficeName")
                    wsClass.Address1 = Reader1.Item("Address1")
                    wsClass.Address2 = Reader1.Item("Address2")
                    wsClass.AddressCity = Reader1.Item("AddressCity")
                    wsClass.AddressState = Reader1.Item("AddressState")
                    wsClass.AddressZip = Reader1.Item("AddressZip")

                    wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_office_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_office_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetOfficeDetails
    ' This APD function returns detailed information related to client
    ' office setup from the database table by InsuranceCompanyID.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns detailed information related to client office setup from the database table by InsuranceCompanyID")>
    Public Function GetOfficeDetails(ByVal InsuranceCompanyID As Integer) As List(Of utb_office)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_office
        Dim wsData As New List(Of utb_office)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetOfficeInfo"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_office
                    wsClass.OfficeID = Reader1.Item("OfficeID")
                    wsClass.InsuranceCompanyID = Reader1.Item("InsuranceCompanyID")
                    wsClass.OfficeName = Reader1.Item("OfficeName")
                    wsClass.ClientOfficeId = Reader1.Item("ClientOfficeID")
                    wsClass.Address1 = Reader1.Item("Address1")
                    wsClass.Address2 = Reader1.Item("Address2")
                    wsClass.AddressCity = Reader1.Item("AddressCity")
                    wsClass.AddressState = Reader1.Item("AddressState")
                    wsClass.AddressZip = Reader1.Item("AddressZip")
                    wsClass.MailingAddress1 = Reader1.Item("MailingAddress1")
                    wsClass.MailingAddress2 = Reader1.Item("MailingAddress2")
                    wsClass.MailingAddressCity = Reader1.Item("MailingAddressCity")
                    wsClass.MailingAddressState = Reader1.Item("MailingAddressState")
                    wsClass.MailingAddressZip = Reader1.Item("MailingAddressZip")
                    wsClass.CCEmailAddress = Reader1.Item("CCEmailAddress")
                    wsClass.PhoneAreaCode = Reader1.Item("PhoneAreaCode")
                    wsClass.PhoneExchangeNumber = Reader1.Item("PhoneExchangeNumber")
                    wsClass.PhoneExtensionNumber = Reader1.Item("PhoneExtensionNumber")
                    wsClass.PhoneUnitNumber = Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxAreaCode = Reader1.Item("FaxAreaCode")
                    wsClass.FaxExchangeNumber = Reader1.Item("FaxExchangeNumber")
                    wsClass.FaxExtensionNumber = Reader1.Item("FaxExtensionNumber")
                    wsClass.FaxUnitNumber = Reader1.Item("FaxUnitNumber")
                    wsClass.ReturnDocDestinationValue = Reader1.Item("ReturnDocDestinationValue")
                    wsClass.ReturnDocEmailAddress = Reader1.Item("ReturnDocEmailAddress")
                    wsClass.ReturnDocFaxAreaCode = Reader1.Item("ReturnDocFaxAreaCode")
                    wsClass.ReturnDocFaxExchangeNumber = Reader1.Item("ReturnDocFaxExchangeNumber")
                    wsClass.ReturnDocFaxExtensionNumber = Reader1.Item("ReturnDocFaxExtensionNumber")
                    wsClass.ReturnDocFaxUnitNumber = Reader1.Item("ReturnDocFaxUnitNumber")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")

                    wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    wsClass.ReturnDocFaxCombined = Reader1.Item("ReturnDocFaxAreaCode") & "-" & Reader1.Item("ReturnDocFaxExchangeNumber") & "-" & Reader1.Item("ReturnDocFaxUnitNumber")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_office
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_office
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClientSimpleAssignments
    ' This APD function returns the simple current service channels
    ' from the database table by InsuranceCompany.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns the simple current service channels from the database table by InsuranceCompanyID")>
    Public Function GetClientAssignmentsSimple(ByVal InsuranceCompanyID As Integer) As List(Of utb_client_assignment_type_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_client_assignment_type_simple
        Dim wsData As New List(Of utb_client_assignment_type_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetClientAssignments"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_client_assignment_type_simple
                    wsClass.AssignmentTypeID = Reader1.Item("AssignmentTypeID")
                    wsClass.Name = Reader1.Item("AssignmentName")
                    wsClass.ServiceChannelDefaultCD = Reader1.Item("ServiceChannelDefaultCD")
                    wsClass.ClientAuthorizesPaymentFlag = Reader1.Item("ClientAuthorizesPaymentFlag")
                    wsClass.InvoicingModelBillingCD = Reader1.Item("InvoicingModelBillingCD")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_client_assignment_type_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_client_assignment_type_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClientCoveragesSimple
    ' This APD function returns the simple client coverages
    ' from the database table by InsuranceCompany.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns the simple client coverages from the database table by InsuranceCompanyID")>
    Public Function GetClientCoveragesSimple(ByVal InsuranceCompanyID As Integer) As List(Of utb_client_coverage_type_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_client_coverage_type_simple
        Dim wsData As New List(Of utb_client_coverage_type_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "GetClientCoverages"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_client_coverage_type_simple
                    wsClass.ClientCoverageTypeID = Reader1.Item("ClientCoverageTypeID")
                    wsClass.Name = Reader1.Item("CoverageName")
                    wsClass.AdditionalCoverageFlag = Reader1.Item("AdditionalCoverageFlag")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_client_coverage_type_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_client_coverage_type_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClientClaimAspectsSimple
    ' This APD function returns the simple client claim aspects
    ' from the database table by InsuranceCompany.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns the simple client claim aspects from the database table by InsuranceCompanyID")>
    Public Function GetClientClaimAspectsSimple(ByVal InsuranceCompanyID As Integer) As List(Of utb_client_claim_aspect_type_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_client_claim_aspect_type_simple
        Dim wsData As New List(Of utb_client_claim_aspect_type_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "GetClientClaimAspects"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_client_claim_aspect_type_simple
                    wsClass.ClaimAspectTypeID = Reader1.Item("ClaimAspectTypeID")
                    wsClass.Name = Reader1.Item("AspectName")
                    wsClass.ProfileRequiredFlag = Reader1.Item("ProfileRequiredFlag")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_client_claim_aspect_type_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_client_claim_aspect_type_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClientReportsSimple
    ' This APD function returns the simple client reports
    ' from the database table by InsuranceCompany.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns the simple client reports from the database table by InsuranceCompanyID")>
    Public Function GetClientReportsSimple(ByVal InsuranceCompanyID As Integer) As List(Of utb_client_report_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_client_report_simple
        Dim wsData As New List(Of utb_client_report_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetClientReports"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_client_report_simple
                    wsClass.ClientReportID = Reader1.Item("ClientReportID")
                    wsClass.OfficeID = IIf(Reader1.Item("OfficeID") = 0, "", Reader1.Item("OfficeID"))
                    wsClass.Description = Reader1.Item("ReportName")
                    wsClass.OfficeLevelUserViewFlag = Reader1.Item("OfficeLevelUserViewFlag")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_client_report_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_client_report_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetAdminClaimpointMenu
    ' This APD function returns the Claimpoint menu items
    ' from the database table by MenuLevel.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns the Claimpoint menu items from the database table by MenuLevel.")>
    Public Function GetAdminClaimpointMenu(ByVal MenuLevel As String) As List(Of utb_admin_claimpoint_menu)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_admin_claimpoint_menu
        Dim wsData As New List(Of utb_admin_claimpoint_menu)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetClaimPointMenu"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@vMenuLevel", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@vMenuLevel").Value = MenuLevel

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_admin_claimpoint_menu
                    wsClass.MenuID = Reader1.Item("MenuID")
                    wsClass.MenuLevel = Reader1.Item("MenuLevel")
                    wsClass.NavURL = Reader1.Item("NavURL")
                    wsClass.MouseOverIcon = Reader1.Item("MouseOverIcon")
                    wsClass.MouseOutIcon = Reader1.Item("MouseOutIcon")
                    wsClass.SortOrder = Reader1.Item("SortOrder")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")

                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_admin_claimpoint_menu
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_admin_claimpoint_menu
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminCreateNewClient
    ' This APD function creates a new APD Client
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client")>
    Public Function AdminCreateNewClient(
        ByVal InsuranceCompanyID As Integer _
        , ByVal PaymentCompanyID As Integer _
        , ByVal LYNXContactPhone As String _
        , ByVal CompanyName As String _
        , ByVal CompanyNameShort As String _
        , ByVal Address1 As String _
        , ByVal Address2 As String _
        , ByVal City As String _
        , ByVal State As String _
        , ByVal Zip As String _
        , ByVal AssignmentAtSelectionFlag As Boolean _
        , ByVal BillingModel As String _
        , ByVal CFLogoDisplayFlag As Boolean _
        , ByVal ClaimPointDocumentUploadFlag As Boolean _
        , ByVal ClaimPointCarrierRepSelFlag As Boolean _
        , ByVal ClientAccessFlag As Boolean _
        , ByVal ClientProduct As String _
        , ByVal CompanyFax As String _
        , ByVal CompanyPhone As String _
        , ByVal DemoFlag As Boolean _
        , ByVal DeskReviewLicenseReqFlag As Boolean _
        , ByVal EarlyBilling As Boolean _
        , ByVal EnabledFlag As Boolean _
        , ByVal FedTaxID As String _
        , ByVal PreferredCommunicationMethodID As Integer _
        , ByVal TotalLossValuationPercentage As Decimal _
        , ByVal TotalLossPercentage As Decimal _
        , ByVal WarrantyWorkmanship As Integer _
        , ByVal WarrantyRefinish As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iInscCompID As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspCreateNewClient"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            cmdInsert.Parameters.Add("@iPaymentCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iPaymentCompanyID").Value = PaymentCompanyID

            cmdInsert.Parameters.Add("@vLynxContactPhone", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vLynxContactPhone").Value = LYNXContactPhone

            cmdInsert.Parameters.Add("@vCompanyName", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vCompanyName").Value = CompanyName

            cmdInsert.Parameters.Add("@vCompanyNameShort", SqlDbType.VarChar, 4)
            cmdInsert.Parameters("@vCompanyNameShort").Value = CompanyNameShort

            cmdInsert.Parameters.Add("@vAddress1", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vAddress1").Value = Address1

            cmdInsert.Parameters.Add("@vAddress2", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vAddress2").Value = Address2

            cmdInsert.Parameters.Add("@vAddressCity", SqlDbType.VarChar, 30)
            cmdInsert.Parameters("@vAddressCity").Value = City

            cmdInsert.Parameters.Add("@vAddressState", SqlDbType.VarChar, 2)
            cmdInsert.Parameters("@vAddressState").Value = State

            cmdInsert.Parameters.Add("@vAddressZip", SqlDbType.VarChar, 8)
            cmdInsert.Parameters("@vAddressZip").Value = Zip

            cmdInsert.Parameters.Add("@AssignmentAtSelectionFlag", SqlDbType.Bit)
            cmdInsert.Parameters("@AssignmentAtSelectionFlag").Value = AssignmentAtSelectionFlag

            cmdInsert.Parameters.Add("@vBillingModel", SqlDbType.VarChar, 4)
            cmdInsert.Parameters("@vBillingModel").Value = BillingModel

            cmdInsert.Parameters.Add("@bCFLogoDisplayFlag", SqlDbType.Bit)
            cmdInsert.Parameters("@bCFLogoDisplayFlag").Value = CFLogoDisplayFlag

            cmdInsert.Parameters.Add("@bClaimPointDocumentUploadFlag", SqlDbType.Bit)
            cmdInsert.Parameters("@bClaimPointDocumentUploadFlag").Value = ClaimPointDocumentUploadFlag

            cmdInsert.Parameters.Add("@bClaimPointCarrierRepSelFlag", SqlDbType.Bit)
            cmdInsert.Parameters("@bClaimPointCarrierRepSelFlag").Value = ClaimPointCarrierRepSelFlag

            cmdInsert.Parameters.Add("@bClientAccessFlag", SqlDbType.Bit)
            cmdInsert.Parameters("@bClientAccessFlag").Value = ClientAccessFlag

            cmdInsert.Parameters.Add("@vClientProduct", SqlDbType.VarChar, 4)
            cmdInsert.Parameters("@vClientProduct").Value = ClientProduct

            cmdInsert.Parameters.Add("@vCompanyFax", SqlDbType.VarChar, 12)
            cmdInsert.Parameters("@vCompanyFax").Value = CompanyFax

            cmdInsert.Parameters.Add("@vCompanyPhone", SqlDbType.VarChar, 12)
            cmdInsert.Parameters("@vCompanyPhone").Value = CompanyPhone

            cmdInsert.Parameters.Add("@bDemoFlag", SqlDbType.Bit)
            cmdInsert.Parameters("@bDemoFlag").Value = DemoFlag

            cmdInsert.Parameters.Add("bDeskReviewLicenseReqFlag", SqlDbType.Bit)
            cmdInsert.Parameters("bDeskReviewLicenseReqFlag").Value = DeskReviewLicenseReqFlag

            cmdInsert.Parameters.Add("@bEarlyBill", SqlDbType.Bit)
            cmdInsert.Parameters("@bEarlyBill").Value = EarlyBilling

            cmdInsert.Parameters.Add("@bEnabledFlag", SqlDbType.Bit)
            cmdInsert.Parameters("@bEnabledFlag").Value = EnabledFlag

            cmdInsert.Parameters.Add("@vFedTaxID", SqlDbType.VarChar, 15)
            cmdInsert.Parameters("@vFedTaxID").Value = FedTaxID

            cmdInsert.Parameters.Add("@iPreferredCommunicationMethodID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iPreferredCommunicationMethodID").Value = PreferredCommunicationMethodID

            cmdInsert.Parameters.Add("@dTotalLossValuationPercentage", SqlDbType.Decimal)
            cmdInsert.Parameters("@dTotalLossValuationPercentage").Value = TotalLossValuationPercentage

            cmdInsert.Parameters.Add("@dTotalLossPercentage", SqlDbType.Decimal)
            cmdInsert.Parameters("@dTotalLossPercentage").Value = TotalLossPercentage

            cmdInsert.Parameters.Add("@vWarrantyWorkmanship", SqlDbType.VarChar, 4)
            cmdInsert.Parameters("@vWarrantyWorkmanship").Value = WarrantyWorkmanship

            cmdInsert.Parameters.Add("@vWarrantyRefinish", SqlDbType.VarChar, 4)
            cmdInsert.Parameters("@vWarrantyRefinish").Value = WarrantyRefinish

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            'iRowsAffected = cmdInsert.ExecuteNonQuery()
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iInscCompID = CInt(drRowsAffected.Item("InsuranceCompanyID"))
            Else
                iInscCompID = 0
            End If

            Return iInscCompID
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminEnableDisableClient
    ' This APD function enabled or disables a APD Client
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function enables or disables a APD Client")>
    Public Function AdminEnableDisableClient(ByVal InsuranceCompanyID As Integer, ByVal IsEnabled As Boolean) As Integer

        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim drReader As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspAdminEnableDisableClient"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSQL.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            cmdSQL.Parameters.Add("@bIsEnabled", SqlDbType.Bit)
            cmdSQL.Parameters("@bIsEnabled").Value = IsEnabled

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drReader = cmdSQL.ExecuteReader()

            If drReader.HasRows Then
                drReader.Read()
                iRC = CInt(drReader.Item("RC"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdSQL = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminEnableDisableOffice
    ' This APD function enabled or disables a APD Client Office
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function enables or disables a APD Client Office")>
    Public Function AdminEnableDisableOffice(ByVal OfficeID As Integer, ByVal IsEnabled As Boolean) As Integer

        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim drReader As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspAdminEnableDisableOffice"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@iOfficeID", SqlDbType.Int, 4)
            cmdSQL.Parameters("@iOfficeID").Value = OfficeID

            cmdSQL.Parameters.Add("@bIsEnabled", SqlDbType.Bit)
            cmdSQL.Parameters("@bIsEnabled").Value = IsEnabled

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drReader = cmdSQL.ExecuteReader()

            If drReader.HasRows Then
                drReader.Read()
                iRC = CInt(drReader.Item("RC"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdSQL = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminCreateNewOffice
    ' This APD function creates a new APD Client Office
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client Office")>
    Public Function AdminCreateNewOffice(
        ByVal InsuranceCompanyID As Integer _
        , ByVal Address1 As String _
        , ByVal Address2 As String _
        , ByVal AddressCity As String _
        , ByVal AddressState As String _
        , ByVal AddressZip As String _
        , ByVal CCEmailAddress As String _
        , ByVal ClaimNumberFormatJS As String _
        , ByVal ClaimNumberValidJS As String _
        , ByVal ClaimNumberMsgText As String _
        , ByVal ClientOffice As String _
        , ByVal EnabledFlag As Boolean _
        , ByVal FaxAreaCode As String _
        , ByVal FaxExchangeNumber As String _
        , ByVal FaxExtensionNumber As String _
        , ByVal FaxUnitNumber As String _
        , ByVal MailingAddress1 As String _
        , ByVal MailingAddress2 As String _
        , ByVal MailingAddressCity As String _
        , ByVal MailingAddressState As String _
        , ByVal MailingAddressZip As String _
        , ByVal OfficeName As String _
        , ByVal PhoneAreaCode As String _
        , ByVal PhoneExchangeNumber As String _
        , ByVal PhoneExtensionNumber As String _
        , ByVal PhoneUnitNumber As String _
        , ByVal ReturnDocDestinationValue As String _
        , ByVal ReturnDocEmailAddress As String _
        , ByVal ReturnDocFaxAreaCode As String _
        , ByVal ReturnDocFaxExchangeNumber As String _
        , ByVal ReturnDocFaxExtensionNumber As String _
        , ByVal ReturnDocFaxUnitNumber As String _
        , ByVal Userid As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iOfficeID As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspCreateNewOffice"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            cmdInsert.Parameters.Add("@vAddress1", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vAddress1").Value = Address1

            cmdInsert.Parameters.Add("@vAddress2", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vAddress2").Value = Address2

            cmdInsert.Parameters.Add("@vAddressCity", SqlDbType.VarChar, 30)
            cmdInsert.Parameters("@vAddressCity").Value = AddressCity

            cmdInsert.Parameters.Add("@vAddressState", SqlDbType.VarChar, 2)
            cmdInsert.Parameters("@vAddressState").Value = AddressState

            cmdInsert.Parameters.Add("@vAddressZip", SqlDbType.VarChar, 8)
            cmdInsert.Parameters("@vAddressZip").Value = AddressZip

            cmdInsert.Parameters.Add("@vCCEmailAddress", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vCCEmailAddress").Value = CCEmailAddress

            cmdInsert.Parameters.Add("@vClaimNumberFormatJS", SqlDbType.VarChar, 250)
            cmdInsert.Parameters("@vClaimNumberFormatJS").Value = ClaimNumberFormatJS

            cmdInsert.Parameters.Add("@vClaimNumberValidJS", SqlDbType.VarChar, 250)
            cmdInsert.Parameters("@vClaimNumberValidJS").Value = ClaimNumberValidJS

            cmdInsert.Parameters.Add("@vClaimNumberMsgText", SqlDbType.VarChar, 500)
            cmdInsert.Parameters("@vClaimNumberMsgText").Value = ClaimNumberMsgText

            cmdInsert.Parameters.Add("@vClientOfficeId", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vClientOfficeId").Value = ClientOffice

            cmdInsert.Parameters.Add("@bEnabledFlag", SqlDbType.Bit)
            cmdInsert.Parameters("@bEnabledFlag").Value = EnabledFlag

            cmdInsert.Parameters.Add("@cFaxAreaCode", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cFaxAreaCode").Value = FaxAreaCode

            cmdInsert.Parameters.Add("@cFaxExchangeNumber", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cFaxExchangeNumber").Value = FaxExchangeNumber

            cmdInsert.Parameters.Add("@cFaxExtensionNumber", SqlDbType.Char, 5)
            cmdInsert.Parameters("@cFaxExtensionNumber").Value = FaxExtensionNumber

            cmdInsert.Parameters.Add("@cFaxUnitNumber", SqlDbType.Char, 4)
            cmdInsert.Parameters("@cFaxUnitNumber").Value = FaxUnitNumber

            cmdInsert.Parameters.Add("@vMailingAddress1", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vMailingAddress1").Value = MailingAddress1

            cmdInsert.Parameters.Add("@vMailingAddress2", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vMailingAddress2").Value = MailingAddress2

            cmdInsert.Parameters.Add("@vMailingAddressCity", SqlDbType.VarChar, 30)
            cmdInsert.Parameters("@vMailingAddressCity").Value = MailingAddressCity

            cmdInsert.Parameters.Add("@vMailingAddressState", SqlDbType.VarChar, 2)
            cmdInsert.Parameters("@vMailingAddressState").Value = MailingAddressState

            cmdInsert.Parameters.Add("@vMailingAddressZip", SqlDbType.VarChar, 8)
            cmdInsert.Parameters("@vMailingAddressZip").Value = MailingAddressZip

            cmdInsert.Parameters.Add("@vOfficeName", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vOfficeName").Value = OfficeName

            cmdInsert.Parameters.Add("@cPhoneAreaCode", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cPhoneAreaCode").Value = PhoneAreaCode

            cmdInsert.Parameters.Add("@cPhoneExchangeNumber", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cPhoneExchangeNumber").Value = PhoneExchangeNumber

            cmdInsert.Parameters.Add("@cPhoneExtensionNumber", SqlDbType.Char, 5)
            cmdInsert.Parameters("@cPhoneExtensionNumber").Value = PhoneExtensionNumber

            cmdInsert.Parameters.Add("@cPhoneUnitNumber", SqlDbType.Char, 4)
            cmdInsert.Parameters("@cPhoneUnitNumber").Value = PhoneUnitNumber

            cmdInsert.Parameters.Add("@vReturnDocDestinationValue", SqlDbType.VarChar, 250)
            cmdInsert.Parameters("@vReturnDocDestinationValue").Value = ReturnDocDestinationValue

            cmdInsert.Parameters.Add("@vReturnDocEmailAddress", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vReturnDocEmailAddress").Value = ReturnDocEmailAddress

            cmdInsert.Parameters.Add("@cReturnDocFaxAreaCode", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cReturnDocFaxAreaCode").Value = ReturnDocFaxAreaCode

            cmdInsert.Parameters.Add("@cReturnDocFaxExchangeNumber", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cReturnDocFaxExchangeNumber").Value = ReturnDocFaxExchangeNumber

            cmdInsert.Parameters.Add("@cReturnDocFaxExtensionNumber", SqlDbType.Char, 5)
            cmdInsert.Parameters("@cReturnDocFaxExtensionNumber").Value = ReturnDocFaxExtensionNumber

            cmdInsert.Parameters.Add("@cReturnDocFaxUnitNumber", SqlDbType.Char, 4)
            cmdInsert.Parameters("@cReturnDocFaxUnitNumber").Value = ReturnDocFaxUnitNumber

            cmdInsert.Parameters.Add("@iUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iUserID").Value = Userid

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iOfficeID = CInt(drRowsAffected.Item("OfficeID"))
            Else
                iOfficeID = 0
            End If

            Return iOfficeID
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminUpdateOffice
    ' This APD function updates a APD Client Office
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function updates a APD Client Office")>
    Public Function AdminUpdateOffice(
        ByVal InsuranceCompanyID As Integer _
        , ByVal OfficeID As Integer _
        , ByVal Address1 As String _
        , ByVal Address2 As String _
        , ByVal AddressCity As String _
        , ByVal AddressState As String _
        , ByVal AddressZip As String _
        , ByVal CCEmailAddress As String _
        , ByVal ClaimNumberFormatJS As String _
        , ByVal ClaimNumberValidJS As String _
        , ByVal ClaimNumberMsgText As String _
        , ByVal ClientOffice As String _
        , ByVal EnabledFlag As Boolean _
        , ByVal FaxAreaCode As String _
        , ByVal FaxExchangeNumber As String _
        , ByVal FaxExtensionNumber As String _
        , ByVal FaxUnitNumber As String _
        , ByVal MailingAddress1 As String _
        , ByVal MailingAddress2 As String _
        , ByVal MailingAddressCity As String _
        , ByVal MailingAddressState As String _
        , ByVal MailingAddressZip As String _
        , ByVal OfficeName As String _
        , ByVal PhoneAreaCode As String _
        , ByVal PhoneExchangeNumber As String _
        , ByVal PhoneExtensionNumber As String _
        , ByVal PhoneUnitNumber As String _
        , ByVal ReturnDocDestinationValue As String _
        , ByVal ReturnDocEmailAddress As String _
        , ByVal ReturnDocFaxAreaCode As String _
        , ByVal ReturnDocFaxExchangeNumber As String _
        , ByVal ReturnDocFaxExtensionNumber As String _
        , ByVal ReturnDocFaxUnitNumber As String _
        , ByVal UserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 99

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspAdminUpdateOffice"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            cmdInsert.Parameters.Add("@iOfficeID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iOfficeID").Value = OfficeID

            cmdInsert.Parameters.Add("@vAddress1", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vAddress1").Value = Address1

            cmdInsert.Parameters.Add("@vAddress2", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vAddress2").Value = Address2

            cmdInsert.Parameters.Add("@vAddressCity", SqlDbType.VarChar, 30)
            cmdInsert.Parameters("@vAddressCity").Value = AddressCity

            cmdInsert.Parameters.Add("@vAddressState", SqlDbType.VarChar, 2)
            cmdInsert.Parameters("@vAddressState").Value = AddressState

            cmdInsert.Parameters.Add("@vAddressZip", SqlDbType.VarChar, 8)
            cmdInsert.Parameters("@vAddressZip").Value = AddressZip

            cmdInsert.Parameters.Add("@vCCEmailAddress", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vCCEmailAddress").Value = CCEmailAddress

            cmdInsert.Parameters.Add("@vClaimNumberFormatJS", SqlDbType.VarChar, 250)
            cmdInsert.Parameters("@vClaimNumberFormatJS").Value = ClaimNumberFormatJS

            cmdInsert.Parameters.Add("@vClaimNumberValidJS", SqlDbType.VarChar, 250)
            cmdInsert.Parameters("@vClaimNumberValidJS").Value = ClaimNumberValidJS

            cmdInsert.Parameters.Add("@vClaimNumberMsgText", SqlDbType.VarChar, 500)
            cmdInsert.Parameters("@vClaimNumberMsgText").Value = ClaimNumberMsgText

            cmdInsert.Parameters.Add("@vClientOfficeId", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vClientOfficeId").Value = ClientOffice

            cmdInsert.Parameters.Add("@bEnabledFlag", SqlDbType.Bit)
            cmdInsert.Parameters("@bEnabledFlag").Value = EnabledFlag

            cmdInsert.Parameters.Add("@cFaxAreaCode", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cFaxAreaCode").Value = FaxAreaCode

            cmdInsert.Parameters.Add("@cFaxExchangeNumber", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cFaxExchangeNumber").Value = FaxExchangeNumber

            cmdInsert.Parameters.Add("@cFaxExtensionNumber", SqlDbType.Char, 5)
            cmdInsert.Parameters("@cFaxExtensionNumber").Value = FaxExtensionNumber

            cmdInsert.Parameters.Add("@cFaxUnitNumber", SqlDbType.Char, 4)
            cmdInsert.Parameters("@cFaxUnitNumber").Value = FaxUnitNumber

            cmdInsert.Parameters.Add("@vMailingAddress1", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vMailingAddress1").Value = MailingAddress1

            cmdInsert.Parameters.Add("@vMailingAddress2", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vMailingAddress2").Value = MailingAddress2

            cmdInsert.Parameters.Add("@vMailingAddressCity", SqlDbType.VarChar, 30)
            cmdInsert.Parameters("@vMailingAddressCity").Value = MailingAddressCity

            cmdInsert.Parameters.Add("@vMailingAddressState", SqlDbType.VarChar, 2)
            cmdInsert.Parameters("@vMailingAddressState").Value = MailingAddressState

            cmdInsert.Parameters.Add("@vMailingAddressZip", SqlDbType.VarChar, 8)
            cmdInsert.Parameters("@vMailingAddressZip").Value = MailingAddressZip

            cmdInsert.Parameters.Add("@vName", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vName").Value = OfficeName

            cmdInsert.Parameters.Add("@cPhoneAreaCode", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cPhoneAreaCode").Value = PhoneAreaCode

            cmdInsert.Parameters.Add("@cPhoneExchangeNumber", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cPhoneExchangeNumber").Value = PhoneExchangeNumber

            cmdInsert.Parameters.Add("@cPhoneExtensionNumber", SqlDbType.Char, 5)
            cmdInsert.Parameters("@cPhoneExtensionNumber").Value = PhoneExtensionNumber

            cmdInsert.Parameters.Add("@cPhoneUnitNumber", SqlDbType.Char, 4)
            cmdInsert.Parameters("@cPhoneUnitNumber").Value = PhoneUnitNumber

            cmdInsert.Parameters.Add("@vReturnDocDestinationValue", SqlDbType.VarChar, 250)
            cmdInsert.Parameters("@vReturnDocDestinationValue").Value = ReturnDocDestinationValue

            cmdInsert.Parameters.Add("@vReturnDocEmailAddress", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vReturnDocEmailAddress").Value = ReturnDocEmailAddress

            cmdInsert.Parameters.Add("@cReturnDocFaxAreaCode", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cReturnDocFaxAreaCode").Value = ReturnDocFaxAreaCode

            cmdInsert.Parameters.Add("@cReturnDocFaxExchangeNumber", SqlDbType.Char, 3)
            cmdInsert.Parameters("@cReturnDocFaxExchangeNumber").Value = ReturnDocFaxExchangeNumber

            cmdInsert.Parameters.Add("@cReturnDocFaxExtensionNumber", SqlDbType.Char, 5)
            cmdInsert.Parameters("@cReturnDocFaxExtensionNumber").Value = ReturnDocFaxExtensionNumber

            cmdInsert.Parameters.Add("@cReturnDocFaxUnitNumber", SqlDbType.Char, 4)
            cmdInsert.Parameters("@cReturnDocFaxUnitNumber").Value = ReturnDocFaxUnitNumber

            cmdInsert.Parameters.Add("@iSysLastUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iSysLastUserID").Value = UserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 99
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 99
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetTableDataAsRS
    ' This APD function returns the all data from the specified
    ' database table.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns the all data from the specified database table.")>
    Public Function GetTableDataAsRS(ByVal DBTableName As String) As DataSet
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "EXEC uspGetColumnNames utb_communication_method"
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            'cmdSelect.Parameters.Add("@vTableName", SqlDbType.VarChar, 255)
            'cmdSelect.Parameters("@vTableName").Value = DBTableName

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            'ColumnsReader = cmdSelect.ExecuteReader

            oReturnData.Fill(dsReturnData, "Column")
            Dim Item

            For Each Item In dsReturnData.Tables.Item(0).Rows()
                Debug.Print(Item.ToString)
            Next

            'If ColumnsReader.HasRows Then
            '    Do While ColumnsReader.Read
            '        Debug.Print(ColumnsReader.Item("column_name"))
            '    Loop
            'Else
            '    '"No records found..."
            'End If

            Return dsReturnData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'wsClass = New utb_client_report_simple
            'wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            'wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return dsReturnData
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminUpdateClientInformation
    ' This APD function updates existing client information
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function updates existing client information")>
    Public Function AdminUpdateClientInformation(
        ByVal InsuranceCompanyID As Integer _
        , ByVal LYNXContactPhone As String _
        , ByVal CompanyName As String _
        , ByVal CompanyNameShort As String _
        , ByVal Address1 As String _
        , ByVal Address2 As String _
        , ByVal City As String _
        , ByVal State As String _
        , ByVal Zip As String _
        , ByVal ClientProduct As String _
        , ByVal InvoicingModelPayment As String _
        , ByVal CompanyFax As String _
        , ByVal CompanyPhone As String _
        , ByVal EarlyBilling As Boolean _
        , ByVal TotalLossValuationPercentage As Decimal _
        , ByVal TotalLossPercentage As Decimal _
        , ByVal WarrantyWorkmanship As Integer _
        , ByVal WarrantyRefinish As Integer
        ) As String

        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim sRC As String = ""

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspAdminUpdateClientInformation"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSQL.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            cmdSQL.Parameters.Add("@vLynxContactPhone", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vLynxContactPhone").Value = LYNXContactPhone

            cmdSQL.Parameters.Add("@vCompanyName", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vCompanyName").Value = CompanyName

            cmdSQL.Parameters.Add("@vCompanyNameShort", SqlDbType.VarChar, 4)
            cmdSQL.Parameters("@vCompanyNameShort").Value = CompanyNameShort

            cmdSQL.Parameters.Add("@vAddress1", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vAddress1").Value = Address1

            cmdSQL.Parameters.Add("@vAddress2", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vAddress2").Value = Address2

            cmdSQL.Parameters.Add("@vAddressCity", SqlDbType.VarChar, 30)
            cmdSQL.Parameters("@vAddressCity").Value = City

            cmdSQL.Parameters.Add("@vAddressState", SqlDbType.VarChar, 2)
            cmdSQL.Parameters("@vAddressState").Value = State

            cmdSQL.Parameters.Add("@vAddressZip", SqlDbType.VarChar, 8)
            cmdSQL.Parameters("@vAddressZip").Value = Zip

            cmdSQL.Parameters.Add("@vClientProduct", SqlDbType.VarChar, 4)
            cmdSQL.Parameters("@vClientProduct").Value = ClientProduct

            cmdSQL.Parameters.Add("@vInvoicingModelPayment", SqlDbType.VarChar, 1)
            cmdSQL.Parameters("@vInvoicingModelPayment").Value = InvoicingModelPayment

            cmdSQL.Parameters.Add("@vCompanyFax", SqlDbType.VarChar, 12)
            cmdSQL.Parameters("@vCompanyFax").Value = CompanyFax

            cmdSQL.Parameters.Add("@vCompanyPhone", SqlDbType.VarChar, 12)
            cmdSQL.Parameters("@vCompanyPhone").Value = CompanyPhone

            cmdSQL.Parameters.Add("@bEarlyBill", SqlDbType.Bit)
            cmdSQL.Parameters("@bEarlyBill").Value = EarlyBilling

            cmdSQL.Parameters.Add("@dTotalLossValuationPercentage", SqlDbType.Decimal)
            cmdSQL.Parameters("@dTotalLossValuationPercentage").Value = TotalLossValuationPercentage

            cmdSQL.Parameters.Add("@dTotalLossPercentage", SqlDbType.Decimal)
            cmdSQL.Parameters("@dTotalLossPercentage").Value = TotalLossPercentage

            cmdSQL.Parameters.Add("@vWarrantyWorkmanship", SqlDbType.VarChar, 4)
            cmdSQL.Parameters("@vWarrantyWorkmanship").Value = WarrantyWorkmanship

            cmdSQL.Parameters.Add("@vWarrantyRefinish", SqlDbType.VarChar, 4)
            cmdSQL.Parameters("@vWarrantyRefinish").Value = WarrantyRefinish

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdSQL.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                sRC = drRowsAffected.Item("RetCode")
            End If

            Return sRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Client update failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            Return sError
        Finally
            cmdSQL = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetExistingClientBundlesByInscCompIDAsRS
    ' This APD function returns all bundling data for an 
    ' existing insurance company.
    '-----------------------------------------------------------
    '<WebMethod(Description:="This APD function returns all bundling data for an existing insurance company.")> _
    'Public Function GetExistingClientBundlesByInscCompIDAsRS(ByVal InsuranceCompanyID As Integer) As DataSet
    '    Dim Conn As New SqlConnection
    '    Dim cmdSelect As New SqlClient.SqlCommand
    '    Dim dsReturnData As New DataSet

    '    Try
    '        '-------------------------------------------
    '        ' Open connection to DB and run stored Proc
    '        '-------------------------------------------
    '        cmdSelect.CommandType = CommandType.Text
    '        cmdSelect.CommandText = "EXEC uspGetColumnNames utb_communication_method"
    '        cmdSelect.Connection = Conn

    '        Dim oReturnData As New SqlDataAdapter(cmdSelect)

    '        Conn.ConnectionString = AppSettings("ConnectString")
    '        Conn.Open()

    '        '-------------------------------------------
    '        ' Input Parameters
    '        '-------------------------------------------
    '        'cmdSelect.Parameters.Add("@vTableName", SqlDbType.VarChar, 255)
    '        'cmdSelect.Parameters("@vTableName").Value = DBTableName

    '        '-------------------------------------------
    '        ' Execute the SQL
    '        '-------------------------------------------
    '        'ColumnsReader = cmdSelect.ExecuteReader

    '        oReturnData.Fill(dsReturnData, "Column")
    '        Dim Item

    '        For Each Item In dsReturnData.Tables.Item(0).Rows()
    '            Debug.Print(Item.ToString)
    '        Next

    '        'If ColumnsReader.HasRows Then
    '        '    Do While ColumnsReader.Read
    '        '        Debug.Print(ColumnsReader.Item("column_name"))
    '        '    Loop
    '        'Else
    '        '    '"No records found..."
    '        'End If

    '        Return dsReturnData
    '    Catch oExcept As Exception
    '        '------------------------------
    '        ' Email Notify of the Error
    '        '------------------------------
    '        'wsClass = New utb_client_report_simple
    '        'wsClass.Errors = "WebServices Failed: " & oExcept.ToString

    '        'wsData.Add(wsClass)
    '        'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
    '        Return dsReturnData
    '    End Try
    'End Function

    '-----------------------------------------------------------
    ' APD: GetActiveServiceChannelsAsRS
    ' This APD function returns the all active Service Channels.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns the all active Service Channels.")>
    Public Function GetActiveServiceChannelsAsRS() As DataSet
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetActiveServiceChannels"
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            oReturnData.Fill(dsReturnData, "Column")
            'Dim Item

            'For Each Item In dsReturnData.Tables.Item(0).Rows()
            'Debug.Print(Item.ToString)
            'Next

            'If ColumnsReader.HasRows Then
            '    Do While ColumnsReader.Read
            '        Debug.Print(ColumnsReader.Item("column_name"))
            '    Loop
            'Else
            '    '"No records found..."
            'End If

            Return dsReturnData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'wsClass = New utb_client_report_simple
            'wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            'wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return dsReturnData
        Finally
            cmdSelect = Nothing
            dsReturnData = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetExistingClientBundlesByInscCompIDAsRS
    ' This APD function returns all bundling data for an 
    ' existing insurance company.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns all bundling data for an existing insurance company.")>
    Public Function GetExistingClientBundlesByInscCompIDAsRS(ByVal InsuranceCompanyID As Integer) As DataSet
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "uspGetExistingClientBundles " & InsuranceCompanyID
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            oReturnData.Fill(dsReturnData, "Column")

            Return dsReturnData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'wsClass = New utb_client_report_simple
            'wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            'wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return dsReturnData
        Finally
            cmdSelect = Nothing
            dsReturnData = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetBundleDetailsByBundleIDAsRS
    ' This APD function returns bundling details for an 
    ' existing insurance company by bundling ID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns bundling details for an existing insurance company by bundling ID.")>
    Public Function GetBundleDetailsByBundleIDAsRS(ByVal BundlingID As Integer, ByVal DocumentTypeID As Integer) As DataSet
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "uspGetBundleDetailsByBundleID " & BundlingID & ", " & DocumentTypeID
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            oReturnData.Fill(dsReturnData, "Column")

            Return dsReturnData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "PGWAPDFoundation WebService Error: Failed to retrieve bundling details (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            Dim drData As DataRow
            drData = dsReturnData.Tables(0).NewRow()
            drData("Error") = sError
            dsReturnData.Tables(0).Rows.Add(drData)
            Return dsReturnData
        Finally
            cmdSelect = Nothing
            dsReturnData = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminUpdateBundlingDetails
    ' This APD function updates existing client bundling details
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function updates existing client bundling details.")>
    Public Function AdminUpdateBundlingDetails(
        ByVal BundlingID As Integer _
        , ByVal MessageTemplateID As Integer _
        , ByVal DocumentTypeID As Integer _
        , ByVal Name As String _
        , ByVal Description As String _
        , ByVal DirectionalCD As String _
        , ByVal DirectionToPayFlag As Boolean _
        , ByVal DuplicateFlag As Boolean _
        , ByVal FinalEstimateFlag As Boolean _
        , ByVal MandatoryFlag As Boolean _
        , ByVal VANFlag As Boolean _
        , ByVal WarrantyFlag As Boolean
        ) As String

        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim sRC As String = ""

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspAdminUpdateBundlingDetails"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@iBundlingID", SqlDbType.Int, 4)
            cmdSQL.Parameters("@iBundlingID").Value = BundlingID

            cmdSQL.Parameters.Add("@iMessageTemplateID", SqlDbType.Int, 4)
            cmdSQL.Parameters("@iMessageTemplateID").Value = MessageTemplateID

            cmdSQL.Parameters.Add("@iDocumentTypeID", SqlDbType.Int, 4)
            cmdSQL.Parameters("@iDocumentTypeID").Value = DocumentTypeID

            cmdSQL.Parameters.Add("@vName", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vName").Value = Name

            cmdSQL.Parameters.Add("@vDescription", SqlDbType.VarChar, 8000)
            cmdSQL.Parameters("@vDescription").Value = Description

            cmdSQL.Parameters.Add("@vDirectionalCD", SqlDbType.VarChar, 4)
            cmdSQL.Parameters("@vDirectionalCD").Value = UCase(DirectionalCD)

            cmdSQL.Parameters.Add("@bDirectionToPayFlag", SqlDbType.Bit)
            cmdSQL.Parameters("@bDirectionToPayFlag").Value = DirectionToPayFlag

            cmdSQL.Parameters.Add("@bDuplicateFlag", SqlDbType.Bit)
            cmdSQL.Parameters("@bDuplicateFlag").Value = DuplicateFlag

            cmdSQL.Parameters.Add("@bFinalEstimateFlag", SqlDbType.Bit)
            cmdSQL.Parameters("@bFinalEstimateFlag").Value = FinalEstimateFlag

            cmdSQL.Parameters.Add("@bMandatoryFlag", SqlDbType.Bit)
            cmdSQL.Parameters("@bMandatoryFlag").Value = MandatoryFlag

            cmdSQL.Parameters.Add("@bVANFlag", SqlDbType.Bit)
            cmdSQL.Parameters("@bVANFlag").Value = VANFlag

            cmdSQL.Parameters.Add("@bWarrantyFlag", SqlDbType.Bit)
            cmdSQL.Parameters("@bWarrantyFlag").Value = WarrantyFlag

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdSQL.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                sRC = drRowsAffected.Item("RetCode")
            End If

            Return sRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Client update failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            Return sError
        Finally
            cmdSQL = Nothing
            drRowsAffected = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetExistingClientBundleByBundleIDAsRS
    ' This APD function returns basic bundle info for an 
    ' existing insurance company by bundling ID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns basic bundle info for an existing insurance company by bundling ID.")>
    Public Function GetExistingClientBundleByBundleIDAsRS(ByVal BundlingID As Integer) As DataSet
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "uspGetExistingClientBundleByBundleID " & BundlingID
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            oReturnData.Fill(dsReturnData, "Column")

            Return dsReturnData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "PGWAPDFoundation WebService Error: Failed to retrieve existing basic bundle information (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            Dim drData As DataRow
            drData = dsReturnData.Tables(0).NewRow()
            drData("Error") = sError
            dsReturnData.Tables(0).Rows.Add(drData)
            Return dsReturnData
        Finally
            cmdSelect = Nothing
            dsReturnData = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetExistingBundlesRequiredDocsByBundleIDAsRS
    ' This APD function returns existing required documents
    ' for an existing bundlingID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns existing required documents for an existing bundlingID.")>
    Public Function GetExistingBundlesRequiredDocsByBundleIDAsRS(ByVal BundlingID As Integer) As DataSet
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "uspGetExistingBundlesRequiredDocs " & BundlingID
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            oReturnData.Fill(dsReturnData, "Column")

            Return dsReturnData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "PGWAPDFoundation WebService Error: Failed to retrieve existing bundling required documents information (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            Dim drData As DataRow
            drData = dsReturnData.Tables(0).NewRow()
            drData("Error") = sError
            dsReturnData.Tables(0).Rows.Add(drData)
            Return dsReturnData
        Finally
            cmdSelect = Nothing
            dsReturnData = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ExecuteSQL
    ' This APD function returns information from the database by
    ' executing a stored procedure without parameters and returning 
    ' a Dataset that represents the data.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns information from the database by executing a stored procedure without parameters and returning a Dataset that represents the data.")>
    Public Function ExecuteSQL(ByVal StoredProcedure As String) As DataSet
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet
        Dim XMLParms As New XmlDocument
        Dim sParams As String = ""

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = StoredProcedure
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            oReturnData.Fill(dsReturnData, "Column")

            Return dsReturnData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "PGWAPDFoundation WebService Error: Failed to retrieve any existing information (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            Dim drData As DataRow
            drData = dsReturnData.Tables(0).NewRow()
            drData("Error") = sError
            dsReturnData.Tables(0).Rows.Add(drData)
            Return dsReturnData
        Finally
            cmdSelect = Nothing
            dsReturnData = Nothing
            XMLParms = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetExistingLaborRates
    ' This APD function returns existing labor rates
    ' by InsuranceCompanyID from the database
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns existing labor rates by InsuranceCompanyID from the database")>
    Public Function GetExistingLaborRates(ByVal InsuranceCompanyID As Integer) As DataSet
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "uspGetBundleDetailsByInsuranceCompanyID " & InsuranceCompanyID
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            oReturnData.Fill(dsReturnData, "Column")

            Return dsReturnData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "PGWAPDFoundation WebService Error: Failed to retrieve existing labor rates based on the InsuranceCompanyID passed. (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            Dim drData As DataRow
            drData = dsReturnData.Tables(0).NewRow()
            drData("Error") = sError
            dsReturnData.Tables(0).Rows.Add(drData)
            Return dsReturnData
        Finally
            cmdSelect = Nothing
            dsReturnData = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: CreateAPDClaim
    ' This APD function creates a claim into APD
    '-----------------------------------------------------------
    'Public Function CreateAPDClaim(ByVal Claim As APDClaim, ByVal Vehicles As APDVehicle) As String
    <WebMethod(Description:="This APD function creates a claim into APD.")>
    Public Function CreateAPDClaim(ByVal WSClaim As String) As String
        Dim XMLWSClaim As New XmlDocument
        Dim XMLWSProcessedByClaimRep As New List(Of utb_user_simple)
        Dim sRC As String = ""
        Dim FNOLServices As New FNOLServices.FNOLServices
        Dim sLynxID As String = ""
        Dim LynxNode As XmlNode
        Dim sNewXML As String = ""
        Dim oVehicle As New APDVehicle
        Dim iCnt As Integer = 0
        Dim iInvolvedID() As Integer
        Dim sRetData As String = ""
        Dim XMLWSResponse As New XmlDocument
        'Dim sWSResponse As String = ""

        Try
            '---------------------------------------
            ' Load the Claim XML from the customer
            '---------------------------------------
            XMLWSClaim.LoadXml(WSClaim)
            If WSClaim <> "" Then
                LogEvent("WSServerSideClaimProcessor", "START", "Claim process started", "Claim XML passed-in is in EventXML", WSClaim)
            Else
                LogEvent("WSServerSideClaimProcessor", "ERROR", "Invalid XML Claim Data passed or was a blank value.", "WSClaim = ", IIf(WSClaim = "", "Nothing", WSClaim))
                Throw New SystemException("Invalid XML Claim Data passed or was a blank value.")
            End If

            '---------------------------------------
            ' Parse the WSClaim XML and submit to 
            ' FNOL Load tables via class structure
            '---------------------------------------
            sRC = LoadClaimClass(XMLWSClaim)
            If sRC.Contains("Error:") Then
                LogEvent("WSServerSideClaimProcessor", "ERROR", "Claim XML is not valid.", "Claim XML tag missing or incorrect.", "")
                Throw New SystemException("Claim XML tag missing or incorrect.")
            End If

            If sRC = "0" Then
                'Claim Load Succeeded

                iInsuranceCompanyID = Claim.InsuranceCompanyID

                '---------------------------------------
                ' Load the Vehicle XML from the customer
                '---------------------------------------
                sRC = ""
                sRC = LoadVehicleClass(XMLWSClaim)

                If sRC.Contains("Error:") Then
                    LogEvent("WSServerSideClaimProcessor", "ERROR", "Vehicle XML is not valid.", "Vehicle XML tag missing or incorrect.", "")
                    Throw New SystemException("Vehicle XML tag missing or incorrect.")
                End If

                If sRC = "0" Then
                    '---------------------------------------
                    ' ALL XML validated get the next available 
                    ' LynxID(number) if not passed in Claim
                    '---------------------------------------
                    If Claim.LynxID = "" Then
                        Dim oRetXML As XmlElement
                        oRetXML = FNOLServices.RequestLYNXID()
                        LynxNode = oRetXML.SelectSingleNode("LYNXID")
                        Claim.LynxID = LynxNode.InnerText

                        For Each oVehicle In Vehicles
                            Vehicles(iCnt).LynxID = LynxNode.InnerText
                            iCnt = iCnt + 1
                        Next

                        '---------------------------------------
                        ' Check to make sure we got a LynxID
                        '---------------------------------------
                        If Claim.LynxID = "" Then
                            Throw New SystemException("No LynxID received.")
                        Else
                            LogEvent("WSServerSideClaimProcessor", "SUCCESSFUL", "LynxID requested", "LynxID returned = " & Claim.LynxID, LynxNode.OuterXml)
                        End If
                    Else
                        '---------------------------------------
                        ' This is a vehicle add and we need to
                        ' make sure there isn't any failed
                        ' attempts still in the load tables
                        '---------------------------------------
                        Call ClearFNOLLoadTables(Claim.LynxID)
                    End If

                    '---------------------------------------
                    ' Process the claim to FNOL Load tables
                    '---------------------------------------
                    sRC = FNOLClaimLoad(Claim)

                    '---------------------------------------
                    ' Check to make sure the Claim loaded 
                    ' into the utb_fnol_claim_load table
                    '---------------------------------------
                    If sRC <> "1" Then
                        LogEvent("WSServerSideClaimProcessor", "ERROR", "Claim load failed.", "FNOLClaimLoad failed.  Error: ", sRC)
                        Throw New SystemException("Claim load failed.  Error: " & sRC)
                    End If

                    '---------------------------------------
                    ' Process the vehicles to FNOL Load tables
                    '---------------------------------------
                    iCnt = 0
                    For Each oVehicle In Vehicles
                        sRC = FNOLVehicleLoad(Vehicles(iCnt))

                        '---------------------------------------
                        ' Check to make sure the Vehicle loaded 
                        ' into the utb_fnol_claim_vehicle_load table
                        '---------------------------------------
                        If sRC <> "1" Then
                            LogEvent("WSServerSideClaimProcessor", "ERROR", "Vehicle load failed.", "FNOLVehicleLoad failed.  Error: ", sRC)
                            Throw New SystemException("Vehicle load failed.  Error: " & sRC)
                        Else
                            '----------------------------------------------
                            ' Create the insured involved.  If this is a 
                            ' New Claim, create the Insured Involved record
                            '----------------------------------------------
                            If Claim.NewClaimFlag = 1 Then
                                sRC = ""
                                sRC = FNOLInvolvedLoad(oVehicle, True)

                                If sRC <> "1" Then
                                    LogEvent("WSServerSideClaimProcessor", "ERROR", "Involved Insured load failed.", "FNOLInvolvedLoad of Insured failed.  Error: ", sRC)
                                    Throw New SystemException("Involved load of Insured failed.  Error: " & sRC)
                                End If
                            End If

                            '---------------------------------------
                            ' Load the Involved XML from the customer
                            '---------------------------------------
                            sRC = ""
                            sRC = FNOLInvolvedLoad(oVehicle, False)

                            If sRC = "1" Then
                                '---------------------------------------
                                ' Load the Coverage XML from the customer
                                '---------------------------------------
                                sRC = ""
                                sRC = FNOLCoverageLoad(oVehicle, oVehicle.CoverageProfileCD)

                                If sRC <> "1" Then
                                    '---------------------------------------
                                    ' Coverage load process failed
                                    '---------------------------------------
                                    LogEvent("WSServerSideClaimProcessor", "ERROR", "Coverage load failed.", "FNOLCoverageLoad failed.  Error: Coverage profile type: " & oVehicle.CoverageProfileCD & " is are not authorized for insurance company: " & Claim.InsuranceCompanyID, "")
                                    Throw New SystemException("XMLValidationError: Vehicle Tag: CoverageProfileCD = " & oVehicle.CoverageProfileCD & " is are not authorized for insurance company: " & Claim.InsuranceCompanyID)
                                End If

                                '-----------------------------------
                                ' Add Rentals if they exist    
                                '-----------------------------------
                                'If oVehicle.ExposureCD = 1 Then
                                If UCase(oVehicle.RentalAuthorized) = "YES" Then
                                    '---------------------------------------
                                    ' Load the Rental info into the Coverage 
                                    ' tab
                                    '---------------------------------------
                                    sRC = ""
                                    sRC = FNOLCoverageLoad(oVehicle, "RENT")

                                    If sRC <> "1" Then
                                        '---------------------------------------
                                        ' Coverage load process failed
                                        '---------------------------------------
                                        LogEvent("WSServerSideClaimProcessor", "ERROR", "Coverage load failed.", "FNOLCoverageLoad failed.  Error: Rentals are not authorized for insurance company: " & Claim.InsuranceCompanyID, "")
                                        Throw New SystemException("XMLValidationError: Rentals are not authorized for insurance company: " & Claim.InsuranceCompanyID)
                                    End If
                                End If
                                'Else
                                '    '---------------------------------------
                                '    ' Involved load process failed
                                '    '---------------------------------------
                                '    LogEvent("WSServerSideClaimProcessor", "ERROR", "Involved load failed.", "FNOLInvolvedLoad failed.  Error: ", sRC)
                                '    Throw New SystemException("Involved load failed.  Error: " & sRC)
                                'End If
                            End If
                        End If


                        'Vehicles(iCnt).LynxID = Claim.LynxID
                        iCnt = iCnt + 1
                    Next

                    '---------------------------------------
                    ' ALL data loaded successfully, insert
                    ' the permanent claim
                    '---------------------------------------
                    sRC = FNOLInsClaim(Claim.LynxID, Claim.NewClaimFlag)

                    '---------------------------------------
                    ' Check to make sure the claim was inserted
                    ' correctly into APD
                    '---------------------------------------
                    If sRC <> "-1" Then
                        '---------------------------------------
                        ' Insc Claim process failed
                        '---------------------------------------
                        LogEvent("WSServerSideClaimProcessor", "ERROR", "Insert claim failed.", "FNOLInsClaim failed.  Error: ", sRC)
                        Throw New SystemException(sRC)
                    Else
                        '---------------------------------------
                        ' 31Oct2013 - TVD - Adding support
                        ' for ClaimPoint processing and return
                        ' data format.
                        '---------------------------------------
                        '---------------------------------------
                        ' Find out where the claim came from
                        '---------------------------------------
                        Select Case UCase(Claim.DataSource)
                            Case "WEB SERVICES ASSIGNMENT"
                                '---------------------------------------
                                ' Claim was created, now lets get some
                                ' details from the real utb_claim table
                                '---------------------------------------
                                XMLWSProcessedByClaimRep = GetUserInfoByUserID(XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepUserID").InnerText)

                                sRetData = "<ClaimResponse>"
                                sRetData += "<LynxID>" & Claim.LynxID & "</LynxID>"
                                sRetData += "<InsuranceCompanyName>" & Claim.InsuranceCompanyID & "</InsuranceCompanyName>"
                                sRetData += "<CoverageClaimNumber>" & Claim.CoverageClaimNumber & "</CoverageClaimNumber>"
                                sRetData += "<CarrierRepNameFirst>" & XMLWSProcessedByClaimRep(0).NameFirst & "</CarrierRepNameFirst>"
                                sRetData += "<CarrierRepNameLast>" & XMLWSProcessedByClaimRep(0).NameLast & "</CarrierRepNameLast>"
                                sRetData += "<CarrierRepPhoneDay>" & XMLWSProcessedByClaimRep(0).PhoneCombined & "</CarrierRepPhoneDay>"
                                sRetData += "<CarrierRepEmailAddress>" & XMLWSProcessedByClaimRep(0).EmailAddress & "</CarrierRepEmailAddress>"
                                sRetData += "<StatusID>0</StatusID>"
                                sRetData += "<Status>Claim processed successfully</Status>"
                                sRetData += "</ClaimResponse>"
                            Case "CLAIM POINT WEB ASSIGNMENT"
                                '---------------------------------------
                                ' Build return data back to ClaimPoint
                                '---------------------------------------
                                XMLWSResponse = ExecuteSpAsXML("uspClaimVehicleGetListWSXML", Claim.LynxID & "," & Claim.InsuranceCompanyID)
                                sRetData = XMLWSResponse.InnerXml
                                Return sRetData
                        End Select

                        Dim response1 As New XmlDocument
                        response1.LoadXml(sRetData)

                        LogEvent("WSServerSideClaimProcessor", "SUCCESSFUL", "Claim Processed", "LynxID returned = " & Claim.LynxID, sRetData)
                        LogEvent("WSServerSideClaimProcessor", "END", "Claim process ended", "", "")

                        Return response1.OuterXml
                    End If
                Else
                    '---------------------------------------
                    ' Vehicle load process failed
                    '---------------------------------------
                    Throw New SystemException("Invalid Vehicle XML Data.")
                End If
            Else
                '---------------------------------------
                ' Claim load process failed
                '---------------------------------------
                Throw New SystemException("Invalid Claim XML Data.")
            End If
        Catch oExcept As Exception
            '------------------------------
            ' Build return error XML
            '------------------------------
            'Dim sRetData As String = ""
            sRetData = "<ClaimResponse>"
            sRetData += "<LynxID></LynxID>"
            sRetData += "<InsuranceCompanyName>" & Claim.InsuranceCompanyID & "</InsuranceCompanyName>"
            sRetData += "<CoverageClaimNumber>" & Claim.CoverageClaimNumber & "</CoverageClaimNumber>"
            sRetData += "<CarrierRepNameFirst></CarrierRepNameFirst>"
            sRetData += "<CarrierRepNameLast></CarrierRepNameLast>"
            sRetData += "<CarrierRepPhoneDay></CarrierRepPhoneDay>"
            sRetData += "<CarrierRepEmailAddress></CarrierRepEmailAddress>"
            sRetData += "<StatusID>900</StatusID>"
            sRetData += "<Status>Claim failed to created successfully: Error: " & oExcept.Message & "</Status>"
            sRetData += "</ClaimResponse>"

            Dim response1 As New XmlDocument
            response1.LoadXml(sRetData)
            Return response1.OuterXml

            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Create APD claim failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            LogEvent("WSServerSideClaimProcessor", "ERROR", "Claim create failed", "", sError)

            'Return sError
        Finally
            'cmdSelect = Nothing
            'dsReturnData = Nothing
            'Conn.Close()
        End Try
    End Function

    Private Function SerializeAnObject(ByVal AnObject As Object) As String
        Dim Xml_Serializer As XmlSerializer = New XmlSerializer(AnObject.GetType)
        Dim Writer As StringWriter = New StringWriter
        Xml_Serializer.Serialize(Writer, AnObject)
        Return Writer.ToString
    End Function

    'Private Function DeSerializeAnObject(ByVal XmlOfAnObject As String, ByVal ObjectType As Type) As Object
    '    Dim StrReader As StringReader = New StringReader(XmlOfAnObject)
    '    Dim Xml_Serializer As XmlSerializer = New XmlSerializer(ObjectType)
    '    Dim XmlReader As XmlTextReader = New XmlTextReader(StrReader)
    '    Try
    '        Dim AnObject As Object = Xml_Serializer.Deserialize(XmlReader)
    '        Return AnObject
    '    Finally
    '        XmlReader.Close()
    '        StrReader.Close()
    '    End Try
    'End Function

    'Private Function SubmitToAPD(ByVal requestXml As String) As XmlDocument
    '    Dim lex As Exception = Nothing
    '    Dim response As XmlDocument = Nothing
    '    Dim httpResponse As HttpWebResponse = Nothing
    '    Dim httpRequest As HttpWebRequest = Nothing
    '    Dim attempts As Integer = 0
    '    Dim success As Boolean = False
    '    Dim responseXml As String
    '    Dim encoder As ASCIIEncoding = New ASCIIEncoding
    '    '                                      events.Trace(GenUtils.Append("Submitting Request to APD: ", requestXml))

    '    While Not success
    '        Try
    '            ' Create request handler
    '            httpRequest = CType(WebRequest.Create("http://dapd.pgw.local/FNOLAssignment.asp"), HttpWebRequest)
    '            Dim requestBuffer() As Byte = encoder.GetBytes(requestXml)
    '            'post request to APD Server
    '            httpRequest.Method = "POST"
    '            httpRequest.ContentLength = requestXml.Length
    '            Dim requestStream As Stream = httpRequest.GetRequestStream
    '            requestStream.Write(requestBuffer, 0, requestBuffer.Length)
    '            requestStream.Close()
    '            success = True
    '        Catch ex As Exception
    '            attempts = (attempts + 1)
    '            httpRequest = Nothing
    '            If (attempts >= 5) Then
    '                Throw ex
    '            End If
    '            '_events.Trace(GenUtils.Append("Attempt #", attempts.ToString, "failed: ", ex.ToString))
    '        End Try

    '    End While
    '    attempts = 0
    '    Try
    '        httpResponse = CType(httpRequest.GetResponse, HttpWebResponse)
    '        Dim responseStream As Stream = httpResponse.GetResponseStream
    '        Dim rdr As StreamReader = New StreamReader(responseStream, Encoding.ASCII)
    '        responseXml = rdr.ReadToEnd
    '        rdr.Close()
    '        responseStream.Close()
    '        httpResponse.Close()
    '        response = New XmlDocument
    '        response.LoadXml(responseXml)
    '    Catch ex As Exception
    '        lex = ex
    '    Finally
    '        httpResponse = Nothing
    '        httpRequest = Nothing
    '    End Try
    '    If (Not (lex) Is Nothing) Then
    '        Throw lex
    '    End If
    '    Return response
    'End Function

    Private Function LoadClaimClass(ByVal XMLWSClaim As XmlDocument) As String
        Dim ClaimNode As XmlNode

        Try
            '---------------------------------------
            ' Data available for all claims
            '---------------------------------------
            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LynxID")
            Claim.LynxID = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/NewClaimFlag")
            Claim.NewClaimFlag = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/NoticeMethodID")
            Claim.NoticeMethodID = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/DataSource")
            Claim.DataSource = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/FNOLUserID")
            Claim.FNOLUserID = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuranceCompanyID")
            Claim.InsuranceCompanyID = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/AssignmentAtSelectionFlag")
            Claim.AssignmentAtSelectionFlag = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/DemoFlag")
            Claim.DemoFlag = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/AssignmentDescription")
            Claim.AssignmentDescription = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierOfficeName")
            Claim.CarrierOfficeName = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameFirst")
            Claim.CarrierRepNameFirst = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepNameLast")
            Claim.CarrierRepNameLast = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepUserID")
            Claim.CarrierRepUserID = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepPhoneDay")
            Claim.CarrierRepPhoneDay = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepEmailAddress")
            Claim.CarrierRepEmailAddress = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierName")
            Claim.CarrierName = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CoverageClaimNumber")
            Claim.CoverageClaimNumber = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LossDate")
            Claim.LossDate = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LossAddressState")
            Claim.LossAddressState = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerNameFirst")
            Claim.CallerNameFirst = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerNameLast")
            Claim.CallerNameLast = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerRelationToInsuredID")
            Claim.CallerRelationToInsuredID = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerRelationToInsuredIDDescription")
            Claim.CallerRelationToInsuredIDDescription = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredNameFirst")
            Claim.InsuredNameFirst = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredNameLast")
            Claim.InsuredNameLast = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredPhone")
            'ClaimNode.InnerText = ClaimNode.InnerText.Replace("(", "")
            'ClaimNode.InnerText = ClaimNode.InnerText.Replace(")", "")
            'ClaimNode.InnerText = ClaimNode.InnerText.Replace("-", "")
            Claim.InsuredPhone = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredBusinessName")
            Claim.InsuredBusinessName = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CollisionDeductibleAmt")
            Claim.CollisionDeductibleAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CollisionLimitAmt")
            Claim.CollisionLimitAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ComprehensiveDeductibleAmt")
            Claim.ComprehensiveDeductibleAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ComprehensiveLimitAmt")
            Claim.ComprehensiveLimitAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LiabilityDeductibleAmt")
            Claim.LiabilityDeductibleAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LiabilityLimitAmt")
            Claim.LiabilityLimitAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/UnderInsuredDeductibleAmt")
            Claim.UnderInsuredDeductibleAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/UnderInsuredLimitAmt")
            Claim.UnderInsuredLimitAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/UnInsuredDeductibleAmt")
            Claim.UnInsuredDeductibleAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/UnInsuredLimitAmt")
            Claim.UnInsuredLimitAmt = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/TimeStarted")
            If ClaimNode.InnerText = Nothing Then
                ClaimNode.InnerText = Date.Now
            Else
                Claim.TimeStarted = ClaimNode.InnerText
            End If

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/TimeFinished")
            Claim.TimeFinished = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/IntakeStartSeconds")
            Claim.IntakeStartSeconds = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/IntakeEndSeconds")
            Claim.IntakeEndSeconds = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/IntakeSeconds")
            Claim.IntakeSeconds = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredName")
            Claim.InsuredName = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredPhoneSumm")
            Claim.InsuredPhoneSumm = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalAuthorized")
            Claim.RentalAuthorized = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalDays")
            Claim.RentalDays = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalDayAmount")
            Claim.RentalDayAmount = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalMaxAmount")
            Claim.RentalMaxAmount = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalDaysAuthorized")
            Claim.RentalDaysAuthorized = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/Mileage")
            Claim.Mileage = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LossDescription")
            Claim.LossDescription = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/Remarks")
            Claim.Remarks = ClaimNode.InnerText

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RentalInstructions")
            Claim.RentalInstructions = ClaimNode.InnerText

            If UCase(Claim.AssignmentDescription) = "REPAIR REFERRAL" Then
                ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/RepairReferral")
                Claim.RepairReferral = ClaimNode.InnerText
            End If

            ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/SourceApplicationCD")
            '-----------------------------------
            ' 31Oct2013 - TVD - Added support 
            ' for ClaimPoint Assignments
            '-----------------------------------
            Select Case UCase(Claim.DataSource)
                Case "WEB SERVICES ASSIGNMENT"
                    Claim.SourceApplicationCD = "WS" ' WS Assignment
                Case "CLAIM POINT WEB ASSIGNMENT"
                    Claim.SourceApplicationCD = "CP" ' WS Assignment
            End Select

            'Try
            '    '---------------------------------------
            '    ' These values are PS only, not used in
            '    ' RRP - So skip them if they don't exist
            '    '---------------------------------------
            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerAddress1")
            '    Claim.CallerAddress1 = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerAddressCity")
            '    Claim.CallerAddressCity = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerAddressState")
            '    Claim.CallerAddressState = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerAddressZip")
            '    Claim.CallerAddressZip = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CallerPhoneNight")
            '    Claim.CallerPhoneNight = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ContactAddress1")
            '    Claim.ContactAddress1 = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ContactAddressCity")
            '    Claim.ContactAddressCity = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ContactAddressState")
            '    Claim.ContactAddressState = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ContactAddressZip")
            '    Claim.ContactAddressZip = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ContactNameFirst")
            '    Claim.ContactNameFirst = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ContactNameLast")
            '    Claim.ContactNameLast = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ContactPhoneNight")
            '    Claim.ContactPhoneNight = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ContactRelationToInsuredID")
            '    Claim.ContactRelationToInsuredID = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/CarrierRepLogin")
            '    Claim.CarrierRepLogin = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/ClientOfficeID")
            '    Claim.ClientOfficeID = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredAddress1")
            '    Claim.InsuredAddress1 = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredAddressCity")
            '    Claim.InsuredAddressCity = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredAddressState")
            '    Claim.InsuredAddressState = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredAddressZip")
            '    Claim.InsuredAddressZip = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredPhoneDay")
            '    Claim.InsuredPhoneDay = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/InsuredPhoneNight")
            '    Claim.InsuredPhoneNight = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LossAddressStreet")
            '    Claim.LossAddressStreet = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/PolicyNumber")
            '    Claim.PolicyNumber = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/LossAddressCity")
            '    Claim.LossAddressCity = ClaimNode.InnerText

            '    ClaimNode = XMLWSClaim.SelectSingleNode("WebAssignment/Claim/SourceApplicationCD")
            '    Claim.SourceApplicationCD = ClaimNode.InnerText
            'Catch ex As Exception
            '    '---------------------------------------
            '    ' Don't do anything, just ignore the 
            '    ' PS values if RRP
            '    '---------------------------------------
            'End Try

            LogEvent("WSServerSideClaimProcessor", "SUCCESSFUL", "Claim class file loaded", "Claim Class XML is in EventXML", SerializeAnObject(Claim))
            'LogEvent("WSServerSideClaimProcessor", "SUCCESSFUL", "Claim class file loaded", "Claim Class XML is in EventXML", ClaimNode.OuterXml)

            Return "0"
        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Loading of the claim class failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            LogEvent("WSServerSideClaimProcessor", "ERROR", "Claim class file load failed", "", sError)

            Return sError
        End Try
    End Function

    Private Function LoadVehicleClass(ByVal XMLWSClaim As XmlDocument) As String
        Dim VehicleNode As XmlNode

        Try
            '----------------------------------------
            ' Check for multiple vehicles and build
            ' the class that way.
            '----------------------------------------
            Dim xNodeList As XmlNodeList = XMLWSClaim.SelectNodes("WebAssignment/Vehicle")

            For Each xNode As XmlNode In xNodeList
                Dim Vehicle As New APDVehicle

                VehicleNode = xNode.SelectSingleNode("AssignmentTypeID")
                Vehicle.AssignmentTypeID = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ExposureCD")
                Vehicle.ExposureCD = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("CoverageProfileCD")
                Vehicle.CoverageProfileCD = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("OwnerNameFirst")
                Vehicle.OwnerNameFirst = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("OwnerNameLast")
                Vehicle.OwnerNameLast = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("OwnerBusinessName")
                Vehicle.OwnerBusinessName = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ContactNameFirst")
                If VehicleNode.InnerText = "" Then
                    Vehicle.ContactNameFirst = Vehicle.OwnerNameFirst
                Else
                    Vehicle.ContactNameFirst = VehicleNode.InnerText
                End If

                VehicleNode = xNode.SelectSingleNode("ContactNameLast")
                If VehicleNode.InnerText = "" Then
                    Vehicle.ContactNameLast = Vehicle.OwnerNameLast
                Else
                    Vehicle.ContactNameLast = VehicleNode.InnerText
                End If

                VehicleNode = xNode.SelectSingleNode("ContactBestPhoneCD")
                Vehicle.ContactBestPhoneCD = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("VIN")
                Vehicle.VIN = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("LicensePlateNumber")
                Vehicle.LicensePlateNumber = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("VehicleYear")
                Vehicle.VehicleYear = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("Make")
                Vehicle.Make = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("Model")
                Vehicle.Model = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("Drivable")
                Vehicle.Drivable = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("PrimaryDamage")
                Vehicle.PrimaryDamage = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("SecondaryDamage")
                Vehicle.SecondaryDamage = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ImpactLocations")
                Vehicle.ImpactLocations = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopLocationID")
                Vehicle.ShopLocationID = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopSearchLogID")
                Vehicle.ShopSearchLogID = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("SelectedShopRank")
                Vehicle.SelectedShopRank = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("SelectedShopScore")
                Vehicle.SelectedShopScore = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("OwnerPhone")
                Vehicle.OwnerPhone = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ContactPhone")
                If VehicleNode.InnerText = "" Then
                    Vehicle.ContactPhone = Vehicle.OwnerPhone
                Else
                    Vehicle.ContactPhone = VehicleNode.InnerText
                End If

                VehicleNode = xNode.SelectSingleNode("ContactNightPhone")
                Vehicle.ContactNightPhone = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ContactAltPhone")
                Vehicle.ContactAltPhone = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("OwnerPhoneSumm")
                Vehicle.OwnerPhoneSumm = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ContactPhoneSumm")
                Vehicle.ContactPhoneSumm = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ContactNightPhoneSumm")
                Vehicle.ContactNightPhoneSumm = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ContactAltPhoneSumm")
                Vehicle.ContactAltPhoneSumm = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopName")
                Vehicle.ShopName = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopAddress1")
                Vehicle.ShopAddress1 = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopAddress2")
                Vehicle.ShopAddress2 = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopCity")
                Vehicle.ShopCity = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopState")
                Vehicle.ShopState = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopZip")
                Vehicle.ShopZip = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopPhone")
                Vehicle.ShopPhone = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopFax")
                Vehicle.ShopFax = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("Remarks")
                Vehicle.Remarks = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("RentalAuthorized_")
                Vehicle.RentalAuthorized = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("RentalDays")
                Vehicle.RentalDays = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("RentalDayAmount")
                Vehicle.RentalDayAmount = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("RentalMaxAmount")
                Vehicle.RentalMaxAmount = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("RentalDaysAuthorized")
                Vehicle.RentalDaysAuthorized = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("DeductibleAmt")
                Vehicle.DeductibleAmt = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("LimitAmt")
                Vehicle.LimitAmt = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("CoverageProfileUiCD")
                Vehicle.CoverageProfileUiCD = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("PrimaryDamageDescription")
                Vehicle.PrimaryDamageDescription = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("SecondaryDamageDescription")
                Vehicle.SecondaryDamageDescription = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ImpactLocations")
                Vehicle.ImpactLocations = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("AssignmentTypeIDDescription")
                Vehicle.AssignmentTypeIDDescription = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("Mileage")
                Vehicle.Mileage = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ClientCoverageTypeID")
                Vehicle.ClientCoverageTypeID = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ClientCoverageTypeDesc")
                Vehicle.ClientCoverageTypeDesc = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("VehicleNumber")
                Vehicle.VehicleNumber = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShowScriptingMemoFlag")
                Vehicle.ShowScriptingMemoFlag = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("CellPhoneCarrier")
                Vehicle.CellPhoneCarrier = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ContactEmailAddress")
                Vehicle.ContactEmailAddress = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("PrefMethodUpd")
                Vehicle.PrefMethodUpd = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ContactCellPhone")
                Vehicle.ContactCellPhone = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ContactCellPhoneSumm")
                Vehicle.ContactCellPhoneSumm = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("ShopRemarks")
                Vehicle.ShopRemarks = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("SourceApplicationPassthruDataVeh")
                Vehicle.SourceApplicationPassthruDataVeh = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("DrivingDirections")
                Vehicle.DrivingDirections = VehicleNode.InnerText

                VehicleNode = xNode.SelectSingleNode("RentalInstructions")
                Vehicle.RentalInstructions = VehicleNode.InnerText

                'Try
                '    '---------------------------------------
                '    ' These values are PS only, not used in
                '    ' RRP - So skip them if they don't exist
                '    '---------------------------------------
                '    VehicleNode = xNode.SelectSingleNode("AirBagDriverFront")
                '    Vehicle.AirBagDriverFront = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("AirBagDriverSide")
                '    Vehicle.AirBagDriverSide = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("AirBagDriverHeadliner")
                '    Vehicle.AirBagDriverHeadliner = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("AirBagPassengerFront")
                '    Vehicle.AirBagPassengerFront = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("AirBagPassengerSide")
                '    Vehicle.AirBagPassengerSide = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("BodyStyle")
                '    Vehicle.BodyStyle = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("Color")
                '    Vehicle.Color = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactAddress1")
                '    Vehicle.ContactAddress1 = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactAddress2")
                '    Vehicle.ContactAddress2 = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactAddressCity")
                '    Vehicle.ContactAddressCity = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactAddressState")
                '    Vehicle.ContactAddressState = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactAddressZip")
                '    Vehicle.ContactAddressZip = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactBestPhoneCode")
                '    Vehicle.ContactBestPhoneCode = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactBestTimeToCall")
                '    Vehicle.ContactBestTimeToCall = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactTitle")
                '    Vehicle.ContactNameTitle = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactAltPhoneExt")
                '    Vehicle.ContactAltPhoneExt = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactPhoneExt")
                '    Vehicle.ContactPhoneExt = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ContactNightPhoneExt")
                '    Vehicle.ContactNightPhoneExt = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("GlassDamageFlag")
                '    Vehicle.GlassDamageFlag = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("GlassDispatchNumber")
                '    Vehicle.GlassDispatchNumber = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("GlassNotDispatchedReason")
                '    Vehicle.GlassNotDispatchedReason = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("GlassReferenceNumber")
                '    Vehicle.GlassReferenceNumber = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("GlassShopAppointmentDate")
                '    Vehicle.GlassShopAppointmentDate = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("GlassShopName")
                '    Vehicle.GlassShopName = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("GlassShopPhoneNumber")
                '    Vehicle.GlassShopPhoneNumber = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("ImpactSpeed")
                '    Vehicle.ImpactSpeed = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("LicensePlateState")
                '    Vehicle.LicensePlateState = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("LocationAddress1")
                '    Vehicle.LocationAddress1 = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("LocationAddress2")
                '    Vehicle.LocationAddress2 = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("LocationAddressCity")
                '    Vehicle.LocationAddressCity = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("LocationAddressState")
                '    Vehicle.LocationAddressState = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("LocationAddressZip")
                '    Vehicle.LocationAddressZip = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("LocationName")
                '    Vehicle.LocationName = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("LocationPhone")
                '    Vehicle.LocationPhone = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("NADAId")
                '    Vehicle.NADAId = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerAddress1")
                '    Vehicle.OwnerAddress1 = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerAddressCity")
                '    Vehicle.OwnerAddressCity = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerAddressState")
                '    Vehicle.OwnerAddressState = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerAddressZip")
                '    Vehicle.OwnerAddressZip = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerPhoneAlternate")
                '    Vehicle.OwnerPhoneAlternate = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerPhoneAlternateExt")
                '    Vehicle.OwnerPhoneAlternateExt = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerPhoneDay")
                '    Vehicle.OwnerPhoneDay = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerPhoneDayExt")
                '    Vehicle.OwnerPhoneDayExt = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerPhoneNight")
                '    Vehicle.OwnerPhoneNight = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerPhoneNightExt")
                '    Vehicle.OwnerPhoneNightExt = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("OwnerBestPhoneCD")
                '    Vehicle.OwnerBestPhoneCD = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("PermissionToDrive")
                '    Vehicle.PermissionToDrive = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("PhysicalDamageFlag")
                '    Vehicle.PhysicalDamageFlag = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("PostedSpeed")
                '    Vehicle.PostedSpeed = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("PriorDamage")
                '    Vehicle.PriorDamage = VehicleNode.InnerText

                '    VehicleNode = xNode.SelectSingleNode("Remarks")
                '    Vehicle.Remarks = VehicleNode.InnerText
                'Catch ex As Exception
                '    '---------------------------------------
                '    ' Don't do anything, just ignore the 
                '    ' PS values if RRP
                '    '---------------------------------------
                'End Try

                Vehicles.Add(Vehicle)
                LogEvent("WSServerSideClaimProcessor", "SUCCESSFUL", "Vehicle (" & Vehicle.VehicleNumber & ") class file loaded", "Vehicle Class XML is in EventXML", SerializeAnObject(Vehicle))
            Next

            Return "0"
        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Loading of the vehicle class failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            LogEvent("WSServerSideClaimProcessor", "ERROR", "Vehicle class file load failed", "", sError)

            Return sError
        End Try
    End Function

    'Private Function LoadInvolvedClass(ByVal XMLWSClaim As XmlDocument) As String
    '    Dim InvolvedNode As XmlNode

    '    Try
    '        '---------------------------------------
    '        ' Data available for all Involveds
    '        '---------------------------------------
    '        InvolvedNode = XMLWSInvolved.SelectSingleNode("WebAssignment/Involved/LynxID")
    '        Involved.LynxID = InvolvedNode.InnerText

    '        Try
    '            '---------------------------------------
    '            ' These values are PS only, not used in
    '            ' RRP - So skip them if they don't exist
    '            '---------------------------------------
    '            InvolvedNode = XMLWSInvolved.SelectSingleNode("WebAssignment/Involved/CallerAddress1")
    '            Involved.CallerAddress1 = InvolvedNode.InnerText

    '        Catch ex As Exception
    '            '---------------------------------------
    '            ' Don't do anything, just ignore the 
    '            ' PS values if RRP
    '            '---------------------------------------
    '        End Try

    '        LogEvent("WSServerSideInvolvedProcessor", "SUCCESSFUL", "Involved class file loaded", "Involved Class XML is in EventXML", SerializeAnObject(Involved))
    '        'LogEvent("WSServerSideInvolvedProcessor", "SUCCESSFUL", "Involved class file loaded", "Involved Class XML is in EventXML", InvolvedNode.OuterXml)

    '        Return "0"
    '    Catch oExcept As Exception
    '        '------------------------------
    '        ' Notify of the Error
    '        '------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error: {0}", "WebSerices Failed: Loading of the Involved class failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
    '        LogEvent("WSServerSideInvolvedProcessor", "ERROR", "Involved class file load failed", "", sError)

    '        Return sError
    '    End Try
    'End Function


    Public Function FNOLClaimLoad(ByVal Claim As APDClaim) As String
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim iRC As Integer = 0
        Dim iValue As Integer = 0
        Dim bValue As Boolean = False

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspFNOLLoadClaim"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@LynxID", SqlDbType.BigInt)
            cmdSelect.Parameters("@LynxID").Value = CInt(Claim.LynxID)

            cmdSelect.Parameters.Add("@AdvanceAmount", SqlDbType.Money)
            cmdSelect.Parameters("@AdvanceAmount").Value = Nothing

            cmdSelect.Parameters.Add("@AgentName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@AgentName").Value = Nothing

            cmdSelect.Parameters.Add("@AgentPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@AgentPhone").Value = Nothing

            cmdSelect.Parameters.Add("@AgentExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@AgentExt").Value = Nothing

            cmdSelect.Parameters.Add("@CallerAddress1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@CallerAddress1").Value = Claim.CallerAddress1

            cmdSelect.Parameters.Add("@CallerAddress2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@CallerAddress2").Value = Nothing

            cmdSelect.Parameters.Add("@CallerAddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@CallerAddressCity").Value = Claim.CallerAddressCity

            cmdSelect.Parameters.Add("@CallerAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@CallerAddressState").Value = Claim.CallerAddressState

            cmdSelect.Parameters.Add("@CallerAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@CallerAddressZip").Value = Claim.CallerAddressZip

            cmdSelect.Parameters.Add("@CallerBestPhoneCode", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@CallerBestPhoneCode").Value = Nothing

            cmdSelect.Parameters.Add("@CallerBestTimeToCall", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@CallerBestTimeToCall").Value = Nothing

            cmdSelect.Parameters.Add("@CallerNameFirst", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@CallerNameFirst").Value = Claim.CallerNameFirst

            cmdSelect.Parameters.Add("@CallerNameLast", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@CallerNameLast").Value = Claim.CallerNameLast

            cmdSelect.Parameters.Add("@CallerNameTitle", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@CallerNameTitle").Value = Nothing

            cmdSelect.Parameters.Add("@CallerPhoneAlternate", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@CallerPhoneAlternate").Value = Nothing

            cmdSelect.Parameters.Add("@CallerPhoneAlternateExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@CallerPhoneAlternateExt").Value = Nothing

            cmdSelect.Parameters.Add("@CallerPhoneDay", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@CallerPhoneDay").Value = Claim.InsuredPhone

            cmdSelect.Parameters.Add("@CallerPhoneDayExt", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@CallerPhoneDayExt").Value = Nothing

            cmdSelect.Parameters.Add("@CallerPhoneNight", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@CallerPhoneNight").Value = Nothing 'Claim.CallerPhoneNight

            cmdSelect.Parameters.Add("@CallerPhoneNightExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@CallerPhoneNightExt").Value = Nothing

            If Claim.CallerRelationToInsuredID = Nothing Then
            Else
                cmdSelect.Parameters.Add("@CallerRelationToInsuredID", SqlDbType.Int)
                cmdSelect.Parameters("@CallerRelationToInsuredID").Value = CInt(Claim.CallerRelationToInsuredID)
            End If

            cmdSelect.Parameters.Add("@CarrierRepUserID", SqlDbType.Int)
            If Claim.CarrierRepUserID <> "" Then
                cmdSelect.Parameters("@CarrierRepUserID").Value = CInt(Claim.CarrierRepUserID)
            Else
                cmdSelect.Parameters("@CarrierRepUserID").Value = Nothing
            End If

            cmdSelect.Parameters.Add("@ContactAddress1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactAddress1").Value = Nothing 'Claim.ContactAddress1

            cmdSelect.Parameters.Add("@ContactAddress2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactAddress2").Value = Nothing

            cmdSelect.Parameters.Add("@ContactAddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@ContactAddressCity").Value = Nothing 'Claim.ContactAddressCity

            cmdSelect.Parameters.Add("@ContactAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@ContactAddressState").Value = Nothing 'Claim.ContactAddressState

            cmdSelect.Parameters.Add("@ContactAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@ContactAddressZip").Value = Nothing 'Claim.ContactAddressZip

            cmdSelect.Parameters.Add("@ContactBestPhoneCode", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@ContactBestPhoneCode").Value = Nothing

            cmdSelect.Parameters.Add("@ContactBestTimeToCall", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactBestTimeToCall").Value = Nothing

            cmdSelect.Parameters.Add("@ContactNameFirst", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactNameFirst").Value = Nothing

            cmdSelect.Parameters.Add("@ContactNameLast", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactNameLast").Value = Nothing 'Claim.InsuredNameFirst

            cmdSelect.Parameters.Add("@ContactNameTitle", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactNameTitle").Value = Nothing 'Claim.InsuredNameLast

            cmdSelect.Parameters.Add("@ContactPhoneAlternate", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactPhoneAlternate").Value = Nothing

            cmdSelect.Parameters.Add("@ContactPhoneAlternateExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@ContactPhoneAlternateExt").Value = Nothing

            cmdSelect.Parameters.Add("@ContactPhoneDay", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactPhoneDay").Value = Nothing 'Claim.InsuredPhone

            cmdSelect.Parameters.Add("@ContactPhoneDayExt", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactPhoneDayExt").Value = Nothing

            cmdSelect.Parameters.Add("@ContactPhoneNight", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactPhoneNight").Value = Nothing 'Claim.ContactPhoneNight

            cmdSelect.Parameters.Add("@ContactPhoneNightExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@ContactPhoneNightExt").Value = Nothing

            If Claim.ContactRelationToInsuredID = Nothing Then
            Else
                cmdSelect.Parameters.Add("@ContactRelationToInsuredID", SqlDbType.Int)
                cmdSelect.Parameters("@ContactRelationToInsuredID").Value = CInt(Claim.ContactRelationToInsuredID)
            End If

            cmdSelect.Parameters.Add("@CoverageClaimNumber", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@CoverageClaimNumber").Value = Claim.CoverageClaimNumber

            If Claim.DemoFlag = Nothing Then
            Else
                cmdSelect.Parameters.Add("@DemoFlag", SqlDbType.Int)
                cmdSelect.Parameters("@DemoFlag").Value = CBool(Claim.DemoFlag)
            End If

            cmdSelect.Parameters.Add("@FNOLUserID", SqlDbType.Int)
            cmdSelect.Parameters("@FNOLUserID").Value = CInt(Claim.FNOLUserID)

            cmdSelect.Parameters.Add("@InsuranceCompanyID", SqlDbType.Int)
            cmdSelect.Parameters("@InsuranceCompanyID").Value = CInt(Claim.InsuranceCompanyID)

            If Claim.IntakeSeconds = Nothing Then
            Else
                cmdSelect.Parameters.Add("@IntakeSeconds", SqlDbType.Int)
                cmdSelect.Parameters("@IntakeSeconds").Value = CInt(Claim.IntakeSeconds)
            End If

            cmdSelect.Parameters.Add("@LienHolderAddress1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@LienHolderAddress1").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderAddress2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@LienHolderAddress2").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderAddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@LienHolderAddressCity").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@LienHolderAddressState").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@LienHolderAddressZip").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@LienHolderPhone").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderFax", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@LienHolderFax").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderEmail", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@LienHolderEmail").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderContactName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@LienHolderContactName").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderPayoffAmount", SqlDbType.Money)
            cmdSelect.Parameters("@LienHolderPayoffAmount").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderPayoffExpirationDate", SqlDbType.DateTime)
            cmdSelect.Parameters("@LienHolderPayoffExpirationDate").Value = Nothing

            cmdSelect.Parameters.Add("@LienHolderAccountNumber", SqlDbType.VarChar, 250)
            cmdSelect.Parameters("@LienHolderAccountNumber").Value = Nothing

            cmdSelect.Parameters.Add("@LoGAmount", SqlDbType.Money)
            cmdSelect.Parameters("@LoGAmount").Value = Nothing

            cmdSelect.Parameters.Add("@LossDate", SqlDbType.DateTime)
            If Claim.LossDate <> "" Then
                cmdSelect.Parameters("@LossDate").Value = CDate(Claim.LossDate)
            Else
                cmdSelect.Parameters("@LossDate").Value = Nothing
            End If

            cmdSelect.Parameters.Add("@LossAddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@LossAddressCity").Value = Nothing 'Claim.LossAddressCity

            cmdSelect.Parameters.Add("@LossAddressCounty", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@LossAddressCounty").Value = Nothing

            cmdSelect.Parameters.Add("@LossAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@LossAddressState").Value = Claim.LossAddressState

            cmdSelect.Parameters.Add("@LossAddressStreet", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@LossAddressStreet").Value = Nothing 'Claim.LossAddressStreet

            cmdSelect.Parameters.Add("@LossAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@LossAddressZip").Value = Nothing

            cmdSelect.Parameters.Add("@LossDescription", SqlDbType.VarChar, 1000)
            cmdSelect.Parameters("@LossDescription").Value = Claim.LossDescription

            cmdSelect.Parameters.Add("@LossTime", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@LossTime").Value = Nothing

            cmdSelect.Parameters.Add("@LossTypeLevel1ID", SqlDbType.Int)
            cmdSelect.Parameters("@LossTypeLevel1ID").Value = Nothing

            cmdSelect.Parameters.Add("@LossTypeLevel2ID", SqlDbType.Int)
            cmdSelect.Parameters("@LossTypeLevel2ID").Value = Nothing

            cmdSelect.Parameters.Add("@LossTypeLevel3ID", SqlDbType.Int)
            cmdSelect.Parameters("@LossTypeLevel3ID").Value = Nothing

            cmdSelect.Parameters.Add("@NoticeDate", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@NoticeDate").Value = Nothing

            cmdSelect.Parameters.Add("@NoticeMethodID", SqlDbType.Int)
            cmdSelect.Parameters("@NoticeMethodID").Value = Nothing

            cmdSelect.Parameters.Add("@PoliceDepartmentName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@PoliceDepartmentName").Value = Nothing

            cmdSelect.Parameters.Add("@PolicyNumber", SqlDbType.VarChar, 20)
            cmdSelect.Parameters("@PolicyNumber").Value = Nothing 'Claim.PolicyNumber

            cmdSelect.Parameters.Add("@Remarks", SqlDbType.VarChar, 1000)
            cmdSelect.Parameters("@Remarks").Value = Claim.Remarks

            cmdSelect.Parameters.Add("@RoadLocationID", SqlDbType.Int)
            cmdSelect.Parameters("@RoadLocationID").Value = Nothing

            cmdSelect.Parameters.Add("@RoadTypeID", SqlDbType.Int)
            cmdSelect.Parameters("@RoadTypeID").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@SalvageName").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageAddress1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@SalvageAddress1").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageAddress2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@SalvageAddress2").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageAddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@SalvageAddressCity").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@SalvageAddressState").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@SalvageAddressZip").Value = Nothing

            cmdSelect.Parameters.Add("@SalvagePhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@SalvagePhone").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageFax", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@SalvageFax").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageEmail", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@SalvageEmail").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageContactName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@SalvageContactName").Value = Nothing

            cmdSelect.Parameters.Add("@SalvageControlNumber", SqlDbType.VarChar, 250)
            cmdSelect.Parameters("@SalvageControlNumber").Value = Nothing

            cmdSelect.Parameters.Add("@SettlementDate", SqlDbType.DateTime)
            cmdSelect.Parameters("@SettlementDate").Value = Nothing

            cmdSelect.Parameters.Add("@SettlementAmount", SqlDbType.Money)
            cmdSelect.Parameters("@SettlementAmount").Value = Nothing

            cmdSelect.Parameters.Add("@SourceApplicationCD", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@SourceApplicationCD").Value = Claim.SourceApplicationCD

            cmdSelect.Parameters.Add("@SourceApplicationPassThruData", SqlDbType.VarChar, 8000)
            cmdSelect.Parameters("@SourceApplicationPassThruData").Value = Nothing

            cmdSelect.Parameters.Add("@TripPurpose", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@TripPurpose").Value = Nothing

            cmdSelect.Parameters.Add("@WeatherConditionID", SqlDbType.Int)
            cmdSelect.Parameters("@WeatherConditionID").Value = Nothing

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            iRC = cmdSelect.ExecuteNonQuery

            Return CStr(iRC)
        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: FNOL Claim Load failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            LogEvent("WSServerSideClaimProcessor", "ERROR", "FNOL Claim table load failed", "", sError)

            Return sError
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    Public Function FNOLVehicleLoad(ByVal Vehicle As APDVehicle) As String
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim iRC As Integer = 0
        Dim iValue As Integer = 0
        Dim bValue As Boolean = False

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspFNOLLoadClaimVehicle"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@LynxID", SqlDbType.BigInt)
            cmdSelect.Parameters("@LynxID").Value = CInt(Claim.LynxID)

            cmdSelect.Parameters.Add("@VehicleNumber", SqlDbType.Int)
            cmdSelect.Parameters("@VehicleNumber").Value = CInt(Vehicle.VehicleNumber)

            cmdSelect.Parameters.Add("@AirBagDriverFront", SqlDbType.Bit)
            cmdSelect.Parameters("@AirBagDriverFront").Value = Nothing 'Vehicle.AirBagDriverFront

            cmdSelect.Parameters.Add("@AirBagDriverSide", SqlDbType.Bit)
            cmdSelect.Parameters("@AirBagDriverSide").Value = Nothing 'Vehicle.AirBagDriverSide

            cmdSelect.Parameters.Add("@AirBagHeadliner", SqlDbType.Bit)
            cmdSelect.Parameters("@AirBagHeadliner").Value = Nothing 'Vehicle.AirBagDriverHeadliner

            cmdSelect.Parameters.Add("@AirBagPassengerFront", SqlDbType.Bit)
            cmdSelect.Parameters("@AirBagPassengerFront").Value = Nothing ' Vehicle.AirBagPassengerFront

            cmdSelect.Parameters.Add("@AirBagPassengerSide", SqlDbType.Bit)
            cmdSelect.Parameters("@AirBagPassengerSide").Value = Nothing 'Vehicle.AirBagPassengerSide

            cmdSelect.Parameters.Add("@AssignmentTypeID", SqlDbType.Int)
            cmdSelect.Parameters("@AssignmentTypeID").Value = CInt(Vehicle.AssignmentTypeID)

            cmdSelect.Parameters.Add("@BodyStyle", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@BodyStyle").Value = Nothing 'Vehicle.BodyStyle

            '----------------------------------------------
            ' Lookup the ClientCoverageTypeID based on the
            ' Insurance Company and the Coverage Type
            '----------------------------------------------
            cmdSelect.Parameters.Add("@ClientCoverageTypeID", SqlDbType.Int)
            cmdSelect.Parameters("@ClientCoverageTypeID").Value = GetCoverageTypeID(Claim.InsuranceCompanyID, Vehicle.CoverageProfileCD) 'CInt(Vehicle.ClientCoverageTypeID)

            cmdSelect.Parameters.Add("@Color", SqlDbType.VarChar, 20)
            cmdSelect.Parameters("@Color").Value = Nothing 'Vehicle.Color

            cmdSelect.Parameters.Add("@ContactAddress1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactAddress1").Value = Nothing 'Vehicle.ContactAddress1

            cmdSelect.Parameters.Add("@ContactAddress2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactAddress2").Value = Nothing 'Vehicle.ContactAddress2

            cmdSelect.Parameters.Add("@ContactAddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@ContactAddressCity").Value = Nothing 'Vehicle.ContactAddressCity

            cmdSelect.Parameters.Add("@ContactAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@ContactAddressState").Value = Nothing 'Vehicle.ContactAddressState

            cmdSelect.Parameters.Add("@ContactAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@ContactAddressZip").Value = Nothing 'Vehicle.ContactAddressZip

            cmdSelect.Parameters.Add("@ContactAltPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactAltPhone").Value = Vehicle.ContactAltPhone

            cmdSelect.Parameters.Add("@ContactAltPhoneExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@ContactAltPhoneExt").Value = Nothing 'Vehicle.ContactAltPhoneExt

            'If Vehicle.ContactBestPhoneCode = Nothing Then
            '    cmdSelect.Parameters.Add("@ContactBestPhoneCD", SqlDbType.VarChar, 4)
            '    cmdSelect.Parameters("@ContactBestPhoneCD").Value = "D"
            'Else
            '    cmdSelect.Parameters.Add("@ContactBestPhoneCD", SqlDbType.VarChar, 4)
            '    cmdSelect.Parameters("@ContactBestPhoneCD").Value = Vehicle.ContactBestPhoneCode
            'End If

            cmdSelect.Parameters.Add("@ContactBestPhoneCD", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@ContactBestPhoneCD").Value = Vehicle.ContactBestPhoneCode

            cmdSelect.Parameters.Add("@ContactNameFirst", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactNameFirst").Value = Vehicle.ContactNameFirst

            cmdSelect.Parameters.Add("@ContactNameLast", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactNameLast").Value = Vehicle.ContactNameLast

            cmdSelect.Parameters.Add("@ContactNameTitle", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactNameTitle").Value = Nothing 'Vehicle.ContactNameTitle

            cmdSelect.Parameters.Add("@ContactNightPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactNightPhone").Value = Vehicle.ContactNightPhone

            cmdSelect.Parameters.Add("@ContactNightPhoneExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@ContactNightPhoneExt").Value = Nothing 'Vehicle.ContactNightPhoneExt

            cmdSelect.Parameters.Add("@ContactPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactPhone").Value = Vehicle.ContactPhone

            cmdSelect.Parameters.Add("@ContactPhoneExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@ContactPhoneExt").Value = Nothing 'Vehicle.ContactPhoneExt

            cmdSelect.Parameters.Add("@CoverageProfileCD", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@CoverageProfileCD").Value = Vehicle.CoverageProfileCD

            If Vehicle.Drivable = Nothing Then
            Else
                cmdSelect.Parameters.Add("@Drivable", SqlDbType.Int)
                cmdSelect.Parameters("@Drivable").Value = CInt(Vehicle.Drivable)
            End If

            cmdSelect.Parameters.Add("@ExposureCD", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@ExposureCD").Value = Vehicle.ExposureCD

            cmdSelect.Parameters.Add("@GlassDamageFlag", SqlDbType.Int)
            If Vehicle.GlassDamageFlag = "" Then
                iValue = 0
            Else
                iValue = CInt(Vehicle.GlassDamageFlag)
            End If
            cmdSelect.Parameters("@GlassDamageFlag").Value = iValue

            cmdSelect.Parameters.Add("@ImpactLocations", SqlDbType.VarChar, 100)
            If Vehicle.ImpactLocations = Nothing Then
                cmdSelect.Parameters("@ImpactLocations").Value = Vehicle.PrimaryDamage & "," & Vehicle.SecondaryDamage
            Else
                cmdSelect.Parameters("@ImpactLocations").Value = Vehicle.ImpactLocations
            End If

            cmdSelect.Parameters.Add("@ImpactSpeed", SqlDbType.VarChar, 10)
            cmdSelect.Parameters("@ImpactSpeed").Value = Nothing 'Vehicle.ImpactSpeed

            cmdSelect.Parameters.Add("@LicensePlateNumber", SqlDbType.VarChar, 10)
            cmdSelect.Parameters("@LicensePlateNumber").Value = Vehicle.LicensePlateNumber

            cmdSelect.Parameters.Add("@LicensePlateState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@LicensePlateState").Value = Nothing 'Vehicle.LicensePlateState

            cmdSelect.Parameters.Add("@LocationAddress1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@LocationAddress1").Value = Nothing 'Vehicle.LocationAddress1

            cmdSelect.Parameters.Add("@LocationAddress2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@LocationAddress2").Value = Nothing 'Vehicle.LocationAddress2

            cmdSelect.Parameters.Add("@LocationAddressCity", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@LocationAddressCity").Value = Nothing 'Vehicle.LocationAddressCity

            cmdSelect.Parameters.Add("@LocationName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@LocationName").Value = Nothing 'Vehicle.LocationName

            cmdSelect.Parameters.Add("@LocationPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@LocationPhone").Value = Nothing 'Vehicle.LocationPhone

            cmdSelect.Parameters.Add("@LocationAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@LocationAddressState").Value = Nothing 'Vehicle.LocationAddressState

            cmdSelect.Parameters.Add("@LocationAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@LocationAddressZip").Value = Nothing 'Vehicle.LocationAddressZip

            cmdSelect.Parameters.Add("@Make", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@Make").Value = Vehicle.Make

            cmdSelect.Parameters.Add("@Mileage", SqlDbType.VarChar, 10)
            cmdSelect.Parameters("@Mileage").Value = Vehicle.Mileage

            cmdSelect.Parameters.Add("@Model", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@Model").Value = Vehicle.Model

            cmdSelect.Parameters.Add("@NADAId", SqlDbType.Int)
            If Vehicle.NADAId = "" Then
                iValue = 0
            Else
                iValue = CInt(Vehicle.NADAId)
            End If
            cmdSelect.Parameters("@GlassDamageFlag").Value = iValue

            cmdSelect.Parameters.Add("@PermissionToDrive", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@PermissionToDrive").Value = Nothing 'Vehicle.PermissionToDrive

            cmdSelect.Parameters.Add("@PhysicalDamageFlag", SqlDbType.Bit)
            If Vehicle.PhysicalDamageFlag = "" Then
                iValue = False
            Else
                iValue = CBool(Vehicle.PhysicalDamageFlag)
            End If
            cmdSelect.Parameters("@PhysicalDamageFlag").Value = iValue

            cmdSelect.Parameters.Add("@PostedSpeed", SqlDbType.VarChar, 10)
            cmdSelect.Parameters("@PostedSpeed").Value = Nothing 'Vehicle.PostedSpeed

            cmdSelect.Parameters.Add("@PriorDamage", SqlDbType.VarChar, 100)
            cmdSelect.Parameters("@PriorDamage").Value = Nothing 'Vehicle.PriorDamage

            cmdSelect.Parameters.Add("@PriorityFlag", SqlDbType.Bit)
            cmdSelect.Parameters("@PriorityFlag").Value = Nothing 'Vehicle.PriorDamage

            cmdSelect.Parameters.Add("@Remarks", SqlDbType.VarChar, 1000)
            cmdSelect.Parameters("@Remarks").Value = Vehicle.Remarks

            cmdSelect.Parameters.Add("@RentalDaysAuthorized", SqlDbType.Int, 4)
            If Vehicle.RentalDaysAuthorized = "" Then
                iValue = 0
            Else
                iValue = CInt(Vehicle.RentalDaysAuthorized)
            End If
            cmdSelect.Parameters("@RentalDaysAuthorized").Value = iValue

            cmdSelect.Parameters.Add("@RentalInstructions", SqlDbType.VarChar, 250)
            cmdSelect.Parameters("@RentalInstructions").Value = Vehicle.RentalInstructions

            cmdSelect.Parameters.Add("@RepairLocationCity", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@RepairLocationCity").Value = Nothing

            cmdSelect.Parameters.Add("@RepairLocationState", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@RepairLocationState").Value = Nothing

            cmdSelect.Parameters.Add("@RepairLocationCounty", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@RepairLocationCounty").Value = Nothing

            cmdSelect.Parameters.Add("@SelectedShopRank", SqlDbType.Int)
            If Vehicle.SelectedShopRank = "" Then
                iValue = 0
            Else
                iValue = CInt(Vehicle.SelectedShopRank)
            End If
            cmdSelect.Parameters("@SelectedShopRank").Value = iValue

            cmdSelect.Parameters.Add("@SelectedShopScore", SqlDbType.Int)
            If Vehicle.SelectedShopScore = "" Then
                iValue = 0
            Else
                iValue = CInt(Vehicle.SelectedShopScore)
            End If
            cmdSelect.Parameters("@SelectedShopScore").Value = iValue

            If Vehicle.ShopLocationID = Nothing Then
            Else
                cmdSelect.Parameters.Add("@ShopLocationID", SqlDbType.Int)
                cmdSelect.Parameters("@ShopLocationID").Value = CInt(Vehicle.ShopLocationID)
            End If

            cmdSelect.Parameters.Add("@ShopRemarks", SqlDbType.VarChar, 1000)
            cmdSelect.Parameters("@ShopRemarks").Value = Vehicle.ShopRemarks

            If Vehicle.ShopSearchLogID = Nothing Then
            Else
                cmdSelect.Parameters.Add("@ShopSearchLogID", SqlDbType.Int)
                cmdSelect.Parameters("@ShopSearchLogID").Value = CInt(Vehicle.ShopSearchLogID)
            End If

            cmdSelect.Parameters.Add("@TitleName", SqlDbType.VarChar, 100)
            cmdSelect.Parameters("@TitleName").Value = Nothing

            cmdSelect.Parameters.Add("@TitleState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@TitleState").Value = Nothing

            cmdSelect.Parameters.Add("@TitleStatus", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@TitleStatus").Value = Nothing

            cmdSelect.Parameters.Add("@VehicleYear", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@VehicleYear").Value = Vehicle.VehicleYear

            cmdSelect.Parameters.Add("@VIN", SqlDbType.VarChar, 17)
            cmdSelect.Parameters("@VIN").Value = Vehicle.VIN

            cmdSelect.Parameters.Add("@SourceApplicationPassthruDataVeh", SqlDbType.VarChar, 8000)
            cmdSelect.Parameters("@SourceApplicationPassthruDataVeh").Value = Vehicle.SourceApplicationPassthruDataVeh

            cmdSelect.Parameters.Add("@PrefMethodUpd", SqlDbType.VarChar, 25)
            cmdSelect.Parameters("@PrefMethodUpd").Value = Vehicle.PrefMethodUpd

            cmdSelect.Parameters.Add("@CellPhoneCarrier", SqlDbType.VarChar, 25)
            cmdSelect.Parameters("@CellPhoneCarrier").Value = Vehicle.CellPhoneCarrier

            cmdSelect.Parameters.Add("@ContactCellPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ContactCellPhone").Value = Vehicle.ContactCellPhone

            cmdSelect.Parameters.Add("@ContactEmailAddress", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ContactEmailAddress").Value = Vehicle.ContactEmailAddress

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            iRC = cmdSelect.ExecuteNonQuery

            Return CStr(iRC)
        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: FNOL Vehicle Load failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            LogEvent("WSServerSideClaimProcessor", "ERROR", "FNOL Vehicle table load failed", "", sError)

            Return sError
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    Public Function FNOLInvolvedLoad(ByVal Vehicle As APDVehicle, ByVal isInsured As Boolean) As String
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim iRC As Integer = 0
        Dim iValue As Integer = 0
        Dim bValue As Boolean = False

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspFNOLLoadInvolved"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@LynxID", SqlDbType.BigInt)
            'cmdSelect.Parameters("@LynxID").Value = CInt(Vehicle.LynxID)
            cmdSelect.Parameters("@LynxID").Value = CInt(Claim.LynxID)

            '-----------------------------------------
            ' Adjust for Owner vs Insured
            '-----------------------------------------
            cmdSelect.Parameters.Add("@VehicleNumber", SqlDbType.Int)
            cmdSelect.Parameters("@VehicleNumber").Value = Vehicle.VehicleNumber
            If (isInsured) Then
                If (Vehicle.OwnerNameFirst = Claim.InsuredNameFirst) And (Vehicle.OwnerNameLast = Claim.InsuredNameLast) Then
                Else
                    cmdSelect.Parameters("@VehicleNumber").Value = 0
                End If
            End If

            cmdSelect.Parameters.Add("@Address1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@Address1").Value = Nothing 'Vehicle.ContactAddress1

            cmdSelect.Parameters.Add("@Address2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@Address2").Value = Nothing 'Vehicle.ContactAddress2

            cmdSelect.Parameters.Add("@AddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@AddressCity").Value = Nothing 'Vehicle.ContactAddressCity

            cmdSelect.Parameters.Add("@AddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@AddressState").Value = Nothing 'Vehicle.ContactAddressState

            cmdSelect.Parameters.Add("@AddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@AddressZip").Value = Nothing 'Vehicle.ContactAddressZip

            cmdSelect.Parameters.Add("@Age", SqlDbType.TinyInt)
            cmdSelect.Parameters("@Age").Value = Nothing

            cmdSelect.Parameters.Add("@AttorneyAddress1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@AttorneyAddress1").Value = Nothing

            cmdSelect.Parameters.Add("@AttorneyAddress2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@AttorneyAddress2").Value = Nothing

            cmdSelect.Parameters.Add("@AttorneyAddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@AttorneyAddressCity").Value = Nothing

            cmdSelect.Parameters.Add("@AttorneyAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@AttorneyAddressState").Value = Nothing

            cmdSelect.Parameters.Add("@AttorneyAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@AttorneyAddressZip").Value = Nothing

            cmdSelect.Parameters.Add("@AttorneyName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@AttorneyName").Value = Nothing

            cmdSelect.Parameters.Add("@AttorneyPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@AttorneyPhone").Value = Nothing

            cmdSelect.Parameters.Add("@AttorneyPhoneExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@AttorneyPhoneExt").Value = Nothing

            '-------------------------------------------
            ' This data can't be null so if it is, default
            ' it to D for Day
            '-------------------------------------------
            If Vehicle.ContactBestPhoneCode = Nothing Then
                cmdSelect.Parameters.Add("@BestPhoneCode", SqlDbType.VarChar, 4)
                cmdSelect.Parameters("@BestPhoneCode").Value = "D"
            Else
                cmdSelect.Parameters.Add("@BestPhoneCode", SqlDbType.VarChar, 4)
                cmdSelect.Parameters("@BestPhoneCode").Value = Vehicle.ContactBestPhoneCode
            End If

            cmdSelect.Parameters.Add("@BestTimeToCall", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@BestTimeToCall").Value = Vehicle.ContactBestTimeToCall

            '-----------------------------------------
            ' Adjust for Owner vs Insured
            '-----------------------------------------
            If isInsured Then
                cmdSelect.Parameters.Add("@BusinessName", SqlDbType.VarChar, 50)
                cmdSelect.Parameters("@BusinessName").Value = Claim.InsuredBusinessName
            Else
                cmdSelect.Parameters.Add("@BusinessName", SqlDbType.VarChar, 50)
                cmdSelect.Parameters("@BusinessName").Value = Vehicle.OwnerBusinessName
            End If

            cmdSelect.Parameters.Add("@DateOfBirth", SqlDbType.VarChar, 10)
            cmdSelect.Parameters("@DateOfBirth").Value = Nothing

            cmdSelect.Parameters.Add("@DoctorAddress1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@DoctorAddress1").Value = Nothing

            cmdSelect.Parameters.Add("@DoctorAddress2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@DoctorAddress2").Value = Nothing

            cmdSelect.Parameters.Add("@DoctorAddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@DoctorAddressCity").Value = Nothing

            cmdSelect.Parameters.Add("@DoctorAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@DoctorAddressState").Value = Nothing

            cmdSelect.Parameters.Add("@DoctorAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@DoctorAddressZip").Value = Nothing

            cmdSelect.Parameters.Add("@DoctorName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@DoctorName").Value = Nothing

            cmdSelect.Parameters.Add("@DoctorPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@DoctorPhone").Value = Nothing

            cmdSelect.Parameters.Add("@DoctorPhoneExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@DoctorPhoneExt").Value = Nothing

            cmdSelect.Parameters.Add("@DriverLicenseNumber", SqlDbType.VarChar, 20)
            cmdSelect.Parameters("@DriverLicenseNumber").Value = Nothing

            cmdSelect.Parameters.Add("@DriverLicenseState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@DriverLicenseState").Value = Nothing

            cmdSelect.Parameters.Add("@EmployerAddress1", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@EmployerAddress1").Value = Nothing

            cmdSelect.Parameters.Add("@EmployerAddress2", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@EmployerAddress2").Value = Nothing

            cmdSelect.Parameters.Add("@EmployerAddressCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@EmployerAddressCity").Value = Nothing

            cmdSelect.Parameters.Add("@EmployerAddressState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@EmployerAddressState").Value = Nothing

            cmdSelect.Parameters.Add("@EmployerAddressZip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@EmployerAddressZip").Value = Nothing

            cmdSelect.Parameters.Add("@EmployerName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@EmployerName").Value = Nothing

            cmdSelect.Parameters.Add("@EmployerPhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@EmployerPhone").Value = Nothing

            cmdSelect.Parameters.Add("@EmployerPhoneExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@EmployerPhoneExt").Value = Nothing

            cmdSelect.Parameters.Add("@Gender", SqlDbType.Char, 1)
            cmdSelect.Parameters("@Gender").Value = Nothing

            cmdSelect.Parameters.Add("@Injured", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@Injured").Value = Nothing

            cmdSelect.Parameters.Add("@InjuryDescription", SqlDbType.VarChar, 500)
            cmdSelect.Parameters("@InjuryDescription").Value = Nothing

            cmdSelect.Parameters.Add("@InjuryTypeId", SqlDbType.Int)
            cmdSelect.Parameters("@InjuryTypeId").Value = Nothing

            cmdSelect.Parameters.Add("@InvolvedTypeClaimant", SqlDbType.Bit)
            cmdSelect.Parameters("@InvolvedTypeClaimant").Value = 0     ' 03Feb2020 TVD - PASS IN A 0 to correct vehicle # issues > 1 because of txfb using vehicle.

            cmdSelect.Parameters.Add("@InvolvedTypeDriver", SqlDbType.Bit)
            cmdSelect.Parameters("@InvolvedTypeDriver").Value = Nothing

            '-------------------------------------------
            ' Determine who is the insured person here
            ' and by default make the them the owner
            '-------------------------------------------
            cmdSelect.Parameters.Add("@InvolvedTypeOwner", SqlDbType.Bit)
            cmdSelect.Parameters("@InvolvedTypeOwner").Value = True

            'If Vehicle.ExposureCD = 3 Then
            'cmdSelect.Parameters("@InvolvedTypeOwner").Value = True
            'End If


            'If (isInsured) Then
            '    If (Vehicle.OwnerNameFirst = Claim.InsuredNameFirst) And (Vehicle.OwnerNameLast = Claim.InsuredNameLast) Then
            '        '-------------------------------------------
            '        ' Insured is also the vehicle owner
            '        '-------------------------------------------
            '        cmdSelect.Parameters.Add("@InvolvedTypeInsured", SqlDbType.Bit)
            '        cmdSelect.Parameters("@InvolvedTypeInsured").Value = True
            '    Else
            '        cmdSelect.Parameters.Add("@InvolvedTypeInsured", SqlDbType.Bit)
            '        cmdSelect.Parameters("@InvolvedTypeInsured").Value = False
            '    End If
            'Else

            'End If

            cmdSelect.Parameters.Add("@InvolvedTypeInsured", SqlDbType.Int)
            cmdSelect.Parameters("@InvolvedTypeInsured").Value = 0
            If (isInsured) Then
                cmdSelect.Parameters("@InvolvedTypeInsured").Value = 1
            End If

            '-------------------------------------------
            ' Determine if the insured is the vehicle 
            ' owner, if so set owner flag.
            '-------------------------------------------
            'cmdSelect.Parameters.Add("@InvolvedTypeOwner", SqlDbType.Bit)
            '            If (Claim.InsuredNameFirst = Vehicle.OwnerNameFirst) And (Claim.InsuredNameLast = Vehicle.OwnerNameLast) Then ' 
            'If (Vehicle.ExposureCD = 1) And (Vehicle.OwnerNameFirst = Claim.InsuredNameFirst) And (Vehicle.OwnerNameLast = Claim.InsuredNameLast) Then
            'If isInsured Then
            'cmdSelect.Parameters("@InvolvedTypeOwner").Value = True
            'Else
            'cmdSelect.Parameters("@InvolvedTypeOwner").Value = False
            'End If

            cmdSelect.Parameters.Add("@InvolvedTypePassenger", SqlDbType.Bit)
            cmdSelect.Parameters("@InvolvedTypePassenger").Value = Nothing

            cmdSelect.Parameters.Add("@InvolvedTypePedestrian", SqlDbType.Bit)
            cmdSelect.Parameters("@InvolvedTypePedestrian").Value = Nothing

            cmdSelect.Parameters.Add("@InvolvedTypeWitness", SqlDbType.Bit)
            cmdSelect.Parameters("@InvolvedTypeWitness").Value = Nothing

            cmdSelect.Parameters.Add("@WitnessLocation", SqlDbType.VarChar, 500)
            cmdSelect.Parameters("@WitnessLocation").Value = Nothing

            cmdSelect.Parameters.Add("@LocationInVehicleId", SqlDbType.Int)
            cmdSelect.Parameters("@LocationInVehicleId").Value = Nothing

            '-----------------------------------------
            ' Adjust for Owner vs Insured
            '-----------------------------------------
            If isInsured Then
                cmdSelect.Parameters.Add("@NameFirst", SqlDbType.VarChar, 50)
                cmdSelect.Parameters("@NameFirst").Value = Claim.InsuredNameFirst

                cmdSelect.Parameters.Add("@NameLast", SqlDbType.VarChar, 50)
                cmdSelect.Parameters("@NameLast").Value = Claim.InsuredNameLast
            Else
                cmdSelect.Parameters.Add("@NameFirst", SqlDbType.VarChar, 50)
                cmdSelect.Parameters("@NameFirst").Value = Vehicle.OwnerNameFirst

                cmdSelect.Parameters.Add("@NameLast", SqlDbType.VarChar, 50)
                cmdSelect.Parameters("@NameLast").Value = Vehicle.OwnerNameLast
            End If

            cmdSelect.Parameters.Add("@NameTitle", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@NameTitle").Value = Nothing 'Vehicle.ContactNameTitle

            cmdSelect.Parameters.Add("@PhoneAlternateExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@PhoneAlternateExt").Value = Nothing 'Vehicle.ContactAltPhoneExt

            '-----------------------------------------
            ' Adjust for Owner vs Insured
            '-----------------------------------------
            If isInsured Then
                '------------------------
                ' Insured
                '------------------------
                cmdSelect.Parameters.Add("@PhoneDay", SqlDbType.VarChar, 15)
                cmdSelect.Parameters("@PhoneDay").Value = Claim.InsuredPhone

                cmdSelect.Parameters.Add("@PhoneNight", SqlDbType.VarChar, 15)
                cmdSelect.Parameters("@PhoneNight").Value = Nothing

                cmdSelect.Parameters.Add("@PhoneAlternate", SqlDbType.VarChar, 15)
                cmdSelect.Parameters("@PhoneAlternate").Value = Nothing
            Else
                '------------------------
                ' Owner
                '------------------------
                cmdSelect.Parameters.Add("@PhoneDay", SqlDbType.VarChar, 15)
                cmdSelect.Parameters("@PhoneDay").Value = Vehicle.OwnerPhone

                cmdSelect.Parameters.Add("@PhoneNight", SqlDbType.VarChar, 15)
                cmdSelect.Parameters("@PhoneNight").Value = Vehicle.OwnerPhoneNight

                cmdSelect.Parameters.Add("@PhoneAlternate", SqlDbType.VarChar, 15)
                cmdSelect.Parameters("@PhoneAlternate").Value = Vehicle.OwnerPhoneAlternate
            End If

            cmdSelect.Parameters.Add("@PhoneDayExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@PhoneDayExt").Value = Nothing 'Vehicle.ContactPhoneExt

            cmdSelect.Parameters.Add("@PhoneNightExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@PhoneNightExt").Value = Nothing 'Vehicle.ContactNightPhoneExt

            cmdSelect.Parameters.Add("@SeatBelt", SqlDbType.VarChar, 4)
            cmdSelect.Parameters("@SeatBelt").Value = Nothing

            cmdSelect.Parameters.Add("@TaxID", SqlDbType.Int)
            cmdSelect.Parameters("@TaxID").Value = Nothing

            cmdSelect.Parameters.Add("@ThirdPartyInsuranceName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ThirdPartyInsuranceName").Value = Nothing

            cmdSelect.Parameters.Add("@ThirdPartyInsurancePhone", SqlDbType.VarChar, 15)
            cmdSelect.Parameters("@ThirdPartyInsurancePhone").Value = Nothing

            cmdSelect.Parameters.Add("@ThirdPartyInsurancePhoneExt", SqlDbType.VarChar, 5)
            cmdSelect.Parameters("@ThirdPartyInsurancePhoneExt").Value = Nothing

            cmdSelect.Parameters.Add("@Violation", SqlDbType.Bit)
            cmdSelect.Parameters("@Violation").Value = Nothing

            cmdSelect.Parameters.Add("@ViolationDescription", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ViolationDescription").Value = Nothing

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            iRC = cmdSelect.ExecuteNonQuery

            Return CStr(iRC)
        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: FNOL Involved Load failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            LogEvent("WSServerSideClaimProcessor", "ERROR", "FNOL Involved table load failed", "", sError)

            Return sError
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    Public Function FNOLCoverageLoad(ByVal Vehicle As APDVehicle, ByVal CoverageProfileType As String) As String
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim iRC As Integer = 0
        Dim iValue As Integer = 0
        Dim bValue As Boolean = False

        Try
            ' Additional Debugging
            'LogEvent("WSServerSideClaimProcessor", "DEBUGGING", "FNOLCoverageLoad Data Load Debugging", "Details in EventXML: ", Claim.InsuranceCompanyID & " - " & CoverageProfileType)

            '-------------------------------------------
            ' Check to see if CoverageProfileType is
            ' authorized for the insurance company
            '-------------------------------------------
            If GetCoverageTypeID(Claim.InsuranceCompanyID, CoverageProfileType) > 0 Then
            Else
                Throw New SystemException("XMLValidationError: Vehicle Tag: CoverageProfileCD = " & CoverageProfileType & " is not a valid coverage for insurance company: " & Claim.InsuranceCompanyID)
            End If

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspFNOLLoadClaimCoverage"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@LynxID", SqlDbType.BigInt)
            cmdSelect.Parameters("@LynxID").Value = CInt(Claim.LynxID)

            cmdSelect.Parameters.Add("@ClientCode", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ClientCode").Value = Nothing

            Select Case UCase(CoverageProfileType)
                Case "RENT"
                    cmdSelect.Parameters.Add("@ClientCoverageTypeID", SqlDbType.Int)
                    cmdSelect.Parameters("@ClientCoverageTypeID").Value = GetCoverageTypeID(Claim.InsuranceCompanyID, CoverageProfileType)   '--Rental--

                    cmdSelect.Parameters.Add("@CoverageTypeCD", SqlDbType.VarChar, 4)
                    cmdSelect.Parameters("@CoverageTypeCD").Value = CoverageProfileType

                    '-----------------------------------
                    ' 1st Party Rental Vehicle
                    '-----------------------------------
                    If Vehicle.ExposureCD = "1" Then
                        '-----------------------------------
                        ' 1st Party Rental
                        '-----------------------------------
                        If Vehicle.RentalMaxAmount = Nothing Then
                        Else
                            cmdSelect.Parameters.Add("@LimitAmt", SqlDbType.Int)
                            cmdSelect.Parameters("@LimitAmt").Value = CInt(Vehicle.RentalMaxAmount)
                        End If

                        If Vehicle.RentalDayAmount = Nothing Then
                        Else
                            cmdSelect.Parameters.Add("@LimitDailyAmt", SqlDbType.Int)
                            cmdSelect.Parameters("@LimitDailyAmt").Value = CInt(Vehicle.RentalDayAmount)
                        End If

                        If Vehicle.RentalDaysAuthorized = Nothing Then
                        Else
                            cmdSelect.Parameters.Add("@MaximumDays", SqlDbType.TinyInt)
                            cmdSelect.Parameters("@MaximumDays").Value = CInt(Vehicle.RentalDaysAuthorized)
                        End If
                    Else
                        '-----------------------------------
                        ' 3rd Party Rental Vehicle
                        '-----------------------------------
                        If Claim.RentalDaysAuthorized = Nothing Then
                        Else
                            cmdSelect.Parameters.Add("@MaximumDays", SqlDbType.TinyInt)
                            cmdSelect.Parameters("@MaximumDays").Value = CInt(Vehicle.RentalDaysAuthorized)
                        End If
                    End If
                Case "COLL"
                    cmdSelect.Parameters.Add("@ClientCoverageTypeID", SqlDbType.Int)
                    cmdSelect.Parameters("@ClientCoverageTypeID").Value = GetCoverageTypeID(Claim.InsuranceCompanyID, Vehicle.CoverageProfileCD)   '--From <Vehicle>--

                    cmdSelect.Parameters.Add("@CoverageTypeCD", SqlDbType.VarChar, 4)
                    cmdSelect.Parameters("@CoverageTypeCD").Value = Vehicle.CoverageProfileCD

                    If Claim.CollisionDeductibleAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@DeductibleAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@DeductibleAmt").Value = CInt(Claim.CollisionDeductibleAmt)
                    End If

                    If Claim.CollisionLimitAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@LimitAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@LimitAmt").Value = CInt(Claim.CollisionLimitAmt)
                    End If

                    'If Vehicle.LimitAmt = Nothing Then
                    'Else
                    '    cmdSelect.Parameters.Add("@LimitDailyAmt", SqlDbType.Int)
                    '    cmdSelect.Parameters("@LimitDailyAmt").Value = CInt(Vehicle.LimitAmt)
                    'End If
                Case "COMP"
                    cmdSelect.Parameters.Add("@ClientCoverageTypeID", SqlDbType.Int)
                    cmdSelect.Parameters("@ClientCoverageTypeID").Value = GetCoverageTypeID(Claim.InsuranceCompanyID, Vehicle.CoverageProfileCD)   '--From <Vehicle>--

                    cmdSelect.Parameters.Add("@CoverageTypeCD", SqlDbType.VarChar, 4)
                    cmdSelect.Parameters("@CoverageTypeCD").Value = Vehicle.CoverageProfileCD

                    If Claim.ComprehensiveDeductibleAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@DeductibleAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@DeductibleAmt").Value = CInt(Claim.ComprehensiveDeductibleAmt)
                    End If

                    If Claim.ComprehensiveLimitAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@LimitAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@LimitAmt").Value = CInt(Claim.ComprehensiveLimitAmt)
                    End If

                    'If Vehicle.LimitAmt = Nothing Then
                    'Else
                    '    cmdSelect.Parameters.Add("@LimitDailyAmt", SqlDbType.Int)
                    '    cmdSelect.Parameters("@LimitDailyAmt").Value = CInt(Vehicle.LimitAmt)
                    'End If
                Case "UIM"
                    cmdSelect.Parameters.Add("@ClientCoverageTypeID", SqlDbType.Int)
                    cmdSelect.Parameters("@ClientCoverageTypeID").Value = GetCoverageTypeID(Claim.InsuranceCompanyID, Vehicle.CoverageProfileCD)   '--From <Vehicle>--

                    cmdSelect.Parameters.Add("@CoverageTypeCD", SqlDbType.VarChar, 4)
                    cmdSelect.Parameters("@CoverageTypeCD").Value = Vehicle.CoverageProfileCD

                    If Claim.UnderInsuredDeductibleAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@DeductibleAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@DeductibleAmt").Value = CInt(Claim.UnderInsuredDeductibleAmt)
                    End If

                    If Claim.UnderInsuredLimitAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@LimitAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@LimitAmt").Value = CInt(Claim.UnderInsuredLimitAmt)
                    End If

                    'If Vehicle.LimitAmt = Nothing Then
                    'Else
                    '    cmdSelect.Parameters.Add("@LimitDailyAmt", SqlDbType.Int)
                    '    cmdSelect.Parameters("@LimitDailyAmt").Value = CInt(Vehicle.LimitAmt)
                    'End If
                Case "UM"
                    cmdSelect.Parameters.Add("@ClientCoverageTypeID", SqlDbType.Int)
                    cmdSelect.Parameters("@ClientCoverageTypeID").Value = GetCoverageTypeID(Claim.InsuranceCompanyID, Vehicle.CoverageProfileCD)   '--From <Vehicle>--

                    cmdSelect.Parameters.Add("@CoverageTypeCD", SqlDbType.VarChar, 4)
                    cmdSelect.Parameters("@CoverageTypeCD").Value = Vehicle.CoverageProfileCD

                    If Claim.UnInsuredDeductibleAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@DeductibleAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@DeductibleAmt").Value = CInt(Claim.UnInsuredDeductibleAmt)
                    End If

                    If Claim.UnInsuredLimitAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@LimitAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@LimitAmt").Value = CInt(Claim.UnInsuredLimitAmt)
                    End If

                    'If Vehicle.LimitAmt = Nothing Then
                    'Else
                    '    cmdSelect.Parameters.Add("@LimitDailyAmt", SqlDbType.Int)
                    '    cmdSelect.Parameters("@LimitDailyAmt").Value = CInt(Vehicle.LimitAmt)
                    'End If
                Case "LIAB"
                    cmdSelect.Parameters.Add("@ClientCoverageTypeID", SqlDbType.Int)
                    cmdSelect.Parameters("@ClientCoverageTypeID").Value = GetCoverageTypeID(Claim.InsuranceCompanyID, Vehicle.CoverageProfileCD)   '--From <Vehicle>--

                    cmdSelect.Parameters.Add("@CoverageTypeCD", SqlDbType.VarChar, 4)
                    cmdSelect.Parameters("@CoverageTypeCD").Value = Vehicle.CoverageProfileCD

                    If Claim.LiabilityDeductibleAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@DeductibleAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@DeductibleAmt").Value = CInt(Claim.LiabilityDeductibleAmt)
                    End If

                    If Claim.LiabilityLimitAmt = Nothing Then
                    Else
                        cmdSelect.Parameters.Add("@LimitAmt", SqlDbType.Int)
                        cmdSelect.Parameters("@LimitAmt").Value = CInt(Claim.LiabilityLimitAmt)
                    End If

                    'If Vehicle.LimitAmt = Nothing Then
                    'Else
                    '    cmdSelect.Parameters.Add("@LimitDailyAmt", SqlDbType.Int)
                    '    cmdSelect.Parameters("@LimitDailyAmt").Value = CInt(Vehicle.LimitAmt)
                    'End If
            End Select

            ' Additional Debugging
            'LogEvent("WSServerSideClaimProcessor", "DEBUGGING", "After CoverageLoad Data Populating", "Details in EventXML: ", cmdSelect.CommandText & " - " & cmdSelect.Parameters("@LynxID").Value & " - " & cmdSelect.Parameters("@ClientCoverageTypeID").Value & " - " & cmdSelect.Parameters("@CoverageTypeCD").Value & " - " & )

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            iRC = cmdSelect.ExecuteNonQuery

            Return CStr(iRC)
        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: FNOL Coverage Load failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            LogEvent("WSServerSideClaimProcessor", "ERROR", "FNOL Coverage table load failed", "", sError)

            Return sError
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    Public Function FNOLInsClaim(ByVal LynxID As String, ByVal NewClaimFlag As String) As String
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim iRC As Integer = 0
        Dim iValue As Integer = 0
        Dim bValue As Boolean = False
        Dim iCnt As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspFNOLInsClaim"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@LynxID", SqlDbType.BigInt)
            cmdSelect.Parameters("@LynxID").Value = LynxID

            cmdSelect.Parameters.Add("@NewClaimFlag", SqlDbType.Int)
            cmdSelect.Parameters("@NewClaimFlag").Value = NewClaimFlag

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            ' 26Feb2014 - TVD - Adding looping on the SP call to correct deadlock issues.
            Dim bProcessed As Boolean = False
            While (iCnt < 6) And Not bProcessed
                Try
                    '------------------------------
                    ' Try EXECUTE
                    '------------------------------
                    iRC = cmdSelect.ExecuteNonQuery
                    bProcessed = True
                Catch oProcessException As Exception
                    '--------------------------------
                    ' EXECUTE failed notify support
                    ' and reprocess
                    '--------------------------------
                    Select Case iCnt
                        Case 0
                            '-----------------------------------------
                            ' 1st attempt failure, notify as warning
                            '-----------------------------------------
                            SendMail(AppSettings("ErrorToEmail"), AppSettings("ErrorFromEmail"), "", "WARNING: FNOLInsClaim - 1st Attempt failed.", "WARNING: FNOLInsClaim - 1st Attempt failed.  Reason: " & oProcessException.Message, AppSettings("SMTPServer"))
                            LogEvent("WSServerSideClaimProcessor", "WARNING", "FNOLInsClaim - 1st Attempt failed.", "FNOLInsClaim - 1st Attempt failed.  Reason: " & oProcessException.Message, "")
                        Case 5
                            '-----------------------------------------
                            ' 5th attempt failure, notify as Error
                            '-----------------------------------------
                            SendMail(AppSettings("ErrorToEmail"), AppSettings("ErrorFromEmail"), "", "ERROR: FNOLInsClaim - 5th Attempt failed.", "ERROR: FNOLInsClaim - 5th Attempt failed.  Reason: " & oProcessException.Message, AppSettings("SMTPServer"))
                            LogEvent("WSServerSideClaimProcessor", "ERROR", "SERIOUS - FNOLInsClaim - 5th Attempt failed.", "SERIOUS - FNOLInsClaim - 5th Attempt failed.  Reason: " & oProcessException.Message, "")
                            Throw New SystemException("An unexpected error has caused a delay in processing this claim.  LYNX Services have been notified and it will be processed shortly.")
                        Case Else
                            LogEvent("WSServerSideClaimProcessor", "WARNING", "FNOLInsClaim - " & iCnt & " Attempt failed.", "FNOLInsClaim - Attempt failed.  Reason: " & oProcessException.Message, "")
                    End Select
                    bProcessed = False
                End Try
                '------------------------------
                ' If processed continue
                '------------------------------
                If bProcessed Then
                    Exit While
                End If
                System.Threading.Thread.Sleep(AppSettings("ErrorRetryTime"))
                iCnt += 1
            End While

            Return CStr(iRC)
        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: FNOL Insert Claim failed (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            LogEvent("WSServerSideClaimProcessor", "ERROR", "FNOL Insert Claim failed", "", sError)
            Return oExcept.Message
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: LogEvent
    ' This APD function make a log entry into the utb_apd_event_log
    ' table for debug purposes
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function make a log entry into the utb_apd_event_log table for debug purposes.")>
    Public Function LogEvent(ByVal EventType As String, ByVal EventStatus As String, ByVal EventDescription As String, ByVal EventDetailedDescription As String, ByVal EventXml As String) As Integer
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim Reader As SqlClient.SqlDataReader
        Dim iRecID As Integer = 0

        Try
            'SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "ERROR-DEBUG", "XML=" & EventXml & " - " & EventDescription, ConfigurationManager.AppSettings("SMTPServer"))

            '-------------------------------------------
            ' If Debug = YES then log to flat file also
            '-------------------------------------------
            If AppSettings("Debug") = "YES" Then
                '----------------------------------------
                ' Took this out for now.  Has issues
                ' with the file system being already
                ' in use
                '----------------------------------------
                'LogEventToFile(EventType, EventStatus, EventDescription, EventDetailedDescription, EventXml)
            End If

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspAPDEventInsLogEntry"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@vEventType", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vEventType").Value = EventType

            cmdSQL.Parameters.Add("@vEventStatus", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vEventStatus").Value = EventStatus

            cmdSQL.Parameters.Add("@vEventDescription", SqlDbType.VarChar, 100)
            cmdSQL.Parameters("@vEventDescription").Value = EventDescription

            cmdSQL.Parameters.Add("@vEventDetailedDescription", SqlDbType.VarChar, 1000)
            cmdSQL.Parameters("@vEventDetailedDescription").Value = EventDetailedDescription

            cmdSQL.Parameters.Add("@vEventXML", SqlDbType.Text)
            cmdSQL.Parameters("@vEventXML").Value = EventXml

            '-------------------------------------------
            ' Output parameters from the stored proc
            '-------------------------------------------
            Dim cmdOutput As New SqlParameter("@iRecID", SqlDbType.Int, 4)
            cmdOutput.Direction = ParameterDirection.Output
            cmdSQL.Parameters.Add(cmdOutput)

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader = cmdSQL.ExecuteReader

            'If Reader.HasRows Then
            'Reader.Read()
            If cmdSQL.Parameters("@iRecID").Value > 0 Then
                Return cmdSQL.Parameters("@iRecID").Value
            Else
                Throw New System.Exception(String.Format("Error: {0}", "WebSerices Failed: Event logging failed to insert a row (LogEvent)..."))
            End If
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Event logging")
            sBody = String.Format("Error: {0}", "WebSerices Failed: Event logging failed to insert a row (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
            Return 0
        Finally
            cmdSQL = Nothing
            Reader.Close()
            Conn.Close()
        End Try
    End Function

    Public Function LogEventToFile(ByVal EventType As String, ByVal EventStatus As String, ByVal EventDescription As String, ByVal EventDetailedDescription As String, ByVal EventXml As String) As Integer
        Dim fs As StreamWriter
        Dim sPathFile As String = ""

        sPathFile = System.DateTime.Now.ToString("yyyy-MM-dd") & " - APD Select - WebService - " & Server.MachineName & ".log"
        fs = File.AppendText(ConfigurationManager.AppSettings("LogFilePath") & sPathFile)

        Try
            fs.WriteLine("[APDFoundation-WebService] ==================================[" & System.DateTime.Now & "]===================================")
            fs.WriteLine("[APDFoundation-WebService] " & EventType & " - " & EventStatus)
            fs.WriteLine("     ---> " & EventDescription)
            fs.WriteLine("    XML = " & Chr(34) & EventXml & Chr(34))
            fs.WriteLine("[APDFoundation-WebService] ======================================================================================")
            fs.Close()

            Return 0
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Event logging")
            sBody = String.Format("Error: {0}", "WebSerices Failed: Event logging to file failed (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
            Return 1
        Finally
            fs.Close()
            fs.Dispose()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: LogEvent
    ' This APD function make a log entry to a file
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function make a log entry into the utb_apd_event_log table for debug purposes.")>
    Public Function LogEventToFile(ByVal LogFileName As String, ByVal EventType As String, ByVal EventStatus As String, ByVal EventDescription As String, ByVal EventDetailedDescription As String, ByVal EventXml As String) As Integer
        Dim fs As StreamWriter = Nothing
        Dim sPathFile As String = ""

        sPathFile = System.DateTime.Now.ToString("yyyy-MM-dd") & " - APD Select - " & LogFileName & " - " & Server.MachineName & ".log"
        fs = File.AppendText(ConfigurationManager.AppSettings("LogFilePath") & sPathFile)

        Try
            fs.WriteLine("[APDFoundation-" & LogFileName & "] ==================================[" & System.DateTime.Now & "]===================================")
            fs.WriteLine("[APDFoundation-" & LogFileName & "] " & EventType & " - " & EventStatus)
            fs.WriteLine("     ---> " & EventDescription)
            fs.WriteLine("    XML = " & Chr(34) & EventXml & Chr(34))
            fs.WriteLine("[/APDFoundation-" & LogFileName & "] ======================================================================================")
            fs.Close()

            Return 0
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Event logging")
            sBody = String.Format("Error: {0}", "WebSerices Failed: Event logging to file by name failed (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
            Return 1
        Finally
            fs.Dispose()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClassicSessionVars
    ' This APD function takes a classic asp APD session GUID and 
    ' looks up the session information.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function takes a classic asp APD session GUID and looks up the session information.")>
    Public Function GetClassicSessionVars(ByVal SessionID As String, ByVal SessionVariable As String) As List(Of ClassicSessionVars) 'XmlDocument
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim iRecID As Integer = 0
        Dim sData As String = ""
        Dim wsClass As New ClassicSessionVars
        Dim wsData As New List(Of ClassicSessionVars)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspSessionDataGet"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@SessionID", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@SessionID").Value = SessionID

            If SessionVariable <> "" Then
                cmdSQL.Parameters.Add("@SessionVariable", SqlDbType.VarChar, 7500)
                cmdSQL.Parameters("@SessionVariable").Value = SessionVariable
            End If

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSQL.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New ClassicSessionVars
                    wsClass.SessionID = Reader1.Item("SessionID").ToString
                    wsClass.SessionVariable = Reader1.Item("SessionVariable").ToString
                    wsClass.SessionValue = Reader1.Item("SessionValue").ToString
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate").ToString
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New ClassicSessionVars
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New ClassicSessionVars
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSQL = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetOfficeDetailsByOffice
    ' This APD function returns detailed information related to client
    ' office setup from the database table by OfficeID.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns detailed information related to client office setup from the database table by OfficeID")>
    Public Function GetOfficeDetailsByOffice(ByVal OfficeID As Integer) As List(Of utb_office)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_office
        Dim wsData As New List(Of utb_office)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetOfficeDetails"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iOfficeID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iOfficeID").Value = OfficeID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_office
                    wsClass.OfficeID = Reader1.Item("OfficeID")
                    wsClass.InsuranceCompanyID = Reader1.Item("InsuranceCompanyID")
                    wsClass.OfficeName = Reader1.Item("OfficeName")
                    wsClass.ClientOfficeId = Reader1.Item("ClientOfficeID")
                    wsClass.Address1 = Reader1.Item("Address1")
                    wsClass.Address2 = Reader1.Item("Address2")
                    wsClass.AddressCity = Reader1.Item("AddressCity")
                    wsClass.AddressState = Reader1.Item("AddressState")
                    wsClass.AddressZip = Reader1.Item("AddressZip")
                    wsClass.MailingAddress1 = Reader1.Item("MailingAddress1")
                    wsClass.MailingAddress2 = Reader1.Item("MailingAddress2")
                    wsClass.MailingAddressCity = Reader1.Item("MailingAddressCity")
                    wsClass.MailingAddressState = Reader1.Item("MailingAddressState")
                    wsClass.MailingAddressZip = Reader1.Item("MailingAddressZip")
                    wsClass.CCEmailAddress = Reader1.Item("CCEmailAddress")
                    wsClass.ClaimNumberFormatJS = Reader1.Item("ClaimNumberFormatJS")
                    wsClass.ClaimNumberValidJS = Reader1.Item("ClaimNumberValidJS")
                    wsClass.ClaimNumberMsgText = Reader1.Item("ClaimNumberMsgText")
                    wsClass.PhoneAreaCode = Reader1.Item("PhoneAreaCode")
                    wsClass.PhoneExchangeNumber = Reader1.Item("PhoneExchangeNumber")
                    wsClass.PhoneExtensionNumber = Reader1.Item("PhoneExtensionNumber")
                    wsClass.PhoneUnitNumber = Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxAreaCode = Reader1.Item("FaxAreaCode")
                    wsClass.FaxExchangeNumber = Reader1.Item("FaxExchangeNumber")
                    wsClass.FaxExtensionNumber = Reader1.Item("FaxExtensionNumber")
                    wsClass.FaxUnitNumber = Reader1.Item("FaxUnitNumber")
                    wsClass.ReturnDocDestinationValue = Reader1.Item("ReturnDocDestinationValue")
                    wsClass.ReturnDocEmailAddress = Reader1.Item("ReturnDocEmailAddress")
                    wsClass.ReturnDocFaxAreaCode = Reader1.Item("ReturnDocFaxAreaCode")
                    wsClass.ReturnDocFaxExchangeNumber = Reader1.Item("ReturnDocFaxExchangeNumber")
                    wsClass.ReturnDocFaxExtensionNumber = Reader1.Item("ReturnDocFaxExtensionNumber")
                    wsClass.ReturnDocFaxUnitNumber = Reader1.Item("ReturnDocFaxUnitNumber")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")

                    wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    wsClass.ReturnDocFaxCombined = Reader1.Item("ReturnDocFaxAreaCode") & "-" & Reader1.Item("ReturnDocFaxExchangeNumber") & "-" & Reader1.Item("ReturnDocFaxUnitNumber")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_office
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_office
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetUsers
    ' This APD function returns a list of users by InsuranceCompanyID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a list of users by InsuranceCompanyID")>
    Public Function GetUsers(ByVal InsuranceCompanyID As Integer) As List(Of utb_user)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_user
        Dim wsData As New List(Of utb_user)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetUsers"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_user
                    wsClass.UserID = Reader1.Item("UserID")
                    wsClass.OfficeID = Reader1.Item("OfficeID")
                    wsClass.SupervisorUserID = Reader1.Item("SupervisorUserID")
                    wsClass.AssignmentBeginDate = Reader1.Item("AssignmentBeginDate")
                    wsClass.AssignmentEndDate = Reader1.Item("AssignmentEndDate")
                    wsClass.ClientUserId = Reader1.Item("ClientUserId")
                    wsClass.EmailAddress = Reader1.Item("EmailAddress")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.FaxAreaCode = Reader1.Item("FaxAreaCode")
                    wsClass.FaxExchangeNumber = Reader1.Item("FaxExchangeNumber")
                    wsClass.FaxExtensionNumber = Reader1.Item("FaxExtensionNumber")
                    wsClass.FaxUnitNumber = Reader1.Item("FaxUnitNumber")
                    wsClass.LastAssignmentDate = Reader1.Item("LastAssignmentDate")
                    wsClass.NameFirst = Reader1.Item("NameFirst")
                    wsClass.NameLast = Reader1.Item("NameLast")
                    wsClass.NameTitle = Reader1.Item("NameTitle")
                    wsClass.OperatingFridayEndTime = Reader1.Item("OperatingFridayEndTime")
                    wsClass.OperatingFridayStartTime = Reader1.Item("OperatingFridayStartTime")
                    wsClass.OperatingMondayEndTime = Reader1.Item("OperatingMondayEndTime")
                    wsClass.OperatingMondayStartTime = Reader1.Item("OperatingMondayStartTime")
                    wsClass.OperatingSaturdayEndTime = Reader1.Item("OperatingSaturdayEndTime")
                    wsClass.OperatingSaturdayStartTime = Reader1.Item("OperatingSaturdayStartTime")
                    wsClass.OperatingSundayEndTime = Reader1.Item("OperatingSundayEndTime")
                    wsClass.OperatingSundayStartTime = Reader1.Item("OperatingSundayStartTime")
                    wsClass.OperatingThursdayEndTime = Reader1.Item("OperatingThursdayEndTime")
                    wsClass.OperatingThursdayStartTime = Reader1.Item("OperatingThursdayStartTime")
                    wsClass.OperatingThursdayStartTime = Reader1.Item("OperatingThursdayStartTime")
                    wsClass.OperatingTuesdayEndTime = Reader1.Item("OperatingTuesdayEndTime")
                    wsClass.OperatingTuesdayStartTime = Reader1.Item("OperatingTuesdayStartTime")
                    wsClass.OperatingWednesdayEndTime = Reader1.Item("OperatingWednesdayEndTime")
                    wsClass.OperatingWednesdayStartTime = Reader1.Item("OperatingWednesdayStartTime")
                    wsClass.PhoneAreaCode = Reader1.Item("PhoneAreaCode")
                    wsClass.PhoneExchangeNumber = Reader1.Item("PhoneExchangeNumber")
                    wsClass.PhoneExtensionNumber = Reader1.Item("PhoneExtensionNumber")
                    wsClass.PhoneUnitNumber = Reader1.Item("PhoneUnitNumber")
                    wsClass.SupervisorFlag = Reader1.Item("SupervisorFlag")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")
                    wsClass.CCCOneCommunicationAddress = Reader1.Item("CCCOneCommunicationAddress")
                    wsClass.ReceiveCCCOneAssignmentFlag = Reader1.Item("ReceiveCCCOneAssignmentFlag")
                    wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_user
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_user
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetUsersSimple
    ' This APD function returns a simple list of users by InsuranceCompanyID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a simple list of users by InsuranceCompanyID")>
    Public Function GetUsersSimple(ByVal InsuranceCompanyID As Integer) As List(Of utb_user_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_user_simple
        Dim wsData As New List(Of utb_user_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetUsers"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_user_simple
                    wsClass.UserID = Reader1.Item("UserID")
                    wsClass.OfficeID = Reader1.Item("OfficeID")
                    wsClass.SupervisorUserID = Reader1.Item("SupervisorUserID")
                    'wsClass.AssignmentBeginDate = Reader1.Item("AssignmentBeginDate")
                    'wsClass.AssignmentEndDate = Reader1.Item("AssignmentEndDate")
                    wsClass.ClientUserId = Reader1.Item("ClientUserId")
                    wsClass.EmailAddress = Reader1.Item("EmailAddress")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    'wsClass.FaxAreaCode = Reader1.Item("FaxAreaCode")
                    'wsClass.FaxExchangeNumber = Reader1.Item("FaxExchangeNumber")
                    'wsClass.FaxExtensionNumber = Reader1.Item("FaxExtensionNumber")
                    'wsClass.FaxUnitNumber = Reader1.Item("FaxUnitNumber")
                    'wsClass.LastAssignmentDate = Reader1.Item("LastAssignmentDate")
                    wsClass.NameFirst = Reader1.Item("NameFirst")
                    wsClass.NameLast = Reader1.Item("NameLast")
                    wsClass.NameTitle = Reader1.Item("NameTitle")
                    'wsClass.OperatingFridayEndTime = Reader1.Item("OperatingFridayEndTime")
                    'wsClass.OperatingFridayStartTime = Reader1.Item("OperatingFridayStartTime")
                    'wsClass.OperatingMondayEndTime = Reader1.Item("OperatingMondayEndTime")
                    'wsClass.OperatingMondayStartTime = Reader1.Item("OperatingMondayStartTime")
                    'wsClass.OperatingSaturdayEndTime = Reader1.Item("OperatingSaturdayEndTime")
                    'wsClass.OperatingSaturdayStartTime = Reader1.Item("OperatingSaturdayStartTime")
                    'wsClass.OperatingSundayEndTime = Reader1.Item("OperatingSundayEndTime")
                    'wsClass.OperatingSundayStartTime = Reader1.Item("OperatingSundayStartTime")
                    'wsClass.OperatingThursdayEndTime = Reader1.Item("OperatingThursdayEndTime")
                    'wsClass.OperatingThursdayStartTime = Reader1.Item("OperatingThursdayStartTime")
                    'wsClass.OperatingThursdayStartTime = Reader1.Item("OperatingThursdayStartTime")
                    'wsClass.OperatingTuesdayEndTime = Reader1.Item("OperatingTuesdayEndTime")
                    'wsClass.OperatingTuesdayStartTime = Reader1.Item("OperatingTuesdayStartTime")
                    'wsClass.OperatingWednesdayEndTime = Reader1.Item("OperatingWednesdayEndTime")
                    'wsClass.OperatingWednesdayStartTime = Reader1.Item("OperatingWednesdayStartTime")
                    'wsClass.PhoneAreaCode = Reader1.Item("PhoneAreaCode")
                    'wsClass.PhoneExchangeNumber = Reader1.Item("PhoneExchangeNumber")
                    'wsClass.PhoneExtensionNumber = Reader1.Item("PhoneExtensionNumber")
                    'wsClass.PhoneUnitNumber = Reader1.Item("PhoneUnitNumber")
                    wsClass.SupervisorFlag = Reader1.Item("SupervisorFlag")
                    'wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")
                    'wsClass.CCCOneCommunicationAddress = Reader1.Item("CCCOneCommunicationAddress")
                    'wsClass.ReceiveCCCOneAssignmentFlag = Reader1.Item("ReceiveCCCOneAssignmentFlag")
                    wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_user_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_user_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetWorkflowSimple
    ' This APD function returns a simple list of workflows by InsuranceCompanyID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a simple list of workflows by InsuranceCompanyID")>
    Public Function GetWorkflowSimple(ByVal InsuranceCompanyID As Integer) As List(Of utb_workflow_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_workflow_simple
        Dim wsData As New List(Of utb_workflow_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetWorkflow"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_workflow_simple
                    wsClass.WorkflowID = Reader1.Item("WorkflowID")
                    wsClass.IgnoreDefaultFlag = Reader1.Item("IgnoreDefaultFlag")
                    wsClass.TaskName = Reader1.Item("TaskName")
                    wsClass.OriginatorTypeCD = Reader1.Item("OriginatorTypeCD")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_workflow_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_workflow_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetCustomFormsSimple
    ' This APD function returns a simple list of custom forms by InsuranceCompanyID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a simple list of custom forms by InsuranceCompanyID")>
    Public Function GetCustomFormsSimple(ByVal InsuranceCompanyID As Integer) As List(Of utb_custom_forms_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_custom_forms_simple
        Dim wsData As New List(Of utb_custom_forms_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetCustomForms"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_custom_forms_simple
                    wsClass.FormSupplementID = Reader1.Item("FormSupplementID")
                    wsClass.FormType = Reader1.Item("FormType")
                    wsClass.ShopStateCode = Reader1.Item("ShopStateCode")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.FormName = Reader1.Item("FormName")
                    wsClass.PDFPath = Reader1.Item("PDFPath")
                    wsClass.ServiceChannel = Reader1.Item("ServiceChannelCD")
                    wsClass.SQLProcedure = Reader1.Item("SQLProcedure")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_custom_forms_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_custom_forms_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetFaxTemplatesSimple
    ' This APD function returns a simple list of fax templates by InsuranceCompanyID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a simple list of fax templates by InsuranceCompanyID")>
    Public Function GetFaxTemplatesSimple(ByVal InsuranceCompanyID As Integer) As List(Of utb_fax_templates_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_fax_templates_simple
        Dim wsData As New List(Of utb_fax_templates_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetFaxTemplates"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_fax_templates_simple
                    wsClass.FormID = Reader1.Item("FormID")
                    wsClass.DocType = Reader1.Item("DocType")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.PDFPath = Reader1.Item("PDFPath")
                    wsClass.PertainsToCD = Reader1.Item("PertainsToCD")
                    wsClass.ServiceChannel = Reader1.Item("ServiceChannelCD")
                    wsClass.SQLProcedure = Reader1.Item("SQLProcedure")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_fax_templates_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_fax_templates_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminCreateAssignmentType
    ' This APD function creates a new APD Client Assignment Type
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client Assignment Type")>
    Public Function AdminModelAssignmentType(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspModelNewClientAssignmentType"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelServiceChannel
    ' This APD function creates a new APD Client Service Channel
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client Service Channel")>
    Public Function AdminModelServiceChannel(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspModelNewClientServiceChannel"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelContractState
    ' This APD function creates a new APD Client Contract State
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client Contract State")>
    Public Function AdminModelContractState(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspModelNewClientContractState"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelCoverageType
    ' This APD function creates a new APD Client Coverage Type
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client Coverage Type")>
    Public Function AdminModelCoverageType(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspModelNewClientCoverageType"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelAspectType
    ' This APD function creates a new APD Client Aspect Type
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client Aspect Type")>
    Public Function AdminModelAspectType(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspModelNewClientAspectType"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelReport
    ' This APD function creates a new APD Client Report
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client Report")>
    Public Function AdminModelReport(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspModelNewClientReport"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelOfficeAssignment
    ' This APD function creates a new APD Client Office Assignment
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client Office Assignment")>
    Public Function AdminModelOfficeAssignment(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspModelNewClientOfficeAssignment"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelOfficeContractState
    ' This APD function creates a new APD Client Office Contract 
    ' State
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Client Office Contract State")>
    Public Function AdminModelOfficeContractState(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal UseCEIShop As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspCreateNewClientOfficeContractState"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iUseCEIShop", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iUseCEIShop").Value = UseCEIShop

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelWorkflow
    ' This APD function creates a new APD Workflow 
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Workflow")>
    Public Function AdminModelWorkflow(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspCreateNewWorkflow"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelSpawn
    ' This APD function creates a new APD Spawn 
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Spawn")>
    Public Function AdminModelSpawn(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspCreateNewSpawn"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '<WebMethod(Description:="This APD function creates a new APD Bundling")> _
    'Public Function Test( _
    '    ByVal NewInsuranceCompanyID As Integer _
    '    , ByVal ModelInsuranceCompanyID As Integer _
    '    , ByVal CreatedByUserID As Integer
    '    ) As Integer

    '    Call CreateBundleFiles(NewInsuranceCompanyID, ModelInsuranceCompanyID)

    'End Function

    '-----------------------------------------------------------
    ' APD: AdminModelBundling
    ' This APD function creates a new APD Bundling
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Bundling")>
    Public Function AdminModelBundling(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal NewCompanyNameShort As String _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspCreateModelBundling"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@vCompanyNameShort", SqlDbType.VarChar, 6)
            cmdInsert.Parameters("@vCompanyNameShort").Value = NewCompanyNameShort

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
                'Call CreateBundleFiles(NewInsuranceCompanyID, ModelInsuranceCompanyID, NewCompanyNameShort)
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelFaxTemplate
    ' This APD function creates a new APD Fax Template
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Fax Template")>
    Public Function AdminModelFaxTemplate(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspCreateModelFaxTemplate"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelFormSupplement
    ' This APD function creates a new APD Form Supplement
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Form Supplement")>
    Public Function AdminModelFormSupplement(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspCreateModelFormSupplement"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

            cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

            cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminModelCreateBundleFiles
    ' This APD function creates a new APD bundle files
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD bundle files")>
    Public Function AdminModelCreateBundleFiles(
        ByVal NewInsuranceCompanyID As Integer _
        , ByVal ModelInsuranceCompanyID As Integer _
        , ByVal NewCompanyNameShort As String _
        , ByVal CreatedByUserID As Integer
        ) As Integer

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspGetBundleDetailsByBundleID"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@iBundlingID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iBundlingID").Value = ModelInsuranceCompanyID

            'cmdInsert.Parameters.Add("@iDocumentTypeID", SqlDbType.Int, 4)
            'cmdInsert.Parameters("@iDocumentTypeID").Value = DocumentTypeID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                iRC = CInt(drRowsAffected.Item("RetCode"))
            Else
                iRC = 0
            End If

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 0
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function

    '-------------------------------------------------------------------
    ' APD: XslGetSession
    ' This APD function gets a specific veriable value from the session
    ' control database by SessionKey.
    '-------------------------------------------------------------------
    <WebMethod(Description:="This APD function gets a specific veriable value from the session control database by SessionKey.")>
    Public Function XslGetSession(ByVal SessionKey As String, ByVal SessionVariable As String) As String
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader = Nothing
        Dim sValue As String = ""

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspSessionDataGet"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@SessionID", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@SessionID").Value = SessionKey

            If SessionVariable <> "" Then
                cmdSQL.Parameters.Add("@SessionVariable", SqlDbType.VarChar, 7500)
                cmdSQL.Parameters("@SessionVariable").Value = SessionVariable
            End If

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSQL.ExecuteReader
            If Reader1.HasRows Then
                Reader1.Read()
                sValue = Reader1.Item("SessionValue")
            Else
                sValue = ""
            End If

            Return sValue
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return ""
        Finally
            cmdSQL = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function


    ''-----------------------------------------------------------
    '' APD: AdminModelCreateBundleFiles
    '' This APD function creates new bundle files modeled after
    '' existing ones.
    ''-----------------------------------------------------------
    '<WebMethod(Description:="This APD function creates new bundle files modeled after existing ones.")> _
    'Public Sub AdminModelCreateBundleFiles(ByVal NewInsuranceCompanyID As Integer, ByVal ModelInsuranceCompanyID As Integer, ByVal ModelCompanyNameShort As String, ByVal NewCompanyNameShort As String)
    '    'Dim dsModelBundleData As New DataSet
    '    Dim sModelFileName As String = ""

    '    Dim dr As DataRow
    '    Dim dsModelBundleData As DataSet
    '    Dim dt As DataTable
    '    Dim aModelFileName As Array
    '    Dim sModelPathFile As String = ""
    '    Dim sNewPathFile As String = ""

    '    Try
    '        '---------------------------------------
    '        ' Get model and new file names
    '        '---------------------------------------
    '        dsModelBundleData = GetExistingClientBundlesByInscCompIDAsRS(ModelInsuranceCompanyID)
    '        dt = dsModelBundleData.Tables(0)

    '        For Each dr In dt.Rows
    '            '---------------------------------------
    '            ' Check to see if file already exists
    '            '---------------------------------------
    '            aModelFileName = Split(dr("Description"), "|")
    '            sModelFileName = aModelFileName(1)

    '            sModelPathFile = AppSettings("MsgFilePath") & sModelFileName
    '            sNewPathFile = AppSettings("MsgFilePath") & sModelFileName.Replace("msg" & ModelCompanyNameShort, "msg" & NewCompanyNameShort)

    '            If File.Exists(sModelPathFile) = True Then
    '                File.Copy(sModelPathFile, sNewPathFile)
    '            End If
    '        Next

    '        dsModelBundleData.Dispose()


    '        'Dim Conn As New SqlConnection
    '        'Dim cmdSelect As New SqlClient.SqlCommand
    '        'Dim drRowsAffected As SqlClient.SqlDataReader
    '        'Dim iRC As Integer = 0

    '        'Try
    '        '    '-------------------------------------------
    '        '    ' Open connection to DB and run stored Proc
    '        '    '-------------------------------------------
    '        '    cmdSelect.CommandType = CommandType.StoredProcedure
    '        '    cmdSelect.CommandText = "uspGetBundlingCreateModelFormSupplement"
    '        '    cmdSelect.Connection = Conn

    '        '    Conn.ConnectionString = AppSettings("ConnectString")
    '        '    Conn.Open()

    '        '    '-------------------------------------------
    '        '    ' Input Parameters
    '        '    '-------------------------------------------
    '        '    cmdSelect.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
    '        '    cmdSelect.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

    '        '    cmdSelect.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
    '        '    cmdSelect.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

    '        '    cmdSelect.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
    '        '    cmdSelect.Parameters("@iCreatedByUserID").Value = CreatedByUserID

    '        '    '-------------------------------------------
    '        '    ' Execute the SQL
    '        '    '-------------------------------------------
    '        '    drRowsAffected = cmdSelect.ExecuteReader()

    '        '    If drRowsAffected.HasRows Then
    '        '        drRowsAffected.Read()
    '        '        iRC = CInt(drRowsAffected.Item("RetCode"))
    '        '    Else
    '        '        iRC = 0
    '        '    End If

    '    Catch oExcept As Exception
    '        '------------------------------
    '        ' Email Notify of the Error
    '        '------------------------------
    '        'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
    '    Finally
    '        dsModelBundleData.Dispose()
    '    End Try
    'End Sub

    '-----------------------------------------------------------
    ' APD: AdminModelMessageTemplate
    ' This APD function creates a new APD Message Template 
    '-----------------------------------------------------------
    '<WebMethod(Description:="This APD function creates a new APD Message Template")> _
    'Public Function AdminModelMessageTemplate( _
    '    ByVal NewInsuranceCompanyID As Integer _
    '    , ByVal ModelInsuranceCompanyID As Integer _
    '    , ByVal NewCompanyNameShort As String _
    '    , ByVal CreatedByUserID As Integer
    '    ) As Integer

    '    Dim Conn As New SqlConnection
    '    Dim cmdInsert As New SqlClient.SqlCommand
    '    Dim drRowsAffected As SqlClient.SqlDataReader
    '    Dim iRC As Integer = 0

    '    Try
    '        '-------------------------------------------
    '        ' Open connection to DB and run stored Proc
    '        '-------------------------------------------
    '        cmdInsert.CommandType = CommandType.StoredProcedure
    '        cmdInsert.CommandText = "uspCreateNewMessageTemplate"
    '        cmdInsert.Connection = Conn

    '        Conn.ConnectionString = AppSettings("ConnectString")
    '        Conn.Open()

    '        '-------------------------------------------
    '        ' Input Parameters
    '        '-------------------------------------------
    '        cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

    '        cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

    '        cmdInsert.Parameters.Add("@vCompanyNameShort", SqlDbType.VarChar, 6)
    '        cmdInsert.Parameters("@vCompanyNameShort").Value = NewCompanyNameShort

    '        cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

    '        '-------------------------------------------
    '        ' Execute the SQL
    '        '-------------------------------------------
    '        drRowsAffected = cmdInsert.ExecuteReader()

    '        If drRowsAffected.HasRows Then
    '            drRowsAffected.Read()
    '            iRC = CInt(drRowsAffected.Item("RetCode"))
    '        Else
    '            iRC = 0
    '        End If

    '        Return iRC
    '    Catch oExcept As Exception
    '        '------------------------------
    '        ' Email Notify of the Error
    '        '------------------------------
    '        'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
    '        Return 0
    '    Finally
    '        cmdInsert = Nothing
    '        Conn.Close()
    '    End Try
    'End Function

    '-----------------------------------------------------------
    ' APD: AdminModelBundling
    ' This APD function creates a new APD Bundling 
    '-----------------------------------------------------------
    '<WebMethod(Description:="This APD function creates a new APD Bundling")> _
    'Public Function AdminModelBundling( _
    '    ByVal NewInsuranceCompanyID As Integer _
    '    , ByVal ModelInsuranceCompanyID As Integer _
    '    , ByVal NewCompanyNameShort As String _
    '    , ByVal CreatedByUserID As Integer
    '    ) As Integer

    '    Dim Conn As New SqlConnection
    '    Dim cmdInsert As New SqlClient.SqlCommand
    '    Dim drRowsAffected As SqlClient.SqlDataReader
    '    Dim iRC As Integer = 0

    '    Try
    '        '-------------------------------------------
    '        ' Open connection to DB and run stored Proc
    '        '-------------------------------------------
    '        cmdInsert.CommandType = CommandType.StoredProcedure
    '        cmdInsert.CommandText = "uspCreateNewBundling"
    '        cmdInsert.Connection = Conn

    '        Conn.ConnectionString = AppSettings("ConnectString")
    '        Conn.Open()

    '        '-------------------------------------------
    '        ' Input Parameters
    '        '-------------------------------------------
    '        cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

    '        cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

    '        cmdInsert.Parameters.Add("@vCompanyNameShort", SqlDbType.VarChar, 6)
    '        cmdInsert.Parameters("@vCompanyNameShort").Value = NewCompanyNameShort

    '        cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

    '        '-------------------------------------------
    '        ' Execute the SQL
    '        '-------------------------------------------
    '        drRowsAffected = cmdInsert.ExecuteReader()

    '        If drRowsAffected.HasRows Then
    '            drRowsAffected.Read()
    '            iRC = CInt(drRowsAffected.Item("RetCode"))
    '        Else
    '            iRC = 0
    '        End If

    '        Return iRC
    '    Catch oExcept As Exception
    '        '------------------------------
    '        ' Email Notify of the Error
    '        '------------------------------
    '        'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
    '        Return 0
    '    Finally
    '        cmdInsert = Nothing
    '        Conn.Close()
    '    End Try
    'End Function

    '-----------------------------------------------------------
    ' APD: AdminModelClientBundling
    ' This APD function creates a new APD Client Bundling 
    '-----------------------------------------------------------
    '<WebMethod(Description:="This APD function creates a new APD Client Bundling")> _
    'Public Function AdminModelClientBundling( _
    '    ByVal NewInsuranceCompanyID As Integer _
    '    , ByVal ModelInsuranceCompanyID As Integer _
    '    , ByVal NewCompanyNameShort As String _
    '    , ByVal CreatedByUserID As Integer
    '    ) As Integer

    '    Dim Conn As New SqlConnection
    '    Dim cmdInsert As New SqlClient.SqlCommand
    '    Dim drRowsAffected As SqlClient.SqlDataReader
    '    Dim iRC As Integer = 0

    '    Try
    '        '-------------------------------------------
    '        ' Open connection to DB and run stored Proc
    '        '-------------------------------------------
    '        cmdInsert.CommandType = CommandType.StoredProcedure
    '        cmdInsert.CommandText = "uspCreateNewClientBundling"
    '        cmdInsert.Connection = Conn

    '        Conn.ConnectionString = AppSettings("ConnectString")
    '        Conn.Open()

    '        '-------------------------------------------
    '        ' Input Parameters
    '        '-------------------------------------------
    '        cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

    '        cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

    '        cmdInsert.Parameters.Add("@vCompanyNameShort", SqlDbType.VarChar, 6)
    '        cmdInsert.Parameters("@vCompanyNameShort").Value = NewCompanyNameShort

    '        cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

    '        '-------------------------------------------
    '        ' Execute the SQL
    '        '-------------------------------------------
    '        drRowsAffected = cmdInsert.ExecuteReader()

    '        If drRowsAffected.HasRows Then
    '            drRowsAffected.Read()
    '            iRC = CInt(drRowsAffected.Item("RetCode"))
    '        Else
    '            iRC = 0
    '        End If

    '        Return iRC
    '    Catch oExcept As Exception
    '        '------------------------------
    '        ' Email Notify of the Error
    '        '------------------------------
    '        'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
    '        Return 0
    '    Finally
    '        cmdInsert = Nothing
    '        Conn.Close()
    '    End Try
    'End Function

    '-----------------------------------------------------------
    ' APD: AdminModelBundlingDocumentTypes
    ' This APD function creates a new APD Bundling Document Types
    '-----------------------------------------------------------
    '<WebMethod(Description:="This APD function creates a new APD Bundling Document Types")> _
    'Public Function AdminModelBundlingDocumentTypes( _
    '    ByVal NewInsuranceCompanyID As Integer _
    '    , ByVal ModelInsuranceCompanyID As Integer _
    '    , ByVal NewCompanyNameShort As String _
    '    , ByVal CreatedByUserID As Integer
    '    ) As Integer

    '    Dim Conn As New SqlConnection
    '    Dim cmdInsert As New SqlClient.SqlCommand
    '    Dim drRowsAffected As SqlClient.SqlDataReader
    '    Dim iRC As Integer = 0

    '    Try
    '        '-------------------------------------------
    '        ' Open connection to DB and run stored Proc
    '        '-------------------------------------------
    '        cmdInsert.CommandType = CommandType.StoredProcedure
    '        cmdInsert.CommandText = "uspCreateNewBundlingDocumentType"
    '        cmdInsert.Connection = Conn

    '        Conn.ConnectionString = AppSettings("ConnectString")
    '        Conn.Open()

    '        '-------------------------------------------
    '        ' Input Parameters
    '        '-------------------------------------------
    '        cmdInsert.Parameters.Add("@iNewInsuranceCompanyID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iNewInsuranceCompanyID").Value = NewInsuranceCompanyID

    '        cmdInsert.Parameters.Add("@iModelInsuranceCompanyID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iModelInsuranceCompanyID").Value = ModelInsuranceCompanyID

    '        cmdInsert.Parameters.Add("@vCompanyNameShort", SqlDbType.VarChar, 6)
    '        cmdInsert.Parameters("@vCompanyNameShort").Value = NewCompanyNameShort

    '        cmdInsert.Parameters.Add("@iCreatedByUserID", SqlDbType.Int, 4)
    '        cmdInsert.Parameters("@iCreatedByUserID").Value = CreatedByUserID

    '        '-------------------------------------------
    '        ' Execute the SQL
    '        '-------------------------------------------
    '        drRowsAffected = cmdInsert.ExecuteReader()

    '        If drRowsAffected.HasRows Then
    '            drRowsAffected.Read()
    '            iRC = CInt(drRowsAffected.Item("RetCode"))
    '        Else
    '            iRC = 0
    '        End If

    '        Return iRC
    '    Catch oExcept As Exception
    '        '------------------------------
    '        ' Email Notify of the Error
    '        '------------------------------
    '        'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
    '        Return 0
    '    Finally
    '        cmdInsert = Nothing
    '        Conn.Close()
    '    End Try
    'End Function

    '-----------------------------------------------------------
    ' APD: GetLYNXSelectShops
    ' This APD function returns a list of LYNX Select Shops
    ' with in a range of your zip code.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a list of LYNX Select Shops with in a range of your zip code.")>
    Public Function GetLYNXSelectShopsByZipcode(
        ByVal ZipCode As String _
        , ByVal InsuranceCompanyID As Integer _
        , ByVal MaxShops As Integer
        ) As List(Of utb_shop_location)

        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim iRC As Integer = 0
        Dim sShopTypeCode As String
        Dim wsClass As New utb_shop_location
        Dim wsData As New List(Of utb_shop_location)

        Try
            sShopTypeCode = AppSettings("ShopTypeCode")

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspShopSearchByDistanceXML"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@Zip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@Zip").Value = ZipCode

            cmdSelect.Parameters.Add("@InsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@InsuranceCompanyID").Value = InsuranceCompanyID

            cmdSelect.Parameters.Add("@ShopTypeCode", SqlDbType.VarChar, 1)
            cmdSelect.Parameters("@ShopTypeCode").Value = sShopTypeCode

            cmdSelect.Parameters.Add("@MaxShops", SqlDbType.Int, 4)
            cmdSelect.Parameters("@MaxShops").Value = MaxShops

            cmdSelect.Parameters.Add("@UserID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@UserID").Value = 0

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader()

            If Reader1.HasRows Then
                Do While Reader1.Read
                    If Not IsDBNull(Reader1.Item("Shop!2!Name")) Then
                        wsClass = New utb_shop_location
                        wsClass.ShopLocationID = Reader1.Item("Shop!2!ShopLocationID")
                        wsClass.ShopName = Reader1.Item("Shop!2!Name")
                        wsClass.Address1 = Reader1.Item("Shop!2!Address1")
                        wsClass.Address2 = Reader1.Item("Shop!2!Address2")
                        wsClass.AddressCity = Reader1.Item("Shop!2!AddressCity")
                        wsClass.AddressState = Reader1.Item("Shop!2!AddressState")
                        wsClass.AddressZip = Reader1.Item("Shop!2!AddressZip")
                        wsClass.PhoneNumber = Reader1.Item("Shop!2!PhoneAreaCode") & "-" & Reader1.Item("Shop!2!PhoneExchangeNumber") & "-" & Reader1.Item("Shop!2!PhoneUnitNumber")
                        wsClass.FaxNumber = Reader1.Item("Shop!2!FaxAreaCode") & "-" & Reader1.Item("Shop!2!FaxExchangeNumber") & "-" & Reader1.Item("Shop!2!FaxUnitNumber")
                        wsClass.ShopOpen = Reader1.Item("Shop!2!ShopOpen")
                        wsClass.TimeZone = Reader1.Item("Shop!2!TimeZone")
                        'wsClass.ContactName = Reader1.Item("ShopContact!3!Name")
                        'wsClass.ContactPhoneNumber = Reader1.Item("ShopContact!3!PhoneAreaCode") & "-" & Reader1.Item("ShopContact!3!PhoneExchangeNumber") & "-" & Reader1.Item("ShopContact!3!PhoneUnitNumber")
                        wsClass.OperatingMondayStartTime = Reader1.Item("Shop!2!OperatingMondayStartTime")
                        wsClass.OperatingMondayEndTime = Reader1.Item("Shop!2!OperatingMondayEndTime")
                        wsClass.OperatingTuesdayStartTime = Reader1.Item("Shop!2!OperatingTuesdayStartTime")
                        wsClass.OperatingTuesdayEndTime = Reader1.Item("Shop!2!OperatingTuesdayEndTime")
                        wsClass.OperatingWednesdayStartTime = Reader1.Item("Shop!2!OperatingWednesdayStartTime")
                        wsClass.OperatingWednesdayEndTime = Reader1.Item("Shop!2!OperatingWednesdayEndTime")
                        wsClass.OperatingThursdayStartTime = Reader1.Item("Shop!2!OperatingThursdayStartTime")
                        wsClass.OperatingThursdayEndTime = Reader1.Item("Shop!2!OperatingThursdayEndTime")
                        wsClass.OperatingFridayStartTime = Reader1.Item("Shop!2!OperatingFridayStartTime")
                        wsClass.OperatingFridayEndTime = Reader1.Item("Shop!2!OperatingFridayEndTime")
                        wsClass.OperatingSaturdayStartTime = Reader1.Item("Shop!2!OperatingSaturdayStartTime")
                        wsClass.OperatingSaturdayEndTime = Reader1.Item("Shop!2!OperatingSaturdayEndTime")
                        wsClass.OperatingSundayStartTime = Reader1.Item("Shop!2!OperatingSundayStartTime")
                        wsClass.OperatingSundayEndTime = Reader1.Item("Shop!2!OperatingSundayEndTime")

                        wsClass.DrivingDirections = Reader1.Item("Shop!2!DrivingDirections")

                        wsClass.Errors = ""

                        wsData.Add(wsClass)
                    End If
                Loop
            Else
                wsClass = New utb_shop_location
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "APD Web Service: APDService")
            sBody = String.Format("Error: {0}", "APD Web Service: Processing failed (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            'oFoundation.SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))

            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_shop_location
            wsClass.Errors = sBody & " - " & oExcept.ToString

            'wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetLYNXSelectShops
    ' This APD function returns a list of LYNX Select Shops
    ' with in a range based on Name or City State
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a list of LYNX Select Shops with in a range on Name or City State.")>
    Public Function GetLYNXSelectShopsByNameOrCity(
        ByVal ShopName As String _
        , ByVal ShopCity As String _
        , ByVal ShopState As String _
        , ByVal InsuranceCompanyID As Integer _
        , ByVal MaxShops As Integer
        ) As List(Of utb_shop_location)

        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim iRC As Integer = 0
        Dim sShopTypeCode As String
        Dim wsClass As New utb_shop_location
        Dim wsData As New List(Of utb_shop_location)

        Try
            sShopTypeCode = AppSettings("ShopTypeCode")

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspShopSearchByNameXML"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@ShopName", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@ShopName").Value = ShopName

            cmdSelect.Parameters.Add("@ShopCity", SqlDbType.VarChar, 30)
            cmdSelect.Parameters("@ShopCity").Value = ShopCity

            cmdSelect.Parameters.Add("@ShopState", SqlDbType.VarChar, 2)
            cmdSelect.Parameters("@ShopState").Value = ShopState

            cmdSelect.Parameters.Add("@InsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@InsuranceCompanyID").Value = InsuranceCompanyID

            cmdSelect.Parameters.Add("@ShopTypeCode", SqlDbType.VarChar, 1)
            cmdSelect.Parameters("@ShopTypeCode").Value = sShopTypeCode

            cmdSelect.Parameters.Add("@MaxShops", SqlDbType.Int, 4)
            cmdSelect.Parameters("@MaxShops").Value = MaxShops

            cmdSelect.Parameters.Add("@UserID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@UserID").Value = 0

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader()

            If Reader1.HasRows Then
                Do While Reader1.Read
                    If Not IsDBNull(Reader1.Item("Shop!2!Name")) Then
                        wsClass = New utb_shop_location
                        wsClass.ShopLocationID = Reader1.Item("Shop!2!ShopLocationID")
                        wsClass.ShopName = Reader1.Item("Shop!2!Name")
                        wsClass.Address1 = Reader1.Item("Shop!2!Address1")
                        wsClass.Address2 = Reader1.Item("Shop!2!Address2")
                        wsClass.AddressCity = Reader1.Item("Shop!2!AddressCity")
                        wsClass.AddressState = Reader1.Item("Shop!2!AddressState")
                        wsClass.AddressZip = Reader1.Item("Shop!2!AddressZip")
                        wsClass.PhoneNumber = Reader1.Item("Shop!2!PhoneAreaCode") & "-" & Reader1.Item("Shop!2!PhoneExchangeNumber") & "-" & Reader1.Item("Shop!2!PhoneUnitNumber")
                        wsClass.FaxNumber = Reader1.Item("Shop!2!FaxAreaCode") & "-" & Reader1.Item("Shop!2!FaxExchangeNumber") & "-" & Reader1.Item("Shop!2!FaxUnitNumber")
                        wsClass.ShopOpen = Reader1.Item("Shop!2!ShopOpen")
                        wsClass.TimeZone = Reader1.Item("Shop!2!TimeZone")
                        'wsClass.ContactName = Reader1.Item("ShopContact!3!Name")
                        'wsClass.ContactPhoneNumber = Reader1.Item("ShopContact!3!PhoneAreaCode") & "-" & Reader1.Item("ShopContact!3!PhoneExchangeNumber") & "-" & Reader1.Item("ShopContact!3!PhoneUnitNumber")
                        wsClass.OperatingMondayStartTime = Reader1.Item("Shop!2!OperatingMondayStartTime")
                        wsClass.OperatingMondayEndTime = Reader1.Item("Shop!2!OperatingMondayEndTime")
                        wsClass.OperatingTuesdayStartTime = Reader1.Item("Shop!2!OperatingTuesdayStartTime")
                        wsClass.OperatingTuesdayEndTime = Reader1.Item("Shop!2!OperatingTuesdayEndTime")
                        wsClass.OperatingWednesdayStartTime = Reader1.Item("Shop!2!OperatingWednesdayStartTime")
                        wsClass.OperatingWednesdayEndTime = Reader1.Item("Shop!2!OperatingWednesdayEndTime")
                        wsClass.OperatingThursdayStartTime = Reader1.Item("Shop!2!OperatingThursdayStartTime")
                        wsClass.OperatingThursdayEndTime = Reader1.Item("Shop!2!OperatingThursdayEndTime")
                        wsClass.OperatingFridayStartTime = Reader1.Item("Shop!2!OperatingFridayStartTime")
                        wsClass.OperatingFridayEndTime = Reader1.Item("Shop!2!OperatingFridayEndTime")
                        wsClass.OperatingSaturdayStartTime = Reader1.Item("Shop!2!OperatingSaturdayStartTime")
                        wsClass.OperatingSaturdayEndTime = Reader1.Item("Shop!2!OperatingSaturdayEndTime")
                        wsClass.OperatingSundayStartTime = Reader1.Item("Shop!2!OperatingSundayStartTime")
                        wsClass.OperatingSundayEndTime = Reader1.Item("Shop!2!OperatingSundayEndTime")

                        wsClass.DrivingDirections = Reader1.Item("Shop!2!DrivingDirections")

                        wsClass.Errors = ""

                        wsData.Add(wsClass)
                    End If
                Loop
            Else
                wsClass = New utb_shop_location
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "APD Web Service: APDService")
            sBody = String.Format("Error: {0}", "APD Web Service: Processing failed (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            'oFoundation.SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))

            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_shop_location
            wsClass.Errors = sBody & " - " & oExcept.ToString

            'wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClaimRepByID
    ' This APD function returns a list of claim rep details by Userid.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a list of claim rep details by Userid.")>
    Public Function GetUserInfoByUserID(ByVal UserID As Integer) As List(Of utb_user_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_user_simple
        Dim wsData As New List(Of utb_user_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetUserInfoByUserID"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@iUserID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iUserID").Value = UserID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_user_simple
                    wsClass.UserID = Reader1.Item("UserID")
                    wsClass.OfficeID = Reader1.Item("OfficeID")
                    wsClass.SupervisorUserID = Reader1.Item("SupervisorUserID")
                    wsClass.ClientUserId = Reader1.Item("ClientUserId")
                    wsClass.EmailAddress = Reader1.Item("EmailAddress")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.NameFirst = Reader1.Item("NameFirst")
                    wsClass.NameLast = Reader1.Item("NameLast")
                    wsClass.NameTitle = Reader1.Item("NameTitle")
                    wsClass.SupervisorFlag = Reader1.Item("SupervisorFlag")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")
                    wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_user_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_user_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClaimRepByEmail
    ' This APD function returns a list of claim rep details by Email address.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a list of claim rep details by Email address.")>
    Public Function GetUserInfoByEmail(ByVal EmailAddress As String) As List(Of utb_user_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_user_simple
        Dim wsData As New List(Of utb_user_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetUserInfoByEmail"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@vEmailAddress", SqlDbType.VarChar, 50)
            cmdSelect.Parameters("@vEmailAddress").Value = EmailAddress

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_user_simple
                    wsClass.UserID = Reader1.Item("UserID")
                    wsClass.OfficeID = Reader1.Item("OfficeID")
                    wsClass.SupervisorUserID = Reader1.Item("SupervisorUserID")
                    wsClass.ClientUserId = Reader1.Item("ClientUserId")
                    wsClass.EmailAddress = Reader1.Item("EmailAddress")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.NameFirst = Reader1.Item("NameFirst")
                    wsClass.NameLast = Reader1.Item("NameLast")
                    wsClass.NameTitle = Reader1.Item("NameTitle")
                    wsClass.SupervisorFlag = Reader1.Item("SupervisorFlag")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")
                    wsClass.PhoneCombined = Reader1.Item("PhoneAreaCode") & "-" & Reader1.Item("PhoneExchangeNumber") & "-" & Reader1.Item("PhoneUnitNumber")
                    wsClass.FaxCombined = Reader1.Item("FaxAreaCode") & "-" & Reader1.Item("FaxExchangeNumber") & "-" & Reader1.Item("FaxUnitNumber")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_user_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_user_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: AdminCreateClaimRep
    ' This APD function creates a new APD Claim Rep
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates a new APD Claim Rep")>
    Public Function AdminCreateClaimRep(
        ByVal EmailAddress As String _
        , ByVal NameFirst As String _
        , ByVal NameLast As String _
        , ByVal PhoneAreaCode As String _
        , ByVal PhoneExchangeNumber As String _
        , ByVal PhoneExtensionNumber As String _
        , ByVal PhoneUnitNumber As String _
        , ByVal InsuranceCompanyID As Integer
        ) As String

        Dim Conn As New SqlConnection
        Dim cmdInsert As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlClient.SqlDataReader
        Dim sRetCode As String = ""
        Dim wsClass As New utb_user_simple
        Dim wsData As New List(Of utb_user_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdInsert.CommandType = CommandType.StoredProcedure
            cmdInsert.CommandText = "uspCreateNewClaimRep"
            cmdInsert.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Validate Parameters
            '-------------------------------------------
            If (EmailAddress = "") Or (NameFirst = "") Or (PhoneAreaCode = "") Or (PhoneExchangeNumber = "") Or (PhoneUnitNumber = "") Then
                Throw New SystemException("XMLValidationError: Tags: CarrierRepEmailAddress, CarrierRepNameFirst, CarrierRepNameLast, CarrierRepPhoneAreaCode, CarrierRepPhoneExchangeNumber and CarrierRepPhoneUnitNumber are required fields to create a new Carrier Rep.")
            End If

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdInsert.Parameters.Add("@vEmailAddress", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vEmailAddress").Value = EmailAddress

            cmdInsert.Parameters.Add("@vNameFirst", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vNameFirst").Value = NameFirst

            cmdInsert.Parameters.Add("@vNameLast", SqlDbType.VarChar, 50)
            cmdInsert.Parameters("@vNameLast").Value = NameLast

            cmdInsert.Parameters.Add("@vPhoneAreaCode", SqlDbType.VarChar, 3)
            cmdInsert.Parameters("@vPhoneAreaCode").Value = PhoneAreaCode

            cmdInsert.Parameters.Add("@vPhoneExchangeNumber", SqlDbType.VarChar, 3)
            cmdInsert.Parameters("@vPhoneExchangeNumber").Value = PhoneExchangeNumber

            cmdInsert.Parameters.Add("@vPhoneExtensionNumber", SqlDbType.VarChar, 5)
            cmdInsert.Parameters("@vPhoneExtensionNumber").Value = PhoneExtensionNumber

            cmdInsert.Parameters.Add("@vPhoneUnitNumber", SqlDbType.VarChar, 4)
            cmdInsert.Parameters("@vPhoneUnitNumber").Value = PhoneUnitNumber

            cmdInsert.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdInsert.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdInsert.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                sRetCode = drRowsAffected.Item("RetCode")
                'wsClass = New utb_user_simple
                'wsClass.UserID = drRowsAffected.Item("RetCode")
                'wsClass.OfficeID = 0
                'wsClass.SupervisorUserID = 0
                'wsClass.ClientUserId = ""
                'wsClass.EmailAddress = ""
                'wsClass.EnabledFlag = True
                'wsClass.NameFirst = ""
                'wsClass.NameLast = ""
                'wsClass.NameTitle = ""
                'wsClass.SupervisorFlag = False
                'wsClass.SysLastUpdatedDate = Date.Now
                'wsClass.PhoneCombined = ""
                'wsClass.FaxCombined = ""
                'wsClass.Errors = ""

                'wsData.Add(wsClass)
            Else
                Throw New SystemException("Failed to create new carrier rep.")
            End If

            'Return wsData
            Return sRetCode
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            'sError = String.Format("Error: {0}", "APD Web Service: APDService")
            sBody = String.Format("Error: {0}", "APD Web Service: Processing failed (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.Message

            'oFoundation.SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))

            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            'If oExcept.Message.Contains("|") Then
            '    Dim aError As Array
            '    aError = oExcept.Message.Split("|")
            '    Return aError(3).ToString
            'Else
            '    Return oExcept.Message
            'End If

            'Return sBody

            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'wsClass = New utb_user_simple
            'wsClass.Errors = sBody

            'wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            'Return wsData
            Return sBody
        Finally
            cmdInsert = Nothing
            Conn.Close()
        End Try
    End Function
    '-----------------------------------------------------------
    ' APD: GetCoverageTypeID
    ' This APD function returns a specific Coverage Type by 
    ' InsuranceCompanyID and CoverageType.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a specific Coverage Type by InsuranceCompanyID and CoverageType.")>
    Public Function GetCoverageTypeID(ByVal InsuranceCompanyID As Integer, ByVal CoverageType As String) As Integer
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim iClientCoverageTypeID As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetCoverageTypeID"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            cmdSelect.Parameters.Add("@vCoverageProfileCD", SqlDbType.VarChar, 6)
            cmdSelect.Parameters("@vCoverageProfileCD").Value = CoverageType

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    iClientCoverageTypeID = Reader1.Item("ClientCoverageTypeID")
                Loop
            End If

            Return iClientCoverageTypeID
        Catch oExcept As Exception
            Return 0
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClaimAspectID
    ' This APD function returns the claim aspects by LynxID 
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns the claim aspects by LynxID.")>
    Public Function GetClaimAspectID(ByVal LynxID As Integer, ByVal ClaimAspectTypeID As Integer) As Integer()
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim iClaimAspectID() As Integer
        Dim iCnt As Integer = 0

        iClaimAspectID(0) = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetClaimAspectID"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@iLynxID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iLynxID").Value = LynxID

            cmdSelect.Parameters.Add("@iClaimAspectTypeID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iClaimAspectTypeID").Value = ClaimAspectTypeID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    iClaimAspectID(iCnt) = Reader1.Item("ClaimAspectID")
                Loop
            End If

            Return iClaimAspectID
        Catch oExcept As Exception
            iClaimAspectID(0) = 0
            Return iClaimAspectID
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetLogEvents
    ' This APD function read data from the utb_apd_event_log
    ' table for debug purposes
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function read data from the utb_apd_event_log.")>
    Public Function GetLogEvents() As List(Of utb_apd_event_log)
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim iRecID As Integer = 0
        Dim wsClass As New utb_apd_event_log
        Dim wsData As New List(Of utb_apd_event_log)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspGetEventLog"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSQL.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_apd_event_log
                    wsClass.EventID = Reader1.Item("EventID")
                    wsClass.EventType = Reader1.Item("EventType")
                    wsClass.EventStatus = Reader1.Item("EventStatus")
                    wsClass.EventDescription = Reader1.Item("EventDescription")
                    wsClass.EventXML = Reader1.Item("EventXML")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")

                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_apd_event_log
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData

        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_apd_event_log
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSQL = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClaimRepByLynxID
    ' This APD function returns the claim rep ID assigned to
    ' the claim by LynxID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns the claim rep ID assigned to the claim by LynxID.")>
    Public Function GetClaimRepByLynxID(ByVal LynxID As Integer) As List(Of utb_user_simple)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_user_simple
        Dim wsData As New List(Of utb_user_simple)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetClaimRepIDByLynxID"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            ''-------------------------------------------
            '' Input Parameters
            ''-------------------------------------------
            cmdSelect.Parameters.Add("@LynxID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@LynxID").Value = LynxID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_user_simple
                    wsClass.UserID = Reader1.Item("CarrierRepUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")
                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_user_simple
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_user_simple
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetClaimAspectByLynxID
    ' This APD function returns all the exposures for a 
    ' claim by LynxID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns all the exposures for a claim by LynxID.")>
    Public Function GetClaimAspectByLynxID(ByVal LynxID As Integer) As List(Of utb_claim_aspect)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_claim_aspect
        Dim wsData As New List(Of utb_claim_aspect)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetExposuresByLynxID"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@LynxID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@LynxID").Value = LynxID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_claim_aspect
                    wsClass.ClaimAspectID = Reader1.Item("ClaimAspectID")
                    wsClass.AnalystUserID = Reader1.Item("AnalystUserID")
                    wsClass.ClaimAspectTypeID = Reader1.Item("ClaimAspectTypeID")
                    wsClass.ClientCoverageTypeID = Reader1.Item("ClientCoverageTypeID")
                    wsClass.CompletedAnalystUserID = Reader1.Item("CompletedAnalystUserID")
                    wsClass.CompletedOwnerUserID = Reader1.Item("CompletedOwnerUserID")
                    wsClass.CompletedSupportUserID = Reader1.Item("CompletedSupportUserID")
                    wsClass.InitialAssignmentTypeID = Reader1.Item("InitialAssignmentTypeID")
                    wsClass.LynxID = Reader1.Item("LynxID")
                    wsClass.OwnerUserID = Reader1.Item("OwnerUserID")
                    wsClass.SourceApplicationID = Reader1.Item("SourceApplicationID")
                    wsClass.SupportUserID = Reader1.Item("SupportUserID")
                    wsClass.ClaimAspectNumber = Reader1.Item("ClaimAspectNumber")
                    wsClass.CoverageProfileCD = Reader1.Item("CoverageProfileCD")
                    wsClass.CreatedDate = Reader1.Item("CreatedDate")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.ExposureCD = Reader1.Item("ExposureCD")
                    wsClass.NewAssignmentAnalystFlag = Reader1.Item("NewAssignmentAnalystFlag")
                    wsClass.NewAssignmentOwnerFlag = Reader1.Item("NewAssignmentOwnerFlag")
                    wsClass.NewAssignmentSupportFlag = Reader1.Item("NewAssignmentSupportFlag")
                    wsClass.PriorityFlag = Reader1.Item("PriorityFlag")
                    wsClass.SourceApplicationPassThruData = Reader1.Item("SourceApplicationPassThruData")
                    wsClass.WarrantyExistsFlag = Reader1.Item("WarrantyExistsFlag")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")

                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_claim_aspect
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_claim_aspect
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetCustomClientWSMapping
    ' This APD function returns all the conditional based
    ' XML incoming mappings by InsuranceCompanyID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns all the conditional based XML incoming mappings by LynxID.")>
    Public Function GetCustomClientWSMapping(ByVal InsuranceCompanyID As Integer) As List(Of utb_client_webservice_config)
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_client_webservice_config
        Dim wsData As New List(Of utb_client_webservice_config)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspGetCustomClientWSMapping"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@iInsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@iInsuranceCompanyID").Value = InsuranceCompanyID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            If Reader1.HasRows Then
                Do While Reader1.Read
                    wsClass = New utb_client_webservice_config
                    wsClass.WSConfigID = Reader1.Item("WSConfigID")
                    wsClass.XMLConditionalTag = Reader1.Item("XMLConditionalTag")
                    wsClass.Condition = Reader1.Item("Condition")
                    wsClass.XMLMappingFrom = Reader1.Item("XMLMappingFrom")
                    wsClass.XMLMappingTo = Reader1.Item("XMLMappingTo")
                    wsClass.EnabledFlag = Reader1.Item("EnabledFlag")
                    wsClass.SysLastUserID = Reader1.Item("SysLastUserID")
                    wsClass.SysLastUpdatedDate = Reader1.Item("SysLastUpdatedDate")

                    wsClass.Errors = ""

                    wsData.Add(wsClass)
                Loop
            Else
                wsClass = New utb_client_webservice_config
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_client_webservice_config
            wsClass.Errors = "WebServices Failed: " & oExcept.ToString

            wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ClearFNOLLoadTables
    ' This APD function clears out the old data in the FNOL Load
    ' tables by LynxID
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function clears out the old data in the FNOL Load tables by LynxID.")>
    Public Function ClearFNOLLoadTables(ByVal LynxID As Integer) As Boolean
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim wsClass As New utb_client_webservice_config
        Dim wsData As New List(Of utb_client_webservice_config)

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspFNOLClearLoadTables"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@LynxID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@LynxID").Value = LynxID

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader

            Return True
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return False
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function

    ''-----------------------------------------------------------
    '' APD: DataPresenterNet
    '' This APD function executes ExecuteSpAsXML to collect data
    '' and using xslTranslation creates a web page.
    ''-----------------------------------------------------------
    '<WebMethod(Description:="This APD function executes ExecuteSpAsXML to collect data and using xslTranslation creates a web page.")> _
    'Public Function DataPresenterNet(ByVal StoredProcedure As String, ByVal xslPage As String, ByVal Parameters As String) As String
    '    '--------------------------------------
    '    ' Global Variables
    '    '--------------------------------------
    '    Dim docXML As New XmlDocument
    '    Dim XSLTransform As New Xsl.XslCompiledTransform
    '    Dim uspXMLXDoc As New XmlDocument
    '    Dim HTMLData As New StringWriter

    '    Try
    '        '------------------------------------------
    '        ' Get XML data from the WebService and the
    '        ' uspVehicleAssignGetDetailTVD
    '        '------------------------------------------
    '        uspXMLXDoc = ExecuteSpAsXML(StoredProcedure, Parameters)
    '        If uspXMLXDoc.OuterXml = Nothing Then
    '            ' Throw exception
    '        End If

    '        '------------------------------------------
    '        ' Transform the files
    '        '------------------------------------------
    '        'XSLTransform.Load(AppSettings("xslPath") & xslPage)
    '        XSLTransform.Load(AppSettings("xslPath") & xslPage)
    '        XSLTransform.Transform(uspXMLXDoc, Nothing, HTMLData)

    '        Return HTMLData.ToString
    '    Catch oExcept As Exception

    '        '------------------------------
    '        ' Email Notify of the Error
    '        '------------------------------

    '        '?????? NEED BETTER ERROR HANDELER HERE ??????

    '        'wsClass = New uspVehicleAssignGetDetail
    '        'wsClass.Errors = "WebServices Failed: " & oExcept.ToString

    '        'wsData.Add(wsClass)
    '        'SendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
    '        'Return "" 'wsData

    '    End Try

    'End Function

    '-----------------------------------------------------------
    ' APD: ExecuteSpAsXML
    ' This APD function executes the specified WSXML stored procedure
    ' and returns the raw XML
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function executes the specified WSXML stored procedure and returns the raw XML")>
    Public Function ExecuteSpAsXML(ByVal StoredProcedure As String, ByVal Parameters As String) As XmlDocument
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "EXEC " & StoredProcedure & " " & Parameters
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Dim XmlReturnReader As XmlReader
            XmlReturnReader = cmdSelect.ExecuteXmlReader()
            XmlReturnReader.Read()

            Dim xdoc As New XmlDocument
            xdoc.Load(XmlReturnReader)
            Return xdoc
        Catch oExcept As Exception
            Dim XMLErrorDoc As New XmlDocument
            Dim FunctionName As New System.Diagnostics.StackFrame
            Dim sError As String = ""

            XMLErrorDoc.LoadXml("<ErrorHandler><Error>ERROR: Executing stored procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message & "</Error></ErrorHandler>")
            sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
            LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & StoredProcedure, sError, " StoredProc: " & StoredProcedure & " Params: " & Parameters)

            Return XMLErrorDoc
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ExecuteSp
    ' This APD function executes the specified stored procedure
    ' and returns the number of records modified
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function executes the specified stored procedure and returns the number of records modified")>
    Public Function ExecuteSp(ByVal StoredProcedure As String, ByVal Parameters As String) As String
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim sRecordsAffected As String = ""

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "EXEC " & StoredProcedure & " " & Parameters
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            sRecordsAffected = cmdSelect.ExecuteNonQuery

            Return sRecordsAffected
        Catch oExcept As Exception
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
            LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & StoredProcedure, sError, " StoredProc: " & StoredProcedure & " Params: " & Parameters)

            Return sError
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetConfigXMLData
    ' This APD function gets the XML data from the Config.xml
    ' based on the XPath
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function gets the XML data from the Config.xml based on the XPath")>
    Public Function GetConfigXMLData(ByVal XPath As String) As XmlElement
        Dim ConfigXMLDoc As New XmlDocument
        Dim XMLNode As XmlNode

        Try
            '-------------------------------------------
            ' Get the Config.xml file as XMLDocument
            '-------------------------------------------
            ConfigXMLDoc.Load(AppSettings("ConfigXML"))

            '-------------------------------------------
            ' If XPath is passed, get just that data
            '-------------------------------------------
            XMLNode = ConfigXMLDoc.SelectSingleNode(XPath)

            Return XMLNode
        Catch oExcept As Exception
            Dim XMLErrorDoc As New XmlDocument

            XMLErrorDoc.LoadXml("<ErrorHandler><Error>ERROR: Config.xml file or XPath item not found... " & oExcept.ToString & "</Error></ErrorHandler>")
            XMLNode = XMLErrorDoc.SelectSingleNode("ErrorHandler/Error")

            LogEvent("WebServices Failed:", "ERROR", "Config.xml file or XPath item not found...", "ERROR: Config.xml file or XPath item not found... " & oExcept.ToString, "Config.XML Path/XPath: " & AppSettings("ConfigXML") & " - " & XPath)

            Return XMLNode
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: UpdateSessionVar
    ' This APD function updates a APD session variable 
    ' by GUID and WindowID.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function updates a APD session variable by GUID and WindowID.")>
    Public Function UpdateSessionVar(ByVal SessionID As String, ByVal SessionVariable As String, ByVal SessionValue As String) As Integer
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim iRC As Integer = 0

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspSessionDataUpd"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@SessionID", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@SessionID").Value = SessionID

            cmdSQL.Parameters.Add("@SessionVariable", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@SessionVariable").Value = SessionVariable

            cmdSQL.Parameters.Add("@SessionValue", SqlDbType.VarChar, 7500)
            cmdSQL.Parameters("@SessionValue").Value = SessionValue

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            iRC = cmdSQL.ExecuteNonQuery

            Return iRC
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return 99
        Finally
            cmdSQL = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetApplicationVar
    ' This APD function gets an APD application variable 
    ' by Name.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function gets an APD application variable by Name.")>
    Public Function GetApplicationVar(ByVal VariableName As String) As String
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim drRowsAffected As SqlDataReader
        Dim sValue As String = ""

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspGetApplicationVar"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@vVariableName", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vVariableName").Value = VariableName

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            drRowsAffected = cmdSQL.ExecuteReader()

            If drRowsAffected.HasRows Then
                drRowsAffected.Read()
                sValue = drRowsAffected.Item("Value")
            Else
                sValue = ""
            End If

            Return sValue
        Catch oExcept As Exception
            '------------------------------
            ' Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Getting value for application variable: " & VariableName & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)
            LogEvent("WSServerSideProcessor", "ERROR", "Failed to get application value for variable: " & VariableName, "", sError)

            Return sError
        Finally
            cmdSQL = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: StuffClaimSessionVars
    ' This APD function updates all APD session variables for
    ' a claim
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function updates all APD session variables for a claim.")>
    Public Function StuffClaimSessionVars(ByVal StoredProcedure As String, ByVal LynxID As String, ByVal SessionID As String, ByVal WindowID As String) As XmlDocument
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim oXMLDoc As New XmlDocument
        Dim oClaimElement As XmlElement = Nothing
        Dim sCurrentSessionVar As String = ""

        Try
            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(ConfigurationManager.AppSettings("Debug")) = "YES" Then
                LogEvent("PGWAPDFoundation", "DEBUGGING", "StuffClaimSessionVars - Start: " & Date.Now, "Session controls for Window: " & WindowID & " and SessionKey: " & SessionID, "Vars: StoredProc = " & StoredProcedure & ", LynxID = " & LynxID)
            End If

            '-------------------------------------------
            ' Get the XML from the SP in the database
            '-------------------------------------------
            oXMLDoc = ExecuteSpAsXML(StoredProcedure, LynxID)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(ConfigurationManager.AppSettings("Debug")) = "YES" Then
                LogEvent("PGWAPDFoundation", "DEBUGGING", "StuffClaimSessionVars - StoredProc XML", "XML from Database SP in EventDetail", oXMLDoc.InnerText)
            End If

            oClaimElement = oXMLDoc.SelectSingleNode("//Claim")

            sCurrentSessionVar = "LynxID-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("LynxID").Value)

            sCurrentSessionVar = "ClaimStatus-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("ClaimStatus").Value)

            sCurrentSessionVar = "RestrictedFlag-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("RestrictedFlag").Value)

            sCurrentSessionVar = "ClaimOpen-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("ClaimOpen").Value)

            sCurrentSessionVar = "InsuranceCompanyName-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("InsuranceCompanyName").Value)

            sCurrentSessionVar = "InsuranceCompanyID-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("InsuranceCompanyID").Value)

            sCurrentSessionVar = "ClientClaimNumber-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("ClientClaimNumber").Value)

            sCurrentSessionVar = "InsuranceCompanyID-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("InsuranceCompanyID").Value)

            sCurrentSessionVar = "InsuredNameFirst-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("InsuredNameFirst").Value)

            sCurrentSessionVar = "InsuredNameLast-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("InsuredNameLast").Value)

            sCurrentSessionVar = "InsuredBusinessName-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("InsuredBusinessName").Value)

            sCurrentSessionVar = "LossDate-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("LossDate").Value)

            sCurrentSessionVar = "Claim_ClaimAspectID-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("Claim_ClaimAspectID").Value)

            sCurrentSessionVar = "DemoFlag-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("DemoFlag").Value)

            sCurrentSessionVar = "SourceApplicationCode-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("SourceApplicationCode").Value)

            sCurrentSessionVar = "CoverageLimitWarningInd-" & WindowID
            UpdateSessionVar(SessionID, sCurrentSessionVar, oClaimElement.Attributes("CoverageLimitWarningInd").Value)

            '---------------------------------
            ' Debug Data
            '---------------------------------
            If UCase(ConfigurationManager.AppSettings("Debug")) = "YES" Then
                LogEvent("PGWAPDFoundation", "DEBUGGING", "StuffClaimSessionVars - Finished: " & Date.Now, "Session controls for Window: " & WindowID & " and SessionKey: " & SessionID, "Vars: StoredProc = " & StoredProcedure & ", LynxID = " & LynxID)
            End If

            Return oXMLDoc
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim XMLErrorDoc As New XmlDocument

            XMLErrorDoc.LoadXml("<ErrorHandler><Error>ERROR: Session variables not updated (StuffClaimSessionVars)... " & oExcept.ToString & "</Error></ErrorHandler>")
            LogEvent("WebServices Failed:", "ERROR", "Session variables not updated (StuffClaimSessionVars)...", "ERROR: Session variables not updated (StuffClaimSessionVars)... " & oExcept.ToString, "")

            Return XMLErrorDoc
        Finally
            oXMLDoc = Nothing
            oClaimElement = Nothing
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: LogPartnerEvent
    ' This function make a log entry into the utb_partner_event_log
    ' table in the udb_partner database
    '-----------------------------------------------------------
    <WebMethod(Description:="This function make a log entry into the utb_partner_event_log table in the udb_partner database.")>
    Public Function LogPartnerEvent(ByVal EventType As String, ByVal EventStatus As String, ByVal EventDescription As String, ByVal EventDetailedDescription As String, ByVal EventXml As String) As Integer
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim Reader As SqlClient.SqlDataReader
        Dim iRecID As Integer = 0

        Try
            'SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "ERROR-DEBUG", "XML=" & EventXml & " - " & EventDescription, ConfigurationManager.AppSettings("SMTPServer"))

            '-------------------------------------------
            ' If Debug = YES then log to flat file also
            '-------------------------------------------
            If AppSettings("Debug") = "YES" Then
                '----------------------------------------
                ' Took this out for now.  Has issues
                ' with the file system being already
                ' in use
                '----------------------------------------
                'LogEventToFile(EventType, EventStatus, EventDescription, EventDetailedDescription, EventXml)
            End If

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspPartnerEventInsLogEntry"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString_Partner")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@vEventType", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vEventType").Value = EventType

            cmdSQL.Parameters.Add("@vEventStatus", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vEventStatus").Value = EventStatus

            cmdSQL.Parameters.Add("@vEventDescription", SqlDbType.VarChar, 100)
            cmdSQL.Parameters("@vEventDescription").Value = EventDescription

            cmdSQL.Parameters.Add("@vEventDetailedDescription", SqlDbType.VarChar, 1000)
            cmdSQL.Parameters("@vEventDetailedDescription").Value = EventDetailedDescription

            cmdSQL.Parameters.Add("@vEventXML", SqlDbType.Text)
            cmdSQL.Parameters("@vEventXML").Value = EventXml

            '-------------------------------------------
            ' Output parameters from the stored proc
            '-------------------------------------------
            Dim cmdOutput As New SqlParameter("@iRecID", SqlDbType.Int, 4)
            cmdOutput.Direction = ParameterDirection.Output
            cmdSQL.Parameters.Add(cmdOutput)

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader = cmdSQL.ExecuteReader

            'If Reader.HasRows Then
            'Reader.Read()
            If cmdSQL.Parameters("@iRecID").Value > 0 Then
                Return cmdSQL.Parameters("@iRecID").Value
            Else
                Throw New System.Exception(String.Format("Error: {0}", "WebSerices Failed: Partner event logging failed to insert a row (LogEvent)..."))
            End If
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Partner event logging")
            sBody = String.Format("Error: {0}", "WebSerices Failed: Partner event logging failed to insert a row (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
            Return 0
        Finally
            cmdSQL = Nothing
            Reader.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ExecuteSp
    ' This APD function executes the specified stored procedure
    ' and returns the results as a string
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function executes the specified stored procedure and returns the results as string")>
    Public Function ExecuteSpAsString(ByVal StoredProcedure As String, ByVal Parameters As String) As String
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim sReturnData As String = ""
        Dim ReturnDataReader As SqlClient.SqlDataReader

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "EXEC " & StoredProcedure & " " & Parameters
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            ReturnDataReader = cmdSelect.ExecuteReader
            ReturnDataReader.Read()
            sReturnData = ReturnDataReader.Item(0)

            Return sReturnData
        Catch oExcept As Exception
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
            LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & StoredProcedure, sError, " StoredProc: " & StoredProcedure & " Params: " & Parameters)

            Return sError
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: ChoiceShopProcess
    ' This APD function creates/updates HQ Choice shops into APD.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function creates/updates HQ Choice shops into APD.")>
    Public Function ChoiceShopProcess(
        ByVal bNewShopFlag As Boolean _
        , ByVal sHQBusinessTypeCD As String _
        , ByVal sHQAddress1 As String _
        , ByVal sHQAddress2 As String _
        , ByVal sHQAddressCity As String _
        , ByVal sHQAddressState As String _
        , ByVal sHQAddressZip As String _
        , ByVal sHQEmailAddress As String _
        , ByVal bHQEnabledFlag As Boolean _
        , ByVal sHQFaxAreaCode As String _
        , ByVal sHQFaxExchangeNumber As String _
        , ByVal sHQFaxUnitNumber As String _
        , ByVal sHQFedTaxId As String _
        , ByVal sHQName As String _
        , ByVal sHQPhoneAreaCode As String _
        , ByVal sHQPhoneExchangeNumber As String _
        , ByVal sHQPhoneUnitNumber As String _
        , ByVal iHQSysLastUserID As Integer _
        , ByVal sHQApplicationCD As String _
        , ByVal sHQShopUID As String _
        , ByVal sHQShopPartnerID As String
        ) As String

        '-------------------------------
        ' Database Calls
        '-------------------------------  
        Dim sStoredProcedure As String = "uspChoiceShopProcess"
        Dim Conn As New SqlConnection
        Dim cmdSql As New SqlClient.SqlCommand
        'Dim Reader As SqlClient.SqlDataReader
        Dim sReturnData As String = ""
        Dim sProcessingType As String = ""

        Try
            '-------------------------------------------
            ' Event Logging
            '-------------------------------------------
            If AppSettings("Debug") = "YES" Then
                LogEvent("Choice Shop", "START", "Choice processing started", "Processing Choice shop request and parameters. ", "Params: " & bNewShopFlag & ", " & sHQBusinessTypeCD & ", " & sHQAddress1 & ", " & sHQAddress2 & ", " & sHQAddressCity & ", " & sHQAddressState & ", " & sHQAddressZip & ", " & sHQEmailAddress & ", " & bHQEnabledFlag & ", " & sHQFaxAreaCode & ", " & sHQFaxExchangeNumber & ", " & sHQFaxUnitNumber & ", " & sHQFedTaxId & ", " & sHQName & ", " & sHQPhoneAreaCode & ", " & sHQPhoneExchangeNumber & ", " & sHQPhoneUnitNumber & ", " & iHQSysLastUserID & ", " & sHQApplicationCD & ", " & sHQShopUID & ", " & sHQShopPartnerID)
            End If

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.CommandText = sStoredProcedure
            cmdSql.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSql.Parameters.Add("@NewShopFlag", SqlDbType.Bit)
            cmdSql.Parameters("@NewShopFlag").Value = bNewShopFlag

            cmdSql.Parameters.Add("@HQBusinessTypeCD", SqlDbType.VarChar, 4)
            cmdSql.Parameters("@HQBusinessTypeCD").Value = sHQBusinessTypeCD

            cmdSql.Parameters.Add("@HQAddress1", SqlDbType.VarChar, 50)
            cmdSql.Parameters("@HQAddress1").Value = sHQAddress1

            cmdSql.Parameters.Add("@HQAddress2", SqlDbType.VarChar, 50)
            cmdSql.Parameters("@HQAddress2").Value = sHQAddress2

            cmdSql.Parameters.Add("@HQAddressCity", SqlDbType.VarChar, 30)
            cmdSql.Parameters("@HQAddressCity").Value = sHQAddressCity

            cmdSql.Parameters.Add("@HQAddressState", SqlDbType.VarChar, 2)
            cmdSql.Parameters("@HQAddressState").Value = sHQAddressState

            cmdSql.Parameters.Add("@HQAddressZip", SqlDbType.VarChar, 8)
            cmdSql.Parameters("@HQAddressZip").Value = sHQAddressZip

            cmdSql.Parameters.Add("@HQEmailAddress", SqlDbType.VarChar, 50)
            cmdSql.Parameters("@HQEmailAddress").Value = sHQEmailAddress

            cmdSql.Parameters.Add("@HQEnabledFlag", SqlDbType.Bit)
            cmdSql.Parameters("@HQEnabledFlag").Value = bHQEnabledFlag

            cmdSql.Parameters.Add("@HQFaxAreaCode", SqlDbType.VarChar, 3)
            cmdSql.Parameters("@HQFaxAreaCode").Value = sHQFaxAreaCode

            cmdSql.Parameters.Add("@HQFaxExchangeNumber", SqlDbType.VarChar, 3)
            cmdSql.Parameters("@HQFaxExchangeNumber").Value = sHQFaxExchangeNumber

            cmdSql.Parameters.Add("@HQFaxUnitNumber", SqlDbType.VarChar, 4)
            cmdSql.Parameters("@HQFaxUnitNumber").Value = sHQFaxUnitNumber

            cmdSql.Parameters.Add("@HQFedTaxId", SqlDbType.VarChar, 15)
            cmdSql.Parameters("@HQFedTaxId").Value = sHQFedTaxId

            cmdSql.Parameters.Add("@HQName", SqlDbType.VarChar, 50)
            cmdSql.Parameters("@HQName").Value = sHQName

            cmdSql.Parameters.Add("@HQPhoneAreaCode", SqlDbType.VarChar, 3)
            cmdSql.Parameters("@HQPhoneAreaCode").Value = sHQPhoneAreaCode

            cmdSql.Parameters.Add("@HQPhoneExchangeNumber", SqlDbType.VarChar, 3)
            cmdSql.Parameters("@HQPhoneExchangeNumber").Value = sHQPhoneExchangeNumber

            cmdSql.Parameters.Add("@HQPhoneUnitNumber", SqlDbType.VarChar, 4)
            cmdSql.Parameters("@HQPhoneUnitNumber").Value = sHQPhoneUnitNumber

            cmdSql.Parameters.Add("@HQSysLastUserID", SqlDbType.Int)
            cmdSql.Parameters("@HQSysLastUserID").Value = iHQSysLastUserID

            cmdSql.Parameters.Add("@HQApplicationCD", SqlDbType.VarChar, 4)
            cmdSql.Parameters("@HQApplicationCD").Value = sHQApplicationCD

            cmdSql.Parameters.Add("@HQShopUID", SqlDbType.VarChar, 50)
            cmdSql.Parameters("@HQShopUID").Value = sHQShopUID

            cmdSql.Parameters.Add("@HQShopPartnerID", SqlDbType.VarChar, 50)
            cmdSql.Parameters("@HQShopPartnerID").Value = sHQShopPartnerID

            '-------------------------------------------
            ' Output parameters from the stored proc
            '-------------------------------------------
            Dim cmdOutput As New SqlParameter("@ReturnShopLocationID", SqlDbType.Int, 4)
            cmdOutput.Direction = ParameterDirection.Output
            cmdSql.Parameters.Add(cmdOutput)

            '-------------------------------------------
            ' Event Logging
            '-------------------------------------------
            If AppSettings("Debug") = "YES" Then
                If bNewShopFlag = True Then
                    sProcessingType = "INSERT"
                    LogEvent("Choice Shop", "DEBUG", "Choice processing Shop INSERT", "Processing NEW Choice shop request: " & sHQName, "")
                Else
                    sProcessingType = "UPDATE"
                    LogEvent("Choice Shop", "DEBUG", "Choice processing Shop UPDATE", "Processing Choice shop update request for HQShopUID: " & sHQShopUID & ", HQShopPartnerID: " & sHQShopPartnerID, "")
                End If
            End If

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            cmdSql.ExecuteNonQuery()

            '-------------------------------------------
            ' Event Logging
            '-------------------------------------------
            'If AppSettings("Debug") = "YES" Then
            'LogEvent("Choice Shop", "END", "Choice processing ended", "Processing Choice shop request for HQShopUID: " & sHQShopUID & ", HQShopPartnerID: " & sHQShopPartnerID & ", RequestType: " & sProcessingType & ", Return ShopLocationID: " & cmdSql.Parameters("@ReturnShopLocationID").Value & " - Complete", "")
            'End If

            If cmdSql.Parameters("@ReturnShopLocationID").Value > 0 Then
                Return cmdSql.Parameters("@ReturnShopLocationID").Value
            Else
                Throw New System.Exception(String.Format("Error: {0}", "Failed to return a valid ShopLocationID.  Returned @ReturnShopLocationID: " & cmdSql.Parameters("@ReturnShopLocationID").Value))
            End If

            Return sReturnData
        Catch oExcept As Exception
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & sStoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
            LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & sStoredProcedure, sError, " StoredProc: " & sStoredProcedure)

            Return sError
        Finally
            cmdSql = Nothing
            'Reader.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetCRUD
    ' This APD function gets the CRUD permissions from APD
    ' for different screens.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function gets the CRUD permissions from APD for different screens.")>
    Public Function GetCRUD(ByVal sReference As String, ByVal iUserid As Integer) As XmlDocument
        Dim oReturnXML As XmlElement = Nothing
        Dim oXMLNode As XmlNode = Nothing
        Dim oXMLNodeList As XmlNodeList = Nothing
        Dim sCRUD As String = ""
        Dim Conn As New SqlConnection
        Dim cmdSql As New SqlClient.SqlCommand
        Dim sStoredProcedure As String = ""
        Dim sParams As String = ""
        Dim XmlReturnReader As XmlReader = Nothing
        Dim xdoc As New XmlDocument

        Try
            '-------------------------------
            ' Database access
            '-------------------------------  
            sStoredProcedure = "uspCRUDGetDetailWSXML"
            sParams = "'" & sReference & "', " & iUserid

            '-------------------------------------------
            ' Event Logging
            '-------------------------------------------
            If AppSettings("Debug") = "YES" Then
                LogEvent("PGWAPDFoundation", "DEBUGGING", "GetCRUD - Start: " & Date.Now, "CRUD Permissions for screens by reference: " & sReference & " and UserID: " & iUserid, "Vars: StoredProc = " & sStoredProcedure & ", Params = " & sReference & ", " & iUserid)
            End If

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSql.CommandType = CommandType.StoredProcedure
            cmdSql.CommandText = sStoredProcedure
            cmdSql.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSql.Parameters.Add("@EntityList", SqlDbType.VarChar, 500)
            cmdSql.Parameters("@EntityList").Value = sReference

            cmdSql.Parameters.Add("@UserID", SqlDbType.Int)
            cmdSql.Parameters("@UserID").Value = iUserid

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            XmlReturnReader = cmdSql.ExecuteXmlReader()
            XmlReturnReader.Read()

            xdoc.Load(XmlReturnReader)

            '-------------------------------------------
            ' Event Logging
            '-------------------------------------------
            If AppSettings("Debug") = "YES" Then
                LogEvent("PGWAPDFoundation", "DEBUGGING", "GetCRUD - Ended: " & Date.Now, "CRUD Permissions for screens by reference completed.", "Vars: Returned string = " & xdoc.InnerXml)
            End If

            Return xdoc

        Catch oExcept As Exception
            Dim XMLErrorDoc As New XmlDocument
            Dim FunctionName As New System.Diagnostics.StackFrame
            Dim sError As String = ""

            XMLErrorDoc.LoadXml("<ErrorHandler><Error>ERROR: Executing stored procedure: " & sStoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message & "</Error></ErrorHandler>")
            sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & sStoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
            LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & sStoredProcedure, sError, " StoredProc: " & sStoredProcedure & " Params: " & sParams)

            Return XMLErrorDoc
        Finally
            cmdSql = Nothing
            XmlReturnReader.Close()
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APD: GetLYNXCombinedShops
    ' This APD function returns a list of LYNX Select Shops
    ' and Choice Shops with in a range of your zip code.
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function returns a list of LYNX Select Shops and Choice Shops with in a range of your zip code.")>
    Public Function GetLYNXCombinedShopsByZipcode(
        ByVal ZipCode As String _
        , ByVal InsuranceCompanyID As Integer _
        , ByVal MaxShops As Integer
        ) As List(Of utb_shop_location)

        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim Reader1 As SqlClient.SqlDataReader
        Dim iRC As Integer = 0
        Dim sShopTypeCode As String
        Dim wsClass As New utb_shop_location
        Dim wsData As New List(Of utb_shop_location)

        Try
            sShopTypeCode = "B"

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.StoredProcedure
            cmdSelect.CommandText = "uspShopCombinedSearchByDistanceXML"
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSelect.Parameters.Add("@Zip", SqlDbType.VarChar, 8)
            cmdSelect.Parameters("@Zip").Value = ZipCode

            cmdSelect.Parameters.Add("@InsuranceCompanyID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@InsuranceCompanyID").Value = InsuranceCompanyID

            cmdSelect.Parameters.Add("@ShopTypeCode", SqlDbType.VarChar, 1)
            cmdSelect.Parameters("@ShopTypeCode").Value = sShopTypeCode

            cmdSelect.Parameters.Add("@MaxShops", SqlDbType.Int, 4)
            cmdSelect.Parameters("@MaxShops").Value = MaxShops

            cmdSelect.Parameters.Add("@UserID", SqlDbType.Int, 4)
            cmdSelect.Parameters("@UserID").Value = 0

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader1 = cmdSelect.ExecuteReader()

            If Reader1.HasRows Then
                Do While Reader1.Read
                    If Not IsDBNull(Reader1.Item("Shop!2!Name")) Then
                        wsClass = New utb_shop_location
                        wsClass.ShopLocationID = Reader1.Item("Shop!2!ShopLocationID")
                        wsClass.ShopGUID = Reader1.Item("Shop!2!ShopGUID")
                        wsClass.Source = Reader1.Item("Shop!2!Source")
                        wsClass.Distance = Reader1.Item("Shop!2!Distance")
                        wsClass.ShopName = Reader1.Item("Shop!2!Name")
                        wsClass.Address1 = Reader1.Item("Shop!2!Address1")
                        wsClass.Address2 = Reader1.Item("Shop!2!Address2")
                        wsClass.AddressCity = Reader1.Item("Shop!2!AddressCity")
                        wsClass.AddressState = Reader1.Item("Shop!2!AddressState")
                        wsClass.AddressZip = Reader1.Item("Shop!2!AddressZip")
                        wsClass.PhoneNumber = Reader1.Item("Shop!2!PhoneAreaCode") & "-" & Reader1.Item("Shop!2!PhoneExchangeNumber") & "-" & Reader1.Item("Shop!2!PhoneUnitNumber")
                        wsClass.FaxNumber = Reader1.Item("Shop!2!FaxAreaCode") & "-" & Reader1.Item("Shop!2!FaxExchangeNumber") & "-" & Reader1.Item("Shop!2!FaxUnitNumber")
                        wsClass.ShopOpen = Reader1.Item("Shop!2!ShopOpen")
                        wsClass.TimeZone = Reader1.Item("Shop!2!TimeZone")
                        'wsClass.ContactName = Reader1.Item("ShopContact!3!Name")
                        'wsClass.ContactPhoneNumber = Reader1.Item("ShopContact!3!PhoneAreaCode") & "-" & Reader1.Item("ShopContact!3!PhoneExchangeNumber") & "-" & Reader1.Item("ShopContact!3!PhoneUnitNumber")
                        wsClass.OperatingMondayStartTime = Reader1.Item("Shop!2!OperatingMondayStartTime")
                        wsClass.OperatingMondayEndTime = Reader1.Item("Shop!2!OperatingMondayEndTime")
                        wsClass.OperatingTuesdayStartTime = Reader1.Item("Shop!2!OperatingTuesdayStartTime")
                        wsClass.OperatingTuesdayEndTime = Reader1.Item("Shop!2!OperatingTuesdayEndTime")
                        wsClass.OperatingWednesdayStartTime = Reader1.Item("Shop!2!OperatingWednesdayStartTime")
                        wsClass.OperatingWednesdayEndTime = Reader1.Item("Shop!2!OperatingWednesdayEndTime")
                        wsClass.OperatingThursdayStartTime = Reader1.Item("Shop!2!OperatingThursdayStartTime")
                        wsClass.OperatingThursdayEndTime = Reader1.Item("Shop!2!OperatingThursdayEndTime")
                        wsClass.OperatingFridayStartTime = Reader1.Item("Shop!2!OperatingFridayStartTime")
                        wsClass.OperatingFridayEndTime = Reader1.Item("Shop!2!OperatingFridayEndTime")
                        wsClass.OperatingSaturdayStartTime = Reader1.Item("Shop!2!OperatingSaturdayStartTime")
                        wsClass.OperatingSaturdayEndTime = Reader1.Item("Shop!2!OperatingSaturdayEndTime")
                        wsClass.OperatingSundayStartTime = Reader1.Item("Shop!2!OperatingSundayStartTime")
                        wsClass.OperatingSundayEndTime = Reader1.Item("Shop!2!OperatingSundayEndTime")

                        wsClass.Latitude = Reader1.Item("Shop!2!Latitude")
                        wsClass.Longitude = Reader1.Item("Shop!2!Longitude")

                        wsClass.Errors = ""

                        wsData.Add(wsClass)
                    End If
                Loop
            Else
                wsClass = New utb_shop_location
                wsClass.Errors = "No records found..."

                wsData.Add(wsClass)
            End If

            Return wsData
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "APD Web Service: APDService")
            sBody = String.Format("Error: {0}", "APD Web Service: Processing failed (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            'oFoundation.SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))

            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            wsClass = New utb_shop_location
            wsClass.Errors = sBody & " - " & oExcept.ToString

            'wsData.Add(wsClass)
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return wsData
        Finally
            cmdSelect = Nothing
            Reader1.Close()
            Conn.Close()
        End Try
    End Function
End Class