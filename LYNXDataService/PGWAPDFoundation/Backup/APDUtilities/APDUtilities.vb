﻿Imports System.Xml
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail

Public Class APDUtilities
    '------------------------------------------------------------------------
    ' Processes a web assignment it expects an XML document:
    '
    ' <WebAssignment>
    '     <Claim> ... </Claim>
    '     <Vehicle> ... </Vehicle>
    '     <Involved> ... </Involved>
    ' </WebAssignment>
    '
    ' Multiple Vehicle and Involved elements are possible.
    ' Each element has multiple child elements as passed from Claim Point.
    '------------------------------------------------------------------------
    Private Function DoAssignment(ByRef objParamDom As XmlDocument, ByVal strDataSourceCode As String) As Long
        Const PROC_NAME As String = "APDUtilities: DoAssignment "

        'Objects that need clean-up
        Dim objXslDom As XmlDocument
        Dim objXferDom As XmlDocument

        'Primitives that don't
        Dim lngUID As Long = 0
        Dim blnRaiseToCaller As Boolean = False
        Dim strUID As String = ""
        Dim strXferXslName As String = ""
        Dim strXferXslVersion As String = ""

        '---------------------------------------
        ' ?????????????? Add Trace logging
        '---------------------------------------
        'If g_blnDebugMode Then g_objEvents.Trace("", PROC_NAME & "Started")

        'This boolean is used in the error handler to determine whether to
        'raise errors to the calling process or not.  Once we have stored
        'the data in the partner database, assume that the call is a success
        'as far as the caller is concerned.
        blnRaiseToCaller = True

        ' Determine where the assignment is coming from.
        'If strDataSourceCode = "CP" Or strDataSourceCode = "APD" Then
        '    ' Try to get a LynxID from the xml.
        '    strUID = GetChildNodeText(objParamDom, "/WebAssignment/Claim/LynxID", False)

        '    If IsNumeric(strUID) Then
        '        lngUID = CLng(strUID)
        '    Else
        '        lngUID = 0
        '    End If

        '    ' If not LynxID exists, create a new one.
        '    If CBool(lngUID = 0) Then
        '        lngUID = GetFnolUID()
        '    End If
        'Else
        'lngUID = CLng(GetChildNodeText(objParamDom, "/WebAssignment/Claim/LynxID", True))
        'End If

        'Store the assignment in the partner database
        'StoreAssignmentXml(lngUID, GetChildNodeText(objParamDom, "//DataSource", False), objParamDom.xml)

        'Ok, we made it into partner, so from the caller's standpoint
        'assume that the transaction is good and dont raise.
        'blnRaiseToCaller = False

        'Get some DOM objects to work with.
        'objXslDom = New MSXML2.DOMDocument40
        'objXferDom = New MSXML2.DOMDocument40

        'strXferXslName = GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Code='" & strDataSourceCode & "']/@Xsl")
        'strXferXslVersion = GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Code='" & strDataSourceCode & "']/@XslVersion")

        'Load up the assignment transfer style sheet.
        'LoadXmlFile(objXslDom, g_strSupportDocPath & "\" & strXferXslName & strXferXslVersion & ".xsl", PROC_NAME, "Style Sheet")

        'Transform the passed XML into XML submittable to APD.
        'objParamDom.transformNodeToObject(objXslDom, objXferDom)

        'Call sister function to do the actual load.
        'DoAssignment = ApdLoadAssignment(objXferDom, lngUID)

        'If g_blnDebugMode Then g_objEvents.Trace("LynxID = " & lngUID, PROC_NAME & "Finished")

        'ErrorHandler:

        '        objXslDom = Nothing
        '        objXferDom = Nothing

        '        'Everything below will be sent out when errors occur.
        '        If Err.Number <> 0 Then
        '            g_objEvents.Env("Raise To Caller = " & CStr(blnRaiseToCaller))
        '            g_objEvents.HandleEvent(Err.Number, "[Line " & Erl() & "] " & PROC_NAME & Err.Source, Err.Description, _
        '                "Param XML = '" & objParamDom.xml & "'", "", blnRaiseToCaller)
        '        End If
        Return 0
    End Function

    '------------------------------------------------------------------------
    ' Store the assignment in the partner database
    '------------------------------------------------------------------------
    Public Sub StoreAssignmentXml( _
        ByVal lUID As Long, _
        ByVal sDataSource As String, _
        ByVal sXml As String)

        Const PROC_NAME As String = "APDUtilities: StoreAssignmentXml "

        '---------------------------------------
        ' ?????????????? Add Trace logging
        '---------------------------------------
        'If g_blnDebugMode Then g_objEvents.Trace("", PROC_NAME)

        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "uspFNOLLoadTransaction " & lUID & ", '" & sDataSource & "', '" & sXml & "'"
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString_Partner")
            Conn.Open()

        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "PGWAPDFoundation WebService Error: Failed to retrieve existing basic bundle information (" & FunctionName.GetMethod.Name & ")...  " & oExcept.ToString)

            Dim drData As DataRow
            drData = dsReturnData.Tables(0).NewRow()
            drData("Error") = sError
            dsReturnData.Tables(0).Rows.Add(drData)
        End Try

        'Get the procedure level list from the APD database.
        'ConnectToDB(ePartner_Recordset)
        '("ConnectString_Partner")

        '(uspFNOLLoadTransaction, lngUID, 'strDataSource','strXML')

        'g_objDataAccessor.ExecuteSp( _
        '    GetConfig("ClaimPoint/WebAssignment/StoreAssignmentXmlSP"), _
        '    lngUID, "'" & strDataSource & "'", "'" & SQLQueryString(strXml) & "'")

        'ErrorHandler:

        '        If Err.Number <> 0 Then
        '            g_objEvents.HandleEvent(Err.Number, "[Line " & Erl() & "] " & PROC_NAME & Err.Source, Err.Description)
        '        End If
    End Sub

    Public Shared Sub SendMail1(ByVal MailFrom As String, ByVal MailTo As String, ByVal MailBCC As String, ByVal MailCC As String, ByVal MailSubject As String, ByVal MailBody As String)
        Dim mMailMessage As MailMessage = New MailMessage
        mMailMessage.From = New MailAddress(MailFrom)
        mMailMessage.To.Add(New MailAddress(MailTo))

        If ((Not (MailBCC) Is Nothing) _
                    AndAlso (MailBCC <> String.Empty)) Then
            mMailMessage.Bcc.Add(New MailAddress(MailBCC))
        End If

        If ((Not (MailCC) Is Nothing) _
                    AndAlso (MailCC <> String.Empty)) Then
            ' Set the CC address of the mail message
            mMailMessage.CC.Add(New MailAddress(MailCC))
        End If

        mMailMessage.Subject = MailSubject
        mMailMessage.Body = MailBody
        mMailMessage.IsBodyHtml = True
        mMailMessage.Priority = MailPriority.Normal
        Dim mSmtpClient As SmtpClient = New SmtpClient
        mSmtpClient.Send(mMailMessage)
    End Sub
End Class
