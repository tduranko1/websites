﻿Public Class ClassicSessionVars
    Private m_sSessionID As String
    Private m_sSessionVariable As String
    Private m_sSessionValue As String
    Private m_sSysLastUpdatedDate As String

    Private m_sErrors As String

    Public Property SessionID() As String
        Get
            Return m_sSessionID
        End Get
        Set(ByVal Value As String)
            m_sSessionID = Value
        End Set
    End Property

    Public Property SessionVariable() As String
        Get
            Return m_sSessionVariable
        End Get
        Set(ByVal Value As String)
            m_sSessionVariable = Value
        End Set
    End Property

    Public Property SessionValue() As String
        Get
            Return m_sSessionValue
        End Get
        Set(ByVal Value As String)
            m_sSessionValue = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As String
        Get
            Return m_sSysLastUpdatedDate
        End Get
        Set(ByVal Value As String)
            m_sSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
