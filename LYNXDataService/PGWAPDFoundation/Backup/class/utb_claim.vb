﻿Imports System.Xml.Serialization

Public Class utb_claim
    Private m_iLynxID As Integer
    Private m_iCallerInvolvedID As Integer
    Private m_iCarrierRepUserID As Integer
    Private m_iCatastrophicLossID As Integer
    Private m_iClaimantContactMethodID As Integer
    Private m_iContactInvolvedID As Integer
    Private m_iInsuranceCompanyID As Integer
    Private m_iIntakeUserID As Integer
    Private m_iLossTypeID As Integer
    Private m_iRoadLocationID As Integer
    Private m_iRoadtypeID As Integer
    Private m_iWeatherConditionID As Integer
    Private m_sAgentAreaCode As String
    Private m_sAgentExchangeNumber As String
    Private m_sAgentExtensionNumber As String
    Private m_sAgentName As String
    Private m_sAgentUnitNumber As String
    Private m_sClientClaimNumber As String
    Private m_sClientClaimNumberSquished As String
    Private m_iComplexityFinalScore As Integer
    Private m_iComplexityInitialScore As Integer
    Private m_bDemoFlag As Boolean
    Private m_dtIntakeFinishDate As Date
    Private m_dtIntakeStartDate As Date
    Private m_sLossCity As String
    Private m_sLossCounty As String
    Private m_dtLossDate As Date
    Private m_sLossDescription As String
    Private m_sLossLocation As String
    Private m_sLossState As String
    Private m_sLossZip As String
    Private m_sPoliceDepartmentName As String
    Private m_sPolicyNumber As String
    Private m_sRemarks As String
    Private m_bRestrictedFlag As Boolean
    Private m_sTripPurposeCD As String
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As Date

    Private m_sErrors As String

    Public Property LynxID() As Integer
        Get
            Return m_iLynxID
        End Get
        Set(ByVal Value As Integer)
            m_iLynxID = Value
        End Set
    End Property

    Public Property CallerInvolvedID() As Integer
        Get
            Return m_iCallerInvolvedID
        End Get
        Set(ByVal Value As Integer)
            m_iCallerInvolvedID = Value
        End Set
    End Property

    Public Property CarrierRepUserID() As Integer
        Get
            Return m_iCarrierRepUserID
        End Get
        Set(ByVal Value As Integer)
            m_iCarrierRepUserID = Value
        End Set
    End Property

    Public Property CatastrophicLossID() As Integer
        Get
            Return m_iCatastrophicLossID
        End Get
        Set(ByVal Value As Integer)
            m_iCatastrophicLossID = Value
        End Set
    End Property

    Public Property ClaimantContactMethodID() As Integer
        Get
            Return m_iClaimantContactMethodID
        End Get
        Set(ByVal Value As Integer)
            m_iClaimantContactMethodID = Value
        End Set
    End Property

    Public Property ContactInvolvedID() As Integer
        Get
            Return m_iContactInvolvedID
        End Get
        Set(ByVal Value As Integer)
            m_iContactInvolvedID = Value
        End Set
    End Property

    Public Property InsuranceCompanyID() As Integer
        Get
            Return m_iInsuranceCompanyID
        End Get
        Set(ByVal Value As Integer)
            m_iInsuranceCompanyID = Value
        End Set
    End Property

    Public Property IntakeUserID() As Integer
        Get
            Return m_iIntakeUserID
        End Get
        Set(ByVal Value As Integer)
            m_iIntakeUserID = Value
        End Set
    End Property

    Public Property LossTypeID() As Integer
        Get
            Return m_iLossTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iLossTypeID = Value
        End Set
    End Property

    Public Property RoadLocationID() As Integer
        Get
            Return m_iRoadLocationID
        End Get
        Set(ByVal Value As Integer)
            m_iRoadLocationID = Value
        End Set
    End Property

    Public Property RoadtypeID() As Integer
        Get
            Return m_iRoadtypeID
        End Get
        Set(ByVal Value As Integer)
            m_iRoadtypeID = Value
        End Set
    End Property

    Public Property WeatherConditionID() As Integer
        Get
            Return m_iWeatherConditionID
        End Get
        Set(ByVal Value As Integer)
            m_iWeatherConditionID = Value
        End Set
    End Property

    Public Property sAgentAreaCode() As String
        Get
            Return m_sAgentAreaCode
        End Get
        Set(ByVal Value As String)
            m_sAgentAreaCode = Value
        End Set
    End Property

    Public Property AgentExchangeNumber() As String
        Get
            Return m_sAgentExchangeNumber
        End Get
        Set(ByVal Value As String)
            m_sAgentExchangeNumber = Value
        End Set
    End Property

    Public Property AgentExtensionNumber() As String
        Get
            Return m_sAgentExtensionNumber
        End Get
        Set(ByVal Value As String)
            m_sAgentExtensionNumber = Value
        End Set
    End Property

    Public Property AgentName() As String
        Get
            Return m_sAgentName
        End Get
        Set(ByVal Value As String)
            m_sAgentName = Value
        End Set
    End Property

    Public Property AgentUnitNumber() As String
        Get
            Return m_sAgentUnitNumber
        End Get
        Set(ByVal Value As String)
            m_sAgentUnitNumber = Value
        End Set
    End Property

    Public Property ClientClaimNumber() As String
        Get
            Return m_sClientClaimNumber
        End Get
        Set(ByVal Value As String)
            m_sClientClaimNumber = Value
        End Set
    End Property

    Public Property ClientClaimNumberSquished() As String
        Get
            Return m_sClientClaimNumberSquished
        End Get
        Set(ByVal Value As String)
            m_sClientClaimNumberSquished = Value
        End Set
    End Property

    Public Property ComplexityFinalScore() As Integer
        Get
            Return m_iComplexityFinalScore
        End Get
        Set(ByVal Value As Integer)
            m_iComplexityFinalScore = Value
        End Set
    End Property

    Public Property ComplexityInitialScore() As Integer
        Get
            Return m_iComplexityInitialScore
        End Get
        Set(ByVal Value As Integer)
            m_iComplexityInitialScore = Value
        End Set
    End Property

    Public Property DemoFlag() As Boolean
        Get
            Return m_bDemoFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bDemoFlag = Value
        End Set
    End Property

    Public Property IntakeFinishDate() As Date
        Get
            Return m_dtIntakeFinishDate
        End Get
        Set(ByVal Value As Date)
            m_dtIntakeFinishDate = Value
        End Set
    End Property

    Public Property IntakeStartDate() As Date
        Get
            Return m_dtIntakeStartDate
        End Get
        Set(ByVal Value As Date)
            m_dtIntakeStartDate = Value
        End Set
    End Property

    Public Property LossCity() As String
        Get
            Return m_sLossCity
        End Get
        Set(ByVal Value As String)
            m_sLossCity = Value
        End Set
    End Property

    Public Property LossCounty() As String
        Get
            Return m_sLossCounty
        End Get
        Set(ByVal Value As String)
            m_sLossCounty = Value
        End Set
    End Property

    Public Property LossDate() As Date
        Get
            Return m_dtLossDate
        End Get
        Set(ByVal Value As Date)
            m_dtLossDate = Value
        End Set
    End Property

    Public Property LossDescription() As String
        Get
            Return m_sLossDescription
        End Get
        Set(ByVal Value As String)
            m_sLossDescription = Value
        End Set
    End Property

    Public Property LossLocation() As String
        Get
            Return m_sLossLocation
        End Get
        Set(ByVal Value As String)
            m_sLossLocation = Value
        End Set
    End Property

    Public Property LossState() As String
        Get
            Return m_sLossState
        End Get
        Set(ByVal Value As String)
            m_sLossState = Value
        End Set
    End Property

    Public Property LossZip() As String
        Get
            Return m_sLossZip
        End Get
        Set(ByVal Value As String)
            m_sLossZip = Value
        End Set
    End Property

    Public Property PoliceDepartmentName() As String
        Get
            Return m_sPoliceDepartmentName
        End Get
        Set(ByVal Value As String)
            m_sPoliceDepartmentName = Value
        End Set
    End Property

    Public Property PolicyNumber() As String
        Get
            Return m_sPolicyNumber
        End Get
        Set(ByVal Value As String)
            m_sPolicyNumber = Value
        End Set
    End Property

    Public Property Remarks() As String
        Get
            Return m_sRemarks
        End Get
        Set(ByVal Value As String)
            m_sRemarks = Value
        End Set
    End Property

    Public Property RestrictedFlag() As Boolean
        Get
            Return m_bRestrictedFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bRestrictedFlag = Value
        End Set
    End Property

    Public Property TripPurposeCD() As String
        Get
            Return m_sTripPurposeCD
        End Get
        Set(ByVal Value As String)
            m_sTripPurposeCD = Value
        End Set
    End Property

    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As Date
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As Date)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
