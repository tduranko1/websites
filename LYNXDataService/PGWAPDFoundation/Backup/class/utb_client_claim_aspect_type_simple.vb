﻿Public Class utb_client_claim_aspect_type_simple
    Private m_iClaimAspectTypeID As Integer
    Private m_vName As String
    Private m_bProfileRequiredFlag As Boolean
    Private m_sErrors As String

    Public Property ClaimAspectTypeID() As Integer
        Get
            Return m_iClaimAspectTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iClaimAspectTypeID = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return m_vName
        End Get
        Set(ByVal Value As String)
            m_vName = Value
        End Set
    End Property
    Public Property ProfileRequiredFlag() As Boolean
        Get
            Return m_bProfileRequiredFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bProfileRequiredFlag = Value
        End Set
    End Property
    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
