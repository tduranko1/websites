﻿Public Class utb_client_coverage_type_simple
    Private m_iClientCoverageTypeID As Integer
    Private m_vName As String
    Private m_bAdditionalCoverageFlag As Boolean
    Private m_sErrors As String

    Public Property ClientCoverageTypeID() As Integer
        Get
            Return m_iClientCoverageTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iClientCoverageTypeID = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return m_vName
        End Get
        Set(ByVal Value As String)
            m_vName = Value
        End Set
    End Property
    Public Property AdditionalCoverageFlag() As Boolean
        Get
            Return m_bAdditionalCoverageFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bAdditionalCoverageFlag = Value
        End Set
    End Property
    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property


End Class
