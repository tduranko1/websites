﻿Public Class utb_custom_forms_simple
    Private m_iFormSupplementID As Integer
    Private m_vFormType As String
    Private m_vShopStateCode As String
    Private m_bEnabledFlag As Boolean
    Private m_vFormName As String
    Private m_vPDFPath As String
    Private m_vServiceChannel As String
    Private m_vSQLProcedure As String
    Private m_dtSysLastUpdatedDate As DateTime

    Private m_sErrors As String

    Public Property FormSupplementID() As Integer
        Get
            Return m_iFormSupplementID
        End Get
        Set(ByVal Value As Integer)
            m_iFormSupplementID = Value
        End Set
    End Property

    Public Property FormType() As String
        Get
            Return m_vFormType
        End Get
        Set(ByVal Value As String)
            m_vFormType = Value
        End Set
    End Property

    Public Property ShopStateCode() As String
        Get
            Return m_vShopStateCode
        End Get
        Set(ByVal Value As String)
            m_vShopStateCode = Value
        End Set
    End Property

    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property

    Public Property FormName() As String
        Get
            Return m_vFormName
        End Get
        Set(ByVal Value As String)
            m_vFormName = Value
        End Set
    End Property

    Public Property PDFPath() As String
        Get
            Return m_vPDFPath
        End Get
        Set(ByVal Value As String)
            m_vPDFPath = Value
        End Set
    End Property

    Public Property ServiceChannel() As String
        Get
            Return m_vServiceChannel
        End Get
        Set(ByVal Value As String)
            m_vServiceChannel = Value
        End Set
    End Property

    Public Property SQLProcedure() As String
        Get
            Return m_vSQLProcedure
        End Get
        Set(ByVal Value As String)
            m_vSQLProcedure = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class
