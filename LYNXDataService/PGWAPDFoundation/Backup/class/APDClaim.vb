﻿Imports System.Xml.Serialization

Public Class APDClaim
    Private m_sLynxID As String
    Private m_sNewClaimFlag As String
    Private m_sNoticeMethodID As String
    Private m_sPolicyNumber As String
    Private m_sDataSource As String
    Private m_sFNOLUserID As String
    Private m_sInsuranceCompanyID As String
    Private m_sAssignmentAtSelectionFlag As String
    Private m_sDemoFlag As String
    Private m_sAssignmentDescription As String
    Private m_sCarrierOfficeName As String
    Private m_sCarrierRepNameFirst As String
    Private m_sCarrierRepNameLast As String
    Private m_sCarrierRepUserID As String
    Private m_sCarrierRepPhoneDay As String
    Private m_sCarrierRepEmailAddress As String
    Private m_sCarrierName As String
    Private m_sCarrierRepLogin As String
    Private m_sClientOfficeID As String
    Private m_sCoverageClaimNumber As String
    Private m_sLossDate As String
    Private m_sLossAddressCity As String
    Private m_sLossAddressState As String
    Private m_sLossAddressStreet As String
    Private m_sCallerNameFirst As String
    Private m_sCallerNameLast As String
    Private m_sCallerAddress1 As String
    Private m_sCallerAddressCity As String
    Private m_sCallerAddressState As String
    Private m_sCallerAddressZip As String
    Private m_sCallerPhoneNight As String
    Private m_sCallerRelationToInsuredID As String
    Private m_sCallerRelationToInsuredIDDescription As String
    Private m_sContactNameFirst As String
    Private m_sContactNameLast As String
    Private m_sContactAddress1 As String
    Private m_sContactAddressCity As String
    Private m_sContactAddressState As String
    Private m_sContactAddressZip As String
    Private m_sContactBestPhoneCode As String
    Private m_sContactBestTimeToCall As String
    Private m_sContactPhoneNight As String
    Private m_sContactRelationToInsuredID As String
    Private m_sInsuredNameFirst As String
    Private m_sInsuredNameLast As String
    Private m_sInsuredAddress1 As String
    Private m_sInsuredAddressCity As String
    Private m_sInsuredAddressState As String
    Private m_sInsuredAddressZip As String
    Private m_sInsuredPhoneDay As String
    Private m_sInsuredPhoneNight As String
    Private m_sInsuredPhone As String
    Private m_sInsuredBusinessName As String
    Private m_sCollisionDeductibleAmt As String
    Private m_sCollisionLimitAmt As String
    Private m_sComprehensiveDeductibleAmt As String
    Private m_sComprehensiveLimitAmt As String
    Private m_sLiabilityDeductibleAmt As String
    Private m_sLiabilityLimitAmt As String
    Private m_sUnderInsuredDeductibleAmt As String
    Private m_sUnderInsuredLimitAmt As String
    Private m_sUnInsuredDeductibleAmt As String
    Private m_sUnInsuredLimitAmt As String
    Private m_sTimeStarted As String
    Private m_sTimeFinished As String
    Private m_sIntakeStartSeconds As String
    Private m_sIntakeEndSeconds As String
    Private m_sIntakeSeconds As String
    Private m_sInsuredName As String
    Private m_sInsuredPhoneSumm As String
    Private m_sRentalAuthorized As String
    Private m_sRentalDays As String
    Private m_sRentalDayAmount As String
    Private m_sRentalMaxAmount As String
    Private m_sRentalDaysAuthorized As String
    Private m_sMileage As String
    Private m_sLossDescription As String
    Private m_sRemarks As String
    Private m_sSourceApplicationCD As String
    Private m_sRentalInstructions As String
    Private m_sRepairReferral As String
    Private m_sSourceApplicationPassThruData As String
    Private m_sTripPurpose As String


    Private m_sErrors As String

    Public Property LynxID() As String
        Get
            Return m_sLynxID
        End Get
        Set(ByVal Value As String)
            m_sLynxID = Value
        End Set
    End Property

    Public Property NewClaimFlag() As String
        Get
            Return m_sNewClaimFlag
        End Get
        Set(ByVal Value As String)
            m_sNewClaimFlag = Value
        End Set
    End Property

    Public Property NoticeMethodID() As String
        Get
            Return m_sNoticeMethodID
        End Get
        Set(ByVal Value As String)
            m_sNoticeMethodID = Value
        End Set
    End Property

    Public Property PolicyNumber() As String
        Get
            Return m_sPolicyNumber
        End Get
        Set(ByVal Value As String)
            m_sPolicyNumber = Value
        End Set
    End Property

    Public Property DataSource() As String
        Get
            Return m_sDataSource
        End Get
        Set(ByVal Value As String)
            m_sDataSource = Value
        End Set
    End Property

    Public Property FNOLUserID() As String
        Get
            Return m_sFNOLUserID
        End Get
        Set(ByVal Value As String)
            m_sFNOLUserID = Value
        End Set
    End Property

    Public Property InsuranceCompanyID() As String
        Get
            Return m_sInsuranceCompanyID
        End Get
        Set(ByVal Value As String)
            m_sInsuranceCompanyID = Value
        End Set
    End Property

    Public Property AssignmentAtSelectionFlag() As String
        Get
            Return m_sAssignmentAtSelectionFlag
        End Get
        Set(ByVal Value As String)
            m_sAssignmentAtSelectionFlag = Value
        End Set
    End Property

    Public Property DemoFlag() As String
        Get
            Return m_sDemoFlag
        End Get
        Set(ByVal Value As String)
            m_sDemoFlag = Value
        End Set
    End Property

    Public Property AssignmentDescription() As String
        Get
            Return m_sAssignmentDescription
        End Get
        Set(ByVal Value As String)
            m_sAssignmentDescription = Value
        End Set
    End Property

    Public Property CarrierOfficeName() As String
        Get
            Return m_sCarrierOfficeName
        End Get
        Set(ByVal Value As String)
            m_sCarrierOfficeName = Value
        End Set
    End Property

    Public Property CarrierRepNameFirst() As String
        Get
            Return m_sCarrierRepNameFirst
        End Get
        Set(ByVal Value As String)
            m_sCarrierRepNameFirst = Value
        End Set
    End Property

    Public Property CarrierRepNameLast() As String
        Get
            Return m_sCarrierRepNameLast
        End Get
        Set(ByVal Value As String)
            m_sCarrierRepNameLast = Value
        End Set
    End Property

    Public Property CarrierRepUserID() As String
        Get
            Return m_sCarrierRepUserID
        End Get
        Set(ByVal Value As String)
            m_sCarrierRepUserID = Value
        End Set
    End Property

    Public Property CarrierRepPhoneDay() As String
        Get
            Return m_sCarrierRepPhoneDay
        End Get
        Set(ByVal Value As String)
            m_sCarrierRepPhoneDay = Value
        End Set
    End Property

    Public Property CarrierRepEmailAddress() As String
        Get
            Return m_sCarrierRepEmailAddress
        End Get
        Set(ByVal Value As String)
            m_sCarrierRepEmailAddress = Value
        End Set
    End Property

    Public Property CarrierName() As String
        Get
            Return m_sCarrierName
        End Get
        Set(ByVal Value As String)
            m_sCarrierName = Value
        End Set
    End Property

    Public Property CarrierRepLogin() As String
        Get
            Return m_sCarrierRepLogin
        End Get
        Set(ByVal Value As String)
            m_sCarrierRepLogin = Value
        End Set
    End Property

    Public Property ClientOfficeID() As String
        Get
            Return m_sClientOfficeID
        End Get
        Set(ByVal Value As String)
            m_sClientOfficeID = Value
        End Set
    End Property

    Public Property CoverageClaimNumber() As String
        Get
            Return m_sCoverageClaimNumber
        End Get
        Set(ByVal Value As String)
            m_sCoverageClaimNumber = Value
        End Set
    End Property

    Public Property LossDate() As String
        Get
            Return m_sLossDate
        End Get
        Set(ByVal Value As String)
            m_sLossDate = Value
        End Set
    End Property

    Public Property LossAddressCity() As String
        Get
            Return m_sLossAddressCity
        End Get
        Set(ByVal Value As String)
            m_sLossAddressCity = Value
        End Set
    End Property

    Public Property LossAddressState() As String
        Get
            Return m_sLossAddressState
        End Get
        Set(ByVal Value As String)
            m_sLossAddressState = Value
        End Set
    End Property

    Public Property LossAddressStreet() As String
        Get
            Return m_sLossAddressStreet
        End Get
        Set(ByVal Value As String)
            m_sLossAddressStreet = Value
        End Set
    End Property

    Public Property CallerNameFirst() As String
        Get
            Return m_sCallerNameFirst
        End Get
        Set(ByVal Value As String)
            m_sCallerNameFirst = Value
        End Set
    End Property

    Public Property CallerNameLast() As String
        Get
            Return m_sCallerNameLast
        End Get
        Set(ByVal Value As String)
            m_sCallerNameLast = Value
        End Set
    End Property

    Public Property CallerAddress1() As String
        Get
            Return m_sCallerAddress1
        End Get
        Set(ByVal Value As String)
            m_sCallerAddress1 = Value
        End Set
    End Property

    Public Property CallerAddressCity() As String
        Get
            Return m_sCallerAddressCity
        End Get
        Set(ByVal Value As String)
            m_sCallerAddressCity = Value
        End Set
    End Property

    Public Property CallerAddressState() As String
        Get
            Return m_sCallerAddressState
        End Get
        Set(ByVal Value As String)
            m_sCallerAddressState = Value
        End Set
    End Property

    Public Property CallerAddressZip() As String
        Get
            Return m_sCallerAddressZip
        End Get
        Set(ByVal Value As String)
            m_sCallerAddressZip = Value
        End Set
    End Property

    Public Property CallerPhoneNight() As String
        Get
            Return m_sCallerPhoneNight
        End Get
        Set(ByVal Value As String)
            m_sCallerPhoneNight = Value
        End Set
    End Property

    Public Property CallerRelationToInsuredID() As String
        Get
            Return m_sCallerRelationToInsuredID
        End Get
        Set(ByVal Value As String)
            m_sCallerRelationToInsuredID = Value
        End Set
    End Property

    Public Property CallerRelationToInsuredIDDescription() As String
        Get
            Return m_sCallerRelationToInsuredIDDescription
        End Get
        Set(ByVal Value As String)
            m_sCallerRelationToInsuredIDDescription = Value
        End Set
    End Property

    Public Property ContactNameFirst() As String
        Get
            Return m_sContactNameFirst
        End Get
        Set(ByVal Value As String)
            m_sContactNameFirst = Value
        End Set
    End Property

    Public Property ContactNameLast() As String
        Get
            Return m_sContactNameLast
        End Get
        Set(ByVal Value As String)
            m_sContactNameLast = Value
        End Set
    End Property

    Public Property ContactAddress1() As String
        Get
            Return m_sContactAddress1
        End Get
        Set(ByVal Value As String)
            m_sContactAddress1 = Value
        End Set
    End Property

    Public Property ContactAddressCity() As String
        Get
            Return m_sContactAddressCity
        End Get
        Set(ByVal Value As String)
            m_sContactAddressCity = Value
        End Set
    End Property

    Public Property ContactAddressState() As String
        Get
            Return m_sContactAddressState
        End Get
        Set(ByVal Value As String)
            m_sContactAddressState = Value
        End Set
    End Property

    Public Property ContactBestPhoneCode() As String
        Get
            Return m_sContactBestPhoneCode
        End Get
        Set(ByVal Value As String)
            m_sContactBestPhoneCode = Value
        End Set
    End Property

    Public Property ContactBestTimeToCall() As String
        Get
            Return m_sContactBestTimeToCall
        End Get
        Set(ByVal Value As String)
            m_sContactBestTimeToCall = Value
        End Set
    End Property

    Public Property ContactAddressZip() As String
        Get
            Return m_sContactAddressZip
        End Get
        Set(ByVal Value As String)
            m_sContactAddressZip = Value
        End Set
    End Property

    Public Property ContactPhoneNight() As String
        Get
            Return m_sContactPhoneNight
        End Get
        Set(ByVal Value As String)
            m_sContactPhoneNight = Value
        End Set
    End Property

    Public Property ContactRelationToInsuredID() As String
        Get
            Return m_sContactRelationToInsuredID
        End Get
        Set(ByVal Value As String)
            m_sContactRelationToInsuredID = Value
        End Set
    End Property

    Public Property InsuredNameFirst() As String
        Get
            Return m_sInsuredNameFirst
        End Get
        Set(ByVal Value As String)
            m_sInsuredNameFirst = Value
        End Set
    End Property

    Public Property InsuredNameLast() As String
        Get
            Return m_sInsuredNameLast
        End Get
        Set(ByVal Value As String)
            m_sInsuredNameLast = Value
        End Set
    End Property

    Public Property InsuredAddress1() As String
        Get
            Return m_sInsuredAddress1
        End Get
        Set(ByVal Value As String)
            m_sInsuredAddress1 = Value
        End Set
    End Property

    Public Property InsuredAddressCity() As String
        Get
            Return m_sInsuredAddressCity
        End Get
        Set(ByVal Value As String)
            m_sInsuredAddressCity = Value
        End Set
    End Property

    Public Property InsuredAddressState() As String
        Get
            Return m_sInsuredAddressState
        End Get
        Set(ByVal Value As String)
            m_sInsuredAddressState = Value
        End Set
    End Property

    Public Property InsuredAddressZip() As String
        Get
            Return m_sInsuredAddressZip
        End Get
        Set(ByVal Value As String)
            m_sInsuredAddressZip = Value
        End Set
    End Property

    Public Property InsuredPhoneDay() As String
        Get
            Return m_sInsuredPhoneDay
        End Get
        Set(ByVal Value As String)
            m_sInsuredPhoneDay = Value
        End Set
    End Property

    Public Property InsuredPhoneNight() As String
        Get
            Return m_sInsuredPhoneNight
        End Get
        Set(ByVal Value As String)
            m_sInsuredPhoneNight = Value
        End Set
    End Property

    Public Property InsuredPhone() As String
        Get
            Return m_sInsuredPhone
        End Get
        Set(ByVal Value As String)
            m_sInsuredPhone = Value
        End Set
    End Property

    Public Property InsuredBusinessName() As String
        Get
            Return m_sInsuredBusinessName
        End Get
        Set(ByVal Value As String)
            m_sInsuredBusinessName = Value
        End Set
    End Property

    Public Property CollisionDeductibleAmt() As String
        Get
            Return m_sCollisionDeductibleAmt
        End Get
        Set(ByVal Value As String)
            m_sCollisionDeductibleAmt = Value
        End Set
    End Property

    Public Property CollisionLimitAmt() As String
        Get
            Return m_sCollisionLimitAmt
        End Get
        Set(ByVal Value As String)
            m_sCollisionLimitAmt = Value
        End Set
    End Property

    Public Property ComprehensiveDeductibleAmt() As String
        Get
            Return m_sComprehensiveDeductibleAmt
        End Get
        Set(ByVal Value As String)
            m_sComprehensiveDeductibleAmt = Value
        End Set
    End Property

    Public Property ComprehensiveLimitAmt() As String
        Get
            Return m_sComprehensiveLimitAmt
        End Get
        Set(ByVal Value As String)
            m_sComprehensiveLimitAmt = Value
        End Set
    End Property

    Public Property LiabilityDeductibleAmt() As String
        Get
            Return m_sLiabilityDeductibleAmt
        End Get
        Set(ByVal Value As String)
            m_sLiabilityDeductibleAmt = Value
        End Set
    End Property

    Public Property LiabilityLimitAmt() As String
        Get
            Return m_sLiabilityLimitAmt
        End Get
        Set(ByVal Value As String)
            m_sLiabilityLimitAmt = Value
        End Set
    End Property

    Public Property UnderInsuredDeductibleAmt() As String
        Get
            Return m_sUnderInsuredDeductibleAmt
        End Get
        Set(ByVal Value As String)
            m_sUnderInsuredDeductibleAmt = Value
        End Set
    End Property

    Public Property UnderInsuredLimitAmt() As String
        Get
            Return m_sUnderInsuredLimitAmt
        End Get
        Set(ByVal Value As String)
            m_sUnderInsuredLimitAmt = Value
        End Set
    End Property

    Public Property UnInsuredDeductibleAmt() As String
        Get
            Return m_sUnInsuredDeductibleAmt
        End Get
        Set(ByVal Value As String)
            m_sUnInsuredDeductibleAmt = Value
        End Set
    End Property

    Public Property UnInsuredLimitAmt() As String
        Get
            Return m_sUnInsuredLimitAmt
        End Get
        Set(ByVal Value As String)
            m_sUnInsuredLimitAmt = Value
        End Set
    End Property

    Public Property TimeStarted() As String
        Get
            Return m_sTimeStarted
        End Get
        Set(ByVal Value As String)
            m_sTimeStarted = Value
        End Set
    End Property

    Public Property TimeFinished() As String
        Get
            Return m_sTimeFinished
        End Get
        Set(ByVal Value As String)
            m_sTimeFinished = Value
        End Set
    End Property

    Public Property IntakeStartSeconds() As String
        Get
            Return m_sIntakeStartSeconds
        End Get
        Set(ByVal Value As String)
            m_sIntakeStartSeconds = Value
        End Set
    End Property

    Public Property IntakeEndSeconds() As String
        Get
            Return m_sIntakeEndSeconds
        End Get
        Set(ByVal Value As String)
            m_sIntakeEndSeconds = Value
        End Set
    End Property

    Public Property IntakeSeconds() As String
        Get
            Return m_sIntakeSeconds
        End Get
        Set(ByVal Value As String)
            m_sIntakeSeconds = Value
        End Set
    End Property

    Public Property InsuredName() As String
        Get
            Return m_sInsuredName
        End Get
        Set(ByVal Value As String)
            m_sInsuredName = Value
        End Set
    End Property

    Public Property InsuredPhoneSumm() As String
        Get
            Return m_sInsuredPhoneSumm
        End Get
        Set(ByVal Value As String)
            m_sInsuredPhoneSumm = Value
        End Set
    End Property

    Public Property RentalAuthorized() As String
        Get
            Return m_sRentalAuthorized
        End Get
        Set(ByVal Value As String)
            m_sRentalAuthorized = Value
        End Set
    End Property

    Public Property RentalDays() As String
        Get
            Return m_sRentalDays
        End Get
        Set(ByVal Value As String)
            m_sRentalDays = Value
        End Set
    End Property

    Public Property RentalDayAmount() As String
        Get
            Return m_sRentalDayAmount
        End Get
        Set(ByVal Value As String)
            m_sRentalDayAmount = Value
        End Set
    End Property

    Public Property RentalMaxAmount() As String
        Get
            Return m_sRentalMaxAmount
        End Get
        Set(ByVal Value As String)
            m_sRentalMaxAmount = Value
        End Set
    End Property

    Public Property RentalDaysAuthorized() As String
        Get
            Return m_sRentalDaysAuthorized
        End Get
        Set(ByVal Value As String)
            m_sRentalDaysAuthorized = Value
        End Set
    End Property

    Public Property Mileage() As String
        Get
            Return m_sMileage
        End Get
        Set(ByVal Value As String)
            m_sMileage = Value
        End Set
    End Property

    Public Property LossDescription() As String
        Get
            Return m_sLossDescription
        End Get
        Set(ByVal Value As String)
            m_sLossDescription = Value
        End Set
    End Property

    Public Property Remarks() As String
        Get
            Return m_sRemarks
        End Get
        Set(ByVal Value As String)
            m_sRemarks = Value
        End Set
    End Property

    Public Property SourceApplicationCD() As String
        Get
            Return m_sSourceApplicationCD
        End Get
        Set(ByVal Value As String)
            m_sSourceApplicationCD = Value
        End Set
    End Property

    Public Property RentalInstructions() As String
        Get
            Return m_sRentalInstructions
        End Get
        Set(ByVal Value As String)
            m_sRentalInstructions = Value
        End Set
    End Property

    Public Property SourceApplicationPassThruData() As String
        Get
            Return m_sSourceApplicationPassThruData
        End Get
        Set(ByVal Value As String)
            m_sSourceApplicationPassThruData = Value
        End Set
    End Property

    Public Property TripPurpose() As String
        Get
            Return m_sTripPurpose
        End Get
        Set(ByVal Value As String)
            m_sTripPurpose = Value
        End Set
    End Property

    Public Property RepairReferral() As String
        Get
            Return m_sRepairReferral
        End Get
        Set(ByVal Value As String)
            m_sRepairReferral = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
