﻿Public Class utb_client_towing
    '----------------------------------------------
    ' Variables
    '----------------------------------------------
    Private m_iTowingID As Integer
    Private m_iInsuranceCompanyID As Integer
    Private m_iAnalystUserID As Integer
    Private m_sCusAddress As String
    Private m_sCusAltPhone As String
    Private m_sCusCity As String
    Private m_sCusEmail As String
    Private m_sCusHomePhone As String
    Private m_sCusName As String
    Private m_sCusState As String
    Private m_sCusWorkPhone As String
    Private m_sCusZip As String
    Private m_sDesAddress As String
    Private m_sDesCity As String
    Private m_sDesFacilityContactName As String
    Private m_sDesHoursOfOperation As String
    Private m_sDesPhone As String
    Private m_sDesState As String
    Private m_sDesZip As String
    Private m_bEnabledFlag As Boolean
    Private m_sLocAddress As String
    Private m_sLocCity As String
    Private m_sLocFacilityContactName As String
    Private m_sLocHoursOfOperation As String
    Private m_sLocPhone As String
    Private m_sLocState As String
    Private m_sLocZip As String
    Private m_sVehConditionDescription As String
    Private m_sVehLicensePlateNumber As String
    Private m_sVehMake As String
    Private m_sVehModel As String
    Private m_sVehVINState As String
    Private m_iVehYear As Integer
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As Date

    Private m_sErrors As String

    Public Property TowingID() As Integer
        Get
            Return m_iTowingID
        End Get

        Set(ByVal Value As Integer)
            m_iTowingID = Value
        End Set
    End Property

    Public Property InsuranceCompanyID() As Integer
        Get
            Return m_iInsuranceCompanyID
        End Get

        Set(ByVal Value As Integer)
            m_iInsuranceCompanyID = Value
        End Set
    End Property

    Public Property AnalystUserID() As Integer
        Get
            Return m_iAnalystUserID
        End Get

        Set(ByVal Value As Integer)
            m_iAnalystUserID = Value
        End Set
    End Property

    Public Property CusAddress() As String
        Get
            Return m_sCusAddress
        End Get

        Set(ByVal Value As String)
            m_sCusAddress = Value
        End Set
    End Property

    Public Property CusAltPhone() As String
        Get
            Return m_sCusAltPhone
        End Get

        Set(ByVal Value As String)
            m_sCusAltPhone = Value
        End Set
    End Property

    Public Property CusCity() As String
        Get
            Return m_sCusCity
        End Get

        Set(ByVal Value As String)
            m_sCusCity = Value
        End Set
    End Property

    Public Property CusEmail() As String
        Get
            Return m_sCusEmail
        End Get

        Set(ByVal Value As String)
            m_sCusEmail = Value
        End Set
    End Property

    Public Property CusHomePhone() As String
        Get
            Return m_sCusHomePhone
        End Get

        Set(ByVal Value As String)
            m_sCusHomePhone = Value
        End Set
    End Property

    Public Property CusName() As String
        Get
            Return m_sCusName
        End Get

        Set(ByVal Value As String)
            m_sCusName = Value
        End Set
    End Property

    Public Property CusState() As String
        Get
            Return m_sCusState
        End Get

        Set(ByVal Value As String)
            m_sCusState = Value
        End Set
    End Property

    Public Property CusWorkPhone() As String
        Get
            Return m_sCusWorkPhone
        End Get

        Set(ByVal Value As String)
            m_sCusWorkPhone = Value
        End Set
    End Property

    Public Property CusZip() As String
        Get
            Return m_sCusZip
        End Get

        Set(ByVal Value As String)
            m_sCusZip = Value
        End Set
    End Property

    Public Property DesAddress() As String
        Get
            Return m_sDesAddress
        End Get

        Set(ByVal Value As String)
            m_sDesAddress = Value
        End Set
    End Property

    Public Property DesCity() As String
        Get
            Return m_sDesCity
        End Get

        Set(ByVal Value As String)
            m_sDesCity = Value
        End Set
    End Property

    Public Property DesFacilityContactName() As String
        Get
            Return m_sDesFacilityContactName
        End Get

        Set(ByVal Value As String)
            m_sDesFacilityContactName = Value
        End Set
    End Property

    Public Property DesHoursOfOperation() As String
        Get
            Return m_sDesHoursOfOperation
        End Get

        Set(ByVal Value As String)
            m_sDesHoursOfOperation = Value
        End Set
    End Property

    Public Property DesPhone() As String
        Get
            Return m_sDesPhone
        End Get

        Set(ByVal Value As String)
            m_sDesPhone = Value
        End Set
    End Property

    Public Property DesState() As String
        Get
            Return m_sDesState
        End Get

        Set(ByVal Value As String)
            m_sDesState = Value
        End Set
    End Property

    Public Property DesZip() As String
        Get
            Return m_sDesZip
        End Get

        Set(ByVal Value As String)
            m_sDesZip = Value
        End Set
    End Property

    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get

        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property

    Public Property LocAddress() As String
        Get
            Return m_sLocAddress
        End Get

        Set(ByVal Value As String)
            m_sLocAddress = Value
        End Set
    End Property

    Public Property LocCity() As String
        Get
            Return m_sLocCity
        End Get

        Set(ByVal Value As String)
            m_sLocCity = Value
        End Set
    End Property

    Public Property LocFacilityContactName() As String
        Get
            Return m_sLocFacilityContactName
        End Get

        Set(ByVal Value As String)
            m_sLocFacilityContactName = Value
        End Set
    End Property

    Public Property LocHoursOfOperation() As String
        Get
            Return m_sLocHoursOfOperation
        End Get

        Set(ByVal Value As String)
            m_sLocHoursOfOperation = Value
        End Set
    End Property

    Public Property LocPhone() As String
        Get
            Return m_sLocPhone
        End Get

        Set(ByVal Value As String)
            m_sLocPhone = Value
        End Set
    End Property

    Public Property LocState() As String
        Get
            Return m_sLocState
        End Get

        Set(ByVal Value As String)
            m_sLocState = Value
        End Set
    End Property

    Public Property LocZip() As String
        Get
            Return m_sLocZip
        End Get

        Set(ByVal Value As String)
            m_sLocZip = Value
        End Set
    End Property

    Public Property VehConditionDescription() As String
        Get
            Return m_sVehConditionDescription
        End Get

        Set(ByVal Value As String)
            m_sVehConditionDescription = Value
        End Set
    End Property

    Public Property VehLicensePlateNumber() As String
        Get
            Return m_sVehLicensePlateNumber
        End Get

        Set(ByVal Value As String)
            m_sVehLicensePlateNumber = Value
        End Set
    End Property

    Public Property VehMake() As String
        Get
            Return m_sVehMake
        End Get

        Set(ByVal Value As String)
            m_sVehMake = Value
        End Set
    End Property

    Public Property VehModel() As String
        Get
            Return m_sVehModel
        End Get

        Set(ByVal Value As String)
            m_sVehModel = Value
        End Set
    End Property

    Public Property VehVINState() As String
        Get
            Return m_sVehVINState
        End Get

        Set(ByVal Value As String)
            m_sVehVINState = Value
        End Set
    End Property

    Public Property VehYear() As Integer
        Get
            Return m_iVehYear
        End Get

        Set(ByVal Value As Integer)
            m_iVehYear = Value
        End Set
    End Property

    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get

        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get

        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get

        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property


End Class
