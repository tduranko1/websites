﻿Public Class utb_client_claim_aspect_type
    Private m_iInsuranceCompanyID As Integer
    Private m_iClaimAspectTypeID As Integer
    Private m_vName As String
    Private m_vCode As String
    Private m_bDefaultExposureFlag As Boolean
    Private m_iDisplayOrder As Integer
    Private m_bEnabledFlag As Boolean
    Private m_bEnumeratedFlag As Boolean
    Private m_bGeneratesPertainsToFlag As Boolean
    Private m_bProfileRequiredFlag As Boolean
    Private m_bSysMaintainedFlag As Boolean
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As DateTime
    Private m_sErrors As String

    Public Property InsuranceCompanyID() As Integer
        Get
            Return m_iInsuranceCompanyID
        End Get
        Set(ByVal Value As Integer)
            m_iInsuranceCompanyID = Value
        End Set
    End Property
    Public Property ClaimAspectTypeID() As Integer
        Get
            Return m_iClaimAspectTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iClaimAspectTypeID = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return m_vName
        End Get
        Set(ByVal Value As String)
            m_vName = Value
        End Set
    End Property
    Public Property Code() As String
        Get
            Return m_vCode
        End Get
        Set(ByVal Value As String)
            m_vCode = Value
        End Set
    End Property
    Public Property DefaultExposureFlag() As Boolean
        Get
            Return m_bDefaultExposureFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bDefaultExposureFlag = Value
        End Set
    End Property
    Public Property DisplayOrder() As Integer
        Get
            Return m_iDisplayOrder
        End Get
        Set(ByVal Value As Integer)
            m_iDisplayOrder = Value
        End Set
    End Property
    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property EnumeratedFlag() As Boolean
        Get
            Return m_bEnumeratedFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnumeratedFlag = Value
        End Set
    End Property
    Public Property GeneratesPertainsToFlag() As Boolean
        Get
            Return m_bGeneratesPertainsToFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bGeneratesPertainsToFlag = Value
        End Set
    End Property
    Public Property ProfileRequiredFlag() As Boolean
        Get
            Return m_bProfileRequiredFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bProfileRequiredFlag = Value
        End Set
    End Property
    Public Property SysMaintainedFlag() As Boolean
        Get
            Return m_bSysMaintainedFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bSysMaintainedFlag = Value
        End Set
    End Property
    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property
    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As DateTime
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As DateTime)
            m_sErrors = Value
        End Set
    End Property
End Class
