﻿Public Class utb_office_simple
    Private m_iOfficeID As Integer
    Private m_bEnabledFlag As Boolean
    Private m_vOfficeName As String
    Private m_vAddress1 As String
    Private m_vAddress2 As String
    Private m_vAddressCity As String
    Private m_cAddressState As String
    Private m_vAddressZip As String
    Private m_sPhoneCombined As String
    Private m_sFaxCombined As String

    Private m_sErrors As String

    Public Property OfficeID() As Integer
        Get
            Return m_iOfficeID
        End Get
        Set(ByVal Value As Integer)
            m_iOfficeID = Value
        End Set
    End Property
    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property OfficeName() As String
        Get
            Return m_vOfficeName
        End Get
        Set(ByVal Value As String)
            m_vOfficeName = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return m_vAddress1
        End Get
        Set(ByVal Value As String)
            m_vAddress1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return m_vAddress2
        End Get
        Set(ByVal Value As String)
            m_vAddress2 = Value
        End Set
    End Property
    Public Property AddressCity() As String
        Get
            Return m_vAddressCity
        End Get
        Set(ByVal Value As String)
            m_vAddressCity = Value
        End Set
    End Property
    Public Property AddressState() As String
        Get
            Return m_cAddressState
        End Get
        Set(ByVal Value As String)
            m_cAddressState = Value
        End Set
    End Property
    Public Property AddressZip() As String
        Get
            Return m_vAddressZip
        End Get
        Set(ByVal Value As String)
            m_vAddressZip = Value
        End Set
    End Property
    Public Property PhoneCombined() As String
        Get
            Return m_sPhoneCombined
        End Get
        Set(ByVal Value As String)
            m_sPhoneCombined = Value
        End Set
    End Property
    Public Property FaxCombined() As String
        Get
            Return m_sFaxCombined
        End Get
        Set(ByVal Value As String)
            m_sFaxCombined = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
