﻿Imports System.Xml.Serialization

Public Class utb_client_webservice_config
    Private m_iWSConfigID As Integer
    Private m_iInsuranceCompanyID As Integer
    Private m_sXMLConditionalTag As String
    Private m_sCondition As String
    Private m_sXMLMappingFrom As String
    Private m_sXMLMappingTo As String
    Private m_bEnabledFlag As Boolean
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As Date

    Private m_sErrors As String

    Public Property WSConfigID() As Integer
        Get
            Return m_iWSConfigID
        End Get
        Set(ByVal Value As Integer)
            m_iWSConfigID = Value
        End Set
    End Property

    Public Property InsuranceCompanyID() As Integer
        Get
            Return m_iInsuranceCompanyID
        End Get
        Set(ByVal Value As Integer)
            m_iInsuranceCompanyID = Value
        End Set
    End Property

    Public Property XMLConditionalTag() As String
        Get
            Return m_sXMLConditionalTag
        End Get
        Set(ByVal Value As String)
            m_sXMLConditionalTag = Value
        End Set
    End Property

    Public Property Condition() As String
        Get
            Return m_sCondition
        End Get
        Set(ByVal Value As String)
            m_sCondition = Value
        End Set
    End Property

    Public Property XMLMappingFrom() As String
        Get
            Return m_sXMLMappingFrom
        End Get
        Set(ByVal Value As String)
            m_sXMLMappingFrom = Value
        End Set
    End Property

    Public Property XMLMappingTo() As String
        Get
            Return m_sXMLMappingTo
        End Get
        Set(ByVal Value As String)
            m_sXMLMappingTo = Value
        End Set
    End Property

    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property

    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As Date
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As Date)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
