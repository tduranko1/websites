﻿Public Class utb_workflow_simple
    Private m_iWorkflowID As Integer
    Private m_bIgnoreDefaultFlag As Boolean
    Private m_vTaskName As String
    Private m_vOriginatorTypeCD As String
    Private m_dtSysLastUpdatedDate As DateTime

    Private m_sErrors As String

    Public Property WorkflowID() As Integer
        Get
            Return m_iWorkflowID
        End Get
        Set(ByVal Value As Integer)
            m_iWorkflowID = Value
        End Set
    End Property
    Public Property IgnoreDefaultFlag() As Boolean
        Get
            Return m_bIgnoreDefaultFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bIgnoreDefaultFlag = Value
        End Set
    End Property

    Public Property TaskName() As String
        Get
            Return m_vTaskName
        End Get
        Set(ByVal Value As String)
            m_vTaskName = Value
        End Set
    End Property

    Public Property OriginatorTypeCD() As String
        Get
            Return m_vOriginatorTypeCD
        End Get
        Set(ByVal Value As String)
            m_vOriginatorTypeCD = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class
