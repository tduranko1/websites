﻿Public Class utb_client_report
    Private m_iClientReportID As Integer
    Private m_iInsuranceCompanyID As Integer
    Private m_iOfficeID As Integer
    Private m_iReportID As Integer
    Private m_vDescription As String
    Private m_iDisplayOrder As Integer
    Private m_bEnabledFlag As Boolean
    Private m_vFileName As String
    Private m_bOfficeLevelUserViewFlag As Boolean
    Private m_vReportTypeCD As String
    Private m_vSPName As String
    Private m_vStaticParameters As String
    Private m_bSysMaintainedFlag As Boolean

    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As DateTime

    Private m_sErrors As String

    Public Property ClientReportID() As Integer
        Get
            Return m_iClientReportID
        End Get
        Set(ByVal Value As Integer)
            m_iClientReportID = Value
        End Set
    End Property
    Public Property InsuranceCompanyID() As Integer
        Get
            Return m_iInsuranceCompanyID
        End Get
        Set(ByVal Value As Integer)
            m_iInsuranceCompanyID = Value
        End Set
    End Property
    Public Property OfficeID() As Integer
        Get
            Return m_iOfficeID
        End Get
        Set(ByVal Value As Integer)
            m_iOfficeID = Value
        End Set
    End Property
    Public Property ReportID() As Integer
        Get
            Return m_iReportID
        End Get
        Set(ByVal Value As Integer)
            m_iReportID = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return m_vDescription
        End Get
        Set(ByVal Value As String)
            m_vDescription = Value
        End Set
    End Property
    Public Property DisplayOrder() As Integer
        Get
            Return m_iDisplayOrder
        End Get
        Set(ByVal Value As Integer)
            m_iDisplayOrder = Value
        End Set
    End Property
    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property FileName() As String
        Get
            Return m_vFileName
        End Get
        Set(ByVal Value As String)
            m_vFileName = Value
        End Set
    End Property
    Public Property OfficeLevelUserViewFlag() As Boolean
        Get
            Return m_bOfficeLevelUserViewFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bOfficeLevelUserViewFlag = Value
        End Set
    End Property
    Public Property ReportTypeCD() As String
        Get
            Return m_vReportTypeCD
        End Get
        Set(ByVal Value As String)
            m_vReportTypeCD = Value
        End Set
    End Property
    Public Property SPName() As String
        Get
            Return m_vSPName
        End Get
        Set(ByVal Value As String)
            m_vSPName = Value
        End Set
    End Property
    Public Property StaticParameters() As String
        Get
            Return m_vStaticParameters
        End Get
        Set(ByVal Value As String)
            m_vStaticParameters = Value
        End Set
    End Property
    Public Property SysMaintainedFlag() As Boolean
        Get
            Return m_bSysMaintainedFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bSysMaintainedFlag = Value
        End Set
    End Property
    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property
    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As DateTime
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As DateTime)
            m_sErrors = Value
        End Set
    End Property
End Class
