﻿Public Class utb_client_coverage_type
    Private m_iClientCoverageTypeID As Integer
    Private m_iInsuranceCompanyID As Integer
    Private m_bAdditionalCoverageFlag As Boolean
    Private m_vClientCode As String
    Private m_vCoverageProfileCD As String
    Private m_iDisplayOrder As Integer
    Private m_bEnabledFlag As Boolean
    Private m_vName As String
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As DateTime
    Private m_sErrors As String

    Public Property ClientCoverageTypeID() As Integer
        Get
            Return m_iClientCoverageTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iClientCoverageTypeID = Value
        End Set
    End Property
    Public Property InsuranceCompanyID() As Integer
        Get
            Return m_iInsuranceCompanyID
        End Get
        Set(ByVal Value As Integer)
            m_iInsuranceCompanyID = Value
        End Set
    End Property
    Public Property AdditionalCoverageFlag() As Boolean
        Get
            Return m_bAdditionalCoverageFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bAdditionalCoverageFlag = Value
        End Set
    End Property
    Public Property ClientCode() As String
        Get
            Return m_vClientCode
        End Get
        Set(ByVal Value As String)
            m_vClientCode = Value
        End Set
    End Property
    Public Property CoverageProfileCD() As String
        Get
            Return m_vCoverageProfileCD
        End Get
        Set(ByVal Value As String)
            m_vCoverageProfileCD = Value
        End Set
    End Property
    Public Property DisplayOrder() As Integer
        Get
            Return m_iDisplayOrder
        End Get
        Set(ByVal Value As Integer)
            m_iDisplayOrder = Value
        End Set
    End Property
    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return m_vName
        End Get
        Set(ByVal Value As String)
            m_vName = Value
        End Set
    End Property
    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property
    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property
    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class
