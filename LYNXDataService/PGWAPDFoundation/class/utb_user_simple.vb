﻿Public Class utb_user_simple
    Private m_iUserID As Integer
    Private m_iOfficeID As Integer
    Private m_iSupervisorUserID As Integer
    Private m_vClientUserId As String
    Private m_vEmailAddress As String
    Private m_bEnabledFlag As Boolean
    Private m_vNameFirst As String
    Private m_vNameLast As String
    Private m_vNameTitle As String
    Private m_bSupervisorFlag As Boolean
    Private m_dtSysLastUpdatedDate As DateTime
    Private m_vPhoneCombined As String
    Private m_vFaxCombined As String

    Private m_sErrors As String

    Public Property UserID() As Integer
        Get
            Return m_iUserID
        End Get
        Set(ByVal Value As Integer)
            m_iUserID = Value
        End Set
    End Property
    Public Property OfficeID() As Integer
        Get
            Return m_iOfficeID
        End Get
        Set(ByVal Value As Integer)
            m_iOfficeID = Value
        End Set
    End Property
    Public Property SupervisorUserID() As Integer
        Get
            Return m_iSupervisorUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSupervisorUserID = Value
        End Set
    End Property
    Public Property ClientUserId() As String
        Get
            Return m_vClientUserId
        End Get
        Set(ByVal Value As String)
            m_vClientUserId = Value
        End Set
    End Property
    Public Property EmailAddress() As String
        Get
            Return m_vEmailAddress
        End Get
        Set(ByVal Value As String)
            m_vEmailAddress = Value
        End Set
    End Property
    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property NameFirst() As String
        Get
            Return m_vNameFirst
        End Get
        Set(ByVal Value As String)
            m_vNameFirst = Value
        End Set
    End Property
    Public Property NameLast() As String
        Get
            Return m_vNameLast
        End Get
        Set(ByVal Value As String)
            m_vNameLast = Value
        End Set
    End Property
    Public Property NameTitle() As String
        Get
            Return m_vNameTitle
        End Get
        Set(ByVal Value As String)
            m_vNameTitle = Value
        End Set
    End Property
    Public Property SupervisorFlag() As Boolean
        Get
            Return m_bSupervisorFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bSupervisorFlag = Value
        End Set
    End Property
    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property
    Public Property PhoneCombined() As String
        Get
            Return m_vPhoneCombined
        End Get
        Set(ByVal Value As String)
            m_vPhoneCombined = Value
        End Set
    End Property
    Public Property FaxCombined() As String
        Get
            Return m_vFaxCombined
        End Get
        Set(ByVal Value As String)
            m_vFaxCombined = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class
