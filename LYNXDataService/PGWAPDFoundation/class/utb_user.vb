﻿Public Class utb_user
    Private m_iUserID As Integer
    Private m_iOfficeID As Integer
    Private m_iSupervisorUserID As Integer
    Private m_dtAssignmentBeginDate As DateTime
    Private m_dtAssignmentEndDate As DateTime
    Private m_iClientUserId As Integer
    Private m_vEmailAddress As String
    Private m_bEnabledFlag As Boolean
    Private m_vFaxAreaCode As String
    Private m_vFaxExchangeNumber As String
    Private m_vFaxExtensionNumber As String
    Private m_vFaxUnitNumber As String
    Private m_dtLastAssignmentDate As DateTime
    Private m_vNameFirst As String
    Private m_vNameLast As String
    Private m_vNameTitle As String
    Private m_dtOperatingFridayEndTime As DateTime
    Private m_dtOperatingFridayStartTime As DateTime
    Private m_dtOperatingMondayEndTime As DateTime
    Private m_dtOperatingMondayStartTime As DateTime
    Private m_dtOperatingSaturdayEndTime As DateTime
    Private m_dtOperatingSaturdayStartTime As DateTime
    Private m_dtOperatingSundayEndTime As DateTime
    Private m_dtOperatingSundayStartTime As DateTime
    Private m_dtOperatingThursdayEndTime As DateTime
    Private m_dtOperatingThursdayStartTime As DateTime
    Private m_dtOperatingTuesdayEndTime As DateTime
    Private m_dtOperatingTuesdayStartTime As DateTime
    Private m_dtOperatingWednesdayEndTime As DateTime
    Private m_dtOperatingWednesdayStartTime As DateTime
    Private m_vPhoneAreaCode As String
    Private m_vPhoneExchangeNumber As String
    Private m_vPhoneExtensionNumber As String
    Private m_vPhoneUnitNumber As String
    Private m_bSupervisorFlag As Boolean
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As DateTime
    Private m_vCCCOneCommunicationAddress As String
    Private m_bReceiveCCCOneAssignmentFlag As Boolean
    Private m_vPhoneCombined As String
    Private m_vFaxCombined As String

    Private m_sErrors As String

    Public Property UserID() As Integer
        Get
            Return m_iUserID
        End Get
        Set(ByVal Value As Integer)
            m_iUserID = Value
        End Set
    End Property
    Public Property OfficeID() As Integer
        Get
            Return m_iOfficeID
        End Get
        Set(ByVal Value As Integer)
            m_iOfficeID = Value
        End Set
    End Property
    Public Property SupervisorUserID() As Integer
        Get
            Return m_iSupervisorUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSupervisorUserID = Value
        End Set
    End Property
    Public Property AssignmentBeginDate() As DateTime
        Get
            Return m_dtAssignmentBeginDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtAssignmentBeginDate = Value
        End Set
    End Property
    Public Property AssignmentEndDate() As DateTime
        Get
            Return m_dtAssignmentEndDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtAssignmentEndDate = Value
        End Set
    End Property
    Public Property ClientUserId() As Integer
        Get
            Return m_iClientUserId
        End Get
        Set(ByVal Value As Integer)
            m_iClientUserId = Value
        End Set
    End Property
    Public Property EmailAddress() As String
        Get
            Return m_vEmailAddress
        End Get
        Set(ByVal Value As String)
            m_vEmailAddress = Value
        End Set
    End Property
    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property FaxAreaCode() As String
        Get
            Return m_vFaxAreaCode
        End Get
        Set(ByVal Value As String)
            m_vFaxAreaCode = Value
        End Set
    End Property
    Public Property FaxExchangeNumber() As String
        Get
            Return m_vFaxExchangeNumber
        End Get
        Set(ByVal Value As String)
            m_vFaxExchangeNumber = Value
        End Set
    End Property
    Public Property FaxExtensionNumber() As String
        Get
            Return m_vFaxExtensionNumber
        End Get
        Set(ByVal Value As String)
            m_vFaxExtensionNumber = Value
        End Set
    End Property
    Public Property FaxUnitNumber() As String
        Get
            Return m_vFaxUnitNumber
        End Get
        Set(ByVal Value As String)
            m_vFaxUnitNumber = Value
        End Set
    End Property
    Public Property LastAssignmentDate() As DateTime
        Get
            Return m_dtLastAssignmentDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtLastAssignmentDate = Value
        End Set
    End Property
    Public Property NameFirst() As String
        Get
            Return m_vNameFirst
        End Get
        Set(ByVal Value As String)
            m_vNameFirst = Value
        End Set
    End Property
    Public Property NameLast() As String
        Get
            Return m_vNameLast
        End Get
        Set(ByVal Value As String)
            m_vNameLast = Value
        End Set
    End Property
    Public Property NameTitle() As String
        Get
            Return m_vNameTitle
        End Get
        Set(ByVal Value As String)
            m_vNameTitle = Value
        End Set
    End Property
    Public Property OperatingFridayEndTime() As DateTime
        Get
            Return m_dtOperatingFridayEndTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingFridayEndTime = Value
        End Set
    End Property
    Public Property OperatingFridayStartTime() As DateTime
        Get
            Return m_dtOperatingFridayStartTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingFridayStartTime = Value
        End Set
    End Property
    Public Property OperatingMondayEndTime() As DateTime
        Get
            Return m_dtOperatingMondayEndTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingMondayEndTime = Value
        End Set
    End Property
    Public Property OperatingMondayStartTime() As DateTime
        Get
            Return m_dtOperatingMondayStartTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingMondayStartTime = Value
        End Set
    End Property
    Public Property OperatingSaturdayEndTime() As DateTime
        Get
            Return m_dtOperatingSaturdayEndTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingSaturdayEndTime = Value
        End Set
    End Property
    Public Property OperatingSaturdayStartTime() As DateTime
        Get
            Return m_dtOperatingSaturdayStartTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingSaturdayStartTime = Value
        End Set
    End Property
    Public Property OperatingSundayEndTime() As DateTime
        Get
            Return m_dtOperatingSundayEndTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingSundayEndTime = Value
        End Set
    End Property
    Public Property OperatingSundayStartTime() As DateTime
        Get
            Return m_dtOperatingSundayStartTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingSundayStartTime = Value
        End Set
    End Property
    Public Property OperatingThursdayEndTime() As DateTime
        Get
            Return m_dtOperatingThursdayEndTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingThursdayEndTime = Value
        End Set
    End Property
    Public Property OperatingThursdayStartTime() As DateTime
        Get
            Return m_dtOperatingThursdayStartTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingThursdayStartTime = Value
        End Set
    End Property
    Public Property OperatingTuesdayEndTime() As DateTime
        Get
            Return m_dtOperatingTuesdayEndTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingTuesdayEndTime = Value
        End Set
    End Property
    Public Property OperatingTuesdayStartTime() As DateTime
        Get
            Return m_dtOperatingTuesdayStartTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingTuesdayStartTime = Value
        End Set
    End Property
    Public Property OperatingWednesdayEndTime() As DateTime
        Get
            Return m_dtOperatingWednesdayEndTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingWednesdayEndTime = Value
        End Set
    End Property
    Public Property OperatingWednesdayStartTime() As DateTime
        Get
            Return m_dtOperatingWednesdayStartTime
        End Get
        Set(ByVal Value As DateTime)
            m_dtOperatingWednesdayStartTime = Value
        End Set
    End Property
    Public Property PhoneAreaCode() As String
        Get
            Return m_vPhoneAreaCode
        End Get
        Set(ByVal Value As String)
            m_vPhoneAreaCode = Value
        End Set
    End Property
    Public Property PhoneExchangeNumber() As String
        Get
            Return m_vPhoneExchangeNumber
        End Get
        Set(ByVal Value As String)
            m_vPhoneExchangeNumber = Value
        End Set
    End Property
    Public Property PhoneExtensionNumber() As String
        Get
            Return m_vPhoneExtensionNumber
        End Get
        Set(ByVal Value As String)
            m_vPhoneExtensionNumber = Value
        End Set
    End Property
    Public Property PhoneUnitNumber() As String
        Get
            Return m_vPhoneUnitNumber
        End Get
        Set(ByVal Value As String)
            m_vPhoneUnitNumber = Value
        End Set
    End Property
    Public Property SupervisorFlag() As Boolean
        Get
            Return m_bSupervisorFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bSupervisorFlag = Value
        End Set
    End Property
    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property
    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property
    Public Property CCCOneCommunicationAddress() As String
        Get
            Return m_vCCCOneCommunicationAddress
        End Get
        Set(ByVal Value As String)
            m_vCCCOneCommunicationAddress = Value
        End Set
    End Property
    Public Property ReceiveCCCOneAssignmentFlag() As Boolean
        Get
            Return m_bReceiveCCCOneAssignmentFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bReceiveCCCOneAssignmentFlag = Value
        End Set
    End Property
    Public Property PhoneCombined() As String
        Get
            Return m_vPhoneCombined
        End Get
        Set(ByVal Value As String)
            m_vPhoneCombined = Value
        End Set
    End Property
    Public Property FaxCombined() As String
        Get
            Return m_vFaxCombined
        End Get
        Set(ByVal Value As String)
            m_vFaxCombined = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class
