﻿Public Class utb_office
    Private m_iOfficeID As Integer
    Private m_iInsuranceCompanyID As Integer
    Private m_cPhoneUnitNumber As String
    Private m_vReturnDocDestinationValue As String
    Private m_vReturnDocEmailAddress As String
    Private m_cReturnDocFaxAreaCode As String
    Private m_cReturnDocFaxExchangeNumber As String
    Private m_vReturnDocFaxExtensionNumber As String
    Private m_cReturnDocFaxUnitNumber As String
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As DateTime
    Private m_vAddress1 As String
    Private m_vAddress2 As String
    Private m_vAddressCity As String
    Private m_cAddressState As String
    Private m_vAddressZip As String
    Private m_vCCEmailAddress As String
    Private m_vClaimNumberFormatJS As String
    Private m_vClaimNumberValidJS As String
    Private m_vClaimNumberMsgText As String
    Private m_vClientOfficeId As String
    Private m_bEnabledFlag As Boolean
    Private m_cFaxAreaCode As String
    Private m_cFaxExchangeNumber As String
    Private m_vFaxExtensionNumber As String
    Private m_cFaxUnitNumber As String
    Private m_vMailingAddress1 As String
    Private m_vMailingAddress2 As String
    Private m_vMailingAddressCity As String
    Private m_cMailingAddressState As String
    Private m_vMailingAddressZip As String
    Private m_vOfficeName As String
    Private m_cPhoneAreaCode As String
    Private m_cPhoneExchangeNumber As String
    Private m_vPhoneExtensionNumber As String

    Private m_sPhoneCombined As String
    Private m_sFaxCombined As String
    Private m_sReturnDocFaxCombined As String

    Private m_sErrors As String

    Public Property OfficeID() As Integer
        Get
            Return m_iOfficeID
        End Get
        Set(ByVal Value As Integer)
            m_iOfficeID = Value
        End Set
    End Property
    Public Property InsuranceCompanyID() As Integer
        Get
            Return m_iInsuranceCompanyID
        End Get
        Set(ByVal Value As Integer)
            m_iInsuranceCompanyID = Value
        End Set
    End Property
    Public Property PhoneUnitNumber() As String
        Get
            Return m_cPhoneUnitNumber
        End Get
        Set(ByVal Value As String)
            m_cPhoneUnitNumber = Value
        End Set
    End Property
    Public Property ReturnDocDestinationValue() As String
        Get
            Return m_vReturnDocDestinationValue
        End Get
        Set(ByVal Value As String)
            m_vReturnDocDestinationValue = Value
        End Set
    End Property
    Public Property ReturnDocEmailAddress() As String
        Get
            Return m_vReturnDocEmailAddress
        End Get
        Set(ByVal Value As String)
            m_vReturnDocEmailAddress = Value
        End Set
    End Property
    Public Property ReturnDocFaxAreaCode() As String
        Get
            Return m_cReturnDocFaxAreaCode
        End Get
        Set(ByVal Value As String)
            m_cReturnDocFaxAreaCode = Value
        End Set
    End Property
    Public Property ReturnDocFaxExchangeNumber() As String
        Get
            Return m_cReturnDocFaxExchangeNumber
        End Get
        Set(ByVal Value As String)
            m_cReturnDocFaxExchangeNumber = Value
        End Set
    End Property
    Public Property ReturnDocFaxExtensionNumber() As String
        Get
            Return m_vReturnDocFaxExtensionNumber
        End Get
        Set(ByVal Value As String)
            m_vReturnDocFaxExtensionNumber = Value
        End Set
    End Property
    Public Property ReturnDocFaxUnitNumber() As String
        Get
            Return m_cReturnDocFaxUnitNumber
        End Get
        Set(ByVal Value As String)
            m_cReturnDocFaxUnitNumber = Value
        End Set
    End Property
    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property
    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return m_vAddress1
        End Get
        Set(ByVal Value As String)
            m_vAddress1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return m_vAddress2
        End Get
        Set(ByVal Value As String)
            m_vAddress2 = Value
        End Set
    End Property
    Public Property AddressCity() As String
        Get
            Return m_vAddressCity
        End Get
        Set(ByVal Value As String)
            m_vAddressCity = Value
        End Set
    End Property
    Public Property AddressState() As String
        Get
            Return m_cAddressState
        End Get
        Set(ByVal Value As String)
            m_cAddressState = Value
        End Set
    End Property
    Public Property AddressZip() As String
        Get
            Return m_vAddressZip
        End Get
        Set(ByVal Value As String)
            m_vAddressZip = Value
        End Set
    End Property
    Public Property CCEmailAddress() As String
        Get
            Return m_vCCEmailAddress
        End Get
        Set(ByVal Value As String)
            m_vCCEmailAddress = Value
        End Set
    End Property
    Public Property ClaimNumberFormatJS() As String
        Get
            Return m_vClaimNumberFormatJS
        End Get
        Set(ByVal Value As String)
            m_vClaimNumberFormatJS = Value
        End Set
    End Property
    Public Property ClaimNumberValidJS() As String
        Get
            Return m_vClaimNumberValidJS
        End Get
        Set(ByVal Value As String)
            m_vClaimNumberValidJS = Value
        End Set
    End Property
    Public Property ClaimNumberMsgText() As String
        Get
            Return m_vClaimNumberMsgText
        End Get
        Set(ByVal Value As String)
            m_vClaimNumberMsgText = Value
        End Set
    End Property
    Public Property ClientOfficeId() As String
        Get
            Return m_vClientOfficeId
        End Get
        Set(ByVal Value As String)
            m_vClientOfficeId = Value
        End Set
    End Property
    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property FaxAreaCode() As String
        Get
            Return m_cFaxAreaCode
        End Get
        Set(ByVal Value As String)
            m_cFaxAreaCode = Value
        End Set
    End Property
    Public Property FaxExchangeNumber() As String
        Get
            Return m_cFaxExchangeNumber
        End Get
        Set(ByVal Value As String)
            m_cFaxExchangeNumber = Value
        End Set
    End Property
    Public Property FaxExtensionNumber() As String
        Get
            Return m_vFaxExtensionNumber
        End Get
        Set(ByVal Value As String)
            m_vFaxExtensionNumber = Value
        End Set
    End Property
    Public Property FaxUnitNumber() As String
        Get
            Return m_cFaxUnitNumber
        End Get
        Set(ByVal Value As String)
            m_cFaxUnitNumber = Value
        End Set
    End Property
    Public Property MailingAddress1() As String
        Get
            Return m_vMailingAddress1
        End Get
        Set(ByVal Value As String)
            m_vMailingAddress1 = Value
        End Set
    End Property
    Public Property MailingAddress2() As String
        Get
            Return m_vMailingAddress2
        End Get
        Set(ByVal Value As String)
            m_vMailingAddress2 = Value
        End Set
    End Property
    Public Property MailingAddressCity() As String
        Get
            Return m_vMailingAddressCity
        End Get
        Set(ByVal Value As String)
            m_vMailingAddressCity = Value
        End Set
    End Property
    Public Property MailingAddressState() As String
        Get
            Return m_cMailingAddressState
        End Get
        Set(ByVal Value As String)
            m_cMailingAddressState = Value
        End Set
    End Property
    Public Property MailingAddressZip() As String
        Get
            Return m_vMailingAddressZip
        End Get
        Set(ByVal Value As String)
            m_vMailingAddressZip = Value
        End Set
    End Property
    Public Property OfficeName() As String
        Get
            Return m_vOfficeName
        End Get
        Set(ByVal Value As String)
            m_vOfficeName = Value
        End Set
    End Property
    Public Property PhoneAreaCode() As String
        Get
            Return m_cPhoneAreaCode
        End Get
        Set(ByVal Value As String)
            m_cPhoneAreaCode = Value
        End Set
    End Property
    Public Property PhoneExchangeNumber() As String
        Get
            Return m_cPhoneExchangeNumber
        End Get
        Set(ByVal Value As String)
            m_cPhoneExchangeNumber = Value
        End Set
    End Property
    Public Property PhoneExtensionNumber() As String
        Get
            Return m_vPhoneExtensionNumber
        End Get
        Set(ByVal Value As String)
            m_vPhoneExtensionNumber = Value
        End Set
    End Property
    Public Property PhoneCombined() As String
        Get
            Return m_sPhoneCombined
        End Get
        Set(ByVal Value As String)
            m_sPhoneCombined = Value
        End Set
    End Property
    Public Property FaxCombined() As String
        Get
            Return m_sFaxCombined
        End Get
        Set(ByVal Value As String)
            m_sFaxCombined = Value
        End Set
    End Property
    Public Property ReturnDocFaxCombined() As String
        Get
            Return m_sReturnDocFaxCombined
        End Get
        Set(ByVal Value As String)
            m_sReturnDocFaxCombined = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class
