﻿Public Class utb_client_report_simple
    Private m_iClientReportID As Integer
    Private m_iOfficeID As String
    Private m_vDescription As String
    Private m_bOfficeLevelUserViewFlag As Boolean

    Private m_sErrors As String

    Public Property ClientReportID() As Integer
        Get
            Return m_iClientReportID
        End Get
        Set(ByVal Value As Integer)
            m_iClientReportID = Value
        End Set
    End Property
    Public Property OfficeID() As String
        Get
            Return m_iOfficeID
        End Get
        Set(ByVal Value As String)
            m_iOfficeID = Value
        End Set
    End Property
    Public Property Description() As String
        Get
            Return m_vDescription
        End Get
        Set(ByVal Value As String)
            m_vDescription = Value
        End Set
    End Property
    Public Property OfficeLevelUserViewFlag() As Boolean
        Get
            Return m_bOfficeLevelUserViewFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bOfficeLevelUserViewFlag = Value
        End Set
    End Property
    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
