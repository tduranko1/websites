﻿Public Class APDVehicle
    Private m_sLynxID As String
    Private m_sAirBagDriverFront As String
    Private m_sAirBagDriverSide As String
    Private m_sAirBagDriverHeadliner As String
    Private m_sAirBagPassengerFront As String
    Private m_sAirBagPassengerSide As String
    Private m_sProgramShop As String
    Private m_sAssignmentTypeID As String
    Private m_sBodyStyle As String
    Private m_sExposureCD As String
    Private m_sGlassDamageFlag As String
    Private m_sGlassDispatchNumber As String
    Private m_sGlassNotDispatchedReason As String
    Private m_sGlassReferenceNumber As String
    Private m_sGlassShopAppointmentDate As String
    Private m_sGlassShopName As String
    Private m_sGlassShopPhoneNumber As String
    Private m_sCoverageProfileCD As String
    Private m_sOwnerNameFirst As String
    Private m_sOwnerNameLast As String
    Private m_sOwnerAddress1 As String
    Private m_sOwnerAddressCity As String
    Private m_sOwnerAddressState As String
    Private m_sOwnerAddressZip As String
    Private m_sOwnerPhoneAlternate As String
    Private m_sOwnerPhoneAlternateExt As String
    Private m_sOwnerPhoneDay As String
    Private m_sOwnerPhoneDayExt As String
    Private m_sOwnerPhoneNight As String
    Private m_sOwnerPhoneNightExt As String
    Private m_sOwnerBestPhoneCD As String
    Private m_sPermissionToDrive As String
    Private m_sPhysicalDamageFlag As String
    Private m_sPostedSpeed As String
    Private m_sPriorDamage As String
    Private m_sRemarks As String
    Private m_sOwnerBusinessName As String
    Private m_sContactNameFirst As String
    Private m_sContactNameLast As String
    Private m_sContactNameTitle As String
    Private m_sContactEmailAddress As String
    Private m_sContactBestPhoneCD As String
    Private m_sVIN As String
    Private m_sLicensePlateNumber As String
    Private m_sLicensePlateState As String
    Private m_sLocationAddress1 As String
    Private m_sLocationAddress2 As String
    Private m_sLocationAddressCity As String
    Private m_sLocationAddressState As String
    Private m_sLocationAddressZip As String
    Private m_sLocationName As String
    Private m_sLocationPhone As String
    Private m_sVehicleYear As String
    Private m_sMake As String
    Private m_sModel As String
    Private m_sNADAId As String
    Private m_sDrivable As String
    Private m_sPrimaryDamage As String
    Private m_sSecondaryDamage As String
    Private m_sImpactLocations As String
    Private m_sImpactSpeed As String
    Private m_sShopLocationID As String
    Private m_sShopSearchLogID As String
    Private m_sSelectedShopRank As String
    Private m_sSelectedShopScore As String
    Private m_sOwnerPhone As String
    Private m_sContactPhone As String
    Private m_sContactPhoneExt As String
    Private m_sContactAddress1 As String
    Private m_sContactAddress2 As String
    Private m_sContactAddressCity As String
    Private m_sContactAddressState As String
    Private m_sContactAddressZip As String
    Private m_sContactBestPhoneCode As String
    Private m_sContactBestTimeToCall As String
    Private m_sContactNightPhone As String
    Private m_sContactNightPhoneExt As String
    Private m_sContactAltPhone As String
    Private m_sContactAltPhoneExt As String
    Private m_sCellPhoneCarrier As String
    Private m_sContactCellPhone As String
    Private m_sOwnerPhoneSumm As String
    Private m_sContactPhoneSumm As String
    Private m_sContactNightPhoneSumm As String
    Private m_sContactAltPhoneSumm As String
    Private m_sContactCellPhoneSumm As String
    Private m_sPrefMethodUpd As String
    Private m_sShopName As String
    Private m_sShopAddress1 As String
    Private m_sShopAddress2 As String
    Private m_sShopCity As String
    Private m_sShopState As String
    Private m_sShopZip As String
    Private m_sShopPhone As String
    Private m_sShopFax As String
    Private m_sRentalAuthorized As String
    Private m_sRentalDays As String
    Private m_sRentalDayAmount As String
    Private m_sRentalMaxAmount As String
    Private m_sRentalDaysAuthorized As String
    Private m_sDeductibleAmt As String
    Private m_sLimitAmt As String
    Private m_sCoverageProfileUiCD As String
    Private m_sPrimaryDamageDescription As String
    Private m_sSecondaryDamageDescription As String
    Private m_sAssignmentTypeIDDescription As String
    Private m_sMileage As String
    Private m_sClientCoverageTypeID As String
    Private m_sColor As String
    Private m_sClientCoverageTypeDesc As String
    Private m_sVehicleNumber As String
    Private m_sShowScriptingMemoFlag As String
    Private m_sSourceApplicationPassthruDataVeh As String
    Private m_srbTypeAuthorized As String
    Private m_srbTypeEstimate As String
    Private m_sShopRemarks As String
    Private m_sDrivingDirections As String
    Private m_sRentalInstructions As String

    'Public Property XXX() As String
    '    Get
    '        Return XXX
    '    End Get
    '    Set(ByVal Value As String)
    '        XXX = Value
    '    End Set
    'End Property

    Public Property LynxID() As String
        Get
            Return m_sLynxID
        End Get
        Set(ByVal Value As String)
            m_sLynxID = Value
        End Set
    End Property

    Public Property AirBagDriverFront() As String
        Get
            Return m_sAirBagDriverFront
        End Get
        Set(ByVal Value As String)
            m_sAirBagDriverFront = Value
        End Set
    End Property

    Public Property AirBagDriverSide() As String
        Get
            Return m_sAirBagDriverSide
        End Get
        Set(ByVal Value As String)
            m_sAirBagDriverSide = Value
        End Set
    End Property

    Public Property AirBagDriverHeadliner() As String
        Get
            Return m_sAirBagDriverHeadliner
        End Get
        Set(ByVal Value As String)
            m_sAirBagDriverHeadliner = Value
        End Set
    End Property

    Public Property AirBagPassengerFront() As String
        Get
            Return m_sAirBagPassengerFront
        End Get
        Set(ByVal Value As String)
            m_sAirBagPassengerFront = Value
        End Set
    End Property

    Public Property AirBagPassengerSide() As String
        Get
            Return m_sAirBagPassengerSide
        End Get
        Set(ByVal Value As String)
            m_sAirBagPassengerSide = Value
        End Set
    End Property

    Public Property ProgramShop() As String
        Get
            Return m_sProgramShop
        End Get
        Set(ByVal Value As String)
            m_sProgramShop = Value
        End Set
    End Property

    Public Property AssignmentTypeID() As String
        Get
            Return m_sAssignmentTypeID
        End Get
        Set(ByVal Value As String)
            m_sAssignmentTypeID = Value
        End Set
    End Property

    Public Property BodyStyle() As String
        Get
            Return m_sBodyStyle
        End Get
        Set(ByVal Value As String)
            m_sBodyStyle = Value
        End Set
    End Property

    Public Property ExposureCD() As String
        Get
            Return m_sExposureCD
        End Get
        Set(ByVal Value As String)
            m_sExposureCD = Value
        End Set
    End Property

    Public Property GlassDamageFlag() As String
        Get
            Return m_sGlassDamageFlag
        End Get
        Set(ByVal Value As String)
            m_sGlassDamageFlag = Value
        End Set
    End Property

    Public Property GlassDispatchNumber() As String
        Get
            Return m_sGlassDispatchNumber
        End Get
        Set(ByVal Value As String)
            m_sGlassDispatchNumber = Value
        End Set
    End Property

    Public Property GlassNotDispatchedReason() As String
        Get
            Return m_sGlassNotDispatchedReason
        End Get
        Set(ByVal Value As String)
            m_sGlassNotDispatchedReason = Value
        End Set
    End Property

    Public Property GlassReferenceNumber() As String
        Get
            Return m_sGlassReferenceNumber
        End Get
        Set(ByVal Value As String)
            m_sGlassReferenceNumber = Value
        End Set
    End Property

    Public Property GlassShopAppointmentDate() As String
        Get
            Return m_sGlassShopAppointmentDate
        End Get
        Set(ByVal Value As String)
            m_sGlassShopAppointmentDate = Value
        End Set
    End Property

    Public Property GlassShopName() As String
        Get
            Return m_sGlassShopName
        End Get
        Set(ByVal Value As String)
            m_sGlassShopName = Value
        End Set
    End Property

    Public Property GlassShopPhoneNumber As String
        Get
            Return m_sGlassShopPhoneNumber
        End Get
        Set(ByVal Value As String)
            m_sGlassShopPhoneNumber = Value
        End Set
    End Property

    Public Property CoverageProfileCD() As String
        Get
            Return m_sCoverageProfileCD
        End Get
        Set(ByVal Value As String)
            m_sCoverageProfileCD = Value
        End Set
    End Property

    Public Property OwnerNameFirst() As String
        Get
            Return m_sOwnerNameFirst
        End Get
        Set(ByVal Value As String)
            m_sOwnerNameFirst = Value
        End Set
    End Property

    Public Property OwnerNameLast() As String
        Get
            Return m_sOwnerNameLast
        End Get
        Set(ByVal Value As String)
            m_sOwnerNameLast = Value
        End Set
    End Property

    Public Property OwnerAddress1() As String
        Get
            Return m_sOwnerAddress1
        End Get
        Set(ByVal Value As String)
            m_sOwnerAddress1 = Value
        End Set
    End Property

    Public Property OwnerAddressCity() As String
        Get
            Return m_sOwnerAddressCity
        End Get
        Set(ByVal Value As String)
            m_sOwnerAddressCity = Value
        End Set
    End Property

    Public Property OwnerAddressState() As String
        Get
            Return m_sOwnerAddressState
        End Get
        Set(ByVal Value As String)
            m_sOwnerAddressState = Value
        End Set
    End Property

    Public Property OwnerAddressZip() As String
        Get
            Return m_sOwnerAddressZip
        End Get
        Set(ByVal Value As String)
            m_sOwnerAddressZip = Value
        End Set
    End Property

    Public Property OwnerPhoneAlternate() As String
        Get
            Return m_sOwnerPhoneAlternate
        End Get
        Set(ByVal Value As String)
            m_sOwnerPhoneAlternate = Value
        End Set
    End Property

    Public Property OwnerPhoneAlternateExt() As String
        Get
            Return m_sOwnerPhoneAlternateExt
        End Get
        Set(ByVal Value As String)
            m_sOwnerPhoneAlternateExt = Value
        End Set
    End Property

    Public Property OwnerPhoneDay() As String
        Get
            Return m_sOwnerPhoneDay
        End Get
        Set(ByVal Value As String)
            m_sOwnerPhoneDay = Value
        End Set
    End Property

    Public Property OwnerPhoneDayExt() As String
        Get
            Return m_sOwnerPhoneDayExt
        End Get
        Set(ByVal Value As String)
            m_sOwnerPhoneDayExt = Value
        End Set
    End Property

    Public Property OwnerPhoneNight() As String
        Get
            Return m_sOwnerPhoneNight
        End Get
        Set(ByVal Value As String)
            m_sOwnerPhoneNight = Value
        End Set
    End Property

    Public Property OwnerPhoneNightExt() As String
        Get
            Return m_sOwnerPhoneNightExt
        End Get
        Set(ByVal Value As String)
            m_sOwnerPhoneNightExt = Value
        End Set
    End Property

    Public Property OwnerBestPhoneCD() As String
        Get
            Return m_sOwnerBestPhoneCD
        End Get
        Set(ByVal Value As String)
            m_sOwnerBestPhoneCD = Value
        End Set
    End Property

    Public Property PermissionToDrive() As String
        Get
            Return m_sPermissionToDrive
        End Get
        Set(ByVal Value As String)
            m_sPermissionToDrive = Value
        End Set
    End Property

    Public Property PhysicalDamageFlag() As String
        Get
            Return m_sPhysicalDamageFlag
        End Get
        Set(ByVal Value As String)
            m_sPhysicalDamageFlag = Value
        End Set
    End Property

    Public Property PostedSpeed() As String
        Get
            Return m_sPostedSpeed
        End Get
        Set(ByVal Value As String)
            m_sPostedSpeed = Value
        End Set
    End Property

    Public Property PriorDamage() As String
        Get
            Return m_sPriorDamage
        End Get
        Set(ByVal Value As String)
            m_sPriorDamage = Value
        End Set
    End Property

    Public Property Remarks() As String
        Get
            Return m_sRemarks
        End Get
        Set(ByVal Value As String)
            m_sRemarks = Value
        End Set
    End Property

    Public Property OwnerBusinessName() As String
        Get
            Return m_sOwnerBusinessName
        End Get
        Set(ByVal Value As String)
            m_sOwnerBusinessName = Value
        End Set
    End Property

    Public Property ContactNameFirst() As String
        Get
            Return m_sContactNameFirst
        End Get
        Set(ByVal Value As String)
            m_sContactNameFirst = Value
        End Set
    End Property

    Public Property ContactNameLast() As String
        Get
            Return m_sContactNameLast
        End Get
        Set(ByVal Value As String)
            m_sContactNameLast = Value
        End Set
    End Property

    Public Property ContactNameTitle() As String
        Get
            Return m_sContactNameTitle
        End Get
        Set(ByVal Value As String)
            m_sContactNameTitle = Value
        End Set
    End Property

    Public Property ContactAddress1() As String
        Get
            Return m_sContactAddress1
        End Get
        Set(ByVal Value As String)
            m_sContactAddress1 = Value
        End Set
    End Property

    Public Property ContactAddress2() As String
        Get
            Return m_sContactAddress2
        End Get
        Set(ByVal Value As String)
            m_sContactAddress2 = Value
        End Set
    End Property

    Public Property ContactAddressCity() As String
        Get
            Return m_sContactAddressCity
        End Get
        Set(ByVal Value As String)
            m_sContactAddressCity = Value
        End Set
    End Property

    Public Property ContactAddressState() As String
        Get
            Return m_sContactAddressState
        End Get
        Set(ByVal Value As String)
            m_sContactAddressState = Value
        End Set
    End Property

    Public Property ContactAddressZip() As String
        Get
            Return m_sContactAddressZip
        End Get
        Set(ByVal Value As String)
            m_sContactAddressZip = Value
        End Set
    End Property

    Public Property ContactBestPhoneCode() As String
        Get
            Return m_sContactBestPhoneCode
        End Get
        Set(ByVal Value As String)
            m_sContactBestPhoneCode = Value
        End Set
    End Property

    Public Property ContactBestTimeToCall() As String
        Get
            Return m_sContactBestTimeToCall
        End Get
        Set(ByVal Value As String)
            m_sContactBestTimeToCall = Value
        End Set
    End Property

    Public Property ContactEmailAddress() As String
        Get
            Return m_sContactEmailAddress
        End Get
        Set(ByVal Value As String)
            m_sContactEmailAddress = Value
        End Set
    End Property

    Public Property ContactBestPhoneCD() As String
        Get
            Return m_sContactBestPhoneCD
        End Get
        Set(ByVal Value As String)
            m_sContactBestPhoneCD = Value
        End Set
    End Property

    Public Property VIN() As String
        Get
            Return m_sVIN
        End Get
        Set(ByVal Value As String)
            m_sVIN = Value
        End Set
    End Property

    Public Property LicensePlateNumber() As String
        Get
            Return m_sLicensePlateNumber
        End Get
        Set(ByVal Value As String)
            m_sLicensePlateNumber = Value
        End Set
    End Property

    Public Property LicensePlateState() As String
        Get
            Return m_sLicensePlateState
        End Get
        Set(ByVal Value As String)
            m_sLicensePlateState = Value
        End Set
    End Property

    Public Property LocationAddress1() As String
        Get
            Return m_sLocationAddress1
        End Get
        Set(ByVal Value As String)
            m_sLocationAddress1 = Value
        End Set
    End Property

    Public Property LocationAddress2() As String
        Get
            Return m_sLocationAddress2
        End Get
        Set(ByVal Value As String)
            m_sLocationAddress2 = Value
        End Set
    End Property

    Public Property LocationAddressCity() As String
        Get
            Return m_sLocationAddressCity
        End Get
        Set(ByVal Value As String)
            m_sLocationAddressCity = Value
        End Set
    End Property

    Public Property LocationAddressState() As String
        Get
            Return m_sLocationAddressState
        End Get
        Set(ByVal Value As String)
            m_sLocationAddressState = Value
        End Set
    End Property

    Public Property LocationAddressZip() As String
        Get
            Return m_sLocationAddressZip
        End Get
        Set(ByVal Value As String)
            m_sLocationAddressZip = Value
        End Set
    End Property

    Public Property LocationName() As String
        Get
            Return m_sLocationName
        End Get
        Set(ByVal Value As String)
            m_sLocationName = Value
        End Set
    End Property

    Public Property LocationPhone() As String
        Get
            Return m_sLocationPhone
        End Get
        Set(ByVal Value As String)
            m_sLocationPhone = Value
        End Set
    End Property

    Public Property VehicleYear() As String
        Get
            Return m_sVehicleYear
        End Get
        Set(ByVal Value As String)
            m_sVehicleYear = Value
        End Set
    End Property

    Public Property Make() As String
        Get
            Return m_sMake
        End Get
        Set(ByVal Value As String)
            m_sMake = Value
        End Set
    End Property

    Public Property Model() As String
        Get
            Return m_sModel
        End Get
        Set(ByVal Value As String)
            m_sModel = Value
        End Set
    End Property

    Public Property NADAId() As String
        Get
            Return m_sNADAId
        End Get
        Set(ByVal Value As String)
            m_sNADAId = Value
        End Set
    End Property

    Public Property Drivable() As String
        Get
            Return m_sDrivable
        End Get
        Set(ByVal Value As String)
            m_sDrivable = Value
        End Set
    End Property

    Public Property PrimaryDamage() As String
        Get
            Return m_sPrimaryDamage
        End Get
        Set(ByVal Value As String)
            m_sPrimaryDamage = Value
        End Set
    End Property

    Public Property SecondaryDamage() As String
        Get
            Return m_sSecondaryDamage
        End Get
        Set(ByVal Value As String)
            m_sSecondaryDamage = Value
        End Set
    End Property

    Public Property ImpactLocations() As String
        Get
            Return m_sImpactLocations
        End Get
        Set(ByVal Value As String)
            m_sImpactLocations = Value
        End Set
    End Property

    Public Property ImpactSpeed() As String
        Get
            Return m_sImpactSpeed
        End Get
        Set(ByVal Value As String)
            m_sImpactSpeed = Value
        End Set
    End Property

    Public Property ShopLocationID() As String
        Get
            Return m_sShopLocationID
        End Get
        Set(ByVal Value As String)
            m_sShopLocationID = Value
        End Set
    End Property

    Public Property ShopSearchLogID() As String
        Get
            Return m_sShopSearchLogID
        End Get
        Set(ByVal Value As String)
            m_sShopSearchLogID = Value
        End Set
    End Property

    Public Property SelectedShopRank() As String
        Get
            Return m_sSelectedShopRank
        End Get
        Set(ByVal Value As String)
            m_sSelectedShopRank = Value
        End Set
    End Property

    Public Property SelectedShopScore() As String
        Get
            Return m_sSelectedShopScore
        End Get
        Set(ByVal Value As String)
            m_sSelectedShopScore = Value
        End Set
    End Property

    Public Property OwnerPhone() As String
        Get
            Return m_sOwnerPhone
        End Get
        Set(ByVal Value As String)
            m_sOwnerPhone = Value
        End Set
    End Property

    Public Property ContactPhone() As String
        Get
            Return m_sContactPhone
        End Get
        Set(ByVal Value As String)
            m_sContactPhone = Value
        End Set
    End Property

    Public Property ContactPhoneExt() As String
        Get
            Return m_sContactPhoneExt
        End Get
        Set(ByVal Value As String)
            m_sContactPhoneExt = Value
        End Set
    End Property

    Public Property ContactNightPhone() As String
        Get
            Return m_sContactNightPhone
        End Get
        Set(ByVal Value As String)
            m_sContactNightPhone = Value
        End Set
    End Property

    Public Property ContactNightPhoneExt() As String
        Get
            Return m_sContactNightPhoneExt
        End Get
        Set(ByVal Value As String)
            m_sContactNightPhoneExt = Value
        End Set
    End Property

    Public Property ContactAltPhone() As String
        Get
            Return m_sContactAltPhone
        End Get
        Set(ByVal Value As String)
            m_sContactAltPhone = Value
        End Set
    End Property

    Public Property ContactAltPhoneExt() As String
        Get
            Return m_sContactAltPhoneExt
        End Get
        Set(ByVal Value As String)
            m_sContactAltPhoneExt = Value
        End Set
    End Property

    Public Property CellPhoneCarrier() As String
        Get
            Return m_sCellPhoneCarrier
        End Get
        Set(ByVal Value As String)
            m_sCellPhoneCarrier = Value
        End Set
    End Property

    Public Property ContactCellPhone() As String
        Get
            Return m_sContactCellPhone
        End Get
        Set(ByVal Value As String)
            m_sContactCellPhone = Value
        End Set
    End Property

    Public Property OwnerPhoneSumm() As String
        Get
            Return m_sOwnerPhoneSumm
        End Get
        Set(ByVal Value As String)
            m_sOwnerPhoneSumm = Value
        End Set
    End Property

    Public Property ContactPhoneSumm() As String
        Get
            Return m_sContactPhoneSumm
        End Get
        Set(ByVal Value As String)
            m_sContactPhoneSumm = Value
        End Set
    End Property

    Public Property ContactNightPhoneSumm() As String
        Get
            Return m_sContactNightPhoneSumm
        End Get
        Set(ByVal Value As String)
            m_sContactNightPhoneSumm = Value
        End Set
    End Property

    Public Property ContactAltPhoneSumm() As String
        Get
            Return m_sContactAltPhoneSumm
        End Get
        Set(ByVal Value As String)
            m_sContactAltPhoneSumm = Value
        End Set
    End Property

    Public Property ContactCellPhoneSumm() As String
        Get
            Return m_sContactCellPhoneSumm
        End Get
        Set(ByVal Value As String)
            m_sContactCellPhoneSumm = Value
        End Set
    End Property

    Public Property PrefMethodUpd() As String
        Get
            Return m_sPrefMethodUpd
        End Get
        Set(ByVal Value As String)
            m_sPrefMethodUpd = Value
        End Set
    End Property

    Public Property ShopName() As String
        Get
            Return m_sShopName
        End Get
        Set(ByVal Value As String)
            m_sShopName = Value
        End Set
    End Property

    Public Property ShopAddress1() As String
        Get
            Return m_sShopAddress1
        End Get
        Set(ByVal Value As String)
            m_sShopAddress1 = Value
        End Set
    End Property

    Public Property ShopAddress2() As String
        Get
            Return m_sShopAddress2
        End Get
        Set(ByVal Value As String)
            m_sShopAddress2 = Value
        End Set
    End Property

    Public Property ShopCity() As String
        Get
            Return m_sShopCity
        End Get
        Set(ByVal Value As String)
            m_sShopCity = Value
        End Set
    End Property

    Public Property ShopState() As String
        Get
            Return m_sShopState
        End Get
        Set(ByVal Value As String)
            m_sShopState = Value
        End Set
    End Property

    Public Property ShopZip() As String
        Get
            Return m_sShopZip
        End Get
        Set(ByVal Value As String)
            m_sShopZip = Value
        End Set
    End Property

    Public Property ShopPhone() As String
        Get
            Return m_sShopPhone
        End Get
        Set(ByVal Value As String)
            m_sShopPhone = Value
        End Set
    End Property

    Public Property ShopFax() As String
        Get
            Return m_sShopFax
        End Get
        Set(ByVal Value As String)
            m_sShopFax = Value
        End Set
    End Property

    Public Property RentalAuthorized() As String
        Get
            Return m_sRentalAuthorized
        End Get
        Set(ByVal Value As String)
            m_sRentalAuthorized = Value
        End Set
    End Property

    Public Property RentalDays() As String
        Get
            Return m_sRentalDays
        End Get
        Set(ByVal Value As String)
            m_sRentalDays = Value
        End Set
    End Property

    Public Property RentalDayAmount() As String
        Get
            Return m_sRentalDayAmount
        End Get
        Set(ByVal Value As String)
            m_sRentalDayAmount = Value
        End Set
    End Property

    Public Property RentalMaxAmount() As String
        Get
            Return m_sRentalMaxAmount
        End Get
        Set(ByVal Value As String)
            m_sRentalMaxAmount = Value
        End Set
    End Property

    Public Property RentalDaysAuthorized() As String
        Get
            Return m_sRentalDaysAuthorized
        End Get
        Set(ByVal Value As String)
            m_sRentalDaysAuthorized = Value
        End Set
    End Property

    Public Property DeductibleAmt() As String
        Get
            Return m_sDeductibleAmt
        End Get
        Set(ByVal Value As String)
            m_sDeductibleAmt = Value
        End Set
    End Property

    Public Property LimitAmt() As String
        Get
            Return m_sLimitAmt
        End Get
        Set(ByVal Value As String)
            m_sLimitAmt = Value
        End Set
    End Property

    Public Property CoverageProfileUiCD() As String
        Get
            Return m_sCoverageProfileUiCD
        End Get
        Set(ByVal Value As String)
            m_sCoverageProfileUiCD = Value
        End Set
    End Property

    Public Property PrimaryDamageDescription() As String
        Get
            Return m_sPrimaryDamageDescription
        End Get
        Set(ByVal Value As String)
            m_sPrimaryDamageDescription = Value
        End Set
    End Property

    Public Property SecondaryDamageDescription() As String
        Get
            Return m_sSecondaryDamageDescription
        End Get
        Set(ByVal Value As String)
            m_sSecondaryDamageDescription = Value
        End Set
    End Property

    Public Property AssignmentTypeIDDescription() As String
        Get
            Return m_sAssignmentTypeIDDescription
        End Get
        Set(ByVal Value As String)
            m_sAssignmentTypeIDDescription = Value
        End Set
    End Property

    Public Property Mileage() As String
        Get
            Return m_sMileage
        End Get
        Set(ByVal Value As String)
            m_sMileage = Value
        End Set
    End Property

    Public Property ClientCoverageTypeID() As String
        Get
            Return m_sClientCoverageTypeID
        End Get
        Set(ByVal Value As String)
            m_sClientCoverageTypeID = Value
        End Set
    End Property

    Public Property Color() As String
        Get
            Return m_sColor
        End Get
        Set(ByVal Value As String)
            m_sColor = Value
        End Set
    End Property

    Public Property ClientCoverageTypeDesc() As String
        Get
            Return m_sClientCoverageTypeDesc
        End Get
        Set(ByVal Value As String)
            m_sClientCoverageTypeDesc = Value
        End Set
    End Property

    Public Property VehicleNumber As String
        Get
            Return m_sVehicleNumber
        End Get
        Set(ByVal Value As String)
            m_sVehicleNumber = Value
        End Set
    End Property

    Public Property ShowScriptingMemoFlag() As String
        Get
            Return m_sShowScriptingMemoFlag
        End Get
        Set(ByVal Value As String)
            m_sShowScriptingMemoFlag = Value
        End Set
    End Property

    Public Property SourceApplicationPassthruDataVeh() As String
        Get
            Return m_sSourceApplicationPassthruDataVeh
        End Get
        Set(ByVal Value As String)
            m_sSourceApplicationPassthruDataVeh = Value
        End Set
    End Property

    Public Property rbTypeAuthorized() As String
        Get
            Return m_srbTypeAuthorized
        End Get
        Set(ByVal Value As String)
            m_srbTypeAuthorized = Value
        End Set
    End Property

    Public Property rbTypeEstimate() As String
        Get
            Return m_srbTypeEstimate
        End Get
        Set(ByVal Value As String)
            m_srbTypeEstimate = Value
        End Set
    End Property

    Public Property ShopRemarks() As String
        Get
            Return m_sShopRemarks
        End Get
        Set(ByVal Value As String)
            m_sShopRemarks = Value
        End Set
    End Property

    Public Property DrivingDirections() As String
        Get
            Return m_sDrivingDirections
        End Get
        Set(ByVal Value As String)
            m_sDrivingDirections = Value
        End Set
    End Property

    Public Property RentalInstructions() As String
        Get
            Return m_sRentalInstructions
        End Get
        Set(ByVal Value As String)
            m_sRentalInstructions = Value
        End Set
    End Property
End Class
