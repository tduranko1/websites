﻿Public Class utb_client_assignment_type_simple
    Private m_iAssignmentTypeID As Integer
    Private m_vName As String
    Private m_vServiceChannelDefaultCD As String
    Private m_bClientAuthorizesPaymentFlag As String
    Private m_vInvoicingModelBillingCD As String

    Private m_sErrors As String

    Public Property AssignmentTypeID() As Integer
        Get
            Return m_iAssignmentTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iAssignmentTypeID = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return m_vName
        End Get
        Set(ByVal Value As String)
            m_vName = Value
        End Set
    End Property
    Public Property ServiceChannelDefaultCD() As String
        Get
            Return m_vServiceChannelDefaultCD
        End Get
        Set(ByVal Value As String)
            m_vServiceChannelDefaultCD = Value
        End Set
    End Property
    Public Property ClientAuthorizesPaymentFlag() As String
        Get
            Return m_bClientAuthorizesPaymentFlag
        End Get
        Set(ByVal Value As String)
            m_bClientAuthorizesPaymentFlag = Value
        End Set
    End Property
    Public Property InvoicingModelBillingCD() As String
        Get
            Return m_vInvoicingModelBillingCD
        End Get
        Set(ByVal Value As String)
            m_vInvoicingModelBillingCD = Value
        End Set
    End Property
    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class
