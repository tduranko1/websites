﻿Public Class utb_admin_claimpoint_menu
    Private m_iMenuID As Integer
    Private m_vMenuLevel As String
    Private m_vButtonText As String
    Private m_vNavURL As String
    Private m_vMouseOverIcon As String
    Private m_vMouseOutIcon As String
    Private m_iSortOrder As Integer
    Private m_bEnabledFlag As Boolean
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As DateTime
    Private m_sErrors As String

    Public Property MenuID() As Integer
        Get
            Return m_iMenuID
        End Get
        Set(ByVal Value As Integer)
            m_iMenuID = Value
        End Set
    End Property

    Public Property MenuLevel() As String
        Get
            Return m_vMenuLevel
        End Get
        Set(ByVal Value As String)
            m_vMenuLevel = Value
        End Set
    End Property
    Public Property ButtonText() As String
        Get
            Return m_vButtonText
        End Get
        Set(ByVal Value As String)
            m_vButtonText = Value
        End Set
    End Property
    Public Property NavURL() As String
        Get
            Return m_vNavURL
        End Get
        Set(ByVal Value As String)
            m_vNavURL = Value
        End Set
    End Property
    Public Property MouseOverIcon() As String
        Get
            Return m_vMouseOverIcon
        End Get
        Set(ByVal Value As String)
            m_vMouseOverIcon = Value
        End Set
    End Property
    Public Property MouseOutIcon() As String
        Get
            Return m_vMouseOutIcon
        End Get
        Set(ByVal Value As String)
            m_vMouseOutIcon = Value
        End Set
    End Property
    Public Property SortOrder() As Integer
        Get
            Return m_iSortOrder
        End Get
        Set(ByVal Value As Integer)
            m_iSortOrder = Value
        End Set
    End Property
    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property
    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property
    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
