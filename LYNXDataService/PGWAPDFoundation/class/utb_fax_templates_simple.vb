﻿Public Class utb_fax_templates_simple
    Private m_iFormID As Integer
    Private m_vDocType As String
    Private m_vName As String
    Private m_bEnabledFlag As Boolean
    Private m_vPDFPath As String
    Private m_vPertainsToCD As String
    Private m_vServiceChannel As String
    Private m_vSQLProcedure As String
    Private m_dtSysLastUpdatedDate As DateTime

    Private m_sErrors As String

    Public Property FormID() As Integer
        Get
            Return m_iFormID
        End Get
        Set(ByVal Value As Integer)
            m_iFormID = Value
        End Set
    End Property

    Public Property DocType() As String
        Get
            Return m_vDocType
        End Get
        Set(ByVal Value As String)
            m_vDocType = Value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return m_vName
        End Get
        Set(ByVal Value As String)
            m_vName = Value
        End Set
    End Property

    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property

    Public Property PDFPath() As String
        Get
            Return m_vPDFPath
        End Get
        Set(ByVal Value As String)
            m_vPDFPath = Value
        End Set
    End Property

    Public Property PertainsToCD() As String
        Get
            Return m_vPertainsToCD
        End Get
        Set(ByVal Value As String)
            m_vPertainsToCD = Value
        End Set
    End Property

    Public Property ServiceChannel() As String
        Get
            Return m_vServiceChannel
        End Get
        Set(ByVal Value As String)
            m_vServiceChannel = Value
        End Set
    End Property

    Public Property SQLProcedure() As String
        Get
            Return m_vSQLProcedure
        End Get
        Set(ByVal Value As String)
            m_vSQLProcedure = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class
