﻿Imports System.Xml.Serialization

Public Class utb_claim_aspect
    Private m_iClaimAspectID As Integer
    Private m_iAnalystUserID As Integer
    Private m_iClaimAspectTypeID As Integer
    Private m_iClientCoverageTypeID As Integer
    Private m_iCompletedAnalystUserID As Integer
    Private m_iCompletedOwnerUserID As Integer
    Private m_iCompletedSupportUserID As Integer
    Private m_iInitialAssignmentTypeID As Integer
    Private m_iLynxID As Integer
    Private m_iOwnerUserID As Integer
    Private m_iSourceApplicationID As Integer
    Private m_iSupportUserID As Integer
    Private m_iClaimAspectNumber As Integer
    Private m_sCoverageProfileCD As String
    Private m_dtCreatedDate As Date
    Private m_bEnabledFlag As Boolean
    Private m_sExposureCD As String
    Private m_bNewAssignmentAnalystFlag As Boolean
    Private m_bNewAssignmentOwnerFlag As Boolean
    Private m_bNewAssignmentSupportFlag As Boolean
    Private m_bPriorityFlag As Boolean
    Private m_sSourceApplicationPassThruData As String
    Private m_bWarrantyExistsFlag As Boolean
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As Date

    Private m_sErrors As String

    Public Property ClaimAspectID() As Integer
        Get
            Return m_iClaimAspectID
        End Get
        Set(ByVal Value As Integer)
            m_iClaimAspectID = Value
        End Set
    End Property

    Public Property AnalystUserID() As Integer
        Get
            Return m_iAnalystUserID
        End Get
        Set(ByVal Value As Integer)
            m_iAnalystUserID = Value
        End Set
    End Property

    Public Property ClaimAspectTypeID() As Integer
        Get
            Return m_iClaimAspectTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iClaimAspectTypeID = Value
        End Set
    End Property

    Public Property ClientCoverageTypeID() As Integer
        Get
            Return m_iClientCoverageTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iClientCoverageTypeID = Value
        End Set
    End Property

    Public Property CompletedAnalystUserID() As Integer
        Get
            Return m_iCompletedAnalystUserID
        End Get
        Set(ByVal Value As Integer)
            m_iCompletedAnalystUserID = Value
        End Set
    End Property

    Public Property CompletedOwnerUserID() As Integer
        Get
            Return m_iCompletedOwnerUserID
        End Get
        Set(ByVal Value As Integer)
            m_iCompletedOwnerUserID = Value
        End Set
    End Property

    Public Property CompletedSupportUserID() As Integer
        Get
            Return m_iCompletedSupportUserID
        End Get
        Set(ByVal Value As Integer)
            m_iCompletedSupportUserID = Value
        End Set
    End Property

    Public Property InitialAssignmentTypeID() As Integer
        Get
            Return m_iInitialAssignmentTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iInitialAssignmentTypeID = Value
        End Set
    End Property

    Public Property LynxID() As Integer
        Get
            Return m_iLynxID
        End Get
        Set(ByVal Value As Integer)
            m_iLynxID = Value
        End Set
    End Property

    Public Property OwnerUserID() As Integer
        Get
            Return m_iOwnerUserID
        End Get
        Set(ByVal Value As Integer)
            m_iOwnerUserID = Value
        End Set
    End Property

    Public Property SourceApplicationID() As Integer
        Get
            Return m_iSourceApplicationID
        End Get
        Set(ByVal Value As Integer)
            m_iSourceApplicationID = Value
        End Set
    End Property

    Public Property SupportUserID() As Integer
        Get
            Return m_iSupportUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSupportUserID = Value
        End Set
    End Property

    Public Property ClaimAspectNumber() As Integer
        Get
            Return m_iClaimAspectNumber
        End Get
        Set(ByVal Value As Integer)
            m_iClaimAspectNumber = Value
        End Set
    End Property

    Public Property CoverageProfileCD() As String
        Get
            Return m_sCoverageProfileCD
        End Get
        Set(ByVal Value As String)
            m_sCoverageProfileCD = Value
        End Set
    End Property

    Public Property CreatedDate() As Date
        Get
            Return m_dtCreatedDate
        End Get
        Set(ByVal Value As Date)
            m_dtCreatedDate = Value
        End Set
    End Property

    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property

    Public Property ExposureCD() As String
        Get
            Return m_sExposureCD
        End Get
        Set(ByVal Value As String)
            m_sExposureCD = Value
        End Set
    End Property

    Public Property NewAssignmentAnalystFlag() As Boolean
        Get
            Return m_bNewAssignmentAnalystFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bNewAssignmentAnalystFlag = Value
        End Set
    End Property

    Public Property NewAssignmentOwnerFlag() As Boolean
        Get
            Return m_bNewAssignmentOwnerFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bNewAssignmentOwnerFlag = Value
        End Set
    End Property

    Public Property NewAssignmentSupportFlag() As Boolean
        Get
            Return m_bNewAssignmentSupportFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bNewAssignmentSupportFlag = Value
        End Set
    End Property

    Public Property PriorityFlag() As Boolean
        Get
            Return m_bPriorityFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bPriorityFlag = Value
        End Set
    End Property

    Public Property SourceApplicationPassThruData() As String
        Get
            Return m_sSourceApplicationPassThruData
        End Get
        Set(ByVal Value As String)
            m_sSourceApplicationPassThruData = Value
        End Set
    End Property

    Public Property WarrantyExistsFlag() As Boolean
        Get
            Return m_bWarrantyExistsFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bWarrantyExistsFlag = Value
        End Set
    End Property


    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As Date
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As Date)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
