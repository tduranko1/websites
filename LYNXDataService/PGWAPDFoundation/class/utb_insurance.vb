﻿Public Class utb_insurance

    Private m_iInsuranceCompanyID As Integer
    Private m_iDeskAuditPreferredCommunicationMethodID As String
    Private m_vFedTaxId As String
    Private m_iIngresAccountingId As Integer
    Private m_vInvoicingModelPaymentCD As String
    Private m_vInvoiceMethodCD As String
    Private m_vLicenseDeterminationCD As String
    Private m_vName As String
    Private m_cPhoneAreaCode As String
    Private m_cPhoneExchangeNumber As String
    Private m_vPhoneExtensionNumber As String
    Private m_cPhoneUnitNumber As String
    Private m_vReturnDocDestinationCD As String
    Private m_vReturnDocPackageTypeCD As String
    Private m_vReturnDocRoutingCD As String
    Private m_dTotalLossValuationWarningPercentage As String
    Private m_dTotalLossWarningPercentage As String
    Private m_vWarrantyPeriodRefinishMinCD As String
    Private m_vWarrantyPeriodWorkmanshipMinCD As String
    Private m_iSysLastUserID As String
    Private m_dtSysLastUpdatedDate As String
    Private m_vAddress1 As String
    Private m_vAddress2 As String
    Private m_vAddressCity As String
    Private m_cAddressState As String
    Private m_vAddressZip As String
    Private m_bAssignmentAtSelectionFlag As String
    Private m_bAuthorizeLaborRatesFlag As String
    Private m_vBillingModelCD As String
    Private m_vBusinessTypeCD As String
    Private m_vCarrierLynxContactPhone As String
    Private m_bCFLogoDisplayFlag As String
    Private m_bClaimPointCarrierRepSelFlag As String
    Private m_bClaimPointDocumentUploadFlag As String
    Private m_bClientAccessFlag As String
    Private m_bDemoFlag As String
    Private m_vDeskAuditCompanyCD As String
    Private m_bDeskReviewLicenseReqFlag As String
    Private m_bEarlyBillFlag As String
    Private m_dtEarlyBillStartDate As String
    Private m_bEnabledFlag As String
    Private m_cFaxAreaCode As String
    Private m_cFaxExchangeNumber As String
    Private m_vFaxExtensionNumber As String
    Private m_cFaxUnitNumber As String

    Private m_sPhoneCombined As String
    Private m_sFaxCombined As String

    Private m_sErrors As String

    Public Property InsuranceCompanyID() As Integer
        Get
            Return m_iInsuranceCompanyID
        End Get
        Set(ByVal Value As Integer)
            m_iInsuranceCompanyID = Value
        End Set
    End Property
    Public Property DeskAuditPreferredCommunicationMethodID() As String
        Get
            Return m_iDeskAuditPreferredCommunicationMethodID
        End Get
        Set(ByVal Value As String)
            m_iDeskAuditPreferredCommunicationMethodID = Value
        End Set
    End Property
    Public Property FedTaxId() As String
        Get
            Return m_vFedTaxId
        End Get
        Set(ByVal Value As String)
            m_vFedTaxId = Value
        End Set
    End Property
    Public Property IngresAccountingId() As Integer
        Get
            Return m_iIngresAccountingId
        End Get
        Set(ByVal Value As Integer)
            m_iIngresAccountingId = Value
        End Set
    End Property
    Public Property InvoicingModelPaymentCD() As String
        Get
            Return m_vInvoicingModelPaymentCD
        End Get
        Set(ByVal Value As String)
            m_vInvoicingModelPaymentCD = Value
        End Set
    End Property
    Public Property InvoiceMethodCD() As String
        Get
            Return m_vInvoiceMethodCD
        End Get
        Set(ByVal Value As String)
            m_vInvoiceMethodCD = Value
        End Set
    End Property
    Public Property LicenseDeterminationCD() As String
        Get
            Return m_vLicenseDeterminationCD
        End Get
        Set(ByVal Value As String)
            m_vLicenseDeterminationCD = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return m_vName
        End Get
        Set(ByVal Value As String)
            m_vName = Value
        End Set
    End Property
    Public Property PhoneAreaCode() As String
        Get
            Return m_cPhoneAreaCode
        End Get
        Set(ByVal Value As String)
            m_cPhoneAreaCode = Value
        End Set
    End Property
    Public Property PhoneExchangeNumber() As String
        Get
            Return m_cPhoneExchangeNumber
        End Get
        Set(ByVal Value As String)
            m_cPhoneExchangeNumber = Value
        End Set
    End Property
    Public Property PhoneExtensionNumber() As String
        Get
            Return m_vPhoneExtensionNumber
        End Get
        Set(ByVal Value As String)
            m_vPhoneExtensionNumber = Value
        End Set
    End Property
    Public Property PhoneUnitNumber() As String
        Get
            Return m_cPhoneUnitNumber
        End Get
        Set(ByVal Value As String)
            m_cPhoneUnitNumber = Value
        End Set
    End Property
    Public Property ReturnDocDestinationCD() As String
        Get
            Return m_vReturnDocDestinationCD
        End Get
        Set(ByVal Value As String)
            m_vReturnDocDestinationCD = Value
        End Set
    End Property
    Public Property ReturnDocPackageTypeCD() As String
        Get
            Return m_vReturnDocPackageTypeCD
        End Get
        Set(ByVal Value As String)
            m_vReturnDocPackageTypeCD = Value
        End Set
    End Property
    Public Property ReturnDocRoutingCD() As String
        Get
            Return m_vReturnDocRoutingCD
        End Get
        Set(ByVal Value As String)
            m_vReturnDocRoutingCD = Value
        End Set
    End Property
    Public Property TotalLossValuationWarningPercentage() As String
        Get
            Return m_dTotalLossValuationWarningPercentage
        End Get
        Set(ByVal Value As String)
            m_dTotalLossValuationWarningPercentage = Value
        End Set
    End Property
    Public Property TotalLossWarningPercentage() As String
        Get
            Return m_dTotalLossWarningPercentage
        End Get
        Set(ByVal Value As String)
            m_dTotalLossWarningPercentage = Value
        End Set
    End Property
    Public Property WarrantyPeriodRefinishMinCD() As String
        Get
            Return m_vWarrantyPeriodRefinishMinCD
        End Get
        Set(ByVal Value As String)
            m_vWarrantyPeriodRefinishMinCD = Value
        End Set
    End Property
    Public Property WarrantyPeriodWorkmanshipMinCD() As String
        Get
            Return m_vWarrantyPeriodWorkmanshipMinCD
        End Get
        Set(ByVal Value As String)
            m_vWarrantyPeriodWorkmanshipMinCD = Value
        End Set
    End Property
    Public Property SysLastUserID() As String
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As String)
            m_iSysLastUserID = Value
        End Set
    End Property
    Public Property SysLastUpdatedDate() As String
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As String)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property
    Public Property Address1() As String
        Get
            Return m_vAddress1
        End Get
        Set(ByVal Value As String)
            m_vAddress1 = Value
        End Set
    End Property
    Public Property Address2() As String
        Get
            Return m_vAddress2
        End Get
        Set(ByVal Value As String)
            m_vAddress2 = Value
        End Set
    End Property
    Public Property AddressCity() As String
        Get
            Return m_vAddressCity
        End Get
        Set(ByVal Value As String)
            m_vAddressCity = Value
        End Set
    End Property
    Public Property AddressState() As String
        Get
            Return m_cAddressState
        End Get
        Set(ByVal Value As String)
            m_cAddressState = Value
        End Set
    End Property
    Public Property AddressZip() As String
        Get
            Return m_vAddressZip
        End Get
        Set(ByVal Value As String)
            m_vAddressZip = Value
        End Set
    End Property
    Public Property AssignmentAtSelectionFlag() As String
        Get
            Return m_bAssignmentAtSelectionFlag
        End Get
        Set(ByVal Value As String)
            m_bAssignmentAtSelectionFlag = Value
        End Set
    End Property
    Public Property AuthorizeLaborRatesFlag() As String
        Get
            Return m_bAuthorizeLaborRatesFlag
        End Get
        Set(ByVal Value As String)
            m_bAuthorizeLaborRatesFlag = Value
        End Set
    End Property
    Public Property BillingModelCD() As String
        Get
            Return m_vBillingModelCD
        End Get
        Set(ByVal Value As String)
            m_vBillingModelCD = Value
        End Set
    End Property
    Public Property BusinessTypeCD() As String
        Get
            Return m_vBusinessTypeCD
        End Get
        Set(ByVal Value As String)
            m_vBusinessTypeCD = Value
        End Set
    End Property
    Public Property CarrierLynxContactPhone() As String
        Get
            Return m_vCarrierLynxContactPhone
        End Get
        Set(ByVal Value As String)
            m_vCarrierLynxContactPhone = Value
        End Set
    End Property
    Public Property CFLogoDisplayFlag() As String
        Get
            Return m_bCFLogoDisplayFlag
        End Get
        Set(ByVal Value As String)
            m_bCFLogoDisplayFlag = Value
        End Set
    End Property
    Public Property ClaimPointCarrierRepSelFlag() As String
        Get
            Return m_bClaimPointCarrierRepSelFlag
        End Get
        Set(ByVal Value As String)
            m_bClaimPointCarrierRepSelFlag = Value
        End Set
    End Property
    Public Property ClaimPointDocumentUploadFlag() As String
        Get
            Return m_bClaimPointDocumentUploadFlag
        End Get
        Set(ByVal Value As String)
            m_bClaimPointDocumentUploadFlag = Value
        End Set
    End Property
    Public Property ClientAccessFlag() As String
        Get
            Return m_bClientAccessFlag
        End Get
        Set(ByVal Value As String)
            m_bClientAccessFlag = Value
        End Set
    End Property
    Public Property DemoFlag() As String
        Get
            Return m_bDemoFlag
        End Get
        Set(ByVal Value As String)
            m_bDemoFlag = Value
        End Set
    End Property
    Public Property DeskAuditCompanyCD() As String
        Get
            Return m_vDeskAuditCompanyCD
        End Get
        Set(ByVal Value As String)
            m_vDeskAuditCompanyCD = Value
        End Set
    End Property
    Public Property DeskReviewLicenseReqFlag() As String
        Get
            Return m_bDeskReviewLicenseReqFlag
        End Get
        Set(ByVal Value As String)
            m_bDeskReviewLicenseReqFlag = Value
        End Set
    End Property
    Public Property EarlyBillFlag() As String
        Get
            Return m_bEarlyBillFlag
        End Get
        Set(ByVal Value As String)
            m_bEarlyBillFlag = Value
        End Set
    End Property
    Public Property EarlyBillStartDate() As String
        Get
            Return m_dtEarlyBillStartDate
        End Get
        Set(ByVal Value As String)
            m_dtEarlyBillStartDate = Value
        End Set
    End Property
    Public Property EnabledFlag() As String
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As String)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property FaxAreaCode() As String
        Get
            Return m_cFaxAreaCode
        End Get
        Set(ByVal Value As String)
            m_cFaxAreaCode = Value
        End Set
    End Property
    Public Property FaxExchangeNumber() As String
        Get
            Return m_cFaxExchangeNumber
        End Get
        Set(ByVal Value As String)
            m_cFaxExchangeNumber = Value
        End Set
    End Property
    Public Property FaxExtensionNumber() As String
        Get
            Return m_vFaxExtensionNumber
        End Get
        Set(ByVal Value As String)
            m_vFaxExtensionNumber = Value
        End Set
    End Property
    Public Property FaxUnitNumber() As String
        Get
            Return m_cFaxUnitNumber
        End Get
        Set(ByVal Value As String)
            m_cFaxUnitNumber = Value
        End Set
    End Property
    Public Property PhoneCombined() As String
        Get
            Return m_sPhoneCombined
        End Get
        Set(ByVal Value As String)
            m_sPhoneCombined = Value
        End Set
    End Property
    Public Property FaxCombined() As String
        Get
            Return m_sFaxCombined
        End Get
        Set(ByVal Value As String)
            m_sFaxCombined = Value
        End Set
    End Property
    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property

End Class
