﻿Imports System.Xml.Serialization

Public Class utb_apd_event_log
    Private m_iEventID As Integer
    Private m_sEventType As String
    Private m_sEventStatus As String
    Private m_sEventDescription As String
    Private m_sEventDetailedDescription As String
    Private m_sEventXML As String
    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As Date

    Private m_sErrors As String

    Public Property EventID() As Integer
        Get
            Return m_iEventID
        End Get
        Set(ByVal Value As Integer)
            m_iEventID = Value
        End Set
    End Property

    Public Property EventType() As String
        Get
            Return m_sEventType
        End Get
        Set(ByVal Value As String)
            m_sEventType = Value
        End Set
    End Property

    Public Property EventStatus() As String
        Get
            Return m_sEventStatus
        End Get
        Set(ByVal Value As String)
            m_sEventStatus = Value
        End Set
    End Property

    Public Property EventDescription() As String
        Get
            Return m_sEventDescription
        End Get
        Set(ByVal Value As String)
            m_sEventDescription = Value
        End Set
    End Property

    Public Property EventDetailedDescription() As String
        Get
            Return m_sEventDetailedDescription
        End Get
        Set(ByVal Value As String)
            m_sEventDetailedDescription = Value
        End Set
    End Property

    Public Property EventXML() As String
        Get
            Return m_sEventXML
        End Get
        Set(ByVal Value As String)
            m_sEventXML = Value
        End Set
    End Property

    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property

    Public Property SysLastUpdatedDate() As Date
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As Date)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property

    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
