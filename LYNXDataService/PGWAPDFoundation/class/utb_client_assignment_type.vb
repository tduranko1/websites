﻿Public Class utb_client_assignment_type
    Private m_iInsuranceCompanyID As Integer
    Private m_iAssignmentTypeID As Integer
    Private m_iDisplayOrder As Integer
    Private m_bEnabledFlag As Boolean
    Private m_vName As String
    Private m_vServiceChannelDefaultCD As String
    Private m_bSysMaintainedFlag As Boolean

    Private m_iSysLastUserID As Integer
    Private m_dtSysLastUpdatedDate As DateTime

    Private m_sErrors As String

    Public Property InsuranceCompanyID() As Integer
        Get
            Return m_iInsuranceCompanyID
        End Get
        Set(ByVal Value As Integer)
            m_iInsuranceCompanyID = Value
        End Set
    End Property
    Public Property AssignmentTypeID() As Integer
        Get
            Return m_iAssignmentTypeID
        End Get
        Set(ByVal Value As Integer)
            m_iAssignmentTypeID = Value
        End Set
    End Property
    Public Property DisplayOrder() As Integer
        Get
            Return m_iDisplayOrder
        End Get
        Set(ByVal Value As Integer)
            m_iDisplayOrder = Value
        End Set
    End Property
    Public Property EnabledFlag() As Boolean
        Get
            Return m_bEnabledFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bEnabledFlag = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return m_vName
        End Get
        Set(ByVal Value As String)
            m_vName = Value
        End Set
    End Property
    Public Property ServiceChannelDefaultCD() As String
        Get
            Return m_vServiceChannelDefaultCD
        End Get
        Set(ByVal Value As String)
            m_vServiceChannelDefaultCD = Value
        End Set
    End Property
    Public Property SysMaintainedFlag() As Boolean
        Get
            Return m_bSysMaintainedFlag
        End Get
        Set(ByVal Value As Boolean)
            m_bSysMaintainedFlag = Value
        End Set
    End Property
    Public Property SysLastUserID() As Integer
        Get
            Return m_iSysLastUserID
        End Get
        Set(ByVal Value As Integer)
            m_iSysLastUserID = Value
        End Set
    End Property
    Public Property SysLastUpdatedDate() As DateTime
        Get
            Return m_dtSysLastUpdatedDate
        End Get
        Set(ByVal Value As DateTime)
            m_dtSysLastUpdatedDate = Value
        End Set
    End Property
    Public Property Errors() As String
        Get
            Return m_sErrors
        End Get
        Set(ByVal Value As String)
            m_sErrors = Value
        End Set
    End Property
End Class
