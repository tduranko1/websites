﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Configuration.ConfigurationManager
'Imports System.Net.Mail

Imports System.Xml
'Imports System.IO
'Imports System.Xml.Serialization
'Imports System.Net

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class APDPartnerService
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function Ping() As String
        Try
            'Throw New System.Exception(String.Format("Error: {0}", "WebServices Ready..."))
            Return "Server Side APDParther WebServices Ready!!!"
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            'Utility_Object.sendEmail(oExcept.Message, "Email Ping Test", AppSettings("email_to"), "", "", AppSettings("email_from"), "")
            Return "APDPartner WebServices Failed: " & oExcept.ToString
        End Try
    End Function

    '-----------------------------------------------------------
    ' APDPartner: ExecuteSpAsXML
    ' This APD Partner function executes the specified WSXML 
    ' stored procedure and returns the raw XML
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD Partner function executes the specified WSXML stored procedure and returns the raw XML")> _
    Public Function ExecuteSpAsXML(ByVal StoredProcedure As String, ByVal Parameters As String) As XmlDocument
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "EXEC " & StoredProcedure & " " & Parameters
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString_Partner")
            Conn.Open()

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Dim XmlReturnReader As XmlReader
            XmlReturnReader = cmdSelect.ExecuteXmlReader()
            XmlReturnReader.Read()

            Dim xdoc As New XmlDocument
            xdoc.Load(XmlReturnReader)
            Return xdoc
        Catch oExcept As Exception
            Dim XMLErrorDoc As New XmlDocument
            Dim FunctionName As New System.Diagnostics.StackFrame
            Dim sError As String = ""

            XMLErrorDoc.LoadXml("<ErrorHandler><Error>ERROR: Executing stored procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message & "</Error></ErrorHandler>")
            sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
            'LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & StoredProcedure, sError, " StoredProc: " & StoredProcedure & " Params: " & Parameters)

            Return XMLErrorDoc
        Finally
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APDPartner: ExecuteSpAsDS
    ' This APD Partner function executes the specified 
    ' stored procedure and returns the data as a dataset
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD Partner function executes the specified stored procedure and returns the data as a dataset")> _
    Public Function ExecuteSpAsDS(ByVal StoredProcedure As String, ByVal Parameters As String) As DataSet
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim dsReturnData As New DataSet

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "EXEC " & StoredProcedure & " " & Parameters
            cmdSelect.Connection = Conn

            Dim oReturnData As New SqlDataAdapter(cmdSelect)

            Conn.ConnectionString = AppSettings("ConnectString_Partner")
            Conn.Open()

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            oReturnData.Fill(dsReturnData, "Column")

            Return dsReturnData
        Catch oExcept As Exception
            Dim XMLErrorDoc As New XmlDocument
            Dim FunctionName As New System.Diagnostics.StackFrame
            Dim sError As String = ""

            sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
            'LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & StoredProcedure, sError, " StoredProc: " & StoredProcedure & " Params: " & Parameters)

            Dim drData As DataRow
            drData = dsReturnData.Tables(0).NewRow()
            drData("Error") = sError
            dsReturnData.Tables(0).Rows.Add(drData)
            Return dsReturnData
        Finally
            cmdSelect = Nothing
            dsReturnData = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APDPartner: ExecuteSpAsString
    ' This APDPartner function executes the specified stored procedure
    ' and returns a string value from the SP
    '-----------------------------------------------------------
    <WebMethod(Description:="This APD function executes the specified stored procedure and returns a string value from the SP")> _
    Public Function ExecuteSpAsString(ByVal StoredProcedure As String, ByVal Parameters As String) As String
        Dim Conn As New SqlConnection
        Dim cmdSelect As New SqlClient.SqlCommand
        Dim sReturnData As String = ""
        Dim ReturnDataReader As SqlClient.SqlDataReader

        Try
            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSelect.CommandType = CommandType.Text
            cmdSelect.CommandText = "EXEC " & StoredProcedure & " " & Parameters
            cmdSelect.Connection = Conn

            Conn.ConnectionString = AppSettings("ConnectString_Partner")
            Conn.Open()

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            'sReturnData = cmdSelect.ExecuteNonQuery
            ReturnDataReader = cmdSelect.ExecuteReader
            ReturnDataReader.Read()
            sReturnData = ReturnDataReader.Item(0)

            Return sReturnData
        Catch oExcept As Exception
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New System.Diagnostics.StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
            'LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & StoredProcedure, sError, " StoredProc: " & StoredProcedure & " Params: " & Parameters)

            Return sError
        Finally
            ReturnDataReader = Nothing
            cmdSelect = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APDPartner: LogPartnerEvent
    ' This APDPartner function make a log entry into the utb_partner_event_log
    ' table in the udb_partner database
    '-----------------------------------------------------------
    <WebMethod(Description:="This APDPartner function make a log entry into the utb_partner_event_log table in the udb_partner database.")> _
    Public Function LogPartnerEvent(ByVal EventTransactionID As String, ByVal EventType As String, ByVal EventStatus As String, ByVal EventDescription As String, ByVal EventDetailedDescription As String, ByVal EventXml As String) As Integer
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim Reader As SqlClient.SqlDataReader
        Dim iRecID As Integer = 0

        Try
            'SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "ERROR-DEBUG", "XML=" & EventXml & " - " & EventDescription, ConfigurationManager.AppSettings("SMTPServer"))

            '-------------------------------------------
            ' If Debug = YES then log to flat file also
            '-------------------------------------------
            If AppSettings("Debug") = "YES" Then
                '----------------------------------------
                ' Took this out for now.  Has issues
                ' with the file system being already
                ' in use
                '----------------------------------------
                'LogEventToFile(EventType, EventStatus, EventDescription, EventDetailedDescription, EventXml)
            End If

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspPartnerEventInsLogEntry"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString_Partner")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@vEventTransactionID", SqlDbType.VarChar, 100)
            cmdSQL.Parameters("@vEventTransactionID").Value = EventTransactionID

            cmdSQL.Parameters.Add("@vEventType", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vEventType").Value = EventType

            cmdSQL.Parameters.Add("@vEventStatus", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vEventStatus").Value = EventStatus

            cmdSQL.Parameters.Add("@vEventDescription", SqlDbType.VarChar, 100)
            cmdSQL.Parameters("@vEventDescription").Value = EventDescription

            cmdSQL.Parameters.Add("@vEventDetailedDescription", SqlDbType.VarChar, 1000)
            cmdSQL.Parameters("@vEventDetailedDescription").Value = EventDetailedDescription

            cmdSQL.Parameters.Add("@vEventXML", SqlDbType.Text)
            cmdSQL.Parameters("@vEventXML").Value = EventXml

            '-------------------------------------------
            ' Output parameters from the stored proc
            '-------------------------------------------
            Dim cmdOutput As New SqlParameter("@iRecID", SqlDbType.Int, 4)
            cmdOutput.Direction = ParameterDirection.Output
            cmdSQL.Parameters.Add(cmdOutput)

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            Reader = cmdSQL.ExecuteReader

            'If Reader.HasRows Then
            'Reader.Read()
            If cmdSQL.Parameters("@iRecID").Value > 0 Then
                Return cmdSQL.Parameters("@iRecID").Value
            Else
                Throw New System.Exception(String.Format("Error: {0}", "WebSerices Failed: Partner event logging failed to insert a row (LogEvent)..."))
            End If
        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Partner event logging")
            sBody = String.Format("Error: {0}", "WebSerices Failed: Partner event logging failed to insert a row (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            'SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
            Return 0
        Finally
            cmdSQL = Nothing
            Reader.Close()
            Conn.Close()
        End Try
    End Function
    ''-----------------------------------------------------------
    '' APDPartner: ExecuteSpAsAny
    '' This APD Partner function executes the specified 
    '' stored procedure and returns the data based on the
    '' ReturnDataType.  Supported:  XML, DS, STRING
    ''-----------------------------------------------------------
    '<WebMethod(Description:="This APD Partner function executes the specified WSXML stored procedure and returns the data based on the ReturnDataType.  Supported:  XML, DS, STRING")> _
    'Public Function ExecuteSpAsAny(ByVal StoredProcedure As String, ByVal Parameters As String, ByVal ReturnDataType As String) As Object
    '    Dim Conn As New SqlConnection
    '    Dim cmdSelect As New SqlClient.SqlCommand

    '    Try
    '        '-------------------------------------------
    '        ' Open connection to DB and run stored Proc
    '        '-------------------------------------------
    '        cmdSelect.CommandType = CommandType.Text
    '        cmdSelect.CommandText = "EXEC " & StoredProcedure & " " & Parameters
    '        cmdSelect.Connection = Conn

    '        Conn.ConnectionString = AppSettings("ConnectString_Partner")
    '        Conn.Open()

    '        '-------------------------------------------
    '        ' Execute the SQL
    '        '-------------------------------------------
    '        Select Case UCase(ReturnDataType)
    '            Case "XML"
    '                Dim XmlReturnReader As XmlReader
    '                XmlReturnReader = cmdSelect.ExecuteXmlReader()
    '                XmlReturnReader.Read()

    '                Dim xdoc As New XmlDocument
    '                xdoc.Load(XmlReturnReader)
    '                Return xdoc
    '            Case "DS"
    '                Dim oReturnData As New SqlDataAdapter(cmdSelect)
    '                Dim dsReturnData As New DataSet
    '                oReturnData.Fill(dsReturnData, "Column")
    '                Return dsReturnData
    '            Case "STRING"
    '                Dim sReturnData As String = ""
    '                sReturnData = cmdSelect.ExecuteNonQuery
    '                Return sReturnData
    '            Case Else
    '                ' throw exception
    '        End Select
    '    Catch oExcept As Exception
    '        Dim XMLErrorDoc As New XmlDocument
    '        Dim FunctionName As New System.Diagnostics.StackFrame
    '        Dim sError As String = ""

    '        sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
    '        'LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & StoredProcedure, sError, " StoredProc: " & StoredProcedure & " Params: " & Parameters)

    '        Select Case UCase(ReturnDataType)
    '            Case "XML"
    '                XMLErrorDoc.LoadXml("<ErrorHandler><Error>ERROR: Executing stored procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message & "</Error></ErrorHandler>")
    '                Return XMLErrorDoc
    '            Case "DS"
    '                Dim dsReturnData As New DataSet
    '                Dim drData As DataRow
    '                drData = dsReturnData.Tables(0).NewRow()
    '                drData("Error") = sError
    '                dsReturnData.Tables(0).Rows.Add(drData)
    '                Return dsReturnData
    '            Case "STRING"
    '                Return sError
    '            Case Else
    '                ' throw exception
    '        End Select
    '    Finally
    '        cmdSelect = Nothing
    '        Conn.Close()
    '    End Try
    'End Function

    ''-----------------------------------------------------------
    '' APD: ExecuteSQLCommand
    '' This APDPartner function executes direct SQL
    '' and returns a string value from the SP
    ''-----------------------------------------------------------
    '<WebMethod(Description:="This APD function executes the specified stored procedure and returns a string value from the SP")> _
    'Public Function ExecuteSpAsString(ByVal StoredProcedure As String, ByVal Parameters As String) As String
    '    Dim Conn As New SqlConnection
    '    Dim cmdSelect As New SqlClient.SqlCommand
    '    Dim sReturnData As String = ""

    '    Try
    '        '-------------------------------------------
    '        ' Open connection to DB and run stored Proc
    '        '-------------------------------------------
    '        cmdSelect.CommandType = CommandType.Text
    '        cmdSelect.CommandText = "EXEC " & StoredProcedure & " " & Parameters
    '        cmdSelect.Connection = Conn

    '        Conn.ConnectionString = AppSettings("ConnectString_Partner")
    '        Conn.Open()

    '        '-------------------------------------------
    '        ' Execute the SQL
    '        '-------------------------------------------
    '        sReturnData = cmdSelect.ExecuteNonQuery

    '        Return sReturnData
    '    Catch oExcept As Exception
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New System.Diagnostics.StackFrame

    '        sError = String.Format("Error: {0}", "WebSerices Failed: Executing store procedure: " & StoredProcedure & " (" & FunctionName.GetMethod.Name & ")...  " & oExcept.Message)
    '        'LogEvent("WSServerSideProcessor", "ERROR", "Failed executing stored procedure: " & StoredProcedure, sError, " StoredProc: " & StoredProcedure & " Params: " & Parameters)

    '        Return sError
    '    Finally
    '        cmdSelect = Nothing
    '        Conn.Close()
    '    End Try
    'End Function

    '-----------------------------------------------------------
    ' APDPartner: LogHyperquestPartnerEvent
    ' This APDPartner function make a log entry into the utb_hyperquest_event_log
    ' table in the udb_apd database
    '-----------------------------------------------------------
    <WebMethod(Description:="This APDPartner function make a log entry into the utb_hyperquest_event_log table in the udb_apd database.")> _
    Public Function LogHyperquestPartnerEvent(ByVal ProcessingServer As String, ByVal EventTransactionID As String, ByVal EventType As String, ByVal EventSeq As String, ByVal EventStatus As String, ByVal EventDescription As String, ByVal EventDetailedDescription As String, ByVal EventXml As String) As Integer
        '---------------------------------------------
        ' Log Event to the database
        '---------------------------------------------
        Dim Conn As New SqlConnection
        Dim cmdSQL As New SqlClient.SqlCommand
        Dim iRecID As Integer = 0

        Try
            'SendMail("tduranko@pgwglass.com", "tduranko@pgwglass.com", "", "ERROR-DEBUG", "XML=" & EventXml & " - " & EventDescription, ConfigurationManager.AppSettings("SMTPServer"))

            '-------------------------------------------
            ' Open connection to DB and run stored Proc
            '-------------------------------------------
            cmdSQL.CommandType = CommandType.StoredProcedure
            cmdSQL.CommandText = "uspHyperquestEventInsLogEntry"
            cmdSQL.Connection = Conn

            Conn.ConnectionString = ConfigurationManager.AppSettings("ConnectString_APD")
            Conn.Open()

            '-------------------------------------------
            ' Input Parameters
            '-------------------------------------------
            cmdSQL.Parameters.Add("@vProcessingServer", SqlDbType.VarChar, 100)
            cmdSQL.Parameters("@vProcessingServer").Value = ProcessingServer

            cmdSQL.Parameters.Add("@vEventTransactionID", SqlDbType.VarChar, 100)
            cmdSQL.Parameters("@vEventTransactionID").Value = EventTransactionID

            cmdSQL.Parameters.Add("@vEventType", SqlDbType.VarChar, 50)
            cmdSQL.Parameters("@vEventType").Value = EventType

            cmdSQL.Parameters.Add("@iEventSeq", SqlDbType.Int)
            cmdSQL.Parameters("@iEventSeq").Value = CInt(EventSeq)

            cmdSQL.Parameters.Add("@vEventStatus", SqlDbType.VarChar, 150)
            cmdSQL.Parameters("@vEventStatus").Value = EventStatus

            cmdSQL.Parameters.Add("@vEventDescription", SqlDbType.VarChar, 150)
            cmdSQL.Parameters("@vEventDescription").Value = EventDescription

            cmdSQL.Parameters.Add("@vEventDetailedDescription", SqlDbType.VarChar, 2000)
            cmdSQL.Parameters("@vEventDetailedDescription").Value = EventDetailedDescription

            cmdSQL.Parameters.Add("@vEventXML", SqlDbType.Text)
            cmdSQL.Parameters("@vEventXML").Value = EventXml

            '-------------------------------------------
            ' Output parameters from the stored proc
            '-------------------------------------------
            Dim cmdOutput As New SqlParameter("@iRecID", SqlDbType.Int, 4)
            cmdOutput.Direction = ParameterDirection.Output
            cmdSQL.Parameters.Add(cmdOutput)

            '-------------------------------------------
            ' Execute the SQL
            '-------------------------------------------
            cmdSQL.ExecuteNonQuery()

            If cmdSQL.Parameters("@iRecID").Value = 0 Then
                Return cmdSQL.Parameters("@iRecID").Value
            Else
                Throw New System.Exception(String.Format("Error: {0}", "WebSerices Failed: Hyperquest event logging failed to insert a row (LogHyperquestPartnerEvent)..."))
            End If

        Catch oExcept As Exception
            '------------------------------
            ' Email Notify of the Error
            '------------------------------
            Dim sError As String = ""
            Dim sBody As String = ""
            Dim FunctionName As New StackFrame

            sError = String.Format("Error: {0}", "WebSerices Failed: Hyperquest event logging")
            sBody = String.Format("Error: {0}", "WebSerices Failed: Hyperquest event logging failed to insert a row (" & FunctionName.GetMethod.Name & ")...  ")
            sBody += oExcept.ToString

            'SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
            Return 0
        Finally
            cmdSQL = Nothing
            Conn.Close()
        End Try
    End Function

    '-----------------------------------------------------------
    ' APDPartner: SendHyperquestDocument
    ' This APDPartner function formats and sends a AttachmentUpload
    ' to Hyperquest
    '-----------------------------------------------------------
    '<WebMethod(Description:="This APDPartner function formats and sends a AttachmentUpload to Hyperquest")> _
    'Public Function SendHyperquestDocument(ByVal ProcessingServer As String, ByVal EventTransactionID As String, ByVal EventType As String, ByVal EventSeq As String, ByVal EventStatus As String, ByVal EventDescription As String, ByVal EventDetailedDescription As String, ByVal EventXml As String) As Integer
    '    Try
    '        '------------------------------------
    '        ' Insert job details for each
    '        ' document into utb_hyperquest_jobs 
    '        ' table
    '        '------------------------------------


    '    Catch oExcept As Exception
    '        '------------------------------
    '        ' Email Notify of the Error
    '        '------------------------------
    '        Dim sError As String = ""
    '        Dim sBody As String = ""
    '        Dim FunctionName As New StackFrame

    '        sError = String.Format("Error: {0}", "WebSerices Failed: Hyperquest send document failed.")
    '        sBody = String.Format("Error: {0}", "WebSerices Failed: Hyperquest document failed to insert a row into the utb_hyperquest_jobs table. (" & FunctionName.GetMethod.Name & ")...  ")
    '        sBody += oExcept.ToString

    '        'SendMail(ConfigurationManager.AppSettings("ErrorToEmail"), ConfigurationManager.AppSettings("ErrorFromEmail"), "", sError, sBody, ConfigurationManager.AppSettings("SMTPServer"))
    '        Return 0
    '    Finally
    '    End Try
    'End Function
End Class