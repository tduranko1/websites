﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports LynxServices.Library.Data
Imports System.Net.Mail
Imports System.Xml.XPath
Imports System.Xml
Imports System.Data.SqlClient
Imports LynxServices.Library.ApplicationBlocks.Data.Ingres
Imports System.Data.Common
Imports System.IO
Imports LYNXDataService.Library
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO.Path

Imports System.Runtime.InteropServices
Imports System.Security.Principal

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://lynxdataservice/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class DataService
    Inherits System.Web.Services.WebService

    'Public Enum InstallerType
    '    Company = 0
    '    ServiceCenter = 1
    '    Technician = 2
    'End Enum

    ''' <summary>
    ''' Get Sql connection strings from config file
    ''' </summary>
    ''' <param name="connectionType">APD, LYNX</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 

    Private Function getConnection(ByVal connectionType As String) As String
        Dim connection As String = ""

        If connectionType.ToUpper.Trim = "APD" Then
            Select Case PGW.Business.Environment.getEnvironment.ToUpper
                Case "DEV"
                    connection = System.Configuration.ConfigurationManager.AppSettings("SqlServerDEV")
                Case "STG"
                    connection = System.Configuration.ConfigurationManager.AppSettings("SqlServerSTG")
                Case "PRD"
                    connection = System.Configuration.ConfigurationManager.AppSettings("SqlServerPRD")
                Case Else
                    connection = System.Configuration.ConfigurationManager.AppSettings("SqlServerDEV")
            End Select
        ElseIf connectionType.ToUpper.Trim = "LYNX" Then
            Select Case PGW.Business.Environment.getEnvironment.ToUpper
                Case "DEV"
                    connection = System.Configuration.ConfigurationManager.AppSettings("LynxSqlDev")
                Case "STG"
                    connection = System.Configuration.ConfigurationManager.AppSettings("LynxSqlStg")
                Case "PRD"
                    connection = System.Configuration.ConfigurationManager.AppSettings("LynxSqlPrd")
                    'Case Else
                    'connection = System.Configuration.ConfigurationManager.AppSettings("LynxSqlDev")
            End Select
        ElseIf connectionType.ToUpper.Trim = "INGRES" Then
            Select Case PGW.Business.Environment.getEnvironment.ToUpper
                Case "DEV"
                    connection = System.Configuration.ConfigurationManager.AppSettings("IngresDevdb")
                Case "STG"
                    connection = System.Configuration.ConfigurationManager.AppSettings("IngresStgdb")
                Case "PRD"
                    connection = System.Configuration.ConfigurationManager.AppSettings("IngresPrddb")
                Case Else
                    connection = System.Configuration.ConfigurationManager.AppSettings("IngresDevdb")
            End Select
        End If

        Return connection
    End Function

    Private Function getMailHost() As String
        Dim mailHost As String = ""

        Select Case PGW.Business.Environment.getEnvironment.ToUpper
            Case "DEV"
                mailHost = System.Configuration.ConfigurationManager.AppSettings("DevSmtpServer")
            Case "STG"
                mailHost = System.Configuration.ConfigurationManager.AppSettings("StgSmtpServer")
            Case "PRD"
                mailHost = System.Configuration.ConfigurationManager.AppSettings("PrdSmtpServer")
            Case Else
                mailHost = System.Configuration.ConfigurationManager.AppSettings("TestServer")
        End Select

        Return mailHost
    End Function

    Private Function GetEnvironment() As String
        Dim environment As String = String.Empty
        Dim returnValue As String

        environment = PGW.Business.Environment.getEnvironment().ToUpper()

        Select Case environment
            Case "DEV"
                returnValue = "DEV"
            Case "STG", "STAGE"
                returnValue = "STAGE"
            Case "PRD", "PROD"
                returnValue = "PROD"
            Case "DR"
                returnValue = "DR"
            Case Else
                returnValue = "LOCAL"
        End Select

        Return returnValue
    End Function

    Private Function GetUrl() As String
        Dim url As String = String.Empty

        Select Case GetEnvironment()
            Case "DEV", "LOCAL"
                url = ConfigurationManager.AppSettings("LYNXWebService - Dev")
            Case "STAGE"
                url = ConfigurationManager.AppSettings("LYNXWebService - Stage")
            Case "PROD"
                url = ConfigurationManager.AppSettings("LYNXWebService - Prod")
            Case "DR"
                url = ConfigurationManager.AppSettings("LYNXWebService - DR")
        End Select

        Return url
    End Function

    Public Function GetApplicationLogInstance() As LYNXWebServices.ApplicationLog
        Dim appLog As New LYNXWebServices.ApplicationLog()

        appLog.Url = GetUrl() & "ApplicationLog.asmx"
        appLog.Timeout = 300000

        Return appLog
    End Function

    <WebMethod()> _
    Public Function ExecuteDataSetWithoutParms(ByVal connectionType As String, ByVal command As String, ByVal cmdType As Integer) As DataSet
        Dim connectionString As String = getConnection(connectionType)
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim ds As DataSet = dataLayer.ExecuteDataSet(command, cmdType)
        Return ds
    End Function

    <WebMethod()> _
    Public Function ExecuteDataSetWithParms(ByVal connectionType As String, ByVal command As String, ByVal cmdType As Integer, ByVal xmlParams As String) As DataSet
        Try
            Dim connectionString As String = getConnection(connectionType)
            Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
            Dim doc As New XmlDocument()
            Dim sr As New System.IO.StringReader(xmlParams)
            doc.LoadXml(xmlParams)
            Dim parms As New Parameters

            For Each nod As XmlNode In doc.ChildNodes
                For Each childNode As XmlNode In nod.ChildNodes
                    Dim nodType As DbType
                    Select Case childNode.Item("Type").InnerText
                        Case "String"
                            nodType = DbType.String
                        Case "Int"
                            nodType = DbType.Int32
                        Case "Bigint"
                            nodType = DbType.Int64
                        Case "Decimal"
                            nodType = DbType.Decimal
                        Case "Boolean"
                            nodType = DbType.Boolean
                    End Select

                    Try
                        parms.AddInputParameter("@" & childNode.Item("Name").InnerText, childNode.Item("Value").InnerText, nodType)
                    Catch ex As Exception
                        Throw
                    End Try

                Next
            Next

            Dim ds As DataSet = dataLayer.ExecuteDataSet(command, cmdType, parms)
            Return ds
        Catch ex As Exception
            'Return ex.Message & connectionType & "-" & command & " - " & cmdType & "-" & xmlParams
            Throw ex
        End Try
    End Function

    <WebMethod()> _
    Public Function ExecuteScalarWithParms(ByVal connectionType As String, ByVal command As String, ByVal cmdType As Integer, _
                                           ByVal xml As String) As Integer

        Dim connectionString As String = getConnection(connectionType)
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim doc As New XmlDocument()
        Dim sr As New System.IO.StringReader(xml)
        doc.LoadXml(xml)
        Dim parms As New Parameters
        parms = CreateParms(doc)

        Dim returnid As Integer = dataLayer.ExecuteScalar(command, cmdType, parms)
        Return returnid
    End Function

    Public Function CreateParms(ByVal doc As XmlDocument) As Parameters
        Dim parms As New Parameters

        For Each nod As XmlNode In doc.ChildNodes
            If nod.Name = "Table" Then
                For Each childNode As XmlNode In nod.ChildNodes
                    Dim nodType As DbType
                    Select Case childNode.Item("Type").InnerText
                        Case "String"
                            nodType = DbType.String
                        Case "Int"
                            nodType = DbType.Int32
                        Case "Bigint"
                            nodType = DbType.Int64
                        Case "Decimal"
                            nodType = DbType.Decimal
                        Case "Boolean"
                            nodType = DbType.Boolean
                        Case "Byte"
                            nodType = DbType.Byte
                        Case "DateTime"
                            nodType = DbType.DateTime
                    End Select

                    If childNode.InnerText.ToString = "" Then
                        parms.AddInputParameter("@" & childNode.Name, DBNull.Value, nodType)
                    Else
                        parms.AddInputParameter("@" & childNode.Name, childNode.InnerText.ToString, nodType)
                    End If
                Next
            End If
        Next

        For Each nod As XmlNode In doc.ChildNodes
            For Each childNode As XmlNode In nod.ChildNodes
                Dim nodType As DbType
                Select Case childNode.Item("Type").InnerText
                    Case "String"
                        nodType = DbType.String
                    Case "Int"
                        nodType = DbType.Int32
                    Case "Bigint"
                        nodType = DbType.Int64
                    Case "Decimal"
                        nodType = DbType.Decimal
                    Case "Boolean"
                        nodType = DbType.Boolean
                End Select

                Try
                    parms.AddInputParameter("@" & childNode.Item("Name").InnerText, childNode.Item("Value").InnerText, nodType)
                Catch ex As Exception
                    Throw
                End Try

            Next
        Next

        Return parms
    End Function

    <WebMethod()> _
    Public Function ExecuteScalarWithoutParms(ByVal connectionType As String, ByVal command As String, ByVal commandType As Integer) As Integer
        Dim connectionString As String = getConnection(connectionType)
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim shopLoadID As Integer = dataLayer.ExecuteScalar(command, commandType)

        Return shopLoadID
    End Function

#Region "VRE Metryx"
    <WebMethod()> _
    Public Function SendEMail(ByVal sendTo As String, ByVal sendFrom As String, ByVal sendBCC As String, ByVal subject As String, _
                             ByVal body As String, ByVal mailHost As String) As Boolean
        Dim email As New MailMessage

        If sendFrom = "" Then
            sendFrom = "NoReply@PGWGlass.com"
        End If

        If mailHost = "" Then
            mailHost = getMailHost()
        End If

        email.From = New MailAddress(sendFrom)
        email.To.Add(sendTo)
        If sendBCC <> "" Then
            email.Bcc.Add(sendBCC)
        End If
        email.Subject = subject
        email.IsBodyHtml = True
        email.Body = body
        email.Priority = MailPriority.Normal

        Dim smtpMail As New System.Net.Mail.SmtpClient
        smtpMail.Host = mailHost
        smtpMail.Send(email)

        Return True
    End Function

    <WebMethod()> _
    Public Function RetrieveImage(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal Type As String) As Byte()
        Return Retrieve(InstallerTypeID, EIN, Type)
    End Function

    <WebMethod()> _
    Public Function SaveImage(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal ImageData As Byte(), ByVal User As Integer, ByVal Type As String, ByVal CompanyID As Integer, ByVal ServiceCenterID As Integer, ByVal UserType As Integer, ByVal CompanyName As String, ByVal ServiceCenterName As String, ByVal TechnicianName As String) As String
        Return Save(InstallerTypeID, EIN, ImageData, User, Type, CompanyID, ServiceCenterID, UserType, CompanyName, ServiceCenterName, TechnicianName)
    End Function

    <WebMethod()> _
    Public Function ApproveImage(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal ApproverID As Integer, ByVal Type As String, ByVal ImageID As Integer)
        Return Approve(InstallerTypeID, EIN, ApproverID, Type, ImageID)
    End Function

    <WebMethod()> _
    Public Function RejectImage(ByVal userID As Integer, ByVal EIN As Integer, ByVal Type As String, ByVal RejectionReason As String, ByVal ImageID As Integer, ByVal RID As Integer)
        Return Reject(userID, EIN, Type, RejectionReason, ImageID, RID)
    End Function

    <WebMethod()> _
    Public Sub UpdateImage(ByVal Type As String, ByVal TechnicianId As Integer, ByVal EIN As Integer, ByVal NewScId As Integer, ByVal NewScName As String, ByVal NewCompId As Integer, ByVal NewCompName As String)
        Call Update(Type, TechnicianId, EIN, NewScId, NewScName, NewCompId, NewCompName)
    End Sub

    <WebMethod()> _
    Public Function GetFrameCountInvoice(ByVal TifKey As String) As String
        Return GetFrameCount(TifKey)
    End Function

    <WebMethod()> _
    Public Function GetConvertedInvoiceImagePage(ByVal TifKey As String, ByVal FrameIndex As Integer) As Byte()
        Return GetConvertedInvoiceImage(TifKey, FrameIndex)
    End Function

    <WebMethod()> _
    Public Function GetOriginalInvoiceImage(ByVal TifKey As String) As Byte()
        Return GetOriginalInvoice(TifKey)
    End Function

    <WebMethod()> _
    Public Function GetEMail(ByVal UserId As Integer) As String
        Return GetEMailID(UserId)
    End Function

    <WebMethod()> _
    Public Function GetModifiedUserID(ByVal userID As Integer, ByVal EIN As Integer, ByVal Type As String)
        Return GetUserID(userID, EIN, Type)
    End Function

    <WebMethod()> _
    Public Function GetQueueImages(ByVal Type As String)
        Return GetPendingApprovalImages(Type)
    End Function

    <WebMethod()> _
    Public Function CheckImageUpload(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal InstallerType As String) As XmlDocument
        Return GetCheckingImage(InstallerTypeID, EIN, InstallerType)
    End Function

    <WebMethod()> _
    Public Function DeleteImages(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal InstallerType As String, ByVal ImageID As Integer)
        Return DeleteImage(InstallerTypeID, EIN, InstallerType, ImageID)
    End Function

    <WebMethod()> _
    Public Function GetPendingApprovalImageByTypeID(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal InstallerType As String) As XmlDocument
        Return GetPendingApprovalImageByID(InstallerTypeID, EIN, InstallerType)
    End Function

    <WebMethod()> _
    Public Function RetrieveApprovedImage(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal InstallerType As String)
        Return RetrieveApprovedImageByID(InstallerTypeID, EIN, InstallerType)
    End Function

    <WebMethod()> _
    Public Function GetTechnicianName(ByVal InstallerTypeID As Integer, ByVal EIN As Integer)
        Return RetrieveTechnicianName(InstallerTypeID, EIN)
    End Function

    Private Function RetrieveTechnicianName(ByVal InstallerTypeID As Integer, ByVal EIN As Integer)
        Dim TechnicianName As String
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Try
            parms.AddInputParameter("@TechnicianID", InstallerTypeID, DbType.Int32)
            parms.AddInputParameter("@EIN", EIN, DbType.Int32)
            TechnicianName = dataLayer.ExecuteScalar("spGetTechnicianName", CommandType.StoredProcedure, parms)
        Catch ex As Exception
            Throw ex
        End Try
        Return TechnicianName
    End Function

    Private Function RetrieveApprovedImageByID(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal InstallerType As String)
        Dim imageData As Byte() = Nothing
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Try
            Select Case InstallerType
                Case "Company"
                    parms.AddInputParameter("@CompanyID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    imageData = dataLayer.ExecuteScalar(VREConstants.SQL.Procedures.ApprovedImagesByCompanyGet, CommandType.StoredProcedure, parms)

                Case "ServiceCenter"
                    parms.AddInputParameter("@ServiceCenterID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    imageData = dataLayer.ExecuteScalar(VREConstants.SQL.Procedures.ApprovedImagesByServiceCenterGet, CommandType.StoredProcedure, parms)

                Case "Technician"
                    parms.AddInputParameter("@TechnicianID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    imageData = dataLayer.ExecuteScalar(VREConstants.SQL.Procedures.ApprovedImagesByTechnicianGet, CommandType.StoredProcedure, parms)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
        Return imageData
    End Function

    Private Function GetPendingApprovalImageByID(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal InstallerType As String) As XmlDocument
        Dim ds As New DataSet
        Dim Returnxml As String = String.Empty
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Dim xmlDocument As New System.Xml.XmlDocument
        Try
            Select Case InstallerType
                Case "Company"
                    parms.AddInputParameter("@CompanyID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    ds = dataLayer.ExecuteDataSet("spGetPendingApprovalImageForCompanyID", CommandType.StoredProcedure, parms)
                    'ds = dataLayer.ExecuteDataSet("spGetCompanyPendingApprovalImage", CommandType.StoredProcedure, parms)
                    Returnxml = ds.GetXml()
                Case "ServiceCenter"
                    parms.AddInputParameter("@ServiceCenterID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    ds = dataLayer.ExecuteDataSet("spGetPendingApprovalImageForServiceCenterID", CommandType.StoredProcedure, parms)
                    'ds = dataLayer.ExecuteDataSet("spGetServiceCenterPendingApprovalImage", CommandType.StoredProcedure, parms)
                    Returnxml = ds.GetXml()
                Case "Technician"
                    parms.AddInputParameter("@TechnicianID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    ds = dataLayer.ExecuteDataSet("spGetPendingApprovalImageForTechnicianID", CommandType.StoredProcedure, parms)
                    'ds = dataLayer.ExecuteDataSet("spGetTechnicianPendingApprovalImage", CommandType.StoredProcedure, parms)
                    Returnxml = ds.GetXml()
            End Select
        Catch ex As Exception
            Throw ex
        End Try
        xmlDocument.LoadXml(Returnxml)
        Return xmlDocument
    End Function

    Private Function DeleteImage(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal InstallerType As String, ByVal ImageID As Integer)
        Dim IsDelete As Boolean
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Try
            Select Case InstallerType
                Case "Company"
                    parms.AddInputParameter("@CompanyID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@ImageID", ImageID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    IsDelete = dataLayer.ExecuteScalar("spDeleteCompanyImages", CommandType.StoredProcedure, parms)
                Case "ServiceCenter"
                    parms.AddInputParameter("@ServiceCenterID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@ImageID", ImageID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    IsDelete = dataLayer.ExecuteScalar("spDeleteServiceCenterImages", CommandType.StoredProcedure, parms)
                Case "Technician"
                    parms.AddInputParameter("@TechnicianID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@ImageID", ImageID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    IsDelete = dataLayer.ExecuteScalar("spDeleteTechnicianImages", CommandType.StoredProcedure, parms)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
        Return IsDelete
    End Function

    Private Function GetCheckingImage(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal InstallerType As String) As XmlDocument
        Dim ds As New DataSet
        Dim Returnxml As String = String.Empty
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Dim xmlDocument As New System.Xml.XmlDocument
        Try
            Select Case InstallerType
                Case "Company"
                    parms.AddInputParameter("@CompanyID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    ds = dataLayer.ExecuteDataSet("spGetCompanyPendingNonApprovalRejectImage", CommandType.StoredProcedure, parms)
                    'ds = dataLayer.ExecuteDataSet("spGetCompanyPendingApprovalImage", CommandType.StoredProcedure, parms)
                    Returnxml = ds.GetXml()

                Case "ServiceCenter"
                    parms.AddInputParameter("@ServiceCenterID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    ds = dataLayer.ExecuteDataSet("spGetServiceCenterPendingNonApprovalRejectImage", CommandType.StoredProcedure, parms)
                    'ds = dataLayer.ExecuteDataSet("spGetServiceCenterPendingApprovalImage", CommandType.StoredProcedure, parms)
                    Returnxml = ds.GetXml()
                Case "Technician"
                    parms.AddInputParameter("@TechnicianID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    ds = dataLayer.ExecuteDataSet("spGetTechnicianPendingNonApprovalRejectImage", CommandType.StoredProcedure, parms)
                    'ds = dataLayer.ExecuteDataSet("spGetTechnicianPendingApprovalImage", CommandType.StoredProcedure, parms)
                    Returnxml = ds.GetXml()
            End Select
        Catch ex As Exception
            Throw ex
        End Try
        xmlDocument.LoadXml(Returnxml)
        Return xmlDocument
    End Function

    <WebMethod()> _
    Public Function GetCompanyDetails(ByVal lInstallerUidNo As Long, ByVal sUserID As String) As System.Xml.XmlDocument

        Dim RetXDoc As New System.Xml.XmlDocument
        Try
            RetXDoc = GetInfo(lInstallerUidNo, 0, sUserID, "Company")
        Catch ex As Exception
            Throw ex
        End Try
        Return RetXDoc

    End Function
    'TODO:-- Comment the below section

    '<WebMethod()> _
    'Public Function GetCompanyDetails(ByVal lInstallerUidNo As Long, ByVal sUserID As String) As System.Xml.XmlDocument
    '    Dim xmlfile As XmlReader
    '    Dim dsXML As New DataSet
    '    Dim dsReturnXML As New DataSet
    '    Dim dvXML As DataView
    '    Dim xmlDocument As System.Xml.XmlDocument

    '    Dim ReturnXML As String = String.Empty
    '    Try
    '        xmlfile = XmlReader.Create("C:\VRE Metryx\Code\Dispatch XML\so_company.xml", New XmlReaderSettings())
    '        dsXML.ReadXml(xmlfile)

    '        dvXML = New DataView(dsXML.Tables(0), "", "modify_dt Desc", DataViewRowState.CurrentRows)
    '        dsXML.Tables.Clear()
    '        dsXML.Tables.Add(dvXML.ToTable())

    '        Dim rowCount As Integer
    '        rowCount = dsXML.Tables(0).Rows.Count
    '        rowCount = rowCount - 1
    '        For Counter As Integer = rowCount To 1 Step -1
    '            dsXML.Tables(0).Rows(Counter).Delete()
    '        Next Counter

    '        ReturnXML = dsXML.GetXml()

    '        xmlDocument = New System.Xml.XmlDocument()
    '        xmlDocument.LoadXml(ReturnXML)

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    '    Return xmlDocument
    'End Function

    <WebMethod()> _
    Public Function GetServiceCenterDetails(ByVal lInstallerUidNo As Long, ByVal sUserID As String) As System.Xml.XmlDocument

        Dim RetXDoc As New System.Xml.XmlDocument
        Try
            RetXDoc = GetInfo(lInstallerUidNo, 0, sUserID, "ServiceCenter")
        Catch ex As Exception
            Throw ex
        End Try
        Return RetXDoc

    End Function

    '<WebMethod()> _
    'Public Function GetServiceCenterDetails(ByVal lInstallerUidNo As Long, ByVal sUserID As String) As System.Xml.XmlDocument

    '    Dim xmlfile As XmlReader
    '    Dim dsXML As New DataSet
    '    Dim dvXML As DataView
    '    Dim xmlDocument As System.Xml.XmlDocument

    '    Dim ReturnXML As String = String.Empty
    '    Try
    '        xmlfile = XmlReader.Create("C:\VRE Metryx\Code\Dispatch XML\so_service_center.xml", New XmlReaderSettings())
    '        dsXML.ReadXml(xmlfile)
    '        dvXML = New DataView(dsXML.Tables(0), "", "modify_dt Desc", DataViewRowState.CurrentRows)
    '        dsXML.Tables.Clear()
    '        dsXML.Tables.Add(dvXML.ToTable())

    '        Dim rowCount As Integer
    '        rowCount = dsXML.Tables(0).Rows.Count
    '        rowCount = rowCount - 1
    '        For Counter As Integer = rowCount To 1 Step -1
    '            dsXML.Tables(0).Rows(Counter).Delete()
    '        Next Counter

    '        ReturnXML = dsXML.GetXml()

    '        xmlDocument = New System.Xml.XmlDocument()
    '        xmlDocument.LoadXml(ReturnXML)

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    '    Return xmlDocument
    'End Function

    <WebMethod()> _
    Public Function GetTechnicianDetails(ByVal InstallerUidNo As Long, ByVal ParentUidNo As Long, ByVal UserID As String) As System.Xml.XmlDocument

        Dim RetXDoc As New System.Xml.XmlDocument
        Try
            RetXDoc = GetInfo(InstallerUidNo, ParentUidNo, UserID, "Technician")
        Catch ex As Exception
            Throw ex
        End Try
        Return RetXDoc
    End Function

    '<WebMethod()> _
    'Public Function GetTechnicianDetails(ByVal InstallerUidNo As Long, ByVal ParentUidNo As Long, ByVal UserID As String) As System.Xml.XmlDocument

    '    'Dim RetXDoc As New System.Xml.XmlDocument
    '    'Try
    '    '    RetXDoc = GetInfo(InstallerUidNo, ParentUidNo, UserID, "Technician")
    '    Dim xmlfile As XmlReader
    '    Dim dsxml As New DataSet
    '    Dim dvxml As DataView
    '    Dim xmldocument As System.Xml.XmlDocument

    '    Dim returnxml As String = String.Empty
    '    Try
    '        xmlfile = XmlReader.Create("c:\vre metryx\code\dispatch xml\so_technician.xml", New XmlReaderSettings())
    '        dsxml.ReadXml(xmlfile)
    '        dvxml = New DataView(dsxml.Tables(0), "", "modify_dt desc", DataViewRowState.CurrentRows)
    '        dsxml.Tables.Clear()
    '        dsxml.Tables.Add(dvxml.ToTable())

    '        Dim rowcount As Integer
    '        rowcount = dsxml.Tables(0).Rows.Count
    '        rowcount = rowcount - 1
    '        For counter As Integer = rowcount To 1 Step -1
    '            dsxml.Tables(0).Rows(counter).Delete()
    '        Next counter

    '        returnxml = dsxml.GetXml()

    '        xmldocument = New System.Xml.XmlDocument()
    '        xmldocument.LoadXml(returnxml)

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    '    'Return RetXDoc
    '    Return xmldocument

    'End Function

    <WebMethod()> _
    Public Function GetCompanyInfoByServiceCenterID(ByVal InstallerUidNo As Long, ByVal UserID As String, ByVal InstallerType As String) As InfoBase
        Return GetParentInfo(InstallerUidNo, UserID, InstallerType)
    End Function

    '<WebMethod()> _
    'Public Function GetServiceCenterIDByTechnicianID(ByVal InstallerUidNo As Long, ByVal UserID As String) As Long
    '    Return GetParentID(InstallerUidNo, UserID, "Technician")
    'End Function

    <WebMethod()> _
    Public Function GetCompanyInfoByID(ByVal InstallerUidNo As Long, ByVal UserID As String) As InfoBase
        Return GetCurrentInfo(InstallerUidNo, 0, UserID, "Company")
    End Function

    <WebMethod()> _
    Public Function GetServiceCenterNameByID(ByVal InstallerUidNo As Long, ByVal UserID As String) As InfoBase
        Return GetCurrentInfo(InstallerUidNo, 0, UserID, "ServiceCenter")
    End Function

    <WebMethod()> _
    Public Function GetTechnicianNameByID(ByVal InstallerUidNo As Long, ByVal ParentUidNo As Long, ByVal UserID As String) As InfoBase
        Return GetCurrentInfo(InstallerUidNo, ParentUidNo, UserID, "Technician")
    End Function

    Public Function Retrieve(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal Type As String) As Byte()

        Dim imageData As Byte() = Nothing
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Try
            Select Case Type
                Case "Company"
                    parms.AddInputParameter("@CompanyID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    imageData = dataLayer.ExecuteScalar(VREConstants.SQL.Procedures.ImagesByCompanyGet, CommandType.StoredProcedure, parms)

                Case "ServiceCenter"
                    parms.AddInputParameter("@ServiceCenterID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    imageData = dataLayer.ExecuteScalar(VREConstants.SQL.Procedures.ImagesByServiceCenterGet, CommandType.StoredProcedure, parms)

                Case "Technician"
                    parms.AddInputParameter("@TechnicianID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    imageData = dataLayer.ExecuteScalar(VREConstants.SQL.Procedures.ImagesByTechnicianGet, CommandType.StoredProcedure, parms)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
        Return imageData
    End Function

    Private Function Save(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal ImageData As Byte(), ByVal User As Integer, ByVal Type As String, ByVal CompanyID As Integer, ByVal ServiceCenterID As Integer, ByVal UserType As Integer, ByVal CompanyName As String, ByVal ServiceCenterName As String, ByVal TechnicianName As String) As String

        Dim msg As String = String.Empty
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters

        Try
            parms.AddInputParameter("@EIN", EIN, DbType.Int32)
            parms.AddInputParameter("@ImageData", ImageData, DbType.Binary)
            parms.AddInputParameter("@ModifiedByUser", User, DbType.Int32)
            parms.AddInputParameter("@ModifiedDate", DateTime.Now, DbType.DateTime)

            Select Case Type
                Case "Company"
                    parms.AddInputParameter("@CompanyID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@UserType", UserType, DbType.Int32)
                    parms.AddInputParameter("@CompanyName", CompanyName, DbType.String)
                    msg = dataLayer.ExecuteScalar("spImagesByCompanyInsert", CommandType.StoredProcedure, parms)

                Case "ServiceCenter"
                    parms.AddInputParameter("@ServiceCenterID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@CompanyID", CompanyID, DbType.Int32)
                    parms.AddInputParameter("@CompanyName", CompanyName, DbType.String)
                    parms.AddInputParameter("@ServiceCenterName", ServiceCenterName, DbType.String)
                    msg = dataLayer.ExecuteScalar("spImagesByServiceCenterInsert", CommandType.StoredProcedure, parms)

                Case "Technician"
                    parms.AddInputParameter("@TechnicianID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@ServiceCenterID", ServiceCenterID, DbType.Int32)
                    parms.AddInputParameter("@CompanyID", CompanyID, DbType.Int32)
                    parms.AddInputParameter("@CompanyName", CompanyName, DbType.String)
                    parms.AddInputParameter("@ServiceCenterName", ServiceCenterName, DbType.String)
                    parms.AddInputParameter("@TechnicianName", TechnicianName, DbType.String)
                    msg = dataLayer.ExecuteScalar("spImagesByTechnicianInsert", CommandType.StoredProcedure, parms)
            End Select
        Catch ex As Exception
            Throw ex
        End Try

        Return msg
    End Function

    Private Function Approve(ByVal InstallerTypeID As Integer, ByVal EIN As Integer, ByVal ApproverID As Integer, ByVal Type As String, ByVal ImageID As Integer)
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Dim dtReturn As DataTable
        Dim ModifiedUser As Integer

        Try
            parms.AddInputParameter("@EIN", EIN, DbType.Int32)
            parms.AddInputParameter("@AID", ApproverID, DbType.Int32)
            parms.AddInputParameter("@ApproveDate", DateTime.Now, DbType.DateTime)
            Select Case Type
                Case "Company"
                    parms.AddInputParameter("@CompanyID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@ImageID", ImageID, DbType.Int32)
                    dtReturn = dataLayer.ExecuteDataTable("spImagesByCompanyApprove", CommandType.StoredProcedure, parms)

                    If (Not dtReturn Is Nothing) And (dtReturn.Rows.Count > 0) Then
                        ModifiedUser = dtReturn.Rows(0)("ModifiedByUser")
                    End If
                Case "ServiceCenter"
                    parms.AddInputParameter("@ServiceCenterID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@ImageID", ImageID, DbType.Int32)
                    dtReturn = dataLayer.ExecuteDataTable("spImagesByServiceCenterApprove", CommandType.StoredProcedure, parms)

                    If (Not dtReturn Is Nothing) And (dtReturn.Rows.Count > 0) Then
                        ModifiedUser = dtReturn.Rows(0)("ModifiedByUser")
                    End If
                Case "Technician"
                    parms.AddInputParameter("@TechnicianID", InstallerTypeID, DbType.Int32)
                    parms.AddInputParameter("@ImageID", ImageID, DbType.Int32)
                    dtReturn = dataLayer.ExecuteDataTable("spImagesByTechnicianApprove", CommandType.StoredProcedure, parms)

                    If (Not dtReturn Is Nothing) And (dtReturn.Rows.Count > 0) Then
                        ModifiedUser = dtReturn.Rows(0)("ModifiedByUser")
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try

        Return ModifiedUser

    End Function

    Private Function Reject(ByVal userID As Integer, ByVal EIN As Integer, ByVal Type As String, ByVal RejectionReason As String, ByVal ImageID As Integer, ByVal RID As Integer)
        Dim IsRejected As Boolean
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters

        Try
            Select Case Type
                Case "Company"
                    parms.AddInputParameter("@CompanyID", userID, DbType.Int32)
                    parms.AddInputParameter("@RID", RID, DbType.Int32)
                    parms.AddInputParameter("@ImageID", ImageID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    parms.AddInputParameter("@RejectionReason", RejectionReason, DbType.String)
                    parms.AddInputParameter("@RejectDate", DateTime.Now, DbType.DateTime)
                    IsRejected = dataLayer.ExecuteScalar("spImagesByCompanyReject", CommandType.StoredProcedure, parms)

                Case "ServiceCenter"
                    parms.AddInputParameter("@ServiceCenterID", userID, DbType.Int32)
                    parms.AddInputParameter("@RID", RID, DbType.Int32)
                    parms.AddInputParameter("@ImageID", ImageID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    parms.AddInputParameter("@RejectionReason", RejectionReason, DbType.String)
                    parms.AddInputParameter("@RejectDate", DateTime.Now, DbType.DateTime)
                    IsRejected = dataLayer.ExecuteScalar("spImagesByServiceCenterReject", CommandType.StoredProcedure, parms)

                Case "Technician"
                    parms.AddInputParameter("@TechnicianID", userID, DbType.Int32)
                    parms.AddInputParameter("@RID", RID, DbType.Int32)
                    parms.AddInputParameter("@ImageID", ImageID, DbType.Int32)
                    parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                    parms.AddInputParameter("@RejectionReason", RejectionReason, DbType.String)
                    parms.AddInputParameter("@RejectDate", DateTime.Now, DbType.DateTime)
                    IsRejected = dataLayer.ExecuteScalar("spImagesByTechnicianReject", CommandType.StoredProcedure, parms)
            End Select
        Catch ex As Exception
            Throw ex
        End Try

        Return IsRejected

    End Function

    Private Sub Update(ByVal Type As String, ByVal TechnicianId As Integer, ByVal EIN As Integer, ByVal NewScId As Integer, ByVal NewScName As String, ByVal NewCompId As Integer, ByVal NewCompName As String)
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Dim appLog As LYNXWebServices.ApplicationLog = GetApplicationLogInstance()

        'appLog.Publish("UpdateTechImage_Input", Type & " / " & TechnicianId & " / " & EIN & " / " & NewScId & " / " & NewScName & " / " & NewCompId & " / " & NewCompName, GetEnvironment(), LYNXWebServices.ErrorLevel.Information, 1, Guid.NewGuid().ToString())
        Try
            Select Case Type
                Case "CrossServiceCenter"
                    parms.AddInputParameter("@Technician_Id", TechnicianId, DbType.Int32)
                    parms.AddInputParameter("@New_Sc_Id", NewScId, DbType.Int32)
                    parms.AddInputParameter("@New_Sc_Name", NewScName, DbType.String)
                    dataLayer.ExecuteNonQuery("spUpdateReassignTechImages", CommandType.StoredProcedure, parms)
                Case "CrossCompany"
                    parms.AddInputParameter("@Technician_Id", TechnicianId, DbType.Int32)
                    parms.AddInputParameter("@Federal_Tax_Id", EIN, DbType.Int32)
                    parms.AddInputParameter("@New_Sc_Id", NewScId, DbType.Int32)
                    parms.AddInputParameter("@New_Sc_Name", NewScName, DbType.String)
                    parms.AddInputParameter("@New_Company_Id", NewCompId, DbType.Int32)
                    parms.AddInputParameter("@New_Company_Name", NewCompName, DbType.String)
                    dataLayer.ExecuteNonQuery("spUpdateReassignTechImagesArg", CommandType.StoredProcedure, parms)
            End Select
        Catch ex As Exception
            appLog.Publish("UpdateTechImage_Error", ex.Message + " " & ex.StackTrace, GetEnvironment(), LYNXWebServices.ErrorLevel.Critical, 1, Guid.NewGuid().ToString())
        End Try

    End Sub

    Private Function GetFrameCount(ByVal TifKey As String) As String
        Dim img As Image
        Dim ImgPath As String
        Dim FileName As String
        Dim frameCount As Integer = 0
        Dim tokenHandle As New IntPtr(0)

        'Declarations for Impersonation Parameters
        Dim impName As String = "svc-lynxagcweb"
        Dim impDomain As String = "pgw"
        Dim impPass As String = "cddp1AUf"

        'Declarations to Construct an xml Response
        Dim dataset As New DataSet("DataSet")
        Dim datarow As DataRow
        Dim invoiceTable As DataTable = dataset.Tables.Add("convertedInvoiceImage")

        'Declarations to Access the AppLog
        Dim connectionString As String = getConnection("LYNX")
        Dim appLog As LYNXWebServices.ApplicationLog = GetApplicationLogInstance()

        'appLog.Publish("GetFrameCount_ErrorInit", TifKey, GetEnvironment(), LYNXWebServices.ErrorLevel.Information, 1, Guid.NewGuid().ToString())

        'Get the Image path for the current environment
        Select Case PGW.Business.Environment.getEnvironment.ToUpper
            Case "DEV"
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Dev\Storage"
            Case "STG"
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Stg\Storage"
            Case "PRD"
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Prd\Storage"
            Case Else
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Dev\Storage"
        End Select

        'Form the full Image Path
        FileName = TifKey
        FileName = Replace(FileName, "\\", "\")
        FileName = FileName & ".tif"
        ImgPath = ImgPath & FileName

        Try
            'Begin Impersonation
            If LogonUser(impName, impDomain, impPass, 2, 0, tokenHandle) Then
                Dim newId As New WindowsIdentity(tokenHandle)
                Using impersonatedUser As WindowsImpersonationContext = newId.Impersonate()

                    'Run commands as the impersonated user
                    img = Image.FromFile(ImgPath)

                    'Obtain the number of frames in the tiff
                    frameCount = img.GetFrameCount(FrameDimension.Page)

                    'Add the framecount to the dataset to be returned
                    invoiceTable.Columns.Add("totalFrames", Type.GetType("System.String"))
                    datarow = invoiceTable.NewRow()
                    datarow("totalFrames") = frameCount.ToString
                    invoiceTable.Rows.Add(datarow)

                    'Cleanup Image resources
                    img.Dispose()
                End Using
                CloseHandle(tokenHandle)
            Else
                'Impersonation Logon has failed
                appLog.Publish("GetFrameCount_ImpersonationLogonError", LogonUser(impName, impDomain, impPass, 2, 0, tokenHandle), GetEnvironment(), LYNXWebServices.ErrorLevel.Information, 1, Guid.NewGuid().ToString())
            End If
        Catch ex As Exception
            appLog.Publish("GetFrameCount_Error", ex.Message + " " & ex.StackTrace, GetEnvironment(), LYNXWebServices.ErrorLevel.Critical, 1, Guid.NewGuid().ToString())
            Throw ex
        End Try

        Return dataset.GetXml()
    End Function

    Private Function GetOriginalInvoice(ByVal TifKey As String) As Byte()
        Dim ImgPath As String
        Dim FileName As String
        Dim stream As FileStream
        Dim bReader As BinaryReader
        Dim invoiceImage As Byte() = Nothing
        Dim tokenHandle As New IntPtr(0)

        'Declarations for Impersonation Parameters
        Dim impName As String = "svc-lynxagcweb"
        Dim impDomain As String = "pgw"
        Dim impPass As String = "cddp1AUf"

        'Declarations to Access the AppLog
        Dim connectionString As String = getConnection("LYNX")
        Dim appLog As LYNXWebServices.ApplicationLog = GetApplicationLogInstance()

        'appLog.Publish("GetOriginalInvoice_ErrorInit", TifKey, GetEnvironment(), LYNXWebServices.ErrorLevel.Information, 1, Guid.NewGuid().ToString())

        'Get the Image path for the current environment
        Select Case PGW.Business.Environment.getEnvironment.ToUpper
            Case "DEV"
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Dev\Storage"
            Case "STG"
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Stg\Storage"
            Case "PRD"
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Prd\Storage"
            Case Else
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Dev\Storage"
        End Select

        'Form the full Image Path
        FileName = TifKey
        FileName = Replace(FileName, "\\", "\")
        FileName = FileName & ".tif"
        ImgPath = ImgPath & FileName

        Try
            'Begin Impersonation
            If LogonUser(impName, impDomain, impPass, 2, 0, tokenHandle) Then
                Dim newId As New WindowsIdentity(tokenHandle)
                Using impersonatedUser As WindowsImpersonationContext = newId.Impersonate()

                    'Run commands as the impersonated user
                    stream = New FileStream(ImgPath, FileMode.Open, FileAccess.Read)
                    bReader = New BinaryReader(stream)
                    invoiceImage = bReader.ReadBytes(CType(stream.Length, Integer))

                    'Close and Cleanup Resources
                    bReader.Close()
                    stream.Close()
                End Using
                CloseHandle(tokenHandle)
            Else
                'Impersonation Logon has failed
                appLog.Publish("GetOriginalInvoice_ImpersonationLogonError", LogonUser(impName, impDomain, impPass, 2, 0, tokenHandle), GetEnvironment(), LYNXWebServices.ErrorLevel.Information, 1, Guid.NewGuid().ToString())
            End If
        Catch ex As Exception
            appLog.Publish("GetOriginalInvoice_Error", ex.Message + " " & ex.StackTrace, GetEnvironment(), LYNXWebServices.ErrorLevel.Critical, 1, Guid.NewGuid().ToString())
            Throw ex
        End Try

        Return invoiceImage
    End Function

    Private Function GetConvertedInvoiceImage(ByVal TifKey As String, ByVal FrameIndex As Integer) As Byte()
        Dim ImgPath As String
        Dim Filename As String
        Dim frameBitmap As Bitmap
        Dim memoryStream As MemoryStream = New MemoryStream()
        Dim imageData As Byte() = Nothing

        Dim tokenHandle As New IntPtr(0)

        'Declarations for Impersonation Parameters
        Dim impName As String = "svc-lynxagcweb"
        Dim impDomain As String = "pgw"
        Dim impPass As String = "cddp1AUf"

        'Declarations to Access the AppLog
        Dim connectionString As String = getConnection("LYNX")
        Dim appLog As LYNXWebServices.ApplicationLog = GetApplicationLogInstance()

        'appLog.Publish("GetConvertedInvoiceImage_ErrorInit", TifKey & "/" & FrameIndex, GetEnvironment(), LYNXWebServices.ErrorLevel.Information, 1, Guid.NewGuid().ToString())

        'Get the Image path for the current environment
        Select Case PGW.Business.Environment.getEnvironment.ToUpper
            Case "DEV"
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Dev\Storage"
            Case "STG"
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Stg\Storage"
            Case "PRD"
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Prd\Storage"
            Case Else
                ImgPath = "\\pgw.local\dfs\intra\glassdocuments\Dev\Storage"
        End Select

        'Form the full Image Path
        Filename = TifKey
        Filename = Replace(Filename, "\\", "\")
        Filename = Filename & ".tif"
        ImgPath = ImgPath & Filename

        Try
            'Begin Impersonation
            If LogonUser(impName, impDomain, impPass, 2, 0, tokenHandle) Then
                Dim newId As New WindowsIdentity(tokenHandle)
                Using impersonatedUser As WindowsImpersonationContext = newId.Impersonate()

                    'Run commands as the impersonated user
                    frameBitmap = New Bitmap(ImgPath)
                    'invoiceTable.Columns.Add("frameImage", Type.GetType("System.String"))

                    frameBitmap.SelectActiveFrame(FrameDimension.Page, FrameIndex)
                    frameBitmap.Save(memoryStream, ImageFormat.Jpeg)
                    imageData = memoryStream.GetBuffer()

                    'Close and Cleanup Resources
                    memoryStream.Close()
                    memoryStream.Dispose()
                    frameBitmap.Dispose()
                End Using
                CloseHandle(tokenHandle)
            Else
                'Impersonation Logon has failed
                appLog.Publish("GetConvertedInvoiceImage_ImpersonationLogonError", LogonUser(impName, impDomain, impPass, 2, 0, tokenHandle), GetEnvironment(), LYNXWebServices.ErrorLevel.Information, 1, Guid.NewGuid().ToString())
            End If
        Catch ex As Exception
            appLog.Publish("GetConvertedInvoiceImage_Error", ex.Message + " " & ex.StackTrace, GetEnvironment(), LYNXWebServices.ErrorLevel.Critical, 1, Guid.NewGuid().ToString())
            Throw ex
        End Try

        Return imageData
    End Function

    Private Function GetEMailID(ByVal UserId As Integer) As String
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Dim MailId As String
        Try
            parms.AddInputParameter("@ModifiedByUser", UserId, DbType.Int32)
            MailId = dataLayer.ExecuteScalar("spEmailByIdGet", CommandType.StoredProcedure, parms)

        Catch ex As Exception
            Throw ex
        End Try
        Return MailId
    End Function

    Private Function GetUserID(ByVal userID As Integer, ByVal EIN As Integer, ByVal Type As String)
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Dim retUserID As Integer
        Select Case Type
            Case "Company"
                parms.AddInputParameter("@CompanyID", userID, DbType.Int32)
                parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                retUserID = dataLayer.ExecuteScalar("spGetUserIDByCompany", CommandType.StoredProcedure, parms)
            Case "ServiceCenter"
                parms.AddInputParameter("@ServiceCenterID", userID, DbType.Int32)
                parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                retUserID = dataLayer.ExecuteScalar("spGetUserIDByServiceCenter", CommandType.StoredProcedure, parms)
            Case "Technician"
                parms.AddInputParameter("@TechnicianID", userID, DbType.Int32)
                parms.AddInputParameter("@EIN", EIN, DbType.Int32)
                retUserID = dataLayer.ExecuteScalar("spGetUserIDByTechnician", CommandType.StoredProcedure, parms)
        End Select
        Return retUserID
    End Function

    Private Function GetPendingApprovalImages(ByVal Type As String)
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Dim ds As New DataSet
        Dim Returnxml As String = String.Empty

        Try
            Select Case Type
                Case "Company"
                    ds = dataLayer.ExecuteDataSet("spGetCompanyPendingApprovalImage", CommandType.StoredProcedure, parms)
                    Returnxml = ds.GetXml()
                Case "ServiceCenter"
                    ds = dataLayer.ExecuteDataSet("spGetServiceCenterPendingApprovalImage", CommandType.StoredProcedure, parms)
                    Returnxml = ds.GetXml()
                Case "Technician"
                    ds = dataLayer.ExecuteDataSet("spGetTechnicianPendingApprovalImage", CommandType.StoredProcedure, parms)
                    Returnxml = ds.GetXml()
            End Select
        Catch ex As Exception
            Throw ex
        End Try
        Return Returnxml
    End Function

    Private Function GetInfo(ByVal InstallerUidNo As Long, ByVal ParentUidNo As Long, ByVal UserID As String, ByVal Type As String) As System.Xml.XmlDocument
        Dim xmlDoc As New XmlDocument()
        Dim commandText As String
        Dim connectionString As String = getConnection("INGRES")
        Dim commitTransaction As Boolean = True
        Dim transaction As DbTransaction = Nothing
        Dim dbLayer As New DataLayer(connectionString, DataLayer.DatabaseType.Ingres)
        Dim parameters As New Parameters()

        Dim sessionTableCreated As Boolean
        Dim sActionCode As String
        Dim sTempTable As String
        Dim sStoredProc As String

        Dim dtResult As DataTable

        Dim dsResult As DataSet
        Dim dvXML As DataView
        Dim ReturnXML As String

        Dim xmlDocument As System.Xml.XmlDocument

        Select Case Type
            Case "Company"
                sTempTable = "so_company"
                sStoredProc = "dp_manage_so_company"
            Case "ServiceCenter"
                sTempTable = "so_service_center"
                sStoredProc = "dp_manage_so_service_center"
            Case "Technician"
                sTempTable = "so_service_technician"
                sStoredProc = "dp_get_so_technician"
            Case Else
                sTempTable = ""
                sStoredProc = ""
        End Select

        Try
            sActionCode = "GET"

            '/* Step 1: Create the session table */
            transaction = dbLayer.BeginTransaction()

            commandText = "declare global temporary table session." & sTempTable & " as " & _
            "select * from " & sTempTable & " on commit preserve rows with norecovery"
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            sessionTableCreated = True

            '/* Step 2: Insert the parameters in session table */
            'commandText = "insert into session." & sTempTable & " (inst_uid_no, user_id_no)" & _
            '" values (" & InstallerUidNo & ", '" & UserID & "')"

            Select Case Type
                Case "Company"
                    'commandText = "insert into session." & sTempTable & " (inst_uid_no, action_cd,modify_user_type_no, user_id_no)" & _
                    ' "values (" & InstallerUidNo & ",'" & sActionCode & "'," & UserType & ", '" & UserID & "')"
                    commandText = "insert into session." & sTempTable & " (inst_uid_no, action_cd, user_id_no)" & _
                     "values (" & InstallerUidNo & ",'" & sActionCode & "', '" & UserID & "')"
                Case "ServiceCenter"
                    commandText = "insert into session." & sTempTable & " (inst_uid_no, action_cd, user_id_no)" & _
                    " values (" & InstallerUidNo & ",'" & sActionCode & "', '" & UserID & "')"
                Case "Technician"
                    'commandText = "insert into session." & sTempTable & " (inst_uid_no, user_id_no)" & _
                    '" values (" & InstallerUidNo & ", '" & UserID & "')"
                    commandText = "insert into session." & sTempTable & " (inst_uid_no, technician_uid_no, user_id_no)" & _
                    "values (" & ParentUidNo & "," & InstallerUidNo & ", '" & UserID & "')"
            End Select

            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)

            '/* Step 3: Execute the stored Procedure */
            commandText = "{ ? = call lynxdba." & sStoredProc & "(dummy=session." & sTempTable & ") }"
            parameters.AddReturnParameter("retval", DbType.Int32)
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, parameters, transaction)
            'returnValue = parameters("retval").Value

            '/* Step 4: Retrieve the result from session table */
            commandText = "select * from session." & sTempTable & ""

            Select Case Type
                Case "Company"
                    dtResult = dbLayer.ExecuteDataTable(commandText, CommandType.Text, "Company", transaction)
                    dtResult.DataSet.DataSetName = "Company"
                    dsResult = dtResult.DataSet
                Case "ServiceCenter"
                    dtResult = dbLayer.ExecuteDataTable(commandText, CommandType.Text, "ServiceCenter", transaction)
                    dtResult.DataSet.DataSetName = "ServiceCenter"
                    dsResult = dtResult.DataSet
                Case "Technician"
                    dtResult = dbLayer.ExecuteDataTable(commandText, CommandType.Text, "Technician", transaction)
                    dtResult.DataSet.DataSetName = "Technician"
                    dsResult = dtResult.DataSet
                Case Else
                    dsResult = Nothing
            End Select

            If Not dsResult Is Nothing Then
                'dvXML = New DataView(dsResult.Tables(0), "", "modify_dt Desc", DataViewRowState.CurrentRows)
                'dsResult.Tables.Clear()
                'dsResult.Tables.Add(dvXML.ToTable())
                'ReturnXML = dsResult.GetXml()

                Select Case Type
                    Case "Company"
                        dvXML = New DataView(dsResult.Tables(0), "Isnull(modify_user_id_no, '') <> '' AND inst_uid_no = " & InstallerUidNo, "modify_dt Desc", DataViewRowState.CurrentRows)
                    Case "ServiceCenter"
                        dvXML = New DataView(dsResult.Tables(0), "Isnull(modify_user_id_no, '') <> '' AND inst_uid_no = " & InstallerUidNo, "modify_dt Desc", DataViewRowState.CurrentRows)
                    Case "Technician"
                        dvXML = New DataView(dsResult.Tables(0), "Isnull(modify_user_id_no, '') <> '' AND technician_uid_no=" & InstallerUidNo, "modify_dt Desc", DataViewRowState.CurrentRows)
                    Case Else
                        dvXML = Nothing
                End Select

                dsResult.Tables.Clear()
                dsResult.Tables.Add(dvXML.ToTable())

                Dim rowCount As Integer
                rowCount = dsResult.Tables(0).Rows.Count
                rowCount = rowCount - 1
                For Counter As Integer = rowCount To 1 Step -1
                    dsResult.Tables(0).Rows(Counter).Delete()
                Next Counter

                ReturnXML = dsResult.GetXml()

                xmlDocument = New System.Xml.XmlDocument()
                xmlDocument.LoadXml(ReturnXML)
            Else
                xmlDocument = New System.Xml.XmlDocument()
            End If

        Catch ex As Exception
            commitTransaction = False
            Throw
        Finally
            '/* Remove the session table if it has been created */
            If sessionTableCreated Then
                commandText = "drop table session." & sTempTable & ""
                dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            End If

            '/* Either commit the transaction or roll it back */
            If commitTransaction Then
                dbLayer.CommitTransaction()
            Else
                dbLayer.RollbackTransaction()
            End If

            '/* Dispose of the data layer */
            dbLayer.Dispose()
        End Try
        Return xmlDocument
    End Function

    Private Function GetParentInfo(ByVal InstallerUidNo As Long, ByVal UserID As String, ByVal Type As String) As InfoBase
        Dim dsData As New DataSet
        Dim retInfo As New InfoBase
        Try
            Select Case Type
                Case "ServiceCenter"
                    dsData.ReadXml(New XmlNodeReader(GetServiceCenterDetails(InstallerUidNo, UserID)))
                Case "Technician"
                    dsData.ReadXml(New XmlNodeReader(GetTechnicianDetails(InstallerUidNo, 0, UserID)))
            End Select

            If dsData.Tables.Count > 0 And dsData.Tables(0).Rows.Count > 0 Then
                If (dsData.Tables(0).Columns.Contains("parent_uid_no")) Then
                    retInfo.Identifier = Long.Parse(dsData.Tables(0).Rows(0)("parent_uid_no"))
                    retInfo.Name = Convert.ToString(dsData.Tables(0).Rows(0)("parent_nm"))
                    Return retInfo
                Else
                    Return Nothing

                End If
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetCurrentInfo(ByVal InstallerUidNo As Long, ByVal ParentUidNo As Long, ByVal UserID As String, ByVal Type As String) As InfoBase
        Dim dsData As New DataSet
        Dim technicianLastName As String
        Dim technicianFirstName As String
        Dim retInfo As New InfoBase
        Try
            technicianLastName = String.Empty
            technicianFirstName = String.Empty

            Select Case Type
                Case "Company"
                    dsData.ReadXml(New XmlNodeReader(GetCompanyDetails(InstallerUidNo, UserID)))
                    If (dsData.Tables(0).Columns.Contains("inst_uid_no") And dsData.Tables(0).Columns.Contains("inst_nm")) Then
                        retInfo.Identifier = Convert.ToString(dsData.Tables(0).Rows(0)("inst_uid_no"))
                        retInfo.Name = Convert.ToString(dsData.Tables(0).Rows(0)("inst_nm"))
                        retInfo.FirstName = ""
                        retInfo.LastName = ""
                        Return retInfo
                    Else
                        Return Nothing
                    End If
                Case "ServiceCenter"
                    dsData.ReadXml(New XmlNodeReader(GetServiceCenterDetails(InstallerUidNo, UserID)))
                    If (dsData.Tables(0).Columns.Contains("inst_uid_no") And dsData.Tables(0).Columns.Contains("inst_nm")) Then
                        retInfo.Identifier = Convert.ToString(dsData.Tables(0).Rows(0)("inst_uid_no"))
                        retInfo.Name = Convert.ToString(dsData.Tables(0).Rows(0)("inst_nm"))
                        retInfo.FirstName = ""
                        retInfo.LastName = ""
                        Return retInfo
                    Else
                        Return Nothing
                    End If
                Case "Technician"
                    dsData.ReadXml(New XmlNodeReader(GetTechnicianDetails(InstallerUidNo, ParentUidNo, UserID)))
                    retInfo.Identifier = 0
                    retInfo.Name = ""

                    If (dsData.Tables(0).Columns.Contains("last_nm")) Then
                        retInfo.LastName = dsData.Tables(0).Rows(0)("last_nm")
                    End If

                    If (dsData.Tables(0).Columns.Contains("first_nm")) Then
                        retInfo.FirstName = dsData.Tables(0).Rows(0)("first_nm")
                    End If
                    Return retInfo
                Case Else
                    Return Nothing
            End Select

        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

#Region "LandingPage"

    <WebMethod()> _
    Public Function GetDispatchDetail(ByVal lDispatchNumber As Long) As System.Xml.XmlDocument
        Dim commandText As String
        Dim connectionString As String = getConnection("INGRES")
        Dim commitTransaction As Boolean = True
        Dim transaction As DbTransaction = Nothing
        Dim dbLayer As New DataLayer(connectionString, DataLayer.DatabaseType.Ingres)
        Dim parameters As New Parameters()
        Dim sUserID As String
        'Dim lInstallerUidNo As Long
        Dim sessionTableCreated As Boolean
        Dim sActionCode As String
        Dim sTempTable As String
        Dim sStoredProc As String

        Dim dtResult As DataTable

        Dim dsResult As DataSet
        Dim dvXML As DataView
        Dim ReturnXML As String

        Dim xmlDocument As System.Xml.XmlDocument

        sTempTable = "so_dispatch_detail"
        sStoredProc = "dp_get_so_dispatch_detail"

        Try
            sUserID = "lynxweb"
            sActionCode = "GET"

            '/* Step 1: Create the session table */
            transaction = dbLayer.BeginTransaction()

            commandText = "declare global temporary table session." & sTempTable & " as " & _
            "select * from " & sTempTable & " on commit preserve rows with norecovery"
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            sessionTableCreated = True

            '/* Step 2: Insert the parameters in session table */
            commandText = "insert into session." & sTempTable & " (dispatch_no, user_id_no)" & _
            " values (" & lDispatchNumber & ", '" & sUserID & "')"

            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)

            '/* Step 3: Execute the stored Procedure */
            commandText = "{ ? = call lynxdba." & sStoredProc & "(dummy=session." & sTempTable & ") }"
            parameters.AddReturnParameter("retval", DbType.Int32)
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, parameters, transaction)
            'returnValue = parameters("retval").Value

            '/* Step 4: Retrieve the result from session table */
            commandText = "select * from session." & sTempTable & ""
            dtResult = dbLayer.ExecuteDataTable(commandText, CommandType.Text, "DispatchDetail", transaction)
            dtResult.DataSet.DataSetName = "DispatchDetail"

            'dtResult.DataSet.WriteXml("C:/VRE/NAGA Ingres/XML Data/so_dispatch_details.xml")dsResult = dtResult.DataSet

            dsResult = dtResult.DataSet
            If Not dsResult Is Nothing Then
                dvXML = New DataView(dsResult.Tables(0), "", "", DataViewRowState.CurrentRows)

                dsResult.Tables.Clear()
                dsResult.Tables.Add(dvXML.ToTable())

                ReturnXML = dsResult.GetXml()


                xmlDocument = New System.Xml.XmlDocument()
                xmlDocument.LoadXml(ReturnXML)
            Else
                xmlDocument = New System.Xml.XmlDocument()

            End If

        Catch ex As Exception
            commitTransaction = False
            Throw ex
        Finally
            '/* Remove the session table if it has been created */
            If sessionTableCreated Then
                commandText = "drop table session." & sTempTable & ""
                dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            End If

            '/* Either commit the transaction or roll it back */
            If commitTransaction Then
                dbLayer.CommitTransaction()
            Else
                dbLayer.RollbackTransaction()
            End If

            '/* Dispose of the data layer */
            dbLayer.Dispose()
        End Try

        Return xmlDocument

    End Function

    <WebMethod()> _
    Public Function GetDispatchDetailForLandingPage(ByVal lDispatchNumber As Long, ByVal sCalledFrom As String) As System.Xml.XmlDocument
        Dim commandText As String
        Dim connectionString As String = getConnection("INGRES")
        Dim commitTransaction As Boolean = True
        Dim transaction As DbTransaction = Nothing
        Dim dbLayer As New DataLayer(connectionString, DataLayer.DatabaseType.Ingres)
        Dim parameters As New Parameters()
        Dim sUserID As String
        'Dim lInstallerUidNo As Long
        Dim sessionTableCreated As Boolean
        Dim sActionCode As String
        Dim sTempTable As String
        Dim sStoredProc As String

        Dim dtResult As DataTable

        Dim dsResult As DataSet
        Dim dvXML As DataView
        Dim ReturnXML As String

        Dim xmlDocument As System.Xml.XmlDocument

        sTempTable = "so_dispatch_detail"
        sStoredProc = "dp_get_so_dispatch_detail"

        Try
            sUserID = "lynxweb"
            sActionCode = "GET"

            '/* Step 1: Create the session table */
            transaction = dbLayer.BeginTransaction()

            commandText = "declare global temporary table session." & sTempTable & " as " & _
            "select * from " & sTempTable & " on commit preserve rows with norecovery"
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            sessionTableCreated = True

            '/* Step 2: Insert the parameters in session table */
            commandText = "insert into session." & sTempTable & " (dispatch_no, user_id_no,called_from)" & _
            " values (" & lDispatchNumber & ", '" & sUserID & "', '" & sCalledFrom & "')"

            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)

            '/* Step 3: Execute the stored Procedure */
            commandText = "{ ? = call lynxdba." & sStoredProc & "(dummy=session." & sTempTable & ") }"
            parameters.AddReturnParameter("retval", DbType.Int32)
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, parameters, transaction)
            'returnValue = parameters("retval").Value

            '/* Step 4: Retrieve the result from session table */
            commandText = "select * from session." & sTempTable & ""
            dtResult = dbLayer.ExecuteDataTable(commandText, CommandType.Text, "DispatchDetail", transaction)
            dtResult.DataSet.DataSetName = "DispatchDetail"

            'dtResult.DataSet.WriteXml("C:/VRE/NAGA Ingres/XML Data/so_dispatch_details.xml")dsResult = dtResult.DataSet

            dsResult = dtResult.DataSet
            If Not dsResult Is Nothing Then
                dvXML = New DataView(dsResult.Tables(0), "", "", DataViewRowState.CurrentRows)

                dsResult.Tables.Clear()
                dsResult.Tables.Add(dvXML.ToTable())

                ReturnXML = dsResult.GetXml()


                xmlDocument = New System.Xml.XmlDocument()
                xmlDocument.LoadXml(ReturnXML)
            Else
                xmlDocument = New System.Xml.XmlDocument()

            End If

        Catch ex As Exception
            commitTransaction = False
            Throw ex
        Finally
            '/* Remove the session table if it has been created */
            If sessionTableCreated Then
                commandText = "drop table session." & sTempTable & ""
                dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            End If

            '/* Either commit the transaction or roll it back */
            If commitTransaction Then
                dbLayer.CommitTransaction()
            Else
                dbLayer.RollbackTransaction()
            End If

            '/* Dispose of the data layer */
            dbLayer.Dispose()
        End Try

        Return xmlDocument

    End Function

    <WebMethod()> _
    Public Function GetOperatingHours(ByVal lCompanyID As Long) As System.Xml.XmlDocument
        Dim commandText As String
        Dim connectionString As String = getConnection("INGRES")
        Dim commitTransaction As Boolean = True
        Dim transaction As DbTransaction = Nothing
        Dim dbLayer As New DataLayer(connectionString, DataLayer.DatabaseType.Ingres)
        Dim parameters As New Parameters()
        Dim sUserID As String
        'Dim lInstallerUidNo As Long
        Dim sessionTableCreated As Boolean
        Dim sActionCode As String
        Dim sTempTable As String
        Dim sStoredProc As String

        Dim dtResult As DataTable

        Dim dsResult As DataSet
        Dim dvXML As DataView
        Dim ReturnXML As String

        Dim xmlDocument As System.Xml.XmlDocument

        sTempTable = "so_manage_operating_hours"
        sStoredProc = "dp_manage_so_operating_hours"

        Try
            sUserID = "lynxweb"
            sActionCode = "GET"

            '/* Step 1: Create the session table */
            transaction = dbLayer.BeginTransaction()

            commandText = "declare global temporary table session." & sTempTable & " as " & _
            "select * from " & sTempTable & " on commit preserve rows with norecovery"
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            sessionTableCreated = True

            '/* Step 2: Insert the parameters in session table */
            commandText = "insert into session." & sTempTable & " (action_cd, commit_ind, msg_ind, user_id_no, inst_uid_no, user_session_no)" & _
            " values ('" & sActionCode & "', 'N', 'Y',  '" & sUserID & "',  " & lCompanyID & ", '" & Guid.NewGuid().ToString().Replace("-", String.Empty) & "')"

            ' commandText = "insert into session." & sTempTable & " (inst_uid_no, user_id_no)" & _
            '"values (" & lInstallerUidNo & ", '" & sUserID & "')"

            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)

            '/* Step 3: Execute the stored Procedure */
            commandText = "{ ? = call lynxdba." & sStoredProc & "(dummy=session." & sTempTable & ") }"
            parameters.AddReturnParameter("retval", DbType.Int32)
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, parameters, transaction)
            'returnValue = parameters("retval").Value

            '/* Step 4: Retrieve the result from session table */
            commandText = "select * from session." & sTempTable & ""
            dtResult = dbLayer.ExecuteDataTable(commandText, CommandType.Text, "OperationHours", transaction)
            dtResult.DataSet.DataSetName = "OperationHours"

            'TODO - Need to remove the below line.
            'dtResult.DataSet.WriteXml("C:/VRE/NAGA Ingres/XML Data/so_operating_hours.xml")

            dsResult = dtResult.DataSet
            If Not dsResult Is Nothing Then
                dvXML = New DataView(dsResult.Tables(0), "", "dow_no", DataViewRowState.CurrentRows)

                dsResult.Tables.Clear()
                dsResult.Tables.Add(dvXML.ToTable())

                ReturnXML = dsResult.GetXml()

                xmlDocument = New System.Xml.XmlDocument()
                xmlDocument.LoadXml(ReturnXML)
            Else
                xmlDocument = New System.Xml.XmlDocument()
            End If

        Catch ex As Exception
            commitTransaction = False
            Throw ex
        Finally
            '/* Remove the session table if it has been created */
            If sessionTableCreated Then
                commandText = "drop table session." & sTempTable & ""
                dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            End If

            '/* Either commit the transaction or roll it back */
            If commitTransaction Then
                dbLayer.CommitTransaction()
            Else
                dbLayer.RollbackTransaction()
            End If

            '/* Dispose of the data layer */
            dbLayer.Dispose()
        End Try
        Return xmlDocument
    End Function

    <WebMethod()> _
    Public Function IsPPGDesignated(ByVal lCompanyID As Long) As Boolean
        Dim commandText As String
        Dim connectionString As String = getConnection("INGRES")
        Dim commitTransaction As Boolean = True
        Dim transaction As DbTransaction = Nothing
        Dim dbLayer As New DataLayer(connectionString, DataLayer.DatabaseType.Ingres)
        Dim parameters As New Parameters()
        Dim sUserID As String
        Dim sReturnValue As Boolean

        sUserID = "lynxweb"
        Try
            commandText = "{? = call lynxdba.dp_get_profile_item (?,?,?,?,?,?,?,?,?,?,?,?,?) }"
            parameters.AddReturnParameter("retval", DbType.Int32)
            parameters.AddInputOutputParameter("h_msg", String.Empty, DbType.String, 200)
            parameters.AddInputOutputParameter("h_msg_ind", "N", DbType.String, 1)
            parameters.AddInputOutputParameter("h_user_id_no", sUserID, DbType.String, 32)
            parameters.AddInputOutputParameter("h_user_session_id", Guid.NewGuid().ToString().Replace("-", String.Empty), DbType.String, 32)
            parameters.AddInputOutputParameter("h_attrib_amt", 0, DbType.Decimal)
            parameters.AddInputOutputParameter("h_attrib_cd", String.Empty, DbType.String, 50)
            parameters.AddInputOutputParameter("h_attrib_dt", DateTime.Now.ToString("MM/dd/yyyy"), DbType.Date)
            parameters.AddInputOutputParameter("h_attrib_flt_no", 0, DbType.Double)
            parameters.AddInputOutputParameter("h_attrib_nm", "arg_alliance_member_ind", DbType.String, 50)
            parameters.AddInputOutputParameter("h_attrib_no", 0, DbType.Int32)
            parameters.AddInputOutputParameter("h_commit_ind", "Y", DbType.String, 1)
            parameters.AddInputOutputParameter("h_profile_object", "inst", DbType.String, 32)
            parameters.AddInputOutputParameter("h_profile_uid_no", lCompanyID, DbType.Int32)

            transaction = dbLayer.BeginTransaction()
            dbLayer.ExecuteNonQuery(commandText, CommandType.StoredProcedure, parameters, transaction)

            If parameters("h_attrib_cd").Value = "Y" Then
                sReturnValue = True
            Else
                sReturnValue = False
            End If

        Catch ex As Exception
            commitTransaction = False
            Throw ex
        Finally
            '/* Either commit the transaction or roll it back */
            If commitTransaction Then
                dbLayer.CommitTransaction()
            Else
                dbLayer.RollbackTransaction()
            End If

            '/* Dispose of the data layer */
            dbLayer.Dispose()
        End Try
        Return sReturnValue
    End Function

    <WebMethod()> _
    Public Function GetCompnayURL(ByVal lCompanyID As Long) As String
        Dim commandText As String
        Dim connectionString As String = getConnection("INGRES")
        Dim commitTransaction As Boolean = True
        Dim transaction As DbTransaction = Nothing
        Dim dbLayer As New DataLayer(connectionString, DataLayer.DatabaseType.Ingres)
        Dim parameters As New Parameters()
        Dim sUserID As String
        Dim sReturnValue As String

        sUserID = "lynxweb"
        Try
            commandText = "{? = call lynxdba.dp_get_profile_item (?,?,?,?,?,?,?,?,?,?,?,?,?) }"
            parameters.AddReturnParameter("retval", DbType.Int32)
            parameters.AddInputOutputParameter("h_msg", String.Empty, DbType.String, 200)
            parameters.AddInputOutputParameter("h_msg_ind", "N", DbType.String, 1)
            parameters.AddInputOutputParameter("h_user_id_no", sUserID, DbType.String, 32)
            parameters.AddInputOutputParameter("h_user_session_id", Guid.NewGuid().ToString().Replace("-", String.Empty), DbType.String, 32)
            parameters.AddInputOutputParameter("h_attrib_amt", 0, DbType.Decimal)
            parameters.AddInputOutputParameter("h_attrib_cd", String.Empty, DbType.String, 50)
            parameters.AddInputOutputParameter("h_attrib_dt", DateTime.Now.ToString("MM/dd/yyyy"), DbType.Date)
            parameters.AddInputOutputParameter("h_attrib_flt_no", 0, DbType.Double)
            parameters.AddInputOutputParameter("h_attrib_nm", "company_web_url", DbType.String, 50)
            parameters.AddInputOutputParameter("h_attrib_no", 0, DbType.Int32)
            parameters.AddInputOutputParameter("h_commit_ind", "N", DbType.String, 1)
            parameters.AddInputOutputParameter("h_profile_object", "inst", DbType.String, 32)
            parameters.AddInputOutputParameter("h_profile_uid_no", lCompanyID, DbType.Int32)

            transaction = dbLayer.BeginTransaction()
            dbLayer.ExecuteNonQuery(commandText, CommandType.StoredProcedure, parameters, transaction)

            sReturnValue = parameters("h_attrib_cd").Value
        Catch ex As Exception
            commitTransaction = False
            Throw
        Finally
            '/* Either commit the transaction or roll it back */
            If commitTransaction Then
                dbLayer.CommitTransaction()
            Else
                dbLayer.RollbackTransaction()
            End If

            '/* Dispose of the data layer */
            dbLayer.Dispose()
        End Try
        Return sReturnValue
    End Function

    <WebMethod()> _
    Public Function IsDispatchInvoiced(ByVal lDispatchNumber As Long) As Boolean
        Dim commandText As String
        Dim connectionString As String = getConnection("INGRES")
        Dim commitTransaction As Boolean = True
        Dim transaction As DbTransaction = Nothing
        Dim dbLayer As New DataLayer(connectionString, DataLayer.DatabaseType.Ingres)
        Dim parameters As New Parameters()
        Dim sUserID As String
        Dim sReturnValue As Boolean

        sUserID = "lynxweb"
        Try
            commandText = "{? = call lynxdba.dp_get_dispatch_status (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }"
            parameters.AddReturnParameter("retval", DbType.Int32)
            parameters.AddInputParameter("h_commit_ind", "Y", DbType.String, 1)
            parameters.AddInputParameter("msg_ind", "N", DbType.String, 1)
            parameters.AddInputOutputParameter("msg_text", "", DbType.String, 200)
            parameters.AddInputOutputParameter("user_id_no", sUserID, DbType.String, 10)
            parameters.AddInputOutputParameter("user_session_no", Guid.NewGuid().ToString().Replace("-", String.Empty), DbType.String, 32)
            parameters.AddInputParameter("h_user_type_no", 3, DbType.Int32)
            parameters.AddInputOutputParameter("h_bill_dt", "", DbType.String, 26)
            parameters.AddInputOutputParameter("h_check_no", "", DbType.String, 10)
            parameters.AddInputOutputParameter("h_check_dt", "", DbType.String, 26)
            parameters.AddInputOutputParameter("h_claim_type_desc", "", DbType.String, 20)
            parameters.AddInputOutputParameter("h_claim_inv_dt", "", DbType.String, 26)
            parameters.AddInputOutputParameter("h_cleared_dt", "", DbType.String, 26)
            parameters.AddInputOutputParameter("h_co_ind", "", DbType.String, 1)
            parameters.AddInputOutputParameter("h_compet_bid_ind", "", DbType.String, 1)
            parameters.AddInputOutputParameter("h_control_no", "", DbType.String, 8)
            parameters.AddInputOutputParameter("h_deduct_amt", 0, DbType.Decimal, 10)
            parameters.AddInputOutputParameter("h_dispatch_dt", "", DbType.String, 26)
            parameters.AddInputParameter("h_dispatch_no", 0, DbType.Int64)
            parameters.AddInputOutputParameter("h_dispatch_status", 0, DbType.Int32)
            parameters.AddInputOutputParameter("h_dispatch_status_msg", "", DbType.String, 200)
            parameters.AddInputOutputParameter("h_ediproc_dt", "", DbType.String, 26)
            parameters.AddInputOutputParameter("h_error_desc", "", DbType.String, 90)
            parameters.AddInputOutputParameter("h_fax_resend_no", 0, DbType.Int32)
            parameters.AddInputOutputParameter("h_ins_co_uid_no", 0, DbType.Int32)
            parameters.AddInputOutputParameter("h_ins_co_nm", "", DbType.String, 35)
            parameters.AddInputOutputParameter("h_inst_nm", "", DbType.String, 30)
            parameters.AddInputOutputParameter("h_inst_addr_1st", "", DbType.String, 30)
            parameters.AddInputOutputParameter("h_inst_addr_2nd", "", DbType.String, 30)
            parameters.AddInputOutputParameter("h_inst_city_nm", "", DbType.String, 20)
            parameters.AddInputOutputParameter("h_inst_phone_no ", "", DbType.String, 13)
            parameters.AddInputOutputParameter("h_inst_state_cd", "", DbType.String, 2)
            parameters.AddInputOutputParameter("h_inst_zip_cd", "", DbType.String, 9)
            parameters.AddInputOutputParameter("h_insrd_nm", "", DbType.String, 42)
            parameters.AddInputOutputParameter("h_inst_inv_no", "", DbType.String, 10)
            parameters.AddInputOutputParameter("h_job_type", "", DbType.String, 1)
            parameters.AddInputOutputParameter("h_lc_job_type_cd", 0, DbType.Int32)
            parameters.AddInputOutputParameter("h_lc_job_type_cd_desc", "", DbType.String, 40)
            parameters.AddInputOutputParameter("h_max_fax_resend_no", 0, DbType.Int32)
            parameters.AddInputOutputParameter("h_paymt_amt", 0, DbType.Decimal, 10)
            parameters.AddInputOutputParameter("h_pgm_mbr_status_desc", "", DbType.String, 30)
            parameters.AddInputOutputParameter("h_resubmit_dt", "", DbType.String, 26)
            parameters.AddInputOutputParameter("h_sched_appt_dt", "", DbType.String, 26)
            parameters.AddInputOutputParameter("h_vehicle_desc", "", DbType.String, 80)

            transaction = dbLayer.BeginTransaction()
            dbLayer.ExecuteNonQuery(commandText, CommandType.StoredProcedure, parameters, transaction)

            If parameters("h_claim_inv_dt").Value = "" Then
                sReturnValue = False
            Else
                sReturnValue = True
            End If

        Catch ex As Exception
            commitTransaction = False
            Throw ex
        Finally
            '/* Either commit the transaction or roll it back */
            If commitTransaction Then
                dbLayer.CommitTransaction()
            Else
                dbLayer.RollbackTransaction()
            End If

            '/* Dispose of the data layer */
            dbLayer.Dispose()
        End Try
        Return sReturnValue
    End Function

    <WebMethod()> _
    Public Function GetGlassDamageInfo(ByVal lDispatchNumber As Long) As System.Xml.XmlDocument
        Dim profileData As New DataSet()
        Dim xmlDoc As New XmlDocument()
        Dim commandText As String
        Dim connectionString As String = getConnection("INGRES")
        Dim commitTransaction As Boolean = True
        Dim transaction As DbTransaction = Nothing
        Dim dbLayer As New DataLayer(connectionString, DataLayer.DatabaseType.Ingres)
        Dim parameters As New Parameters()
        Dim sUserID As String
        'Dim lInstallerUidNo As Long
        Dim sessionTableCreated As Boolean
        Dim sActionCode As String
        Dim sTempTable As String
        Dim sStoredProc As String

        Dim dtResult As DataTable

        Dim dsResult As DataSet
        Dim dvXML As DataView
        Dim ReturnXML As String

        Dim xmlDocument As System.Xml.XmlDocument

        sTempTable = "so_glass_damage"
        sStoredProc = "dp_get_so_glass_damage"

        Try

            sUserID = "lynxweb"
            sActionCode = "GET"

            '/* Step 1: Create the session table */
            transaction = dbLayer.BeginTransaction()

            commandText = "declare global temporary table session." & sTempTable & " as " & _
            "select * from " & sTempTable & " on commit preserve rows with norecovery"
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            sessionTableCreated = True

            '/* Step 2: Insert the parameters in session table */
            commandText = "insert into session." & sTempTable & " (commit_ind, msg_ind, user_id_no, dispatch_no, user_session_no, msg_text)" & _
            " values ('Y', 'N',  '" & sUserID & "',  " & lDispatchNumber & ", '" & Guid.NewGuid().ToString().Replace("-", String.Empty) & "','GLASS_DAMAGE_DATA')"

            ' commandText = "insert into session." & sTempTable & " (inst_uid_no, user_id_no)" & _
            '"values (" & lInstallerUidNo & ", '" & sUserID & "')"

            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)

            '/* Step 3: Execute the stored Procedure */
            commandText = "{ ? = call lynxdba." & sStoredProc & "(dummy=session." & sTempTable & ") }"
            parameters.AddReturnParameter("retval", DbType.Int32)
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, parameters, transaction)
            'returnValue = parameters("retval").Value

            '/* Step 4: Retrieve the result from session table */
            commandText = "select * from session." & sTempTable & ""
            dtResult = dbLayer.ExecuteDataTable(commandText, CommandType.Text, "GlassDamage", transaction)
            dtResult.DataSet.DataSetName = "GlassDamage"

            'TODO - Need to remove the below line.
            'dtResult.DataSet.WriteXml("C:/VRE/NAGA Ingres/XML Data/so_operating_hours.xml")


            dsResult = dtResult.DataSet
            If Not dsResult Is Nothing Then

                dvXML = New DataView(dsResult.Tables(0), "", "", DataViewRowState.CurrentRows)
                dsResult.Tables.Clear()
                dsResult.Tables.Add(dvXML.ToTable())

                ReturnXML = dsResult.GetXml()

                xmlDocument = New System.Xml.XmlDocument()
                xmlDocument.LoadXml(ReturnXML)
            Else
                xmlDocument = New System.Xml.XmlDocument()
            End If

        Catch ex As Exception
            commitTransaction = False
            Throw ex
        Finally
            '/* Remove the session table if it has been created */
            If sessionTableCreated Then
                commandText = "drop table session." & sTempTable & ""
                dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            End If

            '/* Either commit the transaction or roll it back */
            If commitTransaction Then
                dbLayer.CommitTransaction()
            Else
                dbLayer.RollbackTransaction()
            End If

            '/* Dispose of the data layer */
            dbLayer.Dispose()
        End Try

        Return xmlDocument
    End Function

    <WebMethod()> _
    Public Function GetLogoforLandingPage(ByVal InstallerTypeID As Integer, ByVal InstallerType As String) As Byte()
        Dim imageData As Byte() = Nothing
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Try
            Select Case InstallerType
                Case "Company"
                    parms.AddInputParameter("@CompanyID", InstallerTypeID, DbType.Int32)
                    imageData = dataLayer.ExecuteScalar(VREConstants.LandingPageSQL.Procedures.spLandingPageCompanyLogoGet, CommandType.StoredProcedure, parms)
                Case "ServiceCenter"
                    parms.AddInputParameter("@ServiceCenterID", InstallerTypeID, DbType.Int32)
                    imageData = dataLayer.ExecuteScalar(VREConstants.LandingPageSQL.Procedures.spLandingPageServiceCenterLogoGet, CommandType.StoredProcedure, parms)
                Case "Technician"
                    parms.AddInputParameter("@TechnicianID", InstallerTypeID, DbType.Int32)
                    imageData = dataLayer.ExecuteScalar(VREConstants.LandingPageSQL.Procedures.spLandingPageTechnicianLogoGet, CommandType.StoredProcedure, parms)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
        Return imageData
    End Function

    <WebMethod()> _
    Public Function GetStyles(ByVal InsuranceCompanyID As Integer) As System.Xml.XmlDocument
        Dim imageData As Byte() = Nothing
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Dim dsResult As DataSet
        Dim ReturnXML As String

        Dim xmlDocument As System.Xml.XmlDocument
        Try
            parms.AddInputParameter("@InsuranceCompanyID", InsuranceCompanyID, DbType.Int32)
            dsResult = dataLayer.ExecuteDataSet(VREConstants.LandingPageSQL.Procedures.spLandingPageThemeGet, CommandType.StoredProcedure, parms)
            ReturnXML = dsResult.GetXml()

            xmlDocument = New System.Xml.XmlDocument()
            xmlDocument.LoadXml(ReturnXML)
        Catch ex As Exception
            Throw ex
        End Try
        Return xmlDocument
    End Function

    <WebMethod()> _
    Public Function GetValidDispatchNumber(ByVal lDispatchNumber As Long) As Boolean
        Dim commandText As String
        Dim connectionString As String = getConnection("INGRES")
        Dim commitTransaction As Boolean = True
        Dim transaction As DbTransaction = Nothing
        Dim dbLayer As New DataLayer(connectionString, DataLayer.DatabaseType.Ingres)
        Dim parameters As New Parameters()
        Dim sUserID As String
        Dim sessionTableCreated As Boolean
        Dim sActionCode As String
        Dim sTempTable As String
        Dim sStoredProc As String
        Dim returnValue As Integer
        Dim isValid As Boolean = True

        sTempTable = "so_dispatch_detail"
        sStoredProc = "dp_get_so_dispatch_detail"

        Try
            sUserID = "lynxweb"
            sActionCode = "GET"

            '/* Step 1: Create the session table */
            transaction = dbLayer.BeginTransaction()

            commandText = "declare global temporary table session." & sTempTable & " as " & _
            "select * from " & sTempTable & " on commit preserve rows with norecovery"
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            sessionTableCreated = True

            '/* Step 2: Insert the parameters in session table */
            commandText = "insert into session." & sTempTable & " (dispatch_no, user_id_no)" & _
            " values (" & lDispatchNumber & ", '" & sUserID & "')"

            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)

            '/* Step 3: Execute the stored Procedure */
            commandText = "{ ? = call lynxdba." & sStoredProc & "(dummy=session." & sTempTable & ") }"
            parameters.AddReturnParameter("retval", DbType.Int32)
            dbLayer.ExecuteNonQuery(commandText, CommandType.Text, parameters, transaction)
            returnValue = parameters("retval").Value

        Catch ex As Exception
            commitTransaction = False
            Throw ex
        Finally
            '/* Remove the session table if it has been created */
            If sessionTableCreated Then
                commandText = "drop table session." & sTempTable & ""
                dbLayer.ExecuteNonQuery(commandText, CommandType.Text, transaction)
            End If

            '/* Either commit the transaction or roll it back */
            If commitTransaction Then
                dbLayer.CommitTransaction()
            Else
                dbLayer.RollbackTransaction()
            End If

            '/* Dispose of the data layer */
            dbLayer.Dispose()
        End Try

        If returnValue = -1 Then
            isValid = False
        End If

        Return isValid
    End Function

    <WebMethod()> _
   Public Function SaveAccessLog(ByVal DispatchNumber As String, ByVal DispatchStatus As String, ByVal AccessDateTime As DateTime)
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters

        Try
            parms.AddInputParameter("@DispatchNumber", DispatchNumber, DbType.String)
            parms.AddInputParameter("@DispatchStatus", DispatchStatus, DbType.String)
            parms.AddInputParameter("@AccessDateTime", AccessDateTime, DbType.DateTime)
            dataLayer.ExecuteNonQuery(VREConstants.LandingPageSQL.Procedures.spSaveLandingPageAccessLog, CommandType.StoredProcedure, parms)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod()> _
Public Function GetAccessLogInfo(ByVal AccessDateTime As DateTime) As DataSet
        Dim connectionString As String = getConnection("LYNX")
        Dim dataLayer As New DataLayer(connectionString, dataLayer.DatabaseType.SQLServer)
        Dim parms As New Parameters
        Dim dsResult As DataSet
        Try
            parms.AddInputParameter("@AccessDateTime", AccessDateTime, DbType.DateTime)
            dsResult = dataLayer.ExecuteDataSet(VREConstants.LandingPageSQL.Procedures.spGetLandingPageAccessLog, CommandType.StoredProcedure, parms)
        Catch ex As Exception
            Throw ex
        End Try
        Return dsResult
    End Function

#End Region

#Region "Impersonation Functions"

    Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal userName As String, ByVal domain As String, ByVal password As String, ByVal LogonType As Integer, ByVal LogonProvider As Integer, ByRef safeToken As IntPtr) As Boolean

    Public Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Boolean

#End Region
End Class


