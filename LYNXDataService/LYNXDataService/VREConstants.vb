﻿Public Class VREConstants
    Public Class SQL
        Public Class Procedures
            Public Const ImagesByCompanyGet = "spImagesByCompanyGet"
            Public Const ImagesByServiceCenterGet = "spImagesByServiceCenterGet"
            Public Const ImagesByTechnicianGet = "spImagesByTechnicianGet"
            Public Const ApprovedImagesByCompanyGet = "spApprovedImagesByCompanyGet"
            Public Const ApprovedImagesByServiceCenterGet = "spApprovedImagesByServiceCenterGet"
            Public Const ApprovedImagesByTechnicianGet = "spApprovedImagesByTechnicianGet"
            Public Const ApprovedImagesByCompanyLandingPageGet = "spApprovedImagesByCompanyLandingGet"
            Public Const ApprovedImagesByServiceCenterLandingPageGet = "spApprovedImagesByServiceCenterLandingGet"
        End Class
        Public Class Tables

        End Class
    End Class
    Public Class Ingres
        Public Class Procedures

        End Class
        Public Class Tables

        End Class
    End Class
    Public Class LandingPageSQL
        Public Class Procedures
            Public Const spLandingPageCompanyLogoGet = "spLandingPageCompanyLogoGet"
            Public Const spLandingPageServiceCenterLogoGet = "spLandingPageServiceCenterLogoGet"
            Public Const spLandingPageTechnicianLogoGet = "spLandingPageTechnicianLogoGet"
            Public Const spLandingPageThemeGet = "spLandingPageThemeGet"
            Public Const spSaveLandingPageAccessLog = "spSaveLandingPageAccessLog"
            Public Const spGetLandingPageAccessLog = "spGetLandingPageAccessLog"
        End Class
    End Class
End Class
