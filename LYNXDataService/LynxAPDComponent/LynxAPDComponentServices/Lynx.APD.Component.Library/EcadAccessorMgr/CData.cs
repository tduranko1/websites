﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using ADODB;
using LAPDEcadAccessorMgr;
using VBA;
using Scripting;
using MSXML2;
using Lynx.APD.Component.Library.PartnerDataMgr;
using NadaWrapper;
using System.Reflection;
using ImageMagickObject;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    /// <summary>
    /// * Component LAPDEcadAccessor: Class CData
    /// *
    /// * LYNX APD clients (Insurance Companies) require the ability to view
    /// * information about their claims. This information will be accessible from
    /// * the LYNX Services web site. For the purpose of this document, this web
    /// * site will be known as ClaimPoint.
    /// *
    /// * LYNX APD ClaimPoint will be hosted and developed by E-Commerce
    /// * Application Development (ECAD). Initially the site will be read-only and
    /// * simply provide Insurance companies access to claim information.
    /// *
    /// * This component is a simple wrapper for our data access components that
    /// * both limits ECAD's access to our database and makes that access more
    /// * convenient for them to use.
    /// *
    /// </summary>
    /// 
    class CData
    {
        #region Global Declarations
        private const string APP_NAME = "LAPDEcadAccessorMgr.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CData.");
        MLAPDEcadAccessorMgr objMLAPDEcadAccessorMgr = null;
        MDomUtils objMDomUtils = null;
        MLAPDPartnerDataMgr objMLAPDPartnerDataMgr = null;
        MBase64Utils objMBase64Utils = null;
        MEscapeUtilities objMEscapeUtilities = null;
        MRecordsetUtils objMRecordsetUtils = null;
        CNadaWrapper objNada = null;
        MagickImage imgMagick = null;
        #endregion

        #region Enumerators
        ///<summary>
        ///Internal error codes for this class.
        ///</summary>
        private enum EventCodes : ulong
        {
            eMissingParamFormat = MLAPDEcadAccessorMgr.LAPDEcadAccessorMgr_ErrorCodes.CData_CLS_ErrorCodes,
            eInvalidHTTPPostResponse
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Backup global termination
        /// </summary>
        private void Class_Terminate()
        {
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDEcadAccessorMgr.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Syntax:      objEcad.GetXML( "<Entity param1='p1' param2='p2'/>" )
        /// 
        /// Parameters:  strParamXML - the only parameter, in XML format.
        /// 
        /// Purpose:     Returns XML data required by ECAD for our ClaimPoint app.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// Returns:     The XML data, minus any reference and metadata.<returns></returns>
        public string GetXML(string strParamXML)
        {
            string PROC_NAME = string.Empty;

            //Object declarations
            XmlDocument objParamDom = null;
            XmlDocument objDataDom = null;
            XmlElement objRootElement = null;
            XmlAttribute objAttribute = null;
            XmlNodeList objNodeList = null;
            XmlElement objNode = null;
            XmlElement objVehNode = null;
            Recordset objRS = null;
            Collection colParams = null;
            CWebAssignment objWebAsgn = null;
            XmlElement objxmlElementAppend = null;
            //Variable declarations
            long lngLynxID,
            lngUserID;

            string strConnect = string.Empty,
                strEntity = string.Empty,
                strName = string.Empty,
                strDataSource = string.Empty,
                strDataSourceCode = string.Empty,
                strReturn = string.Empty,
                strLogin = string.Empty,
                strXml = string.Empty,
                strDefaultAssignmentUserID = string.Empty,
                strDefaultAssignmentUserMsg = string.Empty,
                strEmailFrom = string.Empty,
                strEmailTo = string.Empty,
                strUserName = string.Empty,
                strServiceChannel = string.Empty,
                strLossState = string.Empty,
                strInsCompany = string.Empty;
            string strApplicationCD = string.Empty, returnXml = string.Empty;
            bool blnGlassClaim;

            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMDomUtils = new MDomUtils();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                PROC_NAME = string.Concat(MODULE_NAME, "GetXML: ");

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                //Check passed parameters.
                objMLAPDEcadAccessorMgr.g_objEvents.Assert((strParamXML.Length) > 0, "Nothing passed for strParamXML.");

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Param XML = '" + strParamXML + "'", string.Concat(PROC_NAME, "Started"));

                //Initialize DOM objects.
                objParamDom = new XmlDocument();
                objDataDom = new XmlDocument();

                //Loads and validates the xml.
                objMDomUtils.LoadXml(ref objParamDom, ref strParamXML, PROC_NAME, "Parameter");

                //Get the root element of the parameter XML.
                objRootElement = objParamDom.DocumentElement;

                //The entity name is the element name.
                strEntity = objRootElement.Name;

                //* Entity Specific Code

                //* GetWebAssignment must point to the partner DB
                if (strEntity == "GetWebAssignmentXML")
                    objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.ePartner_Recordset);
                else if (strEntity == "Status")
                    objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);
                else
                    objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                //* for claim search we need to tweak passed
                //* params to match stored procedure call.
                if (strEntity == "ClaimSearch")
                {
                    if ((objRootElement.SelectSingleNode("@InsuredNameLast")).InnerText.Length > 0)
                    {
                        objMDomUtils.RenameAttribute(ref objRootElement, "@InsuredNameLast", "InvolvedNameLast");
                        objMDomUtils.AddAttribute(ref objRootElement, "InvolvedTypeCD", "I");
                    }
                }
                else if ((objRootElement.SelectSingleNode("@ClaimantNameLast")).InnerText.Length > 0)
                {
                    objMDomUtils.RenameAttribute(ref objRootElement, "@ClaimantNameLast", "InvolvedNameLast");
                    objMDomUtils.AddAttribute(ref objRootElement, "InvolvedTypeCD", "O");
                }
                //* End Entity Specific Code
                //***************************************************
                //Get the procedure name from the config XML.
                strName = objMLAPDPartnerDataMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity));

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Entity = '" + strEntity + "' strName = '" + strName + "'", string.Concat(PROC_NAME, "Before DB Execute."));

                //Create a collection of all the attributes of the root element.
                colParams = new Collection();

                //* Entity Specific Code
                //* GetWebAssignment expects recordset and bypasses
                //* validation and extraction code.

                if (strEntity == "GetWebAssignmentXML")
                {
                    //Convert attributes into parameters.
                    foreach (XmlAttribute objAttributeWebAssignment in objRootElement.Attributes)
                        colParams.Add(Uri.UnescapeDataString(objAttributeWebAssignment.Value), string.Concat("@", objAttributeWebAssignment.Name));

                    //Get the XML from the database.
                    objRS = objMLAPDEcadAccessorMgr.g_objDataAccessor.OpenRecordsetSpNpCol(strName, colParams);
                    returnXml = objRS.Fields["xml"].ToString();
                    objRS.Close();
                    objRS = null;
                }
                //Status / version # code
                else if (strEntity == "Status")
                {
                    //Convert attributes into parameters.
                    foreach (XmlAttribute objAttributeStatus in objRootElement.Attributes)
                        colParams.Add(Uri.UnescapeDataString(objAttributeStatus.Value), string.Concat("@", objAttributeStatus.Name));

                    //Get the XML from the database.
                    objRS = objMLAPDEcadAccessorMgr.g_objDataAccessor.OpenRecordsetSpNpCol(strName, colParams);
                    returnXml = BuildStatusXml(objRS);
                    objRS.Close();
                    objRS = null;
                }
                //Web Assignment - this calls the CWebAssignent class to do the work.
                else if (strEntity == "WebAssignment")
                {
                    //Create and call the WebAssignment class to submit the assignment.
                    //This returns the new LYNX ID.

                    objWebAsgn = new CWebAssignment();

                    // HACK: add claim submission to AGC as a separate transaction.  Later this will be included in the ME assignment process.
                    blnGlassClaim = Convert.ToBoolean(objParamDom.SelectSingleNode("WebAssignment/@SubmitGlassClaim").InnerText == "true");

                    if (blnGlassClaim)
                    {
                        returnXml = objWebAsgn.DoGlassSubmission(objParamDom);
                        objWebAsgn = null;
                    }
                    else
                    {
                        strDataSource = objParamDom.SelectSingleNode("//DataSource").InnerText;
                        strDataSourceCode = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Name='" + strDataSource + "']/@Code");
                        lngLynxID = objWebAsgn.DoAssignment(objParamDom, strDataSourceCode);

                        if (lngLynxID != 0)    //Generate the web assignment report and attach to the first vehicle (in this assignment)
                            strReturn = objWebAsgn.SaveWAReport(ref objParamDom, lngLynxID);

                        objWebAsgn = null;

                        //Use the new LYNX ID to get the claim aspect XML.
                        //A zero LynxID indicates that an error happened in DoAssignment
                        //it was logged and supressed internally.
                        if (lngLynxID != 0)
                            objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                        // Combine the document id returned with the vehicle list. we will split this on the asp and use it with the old styling
                        strXml = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(
                                           objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/GetVehicleListSP"), new object[] { 
                                                 "@LynxID", lngLynxID, 
                                                 "@InsuranceCompanyID", objParamDom.SelectSingleNode("//InsuranceCompanyID").InnerText, true});

                        // Load returned data into a DOM
                        objMDomUtils.LoadXml(ref objDataDom, ref strXml, "GetXML", "VehicleList");

                        // For FNOLWebAssignment make call to add Claim Owner contact info to xml
                        if (strDataSourceCode == "FNOL")
                            AddClaimOwnerInfoToXML(objDataDom, lngLynxID);

                        // Check for default_assignment user
                        strDefaultAssignmentUserID = objDataDom.SelectSingleNode("/Root/@DefaultAssignmentUserID").InnerText;

                        // APD system has a default assignment user.
                        if (strDefaultAssignmentUserID != string.Empty)
                            //Now check if the default user is part of any vehicles of this claim
                            objNodeList = objDataDom.SelectNodes(string.Concat("//ClaimAspectOwner[@UserID='", strDefaultAssignmentUserID, "']/.."));
                        if (objNodeList != null)
                        {
                            if (objNodeList.ToString().Length > 0)
                            {
                                foreach (XmlNode objNodeClaimAspectOwner in objNodeList)
                                {
                                    strUserName = string.Empty;
                                    objVehNode = (XmlElement)objNodeClaimAspectOwner.SelectSingleNode("ClaimAspectOwner");
                                    if (objVehNode != null)
                                        strUserName = string.Concat(" [", objVehNode.GetAttribute("UserNameFirst"), " ", objVehNode.GetAttribute("UserNameLast"), "]");

                                    strDefaultAssignmentUserMsg = string.Concat(strDefaultAssignmentUserMsg, "  * Vehicle ", objNodeClaimAspectOwner.Attributes["VehicleNumber"].ToString(), " Owner ",
                                                                           strUserName, Environment.NewLine,
                                                                          "    - Service channel: ", objNodeClaimAspectOwner.Attributes["CurrentServiceChannelName"].ToString(), Environment.NewLine);
                                }
                            }
                        }

                        objNodeList = objDataDom.SelectNodes("//ClaimAspectOwner[@AnalystUserID='" + strDefaultAssignmentUserID + "']/..");
                        if (objNodeList != null)
                        {
                            if (objNodeList.ToString().Length > 0)
                            {
                                foreach (XmlNode objNodeClaimAspectAnalyst in objNodeList)
                                {
                                    strUserName = string.Empty;
                                    objVehNode = (XmlElement)objNodeClaimAspectAnalyst.SelectSingleNode("ClaimAspectOwner");
                                    if (objVehNode != null)
                                        strUserName = string.Concat(" [", objVehNode.GetAttribute("AnalystUserNameFirst"), " ", objVehNode.GetAttribute("AnalystUserNameLast"), "]");

                                    strDefaultAssignmentUserMsg = string.Concat(strDefaultAssignmentUserMsg, "  * Vehicle ", objNodeClaimAspectAnalyst.Attributes["VehicleNumber"].ToString(), " Analyst ",
                                                                           strUserName, Environment.NewLine,
                                                                          "    - Service channel: ", objNodeClaimAspectAnalyst.Attributes["CurrentServiceChannelName"].ToString(), Environment.NewLine);
                                }
                            }
                        }

                        objNodeList = objDataDom.SelectNodes("//ClaimAspectOwner[@SupportUserID='" + strDefaultAssignmentUserID + "']/..");
                        if (objNodeList != null)
                        {
                            if (objNodeList.Count > 0)    //TODO: verify  objNodeList.length changed to objNodeList.Count is Correct?
                            {
                                foreach (XmlNode objNodeClaimAspectSupportUser in objNodeList)
                                {
                                    strUserName = string.Empty;
                                    objVehNode = (XmlElement)objNodeClaimAspectSupportUser.SelectSingleNode("ClaimAspectOwner");
                                    if (objVehNode != null)
                                        strUserName = string.Concat(" [", objVehNode.GetAttribute("SupportUserNameFirst"), " ", objVehNode.GetAttribute("SupportUserNameLast"), "]");

                                    strDefaultAssignmentUserMsg = string.Concat(strDefaultAssignmentUserMsg, "  * Vehicle ", objNodeClaimAspectSupportUser.Attributes["VehicleNumber"].ToString(), " Support ",
                                                                           strUserName, Environment.NewLine,
                                                                           "    - Service channel: ", objNodeClaimAspectSupportUser.Attributes["CurrentServiceChannelName"].ToString(), Environment.NewLine);
                                }
                            }
                        }

                        if (strDefaultAssignmentUserMsg != string.Empty)
                        {
                            strDefaultAssignmentUserMsg = string.Concat("The following entities have been assigned to the default user:", Environment.NewLine,
                                                                   "Environment: ", objMLAPDPartnerDataMgr.GetConfig("../@name"), Environment.NewLine,
                                                                   "LYNX ID: ", lngLynxID.ToString(), Environment.NewLine,
                                                                   "Insurance Company: ", objDataDom.SelectSingleNode("/Root/@InsuranceCompanyName").InnerText, Environment.NewLine,
                                                                   "Loss State: ", objDataDom.SelectSingleNode("/Root/@LossState").InnerText, Environment.NewLine,
                                                                   strDefaultAssignmentUserMsg);

                            strEmailFrom = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/DefaultAssignmentNotification/From");
                            strEmailTo = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/DefaultAssignmentNotification/To");

                            // check if the Email From was defined in the config. Else use some default
                            if (strEmailFrom == string.Empty)
                                strEmailFrom = "lynxserv@lynxservices.com";

                            // check if the Email From was defined in the config. Else use some default
                            if (strEmailTo == string.Empty)
                                strEmailTo = "dl-apditgroup@ppg.com";

                            objMLAPDEcadAccessorMgr.g_objEvents.SendEmail(strEmailFrom, strEmailTo, "Warning:  Incoming APD Assignment not Assigned!  No available user found!",
                                                  strDefaultAssignmentUserMsg, false);
                        }

                        // If a Web Assignment Document was created, add the path to the returned xml.
                        if (strReturn != string.Empty)
                        {
                            objxmlElementAppend = objDataDom.DocumentElement;
                            objMDomUtils.AddAttribute(ref objxmlElementAppend, "WebAssignmentDocument", strReturn);
                            returnXml = objDataDom.OuterXml;
                        }
                        else
                            returnXml = "<Error Description='Error occurred during assignment'/>";
                    }
                }

            //Extracts an APD configuration value.
                //Example Query: <Config Path='ClaimPoint/HelpdeskEmail' />
                //Example Return: <Config>helpdesk@lynxservices.com</Config>

                else if (strEntity == "NADAValuation")
                    returnXml = GetVehicleValueFromNADA(objParamDom, false);
                else if (strEntity == "Config")
                    returnXml = string.Concat("<Config>", objMLAPDPartnerDataMgr.GetConfig(objParamDom.SelectSingleNode("//@Path").InnerText), "</Config>");
                else if (strEntity == "ClaimSearch")
                {
                    // Execute database procedure to retrieve requested data
                    ExecuteSPXML(objDataDom, objRootElement, strName, strEntity);

                    if (objRootElement.SelectSingleNode("@InvolvedTypeCD").InnerText == "I")
                    {
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedNameLast", "InsuredNameLast");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedNameFirst", "InsuredNameFirst");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedLastName", "InsuredNameLast");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedFirstName", "InsuredNameFirst");
                    }
                    else if (objRootElement.SelectSingleNode("@InvolvedTypeCD").InnerText == "O")
                    {
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedNameLast", "ClaimantNameLast");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedNameFirst", "ClaimantNameFirst");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedLastName", "ClaimantNameLast");
                        objMDomUtils.RenameAllAttributes(ref objRootElement, "InvolvedFirstName", "ClaimantNameFirst");
                    }
                    //Now return the data XML.
                    returnXml = objDataDom.OuterXml;
                }
                else if (strEntity == "UserSessionDetail")
                {
                    strApplicationCD = objParamDom.SelectSingleNode("//@ApplicationCD").InnerText;

                    // Execute database procedure to retrieve requested data
                    ExecuteSPXML(objDataDom, objRootElement, strName, strEntity);

                    // Now return the data XML.
                    return objDataDom.OuterXml;
                }
                // * End Entity Specific Code
                else if (strEntity == "FNOLService")
                    returnXml = GetFNOLData(objRootElement);
                else
                {
                    // Execute database procedure to retrieve requested data
                    ExecuteSPXML(objDataDom, objRootElement, strName, strEntity);

                    //Now return the data XML.
                    returnXml = objDataDom.OuterXml;
                }
                //Add another trace message to note exit?
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("Return = ", returnXml), string.Concat(PROC_NAME, "Finished"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objRS != null)
                {
                    objRS.Close();
                    objRS = null;
                }

                objMLAPDEcadAccessorMgr.TerminateGlobals();
            }
            return returnXml;
        }

        /// <summary>
        ///  Procedure : ExecuteSPXML
        ///  DateTime  : 3/1/2005 08:20
        ///  Author    : Dan Price
        ///  Purpose   : Generic function to execute a stored proc and return xml.
        /// </summary>
        /// <param name="objDataDom"></param>
        /// <param name="objRootElement"></param>
        /// <param name="strProcName"></param>
        /// <param name="strEntity"></param>
        private void ExecuteSPXML(XmlDocument objDataDom, XmlElement objRootElement, string strProcName, string strEntity)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "ExecuteSPXML");

            XmlAttribute objAttribute = null;
            XmlNodeList objNodeList = null;
            XmlNode objNode = null;
            Collection colParams = null;
            string strDataXML = string.Empty,
                strExtract = string.Empty;
            string strAppend = string.Empty;
            object objAppend = null;
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMDomUtils = new MDomUtils();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMEscapeUtilities = new MEscapeUtilities();

                //Create a collection of all the attributes of the root element.
                colParams = new Collection();

                //Convert attributes into parameters.
                foreach (XmlAttribute objAttributeParam in objRootElement.Attributes)
                {
                    strAppend = objAttributeParam.Value;
                    colParams.Add(objMEscapeUtilities.SQLQueryString(objMEscapeUtilities.UnEscapeString(ref strAppend)), string.Concat("@", objAttributeParam.Name));
                }
                //Get the XML from the database.
                strDataXML = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNpColXML(strProcName, colParams);

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Data XML = '" + strDataXML, PROC_NAME + "After DB Execute.");

                //Loads and validates the xml.
                objMDomUtils.LoadXml(ref objDataDom, ref strDataXML, PROC_NAME, "data");

                //Get the root element of the data XML.
                objRootElement = objDataDom.DocumentElement;

                //Uses the map file to convert codes to reference values.
                objAppend = objMLAPDEcadAccessorMgr.g_objEvents;
                objMDomUtils.MapAPDReferenceData(ref objDataDom, string.Concat(objMLAPDEcadAccessorMgr.g_strSupportDocPath, "\\ConversionMap.xml"), ref objAppend);

                //Do we want to remove some reference data?
                strExtract = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity, "/@Extract"));

                if (strExtract != string.Empty)    //Search for and remove all metadata and reference elements.
                    objNodeList = objRootElement.SelectNodes(strExtract);

                foreach (XmlNode objNodeChildRemove in objNodeList)
                    objRootElement.RemoveChild(objNodeChildRemove);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Syntax:      objEcad.PutXML( "<Entity param1='p1' param2='p2'/>" )
        /// 
        /// Parameters:  strParamXML - the data to add, in XML format.
        /// 
        /// Purpose:     Calls APD DB stored procedures to add data to APD.
        ///              This was designed primarly for the Add Comments functionality
        ///              in ClaimPoint, but can be extended for anything.
        /// </summary>
        /// Returns:     1 For Success, 0 For Error.<param name="strParamXML"></param>
        /// <returns></returns>
        public long PutXML(string strParamXML)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "PutXML: ");

            //Object declarations
            XmlDocument objParamDom = null;
            XmlDocument objResultDom = null;
            XmlElement objRootElement = null, objElement = null;
            XmlNodeList objNodeList = null;
            CWebAssignment objWebAsgn = null;
            Collection colParams = null;

            //Variable declarations
            string strConnect = string.Empty,
                strDataSource = string.Empty,
                strDataSourceCode = string.Empty,
                strEntity = string.Empty,
                strName = string.Empty,
                strResult = string.Empty,
                strParamFormat = string.Empty;
            long returnSuccess = 0;
            string strAppend = string.Empty;
            bool blnTransaction, blnCarryResults;
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMDomUtils = new MDomUtils();
                objMEscapeUtilities = new MEscapeUtilities();

                objMLAPDEcadAccessorMgr.InitializeGlobals();

                //Start with error return code.

                //Check passed parameters.
                objMLAPDEcadAccessorMgr.g_objEvents.Assert((strParamXML).Length > 0, "Nothing passed for strParamXML.");

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Param XML = '" + strParamXML + "'", string.Concat(PROC_NAME, "Started"));

                //Initialize DOM objects.
                objParamDom = new XmlDocument();

                //Loads and validates the xml.
                objMDomUtils.LoadXml(ref objParamDom, ref strParamXML, PROC_NAME, "Passed Parameter");

                //Get the root element.
                objRootElement = objParamDom.DocumentElement;

                //The entity name is the element name.
                strEntity = objRootElement.Name;

                //* Entity specific code
                if (strEntity == "SubmitComment")
                    //Sends a comment email to the APD claims rep.
                    returnSuccess = SubmitComment(objParamDom);
                else if (strEntity == "DocumentUpload")
                    //Submits documents to the APD appliation.
                    returnSuccess = DocumentUpload(objParamDom);
                else if (strEntity == "ShopDocumentUpload")
                    //Submits documents to the APD appliation.
                    returnSuccess = ShopDocumentUpload(objParamDom);
                else if (strEntity == "WebAssignment")
                {
                    //Create and call the WebAssignment class to submit the assignment.
                    //This returns the new LYNX ID.

                    strDataSource = objParamDom.SelectSingleNode("//DataSource").InnerText;
                    strDataSourceCode = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Name='" + strDataSource + "']/@Code");
                    objWebAsgn = new CWebAssignment();
                    returnSuccess = objWebAsgn.DoAssignment(objParamDom, strDataSourceCode);
                    objWebAsgn = null;
                }
                else if (strEntity == "APDRemittance")
                {
                    // Return success
                    // Process will eventually pull an xsl from the config and convert the param xml into some sort of report
                    // format and email it to accounting.  The accounting recipient list will also reside in the config file.

                    if (ProcessAPDRemittance(objParamDom))
                        returnSuccess = 1;
                    else
                        returnSuccess = 0;

                    //* End Entity Specific Code
                }
                else
                {
                    //Multiple Put Calls as child elements of Root
                    if (strEntity == "Root")
                    {
                        objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                        //Does this series of calls require a transaction?
                        blnTransaction = Convert.ToBoolean(objRootElement.SelectSingleNode("@Transaction").InnerText == "1");
                        //Do the results of one call carry over to the next?
                        blnCarryResults = Convert.ToBoolean(objRootElement.SelectSingleNode("@CarryResults").InnerText == "1");

                        //Create a DOM document for SP call results.
                        objResultDom = new XmlDocument();
                        strResult = string.Empty;

                        if (blnTransaction)
                            objMLAPDEcadAccessorMgr.g_objDataAccessor.BeginTransaction();

                        //Loop through all the elements in the passed Root.
                        foreach (XmlElement objElementforchild in objRootElement.ChildNodes)
                        {
                            //The sub-entity name is the element name.
                            strEntity = objElementforchild.Name;

                            //Get the procedure name from the config XML.
                            strName = objMLAPDPartnerDataMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity));
                            strParamFormat = objMLAPDPartnerDataMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity, "/@ParamFormat"));

                            //Add a trace mesage to the debug log.
                            if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                objMLAPDEcadAccessorMgr.g_objEvents.Trace("Entity = '" + strEntity + "' Proc Name = '" + strName + "'", string.Concat(PROC_NAME, "Before DB Execute."));

                            //Create the params collection.
                            colParams = new Collection();

                            if ((strParamFormat).ToUpper() == "ELEMENTS")
                            {
                                //Loop through all the attributes in the passed element.
                                foreach (XmlElement objParamElementforNode in objElement.ChildNodes)
                                {
                                    strAppend = objParamElementforNode.NodeType.ToString();
                                    colParams.Add(objMEscapeUtilities.SQLQueryString(objMEscapeUtilities.UnEscapeString(ref strAppend)), string.Concat("@", objParamElementforNode.Name));
                                }
                            }
                            else if ((strParamFormat).ToUpper() == "ATTRIBUTES")
                            {
                                //Convert attributes into parameters.
                                foreach (XmlAttribute objAttributes in objElement.Attributes)
                                {
                                    strAppend = objAttributes.Value;
                                    colParams.Add(objMEscapeUtilities.SQLQueryString(objMEscapeUtilities.UnEscapeString(ref strAppend)), string.Concat("@", objAttributes.Name));
                                }
                            }
                            else
                                throw new Exception(string.Concat(EventCodes.eMissingParamFormat.ToString(), "", "ParamFormat in SPMap Configuration should have been Attributes or Elements"));

                            //Convert last returned XML into parameters.
                            if (blnCarryResults && strResult != string.Empty)
                            {
                                //Select all attributes in returned XML
                                objNodeList = objResultDom.SelectNodes("//@*");

                                //Ignore errors from duplicates
                                //Add a parameter for each attribute.

                                foreach (XmlNode objNodeforlist in objNodeList)
                                    colParams.Add(objMEscapeUtilities.SQLQueryString(objNodeforlist.Value), string.Concat("@", objNodeforlist.Name));
                                //Reenable error handling.
                            }

                            //Put the XML to the database.  Assume results in XML.
                            strResult = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNpColXML(strName, colParams);

                            if (blnCarryResults)
                                //Load XML Results into the results DOM
                                objMDomUtils.LoadXml(ref objResultDom, ref strResult, PROC_NAME, "Returned");

                            colParams = null;
                        }
                    }
                    //Single Put Call - single element, no Root.
                    else
                    {
                        objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);

                        //Get the procedure name from the config XML.
                        strName = objMLAPDPartnerDataMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity));
                        strParamFormat = objMLAPDPartnerDataMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity, "/@ParamFormat"));

                        //Add a trace mesage to the debug log.
                        if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                            objMLAPDEcadAccessorMgr.g_objEvents.Trace("Entity = '" + strEntity + "' Proc Name = '" + strName + "'", string.Concat(PROC_NAME, "Before DB Execute."));

                        //Create the params collection.
                        colParams = new Collection();

                        if ((strParamFormat).ToUpper() == "ELEMENTS")
                        {
                            //Loop through all the attributes in the passed element.
                            foreach (XmlElement objElementforChild in objRootElement.ChildNodes)
                            {
                                strAppend = objElementforChild.NodeType.ToString();
                                colParams.Add(objMEscapeUtilities.UnEscapeString(ref strAppend), string.Concat("@", objElementforChild.Name));
                            }
                        }
                        else if ((strParamFormat).ToUpper() == "ATTRIBUTES")
                        {
                            //Convert attributes into parameters.
                            foreach (XmlAttribute objAttributefromElement in objRootElement.Attributes)
                            {
                                strAppend = objAttributefromElement.Value;
                                colParams.Add(objMEscapeUtilities.UnEscapeString(ref strAppend), string.Concat("@", objAttributefromElement.Name));
                            }
                        }
                        else
                            throw new Exception(string.Concat(EventCodes.eMissingParamFormat.ToString(), "", "ParamFormat in SPMap Configuration should have been 'Attributes' or 'Elements'"));

                        //Put the XML to the database.
                        objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNpCol(strName, colParams);

                        colParams = null;
                    }
                    //Return success code.
                    returnSuccess = 1;
                }
                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Results = '" + returnSuccess + "'", string.Concat(PROC_NAME, "Finished."));

                //Do not exit - fall into the error handler below for one stop object cleanup code.
            }
            catch (Exception ex)
            {
                objMLAPDEcadAccessorMgr.g_objDataAccessor.CommitTransaction();
                throw ex;
            }

            finally
            {
                objMLAPDEcadAccessorMgr.g_objDataAccessor.RollbackTransaction();
                objMLAPDEcadAccessorMgr.TerminateGlobals();
            }
            return returnSuccess;
        }

        private string BuildStatusXml(ADODB.Recordset objRS)
        {
            string PROC_NAME = string.Empty;
            string strXml = string.Empty,
                strConnect = string.Empty;
            int intIdx = 0,
                intEnd = 0;

            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                PROC_NAME = string.Concat(MODULE_NAME, "BuildStatusXml: ");
                strXml = "<Root>";

                //APD Configuration
                strXml = string.Concat(strXml, "<Config application=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.Application, "\"");
                strXml = string.Concat(strXml, "environment=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.Environment, "\" ");
                strXml = string.Concat(strXml, "version=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.GetRawSetting("/Root/Application/@version"), "\" ");
                strXml = string.Concat(strXml, "instance=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.Instance, "\" />");

                //Database
                strXml = string.Concat(strXml, "<Database version=\"", objRS.Fields[0].Value.ToString(), "\" ");
                strConnect = objMLAPDPartnerDataMgr.GetConfig("/ConnectionStringXml");

                intIdx = strConnect.IndexOf("data source=") + ("data source=").Length;
                intEnd = strConnect.Substring(intIdx).IndexOf(";");
                strXml = string.Concat(strXml, "server=\"", strConnect.Substring(intIdx, intEnd - 1), "\" ");

                intIdx = strConnect.IndexOf("initial catalog=") + ("initial catalog=").Length;
                intEnd = strConnect.Substring(intIdx).IndexOf(";");
                strXml = string.Concat(strXml, "server=\"", strConnect.Substring(intIdx, intEnd - 1), "\" />");

                //Proxy Server
                strXml = string.Concat(strXml, "<Proxy machine=\"", objMLAPDEcadAccessorMgr.g_objEvents.mSettings.Machine, "\" ");
                strXml = string.Concat(strXml, "version=\"", Assembly.GetExecutingAssembly().GetName().Version.Major.ToString(), ".", Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString(), ".", Assembly.GetExecutingAssembly().GetName().Version.Revision.ToString(), "\" />");
                strXml = string.Concat(strXml, "</Root>");

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(strXml, string.Concat(PROC_NAME, "Finished."));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strXml;
        }

        /// <summary>
        /// Sends a comment email to the APD claims rep.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private long SubmitComment(XmlDocument objParamDom)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "SubmitComment: ");

            //Objects that need cleanup
            Collection colParams = null;
            XmlElement objRootElement = null;
            XmlDocument objResultDom = null;
            XmlAttribute objAttribute = null;

            //Primitives that don't.
            string strSubject = string.Empty,
                strFrom = string.Empty,
                strTo = string.Empty,
                strBody = string.Empty,
                strName = string.Empty,
                strEntity = string.Empty,
               strResult = string.Empty,
               strSentToClaimOwner = string.Empty;
            long lngSuccess = 0;

            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMEscapeUtilities = new MEscapeUtilities();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                //Get the root element.
                objRootElement = objParamDom.DocumentElement;

                //The entity name is the element name.
                strEntity = objRootElement.Name;

                //Create the params collection.
                colParams = new Collection();

                //Buid the collection of all the attributes of the root element.
                foreach (XmlAttribute objAttributefromRootelem in objRootElement.Attributes)
                    colParams.Add(objMEscapeUtilities.SQLQueryString(objAttributefromRootelem.Value), string.Concat("@", objAttributefromRootelem.Name));

                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                //Get the procedure name from the config XML.
                strName = objMLAPDEcadAccessorMgr.GetConfig(string.Concat("ClaimPoint/SPMap/", strEntity));

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Entity = '" + strEntity + "' Proc Name = '" + strName + "'", string.Concat(PROC_NAME, "Before DB Execute."));

                //Get the Lynx Representative info for this claim from the database.
                strResult = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNpColXML(strName, colParams);

                //Load it into a DOM document.
                objResultDom = new XmlDocument();
                objMDomUtils.LoadXml(ref objResultDom, ref strResult, PROC_NAME, "Returned");

                //Get the from and to addresses.
                strFrom = objParamDom.SelectSingleNode("//@UserEmail").InnerText;

                if ("yes" == objParamDom.SelectSingleNode("//@SendToClaimOwner").InnerText)
                    strTo = objResultDom.SelectSingleNode("//@EmailAddress").InnerText;
                else
                    strTo = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/ClaimCommentMailbox/NotificationEmail");

                //Build a subject.
                strSubject = string.Concat("ClaimPoint Comment Submission: Lynx ID = ", objParamDom.SelectSingleNode("//@LynxID").InnerText);

                //Build the body.
                strBody = string.Concat("From: ", objParamDom.SelectSingleNode("//@UserName").InnerText, Environment.NewLine
                     , Environment.NewLine, objParamDom.SelectSingleNode("//@Comment").InnerText);

                //Log a trace of everything.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("From = '" + strFrom + "'" + Environment.NewLine
                         , "  Subject = '" + strSubject + "'", Environment.NewLine
                         , "  Body = '" + strBody + "'"), string.Concat(PROC_NAME, "Email/Comment submitted through ClaimPoint"));

                //Send an email to the Lynx Rep.
                objMLAPDEcadAccessorMgr.g_objEvents.SendEmail(strFrom, strTo, strSubject, strBody);

                //Return success code.
                lngSuccess = 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lngSuccess;
        }

        /// <summary>
        /// Submits documents to the APD appliation.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private long DocumentUpload(XmlDocument objParamDom)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "DocumentUpload: ");

            //Objects that need cleanup
            Recordset objRS = null;
            FileSystemObject objFile = null;
            XmlElement objElementToAppend = null;

            //Primitives that don't.
            string strVehNum = string.Empty,
                strDocSrc = string.Empty,
                strPertainsTo = string.Empty,
                strLynxID = string.Empty,
                strPartnerTransID = string.Empty,
                strNow = string.Empty,
                strFinalName = string.Empty,
                strNotifyEvent = string.Empty,
                strDirectionalCD = string.Empty,
                strSendToCarrierStatusCD = string.Empty,
                strWarrantyFlag = string.Empty;
            string strFileName = string.Empty,
                strExtension = string.Empty,
                strUserID = string.Empty,
                strFilePath = string.Empty,
                strFinalFileName = string.Empty,
                strBMPFile = string.Empty,
               strJPGFile = string.Empty;
            long lngDocumentID = 0;
            int intSuffix = 0, intIdx = 0;
            byte[] arrBuffer;
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMBase64Utils = new MBase64Utils();
                objMEscapeUtilities = new MEscapeUtilities();
                objMRecordsetUtils = new MRecordsetUtils();

                //Build a date time stamp string
                strNow = string.Concat(DateTime.Now.ToString("yyyy-mm-dd"), " ", DateTime.Now.ToString("hh:mm:ss"));

                //Extract the Lynx ID.
                strLynxID = objParamDom.SelectSingleNode("//LynxID").InnerText;

                //Extract a vehicle number from the pertains to.
                strPertainsTo = objParamDom.SelectSingleNode("//PertainsTo").InnerText;
                if (strPertainsTo.Substring(0, 3).ToUpper() == "VEH")
                    strVehNum = strPertainsTo.Substring(4);

                //Build the document source string from what was passed OR hard coded.
                strDocSrc = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//DocumentSource", false);

                if (strDocSrc == string.Empty)
                    strDocSrc = "ClaimPoint";

                //* Store the document upload in the partner database.
                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.ePartner_Recordset);

                //Call the partner load procedure.
                objRS = objMLAPDEcadAccessorMgr.g_objDataAccessor.OpenRecordsetNamedParams(objMLAPDPartnerDataMgr.GetConfig("PartnerSettings/PartnerLoadSP"), new object[] {
                   "@LynxID", strLynxID,
                   "@AssignmentID", "0",
                   "@VehicleNumber", strVehNum,
                   "@SequenceNumber", objParamDom.SelectSingleNode("//SuppSeqNumber").InnerText,
                   "@TransactionDate", strNow,
                   "@TransactionSource", "CLMPT",
                   "@TransactionType", "DocumentUpload",
                   "@PartnerXML", objMEscapeUtilities.SQLQueryString(objParamDom.OuterXml)});

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMRecordsetUtils.DebugRS(objRS);

                strPartnerTransID = objRS.Fields["PartnerTransID"].ToString();

                objRS.Close();
                objRS = null;
                //Insert the document into the LYXN Select application.
                //Extract the user id, default to system if necessary.
                strUserID = objParamDom.SelectSingleNode("//UserID").InnerText;
                if (strUserID == string.Empty)
                    strUserID = objMLAPDPartnerDataMgr.GetConfig("WorkFlow/SysUserID");

                //Build the directory structure from the last four digits of the LynxID.
                for (intIdx = 1; intIdx <= 4; intIdx++)
                    strFileName = string.Concat(strFileName, "\\", strLynxID.Substring(strLynxID.Length - 4, 4).Substring(intIdx, 1));

                strFileName = string.Concat(strFileName, "\\");
                strExtension = objParamDom.SelectSingleNode("//FileExtension").InnerText;

                objFile = new FileSystemObject();

                // Appended to end of filename to ensure uniqueness.
                intSuffix = 1;

                //Create a unique file name for the document.
                strFileName = string.Concat(strFileName, "DU", DateTime.Now.ToString("yyyymmddhhmmss"), "LYNXID", strLynxID
                     , "V", strVehNum, "Doc");

                strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);

                strFilePath = (string.Concat(objMLAPDPartnerDataMgr.GetConfig("Document/RootDirectory"), strFinalFileName).Replace("\\\\", "\\"));

                //Create binary data from the Base64 encoded info.
                objElementToAppend = (XmlElement)objMLAPDPartnerDataMgr.GetChildNode(ref objParamDom, "//FileData", true);
                arrBuffer = (byte[])objMBase64Utils.ReadBinaryDataFromDomElement(ref objElementToAppend);

                if (strExtension.ToLower() == "bmp")
                {
                    strBMPFile = objFile.BuildPath(objFile.GetSpecialFolder(Scripting.SpecialFolderConst.TemporaryFolder).ToString(), string.Concat(objFile.GetBaseName(objFile.GetTempName()), ".bmp"));

                    //Write the binary data to a file in the document structure.
                    objMBase64Utils.WriteBinaryDataToFile(strBMPFile, ref arrBuffer);

                    if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                        objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("BMP Temp File Name = ", strBMPFile), string.Concat(PROC_NAME, "Document Created"));

                    //convert bmp to jpg
                    if (ConvertBmp2Jpg(strBMPFile))
                    {
                        strJPGFile = objFile.BuildPath(objFile.GetParentFolderName(strBMPFile), string.Concat(objFile.GetBaseName(strBMPFile), ".jpg"));
                        if (objFile.FileExists(strJPGFile))
                        {
                            if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("JPG Local File Name = ", strJPGFile), string.Concat(PROC_NAME, "Document Created"));
                            strExtension = "jpg";
                            strFilePath = objFile.BuildPath(objFile.GetParentFolderName(strFilePath), string.Concat(objFile.GetBaseName(strFilePath), ".", strExtension));
                            strFinalFileName = strFinalFileName.Replace("bmp", "jpg");
                            // If the file name construced above already exists, increment the suffix until a unique file name is found.
                            while (objFile.FileExists(strFilePath))
                            {
                                intSuffix = intSuffix + 1;
                                strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);
                                strFilePath = (string.Concat(objMLAPDPartnerDataMgr.GetConfig("Document/RootDirectory"), strFinalFileName)).Replace("\\\\", "\\");
                            }
                            objFile.MoveFile(strJPGFile, strFilePath);
                            if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("Document saved at  = ", strFilePath), string.Concat(PROC_NAME, "Document Created"));
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        //JPG conversion failed. Save it in bmp format
                        // If the file name construced above already exists, increment the suffix until a unique file name is found.
                        while (objFile.FileExists(strFilePath))
                        {
                            intSuffix = intSuffix + 1;
                            strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);
                            strFilePath = (string.Concat(objMLAPDPartnerDataMgr.GetConfig("Document/RootDirectory"), strFinalFileName)).Replace("\\\\", "\\");
                        }
                        objMBase64Utils.WriteBinaryDataToFile(strFilePath, ref arrBuffer);
                    }
                }
                else
                {
                    // If the file name construced above already exists, increment the suffix until a unique file name is found.
                    while (objFile.FileExists(strFilePath))
                    {
                        intSuffix = intSuffix + 1;
                        strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);
                        strFilePath = (string.Concat(objMLAPDPartnerDataMgr.GetConfig("Document/RootDirectory"), strFinalFileName)).Replace("\\\\", "\\");
                    }

                    //Write the binary data to a file in the document structure.
                    objMBase64Utils.WriteBinaryDataToFile(strFilePath, ref arrBuffer);
                }

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("File Name = ", strFilePath), string.Concat(PROC_NAME, "Document Created"));

                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);

                //HACK ALERT - workaround for pertains to.
                strNotifyEvent = objParamDom.SelectSingleNode("//NotifyEvent").InnerText;

                if (strNotifyEvent != "0")
                    strNotifyEvent = "1";

                strDirectionalCD = objParamDom.SelectSingleNode("//DirectionalCD").InnerText;

                if (strDirectionalCD != "O")
                    strDirectionalCD = "I";

                strSendToCarrierStatusCD = objParamDom.SelectSingleNode("//SendToCarrierStatusCD").InnerText;
                if (strSendToCarrierStatusCD != "S")
                    strSendToCarrierStatusCD = "NS";

                //Extract the Warranty Flag
                strWarrantyFlag = objParamDom.SelectSingleNode("//WarrantyFlag").InnerText;

                //Call the document insert procedure.
                //Returns the new Document ID
                lngDocumentID = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParams(
                   objMLAPDPartnerDataMgr.GetConfig("PartnerSettings/InsertSP"), new object[] {
                    "@UserID", strUserID,
                    "@PertainsTo", strPertainsTo,
                    "@LynxID", strLynxID,
                    "@NotifyEvent", strNotifyEvent,
                    "@CreatedDate", strNow,
                    "@ImageDate", strNow,
                    "@ImageLocation", strFinalFileName,
                    "@DocumentSource", strDocSrc,
                    "@DirectionalCD", strDirectionalCD,
                    "@DocumentType", objParamDom.SelectSingleNode("//DocumentType").InnerText,
                    "@SupplementSeqNumber", objParamDom.SelectSingleNode("//SuppSeqNumber").InnerText,
                    "@ExternalReference", strPartnerTransID,
                    "@ImageType", objParamDom.SelectSingleNode("//FileExtension").InnerText,
                    "@ClaimAspectServiceChannelID", objParamDom.SelectSingleNode("//ClaimAspectServiceChannelID").InnerText,
                    "@SendToCarrierStatusCD", strSendToCarrierStatusCD,
                    "@WarrantyFlag", strWarrantyFlag});

                //Possible mismatch of pertains to for 3rd party claim?
                //Try again with vehicle 2.
                if (strPertainsTo.ToUpper() == "VEH1")                                             //Todo Verify ..error number
                {
                    //Call the document insert procedure again with the new pertains to.
                    lngDocumentID = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParams(
                       objMLAPDPartnerDataMgr.GetConfig("PartnerSettings/InsertSP"), new object[] {
                        "@UserID", strUserID,
                        "@PertainsTo", "VEH2",
                        "@LynxID", strLynxID,
                        "@NotifyEvent", 1,
                        "@CreatedDate", strNow,
                        "@ImageDate", strNow,
                        "@ImageLocation", strFileName,
                        "@DocumentSource", strDocSrc,
                        "@DocumentType", objParamDom.SelectSingleNode("//DocumentType").InnerText,
                        "@SupplementSeqNumber", objParamDom.SelectSingleNode("//SuppSeqNumber").InnerText,
                        "@ExternalReference", strPartnerTransID,
                        "@ImageType", objParamDom.SelectSingleNode("//FileExtension").InnerText,
                        "@ClaimAspectServiceChannelID", objParamDom.SelectSingleNode("//ClaimAspectServiceChannelID").InnerText});
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objRS != null)
                {
                    if (objRS.State == Convert.ToInt32(ObjectStateEnum.adStateOpen))
                        objRS.Close();
                    objRS = null;
                }
            }
            return lngDocumentID;
        }

        /// <summary>
        /// Procedure : AddClaimOwnerInfoToXML
        /// DateTime  : 10/4/2004 13:20
        /// Author    : Dan Price
        /// Purpose   : Finds the owner of claim, lngLynxID, and adds that user's contact
        ///              information as node, <ClaimOwner>, to strParamXML
        /// Parameters: strParamXML - string representation of an xml document.
        ///             lngLynxID - LynxID from which Claim Owner data should be retrieved.
        /// Return    : passed in xml string with <ClaimOwner> node appended.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <param name="lngLynxID"></param>
        private void AddClaimOwnerInfoToXML(XmlDocument objParamDom, long lngLynxID)
        {
            string PROC_NAME = "AddClaimOwnerInfoToXML";
            XmlDocument objDataDom = null;
            XmlElement objElem = null;
            XmlElement objElemAppend = null;
            string strName = string.Empty,
            strDataXML = string.Empty,
            strOwnerUserID = string.Empty,
            strOwnerUserFirstName = string.Empty,
            strOwnerUserLastName = string.Empty,
            strOwnerUserPhoneAreaCode = string.Empty,
            strOwnerUserPhoneExchangeNumber = string.Empty,
            strOwnerUserPhoneUnitNumber = string.Empty,
            strOwnerUserPhoneExtensionNumber = string.Empty,
            strOwnerUserEmail = string.Empty;

            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMDomUtils = new MDomUtils();

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Param XML = '" + objParamDom.OuterXml + "'", string.Concat(PROC_NAME, "Started"));

                //Initialize DOM objects.
                objDataDom = new XmlDocument();

                //Get the procedure name from the config XML.
                strName = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/SPMap/ClaimSessionDetail");

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(" strName = '" + strName + "'", string.Concat(PROC_NAME, "Before DB Execute."));

                // Call proc to return claim session data.
                strDataXML = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(strName, new object[] { "@LynxID", lngLynxID });

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Return = '" + strDataXML + "'", string.Concat(PROC_NAME, "After DB Execute."));

                //Loads and validates the xml.
                objMDomUtils.LoadXml(ref objDataDom, ref strDataXML, PROC_NAME, "Data");

                // Grab Claim Owner contact info from the retrieved xml.
                strOwnerUserID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserID", true);
                strOwnerUserFirstName = objMLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserNameFirst", true);
                strOwnerUserLastName = objMLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserNameLast", true);
                strOwnerUserPhoneAreaCode = objMLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserPhoneAreaCode", true);
                strOwnerUserPhoneExchangeNumber = objMLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserPhoneExchangeNumber", true);
                strOwnerUserPhoneUnitNumber = objMLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserPhoneUnitNumber", true);
                strOwnerUserPhoneExtensionNumber = objMLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserExtensionNumber", true);
                strOwnerUserEmail = objMLAPDPartnerDataMgr.GetChildNodeText(ref objDataDom, "//@OwnerUserEmail", true);

                // Add a ClaimOwner element to the passed in xml.
                objElemAppend = objParamDom.DocumentElement;
                objElem = objMDomUtils.AddElement(ref objElemAppend, "ClaimOwner");

                // Add ClaimOwner info as attributes to the newly created element.
                objMDomUtils.AddAttribute(ref objElem, "UserID", strOwnerUserID);
                objMDomUtils.AddAttribute(ref objElem, "NameFirst", strOwnerUserFirstName);
                objMDomUtils.AddAttribute(ref objElem, "NameLast", strOwnerUserLastName);
                objMDomUtils.AddAttribute(ref objElem, "PhoneAreaCode", strOwnerUserPhoneAreaCode);
                objMDomUtils.AddAttribute(ref objElem, "PhoneExchangeNumber", strOwnerUserPhoneExchangeNumber);
                objMDomUtils.AddAttribute(ref objElem, "PhoneUnitNumber", strOwnerUserPhoneUnitNumber);
                objMDomUtils.AddAttribute(ref objElem, "PhoneExtensionNumber", strOwnerUserPhoneExtensionNumber);
                objMDomUtils.AddAttribute(ref objElem, "Email", strOwnerUserEmail);

                //Add another trace message to note exit.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("Return = ", objParamDom.OuterXml), string.Concat(PROC_NAME, "Finished"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetVehicleValueFromNADA(XmlDocument objParamDom, bool blnXformOPtoHTML)
        {
            string PROC_NAME = string.Empty;
            string strLossDate = string.Empty,
  strRegion = string.Empty,
  strRegionDesc = string.Empty,
  strYear = string.Empty,
  strMake = string.Empty,
  strModel = string.Empty,
  strBody = string.Empty,
  strVIN = string.Empty,
 strMileage = string.Empty,
  strZip = string.Empty,
  strNADAVehicleId = string.Empty,
  strAction = string.Empty,
  strReadOnly = string.Empty;

            try
            {
                PROC_NAME = "GetVehicleValueFromNADA";
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objNada = new CNadaWrapper();
                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Param XML = '" + objParamDom.OuterXml + "'", string.Concat(PROC_NAME, "Started"));

                // Initialize DOM objects.
                objNada = new CNadaWrapper();

                // Retrieve data from passed xml.
                strLossDate = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@LossDate", true);
                strRegion = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Region", false);
                strRegionDesc = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@RegionDesc", false);
                strYear = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Year", false);
                strMake = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Make", false);
                strModel = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Model", false);
                strBody = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Body", false);
                strVIN = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@VIN", false);
                strMileage = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Mileage", false);
                strZip = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Zip", false);
                strNADAVehicleId = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@NADAVehicleId", false);
                strAction = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@Action", true);
                strReadOnly = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ReadOnly", true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objNada.GetAllAsXML2(strLossDate, strRegion, strRegionDesc, strYear, strMake, strModel,
                                              strBody, strMileage, strVIN, strNADAVehicleId,
                                            strAction, strZip, strReadOnly, blnXformOPtoHTML);
        }

        private bool ProcessAPDRemittance(XmlDocument objParamDom)
        {
            string PROC_NAME = string.Empty;
            string strReturn = string.Empty,
 strSource = string.Empty,
 strXSLT = string.Empty,
 strFrom = string.Empty,
 strSubject = string.Empty,
 strDest = string.Empty;
            bool blnOverrideRecip = false;
            IXMLDOMDocument2 objXSLdoc = null;
            XSLTemplate40 objXSLT = null;
            IXSLProcessor objXSLProc = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, ".ProcessAPDRemittance");

                // Get the source of the remittance.
                strSource = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//InsuranceCompany", true);

                // Obtain the client specific stylesheet from the config file.
                strXSLT = string.Concat(objMLAPDEcadAccessorMgr.g_strSupportDocPath, "\\", objMLAPDPartnerDataMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/@xsl"),
                          objMLAPDPartnerDataMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/Client[@name='" + strSource + "']"), ".xsl");

                // Load the XSL into a Free Threaded DOM.
                objXSLdoc.async = false;
                objXSLdoc.load(strXSLT);

                // Load the XSL document into the XSL Template object.
                objXSLT.stylesheet = objXSLdoc;

                // Create an XSL processor from the XSL Template object.
                objXSLProc = objXSLT.createProcessor();

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("XSLT Processor created.");

                // Load the source XML into the XSL processor.
                objXSLProc.input = objParamDom;

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("XSLT Input and Parameters set.");

                // Perform the XSL transformation.
                objXSLProc.transform();

                // Retrieve the transormed XML from the XSL processor.
                strReturn = (objXSLProc.output).ToString();

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("XSLT output formatted for AGC: ", Environment.NewLine, strReturn));

                blnOverrideRecip = Convert.ToBoolean(objMLAPDPartnerDataMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/@overrideRecip") == "true");

                if (!blnOverrideRecip)
                    strDest = objMLAPDPartnerDataMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/Recipient");
                else
                    strDest = objMLAPDPartnerDataMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/OverrideRecip");

                strFrom = objMLAPDPartnerDataMgr.GetConfig("EventHandling/Email/@from");
                strSubject = objMLAPDPartnerDataMgr.GetConfig("Accounting/ElectronicInvoice/Remittance/@emailSubject");
                objMLAPDEcadAccessorMgr.g_objEvents.SendEmail(strFrom, strDest, strSubject, strReturn, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnOverrideRecip;
        }

        private long ShopDocumentUpload(XmlDocument objParamDom)
        {
            string PROC_NAME = string.Empty;
            //Objects that need cleanup
            Recordset objRS = null;
            FileSystemObject objFile = null;
            XmlElement objxmlElement = null;
            //Primitives that don't.
            string strShopLocationID = string.Empty,
              strDocSrc = string.Empty,
              strLynxID = string.Empty,
              strNow = string.Empty,
                        strFinalName = string.Empty,
              strComments = string.Empty,
              strEffectiveDate = string.Empty, strExpirationDate = string.Empty;
            string strFileName = string.Empty, strFilePath = string.Empty,
                strExtension = string.Empty,
                                strUserID = string.Empty, strCSRNo = string.Empty, strFinalFileName = string.Empty;
            int intSuffix = 0, intIdx = 0;
            byte[] arrBuffer = null;

            try
            {
                objMBase64Utils = new MBase64Utils();

                PROC_NAME = string.Concat(MODULE_NAME, "ShopDocumentUpload: ");

                //Build a date time stamp string
                strNow = string.Concat(DateTime.Now.ToString("yyyy-mm-dd"), " ", DateTime.Now.ToString("hh:mm:ss"));

                //Extract the Lynx ID.
                strShopLocationID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//ShopLocationID", true);
                strShopLocationID = string.Format("{0:0000}", strShopLocationID);

                //Build the document source string from what was passed OR hard coded.
                strDocSrc = objParamDom.SelectSingleNode("//DocumentSource").InnerText;
                if (strDocSrc == string.Empty)
                    strDocSrc = "User";

                //* Store the document upload in the partner database.
                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.ePartner_Recordset);

                //* Insert the document into the LYNX Select application.
                //Extract the user id, default to system if necessary.
                strUserID = objParamDom.SelectSingleNode("//UserID").InnerText;
                if (strUserID == string.Empty)
                    strUserID = objMLAPDPartnerDataMgr.GetConfig("WorkFlow/SysUserID");

                strCSRNo = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//CSRNo", false);
                if (strCSRNo == string.Empty)
                    strCSRNo = "CSR0000";

                strEffectiveDate = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//EffectiveDate", false);
                if (strEffectiveDate == string.Empty)
                    strEffectiveDate = "1/1/1900";

                strExpirationDate = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//ExpirationDate", false);
                if (strExpirationDate == string.Empty)
                    strExpirationDate = "12/31/2300";

                strComments = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Comments", false);

                //Build the directory structure from the last four digits of the LynxID.
                for (intIdx = 1; intIdx <= 4; intIdx++)
                    strFileName = string.Concat(strFileName, "\\\\", (strShopLocationID.Substring(strShopLocationID.Length - 4, 4).Substring(intIdx, 1)));

                strFileName = string.Concat(strFileName, "\\\\");
                //Build the path
                strExtension = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//FileExtension", true);

                objFile = new FileSystemObject();
                // Appended to end of filename to ensure uniqueness.
                intSuffix = 1;

                //Create a unique file name for the document.
                strFileName = string.Concat(strFileName, "F-", DateTime.Now.ToString("yyyymmddhhmmss"), "-", strCSRNo, "-"
                     , strShopLocationID, "-", "Doc");

                strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);
                strFilePath = (string.Concat(objMLAPDPartnerDataMgr.GetConfig("Document/ShopRootDirectory"), strFinalFileName).Replace("\\\\", "\\"));

                if (BuildPath(objFile.GetParentFolderName(strFilePath)) == false)  //exit function need to verify...
                    //error creating the path. the build path function will generate an error. no need to handle again here

                    // If the file name construced above already exists, increment the suffix until a unique file name is found.
                    while (objFile.FileExists(strFilePath))
                    {
                        intSuffix = intSuffix + 1;
                        strFinalFileName = string.Concat(strFileName, objMLAPDEcadAccessorMgr.ZeroPad(intSuffix, 4), ".", strExtension);  //TODO:verify Zeropad 
                        strFilePath = (string.Concat(objMLAPDPartnerDataMgr.GetConfig("Document/RootDirectory"), strFinalFileName)).Replace("\\\\", "\\");
                    }

                //Create binary data from the Base64 encoded info.
                objxmlElement = (XmlElement)objMLAPDPartnerDataMgr.GetChildNode(ref objParamDom, "//FileData", true);
                arrBuffer = (byte[])objMBase64Utils.ReadBinaryDataFromDomElement(ref objxmlElement);

                //Write the binary data to a file in the document structure.
                objMBase64Utils.WriteBinaryDataToFile(strFilePath, ref arrBuffer);

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("File Name = ", strFilePath), string.Concat(PROC_NAME, "Document Created"));

                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);

                //HACK ALERT - workaround for pertains to.
                //Call the document insert procedure.
                //Returns the new Document ID
                return objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParams(
                   objMLAPDPartnerDataMgr.GetConfig("PartnerSettings/ShopDocumentInsertSPName"), new object[] {
                    "@ShopLocationID", strShopLocationID,
                    "@EffectiveDate", strEffectiveDate,
                    "@ExpirationDate", strExpirationDate,
                    "@Comments", strComments,
                    "@UserID", strUserID,
                    "@ImageDate", strNow,
                    "@ImageLocation", strFinalFileName,
                    "@DocumentSource", strDocSrc,
                    "@DocumentType", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//DocumentType", true),
                    "@ImageType", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//FileExtension", true)});
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objRS != null)
                    if (objRS.State == Convert.ToInt32(ObjectStateEnum.adStateOpen))
                        objRS.Close();
                objRS = null;
            }
        }

        private bool BuildPath(string sPath)
        {
            string PROC_NAME = string.Empty, sCurPath = string.Empty;
            string[] aPath = null;
            int i;
            FileSystemObject oFSO = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "BuildPath()");

                if (sPath != string.Empty)
                {
                    oFSO = new FileSystemObject();
                    if (oFSO.FolderExists(sPath) == false)
                    {
                        if (sPath.Substring(0, 2) == "\\")
                        {
                            sCurPath = sPath.Substring(1, sPath.IndexOf("\\", 3));
                            sPath = sPath.Substring(sPath.IndexOf("\\", 3) + 1);
                            aPath = sPath.Split('\\');
                        }
                        for (i = aPath.GetLowerBound(0); i <= aPath.GetUpperBound(0); i++)
                        {
                            sCurPath = oFSO.BuildPath(sCurPath, aPath[i]);
                            if (!oFSO.FolderExists(sCurPath))
                                oFSO.CreateFolder(sCurPath);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        private string GetFNOLData(XmlElement objParamDom)
        {
            string PROC_NAME = string.Empty;
            XMLHTTP objXMLHTTPPost = null;
            IXMLDOMDocument2 objFNOLClaimXML = null;
            IXMLDOMDocument2 objFNOLClaimXSL = null;
            XmlDocument objXmlDocClaimXml = null, objXmlDocClaimXsl = null;
            string strFNOLServiceURL = string.Empty,
             strResponseText = string.Empty,
             strFNOL2APDXSLPath = string.Empty;

            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                PROC_NAME = string.Concat(MODULE_NAME, "GetFNOLData()");
                objMLAPDEcadAccessorMgr.g_objEvents.Trace("Param XML = '" + objParamDom.OuterXml + "'", string.Concat(PROC_NAME, "Started"));

                strFNOLServiceURL = objMLAPDPartnerDataMgr.GetConfig("FNOLService/URL");
                strFNOL2APDXSLPath = string.Concat(objMLAPDEcadAccessorMgr.g_strSupportDocPath, "\\", objMLAPDPartnerDataMgr.GetConfig("FNOLService/APDAssignmentXSL"));
                objMLAPDEcadAccessorMgr.g_objEvents.Trace("FNOL Service located at '" + strFNOLServiceURL + "'");

                //post the xml to the webserver
                objXMLHTTPPost = new XMLHTTP();
                objXMLHTTPPost.open("POST", strFNOLServiceURL, false);
                objXMLHTTPPost.send(objParamDom.FirstChild.OuterXml);
                strResponseText = (objXMLHTTPPost.responseText).Trim();

                objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("  POST Result", Environment.NewLine,
                                    "  ----------------", Environment.NewLine,
                                    "    Status       : ", objXMLHTTPPost.status.ToString(), Environment.NewLine,
                                    "    Response Text: ", strResponseText, Environment.NewLine));

                if (objXMLHTTPPost.status != 200) //the response was not ok. raise an error.
                    throw new Exception(string.Concat(EventCodes.eInvalidHTTPPostResponse, PROC_NAME, strResponseText));

                if (objParamDom.FirstChild.Name == "RetrieveFNOL")
                {
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat("FNOL Claim XML is converted to APD standard using ", strFNOL2APDXSLPath));

                    //need to transform the FNOL Claim to APD standard
                    objFNOLClaimXML.async = false;
                    objXmlDocClaimXml = new XmlDocument();
                    objXmlDocClaimXml = (XmlDocument)objFNOLClaimXML;
                    objMDomUtils.LoadXml(ref objXmlDocClaimXml, ref strResponseText);

                    objFNOLClaimXSL.async = false;
                    objXmlDocClaimXsl = new XmlDocument();
                    objXmlDocClaimXsl = (XmlDocument)objFNOLClaimXSL;
                    objMDomUtils.LoadXmlFile(ref objXmlDocClaimXsl, ref strFNOL2APDXSLPath, PROC_NAME, "LAPDEcadAccessorMgr");

                    strResponseText = objFNOLClaimXML.transformNode(objFNOLClaimXSL);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strResponseText;
        }

        private bool ConvertBmp2Jpg(string strFileName)
        {
            FileSystemObject objFile = null;
            bool blnSuccess = false;
            string strJPGFile = string.Empty,
                strConverted = string.Empty;

            try
            {
                if (objFile.FileExists(strFileName))
                {
                    imgMagick = new MagickImage();
                    strJPGFile = objFile.BuildPath(objFile.GetParentFolderName(strFileName), string.Concat(objFile.GetBaseName(strFileName), ".jpg"));
                    strConverted = Convert.ToString(imgMagick.Convert(new object[] { "-quality=90", strFileName, strJPGFile }));

                    if (objFile.FileExists(strJPGFile))
                        blnSuccess = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                imgMagick = null;
            }

            return blnSuccess;
        }
        #endregion
    }
}
