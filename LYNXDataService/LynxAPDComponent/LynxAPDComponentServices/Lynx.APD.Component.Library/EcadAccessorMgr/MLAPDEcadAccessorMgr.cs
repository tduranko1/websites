﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SiteUtilities;
using DataAccessor;
using System.Runtime.InteropServices;
using Microsoft.CSharp;
using Microsoft.Win32;
using Microsoft.VisualBasic;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    class MLAPDEcadAccessorMgr
    {

        #region Variable Declaration
        CData mobjCData = new CData();
        public const string APP_NAME = "LAPDEcadAccessorMgr.";
        public const long LAPDEcadAccessorMgr_FirstError = 0x80066000;

        //Global events and data access objects.
        public CEvents g_objEvents = null;
        public CDataAccessor g_objDataAccessor = null;

        public string g_strSupportDocPath = string.Empty;
        public bool g_blnDebugMode = false;

        // This counter used to ensure the globals get intialized
        // and desposed of only once.  This is required because various
        // parts of the ECAD stuff have different entry points.
        private int mintInitCount = 0;

        // Util methods to prevent modification of
        // any error information stored in the Err object.
        #endregion

        #region Enum Declaration
        public enum LAPDEcadAccessorMgr_ErrorCodes : ulong
        {
            eSQLServerErrorMin = 0x80050000,
            eSQLServerErrorMax = 0x8005FFFF,
            eSQLAssignmentIDNotFound = 0x80050072,

            //General error codes applicable to all modules of this component
            eCreateObjectExError = LAPDEcadAccessorMgr_FirstError,

            //Module specific error ranges.
            CWebAssignment_CLS_ErrorCodes = LAPDEcadAccessorMgr_FirstError + 0x80,
            CData_CLS_ErrorCodes = LAPDEcadAccessorMgr_FirstError + 0x100,
            CDocument_CLS_ErrorCodes = LAPDEcadAccessorMgr_FirstError + 0x180
        }


        public enum EDatabase : ulong
        {
            eFNOL_Recordset,
            ePartner_Recordset,
            eAPD_Recordset,
            eAPD_XML
        }
        #endregion

        #region Structure Declaration
        private struct GUID
        {
            public long Data1;
            public int Data2;
            public int Data3;
            public byte[] Data4;
        };


        public struct typError
        {
            long Number;
            string Description;
            string Source;
        };

        #endregion

        //#region Private Methods
        //[DllImport("ole32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        //private long CoCreateGuid(Guid pGUID);


        //[DllImport("ole32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        //private long StringFromGUID2(Guid pGUID, string pBuffer, long cbSize);
        //#endregion

        #region Public Methods

        ////GUID Creation Type, Constants, and APIs
        //[DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        //public void Sleep(long dwMilliseconds);

        //TODO.. 

        //public void PushErr(ref typError Error)
        //{
        //    Error.Number = Err.Number;
        //    Error.Description = Err.Description;
        //    Error.Source = Err.Source;
        //}

        //public void PopErr(ref typError Error)
        //{
        //    Err.Number = Err.Number;
        //    Err.Description = Err.Description;
        //    Err.Source = Err.Source;
        //}


        // Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
        // Parameters:  strObjectName = The object to call CreateObject for.
        // Purpose:     Wraps CreateObject() with better debug information.
        // Returns:     The Object created or Nothing.
        public object CreateObjectEx(string strObjectName)
        {
            object mobjCreateObject = null;
            Type createdObj = null;
            try
            {
                createdObj = Type.GetTypeFromProgID(strObjectName);
                if (createdObj == null)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(LAPDEcadAccessorMgr_ErrorCodes.eCreateObjectExError), ",", "", ",", "CreateObject('", strObjectName, "') returned Nothing." }));

                mobjCreateObject = Activator.CreateInstance(createdObj);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                createdObj = null;
            }

            return mobjCreateObject;
        }


        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string strParam, int intLength)
        {
            string strResult = strParam.Substring(0, intLength);
            return strResult;
        }


        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string strParam, int intLength)
        {
            string strResult = strParam.Substring(strParam.Length - intLength, intLength);
            return strResult;
        }


        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = strParam.Substring(startIndex, intLength);
            return strResult;
        }


        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = strParam.Substring(intstartIndex);
            return strResult;
        }

        //Initializes global objects and variables
        public void InitializeGlobals()
        {
            try
            {
                g_objEvents = new CEvents();
                g_objDataAccessor = new CDataAccessor();

                if (mintInitCount == 0)
                {
                    // Create DataAccessor.
                    g_objDataAccessor = new CDataAccessor();

                    // Share DataAccessor's events object.
                    g_objEvents = g_objDataAccessor.mEvents;

                    // Get path to support document directory
                    g_strSupportDocPath = string.Concat(new string[] { System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetEntryAssembly().Location), "\\", Left(APP_NAME,(APP_NAME.Length) - 1) });

                    // This will give partner data its own log file.
                    g_objEvents.ComponentInstance = "Client Access";

                    // Initialize our member components first, so we have logging set up.
                    g_objDataAccessor.InitEvents(string.Concat(new string[] {System.IO.Path.GetDirectoryName(
               System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) , @"\..\config\config.xml"}));

                    // Get config debug mode status.
                    g_blnDebugMode = g_objEvents.IsDebugMode;
                }
                mintInitCount = mintInitCount + 1;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //Terminates global objects and variables
        public void TerminateGlobals()
        {
            try
            {
                g_objEvents = new CEvents();
                g_objDataAccessor = new CDataAccessor();

                if (mintInitCount <= 1)
                {
                    g_objEvents = null;
                    g_objDataAccessor = null;
                    mintInitCount = 0;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //Returns the requested configuration setting.
        public string GetConfig(string strSetting)
        {
            string strGetConfig = string.Empty;
            try
            {
                g_objEvents = new CEvents();

                strGetConfig = g_objEvents.mSettings.GetParsedSetting(strSetting);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strGetConfig;
        }


        // Points the global data accessor object at the proper database.
        public void ConnectToDB(EDatabase eDBDetails)
        {
            string strConnect = string.Empty;
            string strConfig = string.Empty;
            try
            {
                g_objEvents = new CEvents();
                g_objDataAccessor = new CDataAccessor();

                //Get configuration settings for FNOL database connect string.
                switch (eDBDetails)
                {
                    case EDatabase.eFNOL_Recordset:
                        strConfig = @"ClaimPoint/WebAssignment/ConnectionStringStd";
                        break;
                    case EDatabase.ePartner_Recordset:
                        strConfig = @"PartnerSettings/ConnectionStringStd";
                        break;
                    case EDatabase.eAPD_Recordset:
                        strConfig = @"ConnectionStringStd";
                        break;
                    case EDatabase.eAPD_XML:
                        strConfig = @"ConnectionStringXml";
                        break;
                }
                strConnect = GetConfig(strConfig);
                g_objEvents.Assert(Convert.ToBoolean(strConnect.Length > 0), string.Concat(new string[] { "Blank connection string returned from config file: ", strConfig }));

                //Set the connect string.
                g_objDataAccessor.SetConnectString(strConnect);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //Pads a number out with zeros to make the requested number of digits.
        public string ZeroPad(long lngValue, int intDigits)
        {
            string strValue = string.Empty;
            try
            {
                strValue = Convert.ToString(lngValue);
                while (strValue.Length < intDigits)
                    strValue = string.Concat(new string[] { "0", strValue });

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strValue;
        }


        //TODO... Need to verify
        //Generates GUIDs
        //public object GenerateGUID()
        //{
        //    object objGUID = null;
        //    Guid tGuid = default(Guid);
        //    string sGuid = string.Empty;
        //    const long S_OK = 0x0;
        //    try
        //    {
        //        //create the 128 bit integer
        //        if (CoCreateGuid(tGuid) == S_OK)
        //        {
        //            //create a buffer of suitable size
        //            sGuid = Convert.ToString(80, (char)0);

        //            //convert the 128 bit integer into a Unicode string
        //            if (StringFromGUID2(tGuid, sGuid, 80) > 0)
        //                objGUID = (object)System.Text.Encoding.Unicode.GetBytes(sGuid);
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }

        //    return objGUID;
        //}    //GenerateGUID
        #endregion
    }
}
