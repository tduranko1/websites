﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ADODB;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    /// <summary>
    ///  Module MRecordsetUtils
    /// 
    ///  Utility methods for dealing with recordsets.
    /// </summary>
    class MRecordsetUtils
    {
        #region Declarations
        private const string APP_NAME = "LAPDEcadAccessorMgr.";
        private string MODULE_NAME = string.Concat(new string[] { APP_NAME, "MDomUtils." });
        #endregion

        #region Public Functions

        /// <summary>
        /// Spits recordset contents out to the debug log.
        /// </summary>
        /// <param name="rsDebugLog"></param>
        public void DebugRS(Recordset rsDebugLog)
        {
            string strAppend = string.Empty,
                strValue = string.Empty;

            try
            {
                rsDebugLog.MoveFirst();

                strAppend = string.Concat(new string[] { "Record Count = ", rsDebugLog.RecordCount.ToString(), Environment.NewLine });
                strAppend = string.Concat(new string[] { strAppend, "  Fields = " });

                foreach (Field fldExist in rsDebugLog.Fields)
                    strAppend = string.Concat(new string[] { strAppend, fldExist.Name, ", " });

                strAppend = string.Concat(new string[] { strAppend, Environment.NewLine });

                while (!rsDebugLog.EOF)
                {
                    strAppend = string.Concat(new string[] { strAppend, "  Data: " });
                    foreach (Field fld in rsDebugLog.Fields)
                    {
                        if ((fld.Value) != null)
                        {
                            strValue = ((fld.Value).ToString().Replace(Environment.NewLine, "")).Trim();
                            if (strValue.Length > 0)
                            {
                                if (strValue.Substring(0, 1) == "<" && strValue.Substring(strValue.Length - 1, 1) == ">")
                                    strAppend = string.Concat(new string[] { strAppend, "(XML NOT LOGGED), " });
                                else
                                    strAppend = string.Concat(new string[] { strAppend, "'", fld.Value.ToString(), "', " });
                            }
                            else
                            {
                                strAppend = string.Concat(new string[] { strAppend, "'' , " });
                            }
                        }
                        else
                        {
                            strAppend = string.Concat(new string[] { strAppend, "NULL, " });
                        }

                    }
                    strAppend = string.Concat(new string[] { strAppend, Environment.NewLine });

                    rsDebugLog.MoveNext();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
