﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    public class CUnit
    {
        private const string APP_NAME = "LAPDEcadAccessorMgr.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CUnitTest.");

        //The number of tests called from this CUnitTest implementation.
        private const int mcintNumberOfTests = 3;
        private string mstrResults = string.Empty;
        private string mintErrors = string.Empty;


        /// <summary>
        /// Returns a count of the number of tests that can be run for this application.
        /// </summary>
        /// <returns></returns>
        public int NumTests()
        {
            return mcintNumberOfTests;
        }


        /// <summary>
        /// Returns the indexed test description.
        /// </summary>
        /// <param name="intIndex"></param>
        /// <returns></returns>
        public string TestDesc(int intIndex)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TestDesc:" });
            try
            {
                //'CLS modules
                switch (intIndex)
                {
                    case 1: return string.Concat(new string[] { APP_NAME, "CData" });
                        break;
                    case 2: return string.Concat(new string[] { APP_NAME, "CDocument" });
                        break;
                    case 3: return string.Concat(new string[] { APP_NAME, "CWebAssignment" });
                        break;
                    default: throw new Exception(string.Concat(new string[] { Constants.vbObjectError.ToString() + 1, PROC_NAME, "Passed test index out of range!" }));

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Runs the indexed test.
        /// Returns the results as a giant formatted string.
        /// Returns an error count through intErrors.
        /// </summary>
        /// <returns></returns>
        public string RunTest(int intIndex, ref int intErrors)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "RunTest: " });
            string strRunTest = string.Empty;
            try
            {

                //Initialize results counters.
                Init(intIndex);

                //'Run the test.
                switch (intIndex)
                {

                    //CLS modules
                    case 1: RunTest_CData(ref intErrors);
                        break;
                    case 2: RunTest_CDocument(ref intErrors);
                        break;
                    case 3: RunTest_CWebAssignment(ref intErrors);
                        break;
                    default: throw new Exception(string.Concat(new string[] { Constants.vbObjectError.ToString() + 1, PROC_NAME, "Passed test index out of range!" }));
                }
                strRunTest = Done(ref intErrors);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strRunTest;

        }


        /// <summary>
        ///  Private results formatting helpers
        /// </summary>
        /// <param name="intIndex"></param>
        /// 'Initialize results
        private void Init(int intIndex)
        {
            try
            {
                mstrResults = string.Concat(new object[] { Environment.NewLine, "TESTING:", TestDesc(intIndex), Environment.NewLine });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void Success(string strLine)
        {
            try
            {
                mstrResults = string.Concat(new string[] { mstrResults, "SUCCESS : ", strLine, Environment.NewLine });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void Verify(string strLine)
        {
            try
            {
                mstrResults = string.Concat(new string[] { mstrResults, "VERIFY:", strLine, "Does this look right?", Environment.NewLine });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void Failure(string strLine)
        {
            try
            {
                mintErrors = mintErrors + 1;
                mstrResults = string.Concat(new string[] { mstrResults, "FAILURE : ", strLine, Environment.NewLine });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Add new results line.
        /// </summary>
        /// <param name="intErrors"></param>
        /// <returns></returns>
        private string Done(ref int intErrors)
        {
            try
            {
                intErrors = Convert.ToInt32(mintErrors);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mstrResults;
        }


        /// <summary>
        /// Tests Class Module CData
        /// </summary>
        /// <param name="intErrors"></param>
        /// <returns></returns>
        private string RunTest_CData(ref int intErrors)
        {

            //'General object creation test:
            CData objCdata = null;
            string RunTest_CData = string.Empty;
            try
            {
                objCdata = new CData();
                if (objCdata == null)
                    Failure("Could not create CData ");
                else
                    Success("Created CData object.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCdata = null;
            }
            return string.Empty;
        }


        /// <summary>
        /// Tests Class Module CDocument
        /// </summary>
        /// <param name="intErrors"></param>
        /// <returns></returns>
        private string RunTest_CDocument(ref int intErrors)
        {

            //'General object creation test:
            CDocument objCDocument = null;
            string strRunTest_CDocument = string.Empty;
            try
            {
                objCDocument = new CDocument();
                if (objCDocument == null)
                    Failure("Could not create CDocument ");
                else
                    Success("Created CDocument  object.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCDocument = null;
            }
            return strRunTest_CDocument;
        }


        /// <summary>
        /// Tests Class Module CWebAssignment
        /// </summary>
        /// <param name="inErrors"></param>
        /// <returns></returns>
        private string RunTest_CWebAssignment(ref int inErrors)
        {

            //'General object creation test:
            CWebAssignment objCWebAssignment = null;
            string strRunTest_CWebAssignment = string.Empty;
            try
            {
                objCWebAssignment = new CWebAssignment();

                if (objCWebAssignment == null)
                    Failure("Could not create CWebAssignment");
                else
                    Success("Created CWebAssignment object.");

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objCWebAssignment = null;
            }
            return strRunTest_CWebAssignment;
        }
    }
}
