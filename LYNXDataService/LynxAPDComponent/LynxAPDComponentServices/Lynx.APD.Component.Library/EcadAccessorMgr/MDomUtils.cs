﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.Diagnostics;
using System.IO;
using Lynx.APD.Component.Library.PartnerDataMgr;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    public class MDomUtils
    {
        private const string MODULE_NAME = "LAPDEcadAccessorMgr.MDomUtils.";
        public SiteUtilities.CEvents g_objEvents = new SiteUtilities.CEvents();
        private enum EventCodes : ulong
        {
            eDomDataValidationFailed = 0x80065000 + 0x100,
            eXmlInvalid,
            eXmlMissingAttribute,
            eXmlMissingElement,
            eXmlEmptyNodeSet
        }


        public void LoadXml(ref XmlDocument objDom, ref string strXml, string strCallerName = null, string strXmlDescription = null)
        {
            bool blnValidXml = false;
            try
            {
                if (!string.IsNullOrEmpty(strXml))
                    objDom.LoadXml(strXml);

                if (!string.IsNullOrEmpty(objDom.OuterXml) && !objDom.HasChildNodes)
                    blnValidXml = true;

                if (blnValidXml)
                {
                    throw new Exception(string.Concat(new string[] { "MDomUtils.LoadXml -The", strXmlDescription, " XML could not be loaded -", strXml }));
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void LoadXmlFile(ref XmlDocument objDom, ref string strFileName, string strCallerName = null, string strXmlDescription = null)
        {
            bool blnValidXml = false;
            try
            {
                if (!string.IsNullOrEmpty(strFileName))
                    objDom.LoadXml(strFileName);

                if (!string.IsNullOrEmpty(objDom.OuterXml) && !objDom.HasChildNodes)
                    blnValidXml = true;
                if (blnValidXml)
                    throw new Exception(string.Concat(new string[] { "The ", strXmlDescription, " xml could not be loaded. The XML is -", strFileName }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string EscapeXml(ref string strXml)
        {
            XmlDocument objDom = null;
            string strEscapeXml = string.Empty;
            try
            {
                objDom = new XmlDocument();
                LoadXml(ref objDom, ref strXml, "CleanUpXml");
                strEscapeXml = objDom.OuterXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strEscapeXml;
        }


        public XmlNode GetChildNode(ref XmlNode objNode, string strPath, bool blnMustExist = false)
        {
            XmlNode objChild = null;
            XmlNode xmlnodeGetChildNode = null;
            try
            {
                objChild = objNode.SelectSingleNode(strPath);

                if (objChild == null)
                    if (blnMustExist == true)
                        throw new Exception(string.Concat(new string[] { EventCodes.eXmlMissingElement.ToString(), "GetChildNode('", strPath, "')", objNode.Name, "is missing a '", strPath, "'child node." }));

                    else
                        xmlnodeGetChildNode = objChild;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objChild = null;
            }
            return xmlnodeGetChildNode;
        }


        public string GetChildNodeText(ref XmlNode objNode, string strPath, bool blnMustExist = false)
        {
            XmlNode objChild = null;
            string strGetChildNodeText = string.Empty;
            try
            {
                objChild = GetChildNode(ref objNode, strPath, blnMustExist);
                if (objNode == null)
                    return strGetChildNodeText;
                else
                    return strGetChildNodeText = objChild.InnerText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public XmlNodeList GetChildNodeList(ref XmlNode objNode, string strPath, bool blnMustExist = false)
        {
            XmlNodeList objChildList = null;
            XmlNodeList xmlnodelistGetChildNodeList = null;
            try
            {
                objChildList = objNode.SelectNodes(strPath);
                if (objChildList == null)
                    if (blnMustExist)
                        throw new Exception(string.Concat(new string[] { EventCodes.eXmlEmptyNodeSet.ToString(), "GetChildNodeList('", strPath, "')", objNode.Name, "is missing a '", strPath, "'child nodes." }));
                    else if (objChildList.ToString().Length == 0 && blnMustExist)
                        throw new Exception(string.Concat(new string[] { EventCodes.eXmlEmptyNodeSet.ToString(), "GetChildNodeList('", strPath, "')", objNode.Name, "is missing a '", strPath, "'child nodes." }));
                    else
                        xmlnodelistGetChildNodeList = objChildList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objChildList = null;
            }
            return xmlnodelistGetChildNodeList;
        }


        public string ValidateDomData(ref XmlDocument domXML, ref string strXslFileName, ref object objEvents, bool blnRaise = true)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME + "ValidateDomData: " });
            XmlDocument domXSL = null;
            string strValidateDomData = string.Empty;
            string strResult = string.Empty,
                   strdomXML = string.Empty;
            bool g_blnDebugMode;
            try
            {
                domXSL = new XmlDocument();
                g_blnDebugMode = g_objEvents.IsDebugMode;

                if (g_blnDebugMode)
                    g_objEvents.Trace(string.Concat(new string[] { "XML = ", domXML.OuterXml, PROC_NAME }), (string.Concat(new string[] { "Validating XML versus file ", strXslFileName })));

                if (domXML != null)
                {
                    strdomXML = domXML.OuterXml;
                    using (StringReader sReader = new StringReader(strdomXML))
                    using (XmlReader xReader = XmlReader.Create(sReader))
                    using (StringWriter sWriter = new StringWriter())
                    using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                    {
                        XslCompiledTransform transformxslt = new XslCompiledTransform();
                        transformxslt.Load(strXslFileName);
                        transformxslt.Transform(xReader, xWriter);
                        strResult = sWriter.ToString();
                    }
                }

                if (Left(strResult, 1) == "|")
                    strResult = Mid(strResult, 2);

                strValidateDomData = strResult;

                if (strResult.Length > 0)
                    g_objEvents.HandleMultiEvents(Convert.ToInt32(EventCodes.eDomDataValidationFailed), PROC_NAME, strResult);
                else
                    if (g_blnDebugMode)
                        g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Passed Data Validation" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                domXSL = null;
            }
            return strValidateDomData;
        }


        public void AddAttribute(ref XmlElement objElem, string strName, string strValue)
        {
            XmlAttribute objattribute = null;
            try
            {
                objattribute = objElem.OwnerDocument.CreateAttribute(strName);
                objattribute.Value = strValue;
                objElem.SetAttributeNode(objattribute);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objattribute = null;
            }
        }


        public XmlElement AddElement(ref XmlElement objElem, string strName)
        {
            XmlElement objNewElem = null;
            XmlElement xmlelemAddElement = null;
            try
            {
                objNewElem = objElem.OwnerDocument.CreateElement(strName);
                objElem.AppendChild(objNewElem);
                xmlelemAddElement = objNewElem;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objNewElem = null;
            }
            return xmlelemAddElement;
        }


        public void RenameAttribute(ref XmlElement objElem, string strPath, string strNewName)
        {
            XmlAttribute objAtt = null;
            XmlNode objNode = null;
            string strValue = string.Empty;
            try
            {
                objNode = (XmlNode)(objElem);
                objAtt = (XmlAttribute)GetChildNode(ref objNode, strPath, false);
                if (objAtt != null)
                {
                    strValue = objAtt.InnerText;
                    objElem.RemoveAttribute(objAtt.Name);
                    AddAttribute(ref objElem, strNewName, strValue);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void RenameAllAttributes(ref XmlElement objElem, string strOldName, string strNewName)
        {
            XmlElement objxmlelementtemp = null;
            try
            {
                RenameAttribute(ref objElem, string.Concat(new string[] { "@", strOldName }), strNewName);
                if (objElem.ChildNodes.Count > 0)
                    foreach (XmlNode objChild in objElem.ChildNodes)
                        if (objChild != null)
                        {
                            objxmlelementtemp = (XmlElement)objChild;
                            RenameAllAttributes(ref objxmlelementtemp, strOldName, strNewName);
                        }

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }


        public void RemoveBlankAttributes(ref XmlElement objElem)
        {
            XmlAttribute objAtt = null;
            int intIdx = 0;
            try
            {
                while (intIdx < objElem.Attributes.Count)
                {
                    objAtt = objElem.Attributes[intIdx];
                    if (objAtt.InnerText == "")
                        objElem.RemoveAttribute(objAtt.Name);
                    else
                        intIdx = intIdx + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void RemoveAllChildBlankAttributes(ref XmlElement objElem)
        {
            XmlElement objelemtemp = null;
            try
            {
                RemoveBlankAttributes(ref objElem);
                if (objElem.ChildNodes.Count > 0)
                    foreach (XmlNode objChild in objElem.ChildNodes)
                        if (objChild != null)
                        {
                            objelemtemp = (XmlElement)objChild;
                            RemoveBlankAttributes(ref objelemtemp);
                        }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void MapAPDReferenceData(ref XmlDocument objDom, string strMapFile, ref object objEvents)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "MapAPDReferenceData:" });
            string strCodeAtt = string.Empty,
            strParent = string.Empty,
            strCode = string.Empty,
            strList = string.Empty,
            strMapAtt = string.Empty,
            strValue = string.Empty;
            XmlDocument objMapDom = null;
            XmlNodeList objMapList = null;
            XmlNodeList objAttParentList = null;
            XmlNodeList objReplaceList = null;
            XmlNode objxmltmpnode = null;
            XmlElement objxmlelemtemp = null;

            try
            {
                objMapDom = new XmlDocument();
                LoadXmlFile(ref objMapDom, ref strMapFile, PROC_NAME, "Map");
                objMapList = objMapDom.SelectNodes("//Map");
                foreach (XmlNode objMapNode in objMapList)
                {
                    objReplaceList = objMapNode.SelectNodes("Replace");
                    objxmltmpnode = objMapNode;
                    strCodeAtt = GetChildNodeText(ref objxmltmpnode, "@Att", true);
                    strParent = GetChildNodeText(ref objxmltmpnode, "@Parent", false);
                    strList = GetChildNodeText(ref objxmltmpnode, "@List", true);
                    strMapAtt = GetChildNodeText(ref objxmltmpnode, "@MapAtt", true);
                    objxmltmpnode = null;

                    if (strParent == null)
                        strParent = "Root";

                    objAttParentList = objDom.SelectNodes(string.Concat(new string[] { "//", strParent, "//*[@", strCodeAtt, "]" }));
                    foreach (XmlNode objAttParentNode in objAttParentList)
                    {
                        objxmltmpnode = objAttParentNode;
                        strCode = GetChildNodeText(ref objxmltmpnode, string.Concat(new string[] { "@", strCodeAtt }), true);
                        objxmltmpnode = null;

                        if (strList == null)
                            strValue = strCode;
                        else
                            if (strCode.Length > 0)
                            {
                                objxmltmpnode = (XmlNode)objDom;
                                strValue = GetChildNodeText(ref objxmltmpnode, string.Concat(new string[] { "/Root/Reference", "[@List='", strList, "']", "[@ReferenceID='", strCode, "']", "/@Name" }), false);
                            }

                            else
                            {
                                strValue = string.Empty;
                            }
                        objxmltmpnode = null;
                        foreach (XmlNode objReplaceNode in objReplaceList)
                        {
                            objxmltmpnode = objReplaceNode;
                            strValue = GetChildNodeText(ref objxmltmpnode, "@Find", true);
                        }
                        objxmltmpnode = null;
                        objxmltmpnode = objAttParentNode;
                        if (GetChildNodeText(ref objxmltmpnode, string.Concat(new string[] { "@", strMapAtt }), false) == null)
                        {
                            objxmlelemtemp = (XmlElement)objAttParentNode;
                            AddAttribute(ref objxmlelemtemp, strMapAtt, strValue);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string FormatDomParseError(ref XmlException objErr)
        {
            string strFormatDomParseError = string.Empty;
            try
            {
                strFormatDomParseError = string.Concat(new string[]{"Error code (",objErr.GetHashCode().ToString(),") was raised during parsing.",Environment.NewLine,
                                         "File location: line = ",objErr.LineNumber.ToString()," and column = ",objErr.LinePosition.ToString(),".",Environment.NewLine,
                                         "Bad XML:'",objErr.SourceUri,"'",Environment.NewLine,
                                         "Reason given:",objErr.Message});

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strFormatDomParseError;
        }


        public XmlDocument TransformDomAsDom(ref XmlDocument domXML, string strXslPath, ref object objEvents)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransformXmlAsDom: " });
            string domResult = string.Empty;
            string strdomXML = string.Empty;
            XmlDocument xmldocTransformDomAsDom = null;
            bool blnDebugMode;
            try
            {

                g_objEvents.Assert((domXML != null), "Passed XML DOM was Nothing.");
                g_objEvents.Assert(!string.IsNullOrEmpty(strXslPath), "Passed XSL file path string was blank.");
                blnDebugMode = g_objEvents.IsDebugMode;

                if (blnDebugMode)
                    g_objEvents.Trace(string.Concat(new string[] { string.Concat(new string[] { "StyleSheet = ", strXslPath }), PROC_NAME }));

                if (domXML != null)
                {
                    strdomXML = domXML.OuterXml;
                    using (StringReader sReader = new StringReader(strdomXML))
                    using (XmlReader xReader = XmlReader.Create(sReader))
                    using (StringWriter sWriter = new StringWriter())
                    using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                    {
                        XslCompiledTransform transformxslt = new XslCompiledTransform();
                        transformxslt.Load(strXslPath);
                        transformxslt.Transform(xReader, xWriter);
                        domResult = sWriter.ToString();
                    }

                    xmldocTransformDomAsDom.LoadXml(domResult);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                domResult = null;
            }
            return xmldocTransformDomAsDom;
        }


        public XmlDocument TransformXmlAsDom(ref string strXml, string strXslPath, ref object objEvents)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransformXmlAsDom: " });
            XmlDocument domXML = null;
            XmlDocument xmldocTransformXmlAsDom = null;
            try
            {
                domXML = new XmlDocument();
                g_objEvents.Assert(!string.IsNullOrEmpty(strXml), "Passed XML string was blank.");
                LoadXml(ref domXML, ref strXml, PROC_NAME, "Source XML");
                xmldocTransformXmlAsDom = TransformDomAsDom(ref domXML, strXslPath, ref objEvents);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                domXML = null;
            }
            return xmldocTransformXmlAsDom;
        }


        public string TransformDomAsXml(ref XmlDocument domXML, string strXslPath, ref object objEvents)
        {
            string strTransformDomAsXml = null;
            try
            {
                strTransformDomAsXml = TransformDomAsDom(ref domXML, strXslPath, ref objEvents).OuterXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strTransformDomAsXml;
        }


        public string TransformXmlAsXml(ref string strXml, string strXslPath, ref object objEvents)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransformXmlAsDom: " });
            string strTransformXmlAsXml = null;
            XmlDocument domXML = null;
            try
            {
                domXML = new XmlDocument();
                g_objEvents.Assert(!string.IsNullOrEmpty(strXml), "Passed XML string was blank.");
                LoadXml(ref domXML, ref strXml, PROC_NAME, "Source XML");
                strTransformXmlAsXml = TransformDomAsXml(ref domXML, strXslPath, ref objEvents).ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                domXML = null;
            }
            return strTransformXmlAsXml;

        }


        public string Left(string param, int length)
        {
            try
            {
                string strLeft = param.Substring(0, length);
                return strLeft;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string Mid(string param, int startIndex)
        {
            try
            {
                string strMid = param.Substring(startIndex);
                return strMid;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
