﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using IMAGEUTILSLib;
using System.IO;
using System.Reflection;
using ReportUtils;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    class CDocument
    {
        #region Variable Declaration
        private const string MODULE_NAME = "LAPDEcadAccessorMgr.CDocument.";

        MLAPDEcadAccessorMgr mobjEcadAccessorMgr = null;
        MLAPDPartnerDataMgr_Utility mobjMLAPDPartnerDataMgr = null;
        MBase64Utils mobjMBase64Utils = null;
        MDomUtils mobjMDomUtils = null;
        Thumbnail objImageUtils = null;
        clsReport objReportUtils = null;
        #endregion

        #region Enum Declaration
        //Internal error codes for this class.
        public enum EventCodes : ulong
        {
            eReturnedStreamIsBlank = MLAPDEcadAccessorMgr.LAPDEcadAccessorMgr_ErrorCodes.CDocument_CLS_ErrorCodes
        }
        #endregion

        #region Private Methods

        private void Class_Terminate()
        {
            mobjEcadAccessorMgr = new MLAPDEcadAccessorMgr();
            mobjEcadAccessorMgr.TerminateGlobals();
        }
        #endregion

        #region Public Methods

        public object GetDocument(string strParamXML)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "GetDocument:" });

            //Object declarations
            XmlDocument objParamDom = null;
            //object objReportUtils = null;
            //object objImageUtils = null;
            object objStream = null;
            ADODB.Stream objAdoStream = null;
            XmlNode objxmlnode = null;
            XmlNode objxmlDocumentRoot = null;
            XmlNode objxmlheight = null;
            XmlNode objxmlwidth = null;
            XmlDocument objTempDoc = null;


            //Variable declarations
            string strFilePath = string.Empty;
            string strDocumentRoot = string.Empty;
            string strGetDocument = string.Empty;
            string strWidth = string.Empty;
            string strHeight = string.Empty;
            object objGetDocument = null;

            try
            {
                mobjMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr_Utility();
                mobjEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                mobjMBase64Utils = new MBase64Utils();
                mobjMDomUtils = new MDomUtils();

                mobjEcadAccessorMgr.InitializeGlobals();

                //Check passed parameters.
                mobjMLAPDPartnerDataMgr.g_objEvents.Assert(Convert.ToBoolean((strParamXML.Length) > 0), "Nothing passed for strParamXML.");

                //Add a trace mesage to the debug log.
                if (mobjMLAPDPartnerDataMgr.g_blnDebugMode)
                {
                    mobjMLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Param XML = '", strParamXML, "'", ",", PROC_NAME, "Started" }));
                    //Initialize DOM objects
                    objParamDom = new XmlDocument();
                    mobjMDomUtils.LoadXml(ref objParamDom, ref strParamXML, PROC_NAME, "Passed Parameter");

                    //Entity Specific Code - select on the entity
                    switch (objParamDom.DocumentElement.Name)
                    {
                        case "Document":
                            //Get the file path from the parameter
                            objxmlnode = (XmlNode)(objParamDom.DocumentElement.SelectSingleNode("@ImageLocation"));
                            strFilePath = mobjMDomUtils.GetChildNodeText(ref objxmlnode, "@ImageLocation", true);

                            //Get the document root from the config file.
                            strDocumentRoot = mobjMLAPDPartnerDataMgr.GetConfig("Document/RootDirectory");
                            //Combine and escape out the double slashes.
                            strFilePath = string.Concat(new string[] { strDocumentRoot, strFilePath }).Replace("\\\\", "\\");
                            if (mobjMLAPDPartnerDataMgr.g_blnDebugMode)
                                mobjMLAPDPartnerDataMgr.g_objEvents.Trace(strFilePath, "Reading file.");
                            //Return the results.
                            objGetDocument = mobjMBase64Utils.ReadBinaryDataFromFile(strFilePath);
                            break;
                        case "Report":
                            //Create the report utilities object.
                            //objReportUtils = mobjEcadAccessorMgr.CreateObjectEx("ReportUtils.clsReport");
                            objReportUtils = new clsReport();

                            if (mobjMLAPDPartnerDataMgr.g_blnDebugMode)
                                mobjMLAPDPartnerDataMgr.g_objEvents.Trace("", "Calling GetReport()");

                            //Call it to stream the report file.
                            objStream = objReportUtils.getReport(string.Concat(new string[]{Path.GetDirectoryName(
                      Assembly.GetEntryAssembly().Location) , mobjMLAPDPartnerDataMgr.GetConfig("ClaimPoint/ReportPath") ,
                      mobjMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/Report/@sReportFileName", true)}),
                      mobjMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/Report/@sStoredProc", true),
                      mobjMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/Report/@sReportParams", true),
                      mobjMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/Report/@sReportStaticParams", true),
                      mobjMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/Report/@sReportFormat", false));

                            if (objStream == null)
                                throw new Exception(string.Concat(new string[] { Convert.ToString(EventCodes.eReturnedStreamIsBlank), PROC_NAME, "ADO Stream returned from ReportUtils is blank." }));

                            if (mobjMLAPDPartnerDataMgr.g_blnDebugMode)
                                mobjMLAPDPartnerDataMgr.g_objEvents.Trace("", "Received Stream");

                            //Read all the data from the stream.
                            return strGetDocument = Convert.ToString(objAdoStream.Read());

                            objAdoStream.Close();
                            objAdoStream = null;

                            //Clean up and go home.
                            objReportUtils = null;
                            break;
                        case "Thumbnail":
                            //Create the report utilities object.
                            //objImageUtils = mobjEcadAccessorMgr.CreateObjectEx("ImageUtils.Thumbnail");
                            objImageUtils = new Thumbnail();


                            //Get the file path from the parameter
                            objxmlDocumentRoot = objParamDom.DocumentElement.SelectSingleNode("@ImageLocation");
                            strFilePath = mobjMDomUtils.GetChildNodeText(ref objxmlDocumentRoot, "@ImageLocation", true);
                            //Get the document root from the config file.
                            strDocumentRoot = mobjMLAPDPartnerDataMgr.GetConfig("Document/RootDirectory");
                            //Combine and escape out the double slashes.
                            strFilePath = string.Concat(new string[] { strDocumentRoot, strFilePath }).Replace("\\\\", "\\");
                            if (mobjMLAPDPartnerDataMgr.g_blnDebugMode)
                                mobjMLAPDPartnerDataMgr.g_objEvents.Trace("", "Calling GetThumbnail()");

                            //Get the thumbnail height and width from the param XML.
                            //If not passed, pull the default from the config file.
                            objxmlheight = objParamDom.DocumentElement.SelectSingleNode("@Height");
                            strHeight = mobjMDomUtils.GetChildNodeText(ref objxmlheight, "@Height", false);
                            objxmlwidth = objParamDom.DocumentElement.SelectSingleNode("@Width");
                            strWidth = mobjMDomUtils.GetChildNodeText(ref objxmlwidth, "@Width", false);
                            if (strHeight == "")
                                strHeight = mobjMLAPDPartnerDataMgr.GetConfig("ClaimPoint/Thumbnail/@Height");
                            if (strWidth == "")
                                strWidth = mobjMLAPDPartnerDataMgr.GetConfig("ClaimPoint/Thumbnail/@Width");

                            //Call ImateUtils component to generate the thumbnail image.
                            objAdoStream = (ADODB.Stream)objImageUtils.GetThumbnail(strFilePath, Convert.ToInt32(strHeight), Convert.ToInt32(strWidth));

                            if (mobjMLAPDPartnerDataMgr.g_blnDebugMode)
                                mobjMLAPDPartnerDataMgr.g_objEvents.Trace("", "Received Stream");

                            //Read all the data from the stream.
                            strGetDocument = Convert.ToString(objAdoStream.Read());

                            objAdoStream.Close();

                            //Clean up and go home.
                            objImageUtils = null;
                            break;
                    }
                    // End Entity Specific Code
                    //Add a trace mesage to the debug log.
                    if (mobjMLAPDPartnerDataMgr.g_blnDebugMode)
                        mobjMLAPDPartnerDataMgr.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Exiting" }));

                    //Do not exit - fall into the error handler below for one stop object cleanup code.
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                //Object clean up.
                objParamDom = null;
                objReportUtils = null;
                objImageUtils = null;
                objStream = null;
                objAdoStream = null;
            }
            return strGetDocument;
        }


        public string GetDocumentXML(string strParamXML)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "GetDocumentXML:" });

            //Object declarations
            XmlDocument objParamDom = null;
            XmlDocument objResultDom = null;
            XmlNode objxmlnode = null;
            //Variable declarations
            string strFilePath = string.Empty;
            string strDocumentRoot = string.Empty;
            string strGetDocumentXML = string.Empty;

            try
            {
                objResultDom = new XmlDocument();
                mobjEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                mobjMDomUtils = new MDomUtils();

                mobjEcadAccessorMgr.InitializeGlobals();
                //Check passed parameters.
                mobjMLAPDPartnerDataMgr.g_objEvents.Assert(Convert.ToBoolean((strParamXML.Length) > 0), "Nothing passed for strParamXML.");

                //Add a trace mesage to the debug log.
                if (mobjMLAPDPartnerDataMgr.g_blnDebugMode)
                    mobjMLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Param XML = '", strParamXML, "'", ",", PROC_NAME, "Started" }));

                //   Initialize DOM objects.
                objParamDom = new XmlDocument();
                objResultDom = new XmlDocument();

                //   Loads and validates the xml.
                mobjMDomUtils.LoadXml(ref objParamDom, ref strParamXML, PROC_NAME, "Passed Parameter");

                // Get the file path from the parameter
                objxmlnode = objParamDom.DocumentElement.SelectSingleNode("@ImageLocation");
                strFilePath = Convert.ToString(mobjMDomUtils.GetChildNode(ref objxmlnode, "@ImageLocation", true));

                //Get the document root from the config file.
                strDocumentRoot = mobjMLAPDPartnerDataMgr.GetConfig("Document/RootDirectory");

                // Combine and escape out the double slashes.
                strFilePath = string.Concat(new string[] { strDocumentRoot, strFilePath }).Replace("\\", @"\");

                // Load up the xml file from disk.
                mobjMDomUtils.LoadXmlFile(ref objResultDom, ref strFilePath, PROC_NAME, "Document");

                //Now return the escaped XML.
                strGetDocumentXML = Convert.ToString(objResultDom.OuterXml);

                //Add a trace mesage to the debug log.
                if (mobjMLAPDPartnerDataMgr.g_blnDebugMode)
                    mobjMLAPDPartnerDataMgr.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Exiting" }));

                //Do not exit - fall into the error handler below for one stop object cleanup code.
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strGetDocumentXML;
        }
        #endregion
    }
}

