﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    class MEscapeUtilities
    {

        #region Variable Declaration
        private const string MODULE_NAME = "LAPDEcadAccessorMgr.MstrEscapeUtils.";
        #endregion

        #region Public Methods

        /// <summary>
        /// Returns string suitable for SQL queries by replacing all single quotes
        /// with 2 single quotes so that they will be accurately interpreted by the
        /// SQL engine as a literal single quote rather than a string delimiter
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public string SQLQueryString(string strInput)
        {
            string strSQLQueryString = string.Empty;
            try
            {
                strSQLQueryString = strInput.Replace("'", "''");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strSQLQueryString;
        }


        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string strParam, int intLength)
        {
            string strResult = strParam.Substring(0, intLength);
            return strResult;
        }


        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string strParam, int intLength)
        {
            string strResult = strParam.Substring(strParam.Length - intLength, intLength);
            return strResult;
        }


        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = strParam.Substring(startIndex, intLength);
            return strResult;
        }


        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = strParam.Substring(intstartIndex);
            return strResult;
        }


        // Escapes a string using URL type escaping (i.e. '%20' for a space ).
        // Replaces all non-alphanumeric characters in a string as strEscaped sequences
        // compatible with strEscaped URL strings.  That is, all special characters are
        // replaced with %HH where HH is the two digit hex value of the character.
        // Note that although we use the same mechanism as URL escaping, we are more
        // regiorous in our interpretation of "special" characters.  For example,
        // dashes and underscores will be strEscaped although they are not typically
        // strEscaped in URL strings.
        public string EscapeString(ref string strInput)
        {
            string strOut = string.Empty;
            int intChar = 0;
            string strEscape = string.Empty;
            int charCount = 0;
            string strEscapeString = string.Empty;
            try
            {
                strOut = strInput;
                for (charCount = strInput.Length; charCount > 1; charCount += -1)
                {
                    intChar = (int)System.Convert.ToChar(Mid(strInput, charCount, 1));
                    if (((intChar < 48) && (intChar != 32)) || ((intChar > 57) && (intChar < 65)) || ((intChar > 90) && (intChar < 97)) || (intChar > 122))
                    {
                        strEscape = string.Format("{0:X}", intChar);
                        if (strEscape.Length < 2)
                            strEscape = string.Concat(new string[] { "%0", strEscape });
                        else
                            strEscape = string.Concat(new string[] { "%", strEscape });
                    }
                    strOut = string.Concat(new string[] { Left(strOut, charCount - 1), strEscape, Right(strOut, strOut.Length - charCount) });
                }
                strEscapeString = strOut.Replace(" ", "+");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strEscapeString;
        }


        //Un-Escapes a string of URL type escaping (i.e. '%20' for a space ).
        //Replaces all strEscape sequences within a string with the character represented
        //by the strEscape sequence.  An strEscape sequence has the form %HH where HH is a
        //two-digit hexidecimal number representing the ASCII code of the original
        //character.  Note that this function does not properly check if the string
        //was validly strEscaped in the first place.  When processing a string that
        // contains literal %'s this function will behave unpredictably.
        public string UnEscapeString(ref string strInput)
        {
            string strOut = string.Empty;
            int charCount = 0;
            try
            {
                strOut = strInput.Replace("+", " ");

                for (charCount = 1; charCount <= strOut.Length; charCount++)
                {
                    if (Mid(strOut, charCount, 1) == "%")
                    {
                        if (Mid(strOut, charCount + 1, 1) == "u")
                        {
                            //Convert unicode character.
                            strOut = string.Concat(new string[] { Left(strOut, charCount - 1), Convert.ToString(Convert.ToInt32(string.Concat(new string[] { "&H", Mid(strOut, charCount + 2, 4) }))), Right(strOut, strOut.Length - charCount - 5) });
                        }
                        else
                        {
                            //Convert standard byte character.
                            strOut = string.Concat(new string[] { Left(strOut, charCount - 1), Convert.ToString(Convert.ToInt32(string.Concat(new string[] { "&H", Mid(strOut, charCount + 1, 2) }))), Right(strOut, strOut.Length - charCount - 2) });
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strOut;
        }


        // Escapes a string of URL type escaping (i.e. '%20' for a space )
        // except this does NOT un-escape those characters not allowed in XML.
        // Thus this function is the nearly the same as UnEscapeString.
        public string UnEscapeStringAsXmlCompliant(ref string strInput)
        {
            string strOut = string.Empty;
            int charCount = 0;
            string strCharacter = string.Empty;
            int iLen = 0;
            try
            {
                strOut = strInput.Replace("+", " ");
                for (charCount = 1; charCount <= strOut.Length; charCount++)
                {

                    if (Mid(strOut, charCount, 1) == "%")
                    {
                        if (Mid(strOut, charCount + 1, 1) == "u")
                        {
                            //Convert unicode character.
                            strCharacter = Convert.ToString(Convert.ToInt16(string.Concat(new string[] { "&H", Mid(strOut, charCount + 2, 4) })));
                            iLen = 5;
                        }
                        else
                        {
                            //Convert standard byte character.
                            strCharacter = Convert.ToString(Convert.ToInt16(string.Concat(new string[] { "&H", Mid(strOut, charCount + 1, 2) })));
                            iLen = 2;
                        }
                        switch (strCharacter)
                        {
                            case "<":
                            case ">":
                            case "&":
                            default:
                                {
                                    strOut = string.Concat(new string[] { Left(strOut, charCount - 1), strCharacter, Right(strOut, strOut.Length - charCount - iLen) });
                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strOut;
        }


        // Removes strange formatting characters from XML.
        // This function removes characters that are non-standard.
        // It was created to strip these characters out of XML passed to our partners,
        // whose mainframes could not handle 'A0' in spite of it being valid XML.
        public string StripNonStdChars(ref string strInput)
        {
            string strOut = string.Empty;
            int charCount = 0;
            int intChar = 0;

            try
            {
                strOut = strInput;
                for (charCount = 1; charCount <= strOut.Length; charCount++)
                {
                    intChar = (int)System.Convert.ToChar(Mid(strOut, charCount, 1));
                    if (intChar < 32 || intChar > 126)
                        strOut = string.Concat(new string[] { Left(strOut, charCount - 1), " ", Mid(strOut, charCount + 1) });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOut;
        }


        //Escapes an XML string of any markup characters (i.e. '&gt;' for '>')
        public string EscapeXmlMarkup(ref string strInput)
        {
            return strInput.Replace("&", "&amp;").Replace(">", "&gt;").Replace("", "&quot;").Replace("'", "&apos;").Replace("<", "&lt;");
        }

        /// <summary>
        /// UnEscapes an XML string of any markup characters (i.e. '>' for '&gt;')
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public string UnEscapeXmlMarkup(ref string strInput)
        {
            return strInput.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "").Replace("&apos;", "'").Replace("&amp;", "&");
        }
        #endregion
    }
}
