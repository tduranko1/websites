﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Runtime.InteropServices;
using ADODB;
using VBA;
using Scripting;
using MSXML2;
using Lynx.APD.Component.Library.PartnerDataMgr;
using System.Text.RegularExpressions;
using LAPDCommServer;
using ReportUtils;
using lynxReceive;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    /// <summary>
    /// Component EcadAccessor : Class Module CWebAssignment
    /// 
    /// This class module processes web assignments from the Claim Point Web
    /// Assignment website.  It was originally written to be private and called
    /// from other modules of the application only.  It has since been exposed
    /// to the world so as to be called from PartnerData.
    /// 
    /// </summary>
    class CWebAssignment
    {
        #region Declarations
        private const string APP_NAME = "LAPDEcadAccessorMgr.";
        private string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CWebAssignment." });
        private string mstrPartner;
        MLAPDEcadAccessorMgr objMLAPDEcadAccessorMgr = null;
        MRecordsetUtils objMRecordsetUtils = null;
        MLAPDPartnerDataMgr objMLAPDPartnerDataMgr = null;
        MEscapeUtilities objMEscapeUtilities = null;
        MDomUtils objMDomUtils = null;
        CCommServer objCommSvr = null;
        clsReport objReportUtil = null;
        Loss objGlaxisProxy = null;
        #endregion

        #region Enumerators
        //Internal error codes for this module.
        private enum EventCodes : ulong
        {
            eProcedureLevelDoesntExist = MLAPDEcadAccessorMgr.LAPDEcadAccessorMgr_ErrorCodes.CWebAssignment_CLS_ErrorCodes,
            eNoRecordsetReturned,
            eBadDataReturned,
            eUserIdDoesNotExist,
            eUserIdDoesNotMatch,
            eLoginAlreadyUsed
        }
        #endregion

        #region Structures

        public struct typProcLevel
        {
            public int intLevelID;
            public string strName;
        }
        public struct typProc
        {
            public int intID;
            public int intLevelID;
            public string strName;
        }

        public typProcLevel[] marrProcLevelList;
        public typProc[] marrProcList;

        #endregion


        #region Public Functions

        /// <summary>
        ///  Processes a web assignment from the Claim Point Web Assignment module. 
        ///  It expects an XML document with the form:
        /// 
        ///  <WebAssignment>
        ///      <Claim> ... </Claim>
        ///      <Vehicle> ... </Vehicle>
        ///      <Involved> ... </Involved>
        ///  </WebAssignment>
        /// 
        ///  Multiple Vehicle and Involved elements are possible.
        ///  Each element has multiple child elements as passed from Claim Point.
        /// 
        ///  Calls ApdLoadAssignment below to insert the claim into APD.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <param name="strDataSourceCode"></param>
        /// <returns></returns>

        public long DoAssignment(XmlDocument objParamDom, string strDataSourceCode)
        {
            string PROC_NAME = string.Empty;

            //Objects that need clean-up
            XmlDocument objXslDom = null;
            XmlDocument objXferDom = null;
            IXMLDOMDocument2 objIXmldomdoc = null,
                objIXslDom = null,
                objIXferDom = null;
            //Primitives that don't
            long lngUID = 0;
            bool blnRaiseToCaller;
            string strUID = string.Empty,
              strXferXslName = string.Empty,
              strXferXslVersion = string.Empty,
              strAppend = string.Empty;

            try
            {
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "DoAssignment: " });

                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Started" }));

                //This boolean is used in the error handler to determine whether to
                //raise errors to the calling process or not.  Once we have stored
                //the data in the partner database, assume that the call is a success
                //as far as the caller is concerned.
                blnRaiseToCaller = true;

                // Determine where the assignment is coming from.
                if (strDataSourceCode == "CP" || strDataSourceCode == "APD")
                {
                    // Try to get a LynxID from the xml.
                    strUID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/LynxID", false);

                    if (Convert.ToBoolean(int.Parse(strUID)))
                        lngUID = Convert.ToInt64(strUID);
                    else
                        lngUID = 0;

                    //If not LynxID exists, create a new one.
                    if (Convert.ToBoolean(lngUID == 0))
                        lngUID = GetFnolUID();
                }
                else
                {
                    lngUID = Convert.ToInt64(objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/LynxID", true));
                }

                //Store the assignment in the partner database
                StoreAssignmentXml(lngUID, objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//DataSource", false), objParamDom.OuterXml);

                //Ok, we made it into partner, so from the caller's standpoint
                //assume that the transaction is good and dont raise.
                blnRaiseToCaller = false;

                //Get some DOM objects to work with.
                objXslDom = new XmlDocument();
                objXferDom = new XmlDocument();

                strXferXslName = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Code='" + strDataSourceCode + "']/@Xsl");
                strXferXslVersion = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/DataSource/Source[@Code='" + strDataSourceCode + "']/@XslVersion");

                //Load up the assignment transfer style sheet.
                strAppend = string.Concat(new string[] { objMLAPDEcadAccessorMgr.g_strSupportDocPath, "\\", strXferXslName, strXferXslVersion, ".xsl" });
                objMDomUtils.LoadXmlFile(ref objXslDom, ref strAppend, PROC_NAME, "Style Sheet");

                //Transform the passed XML into XML submittable to APD.
                objIXmldomdoc = (IXMLDOMDocument2)objParamDom;
                objIXferDom = (IXMLDOMDocument2)objXferDom;
                objIXslDom = (IXMLDOMDocument2)objXslDom;
                objIXmldomdoc.transformNodeToObject(objIXslDom, objIXferDom);

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "LynxID = ", lngUID.ToString() }), string.Concat(new string[] { PROC_NAME, "Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Call sister function to do the actual load.
            return ApdLoadAssignment(ref objXferDom, lngUID);
        }

        /// <summary>
        /// Procedure : ApdLoadAssignmentExternal
        /// DateTime  : 7/6/2005 13:31
        /// Author    : CSR0901
        /// Purpose   : Added this to get around memory access error when ApdLoadAssignment is called
        ///             from another COM+ component.  The memeory access issue arises because MSXML
        ///             interfaces are not marshalled accross process boundaries.
        /// 
        /// Parameters: strParamXml - an XML string that can be used to created the MSXML DOMDocument
        ///             required by ApdLoadAssignment.
        /// 
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="strPartner"></param>
        /// <param name="lngUID"></param>
        /// <returns></returns>
        public long ApdLoadAssignmentExternal(ref string strParamXML, string strPartner, [Optional]long lngUID)
        {
            string PROC_NAME = "ApdLoadAssignmentExternal";
            XmlDocument objDom = null;
            lngUID = 0;
            try
            {
                objDom = new XmlDocument();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMDomUtils = new MDomUtils();
                // Load the passed XML string in to a DOM object.
                objMDomUtils.LoadXml(ref objDom, ref strParamXML, PROC_NAME, "ApdLoadAssignmentExternal");

                // Save passed partner for later use.
                mstrPartner = strPartner;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDom = null;
            }
            // Pass the locally created DOM object to ApdLoadAssignmentExternal.
            return ApdLoadAssignment(ref objDom,0);
        }

        /// <summary>
        ///  Processes a web assignment in an attribute-centric format.
        ///  It expects an XML document with the form:
        /// 
        ///  <WebAssignment>
        ///     <Claim ... >
        ///        <Vehicle ... >
        ///           <Involved ... />
        ///        </Vehicle>
        ///     </Claim>
        ///  </WebAssignment>
        /// 
        ///  Multiple Vehicle and Involved elements are possible.
        ///  Each element has multiple attributes as defined by the APD DB SPs.
        /// </summary>
        ///  Returns the LYNX ID of the new claim.
        ///  <param name="objParamDom"></param>
        /// <param name="lngUID"></param>
        /// <returns></returns>
        public long ApdLoadAssignment(ref XmlDocument objParamDom, [Optional] long lngUID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdLoadAssignment: " });

            //Objects that need clean-up
            XmlNodeList objElementList = null;
            XmlElement objxmlElement = null;
            object objCommServer = null;
            XmlElement xmlElemAppend = null;

            //Primitives that don't
            bool blnClaimDataIncluded,
              blnRollback = false,
              blnInTrans = false;
            long lngUserID=0;
            string strCarrierRepEmail = string.Empty,
                strElement = string.Empty;
            int intLevel, intIndex;
            object tProc = null;

            try
            {
                lngUID = 0;
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "Param DOM = ", objParamDom.OuterXml }), string.Concat(new string[] { PROC_NAME, "Started" }));

                blnClaimDataIncluded = Convert.ToBoolean(objParamDom.SelectSingleNode("/WebAssignment/Claim/@NewClaimFlag").InnerText);

                // HACK: we will not maintain a list of users for GRANGE.  We will use one user whose email addrress is in the config file.
                if ((mstrPartner.Trim().Length) > 0)
                {
                    strCarrierRepEmail = objMLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", mstrPartner, "/AssignmentUser/@email" }));

                    // Check whether CarrierRepLogin attribute in passed xml is populated.
                    if (0 == ((objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepLogin", true)).Trim().Length))    // If not, populate with value from config file.
                        objParamDom.SelectSingleNode("//@CarrierRepLogin").InnerText = strCarrierRepEmail;

                    // Check whether CarrierRepLogin attribute in passed xml is populated.
                    if (0 == ((objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepEmailAddress", true)).Trim().Length))     // If not, populate with value from config file.
                        objParamDom.SelectSingleNode("//@CarrierRepEmailAddress").InnerText = strCarrierRepEmail;
                }

                //Get the entered user detail and add it to the param dom.
                if (blnClaimDataIncluded)
                    lngUserID = FindOrAddUser(ref objParamDom);

                //Get a new LynxID for the assignment if none was passed in.
                if (lngUID == 0)
                    lngUID = GetFnolUID();

                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);

                //Remove all blank child attributes in the passed XML.
                //This way all blank attributes will be replaced by the default values.

                objxmlElement = objParamDom.DocumentElement;
                objMDomUtils.RemoveAllChildBlankAttributes(ref objxmlElement);

                //This boolean is used to determine whether we should rollback when an error occurs.
                blnRollback = true;
                blnInTrans = true;

                //Start a transaction.
                objMLAPDEcadAccessorMgr.g_objDataAccessor.BeginTransaction();

                //Find all elements within the passed document below the root.
                //Each one corresponds to a procedure level.
                objElementList = (XmlNodeList)objParamDom.SelectNodes("//WebAssignment//*");

                //Loop through and process each of them.
                foreach (XmlElement objElement in objElementList)
                {
                    xmlElemAppend = objElement;
                    //The current procedure level name.
                    strElement = xmlElemAppend.Name;

                    //Add the LynxID attribute to every element.
                    objMDomUtils.AddAttribute(ref xmlElemAppend, "LynxID", lngUID.ToString());

                    //Add the FNOLUserID and CarrierRepUserID attribute to every element, if not already present.
                    if (objElement.SelectSingleNode("@FNOLUserID") == null)
                        objMDomUtils.AddAttribute(ref xmlElemAppend, "FNOLUserID", lngUserID.ToString());

                    //Add the FNOLUserID attribute to every element.
                    if (objElement.SelectSingleNode("@CarrierRepUserID") == null)
                        objMDomUtils.AddAttribute(ref xmlElemAppend, "CarrierRepUserID", lngUserID.ToString());

                    //The current procedure level (index).
                    //        intLevel = LevelFromName(strElement)

                    //Commit the transaction before the last call to uspFnolLoadClaim.
                    //This is to fix the transaction deadlock problems we are having.
                    if (strElement == "BeginInsert")
                    {
                        if (blnInTrans == true)
                        {
                            blnInTrans = true;
                            objMLAPDEcadAccessorMgr.g_objDataAccessor.CommitTransaction();
                        }
                    }

                    switch (strElement)
                    {
                        case "Claim":
                            CallLoadProcedure(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/FNOLClearLoadTablesSP"), objElement);
                            // Call FNOLLoadClaimSP only if we are creating a new claim.
                            //if(blnClaimDataIncluded)
                            CallLoadProcedure(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadClaimSP"), objElement);
                            break;
                        case "Coverage":
                            CallLoadProcedure(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadClaimCoverageSP"), objElement);
                            break;
                        case "Vehicle":
                            CallLoadProcedure(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadClaimVehicleSP"), objElement);
                            break;
                        case "Involved":
                            CallLoadProcedure(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadInvolvedSP"), objElement);
                            break;
                        case "Property":
                            CallLoadProcedure(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/FNOLLoadPropertySP"), objElement);
                            break;
                        case "BeginInsert":
                            //Add the ClaimDataIncluded attribute.
                            xmlElemAppend = objElement;
                            if (objElement.SelectSingleNode("@NewClaimFlag") == null)
                                objMDomUtils.AddAttribute(ref xmlElemAppend, "NewClaimFlag", (blnClaimDataIncluded ? "1" : "0"));

                            CallLoadProcedure(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/FNOLInsClaimSP"), objElement);
                            break;
                    }

                    if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                        objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "LynxID = ", lngUID.ToString() }), string.Concat(new string[] { PROC_NAME, "FNOL portion completed to ADP DB" }));

                    //Automatic shop assignment is not critical to the web assignment because
                    //the claims reps can pick it up later.  Disable rollbacks and commit.
                    blnRollback = false;

                    //Commit if the transaction still exists.
                    if (blnInTrans == true)
                        objMLAPDEcadAccessorMgr.g_objDataAccessor.CommitTransaction();

                    //Should we send an assignment through CCC to Autoverse to Mitchell for our Auto Techs?
                    if (IsAutoTechAssignment(ref objParamDom))
                    {
                        //Allow an override from the config file.
                        if (objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/AutoAssignAutoTech/@Enabled") == "True")
                        {
                            if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Auto-Tech Auto Assignment" }));

                            //Make the assignment through CCC to AutoVerse to Mitchell.
                            SendShopAssignments(objParamDom, lngUID, true);
                        }
                        else if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                            objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Auto-Tech Auto Assignment - disabled in config.xml" }));
                    }

                       //Should we call CommServer to send a program shop assignment?

                       //HACK:  The AssignmentAtSelectionFlag is currently an attribute of the Insurance Company.  This will now need to be
                    // resolved based on the InsuranceCompany and AssignmentType.  The 'or' condition below must then be removed.
                    else if (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Claim/@AssignmentAtSelectionFlag", true) == "1" ||
                          objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Claim/@AssignmentDescription", true) == "Mobile Electronics Assignment")

                        //Allow an override from the config file.
                        if (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Claim/@AssignmentDescription", true) == "Mobile Electronics Assignment")
                        {
                            //ME assignment to FCS
                            if (objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/AutoAssignME/@Enabled") == "True")
                            {
                                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "ME Auto Assignment" }));

                                //Make the assignments, one per vehicle.
                                SendShopAssignments(objParamDom, lngUID, false);
                            }
                            else if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "ME Auto Assignment - disabled in config.xml" }));
                        }
                        else if (objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/AutoAssignShop/@Enabled") == "True")
                        {
                            //regular shop assignment
                            if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Shop Auto Assignment" }));

                            //Make the assignments, one per vehicle.
                            SendShopAssignments(objParamDom, lngUID, false);
                        }
                        else if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                            objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Shop Auto Assignment - disabled in config.xml" }));

                        else if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                            objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Automatic Assignment not required." }));

                    if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                        objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "LynxID = ", lngUID.ToString() }), string.Concat(new string[] { PROC_NAME, "Finished" }));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (blnRollback)
                    objMLAPDEcadAccessorMgr.g_objDataAccessor.RollbackTransaction();
            }
            //Hand back the new LynxID.
            return lngUID;
        }

        /// <summary>
        /// Generate and save the web assignment report.
        /// Returns the document location of the generated report
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <param name="lngLynxID"></param>
        /// <returns></returns>
        public string SaveWAReport(ref XmlDocument objParamDom, long lngLynxID)
        {
            string PROC_NAME = string.Empty;

            XmlDocument objWARptConfigDOM = null;
            XmlDocument objUserClaimDOM = null;
            Recordset objRS = null;
            Stream objPDFReport = null;
            XmlElement objFirstVehicleNode = null;
            XmlElement objClaimAspectOwner = null;

            string strCreatedUserID = string.Empty,
             strDocLocation = string.Empty,
             strLoadUserClaim = string.Empty,
             strAssignmentTypeID = string.Empty,
             strOwnerNameFirst = string.Empty,
             strOwnerNameLast = string.Empty,
             strOwnerPhone = string.Empty,
             strOwnerEmailAddress = string.Empty,
             strRSFileName = string.Empty,
             strReportFileName = string.Empty,
             strFileName = string.Empty,
             strFilePath = string.Empty,
             strSqlNow = string.Empty,
              strWAFirstVehicleNumber = string.Empty,
             strWAFirstVehicleVIN = string.Empty,
             strWAInsuranceCompanyID = string.Empty,
             strCurServiceChannelCD = string.Empty,
             strPertainsTo = string.Empty,
             strExposure = string.Empty;
            bool blnRaiseToCaller = true;
            int iFailOverCount = 0;
            string strAppend = string.Empty,
                strXmlAppend = string.Empty;

            try
            {
                objReportUtil = new clsReport();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SaveWAReport: " });

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "Creating web assignment report for LynxID: ", (lngLynxID.ToString()) }), PROC_NAME);

                if (lngLynxID > 0)
                {
                    //Get the assignment type id from the web assignment
                    strAssignmentTypeID = (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Vehicle/AssignmentTypeID", true));
                    strWAFirstVehicleNumber = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Vehicle/VehicleNumber", false);
                    strWAFirstVehicleVIN = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Vehicle/VIN", false);
                    strWAInsuranceCompanyID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/InsuranceCompanyID", true);

                    objWARptConfigDOM = new XmlDocument();
                    objUserClaimDOM = new XmlDocument();

                    //Load the Web assignment report config file
                    strAppend = string.Concat(new string[] { objMLAPDEcadAccessorMgr.g_strSupportDocPath, "\\WA ReportMap", ".xml" });
                    objMDomUtils.LoadXmlFile(ref objWARptConfigDOM, ref strAppend, PROC_NAME, "Web Assignment Report Config");

                    //Get the RS template filename
                    //        strRSFileName = GetChildNodeText(objWARptConfigDOM, "/Root/Map[@AssignmentTypeID='" & strAssignmentTypeID & "']/@RS", True)
                    //we are going to use the crystal ttx input format instead of rs. RS is creating multi-step errors when setting the data.
                    // we will build the recordset schema on-the-fly from the ttx.
                    strRSFileName = objMLAPDPartnerDataMgr.GetChildNodeText(ref objWARptConfigDOM, "/Root/Map[@AssignmentTypeID='" + strAssignmentTypeID + "']/@TTX", true);

                    //Get the report file name
                    strReportFileName = objMLAPDPartnerDataMgr.GetChildNodeText(ref objWARptConfigDOM, "/Root/Map[@AssignmentTypeID='" + strAssignmentTypeID + "']/@RPT", true);

                    //Get the LoadUserClaim setting
                    strLoadUserClaim = objMLAPDPartnerDataMgr.GetChildNodeText(ref objWARptConfigDOM, "/Root/Map[@AssignmentTypeID='" + strAssignmentTypeID + "']/@loadClaimUser", true);

                    if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                        objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] {"AssignmentTypeID=" , strAssignmentTypeID , " " ,
                                                              "strRSFileName=" , strRSFileName , " ", 
                                                              "strReportFileName=" , strReportFileName , " " , 
                                                              "strLoadUserClaim=" , strLoadUserClaim}), PROC_NAME);

                    objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                    if (strLoadUserClaim == "1")
                    {
                        //Need to find out the claim owner of this web assignment
                        strXmlAppend = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(
                             objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/GetVehicleListSP"), new object[] {
                              "@LynxID", lngLynxID,
                              "@InsuranceCompanyID", strWAInsuranceCompanyID});

                        objMDomUtils.LoadXml(ref objUserClaimDOM, ref strXmlAppend, "UserClaimDetail", "User Claim Detail");

                        if (strWAFirstVehicleVIN != string.Empty)
                            //choosing the vehicle by VIN will be more accurate than the vehicle number.
                            objFirstVehicleNode = (XmlElement)objMLAPDPartnerDataMgr.GetChildNode(ref objUserClaimDOM, "/Root/Vehicle[@VIN='" + strWAFirstVehicleVIN + "']"); //Todo:Verify
                        else
                            // Theoritically the vehicle number passed in will match the claims vehicle number.
                            objFirstVehicleNode = (XmlElement)objMLAPDPartnerDataMgr.GetChildNode(ref objUserClaimDOM, "/Root/Vehicle[@VehicleNumber='" + strWAFirstVehicleNumber + "']");

                        if (objFirstVehicleNode != null)
                        {
                            strCurServiceChannelCD = objFirstVehicleNode.GetAttribute("CurrentServiceChannelCD");
                            objClaimAspectOwner = (XmlElement)objFirstVehicleNode.SelectSingleNode("ClaimAspectOwner");

                            if (objClaimAspectOwner != null)
                            {
                                //Extract the Entity Owner Information based on the service channel
                                switch (strCurServiceChannelCD)
                                {
                                    case "PS":
                                    case "ME":
                                    case "TL":
                                        strOwnerNameFirst = objClaimAspectOwner.GetAttribute("UserNameFirst");
                                        strOwnerNameLast = objClaimAspectOwner.GetAttribute("UserNameLast");
                                        strOwnerPhone = string.Concat(new string[] {objClaimAspectOwner.GetAttribute("UserPhoneAreaCode") , "-" , 
                                                         objClaimAspectOwner.GetAttribute("UserPhoneExchangeNumber") , "-" , 
                                                         objClaimAspectOwner.GetAttribute("UserPhoneUnitNumber")});

                                        if (objClaimAspectOwner.GetAttribute("UserExtensionNumber") != string.Empty)
                                            strOwnerPhone = string.Concat(new string[] { strOwnerPhone, " x", objClaimAspectOwner.GetAttribute("UserExtensionNumber") });

                                        strOwnerEmailAddress = objClaimAspectOwner.GetAttribute("UserEmail");
                                        break;
                                    case "DA":
                                    case "DR":
                                        strOwnerNameFirst = objClaimAspectOwner.GetAttribute("AnalystUserNameFirst");
                                        strOwnerNameLast = objClaimAspectOwner.GetAttribute("AnalystUserNameLast");
                                        strOwnerPhone = string.Concat(new string[] { objClaimAspectOwner.GetAttribute("AnalystUserPhoneAreaCode"), "-", objClaimAspectOwner.GetAttribute("AnalystUserPhoneExchangeNumber"), "-", objClaimAspectOwner.GetAttribute("AnalystUserPhoneUnitNumber") });

                                        if (objClaimAspectOwner.GetAttribute("AnalystUserExtensionNumber") != string.Empty)
                                            strOwnerPhone = string.Concat(new string[] { strOwnerPhone, " x", objClaimAspectOwner.GetAttribute("AnalystUserExtensionNumber") });

                                        strOwnerEmailAddress = objClaimAspectOwner.GetAttribute("AnalystUserEmail");
                                        break;
                                    default:
                                        strOwnerNameFirst = objClaimAspectOwner.GetAttribute("SupportUserNameFirst");
                                        strOwnerNameLast = objClaimAspectOwner.GetAttribute("SupportUserNameLast");
                                        strOwnerPhone = string.Concat(new string[] { objClaimAspectOwner.GetAttribute("SupportUserPhoneAreaCode"), "-", objClaimAspectOwner.GetAttribute("SupportUserPhoneExchangeNumber"), "-", objClaimAspectOwner.GetAttribute("SupportUserPhoneUnitNumber") });

                                        if (objClaimAspectOwner.GetAttribute("SupportUserExtensionNumber") != string.Empty)
                                            strOwnerPhone = string.Concat(new string[] { strOwnerPhone, " x", objClaimAspectOwner.GetAttribute("SupportUserExtensionNumber") });

                                        strOwnerEmailAddress = objClaimAspectOwner.GetAttribute("SupportUserEmail");
                                        break;
                                }
                            }

                            if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] {"strOwnerNameFirst=" , strOwnerNameFirst , " ", 
                                                                      "strOwnerNameLast=" , strOwnerNameLast , " " , 
                                                                      "strOwnerPhone=" , strOwnerPhone , " " , 
                                                                       "strOwnerEmail=" , strOwnerEmailAddress}), PROC_NAME);
                        }

                        objFirstVehicleNode = null;
                    }
                    //Transform the Web Assignment XML to Recordset
                    objRS = LoadWAXML2RS(ref objParamDom, lngLynxID, strRSFileName, strOwnerNameFirst, strOwnerNameLast, strOwnerPhone, strOwnerEmailAddress);

                    //check if the recordset is valid
                    if (objRS.Fields.Count == 0)
                        //something went wrong and the recordset is empty
                        throw new Exception(string.Concat(new string[] { MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset.ToString(), PROC_NAME, "Invalid recordset returned while converting Web Assignment to Recordset" }));

                    //Generate the report file

                    if (objReportUtil != null)
                    {
                        iFailOverCount = 1;
                        //Randomize();

                        objPDFReport = objReportUtil.getWAReport(objRS, string.Concat(new string[] { objMLAPDEcadAccessorMgr.g_strSupportDocPath, "\\", strReportFileName }), "PDF");

                        if (objPDFReport != null)
                        {
                            strFileName = BuildFileName("WA", lngLynxID);
                            if (strFileName != string.Empty)
                            {
                                strFilePath = string.Concat(new string[] { (objMLAPDEcadAccessorMgr.GetConfig("Document/RootDirectory")).Replace("\\\\", "\\"), strFileName });
                                objPDFReport.SaveToFile(strFilePath, SaveOptionsEnum.adSaveCreateNotExist);

                                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "Document saved at ", strFilePath }), PROC_NAME);

                                strSqlNow = DateTime.Now.ToString("yyyy-mm-ddThh:mm:ss");

                                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_Recordset);

                                // This hack added because VehicleNumber is not part of the FNOL Assignment XML.  This report must be switched to
                                // be produced from APD's standard (post XSLT) XML.
                                if (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//DataSource") == "FNOL Assignment")
                                {
                                    strExposure = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Vehicle/ExposureCD");
                                    strPertainsTo = string.Concat(new string[] { "Veh", strExposure == "1" ? "1" : "2" });
                                }
                                else
                                    strPertainsTo = string.Concat(new string[] { "Veh", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Vehicle/VehicleNumber", true) });

                                // Get the created user ID.
                                if (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/DataSource", true) == "FNOL Assignment")
                                    strCreatedUserID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/CarrierRepUserID");

                                if (strCreatedUserID == string.Empty)
                                    strCreatedUserID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/FNOLUserID", true);
                                else
                                    strCreatedUserID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "/WebAssignment/Claim/CarrierRepUserID", true);

                                //Attach the document to the first vehicle in the web assignment
                                objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParams(
                                    objMLAPDPartnerDataMgr.GetConfig("PartnerSettings/InsertSP"),new object[] {
                                            "@NTUserID", "SYSTEM",
                                            "@CreatedUserID", strCreatedUserID,
                                            "@PertainsTo", strPertainsTo,
                                            "@LynxID", lngLynxID,
                                            "@NotifyEvent", 0,
                                            "@DirectionalCD", "I",
                                            "@CreatedDate", strSqlNow,
                                            "@ImageDate", strSqlNow,
                                            "@ImageLocation", strFileName.Replace("\\", "\\\\"),
                                            "@DocumentSource", "ClaimPoint",
                                            "@DocumentType", "Client Assignment",
                                            "@ImageType", "pdf"});

                                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "Web assignment report attached to ", strPertainsTo }), PROC_NAME);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Add file name to xml as a root level attribute.
            return strFileName;
        }


        #endregion

        #region Private Helper Functions
        // Create and Destruct globals
        private void Class_Initialize()
        {
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDEcadAccessorMgr.InitializeGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Class_Terminate()
        {
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDEcadAccessorMgr.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns True if we should send an assignment through
        /// CCC to Autoverse to Mitchell for our Auto Techs?
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>

        private bool IsAutoTechAssignment(ref XmlDocument objParamDom)
        {
            int intAssignmentType;
            bool blnAssignmentType = false;
            try
            {
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                //Extract the assignment type.

                intAssignmentType = Convert.ToInt32(objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//Vehicle/@AssignmentTypeID", true));

                //Temporary hack - match assignment type up to config settings.
                blnAssignmentType = Convert.ToBoolean("True" == objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/AutoAssignAutoTech/AssignmentType[@ID='" + intAssignmentType + "']/@Enabled"));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnAssignmentType;
        }

        /// <summary>
        ///  Get a new LynxID for the assignment.
        /// </summary>
        /// <returns></returns>

        private long GetFnolUID()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "GetFnolUID: " });
            string strFNOLUID = string.Empty;
            try
            {

                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, PROC_NAME);

                //Get the procedure level list from the APD database.
                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eFNOL_Recordset);

                strFNOLUID =Convert.ToString(objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParams(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/GetLynxIdSP").ToString(),null));

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { "UID = ", strFNOLUID }));

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Convert.ToInt64(strFNOLUID);
        }

        /// <summary>
        /// Store the assignment in the partner database
        /// </summary>
        /// <param name="lngUID"></param>
        /// <param name="strDataSource"></param>
        /// <param name="strXml"></param>

        private void StoreAssignmentXml(long lngUID, string strDataSource, string strXml)
        {
            string PROC_NAME = string.Empty;
            try
            {
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StoreAssignmentXml: " });
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMEscapeUtilities = new MEscapeUtilities();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, PROC_NAME);

                //Get the procedure level list from the APD database.
                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.ePartner_Recordset);

                objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/StoreAssignmentXmlSP"),new object[] { lngUID, "'" + strDataSource + "'", "'" + objMEscapeUtilities.SQLQueryString(strXml) + "'"});

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Gets the procedure level list for the default Client Access insurance company.
        /// </summary>
        private void GetProcedureLevelList()
        {
            string PROC_NAME = string.Empty;
            string strName = string.Empty,
                strDebug = string.Empty;
            int intIndex = 0;
            int intID;
            typProc tProc;
            Recordset rsRecords = null;
            try
            {
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "GetProcedureLevelList: " });
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMRecordsetUtils = new MRecordsetUtils();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                //Add a trace mesage to the debug log.
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Started" }));

                rsRecords = new Recordset();

                rsRecords = objMLAPDEcadAccessorMgr.g_objDataAccessor.OpenRecordsetSp(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/ProcedureLevelListSP"),null);

                if (rsRecords == null)
                    throw new Exception(string.Concat(new string[] { EventCodes.eNoRecordsetReturned.ToString(), PROC_NAME, "No recordset returned from DB." }));


                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMRecordsetUtils.DebugRS(rsRecords);

                rsRecords.MoveFirst();
                //  ReDim marrProcList(RS.RecordCount)
                intIndex = 0;

                while (!rsRecords.EOF)
                {
                    intID = Convert.ToInt32(rsRecords.Fields["ProcedureID"]);
                    tProc.intID = intID;
                    tProc.intLevelID = Convert.ToInt32(rsRecords.Fields["ProcedureLevelID"]);
                    tProc.strName = (rsRecords.Fields["ProcName"]).ToString();

                    marrProcList[intIndex] = tProc;

                    rsRecords.MoveNext();
                    intIndex = intIndex + 1;
                }

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (rsRecords != null)
                {
                    if (rsRecords.State == Convert.ToInt32(ObjectStateEnum.adStateOpen))
                        rsRecords.Close();
                    rsRecords = null;
                }

            }
        }

        /// <summary>
        /// Calls an APD load procedure to add some data.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="objElement"></param>
        /// <returns></returns>
        private void CallLoadProcedure(string strProcName, XmlElement objElement)
        {
            string PROC_NAME = string.Empty;
            Collection colParam = null;
            try
            {
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CallLoadProcedure: " });

                colParam = new Collection();

                //Loop through all the attributes in the passed element.
                foreach (XmlAttribute objAttrib in objElement.Attributes)     //Build up a collection of parameters with values.
                    colParam.Add(objAttrib.Value, string.Concat(new string[] { "@", objAttrib.Name }));

                //Now call DataAccessor and let DA match up the params.
                objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNpCol(strProcName, colParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }  //TODO:verify

        /// <summary>
        /// SendShopAssignments sends shop assignments automatically.
        /// 
        /// The purpose of this method is to make the assignment so that our claims
        /// reps do not have to.  This class used to be rather independant of the XML
        /// passed in, but this method breaks that and ties this class to Vehicle.
        /// 
        /// This method loops through the vehicles and calls an APD procedure to get the
        /// AssignmentID for any shop assignment.
        /// 
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="lngLynxID"></param>
        /// <param name="blnStaffAppraiserAssignment"></param>
        private void SendShopAssignments(XmlDocument objDom, long lngLynxID, bool blnStaffAppraiserAssignment)
        {
            string PROC_NAME = string.Empty;

            //Objects that need clean-up
            XmlNodeList objVehList = null;

            //Primitives that don't
            string strAssignmentID = string.Empty;
            int intVehNum = 0;
            try
            {
                //Create an instance of CommServer.
                objCommSvr = new CCommServer();

                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendShopAssignments: " });

                //Select and loop through all the "valid" vehicle elements (These will have an AssignmentTypeID attribute).
                objVehList = objDom.SelectNodes("//Vehicle[@AssignmentTypeID]");

                foreach (XmlElement objVeh in objVehList)
                {
                    intVehNum = Convert.ToInt32(objVeh.GetAttribute("VehicleNumber"));

                    //Get the assignment ID of the assignment for this vehicle.
                    strAssignmentID =Convert.ToString(objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParams(objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/SPMap/AssignmentDecodeSP"),new object[] {
                            "@LynxID", lngLynxID,
                            "@VehicleNumber", intVehNum,
                            "@AssignmentSuffix", string.Empty}));

                    //  Skip the assignment process if no assignment record was found for the LynxID-VehicleNumber
                    if (strAssignmentID != string.Empty && strAssignmentID != "0")

                        //Assign the shop first.
                        objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSp(objMLAPDEcadAccessorMgr.GetConfig("ClaimPoint/WebAssignment/AssignShopSP"),new object[] {strAssignmentID});

                    //Now send the assignment, electronic and/or fax.
                    objCommSvr.AssignShop(Convert.ToInt32(strAssignmentID), Convert.ToInt32(objMLAPDEcadAccessorMgr.GetConfig("WorkFlow/SysUserID")), blnStaffAppraiserAssignment);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Queries APD for user information based upon the login ID.
        /// Adds that user to APD if it was not found in APD.
        /// </summary>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        internal long FindOrAddUser(ref XmlDocument objParamDom)
        {
            string PROC_NAME = string.Empty;
            //Objects that will require clean-up.
            XmlDocument objResults = null;

            //Primiatives that don't require clean-up.
            bool blnExternalUser;

            string strUserID = string.Empty, strPassedUserID = string.Empty, strOtherUserID = string.Empty,
             strLogin = string.Empty,
             strEmail = string.Empty,
             strApplicationCD = string.Empty,
             strApplicationID = string.Empty,
             strInsuranceCoID = string.Empty;
            long lngUserId = 0;

            try
            {
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "FindOrAddUser: " });
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMLAPDEcadAccessorMgr.ConnectToDB(MLAPDEcadAccessorMgr.EDatabase.eAPD_XML);

                strApplicationCD = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ApplicationCD", true);
                strApplicationID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ApplicationID", true);
                strInsuranceCoID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@InsuranceCompanyID", true);

                //Pull user info from the assignment DOM.
                strUserID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepUserID", false);
                strLogin = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepLogin", true);
                strEmail = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepEmailAddress", true);

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] {"Login = " , strLogin , " Email = " , strEmail 
        , " UserID = " , strUserID , " AppCD = " , strApplicationCD , " AppID = " 
        , strApplicationID , " InsCo = " , strInsuranceCoID}), string.Concat(new string[] { PROC_NAME, "Started" }));

                strPassedUserID = strUserID;

                //Passed a valid user ID?  --> not '0', '-1', or ''
                if (IsValidUserID(strPassedUserID))
                {
                    //Find user ID in DB for any application.
                    strUserID = SessionGetUserID(string.Empty, strPassedUserID, "", "ALL");

                    // If a user ID is passed, that user record must exist, otherwise raise an error.
                    if (!IsValidUserID(strUserID))
                        throw new Exception(string.Concat(new string[] { EventCodes.eUserIdDoesNotExist.ToString(), "User Validation", string.Concat(new string[] { "User ", strEmail, " does not exist for APD." }) }));

                    //Were not passed a valid user ID...
                    else
                        //Find login in DB for passed application.
                        strOtherUserID = SessionGetUserID(strLogin, string.Empty, string.Empty, strApplicationCD);

                    //User already exists in passed application?
                    if (IsValidUserID(strOtherUserID))
                        strUserID = strOtherUserID;

                        //Did not exist in passed application.
                    else
                        // Find userid by email
                        strUserID = SessionGetUserID(string.Empty, string.Empty, strEmail, "ALL");

                    // UserID found for passed email address?  Add to the other (CP,SG) application.
                    if (IsValidUserID(strUserID))
                    {
                        // User exists but not for the passed application.  Add new application record for existing user record.
                        AddUserToApplication(strUserID, strApplicationID, strLogin);
                    }
                    else
                    {
                        //Otherwise create new user.
                        strUserID = InsertToUserTable(strEmail, ref objParamDom);

                        //Add application record for passed application.
                        AddUserToApplication(strUserID, strApplicationID, strLogin);
                    }
                }

                if (IsValidUserID(strUserID))
                    lngUserId = Convert.ToInt64(strUserID);

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "UserID = ", strUserID }), string.Concat(new string[] { PROC_NAME, "Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lngUserId;
        }

        /// <summary>
        /// Checks validity of passed user ID
        /// </summary>
        /// <param name="strUserID"></param>
        /// <returns></returns>
        private bool IsValidUserID(string strUserID)
        {
            try
            {
                return Convert.ToBoolean(strUserID != string.Empty && strUserID != "0" && strUserID != "-1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the APD user id for a user.
        /// </summary>
        /// <param name="strLoginID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strEmail"></param>
        /// <param name="strApplicationCD"></param>
        /// <returns></returns>
        private string SessionGetUserID(string strLoginID, string strUserID, string strEmail, string strApplicationCD)
        {
            XmlDocument objResults = null;
            string strAppend = string.Empty;
            try
            {
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                //Get the session user detail for this user.
                if (strUserID != string.Empty)
                {
                    strAppend = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(
                            objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/GetUserDetailSP"), new object[] {
                             "@ApplicationCD", strApplicationCD,
                             "@UserID", strUserID});
                    objMDomUtils.LoadXml(ref objResults, ref strAppend, "SessionGetUserID", "User Detail");
                }
                else if (strEmail != string.Empty)
                {
                    strAppend = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(
                            objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/GetUserDetailSP"), new object[] {
                             "@ApplicationCD", strApplicationCD,
                             "@Email", strEmail});
                    objMLAPDPartnerDataMgr.LoadXml(ref objResults, ref strAppend, "SessionGetUserID", "User Detail");
                }
                else
                {
                    strAppend = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(
                            objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/GetUserDetailSP"), new object[] {
                             "@ApplicationCD", strApplicationCD,
                             "@Login", strLoginID});
                    objMLAPDPartnerDataMgr.LoadXml(ref objResults, ref strAppend, "SessionGetUserID", "User Detail");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objResults = null;
            }
            //Get the user ID from the returned XML
            return objMLAPDPartnerDataMgr.GetChildNodeText(ref objResults, "/Root/User/@UserID");
        }

        /// <summary>
        /// Gets the user table id for a user.
        /// </summary>
        /// <param name="strEmail"></param>
        /// <param name="strApplicationCD"></param>
        /// <param name="strInsuranceCoID"></param>
        /// <returns></returns>
        private string AdmGetUserID(string strEmail, string strApplicationCD, string strInsuranceCoID)
        {
            XmlDocument objResults = null;
            string strXmlAppend = string.Empty;
            try
            {
                objResults = new XmlDocument();
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                //Get the Admin user detail for this user.
                strXmlAppend = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(
                        objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/AdmGetUserDetailSP"), new object[] {
                         "@EmailAddress", strEmail,
                         "@ApplicationCD", strApplicationCD,
                         "@InsuranceCompanyID", strInsuranceCoID});

                objMDomUtils.LoadXml(ref objResults, ref strXmlAppend, "AdmGetUserID", "Adm User Detail");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Get the user ID from the returned XML
            return objMLAPDPartnerDataMgr.GetChildNodeText(ref objResults, "/Root/@UserID");
        }

        /// <summary>
        ///  Adds a user to the user table.
        /// </summary>
        /// <param name="strEmail"></param>
        /// <param name="objParamDom"></param>
        /// <returns></returns>
        private string InsertToUserTable(string strEmail, ref XmlDocument objParamDom)
        {
            XmlDocument objResults = null;
            string strOfficeID = string.Empty,
                strOfficeCode = string.Empty,
                strApplicationCD = string.Empty,
                strParamXmlAppend = string.Empty;
            //Insert the Admin user detail for this user.

            // FNOL Office IDs can be resolved from the database.  Need to ensure any other users of this code that wish to do the
            // same include InsuranceCompanyID and ClientOfficeID values in their xml.  Currently the only other user of this code
            // is Scene Genesis -- their OfficeID is pulled from the config file.
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                strApplicationCD = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ApplicationCD");

                if (strApplicationCD != "FNOL" && strApplicationCD != "CCAV")
                    strOfficeID = objMLAPDPartnerDataMgr.GetConfig("PartnerSettings/SceneGenesis/Carrier/@OfficeID");
                else
                {
                    strOfficeCode = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ClientOfficeID", true);
                    strOfficeID = objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/ClientOfficeID/Client[@Code='" + strOfficeCode + "']/@OfficeID");
                }

                strParamXmlAppend = objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(
                              objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/AdmInsUserDetailSP"), new object[] { "@OfficeID", strOfficeID,
                               "@InsuranceCompanyID", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@InsuranceCompanyID"),
                               "@ClientOfficeID", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@ClientOfficeID"),
                               "@SupervisorUserID", "", "@SupervisorFlag", "0", "@AssignmentBeginDate", "", "@AssignmentEndDate", "",
                               "@EmailAddress", strEmail,
                               "@FaxAreaCode", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFax")).Substring(0, 3),
                               "@FaxExchangeNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFax")).Substring(4, 3),
                               "@FaxUnitNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFax").Substring((objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFax")).Length - 4, 4)),
                               "@FaxExtensionNumber", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepFaxExt"),
                               "@NameFirst", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepNameFirst"),
                               "@NameLast", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepNameLast"),
                               "@NameTitle", objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepNameTitle"),
                               "@PhoneAreaCode", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDay")).Substring(0, 3),
                               "@PhoneExchangeNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDay")).Substring(4, 3),
                               "@PhoneExtensionNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDayExtn")).Substring(0, 5),
                               "@PhoneUnitNumber", (objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDay")).Substring((objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//@CarrierRepPhoneDay")).Length - 4, 4),
                               "@WorkdayList", "", "@LicenseAssignStList", "",
                               "@RoleList", objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/NewUserRoleList"),
                               "@ProfileList", objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/NewUserProfileList"),
                               "@PermissionList", objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/NewUserPermissionList"),
                               "@SysLastUserID", 0});

                objMDomUtils.LoadXml(ref objResults, ref strParamXmlAppend, "InsertToUserTable", "Adm Ins User Detail");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Extract and return the newly created user ID.
            return objMLAPDPartnerDataMgr.GetChildNodeText(ref objResults, "/Root/User/@UserID");
        }

        /// <summary>
        ///  Adds a user to the user table.
        /// </summary>
        /// <param name="strUserID"></param>
        /// <param name="strApplicationID"></param>
        /// <param name="strLogin"></param>
        private void AddUserToApplication(string strUserID, string strApplicationID, string strLogin)
        {
            string[] arrParamToSp = null;
            try
            {
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                objMLAPDEcadAccessorMgr.g_objDataAccessor.BeginTransaction();

                arrParamToSp = new string[] { };

                //Add the user to the application.
                objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(
                     objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/AdmInsUserAppDetailSP"), new object[] {
                      "@UserID", strUserID,
                      "@ApplicationID", strApplicationID,
                      "@LogonId", strLogin,
                      "@PasswordHint", "",
                      "@SysLastUserID", 0});

                //Activate the user for the application.
                objMLAPDEcadAccessorMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(
                    objMLAPDPartnerDataMgr.GetConfig("ClaimPoint/WebAssignment/AdmUserSetStatusSP"), new object[] {
                     "@UserID", strUserID,
                     "@ApplicationID", strApplicationID,
                     "@SysLastUserID", 0});
            }
            catch (Exception ex)
            {
                objMLAPDEcadAccessorMgr.g_objDataAccessor.RollbackTransaction();
                throw ex;
            }
            finally
            {
                objMLAPDEcadAccessorMgr.g_objDataAccessor.CommitTransaction();
            }
        }

        private Recordset LoadWAXML2RS(ref XmlDocument objParamDom, long lngLynxID, string strRSFileName, string strOwnerNameFirst, string strOwnerNameLast, string strOwnerPhone, string strOwnerEmailAddress)
        {
            string PROC_NAME = string.Empty;
            Recordset objRS = null;
            XmlNode objClaimNode = null;
            XmlNodeList objVehiclesNodeList = null;
            XmlNode objVehicleNode = null, objChildNode = null;
            Dictionary objFieldsDef = null;
            Field objFld = null;
            TextStream oTTXFile = null;
            FileSystemObject oFSO = null;
            string sLine = string.Empty,
                sFldName = string.Empty,
                sFldType = string.Empty;
            string[] aData = null;
            try
            {
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "LoadWAXML2RS: " });
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "RSFile=", strRSFileName, Environment.NewLine, "Creating the Recordset..." }));

                //Generate the recordset format from the crystal reports input ttx format
                //ttx file is a tab delimited file
                objRS = new Recordset();
                objFieldsDef = new Dictionary();
                oFSO = new FileSystemObject();

                objFieldsDef.RemoveAll();
                objFieldsDef.CompareMode.Equals(StringComparison.CurrentCultureIgnoreCase);

                oTTXFile = oFSO.OpenTextFile(string.Concat(new string[] { objMLAPDEcadAccessorMgr.g_strSupportDocPath, "\\", strRSFileName }), IOMode.ForReading, false);
                while (!oTTXFile.AtEndOfStream)
                {
                    sLine = (oTTXFile.ReadLine().Trim());
                    //comments start with ;
                    if (sLine.Substring(0, 1) != ";")
                    {
                        //current line is not a comment
                        //Field Name<tab>data type<tab>data length (for string)<tab>sample text
                        aData = Regex.Split(sLine, Environment.NewLine, RegexOptions.IgnoreCase);
                        sFldName = aData[0];
                        sFldType = aData[1];
                        //default field type is string and is 255 chars long
                        switch (sFldType)
                        {
                            case "memo":
                                objRS.Fields.Append(sFldName, DataTypeEnum.adVarChar, 10000);
                                break;
                            default:
                                objRS.Fields.Append(sFldName, DataTypeEnum.adVarChar, 255);  //Crystal 8.5 string type limit is 255
                                break;
                        }
                        objFieldsDef.Add(sFldName, sFldName);
                    }
                }
                objRS.Open();
                oFSO = null;
                oTTXFile = null;

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("Loading data into the recordset...");

                //Get the claim node from the web assignment
                objClaimNode = objMLAPDPartnerDataMgr.GetChildNode(ref objParamDom, "/WebAssignment/Claim", true);

                //Get the vehicle list from the web assignment
                objVehiclesNodeList = objParamDom.SelectNodes("/WebAssignment/Vehicle");
                if (objVehiclesNodeList == null)
                    throw new Exception(string.Concat(new string[] { MLAPDEcadAccessorMgr.EDatabase.eAPD_XML.ToString(), "/WebAssignment/Vehicle", "WebAssignment is missing a 'Vehicle' child node." }));

                //Now load the XML into the RS

                //We will loop thru the vehicle list and merge the vehicle and claim data
                foreach (XmlNode objVehicleNodeforlists in objVehiclesNodeList)
                {
                    objRS.AddNew();

                    if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                        objMLAPDEcadAccessorMgr.g_objEvents.Trace("Populating Claim Data");
                    //First load the claim information
                    foreach (XmlNode objChildNodeforclaim in objClaimNode.ChildNodes)
                    {
                        if (objFieldsDef.Exists(string.Concat(new string[] { "Claim_", objChildNodeforclaim.Name })))
                            objRS.Fields[string.Concat(new string[] { "Claim_", objChildNodeforclaim.Name })].Value = objChildNodeforclaim.InnerText;
                    }

                    if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                        objMLAPDEcadAccessorMgr.g_objEvents.Trace("Populating Vehicle Data");

                    //Load the vehicle information
                    foreach (XmlNode objChildNodeforVehicle in objVehicleNode.ChildNodes)
                    {
                        if (objFieldsDef.Exists(string.Concat(new string[] { "Vehicle_", objChildNodeforVehicle.Name })))
                            objRS.Fields[string.Concat(new string[] { "Vehicle_", objChildNodeforVehicle.Name })].Value = objChildNodeforVehicle.InnerText;
                    }

                    if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                        objMLAPDEcadAccessorMgr.g_objEvents.Trace("Populating Claim Header data");
                    //stuff in the LynxID
                    if (objFieldsDef.Exists("Claim_LynxID"))
                        objRS.Fields["Claim_LynxID"].Value = (lngLynxID).ToString();

                    //stuff in the owner information
                    if (objFieldsDef.Exists("Claim_OwnerNameFirst"))
                        objRS.Fields["Claim_OwnerNameFirst"].Value = strOwnerNameFirst;

                    if (objFieldsDef.Exists("Claim_OwnerNameLast"))
                        objRS.Fields["Claim_OwnerNameLast"].Value = strOwnerNameLast;

                    if (objFieldsDef.Exists("Claim_OwnerPhone"))
                        objRS.Fields["Claim_OwnerPhone"].Value = strOwnerPhone;

                    if (objFieldsDef.Exists("Claim_OwnerEmailAddress"))
                        objRS.Fields["Claim_OwnerEmailAddress"].Value = strOwnerEmailAddress;

                    objRS.Update();
                }
                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "objRS ", objRS.GetString() }), PROC_NAME);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oFSO = null;
                oTTXFile = null;
                objRS = null;
                objClaimNode = null;
                objVehicleNode = null;
                objVehiclesNodeList = null;
                objChildNode = null;
                objFieldsDef = null;
                objFld = null;
            }
            //Return the recordset
            return objRS;
        }

        private string BuildFileName(string strEntity, long lngLynxID)
        {
            int x = 0;
            string strImageFolder = string.Empty,
             strLynxID = string.Empty,
             strDateTime = string.Empty;
            try
            {
                //Use the current date time to make the file name unique.
                strDateTime = DateTime.Now.ToString("yyyymmddhhmmss");

                //Get the last 4 digits of the lynx id for the file folder hierarchy.
                strLynxID = (lngLynxID.ToString()).Substring(Convert.ToInt32(lngLynxID) - 4, 4);

                for (x = 1; x <= 4; x++)
                    strImageFolder = string.Concat(new string[] { strImageFolder, "\\", strLynxID.Substring(x, 1) });

                strImageFolder = string.Concat(new string[] { strImageFolder, "\\" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //Put it all together now...
            return string.Concat(new string[] { strImageFolder, strEntity, strDateTime, "LYNXID", lngLynxID.ToString(), "Doc.pdf" });
        }

        internal string DoGlassSubmission(XmlDocument objParamDom)
        {
            string PROC_NAME = string.Empty;

            // Objects that must be destroyed.
            XSLTemplate40 objXSLT = null;
            Guid objGuid;
            IXSLProcessor objXSLProc = null;
            XmlElement objElem = null;
            IXMLDOMDocument2 objXSLdoc = null;
            // Primitives.
            string xslName = string.Empty,
             strDate = string.Empty,
             strTime = string.Empty,
             strAGCXML = string.Empty,
             strAGCEnvironmentCode = string.Empty,
             strRetVal = string.Empty,
             strGuid = string.Empty,
             strInsCoID = string.Empty,
             strAgcID = string.Empty,
             strSourceXML = string.Empty,
             strLossType = string.Empty;
            long lngErrNum = 0;
            string strErrNum = string.Empty,
             strSource = string.Empty,
             strDescription = string.Empty;

            try
            {
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "DoGlassSubmission" });
                objMLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { PROC_NAME, " Started" }));

                strInsCoID = objMLAPDPartnerDataMgr.GetChildNodeText(ref objParamDom, "//InsuranceCompanyID", true);

                // Retreive values that will be plugged into the XSLT as parameters
                strDate = (DateTime.Now.ToString("mm/dd/yyyy"));
                strTime = (DateTime.Now.ToString("hh:mm:ss"));
                strAGCEnvironmentCode = objMLAPDPartnerDataMgr.GetConfig("LynxAppSettings/AGC/@EnvironmentCode");
                strAgcID = objMLAPDPartnerDataMgr.GetConfig("LynxAppSettings/AGC/Insurance/Company[@ID='" + strInsCoID + "']/@AgcID");

                // Resolve the path and name of the XSL used to transform the Web Assignment XML.
                xslName = string.Concat(new string[] { objMLAPDEcadAccessorMgr.g_strSupportDocPath, "\\APDtoAGC", strInsCoID, ".xsl" });

                //Generate guid
                objGuid = new Guid();
                strGuid = Convert.ToString(objGuid).Trim();
                strGuid = strGuid.Substring(2, strGuid.IndexOf("}", 1) - 2);

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] {"XSLT Parameter values - TransactionDate: " , strDate , " TransactionTime: " , strTime , 
                                                         " EnvironmentID: " , strAGCEnvironmentCode , " TransactionID: " , strGuid , Environment.NewLine , "XSL - " , xslName}));


                // Load the XSL into a Free Threaded DOM.
                objXSLdoc.async = false;
                objXSLdoc.load(xslName);

                // Load the XSL document into the XSL Template object.
                objXSLT.stylesheet = objXSLdoc;

                // Create an XSL processor from the XSL Template object.
                objXSLProc = objXSLT.createProcessor();

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("XSLT Processor created.");

                // Load the source XML into the XSL processor.
                objXSLProc.input = objParamDom;

                // Populate the XSL parameters.
                objXSLProc.addParameter("TransactionDate", strDate);
                objXSLProc.addParameter("TransactionTime", strTime);
                objXSLProc.addParameter("EnvironmentID", strAGCEnvironmentCode);
                objXSLProc.addParameter("TransactionID", strGuid);
                objXSLProc.addParameter("AgcID", strAgcID);

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace("XSLT Input and Parameters set.");

                // Perform the XSL transformation.
                objXSLProc.transform();

                // Retrieve the transormed XML from the XSL processor.
                strAGCXML = Convert.ToString(objXSLProc.output);

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "XSLT output formatted for AGC: ", Environment.NewLine, strAGCXML }));

                // Transferring this to GO wil be done through a com+ call rather than the normal post to glaxis.  Chris Billick
                // from Glaxis will provide the compoent/proxy info.

                // Grab data from config and build xml parameter required by Glaxis.
                strLossType = objMLAPDPartnerDataMgr.GetConfig("LynxAppSettings/AGC/Insurance/Company[@ID='" + strInsCoID + "']");
                strSourceXML = string.Concat(new string[] { "<Source><Id>", strAgcID, "</Id><Name>", strLossType, "</Name></Source>" });

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "SourceXML (Glaxis required parameter): ", Environment.NewLine, strSourceXML }));

                // Make Glaxis call.
                objGlaxisProxy = new Loss();
                strRetVal = objGlaxisProxy.SaveLoss(strAGCXML, strSourceXML);

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { "GLAXIS return value: ", Environment.NewLine, strRetVal }));

                if (objMLAPDEcadAccessorMgr.g_blnDebugMode)
                    objMLAPDEcadAccessorMgr.g_objEvents.Trace(string.Concat(new string[] { PROC_NAME, " Complete" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strRetVal;
        }
        #endregion
    }
}
