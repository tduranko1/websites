﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Globalization;
using PartnerDataMgr = Lynx.APD.Component.Library.PartnerDataMgr;

namespace Lynx.APD.Component.Library.EcadAccessorMgr
{
    class MBase64Utils
    {
        private string MODULE_NAME = string.Concat(new string[] { "LAPDEcadAccessorMgr.MBase64Utils." });
        private SiteUtilities.CEvents g_objEvents = new SiteUtilities.CEvents();
        private PartnerDataMgr.MLAPDPartnerDataMgr mlapdPartnerDataMgr = new PartnerDataMgr.MLAPDPartnerDataMgr();
        private enum EventCodes : ulong
        {
            eFileDoesNotExist = 0x80065000 + 0x400,
            eHexDataInvalid
        }

        public object ReadBinaryDataFromFile(string strFileName)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ReadBinaryDataFromFile: " });
            int intFile = 0;
            long lngLen;
            byte[] arrBytes = null;
            try
            {
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    g_objEvents.Trace(string.Concat(new string[] { strFileName, PROC_NAME, " executed." }));

                g_objEvents.Assert(strFileName != "", "File Name param was blank.");


                File.Open(strFileName, FileMode.Open);

                lngLen = strFileName.Length;

                if (lngLen < 1)
                    throw new Exception(string.Concat(new string[] { PROC_NAME, "Open", "File not found" }));

                arrBytes = File.ReadAllBytes(strFileName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return arrBytes;
        }

        public void WriteBinaryDataToFile(string strFileName, ref byte[] arrBuffer)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "WriteBinaryDataToFile: " });

            long lngLen;
            byte[] arrBytes = null;
            try
            {
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    g_objEvents.Trace(string.Concat(new string[] { strFileName, PROC_NAME, " executed." }));

                g_objEvents.Assert(strFileName != "", "File Name param was blank.");
                File.Open(strFileName, FileMode.Open);

                lngLen = strFileName.Length;

                if (lngLen < 1)
                    throw new Exception(string.Concat(new string[] { PROC_NAME, "Open", "File not found" }));

                File.WriteAllBytes(strFileName, arrBytes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object ReadBinaryDataFromDomElement(ref XmlElement objElement)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ReadBinaryDataFromDomElement: " });
            object varType = null;
            try
            {
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    g_objEvents.Trace(string.Concat(new string[] { string.Empty, PROC_NAME, " executed." }));

                varType = objElement.GetType();

                objElement.GetType().Equals("bin.base64");


                if (varType != null)
                    objElement.GetType().Equals(varType);
                else
                    objElement.GetType().Equals(string.Empty);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objElement.NodeType;
        }


        public void WriteBinaryDataToDomElement(ref XmlElement objElement, ref object varBuffer)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "WriteBinaryDataToDomElement: " });
            object varType;
            try
            {
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " executed." }));


                varType = objElement.GetType();


                objElement.GetType().Equals("bin.base64");

                objElement.Value = varBuffer.ToString();


                if (varType != null)
                    objElement.GetType().Equals(varType);
                else
                    objElement.GetType().Equals(string.Empty);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string ReadTextDataFromFile(string strFileName)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ReadTextDataFromFile: " });
            string readedContent = string.Empty;
            try
            {
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    g_objEvents.Trace(strFileName, string.Concat(new string[] { PROC_NAME, " executed." }));

                g_objEvents.Assert(strFileName != "", "File Name param was blank.");

                using (StreamReader srFileReader = new StreamReader(strFileName))
                {
                    readedContent = srFileReader.ReadToEnd();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return readedContent;
        }

        public void WriteTextDataToFile(string strFileName, string strData)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "WriteTextDataToFile: " });
            try
            {
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    g_objEvents.Trace(strFileName, string.Concat(new string[] { PROC_NAME, " executed." }));

                g_objEvents.Assert(strFileName != "", "File Name param was blank.");

                using (StreamWriter swcontentWriter = new StreamWriter(strFileName, true))
                {
                    swcontentWriter.WriteLine(strData);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ReadTextFromCDATA(ref XmlCDataSection objCdata)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ReadTextFromCDATA: " });
            try
            {
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " executed." }));

                g_objEvents.Assert((objCdata != null), "DOM node param was Nothing.");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objCdata.Data;
        }

        public void WriteTextToCDATA(ref XmlCDataSection objCdata, ref string strData)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "WriteTextToCDATA: " });
            try
            {
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " executed." }));


                g_objEvents.Assert((objCdata != null), "DOM node param was Nothing.");
                objCdata.Data = strData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long DecodeHexBin(string HexString, ref byte[] buffer)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "DecodeHexBin: " });
            string hexValue = string.Empty,
                HexStringToDecode = string.Empty;
            int intValue = 0,
                ptr = 0,
                i = 0,
                bufferSize = 0;
            try
            {
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " executed." }));

                HexStringToDecode = (HexString.Trim().ToUpper());

                mlapdPartnerDataMgr.g_objEvents.Assert(ValidateHexString(HexStringToDecode), "Hex encoded data contains invalid character.");


                bufferSize = Convert.ToInt32(HexString.Length / 2);
                buffer = new byte[bufferSize + 1];

                //Now walk through and perform decoding
                for (i = 0; i <= HexString.Length; i += 2)
                {
                    hexValue = string.Concat(new string[] { "&H", HexString.Substring(i, 2), "&" });
                    intValue = Convert.ToInt32((hexValue));
                    buffer[ptr] = Convert.ToByte(intValue);
                    ptr = ptr + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bufferSize;
        }

        public string EncodeHexBin(long bufferSize, ref byte[] buffer)
        {
            string hexValue = string.Empty;
            string HexString = string.Empty;
            string strTempFile = string.Empty;
            Int32 intValue = 0;
            long ptr;
            ADODB.Stream objStream = null;
            try
            {
                objStream.Type = ADODB.StreamTypeEnum.adTypeText;
                objStream.Open();
                for (ptr = 0; ptr <= bufferSize - 1; ptr++)
                {
                    intValue = buffer[ptr];
                    if (intValue <= 15)
                        hexValue = "0" + int.Parse(intValue.ToString(), NumberStyles.HexNumber);
                    else
                        hexValue = int.Parse(intValue.ToString(), NumberStyles.HexNumber).ToString();

                    objStream.WriteText(hexValue, ADODB.StreamWriteEnum.adWriteChar);
                }
                objStream.Position = 0;
                return objStream.ReadText((int)ADODB.StreamReadEnum.adReadAll);


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ValidateHexString(string HexString)
        {
            Int32 ptr = 0,
                intAscVal = 0;
            bool blnOk = true;
            try
            {
                for (ptr = 1; ptr <= HexString.Length; ptr++)
                {
                    intAscVal = Convert.ToInt32(Encoding.ASCII.GetBytes(HexString.Substring(ptr, 1)));
                    if ((intAscVal < 48 || intAscVal > 70 || (intAscVal > 57 && intAscVal < 65)))
                    {
                        // Not a valid Hex Character
                        // Seems its guilty afterall
                        blnOk = false;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnOk;
        }

    }
}
