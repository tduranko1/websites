﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LAPDEcadAccessorMgr;
using ECADAccessorMgr = Lynx.APD.Component.Library.EcadAccessorMgr;

namespace Lynx.APD.Component.Library.EcadAccessor
{
    class CData
    {
        /// <summary>
        /// Component LAPDEcadAccessor: Class CData
        /// 
        /// This component is a simple wrapper for LAPDEcadAccessorMgr so to allow
        /// the two components to run under different user IDs.
        /// </summary>
        #region Declarations
        private const string APP_NAME = "LAPDEcadAccessor.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CData.");
       // CData objMgr = null;
        EcadAccessorMgr.CData objEcadAccessorMgr = null;
        MLAPDEcadAccessor objMLAPDEcadAccessor = null;

        #endregion

        #region Public Functions
             
        /// <summary>
        /// Syntax:      objEcad.GetXML( "<Entity param1='p1' param2='p2'/>" )
        /// 
        /// Parameters:  strParamXML - the only parameter, in XML format.
        /// 
        /// Purpose:     Returns XML data required by ECAD for our Client Access app.
        /// 
        /// Returns:     The XML data, minus any reference and metadata.
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <returns></returns>
        public string GetXML(string strParamXML)
        {

            bool blnDebugMode;
            string PROC_NAME = string.Empty;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "GetXML: ");
                objEcadAccessorMgr = new ECADAccessorMgr.CData();
                objMLAPDEcadAccessor.InitializeGlobals();

                //Create and call LAPDEcadAccessorMgr to do the real work.
                //objMgr = new CData();

                //Get config debug mode status.
                blnDebugMode = objMLAPDEcadAccessor.mobjEvents.IsDebugMode;

                //Check passed parameters.
                objMLAPDEcadAccessor.mobjEvents.Assert((strParamXML.Length) > 0, "Nothing passed for strParamXML.");

                //Add a trace mesage to the debug log.
                if (blnDebugMode)
                    objMLAPDEcadAccessor.mobjEvents.Trace(string.Concat("Param XML = '" + strParamXML + "'", PROC_NAME, "Started"));

                //Add another trace message to note exit?
                if (blnDebugMode)
                    objMLAPDEcadAccessor.mobjEvents.Trace(string.Concat("Return = ", objEcadAccessorMgr.GetXML(strParamXML)), string.Concat(PROC_NAME, "Finished"));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objEcadAccessorMgr.GetXML(strParamXML);
        }

        public long PutXML(string strParamXML)
        {

            bool blnDebugMode = false;
            string PROC_NAME = string.Empty;
            try
            {

                PROC_NAME = string.Concat(MODULE_NAME, "PutXML: ");

                //Create and call LAPDEcadAccessorMgr to do the real work.
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objEcadAccessorMgr = new ECADAccessorMgr.CData();
                objMLAPDEcadAccessor.InitializeGlobals();

                //Get config debug mode status.
                blnDebugMode = objMLAPDEcadAccessor.mobjEvents.IsDebugMode;

                //Check passed parameters.
                objMLAPDEcadAccessor.mobjEvents.Assert((strParamXML.Length) > 0, "Nothing passed for strParamXML.");

                //Add a trace mesage to the debug log.
                if (blnDebugMode)
                    objMLAPDEcadAccessor.mobjEvents.Trace(string.Concat("Param XML = '" + strParamXML + "'", PROC_NAME, "Started"));


                //Add another trace message to note exit?
                if (blnDebugMode)
                    objMLAPDEcadAccessor.mobjEvents.Trace(string.Concat("Return = ", objEcadAccessorMgr.PutXML(strParamXML)), string.Concat(PROC_NAME, "Finished"));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objEcadAccessorMgr.PutXML(strParamXML);
        }

        #endregion

        #region Private Helper Functions
       
        /// <summary>
        /// Backup global termination
        /// </summary>
        private void Class_Terminate()
        {
            try
            {
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMLAPDEcadAccessor.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
