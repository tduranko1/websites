﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Lynx.APD.Component.Library.EcadAccessor
{
    /// <summary>
    /// Component LAPDEcadAccessor: Class CDocument
    /// 
    /// LYNX APD clients (Insurance Companies) require the ability to view
    /// information about their claims. This information will be accessible from
    /// the LYNX Services web site. For the purpose of this document, this web
    /// site will be known as Client Access.
    /// 
    /// LYNX APD Client Access will be hosted and developed by E-Commerce
    /// Application Development (ECAD). Initially the site will be read-only and
    /// simply provide Insurance companies access to claim information.
    /// 
    /// This component is used to return binary documents to ECAD.
    /// </summary>
    class CDocument
    {
        #region Variable Declarations

        private const string APP_NAME = "LAPDEcadAccessor.";
        private string MODULE_NAME = string.Concat(APP_NAME, "CDocument.");
        MLAPDEcadAccessor objMLAPDEcadAccessor = null;
        LAPDEcadAccessorMgr.CDocument objMgr = null;
        #endregion

        #region Private Helper Functions
       
       
        /// <summary>
        /// Backup global termination
        /// </summary>
        private void Class_Terminate()
        {
            try
            {
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMLAPDEcadAccessor.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Public Functions
        public object GetDocument(string strParamXML)
        {
            string PROC_NAME = string.Empty;
            bool blnDebugMode = false;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "GetDocument: ");

                //Create and call LAPDEcadAccessorMgr to do the real work.
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMLAPDEcadAccessor.InitializeGlobals();
                objMgr = new LAPDEcadAccessorMgr.CDocument();

                //Get config debug mode status.
                blnDebugMode = objMLAPDEcadAccessor.mobjEvents.IsDebugMode;

                //Check passed parameters.
                objMLAPDEcadAccessor.mobjEvents.Assert((strParamXML.Length) > 0, "Nothing passed for strParamXML.");

                //Add a trace mesage to the debug log.
                if (blnDebugMode)
                    objMLAPDEcadAccessor.mobjEvents.Trace("Param XML = '" + strParamXML + "'", string.Concat(PROC_NAME, "Started"));


                //Add another trace message to note exit?
                if (blnDebugMode)
                    objMLAPDEcadAccessor.mobjEvents.Trace(string.Empty, string.Concat(PROC_NAME, "Finished"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objMgr.GetDocument(strParamXML);

        }

        public string GetDocumentXML(string strParamXML)
        {

            bool blnDebugMode = false;
            string PROC_NAME = string.Empty;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "GetDocumentXML: ");
                //Create and call LAPDEcadAccessorMgr to do the real work.
                objMLAPDEcadAccessor = new MLAPDEcadAccessor();
                objMLAPDEcadAccessor.InitializeGlobals();
                objMgr = new LAPDEcadAccessorMgr.CDocument();

                //Get config debug mode status.
                blnDebugMode = objMLAPDEcadAccessor.mobjEvents.IsDebugMode;

                //Check passed parameters.
                objMLAPDEcadAccessor.mobjEvents.Assert((strParamXML.Length) > 0, "Nothing passed for strParamXML.");

                //Add a trace mesage to the debug log.
                if (blnDebugMode)
                    objMLAPDEcadAccessor.mobjEvents.Trace("Param XML = '" + strParamXML + "'", string.Concat(PROC_NAME, "Started"));


                //Add another trace message to note exit?
                if (blnDebugMode)
                    objMLAPDEcadAccessor.mobjEvents.Trace(string.Concat("Return = ", objMgr.GetDocumentXML(strParamXML)), string.Concat(PROC_NAME, "Finished"));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objMgr.GetDocumentXML(strParamXML);
        }

        #endregion

    }
}
