﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
namespace Lynx.APD.Component.Library.EcadAccessor
{
    /// <summary>
    ///  Component LAPDEcadAccessor : Module MLAPDEcadAccessor
    /// 
    ///  Global Consts and Utility Methods
    /// </summary>
    class MLAPDEcadAccessor
    {
        #region Declarations


        private const string APP_NAME = "LAPDEcadAccessor.";
        private const long LAPDEcadAccessor_FirstError = 0x80066000;
        #endregion

        #region public Functions
              
        /// <summary>
        /// Error codes specific to LAPDEcadAccessor
        /// </summary>
        public enum EcadAccessorErrors : ulong
        {
            eCreateObjectExError = LAPDEcadAccessor_FirstError + 0x100
        }
        /// <summary>
        /// Global events and data access objects.
        /// </summary>
        public SiteUtilities.CEvents mobjEvents;

        /// <summary>
        /// Initializes global objects and variables
        /// </summary>
        public void InitializeGlobals()
        {
            try
            {
                //Create events object.
                mobjEvents = new SiteUtilities.CEvents();

                //This will give partner data its own log file.
                mobjEvents.ComponentInstance = "Client Access";

                //Initialize our member components first, so we have logging set up.
                mobjEvents.Initialize(string.Concat(Assembly.GetExecutingAssembly().Location, "\\..\\config\\config.xml"));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Terminates global objects and variables
        /// </summary>
        public void TerminateGlobals()
        {
            try
            {
                mobjEvents = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Returns the requested configuration setting.
        /// </summary>
        /// <param name="strSetting"></param>
        /// <returns></returns>
        public object GetConfig(string strSetting)
        {
            try
            {
                mobjEvents = new SiteUtilities.CEvents();
                return mobjEvents.mSettings.GetParsedSetting(strSetting);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
