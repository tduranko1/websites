﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lynx.APD.Component.Library.PartnerData
{
    class MLAPDPartnerData
    {
        public const string APP_NAME = "LAPDPartnerData";
        public const long LAPDPartnerData_FirstError = 0x80064800;
        public SiteUtilities.CEvents g_objEvents;

        /// <summary>
        ///  Wraps CreateObject() with better debug information.
        /// </summary>
        /// <param name="strObjectName"></param>
        /// <returns></returns>
        public object CreateObjectEx(string strObjectName)
        {
            object createdObject = null;
            Type CreateObj;
            try
            {                
                CreateObj = Type.GetTypeFromProgID(strObjectName);
                createdObject = Activator.CreateInstance(CreateObj);
            }
            catch (Exception ex)
            {
            }
            return createdObject;
        }
    }
}
