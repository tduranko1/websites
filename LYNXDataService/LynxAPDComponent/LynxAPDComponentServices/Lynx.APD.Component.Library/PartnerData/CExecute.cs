﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Lynx.APD.Component.Library.PartnerData;
using Lynx.APD.Component.Library.PartnerDataMgr;
using System.Configuration;

namespace Lynx.APD.Component.Library.PartnerData
{
    public class CExecute
    {
        //PartnerDataClasses.MLAPDPartnerData mobjMLAPDPartnerData = new MLAPDPartnerData();

      // MLAPDPartnerData objMLAPDPartnerData = new MLAPDPartnerData();
        PartnerDataMgr.CExecute mobjExecute = null;
        public string APP_NAME = "LAPDPartnerData",
        MODULE_NAME = "CExecute";
        private Boolean mblnDebugMode;
        private Boolean mblnDisableXmlReturn;
        public SiteUtilities.CEvents mobjEvents;
       // public clsHelper mobjclsHelper = new clsHelper();
        AppSettingsReader appSettingsReader;

        private enum Error_Codes
        {
            //eXmlParseError = MLAPDPartnerData.LAPDPartnerData_FirstError + Convert.ToInt64(100) //MLAPDPartnerData.LAPDPartnerData_FirstError + 0x100  //100 added from LAPDPartnerData_FirstError

        }


        /// <summary>
        /// Initialize and Terminate
        /// </summary>
        private void InitializeGlobals()
        {
            string configPath = string.Empty;
            try
            {
                mobjEvents.ComponentInstance = "Partner Data";
                appSettingsReader = new AppSettingsReader();
                configPath = appSettingsReader.GetValue("ConfigPath", typeof(string)).ToString();
                //mobjEvents.Initialize(AppDomain.CurrentDomain.BaseDirectory + "\\..\\config\\config.xml");
                mobjEvents.Initialize(configPath);
                mblnDebugMode = mobjEvents.IsDebugMode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TerminateGlobals()
        {
            mobjEvents = null;
        }

        /// <summary>
        /// Receive an Xml from an external application and then act on it
        /// based on the content itself.  Typical usage would be to send the
        /// Xml document to a stored proc for storage.
        /// </summary>
        /// <param name="strXmlDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <returns></returns>
        public long SendXmlDoc(string strXmlDoc, string strTradingPartner)
        {
            string strResult = string.Empty;
            long sendXmlDoc = 1;
            try
            {
                mblnDisableXmlReturn = true;
                strResult = SendXmlRequest(strXmlDoc, strTradingPartner);

                if (!string.IsNullOrEmpty(strResult))
                {
                    if (strResult.Substring(0, 6) == "<Error")
                        sendXmlDoc = 0;
                    else
                        sendXmlDoc = 1;
                }

                mblnDisableXmlReturn = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sendXmlDoc;
        }

        /// <summary>
        /// Receive an Xml from an external application and then act on it
        /// based on the content itself.  Typical usage would be to send the
        /// Xml document to a stored proc for storage.
        /// </summary>
        /// <param name="strXmlDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <returns></returns>
        public string SendXmlRequest(string strXmlDoc, string strTradingPartner)
        {
            // object objLDM = null;
            XmlDocument objXmldoc = null;
            bool blnRaiseToCaller = false;
            string PROC_NAME = string.Empty,
                strSendXmlRequest = string.Empty;
            new PartnerDataMgr.CExecute();
            try
            {
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, APP_NAME, "SendXmlRequest" });
                blnRaiseToCaller = false;

                //'Used by our error handler in LYNX to create a log of the components work
                InitializeGlobals();

                // 'Add parameters to trace data.
                // 'I removed the XML output from this trace because it will get traced again in DataAccessor.
                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "XML from ", strTradingPartner, " at ", Convert.ToString(DateTime.Now), PROC_NAME, "Started" }));

                blnRaiseToCaller = true;    //'The following does relate to caller.

                //'Validate our parameters and trace them IF they are invalid.
                if (!string.IsNullOrEmpty(strXmlDoc))
                    mobjEvents.Assert(Convert.ToBoolean(strXmlDoc.Length > 0), "No Xml Document received");

                if (!string.IsNullOrEmpty(strTradingPartner))
                    mobjEvents.Assert(Convert.ToBoolean(strTradingPartner.Length > 0), "No Trading Partner value received");

                objXmldoc = new XmlDocument();

                if (!string.IsNullOrEmpty(strXmlDoc))
                    objXmldoc.LoadXml(strXmlDoc);

                blnRaiseToCaller = false;

                //objLDM = objMLAPDPartnerData.CreateObjectEx("LAPDPartnerDataMgr.CExecute");
                strSendXmlRequest = mobjExecute.ConsumePartnerXmlTransaction(strXmlDoc, strTradingPartner, !mblnDisableXmlReturn);

                if (mblnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "Returned XML=", strSendXmlRequest, PROC_NAME, "Finished" }));
            }
            catch (Exception ex)
            {
                //'Log XML.
                mobjEvents.Env(string.Concat(new string[] { "Param XML = ", strXmlDoc }));
                mobjEvents.Env(string.Concat(new string[] { "Result XML (if any) = ", strSendXmlRequest }));

                //Send an Error XML element back to caller.
                strSendXmlRequest = string.Concat(new string[] { "<Error Number='", ex.Message, "Source='SendXmlRequest' Description='None'", " Resend='", Convert.ToString(!blnRaiseToCaller), "' />" });

                mobjEvents.Env(string.Concat(new string[] { "Error XML = ", strSendXmlRequest }));

                // 'Log and raise (perhaps).
                //mobjEvents.HandleEvent(System.Runtime.InteropServices.Marshal.GetExceptionCode(),

            }
            finally
            {
                mobjExecute = null;
                objXmldoc = null;
                TerminateGlobals();
            }
            return strSendXmlRequest;
        }
    }
}
