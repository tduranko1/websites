﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SiteUtilities;
using System.Runtime.InteropServices;
using System.Collections;
using ADODB;

namespace Lynx.APD.Component.Library.DataAccessor
{
    //********************************************************************************
    //* Component DataAccessor : Class CDataAccessor
    //*
    //* DataAccessor is intended to be our sole database access layer.
    //* Any and every call to the database MUST go through DataAccessor.
    //*
    //* As such, Class CDataAccessor has bloomed into a massive collection of methods
    //* with some confusion and overlap of functionality.  This was done intentionally
    //* for backwards compatability purposes - breaking DataAccessor would break every
    //* single component in our apps.  So deal.
    //*
    //********************************************************************************
    class CDataAccessor
    {
        #region Variables
        string MODULE_NAME = string.Concat(new string[] { "DataAccess.", "CDataAccessor." });
        ADODB.Connection madoConn = null, madoShapeConn = null;

        ADODB.Command madoFpCmd = null;

        MDataAccessor mDataAccessor = null;

        string mstrFpCmdText = string.Empty,
            msConnectString = string.Empty;

        bool mblnInTrans = false, mblnInShapeTrans = false;

        int mintFailoverCount;

        //Retry parameters when database connections fail.
        private const long MaxFailoverAttempts = 20;
        private const long FailoverSleepPeriod = 500;
        private int ConnectionTimeout;

        //CSiteUtilities Declaration and initialization
        public CEvents mobjEvents = null;

        //Are we in debug mode?  When true then we do lots of tracing through mEvents.
        private bool m_blnDebugMode = false;

        //Do we want to send errors to mEvents for logging and notification?
        //This defaults to True, but in special situations can be disabled
        //through the NotifyEvents property.
        private bool m_blnNotifyEvents = true;

        //The config file path passed to InitEvents
        private string m_strConfigFilePath = string.Empty;
        private ParameterDirectionEnum adParamInputOutput;
        #endregion

        #region ENUM
        private enum ErrorEnum : uint
        {
            //These event codes map to specific events within error.xml.
            eInvalidParameter = 0x80061000 + 50,
            eInvalidParameter_StringLength,
            eInvalidParameter_NonNumeric,
            eInvalidParameter_OutOfRange,
            eInvalidParameterType,
            eMaintenanceMode,
            //These event codes will map to the default.
            eConnectionStringNotSet = 0x80061000 + 0x100,
            eBlankUpdateGram,
            eWrongDataProvider,
            eParameterMissingFromCollection,
            eConnectionOpenError,
            eConnectionInTransaction,

            eCannotNestTrans,
            eNoTransExists,
            eTransactionLeftOpen
        }
        #endregion

        #region Properties
        public string DebugLog
        {
            set
            {
                mobjEvents = new CEvents();
                mobjEvents.DebugLog = value;
                m_blnDebugMode = true;
            }

        }

        /// <summary>
        /// Do we want to send errors to mEvents for logging and notification?
        /// This defaults to True, but in special situations can be disabled.
        /// </summary>
        public bool NotifyEvents
        {
            get
            {
                return m_blnNotifyEvents;
            }
            set
            {
                m_blnNotifyEvents = value;
            }
        }
        #endregion

        #region Common Methods

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }
        #endregion

        #region Methods

        private void Class_Initialize()
        {
            m_blnDebugMode = false;
            m_blnNotifyEvents = true;
        }

        private void Class_Terminate()
        {
            madoConn = null;
            madoShapeConn = null;
            mobjEvents = null;

            if (mblnInTrans)
                throw new Exception(string.Concat(new string[] { Convert.ToString(ErrorEnum.eTransactionLeftOpen), "Class_Terminate()", "Transaction left open!  Must Commit or Rollback!" }));
            else if (mblnInShapeTrans)
                throw new Exception(string.Concat(new string[] { Convert.ToString(ErrorEnum.eTransactionLeftOpen), "Class_Terminate()", "Shaped Transaction left open!  Must Commit or Rollback!" }));
        }

        /// <summary>
        /// Initialization of events object settings.
        /// Call InitEvents if you have a config file path.
        /// </summary>
        /// <param name="strConfigPath"></param>
        /// <param name="strDebugId"></param>
        /// <param name="blnInitLog"></param>
        public void InitEvents(string strConfigPath, [Optional] string strDebugId, [Optional] bool blnInitLog)
        {
            int iCommandTimeoutSetting;
            //Clear before we call Inititialize.
            m_strConfigFilePath = string.Empty;
            m_blnDebugMode = false;
            try
            {
                strDebugId = "Debug";
                blnInitLog = false;
                mobjEvents = new CEvents();
                mobjEvents.Initialize(strConfigPath, strDebugId, blnInitLog);
                m_blnDebugMode = mobjEvents.IsDebugMode;
                m_strConfigFilePath = strConfigPath;
                iCommandTimeoutSetting = Convert.ToInt32(GetConfig("DataAccessor/@timeoutSeconds"));

                if (iCommandTimeoutSetting > 0)
                    ConnectionTimeout = iCommandTimeoutSetting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns true if InitEvents was called with a valid config.xml path.
        /// </summary>
        /// <returns></returns>
        private bool HasConfigurationInfo()
        {
            return Convert.ToBoolean(m_strConfigFilePath != string.Empty);
        }

        /// <summary>
        /// Returns the requested configuration setting.
        /// </summary>
        /// <param name="strSetting"></param>
        /// <returns></returns>
        private string GetConfig(string strSetting)
        {
            mobjEvents = new CEvents();
            return mobjEvents.mSettings.GetParsedSetting(strSetting);
        }

        /// <summary>
        /// Executes an action-only stored procedure
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="Params"></param>
        /// <returns></returns>
        public long ExecuteSp(string strProcName, params object[] Params)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExecuteSp:" });
            object lngRecordsAffected;
            string strSQL = string.Empty,
                strValue = string.Empty;
            object varParams;
            try
            {
                mobjEvents = new CEvents();
                //Fix any ParamArray pass-through problems.
                if (Params.GetUpperBound(0) == -1)
                    varParams = null;
                else if (Params.GetValue(0) != null)
                    varParams = Params[0];
                else
                    varParams = Params;

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strProcName }), string.Concat(new string[] { PROC_NAME, "Build SQL Query" }));

                strSQL = ConvertParamArrayToQueryString(strProcName, varParams);

                InitFailoverLoop();
                CheckConnection();

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SQL = ", strSQL }), string.Concat(new string[] { PROC_NAME, "madoConn.Execute" }));

                madoConn.Execute(strSQL, out  lngRecordsAffected, Convert.ToInt32(ADODB.ExecuteOptionEnum.adExecuteNoRecords));

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Convert.ToInt64(lngRecordsAffected);
        }

        /// <summary>
        /// Executes an action only stored procedure using named parameters.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="Params"></param>
        /// <returns></returns>
        public long ExecuteSpNamedParams(string strProcName, params object[] Params)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExecuteSpNamedParams:" });
            long lngExecuteSpNamedParams;
            ADODB.Command adoCmd = null;
            object lngRecordsAffected;
            string strSQL = string.Empty,
                strValue = string.Empty,
                strItem = string.Empty;
            object[] varParams = null;
            try
            {
                mobjEvents = new CEvents();

                //Fix any ParamArray pass-through problems.
                if (Params.GetUpperBound(0) == -1)
                    varParams = null;
                else if (Params.GetValue(0) != null)
                    varParams[0] = Params[0];
                else
                    varParams = Params;

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strProcName }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                adoCmd = new ADODB.Command();

                InitFailoverLoop();

                //Initialize the ADO Command object
                adoCmd.ActiveConnection = madoConn;
                adoCmd.CommandText = strProcName;
                adoCmd.CommandType = ADODB.CommandTypeEnum.adCmdStoredProc;
                adoCmd.CommandTimeout = ConnectionTimeout;
                adoCmd.NamedParameters = true;

                //Automatically fill in parameter info from stored procedure.
                adoCmd.Parameters.Refresh();

                ConvertParamArrayToAdoParams(adoCmd, varParams, strProcName);

                if (m_blnDebugMode)
                {
                    mobjEvents.Trace(string.Concat(new string[] { "CmdTxt = ", adoCmd.CommandText }), string.Concat(new string[] { PROC_NAME, "adoCmd.Execute" }));
                    TraceAdoParameters(adoCmd.Parameters);
                }

                adoCmd.Execute(out lngRecordsAffected, null, Convert.ToInt32(ADODB.ExecuteOptionEnum.adExecuteNoRecords));
                lngExecuteSpNamedParams = (long)BestReturnValue(ref adoCmd, Convert.ToInt64(lngRecordsAffected));

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat("Result = ", Convert.ToString(lngExecuteSpNamedParams)), string.Concat(PROC_NAME, "Finished"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lngExecuteSpNamedParams;

        }

        /// <summary>
        /// Executes an action only stored procedure.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="colForm"></param>
        /// <returns></returns>
        public long ExecuteSpNpCol(string strProcName, ICollection colForm)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExecuteSpNpCol:" });
            long lngExecuteSpNpCol;
            ADODB.Command adoCmd = null;
            object[] tmpObj = null;
            object lngRecordsAffected;
            string strSQL = string.Empty,
                strValue = string.Empty,
                strItem = string.Empty;
            try
            {
                mobjEvents = new CEvents();

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strProcName }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                adoCmd = new ADODB.Command();

                InitFailoverLoop();

                CheckConnection();

                //Initialize the ADO Command object
                adoCmd.ActiveConnection = madoConn;
                adoCmd.CommandText = strProcName;
                adoCmd.CommandType = ADODB.CommandTypeEnum.adCmdStoredProc;
                adoCmd.CommandTimeout = ConnectionTimeout;
                adoCmd.NamedParameters = true;

                //Automatically fill in parameter info from stored procedure.
                adoCmd.Parameters.Refresh();
                tmpObj = (object[])colForm;
                ConvertParamArrayToAdoParams(adoCmd, tmpObj, strProcName);


                if (m_blnDebugMode)
                {
                    mobjEvents.Trace(string.Concat(new string[] { "CmdTxt = ", adoCmd.CommandText }), string.Concat(new string[] { PROC_NAME, "adoCmd.Execute" }));
                    TraceAdoParameters(adoCmd.Parameters);
                }

                adoCmd.Execute(out lngRecordsAffected, null, Convert.ToInt32(ADODB.ExecuteOptionEnum.adExecuteNoRecords));
                lngExecuteSpNpCol = (long)BestReturnValue(ref adoCmd, Convert.ToInt64(lngRecordsAffected));

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat("Result = ", Convert.ToString(lngExecuteSpNpCol)), string.Concat(PROC_NAME, "Finished"));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Get the smartest return value.
            return lngExecuteSpNpCol;
        }

        /// <summary>
        /// Executes a stored procedure that returns client side XML.
        /// Uses named parameters from the database to extract the
        /// parameters from the passed collection.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="Params"></param>
        /// <returns></returns>
        public string ExecuteSpNamedParamsXML(string strProcName, params object[] Params)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExecuteSpNamedParamsXML:" });
            ADODB.Command adoCmd = null;
            ADODB.Stream adoOutStr = null;

            object lngRecordsAffected;
            string strSQL = string.Empty,
                strValue = string.Empty,
                strItem = string.Empty,
                strProc = string.Empty,
                strReturnValue = string.Empty;

            object[] varParams = null;
            try
            {
                mobjEvents = new CEvents();

                //Fix any ParamArray pass-through problems.
                if (Params.GetUpperBound(0) == -1)
                    varParams = null;
                else if (Params.GetValue(0) != null)
                    varParams[0] = Params[0];
                else
                    varParams = Params;

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strProcName }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                adoCmd = new ADODB.Command();
                adoOutStr = new ADODB.Stream();

                adoOutStr.Open();

                InitFailoverLoop();

                if (DoFailoverRetry(ref madoConn, PROC_NAME))
                {
                    adoOutStr.Close();
                    adoOutStr.Open();
                }

                CheckConnection();

                //Initialize the ADO objects
                adoCmd.ActiveConnection = madoConn;
                adoCmd.CommandText = strProcName;
                adoCmd.CommandType = ADODB.CommandTypeEnum.adCmdStoredProc;
                adoCmd.CommandTimeout = ConnectionTimeout;
                adoCmd.Parameters.Refresh();

                strProc = strProcName;

                ConvertParamArrayToAdoParams(adoCmd, varParams, strProcName);

                strProc = ConvertAdoCmdToClientSideXmlQuery(adoCmd, strProc);

                if (m_blnDebugMode)
                    mobjEvents.Trace(strProc, string.Concat(new string[] { PROC_NAME, "adoCmd.Execute" }));

                //Now set up the command again and execute.
                adoCmd.CommandType = ADODB.CommandTypeEnum.adCmdText;
                adoCmd.CommandText = strProc;
                adoCmd.Properties["Output Stream"].Value = adoOutStr;
                adoCmd.Properties["ClientSideXML"].Value = "true";
                adoCmd.Execute(out lngRecordsAffected, null, Convert.ToInt32(ADODB.ExecuteOptionEnum.adExecuteStream));

                if (!adoOutStr.EOS)
                    strReturnValue = adoOutStr.ReadText();
                else
                    strReturnValue = Convert.ToString(BestReturnValue(ref adoCmd, 0));

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "Result = ", strReturnValue }), string.Concat(new string[] { PROC_NAME, "Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                adoOutStr = null;
                adoCmd = null;
            }

            return strReturnValue;
        }

        /// <summary>
        /// Executes a stored procedure that returns client side XML.
        /// Uses named parameters from the database to extract the
        /// parameters from the passed collection.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="colForm"></param>
        /// <returns></returns>
        public string ExecuteSpNamedParamsXML(string strProcName, ICollection colForm)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExecuteSpNpColXML:" });
            ADODB.Command adoCmd = null;
            ADODB.Stream adoOutStr = null;

            object[] tmpObj = null;
            object lngRecordsAffected;
            string strSQL = string.Empty,
                strValue = string.Empty,
                strItem = string.Empty,
                strProc = string.Empty,
                strReturnValue = string.Empty;
            try
            {
                mobjEvents = new CEvents();

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strProcName }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                adoCmd = new ADODB.Command();
                adoOutStr = new ADODB.Stream();

                adoOutStr.Open();

                InitFailoverLoop();

                if (DoFailoverRetry(ref madoConn, PROC_NAME))
                {
                    adoOutStr.Close();
                    adoOutStr.Open();
                }

                CheckConnection();

                //Initialize the ADO objects
                adoCmd.ActiveConnection = madoConn;
                adoCmd.CommandText = strProcName;
                adoCmd.CommandType = ADODB.CommandTypeEnum.adCmdStoredProc;
                adoCmd.CommandTimeout = ConnectionTimeout;
                adoCmd.Parameters.Refresh();

                strProc = strProcName;
                tmpObj = (object[])colForm;
                ConvertParamArrayToAdoParams(adoCmd, tmpObj, strProcName);

                strProc = ConvertAdoCmdToClientSideXmlQuery(adoCmd, strProc);

                if (m_blnDebugMode)
                    mobjEvents.Trace(strProc, string.Concat(new string[] { PROC_NAME, "adoCmd.Execute" }));

                //Now set up the command again and execute.
                adoCmd.CommandType = ADODB.CommandTypeEnum.adCmdText;
                adoCmd.CommandText = strProc;
                adoCmd.Properties["Output Stream"].Equals(adoOutStr);
                adoCmd.Properties["ClientSideXML"].Equals("true");
                adoCmd.Execute(out lngRecordsAffected, null, Convert.ToInt32(ADODB.ExecuteOptionEnum.adExecuteStream));

                if (!adoOutStr.EOS)
                    strReturnValue = adoOutStr.ReadText();
                else
                    strReturnValue = Convert.ToString(BestReturnValue(ref adoCmd, 0));

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "Result = ", strReturnValue }), string.Concat(new string[] { PROC_NAME, "Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                adoOutStr = null;
                adoCmd = null;
            }

            return strReturnValue;
        }

        /// <summary>
        /// Executes a stored procedure in the database that returns a recordset.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="Params"></param>
        /// <returns></returns>
        public Recordset OpenRecordsetSp(string strProcName, params object[] Params)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "OpenRecordsetSp:" });
            Recordset adoRS = null;
            string strSQL = string.Empty;
            object varParams;
            try
            {
                mobjEvents = new CEvents();

                adoRS = new Recordset();

                //Fix any ParamArray pass-through problems.
                if (Params.GetUpperBound(0) == -1)
                    varParams = null;
                else if (Params.GetValue(0) != null)
                    varParams = Params[0];
                else
                    varParams = Params;

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strProcName }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                InitFailoverLoop();

                CheckConnection();

                strSQL = ConvertParamArrayToQueryString(strProcName, varParams);

                mobjEvents.Trace(string.Concat(new string[] { "SQL = ", strSQL }), string.Concat(new string[] { PROC_NAME, "adoRS.Open" }));

                //Fix MS issue with returning "select all" from temp recorsets
                adoRS.ActiveConnection = madoConn;
                adoRS.LockType = LockTypeEnum.adLockOptimistic;
                adoRS.Open("SET NOCOUNT ON");

                adoRS.Open(strSQL, madoConn, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockOptimistic);

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

            return adoRS;
        }

        /// <summary>
        /// Executes a stored procedure in the database that returns a recordset.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="Params"></param>
        /// <returns></returns>
        public Recordset OpenRecordsetSpNpCol(string strProcName, ICollection colForm)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "OpenRecordsetSpNpCol:" });
            Recordset adoRS = null;
            Command adoCmd = null;
            object lngRecordsAffected;
            string strSQL = string.Empty,
                strValue = string.Empty,
               strItem = string.Empty;
            try
            {
                mobjEvents = new CEvents();

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strProcName }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                adoRS = new Recordset();
                adoCmd = new Command();

                InitFailoverLoop();

                CheckConnection();

                adoCmd.ActiveConnection = madoConn;
                adoCmd.CommandText = strProcName;
                adoCmd.CommandType = CommandTypeEnum.adCmdStoredProc;
                adoCmd.CommandTimeout = ConnectionTimeout;
                adoCmd.NamedParameters = true;

                //Automatically fill in pa
                adoCmd.Parameters.Refresh();

                ConvertCollectionToAdoParams(adoCmd, colForm, strProcName);

                if (m_blnDebugMode)
                {
                    mobjEvents.Trace(string.Concat(new string[] { "CmdTxt = ", adoCmd.CommandText }), string.Concat(new string[] { PROC_NAME, "adoCmd.Execute" }));
                    TraceAdoParameters(adoCmd.Parameters);
                }

                adoRS = adoCmd.Execute(out lngRecordsAffected, null, -1);

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                adoCmd = null;
                adoRS = null;
            }

            return adoRS;
        }

        /// <summary>
        /// Executes a stored procedure in the database that returns a recordset.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <returns></returns>
        public Recordset OpenShapedRecordsetSql(string strSQL)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "OpenShapedRecordsetSql:" });
            Recordset adoRS = null;
            try
            {
                mobjEvents = new CEvents();
                adoRS = new Recordset();

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SQL = ", strSQL }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                InitFailoverLoop();

                CheckShapeConnection();

                mobjEvents.Trace(string.Concat(new string[] { "SQL = ", strSQL }), string.Concat(new string[] { PROC_NAME, "adoRS.Open" }));

                //Fix MS issue with returning "select all" from temp recorsets
                adoRS.ActiveConnection = madoConn;
                adoRS.LockType = LockTypeEnum.adLockOptimistic;
                adoRS.Open("SET NOCOUNT ON");

                adoRS.Open(strSQL, madoConn, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockOptimistic);

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                adoRS = null;
            }

            return adoRS;
        }

        /// <summary>
        /// This procedure will receive a string that holds a connection
        ///string used by ADO to connect to a database.
        /// </summary>
        /// <param name="sConnectString"></param>
        public void SetConnectString(string sConnectString)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SetConnectString:" });
            try
            {
                mobjEvents = new CEvents();

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "Connect= ", sConnectString }), string.Concat(new string[] { PROC_NAME, "SetConnectString" }));

                if (msConnectString != string.Empty && msConnectString != sConnectString)
                {
                    if (mblnInTrans || mblnInShapeTrans)
                        throw new Exception(string.Concat(new string[] { Convert.ToString(ErrorEnum.eConnectionInTransaction), PROC_NAME, "Cannot change connect string while in a transaction." }));

                    //TODO
                    if (madoConn != null)
                    {
                        madoConn.Close();
                        madoConn = null;
                    }

                    if (madoShapeConn != null)
                    {
                        madoShapeConn.Close();
                        madoShapeConn = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            msConnectString = sConnectString;
        }

        /// <summary>
        /// This procedure will verify and/or make a connection to the database.
        /// </summary>
        private void CheckConnection()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CheckConnection:" });
            try
            {
                mobjEvents = new CEvents();

                //Check to make sure we have a connection string
                //before attempting to open the connection.
                if (msConnectString == string.Empty)
                    return;

                //Check to see if we are in maintenance mode.
                //We can only do this if we have a config file to peek into.
                if (HasConfigurationInfo())
                {
                    if (GetConfig("Maintenance/@ModeActive") == "True")
                        throw new Exception(string.Concat(new string[] { Convert.ToString(ErrorEnum.eMaintenanceMode), PROC_NAME, GetConfig("Maintenance/Message") }));
                }

                //Create the connection object if nothing.
                if (madoConn == null)
                    madoConn = new Connection();

                //If the connection does not exist yet then open it.
                if (madoConn == null)
                {
                    madoConn.Errors.Clear();
                    madoConn.CommandTimeout = ConnectionTimeout;
                    madoConn.CursorLocation = CursorLocationEnum.adUseClient;
                    madoConn.Open(msConnectString);
                }

                //Check for any errors that didn't raise.
                if (madoConn.Errors.Count > 0)
                {
                    //Throw exception
                }

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "ConnectString= ", Convert.ToString(madoConn) }), string.Concat(new string[] { PROC_NAME, "- Checked Ok" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This procedure will verify and/or make a connection to the database.
        /// </summary>
        private void CheckShapeConnection()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CheckShapeConnection:" });
            try
            {
                mobjEvents = new CEvents();

                //Check to make sure we have a connection string
                //before attempting to open the connection.
                if (msConnectString == string.Empty)
                    return;

                //Check to see if we are in maintenance mode.
                //We can only do this if we have a config file to peek into.
                if (HasConfigurationInfo())
                {
                    if (GetConfig("Maintenance/@ModeActive") == "True")
                        throw new Exception(string.Concat(new string[] { Convert.ToString(ErrorEnum.eMaintenanceMode), PROC_NAME, GetConfig("Maintenance/Message") }));
                }

                //Create the connection object if nothing.
                if (madoShapeConn == null)
                    madoShapeConn = new Connection();

                //If the connection does not exist yet then open it.
                if (madoConn == null)
                {
                    madoShapeConn.Errors.Clear();
                    madoShapeConn.Provider = "MSDataShape";
                    madoShapeConn.CommandTimeout = ConnectionTimeout;
                    madoShapeConn.CursorLocation = CursorLocationEnum.adUseClient;
                    madoShapeConn.Open(msConnectString);
                }

                //Check for any errors that didn't raise.
                if (madoShapeConn.Errors.Count > 0)
                {
                    //Throw exception
                }

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "ConnectString= ", Convert.ToString(madoShapeConn) }), string.Concat(new string[] { PROC_NAME, "- Checked Ok" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Executes a stored procedure in the database.  If records are
        ///returned it will return the records to the caller in an xml string.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="Params"></param>
        /// <returns></returns>
        public string OpenRecordsetAsXML(string strProcName, params object[] Params)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "OpenRecordsetAsXML:" });
            string strSQL = string.Empty;
            Stream adoOutStr = null;
            Command adoCmd = null;
            object varParams, lngRecordsAffected;
            try
            {
                mobjEvents = new CEvents();

                //Fix any ParamArray pass-through problems.
                if (Params.GetUpperBound(0) == -1)
                    varParams = null;
                else if (Params.GetValue(0) != null)
                    varParams = Params[0];
                else
                    varParams = Params;

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strProcName }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                adoCmd = new Command();
                adoOutStr = new Stream();
                adoOutStr.Open();

                InitFailoverLoop();

                if (DoFailoverRetry(ref madoConn, PROC_NAME))
                {
                    //Failovers might have put garbage into the stream - try to clear it.
                    adoOutStr.Close();
                    adoOutStr.Open();
                }

                CheckConnection();

                strSQL = ConvertParamArrayToQueryString(strProcName, varParams);

                mobjEvents.Trace(string.Concat(new string[] { "SQL = ", strSQL }), string.Concat(new string[] { PROC_NAME, "adoCmd.Execute" }));

                adoCmd.ActiveConnection = madoConn;
                adoCmd.CommandType = CommandTypeEnum.adCmdText;
                adoCmd.CommandText = strSQL;
                adoCmd.CommandTimeout = ConnectionTimeout;
                adoCmd.Properties["Output Stream"].Value = adoOutStr;
                adoCmd.Execute(out lngRecordsAffected, null, Convert.ToInt32(ExecuteOptionEnum.adExecuteStream));
                adoCmd.ActiveConnection = null;

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "Result = ", adoOutStr.ReadText() }), string.Concat(new string[] { PROC_NAME, "Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                adoOutStr = null;
                adoCmd = null;
                adoOutStr.Close();
            }
            return adoOutStr.ReadText();
        }

        /// <summary>
        /// Executes a stored procedure in the database.  If records are
        ///returned it will return the records to the caller in an xml string.
        /// </summary>
        /// <param name="strProcName"></param>
        /// <param name="Params"></param>
        /// <returns></returns>
        public string OpenRecordsetAsClientSideXML(string strProcName, params object[] Params)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "OpenRecordsetAsClientSideXML:" });
            string strSQL = string.Empty;
            Stream adoOutStr = null;
            Command adoCmd = null;
            object varParams, lngRecordsAffected;
            try
            {
                mobjEvents = new CEvents();

                //Fix any ParamArray pass-through problems.
                if (Params.GetUpperBound(0) == -1)
                    varParams = null;
                else if (Params.GetValue(0) != null)
                    varParams = Params[0];
                else
                    varParams = Params;

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strProcName }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                adoCmd = new Command();
                adoOutStr = new Stream();
                adoOutStr.Open();

                InitFailoverLoop();

                if (DoFailoverRetry(ref madoConn, PROC_NAME))
                {
                    //Failovers might have put garbage into the stream - try to clear it.
                    adoOutStr.Close();
                    adoOutStr.Open();
                }

                CheckConnection();

                strSQL = ConvertParamArrayToQueryString(strProcName, varParams);

                strSQL = string.Concat(new string[] { strSQL, " FOR XML EXPLICIT" });

                mobjEvents.Trace(string.Concat(new string[] { "SQL = ", strSQL }), string.Concat(new string[] { PROC_NAME, "adoCmd.Execute" }));

                adoCmd.ActiveConnection = madoConn;
                adoCmd.CommandType = CommandTypeEnum.adCmdText;
                adoCmd.CommandText = strSQL;
                adoCmd.CommandTimeout = ConnectionTimeout;
                adoCmd.Properties["ClientSideXML"].Value = "True";
                adoCmd.Properties["Output Stream"].Value = adoOutStr;
                adoCmd.Execute(out lngRecordsAffected, null, Convert.ToInt32(ExecuteOptionEnum.adExecuteStream));
                adoCmd.ActiveConnection = null;

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "Result = ", adoOutStr.ReadText() }), string.Concat(new string[] { PROC_NAME, "Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                adoOutStr = null;
                adoCmd = null;
                adoOutStr.Close();
            }
            return adoOutStr.ReadText();
        }

        /// <summary>
        /// This procedure will recieve a SQL statement and will send it to the
        ///database for execution.  If records are returned it will return the records
        ///to the caller in the form of a disconnected recordset
        /// </summary>
        /// <param name="strSQL"></param>
        /// <returns></returns>
        public Recordset OpenRecordset(string strSQL)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "OpenRecordset:" });
            Recordset adoRS = null;
            try
            {
                mobjEvents = new CEvents();
                adoRS = new Recordset();

                InitFailoverLoop();

                CheckConnection();

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SQL = ", strSQL }), string.Concat(new string[] { PROC_NAME, "adoRS.Open" }));

                //Fix MS issue with returning "select all" from temp recorsets
                adoRS.ActiveConnection = madoConn;
                adoRS.LockType = LockTypeEnum.adLockOptimistic;
                adoRS.Open("SET NOCOUNT ON");

                adoRS.Open(strSQL, madoConn, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockOptimistic);

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }

            return adoRS;
        }

        /// <summary>
        ///  This procedure executes the passed XML Update Gram.
        /// </summary>
        /// <param name="strXML"></param>
        /// <returns></returns>
        public string ExecuteUpdateGram(string strXML)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExecuteUpdateGram:" });
            Command cmdUpdategram = null;
            Stream strmUpdategram, strmResults;
            object lngRecordsAffected;
            try
            {
                mobjEvents = new CEvents();

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "SP = ", strXML }), string.Concat(new string[] { PROC_NAME, "Initialize ADO" }));

                if (strXML != string.Empty)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(ErrorEnum.eBlankUpdateGram), "Update Gram passed in was blank." }));

                cmdUpdategram = new Command();
                strmUpdategram = new Stream();
                strmResults = new Stream();
                strmResults.Open();

                InitFailoverLoop();

                if (DoFailoverRetry(ref madoConn, PROC_NAME))
                {
                    //Failovers might have put garbage into the stream - try to clear it.
                    strmResults.Close();
                    strmResults.Open();
                }

                CheckConnection();

                madoConn.Properties["SQLXML Version"].Value = "SQLXML.3.0";

                cmdUpdategram.ActiveConnection = madoConn;

                //Use the SQL Server 2000 XML Dialect
                cmdUpdategram.Dialect = "{5d531cb2-e6ed-11d2-b252-00c04f681b71}";

                //Open the Updategram (Input) Stream, using the
                //contents of the Updategram Text Box
                strmUpdategram.Open();
                strmUpdategram.WriteText(strXML);
                strmUpdategram.Position = 0;
                cmdUpdategram.CommandStream = strmUpdategram;

                //Open the Results (Output) Stream ready to catch the output
                cmdUpdategram.Properties["Output Stream"].Value = strmResults;
                cmdUpdategram.Properties["Output Encoding"].Value = "UTF-8";

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "cmdUpdategram.Execute" }));

                //Execute the Updategram
                cmdUpdategram.Execute(out lngRecordsAffected, null, Convert.ToInt32(ExecuteOptionEnum.adExecuteStream));

                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat(new string[] { "Result = ", strmResults.ReadText() }), string.Concat(new string[] { PROC_NAME, "Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmdUpdategram = null;
                strmResults = null;
                strmUpdategram = null;
            }

            return strmResults.ReadText();
        }

        public string ConvertAdoCmdToClientSideXmlQuery(Command adoCmd, string strProc)
        {
            string strValue = string.Empty;
            try
            {
                //Loop through each parameter and extract its value.
                foreach (Parameter objParam in adoCmd.Parameters)
                {
                    //Skip past the return value parameter.
                    if (objParam.Direction != ParameterDirectionEnum.adParamReturnValue)
                    {
                        //Special handling for some types.
                        switch (objParam.Type)
                        {
                            case DataTypeEnum.adBoolean:
                                //If Empty then allow procedure to use it's default.
                                if (objParam.Value == null)
                                    strValue = string.Empty;
                                else if (objParam.Value == null)
                                    strValue = string.Concat(objParam.Name, " =NULL");
                                else if (Convert.ToBoolean(objParam.Value) == true)
                                    strValue = string.Concat(objParam.Name, " = '1'");

                                else
                                    strValue = string.Concat(objParam.Name, " = '0'");
                                break;
                            default:
                                if (objParam.Value == null)
                                    strValue = string.Empty;
                                else if (objParam.Value == null)
                                    strValue = string.Concat(objParam.Name, " =NULL");
                                else
                                    strValue = string.Concat(objParam.Name, " = '", objParam.Value, "'");
                                break;

                        }
                        if (strValue.Length > 0)
                            strProc = Left(strProc, (strProc.Length - 1));
                    }

                }
                if (Right(strProc, 1) == ",")
                    strProc = Left(strProc, (strProc.Length - 1));
                strProc = string.Concat(strProc, " FOR XML EXPLICIT");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return strProc;
        }


        /// <summary>
        /// Builds a SQL Query string from variant ParamArray.
        /// </summary>
        /// <param name="strProc"></param>
        /// <param name="varParams"></param>
        /// <returns></returns>
        public string ConvertParamArrayToQueryString(string strProc, params object[] varParams)
        {
            string strSQL = string.Empty,
                strValue = string.Empty;
            int icount;
            try
            {
                strSQL = strProc;
                if (varParams != null)
                {
                    if (varParams.GetUpperBound(0) != 1)
                    {
                        //Loop through varParams and stuff them into the query string.
                        for (icount = varParams.GetLowerBound(0); icount < varParams.GetUpperBound(0); icount++)
                        {
                            //Ignore null items in varParams.
                            if (varParams[icount] != null)
                            {
                                strValue = Convert.ToString(varParams[icount]);

                                //Set blank strings to NULL for the stored procedures.
                                if (strValue.Length == 0)
                                    strSQL = string.Concat(strSQL, " NULL,");
                                else
                                    strSQL = string.Concat(strSQL, " ", strValue, ",");

                            }
                        }

                        //Strip off any trailing comma.
                        if (this.Right(strSQL, 1) == ",")
                            strSQL = this.Left(strSQL, (strSQL.Length - 1));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return strSQL;
        }

        /// <summary>
        /// Fills in the Command parameters from the passed param array.
        ///The list of parameters is retrieved from the database.
        /// </summary>
        /// <param name="adoCmd"></param>
        /// <param name="varParams"></param>
        /// <param name="strSP"></param>
        public void ConvertParamArrayToAdoParams(Command adoCmd, object[] varParams, string strSP)
        {
            Parameter objParam = null;
            bool blnItemFound = false;
            int icount, icountParameter = 0;
            try
            {
                //Check to make sure varParams has data.
                if (varParams != null)
                {
                    if (varParams.GetUpperBound(0) != 1)
                    {
                        //Loop through varParams and stuff them into the query string.
                        for (icount = varParams.GetLowerBound(0); icount < varParams.GetUpperBound(0); icount++)
                        {
                            //Ignore null items in varParams.
                            if (varParams[icount] != null)
                            {

                                //Item() will throw if the paramter is not found.
                                //Skip over this error as we allow params to be passed
                                //in that we do not need.
                                objParam = adoCmd.Parameters[Convert.ToString(varParams[icount])];

                                blnItemFound = Convert.ToBoolean(objParam != null);

                                //Was the param found in the collection?  Dont set any param
                                //for which we don't have data, allowing it to remain empty.
                                //This in turn allows us to leave it out of the SP call so
                                //that the procedure will take the default value.
                                if (blnItemFound)
                                    SetAdoParameterValue(ref objParam, Convert.ToString(varParams[icount]), strSP);
                            }
                        }

                    }
                }

                //Delete any unstuffed params so that they will take the default value.
                //Note that VB doesn't recalc the loop bounds at each iteration like C++
                //would, so we have to put the crappy Exit For at the top.
                while (icountParameter < adoCmd.Parameters.Count - 1)
                {
                    if (icountParameter >= adoCmd.Parameters.Count)
                        return;

                    objParam = adoCmd.Parameters[icountParameter];
                    if (objParam.Direction != ParameterDirectionEnum.adParamReturnValue)
                    {
                        adoCmd.Parameters.Delete(icountParameter);
                        icountParameter = icountParameter - 1;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objParam = null;
            }
        }

        /// <summary>
        ///  Fills in the Command parameters from the passed collection.
        /// </summary>
        /// <param name="adoCmd"></param>
        /// <param name="colForm"></param>
        /// <param name="strProcName"></param>
        private void ConvertCollectionToAdoParams(Command adoCmd, ICollection colForm, string strProcName)
        {
            bool blnItemFound = false;
            Parameter objParam = null;
            int countVal = 0;
            string strValue = string.Empty;
            try
            {
                //Add the parameters from colForm.
                foreach (Parameter objTemp in adoCmd.Parameters)
                {
                    objParam = objTemp;

                    //Don't try to set the return value, doh.
                    if (objParam.Direction != ADODB.ParameterDirectionEnum.adParamReturnValue)
                    {
                        // If the SP has a parameter that was not found in
                        // colForm then the call to colForm.Item will throw.
                        strValue = colForm.Count.ToString(objParam.Name);

                        //TODO
                        //blnItemFound = Convert.ToBoolean(Err.Number = 0);

                        //Was the param found in the collection?  Dont set any param
                        //for which we don't have data, allowing it to remain empty.
                        //This in turn allows us to leave it out of the SP call so
                        //that the procedure will take the default value.
                        if (blnItemFound)
                            SetAdoParameterValue(ref objParam, strValue, strProcName);
                        else
                            objParam.Value = null;

                        //Delete any unstuffed params so that they will take the default value.
                        // Note that VB doesn't recalc the loop bounds at each iteration like C++
                        // would, so we have to put the crappy Exit For at the top.
                        for (countVal = 0; countVal < adoCmd.Parameters.Count; countVal--)
                        {
                            if (countVal >= adoCmd.Parameters.Count)
                                objParam = adoCmd.Parameters[countVal];
                            if (objParam.Direction != ADODB.ParameterDirectionEnum.adParamReturnValue && string.IsNullOrEmpty(objParam.Value.ToString()))
                            {
                                adoCmd.Parameters.Delete(countVal);
                                countVal -= 1;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// When in debug mode, will trace out the Parameters.
        /// </summary>
        /// <param name="objParams"></param>
        public void TraceAdoParameters(Parameters objParams)
        {
            //Parameter objParam;
            string strTrace = string.Empty;
            try
            {
                mobjEvents = new CEvents();
                foreach (Parameter objParam in objParams)
                {
                    if (objParam.Direction != ParameterDirectionEnum.adParamReturnValue)
                    {
                        strTrace = string.Concat(strTrace, objParam.Name, "=");

                        if (objParam.Value == null)
                            strTrace = string.Concat(strTrace, "NULL", ", ");
                        else
                            strTrace = string.Concat(strTrace, "'", Convert.ToString(objParam.Value), "', ");

                    }
                }

                if (strTrace.Length > 2)
                {
                    strTrace = this.Left(strTrace, ((strTrace.Length) - 2));
                    mobjEvents.Trace(strTrace, "ADO Parameters");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Purpose: Attempts to set the passed Parameter from the passed value.
        /// Takes into account the parameter type and throws warnings that
        /// will make more sense.
        /// </summary>
        /// <param name="objParam"></param>
        /// <param name="strValue"></param>
        /// <param name="strSP"></param>
        public void SetAdoParameterValue(ref ADODB.Parameter objParam, string strValue, string strSP)
        {
            string PROC_NAME = "SetAdoParameterValue()";
            long checkLong = 0;
            bool blnIsnumeric;
            try
            {
                //'For blank strings set the param to Null so that the procedure sees a NULL.
                if (strValue.Length == 0)
                    objParam.Value = null;
                else
                {
                    switch (objParam.Type)
                    {
                        // 'Numeric Types
                        case DataTypeEnum.adBigInt:
                        case DataTypeEnum.adBinary:
                        case DataTypeEnum.adChapter:
                        case DataTypeEnum.adError:
                        case DataTypeEnum.adDate:
                        case DataTypeEnum.adCurrency:
                        case DataTypeEnum.adDecimal:
                        case DataTypeEnum.adDouble:
                        case DataTypeEnum.adFileTime:
                        case DataTypeEnum.adInteger:
                        case DataTypeEnum.adLongVarBinary:
                        case DataTypeEnum.adNumeric:
                        case DataTypeEnum.adSingle:
                        case DataTypeEnum.adSmallInt:
                        case DataTypeEnum.adTinyInt:
                        case DataTypeEnum.adUnsignedBigInt:
                        case DataTypeEnum.adUnsignedInt:
                        case DataTypeEnum.adUnsignedSmallInt:
                        case DataTypeEnum.adUnsignedTinyInt:
                        case DataTypeEnum.adVarBinary:
                        case DataTypeEnum.adVarNumeric:

                            blnIsnumeric = long.TryParse(strValue, out checkLong);
                            if (!blnIsnumeric)
                                throw new Exception(string.Concat(ErrorEnum.eInvalidParameter_NonNumeric, PROC_NAME, string.Concat(strValue, "|", objParam.Name, "|", objParam.Type, "|", strSP)));
                            objParam.Value = strValue;
                            break;

                        //String types
                        case DataTypeEnum.adBSTR:
                        case DataTypeEnum.adChar:
                        case DataTypeEnum.adLongVarChar:
                        case DataTypeEnum.adLongVarWChar:
                        case DataTypeEnum.adVarChar:
                        case DataTypeEnum.adVarWChar:
                        case DataTypeEnum.adWChar:

                            if (objParam.Size > 0)
                            {
                                if (strValue.Replace("''", "'").Length > objParam.Size)
                                {
                                    //'if the unescaped data length is greater than the param size then raise an error
                                    if (strValue.Length > objParam.Size)
                                    {
                                        if (strValue.Length > 50)
                                        {
                                            throw new Exception(string.Concat(ErrorEnum.eInvalidParameter_StringLength, PROC_NAME, string.Concat(Left(strValue, 50), "...|", objParam.Name, "|", objParam.Type, "|", strSP, "|", objParam.Size)));
                                        }
                                        else
                                        {
                                            throw new Exception(string.Concat(ErrorEnum.eInvalidParameter_StringLength, PROC_NAME, string.Concat(strValue, "|", objParam.Name, "|", objParam.Type, "|", strSP, "|", objParam.Size)));
                                        }
                                    }
                                }
                            }

                            objParam.Value = strValue;
                            break;
                        //'Other special types.
                        case DataTypeEnum.adBoolean:
                        case DataTypeEnum.adDBDate:
                        case DataTypeEnum.adDBTime:
                        case DataTypeEnum.adDBTimeStamp:
                        case DataTypeEnum.adEmpty:
                        case DataTypeEnum.adGUID:
                        case DataTypeEnum.adPropVariant:
                        case DataTypeEnum.adUserDefined:
                            objParam.Value = strValue;
                            break;
                        //'Unknown type?
                        default:
                            throw new Exception(string.Concat(ErrorEnum.eInvalidParameterType, PROC_NAME, string.Concat(objParam.Name, "|", objParam.Type, "|", strSP)));

                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Builds up a "||" delimeted list of error messages.
        /// Returns: The list as a string.
        /// </summary>
        /// <param name="objAdoConn"></param>
        /// <returns></returns>
        private string GetErrorDescriptions(ref ADODB.Connection objAdoConn)
        {
            string strErr = string.Empty;
            try
            {
                if (objAdoConn != null)
                {
                    mobjEvents.Env(string.Concat("Actual Connect String = ", objAdoConn.ConnectionString));
                    if (objAdoConn.Errors.Count > 0)
                    {
                        // 'Loop through all the ADO errors and extract their descriptions.
                        foreach (ADODB.Error objErr in objAdoConn.Errors)
                        {
                            if (Left(objErr.Description, 8).ToUpper() != "WARNING:")
                                strErr = string.Concat(strErr, objErr.Description, "||");
                        }

                        // 'Remove trailing "||"
                        strErr = Left(strErr, ((strErr.Length) - 2));
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strErr;
        }

        ///// No Need
        //private bool IsFailoverErr()
        //{
        //    bool blnIsFailoverErr = false;
        //    try
        //    {
        //        switch (Err().Number)//TODO verify...
        //        {

        //            //Errors listed in the ADO 2.7 API Reference
        //            case ErrorValueEnum.adErrCannotComplete:
        //            case ErrorValueEnum.adErrFeatureNotAvailable:
        //            case ErrorValueEnum.adErrIllegalOperation:
        //            case ErrorValueEnum.adErrInvalidConnection:
        //            case ErrorValueEnum.adErrObjectClosed:
        //            case ErrorValueEnum.adErrObjectNotSet:
        //            case ErrorValueEnum.adErrOpeningFile:
        //            case ErrorValueEnum.adErrOutOfSpace:
        //            case ErrorValueEnum.adErrProviderFailed:
        //            case ErrorValueEnum.adErrProviderNotFound:
        //            case ErrorValueEnum.adErrReadFile:
        //            case ErrorValueEnum.adErrResourceLocked:
        //            case ErrorValueEnum.adErrStillConnecting:
        //            case ErrorValueEnum.adErrStillExecuting:
        //            case ErrorValueEnum.adErrUnavailable:
        //            case ErrorValueEnum.adErrUnsafeOperation:
        //            case ErrorValueEnum.adErrURLDoesNotExist:
        //            case ErrorValueEnum.adErrVolumeNotFound:
        //            case ErrorValueEnum.adErrWriteFile:
        //            case ErrorValueEnum.adwrnSecurityDialog:
        //            case ErrorValueEnum.adwrnSecurityDialogHeader:

        //                blnIsFailoverErr = true;
        //                break;

        //            //'Errors listed in VC98\Include\OLEDBERR.H
        //            case DB_E_BADACCESSORHANDLE:
        //            case DB_SEC_E_PERMISSIONDENIED:
        //            case DB_E_BADLOCKMODE:
        //            case DB_E_NOTREENTRANT:
        //            case DB_E_ABORTLIMITREACHED:
        //            case DB_E_OBJECTCREATIONLIMITREACHED:
        //            case DB_S_ASYNCHRONOUS:
        //            case DB_E_ERRORSOCCURRED:

        //                blnIsFailoverErr = true;
        //                break;
        //            // 'Error listed in VC98\Include\TRANSACT.H
        //            case XACT_E_CONNECTION_DOWN:
        //            case XACT_E_CONNECTION_DENIED:

        //                blnIsFailoverErr = true;
        //                break;

        //            //'COM Errors
        //            case E_UNEXPECTED:
        //            case E_OUTOFMEMORY:
        //            case E_ABORT:
        //            case E_ACCESSDENIED:

        //                blnIsFailoverErr = true;
        //                break;

        //            //'Errors listed in MSDN articles.
        //            case 0x80020009:

        //                blnIsFailoverErr = true;
        //                break;
        //            //Error that actually cropped up during a failover test.
        //            case E_FAIL:

        //                blnIsFailoverErr = true;
        //                break;
        //            //Internal connection error.
        //            case eConnectionOpenError:

        //                blnIsFailoverErr = true;
        //                break;
        //            //Otherwise don't consider this a possible failover error.
        //            default:
        //                blnIsFailoverErr = false;

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return blnIsFailoverErr;

        //}

        /// <summary>
        /// Initializes DoFailoverRetry below.
        ///  Returns: Nothing.
        /// </summary>
        public void InitFailoverLoop()
        {
            //'Reset the failover loop counter.
            mintFailoverCount = 1;
        }

        /// <summary>
        /// Purpose: Determines if the current error is a failover error, and if
        /// so resets the passed connection and error.
        ///  Returns: True if we need to retry.
        /// </summary>
        /// <param name="adoConn"></param>
        /// <param name="strFunction"></param>
        /// <returns></returns>
        public bool DoFailoverRetry(ref ADODB.Connection adoConn, string strFunction)//Todo err.number
        {
            bool blnDoFailoverRetry = false;
            mDataAccessor = new MDataAccessor();
            try
            {
                //if (IsFailoverErr())
                //{
                if (mintFailoverCount <= MaxFailoverAttempts)
                {
                    blnDoFailoverRetry = true;

                    //Log failure internally, but do not raise to caller just yet.
                    //'Note that we only log the first one to avoid junking up
                    //'our inboxes with error emails.

                    if (mintFailoverCount <= 1)
                    {
                        //'Per TestTrack 1782 - Disable all error and warning emails that no longer hold relevance.
                        // 'Thus Reduced the following message from an event to a trace message.

                        if (m_blnDebugMode)
                            mobjEvents.Trace(string.Concat("A CRITICAL DATABASE ERROR HAS OCCURRED", Environment.NewLine, "ENTERING FAILOVER RETRY LOOP", Environment.NewLine, GetErrorDescriptions(ref madoConn)), string.Concat("DoFailoverRetry: ", strFunction));

                        //'If Not madoConn Is Nothing Then mEvents.Env "Actual Connect String = " & adoConn.ConnectionString
                        //'HandleEvent Err.Number, strFunction & Err.Source, Err.Description, _
                        //'"DoFailoverRetry: A CRITICAL DATABASE ERROR HAS OCCURRED - ENTERING FAILOVER RETRY LOOP", _
                        //'GetErrorDescriptions(madoConn), False

                        adoConn = null;

                        //'Pause to give time for cluster failover to complete.

                        mDataAccessor.DoSleep(FailoverSleepPeriod);
                    }

                //}
                    else
                    {
                        //'Add note to log that the retry count has been exceeded.
                        mobjEvents.Env(string.Concat("DoFailoverRetry: MAXIMUM RETRY COUNT OF ", MaxFailoverAttempts, " HAS BEEN EXCEEDED!"));
                    }


                    //'Increment the failover loop counter.
                    mintFailoverCount = mintFailoverCount + 1;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return blnDoFailoverRetry;
        }


        /// <summary>
        /// This method used to be called after every query execution, but has since
        /// been removed because it can screw up transactions begun in caller
        /// </summary>
        /// <param name="blnShaped"></param>
        public void CheckTransaction([Optional] bool blnShaped)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "CheckTransaction: ");
            object RecordsAffected = null;
            try
            {
                if (blnShaped)
                    madoShapeConn.Execute("IF @@TRANCOUNT > 0 ROLLBACK TRAN", out RecordsAffected, Convert.ToInt32(ExecuteOptionEnum.adExecuteNoRecords));
                else
                    madoConn.Execute("IF @@TRANCOUNT > 0 ROLLBACK TRAN", out RecordsAffected, Convert.ToInt32(ExecuteOptionEnum.adExecuteNoRecords));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Returns the most useful return value for calls that have use a command object
        /// and a parameters collection.
        /// </summary>
        public object BestReturnValue(ref ADODB.Command adoCmd, long lngRecordsAffected)
        {
            object objBestReturnValue = null;

            try
            {
                //'Start off with the records affected value.
                objBestReturnValue = lngRecordsAffected;

                //'Use any return value.
                foreach (ADODB.Parameter objParam in adoCmd.Parameters)
                {
                    if (objParam.Direction == ParameterDirectionEnum.adParamReturnValue)
                    {
                        objBestReturnValue = objParam.Value;
                    }
                }

                //'If no return values, then take the first output param.
                foreach (ADODB.Parameter objParam in adoCmd.Parameters)
                {
                    if (objParam.Direction == ParameterDirectionEnum.adParamOutput || objParam.Direction == ParameterDirectionEnum.adParamInputOutput)
                    {
                        objBestReturnValue = objParam.Value;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objBestReturnValue;
        }


        /// <summary>
        /// These can be used when you are not using COM+ to automatically do transaction handling.
        /// </summary>
        public void BeginTransaction()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "BeginTransaction: ");
            try
            {
                if (m_blnDebugMode)
                    mobjEvents.Trace("", PROC_NAME);

                //If madoConn Is Nothing Then CheckConnection
                CheckConnection();

                if (mblnInTrans == true)
                    throw new Exception(string.Concat(ErrorEnum.eCannotNestTrans, PROC_NAME, "Transaction already started!"));
                else
                {
                    madoConn.CommitTrans();
                    mblnInTrans = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CommitTransaction()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "CommitTransaction: ");
            try
            {
                if (m_blnDebugMode)
                    mobjEvents.Trace("", PROC_NAME);
                if (mblnInTrans == false)
                    throw new Exception(string.Concat(ErrorEnum.eNoTransExists, PROC_NAME, "No transaction exists!"));
                else
                {
                    mblnInTrans = false;
                    madoConn.CommitTrans();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RollbackTransaction()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "RollbackTransaction: ");
            try
            {
                if (m_blnDebugMode)
                    mobjEvents.Trace("", PROC_NAME);
                if (mblnInShapeTrans == false)
                {
                    //'Allow owner processes to call this in their error
                    //' handlers without doing any weenie boolean state tracking.
                    //'Err.Raise eNoTransExists, PROC_NAME, "No transaction exists!"
                }
                else
                {
                    mblnInTrans = false;
                    madoConn.RollbackTrans();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Shape_BeginTransaction()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "Shape_BeginTransaction: ");
            try
            {
                if (m_blnDebugMode)
                    mobjEvents.Trace("", PROC_NAME);

                if (madoShapeConn == null)
                    CheckConnection();
                if (mblnInShapeTrans == true)
                    throw new Exception(string.Concat(ErrorEnum.eCannotNestTrans, PROC_NAME, "Already in a transaction!"));
                else
                {
                    madoShapeConn.BeginTrans();
                    mblnInShapeTrans = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Shape_CommitTransaction()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "Shape_CommitTransaction:");
            try
            {
                if (m_blnDebugMode)
                    mobjEvents.Trace("", PROC_NAME);
                if (mblnInShapeTrans == false)
                    throw new Exception(string.Concat(ErrorEnum.eNoTransExists, PROC_NAME, "No transaction exists!"));
                else
                {
                    mblnInShapeTrans = false;
                    madoShapeConn.CommitTrans();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Shape_RollbackTransaction()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "Shape_RollbackTransaction: ");
            try
            {
                if (m_blnDebugMode)
                    mobjEvents.Trace("", PROC_NAME);

                if (mblnInShapeTrans == false)
                {
                    // Allow owner processes to call this in their error
                    //'handlers without doing any weenie boolean state tracking.
                    //'Err.Raise eNoTransExists, PROC_NAME, "No transaction exists!"
                }
                else
                {
                    mblnInShapeTrans = false;
                    madoShapeConn.RollbackTrans();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// This method creates the command object.
        /// </summary>
        /// <param name="strCommandText"></param>
        /// <param name="lngCommandTimeout"></param>
        public void FpCreateCommand(string strCommandText, [Optional]long lngCommandTimeout)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "FpCreateCommand: ");
            madoFpCmd = new ADODB.Command();
            try
            {
                if (m_blnDebugMode)
                    mobjEvents.Trace(string.Concat("Command = '", strCommandText, "'", PROC_NAME));

                mobjEvents.Assert(Convert.ToBoolean(madoFpCmd == null), "CreateSpCommand object was not Nothing");
                mobjEvents.Assert(Convert.ToBoolean(!string.IsNullOrEmpty(strCommandText)), "Must pass a command");

                madoFpCmd.CommandTimeout = ConnectionTimeout;

                if (strCommandText.IndexOf("?") > 0)
                    madoFpCmd.CommandType = CommandTypeEnum.adCmdText;
                else
                    madoFpCmd.CommandType = CommandTypeEnum.adCmdStoredProc;

                madoFpCmd.CommandText = strCommandText;
                mstrFpCmdText = strCommandText;

                if (lngCommandTimeout != 0)
                    madoFpCmd.CommandTimeout = Convert.ToInt32(lngCommandTimeout);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// This method adds a parameter to the command object.
        /// </summary>
        public void FpAddParam(string strName, DataTypeEnum enmDataType, ParameterDirectionEnum enmDirection, [Optional]long lngSize, [Optional]object varValue)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "FpAddParam: ");
            try
            {
                mobjEvents.Assert(Convert.ToBoolean((madoFpCmd != null)), "FpCreateCommand must be called successfully before FpAddParam");
                madoFpCmd.Parameters.Append(madoFpCmd.CreateParameter(strName, enmDataType, enmDirection, Convert.ToInt32(lngSize), varValue));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The "Fp" methods are used to create an ADO command where the paramters
        /// are specified via CreateParameter.  See FpCreateCommand comments.
        /// This method executes the command.
        /// </summary>
        /// <returns></returns>
        public object FpExecute()//todo failovercheck
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "FpExecute: ");
            long lngRecordsAffected = 0;
            object objFpExecute = null;
            object RecordAffected = null;
            object tmpObject = null;
            try
            {
                mobjEvents.Assert(Convert.ToBoolean((madoFpCmd != null)), "FpCreateCommand must be called successfully before FpExecute");

                InitFailoverLoop();
                //FailOverLoop:

                CheckShapeConnection();

                //'Initialize the ADO Command object
                madoFpCmd.ActiveConnection = madoConn;

                if (m_blnDebugMode)
                    TraceAdoParameters(madoFpCmd.Parameters);
                tmpObject = lngRecordsAffected as object;
                madoFpCmd.Execute(out tmpObject, RecordAffected, Convert.ToInt32(ExecuteOptionEnum.adExecuteNoRecords));

                //FailOverCheck:

                if (DoFailoverRetry(ref madoConn, PROC_NAME))
                    //IsFailoverErr();

                    //'Get the smartest return value.
                    objFpExecute = BestReturnValue(ref madoFpCmd, lngRecordsAffected);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                madoFpCmd = null;
            }
            return objFpExecute;
        }


        /// <summary>
        /// The "Fp" methods are used to create an ADO command where the paramters
        /// are specified via CreateParameter.  See FpCreateCommand comments.
        /// This method extracts a returned parameters.
        /// </summary>
        /// <param name="strName"></param>
        public object FpGetParameter(string strName)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "FpGetParameter: ");
            ADODB.Parameter objParam = null;
            object objFpGetParam = null;
            try
            {
                objParam = new ADODB.Parameter();

                //'Assert that the command exists.
                mobjEvents.Assert(Convert.ToBoolean((madoFpCmd != null)), string.Concat("FpCreateCommand must be called successfully before ", PROC_NAME));

                //'Assert that the command has parameters.
                mobjEvents.Assert(Convert.ToBoolean((madoFpCmd.Parameters.Count > 0)), "There are no Fp Parameters to return");

                //'Get the requested parameter.
                objParam = madoFpCmd.Parameters[strName];

                //'Assert that we found the parameter.
                mobjEvents.Assert(Convert.ToBoolean((objParam != null)), string.Concat(strName, "not found in Parameters collection for ", mstrFpCmdText));

                //'Assert that the param was an output or return param.
                mobjEvents.Assert(Convert.ToBoolean(objParam.Direction == ParameterDirectionEnum.adParamInputOutput || objParam.Direction == ParameterDirectionEnum.adParamOutput || objParam.Direction == ParameterDirectionEnum.adParamReturnValue), string.Concat(strName, " is neither an output nor a return parameter for ", mstrFpCmdText));
                objFpGetParam = objParam.Value;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objParam = null;
            }
            return objFpGetParam;
        }

        /// <summary>
        /// This method cleans up the Fp method globals.
        /// </summary>
        public void FpCleanUp()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "FpCleanUp: ");
            try
            {
                // 'Assert that the command exists.
                mobjEvents.Assert(Convert.ToBoolean((madoFpCmd != null)), string.Concat("FpCreateCommand must be called successfully before ", PROC_NAME));
                mstrFpCmdText = "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                madoFpCmd = null;
            }
        }

        /// <summary>
        /// Add custom NotifyEvents handling logic before calling mEvents.HandleEvent().
        /// </summary>
        /// <param name="lngNumber"></param>
        public string HandleEvent(long lngNumber, string strSource, string strDescription, [Optional]string strTrace, [Optional]string strAdoErr, [Optional]bool blnRaise)
        {
            string strHandleEvent = string.Empty;
            try
            {
                if (m_blnNotifyEvents)
                    strHandleEvent = mobjEvents.HandleEvent(Convert.ToInt32(lngNumber), strSource, strDescription, strTrace, strAdoErr, blnRaise);
                else if (blnRaise)
                    throw new Exception(string.Concat(lngNumber, strSource, strDescription));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strHandleEvent;
        }

        ///<summary>
        /// Executes an action only stored procedure using named parameters.
        /// A Recordset object
        /// </summary>
        public ADODB.Recordset OpenRecordsetNamedParams(string strProcName, params object[] Params)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "OpenRecordsetNamedParams: ");
            ADODB.Command adoCmd = null;
            ADODB.Recordset adoRS = null;
            object[] varParams = null;
            string strSQL = string.Empty,
                   strValue = string.Empty,
                   strItem = string.Empty;
            object objRecordsAffected;
            try
            {
                adoCmd = new ADODB.Command();

                if (Params.GetUpperBound(0) == -1)
                    varParams = null;
                else if (Params.GetValue(0) != null)
                    varParams[0] = Params[0];
                else
                    varParams = Params;

                InitFailoverLoop();

                CheckConnection();
                adoCmd.ActiveConnection = madoConn;
                adoCmd.CommandText = strProcName;
                adoCmd.CommandType = CommandTypeEnum.adCmdStoredProc;
                adoCmd.CommandTimeout = ConnectionTimeout;
                adoCmd.NamedParameters = true;

                adoCmd.Parameters.Refresh();

                ConvertParamArrayToAdoParams(adoCmd, varParams, strProcName);

                if (m_blnDebugMode)
                {
                    mobjEvents.Trace(string.Concat("CmdTxt = ", adoCmd.CommandText, PROC_NAME, "adoCmd.Execute"));
                    TraceAdoParameters(adoCmd.Parameters);
                }

                adoRS = adoCmd.Execute(out objRecordsAffected, null, -1);

                if (m_blnDebugMode)
                    mobjEvents.Trace("", string.Concat(PROC_NAME, "Finished"));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                adoCmd = null;
                adoRS = null;
            }
            return adoRS;
        }

        #endregion

    }
}
