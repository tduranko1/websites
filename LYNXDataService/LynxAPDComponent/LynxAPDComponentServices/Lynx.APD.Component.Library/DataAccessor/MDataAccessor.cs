﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;

namespace Lynx.APD.Component.Library.DataAccessor
{
    class MDataAccessor
    {
        public string APP_NAME = "DataAccess.";

        public long DataAccessor_FirstError = 0x80061000;

        //API declares
        //[DllImport("kernel32")]
        //public void sleep(long dwMilliseconds);

        /// <summary>
        /// Wraps CreateObject() with better debug information.
        /// </summary>
        /// <param name="strObjectName"></param>
        /// <returns></returns>
        public object CreateObjectEx(string strObjectName)
        {
            object mobjCreateObject = null;
            Type createdObj = null;
            try
            {
                createdObj = Type.GetTypeFromProgID(strObjectName);
                if (createdObj == null)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(DataAccessor_FirstError + 10), "CreateObject('", strObjectName, "') returned Nothing." }));

                mobjCreateObject = Activator.CreateInstance(createdObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mobjCreateObject;
        }

        /// <summary>
        ///  Wraps Sleep().
        /// </summary>
        /// <param name="lngMS"></param>
        public void DoSleep(long lngMS)
        {
            long lCount;
            for (lCount = 0; lCount < lngMS / 10; lCount++)
            {
                Thread.Sleep(30);
            }
        }
    }
}
