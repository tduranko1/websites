﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Xsl;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using SiteUtilities;

namespace Lynx.APD.Component.Library.Common
{
    public class MDomUtils
    {
        private const string MODULE_NAME = "MDomUtils.";
        CEvents g_objEvents = new CEvents();        

        //Internal error codes for this module.  
        public enum EventCodes
        {
            // eDomDataValidationFailed = 0x80065000 + Convert.ToInt64(100),
            eXmlInvalid,
            eXmlMissingAttribute,
            eXmlMissingElement,
            eXmlEmptyNodeSet
        }

        /// <summary>
        /// Loads the passed Dom from the passed string and reports any errors.
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="strXml"></param>
        /// <param name="strCallerName"></param>
        /// <param name="strXmlDescription"></param>
        public void LoadXml(ref XmlDocument objDom, ref string strXml, [Optional] string strCallerName, [Optional] string strXmlDescription)
        {
            bool blnValidXml = false;
            try
            {
                //loadXml returns true if the load succeeded.         
                if (!string.IsNullOrEmpty(strXml))
                    objDom.LoadXml(strXml);
                if (!string.IsNullOrEmpty(objDom.OuterXml) && !objDom.HasChildNodes)
                    blnValidXml = true;

                if (blnValidXml)
                {
                    throw new Exception(string.Concat(new string[] { "MDomUtils.LoadXml -The", strXmlDescription, " XML could not be loaded -", strXml }));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Loads the passed Dom from the passed file name string and reports any errors.
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="strFileName"></param>
        /// <param name="strCallerName"></param>
        /// <param name="strXmlDescription"></param>
        public void LoadXmlFile(ref XmlDocument objDom, ref string strFileName, string strCallerName, string strXmlDescription)
        {
            bool blnValidXml = false;
            try
            {
                //loadXml returns true if the load succeeded.
                if (!string.IsNullOrEmpty(strFileName))
                    objDom.LoadXml(strFileName);

                if (!string.IsNullOrEmpty(objDom.OuterXml) && !objDom.HasChildNodes)
                    blnValidXml = true;
                if (blnValidXml)
                    throw new Exception(string.Concat(new string[] { "The ", strXmlDescription, " xml could not be loaded. The XML is -", strFileName }));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        ///  Returns the passed XML escaped of any bad characters.
        ///  Cheese alert - we do this just by loading it into
        ///  a DOM and then converting it back into a string.
        /// </summary>
        /// <param name="strXml"></param>
        /// <returns></returns>
        public string EscapeXml(ref string strXml)
        {
            try
            {
                string strReturnValue = string.Empty;
                XmlDocument objDom = new XmlDocument();
                LoadXml(ref objDom, ref strXml, "", "CleanUpXml");
                strReturnValue = objDom.OuterXml;
                objDom = null;
                return strReturnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns the specified child node of the passed node.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public XmlNode GetChildNode(ref XmlNode objNode, string strPath, bool blnMustExist = false)
        {
            XmlNode xmlReturnValue = null;
            XmlNode objChild;
            try
            {
                objChild = objNode.SelectSingleNode(strPath);

                if (objChild != null)
                    xmlReturnValue = objChild;

                objChild = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xmlReturnValue;
        }

        /// <summary>
        /// Returns the specified child node text of the passed node.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public string GetChildNodeText(ref XmlNode objNode, string strPath, bool blnMustExist = false)
        {
            string strReturnValue = string.Empty;

            XmlNode objChild = null;
            try
            {
                objChild = GetChildNode(ref objNode, strPath, blnMustExist);

                if (objChild == null)
                    strReturnValue = "";
                else
                    strReturnValue = objChild.InnerText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturnValue;

        }

        /// <summary>
        /// Returns a node list for specified XPATH of the passed node.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public XmlNodeList GetChildNodeList(ref XmlNode objNode, string strPath, Boolean blnMustExist = false)
        {
            XmlNodeList objReturnChildList = null;
            XmlNodeList objChildList = null;
            try
            {
                objChildList = objNode.SelectNodes(strPath);

                if (objChildList != null)
                    objReturnChildList = objChildList;

                objChildList = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objReturnChildList;
        }

        /// <summary>
        /// Uses a style sheet to validate the passed Dom.
        /// This style sheet will transform to an empty string if everything
        //  validates just fine, or will return error strings compatible
        //  with SiteUtilities.CEvents.HandleMultiEvents().
        /// </summary>
        /// <param name="domXML"></param>
        /// <param name="strXslFileName"></param>
        /// <param name="objEvents"></param>
        /// <param name="blnRaise"></param>
        /// <returns></returns>
        public string ValidateDomData(ref XmlDocument domXML, ref string strXslFileName, ref object objEvents, bool blnRaise = true)
        {
            string strReturnValue = string.Empty; ;
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ValidateDomData: " });
            XmlDocument domXSL = null;
            string strResult = string.Empty,
                strdomXML = string.Empty;
            bool g_blnDebugMode = false;

            try
            {
                g_blnDebugMode = g_objEvents.IsDebugMode;               
                if (g_blnDebugMode)
                    g_objEvents.Trace(string.Concat(new string[] { "XML = ", domXML.OuterXml, PROC_NAME, "Validating XML versus file ", strXslFileName }));

                //Load the XSL to validate with.
                domXSL = new XmlDocument();

                //LoadXmlFile(ref domXSL, ref strXslFileName, PROC_NAME, "Validation XSL");

                //Transform the XML with the XSL style sheet.
                //ToDo:
                //strResult = domXML.transformNode(domXSL);            

                if (domXML != null)
                {
                    strdomXML = domXML.OuterXml;
                    using (StringReader sReader = new StringReader(strdomXML))
                    using (XmlReader xReader = XmlReader.Create(sReader))
                    using (StringWriter sWriter = new StringWriter())
                    using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                    {
                        XslCompiledTransform transformxslt = new XslCompiledTransform();
                        transformxslt.Load(strXslFileName);
                        transformxslt.Transform(xReader, xWriter);
                        strResult = sWriter.ToString();
                    }
                }


                if (Left(strResult, 1) == "|")
                    strResult = Mid(strResult, 2);

                strReturnValue = strResult;


                //If the result has contents then pass along then raise.
                if (strResult.Length > 0)
                    g_objEvents.HandleMultiEvents(1, PROC_NAME, strResult);
                else
                {
                    if (g_blnDebugMode)
                        g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Passed Data Validation" }));
                }

                domXSL = null;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strReturnValue;

        }

        /// <summary>
        /// Adds an attribute to the passed dom element
        /// </summary>
        /// <param name="objElem"></param>
        /// <param name="strName"></param>
        /// <param name="strValue"></param>
        public void AddAttribute(ref XmlElement objElem, string strName, string strValue)
        {
            XmlAttribute objAtt = null;
            try
            {
                objAtt = objElem.OwnerDocument.CreateAttribute(strName);
                objAtt.Value = strValue;
                objElem.SetAttributeNode(objAtt);
                objAtt = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Adds an attribute to the passed dom element
        /// </summary>
        /// <param name="objElem"></param>
        /// <param name="strName"></param>
        /// <returns></returns>
        public XmlElement AddElement(ref XmlElement objElem, string strName)
        {
            XmlElement xmlReturnValue = null;
            XmlElement objNewElem = null;
            try
            {
                objNewElem = objElem.OwnerDocument.CreateElement(strName);
                objElem.AppendChild(objNewElem);

                xmlReturnValue = objNewElem;
                objNewElem = null;
                return xmlReturnValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Renames an attribute of the passed dom element
        /// </summary>
        /// <param name="objElem"></param>
        /// <param name="strPath"></param>
        /// <param name="strNewName"></param>
        public void RenameAttribute(ref XmlElement objElem, string strPath, string strNewName)
        {
            try
            {
                XmlAttribute objAtt = null;
                //   objAtt = GetChildNode(ref objElem, strPath, false);           
                int intIdx = 0;
                string strValue = string.Empty;
                while (intIdx < objElem.Attributes.Count)
                {
                    objAtt = objElem.Attributes[intIdx];
                    if ((objAtt != null))
                    {
                        strValue = objAtt.InnerText;

                        objElem.RemoveAttribute(objAtt.Name);
                        objAtt = null;

                        AddAttribute(ref objElem, strNewName, strValue);
                    }
                    else
                        intIdx = intIdx + 1;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// Renames all matching attributes below in the passed dom element.
        /// </summary>
        /// <param name="objElem"></param>
        /// <param name="strOldName"></param>
        /// <param name="strNewName"></param>
        public void RenameAllAttributes(ref XmlElement objElem, string strOldName, string strNewName)
        {
            try
            {
                if (!string.IsNullOrEmpty(strOldName) && !string.IsNullOrEmpty(strNewName))
                {
                    //Rename any child attribute that matches.
                    RenameAttribute(ref objElem, string.Concat(new string[] { "@", strOldName }), strNewName);
                }
                //Now recursively do the same through all children.          
                if (objElem.ChildNodes.Count > 0)
                {
                    foreach (XmlNode objChild in objElem.ChildNodes)
                    {
                        if ((objChild != null))
                            RenameAllAttributes(ref objElem, strOldName, strNewName);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Removes all blank attributes of the passed element.
        /// </summary>
        /// <param name="objElem"></param>
        public void RemoveBlankAttributes(ref XmlElement objElem)
        {
            XmlAttribute objAtt = null;
            int intIdx = 0;
            try
            {
                while (intIdx < objElem.Attributes.Count)
                {
                    objAtt = objElem.Attributes[intIdx];
                    if (string.IsNullOrEmpty(objAtt.InnerText))
                        objElem.RemoveAttribute(objAtt.Name);
                    else
                        intIdx = intIdx + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Removes all blank attributes below in the passed dom element.
        /// </summary>
        /// <param name="objElem"></param>
        public void RemoveAllChildBlankAttributes(ref XmlElement objElem)
        {
            try
            {
                //Remove any blank child attributes.
                RemoveBlankAttributes(ref objElem);

                //Now recursively do the same through all children.
                //XmlNode objChild = null;
                if (objElem.ChildNodes.Count > 0)
                {
                    foreach (XmlNode objChild in objElem.ChildNodes)
                    {
                        if ((objChild != null))
                            RemoveBlankAttributes(ref objElem);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Uses the map file to convert codes to reference values.
        /// Requires that the DOM be formatted to the APD DB standard.
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="strMapFile"></param>
        /// <param name="objEvents"></param>
        public void MapAPDReferenceData(ref XmlDocument objDom, string strMapFile, ref object objEvents)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "MapAPDReferenceData: " });

            //Object variable declarations
            XmlDocument objMapDom = null;
            XmlNodeList objMapList = null,
             objAttParentList = null,
             objReplaceList = null;
            XmlNode xmlobjMapNode = null,
             xmlobjAttParentNode = null,
             xmlobjReplaceNode = null;
            XmlElement xmlobjAttParentElement = null;
            string strCodeAtt = string.Empty,
             strParent = string.Empty,
             strCode = string.Empty,
             strList = string.Empty,
             strMapAtt = string.Empty,
             strValue = string.Empty;

            //XmlNode objMapNode = null;
            //XmlNode objAttParentNode = null;
            //XmlNode objReplaceNode = null;
            //Load up the map file into a dom.
            //LoadXmlFile(ref objMapDom, ref strMapFile, PROC_NAME, "Map");
            try
            {
                objMapDom = new XmlDocument();

                if (!string.IsNullOrEmpty(strMapFile))
                {
                    objMapDom.LoadXml(strMapFile);
                    //Get all Map elements in the map file.
                    objMapList = objMapDom.SelectNodes("//Map");

                    //Loop through map elements.
                    foreach (XmlNode objMapNode in objMapList)
                    {
                        //Get a list of Replace elements under the map element.
                        objReplaceList = objMapNode.SelectNodes("Replace");
                        xmlobjMapNode = objMapNode;
                        //Extract map attributes.
                        strCodeAtt = GetChildNodeText(ref xmlobjMapNode, "@Att", true);
                        strParent = GetChildNodeText(ref xmlobjMapNode, "@Parent", false);
                        strList = GetChildNodeText(ref xmlobjMapNode, "@List", true);
                        strMapAtt = GetChildNodeText(ref xmlobjMapNode, "@MapAtt", true);

                        //Enforce some kind of parential presence.
                        if (string.IsNullOrEmpty(strParent))
                            strParent = "Root";


                        //Get a list of attributes matching the code for this map element.
                        objAttParentList = objDom.SelectNodes(string.Concat(new string[] { "//", strParent, "//*[@", strCodeAtt, "]" }));

                        //Loop through attributes list.

                        foreach (XmlNode objAttParentNode in objAttParentList)
                        {
                            xmlobjAttParentNode = objAttParentNode;
                            //Get the code value from the code attribute.
                            strCode = GetChildNodeText(ref xmlobjAttParentNode, string.Concat(new string[] { "@", strCodeAtt }), true);

                            //If strList is blank then just copy the value of Att.
                            if (string.IsNullOrEmpty(strList))
                                strValue = strCode;                                //Otherwise go look up the reference value.                           
                            else
                            {
                                //Get a reference value for this code.
                                if (strCode.Length > 0)
                                    strValue = objDom.SelectSingleNode(string.Concat(new string[] { "/Root/Reference", "[@List='", strList, "']", "[@ReferenceID='", strCode, "']", "/@Name" })).InnerText;
                                else
                                    strValue = "";
                                //Empty
                            }

                            //Loop through the replace elements and do any search-and-replace.
                            foreach (XmlNode objReplaceNode in objReplaceList)
                            {
                                xmlobjReplaceNode = objReplaceNode;
                                strValue = strValue.Replace(GetChildNodeText(ref xmlobjReplaceNode, "@Find", true), GetChildNodeText(ref xmlobjReplaceNode, "@Insert", true));
                                strValue = strValue.TrimEnd();
                                strValue = strValue.TrimStart();
                                //strValue = Strings.LTrim(Strings.RTrim(Strings.Replace(strValue, GetChildNodeText(ref objReplaceNode, "@Find", true), GetChildNodeText(ref objReplaceNode, "@Insert", true))));
                            }

                            if (string.IsNullOrEmpty(GetChildNodeText(ref xmlobjAttParentNode, string.Concat(new string[] { "@", strMapAtt }), false)))
                            {
                                xmlobjAttParentElement = (XmlElement)xmlobjAttParentNode;
                                AddAttribute(ref xmlobjAttParentElement, strMapAtt, strValue);
                            }
                        }
                        xmlobjMapNode = null;
                        xmlobjAttParentNode = null;
                        xmlobjReplaceNode = null;

                    }

                    objMapDom = null;
                    objMapList = null;
                    objAttParentList = null;
                    objReplaceList = null;
                }
                else
                {
                    //Raise the error if empty XML documet loads
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Formats a friendly error string from the passed dom parse error object.
        /// </summary>
        /// <returns></returns>
        public string FormatDomParseError()
        {
            try
            {
            }
            catch (Exception ex)
            {
            }
            return "";

        }

        /// <summary>
        /// Transforms the passed XML DOM to an XML DOM via the passed XSL file.
        /// Validates the original XML and XSL documents and raises intelligent errors.
        /// Does not validate the returned DOM beyond that done by MSXML.
        /// </summary>
        /// <param name="domXML"></param>
        /// <param name="strXslPath"></param>
        /// <param name="objEvents"></param>
        /// <returns></returns>
        public XmlDocument TransformDomAsDom(ref XmlDocument domXML, string strXslPath, ref object objEvents)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransformXmlAsDom: " });
            XmlDocument xmlReturnValue = null;
            //Objects that need clean-up.
            // XmlDocument domXSL = null;
            string domResult = string.Empty,
                strdomXML = string.Empty;
            //Primitives that dont.
            bool blnDebugMode = false;

            try
            {
                g_objEvents.Assert((domXML != null), "Passed XML DOM was Nothing.");
                g_objEvents.Assert(!string.IsNullOrEmpty(strXslPath), "Passed XSL file path string was blank.");

                //Get debug mode status.
                blnDebugMode = g_objEvents.IsDebugMode;

                //domXSL = new XmlDocument();

                if (blnDebugMode)
                    g_objEvents.Trace(string.Concat(new string[] { "StyleSheet = ", strXslPath }), PROC_NAME);

                //Load and validate the style sheet.
                // LoadXmlFile(ref domXSL, ref strXslPath, PROC_NAME, "XSL Style Sheet");

                //Transform the XML with the XSL style sheet.
                // domXML.TransformNodeToObject(domXSL, domResult);

                if (domXML != null)
                {
                    strdomXML = domXML.OuterXml;
                    using (StringReader sReader = new StringReader(strdomXML))
                    using (XmlReader xReader = XmlReader.Create(sReader))
                    using (StringWriter sWriter = new StringWriter())
                    using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                    {
                        XslCompiledTransform transformxslt = new XslCompiledTransform();
                        transformxslt.Load(strXslPath);
                        transformxslt.Transform(xReader, xWriter);
                        domResult = sWriter.ToString();
                    }

                    xmlReturnValue.LoadXml(domResult);
                }

                //Object clean-up
                // domXSL = null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return xmlReturnValue;

        }

        /// <summary>
        /// Transforms the passed XML string to an XML DOM via the passed XSL file.
        /// Validates the original XML and XSL documents and raises intelligent errors.
        /// Does not validate the returned DOM beyond that done by MSXML.
        /// </summary>
        /// <param name="strXml"></param>
        /// <param name="strXslPath"></param>
        /// <param name="objEvents"></param>
        /// <returns></returns>
        public XmlDocument TransformXmlAsDom(ref string strXml, string strXslPath, ref object objEvents)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransformXmlAsDom: " });
            XmlDocument xmlReturnValue = null;
            XmlDocument domXML = null;

            try
            {
                //Objects that need clean-up.
                domXML = new XmlDocument();

                g_objEvents.Assert(!string.IsNullOrEmpty(strXml), "Passed XML string was blank.");

                //Load and validate the param XML
                LoadXml(ref domXML, ref strXml, PROC_NAME, "Source XML");

                xmlReturnValue = TransformDomAsDom(ref domXML, strXslPath, ref objEvents);

                //Object clean-up
                domXML = null;
            }

            catch (Exception ex)
            {
                throw ex;
            }
            return xmlReturnValue;
        }

        /// <summary>
        /// Transforms the passed XML string to an XML DOM via the passed XSL file.
        /// Validates all three documents and raises intelligent errors.
        /// </summary>
        /// <param name="domXML"></param>
        /// <param name="strXslPath"></param>
        /// <param name="objEvents"></param>
        /// <returns></returns>
        public string TransformDomAsXml(ref XmlDocument domXML, string strXslPath, ref object objEvents)
        {
            return TransformDomAsDom(ref domXML, strXslPath, ref objEvents).OuterXml;
        }

        /// <summary>
        /// Transforms the passed XML string to an XML DOM via the passed XSL file.
        /// Validates all three documents and raises intelligent errors.
        /// </summary>
        /// <param name="strXml"></param>
        /// <param name="strXslPath"></param>
        /// <param name="objEvents"></param>
        /// <returns></returns>
        public string TransformXmlAsXml(ref string strXml, string strXslPath, ref object objEvents)
        {
            string strReturnValue = string.Empty;
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransformXmlAsDom: " });
            XmlDocument domXML = null;
            try
            {
                //Objects that need clean-up.
                domXML = new XmlDocument();

                g_objEvents.Assert(!string.IsNullOrEmpty(strXml), "Passed XML string was blank.");

                //Load and validate the param XML
                LoadXml(ref domXML, ref strXml, PROC_NAME, "Source XML");

                strReturnValue = TransformDomAsXml(ref domXML, strXslPath, ref objEvents);

                //Object clean-up
                domXML = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strReturnValue;
        }

        #region Common
        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }
        #endregion
    }
}
