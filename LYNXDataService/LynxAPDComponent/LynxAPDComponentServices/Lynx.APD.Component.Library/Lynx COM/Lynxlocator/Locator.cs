﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using ADODB;
using System.Xml;
using COMSVCSLib;
using MSXML2;

namespace Lynx.APD.Component.Library.Lynx_COM.Lynxlocator
{
    class Locator
    {
         string zMODULE = "lynxLocator.Locator";

         string sIngresDSN = string.Empty,
             sStylesheetPath=string.Empty,
             sCASCacheFilePath=string.Empty,
             sIngresDSNCA=string.Empty;

         ObjectContext ctxObject;
         lynxAccount4.modCommon mobjCommon = null;
         lynxAccount4.modDataAccess mobjDataAccess = null;
         private void ObjectControl_Activate()
         {
             string sOutput = string.Empty;
             XmlDocument oXMLDom = null;
             try
             {               
                 //=get=configuration==========================
                 mobjDataAccess = new lynxAccount4.modDataAccess();
                 mobjCommon = new lynxAccount4.modCommon();
                 sOutput = mobjCommon.GetConfiguration("lynxLocator");
                 oXMLDom = new XmlDocument();
                 oXMLDom.LoadXml(sOutput);

                 if (oXMLDom != null)
                 {
                     sIngresDSN = oXMLDom.SelectSingleNode("//DataSources/Ingres").InnerText;
                     sIngresDSNCA = oXMLDom.SelectSingleNode("//DataSources/IngresCA").InnerText;
                     sStylesheetPath = oXMLDom.SelectSingleNode("//Stylesheets").InnerText;
                     sCASCacheFilePath = oXMLDom.SelectSingleNode("//CASPath").InnerText;
                 }

                 oXMLDom = null;
             }
             catch (Exception)
             {                 
                 throw;
             }
             
         }

         private bool ObjectControl_CanBePooled()
         {
             return true;
         }

         private void ObjectControl_Deactivate()
         {
             ctxObject = null;
         }

        public string ValidatePhone(int iAreaCode, int iExchangeNo, string sUserID, string sUserSessionNo, string sStylesheet = "")
        {
            Command oCmd;
            Connection oConnection;
            string sStoredProc = string.Empty,
                sOutput = string.Empty;
            bool bLogError;
            object objExecute;
            try
            {

                mobjDataAccess = new lynxAccount4.modDataAccess();
                ObjectControl_Activate();
                sStoredProc = "dp_validate_phone_no";
                oCmd = new Command();
                oConnection = new Connection();

                //setup the connection
                oConnection.CommandTimeout = 60;
                oConnection.Open(sIngresDSN);

                oCmd.ActiveConnection = oConnection;
                oCmd.CommandType = ADODB.CommandTypeEnum.adCmdText;
                oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
                oCmd.CommandTimeout = 90;
                oCmd.Parameters.Append(oCmd.CreateParameter("retval", ADODB.DataTypeEnum.adInteger, ADODB.ParameterDirectionEnum.adParamReturnValue));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 1, "N"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 32, sUserID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_id", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 32, sUserSessionNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_area_code_no", ADODB.DataTypeEnum.adInteger, ADODB.ParameterDirectionEnum.adParamInput, -1, iAreaCode));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_exchange_no", ADODB.DataTypeEnum.adInteger, ADODB.ParameterDirectionEnum.adParamInput, -1, iExchangeNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_phone_no", ADODB.DataTypeEnum.adDouble, ADODB.ParameterDirectionEnum.adParamInputOutput, -1, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_city_state", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 20, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_city_nm", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 32, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_county_type", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_latitude", ADODB.DataTypeEnum.adDouble, ADODB.ParameterDirectionEnum.adParamInputOutput, -1, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_longitude", ADODB.DataTypeEnum.adDouble, ADODB.ParameterDirectionEnum.adParamInputOutput, -1, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_local_time", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 27, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_state_cd", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 32, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_tz_offset", ADODB.DataTypeEnum.adTinyInt, ADODB.ParameterDirectionEnum.adParamInputOutput, -1, 0));
                oCmd.Execute(out objExecute);

                ////STEP IV B : Check for Error message returned
                //if (oCmd.Parameters["retval"].Value != 0)
                //{
                //    if (oCmd.Parameters["retval"].ToString().IndexOf("Invalid Area Code", 1) > 0)
                //    {
                //        bLogError = false;
                //    }
                //}

                sOutput = mobjDataAccess.ParametersToXML(ref oCmd, true, null, "ValidatePhone");

                // STEP V : Transform the XML using a XSL Stylesheet
                //if a Stylesheet is specified then we need to transform the XML using that XSL Stylesheet

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet));


            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                oCmd = null;
            }
            return sOutput;
        }

        public string ValidateZip(string sZipCode, string sUserID, string sUserSessionNo, string sStylesheet = "")
        {
            ADODB.Command oCmd;
            ADODB.Connection oConnection;
            string sStoredProc = string.Empty,
                sOutput = string.Empty;
            bool bLogError;
            object objExecute;
            try
            {

                mobjDataAccess = new lynxAccount4.modDataAccess();
                sStoredProc = "dp_validate_city_state_zip";
                oCmd = new ADODB.Command();
                oConnection = new ADODB.Connection();

                //setup the connection
                oConnection.CommandTimeout = 60;
                oConnection.Open("");

                oCmd.ActiveConnection = oConnection;
                oCmd.CommandType = ADODB.CommandTypeEnum.adCmdText;
                oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(?,?,?,?,?,?,?,?,?,?) }");
                oCmd.CommandTimeout = 90;
                oCmd.Parameters.Append(oCmd.CreateParameter("retval", ADODB.DataTypeEnum.adInteger, ADODB.ParameterDirectionEnum.adParamReturnValue));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 1, "N"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 32, sUserID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_id", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 32, sUserSessionNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_city_nm", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 32, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_county_type", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_latitude", ADODB.DataTypeEnum.adDouble, ADODB.ParameterDirectionEnum.adParamInputOutput, -1, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_longitude", ADODB.DataTypeEnum.adDouble, ADODB.ParameterDirectionEnum.adParamInputOutput, -1, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_state_cd", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 32, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_zip_cd", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 9, sZipCode));
                oCmd.Execute(out objExecute);

                //    //STEP IV B : Check for Error message returned
                //if (oCmd.Parameters["retval"].Value != 0)
                //{
                //    if (oCmd.Parameters["retval"].ToString().IndexOf("Invalid Area Code", 1) > 0)
                //    {
                //        bLogError = false;

                //    }
                //}

                sOutput = mobjDataAccess.ParametersToXML(ref oCmd, true, null, "ValidateZip");

                // STEP V : Transform the XML using a XSL Stylesheet
                //if a Stylesheet is specified then we need to transform the XML using that XSL Stylesheet

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet));


            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                oCmd = null;
            }
            return sOutput;
        }

        private Recordset GetInstSearchResults(string sSessionTableName, string sStoredProcName, string sInsertSQL, string sConnectionString, int iLynxPgmNo, long lRetval, string sFilter = "", string sOutputColumns = "", int iMaxRecords = 0)
        {
            string sSQL = string.Empty,
                sOutput = string.Empty;
            Connection oConnection = null;
            Command oCommand = null;
            Recordset oRecordset = null;
            bool bLogError = false;
            object objExecute;
            string sMsgTxt = string.Empty;
            try
            {
                oConnection = new Connection();
                oCommand = new Command();
                oRecordset = new Recordset();
                bLogError = true;

                //setup the connection
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(sConnectionString);

                //create the session table
                sSQL = string.Concat("declare global temporary table session.", sSessionTableName, " as ",
                            "select * from ", sSessionTableName, " on commit preserve rows with norecovery");

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sInsertSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);

                //execute the stored procedure

                if (sStoredProcName != "dp_inst_search")
                {
                    //Execute the DB Procedure passing the Session Table as the Parameter
                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = string.Concat("{ ? = call lynxdba.", sStoredProcName, "(dummy=session.", sSessionTableName, ") }");
                    oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                    oCommand.CommandTimeout = 90;
                    // oCommand.Execute();
                    lRetval = Convert.ToInt64(oCommand.Parameters["retval"].Value);
                }
                else if (sStoredProcName == "dp_inst_search")
                {
                    //Execute the DB Procedure passing the Session Table as the Parameter
                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = string.Concat("{ ? = call lynxdba.", "dp_inst_search_a", "(dummy=session.", sSessionTableName, ") }");
                    oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                    oCommand.CommandTimeout = 90;
                    // oCommand.Execute();
                    lRetval = Convert.ToInt64(oCommand.Parameters["retval"].Value);

                    // Check Return Value from the DB Procedure for Error
                    if (lRetval != 0)
                    {
                        if (lRetval > 0)
                            throw new Exception(string.Concat(Convert.ToString(lRetval), "dp_inst_search_a", "Invalid Return Value ", Convert.ToString(lRetval), " from dp_inst_search_a"));
                    }
                    else
                    {
                        //Execute the DB Procedure passing the Session Table as the Parameter
                        oCommand.CommandType = CommandTypeEnum.adCmdText;
                        oCommand.CommandText = string.Concat("{ ? = call lynxdba.", "dp_inst_search_b", "(dummy=session.", sSessionTableName, ") }");
                        oCommand.CommandTimeout = 90;
                        oCommand.Execute(out objExecute);
                        lRetval =Convert.ToInt64( oCommand.Parameters["retval"].Value);

                        if (lRetval != 0)
                        {
                            if (lRetval > 0)
                                throw new Exception(string.Concat(Convert.ToString(lRetval), "dp_inst_search_b", "Invalid Return Value ", Convert.ToString(lRetval), " from dp_inst_search_b"));
                        }
                        else
                        {
                            //Execute the DB Procedure passing the Session Table as the Parameter
                            oCommand.CommandType = CommandTypeEnum.adCmdText;
                            oCommand.CommandText = string.Concat("{ ? = call lynxdba.", "dp_inst_search_c", "(dummy=session.", sSessionTableName, ") }");
                            oCommand.CommandTimeout = 90;
                            oCommand.Execute(out objExecute);
                            lRetval = Convert.ToInt64(oCommand.Parameters["retval"].Value);

                            if (lRetval != 0)
                            {
                                if (lRetval > 0)
                                    throw new Exception(string.Concat(Convert.ToString(lRetval), "dp_inst_search_c", "Invalid Return Value ", Convert.ToString(lRetval), " from dp_inst_search_c"));
                            }
                        }
                    }

                }

                // setup the command to retrieve the resulting recordset
                sSQL = string.Concat("select ", sOutputColumns, " from session.", sSessionTableName);

                if (sFilter != string.Empty)
                    sSQL = string.Concat(sSQL, sFilter);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                //execute the query for readonly
                oRecordset.MaxRecords = iMaxRecords;
                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, oCommand.ActiveConnection, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);

                //drop the session table
                sSQL = string.Concat("drop table session.", sSessionTableName);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);
                oCommand.ActiveConnection.CommitTrans();

                //disconnect the recordset

                oRecordset.ActiveConnection = null;
                oCommand = null;
                oConnection = null;

                if (lRetval < 0)
                {
                    if (lRetval == -1)
                        bLogError = false;
                    else
                    {
                        bLogError = true;
                        throw new Exception(string.Concat(Convert.ToString(lRetval), sStoredProcName, sMsgTxt));
                    }
                }

                //ToDo
                
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return oRecordset;
        }

        public string LookupDealers(string sXML, string sStylesheet = "")
        {
            //Search Parameters
            string sDealerCode = string.Empty,
                sZipCode = string.Empty,
                sDealerName = string.Empty,
                sState = string.Empty,
                sCity = string.Empty,
                sAccountType = string.Empty,
                sUserID = string.Empty,
                sUserSessionID = string.Empty;
            int iAreaCode,
                iExchangeNo,
                iHouseholdNo,
                iPageNo,
                sDealerParentCode=0;
            DOMDocument40 objDocument40 = null;

            //XML
            XmlDocument oXMLIn = null,
                oXMLResults = null,
                oXMLOut = null;
            XmlElement oRoot = null,
                oNodeElement = null;
            XmlNode oNode = null;
            XmlNodeList oNodeList = null;

            //Ingres
            string sIngres = string.Empty,
             sTempTable = string.Empty,
             sStoredProcedure = string.Empty,
             sStoredProc = string.Empty,
             sInsertSQL = string.Empty,
             sFilter = string.Empty;

            Recordset oOutputRs = null;
            string sOutput = string.Empty,
             sColumnList = string.Empty,
             strParam = string.Empty,
             sValueList = string.Empty;

            //Page Display Variables
            int iCounter=0,
              iEndPageRecord=0,
              iPageNumber=0,
              iNumberOfPages=0,
              iStartPageRecord=0,
              iNumberOfRecords=0;

            try
            {
#if (Developer ==true)
                ObjectControl_Activate();      
#endif
                //Load the input xml
                lynxAGC2.modAGC2Common mobjAGC2Common = new lynxAGC2.modAGC2Common();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                oXMLIn = new XmlDocument();
                oXMLIn.LoadXml(sXML);
                objDocument40 = (DOMDocument40)oXMLIn;
                //Get the node values
                strParam = "DealerCode";
                sDealerCode = mobjAGC2Common.getNodeValue(ref objDocument40, ref strParam).ToUpper();
                strParam = "AreaCode";
                iAreaCode = Convert.ToInt32(getNodeValue(oXMLIn,strParam));
                iExchangeNo = Val(getNodeValue(oXMLIn, "Exchange"));
                iHouseholdNo = Val(getNodeValue(oXMLIn, "HouseholdNo"));
                sZipCode = getNodeValue(oXMLIn, "ZipCode");
                sDealerName = getNodeValue(oXMLIn, "Name").ToUpper();
                sCity = getNodeValue(oXMLIn, "City").ToUpper();
                sState = getNodeValue(oXMLIn, "State").ToUpper();
                sAccountType = getNodeValue(oXMLIn, "AccountType");
                iPageNo = Val(getNodeValue(oXMLIn, "PageNo"));
                sUserID = getNodeValue(oXMLIn, "UserID");
                sUserSessionID = getNodeValue(oXMLIn, "UserSessionID");

                //Establish Ingres call
                sTempTable = "so_dealers_lookup";
                sStoredProc = "dp_so_dealers_lookup";

                if (sAccountType == "10" || sAccountType == "11" || sAccountType == "12" || sAccountType == "CA")
                    sIngres = sIngresDSNCA;
                else
                    sIngres = sIngresDSN;

                sFilter = " 1=1 Order by dealer_nm";

                //    Ingres Parameter       Input Parameter
                //--------------------------------------------
                //dealer_addr_1st
                //dealer_area_code_no    iAreaCode
                //called_from
                //dealer_city_nm         sCity
                //dealer_cd              sDealerCode
                //dealer_nm              sDealerName
                //dealer_exchange_no     iExchangeNo
                //dealer_household_no    iHouseholdNo
                //dealer_state_cd        sState
                //dealer_zip_cd          sZipCode
                //ship_to_no
                //commit_ind
                //msg_ind
                //msg_text
                //user_id_no             sUserID
                //user_session_no        sUserSessionID
                //ins_co_uid_no
                // ---------------------------------------------

                sColumnList = string.Concat("dealer_addr_1st, dealer_area_code_no, called_from, dealer_city_nm, dealer_cd, ",
                                             "dealer_nm, dealer_exchange_no, dealer_household_no, dealer_state_cd, dealer_zip_cd, ship_to_no, ins_co_uid_no  ");


                sValueList = string.Concat(" '', ", iAreaCode, ", '', '", sCity, "', '", sDealerCode, "', '",
                                             sDealerName, "', ", iExchangeNo, ", ", iHouseholdNo, ", '", sState, "', '", sZipCode, "', '', ", sDealerParentCode);
                //sDealerName & "', " & iExchangeNo & ", " & iHouseholdNo & ", '" & sState & "', '" & sZipCode & "', '', '" & sDealerParentCode & "' "

                //Build SQL Statement
                sInsertSQL = mobjDataAccess.BuildInsertSQL(sTempTable, sColumnList, sValueList, sUserID, sUserSessionID);

                //Call the database procedure
                oOutputRs = mobjDataAccess.GetSessionRows2(sTempTable, sStoredProc, sInsertSQL, sIngres, sFilter);

                //Convert the recordset to an xml doc
                if (oOutputRs.EOF)
                    sOutput = "<LookupDealers/>";
                else
                    sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "record", "LookupDealers");

                oXMLResults = new XmlDocument();
                oXMLResults.LoadXml(sOutput);


                //Apply the filters on the Main XML
                oNodeList = oXMLResults.SelectNodes("//record"); // & sFilter???
                if (oNodeList != null)
                {
                    iNumberOfRecords = oNodeList.Count;
                    if (iNumberOfRecords < 11)
                        iNumberOfPages = 1;
                    else
                        iNumberOfPages = Convert.ToInt32(Math.Round((iNumberOfRecords / 10) + 0.49));
                }
                else
                {
                    iNumberOfPages = 1;
                }

                //If there is an error, number of records should be zero
                if (iNumberOfRecords > 0)
                {
                    if (oXMLResults.SelectSingleNode("//record[1]/@msg_text").Value != null)
                    {
                        if (oXMLResults.SelectSingleNode("/record[1]/@msg_text").InnerText != "SUCCESS" && oXMLResults.SelectSingleNode("/record[1]/@msg_text").InnerText != "")
                            iNumberOfRecords = 0;
                    }
                }

                oNodeList = null;
                //Begin to Prepare the Results XML to be returned after all chnges are applied
                oXMLOut = new XmlDocument();
                oRoot = (XmlElement)oXMLOut.AppendChild(oXMLOut.CreateNode(XmlNodeType.Element, "LookupDealers", ""));

                //Get only one page of records, applying sort on these would be easier
                if (iPageNumber > 0)
                    oNodeList = oXMLResults.SelectNodes(string.Concat(new string[] { "//record[position() >=", Convert.ToString(iStartPageRecord), " and position() <= ", Convert.ToString(iEndPageRecord), "]" }));
                else
                {
                    //If the PageNumber has been explicitly passed in as 0
                    //It means that the application is seeking all records without any max limit and no paging is desired
                    //This is currently implemented for the BOA application
                    oNodeList = oXMLResults.SelectNodes("//record");
                }

                if (oNodeList != null)
                {
                    for (iCounter = 0; iCounter < oNodeList.Count - 1; iCounter++)
                    {
                        oNode = oNodeList.Item(iCounter);
                        oRoot.AppendChild(oNode);
                        oNode = null;
                    }

                }

                oNode = oXMLOut.CreateNode(XmlNodeType.Element, "PageNo", "");
                oNode.InnerText = Convert.ToString(iPageNumber);
                oRoot.AppendChild(oNode);
                oNode = null;
                oNode = oXMLOut.CreateNode(XmlNodeType.Element, "TotalPages", "");
                oNode.InnerText = Convert.ToString(iNumberOfPages);
                oRoot.AppendChild(oNode);
                oNode = null;

                oNode = oXMLOut.CreateNode(XmlNodeType.Element, "TotalRecords", "");
                oNode.InnerText = Convert.ToString(iNumberOfRecords);
                oRoot.AppendChild(oNode);
                oNode = null;

                oRoot = null;
                oXMLResults = null;
                oXMLIn = null;

                sOutput = oXMLOut.OuterXml;
                oXMLOut = null;

                if (sStylesheetPath != "" && sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet));

                oOutputRs.Close();
                oOutputRs = null;

            }
            catch (Exception)
            {
                throw;
            }
            return sOutput;

        }

        public string LookupRetailers(string sXML, string sStylesheet = "", string sUserType = "")
        {
            //Search Parameters
            string sCity = string.Empty,
                sContactType = string.Empty,
                sFedTaxID = string.Empty,
                sFilter = string.Empty,
                sInsertSQL = string.Empty,
                sOutput = string.Empty,
                sShopName = string.Empty,
                sSortBy = string.Empty,
                sSortOrder = string.Empty,
                sState = string.Empty,
                sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sZip = string.Empty,
                sTechnicianFirstName = string.Empty,
                sTechnicianLastName = string.Empty,
                sVIN = string.Empty,
                sDriversLicense = string.Empty,
                sDriversLicenseState = string.Empty,
                sAsServiceArea = string.Empty,
                sMsgText = string.Empty;

            long lInsCoUidNo, lProgramNumber, lInstallerID;
            int iAreaCode,
                iExchange,
                iHousehold,
                iStatus,
                iSSN;

            //11/06/07 - Add UserID and UserSessionID
            string sUserID = string.Empty,
             sUserSessionID = string.Empty;

            bool bFiltersChanged = false;

            //Page Display Variables
            int iCounter,
              iEndPageRecord,
              iPageNumber,
              iNumberOfPages,
              iStartPageRecord,
              iNumberOfRecords=0;

            //XML
            XmlElement oChildNode = null,
                oCurrentFilter = null,
                oFilters = null,
                oNodeElement = null,
                oPaging = null,
                oRoot = null;
            XmlDocument oXMLIn = null,
                oXMLResults = null,
                oXMLOut = null;
            XmlNode oNode = null;
            XmlNodeList oNodeList = null,
                oFiltersNodeList = null,
                oStatesNodeList = null;

            Recordset oOutputRs = null;
            try
            {
#if (Developer ==true)
                ObjectControl_Activate();      
#endif
                //Load the input xml
                oXMLIn = new XmlDocument();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLIn.LoadXml(sXML);

                if (getNodeValue(oXMLIn, "StateFarmLookup").ToUpper() == "Y")
                {
                    return StateFarmIDLookup(Convert.ToInt64(Val(getNodeValue(oXMLIn, "StateFarmID"))));
                }


                //Grab the parameters for the dp call
                sShopName = getNodeValue(oXMLIn, "ShopName").Replace("'", "''").ToUpper();
                sFedTaxID = getNodeValue(oXMLIn, "FedTaxID");
                if (sFedTaxID == "")
                    sFedTaxID = "0";
                sCity = getNodeValue(oXMLIn, "City").Replace("'", "''").ToUpper();
                sState = getNodeValue(oXMLIn, "State").ToUpper();
                lInstallerID = Val(getNodeValue(oXMLIn, "InstallerID"));
                sZip = (getNodeValue(oXMLIn, "ZipCode"));
                iAreaCode = Val(getNodeValue(oXMLIn, "AreaCode"));
                iExchange = Val(getNodeValue(oXMLIn, "Exchange"));
                iHousehold = Val(getNodeValue(oXMLIn, "Household"));
                iStatus = Val(getNodeValue(oXMLIn, "Status"));

                iSSN = Val(getNodeValue(oXMLIn, "SSN"));
                sTechnicianFirstName = getNodeValue(oXMLIn, "TechnicianFirstName").ToUpper();
                sTechnicianLastName = getNodeValue(oXMLIn, "TechnicianLastName").ToUpper();
                sVIN = getNodeValue(oXMLIn, "VIN");
                sDriversLicense = Right((getNodeValue(oXMLIn, "DriversLicense")).Trim(), 4);
                sDriversLicenseState = getNodeValue(oXMLIn, "DriversLicenseState");
                if (getNodeValue(oXMLIn, "AsServiceArea").ToUpper() == "ON")
                    sAsServiceArea = "Y";
                else
                    sAsServiceArea = "N";

                lInsCoUidNo = Val(getNodeValue(oXMLIn, "InsCoUidNo"));
                sMsgText = getNodeValue(oXMLIn, "MsgText");
                lProgramNumber = Val(getNodeValue(oXMLIn, "ProgramNumber"));

                //11/06/07 - Add UserId and UserSessionID
                sUserID = getNodeValue(oXMLIn, "user_id_no");
                sUserSessionID = getNodeValue(oXMLIn, "user_session_no");

                //Get a complete list of all distinct States
                oStatesNodeList = oXMLIn.SelectNodes("//record[not(@state_cd=preceding-sibling::*/attribute::state_cd)]");

                //Get the Sorting parameters
                if (oXMLIn.SelectSingleNode("//SortBy") != null)
                    sSortBy = oXMLIn.SelectSingleNode("//SortBy").InnerText;

                if (oXMLIn.SelectSingleNode("//SortOrder") != null)
                    sSortOrder = oXMLIn.SelectSingleNode("//SortOrder").InnerText;

                if (sSortBy == "")
                    sSortBy = "inst_nm";

                sFilter = string.Concat(" 1=1 Order by ", sSortBy, " ", sSortOrder);

                //Check to see if any filters were changed.
                oFilters = (XmlElement)oXMLIn.SelectSingleNode("//Filters");

                if (oFilters == null)
                    bFiltersChanged = false;
                else
                    bFiltersChanged = Convert.ToBoolean(oFilters.GetAttribute("changed"));

                //Get the page number
                if (oXMLIn.SelectSingleNode("//PageNo") == null)
                    iPageNumber = 1;
                else
                {
                    if (bFiltersChanged)
                    {
                        //If any filters were changed then we need to re-paginate
                        iPageNumber = 1;
                    }
                    else
                        iPageNumber = Convert.ToInt32(oXMLIn.SelectSingleNode("//PageNo").InnerText);
                }

                //Calculate the Start and End Record based on the page number
                //As of now, we have to display 10 records per page
                //We can store the No.Of records per page in the lynxEnvironments.xml file
                //as a configuration parameter for this component.
                //However, the no. of records per page is also used in the xsl stylesheet
                //so, we would need to add that as an xml element in the xml which will be styled
                iStartPageRecord = iPageNumber * 10 - 9;
                iEndPageRecord = iStartPageRecord + 9;

                //STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_retailers_lookup";

                //STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_so_retailers_lookup";

                //STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                //11/6/07 - Add UserID and UserSessionID
                sInsertSQL = mobjDataAccess.BuildInsertSQL(sTempTable, string.Concat("service_area_ind,inst_nm,federal_tax_id,city_nm,state_cd,vendor_type_cd,inst_uid_no,zip_cd, ",
                  "area_code_no,exchange_no,household_no,inst_stat_ind, ",
                  "soc_sec_suffix_no, techn_first_nm, techn_last_nm, vin_no, driver_license_no, driver_license_state_cd, ins_co_uid_no, msg_text, lynx_pgm_no, mbr_ind ",
                  "'", sAsServiceArea, "','", sShopName, "',", sFedTaxID, ",'", sCity, "','", sState, "'", ",'G',", lInstallerID, ",'", sZip, "',",
                  iAreaCode, ",", iExchange, ",", iHousehold, ",", iStatus, ",",
                  iSSN, ",'", sTechnicianFirstName, "','", sTechnicianLastName, "','",
                  sVIN, "','", sDriversLicense, "','", sDriversLicenseState, "', ", lInsCoUidNo, ",'", sMsgText, "', ", lProgramNumber, ", ''", sUserID, sUserSessionID),"");

                string sTempIngres = string.Empty;

                if (sUserType == "10" || sUserType == "11" || sUserType == "12" || sUserType == "CA")
                    sTempIngres = sIngresDSNCA;
                else
                    sTempIngres = sIngresDSN;

                //Call the database procedure
                oOutputRs = mobjDataAccess.GetSessionRows2(sTempTable, sStoredProc,
                             sInsertSQL, sTempIngres, sFilter);

                //Convert the recordset to an xml doc
                if (oOutputRs.EOF)
                    sOutput = "<LookupRetailers/>";
                else
                    sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "record", "LookupRetailers");

                oXMLResults = new XmlDocument();
                oXMLResults.LoadXml(sOutput);


                //Apply the filters on the Main XML
                oNodeList = oXMLResults.SelectNodes("//record"); // & sFilter???
                if (oNodeList != null)
                {
                    iNumberOfRecords = oNodeList.Count;
                    if (iNumberOfRecords < 11)
                        iNumberOfPages = 1;
                    else
                        iNumberOfPages = Convert.ToInt32(Math.Round((iNumberOfRecords / 10) + 0.49));
                }
                else
                {
                    iNumberOfPages = 1;
                }

                //If there is an error, number of records should be zero
                if (iNumberOfRecords > 0)
                {
                    if (oXMLResults.SelectSingleNode("//record[1]/@msg_text").Value != null)
                    {
                        if (oXMLResults.SelectSingleNode("/record[1]/@msg_text").InnerText != "SUCCESS" && oXMLResults.SelectSingleNode("/record[1]/@msg_text").InnerText != "")
                            iNumberOfRecords = 0;
                    }
                }

                oNodeList = null;
                //Begin to Prepare the Results XML to be returned after all chnges are applied
                oXMLOut = new XmlDocument();
                oRoot = (XmlElement)oXMLOut.AppendChild(oXMLOut.CreateNode(XmlNodeType.Element, "LookupDealers", ""));

                //Get only one page of records, applying sort on these would be easier
                if (iPageNumber > 0)
                    oNodeList = oXMLResults.SelectNodes(string.Concat(new string[] { "//record[position() >=", Convert.ToString(iStartPageRecord), " and position() <= ", Convert.ToString(iEndPageRecord), "]" }));
                else
                {
                    //If the PageNumber has been explicitly passed in as 0
                    //It means that the application is seeking all records without any max limit and no paging is desired
                    //This is currently implemented for the BOA application
                    oNodeList = oXMLResults.SelectNodes("//record");
                }

                if (oNodeList != null)
                {
                    for (iCounter = 0; iCounter < oNodeList.Count - 1; iCounter++)
                    {
                        oNode = oNodeList.Item(iCounter);
                        oRoot.AppendChild(oNode);
                        oNode = null;
                    }

                }

                oNode = oXMLOut.CreateNode(XmlNodeType.Element, "PageNo", "");
                oNode.InnerText = Convert.ToString(iPageNumber);
                oRoot.AppendChild(oNode);
                oNode = null;
                oNode = oXMLOut.CreateNode(XmlNodeType.Element, "TotalPages", "");
                oNode.InnerText = Convert.ToString(iNumberOfPages);
                oRoot.AppendChild(oNode);
                oNode = null;

                oNode = oXMLOut.CreateNode(XmlNodeType.Element, "TotalRecords", "");
                oNode.InnerText = Convert.ToString(iNumberOfRecords);
                oRoot.AppendChild(oNode);
                oNode = null;

                oNode = oXMLOut.CreateNode(XmlNodeType.Element, "AppliedFilters", "");
                oRoot.AppendChild(oNode);
                if (oFiltersNodeList == null)
                {
                    for (iCounter = 0; iCounter < oFiltersNodeList.Count - 1; iCounter++)
                    {
                        oNode.AppendChild(oFiltersNodeList.Item(iCounter));
                    }
                }
                oNode = null;

                oNode = oXMLOut.CreateNode(XmlNodeType.Element, "StateList", "");
                oRoot.AppendChild(oNode);
                if (oStatesNodeList != null && oStatesNodeList.Count > 0)
                    for (iCounter = 0; iCounter < oStatesNodeList.Count - 1; iCounter++)
                    {
                        oChildNode =(XmlElement)oXMLOut.CreateNode(XmlNodeType.Element, "State", "");
                        oNodeElement = (XmlElement)oStatesNodeList.Item(iCounter);
                        oChildNode.InnerText = oNodeElement.GetAttribute("state_cd");
                        oNodeElement = null;
                        oNode.AppendChild(oChildNode);
                        oChildNode = null;
                    }

                oNode = null;
                oStatesNodeList = null;

                oFiltersNodeList = null;

                oRoot = null;
                oXMLResults = null;
                oXMLIn = null;

                if (sStylesheetPath != "" && sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet));

                oOutputRs.Close();
                oOutputRs = null;

            }
            catch (Exception)
            {
                throw;
            }
            return sOutput;

        }

        public string SearchRetailers(string sXML, string sStylesheet = "")
        {
            string sOutput = string.Empty,
                sClaimTypeCd = string.Empty,
                sWorkZipCd = string.Empty,
                sMbrInd = string.Empty,
                sMsgInd = string.Empty,
                sMsgText = string.Empty,
                sUserIDNo = string.Empty,
                sUserSessionIDNo = string.Empty,
                s24HourServiceInd = string.Empty,
                sSearchType = string.Empty,
                sCity = string.Empty,
                sState = string.Empty,
                sInstallerName = string.Empty,

                sTempTable = string.Empty,
                sStoredProc = string.Empty,
                sSelectSQL = string.Empty,
                sInsertSQL = string.Empty,
                sFilter = string.Empty,

                sPriorState = string.Empty,
                sCurrentState = string.Empty,
                sHoldXML = string.Empty,
                sParentID = string.Empty,
                sRetailerID = string.Empty,
                sUserType = string.Empty;

            long lInsRegUIDNo,
            lMiles,
            lLynxPgmNo,
            lInstallerUidNo,
            lInsCompanyUIdNo,
            lInstallerParentUidNo,
            lAreaCode,
            lExchange,
            lHousehold,

            lRetval=0;

            int iMaxRecords;


            //XML
            XmlDocument oXMLInput = null,
                oXMLDom = null,
                oStateXML = null;
            XmlElement oComponentNode = null,
                oCurrentNode = null;
            XmlNodeList oNodeList = null;

            Recordset oOutputRs = null;

            bool bFirstTime = false;

            try
            {
#if (Developer ==true)
                ObjectControl_Activate();      
#endif
                //Load the input xml
                oXMLInput = new XmlDocument();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                oXMLInput.LoadXml(sXML);

                //Get params from the input XML
                sClaimTypeCd = Convert.ToString(Val(getNodeValue(oXMLInput, "claim_type_cd")));
                lInsRegUIDNo = Val(getNodeValue(oXMLInput, "ins_reg_uid_no"));
                sWorkZipCd = getNodeValue(oXMLInput, "work_zip_cd");
                sMbrInd = getNodeValue(oXMLInput, "mbr_ind");
                lMiles = Val(getNodeValue(oXMLInput, "miles"));
                lLynxPgmNo = Val(getNodeValue(oXMLInput, "lynx_pgm_no"));
                sMsgInd = getNodeValue(oXMLInput, "msg_ind");
                sMsgText = getNodeValue(oXMLInput, "msg_text");
                sUserIDNo = getNodeValue(oXMLInput, "user_id_no");
                sUserSessionIDNo = getNodeValue(oXMLInput, "user_session_no");
                iMaxRecords = Val(getNodeValue(oXMLInput, "maxRecords"));
                lInstallerUidNo = Val(getNodeValue(oXMLInput, "inst_uid_no"));
                lInsCompanyUIdNo = Val(getNodeValue(oXMLInput, "ins_co_uid_no"));
                lInstallerParentUidNo = Val(getNodeValue(oXMLInput, "parent_uid_no"));
                s24HourServiceInd = getNodeValue(oXMLInput, "h24_hour_service_ind");
                sSearchType = getNodeValue(oXMLInput, "inst_search_type");
                lAreaCode = Val(getNodeValue(oXMLInput, "area_code_no"));
                lExchange = Val(getNodeValue(oXMLInput, "exchange_no"));
                lHousehold = Val(getNodeValue(oXMLInput, "household_no"));
                sCity = getNodeValue(oXMLInput, "city_nm");
                sState = getNodeValue(oXMLInput, "state_cd");
                sInstallerName = getNodeValue(oXMLInput, "inst_nm");
                sUserType = getNodeValue(oXMLInput, "sUserType");


                sCity = sCity.Replace("'", "''");
                sInstallerName = sInstallerName.Replace("'", "''");

                //STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_retailer_search";

                //STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_retailer_search";

                //STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Concat("insert into session.so_retailer_search (h24_hour_service_ind, area_code_no, exchange_no, claim_type_cd, ins_co_uid_no, ins_reg_uid_no, work_zip_cd, inst_search_type, inst_uid_no, mbr_ind, miles, lynx_pgm_no, parent_uid_no, household_no, inst_nm, city_nm, state_cd, msg_ind, msg_text, user_id_no, user_session_no)",
                                " values ('", s24HourServiceInd, "', ", lAreaCode, ", ", lExchange, ", ", sClaimTypeCd, ", ", lInsCompanyUIdNo, ", ", lInsRegUIDNo, ", '", sWorkZipCd, "', '", sSearchType, "' ,", lInstallerUidNo, " , '", sMbrInd, "' , ", lMiles, ", ", lLynxPgmNo, ", ", lInstallerParentUidNo, ", ", lHousehold, ", '", sInstallerName, "', '", sCity, "', '", sState, "', '", sMsgInd, "', '", sMsgText, "','", sUserIDNo, "',  '", sUserSessionIDNo, "')");


                //Decide which database to use depending on the UserType (country).

                string sTempIngres = string.Empty;
                if (sUserType == "CA")
                    sTempIngres = sIngresDSNCA;
                else
                    sTempIngres = sIngresDSN;


                sSelectSQL = string.Concat("addr_1st, addr_2nd, alias_nm, city_nm, area_code_no, claim_type_cd, co_state_cd, co_insert_dscd_ind, company_nm, ",
                          "dba_nm, discount_cd, dispatch_method_cd, display_record, distance, driver_view_ind, capacity, contact_first_nm, ",
                          "contact_last_nm, control_no, exchange_no, fax_area_code_no, fax_exchange_no, fax_household_no, fax_question_dt, ",
                          "h24_hour_service_ind, household_no, ins_co_cd, ins_co_uid_no, ins_reg_uid_no, insrd_county_type_cd, inst_count, ",
                          "inst_county_type_cd, inst_uid_no, inst_nm, inst_search_type, inst_time, inst_hhmm, inst_rpr_rate, inst_temp_pct, ",
                          "inst_ws_pct, lat_long_precision_cd, latitude, local_time, local_time_desc, longitude, lynx_mbr_ind, lynx_pgm_no, mbr_cd, mbr_ind, miles, mobile_ind, ",
                          "ncr_county_type_cd, ncr_mbr_ind, ncr_inst_search_ind, op_hrs_area_code_no, op_hrs_exchange_no, op_hrs_household_no, ",
                          "open_sat_begin_tm, open_sat_end_tm, open_sun_begin_tm, open_sun_end_tm, open_wkday_begin_tm, open_wkday_end_tm, ",
                          "parent_uid_no, powersync_ind, power_sync_ind, ppg_prostars_mbr_ind, ppg_select_mbr_ind, repair_only_ind, rotate_days, rotate_dt, ",
                          "service_ind, service_type_id, state_cd, std_rpr_rate, std_temp_pct, std_ws_pct, tf_area_code_no, tf_exchange_no, ",
                          "tf_household_no, tf_natl_ind, tiered_sort_priority, user_id_no, user_session_no, web_http_addr, work_zip_cd, ",
                          "ws_repair_ind, ws_repair_partner_tier, zip_cd, msg_ind, msg_text, ");

                sSelectSQL = string.Concat(sSelectSQL, "int1(charextract('10100', (", sClaimTypeCd, ")))*sort_1_ws   + "
                              , "int1(charextract('01001',(", sClaimTypeCd, ")))*sort_1_temp + "
                              , "int1(charextract('00010',(", sClaimTypeCd, ")))*sort_1_rpr "
                              , "as sort_1,  sort_2 As sort_2");



                if (lInsCompanyUIdNo == 241) //CAS Job
                    sFilter = " order by state_cd ";
                else if (sUserSessionIDNo == "CLAIMPOINT" && sInstallerName != "")
                    sFilter = " order by inst_nm asc ";
                else
                    sFilter = " order by sort_1 asc, sort_2 asc, rotate_dt asc ";

                //STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = GetRetailerSearchResults(sTempTable, sStoredProc, sInsertSQL, sTempIngres, lLynxPgmNo, lRetval, sFilter, sSelectSQL, iMaxRecords);


                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "", "SearchRetailers");

                if (lInsCompanyUIdNo == 241) //CAS Job
                {
                    oXMLDom = new XmlDocument();
                    //oXMLDom.async = false
                    oXMLDom.LoadXml(sOutput);
                    oCurrentNode = (XmlElement)oXMLDom.SelectNodes("//SearchRetailers").Item(0); // .selectSingleNode("//Environment[Machine[@name='" & sMachineName & "']]")

                    oNodeList = oCurrentNode.ChildNodes;
                    bFirstTime = true;

                    foreach (XmlNode oNode in oNodeList)
                    {
                        sCurrentState = oNode.Attributes.GetNamedItem("state_cd").InnerText;
                        sParentID = oNode.Attributes.GetNamedItem("parent_uid_no").InnerText;
                        sRetailerID = oNode.Attributes.GetNamedItem("inst_uid_no").InnerText;

                        if (oNode.Name == "record")
                        {
                            if (bFirstTime)
                            {
                                sPriorState = string.Empty;
                                sHoldXML = "<statsxml>";
                                bFirstTime = false;
                            }
                            else
                            {
                                bFirstTime = false;
                            }

                            if (sPriorState != sCurrentState)
                            {
                                oStateXML = new XmlDocument();
                                oStateXML.LoadXml(string.Concat(sCASCacheFilePath, sCurrentState, ".xml"));

                            }
                            sPriorState = sCurrentState;

                            sHoldXML = string.Concat(sHoldXML, "<CompanyData ParentID=", sParentID, ">");
                            sHoldXML = string.Concat(sHoldXML, oStateXML.SelectSingleNode(string.Concat("//RetailCompany[@RetailerID='", sParentID, "']/RetailCompanyTable")).OuterXml);
                            sHoldXML = string.Concat(sHoldXML, oStateXML.SelectSingleNode(string.Concat("//RetailCompany[@RetailerID='", sParentID, "']/ServiceCenterTable/ServiceCenter[@RetailerID='", sRetailerID, "']")).OuterXml);
                            sHoldXML = string.Concat(sHoldXML, "</CompanyData>");
                        }
                    }
                    if (sHoldXML != "")
                        sHoldXML = string.Concat(sHoldXML, "</statsxml>");
                    else
                        sHoldXML = "<statsxml/>";

                    sOutput = string.Concat("<rs>", sOutput, sHoldXML, "</rs>");
                }


            }
            catch (Exception)
            {
                throw;
            }
            return sOutput;

        }

        private Recordset GetRetailerSearchResults(string sSessionTableName, string sStoredProcName, string sInsertSQL, string sConnectionString, long iLynxPgmNo, long lRetval, string sFilter = "", string sOutputColumns = "*", int iMaxRecords = 0)
        {
            string sSQL = string.Empty,
                sOutput = string.Empty,
                sMsgTxt = string.Empty;
            Connection oConnection = null;
            Command oCommand = null;
            Recordset oRecordset = null;
            bool bLogError = false;
            object objExecute;
            try
            {
                bLogError = true;
                //setup the connection
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(sConnectionString);

                //create the session table
                sSQL = string.Concat("declare global temporary table session.", sSessionTableName, " as ",
                            "select * from ", sSessionTableName, " on commit preserve rows with norecovery");

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);

                //populate the session table
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sInsertSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);

                //execute the stored procedure
                if (sStoredProcName != "dp_retailer_search")
                {
                    //Execute the DB Procedure passing the Session Table as the Parameter
                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = string.Concat("{ ? = call lynxdba.", sStoredProcName, "(dummy=session.", sSessionTableName, ") }");
                    oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                    oCommand.CommandTimeout = 90;
                    oCommand.Execute(out objExecute);
                    lRetval = Convert.ToInt64(oCommand.Parameters["retval"].Value);
                }
                else if (sStoredProcName == "dp_retailer_search")
                {
                    //Execute the DB Procedure passing the Session Table as the Parameter
                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = string.Concat("{ ? = call lynxdba.", "dp_retailer_search_a", "(dummy=session.", sSessionTableName, ") }");
                    oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                    oCommand.CommandTimeout = 90;
                    oCommand.Execute(out objExecute);
                    lRetval = Convert.ToInt64(oCommand.Parameters["retval"].Value);

                    // Check Return Value from the DB Procedure for Error
                    if (lRetval != 0)
                    {
                        if (lRetval > 0)
                            throw new Exception(string.Concat(Convert.ToString(lRetval), "dp_retailer_search_a", "Invalid Return Value ", Convert.ToString(lRetval), " from dp_retailer_search_a"));
                    }
                    else
                    {
                        //Execute the DB Procedure passing the Session Table as the Parameter
                        oCommand.CommandType = CommandTypeEnum.adCmdText;
                        oCommand.CommandText = string.Concat("{ ? = call lynxdba.", "dp_retailer_search_b", "(dummy=session.", sSessionTableName, ") }");
                        oCommand.CommandTimeout = 90;
                        oCommand.Execute(out objExecute);
                        lRetval = Convert.ToInt64(oCommand.Parameters["retval"].Value);

                        if (lRetval != 0)
                        {
                            if (lRetval > 0)
                                throw new Exception(string.Concat(Convert.ToString(lRetval), "dp_retailer_search_b", "Invalid Return Value ", Convert.ToString(lRetval), " from dp_retailer_search_b"));
                        }
                        else
                        {
                            //Execute the DB Procedure passing the Session Table as the Parameter
                            oCommand.CommandType = CommandTypeEnum.adCmdText;
                            oCommand.CommandText = string.Concat("{ ? = call lynxdba.", "dp_retailer_search_c", "(dummy=session.", sSessionTableName, ") }");
                            oCommand.CommandTimeout = 90;
                            oCommand.Execute(out objExecute);
                            lRetval = Convert.ToInt64(oCommand.Parameters["retval"].Value);

                            if (lRetval != 0)
                            {
                                if (lRetval > 0)
                                    throw new Exception(string.Concat(Convert.ToString(lRetval), "dp_retailer_search_c", "Invalid Return Value ", Convert.ToString(lRetval), " from dp_retailer_search_c"));
                            }
                        }
                    }

                }

                // setup the command to retrieve the resulting recordset
                sSQL = string.Concat("select ", sOutputColumns, " from session.", sSessionTableName);

                if (sFilter != string.Empty)
                    sSQL = string.Concat(sSQL, sFilter);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                //execute the query for readonly
                oRecordset.MaxRecords = iMaxRecords;
                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, oCommand.ActiveConnection, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);

                //drop the session table
                sSQL = string.Concat("drop table session.", sSessionTableName);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);
                oCommand.ActiveConnection.CommitTrans();

                //disconnect the recordset

                oRecordset.ActiveConnection = null;
                oCommand = null;
                oConnection = null;

                if (lRetval < 0)
                {
                    if (lRetval == -1)
                        bLogError = false;
                    else
                    {
                        bLogError = true;
                        throw new Exception(string.Concat(Convert.ToString(lRetval), sStoredProcName, sMsgTxt));
                    }
                }

                //ToDo


            }
            catch (Exception)
            {
                throw;
            }

            return oRecordset;
        }

        /// <summary>
        /// Gets the value of an xml node as a string.  returns "" if the node does not exist.
        /// </summary>
        /// <param name="xmlDom"></param>
        /// <param name="sNodeName"></param>
        /// <returns></returns>
        private string getNodeValue(XmlDocument xmlDom, string sNodeName)
        {
            try
            {
                if (xmlDom.SelectSingleNode(string.Concat("//", sNodeName)) == null)
                    return "";
                else
                    return xmlDom.SelectSingleNode(string.Concat("//", sNodeName)).InnerText;


            }
            catch (Exception)
            {
                throw;
            }
        }

        private string StateFarmIDLookup(long lID)
        {
            string sXmlPath = string.Empty,
                sResult = string.Empty;
            XmlDocument oXML = null;
            try
            {
                oXML = new XmlDocument();
                sXmlPath = getXMLPath();
                oXML.LoadXml(string.Concat(sXmlPath , "sf_retailer.xml"));

                if (oXML.SelectSingleNode(string.Concat("//StateFarmGlassVendor[@StateFarmGlassVendorID=", Convert.ToString(lID), "]")) != null)
                    sResult = oXML.SelectSingleNode(string.Concat("//StateFarmGlassVendor[@StateFarmGlassVendorID=", Convert.ToString(lID), "]")).OuterXml;
            }
            catch (Exception)
            {                
                throw;
            }

            return string.Concat("<result>" , sResult , "</result>");
        }

        /// <summary>
        /// Gets the path to lookup.xml
        /// </summary>
        /// <returns></returns>
        private string getXMLPath()
        {
            string sOutput = string.Empty;
            XmlDocument oXMLDom = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                sOutput = mobjCommon.GetConfiguration("lynxAGC2");
                oXMLDom = new XmlDocument();

                oXMLDom.LoadXml(sOutput);

                if (oXMLDom != null)
                {
                    return oXMLDom.SelectSingleNode("//Cache").InnerText;
                }
            }
            catch (Exception)
            {                
                throw;
            }
            return string.Empty;
            
        }

        public static int Val(string value)
        {
            String result = String.Empty;
            foreach (char c in value)
            {
                if (Char.IsNumber(c) || (c.Equals('.') && result.Count(x => x.Equals('.')) == 0))
                    result += c;
                else if (!c.Equals(' '))
                    return String.IsNullOrEmpty(result) ? 0 : Convert.ToInt32(result);
            }
            return String.IsNullOrEmpty(result) ? 0 : Convert.ToInt32(result);
        }

        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }
    }
}
