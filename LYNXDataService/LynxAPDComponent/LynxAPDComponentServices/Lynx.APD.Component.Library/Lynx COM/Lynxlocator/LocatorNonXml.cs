﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMSVCSLib;
using ADODB;
using MSXML2;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.Lynx_COM.Lynxlocator
{
    public class LocatorNonXml
    {
        Installer objinstaller = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;
        lynxAccount4.modCommon mobjCommon = null;
        private ObjectContext ctxObject = null;

        private string zMODULE = "lynxLocator.LocatorNonXml";
        private string zFILEPATH = @"SOFTWARE\LYNXServices\Components\lynxLocator";

        [DllImport("comsvcs.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern ObjectContext GetObjectContext();

        private void ObjectControl_Activate()
        {
            try
            {
                ctxObject = GetObjectContext();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        private void ObjectControl_Deactivate()
        {
            try
            {
                ctxObject = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public Recordset SearchInstallersRs(int iClaimTypeCd, long lInstallerUidNo, double dInsuredLatitude, double dInsuredLongitude,
                                             int iSearchRadius, int iLynxPgmNo, long lInstallerParentUidNo, string sMbrInd, string sIgnoreTimeInd,
                                             string s24HourServiceInd, long lInsCompanyUIdNo, long lInsRegionUIdNo, string sUserId,
                                             string sUserSessionNo, string sEnvId, string sStylesheet)
        {

            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sInsertSQL = string.Empty,
                    sSelectSql = string.Empty,
                    sOutput = string.Empty,
                    sFilter = string.Empty;
            int iRetval = 0;
            bool bLogError;
            long lErrorNumber; ;
            Recordset oOutputRs;

            try
            {
                objinstaller = new Installer();
                sTempTable = "so_inst_search";
                sStoredProc = "dp_inst_search";

                sInsertSQL = string.Format(@"insert into session.so_inst_search (claim_type_cd, inst_uid_no, insrd_latitude, insrd_longitude,  miles, lynx_pgm_no, ins_reg_uid_no, ins_co_uid_no, parent_uid_no, mbr_ind, ignore_time_ind, h24_hour_service_ind) 
                                    values ({0}, {1} ,{2},{3},{4},{5},{6}, {7}, {8} , '{9}', '{10}', '{11}')",
                                            iClaimTypeCd, lInstallerUidNo, dInsuredLatitude, dInsuredLongitude, iSearchRadius, iLynxPgmNo, lInsRegionUIdNo, lInsCompanyUIdNo, lInstallerParentUidNo, sMbrInd, sIgnoreTimeInd, s24HourServiceInd);

                sSelectSql = string.Format(@"inst_nm, addr_1st, addr_2nd, city_nm, state_cd, zip_cd, distance,
                                    area_code_no, exchange_no, household_no,fax_area_code_no, fax_exchange_no, fax_household_no, 
                                    mobile_repair_ind, mobile_radius, ws_repair_ind, rpo_ind, inst_uid_no, parent_uid_no, mbr_ind, 
                                    tf_area_code_no, tf_exchange_no, tf_household_no, tf_natl_ind, 
                                    county_fips, inst_county_type_cd, claim_type_cd, 
                                    local_time, varchar(inst_time) as inst_time, dispatch_method_cd, 
                                    open_sat_begin_tm, open_sat_end_tm, open_sun_begin_tm, open_sun_end_tm, 
                                    open_wkday_begin_tm, open_wkday_end_tm, 
                                    call_parent_ind, 
                                    discount_cd, contact_first_nm,  contact_last_nm, control_no, 
                                    h24_hour_service_ind, ignore_time_ind, ins_co_cd, ins_reg_uid_no, 
                                    insrd_latitude, insrd_longitude, inst_hhmm, inst_temp_pct, inst_ws_pct, 
                                    local_time_desc, mbr_cd, miles, ppg_prostars_conv_ind, 
                                    ppg_prostars_mbr_ind, rotate_dt, sf_county_type_cd, std_temp_pct, std_rpr_rate, 
                                    user_id_no, user_session_no, web_http_addr, 
                                    fax_question_dt, inst_rpr_rate, lynx_pgm_no, pos_cd, msg_text, 
                                    int1(charextract('10100', ({0})))*sort_1_ws   + 
                                    int1(charextract('01001',({1})))*sort_1_temp + 
                                    int1(charextract('00010',({2})))*sort_1_rpr 
                                    as sort_1,  sort_2 As sort_2",
                                             iClaimTypeCd, iClaimTypeCd, iClaimTypeCd);
                sFilter = " order by sort_1, sort_2";
                oOutputRs = GetInstSearchResults(sTempTable, sStoredProc, sInsertSQL, sEnvId, iRetval, sFilter, sSelectSql);

                if (iRetval != 0)
                {
                    bLogError = true;
                    throw new Exception(string.Concat(Convert.ToString(iRetval), sStoredProc, oOutputRs.Fields["msg_text"]));
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oOutputRs;
        }

        public Recordset LookupInstallersRs(long lInstallerUidNo, int iAreaCodeNo, int iExchangeNo, int iHouseholdNo, string sZipCode, string sName,
                                            string sCity, string sCounty, string sState, double dLatitude, double dLongitude, int iLynxPgmNo, string sMbrInd,
                                            int iLookupRadius, long lInsCompanyUIdNo, long lInsRegionUIdNo, string sUserId, string sUserSessionNo, string sEnvId, string sStylesheet = "")
        {


            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sInsertSQL = string.Empty,
                    sSelectSql = string.Empty,
                    sFilter = string.Empty,
                    sOutput = string.Empty;
            bool bLogError;
            long lErrorNumber;
            int iRetval;
            Recordset oOutputRs;
            try
            {

                iRetval = 0;
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();

                sTempTable = "so_inst_lookup";
                sStoredProc = "dp_so_inst_lookup";


                sInsertSQL = string.Format(@"insert into session.so_inst_lookup (inst_uid_no, area_code_no, exchange_no, household_no, zip_cd, inst_nm, city_nm, county_nm, state_cd,  lynx_pgm_no, insrd_latitude, insrd_longitude, ins_co_uid_no, ins_reg_uid_no, distance, user_session_no, user_id_no)
                                    values ({0}, {1}, {2},{3} , '{4}','{5}', '{6}',  '{7}', '{8}',  {9}, {10}, {11}, {12}, {13}, {14}, '{15}', '{16}')",
                                            lInstallerUidNo, iAreaCodeNo, iExchangeNo, iHouseholdNo, sZipCode, sName, sCity, sCounty, sState, iLynxPgmNo, dLatitude, dLongitude, lInsCompanyUIdNo, lInsRegionUIdNo, iLookupRadius, sUserSessionNo, sUserId);

                sSelectSql = @"inst_nm, addr_1st, addr_2nd, 
                    bt_addr_uid_no, bill_addr_1st, bill_addr_2nd, bill_to_nm,  
                    bill_city_nm, bill_state_cd, bill_zip_cd,  
                    city_nm, state_cd, zip_cd, distance,  
                    area_code_no, exchange_no, household_no,  
                    fax_area_code_no, fax_exchange_no, fax_household_no,  
                    mobile_repair_ind, mobile_radius, ws_repair_ind,  
                    rpo_ind, inst_uid_no, parent_uid_no,  
                    tf_area_code_no, tf_exchange_no,  
                    tf_household_no, tf_natl_ind,  
                    county_fips, inst_county_type_cd, local_time,  
                    sat_start_tm, sat_end_tm,  
                    wkday_start_tm, wkday_end_tm,  
                    miles, installer, mbp_ind, insrd_latitude, insrd_longitude,  
                    ins_co_uid_no, ins_reg_uid_no, lynx_pgm_no,  
                    max_latitude, max_longitude, mbr_ind, min_latitude, min_longitude,  
                    ppg_prostars_mbr_ind, call_parent_ind, sf_county_type_cd,  
                    pos_cd, inst_stat_ind, h_7_mile_distance, distance_bak, priority_no,  
                    contact_first_nm, contact_last_nm, fax_question_dt,  
                    rotate_dt, rotate_dt_bak, use_imbp, county_nm, msg_text, msg_ind,  
                    user_id_no, user_session_no";


                if (iLookupRadius > 0 && (!string.IsNullOrEmpty(sName)))
                {
                    if (sMbrInd.Equals("Y"))
                        sFilter = string.Format(" where mbr_ind = 'Y' and distance < {0} order by distance, inst_nm", iLookupRadius);
                    else if (sMbrInd.Equals("N"))
                        sFilter = string.Format(" where mbr_ind = 'N' and distance < {0} order by distance, inst_nm", iLookupRadius);
                    else
                        sFilter = string.Format(" where distance < {0} order by distance, inst_nm", iLookupRadius);
                }
                else
                {
                    if (sMbrInd.Equals("Y"))
                        sFilter = " where mbr_ind = 'Y' order by distance, inst_nm";
                    else if (sMbrInd.Equals("N"))
                        sFilter = " where mbr_ind = 'N' order by distance, inst_nm";
                    else
                        sFilter = " order by distance, inst_nm";
                }


                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sEnvId, sFilter, sSelectSql);

                if (iRetval != 0)
                {
                    bLogError = true;
                    throw new Exception(string.Concat(Convert.ToString(iRetval), sStoredProc, Convert.ToString(oOutputRs.Fields["msg_text"])));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return oOutputRs;
        }

        public string[,] ValidatePhoneAry(int iAreaCode, int iExchangeNo, string sUserID, string sUserSessionNo, string sStylesheet = "")
        {
            Command oCmd;
            Connection oConnection;
            string sStoredProc = string.Empty,
                   sOutput = string.Empty;
            int iCounter=0;
            int iParametersCount=0;
            string[,]Voutput = new string[2, 2];
            bool bLogError;
            object objExcute;

            //       #If (Developer = 1) Then
            //       'use the conditional compilation argument "Developer = 1"
            //       'when debugging this component from your desktop
            //Call ObjectControl_Activate
            //       #End If

            try
            {

                oCmd = new Command();
                oConnection = new Connection();
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                sStoredProc = "dp_validate_phone_no";
                //oConnection.ConnectionTimeout = 60;
                //oConnection.Open(lynxAccount4.modCommon.sIngresDSN);
                //oCmd.ActiveConnection = oConnection;
                //oCmd.CommandType = CommandTypeEnum.adCmdText;
                //oCmd.CommandText = string.Format ("{? = call lynxdba.{0}(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }",sStoredProc);
                //oCmd .CommandTimeout = 90;

                oCmd.Parameters.Append(oCmd.CreateParameter("retval", ADODB.DataTypeEnum.adInteger, ADODB.ParameterDirectionEnum.adParamReturnValue));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 1, "N"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 32, sUserID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_id", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInput, 32, sUserSessionNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_area_code_no", ADODB.DataTypeEnum.adInteger, ADODB.ParameterDirectionEnum.adParamInput, 0, iAreaCode));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_exchange_no", ADODB.DataTypeEnum.adInteger, ADODB.ParameterDirectionEnum.adParamInput, 0, iExchangeNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_phone_no", ADODB.DataTypeEnum.adDouble, ADODB.ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_city_state", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 20, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_city_nm", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 32, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_county_type", ADODB.DataTypeEnum.adChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_latitude", ADODB.DataTypeEnum.adDouble, ADODB.ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_longitude", ADODB.DataTypeEnum.adDouble, ADODB.ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_local_time", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 27, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_state_cd", ADODB.DataTypeEnum.adVarChar, ADODB.ParameterDirectionEnum.adParamInputOutput, 32, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_tz_offset", ADODB.DataTypeEnum.adTinyInt, ADODB.ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCmd.Execute(out objExcute);

                if (Convert.ToInt32(oCmd.Parameters["retval"]) != 0)
                {
                    bLogError = false;
                    throw new Exception(string.Concat(Convert.ToString(oCmd.Parameters["retval"]), Constants.vbObjectError, sStoredProc,oCmd.Parameters["h_msg"]));
                }

                foreach (Parameter oParam in oCmd.Parameters)
                {
                    Voutput[0, iCounter] = oParam.Name;
                    if (iCounter < iParametersCount)
                        iCounter = iCounter + 1;
                }

                iCounter = 0;

                foreach (Parameter oParam in oCmd.Parameters)
                {
                    Voutput[0, iCounter] = Convert.ToString(oParam.Value);
                    if (iCounter < iParametersCount)
                        iCounter = iCounter + 1;
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCmd = null;
            }
            return Voutput;
        }

        public string ValidateZip(string sZipCode, string sUserID, string sUserSessionNo, string sStylesheet = "")
        {
            Command oCmd;
            Connection oConnection;
            string sStoredProc = string.Empty,
                    sOutput = string.Empty;
            bool bLogError;
            object objExecute;

            //       #If (Developer = 1) Then
            //       'use the conditional compilation argument "Developer = 1"
            //       'when debugging this component from your desktop
            //Call ObjectControl_Activate
            //       #End If


            try
            {
                sStoredProc = "dp_validate_city_state_zip";
                oCmd = new Command();
                oConnection = new Connection();
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);

                oCmd.ActiveConnection = oConnection;
                oCmd.CommandType = CommandTypeEnum.adCmdText;
                oCmd.CommandText = string.Format("{? = call lynxdba.{0}(?,?,?,?,?,?,?,?,?,?) }", sStoredProc);
                oCmd.CommandTimeout = 90;

                oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_id", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserSessionNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_city_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_county_type", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_latitude", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_longitude", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_state_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 2, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_zip_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 9, sZipCode));
                oCmd.Execute(out objExecute);

                if (Convert.ToInt16(oCmd.Parameters["retval"]) != 0)
                {
                    bLogError = true;
                    throw new Exception(string.Concat(Convert.ToString(oCmd.Parameters["retval"]), sStoredProc, Convert.ToString(oCmd.Parameters["h_msg"])));
                }

                sOutput = mobjDataAccess.ParametersToXML(ref oCmd, true, "ValidateZip");


                if (!string.IsNullOrEmpty(sStylesheet))
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCmd = null;
            }
            return sOutput;

        }

        private Recordset GetInstSearchResults(string sSessionTableName, string sStoredProcName, string sInsertSQL, string sConnectionString,
                                               int lRetval, string sFilter = "", string sOutputColumns = "*")
        {

            string sSQL = string.Empty,
                    sOutput = string.Empty;

            Connection oConnection = new Connection();
            Command oCommand = new Command();
            Recordset oRecordset = new Recordset();
            bool bLogError = true;
            string sMsgTxt = string.Empty;
            object objExecute;
            try
            {
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(sConnectionString);
                sSQL = string.Format(@"declare global temporary table session.{0} as select * from {1} on commit preserve rows with norecovery",
                                      sSessionTableName, sSessionTableName);

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sInsertSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);

                if (sStoredProcName != "dp_inst_search")
                {
                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = string.Format("{ ? = call lynxdba.{0}(dummy=session.{1}) }", sStoredProcName, sSessionTableName);
                    oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                    oCommand.CommandTimeout = 90;
                    oCommand.Execute(out objExecute);
                    lRetval = Convert.ToInt32(oCommand.Parameters["retval"]);
                }
                else if (sStoredProcName.Equals("dp_inst_search"))
                {
                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = string.Format("{ ? = call lynxdba.dp_inst_search_a(dummy=session.{0}) }", sSessionTableName);
                    oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                    oCommand.CommandTimeout = 90;
                    oCommand.Execute(out objExecute);

                    lRetval = Convert.ToInt32(oCommand.Parameters["retval"]);

                    if (lRetval != 0)
                    {
                        throw new Exception(string.Concat(Convert.ToString(oCommand.Parameters["msg_text"])));
                    }
                    else
                    {
                        oCommand.CommandType = CommandTypeEnum.adCmdText;
                        oCommand.CommandText = string.Format("{ ? = call lynxdba.dp_inst_search_b(dummy=session.{0}) }", sSessionTableName);
                        oCommand.CommandTimeout = 90;
                        oCommand.Execute(out objExecute);
                        lRetval = Convert.ToInt32(oCommand.Parameters["retval"]);


                    }
                }
                sSQL = string.Format("select {0} from session.{1}", sOutputColumns, sSessionTableName);

                if (!string.IsNullOrEmpty(sFilter))
                    sSQL = string.Concat(sSQL, sFilter);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, Type.Missing, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);

                sSQL = string.Format("drop table session.{0}", sSessionTableName);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);
                oCommand.ActiveConnection.CommitTrans();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oRecordset.ActiveConnection = null;
                oCommand = null;
                oConnection = null;
            }
            return oRecordset;

        }

    }
}

