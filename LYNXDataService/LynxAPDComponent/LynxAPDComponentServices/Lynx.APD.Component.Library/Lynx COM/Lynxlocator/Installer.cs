﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ADODB;
using COMSVCSLib;
using System.Runtime.InteropServices;
namespace Lynx.APD.Component.Library.Lynx_COM.Lynxlocator
{
    public class Installer
    {
        ObjectContext ctxObject = null;
        private string zMODULE = "lynxLocator.Installer";
        private string zFILEPATH = @"SOFTWARE\LYNXServices\Components\lynxLocator";

        lynxAccount4.modCommon mobjCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;

        [DllImport("comsvcs.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern ObjectContext GetObjectContext();

        private void ObjectControl_Activate()
        {
            try
            {
                ctxObject = GetObjectContext();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return true;
        }

        private void ObjectControl_Deactivate()
        {
            try
            {
                ctxObject = null; 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public string SearchInstallers(int iClaimTypeCd, long lInstallerUidNo, double dInsuredLatitude, double dInsuredLongitude, int iSearchRadius,
                                       int iLynxPgmNo, int lInstallerParentUidNo, string sMbrInd, string sIgnoreTimeInd, string s24HourServiceInd, long lInsCompanyUIdNo,
                                       long lInsRegionUIdNo, string sUserId, string sUserSessionNo, string sEnvId, string sStylesheet = "")
        {
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sInsertSQL = string.Empty,
                    sSelectSql = string.Empty,
                    sOutput = string.Empty,
                    sFilter = string.Empty;
            int iRetval;
            bool bLogError;
            long lErrorNumber;
            Recordset oOutputRs = null;
            try
            {

                iRetval = 0;
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                sTempTable = "so_inst_search";
                sStoredProc = "dp_inst_search";

                sInsertSQL = string.Format(@"insert into session.so_inst_search (claim_type_cd, inst_uid_no, insrd_latitude, insrd_longitude,  
                                           miles, lynx_pgm_no, ins_reg_uid_no, ins_co_uid_no, parent_uid_no, mbr_ind, ignore_time_ind, h24_hour_service_ind) 
                                           values ({0},{1} ,{2},{3},{4},{5}, {6}, {7}, {8} , '{9}', '{10}', '{11}')",
                                            iClaimTypeCd, lInstallerUidNo, dInsuredLatitude, dInsuredLongitude, iSearchRadius, iLynxPgmNo, lInsRegionUIdNo, lInsCompanyUIdNo, lInstallerParentUidNo, sMbrInd, sIgnoreTimeInd, s24HourServiceInd);

                sSelectSql = string.Format(@"inst_nm, addr_1st, addr_2nd, city_nm, state_cd, zip_cd, distance,  
                                            area_code_no, exchange_no, household_no,fax_area_code_no, fax_exchange_no, fax_household_no,  
                                            mobile_repair_ind, mobile_radius, ws_repair_ind, rpo_ind, inst_uid_no, parent_uid_no, mbr_ind,  
                                            tf_area_code_no, tf_exchange_no, tf_household_no, tf_natl_ind,  
                                            county_fips, inst_county_type_cd, claim_type_cd,  
                                            local_time, varchar(inst_time) as inst_time, dispatch_method_cd,  
                                            open_sat_begin_tm, open_sat_end_tm, open_sun_begin_tm, open_sun_end_tm,  
                                            open_wkday_begin_tm, open_wkday_end_tm,  
                                            call_parent_ind,  
                                            discount_cd, contact_first_nm,  contact_last_nm, control_no,  
                                            h24_hour_service_ind, ignore_time_ind, ins_co_cd, ins_reg_uid_no,  
                                            insrd_latitude, insrd_longitude, inst_hhmm, inst_temp_pct, inst_ws_pct,  
                                            local_time_desc, mbr_cd, miles, ppg_prostars_conv_ind,  
                                            ppg_prostars_mbr_ind, rotate_dt, sf_county_type_cd, std_temp_pct, std_rpr_rate,  
                                            user_id_no, user_session_no, web_http_addr, ws_repair_ind,  
                                            fax_question_dt, inst_rpr_rate, lynx_pgm_no, pos_cd, msg_text,      
                                            int1(charextract('10100', ({0})))*sort_1_ws   +  
                                            int1(charextract('01001',({1})))*sort_1_temp   _
                                            int1(charextract('00010',({2})))*sort_1_rpr  
                                            as sort_1,  sort_2 As sort_2", iClaimTypeCd, iClaimTypeCd, iClaimTypeCd);

                sFilter = " order by sort_1, sort_2";

                // STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = GetInstSearchResults(sTempTable, sStoredProc, sInsertSQL, sEnvId, iRetval, sFilter, sSelectSql);

                if (iRetval != 0)
                {
                    bLogError = true;
                    throw new Exception(string.Concat(iRetval.ToString(), sStoredProc));//, sStoredProc, oOutputRs!msg_text);
                }

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "SearchInstallers");

                // STEP VI : Transform the XML using a XSL Stylesheet
                //if a Stylesheet is specified then we need to transform the XML using that XSL Stylesheet
                if (!string.IsNullOrEmpty(sStylesheet))
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(sStylesheet, zFILEPATH));
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return sOutput;

        }

        public string LookupInstallers(long lInstallerUidNo, int iAreaCodeNo, int iExchangeNo, int iHouseholdNo, string sZipCode,
                                       string sName, string sCity, string sCounty, string sState, double dLatitude, double dLongitude,
                                       int iLynxPgmNo, string sMbrInd, int iLookupRadius, long lInsCompanyUIdNo, long lInsRegionUIdNo,
                                       string sUserId, string sUserSessionNo, string sEnvId, string sStylesheet = "")
        {
            // STEP I : Declare Local Variables
            string sStoredProc = string.Empty,
            sTempTable = string.Empty,
            sInsertSQL = string.Empty,
            sSelectSql = string.Empty,
            sOutput = string.Empty,
            sFilter = string.Empty;
            int iRetval;
            bool bLogError;
            long lErrorNumber;
            Recordset oOutputRs;
            try
            {

                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                iRetval = 0;
                // STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_inst_lookup";

                // STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_so_inst_lookup";

                // STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Format(@"insert into session.so_inst_lookup (inst_uid_no, area_code_no, exchange_no, household_no, zip_cd, 
                                           inst_nm, city_nm, county_nm, state_cd,  lynx_pgm_no, insrd_latitude, insrd_longitude, ins_co_uid_no, 
                                           ins_reg_uid_no, distance, user_session_no, user_id_no) 
                                           values ({0}, {1}, {2} ,{3} , '{4}','{5}', '{6}',  '{7}', '{8}',  {9}, {10}, {11},{12}, {13}, {14}, '{15}', '{16}')",
                                          lInstallerUidNo, iAreaCodeNo, iExchangeNo, iHouseholdNo, sZipCode, sName, sCity, sCounty, sState, iLynxPgmNo, dLatitude, dLongitude, lInsCompanyUIdNo, lInsRegionUIdNo, iLookupRadius, sUserSessionNo, sUserId);
                // STEP III (d) : Define the Select Statement to retrieve records
                sSelectSql = @"inst_nm, addr_1st, addr_2nd,  
                                bt_addr_uid_no, bill_addr_1st, bill_addr_2nd, bill_to_nm,  
                                bill_city_nm, bill_state_cd, bill_zip_cd,  
                                city_nm, state_cd, zip_cd, distance,  
                                area_code_no, exchange_no, household_no,  
                                fax_area_code_no, fax_exchange_no, fax_household_no,  
                                mobile_repair_ind, mobile_radius, ws_repair_ind,  
                                rpo_ind, inst_uid_no, parent_uid_no,  
                                tf_area_code_no, tf_exchange_no,  
                                tf_household_no, tf_natl_ind,  
                                county_fips, inst_county_type_cd, local_time,  
                                sat_start_tm, sat_end_tm,  
                                wkday_start_tm, wkday_end_tm,  
                                miles, installer, mbp_ind, insrd_latitude, insrd_longitude,  
                                ins_co_uid_no, ins_reg_uid_no, lynx_pgm_no,  
                                max_latitude, max_longitude, mbr_ind, min_latitude, min_longitude,  
                                ppg_prostars_mbr_ind, call_parent_ind, sf_county_type_cd,  
                                pos_cd, inst_stat_ind, h_7_mile_distance, distance_bak, priority_no,  
                                contact_first_nm, contact_last_nm, fax_question_dt, 
                                rotate_dt, rotate_dt_bak, use_imbp, county_nm, msg_text, msg_ind, 
                                user_id_no, user_session_no";

                //STEP IV : Sort the Results set by Installer Name if the Lookup Radius is greater than zero
                //          Also If the Member Indicator is passed in then only return those Installers with that
                //          Member Indicator
                if (iLookupRadius > 0 && !string.IsNullOrEmpty(sName))
                {
                    if (sMbrInd.Equals("Y"))
                        sFilter = string.Format(" where mbr_ind = 'Y' and distance < {0} order by distance, inst_nm", iLookupRadius);
                    else if (sMbrInd.Equals("N"))
                        sFilter = string.Format(" where mbr_ind = 'N' and distance < {0} order by distance, inst_nm", iLookupRadius);
                    else
                        sFilter = string.Format(" where distance < {0} order by distance, inst_nm", iLookupRadius);
                }
                else
                {
                    if (sMbrInd.Equals("Y"))
                        sFilter = " where mbr_ind = 'Y' order by distance, inst_nm";
                    else if (sMbrInd.Equals("N"))
                        sFilter = " where mbr_ind = 'N' order by distance, inst_nm";
                    else
                        sFilter = " order by distance, inst_nm";
                }

                // STEP V : Depending on User Choice return either a Recordset or XML string
                //sOutput = GetRecordset(sTempTable, sInsertSQL, sStoredProc, sSelectSql, sEnvId, iRetval, True, "LookupInstallers")
                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sEnvId, sFilter, sSelectSql);

                if (iRetval != 0)
                {
                    bLogError = true;
                    throw new Exception(string.Concat(iRetval, sStoredProc));//, oOutputRs!msg_text
                }

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "LookupInstallers");

                //' STEP VI : Transform the XML using a XSL Stylesheet
                //'if a Stylesheet is specified then we need to transform the XML using that XSL Stylesheet
                if (!string.IsNullOrEmpty(sStylesheet))
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(sStylesheet, zFILEPATH));

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sOutput;
        }

        public Recordset SearchInstallersRs(int iClaimTypeCd, long lInstallerUidNo, double dInsuredLatitude, double dInsuredLongitude, int iSearchRadius,
                                            int iLynxPgmNo, long lInstallerParentUidNo, string sMbrInd, string sIgnoreTimeInd, string s24HourServiceInd,
                                            long lInsCompanyUIdNo, long lInsRegionUIdNo, string sUserId, string sUserSessionNo, string sEnvId, string sStylesheet = "")
        {
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sInsertSQL = string.Empty,
                    sSelectSql = string.Empty,
                    sFilter = string.Empty,
                    sOutput = string.Empty;
            bool bLogError;
            long lErrorNumber;
            int iRetval;
            Recordset oOutputRs;

            try
            {

                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                iRetval = 0;
                sTempTable = "so_inst_search";
                sStoredProc = "dp_inst_search";

                sInsertSQL = string.Format(@"insert into session.so_inst_search (claim_type_cd, inst_uid_no, insrd_latitude, insrd_longitude,  miles, lynx_pgm_no, ins_reg_uid_no, ins_co_uid_no, parent_uid_no, mbr_ind, ignore_time_ind, h24_hour_service_ind) 
                                           values ({0}, {1} ,{2},{3},{4},{5}, {6}, {7},{8} , '{9}', '{10}', '{11}')",
                                           iClaimTypeCd, lInstallerUidNo, dInsuredLatitude, dInsuredLongitude, iSearchRadius, iLynxPgmNo, lInsRegionUIdNo, lInsCompanyUIdNo, lInstallerParentUidNo, sMbrInd, sIgnoreTimeInd, s24HourServiceInd);


                sSelectSql = string.Format(@"inst_nm, addr_1st, addr_2nd, city_nm, state_cd, zip_cd, distance,  
                                           area_code_no, exchange_no, household_no,fax_area_code_no, fax_exchange_no, fax_household_no,  
                                           mobile_repair_ind, mobile_radius, ws_repair_ind, rpo_ind, inst_uid_no, parent_uid_no, mbr_ind, 
                                           tf_area_code_no, tf_exchange_no, tf_household_no, tf_natl_ind,  
                                           county_fips, inst_county_type_cd, claim_type_cd,  
                                           local_time, varchar(inst_time) as inst_time, dispatch_method_cd,  
                                           open_sat_begin_tm, open_sat_end_tm, open_sun_begin_tm, open_sun_end_tm, 
                                           open_wkday_begin_tm, open_wkday_end_tm,  
                                           call_parent_ind,  
                                           discount_cd, contact_first_nm,  contact_last_nm, control_no,  
                                           h24_hour_service_ind, ignore_time_ind, ins_co_cd, ins_reg_uid_no, 
                                           insrd_latitude, insrd_longitude, inst_hhmm, inst_temp_pct, inst_ws_pct,  
                                           local_time_desc, mbr_cd, miles, ppg_prostars_conv_ind,  
                                           ppg_prostars_mbr_ind, rotate_dt, sf_county_type_cd, std_temp_pct, std_rpr_rate,  
                                           user_id_no, user_session_no, web_http_addr, ws_repair_ind,  
                                           fax_question_dt, inst_rpr_rate, lynx_pgm_no, pos_cd, msg_text,   
                                           int1(charextract('10100', ({0})))*sort_1_ws   +  
                                           int1(charextract('01001',({1})))*sort_1_temp +  
                                           int1(charextract('00010',({2})))*sort_1_rpr 
                                           as sort_1,  sort_2 As sort_2",
                                           iClaimTypeCd, iClaimTypeCd, iClaimTypeCd);
                sFilter = " order by sort_1, sort_2";

                // STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = GetInstSearchResults(sTempTable, sStoredProc, sInsertSQL, sEnvId, iRetval, sFilter, sSelectSql);

                if (iRetval != 0)
                {
                    bLogError = true;
                    throw new Exception(string.Concat(iRetval, sStoredProc));//, oOutputRs!msg_text
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oOutputRs;

        }

        public Recordset LookupInstallersRs(long lInstallerUidNo, int iAreaCodeNo, int iExchangeNo, int iHouseholdNo, string sZipCode, string sName,
                                            string sCity, string sCounty, string sState, double dLatitude, double dLongitude, int iLynxPgmNo, string sMbrInd,
                                            int iLookupRadius, long lInsCompanyUIdNo, long lInsRegionUIdNo, string sUserId,
                                            string sUserSessionNo, string sEnvId, string sStylesheet = "")
        {

            mobjCommon = new lynxAccount4.modCommon();
            mobjDataAccess = new lynxAccount4.modDataAccess();
            //STEP I : Declare Local Variables
            string sStoredProc = string.Empty,
              sTempTable = string.Empty,
              sInsertSQL = string.Empty,
              sSelectSql = string.Empty,
              sOutput = string.Empty,
              sFilter = string.Empty;
            int iRetval;
            bool bLogError;
            long lErrorNumber;
            Recordset oOutputRs;

            try
            {
                iRetval = 0;
                // STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_inst_lookup";

                // STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_so_inst_lookup";

                // STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Format(@"insert into session.so_inst_lookup (inst_uid_no, area_code_no, exchange_no, household_no, zip_cd, inst_nm, city_nm, county_nm, state_cd,  lynx_pgm_no, insrd_latitude, insrd_longitude, ins_co_uid_no, ins_reg_uid_no, distance, user_session_no, user_id_no) 
                                           values ({0}, {1}, {2} ,{3} , '{4}','{5}', '{6}',  '{7}', '{8}',  {9}, {10}, {11},{12}, {13},{14}, '{15}', '{16}')",
                                           lInstallerUidNo, iAreaCodeNo, iExchangeNo, iHouseholdNo, sZipCode, sName, sCity, sCounty, sState, iLynxPgmNo, dLatitude, dLongitude, lInsCompanyUIdNo, lInsRegionUIdNo, iLookupRadius, sUserSessionNo, sUserId);


                // STEP III (d) : Define the Select Statement to retrieve records
                sSelectSql = @"inst_nm, addr_1st, addr_2nd,  
                                bt_addr_uid_no, bill_addr_1st, bill_addr_2nd, bill_to_nm,  
                                bill_city_nm, bill_state_cd, bill_zip_cd,  
                                city_nm, state_cd, zip_cd, distance,  
                                area_code_no, exchange_no, household_no,  
                                fax_area_code_no, fax_exchange_no, fax_household_no, 
                                mobile_repair_ind, mobile_radius, ws_repair_ind,  
                                rpo_ind, inst_uid_no, parent_uid_no,  
                                tf_area_code_no, tf_exchange_no,  
                                tf_household_no, tf_natl_ind,  
                                county_fips, inst_county_type_cd, local_time,  
                                sat_start_tm, sat_end_tm,  
                                wkday_start_tm, wkday_end_tm,  
                                miles, installer, mbp_ind, insrd_latitude, insrd_longitude,  
                                ins_co_uid_no, ins_reg_uid_no, lynx_pgm_no, 
                                max_latitude, max_longitude, mbr_ind, min_latitude, min_longitude,  
                                ppg_prostars_mbr_ind, call_parent_ind, sf_county_type_cd,  
                                pos_cd, inst_stat_ind, h_7_mile_distance, distance_bak, priority_no, 
                                contact_first_nm, contact_last_nm, fax_question_dt,  
                                rotate_dt, rotate_dt_bak, use_imbp, county_nm, msg_text, msg_ind, 
                                user_id_no, user_session_no";

                //' STEP IV : Sort the Results set by Installer Name if the Lookup Radius is greater than zero
                //'           Also If the Member Indicator is passed in then only return those Installers with that
                //'           Member Indicator
                if (iLookupRadius > 0 && !string.IsNullOrEmpty(sName))
                {
                    if (sMbrInd.Equals("Y"))
                        sFilter = string.Format(@" where mbr_ind = 'Y' and distance < {0} order by distance, inst_nm", iLookupRadius);
                    else if (sMbrInd.Equals("N"))
                        sFilter = string.Format(@" where mbr_ind = 'N' and distance < {0} order by distance, inst_nm", iLookupRadius);
                    else
                        sFilter = string.Format(@" where distance < {0} order by distance, inst_nm", iLookupRadius);
                }
                else
                {
                    if (sMbrInd.Equals("Y"))
                        sFilter = " where mbr_ind = 'Y' order by distance, inst_nm";
                    else if (sMbrInd.Equals("N"))
                        sFilter = " where mbr_ind = 'N' order by distance, inst_nm";
                    else
                        sFilter = " order by distance, inst_nm";

                }

                // ' STEP V : Depending on User Choice return either a Recordset or XML string
                //iRetval Todo
                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sEnvId, sFilter, sSelectSql);

                if (iRetval != 0)
                {
                    bLogError = true;
                    throw new Exception(string.Concat(iRetval, sStoredProc));//, oOutputRs!msg_text
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oOutputRs;

        }

        private Recordset GetInstSearchResults(string sSessionTableName, string sStoredProcName, string sInsertSQL, string sEnvId,
                                               int iRetval, string sFilter = "", string sOutputColumns = "*")
        {
            string sSQL = string.Empty,
            sOutput = string.Empty;
            object objExecute;
            object missing = Type.Missing;
            Connection oConnection = new ADODB.Connection();
            Command oCommand = new ADODB.Command();
            Recordset oRecordset = new ADODB.Recordset();
            try
            {

                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(mobjCommon.GetConnectionString(sEnvId));
                sSQL = string.Format(@"declare global temporary table session.{0} as    
                                     select * from {1} on commit preserve rows with norecovery",
                                      sSessionTableName, sSessionTableName);
                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sInsertSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);

                if (!sStoredProcName.Equals("dp_inst_search"))
                {

                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = string.Concat("{ ? = call lynxdba.", sStoredProcName, "(dummy=session.", sSessionTableName, ") }");
                    oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                    oCommand.CommandTimeout = 90;
                    oCommand.Execute(out objExecute);
                    iRetval = Convert.ToInt32(oCommand.Parameters["retval"]);
                }
                else if (sStoredProcName.Equals("dp_inst_search"))
                {

                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = string.Concat("{ ? = call lynxdba.", "dp_inst_search_a", "(dummy=session.", sSessionTableName, ") }");
                    oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                    oCommand.CommandTimeout = 90;
                    oCommand.Execute(out objExecute);
                    iRetval = Convert.ToInt32(oCommand.Parameters["retval"]);

                    if (iRetval != 0)
                        throw new Exception(Convert.ToString(oCommand.Parameters["msg_text"]));
                    else
                    {

                        oCommand.CommandType = CommandTypeEnum.adCmdText;
                        oCommand.CommandText = string.Format("{ ? = call lynxdba.dp_inst_search_b(dummy=session.{0}) }", sSessionTableName);
                        oCommand.CommandTimeout = 90;
                        oCommand.Execute(out objExecute);
                        iRetval = Convert.ToInt32(oCommand.Parameters["retval"]);
                    }
                }


                sSQL = string.Format("select {0} from session.{1}", sOutputColumns, sSessionTableName);

                if (!string.IsNullOrEmpty(sFilter))
                    sSQL = string.Concat(sSQL, sFilter);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, missing, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);
                sSQL = string.Concat("drop table session.", sSessionTableName);
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objExecute);
                oCommand.ActiveConnection.CommitTrans();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oRecordset.ActiveConnection = null;
                oCommand = null;
                oConnection = null;
            }
            return oRecordset;
            //CtxSetComplete
        }

    }
}
