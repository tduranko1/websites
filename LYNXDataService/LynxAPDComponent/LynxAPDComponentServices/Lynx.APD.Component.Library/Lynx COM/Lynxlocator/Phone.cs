﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMSVCSLib;
using ADODB;
using MSXML2;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.Lynx_COM.Lynxlocator
{
    class Phone
    {
        private ObjectContext ctxObject = null;
        private string zMODULE = "lynxLocator.Phone";
        private string zFILEPATH=@"SOFTWARE\LYNXServices\Components\lynxLocator";

        lynxAccount4.modDataAccess mobjDataAccess = null;

        [DllImport("comsvcs.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern ObjectContext GetObjectContext();

        private void ObjectControl_Activate()
        {
            try
            {
                ctxObject = GetObjectContext();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
          try 
	      {	        
		
	      }
	     catch (Exception ex)
	     {
		
		      throw ex;
	     }
            return true;
        }

        private void ObjectControl_Deactivate()
        {
           try 
	      {
              ctxObject = null;
	      }
	      catch (Exception ex)
	      {
		     throw ex;
	      }

        }

        public string ValidatePhone(int iAreaCode,int iExchangeNo,double dPhoneNo,string sUserId,string sUserSessionNo,string sEnvId,string sStylesheet="")
        {
            Command oCmd=null;
            Command oOutputCmd=null;
            string sStoredProc=string.Empty,
                   sOutput=string.Empty;
            bool bLogError;
            long lErrorNumber;
            try
            {
                sStoredProc = "dp_validate_phone_no";
                oCmd=new Command();
                mobjDataAccess=new lynxAccount4.modDataAccess();

                oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger,ParameterDirectionEnum.adParamReturnValue));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 32, sUserId));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_id", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 32, sUserSessionNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_area_code_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput,0, iAreaCode));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_exchange_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput,0 , iExchangeNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_phone_no", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput,0 , dPhoneNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_city_state", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 20, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_city_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_county_type", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_latitude", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInputOutput,0 , 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_longitude", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInputOutput,0 , 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_local_time", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 27, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_state_cd", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_tz_offset", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput,0 , 0));
                
                if (Convert.ToInt32(oCmd.Parameters["retval"].Value) != 0)
                {
                    bLogError = true;
                    throw new Exception(string.Concat(oCmd.Parameters["retval"].ToString() + Constants.vbObjectError, sStoredProc, oOutputCmd.Parameters["h_msg"]));
                }

                sOutput = mobjDataAccess.ParametersToXML(ref oOutputCmd,true, "ValidatePhone");

                if(!string.IsNullOrEmpty(sStylesheet))
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(sStylesheet, zFILEPATH));

            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCmd=null;
            }
            return sOutput;
        }

        public string[,] ValidatePhoneAry(int iAreaCode,int iExchangeNo,double dPhoneNo,string sUserId,string sUserSessionNo,string sEnvId,string sStylesheet="")
        {
          Command oCmd=null;
          Command oOutputCmd=null;
          string sStoredProc=string.Empty;
          string[,] vOutput=new string[1,2];
          bool bLogError;
          long lErrorNumber;
          int iParametersCount;
          int iCounter;
          
         try 
	     {	        
		    oCmd=new Command();

            oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 32, sUserId));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_id", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 32, sUserSessionNo));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_area_code_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput,0 , iAreaCode));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_exchange_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput,0 , iExchangeNo));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_phone_no", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput,0, dPhoneNo));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_city_state", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 20, ""));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_city_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, ""));
            oCmd.Parameters.Append(oCmd .CreateParameter("h_county_type", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, ""));
            oCmd.Parameters.Append(oCmd .CreateParameter("h_latitude", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInputOutput,0 , 0));
            oCmd.Parameters.Append(oCmd .CreateParameter("h_longitude", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInputOutput,0 , 0));
            oCmd.Parameters.Append(oCmd .CreateParameter("h_local_time", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 27, ""));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_state_cd", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, ""));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_tz_offset", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput,0 , 0));

            

            if (Convert.ToInt32(oCmd.Parameters["retval"].Value) != 0)
            {
                bLogError = true;
                throw new Exception(string.Concat(oCmd.Parameters["retval"].ToString()+Constants.vbObjectError,sStoredProc, oOutputCmd.Parameters["h_msg"]));
            }

             foreach(Parameter oParam in oOutputCmd.Parameters)
             {
                 vOutput[0,iCounter]=oParam.Name;
                 if(iCounter<iParametersCount)
                     iCounter=iCounter+1;
             }

             iCounter=0;

             foreach(Parameter oParam in oOutputCmd.Parameters)
             {
                 vOutput[0,iCounter]=Convert.ToString(oParam.Value);
                 if(iCounter<iParametersCount)
                     iCounter=iCounter+1;
             }
        
        }
	     catch (Exception ex)
	     {
		     throw ex;
	     }
         finally
         {
             oCmd=null;
         }
          return vOutput;
        }

    }
}
