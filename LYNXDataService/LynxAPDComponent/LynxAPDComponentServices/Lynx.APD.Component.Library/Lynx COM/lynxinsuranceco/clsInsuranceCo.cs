﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ADODB;
using MSXML2;
using COMSVCSLib;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxinsuranceco
{
    class clsInsuranceCo
    {
        private string APP_NAME = "lynxInsuranceCo.";
        private string MODULE_NAME = "clsInsuranceCo.";

        ObjectContext moObjectContext = null;
        lynxAccount4.modCommon mobjCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;
        Connection oConnection = null;
        Command oCommand = null;

        public string GetInsCoInfo(string strCommitInd, string strDNISSetupInd, string strDNIS800No, string strIgnoreActiveInd, string strInsCoCd,
                                   long lngInsCoUIDNo, long lngInsRegUIDNo, long lngInsPcUIDNo, string strInsrdStateCd, string strInsrdPhoneNo, string strUserIDNo,
                                   string strUserSessionNo, string strUserType = "")
        {
            string sTempIngresDSN = string.Empty;
            string stroutput = string.Empty;
            object objExecute = null;

            try
            {
                oConnection = new Connection();
                oCommand = new Command();
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();

                switch (strUserType)
                {
                    case "10":
                    case "11":
                    case "12":
                    case "CA":
                        sTempIngresDSN = lynxAccount4.modCommon.sIngresDSNCA;
                        break;
                    default:
                        sTempIngresDSN = lynxAccount4.modCommon.sIngresDSN;
                        break;
                }

                oConnection.CommandTimeout = 60;
                oConnection.Open(sTempIngresDSN);

                //
                oCommand.ActiveConnection = oConnection;
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = "{? = call lynxdba.dp_get_ins_co_info (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;
                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, strCommitInd));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, strUserIDNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, strUserSessionNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_dnis_setup_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, strDNISSetupInd));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_dnis_800_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, strDNIS800No));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ignore_active_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, strIgnoreActiveInd));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_co_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, strInsCoCd));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_co_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 35, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_co_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lngInsCoUIDNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_reg_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_reg_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 35, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_reg_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lngInsRegUIDNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_pc_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_pc_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 35, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_pc_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lngInsPcUIDNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_insrd_state_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 2, strInsrdStateCd));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_insrd_phone_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, strInsrdPhoneNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tries", DataTypeEnum.adTinyInt, ParameterDirectionEnum.adParamInputOutput, 0, 1)); //'Was zDB_RETRIES???
                oCommand.Execute(out objExecute);

                if (!string.IsNullOrEmpty(Convert.ToString(oCommand.Parameters["retval"])) && Convert.ToInt32(oCommand.Parameters["retval"]) != 0)
                    throw new Exception(string.Concat(Convert.ToString(lynxAccount4.modCommon.lynxDatabaseError + 800), "Data access error", Convert.ToString(oCommand.Parameters["h_msg_text"])));

                stroutput = mobjDataAccess.ParametersToXML(ref oCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCommand = null;
                oConnection = null;
            }

            return stroutput;
        }

        public string GetCauseOfLoss(long lngInsCoUIDNo, string sStylesheet)
        {
            Recordset oOutputRs = null;
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sFilter = string.Empty,
                   sOutput = string.Empty;

            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                sTempTable = "so_cause_of_loss";
                sStoredProc = "dp_get_so_cause_of_loss";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (ins_co_uid_no)", " values (", lngInsCoUIDNo, ")");
                sSelectSql = "*";

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);

                if (!string.IsNullOrEmpty(sStylesheet))
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs.Close();
            }

            return sOutput;
        }

        public string GetDataLynxMenuLinks(string sInputXML, string sStylesheet = "", string sResponseXml = "")
        {
            FreeThreadedDOMDocument40 oXMLIn = null;
            IXMLDOMNode oNode = null;
            Recordset oRS = null;
            IXMLDOMParseError tmpParseError = null;

            string sInsuranceCompanyID = string.Empty,
                   sOutputXml = string.Empty;

            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLIn = new FreeThreadedDOMDocument40();
                oConnection = new Connection();
                oCommand = new Command();
                oRS = new Recordset();

                oXMLIn.loadXML(sInputXML);

                if (oXMLIn.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLIn.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                oNode = oXMLIn.selectSingleNode("//InsuranceCompanyID");

                if (oNode != null)
                    sInsuranceCompanyID = oNode.text;

                oConnection.CommandTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sCairDSN);

                oCommand.ActiveConnection = oConnection;
                oCommand.CommandText = "dbo.GetDataLynxMenuLinks";
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.Parameters.Append(oCommand.CreateParameter("@InsuranceCompanyID", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, sInsuranceCompanyID));

                oRS.CursorLocation = CursorLocationEnum.adUseClient;
                oRS.Open(oCommand, Type.Missing, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);
                oRS.ActiveConnection = null;

                sOutputXml = mobjDataAccess.RsToXML(oRS, true);
                sResponseXml = sOutputXml;

                if (!string.IsNullOrEmpty(sStylesheet))
                    sOutputXml = mobjDataAccess.TransformXML(sOutputXml, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oNode = null;
                oRS = null;
                oConnection = null;
                oCommand = null;
                oXMLIn = null;
            }

            return sOutputXml;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tmpParseError = null;

            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom = new DOMDocument40();

                sOutput = mobjCommon.GetConfiguration("lynxInsuranceCo");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);

                if (oXMLDom.parseError.errorCode == 0)
                {
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sIngresDSNCA = oXMLDom.selectSingleNode("//DataSources/IngresCA").text;
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                }
                else
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        private void ObjectControl_Deactivate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }
    }
}
