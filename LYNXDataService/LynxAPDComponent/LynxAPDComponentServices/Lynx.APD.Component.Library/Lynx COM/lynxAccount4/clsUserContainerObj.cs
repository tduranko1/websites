﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMSVCSLib;
using ADODB;
using System.Runtime.InteropServices;
using MSXML2;
using System.Xml;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class clsUserContainerObj
    {
        #region Global Variable Declarations
        modDataAccess mobjDataAccess = null;
        modCommon mobjCommon = null;
        clsLevelOfAccess mobjLevelOfAccess = null;
        lynxProcessMapping.clsContainerObject mobjContainerObject = null;

        private ObjectContext moObjectContext = null;
        //Define the Application name to be used in Error Handling
        private const string APP_NAME = "lynxAccount4.";
        private const string MODULE_NAME = "clsUserContainerObject.";

        private string sXMLFileFolder = string.Empty;
        private string sXMLFileFolderCA = string.Empty;

        #endregion

        #region Public Functions

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(0, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(param.Length - length, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(startIndex, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(startIndex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        //    DATE CREATED        : Sep 2003
        //    AUTHOR              : John Walker (I&S IT)
        //    DESCRIPTION         :
        //    This function determines if a user has access to an object
        //PARAMETERS
        //    iUID                : The UID you want to look up.
        //    iOID                : The OID (Object ID) as created in the Process Mapping Tool
        //RETURNS                 :
        //Boolean
        //MODIFICATIONS

        public bool CheckSecurity(long iUID, long iOID)
        {
            string sSQL = string.Empty;
            string sOutput = string.Empty;
            bool bHoldSecurity = false;

            //create the ADO objects
            Connection oConnection = null;
            Command oCmd = null;
            object objtemp = null;
            try
            {

                oConnection = new Connection();
                oCmd = new Command();
                mobjCommon = new modCommon();

                oConnection.Open(modCommon.sCairDSN);


                oCmd.CommandText = "spSecurityCheck";
                oCmd.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCmd.ActiveConnection = oConnection;
                //.Parameters.Refresh
                oCmd.Parameters[0].Value = 0;
                oCmd.Parameters[1].Value = iUID;
                oCmd.Parameters[2].Value = iOID;
                oCmd.Execute(out objtemp);

                bHoldSecurity = Convert.ToBoolean(oCmd.Parameters[0].Value = null) ? false : Convert.ToBoolean(oCmd.Parameters[0].Value);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
                oCmd = null;
            }
            return bHoldSecurity;
        }


        // DATE CREATED        : Mar 2006
        // AUTHOR              : John Walker (I&S IT)
        // DESCRIPTION         :
        // This function gets a list of GPAs
        //PARAMETERS
        // sStyleSheet         : Stylesheet to use
        // sSingleGPAID        : Single GPA ID to narrow down selection
        //RETURNS                 :
        //XML or HTML Depending on Stylesheet
        //MODIFICATIONS

        public string GetGPAList([Optional] string sStylesheet, [Optional] string sSingleGPAID, [Optional] string sUserType)
        {
            DOMDocument40 oXMLDom = null;
            DOMDocument40 oXSLDoc = null;
            string sOutput = string.Empty;
            string sTempXMLFileFolder = string.Empty;
            IXMLDOMParseError tempDoc = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                oXMLDom = new DOMDocument40();
                oXSLDoc = new DOMDocument40();

                mobjCommon = new modCommon();
                switch (sUserType)
                {
                    case "10":
                    case "11":
                    case "12":
                    case "CA":
                        sTempXMLFileFolder = sXMLFileFolderCA;
                        break;
                    default:
                        sTempXMLFileFolder = sXMLFileFolder;
                        break;
                }
                oXMLDom.async = false;
                oXMLDom.load(string.Concat(sTempXMLFileFolder, "lookup_group.xml"));
                sOutput = oXMLDom.xml;

                oXMLDom.async = false;
                oXMLDom.load(string.Concat(sTempXMLFileFolder, "lookup.xml"));
                sOutput = string.Concat(sOutput, "<GPAID>", sSingleGPAID, "</GPAID>", oXMLDom.xml);
                tempDoc = oXSLDoc.parseError;
                tempDom = oXMLDom.parseError;

                if (sStylesheet != string.Empty)
                {
                    //=transform=and=style========================================
                    //parse the xml document
                    oXMLDom.async = false;
                    oXMLDom.loadXML(string.Concat("<rs>", sOutput, "</rs>"));
                    if (oXMLDom.parseError.errorCode == 0)
                    {
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));
                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //if no errors then transform the XML
                            //into HTML using the stylesheet
                            sOutput = oXMLDom.transformNode(oXSLDoc);
                        }
                        else
                            mobjCommon.InvalidXML(ref tempDoc);
                    }
                    else
                        mobjCommon.InvalidXML(ref tempDom);
                }
                //=transform=and=style========================================
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
            return sOutput;
        }

        // DATE CREATED        : Sep 2002

        // AUTHOR              : John Walker (I&S IT)
        // DESCRIPTION         :

        // This function gets the Level Of Access that each Account has for a
        // particular feature, as well as, the Level Of Access that is required
        // to have access to that feature
        //PARAMETERS
        // sTID                : The UID you want to look up.  "" will return all.
        //                          If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // sCOID               : The COID you want to look up.  "" will return all.
        //                          If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // sFields             : Lets you specify which fields to return from the table.  "" will return all.  Can also use *.
        // sStyleSheet         : Allows specification of style sheet to transform data, if no style sheet *.xsl is specified returns xml data set.
        // sEnvironmentID      : The environment ID.  Needed only for call to lynxProcessMapping.
        // bPaging             : Whether to page or not
        // iPage               : The page to return
        // iRows               : The number of rows to return/page
        // sLoggedInUser       : The user that is logged in.  If an Installer or Inco
        //                          we want to limit the security options that are returned.
        //RETURNS                 :

        //Recordset/XML

        //MODIFICATIONS

        public string GetUserAppAccess([Optional] string sUID, [Optional] string sCOID, [Optional] string sFields, [Optional] string sStylesheet, [Optional] string sEnvironmentID, [Optional] bool bPaging, [Optional] int iPage, [Optional] int iRows, [Optional] string sLoggedInUser, [Optional] bool bSelfMaintainable)
        {
            //Comma delimited list of container objects that this user has
            //access to
            string sContainers = string.Empty;
            string sOutput = string.Empty;
            int iTotalPages = 0;
            long lMaxLevel = 0;
            string sMaxLevel = string.Empty;
            string sTempXML = string.Empty;
            string sRelatInd = string.Empty;

            //xml objects
            DOMDocument40 oXMLDom = null;
            DOMDocument40 oXSLDoc = null;
            IXMLDOMParseError tempDoc = null;
            IXMLDOMParseError tempDom = null;

            try
            {

                mobjLevelOfAccess = new clsLevelOfAccess();
                mobjContainerObject = new lynxProcessMapping.clsContainerObject();
                oXMLDom = new DOMDocument40();
                oXSLDoc = new DOMDocument40();

                bPaging = false;
                iPage = 1;
                iRows = 10;
                sLoggedInUser = string.Empty;

                sRelatInd = string.Empty; ;
                sMaxLevel = string.Empty; ;

                sOutput = GetUserContainerObj(sUID, string.Empty, sCOID, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, bPaging, false, iPage, "COMenuDisplay, ULastName, UFirstName", false, iRows, "*", ref iTotalPages, string.Empty, string.Empty, bSelfMaintainable, string.Empty);
                oXMLDom.async = false;
                oXMLDom.loadXML(string.Concat("<rs>", sOutput, "</rs>"));
                tempDoc = oXSLDoc.parseError;
                tempDom = oXMLDom.parseError;
                if (oXMLDom.parseError.errorCode == 0)
                {
                    oXSLDoc.async = false;
                    oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, "ContainerObjectCommaDelimited.xsl"));
                    if (oXSLDoc.parseError.errorCode == 0)
                    {
                        //if no errors then transform the XML
                        //into HTML using the stylesheet
                        sContainers = oXMLDom.transformNode(oXSLDoc);
                    }
                    else
                        mobjCommon.InvalidXML(ref tempDoc);
                }
                else
                    mobjCommon.InvalidXML(ref tempDom);
                //Set oXmlDom = Nothing
                //Set oXSLDoc = Nothing

                sContainers = Right(sContainers, sContainers.Length - 1);

                //This is where we limit what options show for security access
                if (sLoggedInUser != string.Empty && sCOID != string.Empty)
                    sOutput = string.Concat(sOutput, GetUserContainerObj(sLoggedInUser, "", sCOID, "", "", "", "", "", "", "", "", true, false, iPage, "COName", true, iRows, "*", ref iTotalPages, string.Empty, string.Empty, false, string.Empty));

                sOutput = string.Concat(sOutput, mobjLevelOfAccess.GetLevelOfAccess("", "", "", "", "", "", false, 1, "LOA.LOALevel", false, 0));
                sOutput = string.Concat(sOutput, mobjContainerObject.GetContainerObject(sContainers, "", "", "", "", 0, "", "CO.COID, CO.LOAID", "", false, 1));

                sOutput = string.Concat("<rs1>", sOutput, "</rs1>");

                if (sStylesheet != string.Empty)
                {
                    //=transform=and=style========================================
                    //parse the xml document
                    oXMLDom.async = false;
                    oXMLDom.loadXML(sOutput);
                    if (oXMLDom.parseError.errorCode == 0)
                    {
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));
                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //if no errors then transform the XML
                            //into HTML using the stylesheet
                            sOutput = oXMLDom.transformNode(oXSLDoc);
                        }
                        else
                            mobjCommon.InvalidXML(ref tempDoc);
                    }
                }
                else
                    mobjCommon.InvalidXML(ref tempDom);

                //=transform=and=style========================================
                if (bPaging == true)
                    sOutput = string.Concat(sOutput, mobjCommon.Paging(Convert.ToInt32(iTotalPages), iPage, 0));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXSLDoc = null;
                oXMLDom = null;
                mobjLevelOfAccess = null;
                mobjContainerObject = null;
            }
            return sOutput;
        }


        //    DATE CREATED        : Sep 2002
        //    AUTHOR              : John Walker (I&S IT)
        //    DESCRIPTION         :
        //    This function gets Level Of Access, Responsibility and User Container Object
        //    records into one XML file and styles it out...combination of calling other
        //    methods
        //PARAMETERS
        //    sUID                : The UID you want to look up.  "" will return all.
        //                          If more than one, put in as (1, 2, 3) to be used with an IN clause.
        //    sCOID               : The COID you want to look up.  "" will return all.
        //                          If more than one, put in as (1, 2, 3) to be used with an IN clause.
        //    sFields             : Lets you specify which fields to return from the table.  "" will return all.  Can also use *.
        //    sUTID               : User Type ID passed in for new adds, so we know what
        //                            type of account was selected
        //    sStyleSheet         : Allows specification of style sheet to transform data, if no style sheet *.xsl is specified returns xml data set.
        //    sEnv                : The environment to use (Ingres, SQL Server, etc.)
        //    sOrganization       : The organization ID that the UID belongs to
        //RETURNS                 :
        //Recordset/XML
        //MODIFICATIONS

        public string GetUserAppDetails([Optional]string sUID, [Optional]string sCOID, [Optional]string sMaxLOALevel, [Optional]string sFields, [Optional]string sUTID, [Optional]string sStylesheet, [Optional]string sEnv, [Optional]string sOrganization, [Optional]string sOrganizationSub)
        {
            string sOutput = string.Empty;
            string sOrg = string.Empty;
            string sType = string.Empty;

            //xml objects
            DOMDocument40 oXMLDom = null;
            DOMDocument40 oXSLDoc = null;

            clsLevelOfAccess oLOA = null;
            clsResponsibilityType oRT = null;
            clsUserType oUT = null;
            clsUsr oUsr = null;
            lynxShop.Shop oShop = null;
            lynxInsuranceCo.clsInsuranceCo oInco = null;
            clsJobTitle oJT = null;

            IXMLDOMParseError tempDoc = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                oXMLDom = new DOMDocument40();
                oXSLDoc = new DOMDocument40();
                oLOA = new clsLevelOfAccess();
                oRT = new clsResponsibilityType();
                oUT = new clsUserType();
                oUsr = new clsUsr();
                oShop = new lynxShop.Shop();
                oInco = new lynxInsuranceCo.clsInsuranceCo();
                oJT = new clsJobTitle();
                mobjCommon = new modCommon();

                int tempvalue = 0;

                sOutput = GetUserContainerObj(sUID, "", sCOID, "", "", "", "", "", "", "", "", false, false, 1, string.Empty, false, 0, string.Empty, ref tempvalue, string.Empty, string.Empty, false, string.Empty);

                if (sOutput != string.Empty)
                {
                    oXMLDom.loadXML(string.Concat("<rs>", sOutput, "</rs>"));
                    sOrg = oXMLDom.selectSingleNode(string.Concat("//usercontainerobject[@COID=", Convert.ToString(modConstantsVariables.zLynxAppId), "]")).attributes.getNamedItem("UCOOrganizationID").text;
                    sType = oXMLDom.selectSingleNode(string.Concat("//usercontainerobject[@COID=", Convert.ToString(modConstantsVariables.zLynxAppId), "]")).attributes.getNamedItem("UTID").text;
                }
                else
                {
                    sOrg = sOrganization;
                    sType = sUTID;
                }

                if (sOrg == string.Empty)
                    sOrg = sOrganization;

                if ((sType == Convert.ToString(modConstantsVariables.zInstallerType)) || (sType == Convert.ToString(modConstantsVariables.zCAInstallerType))) //do installer
                {
                    sOutput = string.Concat(sOutput, oShop.GetShops("intranet", sOrg, sEnv, "", sUTID));
                    sOutput = string.Concat(sOutput, oJT.GetJobTitle("", "", "", sType, "", "", false, 0, false, 0));
                }
                else if ((sType == Convert.ToString(modConstantsVariables.zIncoType)) || (sType == Convert.ToString(modConstantsVariables.zCAIncoType))) //do inco
                    sOutput = string.Concat(sOutput, oInco.GetInsCoInfo("Y", "", "", "Y", "", Convert.ToInt64(sOrg), 0, 0, "", "", "lynxweb", "", sUTID));
                else if ((sType == Convert.ToString(modConstantsVariables.zGPAUserType)) || (sType == Convert.ToString(modConstantsVariables.zCAGPAUserType))) //do inco
                    sOutput = string.Concat(sOutput, GetGPAList("", sOrg, sUTID)); //GPA list
                else if (sType == Convert.ToString(modConstantsVariables.zDealerType)) //Dealership
                    sOutput = string.Concat(sOutput, oJT.GetJobTitle("", "", "", sType, "", "", false, 0, false, 0));
                else if (sType == Convert.ToString(modConstantsVariables.zPPGLynxType)) //'do lynx/ppg
                {
                }

                sOutput = string.Concat(sOutput, oUsr.GetUsr(sUID, "", "", "", "", "", "", "", "", false, 1, string.Empty, false, 0));
                sOutput = string.Concat(sOutput, oLOA.GetLevelOfAccess("", sMaxLOALevel, "<=", "", "", "", false, 1, "LOA.LOALevel", false, 0));
                sOutput = string.Concat(sOutput, oRT.GetResponsibilityType("", "", "", "", false, 0, string.Empty, false, 0));
                sOutput = string.Concat(sOutput, oUT.GetUserType(sUTID, "", "", "", "", "", false, 0, string.Empty, false, 0));
                sOutput = string.Concat("<rs holdorg= ", sOrg, " >", sOutput, "</rs>");

                tempDoc = oXSLDoc.parseError;
                tempDom = oXMLDom.parseError;
                if (sStylesheet != string.Empty)
                {
                    //=transform=and=style========================================
                    //parse the xml document
                    oXMLDom.async = false;
                    oXMLDom.loadXML(sOutput);
                    if (oXMLDom.parseError.errorCode == 0)
                    {
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));
                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //if no errors then transform the XML
                            //into HTML using the stylesheet
                            sOutput = oXMLDom.transformNode(oXSLDoc);
                        }
                        else
                            mobjCommon.InvalidXML(ref tempDoc);
                    }
                    else
                    {
                        mobjCommon.InvalidXML(ref tempDom);
                        //=transform=and=style========================================
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXSLDoc = null;
                oXMLDom = null;
                oLOA = null;
                oRT = null;
                oUT = null;
                oUsr = null;
                oShop = null;
                oInco = null;
            }
        }

        // DATE CREATED        : Sep 2002

        // AUTHOR              : John Walker (I&S IT)

        // DESCRIPTION         :

        // This function gets one/many UserContainerObject records

        //PARAMETERS
        // sTID                : The UID you want to look up.  "" will return all.
        //                       If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // sUEmail             : Part of UEmail you want to search on.  "" will return all.
        //                       Will be used as '%ssUEmail%'
        // sCOID               : The COID you want to look up.  "" will return all.
        //                       If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // sUCOOrganizationID  : The Organization ID (installer ID, inco ID) you want to look up.  "" will return all.
        // sUCOActive          : Indicator if the user is active or not for the particular app.  "" Will return all.
        // sRTID               : The RTID you want to look up.  "" will return all.
        //                       If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // sUTID               : The UTID you want to look up.  "" will return all.
        //                       If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // sLOAID              : The LOAID you want to look up.  "" will return all.
        //                          If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // sFields             : Lets you specify which fields to return from the table.  "" will return all.  Can also use *.
        // sStyleSheet         : Allows specification of style sheet to transform data, if no style sheet *.xsl is specified returns xml data set.
        // bPaging             : Tells whether paging information should be returned from the stored proc
        // bReturnPaging       : Tells whether we want to create html based on the paging info
        //                       from the stored proc
        // iPage               : Tells what page was picked
        // sOrderBy            : Tells what column(s) to sort by
        // bWellFormedXML      : Tells whether the XML should be well formed or not
        // iRows               : Tells how many rows to return per page
        // sOuterJoin          : Tells whether to do an outer join or not
        // iTotalPages         : Total # of pages based on the filter information
        // bOwnerOfficer       : Whether to filter on the Owner/Officer tag or not
        // sJTID               : The JTID you want to look up.  "" will return all.
        // If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // bSelfMaintainable   : This tells us if the COID to be returned is self maintainable.  If
        //                       blank, return all
        // sUCOOrganizationSubID: Use for Dealer Code

        //RETURNS                 :

        //Recordset/XML

        //MODIFICATIONS

        public string GetUserContainerObj([Optional] string sUID,
                                    [Optional] string sUEMail,
                                    [Optional] string sCOID,
                                    [Optional] string sUCOOrganizationID,
                                    [Optional] string sUCOOrganizationNotIn,
                                    [Optional] string sUCOActive,
                                    [Optional] string sRTID,
                                    [Optional] string sUTID,
                                    [Optional] string sLOAID,
                                    [Optional] string sFields,
                                    [Optional] string sStylesheet,
                                    [Optional] bool bPaging,
                                    [Optional] bool bReturnPaging,
                                    [Optional] int iPage,
                                    [Optional] string sOrderBy,
                                    [Optional] bool bWellFormedXML,
                                    [Optional] int iRows,
                                    [Optional] string sOuterJoin,
                                    [Optional] ref int iTotalPages,
                                    [Optional] string bOwnerOfficer,
                                    [Optional] string sJTID,
                                    [Optional] bool bSelfMaintainable,
                                    [Optional] string sUCOOrganizationSubID)
        {
            string sSQL = string.Empty;
            string sOutput = string.Empty;
            string sTotalPages = string.Empty;

            //xml objects
            DOMDocument40 oXMLDom = null;
            DOMDocument40 oXSLDoc = null;

            //create the ADO objects
            Connection oConnection = null;
            Command oCommand = null;
            Recordset oRecordset = null;

            IXMLDOMParseError tempDoc = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                bPaging = false;
                bReturnPaging = false;
                iPage = 1;
                sOrderBy = "ULastName, UFirstName";
                bWellFormedXML = true;
                iRows = 10;
                sOuterJoin = string.Empty;

                oConnection = new Connection();
                oCommand = new Command();
                oRecordset = new Recordset();
                mobjCommon = new modCommon();
                mobjDataAccess = new modDataAccess();

                oConnection.Open(modCommon.sCairDSN);

                oCommand.CommandText = "spUserContainerObjectGet";
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.ActiveConnection = oConnection;
                //.Parameters.Refresh
                oCommand.Parameters[0].Value = 0;
                oCommand.Parameters[1].Value = sUID;
                oCommand.Parameters[2].Value = sUEMail;
                oCommand.Parameters[3].Value = sCOID;
                oCommand.Parameters[4].Value = Convert.ToBoolean(sUCOOrganizationID = string.Empty) ? null : sUCOOrganizationID;
                oCommand.Parameters[5].Value = sUCOOrganizationNotIn;
                oCommand.Parameters[6].Value = sUCOActive;
                oCommand.Parameters[7].Value = sRTID;
                oCommand.Parameters[8].Value = sUTID;
                oCommand.Parameters[9].Value = sLOAID;
                oCommand.Parameters[10].Value = sFields;
                oCommand.Parameters[11].Value = Convert.ToBoolean(iPage = 0) ? 1 : iPage;
                oCommand.Parameters[12].Value = bPaging;
                oCommand.Parameters[13].Value = sOrderBy;
                oCommand.Parameters[14].Value = iRows;
                oCommand.Parameters[15].Value = sOuterJoin;
                oCommand.Parameters[16].Value = bOwnerOfficer;
                oCommand.Parameters[17].Value = sJTID;
                oCommand.Parameters[18].Value = bSelfMaintainable;
                oCommand.Parameters[19].Value = Convert.ToBoolean(sUCOOrganizationSubID = string.Empty) ? null : sUCOOrganizationSubID;

                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, string.Empty, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly);
                sTotalPages = Convert.ToString(Convert.ToBoolean(oCommand.Parameters[0].Value = null) ? 0 : oCommand.Parameters[0].Value);
                iTotalPages = Convert.ToInt32(sTotalPages);

                //process the recordset
                sOutput = mobjDataAccess.RsToXML(oRecordset, bWellFormedXML, "usercontainerobject", "uco");

                tempDoc = oXSLDoc.parseError;
                tempDom = oXMLDom.parseError;

                if (sStylesheet != string.Empty)
                {
                    //=transform=and=style========================================
                    //parse the xml document

                    oXMLDom.async = false;
                    oXMLDom.loadXML(sOutput);
                    if (oXMLDom.parseError.errorCode == 0)
                    {
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));
                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //if no errors then transform the XML
                            //into HTML using the stylesheet
                            sOutput = oXMLDom.transformNode(oXSLDoc);
                        }
                        else
                            mobjCommon.InvalidXML(ref tempDoc);
                    }
                    else
                        mobjCommon.InvalidXML(ref tempDoc);
                    //=transform=and=style========================================

                }
                if ((bPaging == true) && (bReturnPaging = true))
                    sOutput = string.Concat(sOutput, mobjCommon.Paging(Convert.ToInt32(sTotalPages), iPage, 0));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oRecordset = null;
                oConnection = null;
                oCommand = null;
                oXSLDoc = null;
                oXMLDom = null;
            }
            return sOutput;
        }

        // DATE CREATED        : September 2003

        // AUTHOR              : John Walker (I&S IT)

        // DESCRIPTION         :

        // This function inserts a UserContainerObject Record for a container object
        // that is defined as an application

        //PARAMETERS
        // UID                 : The Users ID
        // COID                : The container object (App) ID
        // UCOOrganizationID   : The ID of either the Installer/Ins Co on Ingres
        // UCOActive           : Active (T/F)
        // RTID                : User's Responsibility in this application
        // UTID                : The User Type ID
        // LOAID               : The User's default access at an application level
        // bOwnerOfficer       : Is this an owner officer?
        // JTID                : The JobTitle ID
        // UpdaterUID          : UID of the person doing the update, for logging
        // UCOOrganizationSubID: The dealership code

        //MODIFICATIONS

        public void InsertUserApplication(string UID,
                        string COID,
                        string UCOOrganizationID,
                        bool UCOActive,
                        string RTID,
                        string UTID,
                        string LOAID,
                        bool bOwnerOfficer,
                        [Optional] string JTID,
                        [Optional] string UpdaterUID,
                        [Optional] string UCOOrganizationSubID)
        {
            JTID = "0";

            string sSQL = string.Empty;
            string sXML = string.Empty;

            clsUsr mobjclsuser = null;
            Connection oConnection = null;
            Connection oConnection2 = null;
            Command oCommand2 = null;
            Recordset oRecordset = null;

            string sSessionTableName = string.Empty;
            string sStoredProcName = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            string sEmail = string.Empty;
            string sToday = string.Empty;
            bool bErrorFlag = false;
            DOMDocument40 oXMLDom = null;
            object objtemp = null;
            try
            {
                mobjclsuser = new clsUsr();
                oConnection = new Connection();
                oConnection2 = new Connection();
                oCommand2 = new Command();
                oRecordset = new Recordset();
                oXMLDom = new DOMDocument40();

                sSQL = string.Concat("EXEC spUserAppInsert ", UID, ", ", COID, ", '", UCOOrganizationID, "', ", Convert.ToString(UCOActive),
                          ", ", RTID, ", ", UTID, ", ", LOAID, ", ", Convert.ToString(bOwnerOfficer), ", ", JTID, ", '", UCOOrganizationSubID, "'");
                oConnection.Open(modCommon.sCairDSN);
                oConnection.Execute(sSQL, out objtemp);
                oConnection = null;
                //Get User specific info for the call to Ingres VUser
                sXML = mobjclsuser.GetUsr(UID, "", "", "", "", "", "", "", "", false, 0, string.Empty, false, 0);

                //Call out to an Ingres DP, if it does not work, just keep going
                oXMLDom.async = false;
                oXMLDom.loadXML(sXML);
                if (oXMLDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    sFirstName = Left(oXMLDom.selectSingleNode("//@UFirstName").text, 32);
                    sLastName = Left(oXMLDom.selectSingleNode("//@ULastName").text, 32);
                    sEmail = oXMLDom.selectSingleNode("//@UEMail").text;
                    sToday = DateTime.Now.ToString("DD-MMM-YYYY");
                }
                else
                    return; // Do nothing

                sSessionTableName = "so_web_manage_vusr";
                sStoredProcName = "dp_web_manage_vusr";

                oConnection2.ConnectionTimeout = 60;
                oConnection2.IsolationLevel = IsolationLevelEnum.adXactIsolated;
                oConnection2.Open(modCommon.sIngresDSN);

                //create the session table
                sSQL = string.Concat("declare global temporary table session.", sSessionTableName, " as ",
                "select * from ", sSessionTableName, " on commit preserve rows with norecovery");

                oCommand2.ActiveConnection = oConnection2;
                oCommand2.ActiveConnection.BeginTrans();
                oCommand2.CommandType = CommandTypeEnum.adCmdText;
                oCommand2.CommandText = sSQL;
                oCommand2.CommandTimeout = 90;
                oCommand2.Execute(out objtemp);

                if ((UCOOrganizationID == string.Empty) || (UCOOrganizationID == null) || (UCOOrganizationID == "NULL") || (UCOOrganizationID == "Null"))
                    UCOOrganizationID = "0";

                sSQL = string.Concat("insert into session.", sSessionTableName, " (",
                    "msg_ind, msg_text, run_user_id_no, user_session_no, web_user_id_no, user_first_nm, user_last_nm,            email_addr, active_no, web_user_type_no, begin_access_dt, end_access_dt, term_of_use_ind, term_of_use_dt, hide_faq_ind, hide_faq_dt, web_user_org_id) values (",
                    "'N',     '',       'lynxweb',      'lynxweb',     '", UID, "', '", sFirstName, "','", sLastName, "','", sEmail, "', 1, ", UTID, ",'", sToday, "','01-JAN-1900',             0,              '01-JAN-1900',   '0',            '01-JAN-1900', ", Convert.ToString(UCOOrganizationID), ")");

                oCommand2.CommandType = CommandTypeEnum.adCmdText;
                oCommand2.CommandText = sSQL;
                oCommand2.CommandTimeout = 90;
                oCommand2.Execute(out objtemp);

                //execute the stored procedure
                oCommand2.CommandType = CommandTypeEnum.adCmdText;
                oCommand2.CommandText = string.Concat("{ ? = call lynxdba.", sStoredProcName, "(dummy=session.", sSessionTableName, ") }");
                oCommand2.Parameters.Append(oCommand2.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand2.CommandTimeout = 90;
                oCommand2.Execute(out objtemp);

                if (oCommand2.Parameters["retval"] != null)
                    bErrorFlag = true; //fatal error

                //setup the command to retrieve the resulting recordset
                sSQL = string.Concat("select * from session.", sSessionTableName);

                oCommand2.CommandType = CommandTypeEnum.adCmdText;
                oCommand2.CommandText = sSQL;
                oCommand2.CommandTimeout = 90;

                //execute the query for readonly
                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand2, string.Empty, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);

                //drop the session table
                sSQL = string.Concat("drop table session.", sSessionTableName);

                oCommand2.CommandType = CommandTypeEnum.adCmdText;
                oCommand2.CommandText = sSQL;
                oCommand2.CommandTimeout = 90;
                oCommand2.Execute(out objtemp);
                oCommand2.ActiveConnection.CommitTrans();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCommand2 = null;
                oConnection = null;
                oRecordset = null;
                oConnection = null;
            }
        }


        public string Login([Optional] string sUEMail,
                        [Optional] string sCOID,
                        [Optional] string sFields,
                        [Optional] string sStylesheet,
                        [Optional] string sEnvironmentID,
                        [Optional] string sCairEnv,
                        [Optional] bool bMasked)
        {
            bMasked = false;

            //Comma delimited list of container objects that this user has
            //access to
            string sContainers = string.Empty;
            string sOutput = string.Empty;
            string sHoldOutput = string.Empty;
            string bLogError = string.Empty;
            string sAcctActive = string.Empty;
            string sLynxAcctActive = string.Empty;

            string sInstID = string.Empty;
            string sIncoID = string.Empty;
            string sUID = string.Empty;
            string sParentID = string.Empty;
            string sUTID = string.Empty;

            string sFedTaxID = string.Empty;
            string sShopOutput = string.Empty;
            string sRsts = string.Empty;
            string sHTML = string.Empty;

            long lErrorNumber = 0;
            string sErrorDescription = string.Empty;

            string sLogTime = string.Empty;
            //2/22/05 - JWW - Commented out logging times
            lynxAppLog.AppLog oAppLog = null;

            //xml objects
            DOMDocument40 oXMLDom = null;
            XmlDocument oXmlDom2 = null;
            DOMDocument40 oXSLDoc = null;

            string sOrgID = string.Empty;
            string sRequestXML = string.Empty;
            string sXrefOutput = string.Empty;
            string sFileName = string.Empty;
            string sGroupName = string.Empty;

            clsLevelOfAccess oLOA = null;
            clsUsr oUsr = null;
            lynxProcessMapping.clsContainerObject oCO = null;
            clsProfile oProfile = null;
            lynxInsuranceCo.clsInsuranceCo oInco = null;
            clsRegister oReg = null;
            lynxShop.Shop oShop = null;
            clsUserContainerObj oReturn = null;
            lynxLookups.Xml oLookups = null;

            IXMLDOMParseError tempDoc = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                oAppLog = new lynxAppLog.AppLog();
                oXMLDom = new DOMDocument40();
                oXmlDom2 = new XmlDocument();
                oXSLDoc = new DOMDocument40();
                oLOA = new clsLevelOfAccess();
                oUsr = new clsUsr();
                oCO = new lynxProcessMapping.clsContainerObject();
                oProfile = new clsProfile();
                oInco = new lynxInsuranceCo.clsInsuranceCo();
                oReg = new clsRegister();
                oShop = new lynxShop.Shop();
                oReturn = new clsUserContainerObj();
                oLookups = new lynxLookups.Xml();

                bLogError = Convert.ToString(true);
                //Log Timing
                sLogTime = "<rs>";
                sLogTime = string.Concat(sLogTime, "<rec name=\"SQL-GetUsr\">");
                sLogTime = string.Concat(sLogTime, "<detail type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");
                //Get the Usr information
                sOutput = oUsr.GetUsr("", "", sUEMail, "", "", "", "", "", "", false, 1, string.Empty, false, 0);
                sLogTime = string.Concat(sLogTime, "<detail type=\"end\" time=\"", DateTime.Now.ToString(), "\"/>");
                sLogTime = string.Concat(sLogTime, "</rec>");

                if (sOutput == string.Empty)
                {
                    bLogError = Convert.ToString(false);
                    throw new Exception(string.Concat(modConstantsVariables.zAccountNotExist, ",", MODULE_NAME, ",", "Error"));
                }
                oXmlDom2.LoadXml(string.Concat("<rs>", sOutput, "</rs>"));
                //Check if account is deactivated at the account level
                sAcctActive = oXmlDom2.GetElementsByTagName("user").Item(0).Attributes.GetNamedItem("UActive").InnerText;
                if (sAcctActive != "True")
                {
                    bLogError = Convert.ToString(false);
                    throw new Exception(string.Concat(modConstantsVariables.zAcctInactive, ",", MODULE_NAME, ",", "Error"));
                }
                sUID = oXmlDom2.GetElementsByTagName("user").Item(0).Attributes.GetNamedItem("UID").InnerText;

                //Get the features that this user has access to
                sLogTime = string.Concat(sLogTime, "<rec name=\"SQL-GetUserContainerObj\">");
                sLogTime = string.Concat(sLogTime, "<detail type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");
                int tempvalue = 0;
                sHoldOutput = GetUserContainerObj(sUID, "", "", "", "", "", "", "", "", "", "", false, false, 1, "COMenuDisplay", false, 10, "*", ref tempvalue, string.Empty, string.Empty, false, string.Empty);
                sLogTime = string.Concat(sLogTime, "<detail type=\"end\" time=\"", DateTime.Now.ToString(), "\"/>");
                sLogTime = string.Concat(sLogTime, "</rec>");
                if (sHoldOutput == string.Empty)
                {
                    bLogError = Convert.ToString(false);
                    throw new Exception(string.Concat(modConstantsVariables.zNoLynxRecord, ",", MODULE_NAME, ",", "Error"));
                }
                sOutput = string.Concat(sOutput, sHoldOutput);

                oXMLDom.loadXML(string.Concat("<rs>", sOutput, "</rs>"));

                //Check to see if account is deactivated for Lynx
                sLynxAcctActive = oXMLDom.selectSingleNode(string.Concat("//usercontainerobject[@COID=", Convert.ToString(modConstantsVariables.zLynxAppId), "]")).attributes.getNamedItem("UCOActive").text;
                if (sLynxAcctActive != "True")
                {
                    bLogError = Convert.ToString(false);
                    throw new Exception(string.Concat(modConstantsVariables.zLynxAcctInactive, ",", MODULE_NAME, ",", "Error"));

                }
                sUTID = oXMLDom.selectSingleNode(string.Concat("//usercontainerobject[@COID=", Convert.ToString(modConstantsVariables.zLynxAppId), "]")).attributes.getNamedItem("UTID").text;

                oXMLDom.async = false;

                tempDoc = oXSLDoc.parseError;
                tempDom = oXMLDom.parseError;
                if (oXMLDom.parseError.errorCode == 0)
                {
                    oXSLDoc.async = false;
                    oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, "ContainerObjectCommaDelimited.xsl"));
                    if (oXSLDoc.parseError.errorCode == 0)
                    {
                        //if no errors then transform the XML
                        //into HTML using the stylesheet
                        sContainers = oXMLDom.transformNode(oXSLDoc);
                    }
                    else
                        mobjCommon.InvalidXML(ref tempDoc);
                }
                else
                    mobjCommon.InvalidXML(ref tempDom);

                sContainers = Right(sContainers, sContainers.Length - 1);

                //Check to see if the user has any features to even check security for
                if (sContainers == string.Empty)
                {
                    bLogError = Convert.ToString(false);
                    throw new Exception(string.Concat(modConstantsVariables.zNoFeaturesExist, ",", MODULE_NAME, ",", "Error"));
                }

                if ((sUTID == Convert.ToString(modConstantsVariables.zInstallerType)) || (sUTID == Convert.ToString(modConstantsVariables.zCAInstallerType)))
                {
                    string sLynxPgmNo = string.Empty;
                    if (sUTID == "10")
                        sLynxPgmNo = "101";
                    else
                        sLynxPgmNo = "1";

                    sInstID = oXMLDom.selectSingleNode(string.Concat("//usercontainerobject[@COID=", Convert.ToString(modConstantsVariables.zLynxAppId), "]")).attributes.getNamedItem("UCOOrganizationID").text;
                    sOrgID = sInstID;
                    //Dim oInst As Object
                    //Set oInst = CreateObject("lynxLocator.Locator")

                    //Get installers Fed Tax ID
                    //sLogTime = sLogTime & "<rec name=""INGRES-GetInstallerInfo-dp_get_so_inst_info"">"
                    //sLogTime = sLogTime & "<detail type=""begin"" time=""" & Now() & """/>"

                    sLogTime = string.Concat("dp_get_so_inst_info;", DateTime.Now.ToString(), ";", DateTime.Now.ToString());
                    sOutput = string.Concat(sOutput, Convert.ToString(oReg.GetInstallerInfo(Convert.ToInt64(sInstID), 0, 0, Convert.ToInt64(sLynxPgmNo), sUID, sUEMail, sUTID)));
                    sLogTime = string.Concat(sLogTime, ";;;", DateTime.Now.ToString());
                    //Call LogToAppLog("stats", sLogTime, "")
                    sLogTime = string.Empty;
                    //sLogTime = sLogTime & "<detail type=""end"" time=""" & Now() & """/>"
                    //'sLogTime = sLogTime & "</rec>"
                    oXMLDom.loadXML(string.Concat("<rs>", sOutput, "</rs>"));

                    //TODO:oXMLDom.getElementsByTagName("inst")[0](need to verify)
                    sParentID = oXMLDom.getElementsByTagName("inst")[0].attributes.getNamedItem("parent_uid_no").text;
                    sFedTaxID = oXMLDom.getElementsByTagName("inst")[0].attributes.getNamedItem("federal_tax_id").text;

                    //Set oReg = Nothing

                    //        sLogTime = sLogTime & "<rec name=""INGRES-LookupInstaller-dp_so_inst_lookup"">"
                    //        sLogTime = sLogTime & "<detail type=""begin"" time=""" & Now() & """/>"
                    //        sOutput = sOutput & oInst.LookupInstallers(sInstID, 0, 0, 0, "", "", "", "", "", 0, 0, 1, "", 0, 0, 0, "internet", "")
                    //        sLogTime = sLogTime & "<detail type=""end"" time=""" & Now() & """/>"
                    //        sLogTime = sLogTime & "</rec>"
                    //        oXMLDom.loadXML ("<rs>" & sOutput & "</rs>")
                    //        sParentID = oXMLDom.getElementsByTagName("record").Item(0).Attributes.getNamedItem("parent_uid_no").Text
                    sLogTime = string.Concat(sLogTime, "<rec name=\"INGRES-GetProfileItems-dp_get_so_profile_item\">");
                    sLogTime = string.Concat(sLogTime, "<detail type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");
                    sOutput = string.Concat(sOutput, oProfile.GetProfileItems(string.Empty, Convert.ToInt64(sParentID), "inst", string.Empty, sUTID));
                    sLogTime = string.Concat(sLogTime, "<detail type=\"end\" time=\"", DateTime.Now.ToString(), "\"/>");
                    sLogTime = string.Concat(sLogTime, "</rec>");


                    //Use Fed Tax ID to get PIN
                    //Dim oReg2 As lynxAccount4.clsRegister
                    //Set oReg = CreateObject("lynxAccount4.clsRegister")
                    //sLogTime = sLogTime & "<rec name=""INGRES-FedTaxInfo-dp_get_fed_tax_id_status"">"
                    //sLogTime = sLogTime & "<detail type=""begin"" time=""" & Now() & """/>"
                    sLogTime = string.Concat("dp_get_fed_tax_id_status;", DateTime.Now.ToString(), ";", DateTime.Now.ToString());
                    sOutput = string.Concat(sOutput, oReg.FedTaxInfo(Convert.ToInt64(sFedTaxID), "lynxweb", sUTID));
                    sLogTime = string.Concat(sLogTime, ";;;", DateTime.Now.ToString());
                    //Call LogToAppLog("stats", sLogTime, "")
                    sLogTime = string.Empty;
                    //'sLogTime = sLogTime & "<detail type=""end"" time=""" & Now() & """/>"
                    //'sLogTime = sLogTime & "</rec>"
                    oReg = null;

                    //Check the PIN here, if there is not pin, do we need to continue
                    //processing??

                    //Next block to see if there are any active administrators
                    //sLogTime = sLogTime & "<rec name=""INGRES-GetShops-dp_get_so_shops"">"
                    //sLogTime = sLogTime & "<detail type=""begin"" time=""" & Now() & """/>"
                    sLogTime = string.Concat("dp_get_so_shops;", DateTime.Now.ToString(), ";", DateTime.Now.ToString());
                    sShopOutput = oShop.GetShops("lynxweb", sParentID, sEnvironmentID, "SelectShopOnlyIDs.xsl", sUTID);
                    sLogTime = string.Concat(sLogTime, ";;;", DateTime.Now.ToString());
                    //Call LogToAppLog("stats", sLogTime, "")
                    sLogTime = string.Empty;
                    //sLogTime = sLogTime & "<detail type=""end"" time=""" & Now() & """/>"
                    //sLogTime = sLogTime & "</rec>"
                    oShop = null;
                    sShopOutput = sShopOutput.Replace(",", "");
                    sShopOutput = sShopOutput.Replace(Environment.NewLine, "");

                    //Get any ID's that are active for this Parent
                    sLogTime = string.Concat(sLogTime, "<rec name=\"SQL-GetUserContainerObj\">");
                    sLogTime = string.Concat(sLogTime, "<detail type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");

                    sRsts = oReturn.GetUserContainerObj(string.Empty, string.Empty, Convert.ToString(modConstantsVariables.zLynxAppId), sShopOutput, string.Empty, "1", string.Empty, string.Empty, string.Empty, string.Empty, "UserCommaDelimited.xsl", false, false, 1, "ULastName, UFirstName", true, 0, string.Empty, ref tempvalue, string.Empty, string.Empty, false, string.Empty);
                    sLogTime = string.Concat(sLogTime, "<detail type=\"end\" time=\"", DateTime.Now.ToString(), "\"/>");
                    sLogTime = string.Concat(sLogTime, "</rec>");
                    sRsts = sRsts.Replace(",", "");
                    sRsts = sRsts.Replace(Environment.NewLine, "");

                    //Get any of the active ID's above that have access to Account Maintenance
                    //but only if above string has something in it...
                    if (sRsts == string.Empty)
                    {
                        //'No ID's exist...then there r no active accounts
                        sHTML = string.Empty;
                    }
                    else
                    {
                        //Make this call for Account Maintenance now
                        sLogTime = string.Concat(sLogTime, "<rec name=\"SQL-GetUserAppAccess\">");
                        sLogTime = string.Concat(sLogTime, "<detail type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");
                        sHTML = oReturn.GetUserAppAccess(sRsts, Convert.ToString(modConstantsVariables.zAcctAdminAppId), "", "AccountAdministrators.xsl", sCairEnv, false, 1, 0, string.Empty, false);
                        sLogTime = string.Concat(sLogTime, "<detail type=\"end\" time=\"", DateTime.Now.ToString(), "\"/>");
                        sLogTime = string.Concat(sLogTime, "</rec>");
                    }

                    if (sHTML == string.Empty)
                        sOutput = string.Concat(sOutput, "<admins><ind>N</ind></admins>");
                    else
                        sOutput = string.Concat(sOutput, "<admins><ind>Y</ind></admins>");
                }
                else if ((sUTID == Convert.ToString(modConstantsVariables.zIncoType)) || (sUTID == Convert.ToString(modConstantsVariables.zCAIncoType)))
                {
                    sIncoID = Convert.ToString(oXMLDom.selectSingleNode(string.Concat("//usercontainerobject[@COID=", Convert.ToString(modConstantsVariables.zLynxAppId), "]")).attributes.getNamedItem("UCOOrganizationID").text);
                    sOrgID = sIncoID;
                    sOutput = sOutput & oInco.GetInsCoInfo("Y", "", "", "Y", "", Convert.ToInt64(sIncoID), 0, 0, "", "", "lynxweb", "");
                    //'07/14/09 - Added web_comments_display_ind to the list of profile items to return
                    sOutput = string.Concat(sOutput, oProfile.GetProfileItems("", Convert.ToInt64(sIncoID), "inco", "dwhs_access_ind~web_cube_nm~lynx_pgm_no~web_comments_display_ind~", ""));
                }
                else if (sUTID == "4")
                    sOrgID = oXMLDom.selectSingleNode(string.Concat("//usercontainerobject[@COID=", Convert.ToString(modConstantsVariables.zLynxAppId), "]")).attributes.getNamedItem("UCOOrganizationID").text;
                else
                {
                    //We do nothing if it is a PPG/LYNX Employee
                    sOrgID = "0";
                }


                sLogTime = string.Concat(sLogTime, "<rec name=\"SQL-GetLevelOfAccess\">");
                sLogTime = string.Concat(sLogTime, "<detail type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");
                sOutput = string.Concat(sOutput, oLOA.GetLevelOfAccess("", "", "", "", "", "", false, 1, "LOA.LOALevel", false, 0));
                sLogTime = string.Concat(sLogTime, "<detail type=\"end\" time=\"", DateTime.Now.ToString(), "\"/>");
                sLogTime = string.Concat(sLogTime, "</rec>");
                sLogTime = string.Concat(sLogTime, "<rec name=\"SQL-GetContainerObject\">");
                sLogTime = string.Concat(sLogTime, "<detail type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");
                sOutput = string.Concat(sOutput, oCO.GetContainerObject(sContainers, "", "", "", "", 0, "", "CO.COID, CO.LOAID", "", false, 1));
                sLogTime = string.Concat(sLogTime, "<detail type=\"end\" time=\"", DateTime.Now.ToString(), "\"/>");
                sLogTime = string.Concat(sLogTime, "</rec>");

                sFileName = "lookup_xref.xml";
                sGroupName = string.Concat("metryx_WebUserType_", sUTID, "_WebOrg_", sOrgID);
                sRequestXML = Convert.ToString(BuildXrefLookupRequestXML(sFileName, sGroupName, sUID));

                sXrefOutput = oLookups.GetLookupXrefData(sRequestXML);
                oLookups = null;

                sOutput = string.Concat("<rs1>", sOutput, "<UserType>", sUTID, "</UserType><OrgID>", sOrgID, "</OrgID><sRequestXML>", sRequestXML, "</sRequestXML><XrefOutput>", sXrefOutput, "</XrefOutput></rs1>");

                if (bMasked == true)
                {
                }
                else
                    //This updates the last login date/time with the current date
                    UpdateLastLogin(sUID, Convert.ToString(modConstantsVariables.zLynxAppId));

                if (sStylesheet != string.Empty)
                {
                    //'=transform=and=style========================================
                    //'parse the xml document
                    oXMLDom.async = false;
                    oXMLDom.loadXML(sOutput);
                    if (oXMLDom.parseError.errorCode == 0)
                    {
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));
                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //'if no errors then transform the XML
                            //'into HTML using the stylesheet
                            sOutput = oXMLDom.transformNode(oXSLDoc);
                        }
                        else
                            mobjCommon.InvalidXML(ref tempDoc);
                    }
                    else
                        mobjCommon.InvalidXML(ref tempDom);
                    //=transform=and=style========================================
                }

                sLogTime = string.Concat(sLogTime, "</rs>");
                //Call oAppLog.LogMessage("Login", sLogTime, "SS_WEB_D", 1, 20)
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXSLDoc = null;
                oXMLDom = null;
                oXmlDom2 = null;
                oLOA = null;
                oCO = null;
                oUsr = null;
                oProfile = null;
            }
            return sOutput;
        }

        //     DATE CREATED        : September 2003

        //     AUTHOR              : John Walker (I&S IT)

        //     DESCRIPTION         :

        //     This function updates a UserContainerObject Record for a container object
        //     that is defined as an application

        //PARAMETERS
        //     UID                 : The Users ID
        //     COID                : The container object (App) ID
        //     UCOOrganizationID   : The ID of either the Installer/Ins Co on Ingres
        //     UCOActive           : Active (T/F)
        //     RTID                : User's Responsibility in this application
        //     UTID                : The User Type ID
        //     LOAID               : The User's default access at an application level
        //     bOwnerOfficer       : Is this an owner officer?
        //     JTID                : The JobTitle ID
        //     UpdaterUID          : UID of the person doing the update, for logging

        //MODIFICATIONS

        public void UpdateUserApplication(string UID,
                        string COID,
                        string UCOOrganizationID,
                        bool UCOActive,
                        string RTID,
                        string UTID,
                        string LOAID,
                        bool bOwnerOfficer,
                        [Optional] string JTID,
                        [Optional] string UpdaterUID,
                        [Optional] string UCOOrganizationSubID)
        {
            JTID = "0";
            string sSQL = string.Empty;
            Connection oConnection = null;
            object objtemp = null;
            try
            {
                //create the ADO objects
                oConnection = new Connection();
                if (JTID.Trim() == string.Empty)
                    JTID = "0";


                sSQL = string.Concat("EXEC spUserAppUpdate ", UID, ", ", COID, ", '", UCOOrganizationID, "', ",
               Convert.ToString(UCOActive), ", '", RTID, "', '", UTID, "', ", LOAID, ", ", Convert.ToString(bOwnerOfficer), ", ",
               JTID, ", '", UCOOrganizationSubID, "'");

                oConnection.Open(modCommon.sCairDSN);
                oConnection.Execute(sSQL, out objtemp);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
            }
        }

        //             DATE CREATED        : September 2003

        //     AUTHOR              : John Walker (I&S IT)

        //     DESCRIPTION         :

        //     This function updates a UserContainerObject Record for a container object
        //     that is defined as an application

        //PARAMETERS
        //     UID                 : The Users ID
        //     COID                : The container object (App) ID
        //     LOAID               : The User's default access at an application level
        //     UpdaterUID          : UID of the person doing the update, for logging

        //MODIFICATIONS

        public void UpdateUserFeature(string UID,
                        string COID,
                        string LOAID,
                        [Optional] string UpdaterUID)
        {
            string sSQL = string.Empty;
            Connection oConnection = null;
            object objtemp = null;
            try
            {
                oConnection = new Connection();
                sSQL = string.Concat("EXEC spUserFeatureUpdate ", UID, ", ", COID, ", ", LOAID);
                oConnection.Open(modCommon.sCairDSN);
                oConnection.Execute(sSQL, out objtemp);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
            }
        }
        #endregion

        #region Private Functions
        //     DATE CREATED        : September 2005

        //     AUTHOR              : John Walker (I&S IT)

        //     DESCRIPTION         :

        //     This function updates a UserContainerObject Record's last access date

        //PARAMETERS
        //     UID                 : The Users ID
        //     COID                : The container object (App) ID

        //MODIFICATIONS

        private void UpdateLastLogin(string UID,
                        string COID)
        {

            string sSQL = string.Empty;
            Connection oConnection = null;
            object objtemp = null;
            try
            {
                oConnection = new Connection();
                sSQL = string.Concat("EXEC spUserLastAccessUpdate ", UID, ", ", COID);
                oConnection.Open(modCommon.sCairDSN);
                oConnection.Execute(sSQL, out objtemp);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
            }
        }


        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                oXMLDom = new DOMDocument40();
                mobjCommon = new modCommon();

                sOutput = mobjCommon.GetConfiguration("lynxAccount4");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);

                tempDom = oXMLDom.parseError;
                if (oXMLDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    modCommon.sIngresDSNCA = oXMLDom.selectSingleNode("//DataSources/IngresCA").text;
                    modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    sXMLFileFolder = oXMLDom.selectSingleNode("//XMLFileFolder").text;
                    sXMLFileFolderCA = oXMLDom.selectSingleNode("//XMLFileFolderCA").text;
                }
                else
                    mobjCommon.InvalidXML(ref tempDom);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                mobjCommon = null;
                oXMLDom = null;
            }
        }

        private object BuildXrefLookupRequestXML(string sFileName, string sGroupName, string sUID)
        {
            MXXMLWriter40 oMXXMLWriter = null;
            IVBSAXContentHandler oContentHandler = null;
            SAXAttributes40 oSAXAttributes = null;
            try
            {
                oMXXMLWriter = new MXXMLWriter40();
                oContentHandler = (IVBSAXContentHandler)oMXXMLWriter;
                oSAXAttributes = new SAXAttributes40();

                //Build a valid Xml string for the Request for Coverage

                oMXXMLWriter.omitXMLDeclaration = true;
                oContentHandler.startDocument();
                oSAXAttributes.clear();
                oContentHandler.startElement(string.Empty, string.Empty, "XrefLookupRequest", (IVBSAXAttributes)oSAXAttributes);
                oSAXAttributes.clear();
                oContentHandler.startElement(string.Empty, string.Empty, "XrefObjectGroupName", (IVBSAXAttributes)oSAXAttributes);
                oContentHandler.characters(Convert.ToString(sGroupName));
                oContentHandler.endElement(string.Empty, string.Empty, "XrefObjectGroupName");

                oSAXAttributes.clear();
                oContentHandler.startElement(string.Empty, string.Empty, "ParentObjectCode", (IVBSAXAttributes)oSAXAttributes);
                oContentHandler.characters(string.Empty);
                oContentHandler.endElement(string.Empty, string.Empty, "ParentObjectCode");

                oSAXAttributes.clear();
                oContentHandler.startElement(string.Empty, string.Empty, "XmlFileName", (IVBSAXAttributes)oSAXAttributes);
                oContentHandler.characters(Convert.ToString((sFileName)));
                oContentHandler.endElement(string.Empty, string.Empty, "XmlFileName");

                oSAXAttributes.clear();
                oContentHandler.startElement(string.Empty, string.Empty, "Stylesheet", (IVBSAXAttributes)oSAXAttributes);
                oContentHandler.characters(string.Empty);
                oContentHandler.endElement(string.Empty, string.Empty, "Stylesheet");

                oSAXAttributes.clear();
                oContentHandler.startElement(string.Empty, string.Empty, "UserID", (IVBSAXAttributes)oSAXAttributes);
                oContentHandler.characters(Convert.ToString(string.Concat("w", Right(string.Concat("000000", sUID), 6))));
                oContentHandler.endElement(string.Empty, string.Empty, "UserID");

                oSAXAttributes.clear();
                oContentHandler.startElement(string.Empty, string.Empty, "UserSessionNo", (IVBSAXAttributes)oSAXAttributes);
                oContentHandler.characters(Convert.ToString(sUID));
                oContentHandler.endElement(string.Empty, string.Empty, "UserSessionNo");

                oSAXAttributes.clear();
                oContentHandler.endElement(string.Empty, string.Empty, "XrefLookupRequest");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oMXXMLWriter = null;
                oContentHandler = null;
                oSAXAttributes = null;
            }
            return oMXXMLWriter.output;
        }
        #endregion
    }
}
