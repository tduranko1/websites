﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using MSXML2;
using ADODB;
using VBA;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class modDataAccess
    {
        private string zMODULE = "modDataAccess";
        private modCommon objmodCommon = null;

        /// <summary>
        /// UpdateSessionRows(FUNCTION)
        /// This function is used to perform updates to the Ingres database
        ///  using session tables.
        /// </summary>
        /// <param name="sSessionTableName"></param>
        private Recordset UpdateSessionRows(string sSessionTableName, string sStoredProcName, string sInsertSQL, string sConnectionString, [Optional]string sFilter = "", [Optional]string sOutputColumns = "*")
        {
            int iCounter;
            string[] vInsertSQL;
            string sSQL = string.Empty;
            bool bErrorFlag;
            string sStatsLog = string.Empty;
            object objMissing = Type.Missing;
            Connection oConnection = null;
            Command oCommand = null;
            Recordset oRecordset = null;
            object obj;
            try
            {
                //create the ADO objects
                objmodCommon = new modCommon();
                bErrorFlag = false;
                oConnection = new Connection();
                oCommand = new Command();
                oRecordset = new Recordset();
                sStatsLog = string.Concat(sStoredProcName, ";", Convert.ToString(DateTime.Now));

                //setup the connection
                oConnection.ConnectionTimeout = 60;
                oConnection.IsolationLevel = IsolationLevelEnum.adXactIsolated;
                oConnection.Open(sConnectionString);

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));

                //create the session table
                sSQL = string.Concat("declare global temporary table session.", sSessionTableName, " as ", "select * from ", sSessionTableName, " on commit preserve rows with norecovery");

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));
                vInsertSQL = sInsertSQL.Split('~');

                for (iCounter = 0; iCounter <= vInsertSQL.GetUpperBound(0) - 1; iCounter++)
                {
                    //populate the session table
                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = vInsertSQL[iCounter];
                    oCommand.CommandTimeout = 90;
                    oCommand.Execute(out obj);
                }

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));

                //execute the stored procedure
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = string.Concat("{ ? = call lynxdba.", sStoredProcName, "(dummy=session.", sSessionTableName, ") }");
                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));

                if (Convert.ToInt32(oCommand.Parameters["retval"]) != 0)
                    bErrorFlag = true;//fatal error

                //setup the command to retrieve the resulting recordset
                sSQL = string.Concat("select ", sOutputColumns, " from session.", sSessionTableName);

                if (sFilter != "")
                    sSQL = string.Concat(sSQL, " where ", sFilter);//only retrieve specific profile items

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                //execute the query for readonly
                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, objMissing, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));

                //drop the session table
                sSQL = string.Concat("drop table session.", sSessionTableName);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);
                oCommand.ActiveConnection.CommitTrans();

                //'sStatsLog = sStatsLog & ";" & CStr(Now)
                //'Call LogToAppLog("stats", sStatsLog, "")
                if (bErrorFlag)
                    throw new Exception(string.Concat(Convert.ToString(modCommon.lynxDatabaseError), "UpdateSessionRows", Convert.ToString(oRecordset), "msg_text"));

                objmodCommon.CtxSetComplete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //disconnect the recordset
                oRecordset.ActiveConnection = null;
                oCommand = null;
                oConnection = null;
            }
            //return the resultant recordset
            return oRecordset;
        }

        /// <summary>
        /// UpdateSessionRows2(FUNCTION)
        /// This function is used to perform updates to the Ingres database
        /// using session tables.
        /// </summary>
        /// <returns></returns>
        public Recordset UpdateSessionRows2(string sSessionTableName, string sStoredProcName, string sInsertSQL, string sConnectionString,
                                         [Optional]string sFilter = "", [Optional]string sOutputColumns = "*")
        {
            int iCounter;
            string[] vInsertSQL;
            string sSQL = string.Empty;
            bool bErrorFlag;
            object objMissing = Type.Missing;
            string sStatsLog = string.Empty;
            int sInd;
            Connection oConnection = null;
            Command oCommand = null;
            Recordset oRecordset = null;
            object obj;
            try
            {
                //create the ADO objects
                oConnection = new Connection();
                oCommand = new Command();
                oRecordset = new Recordset();

                sStatsLog = string.Concat(sStoredProcName, ";", Convert.ToString(DateTime.Now));
                bErrorFlag = false;

                //setup the connection
                oConnection.ConnectionTimeout = 60;
                oConnection.IsolationLevel = IsolationLevelEnum.adXactIsolated;
                oConnection.Open(sConnectionString);

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));

                //create the session table
                sSQL = string.Concat("declare global temporary table session.", sSessionTableName, " as ", "select * from ", sSessionTableName, " on commit preserve rows with norecovery");

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));

                vInsertSQL = sInsertSQL.Split('~');
                for (iCounter = 0; iCounter <= vInsertSQL.GetUpperBound(0) - 1; iCounter++)
                {
                    //populate the session table
                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = vInsertSQL[iCounter];
                    oCommand.CommandTimeout = 90;
                    oCommand.Execute(out obj);
                }

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));

                //execute the stored procedure
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = string.Concat("{ ? = call lynxdba.", sStoredProcName, "(dummy=session.", sSessionTableName, ") }");
                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));

                if (Convert.ToInt32(oCommand.Parameters["retval"]) > 0)
                {
                    bErrorFlag = true;//fatal error
                    sInd = 4;
                }
                else
                {
                    if (Convert.ToInt32(oCommand.Parameters["retval"]) < 0)
                        sInd = 1;//Business Logic error
                }

                ////setup the command to retrieve the resulting recordset
                sSQL = string.Concat("select ", oCommand.Parameters["retval"], " as retval, ", (sOutputColumns == "*" ? "session." + sSessionTableName + ".*" : sOutputColumns), " from session.", sSessionTableName);

                if (sFilter != "")
                    sSQL = string.Concat(sSQL, " where ", sFilter);//only retrieve specific profile items

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                //execute the query for readonly
                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, objMissing, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);
                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));

                //drop the session table
                sSQL = string.Concat("drop table session.", sSessionTableName);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);
                oCommand.ActiveConnection.CommitTrans();

                //sStatsLog = sStatsLog & ";" & CStr(Now)
                //'Call LogToAppLog("stats", sStatsLog, "")
                if (bErrorFlag)
                    throw new Exception(string.Concat(Convert.ToString(modCommon.lynxDatabaseError), "UpdateSessionRows2", Convert.ToString(oRecordset), "msg_text"));

                objmodCommon.CtxSetComplete();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                //disconnect the recordset
                oRecordset.ActiveConnection = null;
                oRecordset = null;
                oCommand = null;
                oConnection = null;
            }
            //return the resultant recordset
            return oRecordset;
        }

        /// <summary>
        /// GetSessionRows(FUNCTION)
        /// This function retrieves a ???
        /// </summary>
        /// <returns></returns>
        public Recordset GetSessionRows(string sSessionTableName, string sStoredProcName, string sInsertSQL, string sConnectionString, [Optional]string sFilter = "", [Optional]string sOutputColumns = "*")
        {
            bool bErrorFlag;
            string sSQL = string.Empty;
            string sOutput = string.Empty;
            string sStatsLog = string.Empty;
            string sErrDesc = string.Empty;
            long lErrorNo;
            long lErrLine;
            int sInd;
            object objMissing = Type.Missing;
            object obj;
            Connection oConnection = null;
            Command oCommand = null;
            Recordset oRecordset = null;
            try
            {
                //create the ADO objects
                objmodCommon = new modCommon();
                oConnection = new Connection();
                oCommand = new Command();
                oRecordset = new Recordset();

                bErrorFlag = false;

                //setup the connection
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(sConnectionString);

                //create the session table
                sSQL = string.Concat("declare global temporary table session.", sSessionTableName, " as ", "select * from ", sSessionTableName, " on commit preserve rows with norecovery");

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);

                //create the session table
                //populate the session table
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sInsertSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);

                //execute the stored procedure
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = string.Concat("{ ? = call lynxdba.", sStoredProcName, "(dummy=session.", sSessionTableName, ") }");
                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);

                if (Convert.ToInt32(oCommand.Parameters["retval"]) != 0)
                {
                    bErrorFlag = true;//fatal error
                    if (Convert.ToInt32(oCommand.Parameters["retval"]) < 0)
                        sInd = 1;
                    else
                        sInd = 4;
                }

                //setup the command to retrieve the resulting recordset
                sSQL = string.Concat("select ", sOutputColumns, " from session.", sSessionTableName);

                if (sFilter != "")
                    sSQL = string.Concat(sSQL, " where ", sFilter);//only retrieve specific profile items

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                //execute the query for readonly
                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, objMissing, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);

                //drop the session table
                sSQL = string.Concat("drop table session.", sSessionTableName);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out obj);
                oCommand.ActiveConnection.CommitTrans();

                if (bErrorFlag)
                    throw new Exception(string.Concat(Convert.ToString(modCommon.lynxDatabaseError), "Data access error", Convert.ToString(oRecordset), "msg_text"));

                objmodCommon.CtxSetComplete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //disconnect the recordset
                oRecordset.ActiveConnection = null;
                oRecordset = null;
                oCommand = null;
                oConnection = null;
            }
            //return the resultant recordset
            return oRecordset;
        }

        /// <summary>
        /// ArrayToXML(FUNCTION)
        /// This function converts the output parameters
        /// of a command object to an XML string.
        /// </summary>
        /// <returns></returns>
        public string ArrayToXML(string[] vArray, [Optional]bool bWellFormed = true, [Optional]string sElement = "record", [Optional]string sDocumentElement = "records")
        {
            string sOutput = string.Empty;
            string tmpobj = string.Empty;
            string tmpstr = string.Empty;
            int iCounter;
            MXXMLWriter40 oMXXMLWriter = null;
            IVBSAXContentHandler oContentHandler = null;
            IVBSAXErrorHandler oErrorHandler = null;
            SAXAttributes40 oSAXAttributes = null;
            try
            {
                Type valuetype = vArray.GetType();

                if (!valuetype.IsArray)
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), zMODULE, "Item '", vArray.ToString(), "' not an array"));

                oMXXMLWriter = new MXXMLWriter40();
                oSAXAttributes = new SAXAttributes40();
                oContentHandler = (IVBSAXContentHandler)oMXXMLWriter;
                oErrorHandler = (IVBSAXErrorHandler)oMXXMLWriter;

                // If bWellFormed = False Then
                oMXXMLWriter.omitXMLDeclaration = true;
                //End If

                oContentHandler.startDocument();

                oSAXAttributes.clear();//oSAXAttributes.addAttribute "", "", "cover", "", "hard"

                if (bWellFormed)
                    oContentHandler.startElement(" ", " ", ref sDocumentElement, (IVBSAXAttributes)oSAXAttributes);

                for (iCounter = 0; iCounter <= vArray.GetUpperBound(0); iCounter++)
                {
                    oContentHandler.startElement("", "", string.Concat("index", iCounter), (IVBSAXAttributes)oSAXAttributes);
                    oContentHandler.characters(vArray[iCounter].Trim());
                    oContentHandler.endElement("", "", string.Concat("index", iCounter));
                }
                if (bWellFormed)
                    oContentHandler.endElement("", "", sDocumentElement);

                sOutput = Convert.ToString(oMXXMLWriter.output);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oMXXMLWriter = null;
                oContentHandler = null;
                oErrorHandler = null;
                oSAXAttributes = null;
            }
            return sOutput;
        }

        /// <summary>
        /// ParametersToXML(FUNCTION)
        /// This function converts the output parameters
        /// of a command object to an XML string.
        /// </summary>
        /// <returns></returns>
        public string ParametersToXML(ref Command cmd, [Optional]bool bWellFormed = true, [Optional]string sElement = "record", [Optional]string sDocumentElement = "records")
        {
            string sOutput = string.Empty;
            //Parameter oParameter = null;
            MXXMLWriter40 oMXXMLWriter = null;
            IVBSAXContentHandler oContentHandler = null;
            IVBSAXErrorHandler oErrorHandler = null;
            SAXAttributes40 oSAXAttributes = null;
            try
            {
                oMXXMLWriter = new MXXMLWriter40();
                oSAXAttributes = new SAXAttributes40();
                oContentHandler = (IVBSAXContentHandler)oMXXMLWriter;
                oErrorHandler = (IVBSAXErrorHandler)oMXXMLWriter;

                //If bWellFormed = False Then
                oMXXMLWriter.omitXMLDeclaration = true;
                //End If

                oContentHandler.startDocument();

                oSAXAttributes.clear();//oSAXAttributes.addAttribute "", "", "cover", "", "hard"

                if (bWellFormed)
                    oContentHandler.startElement("", "", sDocumentElement, (IVBSAXAttributes)oSAXAttributes);

                foreach (Parameter oParameter in cmd.Parameters)
                {
                    oSAXAttributes.clear();
                    oContentHandler.startElement("", "", oParameter.Name, (IVBSAXAttributes)oSAXAttributes);
                    oContentHandler.characters(Convert.ToString(oParameter.Value).Trim());
                    oContentHandler.endElement("", "", oParameter.Name);
                }

                if (bWellFormed)
                    oContentHandler.endElement("", "", sDocumentElement);

                sOutput = Convert.ToString(oMXXMLWriter.output);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oMXXMLWriter = null;
                oContentHandler = null;
                oErrorHandler = null;
                oSAXAttributes = null;
            }
            return sOutput;
        }

        /// <summary>
        /// RsToXML(FUNCTION)
        /// This function converts a recordset to an XML string.
        /// </summary>
        /// <returns></returns>
        public string RsToXML(Recordset rs, [Optional]bool bWellFormed = true, [Optional]string sElement = "record", [Optional]string sDocumentElement = "records")
        {
            string sOutput = string.Empty;
            //string[] vField;
            MXXMLWriter40 oMXXMLWriter = null;
            IVBSAXContentHandler oContentHandler = null;
            IVBSAXErrorHandler oErrorHandler = null;
            SAXAttributes40 oSAXAttributes = null;
            try
            {
                if (rs == null)
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), zMODULE, "recordset rs is nothing"));

                oMXXMLWriter = new MXXMLWriter40();
                oSAXAttributes = new SAXAttributes40();
                oContentHandler = (IVBSAXContentHandler)oMXXMLWriter;
                oErrorHandler = (IVBSAXErrorHandler)oMXXMLWriter;

                //If bWellFormed = False Then
                oMXXMLWriter.omitXMLDeclaration = true;
                oContentHandler.startDocument();

                oSAXAttributes.clear();// oSAXAttributes.addAttribute "", "", "cover", "", "hard"

                if (bWellFormed)
                    oContentHandler.startElement("", "", sDocumentElement, (IVBSAXAttributes)oSAXAttributes);

                if (!rs.EOF)
                {
                    rs.MoveFirst();
                    do
                    {
                        oSAXAttributes.clear();
                        foreach (Field vField in rs.Fields)
                        {
                            //create an attribute
                            if ((vField.Value) == null)
                                oSAXAttributes.addAttribute("", "", vField.Name, "", "");
                            else
                                oSAXAttributes.addAttribute("", "", vField.Name, "", Convert.ToString(vField.Value).Trim());
                        }
                        oContentHandler.startElement("", "", sElement, (IVBSAXAttributes)oSAXAttributes);
                        oContentHandler.endElement("", "", sElement);
                        rs.MoveNext();
                    } while (!rs.EOF);
                }
                if (bWellFormed)
                    oContentHandler.endElement("", "", sDocumentElement);

                sOutput = Convert.ToString(oMXXMLWriter.output);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oMXXMLWriter = null;
                oContentHandler = null;
                oErrorHandler = null;
                oSAXAttributes = null;
            }
            return sOutput;
        }

        /// <summary>
        /// RsToXML2(FUNCTION)
        /// This function converts a recordset to an XML string.
        /// </summary>
        /// <returns></returns>
        public string RsToXML2(Recordset rs, [Optional]bool bWellFormed = true, [Optional]string sElement = "record", [Optional]string sDocumentElement = "records", [Optional]string sCommonColumns = "")
        {
            string sOutput = string.Empty;
            //object vField = null;
            string sFieldValue = string.Empty;
            int iLoopcounter;
            MXXMLWriter40 oMXXMLWriter = null;
            IVBSAXContentHandler oContentHandler = null;
            IVBSAXErrorHandler oErrorHandler = null;
            SAXAttributes40 oSAXAttributes = null;
            SAXAttributes40 oSAXAttributes2 = null;
            try
            {
                if (rs == null)
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), zMODULE, "recordset rs is nothing"));

                oMXXMLWriter = new MXXMLWriter40();
                oSAXAttributes = new SAXAttributes40();
                oSAXAttributes2 = new SAXAttributes40();
                oContentHandler = (IVBSAXContentHandler)oMXXMLWriter;
                oErrorHandler = (IVBSAXErrorHandler)oMXXMLWriter;

                //If bWellFormed = False Then
                oMXXMLWriter.omitXMLDeclaration = true;
                //End If

                oContentHandler.startDocument();

                oSAXAttributes.clear();
                //oSAXAttributes.addAttribute "", "", "cover", "", "hard"

                if (bWellFormed)
                    oContentHandler.startElement("", "", sDocumentElement, (IVBSAXAttributes)oSAXAttributes);

                if (!rs.EOF)
                {
                    iLoopcounter = 0;
                    rs.MoveFirst();
                    do
                    {
                        oSAXAttributes.clear();
                        foreach (Field vField in rs.Fields)
                        {
                            sFieldValue = Convert.ToString(vField).Trim();
                            if (sCommonColumns.IndexOf(string.Concat(vField.Name, ",")) > 0)
                            {
                                if (iLoopcounter == 0)
                                {
                                    //create an attribute
                                    oSAXAttributes2.addAttribute("", "", vField.Name, "", sFieldValue);
                                }
                            }
                            else
                                //create an attribute
                                oSAXAttributes.addAttribute("", "", vField.Name, "", sFieldValue);
                        }
                        if(iLoopcounter == 0)
                        {
                            oContentHandler.startElement("", "", sElement, (IVBSAXAttributes)oSAXAttributes);
                            oContentHandler.endElement("", "", "common");
                        }
                        oContentHandler.startElement("", "", sElement, (IVBSAXAttributes)oSAXAttributes);
                        oContentHandler.endElement("", "", sElement);
                        iLoopcounter = iLoopcounter + 1;
                        rs.MoveNext();
                    }
                    while (!rs.EOF);
                }
                if (bWellFormed)
                    oContentHandler.endElement("", "", sDocumentElement);

                sOutput = Convert.ToString(oMXXMLWriter.output);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oMXXMLWriter = null;
                oContentHandler = null;
                oErrorHandler = null;
                oSAXAttributes2 = null;
                oSAXAttributes = null;
            }
            return sOutput;
        }

        /// <summary>
        /// RsToXML3(FUNCTION)
        /// This function converts a recordset to an XML string.
        /// </summary>
        /// <returns></returns>
        public string RsToXML3(Recordset rs, [Optional]bool bWellFormed = true, [Optional]string sElement = "record", [Optional]string sDocumentElement = "records", [Optional]string sCommonColumns = "")
        {
            string sOutput = string.Empty;
            //object vField = null;
            string sFieldValue = string.Empty;
            int iLoopcounter;
            MXXMLWriter40 oMXXMLWriter = null;
            IVBSAXContentHandler oContentHandler = null;
            IVBSAXErrorHandler oErrorHandler = null;
            SAXAttributes40 oSAXAttributes = null;
            SAXAttributes40 oSAXAttributes2 = null;
            try
            {
                if (rs == null)
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), zMODULE, "recordset rs is nothing"));

                oMXXMLWriter = new MXXMLWriter40();
                oSAXAttributes = new SAXAttributes40();
                oSAXAttributes2 = new SAXAttributes40();
                oContentHandler = (IVBSAXContentHandler)oMXXMLWriter;
                oErrorHandler = (IVBSAXErrorHandler)oMXXMLWriter;

                //If bWellFormed = False Then
                oMXXMLWriter.omitXMLDeclaration = true;
                // End If

                oContentHandler.startDocument();

                // oSAXAttributes.addAttribute "", "", "cover", "", "hard"
                oSAXAttributes.clear();

                if (bWellFormed)
                    oContentHandler.startElement("", "", sDocumentElement, (IVBSAXAttributes)oSAXAttributes);

                if (!rs.EOF)
                {
                    iLoopcounter = 0;
                    rs.MoveFirst();
                    do
                    {
                        oSAXAttributes.clear();
                        foreach (Field vField in rs.Fields)
                        {
                            //=build=attributes=====================
                            if (vField.Type != DataTypeEnum.adVarChar || vField.Type == DataTypeEnum.adVarChar && vField.ActualSize < 16)
                            {
                                sFieldValue = Convert.ToString(vField.Value).Trim();
                                if (sCommonColumns.IndexOf(string.Concat(vField.Name, ",")) > 0)
                                {
                                    if (iLoopcounter == 0)
                                    {
                                        //create an attribute
                                        oSAXAttributes2.addAttribute("", "", vField.Name, "", sFieldValue);
                                    }
                                }
                                else
                                {
                                    //create an attribute
                                    oSAXAttributes.addAttribute("", "", vField.Name, "", sFieldValue);
                                }
                            }
                        }
                        oMXXMLWriter.disableOutputEscaping = true;

                        //if this is the first record we may need to build a "common" element
                        if (iLoopcounter == 0)
                        {
                            //'=build=the=common=element=============
                            oContentHandler.startElement("", "", "common", (IVBSAXAttributes)oSAXAttributes2);
                            foreach (Field vField in rs.Fields)
                            {
                                if (vField.Type == DataTypeEnum.adVarChar && vField.ActualSize > 15)
                                {
                                    oSAXAttributes2.clear();
                                    sFieldValue = Convert.ToString(vField.Value).Trim();
                                    if (sCommonColumns.IndexOf(string.Concat(vField.Name, ",")) > 0)
                                    {
                                        //create an element
                                        oContentHandler.startElement("", "", vField.Name, (IVBSAXAttributes)oSAXAttributes2);
                                        oContentHandler.characters(sFieldValue);
                                        oContentHandler.endElement("", "", vField.Name);
                                    }

                                }
                            }
                            //=build=the=common=element=============
                            oContentHandler.endElement("", "", "common");
                        }
                        //=build=the=current=field=element==========
                        oContentHandler.startElement("", "", sElement, (IVBSAXAttributes)oSAXAttributes);
                        foreach (Field vField in rs.Fields)
                        {
                            if (vField.Type == DataTypeEnum.adVarChar && vField.ActualSize > 15)
                            {
                                oSAXAttributes.clear();
                                sFieldValue = Convert.ToString(vField.Value).Trim();
                                if (!(sCommonColumns.IndexOf(string.Concat(vField.Name, ",")) > 0))
                                {
                                    //create an element
                                    oContentHandler.startElement("", "", vField.Name, (IVBSAXAttributes)oSAXAttributes2);
                                    oContentHandler.characters(sFieldValue);
                                    oContentHandler.endElement("", "", vField.Name);
                                }
                            }
                        }
                        oContentHandler.endElement("", "", sElement);
                        //=build=the=current=field=element==========

                        iLoopcounter = iLoopcounter + 1;
                        rs.MoveNext();

                    } while (!rs.EOF);
                }

                if (bWellFormed)
                    oContentHandler.endElement("", "", sDocumentElement);

                sOutput = Convert.ToString(oMXXMLWriter.output);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oMXXMLWriter = null;
                oErrorHandler = null;
                oContentHandler = null;
                oSAXAttributes = null;
                oSAXAttributes2 = null;
            }
            return sOutput;
        }

        /// <summary>
        /// TransformXML(FUNCTION)
        /// This function converts a recordset to an XML string.
        /// </summary>
        /// <param name="sXML"></param>
        public string TransformXML(string sXML, string sStylesheetPath)
        {
            FreeThreadedDOMDocument40 oXMLDom = null;//changed to the free threaded model - smg 08mar2004
            FreeThreadedDOMDocument40 oXSLDoc = null;// changed to the free threaded model - smg 08mar2004
            IXMLDOMParseError tempoXMLDom = null;
            IXMLDOMParseError tempoXSLDoc = null;
            string strTransformxml = string.Empty;
            try
            {
                objmodCommon = new modCommon();
                tempoXMLDom = oXMLDom.parseError;
                tempoXSLDoc = oXSLDoc.parseError;
                if (sStylesheetPath != "")
                {
                    //parse the xml document
                    //Set oXmlDom = CreateObject("MSXML2.DOMDocument.4.0")
                    oXMLDom = new FreeThreadedDOMDocument40();// changed to the free threaded model - smg 08mar2004
                    oXMLDom.async = false;
                    oXMLDom.loadXML(sXML);

                    if (oXMLDom.parseError.errorCode == 0)
                    {
                        //set oXSLDoc = CreateObject("MSXML2.DOMDocument.4.0")
                        oXSLDoc = new FreeThreadedDOMDocument40();
                        oXSLDoc.async = false;
                        oXSLDoc.load(sStylesheetPath);

                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //if no errors then transform the XML using the stylesheet
                            strTransformxml = oXMLDom.transformNode(oXSLDoc);
                        }
                        else
                            objmodCommon.InvalidXML(ref tempoXSLDoc);
                    }
                    else
                        objmodCommon.InvalidXML(ref tempoXMLDom);
                }
                else
                {
                    //since no stylesheet and Path is specified then raise an error
                    throw new Exception(string.Concat(string.Concat(Constants.vbObjectError, 1000), " No Styesheet/Path specified "));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXSLDoc = null;
            }
            return strTransformxml;
        }

        /// <summary>
        /// GetSessionRows2(FUNCTION)
        /// Purpose:
        /// This function retrieves a ???
        /// </summary>
        public Recordset GetSessionRows2(string sSessionTableName, string sStoredProcName, string sInsertSQL, string sConnectionString, [Optional]string sFilter = "", [Optional]string sOutputColumns = "*")
        {
            string sStatsLog = string.Empty;
            bool bErrorFlag;
            string sSQL = string.Empty;
            string sOutput = string.Empty;
            object objMissing = Type.Missing;
            object objResult;
            Connection oConnection = null;
            Command oCommand = null;
            Recordset oRecordset = null;
            int sInd;
            try
            {
                //create the ADO objects
                oConnection = new Connection();
                oCommand = new Command();
                oRecordset = new Recordset();

                bErrorFlag = false;

                //setup the connection
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(sConnectionString);

                //create the session table
                sSQL = string.Concat("declare global temporary table session.", sSessionTableName, " as ", "select * from ", sSessionTableName, " on commit preserve rows with norecovery");

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objResult);

                //create the session table
                //populate the session table
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sInsertSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objResult);

                //'modify table to the heap.  speeds up dp calls that have a unary update
                sSQL = string.Concat("modify session.", sSessionTableName, " to heap");

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objResult);

                //execute the stored procedure
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = string.Concat("{ ? = call lynxdba.", sStoredProcName, "(dummy=session.", sSessionTableName, ") }");
                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objResult);

                if (Convert.ToInt32(oCommand.Parameters["retval"]) > 0)
                {
                    bErrorFlag = true;// 'fatal error
                    sInd = 4;
                }
                else
                {
                    if (Convert.ToInt32(oCommand.Parameters["retval"]) < 0)
                        sInd = 1;
                }

                if (sOutputColumns.Trim() == "")
                    sOutputColumns = "*";

                //setup the command to retrieve the resulting recordset
                sSQL = string.Concat("select ", sOutputColumns, " from session.", sSessionTableName);

                if (sFilter != "")
                {
                    //only retrieve specific profile items
                    sSQL = string.Concat(sSQL, " where ", sFilter);
                }

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                //execute the query for readonly
                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, objMissing, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);

                //drop the session table
                sSQL = string.Concat("drop table session.", sSessionTableName);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objResult);
                oCommand.ActiveConnection.CommitTrans();

                if (bErrorFlag)
                    throw new Exception(string.Concat(Convert.ToString(modCommon.lynxDatabaseError), "Data access error", Convert.ToString(oRecordset), "msg_text"));

                objmodCommon.CtxSetComplete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //disconnect the recordset
                oRecordset.ActiveConnection = null;
                oCommand = null;
                oConnection = null;
                oRecordset = null;
            }
            //return the resultant recordset
            return oRecordset;
        }

        /// <summary>
        /// Gets the appropriate number of question marks for an ingres dp call
        /// </summary>
        /// <param name="sSessionTableName"></param>
        /// <param name="sStoredProcName"></param>
        public string GetSessionXML(string sSessionTableName, string sStoredProcName, string sInsertSQL, string sConnectionString, [Optional]string sFilter = "", [Optional]string sOutputColumns = "*", [Optional]string sStylesheetPath = "", [Optional]string sRootName = "records", [Optional]string sRowName = "record")
        {
            Recordset oOutputRs = null;
            string sOutput = string.Empty;
            try
            {
                oOutputRs = GetSessionRows2(sSessionTableName, sStoredProcName, sInsertSQL, sConnectionString, sFilter, sOutputColumns);

                if (oOutputRs.EOF)
                    sOutput = string.Concat("<", sRootName, "/>");
                else
                    sOutput = RsToXML(oOutputRs, true, sRowName, sRootName);

                if (sStylesheetPath != "")
                    sOutput = TransformXML(sOutput, sStylesheetPath);

                oOutputRs.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs = null;
            }
            return sOutput;
        }

        public string BuildInsertSQL(string sTableName, string sColumnList, string sValueList, [Optional]string sUserID = "lynxweb", [Optional]string sSessionID = "lynxweb")
        {
            string strBuildInsertsql = string.Empty;
            try
            {
                strBuildInsertsql = string.Concat("insert into session.", sTableName, " (", sColumnList, ",commit_ind,msg_ind,user_id_no,user_session_no)", " values (", sValueList, ",'Y','N','", sUserID, "','", sSessionID, "')");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strBuildInsertsql;
        }

        public string getQuestionMarks(long lNumberToGet)
        {
            string sResult = string.Empty;
            string strquestionmark = string.Empty;
            try
            {
                sResult = string.Join("?", new string[lNumberToGet - 2]);//or string.join(enumerable.repeat("?",inumbertoget-2).toarray())
                sResult = sResult.Replace("?", "?,");
                strquestionmark = string.Concat(sResult, "?");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strquestionmark;
        }

        private void dropTable(string sTableName, ref ADODB.Command oCommand)
        {
            try
            {
                object objResult;
                if (oCommand != null)
                {
                    oCommand.CommandType = CommandTypeEnum.adCmdText;
                    oCommand.CommandText = string.Concat("drop table session.", sTableName);
                    oCommand.CommandTimeout = 90;
                    oCommand.Execute(out objResult);
                    oCommand.ActiveConnection.CommitTrans();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
