﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Runtime.InteropServices;
using ADODB;
using MSXML2;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class clsRegister
    {
        private ObjectContext moObjectContext = null;

        //'Define the Application name to be used in Error Handling
        private string APP_NAME = "lynxAccount4.";
        private string MODULE_NAME = "clsRegister.";
        private modCommon objmodCommon = null;
        private modDataAccess objmodDataAccess = null;

        /// <summary>
        /// This function verifies pre-registration information
        /// for a particular shop for a level one account.
        /// </summary>
        /// <returns></returns>
        public string FedTaxDispatchLookup(long lFederalTaxId, long lDispatchNo, int iUserTypeNo, string sEnvId, [Optional]string sUserType="")
        {
            string sOutput = string.Empty;
            object objResult;
            Connection oConnection = null;
            Command oCommand = null;

            try
            {
                objmodCommon=new modCommon();
                objmodDataAccess = new modDataAccess();
                oConnection = new Connection();
                oCommand = new Command();
                switch (lFederalTaxId)
                {
                    case 0:
                    case 111111111:
                    case 222222222:
                    case 333333333:
                    case 444444444:
                    case 555555555:
                    case 666666666:
                    case 777777777:
                    case 888888888:
                    case 999999999:
                         sOutput = "<retval>-1</retval><h_msg>CE_AREG_IFTN</h_msg>";
                         oCommand = null;
                         oConnection = null;
                         sOutput = string.Concat("<response>", sOutput, "</response>");
                         objmodCommon.CtxSetComplete();
                         break;
                         //return sOutput;
                    case 123456789:
                    case 987654321:
                    case 121212121:
                        sOutput = "<retval>-1</retval><h_msg>CE_AREG_IFTN</h_msg>";
                        oCommand = null;
                        oConnection = null;
                        sOutput = string.Concat("<response>", sOutput, "</response>");
                        objmodCommon.CtxSetComplete();
                        break;
                        //return sOutput;
                    //case is > 999999999:
                    //    sOutput = "<retval>-1</retval><h_msg>CE_AREG_IFTN</h_msg>";
                    //    break;
                    default:
                        //ignore this case
                        break;
                }
                if(lFederalTaxId>999999999)
                    sOutput = "<retval>-1</retval><h_msg>CE_AREG_IFTN</h_msg>";

                if (lDispatchNo > 999999999)
                {
                    sOutput = "<retval>-1</retval><h_msg>CE_AREG_IDPN</h_msg>";
                    oCommand = null;
                    oConnection = null;
                    sOutput = string.Concat("<response>", sOutput, "</response>");
                    objmodCommon.CtxSetComplete();
                }
                oConnection.ConnectionTimeout = 60;
                switch (sUserType)
                {
                    case "10":
                    case "11":
                    case "12":
                    case "CA":
                        oConnection.Open(modCommon.sIngresDSNCA);
                        break;
                    default:
                        oConnection.Open(modCommon.sIngresDSN);
                        break;
                }
                
                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = "{? = call lynxdba.dp_web_lvl_1_pre_register (?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;

                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_dispatch_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput,0 , lDispatchNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_federal_tax_id", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput,0, lFederalTaxId));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput,0, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Execute(out objResult);

                oCommand.ActiveConnection.CommitTrans();
                sOutput =objmodDataAccess.ParametersToXML(ref oCommand,false, "item", "info");

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sOutput;
        }

        /// <summary>
        /// Tells various information about a Fed Tax ID, such as if it has a PIN,
        /// what the pin is, active ind and the parent id associated with the fed tax id
        /// </summary>
        /// <returns></returns>
        public string FedTaxInfo(long lFederalTaxId, string sWebUidNo, [Optional]string sUserType=" ")
        {
            string sOutput = string.Empty;
            Connection oConnection = null;
            Command oCommand = null;
            object objresult;
            try
            {
                oConnection = new Connection();
                objmodDataAccess = new modDataAccess();
                oCommand = new Command();
                objmodCommon=new modCommon();

                oConnection.ConnectionTimeout = 60;

                switch (sUserType)
                {
                    case "10":
                    case "11":
                    case "12":
                    case "CA":
                        oConnection.Open(modCommon.sIngresDSNCA);
                        break;
                    default:
                        oConnection.Open(modCommon.sIngresDSN);
                        break;
                }

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = "{? = call lynxdba.dp_get_fed_tax_id_status (?,?,?,?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;

                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger,ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_commit_ind",DataTypeEnum.adChar,ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, sWebUidNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar,ParameterDirectionEnum.adParamInput, 32, sWebUidNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_federal_tax_id", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput,0 , lFederalTaxId));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_parent_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput,0, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_pin_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 4, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_active_status", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, " "));
                oCommand.Execute(out objresult);
                oCommand.ActiveConnection.CommitTrans();

                sOutput = objmodDataAccess.ParametersToXML(ref oCommand,false,"item","info");
                sOutput = string.Concat("<response>",sOutput, "</response>");
                objmodCommon.CtxSetComplete();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCommand=null;
                oConnection=null;
            }
            return sOutput;
        }

        /// <summary>
        /// This function gets installer info
        /// An Array/XML in which I just need to llo up the retval and the h_msg_text to confirm a
        /// successful Installer
        /// </summary>
        /// <returns></returns>
        public object GetInstallerInfo(long lngInstallerUidNo, long lngInsCompanyUIdNo, long lngInsRegionUIdNo, long lngLynxPgmNo, string strUserIdNo, string strUserSessionNo, [Optional]string sUserType="")
        {
            string strStoredProcName = string.Empty;
            string strTempTableName = string.Empty;
            string strInsertSql = string.Empty;
            string strSelectSql = string.Empty;
            string sSQL = string.Empty;
            object activeCon = Type.Missing;
            Connection oConnection = null;
            Command oCommand = null;
            Recordset oRecordset = null;
            object objinstaller;
            object objResult;
            try
            {
                objmodCommon = new modCommon();
                objmodDataAccess = new modDataAccess();
                oConnection = new Connection();
                oCommand = new Command();
                oRecordset = new Recordset();

                oConnection.ConnectionTimeout = 60;
                switch (sUserType)
                {
                    case "10":
                    case "11":
                    case "12":
                    case "CA":
                        oConnection.Open(modCommon.sIngresDSNCA);
                        break;
                    default:
                        oConnection.Open(modCommon.sIngresDSN);
                        break;
                }
                strTempTableName = "so_inst_info";
                strStoredProcName = "dp_get_so_inst_info";

                sSQL = string.Concat("declare global temporary table session.", strTempTableName, " as ", "select * from ", strTempTableName, " on commit preserve rows with norecovery");

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                oCommand.Execute(out objResult);

                sSQL = string.Concat("insert into session.", strTempTableName, " (commit_ind, inst_uid_no, ins_co_uid_no, ins_reg_uid_no, lynx_pgm_no, msg_ind, user_id_no, user_session_no) values ('Y' ,", lngInstallerUidNo, " , ", lngInsCompanyUIdNo, ", ", lngInsRegionUIdNo, ", ", lngLynxPgmNo, ", 'N','", strUserIdNo, "',  '", strUserSessionNo, "')");

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objResult);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = string.Concat("{ ? = call lynxdba.", strStoredProcName, " (dummy=session.", strTempTableName, ")}");
                oCommand.Parameters.Append(oCommand.CreateParameter("retval", ADODB.DataTypeEnum.adInteger, ADODB.ParameterDirectionEnum.adParamReturnValue));
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objResult);

                sSQL = string.Concat("select *", " from session.", strTempTableName);

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, activeCon,CursorTypeEnum.adOpenForwardOnly,LockTypeEnum.adLockReadOnly);

                sSQL = string.Concat("drop table session.", strTempTableName);

                oCommand.CommandType =CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objResult);
                oCommand.ActiveConnection.CommitTrans();

                objinstaller = objmodDataAccess.RsToXML(oRecordset, true, "inst", "instinfo");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCommand = null;
                oConnection = null;
                oRecordset = null;
            }
            return objinstaller;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXmlDom = null;
            IXMLDOMParseError tempoXmlDom = null;
            objmodCommon = new modCommon();
            try
            {
                objmodCommon = new modCommon();
                tempoXmlDom = oXmlDom.parseError;
                sOutput = objmodCommon.GetConfiguration("lynxAccount4");
                oXmlDom.async = false;
                oXmlDom.loadXML(sOutput);
                if (oXmlDom.parseError.errorCode == 0)
                {
                    modCommon.sCairDSN = oXmlDom.selectSingleNode("//DataSources/Cair").text;
                    modCommon.sIngresDSN = oXmlDom.selectSingleNode("//DataSources/Ingres").text;
                    modCommon.sIngresDSNCA = oXmlDom.selectSingleNode("//DataSources/IngresCA").text;
                    modCommon.sStylesheetPath = oXmlDom.selectSingleNode("//Stylesheets").text;
                }
                else
                    objmodCommon.InvalidXML(ref tempoXmlDom);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXmlDom = null;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        private void ObjectControl_Deactivate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
