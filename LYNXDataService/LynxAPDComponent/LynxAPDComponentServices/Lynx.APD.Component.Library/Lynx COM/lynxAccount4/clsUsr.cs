﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMSVCSLib;
using System.Runtime.InteropServices;
using MSXML2;
using ADODB;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class clsUsr
    {
        #region Global Varialble Declaration
        modDataAccess mobjDataAccess = null;
        modCommon mobjCommon = null;
        private ObjectContext moObjectContext = null;
        //Define the Application name to be used in Error Handling
        private const string APP_NAME = "lynxAccount4.";
        private const string MODULE_NAME = "clsUsr.";
        #endregion

        #region Public Functions
        // DATE CREATED        : Sep 2002
        // AUTHOR              : John Walker (I&S IT)
        // DESCRIPTION         :
        // This function gets one/many Usr records
        //PARAMETERS
        // sUID                : The UID you want to look up.  "" will return all.
        //                          If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // sUEMailLike         : Part of UEMail you want to search on.  "" will return all.
        //                          Will be used as '%sUEMailLike%'
        // sUEMailEqual        : Exact match on UEMail
        // sULastName          : Part of ULastName you want to search on.  "" will return all.
        //                          Will be used as '%sULastName%'
        // sUFirstName         : Part of UFirstName you want to search on.  "" will return all.
        //                          Will be used as '%sUFirstName%'
        // sUActive            : Whether the account (user) is active
        // sUPPGID             : Part of UPPGID you want to search on.  "" will return all.
        //                          Will be used as '%sUPPGID%'
        // sFields             : Lets you specify which fields to return from the table.  "" will return all.  Can also use *.
        // sStyleSheet         : Allows specification of style sheet to transform data, if no style sheet *.xsl is specified returns xml data set.
        // bPaging             : Tells whether records should be returned in paging fashio
        // iPage               : Tells what page was picked
        // sOrderBy            : Tells what column(s) to sort by
        // bWellFormedXML      : Tells whether the XML should be well formed or not
        // iRows               : Tells how many rows to return per page
        //RETURNS                 :
        //Recordset/XML
        //MODIFICATIONS
        public string GetUsr( string sUID="", [Optional] string sUEMailLike="", [Optional] string sUEMailEqual="", [Optional] string sULastName="",
            [Optional] string sUFirstName="", [Optional] string sUActive="", [Optional] string sUPPGID="", [Optional] string sFields="", [Optional] string sStylesheet="",
            [Optional] bool bPaging=false, [Optional] int iPage=0, [Optional] string sOrderBy="", [Optional] bool bWellFormedXML=false, [Optional] int iRows=0)
        {
            string sOutput = string.Empty;
            string sTotalPages = string.Empty;

            //xml objects
            DOMDocument40 oXmlDom = null;
            DOMDocument40 oXSLDoc = null;

            //create the ADO objects
            Connection oConnection = null;
            Recordset oRecordset = null;
            Command oCmd = null;

            IXMLDOMParseError tempDoc = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                oConnection = new Connection();
                oRecordset = new Recordset();
                oCmd = new Command();
                mobjDataAccess = new modDataAccess();
                mobjCommon = new modCommon();

                oXmlDom = new DOMDocument40();
                oXSLDoc = new DOMDocument40();

                sOrderBy = "UEMail";
                iRows = 10;
                iPage = 1;
                bPaging = false;
                bWellFormedXML = true;

                oConnection.Open(modCommon.sCairDSN);
                oCmd.CommandText = "spUsrGet";
                oCmd.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCmd.ActiveConnection = oConnection;

                //.Parameters.Refresh
                oCmd.Parameters[0].Value = 0;
                oCmd.Parameters[1].Value = sUID;
                oCmd.Parameters[2].Value = sUEMailLike;
                oCmd.Parameters[3].Value = sUEMailEqual;
                oCmd.Parameters[4].Value = sULastName;
                oCmd.Parameters[5].Value = sUFirstName;
                oCmd.Parameters[6].Value = sUActive;
                oCmd.Parameters[7].Value = sUPPGID;
                oCmd.Parameters[8].Value = sFields;
                oCmd.Parameters[9].Value = (Convert.ToBoolean(iPage = 0) ? 1 : iPage);
                oCmd.Parameters[10].Value = bPaging;
                oCmd.Parameters[11].Value = sOrderBy;
                oCmd.Parameters[12].Value = iRows;

                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCmd, string.Empty, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly);
                sTotalPages = Convert.ToString((Convert.ToBoolean(oCmd.Parameters[0].Value = null) ? 0 : oCmd.Parameters[0].Value));

                //process the recordset
                sOutput = mobjDataAccess.RsToXML(oRecordset, bWellFormedXML, "user", "usr");
                tempDoc = oXSLDoc.parseError;
                tempDom = oXmlDom.parseError;
                if (sStylesheet != string.Empty)
                {
                    //=transform=and=style========================================
                    //parse the xml document
                    //oXmlDom = CreateObject("MSXML2.DOMDocument.4.0");
                    oXmlDom.async = false;
                    oXmlDom.loadXML(sOutput);
                    if (oXmlDom.parseError.errorCode == 0)
                    {
                        // oXSLDoc = CreateObject("MSXML2.DOMDocument.4.0");
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));
                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //if no errors then transform the XML
                            //into HTML using the stylesheet
                            sOutput = oXmlDom.transformNode(oXSLDoc);
                        }
                        else
                            mobjCommon.InvalidXML(ref tempDoc);
                    }
                    else
                        mobjCommon.InvalidXML(ref tempDom);
                    //=transform=and=style========================================
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oRecordset = null;
                oConnection = null;
                oCmd = null;
                oXSLDoc = null;
                oXmlDom = null;
            }
            return sOutput;
        }

        // DATE CREATED        : September 2003
        // AUTHOR              : John Walker (I&S IT)
        // DESCRIPTION         :
        // This function inserts a Usr record
        //PARAMETERS
        // UEMail              : The account's email address
        // ULastName           : User's Last Name
        // UFirstName          : User's First Name
        // UActive             : Active (T/F)
        // UPPGID              : User's PPGID (If applicable)
        //MODIFICATIONS
        public void InsertUsr(string sUEMail, string sULastName, string sUFirstName, bool sUActive, [Optional] ref string sUID, [Optional] string sUPPGID, [Optional] string sUPwd)
        {
            string sSQL = string.Empty;

            //create the ADO objects
            Connection oConnection = null;
            Command oCmd = null;
            object objtemp = null;
            try
            {
                mobjCommon = new modCommon();
                oConnection = new Connection();
                oCmd = new Command();

                oConnection.Open(modCommon.sCairDSN);

                oCmd.CommandText = "spUsrInsert";
                oCmd.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCmd.ActiveConnection = oConnection;
                //.Parameters.Refresh;
                oCmd.Parameters[0].Value = 0;
                oCmd.Parameters[1].Value = sUEMail;
                oCmd.Parameters[2].Value = sULastName;
                oCmd.Parameters[3].Value = sUFirstName;
                oCmd.Parameters[4].Value = Convert.ToInt32(sUActive);
                oCmd.Parameters[5].Value = sUPPGID;
                oCmd.Parameters[6].Value = sUID;
                oCmd.Parameters[7].Value = sUPwd;
                oCmd.Execute(out objtemp);

                sUID = Convert.ToString(oCmd.Parameters[6].Value);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
                oCmd = null;
            }
        }

        //     DATE CREATED        : September 2003
        //     AUTHOR              : John Walker (I&S IT)
        //     DESCRIPTION         :
        //     This function inserts a Usr record
        //PARAMETERS
        //     UID                 : The user's ID #
        //     UEMail              : The account's email address
        //     ULastName           : User's Last Name
        //     UFirstName          : User's First Name
        //     UActive             : Active (T/F)
        //     UPPGID              : User's PPGID (If applicable)

        //MODIFICATIONS
        public void UpdateUsr(long UID, string UEMail, string ULastName, string UFirstName, bool UActive, [Optional] string UPPGID, [Optional] string UPwd)
        {
            string sSQL = string.Empty;
            Connection oConnection = null;
            object objtemp = null;
            try
            {
                //create the ADO objects
                oConnection = new Connection();
                mobjCommon = new modCommon();

                sSQL = string.Concat("EXEC spUsrUpdate ", Convert.ToString(UID), ", '", UEMail, "', '", ULastName, "', '",
            UFirstName, "', ", Convert.ToString(UActive), ", '", UPPGID, "', '", UPwd, "'");
                oConnection.Open(modCommon.sCairDSN);
                oConnection.Execute(sSQL, out objtemp);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
            }
        }

        //     DATE CREATED        : January 2005
        //     AUTHOR              : John Walker (I&S IT)
        //     DESCRIPTION         :
        //     This function updates the user's password that we are keeping
        //PARAMETERS
        //     UEMail              : The account's email address
        //     UPwd                : User's Last Name
        //MODIFICATIONS
        public void UpdateUsrPassword(string UEMail, string UPwd)
        {
            string sSQL = string.Empty;
            Connection oConnection = null;
            object objtemp = null;
            try
            {
                oConnection = new Connection();
                mobjCommon = new modCommon();

                sSQL = string.Concat("EXEC spUsrPwdUpdate '", UEMail, "', '", UPwd, "'");
                oConnection.Open(modCommon.sCairDSN);
                oConnection.Execute(sSQL, out objtemp);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
            }
        }



        public bool ObjectControl_CanBePooled()
        {
            return true;
        }
        #endregion

        #region Private Functions
        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            //xml objects
            DOMDocument40 oXmlDom = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                oXmlDom = new DOMDocument40();
                mobjCommon = new modCommon();
                sOutput = mobjCommon.GetConfiguration("lynxAccount4");
                oXmlDom.async = false;
                oXmlDom.loadXML(sOutput);
                tempDom = oXmlDom.parseError;
                if (oXmlDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    modCommon.sCairDSN = oXmlDom.selectSingleNode("//DataSources/Cair").text;
                    modCommon.sIngresDSN = oXmlDom.selectSingleNode("//DataSources/Ingres").text;
                    modCommon.sStylesheetPath = oXmlDom.selectSingleNode("//Stylesheets").text;
                }
                else
                    mobjCommon.InvalidXML(ref tempDom);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}
