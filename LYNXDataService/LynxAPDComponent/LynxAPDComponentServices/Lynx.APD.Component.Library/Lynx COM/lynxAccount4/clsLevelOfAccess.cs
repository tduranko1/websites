﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Xml;
using ADODB;
using MSXML2;
using COMSVCSLib;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class clsLevelOfAccess
    {
        #region Global Variable Declaration
        modDataAccess mobjDataAccess = null;
        modCommon mobjCommon = null;

        private ObjectContext moObjectContext = null;
        //Define the Application name to be used in Error Handling
        private const string APP_NAME = "lynxAccount4.";
        private const string MODULE_NAME = "clsLevelOfAcess.";
        #endregion

        #region Public Functions

        // CREATED        : Sep 2002
        // AUTHOR              : John Walker (I&S IT)
        // DESCRIPTION         :
        // This function gets one/many UserType records
        // PARAMETERS
        // sLOAID              : The LOAID you want to look up.  "" will return all.
        //                       If more than one, put in as (1, 2, 3) to be used with an IN clause.
        // sLOALevel           : The number value of the level of access
        // sLOALevelRelat      : The relationship to us (=, >, >=, <, <=)
        // sLOAName            : Part of LOAName you want to search on.  "" will return all.
        //                       Will be used as '%sLOAName%'
        // sFields             : Lets you specify which fields to return from the table.  "" will return all.  Can also use *.
        // sStyleSheet         : Allows specification of style sheet to transform data, if no style sheet *.xsl is specified returns xml data set.
        // bPaging             : Tells whether records should be returned in paging fashio
        // iPage               : Tells what page was picked
        // sOrderBy            : Tells what column(s) to sort by
        // bWellFormedXML      : Tells whether the XML should be well formed or not
        // iRows               : Tells how many rows to return per page
        // RETURNS             :
        // Recordset/XML
        // MODIFICATIONS
        public string GetLevelOfAccess([Optional] string sLOAID, [Optional] string sLOALevel, [Optional] string sLOALevelRelat,
            [Optional] string sLOAName, [Optional] string sFields, [Optional] string sStylesheet, [Optional] bool bPaging,
            [Optional] int iPage, [Optional] string sOrderBy, [Optional] bool bWellFormedXML, [Optional] int iRows)
        {
            string sSQL = string.Empty;
            string sOutput = string.Empty;
            string sTotalPages = string.Empty;

            //xml objects
            DOMDocument40 oXmlDom = null;
            DOMDocument40 oXSLDoc = null;

            //create the ADO objects
            Connection oConnection = null;
            Recordset oRecordset = null;
            Command oCmd = null;
            IXMLDOMParseError tempdoc = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                oConnection = new Connection();
                oRecordset = new Recordset();
                oCmd = new Command();
                mobjDataAccess = new modDataAccess();
                mobjCommon = new modCommon();

                oXmlDom = new DOMDocument40();
                oXSLDoc = new DOMDocument40();

                sLOALevelRelat = "=";
                sOrderBy = "LOAName";
                iRows = 10;
                iPage = 1;
                bPaging = false;
                bWellFormedXML = true;

                oConnection.Open(modCommon.sCairDSN);
                oCmd.CommandText = "spLevelOfAccessGet";
                oCmd.CommandType = ADODB.CommandTypeEnum.adCmdStoredProc;
                oCmd.ActiveConnection = oConnection;
                //.Parameters.Refresh
                oCmd.Parameters[0].Value = 0;
                oCmd.Parameters[1].Value = sLOAID;
                oCmd.Parameters[2].Value = sLOALevel;
                oCmd.Parameters[3].Value = sLOALevelRelat;
                oCmd.Parameters[4].Value = sLOAName;
                oCmd.Parameters[5].Value = sFields;
                oCmd.Parameters[6].Value = (Convert.ToBoolean(iPage = 0) ? 1 : iPage);
                oCmd.Parameters[7].Value = bPaging;
                oCmd.Parameters[8].Value = sOrderBy;
                oCmd.Parameters[9].Value = iRows;

                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCmd, string.Empty, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly);
                sTotalPages = Convert.ToString((Convert.ToBoolean(oCmd.Parameters[0].Value = null) ? 0 : oCmd.Parameters[0].Value));

                //process the recordset
                sOutput = mobjDataAccess.RsToXML(oRecordset, bWellFormedXML, "levelofaccess", "loa");
                if (sStylesheet != string.Empty)
                {
                    //=transform=and=style========================================
                    //parse the xml document
                    oXmlDom.async = false;
                    oXmlDom.loadXML(sOutput);
                    if (oXmlDom.parseError.errorCode == 0)
                    {
                        // oXSLDoc = CreateObject("MSXML2.DOMDocument.4.0");
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));

                        tempdoc = oXSLDoc.parseError;
                        tempDom = oXmlDom.parseError;
                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //if no errors then transform the XML
                            //into HTML using the stylesheet
                            sOutput = oXmlDom.transformNode(oXSLDoc);
                        }
                        else
                            mobjCommon.InvalidXML(ref tempdoc);
                    }
                    else
                        mobjCommon.InvalidXML(ref tempDom);
                    //=transform=and=style========================================
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oRecordset = null;
                oConnection = null;
                oCmd = null;
                oXSLDoc = null;
                oXmlDom = null;
            }
            return sOutput;
        }
        #endregion

        #region Private Functions
        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXmlDom = null;
            try
            {
                //xml objects
                oXmlDom = new DOMDocument40();
                mobjCommon = new modCommon();
                IXMLDOMParseError tempDom = null;

                sOutput = mobjCommon.GetConfiguration("lynxAccount4");
                oXmlDom.async = false;
                oXmlDom.loadXML(sOutput);
                tempDom = oXmlDom.parseError;
                if (oXmlDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    modCommon.sCairDSN = oXmlDom.selectSingleNode("//DataSources/Cair").text;
                    modCommon.sIngresDSN = oXmlDom.selectSingleNode("//DataSources/Ingres").text;
                    modCommon.sStylesheetPath = oXmlDom.selectSingleNode("//Stylesheets").text;
                }
                else
                    mobjCommon.InvalidXML(ref tempDom);

                //=get=configuration==========================
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXmlDom = null;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            return true;
        }

        private void ObjectControl_Deactivate()
        {

        }
        #endregion
    }
}
