﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class modConstantsVariables
    {
        #region Constants
        // DATE CREATED: Oct 2003
        // AUTHOR: Shane Gates
        // DESCRIPTION:
        // This module contains common declarations
        // used by the Lynx Account 4
        // MODIFICATIONS:

        //ID of Lynx Application
        public const long zLynxAppId = 52;
        public const long zAcctAdminAppId = 55;

        //User Types
        public const long zInstallerType = 1;
        public const long zIncoType = 2;
        public const long zPPGLynxType = 3;
        public const long zGPAUserType = 4;
        public const long zCAInstallerType = 10;
        public const long zCAIncoType = 11;
        public const long zCAGPAUserType = 12;
        public const long zDealerType = 13;

        //Constants for Account Codes
        //=====
        //No record exists in the Lynx SQL Server Database, even though it does in AWARE
        public const long zAccountNotExist = 1000;

        //No Lynx Application specific records exist in the SQL Server Database
        public const long zNoLynxRecord = 1001;

        //Account was deactivated at the account level
        public const long zAcctInactive = 1002;

        //Account was deactivated at the application level
        public const long zLynxAcctInactive = 1003;

        //Account is not associated with any features to even check security against
        public const long zNoFeaturesExist = 1004;

        //Variables to be used for error handling throughout
        public long lAcctErrorNumber = 0;
        public string sAcctErrorDescription = string.Empty;
        public long lAcctErl = 0; 
        #endregion
    }
}
