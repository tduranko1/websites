﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;
using System.Runtime.InteropServices;
using ADODB;
using MSXML2;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class clsUserType
    {
        private ObjectContext moObjectContext = null;
        private modDataAccess objmodDataAccess = null;
        private modCommon objmodCommon = null;

        //Define the Application name to be used in Error Handling
        private string APP_NAME = "lynxAccount4.";
        private string MODULE_NAME = "clsUserType.";

        /// <summary>
        ///  This function deletes all occurences where there is a relationship
        /// of the pair of UTID/COID
        /// </summary>
        /// <param name="UTID"></param>
        /// <param name="COID"></param>
        public void DeleteUserTypeFeature(string UTID, string COID)
        {
            string sSQL = string.Empty;
            Connection oConnection = null;
            object objResult;
            try
            {
                //create the ADO objects
                oConnection = new Connection();
                sSQL = string.Concat("EXEC spUserTypeFeatureDelete ", UTID, ", ", COID);
                oConnection.Open(modCommon.sCairDSN);
                oConnection.Execute(sSQL, out objResult);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
            }
        }

        /// <summary>
        /// This function gets one/many UserType records
        /// </summary>
        /// <returns></returns>
        public string GetUserType([Optional]string sUTID, [Optional]string sUTName, [Optional]string sCOID, [Optional]string sUTIngresType,
                                      [Optional]string sFields, [Optional]string sStylesheet, [Optional]bool bPaging = false, [Optional]int iPage = 1,
                                      [Optional]string sOrderBy = "UTName", [Optional]bool bWellFormedXML = true, [Optional]int iRows = 10)
        {
            string sSQL = string.Empty;
            string sOutput = string.Empty;
            string sTotalPages = string.Empty;
            object objMissing = Type.Missing;
            DOMDocument40 oXmlDom = null;
            DOMDocument40 oXSLDoc = null;
            IXMLDOMParseError tempoXmlDom = null;
            IXMLDOMParseError tempoXsldoc = null;
            Connection oConnection = null;
            Recordset oRecordset = null;
            Command oCmd = null;
            try
            {
                objmodDataAccess = new modDataAccess();
                objmodCommon = new modCommon();

                //create the ADO objects
                oConnection = new Connection();
                oRecordset = new Recordset();
                oCmd = new Command();

                //xml objects
                oXmlDom = new DOMDocument40();
                oXSLDoc = new DOMDocument40();
                tempoXmlDom = oXmlDom.parseError;
                tempoXsldoc = oXSLDoc.parseError;
                oConnection.Open(modCommon.sCairDSN);

                oCmd.CommandText = "spUserTypeGet";
                oCmd.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCmd.ActiveConnection = oConnection;
                //'.Parameters.Refresh
                oCmd.Parameters[0].Value = 0;
                oCmd.Parameters[1].Value = sUTID;
                oCmd.Parameters[2].Value = sUTName;
                oCmd.Parameters[3].Value = sCOID;
                oCmd.Parameters[4].Value = sUTIngresType;
                oCmd.Parameters[5].Value = sFields;
                oCmd.Parameters[6].Value = (iPage == 0 ? 1 : iPage);
                oCmd.Parameters[7].Value = bPaging;
                oCmd.Parameters[8].Value = sOrderBy;
                oCmd.Parameters[9].Value = iRows;

                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCmd, objMissing, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly);
                sTotalPages = (Convert.ToBoolean((oCmd.Parameters[0].Value = null)) ? 0 : oCmd.Parameters[0].Value).ToString();

                //process the recordset
                sOutput = objmodDataAccess.RsToXML(oRecordset, bWellFormedXML, "usertype", "ut");

                if (sStylesheet != "")
                {
                    //'=transform=and=style========================================
                    //parse the xml document
                    oXmlDom.async = false;
                    oXmlDom.loadXML(sOutput);

                    if (oXmlDom.parseError.errorCode == 0)
                    {
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));

                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //f no errors then transform the XML
                            // into HTML using the stylesheet
                            sOutput = oXmlDom.transformNode(oXSLDoc);
                        }
                        else
                            objmodCommon.InvalidXML(ref tempoXsldoc);
                    }
                    else
                        objmodCommon.InvalidXML(ref tempoXmlDom);
                }
                //=transform=and=style========================================
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oRecordset = null;
                oCmd = null;
                oConnection = null;
                oXSLDoc = null;
                oXmlDom = null;
            }
            return sOutput;
        }

        /// <summary>
        /// This function gets one/many UserType records
        /// </summary>
        /// <returns></returns>
        public string GetUserTypeFeature([Optional]string sUTID, [Optional]string sCOID, [Optional]string sStylesheet, [Optional]bool bWellFormedXML = true)
        {
            string sSQL = string.Empty;
            string sOutput = string.Empty;
            object activeConection = Type.Missing;

            //xml objects
            DOMDocument40 oXmlDom = null;
            DOMDocument40 oXSLDoc = null;
            IXMLDOMParseError tempoXmlDom = null;
            IXMLDOMParseError tempoXSLDoc = null;

            //create the ADO objects
            Connection oConnection = null;
            Recordset oRecordset = null;
            Command oCmd = null;

            try
            {
                oXmlDom = new DOMDocument40();
                objmodDataAccess = new modDataAccess();
                objmodCommon = new modCommon();
                oXSLDoc = new DOMDocument40();
                oConnection = new Connection();
                oRecordset = new Recordset();
                oCmd = new Command();
                tempoXmlDom = oXmlDom.parseError;
                tempoXSLDoc = oXSLDoc.parseError;

                oConnection.Open(modCommon.sCairDSN);

                oCmd.CommandText = "spUserTypeFeatureGet";
                oCmd.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCmd.ActiveConnection = oConnection;

                oCmd.Parameters[0].Value = 0;
                oCmd.Parameters[1].Value = sUTID;
                oCmd.Parameters[2].Value = sCOID;

                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCmd, activeConection, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly);

                //process the recordset
                sOutput = objmodDataAccess.RsToXML(oRecordset, bWellFormedXML, "usertypefeature", "utf");

                if (sStylesheet != "")
                {
                    //=transform=and=style
                    //parse the xml document
                    oXmlDom.async = false;
                    oXmlDom.loadXML(sOutput);

                    if (oXmlDom.parseError.errorCode == 0)
                    {
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));

                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //if no errors then transform the XML
                            //into HTML using the stylesheet
                            sOutput = oXmlDom.transformNode(oXSLDoc);
                        }
                        else
                            objmodCommon.InvalidXML(ref tempoXSLDoc);
                    }
                    else
                        objmodCommon.InvalidXML(ref tempoXmlDom);
                }
                //transform=and=style

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oRecordset = null;
                oConnection = null;
                oCmd = null;
                oXSLDoc = null;
                oXmlDom = null;
            }
            return sOutput;
        }

        /// <summary>
        /// This function inserts a UserTypeFeature record for this combination,
        /// then a trigger is fired to insert records for every user of that user
        /// type.
        /// </summary>
        /// <param name="UTID"></param>
        public void InsertUserTypeFeature(string UTID, string COID, [Optional]bool bSelfMaintained = false)
        {
            //create the ADO objects
            Connection oConnection = null;
            string sSQL = string.Empty;
            object objResult;
            try
            {
                oConnection = new Connection();
                sSQL = string.Concat("EXEC spUserTypeFeatureInsert ", UTID, ", ", COID, ", ", Convert.ToString(bSelfMaintained));//Todo
                oConnection.Open(modCommon.sCairDSN);
                oConnection.Execute(sSQL, out objResult);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
            }
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXmlDom = null;
            IXMLDOMParseError tempoXmlDom = null;
            try
            {
                //xml objects
                oXmlDom = new DOMDocument40();
                objmodCommon = new modCommon();
                tempoXmlDom = oXmlDom.parseError;
                sOutput = objmodCommon.GetConfiguration("lynxAccount4");
                oXmlDom.async = false;
                oXmlDom.loadXML(sOutput);
                if (oXmlDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    modCommon.sCairDSN = oXmlDom.selectSingleNode("//DataSources/Cair").text;
                    modCommon.sIngresDSN = oXmlDom.selectSingleNode("//DataSources/Ingres").text;
                    modCommon.sStylesheetPath = oXmlDom.selectSingleNode("//Stylesheets").text;
                }
                else
                    objmodCommon.InvalidXML(ref tempoXmlDom);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXmlDom = null;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        private void ObjectControl_Deactivate()
        {
            try
            {
                //Cleanup
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
