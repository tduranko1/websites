﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;
using System.Diagnostics;
using MSXML2;
using COMSVCSLib;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class modCommon
    {
        #region Global Variable Declarations
        private const string zMODULE = "modCommon";

        //declarations used by GetConnectionString()
        private string g_sConnectionString = string.Empty;
        private string g_sEnvId = string.Empty;
        public static string sCairDSN = string.Empty;
        public static string sStylesheetPath = string.Empty;
        public static string sIngresDSN = string.Empty;
        public static string sIngresDSNCA = string.Empty;
        public static string sFnolExDSN = string.Empty;
        public static bool bActivateLog = false;
        public static string sXmlPath = string.Empty;

        //Public sXmlPathCA As String

        //declarations used by error methods
        public const long lynxErrorBase = Constants.vbObjectError + 1000;
        public const long lynxDatabaseError = lynxErrorBase + 1000;

        //declarations used by Registry API calls
        public const long REG_SZ = 1;
        public const long HKEY_LOCAL_MACHINE = 0x80000002;
        public const long ERROR_NONE = 0;
        public const long ERROR_BADDB = 1;
        public const long ERROR_BADKEY = 2;
        public const long ERROR_CANTOPEN = 3;
        public const long ERROR_CANTREAD = 4;
        public const long ERROR_CANTWRITE = 5;
        public const long ERROR_OUTOFMEMORY = 6;
        public const long ERROR_ARENA_TRASHED = 7;
        public const long ERROR_ACCESS_DENIED = 8;
        public const long ERROR_INVALID_PARAMETERS = 87;
        public const long ERROR_NO_MORE_ITEMS = 259;
        public const long KEY_QUERY_VALUE = 0x1;
        public const long KEY_SET_VALUE = 0x2;
        public const long KEY_ALL_ACCESS = 0x3F;
        public const long REG_OPTION_NON_VOLATILE = 0;
        #endregion

        #region API Declarations
        //API Declarations for reading from the Registry
        [DllImport("ole32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public void SetErrorInfo(long dwReserved, long perrinfo);

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private long RegCloseKey(long hKey);

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private long RegOpenKeyEx(long hKey, string lpSubKey, long ulOptions, long samDesired, long phkResult);

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private long RegQueryValueExString(long hKey, string lpValueName, long lpReserved, long lpType, string lpData, long lpcbData);

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private long RegQueryValueExNULL(long hKey, string lpValueName, long lpReserved, long lpType, string lpData, long lpcbData);

        [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Auto)]
        private long GetComputerNameAPI(string lpBuffer, long nSize);

        [DllImport("comsvcs.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private ObjectContext GetObjectContext();
        #endregion

        #region Public Functions
        public string Left(string strParam, int intLength)
        {
            string strResult = strParam.Substring(0, intLength);
            return strResult;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string strParam, int intLength)
        {
            string strResult = strParam.Substring(strParam.Length - intLength, intLength);
            return strResult;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = strParam.Substring(startIndex, intLength);
            return strResult;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = strParam.Substring(intstartIndex);
            return strResult;
        }

        ///LogEvent(SUB)
        ///Parameters:
        ///(IN) ModuleName - String - Module name of caller
        ///(IN) Message - String - Message text
        ///Purpose:
        ///writes an Error type event to the Event Log
        ///
        //TODO
        public void LogEvent(string Source, string Message, EventLogEntryType EventType)
        {
            EventLog CeventLog = null;
            try
            {
                CeventLog = new EventLog();
                CeventLog.Source = Source;
                EventLog.WriteEntry(string.Concat(Source, ": ", Message), Message, EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void ClearErrorInfo()
        {
            SetErrorInfo(0, 0);
        }

        // SetErrorSource(FUNCTION)

        //Parameters:
        //(IN) sProcedure - String - Procedure name of caller
        //(IN) sModule - Module name of caller
        //Returns:
        //Returns an error message like:  "[LYNXsvcs1_db.Account] Authenticate [on AHI version 5.21.176]"
        //Purpose:
        //used by error reporting methods
        public string SetErrSource(string sModule, string sProcedure, string sLineNumber)
        {
            string sBuffer = string.Empty;
            long lLength = 0;
            string sComputerName = string.Empty;
            string retErrSource = string.Empty;
            try
            {
                sBuffer = string.Empty.PadRight(255 + 1);
                lLength = sBuffer.Length;
                if (Convert.ToBoolean(GetComputerNameAPI(sBuffer, lLength)))
                    sComputerName = Left(sBuffer, Convert.ToInt32(lLength));
                else
                    sComputerName = string.Empty;

                retErrSource = string.Concat("[", sModule, "]  ", sProcedure, " Line ", sLineNumber);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return retErrSource;
        }

        //        CtxRaiseError(FUNCTION)
        //Parameters:
        //(IN) ModuleName - String - Module name of caller
        //(IN) FunctionName - String - Function name of caller
        //Purpose:
        //saves the error information before calling CtxSetAbort in case it has side effects
        public string CtxRaiseError(string ModuleName, string FunctionName,
                        long lErr, string sErr, long lErl,
                         bool ReturnXML = false,
                        bool blnLogError = true,
                         bool sAlrtInd = true,
                         int sInd = 4)
        {
            string sErrSource = string.Empty;
            MXXMLWriter40 oMXXMLWriter = null;
            IVBSAXContentHandler oContentHandler = null;
            SAXAttributes40 oSAXAttributes = null;
            string sOutput = string.Empty;
            string retoutput = string.Empty;

            try
            {
                sErrSource = SetErrSource(ModuleName, FunctionName, Convert.ToString(lErl));
                CtxSetAbort();
                if (ReturnXML)
                {
                    //Build the Request Xml from the input parameters
                    //Replaced a string xml build with a SAX build - Abhijeet Wadkar 03/29/2004
                    oMXXMLWriter = new MXXMLWriter40();
                    oContentHandler = (IVBSAXContentHandler)oMXXMLWriter;
                    oSAXAttributes = new SAXAttributes40();

                    oMXXMLWriter.omitXMLDeclaration = true;
                    oContentHandler.startDocument();
                    oSAXAttributes.clear();
                    oContentHandler.startElement("", "", "error", (IVBSAXAttributes)oSAXAttributes);
                    oSAXAttributes.clear();
                    oContentHandler.startElement("", "", "number", (IVBSAXAttributes)oSAXAttributes);
                    oContentHandler.characters(Convert.ToString(lErr));
                    oContentHandler.endElement("", "", "number");
                    oContentHandler.startElement("", "", "source", (IVBSAXAttributes)oSAXAttributes);
                    oContentHandler.characters(Convert.ToString(sErrSource));
                    oContentHandler.endElement("", "", "source");
                    oContentHandler.startElement("", "", "description", (IVBSAXAttributes)oSAXAttributes);
                    oContentHandler.characters(Convert.ToString(sErr));
                    oContentHandler.endElement("", "", "description");
                    oContentHandler.startElement("", "", "log", (IVBSAXAttributes)oSAXAttributes);
                    oContentHandler.characters(Convert.ToString(blnLogError));
                    oContentHandler.endElement("", "", "log");
                    oContentHandler.endElement("", "", "error");
                    //convert the error to XML
                    sOutput = Convert.ToString(oMXXMLWriter.output);
                }
                if (sOutput == string.Empty)
                    sOutput = sErr;
                if (blnLogError == true)
                {
                    LogEvent(string.Concat(Convert.ToString(lErl), ModuleName), string.Concat(sErrSource, sOutput), EventLogEntryType.Error);//TODO: Need to verify
                    LogToAppLog(sErrSource, sOutput, "", sInd, 0);
                }
                if (ReturnXML == false)
                {
                    //bubble the event upwards
                    throw new Exception(string.Concat(Convert.ToString(lErr), ",", sErrSource, ",", sErr));
                }
                else
                    retoutput = sOutput;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oMXXMLWriter = null;
                oContentHandler = null;
                oSAXAttributes = null;
            }
            return retoutput;
        }

        // CtxSetAbort(SUB)
        //Parameters:
        //NONE
        //Purpose:
        //Declares that the transaction in which the
        //object is executing must be aborted, and that
        //the object should be deactivated on returning
        //from the currently executing method call.
        public void CtxSetAbort()
        {
            ObjectContext ctx = null;
            try
            {
                ctx = GetObjectContext();
                if (ctx != null)
                    ctx.SetAbort();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                ctx = null;
            }
        }

        // CtxSetComplete(SUB)
        //Parameters:
        //NONE
        //Purpose:
        //Declares that the current object has completed
        //its work and should be deactivated when the
        //currently executing method returns to the client.
        //For objects that are executing within the scope
        //of a transaction, it also indicates that the object's
        //transactional updates can be committed.
        public void CtxSetComplete()
        {
            ObjectContext ctx = null;
            try
            {
                ctx = GetObjectContext();
                if (ctx != null)
                    ctx.SetComplete();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                ctx = null;
            }
        }

        // GetConnectionString(FUNCTION)
        //Parameters:
        //(IN) sEnvId - String - holds the calling environment
        // in the form: group_apptype_env
        // ====================
        //           Group
        //           AGC (Auto Glass Claims)
        //           APD (Auto Physical Damage)
        //           FNOL (First Notice of Loss)
        //           DWHS (Data Warehouse)
        //           ====================
        //           AppType
        //           WEB (Web Applications)
        //           IVR (IVR Applications)
        //           APP (CSR Applications)
        //           ====================
        //           Env
        //           D (Development)
        //           E (Education)
        //           T (Test)
        //           P (Production)
        //           ====================

        //Returns:
        //a connection string

        //Purpose:
        //This function takes an environment string and returns the associated connection string.
        public string GetConnectionString(string sEnvId)
        {
            string retconnectionstring = string.Empty;
            try
            {
                if (sEnvId == g_sEnvId)
                    retconnectionstring = g_sConnectionString;
                else
                {
                    g_sEnvId = sEnvId;
                    g_sConnectionString = QueryValue(@"SOFTWARE\LYNXServices\Environments", sEnvId);
                    retconnectionstring = g_sConnectionString;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return retconnectionstring;
        }

        public string GetMachineName()
        {
            long lSize = 0;
            string sBuffer = string.Empty;
            try
            {
                lSize = 255;
                if (Convert.ToBoolean(GetComputerNameAPI(sBuffer, lSize)))
                {
                    lSize = sBuffer.IndexOf(sBuffer, Convert.ToChar(0)) - 1;
                    return Left(sBuffer, Convert.ToInt32(lSize));
                }
                else
                    return string.Empty;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //GetConfiguration(FUNCTION)

        //Parameters:
        //(IN) sComponentName - String:
        //    The name of the component whose configuration is sought

        //Returns:
        //a xml string containing the configuration information
        //for the requested component

        //Special Requirement:
        //   The configuration information needs tobe stored in the following path:
        //   "C:\lynxDlls\lynxEnvironments.xml"

        //Purpose:
        //This function takes a component name and returns the configuration information
        //for that component.
        public string GetConfiguration(string sComponentName)
        {
            string sMachineName = string.Empty;
            string sDataSource = string.Empty;
            // Dim oXmlDom As MSXML2.DOMDocument40
            FreeThreadedDOMDocument40 oXmlDom = null;// changed to the free threaded model - smg 08mar2004
            IXMLDOMElement oCurrentNode = null;
            IXMLDOMElement oMaintenanceNode = null;
            IXMLDOMElement oComponentNode = null;
            IXMLDOMElement oComponentOverride = null;
            IXMLDOMNodeList oNodeList = null;
            IXMLDOMNode oNode = null;
            string sDate = string.Empty;
            string sWeekDay = string.Empty;
            string sStartTime = string.Empty;
            string sEndTime = string.Empty;
            try
            {
                sMachineName = GetMachineName();

                // Set oXmlDom = New MSXML2.DOMDocument40
                oXmlDom = new FreeThreadedDOMDocument40(); // changed to the free threaded model - smg 08mar2004
                oXmlDom.async = false;
                oXmlDom.load(@"C:\lynxDlls\lynxEnvironments.xml");
                if (oXmlDom.parseError.errorCode != 0)
                    throw new Exception(string.Concat(Convert.ToString(oXmlDom.parseError.errorCode), ",", "oXmldom.load", ",", oXmlDom.parseError.reason));

                //Get the environment block under which the current machine is categorised
                oCurrentNode = (IXMLDOMElement)oXmlDom.selectSingleNode(string.Concat("//Environment[Machine[@name='", sMachineName, "']]"));
                //error handling
                if (oCurrentNode == null)
                    throw new Exception(string.Concat(Convert.ToString(Constants.vbObjectError + 10000), ",", "GetConfiguration", ",", string.Concat("Machine name ", sMachineName, " does not exist in available configuration.")));

                //Check if there are any exceptions to the environment being available
                if (oCurrentNode.selectSingleNode("//Availability").childNodes.length > 0)
                {
                    //Check if there is an outage at this time
                    sDate = DateTime.Now.ToString("mm/dd/yyyy");
                    oMaintenanceNode = (IXMLDOMElement)oCurrentNode.selectSingleNode(string.Concat("//Availability/Outage[@date=\"", sDate, "\"]"));
                    if (oMaintenanceNode != null)
                    {
                        sStartTime = Convert.ToString(oMaintenanceNode.getAttribute("startTime"));
                        sEndTime = Convert.ToString(oMaintenanceNode.getAttribute("endTime"));
                        oMaintenanceNode = null;
                        if ((Convert.ToDateTime(sStartTime) < Convert.ToDateTime(DateTime.Now.ToString("hhmm"))) && (Convert.ToDateTime(sEndTime) > Convert.ToDateTime(DateTime.Now.ToString("hhmm"))))
                            throw new Exception(string.Concat(Convert.ToString(Constants.vbObjectError + 11000), ",", "GetConfiguration", ",", "Environment is not available at this time"));
                    }
                }

                //Check if there is a scheduled maintenance at this time
                sWeekDay = Convert.ToString(DateAndTime.DatePart("w", DateAndTime.Now, Constants.vbSunday));
                switch (Convert.ToInt32(sWeekDay))
                {
                    case 1:
                        sWeekDay = "SUN";
                        break;
                    case 2:
                        sWeekDay = "MON";
                        break;
                    case 3:
                        sWeekDay = "TUE";
                        break;
                    case 4:
                        sWeekDay = "WED";
                        break;
                    case 5:
                        sWeekDay = "THU";
                        break;
                    case 6:
                        sWeekDay = "FRI";
                        break;
                    case 7:
                        sWeekDay = "SAT";
                        break;
                }

                oMaintenanceNode = (IXMLDOMElement)oCurrentNode.selectSingleNode(string.Concat("//Availability/ScheduledMaintenance[@dow=\"", sWeekDay, "\"]"));
                if (oMaintenanceNode != null)
                {
                    sStartTime = Convert.ToString(oMaintenanceNode.getAttribute("startTime"));
                    sEndTime = Convert.ToString(oMaintenanceNode.getAttribute("endTime"));
                    oMaintenanceNode = null;
                    if ((Convert.ToDateTime(sStartTime) < Convert.ToDateTime(DateTime.Now.ToString("hhmm"))) && (Convert.ToDateTime(sEndTime) > Convert.ToDateTime(DateTime.Now.ToString("hhmm"))))
                        throw new Exception(string.Concat(Convert.ToString(Constants.vbObjectError + 12000), ",", "GetConfiguration", ",", "Environment is not available at this time"));
                }


                //Set up the component node for the component passed in if it exists under the environment node
                oComponentNode = (IXMLDOMElement)oCurrentNode.selectSingleNode(string.Concat("//Configuration/Components/", sComponentName));
                //error handling
                if (oComponentNode == null)

                    throw new Exception(string.Concat(Convert.ToString(Constants.vbObjectError + 13000), ",", "GetConfiguration", ",", string.Concat("Component information does not exist for ", sComponentName, " on ", sMachineName)));


                oNodeList = oComponentNode.childNodes;
                foreach (IXMLDOMNode tempnode in oNodeList)
                {
                    oNode = tempnode;
                    if (oNode.nodeName != "DataSources")
                    {
                        //Check to see if there is a machine specific override for the component's child elements
                        oComponentOverride = (IXMLDOMElement)oCurrentNode.selectSingleNode(string.Concat("//Machine[@name='", sMachineName, "']/Components/", sComponentName, "/", oNode.nodeName));
                        if (oComponentOverride != null)
                            oComponentNode.replaceChild(oComponentOverride, oNode);
                        else
                        {
                            //Check to see if there is a environment specific override for the component's child elements
                            oComponentOverride = (IXMLDOMElement)oCurrentNode.selectSingleNode(string.Concat("Components/", sComponentName, "/", oNode.nodeName));
                            if (oComponentOverride != null)
                                oComponentNode.replaceChild(oComponentOverride, oNode);
                        }
                    }
                }

                //Check DataSource overrides at the machine specific and environment levels
                if (oComponentNode.selectSingleNode("DataSources") != null)
                {
                    oNodeList = oComponentNode.selectSingleNode("DataSources").childNodes;
                    foreach (IXMLDOMNode tempnode in oNodeList)
                    {
                        oNode = tempnode;
                        //Check to see if there is a machine specific override for the component's datasources
                        oComponentOverride = (IXMLDOMElement)oCurrentNode.selectSingleNode(string.Concat("//Machine[@name='", sMachineName, "']/DataSources/", oNode.nodeName));
                        if (oComponentOverride != null)
                            oComponentNode.selectSingleNode("DataSources").replaceChild(oComponentOverride, oNode);
                        else
                        {
                            //Check to see if there is a environment specific override for the component's child elements
                            oComponentOverride = (IXMLDOMElement)oCurrentNode.selectSingleNode(string.Concat("DataSources/", oNode.nodeName));
                            if (oComponentOverride != null)
                                oComponentNode.selectSingleNode("DataSources").replaceChild(oComponentOverride, oNode);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCurrentNode = null;
                oComponentNode = null;
                oComponentOverride = null;
                oMaintenanceNode = null;
                oNodeList = null;
                oXmlDom = null;
            }
            return oComponentNode.xml;
        }

        //QueryValue(FUNCTION)

        //Parameters:
        //(IN) sKeyName - String - name of key to open in registry
        //(IN) sValueName - String - name of value to retrieve from registry

        //Returns:
        //string containing the queried value

        //Purpose:
        //returns a string based on a key/value in the registry.
        //used by GetConnectionString()
        public string QueryValue(string sKeyName, string sValueName)
        {

            long lRetval = 0; //result of the API functions
            long hKey = 0;//handle of opened key
            object vValue = null;//setting of queried value
            try
            {
                lRetval = RegOpenKeyEx(HKEY_LOCAL_MACHINE, sKeyName, 0, KEY_QUERY_VALUE, hKey);
                lRetval = QueryValueEx(hKey, sValueName, vValue);
                RegCloseKey(hKey);
                return Convert.ToString(vValue);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void InvalidXML(ref IXMLDOMParseError oParseError)
        {

            string sOutput = string.Empty;
            try
            {
                sOutput = string.Concat("The following error occurred while processing the XML document:", Environment.NewLine,
                     oParseError.reason, Environment.NewLine, "line ", Convert.ToString(oParseError.line), " character ", Convert.ToString(oParseError.linepos), Environment.NewLine,
                     oParseError.srcText, Environment.NewLine);
                //oParseError.filepos
                //oParseError.url
                //    Err.Raise lynxErrorBase + 10000, "xml_error", sOutput
                throw new Exception(string.Concat(Convert.ToString(oParseError.errorCode), ",", "xml parse error", ",", sOutput));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void LogToAppLog(string sSource,
                        string sMessage,
                        string sEnvId,
                         int iType = 1,
                        long lAppId = 1)
        {
            object oAppLog = null;
            lynxAppLog.AppLog mobjAppLog = null;
            try
            {
                mobjAppLog = new lynxAppLog.AppLog();

                oAppLog.LogMessage(sSource, sMessage, sEnvId, iType, lAppId);
                oAppLog = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string PreFixUserId(string sUserID)
        {
            int ilen = 0;
            int i = 0;
            string sZero = string.Empty;
            try
            {
                if (Left(sUserID, 1) == "w")
                    return sUserID;
                ilen = 6 - sUserID.Length;
                i = 0;
                while (!(i == ilen))
                {
                    sZero = string.Concat(sZero, "0");
                    i = i + 1;
                }

                sUserID = string.Concat("w", sZero, sUserID);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return sUserID;
        }

        //FormatVHIScreen(SUB)

        //Parameters:
        //Session

        //Purpose:
        //Gives us a view of the screen in VHI when an error occurs
        public string FormatVHIScreen(string sOffset)
        {

            string sScreenOutput = string.Empty;
            int iCount = 0;
            int iLength = 0;
            int iTotal = 0;
            string sScreen = string.Empty;

            //sScreen = Session.GetStringAtOffset(0, 2000)
            try
            {
                iLength = 80;
                iTotal = 2000;

                for (iCount = 0; iCount <= (iTotal / iLength) - 1; iCount++)
                {
                    sScreenOutput = string.Concat(sScreenOutput, Mid(sOffset, (iCount * iLength) + 1, iLength + 1), Environment.NewLine);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return sScreenOutput;
        }

        //Arguments :

        //toSplit               - String to be Splitted
        //SplitChars            - Delimiters that are used for Splitting the String (Default : Space)
        //EscapeChar            - The Escape Character (if precedes a Split Character, the Split
        //                        Character is recognized as a literal) (Default : Back Slash)
        //TrimStrings           - If True, trims the returned Strings in the Array, else the spaces are
        //                        returned as-is.

        //Returns   : The Splitted String Array
        public string[] SplitEx(string toSplit,
         string SplitChars = " ",
         string EscapeChar = @"\",
         bool TrimStrings = true)
        {
            string[] retArray = null;
            string tempString = string.Empty;
            long arrLength = 0;
            bool isEscapeChar = false;
            long FoundPos = 0;
            string[] retSplitEx = null;
            try
            {
                retArray = new string[1];
                arrLength = -1;

                if (EscapeChar != string.Empty)
                    EscapeChar = Left(EscapeChar, 1);

                if (toSplit.Length == 0)
                    retSplitEx = CopyArray(retArray);

                if (SplitChars.Length == 0)
                    retArray[0] = toSplit;
                retSplitEx = CopyArray(retArray);

                retArray = new string[11];

                toSplit = string.Concat(toSplit, Convert.ToChar(0));
                SplitChars = string.Concat(SplitChars, Convert.ToChar(0));

                for (; ; )
                {
                    FoundPos = toSplit.IndexOfAny(SplitChars.ToCharArray());

                    /*
                     The String before the Split Character (if any) should be "splitted"
                     unless there is a Escape Character preceding the Split Character
                    */
                    FoundPos = FoundPos + 1;

                    if (FoundPos == toSplit.Length || FoundPos == 0)
                    {
                        arrLength = arrLength + 1;

                        if (arrLength > Information.UBound(retArray))
                            Array.Resize(ref retArray, Convert.ToInt32(arrLength + 6));
                        // Pre-Allocate more memory
                        if (isEscapeChar)
                            retArray[arrLength] = string.Concat(tempString, toSplit); // Whatever is remaining
                        else
                            retArray[arrLength] = toSplit;
                        if (TrimStrings)
                            retArray[arrLength] = (retArray[arrLength].Trim());
                        break;
                    }
                    else if (FoundPos > 1)
                    {
                        // Check for Escape Character
                        if (Mid(toSplit, Convert.ToInt32(FoundPos - 1), 1) == EscapeChar)
                        {
                            // The Split Character is intepreted as an ordinary literal.
                            isEscapeChar = true;

                            // Remove everything before the Escape Character for temporary storage
                            if (toSplit.Length > 2)
                                tempString = string.Concat(Left(toSplit, Convert.ToInt32(FoundPos) - 2), Mid(toSplit, Convert.ToInt32(FoundPos), 1));
                            else        //The Escape Character is the First Character
                                tempString = Mid(toSplit, Convert.ToInt32(FoundPos));

                            // To avoid "finding" the Escaped Split Character again, rip that part
                            if (toSplit.Length > FoundPos + 1)
                                toSplit = Mid(toSplit, Convert.ToInt32(FoundPos + 1));

                            else
                                // The String has ended with the Escaped Split Character. Now
                                // this is the last part of the string that must be stored in the
                                // Split Array.
                                arrLength = arrLength + 1;

                            if (arrLength > Information.UBound(retArray))
                                Array.Resize(ref retArray, Convert.ToInt32(arrLength + 5));  // Pre-Allocate more memory

                            retArray[arrLength] = tempString;
                            if (TrimStrings)
                                retArray[arrLength] = retArray[arrLength].Trim();
                            break;
                        }
                        else
                        {
                            // No Escape Character
                            arrLength = arrLength + 1;

                            if (arrLength > Information.UBound(retArray))
                                Array.Resize(ref retArray, Convert.ToInt32(arrLength + 5));  // Pre-Allocate more memory

                            //Might have left-overs of an Escaped Split Character
                            if (isEscapeChar)
                            {
                                retArray[arrLength] = string.Concat(tempString, Left(toSplit, Convert.ToInt32(FoundPos - 1)));
                                isEscapeChar = false;
                            }
                            else
                                retArray[arrLength] = Left(toSplit, Convert.ToInt32(FoundPos - 1));

                            if (TrimStrings)
                                retArray[arrLength] = retArray[arrLength].Trim();

                            // Remove that Part
                            if (toSplit.Length > FoundPos + 1)
                                toSplit = Mid(toSplit, Convert.ToInt32(FoundPos + 1));
                            else
                                break;
                            //No more string to be splitted. Re-dimenstion the array.
                        }
                    }
                    else if (FoundPos == 1)
                    {
                        // The Split Character is at the First Position
                        if (toSplit.Length > 1)
                            toSplit = Mid(toSplit, 2);
                        else
                            break;
                    }
                }

                Array.Resize(ref retArray, Convert.ToInt32(arrLength));
                toSplit = string.Empty;
                retSplitEx = CopyArray(retArray);
            }

            catch (Exception ex)
            {

                throw ex;
            }
            return retSplitEx;
        }

        public string Paging(int iTotalPages, int iCurrentPage, int iRange = 5)
        {
            int iLoopCounter = 0;
            string sOutputHTML = string.Empty;
            try
            {
                if (iTotalPages > 1)
                {
                    sOutputHTML = @"<table width=""550"" align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0""><tr><td align=""right"">";

                    if (iCurrentPage != 1)
                    {
                        //Takes you back to the fist page
                        sOutputHTML = string.Concat(sOutputHTML, @"<a href=""#"" onclick=""Paging(1)""><font color=""blue"">", "<<", "</font></a>&nbsp;");
                        //Goes previous
                        sOutputHTML = string.Concat(sOutputHTML, @"<a href=""#"" onclick=""Paging(", Convert.ToString(iCurrentPage - 1), @")""><font color=""blue"">", "<", "</font></a>&nbsp;");
                    }

                    for (iLoopCounter = 1; iLoopCounter <= iTotalPages; iLoopCounter++)
                    {
                        if (iLoopCounter == iCurrentPage)
                        {
                            //Current page, just show as not a link
                            sOutputHTML = string.Concat(sOutputHTML, Convert.ToString(iLoopCounter), "&nbsp;");
                        }
                        else if ((iLoopCounter < (iCurrentPage - iRange)) || (iLoopCounter > (iCurrentPage + iRange)))
                            sOutputHTML = string.Concat(sOutputHTML, @"<a href=""#"" onclick=""Paging(", Convert.ToString(iLoopCounter), @")""><font color=""blue"">", Convert.ToString(iLoopCounter), "</font></a>&nbsp;");
                    }

                    if (iTotalPages != iCurrentPage)
                    {
                        //Go to the next page
                        sOutputHTML = string.Concat(sOutputHTML, @"<a href=""#"" onclick=""Paging(", Convert.ToString(iCurrentPage + 1), @")""><font color=""blue"">", ">", "</font></a>&nbsp;");
                        //Skip to the last page
                        sOutputHTML = string.Concat(sOutputHTML, @"<a href=""#"" onclick=""Paging(", Convert.ToString(iTotalPages), @")""><font color=""blue"">", ">>", "</font></a>&nbsp;");
                    }

                    //This builds the option list of pages to skip directly to
                    if (iTotalPages > iRange)
                    {
                        sOutputHTML = string.Concat(sOutputHTML, @"</tr><tr><td align=""right"">");
                        sOutputHTML = string.Concat(sOutputHTML, "Go to Page ");
                        sOutputHTML = string.Concat(sOutputHTML, @"<select name=""intSkipPage"" id=""intSkipPage"" onchange=""Paging(document.all.intSkipPage[document.all.intSkipPage.selectedIndex].value)"">");
                        for (iLoopCounter = 1; iLoopCounter <= iTotalPages; iLoopCounter++)
                        {
                            sOutputHTML = string.Concat(sOutputHTML, "<option value=", Convert.ToString(iLoopCounter), "");
                            if (iLoopCounter == iCurrentPage)
                                sOutputHTML = string.Concat(sOutputHTML, @" selected=""selected"">");
                            else
                                sOutputHTML = string.Concat(sOutputHTML, ">");

                            sOutputHTML = string.Concat(sOutputHTML, Convert.ToString(iLoopCounter));
                            sOutputHTML = string.Concat(sOutputHTML, "</option>");
                        }
                    }
                    sOutputHTML = string.Concat(sOutputHTML, "</select> of ", Convert.ToString(iTotalPages));
                    sOutputHTML = string.Concat(sOutputHTML, "</td></tr></table>");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return sOutputHTML;
        }
        #endregion

        #region Private Functions
        //QueryValueEx(Function)

        //Parameters:
        //(IN) lhKey - Long - ???
        //(IN) szValueName - String - ???
        //(IN) vValue - Variant - ???
        //Returns:
        // string containing the error, module, procedure and computer name
        //Purpose:
        //returns the source of the error: error, module, procedure, and computer name
        //used by error reporting methods
        private long QueryValueEx(long lhKey, string szValueName, object vValue)
        {
            long cch = 0;
            long lrc = 0;
            long lType = 0;
            long lValue = 0;
            string sValue = string.Empty;

            try
            {
                //Determine the size and type of data to be read
                lrc = RegQueryValueExNULL(lhKey, szValueName, 0L, lType, Convert.ToString(0L), cch);
                //if(lrc != ERROR_NONE) 
                //    Error(5);

                switch (lType)
                {
                    //For strings
                    case REG_SZ:
                        sValue = new string(Convert.ToChar(cch), 0);
                        lrc = RegQueryValueExString(lhKey, szValueName, 0L, lType, sValue, cch);
                        if (lrc == ERROR_NONE)
                            vValue = Left(sValue, Convert.ToInt32(cch - 1));
                        else
                            vValue = string.Empty;
                        break;
                    default:
                        //all other data types not supported
                        lrc = -1;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lrc;
        }

        private string[] CopyArray(string[] srcArray)
        {
            string[] destArray = null;
            int index = 0;
            try
            {
                Array.Resize(ref destArray, Information.UBound(srcArray) + 2);

                for (index = 0; index <= Information.UBound(srcArray); index++)
                {
                    destArray[index + 1] = srcArray[index].Replace(Convert.ToChar(0), '\0');
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return destArray;
        }
        #endregion
    }
}
