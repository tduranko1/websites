﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMSVCSLib;
using System.Runtime.InteropServices;
using ADODB;
using System.Text.RegularExpressions;
using MSXML2;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class clsProfile
    {
        #region Global Variable Declarations
        modCommon mobjCommon = null;
        modDataAccess mobjDataAccess = null;

        private ObjectContext moObjectContext = null;       
        private const string APP_NAME = "lynxAccount4.";
        private const string MODULE_NAME = "clsRegister."; 
        #endregion

        #region Public Functions

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(0, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(param.Length - length, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(startIndex, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(startIndex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// GetProfileItems(FUNCTION)
        /// Parameters:
        /// (IN) sAttribName - String - the attribute name to retrieve
        /// (IN) lProfileUidNo - Long - the uid no for the profile
        /// (IN) sProfileObject - String - (inst, inco, inrg, inpc, wbid)
        /// (IN) sProfileItems - String - tilde delimited list of profile items
        /// to retrieve e.g. dwhs_access_ind~web_cube_nm~

        /// Returns:
        /// an XML string

        /// Purpose:
        /// This function retrieves a list of profile items for the
        /// given profile object and profile uid no.
        /// user_source_cd (O - Owner, C - Call center, I - LYNX IT)
        /// owner_review_ind (Y, N)
        public string GetProfileItems(string sAttribName, long lProfileUidNo, string sProfileObject, [Optional] object sProfileItems, [Optional] string sUserType)
        {
            string sSafeDOB = string.Empty;
            int iIndex = 0;
            object[] vProfileItems = null;
            string sSQL = string.Empty;
            string sOutput = string.Empty;

            //create the ADO objects
            Connection oConnection = null;
            Command oCommand = null;
            Recordset oRecordset = null;
            object objtemp = null;

            try
            {
                mobjCommon = new modCommon();
                oConnection = new Connection();
                oCommand = new Command();
                oRecordset = new Recordset();
                mobjDataAccess = new modDataAccess();

                //setup the connection
                oConnection.ConnectionTimeout = 60;
                switch (sUserType)
                {
                    case "10":
                    case "11":
                    case "12":
                    case "CA":
                        oConnection.Open(modCommon.sIngresDSNCA);
                        break;
                    default:
                        oConnection.Open(modCommon.sIngresDSN);
                        break;
                }
                //create the session table
                if (sProfileObject == "wbid")
                    sSQL = string.Concat("declare global temporary table session.so_wbid_profile_item as ", "select * from so_wbid_profile_item on commit preserve rows with norecovery");
                else
                    sSQL = string.Concat("declare global temporary table session.so_profile_item as ", "select * from so_profile_item on commit preserve rows with norecovery");

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objtemp);

                //populate the session table
                if (sProfileObject == "wbid")
                {
                    //create an Ingres safe date for Now
                    sSafeDOB = string.Concat(DateTime.Now.Month.ToString(), "-", DateTime.Now.Day.ToString(), "-", Right(DateTime.Now.Year.ToString(), 2));
                    sSQL = string.Concat("insert into session.so_wbid_profile_item (eff_dt, eff_user, attrib_nm, web_uid_no, profile_object, profile_uid_no, ins_co_uid_no, user_session_no, action_cd) ",
            "values ('", sSafeDOB, "','lynxweb','", sAttribName, "','','",
            sProfileObject, "', ", Convert.ToString(lProfileUidNo), ", ", Convert.ToString(lProfileUidNo), ", '', 'GET')");
                }
                else if (sProfileObject == "vusr")
                    sSQL = string.Concat("insert into session.so_profile_item (profile_object, profile_cd) ",
            "values ('", sProfileObject, "', '')");
                else
                    sSQL = string.Concat("insert into session.so_profile_item (profile_object, profile_uid_no, user_id_no) ",
            "values ('", sProfileObject, "', ", Convert.ToString(lProfileUidNo), ", '')");

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objtemp);
                //execute the stored procedure
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                if (sProfileObject == "wbid")
                    oCommand.CommandText = "{ ? = call lynxdba.dp_wbid_so_profile_item(dummy=session.so_wbid_profile_item) }";
                else
                    oCommand.CommandText = "{ ? = call lynxdba.dp_get_so_profile_item(dummy=session.so_profile_item) }";

                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objtemp);

                //setup the command to retrieve the resulting recordset
                if (sProfileObject == "wbid")
                    sSQL = "select * from session.so_wbid_profile_item";
                else
                    sSQL = "select * from session.so_profile_item";

                if ((sProfileItems != null) && (sProfileItems != string.Empty))
                {
                    //only retrieve specific profile items
                    vProfileItems = Regex.Split(Convert.ToString(sProfileItems), "~");
                    if ((vProfileItems.GetType().IsArray) == true)
                    {
                        sProfileItems = "";
                        for (iIndex = 0; iIndex <= vProfileItems.GetUpperBound(0); iIndex++)
                        {
                            sProfileItems = string.Concat(Convert.ToString(sProfileItems), "'", Convert.ToString(vProfileItems[iIndex]), "',");
                        }
                        //strip the last comma off
                        sProfileItems = Left(Convert.ToString(sProfileItems), (sProfileItems.ToString().Length) - 1);
                    }
                    else
                        sProfileItems = string.Concat("'", Convert.ToString(sProfileItems), "'");

                    sSQL = string.Concat(sSQL, " where attrib_nm in (", Convert.ToString(sProfileItems), ")");
                }

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;

                //execute the query for readonly
                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCommand, string.Empty, CursorTypeEnum.adOpenForwardOnly, LockTypeEnum.adLockReadOnly);

                //drop the session table
                if (sProfileObject == "wbid")
                    sSQL = "drop table session.so_wbid_profile_item";
                else
                    sSQL = "drop table session.so_profile_item";

                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = sSQL;
                oCommand.CommandTimeout = 90;
                oCommand.Execute(out objtemp);
                oCommand.ActiveConnection.CommitTrans();

                //process the recordset
                sOutput = mobjDataAccess.RsToXML(oRecordset, true, "item", "profile");
                mobjCommon.CtxSetComplete();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oRecordset = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        } 
        #endregion

        #region Private Functions
        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            //xml objects
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tempDom = null;

            try
            {
                oXMLDom = new DOMDocument40();
                mobjCommon = new modCommon();
                sOutput = mobjCommon.GetConfiguration("lynxAccount4");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);
                tempDom = oXMLDom.parseError;
                if (oXMLDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    modCommon.sIngresDSNCA = oXMLDom.selectSingleNode("//DataSources/IngresCA").text;
                    modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                }
                else
                    mobjCommon.InvalidXML(ref tempDom);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        } 
        #endregion
    }
}
