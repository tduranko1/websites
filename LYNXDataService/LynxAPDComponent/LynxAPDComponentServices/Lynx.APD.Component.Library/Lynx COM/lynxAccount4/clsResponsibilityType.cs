﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using COMSVCSLib;
using MSXML2;
using ADODB;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class clsResponsibilityType
    {
        private modCommon mobjCommon = null;
        private modDataAccess mobjDataAccess = null;
        private ObjectContext moObjectContext = null;

        //Define the Application name to be used in Error Handling
        private const string APP_NAME = "lynxAccount4.";
        private const string MODULE_NAME = "clsResponsibilityType.";

        ///DATE CREATED        : Sep 2002

        ///AUTHOR              : John Walker (I&S IT)

        ///DESCRIPTION         :

        ///This function gets one/many UserType records

        ///PARAMETERS
        ///sRTID               : The RTID you want to look up.  "" will return all.
        ///                     If more than one, put in as (1, 2, 3) to be used with an IN clause.
        ///sRTName             : Part of RTName you want to search on.  "" will return all.
        ///                     Will be used as '%sRTName%'
        ///sFields             : Lets you specify which fields to return from the table.  "" will return all.  Can also use *.
        ///sStyleSheet         : Allows specification of style sheet to transform data, if no style sheet *.xsl is specified returns xml data set.
        ///bPaging             : Tells whether records should be returned in paging fashio
        ///iPage               : Tells what page was picked
        ///sOrderBy            : Tells what column(s) to sort by
        ///bWellFormedXML      : Tells whether the XML should be well formed or not
        ///iRows               : Tells how many rows to return per page

        ///RETURNS                 :

        ///Recordset/XML

        ///MODIFICATIONS

        public string GetResponsibilityType([Optional] string sRTID, [Optional] string sRTName, [Optional] string sFields,
                                            [Optional] string sStylesheet, [Optional] bool bPaging, [Optional] int iPage, [Optional] string sOrderBy, [Optional] bool bWellFormedXML, [Optional] int iRows)
        {
            string sSQL = string.Empty;
            string sOutput = string.Empty;
            string sTotalPages = string.Empty;

            //xml objects
            DOMDocument40 oXmlDom = null;
            DOMDocument40 oXSLDoc = null;

            //create the ADO objects
            Connection oConnection = null;
            Recordset oRecordset = null;
            Command oCmd = null;
            IXMLDOMParseError tempDoc = null;
            IXMLDOMParseError tempDom = null;

            try
            {
                mobjCommon = new modCommon();
                mobjDataAccess = new modDataAccess();
                oConnection.Open(modCommon.sCairDSN);

                oXmlDom = new DOMDocument40();
                oXSLDoc = new DOMDocument40();

                oCmd.CommandText = "spResponsibilityTypeGet";
                oCmd.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCmd.ActiveConnection = oConnection;

                bPaging = false;
                sOrderBy = "RTName";
                iRows = 10;
                //.Parameters.Refresh
                oCmd.Parameters[0].Value = 0;
                oCmd.Parameters[1].Value = sRTID;
                oCmd.Parameters[2].Value = sRTName;
                oCmd.Parameters[3].Value = sFields;
                oCmd.Parameters[4].Value = (Convert.ToBoolean(iPage = 0) ? 1 : iPage);
                oCmd.Parameters[5].Value = bPaging;
                oCmd.Parameters[6].Value = sOrderBy;
                oCmd.Parameters[7].Value = iRows;

                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCmd, oConnection, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly);
                sTotalPages = (Convert.ToString((oCmd.Parameters[0].Value == null) ? 0 : oCmd.Parameters[0].Value));

                //process the recordset
                sOutput = mobjDataAccess.RsToXML(oRecordset, bWellFormedXML, "resptype", "rt");

                if (sStylesheet != string.Empty)
                {
                    //=transform=and=style========================================
                    //parse the xml document
                    oXmlDom.async = false;
                    oXmlDom.loadXML(sOutput);
                    tempDoc = oXSLDoc.parseError;
                    tempDom = oXmlDom.parseError;
                    if (oXmlDom.parseError.errorCode == 0)
                    {
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));
                        if (oXSLDoc.parseError.errorCode == 0)
                        {
                            //if no errors then transform the XML
                            //into HTML using the stylesheet
                            sOutput = oXmlDom.transformNode(oXSLDoc);
                        }
                        else
                            mobjCommon.InvalidXML(ref tempDoc);
                    }
                    else
                        mobjCommon.InvalidXML(ref tempDom);

                    //=transform=and=style========================================
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oRecordset = null;
                oConnection = null;
                oCmd = null;
                oXSLDoc = null;
                oXmlDom = null;
            }
            return sOutput;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            //xml objects
            DOMDocument40 oXmlDom = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                mobjCommon = new modCommon();
                sOutput = mobjCommon.GetConfiguration("lynxAccount4");
                oXmlDom = new DOMDocument40();
                oXmlDom.async = false;
                oXmlDom.loadXML(sOutput);
                tempDom = oXmlDom.parseError;
                if (oXmlDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    modCommon.sCairDSN = oXmlDom.selectSingleNode("//DataSources/Cair").text;
                    modCommon.sIngresDSN = oXmlDom.selectSingleNode("//DataSources/Ingres").text;
                    modCommon.sStylesheetPath = oXmlDom.selectSingleNode("//Stylesheets").text;
                }
                else
                    mobjCommon.InvalidXML(ref tempDom);
                //=get=configuration==========================
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXmlDom = null;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            return true;
        }
    }
}
