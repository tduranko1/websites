﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Data.Objects;
using MSXML2;
using ADODB;
namespace Lynx.APD.Component.Library.Lynx_COM.lynxAccount4
{
    class clsJobTitle
    {
        private ObjectContext moObjectContext = null;
        private string APP_NAME = "lynxAccount4.";
        private string MODULE_NAME = "clsJobTitle.";
        private modCommon objmodCommon = null;
        private modDataAccess objmodDataAccess = null;

        /// <summary>
        /// This function gets one/many Usr records
        /// </summary>
        /// <returns></returns>
        public string GetJobTitle([Optional]string sJTID, [Optional]string sJTName, [Optional]string sJTAdmin, [Optional]string sUTID, [Optional]string sFields, [Optional]string sStylesheet,
                                [Optional]bool bPaging, [Optional]int iPage, [Optional]bool bWellFormedXML, [Optional]int iRows)
        {
            string sOutput = string.Empty;
            string sTotalPages = string.Empty;
            object activeConection = Type.Missing;
            DOMDocument40 oXmlDom = null;
            DOMDocument40 oXSLDoc = null;
            IXMLDOMParseError tempoXSLDoc = null;
            IXMLDOMParseError tempooXmlDom = null;
            Connection oConnection = null;
            Recordset oRecordset = null;
            Command oCmd = null;
            try
            {
                objmodDataAccess = new modDataAccess();
                objmodCommon = new modCommon();
                oConnection = new Connection();
                oRecordset = new Recordset();
                oCmd = new Command();
                oXmlDom = new DOMDocument40();
                oXSLDoc = new DOMDocument40();
                tempoXSLDoc = oXSLDoc.parseError;
                tempooXmlDom = oXmlDom.parseError;

                oConnection.Open(modCommon.sCairDSN);

                oCmd.CommandText = "spJobTitleGet";
                oCmd.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCmd.ActiveConnection = oConnection;
                oCmd.Parameters[0].Value = 0;
                oCmd.Parameters[1].Value = sJTID;
                oCmd.Parameters[2].Value = sJTName;
                oCmd.Parameters[3].Value = sJTAdmin;
                oCmd.Parameters[4].Value = sUTID;
                oCmd.Parameters[5].Value = sFields;
                oCmd.Parameters[6].Value = (iPage == 0 ? 1 : iPage);
                oCmd.Parameters[7].Value = bPaging;
                oCmd.Parameters[8].Value = iRows;

                oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
                oRecordset.Open(oCmd, activeConection, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly);
                sTotalPages = (((oCmd.Parameters[0].Value == null) ? 0 : oCmd.Parameters[0].Value)).ToString();
                sOutput = objmodDataAccess.RsToXML(oRecordset, bWellFormedXML, "JobTitle", "JT");

                if (sStylesheet != "")
                {
                    //oXmlDom=CreateObject("MSXML2.DOMDocument.4.0");
                    oXmlDom.async = false;
                    oXmlDom.loadXML(sOutput);

                    if (oXmlDom.parseError.errorCode == 0)
                    {
                        //oXSLDoc=CreateObject("MSXML2.DOMDocument.4.0");
                        oXSLDoc.async = false;
                        oXSLDoc.load(string.Concat(modCommon.sStylesheetPath, sStylesheet));
                        if (oXSLDoc.parseError.errorCode == 0)
                            sOutput = oXmlDom.transformNode(oXSLDoc);
                        else
                            objmodCommon.InvalidXML(ref tempoXSLDoc);
                    }

                    else
                    {
                        objmodCommon.InvalidXML(ref tempooXmlDom);
                    }
                    oRecordset = null;
                    oConnection = null;
                    oCmd = null;
                    oXSLDoc = null;
                    oXmlDom = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sOutput;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXmlDom = null;
            IXMLDOMParseError tempoXmlDom = null;
            try
            {
                objmodCommon = new modCommon();
                oXmlDom = new DOMDocument40();
                sOutput = objmodCommon.GetConfiguration("lynxAccount4");
                //oXmlDom = CreateObject("MSXML2.DOMDocument.4.0");
                oXmlDom.async = false;
                oXmlDom.loadXML(sOutput);
                if (oXmlDom.parseError.errorCode == 0)
                {
                    modCommon.sCairDSN = oXmlDom.selectSingleNode("//DataSources/Cair").text;
                    modCommon.sStylesheetPath = oXmlDom.selectSingleNode("//Stylesheets").text;
                }
                else
                    objmodCommon.InvalidXML(ref tempoXmlDom);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXmlDom = null;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        private void ObjectControl_Deactivate()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
