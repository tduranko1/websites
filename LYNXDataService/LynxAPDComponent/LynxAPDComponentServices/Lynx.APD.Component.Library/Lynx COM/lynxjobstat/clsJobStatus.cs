﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSXML2;
using System.Xml;
using ADODB;
using System.Runtime.InteropServices;
using COMSVCSLib;
using VBA;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxjobstat
{
    class clsJobStatus
    {
        #region Variable Declaration

        // 11/29/07 - Add check to see if insurance company is State Farm (#91)


        // Private moObjectContext As ObjectContext
        // Private Const zMODULE As String = "lynxJobStat.clsJobStatus"
        // Private Const zFILEPATH = "Software\LYNXServices\Components\lynxJobStat"

        private ObjectContext ctxObject = null;
        private const string zMODULE = "lynxJobStat.clsJobStatus";
        private const int zAppID = 23;

        //Global events and data access objects.
        public string sInsID = string.Empty;
        public string g_strSupportDocPath = string.Empty;
        public bool g_blnDebugMode = false;
        lynxAccount4.modDataAccess objmodDataAccess = null;
        lynxAccount4.modCommon objModCommon = null;

        // NOTE: this component uses a conditional compilation
        // argument called "Developer". This argument must
        // be set as follows:
        // 
        // Developer = 0 (running from the com+ server)
        // Developer = 1 (running from developer desktop)
        // 

        // NOTE: The two Private variables below are read from
        // a configuration file during the ObjectControl_Activate() method.
        // See the note above about setting the "Developer" conditional
        // compilation argument correctly.

        private string sIngresDSN = string.Empty,
        sDatawarehouseDSN = string.Empty,
        sStylesheetPath = string.Empty;

        #endregion

        #region Private Methods

        private void ObjectControl_Activate()
        {
            // Set ctxObject = GetObjectContext()
            string sOutput = string.Empty;
            // xml objects
            FreeThreadedDOMDocument40 oXMLDom = null;
            IXMLDOMParseError oTempError = null;

            try
            {
                objModCommon = new lynxAccount4.modCommon();
                sOutput = objModCommon.GetConfiguration("lynxJobStat");
                // Initialize DOM objects.
                oXMLDom = new FreeThreadedDOMDocument40();
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);

                if (oXMLDom.parseError.errorCode == 0)
                {
                    // retrieve the values here
                    sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    sDatawarehouseDSN = oXMLDom.selectSingleNode("//DataSources/Datawarehouse").text;
                    sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                }
                else
                {
                    objModCommon.InvalidXML(ref oTempError);
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                oXMLDom = null;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            return true;
        }

        private void ObjectControl_Deactivate()
        {
            ctxObject = null;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   GetCheckDetail(FUNCTION)
        ///   Purpose: this function retrieves all checks for a specific dispatch.
        /// </summary>
        /// <param name="lDispatchNo">String - the dispatch number to retrieve</param>
        /// <param name="sWebUidNo">String - the web id of the parent account e.g. testinst</param>
        /// <param name="lUserIdNo">Long - the user id no of the parent account e.g. 9374</param>
        /// <param name="sEnvId">String - holds the calling environment</param>
        /// <returns> an XML string</returns>
        public string GetCheckDetail(long lDispatchNo, string sWebUidNo, long lUserIdNo, string sEnvId)
        {
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sOutputColumns = string.Empty,
                   sOutput = string.Empty;
            // bool bLogError;
            Recordset oOutputRs = null;
            clsHelper objclsHelper = null;
            // int sInd=0;
            try
            {
#if(Developer==true) 
		            // use the conditional compilation argument "Developer = 1"
                    // when debugging this component from your desktop
                    ObjectControl_Activate();
#endif
                objclsHelper = new clsHelper();
                objmodDataAccess = new lynxAccount4.modDataAccess();

                // 11/29/07 - Add check to see if insurance company is State Farm (#91)
                // 12/18/08 - Modified for different Insurance Companies
                if (Left((Right(sWebUidNo, 4)), 1) == "*")
                    //sInsID = Replace(objclsHelper.Right(sWebUidNo, 4), "*", "");
                    sInsID = Right(sWebUidNo, 4).Replace("*", "");
                else
                    sInsID = "0";

                // Make sure we are getting a dispatch number passed in
                if (lDispatchNo < 1 && sInsID != "91")
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), "clsJobStatus.getCheckDetail", "Invalid Dispatch No:", Convert.ToString(lDispatchNo)));

                // STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_check_info";
                // STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_get_so_check_info";
                // STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Concat("insert into session.so_check_info (dispatch_no, user_session_no, user_id_no) ",
                        "values (", lDispatchNo, ", '", sWebUidNo, "', '", Convert.ToString(lUserIdNo), "')");

                // STEP III (d) : Define the Select Statement to retrieve records
                // 620     sOutputColumns = "check_no, paymt_amt, check_dt, cleared_dt"
                sOutputColumns = "*";
                // STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = objmodDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sIngresDSN, "", sOutputColumns);
                sOutput = objmodDataAccess.RsToXML(oOutputRs, true, "", "GetCheckDetail");

                if (!(oOutputRs == null))
                {
                    oOutputRs = null;
                }

                return sOutput;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// GetCommentText(FUNCTION)
        /// Purpose: this function retrieves all comments for a specific dispatch.
        /// </summary>
        /// <param name="lDispatchNo">String -the dispatch number to retrieve comments for</param>
        /// <param name="sWebUidNo">String - the web id of the parent account e.g. testinst</param>
        /// <param name="sEnvId">String - holds the calling environment</param>
        /// <returns>an XML string</returns>
        public string GetCommentText(long lDispatchNo, string sWebUidNo, string sEnvId)
        {
            string sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sInsertSQL = string.Empty,
                sOutputColumns = string.Empty,
                sOutput = string.Empty;

            //bool bLogError;

            Recordset oOutputRs = null;
            clsHelper objclsHelper = null;
            string sFilter = string.Empty;

            try
            {
#if(Developer == true)
                     // use the conditional compilation argument "Developer = 1"
                     // when debugging this component from your desktop
                     ObjectControl_Activate();
#endif

                // 11/29/07 - Add check to see if insurance company is State Farm (#91)
                // 12/18/08 - Modified for different insurance companies
                if (Left((Right(sWebUidNo, 4)), 1) == "*")
                    sInsID = Right(sWebUidNo, 4).Replace("*", "");
                else
                    sInsID = "0";

                // Make sure we are getting a dispatch number passed in

                if (lDispatchNo < 1 && sInsID != "91")
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), "clsJobStatus.getCommentText", "Invalid Dispatch No:", Convert.ToString(lDispatchNo)));

                //  STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_comments1";
                //  STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_get_so_comments1";
                //  STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Concat("insert into session.so_comments1 (dispatch_no, user_session_no, user_id_no) ",
                            "values (", lDispatchNo, ", '", sWebUidNo, "', '", sEnvId, "')");

                // 80 sInsertSQL = "insert into session.so_comments1 (dispatch_no, user_session_no, user_id_no) " & _
                // "values (" & lDispatchNo & ", '" & sWebUidNo & "', '" & sWebUidNo & "')"

                //  STEP III (d) : Define the Select Statement to retrieve records
                sOutputColumns = "*";

                sFilter = "1 = 1 order by create_dt, cmnt_no, line_no, error_no";

                oOutputRs = new Recordset();

                //  STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = objmodDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sIngresDSN, sFilter, sOutputColumns);
                sOutput = objmodDataAccess.RsToXML(oOutputRs, true, null, "GetCommentText");

                if (!(oOutputRs == null))
                {
                    oOutputRs = null;
                }

                return sOutput;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// GetDetail(FUNCTION)
        /// Purpose:
        /// this function retrieves detail information for a specific dispatch.</summary>
        /// <param name="lDispatchNo">String - the dispatch number to retrieve detail for</param>
        /// <param name="lUserIdNo">Long - the user id no of the parent account e.g. 9374</param>
        /// <param name="iUserTypeNo">Integer - (1=installer, 2=insurance company)</param>
        /// <param name="sWebUidNo">String - the web id of the parent account e.g. testinst</param>
        /// <param name="sEnvId">String - holds the calling environment in the form: group_apptype_env. (see comments in modCommon for details)</param>
        /// <returns>xml string</returns>                                          
        public string GetDetail(long lDispatchNo, long lUserIdNo, int iUserTypeNo, string sWebUidNo, string sEnvId)
        {
            bool bIVR; // IVR         
            int iStatus = 0; // IVR
            string sJobType = string.Empty,
                sBillDate = string.Empty,
                sPaymentAmount = string.Empty,
                sCheckNo = string.Empty, // IVR
                sCheckDate = string.Empty, // IVR
                sClearedDate = string.Empty, // IVR
                sOutput = string.Empty;
            Connection oConnection = null;
            Command oCommand = null;
            clsHelper objclsHelper = null;

            object lngRecordsAffected;

            bool bLogError;
            int sInd = 0;

            try
            {
                oConnection = new Connection();
                oCommand = new Command();
                objclsHelper = new clsHelper();
                objmodDataAccess = new lynxAccount4.modDataAccess();

                // TODO: INSTANT C# TODO TASK: C# compiler constants cannot be set to explicit values:
#if (Developer == true)
                    //use the conditional compilation argument "Developer = 1"
                    //when debugging this component from your desktop
                    ObjectControl_Activate();
#endif

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetDetail", "<Inputs><lDispatchNo>" & CStr(lDispatchNo) & "</lDispatchNo><lUserIdNo>" & CStr(lUserIdNo) & "</lUserIdNo><iUserTypeNo>" & CStr(iUserTypeNo) & "</iUserTypeNo><sWebUidNo>" & CStr(sWebUidNo) & "</sWebUidNo></Inputs>", "", 1, 5)

                // 11/29/07 - Add check to see if insurance company is State Farm (#91)
                // 12/18/08 - Modified for other insurance companies
                // (sWebUidNo.Substring(sWebUidNo.Length - 4).Substring(0, 1) == "*")
                if (Left((Right(sWebUidNo, 4)), 1) == "*")
                    // (sWebUidNo.Substring(sWebUidNo.Length - 4)).Replace("*", "");
                    sInsID = Right(sWebUidNo, 4).Replace("*", "");
                else
                    sInsID = "0";

                // Make sure we are getting a dispatch number passed in
                if (lDispatchNo < 1 && sInsID != "91")
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), "clsJobStatus.getDetail", "Invalid Dispatch No:", Convert.ToString(lDispatchNo)));

                if (sEnvId.IndexOf("IVR") == 0)
                    bIVR = false;
                else
                    bIVR = true;

                oConnection.ConnectionTimeout = 60;
                oConnection.Open(sIngresDSN);

                // Run the procedure
                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.IsolationLevel = IsolationLevelEnum.adXactIsolated;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = "{? = call lynxdba.dp_get_dispatch_status (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;

                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, " "));

                if (lUserIdNo > 0)
                    oCommand.Parameters.Append(oCommand.CreateParameter("h_user_id_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 10, string.Concat("w", Right(string.Concat("000000", Convert.ToString(lUserIdNo)), 6))));
                else
                    oCommand.Parameters.Append(oCommand.CreateParameter("h_user_id_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 10, "lynxweb"));

                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, Left(sWebUidNo, 32)));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_type_no", DataTypeEnum.adTinyInt, ParameterDirectionEnum.adParamInput, iUserTypeNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_bill_dt", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 26, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_check_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 10, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_check_dt", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 26, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_claim_type_desc", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 20, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_claim_inv_dt", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 26, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_cleared_dt", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 26, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_co_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_compet_bid_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_control_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 8, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_deduct_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInputOutput, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_dispatch_dt", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 26, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_dispatch_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, Convert.ToInt32(lDispatchNo)));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_dispatch_status", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_dispatch_status_msg", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ediproc_dt", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 26, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_error_desc", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 90, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_fax_resend_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_co_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_co_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 35, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 30, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_addr_1st", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_addr_2nd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_city_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 20, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_phone_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 13, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_state_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 2, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_zip_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 9, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_insrd_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 42, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_inv_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 10, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_job_type", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_lc_job_type_cd", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_lc_job_type_desc", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 40, " "));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_max_fax_resend_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_paymt_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInputOutput, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_pgm_mbr_status_desc", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 30, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_resubmit_dt", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 26, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_sched_appt_dt", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 26, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_vehicle_desc", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 80, " "));

                oCommand.Execute(out lngRecordsAffected, null, Convert.ToInt32(ExecuteOptionEnum.adExecuteStream));
                oCommand.ActiveConnection.CommitTrans();

                if (Convert.ToInt32(oCommand.Parameters["retval"]) != 0)
                {
                    bLogError = true;

                    if (Convert.ToInt32(oCommand.Parameters["retval"]) < 0)
                        sInd = 1;
                    else
                        sInd = 4;
                }

                return objmodDataAccess.ParametersToXML(ref oCommand, true, "record", "GetDetail");

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetDetail", GetDetail, "", 1, 5)
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                oCommand = null;
                oConnection = null;
            }
        }

        /// <summary>
        /// GetDispatchDetail(FUNCTION)
        /// Purpose:
        /// This function retrieves all detail, check detail, comments and EDI</summary>
        /// information for a specific dispatch.<param name="lDispatchNo">String - the dispatch number to retrieve checks for</param>
        /// <param name="sWebUidNo">String - the web id of the parent account e.g. testinst</param>
        /// <param name="lUserIdNo">Long - the user id no of the parent account e.g. 9374</param>
        /// <param name="iUserTypeNo">Integer - (1=installer, 2=insurance company)</param>
        /// <param name="sEnvId">String - holds the calling environment: ("IVR", "IVRDEV", "WEB", "WEBDEV", "DWHS")</param>
        /// <param name="sStylesheet">Optional String - the XSL Stylesheet to be applied to the XML</param>
        /// <returns>an XML string</returns>
        public string GetDispatchDetail(long lDispatchNo, string sWebUidNo, long lUserIdNo, int iUserTypeNo, string sEnvId, string sStylesheet)
        {
            string sHeader = string.Empty,
                sFooter = string.Empty;
            bool bIVR;
            string sOutput;

            // xml objects
            FreeThreadedDOMDocument40 oXMLDom = null;
            IXMLDOMNode oNode = null;
            Command oCmd = null;
            Connection oConn = null;
            clsHelper objclsHelper = null;
            IXMLDOMParseError oTempError = null;

            object lngRecordsAffected = null;

            string sRetVal = string.Empty,
                sIncoID = string.Empty;
            string sUserIDNo = string.Empty; // 11/09/07 Format UserID to w00####.
            string sStoredProc = string.Empty; // 10/30/08 Added for profile check.
            string sIncoCmntInd = string.Empty; // 10/30/08 Added for profile check.

            string sUID = string.Empty,
                sUSessID = string.Empty;

            long sProfileUID;
            bool bLogError;
            int sInd = 0;

            try
            {	 // Object initiallization. 
                oXMLDom = new FreeThreadedDOMDocument40();
                oCmd = new Command();
                oConn = new Connection();

                objclsHelper = new clsHelper();
                objmodDataAccess = new lynxAccount4.modDataAccess();
                objModCommon = new lynxAccount4.modCommon();

                // 11/09/07 Format UserID to w00####.
                sUserIDNo = string.Concat("w", Right(string.Concat("000000", Convert.ToString(lUserIdNo)), 6));

#if(Developer == true)
		                            ObjectControl_Activate(); 
#endif

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetDispatchDetail", "<Inputs><lDispatchNo>" & CStr(lDispatchNo) & "</lDispatchNo><lUserIdNo>" & CStr(lUserIdNo) & "</lUserIdNo><iUserTypeNo>" & CStr(iUserTypeNo) & "</iUserTypeNo><sWebUidNo>" & CStr(sWebUidNo) & "</sWebUidNo></Inputs>", "", 1, 5).

                // 11/29/07 - Add check to see if insurance company is State Farm (#91).
                // 12/18/08 - Modified for other insuranse companies.
                if (Left((Right(sWebUidNo, 4)), 1) == "*")
                {
                    //sInsID = Replace(Right(sWebUidNo, 4), "*", "")
                    sInsID = Right(sWebUidNo, 4).Replace("*", "");
                    sUSessID = Left(sWebUidNo, sWebUidNo.Length - 4);
                }
                else
                {
                    sInsID = "0";
                    sUSessID = sWebUidNo;
                }

                // Check Company profile to determine if Comments should be shown.
                sStoredProc = "dp_get_profile_item";
                sUID = Convert.ToString(lUserIdNo);
                sProfileUID = Convert.ToInt32(sInsID); // insurance company uid #  (i.e. 23 for Allstate or 91 for SF).

                // Setup the connection.
                oConn.ConnectionTimeout = 60;
                oConn.Open(sIngresDSN);
                oCmd.ActiveConnection = oConn;
                oCmd.CommandType = CommandTypeEnum.adCmdText;
                oCmd.CommandText = string.Concat("{? = call ", sStoredProc, " (?,?,?,?,?,?,?,?,?,?,?,?,?) }");
                oCmd.CommandTimeout = 90;
                oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sUID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_id", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sUSessID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_attrib_amt", DataTypeEnum.adCurrency, ParameterDirectionEnum.adParamInputOutput, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_attrib_cd", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 50, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_attrib_dt", DataTypeEnum.adDate, ParameterDirectionEnum.adParamInputOutput, 0, DateTime.Now.Date));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_attrib_flt_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_attrib_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, "web_comments_display_ind"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_attrib_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 1, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_profile_object", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, "inco"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_profile_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, Convert.ToInt32(sProfileUID)));
                oCmd.Execute(out lngRecordsAffected, null, Convert.ToInt32(ExecuteOptionEnum.adExecuteStream));

                // Indicator used to determine if Insurance companies are able to view dispatch status
                sIncoCmntInd = Convert.ToString(oCmd.Parameters["h_attrib_cd"]);

                if (Convert.ToInt32(oCmd.Parameters["retval"]) != 0)
                {
                    bLogError = true;

                    if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0)
                        sInd = 1;
                    else
                        sInd = 4;
                }

                // Make sure we are getting a dispatch number passed in.
                if (lDispatchNo < 1 && sInsID != "91")
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), "clsJobStatus.getDispatchDetail", "Invalid Dispatch No:", Convert.ToString(lDispatchNo)));

                if (sWebUidNo == "lynxIVR")
                {
                    bIVR = true;
                    lUserIdNo = 0;
                }
                else
                    bIVR = false;

                sOutput = GetDetail(lDispatchNo, lUserIdNo, iUserTypeNo, sWebUidNo, sEnvId);
                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetDispatchDetail", "<GetDetail><sOutput>" & CStr(sOutput) & "</sOutput></GetDetail>", "", 1, 5)                
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);
                if (oXMLDom.parseError.errorCode == 0)
                {
                    // retrieve the values here
                    sRetVal = oXMLDom.selectSingleNode("//GetDetail/retval").text;
                    oNode = oXMLDom.selectSingleNode("//GetDetail/h_ins_co_uid_no");

                    if (!(oNode == null))
                        sIncoID = oNode.text.Trim();
                    else
                        objModCommon.InvalidXML(ref oTempError);
                }

                oXMLDom = null;

                if (sRetVal != "1")
                {
                    sOutput = string.Concat(sOutput, GetCheckDetail(lDispatchNo, sWebUidNo, lUserIdNo, sEnvId));

                    if (iUserTypeNo == 3)
                        sOutput = string.Concat(sOutput, GetCommentText(lDispatchNo, sWebUidNo, sEnvId));
                    else if (iUserTypeNo == 2)
                    {
                        // 10/31/2008 - Added sIncoCmntInd to allow other Insurance Companies to get status on disptaches
                        // 12/22/2008 - Removed Reference to State Farm (91) to allow Profile Indicator to determine if comments are displayed
                        if (sIncoCmntInd == "Y")
                            // 11/09/07 Changed sEvnID to be sUserIDNo.  Need to pass the w00#### to the get comments function.
                            sOutput = string.Concat(sOutput, GetCommentText(lDispatchNo, sWebUidNo, sUserIDNo));   // GetCommentText(lDispatchNo, sWebUidNo, sEnvId). 
                    }

                    if (bIVR == false)
                    {
                        // * Date: 21FEB08                                 
                        // * Author: Dan McGill                            
                        // * Description: Include a call to GetEDIErrors in
                        // *              order to return the appropriate  
                        // *              message to the user.             
                        // *
                        // 03/05/08 - Added check to call GetEDIErrors when there is a valid dispatch number.
                        if (lDispatchNo < 1)
                        {
                            // Do not call GetEDIErrors
                        }
                        else
                            sOutput = string.Concat(sOutput, GetEDIErrors(lDispatchNo, sEnvId));
                    }
                }

                switch (iUserTypeNo)
                {
                    case 1: // Installer/IVR
                        if (bIVR == false)
                        {
                            sHeader = "<GetInstDispatchDetail>";
                            sFooter = "</GetInstDispatchDetail>";
                        }
                        else
                        {
                            sHeader = "<GetIVRDispatchDetail>";
                            sFooter = "</GetIVRDispatchDetail>";
                        }
                        break;
                    case 2: // Client
                        sHeader = "<GetClientDispatchDetail>";
                        sFooter = "</GetClientDispatchDetail>";
                        break;
                    case 3: // Intranet
                        sHeader = "<GetIntranetDispatchDetail>";
                        sFooter = "</GetIntranetDispatchDetail>";
                        break;
                }

                sOutput = string.Concat(sHeader, sOutput, sFooter);

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetDispatchDetail", "<Output>" & sOutput & "</Output>", "", 1, 5)
                if (sStylesheet != "")
                    sOutput = objmodDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet));

                return sOutput;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                oConn.Close();
                oCmd = null;
            }
        }

        /// <summary>
        /// GetCompBidInfo(FUNCTION)
        /// 09/28/10 - Added function to call dp_get_so_sf_compbid to get list of
        ///    competitive bids.  This will be accessible to State Farm.
        ///    The info is colleceted from the Dispatch number
        /// </summary>
        /// <param name="sDispNo"></param>
        /// <param name="sStylesheet"></param>
        /// <param name="sWebUidNo"></param>
        /// <returns></returns>
        public string GetCompBidInfo(long sDispNo, string sStylesheet = "", string sWebUidNo = "")
        {
            // Declare Variables
            string sStoredProc = string.Empty,
                 sSessionTableName = string.Empty,
                 sDispatch = string.Empty,
                 sCommitInd = string.Empty,
                 sMsgInd = string.Empty,
                 sMsgTxt = string.Empty,
                 sUsrIDNo = string.Empty,
                 sUsrSessionID = string.Empty,

                 sOutputColumns = string.Empty,
                 sInsertSQL = string.Empty,
                 sOutput = string.Empty,
                 sMultSheets = string.Empty,
                 sStylesheet1 = string.Empty,
                 sStylesheet2 = string.Empty;

            // Declare XML formatting variables
            string sDocumentElement = string.Empty,
                sElement = string.Empty;

            Recordset oOutputRs = null;
            clsHelper objclsHelper = null;


            try
            {
#if(Developer == true)
                    // use the conditional compilation argument "Developer = 1"
                    // when debugging this component from your desktop
		            ObjectControl_Activate(); 
#endif

                oOutputRs = new Recordset();

                objclsHelper = new clsHelper();
                objmodDataAccess = new lynxAccount4.modDataAccess();
                // Set values for variables
                // sMultSheets = InStr(1, sStylesheet, "; ")
                sMultSheets = Convert.ToString(sStylesheet.IndexOf("; ") + 1);

                if (sMultSheets != "0")
                {
                    sStylesheet1 = Left(sStylesheet, sMultSheets.Length - 1);
                    sStylesheet2 = Right(sStylesheet, sStylesheet.Length - sMultSheets.Length);
                    sStylesheet2 = sStylesheet2.Replace(" ", "");
                }
                else
                {
                    sStylesheet1 = sStylesheet;
                    sStylesheet2 = sStylesheet;
                }

                sDispatch = Convert.ToString(sDispNo);
                sCommitInd = "Y";
                sMsgInd = "Y";
                sMsgTxt = "";
                sUsrIDNo = sWebUidNo;
                sUsrSessionID = sWebUidNo;
                sOutputColumns = "*";
                sStoredProc = "dp_get_so_sf_compbid";
                sSessionTableName = "so_sf_compbid";
                sInsertSQL = string.Concat("insert into session.", sSessionTableName, " (claim_type_cd,  claim_type_desc, cmbd_accept_oa_cd, ",
                            "cmbd_bid_amt, cmbd_bid_type_desc, cmbd_contact_nm, cmbd_kit_amt, cmbd_tot_kit_amt, cmbd_labor_amt, cmbd_tot_labor_amt, cmbd_mfg_cd, ",
                            "cmbd_mfg_part_no, deduct_cd, deduct_amt, discount_cd, discount_type_cd, dispatch_no, insrd_first_nm, ",
                            "insrd_last_nm, inst_addr_1st, inst_addr_2nd, inst_area_code_no, inst_city_nm, inst_exchange_no, ",
                            "inst_household_no, inst_nm, inst_state_cd, inst_uid_no, inst_zip_cd, kit_no, loss_dt, policy_no, ",
                            "veh_desc, veh_model_yr, vin_no, veh_uid_no, commit_ind, msg_ind, msg_text, user_id_no, user_session_no) ",
                            "values(0,'','',0,'','',0,0,0,0,'','','',0,0,0,", sDispatch, ",'','','','',0,'',0,0,'','',0,'',0,'','','',0,'',0,'", sCommitInd, "','", sMsgInd, "','", sMsgTxt, "','", sUsrIDNo, "','", sUsrSessionID, "')");

                // Process the data against the stored procedure
                oOutputRs = objmodDataAccess.GetSessionRows(sSessionTableName, sStoredProc, sInsertSQL, sIngresDSN, "", sOutputColumns);

                //Convert the recordset into an XML format
                sOutput = objmodDataAccess.RsToXML(oOutputRs, true, "", "GetCompBidInfo");

                if (sMultSheets != "0")
                {
                    //Take the XML and passit through style sheet (grouping for duplicates)
                    sOutput = objmodDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet2));
                    //Take the XML and passit through style sheet
                    sOutput = objmodDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet1));
                }
                else
                    // Take the XML and passit through style sheet
                    sOutput = objmodDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet1));
                // Clear objects
                if (!(oOutputRs == null))
                    oOutputRs = null;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            // Output formated data
            return sOutput;
        }

        /// <summary>
        /// GetDispatchInfo(FUNCTION)
        /// Purpose:
        /// this function retrieves all ???
        /// </summary>
        /// <param name="sWebUidNo">String - the web id of the parent account e.g. testinst</param>
        /// <param name="lUserIdNo">Long - the user id no of the parent account e.g. 9374</param>
        /// <param name="iUserTypeNo">Integer - (1=installer, 2=insurance company)</param>
        /// <param name="sNumber">String - the claim no or the policy no</param>
        /// <param name="iSummaryType">Integer - (1=invoiced, 2=open)</param>
        /// <param name="iOrderBy">Integer - determines the  order of the returned recordset</param>
        /// <param name="sEnvId">String - holds the calling ensortvironment</param>
        /// <param name="sStart"></param>
        /// <param name="sEnd"></param>
        /// <returns>an XML string</returns>
        public string GetDispatchInfo(string sWebUidNo, long lUserIdNo, int iUserTypeNo, string sNumber, int iSummaryType, int iOrderBy, string sEnvId, string sStart, string sEnd)
        {
            string sSQL = string.Empty;
            // string sOutput=string.Empty;
            string strClaimNo = string.Empty,
                strPolicyNo = string.Empty,
                strUserTypeCode = string.Empty,
                strEIN = "0",
                strStart = string.Empty,
                strEnd = string.Empty,
                sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sInsertSQL = string.Empty,
                sOutputColumns = string.Empty,
                sOutput = string.Empty,
                sFilter = string.Empty;

            //bool bLogError;

            Recordset oOutputRs = null;
            try
            {
#if(Developer == true)
		            // use the conditional compilation argument "Developer = 1"
                    // when debugging this component from your desktop
                    ObjectControl_Activate();
#endif

                objmodDataAccess = new lynxAccount4.modDataAccess();

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetDispatchInfo", "<Inputs><iSummaryType>" & CStr(iSummaryType) & "</iSummaryType><iOrderBy>" & CStr(iOrderBy) & "</iOrderBy><sNumber>" & CStr(sNumber) & "</sNumber><lUserIdNo>" & CStr(lUserIdNo) & "</lUserIdNo><iUserTypeNo>" & CStr(iUserTypeNo) & "</iUserTypeNo><sWebUidNo>" & CStr(sWebUidNo) & "</sWebUidNo></Inputs>", "", 1, 5)

                // Make sure we are getting a UID passed in
                if (lUserIdNo < 1)
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), "clsJobStatus.getDispatchInfo", "Invalid Dispatch No:", Convert.ToString(lUserIdNo)));

                switch (iSummaryType)
                {
                    case 3:
                        strClaimNo = sNumber;
                        break;
                    case 5:
                        strEIN = sNumber;
                        strStart = sStart;
                        strEnd = sEnd;
                        break;
                    default:
                        strPolicyNo = sNumber;
                        break;
                }

                switch (iUserTypeNo)
                {
                    case 1:
                        strUserTypeCode = "INST";
                        break;
                    case 2:
                        strUserTypeCode = "INCO";
                        break;
                    default:
                        strUserTypeCode = "INET";
                        break;
                }

                // STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_dispatch_info";

                // STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_get_so_dispatch_info";

                // STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                // 20120110 SLL - added search options for EIN and Date Range
                sInsertSQL = string.Concat("insert into session.so_dispatch_info (claim_no, policy_no, user_session_no, user_id_no, user_type_cd, user_type_no, user_uid_no, federal_tax_id, search_start_dt, search_end_dt) ",
                        "values ('", strClaimNo, "', '", strPolicyNo, "', '", sWebUidNo, "', '", lUserIdNo, "', '", strUserTypeCode, "', ", iUserTypeNo, ", ", lUserIdNo, ", ", strEIN, ", '", strStart, "', '", strEnd, "')");

                // STEP III (d) : Define the Select Statement to retrieve records
                // 3670        sOutputColumns = "claim_no, dispatch_no, policy_no, inst_uid_no, lcmt_no, loss_dt"
                sOutputColumns = "*";

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetDispatchInfo", "<sInsertSQL><" & sInsertSQL & "</sInsertSQL>", "", 1, 5)

                sFilter = "1 = 1 order by create_dt, cmnt_no, line_no, error_no";

                // STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = objmodDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sIngresDSN, "", sOutputColumns);
                sOutput = objmodDataAccess.RsToXML(oOutputRs, true, "", "GetDispatchInfo");

                if (!(oOutputRs == null))
                    oOutputRs = null;

                return sOutput;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// GetDispatchList(FUNCTION)
        /// Purpose:
        /// this function retrieves a list of dispatches.</summary>
        /// <param name="lUserIdNo">Long - the user id no of the parent account e.g. 9374</param>
        /// <param name="iUserTypeNo">Long - the type of user: 1 = Installer, 2 = Ins Co.</param>
        /// <param name="sNumber">String - the claim no or the policy no</param>
        /// <param name="iSummaryType">Integer - (1=invoiced, 2=open)</param>
        /// <param name="iOrderBy">Integer - determines the sort order of the returned recordset</param>
        /// <param name="sEnvId">String - holds the calling environment: ("IVR", "IVRDEV", "WEB", "WEBDEV", "DWHS")</param>
        /// <param name="sStart"></param>
        /// <param name="sEnd"></param>
        /// <param name="sStylesheet">String - the stylesheet for styling the xml</param>
        /// <param name="sWebUidNo">String - the web id of the parent account e.g. testinst</param>
        /// <returns>an XML string</returns>
        public string GetDispatchList(long lUserIdNo, int iUserTypeNo, string sNumber, int iSummaryType, int iOrderBy, string sEnvId, string sStart, string sEnd, string sStylesheet = "", string sWebUidNo = "")
        {
            string sOutput = string.Empty;
            try
            {
#if(Developer == true)
                    // use the conditional compilation argument "Developer = 1"
                    // when debugging this component from your desktop
                    ObjectControl_Activate();
#endif

                objmodDataAccess = new lynxAccount4.modDataAccess();

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetDispatchList", "<Inputs><iSummaryType>" & CStr(iSummaryType) & "</iSummaryType><iOrderBy>" & CStr(iOrderBy) & "</iOrderBy><sNumber>" & CStr(sNumber) & "</sNumber><lUserIdNo>" & CStr(lUserIdNo) & "</lUserIdNo><iUserTypeNo>" & CStr(iUserTypeNo) & "</iUserTypeNo><sWebUidNo>" & CStr(sWebUidNo) & "</sWebUidNo></Inputs>", "", 1, 5)

                // Make sure we are getting a UID passed in
                if (lUserIdNo < 1)
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), "clsJobStatus.getDispatchList", "Invalid Dispatch No:", Convert.ToString(lUserIdNo)));

                if (iUserTypeNo == 1)
                    sOutput = string.Concat(sOutput, GetSummary(lUserIdNo, iUserTypeNo, sNumber, iSummaryType, iOrderBy, sEnvId));
                else
                    sOutput = string.Concat(sOutput, GetDispatchInfo(sWebUidNo, lUserIdNo, iUserTypeNo, sNumber, iSummaryType, iOrderBy, sEnvId, sStart, sEnd));

                if (iSummaryType == 1)
                    sOutput = string.Concat("<invoiceSummary>", sOutput, "</invoiceSummary>");

                else if (iSummaryType == 2)
                    sOutput = string.Concat("<dispatchSummary>", sOutput, "</dispatchSummary>");

                else if (iSummaryType == 3)
                    sOutput = string.Concat("<claimDispatchInfo>", sOutput, "</claimDispatchInfo>");
                else
                    sOutput = string.Concat("<policyDispatchInfo>", sOutput, "</policyDispatchInfo>");

                if (sStylesheet != "")
                    sOutput = objmodDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return sOutput;
        }

        /// <summary>
        /// GetEDIErrors(FUNCTION)
        /// Purpose:
        /// This function returns a string containing the EDI errors for a dispatch
        /// </summary>
        /// <param name="lDispatchNo">String - the dispatch number to retrieve detail for</param>
        /// <param name="sEnvId">String - holds the calling environment in the form: group_apptype_env (see comments in modCommon for details)</param>
        /// <returns>xml string</returns>
        public string GetEDIErrors(long lDispatchNo, string sEnvId)
        {
            //object vntField =null;
            //object vntEdiProcCd =null;
            //object vntErrorNo=null;
            //object vntClmeErrorDesc=null;
            //object vntErrorDesc =null;
            //object vntLineNo=null;
            //object vntLineSubNo=null;
            //object vntMfgPartNo=null;
            //object vntTableCd=null;

            //int h_line_no=0;

            string h_mfg_part_no = string.Empty,
                sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sInsertSQL = string.Empty,
                sOutputColumns = string.Empty,
                sOutput = string.Empty;

            //bool bLogError;

            Recordset oOutputRs = null;
            string sFilter = string.Empty;

            try
            {
#if(Developer == true)
		            // use the conditional compilation argument "Developer = 1"
                    // when debugging this component from your desktop
                    ObjectControl_Activate();  
#endif

                objmodDataAccess = new lynxAccount4.modDataAccess();

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetEDIErrors", "<Inputs><lDispatchNo>" & CStr(lDispatchNo) & "</lDispatchNo></Inputs>", "", 1, 5)                

                // Make sure we are getting a dispatch number passed in
                if (lDispatchNo < 1)
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), "clsJobStatus.getEDIErrors", "Invalid Dispatch No:", Convert.ToString(lDispatchNo)));

                // STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_web_edi_errors";

                // STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_web_edi_errors";

                // STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Concat("insert into session.so_web_edi_errors (dispatch_no) ",
                            "values (", lDispatchNo, ")");

                sOutputColumns = "*";

                sFilter = "1 = 1 order by create_dt, line_no, error_no";

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetEDIErrors", "<sInsertSQL>" & CStr(sInsertSQL) & "</sInsertSQL>", "", 1, 5)

                // STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = objmodDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sIngresDSN, sFilter, sOutputColumns);
                sOutput = objmodDataAccess.RsToXML(oOutputRs, true, "", "GetEDIErrors");

                if (!(oOutputRs == null))
                    oOutputRs = null;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return sOutput;
        }

        /// <summary>
        /// GetSiblingShops(FUNCTION)
        /// Purpose: This function retrieves all shops with the same parent as the useridno.
        /// </summary>
        /// <param name="sWebUidNo">String - the web id of the parent account e.g. testinst</param>
        /// <param name="lUserIdNo">Long - the user id no of the parent account e.g. 9374</param>
        /// <param name="sEnvId">String - holds the calling environment in the form: group_apptype_env (see comments in modCommon for details)</param>
        /// <param name="sStylesheet">Optional String - the XSL Stylesheet to be applied to the XML</param>
        /// <returns>an XML string</returns>
        public string GetSiblingShops(string sWebUidNo, long lUserIdNo, string sEnvId, string sStylesheet = "")
        {
            string sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sInsertSQL = string.Empty,
                sOutputColumns = string.Empty,
                sOutput = string.Empty;

            //bool bLogError;

            Recordset oOutputRs = null;
            string sFilter = string.Empty;

            try
            {
                oOutputRs = new Recordset();
                objmodDataAccess = new lynxAccount4.modDataAccess();

#if(Developer == true)
		            // use the conditional compilation argument "Developer = 1"
                    // when debugging this component from your desktop
                    ObjectControl_Activate();  
#endif

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetSiblingShops", "<Inputs><lUserIdNo>" & CStr(lUserIdNo) & "</lUserIdNo><sWebUidNo>" & CStr(sWebUidNo) & "</sWebUidNo></Inputs>", "", 1, 5)                

                // Make sure we are getting a UID passed in
                if (lUserIdNo < 1)
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), "clsJobStatus.getSiblingShops", "Invalid UID:", Convert.ToString(lUserIdNo)));

                // STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_sibling_shops";

                // STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_get_so_sibling_shops";

                // STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Concat("insert into session.so_sibling_shops (inst_uid_no, user_id_no, user_session_no) ",
                            "values (", lUserIdNo, ", '", sWebUidNo, "', '", sWebUidNo, "')");

                // STEP III (d) : Define the Select Statement to retrieve records
                //5220        sOutputColumns = "inst_uid_no, inst_nm, addr_1st, addr_2nd, city_nm, state_cd, parent_uid_no"
                sOutputColumns = "*";

                sFilter = "1 = 1 ORDER BY state_cd, city_nm, inst_nm, addr_1st, addr_2nd, inst_uid_no, parent_uid_no";

                //Call LogToAppLog("lynxJobStat.clsJobStatus.GetSiblingShops", "<sInsertSQL>" & CStr(sInsertSQL) & "</sInsertSQL>", "", 1, 5)

                // STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = objmodDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sIngresDSN, sFilter, sOutputColumns);

                if (oOutputRs == null)
                {
                    // Call LogToAppLog("clsJobStatus.getSiblingShops", "recordset returned by GetSessionRows is nothing", "", 25, 2)
                }

                if (oOutputRs.BOF || oOutputRs.EOF)
                {
                    // Call LogToAppLog("clsJobStatus.getSiblingShops", "no rows returned by GetSessionRows", "", 25, 2)
                }

                sOutput = objmodDataAccess.RsToXML(oOutputRs, true, null, "GetSiblingShops");
                sOutput = string.Concat("<GetDispatchShops CurInstaller=\"", lUserIdNo, "\">", sOutput,
                            "</GetDispatchShops>");
                // STEP VI : Transform the XML using a XSL Stylesheet
                // if a Stylesheet is specified then we need to transform the XML using that XSL Stylesheet
                if (sStylesheet != "")
                    sOutput = objmodDataAccess.TransformXML(sOutput, string.Concat(sStylesheetPath, sStylesheet));

                if (!(oOutputRs == null))
                    oOutputRs = null;
            }
            catch (Exception ex)
            {
                throw (ex);
            }

            return sOutput;
        }

        /// <summary>
        /// GetSummary(FUNCTION)
        /// Purpose:
        /// This function retrieves all ???.
        ///      a recordset containing all Dispatches that have been invoiced.
        ///      The three types are Not paid, Not billed, and Payment in process
        /// </summary>
        /// <param name="lUserIdNo">Long - the user id no of the parent account e.g. 9374</param>
        /// <param name="iUserTypeNo">Integer - (1=installer, 2=insurance company)</param>
        /// <param name="sNumber">String - the claim no or the policy no</param>
        /// <param name="iSummaryType">Integer - (1=invoiced, 2=open)</param>
        /// <param name="iOrderBy">Integer - determines the sort order of the returned recordset</param>
        /// <param name="sEnvId">String - holds the calling environment in the form: group_apptype_env (see comments in modCommon for details)</param>
        /// <returns>an XML string</returns>
        public string GetSummary(long lUserIdNo, int iUserTypeNo, string sNumber, int iSummaryType, int iOrderBy, string sEnvId)
        {
            //int intFound=0;

            string strFrom = string.Empty,
                strWhere = string.Empty;

            string sOutput = string.Empty,
                sSQL = string.Empty;

            string strOrderBy = string.Empty;
            Command oCommand = null;
            Connection oConnection = null;
            Recordset oRecordset = null;
            object lngRecordsAffected = null;

            try
            {
                oCommand = new Command();
                oCommand = new Command();
                oConnection = new Connection();
                oRecordset = new Recordset();
                objmodDataAccess = new lynxAccount4.modDataAccess();

#if(Developer == true)
		            // use the conditional compilation argument "Developer = 1"
                    // when debugging this component from your desktop
                    ObjectControl_Activate();  
#endif

                // Call LogToAppLog("lynxJobStat.clsJobStatus.GetSummary", "<Inputs><iSummaryType>" & CStr(iSummaryType) & "</iSummaryType><iOrderBy>" & CStr(iOrderBy) & "</iOrderBy><sNumber>" & CStr(sNumber) & "</sNumber><lUserIdNo>" & CStr(lUserIdNo) & "</lUserIdNo><iUserTypeNo>" & CStr(iUserTypeNo) & "</iUserTypeNo></Inputs>", "", 1, 5)

                // Make sure we are getting a UID passed in
                if (lUserIdNo < 1)
                    throw new Exception(string.Concat(Constants.vbObjectError.ToString(), "clsJobStatus.getSummary", "Invalid UID:", Convert.ToString(lUserIdNo)));

                oConnection.ConnectionTimeout = 60;
                oConnection.Open(sDatawarehouseDSN);
                switch (iSummaryType)
                {
                    case 1:
                        // Invoiced
                        switch (iOrderBy)
                        {
                            case 1:
                                strOrderBy = "invoice_date";
                                break;
                            case 2:
                                strOrderBy = "dispatch_no";
                                break;
                            case 3:
                                strOrderBy = "installer_invoice_no";
                                break;
                            case 4:
                                strOrderBy = "invoice_status";
                                break;
                            default:
                                strOrderBy = "invoice_date";
                                break;
                        }

                        oCommand.ActiveConnection = oConnection;
                        oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                        oCommand.CommandText = "lynx_ref_lookups.get_open_invoices";
                        oCommand.Parameters.Append(oCommand.CreateParameter("installer_ID", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 10, lUserIdNo));
                        oCommand.Parameters.Append(oCommand.CreateParameter("order_By", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 30, strOrderBy));
                        oRecordset = oCommand.Execute(out lngRecordsAffected, null, Convert.ToInt32(ExecuteOptionEnum.adExecuteStream));
                        break;
                    case 2:
                        // Open
                        switch (iOrderBy)
                        {
                            case 1:
                                strOrderBy = "dispatch_date";
                                break;
                            case 2:
                                strOrderBy = "dispatch_no";
                                break;
                            case 3:
                                strOrderBy = "insured_full_name";
                                break;
                            case 4:
                                strOrderBy = "dispatch_status";
                                break;
                            default:
                                strOrderBy = "dispatch_date";
                                break;
                        }

                        oCommand.ActiveConnection = oConnection;
                        oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                        oCommand.CommandText = "lynx_ref_lookups.get_open_dispatches";
                        oCommand.Parameters.Append(oCommand.CreateParameter("installer_ID", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 10, lUserIdNo));
                        oCommand.Parameters.Append(oCommand.CreateParameter("order_By", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 30, strOrderBy));
                        oRecordset = oCommand.Execute(out lngRecordsAffected, null, Convert.ToInt32(ExecuteOptionEnum.adExecuteStream));
                        break;

                    default:
                        // ERROR
                        break;
                }
                sOutput = objmodDataAccess.RsToXML(oRecordset, true, "record", "GetSummary");
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                oCommand = null;
                oConnection = null;
                oRecordset = null;
                oCommand.ActiveConnection = null;
            }

            return sOutput;
        }

        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        #endregion
    }
}
