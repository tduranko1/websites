﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MSXML2;
using System.Data.Objects;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    public class clsRetailerSearch
    {
        private ObjectContext moObjectContext = null;
        private string APP_NAME = "lynxAGC2.";
        private string MODULE_NAME = "clsRetailerSearch.";
       lynxAccount4.modCommon mobjCommon = null;

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tempParseError = null;
            try
            {
                oXMLDom = new DOMDocument40();
                mobjCommon = new lynxAccount4.modCommon();
                sOutput = mobjCommon.GetConfiguration("lynxAGC2");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);
                if (oXMLDom.parseError.errorCode == 0)
                {
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    lynxAccount4.modCommon.bActivateLog = Convert.ToBoolean(oXMLDom.selectSingleNode("//ActivateLog").text);
                }
                else
                {
                    tempParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tempParseError);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        private void ObjectControl_Deactivate()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string GetRetailers(string sCriteriaXml, [Optional]string sStylesheet)
        {
            Object oLocator = null;
            string sOutput = string.Empty;
            try
            {
                oLocator = new Object();
                sOutput = oLocator.LookupRetailers(sCriteriaXml, sStylesheet);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oLocator = null;
            }
            return sOutput;
        }

        private string StateFarmIDLookup(long lID)
        {
            string sXMLPath = string.Empty;
            string sResult = string.Empty;
            string sOutPut = string.Empty;
            FreeThreadedDOMDocument40 oXML = null;
            modCanadaCommon mobjCanadaCommon = null;
            IXMLDOMParseError tempParseError = null;
            try
            {
                oXML = new FreeThreadedDOMDocument40();
                mobjCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon();
                sXMLPath = mobjCanadaCommon.getXMLPath();
                oXML.load(string.Concat(sXMLPath, "sf_retailer.xml"));
                if (oXML.parseError.errorCode != 0)
                {
                    tempParseError = oXML.parseError;
                    mobjCommon.InvalidXML(ref tempParseError);
                }

                if (oXML.selectSingleNode(string.Concat("//StateFarmGlassVendor[@StateFarmGlassVendorID=", Convert.ToString(lID), "]")) != null)
                    sResult = oXML.selectSingleNode(string.Concat("//StateFarmGlassVendor[@StateFarmGlassVendorID=", Convert.ToString(lID), "]")).xml;
                sOutPut = string.Concat("<result>", sResult, "</result>");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sOutPut;
        }

        private string TransformXML(string sXML, string sStylesheetPath)
        {
            lynxAccount4.modDataAccess mobjDataAccess = null;
            modCanadaCommon mobjCanadaCommon = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCanadaCommon = new modCanadaCommon();
                if (sStylesheetPath != null)
                    sXML = mobjDataAccess.TransformXML(sXML, sStylesheetPath);
                if (sXML.IndexOf("<agc2ListBox") > 0)
                    sXML = mobjCanadaCommon.applyListBoxes(sXML);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return sXML;
        }

        public string GetListBox(string sType, string sName, [Optional]string sValue ="")
        {
            modCanadaCommon mobjCanadaCommon = null;
            string sXML = string.Empty;
            string tempreplace=string.Empty;
            try
            {
                mobjCanadaCommon = new modCanadaCommon();
                sXML = string.Concat("<root><agc2ListBox type=\"", sType, "\"name=\"", sName, "\" value=\"", sValue, "\"/></root>");
                sXML = mobjCanadaCommon.applyListBoxes(sXML);
                tempreplace=sXML.Replace("<root>", "");
                sXML = tempreplace.Replace("</root>", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sXML;
        }

    }
}
