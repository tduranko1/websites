﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMSVCSLib;
using MSXML2;
using ADODB;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Threading;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    //Description:
    //NOTE: this component uses a conditional compilation
    //    argument called "Developer". This argument must
    //    be set as follows:
    //    Developer = 0 (running from the com+ server)
    //    Developer = 1 (running from developer desktop)

    class clsAdjuster
    {
        lynxAccount4.modCommon mobjCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;

        private ObjectContext moObjectContext = null;
        private bool bLogError = false;
        //Define the Application name to be used in Error Handling
        private const string APP_NAME = "lynxAGC2.";
        private const string MODULE_NAME = "clsAdjuster.";

        private bool ObjectControl_CanBePooled()
        {
            return true;
        }

        private void ObjectControl_Activate()
        {

            string sOutput = string.Empty;
            //xml objects
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tmpParseError;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                sOutput = mobjCommon.GetConfiguration("lynxAGC2");

                oXMLDom = new DOMDocument40();
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);
                if (oXMLDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sFnolExDSN = oXMLDom.selectSingleNode("//DataSources/FnolEx").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    lynxAccount4.modCommon.bActivateLog = Convert.ToBoolean(oXMLDom.selectSingleNode("//ActivateLog").text);
                }
                else
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        public string GetProducts([Optional] int iProductTypeID)
        {
            string sOutput = string.Empty;
            Recordset oOutputRs = null;
            Command oCommand = null;
            Connection oConnection = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetProducts";
                oCommand.ActiveConnection = oConnection;

                if (iProductTypeID > 0)
                    oCommand.Parameters.Append(oCommand.CreateParameter("@ProductTypeID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iProductTypeID));

                oOutputRs = new Recordset();

                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);
                oOutputRs.ActiveConnection = null;
                oConnection.Close();

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }

        public string GetEntityStatus([Optional] int iEntityStatusID)
        {
            string sOutput = string.Empty;
            Recordset oOutputRs = null;
            Command oCommand = null;
            Connection oConnection = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oCommand = new Command();
                oCommand.CommandText = "uspGetEntityStatus";
                oCommand.ActiveConnection = oConnection;

                if (iEntityStatusID > 0)
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityStatusID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iEntityStatusID));

                oOutputRs = new Recordset();
                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);
                oOutputRs.ActiveConnection = null;
                oConnection.Close();

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }
        public string GetSystems([Optional] int iSystemID)
        {
            string sOutput = string.Empty;
            Recordset oOutputRs = null;
            Command oCommand = null;
            Connection oConnection = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetSystems";
                oCommand.ActiveConnection = oConnection;

                if (iSystemID > 0)
                    oCommand.Parameters.Append(oCommand.CreateParameter("@SystemID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iSystemID));

                oOutputRs = new Recordset();
                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);
                oOutputRs.ActiveConnection = null;
                oConnection.Close();

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }

        public string GetSpecialHandlings(int iProductTypeID, [Optional] string sSpecialHandlingType)
        {
            string sOutput = string.Empty;
            Recordset oOutputRs = null;
            Command oCommand = null;
            Connection oConnection = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetSpecialHandlings";
                oCommand.ActiveConnection = oConnection;

                oCommand.Parameters.Append(oCommand.CreateParameter("@ProductTypeID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iProductTypeID));
                if ((sSpecialHandlingType.Trim()) != string.Empty)
                    oCommand.Parameters.Append(oCommand.CreateParameter("@SpecialHandlingType", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 1, sSpecialHandlingType));

                oOutputRs = new Recordset();
                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);
                oOutputRs.ActiveConnection = null;
                oConnection.Close();

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);

                oOutputRs = null;
                oCommand = null;
                oConnection = null;

                return sOutput;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public string GetAllSpecialHandlings()
        {
            string sOutput = string.Empty;
            Recordset oOutputRs = null;
            Command oCommand = null;
            Connection oConnection = null;
            //SqlConnection sqlConnection = null;
            //SqlCommand sqlCommand = null;
            //Recordset oOutputRs = null;
            //string sp_Name = string.Empty;
            try
            {
                //TODO
                //sp_Name = "uspGetAllSpecialHandlings";
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                //sqlConnection = new SqlConnection(lynxAccount4.modCommon.sFnolExDSN);
                //sqlCommand = new SqlCommand(sp_Name, sqlConnection);
                //sqlConnection.Open();
                //sqlCommand.ExecuteNonQuery();

                //sqlCommand.CommandType = CommandType.StoredProcedure;
                //SqlParameter sp_param = new SqlParameter();
                //sqlCommand.Parameters.AddWithValue("@Name", "Somu");

                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetAllSpecialHandlings";
                oCommand.ActiveConnection = oConnection;

                oOutputRs = new Recordset();
                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);
                oOutputRs.ActiveConnection = null;
                oConnection.Close();

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }

        public void SaveAdjusterInfo(string inputXml)
        {
            //SqlCommand oCommand =null;
            //SqlConnection oConnection =null;
            Connection oConnection = null;
            Command oCommand = null;
            DOMDocument40 oXMLDom = null;
            string sAdjusterType = string.Empty;
            string sAdjusterGroup = string.Empty;
            string sAdjusterName = string.Empty;
            string sAdjusterAddress1 = string.Empty;
            string sAdjusterAddress2 = string.Empty;
            string sAdjusterCity = string.Empty;
            string sAdjusterState = string.Empty;
            string sAdjusterZip = string.Empty;
            string sAdjusterEmail = string.Empty;
            string sAdjusterOfficePhone = string.Empty;
            string sAdjusterOfficePhoneExt = string.Empty;
            string sAdjusterHomePhone = string.Empty;
            string sAdjusterCellPhone = string.Empty;
            string sLastModifiedBy = string.Empty;

            DateTime dtEffectiveDate;
            int iStatus = 0;
            string sFromDate = string.Empty;
            string sToDate = string.Empty;
            string sCATClaim = string.Empty;
            int iMaxClaimsPerDay = 0;
            IXMLDOMNode nodeBrands = null;
            IXMLDOMNode nodeSystems = null;
            IXMLDOMNode nodeLines = null;
            IXMLDOMNode nodeStatesExcluded = null;
            IXMLDOMNode nodeCounties = null;
            IXMLDOMNode nodeSpecialHandlings = null;
            long attributeID = 0;
            bool transactionStarted = false;
            object objtemp = null;
            try
            {
                oXMLDom = new DOMDocument40();
                oXMLDom.async = false;
                oXMLDom.loadXML(inputXml);

                if (oXMLDom.parseError.errorCode == 0)
                {
                    sAdjusterType = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/Type").text;
                    sAdjusterGroup = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/Group").text;
                    sAdjusterName = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/Name").text;
                    sAdjusterAddress1 = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/Address1").text;
                    sAdjusterAddress2 = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/Address2").text;
                    sAdjusterCity = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/City").text;
                    sAdjusterState = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/State").text;
                    sAdjusterZip = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/Zip").text;
                    sAdjusterEmail = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/Email").text;
                    sAdjusterOfficePhone = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/OfficePhone").text;
                    sAdjusterOfficePhoneExt = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/OfficePhoneExt").text;
                    sAdjusterHomePhone = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/HomePhone").text;
                    sAdjusterCellPhone = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/CellPhone").text;
                    sLastModifiedBy = oXMLDom.selectSingleNode("/AdjusterData/PersonalInfo/LastModifiedBy").text;
                    dtEffectiveDate = Convert.ToDateTime(oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/EffectiveDate").text);
                    iStatus = Convert.ToInt32(oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/Status").text);
                    sFromDate = oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/OOOFromDate").text;
                    sToDate = oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/OOOToDate").text;
                    sCATClaim = oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/CATClaim").text;
                    iMaxClaimsPerDay = Convert.ToInt32(oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/MaxClaims").text);

                    nodeBrands = oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/Brands");
                    nodeLines = oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/Lines");
                    nodeSystems = oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/Systems");
                    nodeStatesExcluded = oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/StatesExcluded");
                    nodeCounties = oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/Counties");
                    nodeSpecialHandlings = oXMLDom.selectSingleNode("/AdjusterData/ClaimInfo/SpecialHandlings");

                    oConnection = new Connection();
                    oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                    oCommand = new Command();
                    oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                    oCommand.CommandText = "uspSaveAdjusterInfo";
                    oCommand.ActiveConnection = oConnection;

                    transactionStarted = true;

                    // Save Adjuster's Personal and Claim Info */
                    oCommand.Parameters.Append(oCommand.CreateParameter("@ret_val", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, attributeID));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityType", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 1, sAdjusterType));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityGroupID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, sAdjusterGroup));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityEmail", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, sAdjusterEmail.Trim()));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityName", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, sAdjusterName.Trim()));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@Address1", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, (sAdjusterAddress1.Trim() != string.Empty) ? sAdjusterAddress1.Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@Address2", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, (sAdjusterAddress2.Trim() != string.Empty) ? sAdjusterAddress2.Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@City", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, (sAdjusterCity.Trim() != string.Empty) ? sAdjusterCity.Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@State", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 2, (sAdjusterState.Trim() != string.Empty) ? sAdjusterState.Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@Zip", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, (sAdjusterZip.Trim() != string.Empty) ? sAdjusterZip.Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@OfficePhone", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 12, (sAdjusterOfficePhone.Trim() != string.Empty) ? sAdjusterOfficePhone.Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@OfficePhoneExtention", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 8, (sAdjusterOfficePhoneExt.Trim() != string.Empty) ? sAdjusterOfficePhoneExt.Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@HomePhone", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 12, (sAdjusterHomePhone.Trim() != string.Empty) ? sAdjusterHomePhone.Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@CellPhone", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 12, (sAdjusterCellPhone.Trim() != string.Empty) ? sAdjusterCellPhone.Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@LastModifiedBy", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, sLastModifiedBy));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EffectiveDate", DataTypeEnum.adDate, ParameterDirectionEnum.adParamInput, 0, dtEffectiveDate.ToString("yyyy-mm-dd")));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityStatusID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iStatus));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@CATClaim", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, Convert.ToBoolean(sCATClaim = "1") ? 1 : 0));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@MaxClaimsPerDay", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iMaxClaimsPerDay));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@FromDate", DataTypeEnum.adDate, ParameterDirectionEnum.adParamInput, 0, (sFromDate.Trim() != string.Empty) ? Convert.ToDateTime(sFromDate).ToString("yyyy-mm-dd").Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@ToDate", DataTypeEnum.adDate, ParameterDirectionEnum.adParamInput, 0, (sToDate.Trim() != string.Empty) ? Convert.ToDateTime(sToDate).ToString("yyyy-mm-dd").Trim() : null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@BrandData", DataTypeEnum.adLongVarChar, ParameterDirectionEnum.adParamInput, (nodeBrands.xml.Length), nodeBrands.xml));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@LineData", DataTypeEnum.adLongVarChar, ParameterDirectionEnum.adParamInput, (nodeLines.xml.Length), nodeLines.xml));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@SystemData", DataTypeEnum.adLongVarChar, ParameterDirectionEnum.adParamInput, (nodeSystems.xml.Length), nodeSystems.xml));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@StateData", DataTypeEnum.adLongVarChar, ParameterDirectionEnum.adParamInput, (nodeStatesExcluded.xml.Length), nodeStatesExcluded.xml));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@CountyData", DataTypeEnum.adLongVarChar, ParameterDirectionEnum.adParamInput, (nodeCounties.xml.Length), nodeCounties.xml));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@SpecialHandlingData", DataTypeEnum.adLongVarChar, ParameterDirectionEnum.adParamInput, (nodeSpecialHandlings.xml.Length), nodeSpecialHandlings.xml));
                    oCommand.Execute(out objtemp);
                    attributeID = Convert.ToSByte(oCommand.Parameters["@ret_val"].Value);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCommand = null;
                oConnection.Close();
                oCommand = null;
                oConnection = null;
                oXMLDom = null;

                if (transactionStarted)
                {
                    if (oConnection != null)
                    {
                        if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                            oConnection.Close();
                        oConnection = null;
                    }
                }
            }
        }

        public string GetAdjusterInfo(string inputXml)
        {
            Command oCommand = null;
            Recordset oOutputRs = null;
            Connection oConnection = null;
            string sOutput = string.Empty;
            string columnNames = string.Empty;
            string sEmailID = string.Empty;
            string sBrandID = string.Empty;
            string sEntityType = string.Empty;
            string sEntityGroupID = string.Empty;
            DOMDocument40 oXMLDom = null;
            try
            {
                oXMLDom = new DOMDocument40();
                oOutputRs = new Recordset();
                oCommand = new Command();
                oConnection = new Connection();
                oXMLDom.async = false;
                oXMLDom.loadXML(inputXml);
                sEmailID = oXMLDom.selectSingleNode("//EmailID").text;
                sBrandID = oXMLDom.selectSingleNode("//BrandID").text;
                sEntityType = oXMLDom.selectSingleNode("//EntityType").text;
                sEntityGroupID = oXMLDom.selectSingleNode("//EntityGroupID").text;

                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetAdjusterInfo";
                oCommand.ActiveConnection = oConnection;
                if (sEmailID.Trim() != string.Empty)
                {
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityEmail", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, sEmailID.Trim()));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@ClientID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityType", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 1, null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityGroupID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, null));
                    columnNames = "entity,entityAttribute,entityBrand,entityLine,entitySystem,entityState,entityCounty,entitySpecial";
                }
                else
                {
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityEmail", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, null));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@ClientID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, Convert.ToBoolean((sBrandID.Trim() == string.Empty) || (sBrandID == "0")) ? null : sBrandID));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityType", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 1, Convert.ToBoolean(sEntityType.Trim() == string.Empty) ? null : sEntityType));
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityGroupID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, Convert.ToBoolean(sEntityGroupID.Trim() == string.Empty) ? null : sEntityGroupID));
                    columnNames = "entity,entityBrand,entityLine,entitySystem,entityState,entityCounty,entitySpecial";
                }
                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);

                sOutput = RsMultiToXML(oOutputRs, true, columnNames);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.ActiveConnection = null;
                oOutputRs.Close();
                oOutputRs = null;
                oConnection.Close();
                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return sOutput;
        }

        public string SaveSpecialHandlings(string inputXml)
        {
            Command oCommand = null;
            Recordset oOutputRs = null;
            Connection oConnection = null;
            string sOutput = string.Empty;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oOutputRs = new Recordset();
                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspSaveSpecialHandling";
                oCommand.ActiveConnection = oConnection;

                oCommand.Parameters.Append(oCommand.CreateParameter("@SpecialHandlingData", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, inputXml.Length, inputXml));

                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.ActiveConnection = null;
                oOutputRs.Close();
                oOutputRs = null;
                oConnection.Close();
                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return sOutput;
        }

        public string DeleteSpecialHandlings(string inputXml)
        {
            Command oCommand = null;
            Recordset oOutputRs = null;
            Connection oConnection = null;
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            string sSpecialHandlingType = string.Empty;
            string sSpecialHandlingMatchType = string.Empty;
            string sSpecialHandlingValue = string.Empty;

            try
            {
                oXMLDom = new DOMDocument40();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom.async = false;
                oXMLDom.loadXML(inputXml);

                sSpecialHandlingType = oXMLDom.selectSingleNode("/SpecialHandling").attributes[0].text;
                sSpecialHandlingMatchType = oXMLDom.selectSingleNode("/SpecialHandling").attributes[1].text;
                sSpecialHandlingValue = oXMLDom.selectSingleNode("/SpecialHandling").attributes[2].text;

                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oOutputRs = new Recordset();
                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspDeleteSpecialHandling";
                oCommand.ActiveConnection = oConnection;

                oCommand.Parameters.Append(oCommand.CreateParameter("@SpecificHandlingType", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 1, sSpecialHandlingType));
                oCommand.Parameters.Append(oCommand.CreateParameter("@SpecificHandlingMatchType", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 2, sSpecialHandlingMatchType));
                oCommand.Parameters.Append(oCommand.CreateParameter("@SpecificHandlingValue", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 20, sSpecialHandlingValue));

                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.ActiveConnection = null;
                oOutputRs.Close();
                oOutputRs = null;
                oConnection.Close();

                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return sOutput;
        }

        public string ProfileExists(string emailID)
        {
            Command oCommand = null;
            Connection oConnection = null;
            bool returnValue = false;
            string outputXml = string.Empty;
            object objTemp = null;
            try
            {
                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspAdjusterExists";
                oCommand.ActiveConnection = oConnection;

                oCommand.Parameters.Append(oCommand.CreateParameter("@ret_val", DataTypeEnum.adBoolean, ParameterDirectionEnum.adParamReturnValue, 0, returnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("@EntityEmail", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, emailID));
                oCommand.Execute(out objTemp);

                returnValue = Convert.ToBoolean(oCommand.Parameters["@ret_val"].Value);
                oConnection.Close();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCommand = null;
                oConnection = null;
                outputXml = string.Concat("<records><record ReturnValue=", Convert.ToChar(34), (returnValue ? "True" : "False"), Convert.ToChar(34), " /></records>");

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return outputXml;
        }

        public string GetUserAccessLevel(long UserID, long ContainerObjectID)
        {
            Command oCommand = null;
            Connection oConnection = null;
            int returnValue = 0;
            string outputXml = string.Empty;
            object objTemp = null;
            try
            {
                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sCairDSN);

                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetUserAccessLevel";
                oCommand.ActiveConnection = oConnection;

                oCommand.Parameters.Append(oCommand.CreateParameter("@ret_val", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, returnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("@UID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, UserID));
                oCommand.Parameters.Append(oCommand.CreateParameter("@COID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, ContainerObjectID));
                oCommand.Execute(out objTemp);

                returnValue = Convert.ToInt32(oCommand.Parameters["@ret_val"].Value);
                oConnection.Close();

                outputXml = string.Concat("<records><record ReturnValue=", Convert.ToChar(34), returnValue, Convert.ToChar(34), " /></records>");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return outputXml;
        }
        public string GetAdjusterPool(string inputXml)
        {
            Command oCommand = null;
            Recordset oOutputRs = null;
            Connection oConnection = null;
            string sOutput = string.Empty;
            string sBrandID = string.Empty;
            string sSystemID = string.Empty;
            string sProductTypeID = string.Empty;
            string sCATClaim = string.Empty;
            string sEntityType = string.Empty;
            string sLossState = string.Empty;
            string sLossCounty = string.Empty;
            string sPolicyNumber = string.Empty;
            string sAgencyNumber = string.Empty;
            string sOnlyActiveAdjuster = string.Empty;
            DOMDocument40 oXMLDom = null;
            try
            {
                oXMLDom = new DOMDocument40();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom.async = false;
                oXMLDom.loadXML(inputXml);
                sBrandID = oXMLDom.selectSingleNode("//BrandID").text;
                sSystemID = oXMLDom.selectSingleNode("//SystemID").text;
                sProductTypeID = oXMLDom.selectSingleNode("//ProductTypeID").text;
                sCATClaim = oXMLDom.selectSingleNode("//CATClaim").text;
                sEntityType = oXMLDom.selectSingleNode("//EntityType").text;
                sLossState = oXMLDom.selectSingleNode("//LossState").text;
                sLossCounty = oXMLDom.selectSingleNode("//LossCounty").text;
                sPolicyNumber = oXMLDom.selectSingleNode("//PolicyNumber").text;
                sAgencyNumber = oXMLDom.selectSingleNode("//AgencyNumber").text;
                sOnlyActiveAdjuster = oXMLDom.selectSingleNode("//ActiveOnly").text;

                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oOutputRs = new Recordset();
                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetAdjusterPool";
                oCommand.ActiveConnection = oConnection;
                oCommand.Parameters.Append(oCommand.CreateParameter("@ClientID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, sBrandID));
                oCommand.Parameters.Append(oCommand.CreateParameter("@SystemID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, sSystemID));
                oCommand.Parameters.Append(oCommand.CreateParameter("@ProductTypeID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, sProductTypeID));
                oCommand.Parameters.Append(oCommand.CreateParameter("@CATClaim", DataTypeEnum.adBoolean, ParameterDirectionEnum.adParamInput, 0, sCATClaim));
                oCommand.Parameters.Append(oCommand.CreateParameter("@FieldOrTCC", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 1, sEntityType));
                oCommand.Parameters.Append(oCommand.CreateParameter("@StateOfLoss", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 2, sLossState));
                oCommand.Parameters.Append(oCommand.CreateParameter("@CountyOfLoss", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 30, sLossCounty));
                oCommand.Parameters.Append(oCommand.CreateParameter("@PolicyNumber", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 20, sPolicyNumber));
                oCommand.Parameters.Append(oCommand.CreateParameter("@PolicyAgencyNumber", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 20, sAgencyNumber));
                oCommand.Parameters.Append(oCommand.CreateParameter("@OnlyActiveAdjusters", DataTypeEnum.adBoolean, ParameterDirectionEnum.adParamInput, 0, sOnlyActiveAdjuster));

                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.ActiveConnection = null;
                oOutputRs.Close();
                oOutputRs = null;
                oConnection.Close();
                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return sOutput;
        }

        public string GetAdjusterGroups(string inputXml)
        {
            Command oCommand = null;
            Recordset oOutputRs = null;
            Connection oConnection = null;
            string sEntityGroupID = string.Empty;
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom = new DOMDocument40();
                oXMLDom.async = false;
                oXMLDom.loadXML(inputXml);
                sEntityGroupID = oXMLDom.selectSingleNode("//EntityGroupID").text;

                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oOutputRs = new Recordset();
                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetAdjusterGroups";
                oCommand.ActiveConnection = oConnection;
                if (sEntityGroupID.Trim() != string.Empty)
                    oCommand.Parameters.Append(oCommand.CreateParameter("@EntityGroupID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, sEntityGroupID.Trim()));

                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.ActiveConnection = null;
                oOutputRs.Close();
                oOutputRs = null;
                oConnection.Close();
                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return sOutput;
        }

        public string SaveAdjusterGroup(string inputXml)
        {
            Command oCommand = null;
            Recordset oOutputRs = null;
            Connection oConnection = null;
            string sEntityGroupID = string.Empty;
            string sOutput = string.Empty;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oOutputRs = new Recordset();
                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspSaveEntityGroup";
                oCommand.ActiveConnection = oConnection;
                oCommand.Parameters.Append(oCommand.CreateParameter("@GroupData", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, inputXml.Length, inputXml));
                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.ActiveConnection = null;
                oOutputRs.Close();
                oOutputRs = null;
                oConnection.Close();

                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return sOutput;
        }

        public string DeleteAdjusterGroup(string inputXml)
        {
            Command oCommand = null;
            Recordset oOutputRs = null;
            Connection oConnection = null;
            string sEntityGroupID = string.Empty;
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            try
            {
                oXMLDom = new DOMDocument40();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom.async = false;
                oXMLDom.loadXML(inputXml);
                sEntityGroupID = oXMLDom.selectSingleNode("//EntityGroupID").text;

                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oOutputRs = new Recordset();
                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspDeleteEntityGroup";
                oCommand.ActiveConnection = oConnection;
                oCommand.Parameters.Append(oCommand.CreateParameter("@EntityGroupID", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, sEntityGroupID.Trim()));
                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.ActiveConnection = null;
                oOutputRs.Close();
                oOutputRs = null;
                oConnection.Close();
                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return sOutput;
        }

        public string GetStates()
        {
            Command oCommand = null;
            Recordset oOutputRs = null;
            Connection oConnection = null;
            string sOutput = string.Empty;

            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oConnection = new Connection();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oOutputRs = new Recordset();
                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetStates";
                oCommand.ActiveConnection = oConnection;
                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockReadOnly);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs.ActiveConnection = null;
                oOutputRs.Close();
                oOutputRs = null;
                oConnection.Close();
                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return sOutput;
        }

        public string GetCountiesByState(string StateCode)
        {
            Command oCommand = null;
            Recordset oOutputRs = null;
            Connection oConnection = null;
            string sOutput = string.Empty;
            try
            {
                
                oConnection = new Connection();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oConnection.Open(lynxAccount4.modCommon.sFnolExDSN);

                oOutputRs = new Recordset();
                oCommand = new Command();
                oCommand.CommandType = CommandTypeEnum.adCmdStoredProc;
                oCommand.CommandText = "uspGetCountiesByState";
                oCommand.ActiveConnection = oConnection;
                oCommand.Parameters.Append(oCommand.CreateParameter("@StateCode", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 2, StateCode));
                oOutputRs.CursorLocation = CursorLocationEnum.adUseClient;
                oOutputRs.Open(oCommand, null, CursorTypeEnum.adOpenStatic, LockTypeEnum.adLockOptimistic);
                oOutputRs.ActiveConnection = null;

                if (!(oOutputRs.BOF) && (oOutputRs.EOF))
                {
                    while (!oOutputRs.EOF)
                    {
                        oOutputRs.Fields["County"].Value = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(Convert.ToString(oOutputRs.Fields["County"].Value));
                        oOutputRs.Update();
                        oOutputRs.MoveNext();
                    }

                    oOutputRs.MoveFirst();
                }

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
                oConnection.Close();
                oCommand = null;
                oConnection = null;

                if (oConnection != null)
                {
                    if (oConnection.State == (int)ObjectStateEnum.adStateOpen)
                    {
                        oConnection.Close();
                        oConnection = null;
                    }
                }
            }
            return sOutput;
        }
    }
}
