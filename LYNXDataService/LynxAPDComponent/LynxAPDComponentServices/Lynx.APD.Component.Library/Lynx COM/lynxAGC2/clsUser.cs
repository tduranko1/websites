﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using MSXML2;
using ADODB;
using System.Collections;
using Microsoft.VisualBasic;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    public class clsUser
    {
        private ObjectContext moObjectContext = null;
        private string APP_NAME = "lynxAGC2.";
        private string MODULE_NAME = "clsUser.";
        lynxAccount4.modCommon mobjCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;
        private string GetUserInfo(string sXML)
    {
        string sSQL = string.Empty,
               sOutput = string.Empty;
        long lUserID;
        int iAppID;
        DOMDocument40 oXMLDom = null;
        IXMLDOMNode oXMLNode = null;
        Connection oConnection = null;
        Recordset oRecordset = null;
        Command oCmd = null;
        object activeConection=Type.Missing;
        try
        {
            oConnection = new Connection();
            oRecordset = new Recordset();
            oCmd = new Command();
            oXMLDom = new DOMDocument40();
            mobjDataAccess=new lynxAccount4.modDataAccess();
            oConnection.Open(lynxAccount4.modCommon.sCairDSN);
            oXMLDom.loadXML(sXML);
            lUserID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/UserID").text);
            iAppID=Convert.ToInt32(oXMLDom.selectSingleNode("//header/AppID").text);

            oCmd.CommandText="sp_AGC2_GetUserInfo";
            oCmd .CommandType = CommandTypeEnum.adCmdStoredProc;
            oCmd.ActiveConnection = oConnection;
            //'.Parameters.Refresh
            oCmd.Parameters[0].Value = 0;
            oCmd.Parameters[1].Value = lUserID;
            oCmd.Parameters[2].Value = iAppID;
            oRecordset.CursorLocation = CursorLocationEnum.adUseClient;
            oRecordset.Open(oCmd, activeConection,CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly);
            //'process the recordset
            sOutput = mobjDataAccess.RsToXML(oRecordset,true,"row", "user");

        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            oRecordset=null;
            oConnection=null;
            oCmd=null;
            oXMLDom=null;
            oXMLNode=null;
        }
        return sOutput;
    }

        public void UpdateUserInfo(string sXML)
        {
            string sSQL = string.Empty;
            long lUserID;
            int iAppID;
            int iUAITID;
            int iValue;
            object recordsetcon = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            Connection oConnection = null;
            try
            {
                oXMLDom = new DOMDocument40();
                oConnection = new Connection();
                oXMLDom.loadXML(sXML);
                lUserID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/UserID").text);
                iAppID = Convert.ToInt32(oXMLDom.selectSingleNode("//header/AppID").text);
                iUAITID = Convert.ToInt32(oXMLDom.selectSingleNode("//row").attributes.getNamedItem("UAITID").text);
                iValue = Convert.ToInt32(oXMLDom.selectSingleNode("//row").attributes.getNamedItem("UAIValue").text);
                sSQL = string.Concat("EXEC sp_AGC2_UpdateUserInfo ", lUserID, ",", iAppID, ",", iUAITID, ",", iValue);

                oConnection.Open(lynxAccount4.modCommon.sCairDSN);
                oConnection.Execute(sSQL, out recordsetcon);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oConnection = null;
                oXMLDom = null;
                oXMLNode = null;
            }
        }

        public string GetGlassProgramID(string sXML)
        {
            long Erl = 0;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjCommon.CtxRaiseError(MODULE_NAME, "GetGlassProgramID", Constants.vbObjectError + 1000, "Obsolete Procedure", Erl);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return string.Empty;
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                oXMLDom = new DOMDocument40();
                mobjCommon = new lynxAccount4.modCommon();
                sOutput = mobjCommon.GetConfiguration("lynxAGC2");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);
                if (oXMLDom.parseError.errorCode == 0)
                {
                    //'retrieve the values here
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    lynxAccount4.modCommon.bActivateLog = Convert.ToBoolean(oXMLDom.selectSingleNode("//ActivateLog").text);
                }
                else
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        private void ObjectControl_Deactivate()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
