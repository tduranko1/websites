﻿using System.Web;
using System.Data.Objects;
using System.Runtime.InteropServices;
using System.Web.Mail;
using System;
using MSXML2;
using ADODB;
using Microsoft.VisualBasic;


namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    public class clsParent
    {
        private string APP_NAME = "lynxAGC2.";
        private string MODULE_NAME = "clsParent.";
        private string sXMLPath = string.Empty,
                       sSessionID = string.Empty;
        modAGC2Common objmodAGC2Common = null;
        modCanadaCommon objmodCanadaCommon = null;
        lynxAccount4.modCommon mobjCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;
        public string GetCompanyInformation(string sInputXML, [Optional]string sStylesheet = "")
        {
            string sOutput = string.Empty,
                   sCompanyDataXML = string.Empty,
                   sProgramInfoXML = string.Empty,
                   sUserID = string.Empty,
                   sGpaCode = string.Empty,
                   strParam = string.Empty,
                   sProfileXML = string.Empty;
            IXMLDOMParseError tmpParseError = null;
            int iUserType;
            long lInstallerID;
            long lAdminNumber;
            bool bServiceCenters;
            FreeThreadedDOMDocument40 oXMLIn = null;
            DOMDocument40 objDomxml;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLIn = new FreeThreadedDOMDocument40();
                objmodCanadaCommon = new modCanadaCommon();
                objmodAGC2Common = new modAGC2Common();
                oXMLIn.loadXML(sInputXML);

                if (oXMLIn.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLIn.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                objDomxml = (DOMDocument40)oXMLIn;
                strParam = "InstallerID";
                lInstallerID = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDomxml, ref strParam));
                strParam = "AdminNumber";
                lAdminNumber = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDomxml, ref strParam));
                strParam = "UserType";
                iUserType = Convert.ToInt32(objmodCanadaCommon.getNodeValue(ref objDomxml, ref strParam));
                strParam = "ServiceCenters";

                if (Left(objmodCanadaCommon.getNodeValue(ref objDomxml, ref strParam), 1).ToUpper() == "T")
                    bServiceCenters = true;
                else
                    bServiceCenters = false;

                strParam = "UserID";
                sUserID = objmodCanadaCommon.getNodeValue(ref objDomxml, ref strParam);
                strParam = "UserSession";
                sSessionID = objmodCanadaCommon.getNodeValue(ref objDomxml, ref strParam);
                strParam = "GpaCode";
                sGpaCode = objmodCanadaCommon.getNodeValue(ref objDomxml, ref strParam);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat("<rec name=\"GetCompanyInformation\" inst_uid_no=\"", lInstallerID, "\">");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"****GetCompanyData\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sCompanyDataXML = GetCompanyData2(lInstallerID, bServiceCenters, sUserID, "", iUserType);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"****GetCompanyData\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (!bServiceCenters)
                {
                    //get other company information
                }
                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"****GetCompanyPrograms\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sProgramInfoXML = GetCompanyPrograms(string.Concat("<GetCompanyPrograms><InstallerID>", Convert.ToString(lInstallerID),
                    "</InstallerID>", "<GpaCode>", sGpaCode, "</GpaCode>", "<UserID>", sUserID, "</UserID><UserSession>", sSessionID, "</UserSession></GetCompanyPrograms>"));

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"****GetCompanyPrograms\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"****GetCompanyProfileItems\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sProfileXML = GetCompanyProfileItems(lInstallerID, "", sUserID);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"****GetCompanyProfileItems\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"****GetCompanyProfileItems\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sProfileXML = GetCompanyProfileItems(lInstallerID, "", sUserID);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"****GetCompanyProfileItems\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "</rec>");

                if (lynxAccount4.modCommon.bActivateLog)
                    mobjCommon.LogToAppLog("lynxAGC2.clsParent.GetCompanyInformation", objmodAGC2Common.sLogTime, "", 1, 27);

                sOutput = string.Concat("<data>", sCompanyDataXML, sProgramInfoXML, sProfileXML, "</data>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLIn = null;
            }
            return sOutput;
        }
        public string GetCompanyData(long lInstallerUidNo, bool bServiceCenters, [Optional]string sStylesheet = "")
        {
            string strCompanyData = string.Empty;
            try
            {
                strCompanyData = GetCompanyData2(lInstallerUidNo, bServiceCenters, sStylesheet," ", 0);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strCompanyData;
        }
        public string GetCompanyPrograms(string sInputXML, [Optional]string sStylesheet = "")
        {
            string cACTION_CODE = "GET";
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sInsertSQL = string.Empty,
                    sSelectSql = string.Empty,
                    sOutput = string.Empty,
                    sUserID = string.Empty,
                    sUserSession = string.Empty,
                    sGpaCode = string.Empty,
                    strParam=string.Empty,
                    sFilter = string.Empty;
            long lInstallerID;
            Recordset oOutputRs = null;
            FreeThreadedDOMDocument40 oXMLIn = null;
            IXMLDOMParseError tmpParseError = null;
            DOMDocument40 objDOMDocument = null;
            try
            {
                objmodAGC2Common = new modAGC2Common();
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLIn.loadXML(sInputXML);
                if (oXMLIn.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLIn.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }
                objDOMDocument = (DOMDocument40)oXMLIn;
                strParam = "InstallerID";
                lInstallerID = Convert.ToInt64(objmodAGC2Common.getNodeValue(ref objDOMDocument, ref strParam));
                strParam = "UserID";
                sUserID = objmodAGC2Common.getNodeValue(ref objDOMDocument, ref strParam);
                strParam = "GpaCode";
                sGpaCode = objmodAGC2Common.getNodeValue(ref objDOMDocument, ref strParam);
                strParam = "UserSession";
                sUserSession = objmodAGC2Common.getNodeValue(ref objDOMDocument, ref strParam);

                sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no, action_cd, gpa_cd , user_id_no, user_session_no)",
                             "values (", lInstallerID, ",'", cACTION_CODE, "', '", sGpaCode, "', '", sUserID, "', '", sUserSession, "')");
                sSelectSql = "*";
                sFilter = string.Empty;
                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "program", "companyprograms");
                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs = null;
                oXMLIn = null;
            }
            return sOutput;
        }

        private string GetCompanyLicenesesAndAccredXML(long lInstallerID, string sXML)
    {
        string sCompany_licensesXMLLookup = string.Empty,
               sCompany_accreditationsXMLLookup = string.Empty,
               strServiceCenterStateCodes = string.Empty,
               strServiceCenterStateCodesXML = string.Empty,
               strLicenses = string.Empty,
               sUserID = string.Empty,
               strServiceCenterStateCodesTest = string.Empty,
               strLookUpLicensesParentIDs = string.Empty,
               strLookUpLicenses = string.Empty,
               strActiveLicenseStateCodes = string.Empty,
               strFilteredStateCodeParentIDs = string.Empty,
               strFilteredStateCodes = string.Empty,
               strFilteredParentIDsWithStateCodes = string.Empty,
               strParentID = string.Empty,
               strParam=string.Empty,
               strFilteredLicenses = string.Empty;
        string strCompanyLicenesesAndAccredXML=string.Empty;
        int iUserType, intServiceCenterCount, intLicenseCount, intValidSCLicenseCount,i;
        DOMDocument40 oXMLDom = null;
        DOMDocument40 oXMLDomChild = null;
        IXMLDOMNodeList oXmlNodeList = null;
        IXMLDOMNodeList oXmlNodeListChild = null;
        IXMLDOMParseError tmpParseError=null;
        string[] arrParentID, arrStateArray;
        try
        {
            oXMLDom = new DOMDocument40();
            oXMLDomChild=new DOMDocument40();
            mobjCommon=new lynxAccount4.modCommon();
            mobjDataAccess=new lynxAccount4.modDataAccess();
            objmodAGC2Common = new modAGC2Common();
            oXMLDom.loadXML(sXML);
            strParam = "UserID";
            sUserID = objmodAGC2Common.getNodeValue(ref oXMLDom, ref strParam);
            strParam = "UserType";
            iUserType = Convert.ToInt32(objmodAGC2Common.getNodeValue(ref oXMLDom, ref strParam));
            strServiceCenterStateCodesXML = GetCompanyData2(lInstallerID, true, sUserID, "", iUserType);
            oXMLDom.loadXML(strServiceCenterStateCodesXML);
            oXmlNodeList = oXMLDom.selectNodes("//record[not(@state_cd=preceding-sibling::node()/attribute::state_cd)]");
            intServiceCenterCount = oXmlNodeList.length;
            foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
            {
                if (oXMLNode != null)
                    strServiceCenterStateCodes = string.Concat(strServiceCenterStateCodes,oXMLNode.attributes[3].text,"^");
            }
            strServiceCenterStateCodesTest = strServiceCenterStateCodes;
            arrStateArray = strServiceCenterStateCodes.Split('^');
            oXMLDom.load(string.Concat(sXMLPath, "lookup.xml"));

            if (oXMLDom.parseError.errorCode != 0)
            {
                tmpParseError=oXMLDom.parseError;
                mobjCommon.InvalidXML(ref tmpParseError);
            }

            oXmlNodeList = oXMLDom.selectNodes("//ObjectGroup[@ObjectGroupName = 'company_licenses']/Object");
            intLicenseCount = oXmlNodeList.length;

            foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
            {
                if (oXMLNode != null)
                {
                    strParentID = string.Concat(strParentID, oXMLNode.attributes[1].text, "^");
                    strLicenses = string.Concat(strLicenses, "<strLicenses>",oXMLNode.xml,"</strLicenses>");
                }
            }

            strLookUpLicensesParentIDs = strParentID;
            strLookUpLicenses = strLicenses;
            arrParentID = strParentID.Split('^');
            oXMLDomChild.load(string.Concat(sXMLPath,"lookup_xref.xml"));

            if(oXMLDomChild.parseError.errorCode!=0)
            {
                tmpParseError=oXMLDomChild.parseError;
                mobjCommon.InvalidXML(ref tmpParseError);
            }

            for(i=0;i!=intLicenseCount;i++)
            {
              oXmlNodeListChild = oXMLDomChild.selectNodes(string.Concat("//XrefObjectGroup/ParentObjectGroup[@ParentObjectGroupName= 'company_licenses']/ChildObjectGroup[@CHildParentObjectID= '", arrParentID[i],"' and @StatusID = '1']"));
                foreach(IXMLDOMNode oXMLNodeChild in oXmlNodeListChild)
                {
                    if(oXMLNodeChild!=null)
                        sCompany_licensesXMLLookup = string.Concat(sCompany_licensesXMLLookup, oXMLNodeChild.xml);
                }
            }
             strActiveLicenseStateCodes = sCompany_licensesXMLLookup;
             sCompany_licensesXMLLookup = string.Concat("<getStateCodes>",sCompany_licensesXMLLookup, "</getStateCodes>");
             oXMLDomChild.loadXML(sCompany_licensesXMLLookup);

            if(oXMLDomChild.parseError.errorCode!=0)
            {
                tmpParseError=oXMLDomChild.parseError;
                mobjCommon.InvalidXML(ref tmpParseError);
            }

            sCompany_licensesXMLLookup =string.Empty;
            strParentID=string.Empty;

            for(i=0;i!=intServiceCenterCount;i++)
            {
                oXmlNodeListChild = oXMLDomChild.selectNodes(string.Concat("//ChildObjectGroup[@ChildObjectCode= '", arrStateArray[i], "']"));
                foreach(IXMLDOMNode oXMLNodeChild in oXmlNodeListChild)
                {
                    if(oXMLNodeChild!=null)
                    {
                        strParentID = string.Concat(strParentID , oXMLNodeChild.attributes[1].text, "^");
                        sCompany_licensesXMLLookup = string.Concat(sCompany_licensesXMLLookup,oXMLNodeChild.xml);
                    }
                }
            }

            strFilteredStateCodeParentIDs = strParentID;
            strFilteredStateCodes = sCompany_licensesXMLLookup;
            sCompany_licensesXMLLookup = string.Concat("<sCompany_licensesXMLLookup>", sCompany_licensesXMLLookup, "</sCompany_licensesXMLLookup>");
            oXMLDomChild.loadXML(sCompany_licensesXMLLookup);
            if(oXMLDomChild.parseError.errorCode!=0)
            {
                 tmpParseError=oXMLDomChild.parseError;
                 mobjCommon.InvalidXML(ref tmpParseError);
            }

            oXmlNodeListChild = oXMLDomChild.selectNodes("//ChildObjectGroup[not(@CHildParentObjectID=preceding-sibling::node()/attribute::CHildParentObjectID)]");
            intValidSCLicenseCount = oXmlNodeListChild.length;
            strParentID = string.Empty;

            foreach(IXMLDOMNode oXMLNodeChild in oXmlNodeListChild)
            {
                if(oXMLNodeChild!=null)
                    strParentID = string.Concat(strParentID, oXMLNodeChild.attributes[6].text, "^");
            }

            strFilteredParentIDsWithStateCodes = strParentID;
            arrParentID = strParentID.Split('^');
            strLicenses = string.Concat("<strLicenses1>", strLicenses, "</strLicenses1>");
            oXMLDom.loadXML(strLicenses);

            if(oXMLDom.parseError.errorCode!=0)
            {
                tmpParseError=oXMLDom.parseError;
                mobjCommon.InvalidXML(ref tmpParseError);
            }

            for(i=0;i!=intValidSCLicenseCount;i++)
            {
                oXmlNodeList = oXMLDom.selectNodes(string.Concat("//Object[@ObjectID = '", arrParentID[i], "']"));
                foreach(IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    if(oXMLNode!=null)
                        strLicenses = string.Concat(strLicenses,"<strLicenses>",oXMLNode.xml,"</strLicenses>");
                }
            }
            strFilteredLicenses = strLicenses;
            oXMLDom.load(string.Concat(sXMLPath,"lookup.xml"));

            if(oXMLDom.parseError.errorCode!=0)
            {
                tmpParseError=oXMLDom.parseError;
                mobjCommon.InvalidXML(ref tmpParseError);
            }

             oXmlNodeList = oXMLDom.selectNodes("//ObjectGroup[@ObjectGroupName= 'company_accreditations']");

            foreach(IXMLDOMNode oXMLNode in oXmlNodeList)
            {
                if(oXMLNode!=null)
                    sCompany_accreditationsXMLLookup = string.Concat(sCompany_accreditationsXMLLookup, "<company_accreditations_XREFLookup>",oXMLNode.xml,"</company_accreditations_XREFLookup>");
               
            }
            strCompanyLicenesesAndAccredXML=string.Concat("<Company><strCompany_accreditations>", sCompany_accreditationsXMLLookup ,"</strCompany_accreditations>","<strCompanyLicenseXML>" , strLicenses , "<strServiceCenterStateCodes>" , sCompany_licensesXMLLookup , "</strServiceCenterStateCodes></strCompanyLicenseXML><TestXML><strServiceCenterStateCodesTest>" , strServiceCenterStateCodesTest ,"</strServiceCenterStateCodesTest><strLookUpLicensesParentIDs>", strLookUpLicensesParentIDs , "</strLookUpLicensesParentIDs><strLookUpLicenses>" , strLookUpLicenses , "</strLookUpLicenses><strActiveLicenseStateCodes>" , strActiveLicenseStateCodes , "</strActiveLicenseStateCodes>",
                                            "<strFilteredStateCodeParentIDs>",strFilteredStateCodeParentIDs, "</strFilteredStateCodeParentIDs><strFilteredStateCodes>", strFilteredStateCodes, "</strFilteredStateCodes><strFilteredParentIDsWithStateCodes>", strFilteredParentIDsWithStateCodes, "</strFilteredParentIDsWithStateCodes><strFilteredLicenses>", strFilteredLicenses, "</strFilteredLicenses></TestXML></Company>");


        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            oXMLDom = null;
            oXMLDomChild = null;
            oXmlNodeList = null;
            oXmlNodeListChild =null;
        }
        return strCompanyLicenesesAndAccredXML;
    }
        public string GetCompanyProfileItems(long lInstallerUidNo, [Optional]string sStylesheet = "", [Optional]string sUserID = "")
        {
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sInsertSQL = string.Empty,
                    sSelectSql = string.Empty,
                    sOutput = string.Empty,
                    sFilter = string.Empty,
                    sUserIdNo = string.Empty;
            Recordset oOutputRs = null;
            DOMDocument40 oXMLDom = null;
            try
            {
                objmodAGC2Common = new modAGC2Common();
                sTempTable = "so_profile_item";
                sStoredProc = "dp_get_so_profile_item";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (profile_object,profile_uid_no, user_id_no, user_session_no)",
                             " values ('inst',", lInstallerUidNo, ", '", sUserID, "', '", sSessionID, "')");
                sSelectSql = "*";
                sFilter = "attrib_type_cd=7 order by attrib_desc ";

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "item", "profile");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string ManageCompanyData(long lInstallerUidNo, string sActionCode, [Optional]string sXML, [Optional]string sStylesheet)
    {
        string strPullOutAction = string.Empty,
                strCompanyData = string.Empty,
                strGetXMLFromSubFunc = string.Empty,
                sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sSelectSql = string.Empty,
                sInsertSQL = string.Empty,
                sFieldsSQL = string.Empty,
                sValuesSQL = string.Empty,
                sField = string.Empty,
                sValue = string.Empty,
                sValues = string.Empty,
                sOutput = string.Empty,
                sFilter = string.Empty,
                strParam=string.Empty,
                sDropDownXML = string.Empty,
                sUserID = string.Empty;
        double numeric;
        long lInstallerUID;
        int iUserType;
        string[] varActionArray;
        DOMDocument40 oXMLDom = null;
        IXMLDOMNodeList oXmlNodeList = null;
        Recordset oOutputRs = null;
        IXMLDOMNode ixmlDOMNode=null;
        IXMLDOMParseError tmpDOMParseError=null;
        try
        {
            mobjCommon=new lynxAccount4.modCommon();
            mobjDataAccess=new lynxAccount4.modDataAccess();
            objmodAGC2Common = new modAGC2Common();
            
            oXMLDom = new DOMDocument40();
            if (Left(sActionCode, 16) == "GetTechLookupXML")
                strCompanyData = GetCompanyLicenesesAndAccredXML(lInstallerUidNo, sXML);

            if (Left(sActionCode, 15) == "INSERTEverybody")
            {
                varActionArray = sActionCode.Split('^');
                strPullOutAction = varActionArray[1];
                strGetXMLFromSubFunc = ManageCompanyAccreditation(sXML, strPullOutAction, lInstallerUidNo);
                strGetXMLFromSubFunc = string.Concat(strGetXMLFromSubFunc,ManageCompanyLicenses(sXML, strPullOutAction, lInstallerUidNo));
                strCompanyData = strGetXMLFromSubFunc;
                return strCompanyData;
            }

            if (Left(sActionCode, 13) == "Accreditation")
            {
                varActionArray = sActionCode.Split('^');
                strPullOutAction = varActionArray[1];
                strGetXMLFromSubFunc = ManageCompanyAccreditation(sXML, strPullOutAction, lInstallerUidNo);
                strCompanyData = strGetXMLFromSubFunc;
                return strCompanyData;
            }

            if (Left(sActionCode, 8) == "Licenses")
            {
                varActionArray = sActionCode.Split('^');
                strPullOutAction = varActionArray[1];
                strGetXMLFromSubFunc = ManageCompanyLicenses(sXML, strPullOutAction, lInstallerUidNo);
                strCompanyData = strGetXMLFromSubFunc;
                return strCompanyData;
            }
            if (sXML != string.Empty)
            {
                oXMLDom.loadXML(sXML);
                strParam = "UserType";
                iUserType = Convert.ToInt32(objmodAGC2Common.getNodeValue(ref oXMLDom, ref strParam));
                strParam = "UserID";
                sUserID = objmodAGC2Common.getNodeValue(ref oXMLDom, ref strParam);
            }

            else
                iUserType = 0;

            sTempTable = "so_company";
            sStoredProc = "dp_manage_so_company";

            if (sActionCode.ToUpper() == "GET")
            {
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no, action_cd,modify_user_type_no, user_id_no)",
                             " values (", lInstallerUidNo, ",'", sActionCode, "',", iUserType, ", '", sUserID, "')");
                sSelectSql = "*";
                sFilter = "record_type=1";
                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                oXMLDom.load(string.Concat(sXMLPath, "lookup.xml"));

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpDOMParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpDOMParseError);
                }

                ixmlDOMNode = oXMLDom.selectSingleNode("//ObjectGroup[@ObjectGroupName='franchise_cd']");
                sDropDownXML = string.Concat("<DropDownInfo>", ixmlDOMNode.xml);
                ixmlDOMNode = oXMLDom.selectSingleNode("//ObjectGroup[@ObjectGroupName='pos_cd']");
                sDropDownXML = string.Concat(sDropDownXML, ixmlDOMNode.xml);
                ixmlDOMNode = oXMLDom.selectSingleNode("//ObjectGroup[@ObjectGroupName='incorp_stat_cd']");
                sDropDownXML = string.Concat(sDropDownXML, ixmlDOMNode.xml, "</DropDownInfo>");
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);
                sOutput = string.Concat("<data>", sOutput, sDropDownXML, "</data>");
            }
            else
            {
                lInstallerUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);
                oXmlNodeList = oXMLDom.selectNodes("//data/row");
                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    sField = string.Concat(sField,", ", oXMLNode.attributes.getNamedItem("name").text);
                    if (double.TryParse(oXMLNode.attributes.getNamedItem("value").text, out numeric))
                    {
                        if (oXMLNode.attributes.getNamedItem("name").text.Contains("zip_cd") || oXMLNode.attributes.getNamedItem("name").text.Contains("addr_1st") || oXMLNode.attributes.getNamedItem("name").text.Contains("addr_2nd")
                            || oXMLNode.attributes.getNamedItem("name").text.Contains("insurer_policy_no") || oXMLNode.attributes.getNamedItem("name").text.Contains("dba_nm"))
                            sValue = string.Concat(sValue, ", '", oXMLNode.attributes.getNamedItem("value").text, "'");
                        else
                            sValue = string.Concat(sValue, ", ", oXMLNode.attributes.getNamedItem("value").text, "");
                    }
                    else
                        sValue = string.Concat(sValue,", '",oXMLNode.attributes.getNamedItem("value").text, "'");
                }
                sFieldsSQL = string.Concat("insert into session.",sTempTable, " (action_cd, inst_uid_no, user_id_no, user_session_no, commit_ind,record_type",
                                   sField,") VALUES");
                sValuesSQL = string.Concat("('", sActionCode,"',",lInstallerUID, ",'", sUserID, "','",sUserID, "','Y',1",
                                     sValue, ")~");
                sInsertSQL = string.Concat(sFieldsSQL,sValuesSQL);
                sSelectSql = "*";
                sFilter = string.Empty;
                oOutputRs =mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "response");
            }
            if(sStylesheet!=string.Empty)
                 sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath,sStylesheet));

        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oXMLDom=null;
            oXmlNodeList=null;
            oOutputRs.Close();
            oOutputRs=null;
        }
        return sOutput;
    }
        private string ManageCompanyAccreditation(string sXML, string sActionCode, long lInstallerUidNo)
    {
        string sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sSelectSql = string.Empty,
                sInsertSQL = string.Empty,
                sFieldsSQL = string.Empty,
                sValuesSQL = string.Empty,
                sField = string.Empty,
                sValue = string.Empty,
                sValues = string.Empty,
                sOutput = string.Empty,
                sTechOutput = string.Empty,
                sCertOutput = string.Empty,
                sFilter = string.Empty,
                sDropDownXML = string.Empty,
                sUserID = string.Empty,
                strTech_cert_nm = string.Empty,
                sCertificationNm = string.Empty,
                sCertificationNo=string.Empty,
                sCertificationDate=string.Empty,
                sCertExpDate=string.Empty,
                sUserSessionID=string.Empty,
                sCertCode=string.Empty;
        string[] vCertification;
        DOMDocument40 oXMLDom = null;
        IXMLDOMNodeList oXmlNodeList = null;
        IXMLDOMParseError tmpDOMParseError=null;
        Recordset oOutputRs = null;
        Command oCmd = null;
        Connection oConnection = null;
        bool bLogError;
        long lgAccreditation_uid_no;
        try
        {
            mobjCommon=new lynxAccount4.modCommon();
            mobjDataAccess=new lynxAccount4.modDataAccess();
            oXMLDom = new DOMDocument40();
            oXMLDom.async = false;
            oXMLDom.loadXML(sXML);

            if (oXMLDom.parseError.errorCode != 0)
            {
                tmpDOMParseError=oXMLDom.parseError;
                mobjCommon.InvalidXML(ref tmpDOMParseError);
            }

            sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//UserID").text);
            sUserSessionID = sUserID;

            switch (sActionCode.ToUpper())
            {
                case "GET":
                    sTempTable = "so_company_accreditation";
                    sStoredProc = "dp_manage_so_company_accred";
                    sFieldsSQL =string.Concat("insert into session.",sTempTable, " (action_cd, inst_uid_no, commit_ind, msg_ind, ",
                         "msg_text, user_id_no, user_session_no) VALUES");
                    sValuesSQL = string.Concat("('GET', ", lInstallerUidNo, ", 'Y', 'N', '', '",sUserID , "','" ,sUserID, "')~");
                    sInsertSQL = string.Concat(sInsertSQL,sFieldsSQL, sValuesSQL);
                    sSelectSql = "*";
                    sFilter = string.Empty;
                    break;
                case "DELETE":
                    sTempTable = "so_company_accreditation";
                    sStoredProc = "dp_manage_so_company_accred";
                    oXMLDom.loadXML(sXML);
                    oXmlNodeList = oXMLDom.selectNodes("//CompanyAccreditation/data/CompanyData/Accreditation");
                    foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                    {
                        sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, accreditation_uid_no, inst_uid_no, commit_ind, msg_ind, ",
                                                   "msg_text, user_id_no, user_session_no) VALUES");
                        lgAccreditation_uid_no =Convert.ToInt64(oXMLNode.selectSingleNode("Tech_cert_id").text);
                        sInsertSQL = string.Concat(sInsertSQL,sFieldsSQL,sValuesSQL);
                    }
                    sSelectSql = "*";
                    sFilter = string.Empty;
                    oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                    break;
                case"UPDATE":
                    sTempTable = "so_company_accreditation";
                    sStoredProc = "dp_manage_so_company_accred";
                    oXMLDom.loadXML(sXML);
                    oXmlNodeList = oXMLDom.selectNodes("//CompanyAccreditation/data/CompanyData/Accreditation");
                    foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                    {
                        if (oXMLNode.text != string.Empty)
                        {
                            sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, accreditation_dt,  accreditation_no, accreditation_uid_no, exp_dt, inst_uid_no, commit_ind, msg_ind, ",
                                                "msg_text, user_id_no, user_session_no) VALUES");
                            strTech_cert_nm = oXMLNode.selectSingleNode("Tech_cert_nm").text;
                            vCertification = strTech_cert_nm.Split('^');
                            lgAccreditation_uid_no = Convert.ToInt64(vCertification[0]);
                            sCertificationNo = oXMLNode.selectSingleNode("Tech_cert_id").text;
                            sCertificationDate = oXMLNode.selectSingleNode("Tech_cert_dt").text;
                            sCertExpDate = oXMLNode.selectSingleNode("Tech_exp_dt").text;
                            sValuesSQL = string.Concat("('UPDATE', '", sCertificationDate, "', '", sCertificationNo, "', ", lgAccreditation_uid_no, ", '", sCertExpDate, "', ", lInstallerUidNo, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                            sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        }
                        else
                        {
                            oXMLDom = null;
                            oXmlNodeList = null;
                            if (oOutputRs != null)
                                oOutputRs = null;

                            oOutputRs.Close();
                        }

                    }
                    sSelectSql = "*";
                    sFilter = string.Empty;
                    oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                    break;
                case "INSERT":
                    sInsertSQL = string.Empty;
                    sTempTable = "so_company_accreditation";
                    sStoredProc = "dp_manage_so_company_accred";
                    oXMLDom.loadXML(sXML);
                    oXmlNodeList = oXMLDom.selectNodes("//CompanyAccreditation/data/CompanyData/Accreditation");

                    foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                    {
                        if (oXMLNode.text != string.Empty)
                        {
                            sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, accreditation_dt,  accreditation_no, accreditation_nm, accreditation_uid_no, exp_dt, inst_uid_no, commit_ind, msg_ind, ",
                                           "msg_text, user_id_no, user_session_no) VALUES");
                            strTech_cert_nm = oXMLNode.selectSingleNode("Tech_cert_nm").text;
                            vCertification = strTech_cert_nm.Split('^');
                            lgAccreditation_uid_no = Convert.ToInt64(vCertification[0]);
                            sCertificationNm = vCertification[1];
                            sCertificationNo = oXMLNode.selectSingleNode("Tech_cert_id").text;
                            sCertificationDate = oXMLNode.selectSingleNode("Tech_cert_dt").text;
                            sCertExpDate = oXMLNode.selectSingleNode("Tech_exp_dt").text;
                            sValuesSQL = string.Concat("('INSERT', '", sCertificationDate, "', '", sCertificationNo, "', '", sCertificationNm, "', ", lgAccreditation_uid_no, ", '", sCertExpDate, "', ", lInstallerUidNo, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                            sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        }
                        else
                        {
                            oXMLDom = null;
                            oXmlNodeList = null;
                            if (oOutputRs != null)
                                oOutputRs = null;

                        }
                    }
                    sSelectSql = "*";
                    sFilter = string.Empty;
                    oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                    break;
            }
            sCertOutput = mobjDataAccess.RsToXML(oOutputRs,true, "Accreditation");
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            oXMLDom=null;
            oXmlNodeList=null;
            if(oOutputRs!=null)
                oOutputRs=null;
        }
        return sCertOutput;
    }
        private string ManageCompanyLicenses(string sXML, string sActionCode, long lInstallerUidNo)
    {
        string sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sSelectSql = string.Empty,
                sInsertSQL = string.Empty,
                sFieldsSQL = string.Empty,
                sValuesSQL = string.Empty,
                sField = string.Empty,
                sValue = string.Empty,
                sValues = string.Empty,
                sOutput = string.Empty,
                sTechOutput = string.Empty,
                sCertOutput = string.Empty,
                sFilter = string.Empty,
                sUserID = string.Empty,
                sUserSessionID = string.Empty,
                sInstUidNo = string.Empty,
                sDropDownXML = string.Empty,
                strTech_StateInfo = string.Empty,
                strTech_licenses_nm = string.Empty,
                sLicenseStateCD = string.Empty,
                sLicenseNm = string.Empty,
                sLicenseNo = string.Empty,
                sLicenseExpDate = string.Empty;
        bool bLogError;
        long lgLicenseStateID, lgCompanyLicenseID;
        string[] vStateCode, vLicense;
        DOMDocument40 oXMLDom = null;
        IXMLDOMNodeList oXmlNodeList = null;
        Recordset oOutputRs=null;
        IXMLDOMParseError tmpDOMParseError=null;
        try
        {
            mobjCommon=new lynxAccount4.modCommon();
            mobjDataAccess=new lynxAccount4.modDataAccess();
            oXMLDom = new DOMDocument40();
            oOutputRs = new Recordset();
            oXMLDom.async = false;
            oXMLDom.loadXML(sXML);

            if (oXMLDom.parseError.errorCode != 0)
            {
                tmpDOMParseError=oXMLDom.parseError;
                mobjCommon.InvalidXML(ref tmpDOMParseError);
            }

            sUserSessionID = sUserID;

            switch(sActionCode.ToUpper())
            {
                case "GET":
                    sTempTable = "so_company_license";
                    sStoredProc = "Dp_manage_so_company_license";
                    sFieldsSQL = string.Concat("insert into session.",sTempTable, " (action_cd, inst_uid_no, commit_ind, msg_ind, ",
                                 "msg_text, user_id_no, user_session_no) VALUES");
                    sValuesSQL = string.Concat("('GET', ", lInstallerUidNo, ", 'Y', 'N', '', '" , sUserID, "','" ,sUserID, "')~");
                    sInsertSQL = string.Concat(sInsertSQL,sFieldsSQL, sValuesSQL);
                    sSelectSql = "*";
                    sFilter = string.Empty;
                    oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                    break;
                case "DELETE":
                    sTempTable = "so_company_license";
                    sStoredProc = "Dp_manage_so_company_license";
                    oXMLDom.loadXML(sXML);
                    oXmlNodeList = oXMLDom.selectNodes("//CompanyAccreditation/data/CompanyData/license");
                    foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                    {
                        sFieldsSQL = string.Concat("insert into session.",sTempTable, " (action_cd, company_license_id, inst_uid_no, license_state_id, commit_ind, msg_ind, ",
                         "msg_text, user_id_no, user_session_no) VALUES");
                        lgLicenseStateID = Convert.ToInt64(oXMLNode.selectSingleNode("license_state_id").text);
                        lgCompanyLicenseID = Convert.ToInt64(oXMLNode.selectSingleNode("company_license_id").text);
                        sValuesSQL = string.Concat("('DELETE', ", lgCompanyLicenseID, ", ", lInstallerUidNo, ", " , lgLicenseStateID , ", 'Y', 'N', '', '", sUserID , "', '", sUserID, "')~");
                        sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                            
                    }
                    sSelectSql = "*";
                    sFilter = string.Empty;
                    oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                    break;
                case "UPDATE":
                    sTempTable = "so_company_license";
                    sStoredProc = "Dp_manage_so_company_license";
                    oXMLDom.loadXML(sXML);
                    oXmlNodeList = oXMLDom.selectNodes("//CompanyAccreditation/data/CompanyData/license");
                    foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                    {
                        if (oXMLNode.text != string.Empty)
                        {
                            sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, exp_dt, company_license_id, company_license_no, company_license_nm, inst_uid_no, license_state_id, license_state_cd, commit_ind, msg_ind, ",
                                                       "msg_text, user_id_no, user_session_no) VALUES");
                            strTech_licenses_nm = oXMLNode.selectSingleNode("Tech_licenses_nm").text;
                            lgCompanyLicenseID = Convert.ToInt64(oXMLNode.selectSingleNode("License_state_id").text);
                            sLicenseStateCD = oXMLNode.selectSingleNode("Tech_state_cd").text;
                            lgLicenseStateID = Convert.ToInt64(oXMLNode.selectSingleNode("Tech_license_state_id").text);
                            sLicenseNo = oXMLNode.selectSingleNode("Tech_license_num").text;
                            sLicenseExpDate = oXMLNode.selectSingleNode("Tech_license_exp_dt").text;
                            sValuesSQL = string.Concat("('UPDATE', '", sLicenseExpDate, "', ", lgCompanyLicenseID, ", '", sLicenseNo, "', '", sLicenseNm, "', ", lInstallerUidNo, ", ", lgLicenseStateID, ", '", sLicenseStateCD, "', 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                            sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        }
                        else
                        {
                            oXMLDom = null;
                            oXmlNodeList = null;
                            if (oOutputRs != null)
                                oOutputRs.Close();
                            oOutputRs = null;
                        }
                    }
                    sSelectSql = "*";
                    sFilter = string.Empty;
                    oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                    break;
                case "INSERT":
                    sInsertSQL = string.Empty;
                    sTempTable = "so_company_license";
                    sStoredProc = "Dp_manage_so_company_license";
                    oXMLDom.loadXML(sXML);
                    oXmlNodeList = oXMLDom.selectNodes("//CompanyAccreditation/data/CompanyData/license");
                    foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                    {
                        if (oXMLNode.text != string.Empty)
                        {
                            sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, exp_dt, company_license_id, company_license_no, company_license_nm, inst_uid_no, license_state_id, license_state_cd, commit_ind, msg_ind, ",
                                         "msg_text, user_id_no, user_session_no) VALUES");
                            strTech_StateInfo = oXMLNode.selectSingleNode("Tech_state_cd").text;
                            vStateCode = strTech_StateInfo.Split('^');
                            lgLicenseStateID = Convert.ToInt64(vStateCode[0]);
                            sLicenseStateCD = vStateCode[1].Trim();
                            strTech_licenses_nm = oXMLNode.selectSingleNode("Tech_licenses_nm").text;
                            vLicense = strTech_licenses_nm.Split('^');
                            lgCompanyLicenseID = Convert.ToInt64(vLicense[0]);
                            sLicenseNm = vLicense[1];
                            sLicenseNo = oXMLNode.selectSingleNode("Tech_license_num").text;
                            sLicenseExpDate = oXMLNode.selectSingleNode("Tech_license_exp_dt").text;
                            sValuesSQL = string.Concat("('INSERT', '", sLicenseExpDate, "', ", lgCompanyLicenseID, ", '", sLicenseNo, "', '", sLicenseNm, "', ", lInstallerUidNo, ", ", lgLicenseStateID, ", '", sLicenseStateCD, "', 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                            sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        }
                        else
                        {
                            oXMLDom = null;
                            oXmlNodeList = null;
                            if (oOutputRs != null)
                                oOutputRs.Close();
                            oOutputRs = null;
                        }

                    }
                    sSelectSql = "*";
                    sFilter = string.Empty;
                    oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                    break;
            }
            sCertOutput = mobjDataAccess.RsToXML(oOutputRs, true,"license");
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            oXMLDom=null;
            oXmlNodeList=null;
        }
        return sCertOutput;
    }
        public string ManageCompanyRemitTo(string sXML, [Optional]string sStylesheet = "")
    {
        Command oCmd = null;
        Connection oConnection = null;
        DOMDocument40 oXMLDom = null;
        IXMLDOMElement oRoot = null;
        IXMLDOMNode oNode = null;
        IXMLDOMParseError tmpParseError=null;
        string sStoredProc = string.Empty,
               sOutput = string.Empty,
               sActionCode = string.Empty,
               sUserID = string.Empty,
               sRemitToName = string.Empty,
               sAddress1 = string.Empty,
               sAddress2 = string.Empty,
               sCity = string.Empty,
               sState = string.Empty,
               sZip = string.Empty;
        object objExecute;
        int sInd;
        long lInstallerID;
        long lRemitTo=0;
        bool bLogError;
        try
        {
            oXMLDom = new DOMDocument40();
            oConnection = new Connection();
            oCmd = new Command();
            mobjDataAccess=new lynxAccount4.modDataAccess();
            mobjCommon=new lynxAccount4.modCommon();

            oXMLDom.async = false;
            oXMLDom.loadXML(sXML);

            if (oXMLDom.parseError.errorCode != 0)
            {
                tmpParseError=oXMLDom.parseError;
                mobjCommon.InvalidXML(ref tmpParseError);
            }

            sActionCode = oXMLDom.selectSingleNode("//ActionCode").text;
            lInstallerID = Convert.ToInt64(oXMLDom.selectSingleNode("//InstallerUID").text);
            sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//UserID").text);

            if (sActionCode.ToUpper() == "UPDATE" || sActionCode.ToUpper() == "DELETE")
                lRemitTo = Convert.ToInt64(oXMLDom.selectSingleNode("//remit_bt_addr_uid_no").text);

            sRemitToName = oXMLDom.selectSingleNode("//remit_bill_to_nm").text;
            sAddress1 = oXMLDom.selectSingleNode("//remit_addr_1st").text;
            sAddress2 = oXMLDom.selectSingleNode("//remit_addr_2nd").text;
            sCity = oXMLDom.selectSingleNode("//remit_city_nm").text;
            sState = oXMLDom.selectSingleNode("//remit_state_cd").text;
            sZip = oXMLDom.selectSingleNode("//remit_zip_cd").text;
            sStoredProc = "dp_manage_inbt";

            oConnection.ConnectionTimeout = 60;
            oConnection.Open(lynxAccount4.modCommon.sIngresDSN);

            oCmd.ActiveConnection = oConnection;
            oCmd.CommandType = CommandTypeEnum.adCmdText;
            oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc,"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
            oCmd.CommandTimeout = 90;
            oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sActionCode));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_addr_1st", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, sAddress1));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_addr_2nd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, sAddress2));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_city_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 20, sCity));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_state_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 2, sState));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_zip_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 9, sZip));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_bill_to_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, sRemitToName));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_bt_addr_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lRemitTo));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_infor_bus_partner_cd", DataTypeEnum.adChar,ParameterDirectionEnum.adParamInput, 10, ""));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_infor_pay_to_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, ""));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_parent_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lInstallerID));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 7, sUserID));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));
            oCmd.Execute(out objExecute);

            if (Convert.ToInt64(oCmd.Parameters["retval"]) != 0)
            {
                bLogError = true;
                if (Convert.ToInt64(oCmd.Parameters["retval"]) != 0)
                    sInd = 4;
                else
                    sInd = 1;
            }
            sOutput = mobjDataAccess.ParametersToXML(ref oCmd,true, "response");

            if(sStylesheet!=string.Empty)
                sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath,sStylesheet));
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            oCmd=null;
        }
        return sOutput;
    }

        public string ManageCompanyProfileItems(string sXML, [Optional]string sStylesheet = "")
        {
            string sOutput = string.Empty,
                   sUserID = string.Empty;
            long lInstallerUID;
            object objExecute;
            Connection oConnection = null;
            Command oCommand = null;
            DOMDocument40 oXMLDom = null;

            IXMLDOMNodeList oXmlNodeList = null;
            try
            {
                oConnection = new Connection();
                oCommand = new Command();
                oXMLDom = new DOMDocument40();
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);
                oXMLDom.loadXML(sXML);
                lInstallerUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = "{? = call lynxdba.dp_update_profile_item (?,?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;
                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_uid_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_attrib_value", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_attrib_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_profile_object", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, "inst"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_profile_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, lInstallerUID));
                oXmlNodeList = oXMLDom.selectNodes("//data/row");

                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    oCommand.Parameters["h_attrib_nm"].Value = oXMLNode.attributes.getNamedItem("name").text;
                    oCommand.Parameters["h_attrib_value"].Value = oXMLNode.attributes.getNamedItem("value").text;
                    oCommand.Execute(out objExecute);
                }
                oCommand.ActiveConnection.CommitTrans();
                sOutput = mobjDataAccess.ParametersToXML(ref oCommand);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }

        public string ApplyChanges(string sMainXml, string sChangesXml, [Optional]string sStylesheet)
        {
            string sError = string.Empty,
                  sFilter = string.Empty,
                  sSortBy = string.Empty,
                  strOutout = string.Empty,
                  sSortOrder = string.Empty;
            int iPageNumber, iStartPageRecord, iEndPageRecord, iNumberOfPages=0, iCounter;
            bool bFiltersChanged;
            DOMDocument40 oXMLDom = null;
            DOMDocument40 oNewXmlDom = null;
            IXMLDOMElement oRoot = null;
            IXMLDOMNode oNode = null;
            IXMLDOMElement oChildNode = null;
            IXMLDOMElement oNodeElement = null;
            IXMLDOMElement oPaging = null;
            IXMLDOMNodeList oNodeList = null;
            IXMLDOMNodeList oFiltersNodeList = null;
            IXMLDOMElement oCurrentFilter = null;
            IXMLDOMElement oFilters = null;
            IXMLDOMNodeList oStatesNodeList = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                oXMLDom = new DOMDocument40();
                oNewXmlDom = new DOMDocument40();
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();

                oXMLDom.async = false;
                oXMLDom.load(sMainXml);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                oNewXmlDom.async = false;
                oNewXmlDom.loadXML(sChangesXml);

                if (oNewXmlDom.parseError.errorCode != 0)
                {
                    tmpParseError = oNewXmlDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                oFiltersNodeList = oNewXmlDom.selectNodes("//Filter");

                if (oFiltersNodeList != null && oFiltersNodeList.length > 0)
                {
                    for (iCounter = 1; iCounter <= oFiltersNodeList.length; iCounter++)
                    {
                        oCurrentFilter = (IXMLDOMElement)oFiltersNodeList[iCounter - 1];
                        if (oCurrentFilter.getAttribute("name") == "inst_nm" || oCurrentFilter.getAttribute("name") == "city_nm")
                            sFilter = string.Concat(sFilter, "[contains(@", oCurrentFilter.getAttribute("name"), ", \"", oCurrentFilter.getAttribute("value"), "\")]");
                        else
                            sFilter = string.Concat(sFilter, "[@", oCurrentFilter.getAttribute("name"), "=\"", oCurrentFilter.getAttribute("value"), "\"]");
                    }
                }
                oStatesNodeList = oXMLDom.selectNodes("//record[not(@state_cd=preceding-sibling::*/attribute::state_cd)]");

                sSortBy = oNewXmlDom.selectSingleNode("//SortBy").text;
                sSortOrder = oNewXmlDom.selectSingleNode("//SortOrder").text;

                oPaging = (IXMLDOMElement)oNewXmlDom.selectSingleNode("//Paging");
                iPageNumber = Convert.ToInt32(oPaging.getAttribute("PageNo"));

                iStartPageRecord = iPageNumber * 10 - 9;
                iEndPageRecord = iStartPageRecord + 9;

                oFilters = (IXMLDOMElement)oNewXmlDom.selectSingleNode("//Filters");
                bFiltersChanged = Convert.ToBoolean(oFilters.getAttribute("changed"));

                if (bFiltersChanged)
                {
                    iPageNumber = 1;
                    iStartPageRecord = 1;
                    iEndPageRecord = 10;
                }
                oNodeList = oXMLDom.selectNodes(string.Concat("//record", sFilter));

                if (oNodeList != null)
                {
                    if (oNodeList.length == 1)
                        iNumberOfPages = 1;
                    else
                        iNumberOfPages = Convert.ToInt32(Math.Round((oNodeList.length / 10) + 0.49));
                }
                oRoot = (IXMLDOMElement)oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "Records", "");
                oNewXmlDom.appendChild(oRoot);
                oNodeList = oXMLDom.selectNodes(string.Concat("//record", sFilter));

                if (oNodeList != null)
                {
                    for (iCounter = 0;iCounter<= oNodeList.length - 1; iCounter++)
                    {
                        oNode = oNodeList[iCounter];
                        oRoot.appendChild(oNode);
                        oNode = null;
                    }
                }
                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "PageNo", "");
                oNode.text = Convert.ToString(iPageNumber);
                oRoot.appendChild(oNode);
                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "BeginRecord", "");
                oNode.text = Convert.ToString(iStartPageRecord);
                oRoot.appendChild(oNode);
                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "EndRecord", "");
                oNode.text = Convert.ToString(iEndPageRecord);
                oRoot.appendChild(oNode);
                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "TotalPages", "");
                oNode.text = Convert.ToString(iNumberOfPages);
                oRoot.appendChild(oNode);
                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "BeginRecord", "");
                oNode.text = Convert.ToString(iStartPageRecord);
                oRoot.appendChild(oNode);
                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "EndRecord", "");
                oNode.text = Convert.ToString(iEndPageRecord);
                oRoot.appendChild(oNode);
                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "TotalPages", "");
                oNode.text = Convert.ToString(iNumberOfPages);
                oRoot.appendChild(oNode);
                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "SortBy", "");
                oNode.text = Convert.ToString(sSortBy);
                oRoot.appendChild(oNode);
                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "SortOrder", "");
                oNode.text = Convert.ToString(sSortOrder);
                oRoot.appendChild(oNode);
                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "AppliedFilters", "");
                oRoot.appendChild(oNode);
                if (oFiltersNodeList != null)
                {
                    for (iCounter = 0; iCounter <= oFiltersNodeList.length - 1; iCounter++)
                    {
                        oNode.appendChild(oFiltersNodeList[iCounter]);//need clarification
                    }
                }

                oNode = null;

                oNode = oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "StateList", "");
                oRoot.appendChild(oNode);

                if (oStatesNodeList != null && oStatesNodeList.length > 0)
                {
                    for (iCounter = 0; iCounter <= oStatesNodeList.length - 1; iCounter++)
                    {
                        oChildNode = (IXMLDOMElement)oNewXmlDom.createNode(DOMNodeType.NODE_ELEMENT, "State", "");
                        oNodeElement = (IXMLDOMElement)oStatesNodeList[iCounter];//need clarification
                        oChildNode.text = Convert.ToString(oNodeElement.getAttribute("state_cd"));
                        oNodeElement = null;
                        oNode.appendChild(oChildNode);
                        oChildNode = null;
                    }
                }
                strOutout = oNewXmlDom.xml;

                if (sStylesheet != string.Empty)
                    strOutout = mobjDataAccess.TransformXML(strOutout, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));//need clarification in lynAgc2xwrapperclsparent
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                oNode = null;
                oStatesNodeList = null;
                oFiltersNodeList = null;
                oRoot = null;
                oNewXmlDom = null;
                oXMLDom = null;
            }
            return strOutout;
        }
        public string GetCompanyComments(string sXML, [Optional]string sStylesheet = "")
    {
        string sActionCode = string.Empty,
                sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sInsertSQL = string.Empty,
                sSelectSql = string.Empty,
                sOutput = string.Empty,
                sFilter = string.Empty,
                sDropDownInfo = string.Empty,
                strParam=string.Empty,
                sUserID = string.Empty;
        long lFederalTaxID;
        int iGlassPgmID;
        Recordset oOutputRs = null;
        DOMDocument40 oXMLDom = null;
        IXMLDOMParseError tmpParseError=null;
        try
        {
            objmodCanadaCommon=new modCanadaCommon();
            mobjCommon=new lynxAccount4.modCommon();
            mobjDataAccess=new lynxAccount4.modDataAccess();
            oXMLDom = new DOMDocument40();
            oXMLDom.async = false;
            oXMLDom.loadXML(sXML);

            if (oXMLDom.parseError.errorCode != 0)
            {
                tmpParseError=oXMLDom.parseError;
                mobjCommon.InvalidXML(ref tmpParseError);
            }

            strParam="FederalTaxID";
            lFederalTaxID = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref oXMLDom, ref strParam));
            strParam="GlassProgramID";
            iGlassPgmID = Convert.ToInt32(objmodCanadaCommon.getNodeValue(ref oXMLDom, ref strParam));
            strParam="UserID";
            sUserID = objmodCanadaCommon.getNodeValue(ref oXMLDom, ref strParam);
            strParam="Action";
            sActionCode = objmodCanadaCommon.getNodeValue(ref oXMLDom, ref strParam);

            if (sActionCode == string.Empty)
                sActionCode = "GET";

            sTempTable = "so_tin_comments";
            sStoredProc = "dp_manage_so_tin_comments";
            sInsertSQL = string.Concat("insert into session.", sTempTable, " (federal_tax_id, action_cd, glass_admin_no, user_id_no)",
                          " values (", lFederalTaxID, ",'", sActionCode, "',", iGlassPgmID, ", '", sUserID, "')");

            sSelectSql = "*";
            sFilter = "1=1 order by create_dt desc";
            oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
            sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "comments").Replace( "&#xA;", "");
            sOutput = string.Concat("<CompanyComments>",sOutput,sDropDownInfo,"</CompanyComments>");

            if(sStylesheet!=string.Empty)
                sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath,sStylesheet));
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            oOutputRs.Close();
            oOutputRs=null;
        }
        return sOutput;
    }

        public string ManageCompanyComments(string sXML, [Optional]string sStylesheet = "")
    {
        string cACTION_CODE = "INSERT";
        string sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sSelectSql = string.Empty,
                sInsertSQL = string.Empty,
                sFieldsSQL = string.Empty,
                sValuesSQL = string.Empty,
                sOutput = string.Empty,
                sFilter = string.Empty,
                sUserID = string.Empty,
                sCommentText = string.Empty;
        long lFedTaxID;
        int iCommentNo, iGlassPgmID;
        Recordset oOutputRs = null;
        DOMDocument40 oXMLDom = null;
        IXMLDOMNode oXMLNode = null;
        IXMLDOMNodeList oXmlNodeList = null;
        try
        {
            oXMLDom = new DOMDocument40();
            oOutputRs = new Recordset();

            sTempTable = "so_tin_comments";
            sStoredProc = "dp_manage_so_tin_comments";
            oXMLDom.loadXML(sXML);
            lFedTaxID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/FederalTaxID").text);
            sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);
            iGlassPgmID = Convert.ToInt32(oXMLDom.selectSingleNode("//GlassProgramID").text);
            sFieldsSQL = string.Concat("insert into session.",sTempTable," (action_cd, federal_tax_id,create_dt, user_id_no, user_session_no, ",
                              "comment_text, comment_type_no, glass_admin_no, commit_ind) VALUES");

            sCommentText = oXMLDom.selectSingleNode("//data/comment_text").text;
            iCommentNo = Convert.ToInt32(oXMLDom.selectSingleNode("//data/comment_type_no").text);
            sValuesSQL = string.Concat("('",cACTION_CODE,"',",lFedTaxID,",'" ,DateTime. Now,"','" ,sUserID , "','" , sUserID ,"','",
                              sCommentText,"'," ,iCommentNo, "," ,iGlassPgmID,",'Y')~");
            sInsertSQL = string.Concat(sInsertSQL,sFieldsSQL,sValuesSQL);
            sSelectSql = "*";
            sFilter = string.Empty;
            oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
            sOutput = mobjDataAccess.RsToXML(oOutputRs,true,"response");

            if(sStylesheet!=string.Empty)
                sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath,sStylesheet));
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            oXMLDom=null;
            oXMLNode=null;
            oXmlNodeList=null;
            oOutputRs.Close();
            oOutputRs=null;
        }
        return sOutput;
    }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom = new DOMDocument40();
                sOutput =mobjCommon.GetConfiguration("lynxAGC2");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);

                if (oXMLDom.parseError.errorCode == 0)
                {
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    lynxAccount4.modCommon.bActivateLog = Convert.ToBoolean(oXMLDom.selectSingleNode("//ActivateLog").text);
                    sXMLPath = oXMLDom.selectSingleNode("//Cache").text;
                }
                else
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        private void ObjectControl_Deactivate()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void LogXmlToAppLog(string sSource, string sMessage, string sEnvId, [Optional]int iType = 0, [Optional]long lAppId = 1)
        {
            try
            {



            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string SubmitApplication(string sXML, [Optional]string sStylesheet)
    {
        FreeThreadedDOMDocument40 oXMLIn = null;
        Command oCmd = null;
        Connection oConnection = null;
        string sProcName = string.Empty,
               sAction = string.Empty,
               sFirstName = string.Empty,
               sLastName = string.Empty,
               sUser = string.Empty,
               sFelonyTxt = string.Empty,
               sFelonyInd = string.Empty,
               sEmail = string.Empty,
               strParam=string.Empty,
               sOutput = string.Empty;
        object objexecute;
        int sInd;
        long lFedTaxID, lProgramNumber, lInstallerID, lAdminNumber;
        DOMDocument40 objDomDOMDocument40;
        IXMLDOMParseError tmpParseError=null;
        try
        {
            objmodCanadaCommon=new modCanadaCommon();
            oXMLIn = new FreeThreadedDOMDocument40();
            oCmd = new Command();
            oConnection = new Connection();
            mobjCommon=new lynxAccount4.modCommon();
            mobjDataAccess=new lynxAccount4.modDataAccess();
            oXMLIn.loadXML(sXML);
            if (oXMLIn.parseError.errorCode != 0)
            {
                tmpParseError=oXMLIn.parseError;
                mobjCommon.InvalidXML(ref tmpParseError);
            }
            strParam="";
            objDomDOMDocument40=(DOMDocument40)oXMLIn;
            sAction = objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40,ref strParam );
            strParam="FedTaxID";
            lFedTaxID = Convert.ToInt64((objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40,ref strParam )));
            strParam="ProgramNumber";
            lProgramNumber = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40,ref strParam ));
            strParam="InstallerID";
            lInstallerID = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40,ref strParam));
            strParam="AdminNo";
            lAdminNumber = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40,ref strParam));
            strParam="UserID";
            sUser = mobjCommon.PreFixUserId(objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40, ref strParam));
            strParam="FirstName";
            sFirstName = Left(objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40 ,ref strParam).Trim(), 10);//needclarification
            strParam="LastName";
            sLastName = Left(objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40, ref strParam).Trim(), 30);//need clarification
            strParam="FelonyInd";
            sFelonyInd = objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40, ref strParam);
            strParam="FelonyTxt";
            sFelonyTxt = objmodCanadaCommon.getNodeValue(ref objDomDOMDocument40, ref strParam);

            mobjCommon.LogToAppLog("clsInsuranceCo.ManageTINQueue", sXML, "");
            sProcName = "dp_manage_company_glass_program";

            oConnection.CommandTimeout = 60;
            oConnection.Open(lynxAccount4.modCommon.sIngresDSN);

            oCmd.ActiveConnection = oConnection;
            oCmd.CommandType = CommandTypeEnum.adCmdText;
            oCmd.CommandTimeout = 90;
            oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue,0, -1));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, sAction));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_federal_tax_id", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput,0, lFedTaxID));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_felony_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, sFelonyInd));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_felony_comment_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 1000, sFelonyTxt));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_glass_admin_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput,0,lAdminNumber));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_glass_admin_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput,0,lAdminNumber));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput,0,lInstallerID));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_lynx_pgm_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput,0,lProgramNumber));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_first_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sFirstName));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_last_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, sLastName));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, "Y"));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 250, ""));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sUser));
            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sUser));
            oCmd.CommandText =string.Concat("{? = call lynxdba.",sProcName,"(",mobjDataAccess.getQuestionMarks(oCmd.Parameters.Count),") }");
            oCmd.Execute(out objexecute);

            if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0)
            {
                sInd = 4;
                throw new Exception(string.Concat(oCmd.Parameters["retval"],Constants.vbObjectError,sProcName,oCmd.Parameters["h_msg_text"]));
            }
            else if(Convert.ToInt32(oCmd.Parameters["retval"])>0)
            {
                sInd = 1;
                sOutput= Convert.ToString(oCmd.Parameters["h_msg_text"]);
            }
            else
            {
                if(lProgramNumber==2)
                    sOutput = Convert.ToString(oCmd.Parameters["h_msg_text"]);
                else
                    sOutput= "SUCCESS";
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
        return sOutput;
    }

        public string sendRecertificationEmail(string sOutputXML, string sUserID, string sSubject)
        {
            string sBody = string.Empty,
                   sOutPut = string.Empty,
                   sTo = string.Empty;
            MailMessage Msg = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                Msg = new MailMessage();

                sTo = getEmailAddressForUser(sUserID);

                if (sTo.Trim().Length < 3)
                    sTo = "lynx_it_web@ppg.com";

                sBody = mobjDataAccess.TransformXML(sOutputXML, string.Concat(lynxAccount4.modCommon.sStylesheetPath, "\\RecertificationEmail.xsl"));
                Msg.From = "participantmanagement@lynxservices.com";
                Msg.To = sTo;
                Msg.Subject = sSubject;
                Msg.Body = sBody;
                Msg.BodyFormat = 0;
                mobjCommon.LogToAppLog("LynxAGC2.clsInsuranceCo.sendRecertificationEmail", string.Concat("Sending email to:", sTo, ",Body=", sBody),"");
                SmtpMail.SmtpServer = "";
                SmtpMail.Send(Msg);
                sOutPut = "SUCCESS";
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                Msg = null;
            }
            return sOutPut;
        }

        private string getEmailAddressForUser(string sUserID)
        {
            string sXML = string.Empty,
                  sEmail = string.Empty;
            DOMDocument40 oXML = null;
            lynxAccount4.clsUsr objUser = new lynxAccount4.clsUsr();
            try
            {
                oXML = new DOMDocument40();
                //oUSR = CreateObject("LynxAccount4.clsUsr");//change the object

                sXML = objUser.GetUsr(Mid(sUserID, 2),"");//need clarification

                if (sXML ==string.Empty)
                    sEmail = null;
                else
                {
                    oXML.loadXML(sXML);
                    if (oXML.selectSingleNode("//user") == null)
                        sEmail = "lynx_it_web@ppg.com";
                    else
                        sEmail = oXML.selectSingleNode("//user").attributes.getNamedItem("UEMail").text;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXML = null;
            }
            return sEmail;
        }

        public string AddServiceCenter(string sXML, [Optional]string sStylesheet)
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return string.Empty;
        }

        public string GetPricingZones(string sXML, [Optional]string sStylesheet)
        {
            long lInstallerID, lRegion, lInco, lPricingTypeCode;
            string sSetOfTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sCountytypeCode = string.Empty,
                   sState = string.Empty,
                   sOutPut = string.Empty,
                   strParam = string.Empty,
                   sEffectiveDate = string.Empty;
            FreeThreadedDOMDocument40 oXMLIn = null;
            DOMDocument40 objDOMDocument40;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                objmodCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLIn = new FreeThreadedDOMDocument40();
                oXMLIn.loadXML(sXML);

                if (oXMLIn.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLIn.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                objDOMDocument40 = (DOMDocument40)oXMLIn;
                strParam = "InstallerID";
                lInstallerID = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam));
                strParam = "Region";
                lRegion = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam));
                strParam = "Inco";
                lInco = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam));
                strParam = "CountyTypeCode";
                sCountytypeCode = objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam);
                strParam = "PricingTypeCode";
                lPricingTypeCode = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam));
                strParam = "State";
                sState = objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam);
                strParam = "EffectiveDate";
                sEffectiveDate = objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam);
                sSetOfTable = "so_get_retailer_county";

                sInsertSQL = string.Concat("Insert into session.", sSetOfTable, "(",
                  "county_type_cd,commit_ind,ins_co_uid_no,ins_reg_uid_no,msg_ind,parent_uid_no,",
                  "pricing_type_cd, state_cd,discount_eff_dt,user_id_no,user_session_no",
                  ") values ( '", sCountytypeCode, "', 'Y',",
                   Convert.ToString(lInco), ",", Convert.ToString(lRegion), ",'N',", Convert.ToString(lInstallerID), ", ", Convert.ToString(lPricingTypeCode), ", '", sState, "', '", sEffectiveDate, "', 'LYNXWEB','LYNXWEB')");
                mobjCommon.LogToAppLog("lynxAGC2.GetPricingZones", sInsertSQL, "", 1, 21);

                if (sStylesheet != null)
                    sStylesheet = string.Concat(lynxAccount4.modCommon.sStylesheetPath, "\\", sStylesheet);

                sOutPut = mobjDataAccess.GetSessionXML(sSetOfTable, "Dp_get_so_retailer_county", sInsertSQL, lynxAccount4.modCommon.sIngresDSN, "", "*", sStylesheet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLIn = null;
            }
            return sOutPut;
        }

        public string GetInsuranceCompanies(string sXML, [Optional]string sStylesheet)
        {
            FreeThreadedDOMDocument40 oXMLIn = null;
            DOMDocument40 objDOMDocument40;
            IXMLDOMParseError tmpParseError = null;
            long lInstallerID, lProgramNumber, lInco;
            string sSetOfTable = string.Empty,
                   sOutPut = string.Empty,
                   strParam = string.Empty,
                   sInsertSQL = string.Empty;

            try
            {
                oXMLIn = new FreeThreadedDOMDocument40();
                objmodCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLIn.loadXML(sXML);

                if (oXMLIn.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLIn.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                objDOMDocument40 = (DOMDocument40)oXMLIn;
                strParam = "InstallerID";
                lInstallerID = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam));
                strParam = "ProgramNumber";
                lProgramNumber = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam));
                strParam = "Inco";
                lInco = Convert.ToInt64(objmodCanadaCommon.getNodeValue(ref objDOMDocument40, ref strParam));

                sSetOfTable = "so_ins_pgm_co";
                sInsertSQL = string.Concat("Insert into session.", sSetOfTable, "(",
                              "inst_uid_no,lynx_pgm_no,ins_co_uid_no",
                              ") values (",
                               Convert.ToString(lInstallerID), ",", Convert.ToString(lProgramNumber), ",", Convert.ToString(lInco), ")");

                sOutPut = mobjDataAccess.GetSessionXML(sSetOfTable, string.Concat("Dp_get_", sSetOfTable), sInsertSQL, lynxAccount4.modCommon.sIngresDSN, "", "*", sStylesheet);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLIn = null;
            }
            return sOutPut;
        }

        private string GetCompanyData2(long lInstallerUidNo, bool bServiceCenters, string sUserID, [Optional]string sStylesheet = "", [Optional]int iUserType = 0)
        {
            string cACTION_CODE = "GET";
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty;
            Recordset oOutputRs = null;
            try
            {
                objmodAGC2Common = new modAGC2Common();
                sTempTable = "so_company";
                sStoredProc = "dp_manage_so_company";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no, action_cd,modify_user_type_no, user_id_no)",
                              " values (", lInstallerUidNo, ",'", cACTION_CODE, "',", iUserType, ", '", sUserID, "')");
                if (bServiceCenters)
                {
                    sSelectSql = "inst_nm, inst_uid_no, city_nm, state_cd, store_no,inst_stat_desc";
                    sFilter = "record_type=2";
                }
                else
                {
                    sSelectSql = "*";
                    sFilter = "record_type<>2 order by record_type";
                }

                if (lynxAccount4.modCommon.bActivateLog)
                {
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                }
                oOutputRs = mobjDataAccess.GetSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string Left(string strParam, int intLength)
        {
            string strResult = strParam.Substring(0, intLength);
            return strResult;
        }

        public string Right(string strParam, int intLength)
        {
            string strResult = strParam.Substring(strParam.Length - intLength, intLength);
            return strResult;
        }

        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = strParam.Substring(startIndex, intLength);
            return strResult;
        }

        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = strParam.Substring(intstartIndex);
            return strResult;
        }

    }
}
