﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using ADODB;
using System.Runtime.InteropServices;
using System.Xml;
using MSXML2;
using COMSVCSLib;
namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    class clsCforce
    {
        ObjectContext moObjectContext = null;
        private string APP_NAME = "lynxAGC2.";
        private string MODULE_NAME = "clsCforce.";
        private bool bLogError;
        lynxAccount4.modCommon mobjCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;
        modAGC2Common mobjAGC2Common = null;
        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return true;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom = new DOMDocument40();
                sOutput = mobjCommon.GetConfiguration("lynxAGC2");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);
                if (oXMLDom.parseError.errorCode == 0)
                {
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    lynxAccount4.modCommon.bActivateLog = Convert.ToBoolean(oXMLDom.selectSingleNode("//ActivateLog").text);
                }
                else
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        private void ObjectControl_Deactivate()
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetRetailerStatus(string sXML)
        {
            string sOutput = string.Empty,
                   sUserID = string.Empty;
            long iInstUID, iInscoUID, iPrgNo;
            object objExecute;
            Connection oConnection = null;
            Command oCommand = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNodeList oXmlNodeList = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                oConnection = new Connection();
                oCommand = new Command();
                oXMLDom = new DOMDocument40();

                oXMLDom.loadXML(sXML);

                iInstUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                iInscoUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InsurenceCoID").text);
                iPrgNo = Convert.ToInt64(oXMLDom.selectSingleNode("//header/ProgramID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);


                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;

                oCommand.CommandText = "{? = call lynxdba.dp_manage_inst_tier_override (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;

                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 3, "GET"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_comment_text", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 500, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_co_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iInscoUID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iInstUID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_lynx_pgm_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iPrgNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_opt_in_out_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_opt_in_out_ind_desc", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 40, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_modify_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_modify_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 7, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_opt_reason_cd", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_reason_cd_desc", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 40, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 7, sUserID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));

                oCommand.Execute(out objExecute);
                oCommand.ActiveConnection.CommitTrans();
                sOutput = mobjDataAccess.ParametersToXML(ref oCommand);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXMLNode = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }

        public string updateRetailerStatus(string sXML)
        {
            string sOutput = string.Empty,
                   sUserID = string.Empty,
                   sComment = string.Empty,
                   sOptInd = string.Empty;
            long iReasonCode, iPrgNo, iInscoUID, iInstUID;
            object objExecute;
            Connection oConnection = null;
            Command oCommand = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNodeList oXmlNodeList = null;
            try
            {
                oXMLDom = new DOMDocument40();

                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);
                oXMLDom.loadXML(sXML);

                iInstUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                iInscoUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InsurenceCoID").text);
                iPrgNo = Convert.ToInt64(oXMLDom.selectSingleNode("//header/ProgramID").text);
                sComment = oXMLDom.selectSingleNode("//header/CommentText").text;
                iReasonCode = Convert.ToInt64(oXMLDom.selectSingleNode("//header/ReasonCode").text);
                sOptInd = oXMLDom.selectSingleNode("//header/TierOptInd").text;

                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = "{? = call lynxdba.dp_manage_inst_tier_override (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;

                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 3, "UPD"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_comment_text", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 500, sComment));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_co_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iInscoUID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iInstUID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_lynx_pgm_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iPrgNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_opt_in_out_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, sOptInd));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_opt_in_out_ind_desc", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 40, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_modify_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_modify_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 7, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_opt_reason_cd", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, iReasonCode));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_reason_cd_desc", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 40, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 7, sUserID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));
                oCommand.Execute(out objExecute);
                oCommand.ActiveConnection.CommitTrans();
                sOutput = mobjDataAccess.ParametersToXML(ref oCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXMLNode = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }

        public string deleteRetailerTier(string sXML)
        {
            string sOutput = string.Empty,
                  sOptInd = string.Empty,
                  sComment = string.Empty,
                  sUserID = string.Empty;
            long iInstUID, iInscoUID, iPrgNo, iReasonCode;
            int sInd;
            object objExecute;
            Connection oConnection = null;
            Command oCommand = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNodeList oXmlNodeList = null;
            try
            {
                oConnection = new Connection();
                oCommand = new Command();
                oXMLDom = new DOMDocument40();

                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);
                oXMLDom.loadXML(sXML);

                iInstUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                iInscoUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InsurenceCoID").text);
                iPrgNo = Convert.ToInt64(oXMLDom.selectSingleNode("//header/ProgramID").text);
                sComment = oXMLDom.selectSingleNode("//header/CommentText").text;
                iReasonCode = Convert.ToInt64(oXMLDom.selectSingleNode("//header/ReasonCode").text);
                sOptInd = oXMLDom.selectSingleNode("//header/TierOptInd").text;
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = "{? = call lynxdba.dp_manage_inst_tier_override (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;

                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 3, "DEL"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_comment_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 500, sComment));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_co_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iInscoUID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iInstUID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_lynx_pgm_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iPrgNo));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_opt_in_out_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, sOptInd));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_opt_in_out_ind_desc", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 40, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_modify_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_modify_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 7, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_opt_reason_cd", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, iReasonCode));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_reason_cd_desc", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 40, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 7, sUserID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));
                oCommand.Execute(out objExecute);
                oCommand.ActiveConnection.CommitTrans();

                if (Convert.ToInt32(oCommand.Parameters["retval"]) > 0)
                    sInd = 4;
                else
                    sInd = 1;
                sOutput = mobjDataAccess.ParametersToXML(ref oCommand);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXMLNode = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }

        public Recordset cForceEval(string sXML)
        {
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty,
                   sUserID = string.Empty,
                   sMember = string.Empty;
            long iInstUID, iInscoUID;
            Recordset oOutputRs = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNodeList oXmlNodeList = null;
            try
            {
                oXMLDom = new DOMDocument40();
                oXMLDom.loadXML(sXML);
                sMember = "Y";
                iInstUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                iInscoUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InsurenceCoID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);

                if (oXMLDom.selectSingleNode("//header/Member") != null)
                    sMember = oXMLDom.selectSingleNode("//header/Member").text;

                if (sMember == "N")
                {
                    sTempTable = "so_cforce_nonmbr_eval";
                    sStoredProc = "dp_get_so_cforce_nonmbr_eval";
                }
                else
                {
                    sTempTable = "so_cforce_eval";
                    sStoredProc = "dp_get_so_cforce_eval";
                }

                sInsertSQL=string.Concat("insert into session.", sTempTable, " (commit_ind,comparator_cd,metric_limit_text,consecutive_fail_count,eval_dt,",
                           "ins_co_uid_no, inst_uid_no, metric_id_no, metric_desc, metric_value_text,",
                           "msg_ind, msg_text, orig_tier_status_no, period_ind, raw_value_text, retailer_review_ind,",
                           "status_cd, tier_opt_in_out_ind, tier_status_no,use_metric_ind, user_id_no,user_session_no)",
                           "values ('Y','','',0,'',",iInscoUID, "," ,iInstUID, ",0,'','','N','',0,'','','','','',0,'','",sUserID,"','",sUserID, "')");
                sSelectSql = " * ";
                sFilter = "CFORCE";
                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oOutputRs;
        }

        public string cForceLPEval(string sXML)
        {
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty,
                   sUserID = string.Empty,
                   sEval = string.Empty,
                   TierStatCdOld = string.Empty,
                   MixedStCd = string.Empty,
                   TierStList = string.Empty,
                   TierStat = string.Empty,
                   CompPerform = string.Empty,
                   CompPerformMet = string.Empty,
                   MetDescOld = string.Empty,
                   InsCoOld = string.Empty,
                   PeriodDate = string.Empty,
                   PerInd = string.Empty,
                   P1 = string.Empty,
                   P2 = string.Empty,
                   P3 = string.Empty,
                   InsCo = string.Empty,
                   InsCoID = string.Empty,
                   RetRevInd = string.Empty,
                   MetDesc = string.Empty,
                   InsMetDescOld = string.Empty,
                   InsMetDesc=string.Empty,
                   MetInsList = string.Empty,
                   StatCd=string.Empty,
                   MetVal=string.Empty,
                   MetStatCd=string.Empty,
                   PerDate=string.Empty,
                   PI=string.Empty, 
                   MV=string.Empty, 
                   MSC=string.Empty,
                   RRI=string.Empty,
                   InsPerInd=string.Empty,
                   InsPerDate=string.Empty,
                   InsMetVal=string.Empty,
                   PerIndOld=string.Empty,
                   InsPer=string.Empty, 
                   InsMet=string.Empty, 
                   InsStat=string.Empty,
                   InsPerIndOld=string.Empty,
                   InsCoIDOld=string.Empty,
                   InsMetStatCd=string.Empty,
                   MetInsCo = string.Empty;
            int Cnt = 0;
            int RecTypeNoOld;
            long iInstUID, iInscoUID;
            Recordset oOutputRs = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNodeList oXmlNodeList = null;
            Field RecTypeNo, TierStatCd;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom = new DOMDocument40();
                oXMLDom.loadXML(sXML);

                iInstUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                iInscoUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InsurenceCoID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);
                sEval = oXMLDom.selectSingleNode("//header/EvalCalledFrom").text;

                sTempTable = "so_cforce_lynx_eval";
                sStoredProc = "dp_get_so_cforce_lynx_eval";

                sInsertSQL = string.Concat("insert into session.", sTempTable," (commit_ind, auto_add_to_tier_dt, comparator_cd, ",
                             "consecutive_fail_count, consecutive_pass_count, dw_value_ind, eval_dt, ",
                             "eval_pass_fail_ind, ever_been_tier_1_ind, in_review_flag, ins_co_nm, ins_co_uid_no, ", 
                             "inst_uid_no, metric_id_no, metric_desc, metric_limit_text, metric_value_text, msg_ind, ",
                             "msg_text, orig_tier_status_no, period_ind, raw_value_text, record_type_no, ",
                             "retailer_review_ind, status_cd, tier_opt_in_out_ind, tier_status_no, total_fail_cnt, ",
                             "total_pass_cnt, total_warn_cnt, user_id_no, user_session_no) ",
                             "values ('Y', '', '', ",
                             "0, 0, '', '', ",
                             "'', '', '', '', 0, ",
                             iInstUID, " , 0, '', '', '', 'N', ",
                             "'', 0, '', '', 0, ",
                             "'', '', '', 0, 0, ",
                             "0, 0, '",sUserID, "' , '",sUserID,"')");

                sSelectSql = " * ";
                sFilter = "CFORCE";
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "CompForce", "LynxParticipantData");

                oOutputRs.Sort = "record_type_no, metric_desc, period_ind, ins_co_nm";

                TierStatCdOld = "StCd";
                MixedStCd = "N";
                TierStList = "";
                TierStat = "";
                CompPerform = "";
                CompPerformMet = "";
                MetDescOld = "";
                InsCoOld = "";
                PeriodDate = "";
                Cnt = 0;
                P1 = "";
                P2 = "";
                P3 = "";

                do
                {
                    RecTypeNo = oOutputRs.Fields["record_type_no"];
                    TierStatCd = oOutputRs.Fields["tier_status_no"];
                    InsCo = Convert.ToString(oOutputRs.Fields["ins_co_nm"]).Trim();
                    InsCoID = Convert.ToString(oOutputRs.Fields["ins_co_uid_no"]).Trim();
                    RetRevInd = Convert.ToString(oOutputRs.Fields["retailer_review_ind"]).Trim();
                    if (Convert.ToInt32(RecTypeNo) == 10)
                    {
                        if (Convert.ToString(TierStatCdOld) == Convert.ToString(TierStatCd))
                            StatCd = TierStatCd.ToString();
                        else
                        {
                            if (Convert.ToString(TierStatCdOld) == "StCd")
                                TierStatCdOld = TierStatCd.ToString();
                            else
                                MixedStCd = "Y";
                        }
                        TierStList = string.Concat(TierStList, "<data><InsCo>", InsCo, "</InsCo><InsCoID>", InsCoID,"</InsCoID><CurTier>",TierStatCd, "</CurTier></data>");
                        if (MixedStCd == "Y")
                            TierStat = "<TierStatCd>See Details</TierStatCd>";
                        else
                            TierStat = string.Concat("<TierStatCd>",StatCd,"</TierStatCd>");

                    }
                    if (Convert.ToInt32(RecTypeNo) == 30)
                    {
                        MetDesc = Convert.ToString(oOutputRs.Fields["metric_desc"]).Trim();
                        PerInd =  Convert.ToString(oOutputRs.Fields["period_ind"]);
                        //PerDate = Format(oOutputRs.Fields["eval_dt"], "MM/DD/YYYY");
                        MetVal = Convert.ToString(oOutputRs.Fields["raw_value_text"]).Trim();
                        MetStatCd = Convert.ToString(oOutputRs.Fields["status_cd"]);
                        RetRevInd = Convert.ToString(oOutputRs.Fields["retailer_review_ind"]).Trim();

                        PI = string.Concat("Per",PerInd);
                        MV = string.Concat("MetVal", PerInd);
                        MSC = string.Concat("MetStatCd",PerInd);
                        RRI = string.Concat("RetRevInd", PerInd);

                        switch (PerInd)
                        {
                            case "1":
                                P1 = string.Concat("<P1Dt>",PerDate,"</P1Dt>");
                                break;
                            case "2":
                                P2 = string.Concat("<P2Dt>",PerDate,"</P2Dt>");
                                break;
                            case "3":
                                P3 = string.Concat("<P3Dt>",PerDate,"</P3Dt>");
                                break;
                        }
                        if (MetDescOld == MetDesc || MetDescOld == "")
                        {
                            CompPerform = string.Concat(CompPerform, "<", PI, ">", PerInd, "</", PI, "><", MV, ">", MetVal, "</", MV, "><", MSC, ">", MetStatCd, "</", MSC, "><", RRI, ">", RetRevInd, "</", RRI, ">");
                            PerIndOld = PerInd;
                            if (MetDescOld == "")
                                MetDescOld = MetDesc;

                        }
                        else
                        {
                            CompPerformMet = string.Concat(CompPerformMet, "<data><RecTypeNo>", RecTypeNo, "</RecTypeNo><MetDesc>", MetDescOld, "</MetDesc>", CompPerform,"</data>");
                            MetDescOld = MetDesc;
                            CompPerform = "";
                            CompPerform = string.Concat(CompPerform,"<", PI,">",PerInd,"</",PI,"><",MV,">", MetVal, "</",MV, "><",MSC,">",MetStatCd,"</", MSC,"><",RRI, ">", RetRevInd,"</",RRI, ">");
                            Cnt = Cnt + 1;
                            RecTypeNoOld = Convert.ToInt32(RecTypeNo);
                        }

                    }
                    oOutputRs.MoveNext();

                } while (!oOutputRs.EOF);

                RecTypeNoOld = Convert.ToInt32(RecTypeNo);
                if (RecTypeNoOld == 30)
                    CompPerformMet = string.Concat(CompPerformMet, "<data><RecTypeNo>",RecTypeNo, "</RecTypeNo><MetDesc>", MetDescOld, "</MetDesc>", CompPerform, "</data>");

                oOutputRs.Sort= "record_type_no, metric_desc, ins_co_nm, period_ind";

                InsMetDescOld = "";
                InsCoOld = "";
                MetInsList = "";
                MetInsCo = "";
                RecTypeNoOld = 0;

                do
                {
                   RecTypeNo = oOutputRs.Fields["record_type_no"];
                   TierStatCd = oOutputRs.Fields["tier_status_no"];
                   InsCo = Convert.ToString(oOutputRs.Fields["ins_co_nm"]).Trim();
                   InsCoID = Convert.ToString(oOutputRs.Fields["ins_co_uid_no"]).Trim();

                   if (Convert.ToInt32(RecTypeNo) == 20)
                   {
                       InsMetDesc = Convert.ToString(oOutputRs.Fields["metric_desc"]).Trim();
                       InsPerInd = Convert.ToString(oOutputRs.Fields["period_ind"]);
                       InsPerDate = Convert.ToString(oOutputRs.Fields["eval_dt"]);
                       InsMetVal = Convert.ToString(oOutputRs.Fields["raw_value_text"]).Trim();
                       InsMetStatCd = Convert.ToString(oOutputRs.Fields["status_cd"]);
                       RetRevInd = Convert.ToString(oOutputRs.Fields["retailer_review_ind"]).Trim();
                       InsPer = string.Concat("Per", InsPerInd);
                       InsMet = string.Concat("MetVal", InsPerInd);
                       InsStat = string.Concat("MetStatCd", InsPerInd);
                       RRI = string.Concat("RetRevInd", InsPerInd);
                       RecTypeNoOld = Convert.ToInt32(RecTypeNo);

                       if (InsMetDescOld == InsMetDesc || InsMetDescOld == "")
                       {
                           if (InsMetDescOld == "")
                               InsMetDescOld = InsMetDesc;

                           if (InsCoOld == InsCo || InsCoOld == "")
                           {
                               MetInsCo = string.Concat(MetInsCo, "<", InsPer, ">", InsPerInd, "</", InsPer, "><", InsMet, ">", InsMetVal, "</", InsMet, "><", InsStat, ">", InsMetStatCd, "</", InsStat, "><", RRI, ">", RetRevInd, "</", RRI, ">");
                               InsPerIndOld = InsPerInd;
                               if (InsCoOld == "")
                               {
                                   InsCoOld = InsCo;
                                   InsCoIDOld = InsCoID;
                               }

                           }
                           else
                           {
                               MetInsList = string.Concat(MetInsList, "<InsCoMet><MetName>", InsMetDescOld, "</MetName><InsCo>", InsCoOld, "</InsCo><InsCoID>", InsCoIDOld, "</InsCoID>", MetInsCo, "</InsCoMet>");
                               InsCoOld = InsCo;
                               InsCoIDOld = InsCoID;
                               MetInsCo = "";
                               MetInsCo = string.Concat(MetInsCo, "<", InsPer, ">", InsPerInd, "</", InsPer, "><", InsMet, ">", InsMetVal, "</", InsMet, "><", InsStat, ">", InsMetStatCd, "</", InsStat, "><", RRI, ">", RetRevInd, "</", RRI, ">");
                           }

                       }
                       else
                       {
                           MetInsList = string.Concat(MetInsList,"<InsCoMet><MetName>", InsMetDescOld, "</MetName><InsCo>",InsCoOld,"</InsCo><InsCoID>",InsCoIDOld,"</InsCoID>" ,MetInsCo,"</InsCoMet>");
                           InsMetDescOld = InsMetDesc;
                           InsCoOld = InsCo;
                           InsCoIDOld = InsCoID;
                           MetInsCo = "";
                           MetInsCo = string.Concat(MetInsCo,"<",InsPer,">",InsPerInd,"</",InsPer,"><", InsMet, ">",InsMetVal,"</",InsMet, "><",InsStat,">", InsMetStatCd,"</",InsStat, "><",RRI,">",RetRevInd,"</", RRI,">");

                       }
                   }

                   if (RecTypeNoOld == 20 && Convert.ToInt32(RecTypeNo) == 30)
                   {
                       MetInsList = string.Concat(MetInsList, "<InsCoMet><MetName>",InsMetDescOld, "</MetName><InsCo>",InsCoOld, "</InsCo><InsCoID>",InsCoIDOld,"</InsCoID>", MetInsCo, "</InsCoMet>");
                       RecTypeNoOld = Convert.ToInt32(RecTypeNo);
                   }

                   oOutputRs.MoveNext();

                } while (!oOutputRs.EOF);
                sOutput = string.Concat(sOutput, "<TierStat>",TierStat, "</TierStat><CurTierInfo>",TierStList, "</CurTierInfo>");
                sOutput = string.Concat(sOutput, "<CompPerformMet>",CompPerformMet, "</CompPerformMet>");
                sOutput = string.Concat(sOutput, "<PeriodDates>", P1, P2,P3,"</PeriodDates>");
                sOutput = string.Concat(sOutput, "<MetList>", MetInsList,"</MetList>");
                sOutput = string.Concat("<LynxData>",sOutput,"</LynxData>");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sOutput;
        }

        public string GetcForceProfileItems(long lInstallerUidNo, long lInsCoID, [Optional] string sStylesheet, [Optional] string sUserID)
        {
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty,
                   sUserIdNo = string.Empty;
            Recordset oOutputRs = null;
            DOMDocument40 oXMLDom = null;
            try
            {
                sTempTable = "so_profile_item";
                sStoredProc = "dp_get_so_profile_item";

                sInsertSQL = string.Concat("insert into session.", sTempTable, " (called_from,commit_ind,dnis_no,msg_ind,profile_cd,profile_uid_no,profile_dep_uid_no,profile_object,user_id_no,user_session_no)",
                                           "values ('','Y',0,'N','ALL',", lInstallerUidNo, ",", lInsCoID, ",'inco-inst', 'lynxweb', 'lynxweb')");
                sSelectSql = "*";

                if (lynxAccount4.modCommon.bActivateLog)
                    mobjAGC2Common.sLogTime = string.Concat(mobjAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (lynxAccount4.modCommon.bActivateLog)
                    mobjAGC2Common.sLogTime = string.Concat(mobjAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now.ToString(), "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    mobjAGC2Common.sLogTime = string.Concat(mobjAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "item", "profile");

                if (lynxAccount4.modCommon.bActivateLog)
                    mobjAGC2Common.sLogTime = string.Concat(mobjAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now.ToString(), "\"/>");

                if (sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string GetRetailerTierStatus(string sXML)
        {
            Connection oConnection = null;
            Command oCommand = null;
            DOMDocument40 oxmlDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNodeList oXmlNodeList = null;
            string sOutput = string.Empty,
                   sUserID = string.Empty,
                   sComment = string.Empty,
                   sOptInd = string.Empty;

            long iInstUID, iInscoUID, iPrgNo, iReasonCode;
            int sInd;
            object objExecute;
            try
            {
                oConnection = new Connection();
                oCommand = new Command();
                oxmlDom = new DOMDocument40();

                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);
                oxmlDom.loadXML(sXML);

                iInstUID =Convert.ToInt32(oxmlDom.selectSingleNode("//header/InstallerUID").text);
                iInscoUID = Convert.ToInt32(oxmlDom.selectSingleNode("//header/InsurenceCoID").text);
                sUserID = mobjCommon.PreFixUserId(oxmlDom.selectSingleNode("//header/UserID").text);

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = "{? = call lynxdba.dp_get_cf_tier_status_no (?,?,?,?,?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;

                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_ins_co_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iInscoUID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, iInstUID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_curr_tier_status_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_eligible_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_curr_tier_status_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_tier_eligible_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 7, sUserID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));

                oCommand.Execute(out objExecute);
                oCommand.ActiveConnection.BeginTrans();

                if (Convert.ToInt32(oCommand.Parameters["retval"]) > 0)
                    sInd = 4;
                else
                    sInd = 1;

                sOutput = mobjDataAccess.ParametersToXML(ref oCommand);
                mobjCommon.CtxSetComplete();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oxmlDom = null;
                oXMLNode =null;
                oCommand = null;
                oConnection =null;
            }
            return sOutput;
        }

        public Recordset Get_cForce_Inst_Review(long lPrgNo, long lInsCoID, string sWarningInd)
        {
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sFilter = string.Empty,
                   sUserIdNo = string.Empty,
                   sOutput = string.Empty;
            Recordset oOutputRs = null;
            try
            {
                sTempTable = "so_cforces_review_lookup";
                sStoredProc = "dp_get_so_cforces_inst_review";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (auto_add_to_tier_dt,consecutive_fail_count,dwhs_proc_ind,eval_dt,inst_uid_no,inst_nm,ins_co_uid_no,ins_co_cd,lynx_pgm_no,lynx_pgm_nm,orig_tier_status_no,review_by_dt,tier_opt_in_out_ind,tier_status_no,commit_ind,get_warning_ind,msg_ind,msg_text,user_id_no,user_session_no)",
                                           " values ('',0,'','',0,'',", lInsCoID, ",'', ", lPrgNo, ",'',0,'','',0,'Y','", sWarningInd, "','N','','lynxweb','lynxweb')");
                sSelectSql = "*";
                sFilter = "CForce_GPA_Review";

                if(lynxAccount4.modCommon.bActivateLog)
                    mobjAGC2Common.sLogTime = string.Concat(mobjAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now.ToString(), "\"/>");

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (lynxAccount4.modCommon.bActivateLog)
                    mobjAGC2Common.sLogTime = string.Concat(mobjAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"",DateTime.Now.ToString(),"\"/>");
                       
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oOutputRs;
        }

        public Recordset Get_cForce_Repair(long lInsCoID, long lInstallerUidNo, string sHistoryInd)
        {
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sFilter = string.Empty,
                   sUserIdNo = string.Empty,
                   sOutput = string.Empty;
            Recordset oOutputRs = null;
            try
            {
                sTempTable = "so_cforces_repair";
                sStoredProc = "dp_get_so_cforces_repair";
                sInsertSQL = string.Concat("insert into session." ,sTempTable," (commit_ind,include_sc_hist_ind,ins_co_uid_no,msg_ind,parent_uid_no,user_id_no,user_session_no,eval_pass_fail_ind,msg_text, ", "period_ind,state_cd,comparator_cd,dw_value_ind,eval_dt,metric_id_no,metric_desc,metric_limit_text,metric_value_text,raw_value_text, ",
                                           "retailer_review_ind,status_cd, tier_opt_in_out_ind,use_metric_ind,addr_1st,city_nm,inst_nm,conv_ratio,conv_rpr_clm_cnt,create_dt,inst_uid_no, ",
                                           "tier_assigned,ws_clm_cnt,ws_rpr_clm_cnt,ws_rpr_ratio,total_pass_no,total_fail_no,record_type_cd )",
                                           "values ('Y', '", sHistoryInd, "',", lInsCoID, ", 'N', ",lInstallerUidNo, ", 'lynxweb', 'lynxweb', '','',0,'','','','',0,'','','','','','','','','','','',0,0,'',0,0,0,0,0,0,0,'' )");
                sSelectSql = "*";
                sFilter = "cforce_repair";

               if(lynxAccount4.modCommon.bActivateLog)
		          mobjAGC2Common.sLogTime = string.Concat(mobjAGC2Common.sLogTime,"<detail function=\"GetSessionRows\" type=\"begin\" time=\"",DateTime.Now.ToString(),"\"/>");

               oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

               if (lynxAccount4.modCommon.bActivateLog)
                   mobjAGC2Common.sLogTime = string.Concat(mobjAGC2Common.sLogTime,"<detail function=\"GetSessionRows\" type=\"end\" time=\"",DateTime.Now.ToString(),"\"/>");
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return oOutputRs;
        }
    }
}
