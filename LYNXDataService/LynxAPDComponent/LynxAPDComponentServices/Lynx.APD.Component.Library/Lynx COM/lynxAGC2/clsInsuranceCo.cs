﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMSVCSLib;
using MSXML2;
using ADODB;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using System.Globalization;
using System.Web.Mail;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    class clsInsuranceCo
    {
        private ObjectContext moObjectContext = null;
        //Define the Application name to be used in Error Handling
        private const string APP_NAME = "lynxAGC2.";
        private const string MODULE_NAME = "clsInsuranceCo.";
        lynxAccount4.modCommon mobjCommon = null;
        modCanadaCommon mobjCanadaCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;

        private bool ObjectControl_CanBePooled()
        {
            bool retValue = false;
            try
            {
                retValue = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return retValue;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            //xml objects
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tempparseError = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon(); 
                sOutput = mobjCommon.GetConfiguration("lynxAGC2");
                oXMLDom = new DOMDocument40();
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);
                if (oXMLDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    lynxAccount4.modCommon.bActivateLog = Convert.ToBoolean(oXMLDom.selectSingleNode("//ActivateLog").text);
                }
                else
                {
                    tempparseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tempparseError);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(0, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(param.Length - length, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(startIndex, length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(param))
                    result = param.Substring(startIndex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public string GetHistory(string sInputXML, string sStylesheet = "")
        {


            FreeThreadedDOMDocument40 oXMLIn = null;
            string sType = string.Empty;
            string sInstUidNo = string.Empty;
            string sProgramNo = string.Empty;
            string sFilter = string.Empty;
            string sInsertSQL = string.Empty;
            string sSetOfTable = string.Empty;
            string sStoredProc = string.Empty;
            string sUserID = string.Empty;
            string sCurrentRow = string.Empty;
            string sProfileItem = string.Empty;
            clsParent oParent = null;
            clsServiceCenter oServiceCenter = null;
            string sOutput = string.Empty;
            string sServiceID = string.Empty;
            string strParam = string.Empty;
            string sUserType = string.Empty;
            IXMLDOMParseError tempparseError = null;
            DOMDocument40 objDomdocument = null;
            try
            {

                //Load the input XML into the DOM
                oXMLIn = new FreeThreadedDOMDocument40();
                mobjCommon = new lynxAccount4.modCommon(); 
                mobjCanadaCommon = new modCanadaCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();

                oXMLIn.loadXML(sInputXML);
                tempparseError = oXMLIn.parseError;
                if (oXMLIn.parseError.errorCode != 0)
                    mobjCommon.InvalidXML(ref tempparseError);

                strParam = "Type";
                objDomdocument = (DOMDocument40)oXMLIn;
                
                //Extract the input parameters from the input xml
                sType = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam).ToUpper();
                //'100       lFedTaxID = Val(getNodeValue(oXMLIn, "FedTaxID"))
                //'110       lProgramNumber = Val(getNodeValue(oXMLIn, "ProgramNumber"))
                //'115       lAdminNumber = Val(getNodeValue(oXMLIn, "AdminNumber"))
                //'120       sCommentText = Replace(Left(getNodeValue(oXMLIn, "CommentText"), 1000), "'", "''")
                //'130       sEffectiveDate = getNodeValue(oXMLIn, "EffectiveDate")
                //'135       sUserID = PreFixUserId(getNodeValue(oXMLIn, "UserID"))

                switch (sType)
                {
                    case "COMPANY INFORMATION":
                        strParam = "InstallerID";
                        sInstUidNo = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        strParam = "UserType";
                        sUserType = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        sSetOfTable = "so_company_archive";
                        sStoredProc = "dp_get_so_company_archive";
                        sFilter = "1=1 order by modify_dt desc ";
                        sInsertSQL = string.Concat("Insert into session.", sSetOfTable, " (",
                                        "commit_ind, inst_uid_no, msg_ind, user_id_no) values ('Y",
                                        "',", sInstUidNo, ",'Y','lynxweb')");
                        break;

                    case "SERVICE CENTER INFORMATION":
                        strParam = "ServiceCenterID";
                        sInstUidNo = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        sSetOfTable = "so_service_center_archive";
                        sStoredProc = "dp_get_so_service_center_archive";
                        sFilter = "1=1 order by modify_dt desc ";
                        sInsertSQL = string.Concat("Insert into session.", sSetOfTable, " (",
                                        "commit_ind, inst_uid_no, msg_ind, user_id_no, user_session_no) values ('Y",
                                        "',", sInstUidNo, ",'Y','lynxweb','lynxweb')");
                        break;

                    case "SERVICES PROVIDED":
                        strParam = "ServiceCenterID";
                        sInstUidNo = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        strParam = "ServiceID";
                        sServiceID = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        sSetOfTable = "so_inst_services_archive";
                        sStoredProc = "dp_get_so_inst_services_archive";
                        sFilter = string.Concat("service_type_id = ", sServiceID, " order by modify_dt desc ");
                        sInsertSQL = string.Concat("Insert into session.", sSetOfTable, " (",
                                        "commit_ind, inst_uid_no, msg_ind, user_id_no, user_session_no) values ('Y",
                                        "',", sInstUidNo, ",'Y','lynxweb','lynxweb')");
                        break;

                    case "GLASS PROGRAM":
                        strParam = "InstallerID";
                        sInstUidNo = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        strParam = "ProgramNo";
                        sProgramNo = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        sSetOfTable = "so_company_pgm_archive";
                        sStoredProc = "dp_get_so_company_pgm_archive";
                        sFilter = string.Concat("lynx_pgm_no = '", sProgramNo, "' order by modify_dt desc ");
                        sInsertSQL = string.Concat("Insert into session.", sSetOfTable, " (",
                                        "commit_ind, inst_uid_no, msg_ind, user_id_no, user_session_no) values ('Y",
                                        "',", sInstUidNo, ",'Y','lynxweb','lynxweb')");
                        break;

                    case "COMPANY PROFILE ITEMS":
                        strParam="InstallerID";
                        sInstUidNo = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        strParam = "AttribNm";
                        sProfileItem = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        sSetOfTable = "so_ins_prfi_hist";
                        sStoredProc = "dp_get_so_ins_prfi_hist";
                        sFilter = "1=1 order by modify_dt desc ";
                        sInsertSQL = string.Concat("Insert into session.", sSetOfTable, " (",
                                        "uid_no, table_nm, attrib_nm) values (",
                                        "", sInstUidNo, ",'inst_prfi','", sProfileItem, "')");
                    //1095            sCurrentRow = oParent.GetCompanyProfileItems(sInstUidNo)
                        break;

                    case "SERVICE CENTER PROFILE ITEMS":
                        strParam="ServiceCenterID";
                        sInstUidNo = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        strParam = "AttribNm";
                        sProfileItem = mobjCanadaCommon.getNodeValue(ref objDomdocument, ref strParam);
                        sSetOfTable = "so_ins_prfi_hist";
                        sStoredProc = "dp_get_so_ins_prfi_hist";
                        sFilter = "1=1 order by modify_dt desc ";
                        sInsertSQL = string.Concat("Insert into session.", sSetOfTable, " (",
                                        "uid_no, table_nm, attrib_nm) values (",
                                        "", sInstUidNo, ",'inst_prfi','", sProfileItem, "')");
                        break;
                    //1160            sCurrentRow = oServiceCenter.GetServiceCenterProfileItems(sInstUidNo)

                    default:
                        break;
                    //AGC2_ErrorHandler "Unknown History Type:" & sType
                }

                //Add the full path to the stylesheet, if there is one
                if (sStylesheet != string.Empty)
                    sStylesheet = string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet);

                if (sFilter == string.Empty)
                    sFilter = "1=1";

                //execute the stored proc and apply the stylesheet to the results
                //Take out stylesheet here...put it below
                sOutput = mobjDataAccess.GetSessionXML(sSetOfTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, "distinct *", "");

                if (sCurrentRow != string.Empty)
                    sOutput = string.Concat("<a>", sCurrentRow, sOutput, "</a>");

                //Need to check the stylesheet here in case we are concating
                //XML together
                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, sStylesheet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                //Cleaunp
                oXMLIn = null;
            }

            return sOutput;
        }

        public string ManageAdminAlert(string sInputXML, string sStylesheet = "")
        {
            FreeThreadedDOMDocument40 oXMLIn = null;
            string sAction = string.Empty;
            string sAlertType = string.Empty;
            double lFedTaxID = 0;
            long lProgramNumber = 0;
            long lInsCoID = 0;
            int lAdminNumber = 0;
            string sCommentText = string.Empty;
            string sEffectiveDate = string.Empty;
            string sInsertSQL = string.Empty;
            string sSetOfTable = string.Empty;
            string sUserID = string.Empty;
            Recordset oOutputRs = null;
            string sOutput = string.Empty;
            string sStoredProc = string.Empty;
            string sProductTypeCode = string.Empty;
            string sSuspendLevelCode = string.Empty;
            string sSuspendTypeCode = string.Empty;
            string sUserSessionNo = string.Empty;
            string strParam = string.Empty;
            long lCompanyID = 0;
            IXMLDOMParseError tempParseError = null;
            DOMDocument40 objDocument40 = null;
            int tempVal = 0;
            try
            {
                //Read the config file
#if (Developer == true)
           //'use the conditional compilation argument "Developer = 1"
           //'when debugging this component from your desktop
            ObjectControl_Activate();
#endif

                //Load the input XML into the DOM
                oXMLIn = new FreeThreadedDOMDocument40();
                mobjCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon(); 
                mobjDataAccess = new lynxAccount4.modDataAccess();

                oXMLIn.loadXML(sInputXML);
                tempParseError = oXMLIn.parseError;
                if (oXMLIn.parseError.errorCode != 0)
                    mobjCommon.InvalidXML(ref tempParseError);

                //Extract the input parameters from the input xml
                objDocument40 = (DOMDocument40)oXMLIn;
                strParam="Action";
                sAction = mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam).ToUpper();
                strParam = "FedTaxID";
                lFedTaxID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDocument40,ref strParam));
                strParam = "CompanyID";
                lCompanyID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam));
                strParam = "ProgramNumber";
                lProgramNumber = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam));
                strParam = "InsuranceCompanyID";
                lInsCoID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam));
                strParam = "CommentText";
                sCommentText = Regex.Replace(Left(mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam), 1000), "'", "''", RegexOptions.IgnoreCase);
                strParam = "EffectiveDate";
                sEffectiveDate = mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam);
                strParam = "UserID";
                sUserID = mobjCommon.PreFixUserId(mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam));
                strParam = "ProductTypeCode";
                sProductTypeCode = mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam);
                strParam = "SuspendLevelCode";
                sSuspendLevelCode = mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam);
                strParam = "SuspendTypeCode";
                sSuspendTypeCode = mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam);
                strParam = "UserSessionNo";
                sUserSessionNo = mobjCanadaCommon.getNodeValue(ref objDocument40, ref strParam);

                sSetOfTable = "so_suspend_rtlr";
                sStoredProc = "dp_manage_so_suspend_rtlr";
                sInsertSQL = string.Concat("Insert into session.", sSetOfTable, " (",
                                "action_cd, commit_ind, eff_dt, federal_tax_id, inst_uid_no, ins_co_uid_no, lynx_pgm_no, msg_ind, ",
                                "product_type_cd, suspend_comment_text, suspend_level_cd, suspend_type_cd, user_id_no, user_session_no) values ('",
                                sAction, "', 'Y', '", sEffectiveDate, "',", lFedTaxID, ", ", lCompanyID, ", ", lInsCoID, ", ", lProgramNumber, ", 'N' ",
                                ",'", sProductTypeCode, "','", sCommentText, "','", sSuspendLevelCode, "', '", sSuspendTypeCode, "', '", sUserID,
                                "', '", sUserSessionNo, "')");

                oOutputRs = mobjDataAccess.GetSessionRows2(sSetOfTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, @"\", sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                //Cleaunp
                oXMLIn = null;
            }
            return sOutput;
        }

        public string ManageTINQueue(string sInputXML, string sStylesheet = "")
        {

            FreeThreadedDOMDocument40 oXMLIn = null;

            //Parameters
            string sAction = string.Empty;
            int lProgramNumber = 0;
            string sAssignedUser = string.Empty;
            string sCommentText = string.Empty;
            string sCompletionUser = string.Empty;
            string sFollowUpDate = string.Empty;
            string sNextEligibilityDate = string.Empty;
            string sReasonCode = string.Empty;
            string sStatusCode = string.Empty;
            long lQueueID = 0;
            long lInstallerID = 0;
            long lApplicantContactID = 0;
            string sGpaCode = string.Empty;
            string sUserID = string.Empty;
            string sSession = string.Empty;
            string sInsertSQL = string.Empty;
            string sSetOfTable = string.Empty;
            string sProcName = string.Empty;
            string sFilter = string.Empty;
            string sResult = string.Empty;
            string sOutputXML = string.Empty;
            string sEmail = string.Empty;
            string sSortBy = string.Empty;
            string sSortOrder = string.Empty;
            string strparam = string.Empty;
            DOMDocument40 objDOMDocument = null;
            //Dim lCommentTypeNo As Long
            Command oCmd = null;
            Connection oConnection = null;
            int sInd = 0;

            int tempVal = 0;
            string retTINQueue = string.Empty;
            object objtemp = null;
            IXMLDOMParseError tempParseError = null;
            try
            {

                //Read the config file
#if (Developer == true) 
        //'use the conditional compilation argument "Developer = 1"
        //'when debugging this component from your desktop
         ObjectControl_Activate();
#endif

                //Load the input XML into the DOM
                oXMLIn = new FreeThreadedDOMDocument40();
                mobjCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon(); 
                mobjDataAccess = new lynxAccount4.modDataAccess();

                oXMLIn.loadXML(sInputXML);
                tempParseError = oXMLIn.parseError;
                if (oXMLIn.parseError.errorCode != 0)
                    mobjCommon.InvalidXML(ref tempParseError);
                objDOMDocument=(DOMDocument40)oXMLIn;
                strparam = "Action";
                //Extract the input parameters from the input xml
                sAction = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam).ToUpper();
                strparam= "ProgramNumber";
                lProgramNumber = Convert.ToInt32(mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam));
                strparam="InstallerID";
                lInstallerID = Convert.ToInt64( mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam));
                //lCommentTypeNo = Val(getNodeValue(oXMLIn, "Type"))
                strparam="ApplicantContactiD";
                lApplicantContactID = Convert.ToInt32(mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam ));
                strparam="AssignedUserID";
                sAssignedUser = mobjCommon.PreFixUserId(mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam));
                strparam="CompletionCommentText";
                sCommentText = mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam);
                strparam="CompletionUserID";
                sCompletionUser = mobjCommon.PreFixUserId(mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam ));
                strparam="FollowUpDate";
                sFollowUpDate = mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam );
                strparam="NextEligibilityDate";
                sNextEligibilityDate = mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam );
                strparam="ReasonCode";
                sReasonCode = mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam );
                strparam="GPACode";
                sGpaCode = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam);
                if (!(Information.IsNumeric(sGpaCode)))
                    sGpaCode = "0";

                strparam= "StatusCode";
                sStatusCode = mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam).ToUpper().Trim();
                strparam= "QueueID";
                lQueueID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam));
                strparam= "UserID";
                sUserID = mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam);
                strparam="UserSession";
                sSession = mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam );

                //If the user ID is 0, there really isn't a user id
                if (sCompletionUser == "w000000")
                    sCompletionUser = "";

                //Assigned the queue item to the followup/completion user if not yet assigned
                if ((sAssignedUser == string.Empty) || (sAssignedUser == "w000000"))
                    sAssignedUser = sCompletionUser;

                if (sAction == "GET")
                {
                    sProcName = "dp_get_so_tin_queue";
                    sFilter = " 1=1 ";
                    if (lQueueID > 0)
                        sFilter = string.Concat(sFilter, " and tin_queue_uid_no=", lQueueID, " ");
                    strparam = "FederalTaxID";
                    if (mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam) != string.Empty)
                    {
                        strparam = "FederalTaxID";
                        sFilter = string.Concat(sFilter, " and federal_tax_id=", mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam), " ");
                    }
                    if (lInstallerID > 0)
                        sFilter = string.Concat(sFilter, " and inst_uid_no=", lInstallerID, " ");
                    strparam = "AssignedUserID";
                    if (mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam) != string.Empty)
                    {
                        strparam = "AssignedUserID";
                        sFilter = string.Concat(sFilter, " and assigned_user_id_no='", mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref  strparam), "' ");
                    }
                    strparam = "State";
                    if (mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam) != string.Empty)
                    {
                        strparam = "State";
                        sFilter = string.Concat(sFilter, " and state_cd='", mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam), "' ");
                    }
                    strparam = "Type";
                    if (mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam) != string.Empty)
                    {
                        sFilter = string.Concat(sFilter, " and comment_type_no=", mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strparam), " ");
                    }
                    strparam = "Program";
                    if (mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam) != string.Empty)
                        sFilter = string.Concat(sFilter, " and lynx_pgm_nm='", mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam), "' ");

                    strparam = "SortBy";
                    sSortBy = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam);
                    if (sSortBy == string.Empty)
                        sSortBy = "create_dt ";

                    strparam = "SortOrder";
                    sSortOrder = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strparam);
                    if (sSortOrder == string.Empty)
                        sSortOrder = string.Empty;

                    sFilter = string.Concat(sFilter, " ORDER BY ", sSortBy, " ", sSortOrder);

                    //Create the insert statement for the so table
                    sSetOfTable = "so_tin_queue";
                    sInsertSQL = string.Concat("Insert into session.", sSetOfTable, "(",
                        "glass_admin_no, commit_ind, msg_ind, assigned_user_id_no, user_id_no, user_session_no) values (",
                        Convert.ToInt64(sGpaCode), ", 'Y', 'N', '", sAssignedUser, "', '", sUserID, "', '", sSession, "')");

                    //Add the full path to the stylesheet, if there is one
                    if (sStylesheet != string.Empty)
                        sStylesheet = string.Concat(lynxAccount4.modCommon.sStylesheetPath, @"\", sStylesheet);

                    //execute the stored proc and apply the stylesheet to the results
                    sResult = mobjDataAccess.GetSessionXML(sSetOfTable, sProcName, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, "*", sStylesheet);

                    //Apply list boxes based upon lookup xml data
                    if (sResult.IndexOf("<agc2ListBox") > 0)
                        sResult = mobjCanadaCommon.applyListBoxes(sResult);

                    retTINQueue = sResult;
                }
                else
                {
                    //Call LogToAppLog("clsInsuranceCo.ManageTINQueue", sInputXML, "")
                    sProcName = "dp_manage_tin_queue";
                    oCmd = new Command();
                    oConnection = new Connection();

                    //setup the connection
                    oConnection.ConnectionTimeout = 60;
                    oConnection.Open(lynxAccount4.modCommon.sIngresDSN);

                    oCmd.ActiveConnection = oConnection;
                    oCmd.CommandType = CommandTypeEnum.adCmdText;
                    oCmd.CommandTimeout = 90;
                    oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, -1));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, sAction));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_assigned_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 7, sAssignedUser));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_dba_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 30, ""));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_comment_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 1000, ""));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_comment_type_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_completion_comment_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 1000, sCommentText));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_completion_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, DateTime.Now.ToString(@"dd-MM-yyyy hh:mm:ss tt", new CultureInfo("en-US"))));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_completion_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 7, sCompletionUser));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_decline_reason_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 3, sReasonCode));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_federal_tax_id", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_follow_up_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, sFollowUpDate));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_glass_pgm_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 30, "DEFAULTPROGRAM"));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_glass_admin_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, Convert.ToInt64(sGpaCode)));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 20, "DEFAULTINSTALLER"));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_lynx_pgm_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_next_elligibility_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, sNextEligibilityDate));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_primary_email_addr", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 50, ""));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_process_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, ""));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_process_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 7, ""));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_process_user_first_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, "FIRST"));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_process_user_last_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, "DEFAULTLAST"));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_status_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, sStatusCode));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_tin_queue_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lQueueID));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, "Y"));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sUserID));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sSession));
                    oCmd.CommandText = string.Concat("{? = call lynxdba.", sProcName, "(", mobjDataAccess.getQuestionMarks(oCmd.Parameters.Count), ") }");
                    oCmd.Execute(out objtemp);

                    // Call LogToAppLog("clsInsuranceCo.ManageTINQueue ", sProcName & " result:" & oCmd.Parameters("retval").Value, "")
                    //STEP IV B : Check for Error message returned
                    if (Convert.ToInt32(oCmd.Parameters["retval"]) != 0)
                    {
                        if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0)
                            sInd = 1;
                        else
                            sInd = 4;
                        throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError, ",", sProcName, ",", oCmd.Parameters["h_msg_text"]));
                    }
                }

                //Call LogToAppLog("clsInsuranceCo.ManageTINQueue ", "Action=" & sAction & ";Status=" & sStatusCode & ";", "")

                if (((sAction.ToUpper().Trim()) == "COMPLETE") && (sStatusCode.ToUpper().Trim()) == "A")
                {
                    //Call LogToAppLog("clsInsuranceCo.ManageTINQueue", "preparing completion email for sending", "")
                    sOutputXML = mobjDataAccess.ParametersToXML(ref oCmd);

                    mobjCommon.LogToAppLog("clsInsuranceCo.ManageTINQueue", string.Concat("InputXML:", sInputXML), "");
                    mobjCommon.LogToAppLog("clsInsuranceCo.ManageTINQueue", string.Concat("OutputXML:", sOutputXML), "");
                    sEmail = getEmailAddressForUser(Convert.ToString(oCmd.Parameters["h_process_user_id_no"].Value));
                    if (sEmail != oCmd.Parameters["h_primary_email_addr"].Value)
                        sEmail = string.Concat(sEmail, "; ", oCmd.Parameters["h_primary_email_addr"].Value);
                    mobjCommon.LogToAppLog("clsInsuranceCo.ManageTINQueue ", string.Concat("Email To: ", sEmail), "");
                    sendCongratulatoryEmail(sOutputXML, sEmail, string.Concat(oCmd.Parameters["h_glass_pgm_nm"].Value.ToString().Trim(), " Glass Program Application Confirmation Email"));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                //Cleanup
                oXMLIn = null;
            }
            return retTINQueue;
        }

        public string ManageTINComments(string sInputXML, string sStylesheet = "")
        {

            FreeThreadedDOMDocument40 oXMLIn = null;
            string sAction = string.Empty;
            int lProgramNumber = 0;
            long lFedTaxID = 0;
            string sUser = string.Empty;
            string sCommentText = string.Empty;
            int lCommentType = 0;
            string sCompletionCommentText = string.Empty;
            string sCompletionUser = string.Empty;
            string sCompletionDate = string.Empty;
            string sInsertSQL = string.Empty;
            string sSetOfTable = string.Empty;
            string strParam = string.Empty;
            string sCreateDate = string.Empty;
            int tempVal = 0;
            IXMLDOMParseError tempParseError = null;
            DOMDocument40 objDocument = null;
            string retManageTINComments = string.Empty;
            try
            {
                mobjCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon(); 
                mobjDataAccess = new lynxAccount4.modDataAccess();
                //Read the config file
#if (Developer ==true)
           //'use the conditional compilation argument "Developer = 1"
           //'when debugging this component from your desktop
            ObjectControl_Activate();
#endif

                //Load the input XML into the DOM
                oXMLIn = new FreeThreadedDOMDocument40();
                oXMLIn.loadXML(sInputXML);

                tempParseError = oXMLIn.parseError;
                if (oXMLIn.parseError.errorCode != 0)
                    mobjCommon.InvalidXML(ref tempParseError);

                objDocument = (DOMDocument40)oXMLIn;
                //Extract the input parameters from the input xml
                strParam = "Action";
                sAction = mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam).ToUpper();
                strParam = "ProgramNumber";
                lProgramNumber = Convert.ToInt32(mobjCanadaCommon.getNodeValue(ref objDocument,ref strParam));
                strParam = "CommentText";
                sCommentText = mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam);
                strParam = "CommentType";
                lCommentType = Convert.ToInt32(mobjCanadaCommon.getNodeValue(ref objDocument,ref strParam));
                strParam = "CompletionCommentText";
                sCompletionCommentText = mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam);
                strParam = "CompletionUser";
                sCompletionUser = mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam);
                strParam = "CompletionDate";
                sCompletionDate = mobjCanadaCommon.getNodeValue(ref objDocument,ref strParam);
                strParam = "CreateDate";
                sCreateDate = mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam);
                strParam = "FedTaxID";
                lFedTaxID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam));
                strParam = "User";
                sUser = mobjCanadaCommon.getNodeValue(ref objDocument,ref strParam);


                //Create the insert statement for the so table
                sSetOfTable = "so_tin_Comments";
                sInsertSQL = string.Concat("Insert into session.", sSetOfTable, "(",
                    "action_cd,glass_pgm_no,comment_text, ",
                    "comment_type_no , completion_comment_text, completion_dt, ",
                    "completion_user_id_no , create_dt, federal_tax_id, ",
                    "user_id_no) values ('",
                    sAction, "',", Convert.ToString(lProgramNumber), ",'", sCommentText, "',",
                    lCommentType, ",'", sCompletionCommentText, "','", sCompletionDate, "','",
                    sCompletionUser, "','", sCreateDate, "',", Convert.ToString(lFedTaxID), ",'",
                    sUser, "')");

                //Add the full path to the stylesheet, if there is one
                if (sStylesheet != string.Empty)
                    sStylesheet = string.Concat(lynxAccount4.modCommon.sStylesheetPath, @"\", sStylesheet);

                //execute the stored proc and apply the stylesheet to the results
                retManageTINComments = mobjDataAccess.GetSessionXML(sSetOfTable, "dp_manage_", sSetOfTable, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, "", "*", sStylesheet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                //Cleanup
                oXMLIn = null;
            }
            return retManageTINComments;
        }

        // Private Function val(ByVal vNum As Variant) As Double
        // On Error GoTo errorhandler
        // val = CDbl(vNum)
        // Exit Function
        // errorhandler:
        // val = 0
        // End Function

        public string GetSCForZip(string sInputXML, string sStylesheet = "")
        {
            FreeThreadedDOMDocument40 oXMLIn = null;
            string sZipCode = string.Empty;
            long lFedTaxID = 0;
            string sSetOfTable = string.Empty;
            string sInsertSQL = string.Empty;
            IXMLDOMParseError tempParseError = null;
            DOMDocument40 objDocument = null;
            string strParam = string.Empty;
            int tempVal = 0;
            string retGetSCForZip = string.Empty;
            try
            {
                mobjCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon(); 
                mobjDataAccess = new lynxAccount4.modDataAccess();

                //Read the config file
#if (Developer ==true) 
          //'use the conditional compilation argument "Developer = 1"
          //'when debugging this component from your desktop
           ObjectControl_Activate();
#endif

                //Load the input XML into the DOM
                oXMLIn = new FreeThreadedDOMDocument40();
                oXMLIn.loadXML(sInputXML);
                tempParseError = oXMLIn.parseError;
                if (oXMLIn.parseError.errorCode != 0)
                    mobjCommon.InvalidXML(ref tempParseError);

                //Extract the input parameters from the input xml
                objDocument = (DOMDocument40)oXMLIn;
                strParam = "ZipCode";
                sZipCode = mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam);
                strParam = "FedTaxID";
                lFedTaxID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam));


                //Create the insert statement for the so table
                sSetOfTable = "so_zip_retailer";
                sInsertSQL = string.Concat("Insert into session.", sSetOfTable, "(",
                    "federal_tax_id,zip_cd ",
                    ") values (",
                    Convert.ToString(lFedTaxID), ",'", sZipCode, "')");

                //Add the full path to the stylesheet, if there is one
                if (sStylesheet != string.Empty)
                    sStylesheet = string.Concat(lynxAccount4.modCommon.sStylesheetPath, @"\", sStylesheet);

                //execute the stored proc and apply the stylesheet to the results
                retGetSCForZip = mobjDataAccess.GetSessionXML(sSetOfTable, "dp_get_so_zip_serv_by_retailer", sInsertSQL, lynxAccount4.modCommon.sIngresDSN, "", "*", sStylesheet);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                //Cleanup
                oXMLIn = null;
            }
            return retGetSCForZip;
        }

        private void sendCongratulatoryEmail(string sOutputXML, string sTo, string sSubject)
        {
            DOMDocument40 oXML = null;
            string sBody = string.Empty;
            MailMessage oMail = null;

            try
            {
                mobjCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon(); 
                mobjDataAccess = new lynxAccount4.modDataAccess();

                if (Convert.ToInt32(sTo.Length.ToString().Trim()) < 3)
                    sTo = "lynx_it_web@ppg.com";

                sBody = mobjDataAccess.TransformXML(sOutputXML, string.Concat(lynxAccount4.modCommon.sStylesheetPath, @"\CongratulatoryEmail.xsl"));

                oMail = new MailMessage();
                oMail.From = "participantmanagement@lynxservices.com";
                oMail.To = sTo;
                oMail.Subject = sSubject;
                oMail.Body = sBody;
                oMail.BodyFormat = 0;
                SmtpMail.SmtpServer = "";
                SmtpMail.Send(oMail);

                mobjCommon.LogToAppLog("LynxAGC2.clsInsuranceCo.sendCongratulatoryEmail", string.Concat("Sending email to:", sTo, ",Body=",sBody), "");

                //105     Call LogToAppLog("LynxAGC2.clsInsuranceCo.sendCongratulatoryEmail", "email sent", "")
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oMail = null;
            }
        }

        private string getEmailAddressForUser(string sUserID)
        {
            DOMDocument40 oXML = null;
            string sXML = string.Empty;
            string sEmail = string.Empty;
            int tempVal = 0;
            string retEmailAddressForUser = string.Empty;
            lynxAccount4.clsUsr mobjClsUsr = null;
            try
            {
                mobjClsUsr = new lynxAccount4.clsUsr();

                sXML = mobjClsUsr.GetUsr(Convert.ToString(Convert.ToInt64(Mid(sUserID, 2)))
                    , string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, 0, string.Empty, false, 0);
                if (sXML == string.Empty)
                    sEmail = string.Empty;
                else
                {
                    oXML = new DOMDocument40();
                    oXML.loadXML(sXML);
                    if (oXML.selectSingleNode("//user") == null)
                        sEmail = "lynx_it_web@ppg.com";
                    else
                        sEmail = oXML.selectSingleNode("//user").attributes.getNamedItem("UEMail").text;

                }
                retEmailAddressForUser = sEmail;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXML = null;
            }
            return retEmailAddressForUser;
        }

        public string ManagePricing(string sInputXML, string sStylesheet = "")
        {
            FreeThreadedDOMDocument40 oXMLIn = null;
            DOMDocument40 objDOMDocument = null;
            string sInsertSQL = string.Empty;
            string sSetOfTable = string.Empty;
            string sOutput = string.Empty;
            string sProcName = string.Empty;
            string sFilter = string.Empty;

            string sHistory = string.Empty;
            string sAction = string.Empty;
            string sLevel = string.Empty;
            string sUserID = string.Empty;
            long lIncoUID = 0;
            long lIncoRegionUID = 0;
            string sState = string.Empty;
            string sZip = string.Empty;
            string sCounty = string.Empty;
            string sCurrCountyCode = string.Empty;
            string sCurrEffectiveDate = string.Empty;
            string sCurrExpirationDate = string.Empty;
            string sOldCurrExpirationDate = string.Empty;

            string sFutureCountyCode = string.Empty;
            string sFutureEffectiveDate = string.Empty;
            string sFutureExpirationDate = string.Empty;
            string sOldFutureCountyCode = string.Empty;
            string sOldFutureEffectiveDate = string.Empty;
            string strParam = string.Empty;
            string sOldFutureExpirationDate = string.Empty;
            string sFIPS = string.Empty;
            int sInd = 0;
            int tempVal = 0;
            Command oCmd = null;
            Connection oConnection = null;
            IXMLDOMParseError tempParseError = null;
            object objtemp = null;
            string retManagePricing=string.Empty;
            try
            {
                mobjCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon(); 
                mobjDataAccess = new lynxAccount4.modDataAccess();

                //Read the config file
#if (Developer ==true)
          //use the conditional compilation argument "Developer = 1"
          //when debugging this component from your desktop
           ObjectControl_Activate();
#endif

                //Load the input XML into the DOM
                oXMLIn = new FreeThreadedDOMDocument40();
                oXMLIn.loadXML(sInputXML);
                tempParseError = oXMLIn.parseError;
                if (oXMLIn.parseError.errorCode != 0)
                    mobjCommon.InvalidXML(ref tempParseError);

                //Extract the input parameters from the input xml
                objDOMDocument = (DOMDocument40)oXMLIn;
                strParam = "Action";
                sAction = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam).ToUpper();
                strParam = "Level";
                sLevel = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam).ToUpper();
                strParam = "UserID";
                sUserID = mobjCommon.PreFixUserId(mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam));
                strParam = "State";
                sState = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam).ToUpper();
                strParam = "Zip";
                sZip = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                strParam = "County";
                sCounty = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam).ToUpper();
                strParam = "FIPS";
                sFIPS = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                strParam = "Inco";
                lIncoUID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam));
                strParam = "Region";
                lIncoRegionUID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam));
                strParam = "History";
                sHistory = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                if (sHistory == string.Empty)
                    sHistory = "N";

                if (sAction == "GET")
                {
                    switch (sLevel)
                    {
                        case "ZIP":
                            sSetOfTable = "so_pt_zipcd";
                            sInsertSQL = string.Format("Insert into session.", sSetOfTable, " (",
                              "commit_ind,history_ind,ins_co_uid_no,ins_reg_uid_no,",
                              "zip_cd,county_fips,msg_ind,user_id_no,user_session_no) values ('Y','",
                              sHistory, "',", lIncoUID, ",", lIncoRegionUID, ",'",
                              sZip, "','", sFIPS, "','Y','", sUserID, "','", sUserID, "')");
                            if (sHistory != "Y")
                                sFilter = " 1=1 Order by zip_cd";
                            break;
                        case "STATE":
                            sSetOfTable = "so_pt_county";
                            sInsertSQL = string.Format("Insert into session.", sSetOfTable, " (",
                                "commit_ind,history_ind,ins_co_uid_no,ins_reg_uid_no,",
                                "state_cd,msg_ind,user_id_no,user_session_no) values ('Y','",
                                sHistory, "',", lIncoUID, ",", lIncoRegionUID, ",'",
                                sState, "','Y','", sUserID, "','", sUserID, "')");
                            if (sHistory != "Y")
                                sFilter = " 1=1 Order by county_nm";
                            break;
                        case "COUNTY":
                            sSetOfTable = "so_pt_county";
                            sInsertSQL = string.Format("Insert into session.", sSetOfTable, " (",
                                "commit_ind,history_ind,ins_co_uid_no,ins_reg_uid_no,",
                                "county_fips,msg_ind,user_id_no,user_session_no) values ('Y','",
                                sHistory, "',", lIncoUID, ",", lIncoRegionUID, ",'",
                                sFIPS, "','Y','", sUserID, "','", sUserID, "')");
                            if (sHistory != "Y")
                                sFilter = " 1=1 Order by county_nm";
                            break;
                        default:
                            throw new Exception(string.Format(Convert.ToString(Constants.vbObjectError), ",", "clsInsuranceCo.ManagePricing", ",", "Unknown Level:", sLevel));
                            break;
                    }


                    //Add the full path to the stylesheet, if there is one
                    if (sStylesheet != string.Empty)
                        sStylesheet = string.Format(lynxAccount4.modCommon.sStylesheetPath, @"\", sStylesheet);

                    //execute the stored proc and apply the stylesheet to the results
                    retManagePricing = mobjDataAccess.GetSessionXML(sSetOfTable, string.Format("dp_get_", sSetOfTable), sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, "*", sStylesheet);
                }
                else
                {
                    switch (sLevel)
                    {
                        case "ZIP":
                            sProcName = "dp_manage_pt_zipcd";
                            break;
                        case "COUNTY":
                            sProcName = "dp_manage_pt_county";
                            break;
                        default:
                            throw new Exception(string.Format(Convert.ToString(Constants.vbObjectError), ",", "clsInsuranceCo.ManagePricing", ",", "Unknown Level:", sLevel));
                            break;
                    }

                    strParam = "CurrCountyCode";
                    sCurrCountyCode = mobjCanadaCommon.getNodeValue(ref objDOMDocument,ref strParam);
                    strParam = "CurrEffectiveDate";
                    sCurrEffectiveDate = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                    strParam = "CurrExpirationDate";
                    sCurrExpirationDate = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                    strParam = "OldCurrExpirationDate";
                    sOldCurrExpirationDate = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                    strParam = "FutureCountyCode";

                    sFutureCountyCode = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                    strParam = "FutureEffectiveDate";
                    sFutureEffectiveDate = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                    strParam = "FutureExpirationDate";
                    sFutureExpirationDate = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                    strParam = "OldFutureCountyCode";
                    sOldFutureCountyCode = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                    strParam = "OldFutureEffectiveDate";
                    sOldFutureEffectiveDate = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);
                    strParam = "OldFutureExpirationDate";
                    sOldFutureExpirationDate = mobjCanadaCommon.getNodeValue(ref objDOMDocument, ref strParam);


                    oCmd = new Command();
                    oConnection = new Connection();

                    //setup the connection

                    oConnection.ConnectionTimeout = 60;
                    oConnection.Open(lynxAccount4.modCommon.sIngresDSN);

                    if (sCurrCountyCode != string.Empty)
                    {
                        if (sCurrExpirationDate != sOldCurrExpirationDate)
                        {

                            oCmd.ActiveConnection = oConnection;
                            oCmd.CommandType = CommandTypeEnum.adCmdText;
                            oCmd.CommandTimeout = 90;
                            oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, 1));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 10, sAction));
                            if (sLevel == "COUNTY")
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_county_fips", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 5, sFIPS));

                            oCmd.Parameters.Append(oCmd.CreateParameter("h_county_type_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 5, sCurrCountyCode));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_eff_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, sCurrEffectiveDate));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_exp_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, sCurrExpirationDate));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_ins_co_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, lIncoUID));
                            //.Parameters.Append .CreateParameter("h_ins_co_region_uid_no", adInteger, adParamInput, lIncoRegionUID)
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_orig_county_type_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 5, sCurrCountyCode));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_orig_eff_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, sCurrEffectiveDate));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_orig_exp_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, sOldCurrExpirationDate));
                            if (sLevel == "ZIP")
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_zip_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 5, sZip));

                            oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, DateTime.Now.ToString(@"dd-MM-yyyy hh:mm:ss tt", new CultureInfo("en-US"))));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sUserID));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, sUserID));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));
                            oCmd.CommandText = string.Format("{? = call lynxdba.", sProcName, "(", mobjDataAccess.getQuestionMarks(oCmd.Parameters.Count), ") }");
                            oCmd.Execute(out objtemp);

                            // STEP IV B : Check for Error message returned
                            if (Convert.ToInt32(oCmd.Parameters["retval"]) != 0)
                            {
                                if (Convert.ToInt32(oCmd.Parameters["retval"]) > 0)
                                    sInd = 4;
                                else
                                    sInd = 1;

                                throw new Exception(string.Format(oCmd.Parameters["retval"] + Convert.ToString(Constants.vbObjectError), ",", sProcName, ",", oCmd.Parameters["h_msg_text"]));
                            }
                        }
                    }
                    if (sFutureCountyCode != string.Empty)
                    {
                        if ((sFutureCountyCode != sOldFutureCountyCode) ||
                           (sFutureEffectiveDate != sOldFutureEffectiveDate) ||
                           (sFutureExpirationDate != sOldFutureExpirationDate) ||
                           sAction == "DELETE")
                        {
                            if (sOldFutureCountyCode == string.Empty)
                                sAction = "INSERT";

                            mobjCommon.LogToAppLog("clsInsuranceCo.ManagePricing", string.Format(sProcName, ":", sAction, ":", sInputXML), "");


                            oCmd.ActiveConnection = oConnection;
                            oCmd.CommandType = CommandTypeEnum.adCmdText;
                            oCmd.CommandTimeout = 90;
                            oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, 1));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 10, sAction));
                            if (sLevel == "COUNTY")
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_county_fips", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 5, sFIPS));

                            oCmd.Parameters.Append(oCmd.CreateParameter("h_county_type_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 5, sFutureCountyCode));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_eff_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, sFutureEffectiveDate));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_exp_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, sFutureExpirationDate));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_ins_co_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, lIncoUID));
                            //.Parameters.Append .CreateParameter("h_ins_co_region_uid_no", adInteger, adParamInput, lIncoRegionUID)
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_orig_county_type_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 5, sOldFutureCountyCode));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_orig_eff_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, sOldFutureEffectiveDate));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_orig_exp_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, sOldFutureExpirationDate));
                            if (sLevel == "ZIP")
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_zip_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 5, sZip));

                            oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, DateTime.Now.ToString(@"dd-MM-yyyy hh:mm:ss tt", new CultureInfo("en-US"))));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sUserID));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, sUserID));
                            oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));
                            oCmd.CommandText = string.Format("{? = call lynxdba.", sProcName, "(", mobjDataAccess.getQuestionMarks(oCmd.Parameters.Count), ") }");
                            oCmd.Execute(out objtemp);

                            // STEP IV B : Check for Error message returned
                            if (Convert.ToInt32(oCmd.Parameters["retval"]) != 0)
                            {
                                if (Convert.ToInt32(oCmd.Parameters["retval"]) > 0)
                                    sInd = 4;
                                else
                                    sInd = 1;

                                throw new Exception(string.Format(oCmd.Parameters["retval"] + Convert.ToString(Constants.vbObjectError), ",", sProcName, ",", oCmd.Parameters["h_msg_text"]));
                            }
                        }
                    }

                    sOutput = mobjDataAccess.ParametersToXML(ref oCmd);
                    if (sStylesheet != string.Empty)
                    {
                        sStylesheet = string.Format(lynxAccount4.modCommon.sStylesheetPath, @"\", sStylesheet);
                        retManagePricing = mobjDataAccess.TransformXML(sOutput, sStylesheet);
                    }
                    else
                        retManagePricing = sOutput;
                }

                //Cleaunp
                oXMLIn = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
            return retManagePricing;

        }
    }
}
