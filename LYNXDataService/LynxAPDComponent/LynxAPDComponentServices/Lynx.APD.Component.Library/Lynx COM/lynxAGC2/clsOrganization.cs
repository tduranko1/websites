﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using System.Runtime.InteropServices;
using ADODB;
using System.Xml;
using MSXML2;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    public class clsOrganization
    {
        private ObjectContext moObjectContext = null;
        private string APP_NAME = "lynxAGC2.";
        private string MODULE_NAME = "clsOrganization.";
        modAGC2Common objmodAGC2Common = null;
        lynxAccount4.modCommon mobjCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;
        public string GetOrganizationInfo(long lInstallerUidNo, [Optional]string sStylesheet)
        {
            string sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sInsertSQL = string.Empty,
                sSelectSql = string.Empty,
                sOutput = string.Empty,
                sFilter = string.Empty;
            Recordset oOutputRs = null;
            try
            {
                objmodAGC2Common = new modAGC2Common();
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat("<rec name=\"GetOrganizationInfo\" inst_uid_no=\"", lInstallerUidNo, "\">");

                //' STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_org_info";

                //' STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_get_so_org_info";

                //' STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no)", " values (", lInstallerUidNo, ")");

                //' STEP III (d) : Define the Select Statement to retrieve records
                sSelectSql = "*";

                //' STEP IV : Sort the Results
                sFilter = "";

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (sStylesheet != null)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodAGC2Common.sLogTime = string.Concat(objmodAGC2Common.sLogTime, "</rec>");

                if (lynxAccount4.modCommon.bActivateLog)
                    mobjCommon.LogToAppLog("lynxAGC2.clsOrganization.GetOrganizationInfo", Convert.ToString(objmodAGC2Common.sLogTime), "", 1, 27);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tempParseError = null;
            try
            {
                oXMLDom = new DOMDocument40();
                mobjCommon = new lynxAccount4.modCommon();

                sOutput = mobjCommon.GetConfiguration("lynxAGC2");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);

                if (oXMLDom.parseError.errorCode == 0)
                {
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    lynxAccount4.modCommon.bActivateLog = Convert.ToBoolean(oXMLDom.selectSingleNode("//ActivateLog").text);
                }
                else
                    tempParseError = oXMLDom.parseError;
                mobjCommon.InvalidXML(ref tempParseError);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        private void ObjectControl_Deactivate()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
