﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MSXML2;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    public class modCanadaCommon
    {
        lynxAccount4.modCommon mobjCommon = null;
        public FreeThreadedDOMDocument40 GetXMLFromCache()
        {
            FreeThreadedDOMDocument40 oXMLDom = null;
            string sXMLPath = string.Empty;
            IXMLDOMParseError tempParseError = null;
            try
            {

                if (oXMLDom == null)
                {
                    sXMLPath = getXMLPath();
                    oXMLDom = new FreeThreadedDOMDocument40();
                    oXMLDom.load(string.Concat(sXMLPath, "lookup.xml"));
                    if (oXMLDom.parseError.errorCode != 0)
                    {
                        tempParseError = oXMLDom.parseError;
                        mobjCommon.InvalidXML(ref tempParseError);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
            return oXMLDom;
        }

        public string GetItemFromCache(string sItemName, [Optional]string sRoot = "root")
        {
            FreeThreadedDOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode;
            string soutPut = string.Empty;
            try
            {
                oXMLDom = GetXMLFromCache();
                oXMLNode = oXMLDom.selectSingleNode(string.Concat("//ObjectGroup[@ObjectGroupName='", sItemName, "']"));
                soutPut = string.Concat("<", sRoot, ">", oXMLNode.xml, "</", sRoot, ">");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
            return soutPut;
        }

        public void PopulateListBox(ref FreeThreadedDOMDocument40 oXML, ref IXMLDOMNode oAGC2ListBox, IXMLDOMNode oNewNode)
        {
            string sListBoxType = string.Empty,
                   sValue = string.Empty;
            FreeThreadedDOMDocument40 oCacheXML = null;
            IXMLDOMNode oOptionNode = null;
            IXMLDOMParseError tempParseError = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                foreach (IXMLDOMAttribute oAttribute in oAGC2ListBox.attributes)
                {
                    if (oAttribute.name == "type")
                        sListBoxType = Convert.ToString(oAttribute.value);
                    else if (oAttribute.name == "value")
                        sValue = Convert.ToString(oAttribute.value);
                    else
                        oNewNode.attributes.setNamedItem(oAttribute.cloneNode(true));
                }

                foreach (IXMLDOMNode oChildNode in oAGC2ListBox.attributes)
                    oNewNode.appendChild(oChildNode);

                if (sListBoxType != "")
                {
                    oCacheXML = new FreeThreadedDOMDocument40();
                    oCacheXML.loadXML(GetItemFromCache(sListBoxType));
                    if (oXML.parseError.errorCode != 0)
                    {
                        tempParseError = oXML.parseError;
                        mobjCommon.InvalidXML(ref tempParseError);
                    }

                    foreach (IXMLDOMNode oChildNode in oCacheXML.selectNodes("//Object"))
                    {
                        if (oChildNode.attributes.getNamedItem("StatusID").text == "1")
                        {
                            oOptionNode = oXML.createElement("option");
                            oOptionNode.text = oChildNode.attributes.getNamedItem("ObjectValue").text;
                            oOptionNode.attributes.setNamedItem(oXML.createAttribute("value")).text = oChildNode.attributes.getNamedItem("ObjectCode").text;
                            oNewNode.appendChild(oOptionNode);
                        }
                    }
                }

                foreach (IXMLDOMNode tmpoOptionNode in oNewNode.selectNodes("option"))
                {
                    if (tmpoOptionNode.attributes.getNamedItem("value").text == sValue)
                        tmpoOptionNode.attributes.setNamedItem(oXML.createAttribute("selected")).text = "Y";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string applyListBoxes(string sXML)
        {
            FreeThreadedDOMDocument40 oXML = null;
            IXMLDOMNodeList oAGC2ListBoxes = null;
            IXMLDOMNode oListBox = null;
            IXMLDOMNode tmpoAGC2ListBox = null;
            string sOutPut = string.Empty;
            IXMLDOMParseError tempParseError = null;
            try
            {
                oXML = new FreeThreadedDOMDocument40();
                mobjCommon = new lynxAccount4.modCommon();
                oXML.loadXML(sXML);
                if (oXML.parseError.errorCode != 0)
                {
                    tempParseError = oXML.parseError;
                    mobjCommon.InvalidXML(ref tempParseError);
                }

                oAGC2ListBoxes = oXML.selectNodes("//agc2ListBox");
                foreach (IXMLDOMNode oAGC2ListBox in oAGC2ListBoxes)
                {
                    oListBox = oAGC2ListBox.parentNode.appendChild(oXML.createElement("select"));
                    tmpoAGC2ListBox = oAGC2ListBox;
                    PopulateListBox(ref oXML, ref tmpoAGC2ListBox, oListBox);
                    oAGC2ListBox.parentNode.removeChild(oAGC2ListBox);
                }
                sOutPut = oXML.xml;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return sOutPut;
        }

        public string getXMLPath()
        {
            string sOutput = string.Empty;
            string strgetXMLPath = string.Empty;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tempParseError = null;
            try
            {
                oXMLDom = new DOMDocument40();
                mobjCommon = new lynxAccount4.modCommon();
                sOutput = mobjCommon.GetConfiguration("lynxMETRYXCanada");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);
                if (oXMLDom.parseError.errorCode == 0)
                    strgetXMLPath = oXMLDom.selectSingleNode("//Cache").text;
                else
                {
                    tempParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tempParseError);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
            return strgetXMLPath;
        }

        public string getNodeValue(ref DOMDocument40 xmldom, ref string sNodeName)
        {
            string strNodeValue = string.Empty;
            try
            {
                if (xmldom.selectSingleNode(string.Concat("//", sNodeName)) == null)
                    strNodeValue = "";
                else
                    strNodeValue = xmldom.selectSingleNode(string.Concat("//", sNodeName)).text;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strNodeValue;
        }

    }
}
