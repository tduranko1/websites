﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using COMSVCSLib;
using MSXML2;
using System.Runtime.InteropServices;
using ADODB;
using Microsoft.VisualBasic;
using System.Globalization;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    //Description:
    // NOTE: this component uses a conditional compilation
    //        argument called "Developer". This argument must
    //        be set as follows:

    //        Developer = 0 (running from the com+ server)
    //        Developer = 1 (running from developer desktop)

    class clsContacts
    {
        lynxAccount4.modCommon mobjCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;
        modCanadaCommon mobjCanadaCommon = null;

        private ObjectContext moObjectContext = null;
        private bool bLogError = false;
        //Define the Application name to be used in Error Handling
        private const string APP_NAME = "lynxAGC2.";
        private const string MODULE_NAME = "clsContacts.";

        public string Left(string strParam, int intLength)
        {
            string strResult = strParam.Substring(0, intLength);
            return strResult;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string strParam, int intLength)
        {
            string strResult = strParam.Substring(strParam.Length - intLength, intLength);
            return strResult;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = strParam.Substring(startIndex, intLength);
            return strResult;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = strParam.Substring(intstartIndex);
            return strResult;
        }

        private bool ObjectControl_CanBePooled()
        {
            bool retObjectControl = false;
            try
            {
                retObjectControl = true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return retObjectControl;
        }

        private void ObjectControl_Activate()
        {

            string sOutput = string.Empty;
            //xml objects
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tempDom = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();

                sOutput = mobjCommon.GetConfiguration("lynxAGC2");
                oXMLDom = new DOMDocument40();
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);
                if (oXMLDom.parseError.errorCode == 0)
                {
                    //retrieve the values here
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    lynxAccount4.modCommon.bActivateLog = Convert.ToBoolean(oXMLDom.selectSingleNode("//ActivateLog").text);
                }
                else
                {
                    tempDom = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tempDom);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        //Get contacts by calling dp_get_so_contacts
        public string GetContacts(string sActionCode,
                                    long lID,
                                    string sIDType,
                                [Optional] string sStylesheet)
        {
            string sStoredProc = string.Empty;
            string sTempTable = string.Empty;
            string sInsertSQL = string.Empty;
            string sSelectSql = string.Empty;
            string sOutput = string.Empty;
            string sFilter = string.Empty;
            Recordset oOutputRs = null;
            string sContactType = string.Empty;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                // STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_contacts";

                // STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_get_so_contacts";

                // STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (action_cd,inst_uid_no,",
                                "inst_uid_type,commit_ind,msg_ind,user_id_no,user_session_no)",
                                " values ('", sActionCode, "',", Convert.ToString(lID), ",'", sIDType, "',",
                                "'Y','N','','')");

                // STEP III (d) : Define the Select Statement to retrieve records
                if (sActionCode == "ALL")
                {
                    sSelectSql = " * ";
                    sSelectSql = " contact_uid_no,first_nm,last_nm,email_addr,title_desc,min(contact_type_no) as contact_type_no ";
                }
                else
                    sSelectSql = " * ";

                // STEP IV : Sort the Results
                if (sActionCode == "ALL")
                {
                    switch (sIDType)
                    {
                        case "ORG_ID":
                            sContactType = "ORGANIZATION";
                            break;
                        case "CO_ID":
                            sContactType = "COMPANY";
                            break;
                        case "SC_ID":
                            sContactType = "SERVICE CENTER";
                            break;
                    }

                    sFilter = string.Concat(" contact_uid_no not in(",
                        "select contact_uid_no from session.so_contacts ",
                       " where action_cd = '", sContactType, "' ",
                       "   and inst_uid_no = '", Convert.ToString(lID), "') ",
                       " Group by contact_uid_no,first_nm,last_nm,email_addr,title_desc",
                        " ORDER BY last_nm,first_nm");
                }
                else
                    sFilter = "";

                // STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);

                // STEP VI : Transform the XML using a XSL Stylesheet
                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        //Get contact phones by calling dp_get_so_contact_phone
        public string GetContactPhones(long lContactID, [Optional] string sStylesheet)
        {

            string sStoredProc = string.Empty;
            string sTempTable = string.Empty;
            string sInsertSQL = string.Empty;
            string sSelectSql = string.Empty;
            string sOutput = string.Empty;
            string sFilter = string.Empty;
            Recordset oOutputRs = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                // STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_contact_phone";

                // STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_get_so_contact_phone";

                // STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (contact_uid_no,",
                                "commit_ind,msg_ind,user_id_no,user_session_no)",
                                " values (", Convert.ToString(lContactID), ",'Y','N','lynxweb','lynxweb')");

                // STEP III (d) : Define the Select Statement to retrieve records
                sSelectSql = "*";

                // STEP IV : Sort the Results
                sFilter = string.Empty;

                // STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true);

                // STEP VI : Transform the XML using a XSL Stylesheet
                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        //Takes contactXML as input calls manageContact and manageContactPhone accordingly
        public void ModifyContact(string sContactXML)
        {
            FreeThreadedDOMDocument40 oXML = null;
            DOMDocument40 objDocument = null;
            IXMLDOMNodeList oPhones = null;
            IXMLDOMNode oPhone = null;
            string sOldPhone = string.Empty;
            string sPhoneNumber = string.Empty;
            string sPhoneExt = string.Empty;
            long lPhoneType = 0;
            long lContactID = 0;
            string sResult = string.Empty;
            string sActionCode = string.Empty;
            string sTollFreeCd = string.Empty;
            string sUserID = string.Empty;
            string strParam = string.Empty;
            bool blnProcessPhone = false;
            int tempVal = 0;
            try
            {
                oXML = new FreeThreadedDOMDocument40();
                mobjCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon();

                oXML.loadXML(sContactXML);

                strParam = "ContactID";
                objDocument=(DOMDocument40)oXML;
                lContactID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam));
                strParam = "UserID";
                sUserID = mobjCommon.PreFixUserId(mobjCanadaCommon.getNodeValue(ref objDocument,ref strParam));
                strParam = "Action";
                sActionCode = mobjCanadaCommon.getNodeValue(ref objDocument, ref strParam);
                if (sActionCode == string.Empty)
                {
                    if (lContactID == 0)
                        sActionCode = "INSERT";
                    else
                        sActionCode = "UPDATE";
                }

                sResult = ManageContact(sActionCode, oXML.xml, "");

                oPhones = oXML.selectNodes("//Phone");
                foreach (IXMLDOMNode tempphone in oPhones)
                {
                    oPhone = tempphone;
                    sOldPhone = oPhone.attributes.getNamedItem("OldPhone").text;
                    if (oPhone.attributes.getNamedItem("Delete").text == "1")
                        sActionCode = "DELETE";
                    else
                    {
                        if ((sOldPhone == string.Empty) || (sOldPhone == "0"))
                            sActionCode = "INSERT";
                        else
                            sActionCode = "UPDATE";
                    }
                    sPhoneNumber = oPhone.attributes.getNamedItem("Number").text;
                    sPhoneExt = oPhone.attributes.getNamedItem("Ext").text;
                    lPhoneType = Convert.ToInt64(int.TryParse(oPhone.attributes.getNamedItem("Type").text, out tempVal));
                    sTollFreeCd = oPhone.attributes.getNamedItem("TollFree").text;

                    switch (sActionCode)
                    {
                        case "INSERT":
                            sResult = ManageContactPhone(sActionCode, lContactID, sPhoneNumber, sPhoneExt, lPhoneType, sTollFreeCd, sUserID);
                            break;
                        case "DELETE":
                            //If the old phone is blank, no need to call the delete.  this represents a delete of a phone number
                            //that was never actually inserted into the database
                            if (sOldPhone.Length >= 10)
                                sResult = ManageContactPhone(sActionCode, lContactID, Left(sOldPhone, 10), Mid(sOldPhone, 11, sOldPhone.Length - 11), Convert.ToInt64(Right(sOldPhone, 1)), sTollFreeCd, sUserID);
                            break;
                        case "UPDATE":
                            //If the phone hasn't change, no need to update
                            if (sOldPhone != string.Concat(sPhoneNumber, sPhoneExt, Convert.ToString(lPhoneType)))
                            {
                                //If the phone is changed, process an update by doing a delete of the old phone
                                //followed by an an insert of the new phone
                                sActionCode = "DELETE";
                                sResult = ManageContactPhone(sActionCode, lContactID, Left(sOldPhone, 10), Mid(sOldPhone, 11, sOldPhone.Length - 11), Convert.ToInt64(Right(sOldPhone, 1)), sTollFreeCd, sUserID);
                                sActionCode = "INSERT";
                                sResult = ManageContactPhone(sActionCode, lContactID, sPhoneNumber, sPhoneExt, lPhoneType, sTollFreeCd, sUserID);
                            }
                            break;
                        default:
                            blnProcessPhone = false;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //Looks in the contact list to make sure the contact you are deleting is not the primary contact
        //If it is, canDeleteContact=False, else, canDeleteContact=True
        public bool canDeleteContact(long lContactID, long lInstallerID, string sInstallerIDType)
        {
            string sStoredProc = string.Empty;
            string sTempTable = string.Empty;
            string sInsertSQL = string.Empty;
            string sSelectSql = string.Empty;
            string sOutput = string.Empty;
            string sFilter = string.Empty;
            Recordset oOutputRs = null;
            string sContactType = string.Empty;
            string sManageType = string.Empty;
            bool retcanDeleteContact = false;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();

                switch (sInstallerIDType)
                {
                    case "ORG_ID":
                        sManageType = "ORGANIZATION";
                        break;
                    case "CO_ID":
                        sManageType = "COMPANY";
                        break;
                    default:
                        sManageType = "SERVICE CENTER";
                        break;
                }


                // STEP III (a) : Define the Temporary Session Table Name
                sTempTable = "so_contacts";

                // STEP III (b) : Define the Database Procedure Name
                sStoredProc = "dp_get_so_contacts";

                // STEP III (c) : Define the Seed Row to be inserted in the Temporary Session Table
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (action_cd,inst_uid_no,",
                                "inst_uid_type,commit_ind,msg_ind,user_id_no,user_session_no)",
                                " values ('", sManageType, "',", Convert.ToString(lInstallerID), ",'", sInstallerIDType, "',",
                                "'Y','N','','')");

                // STEP III (d) : Define the Select Statement to retrieve records
                sSelectSql = " * ";

                sFilter = string.Concat(" primary_contact_ind='Y' and contact_uid_no=", Convert.ToString(lContactID));

                // STEP V : Depending on User Choice return either a Recordset or XML string
                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (oOutputRs.EOF)
                    retcanDeleteContact = true;
                else
                    retcanDeleteContact = false;
            }
            catch (Exception)
            {

                throw;
            }
            return retcanDeleteContact;
        }

        //Deletes a contact by calling manage contact
        public void DeleteContact(long lContactID, long lInstallerID, string sInstallerIDType, string sUserID)
        {
            string sResult = string.Empty;
            try
            {
                sResult = ManageContact("DELETE", string.Concat("<Contact>",
                     "<ContactID>", Convert.ToString(lContactID), "</ContactID>",
                     "<InstallerID>", Convert.ToString(lInstallerID), "</InstallerID>",
                     "<InstallerIDType>", sInstallerIDType, "</InstallerIDType>",
                     "<UserID>", sUserID, "</UserID>",
                     "</Contact>"));

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //Deletes a contact phone by valling manageContactPhone
        //This function has been deprecated.  It has only been left in place to preserve
        //binary comptability.  Do not use!!!
        public void DeletePhone(long lContactID, long lPhoneID, string sUserID)
        {
            string sResult = string.Empty;
            try
            {
                throw new Exception(string.Concat(Constants.vbObjectError, ",", "clsContacts.DeletePhone", ",", "This method has been deprecated.  Please use ModifyContact instead."));
                //sResult = ManageContactPhone("DELETE", lContactID, lPhoneID, "", "", 0, "", sUserID)
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        //Gets a particular contact by calling ManageContact
        public string getContact(long lContactID, long lInstallerID, string sInstallerIDType, string sStylesheet = "")
        {
            string retcontact = string.Empty;
            try
            {
                retcontact = ManageContact("GET", string.Concat("<Contact>",
                                 "<ContactID>", Convert.ToString(lContactID), "</ContactID>",
                                 "<InstallerID>", Convert.ToString(lInstallerID), "</InstallerID>",
                                 "<InstallerIDType>", sInstallerIDType, "</InstallerIDType>",
                                 "</Contact>"), sStylesheet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return retcontact;
        }

        //Calls dp_manage_retailer_contacts
        //Called by modifyContact,getContact
        private string ManageContact(string sActionCode, string sParameterXml, string sStylesheet = "")
        {
            Command oCmd = null;
            Connection oConnection = null;
            string sStoredProc = string.Empty;
            string sOutput = string.Empty;

            DOMDocument40 oXMLDom = null;
            IXMLDOMElement oRoot = null;
            IXMLDOMNode oNode = null;

            long lContactID = 0;
            long lInstallerID = 0;
            string sInstallerIDType = string.Empty;
            string sEmail = string.Empty;
            string sFirstName = string.Empty;
            string sLastName = string.Empty;
            long lContactTypeID = 0;
            string sContactTypeInd = string.Empty;
            string sContactTypeDesc = string.Empty;
            string sTitle = string.Empty;
            string sPrimaryContactInd = string.Empty;
            long lModifyContactID = 0;
            string sUserID = string.Empty;
            string sStatsLog = string.Empty;
            string strParam = string.Empty;
            int sInd = 0;
            int tempVal = 0;
            object tempobj = null;
            IXMLDOMParseError tempParseError = null;
            try
            {
                mobjCanadaCommon = new modCanadaCommon();
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom = new DOMDocument40();

                //Load the Main XML in the DOM
                oXMLDom.async = false;
                oXMLDom.loadXML(sParameterXml);
                tempParseError = oXMLDom.parseError;
                if (oXMLDom.parseError.errorCode != 0)
                    mobjCommon.InvalidXML(ref tempParseError);

                strParam = "InstallerID";
                lInstallerID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref oXMLDom, ref strParam));
                strParam = "ContactID";
                lContactID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref oXMLDom, ref strParam));
                strParam = "InstallerIDType";
                sInstallerIDType = mobjCanadaCommon.getNodeValue(ref oXMLDom, ref strParam);
                strParam = "Email";
                sEmail = mobjCanadaCommon.getNodeValue(ref oXMLDom, ref strParam);
                strParam = "FirstName";
                sFirstName = mobjCanadaCommon.getNodeValue(ref oXMLDom, ref strParam);
                strParam = "LastName";
                sLastName = mobjCanadaCommon.getNodeValue(ref oXMLDom, ref strParam);
                strParam = "ContactTypeID";
                lContactTypeID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref oXMLDom, ref strParam));
                strParam = "ContactTypeInd";
                sContactTypeInd = mobjCanadaCommon.getNodeValue(ref oXMLDom,ref strParam);
                strParam = "Title";
                sTitle = mobjCanadaCommon.getNodeValue(ref oXMLDom,ref strParam);
                strParam = "PrimaryContactInd";
                sPrimaryContactInd = mobjCanadaCommon.getNodeValue(ref oXMLDom, ref strParam);
                strParam = "ModifyContactID";
                lModifyContactID = Convert.ToInt64(mobjCanadaCommon.getNodeValue(ref oXMLDom,ref strParam));
                if (lModifyContactID == 0)
                    lModifyContactID = lContactID;

                strParam = "UserID";
                sUserID = mobjCommon.PreFixUserId(mobjCanadaCommon.getNodeValue(ref oXMLDom, ref strParam));
                if ((sActionCode == "UPDATE" || (sActionCode == "INSERT" && lContactID == 0)) && (sLastName == "" || sFirstName == "" || sEmail == "" || sTitle == ""))
                {
                    bLogError = false;
                    throw new Exception(string.Concat(Constants.vbObjectError - 1, ",", "ManageContact(Validate)", ",", "All contact fields are required.  Please enter all fields."));
                }

                sStoredProc = "dp_manage_retailer_contacts";
                oCmd = new Command();
                oConnection = new Connection();

                //setup the connection

                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);


                oCmd.ActiveConnection = oConnection;
                oCmd.CommandType = CommandTypeEnum.adCmdText;
                oCmd.CommandTimeout = 90;

                oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, -1));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, sActionCode));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_contact_type_no", DataTypeEnum.adSmallInt, ParameterDirectionEnum.adParamInputOutput, 0, lContactTypeID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_contact_type_desc", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, sContactTypeDesc));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_contact_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lContactID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_email_addr", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 50, sEmail));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_first_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, sFirstName));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lInstallerID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_uid_type", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sInstallerIDType));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_last_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 20, sLastName));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, DateTime.Now.ToString(@"dd-MM-yyyy hh:mm:ss tt", new CultureInfo("en-US"))));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 7, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_type_no", DataTypeEnum.adSmallInt, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_first_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_last_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_primary_contact_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, sPrimaryContactInd));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_title_desc", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, sTitle));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, "N"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sUserID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sUserID));
                oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(", mobjDataAccess.getQuestionMarks(oCmd.Parameters.Count), ") }");
                oCmd.Execute(out tempobj);

                // STEP IV B : Check for Error message returned
                if (Convert.ToInt32(oCmd.Parameters["retval"]) > 0)
                {
                    bLogError = true;
                    sInd = 4;
                    throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError, ",", sStoredProc, ",", oCmd.Parameters["h_msg_text"]));
                }
                else if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0)
                {
                    bLogError = false;
                    sInd = 1;
                    throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError, ",", sStoredProc, ",", oCmd.Parameters["h_msg_text"]));
                }

                sOutput = mobjDataAccess.ParametersToXML(ref oCmd, true," ", "Contact");

                // STEP V : Transform the XML using a XSL Stylesheet
                //if a Stylesheet is specified then we need to transform the XML using that XSL Stylesheet
                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCmd = null;
            }
            return sOutput;
        }

        // Calls dp_manage_contact_phone to manage a contact phone
        // Called by modify contact
        private string ManageContactPhone(string sActionCode,
                                    long lContactID,
                                    string sPhoneNumber,
                                    string sPhoneExt,
                                    long lPhoneType,
                                    string sTollFreeCd,
                                    string sUserID,
                                    string sStylesheet="")
        {


            Command oCmd = null;
            Connection oConnection = null;
            string sStoredProc = string.Empty;
            string sOutput = string.Empty;
            string sStatsLog = string.Empty;
            int sInd = 0;
            int tempVal = 0;
            object objtemp = null;

            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();

                sUserID = mobjCommon.PreFixUserId(sUserID);
                sStoredProc = "dp_manage_contact_phone";
                sStatsLog = string.Concat(sStoredProc, ";", Convert.ToString(DateTime.Now));
                oCmd = new Command();
                oConnection = new Connection();

                //setup the connection
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);

                sStatsLog = string.Concat(sStatsLog, ";", Convert.ToString(DateTime.Now));


                oCmd.ActiveConnection = oConnection;
                oCmd.CommandType = CommandTypeEnum.adCmdText;
                oCmd.CommandTimeout = 90;
                oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, -1));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sActionCode));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_area_code_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, int.TryParse(Left(sPhoneNumber, 3), out tempVal)));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_contact_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lContactID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_exchange_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, int.TryParse(Mid(sPhoneNumber, 4, 3), out tempVal)));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_household_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, int.TryParse(Right(sPhoneNumber, 4), out tempVal)));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_dt", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 25, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 7, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_type_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, 0));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_first_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_last_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, ""));
                //270     .Parameters.Append .CreateParameter("h_phone_uid_no", adInteger, adParamInputOutput, , lPhoneID)
                oCmd.Parameters.Append(oCmd.CreateParameter("h_phone_type_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lPhoneType));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_type_desc", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_phone_ext_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 5, sPhoneExt));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_toll_free_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, sTollFreeCd));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_toll_free_desc", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, "Y"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sUserID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sUserID));


                oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(", mobjDataAccess.getQuestionMarks(oCmd.Parameters.Count), ") }");

                oCmd.Execute(out objtemp);
                //sStatsLog = sStatsLog & ";;;" & CStr(Now)
                //Call LogToAppLog("stats", sStatsLog, "")

                //STEP IV B : Check for Error message returned
                if (Convert.ToInt32(oCmd.Parameters["retval"]) > 0)
                {
                    bLogError = true;
                    sInd = 4;
                    throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError, ",", sStoredProc, ",", oCmd.Parameters["h_msg_text"]));
                }
                else if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0)
                {
                    bLogError = false;
                    sInd = 1;
                    throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError, ",", sStoredProc, ",", oCmd.Parameters["h_msg_text"]));
                }

                sOutput = mobjDataAccess.ParametersToXML(ref oCmd, true," ", "Contact");
                //Call LogToAppLog("clsContacts.ManageContactPhone", _
                //sOutput, "")

                // STEP V : Transform the XML using a XSL Stylesheet
                //if a Stylesheet is specified then we need to transform the XML using that XSL Stylesheet
                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCmd = null;
            }
            return sOutput;
        }

        //        This function calls modDataAccess to transform the xml,
        //and then applies transformations for <agc2ListBox> objects

        private string TransformXML(string sXML, string sStylesheetPath)
        {
            try
            {
                mobjCanadaCommon = new modCanadaCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();

                if (sStylesheetPath != string.Empty)
                    sXML = mobjDataAccess.TransformXML(sXML, sStylesheetPath);
                if (sXML.IndexOf("<agc2ListBox") > 0)
                    sXML = mobjCanadaCommon.applyListBoxes(sXML);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return sXML;
        }
    }
}
