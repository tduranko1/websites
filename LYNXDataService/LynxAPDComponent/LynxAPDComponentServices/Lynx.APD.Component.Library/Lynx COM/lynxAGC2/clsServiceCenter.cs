﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Objects;
using System.Runtime.InteropServices;
using MSXML2;
using ADODB;
using Microsoft.VisualBasic;
using System.Globalization;

namespace Lynx.APD.Component.Library.Lynx_COM.lynxAGC2
{
    public class clsServiceCenter
    {
        private ObjectContext moObjectContext = null;
        private string APP_NAME = "lynxAGC2";
        private string MODULE_NAME = "clsServiceCenter.";
        private string sXMLPath = string.Empty;
        private string sSessionID = string.Empty;
        modAGC2Common objmodcommon = null;
        lynxAccount4.modCommon mobjCommon = null;
        lynxAccount4.modDataAccess mobjDataAccess = null;

        public string GetServiceCenterInfo(string sRequestXML, string sStylesheet = "")
        {
            string sOutput = string.Empty,
                sServiceTypeXML = string.Empty,
                sOperatingHoursXML = string.Empty,
                sHeaderInfoXML = string.Empty,
                sVehicleInfoXML = string.Empty,
                sProfileItemsXML = string.Empty,
                sUserID = string.Empty,
                sTechniciansXML = string.Empty,
                strtmparg = string.Empty,
                strtmparg2 = string.Empty,
                strtmparg3 = string.Empty,
                strParam = string.Empty,
                sPhoneXML = string.Empty;
            long lInstallerUidNo;
            DOMDocument40 objDomdocumet = null;
            FreeThreadedDOMDocument40 oXMLIn = null;
            IXMLDOMParseError tmpParseError = null;
            objmodcommon = new modAGC2Common();
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLIn = new FreeThreadedDOMDocument40();
                oXMLIn.loadXML(sRequestXML);

                if (oXMLIn.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLIn.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                objDomdocumet = (DOMDocument40)oXMLIn;
                strParam = "InstallerID";
                lInstallerUidNo = Convert.ToInt64(objmodcommon.getNodeValue(ref objDomdocumet, ref strParam));
                strParam = "UserID";
                sUserID = objmodcommon.getNodeValue(ref objDomdocumet, ref strParam);
                strParam = "UserSession";
                sSessionID = objmodcommon.getNodeValue(ref objDomdocumet, ref strParam);

                if (lynxAccount4.modCommon.bActivateLog)
                {
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, string.Format("<rec name=\"GetServiceCenterInfo\" inst_uid_no=\"{0}\">", lInstallerUidNo));
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, string.Format("<detail function=\"****GetServiceType\" type=\"begin\" time=\"{0}\"/>", DateTime.Now));
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetServiceType\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetOperatingHours\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetOperatingHours\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetHeaderInfo\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetHeaderInfo\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetPhoneNumbers\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetPhoneNumbers\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetServiceVehicles\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetServiceVehicles\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetServiceCenterProfileItems\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetServiceCenterProfileItems\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetTechnicians\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                }

                sServiceTypeXML = GetServiceType(lInstallerUidNo);
                sOperatingHoursXML = GetOperatingHours(lInstallerUidNo);
                sHeaderInfoXML = GetHeaderInfo(lInstallerUidNo, sUserID);
                sPhoneXML = GetDetails(lInstallerUidNo, "so_service_center_phone", "dp_get_so_service_center_phone","1=1","*","phoneNumbers");
                sVehicleInfoXML = GetDetails(lInstallerUidNo, "so_service_vehicles", "dp_get_so_service_vehicles", " ", "*", "servicevehicles");
                sProfileItemsXML = GetServiceCenterProfileItems(lInstallerUidNo, "", sUserID);
                sRequestXML = sRequestXML.Replace("</GetServiceCenter>", string.Concat(sHeaderInfoXML, "</GetServiceCenter>"));
                sTechniciansXML = GetTechnicians(lInstallerUidNo, sRequestXML, sUserID);

                if (lynxAccount4.modCommon.bActivateLog)
                {
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"****GetTechnicians\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "</rec>");
                    mobjCommon.LogToAppLog("lynxAGC2.clsServiceCenter.GetServiceCenterInfo", objmodcommon.sLogTime, "", 1, 27);
                }

                // Combine calls into one XML to return
                sOutput = string.Concat("<data>", sHeaderInfoXML, sServiceTypeXML, sOperatingHoursXML, sVehicleInfoXML, sProfileItemsXML, sPhoneXML, sTechniciansXML, "</data>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sOutput;
        }

        public string GetServiceType(long lInstallerUidNo, string sStylesheet = "")
        {
            string cACTION_CODE = "GET";
            string sStoredProc = string.Empty,
               sTempTable = string.Empty,
               sInsertSQL = string.Empty,
               sSelectSql = string.Empty,
               sOutput = string.Empty,
               sFilter = string.Empty,
               sMaxDate = string.Empty,
               sModifyFirst = string.Empty,
               sModifyLast = string.Empty;
            Recordset oOutputRs = null;
            try
            {
                objmodcommon = new modAGC2Common();
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                sTempTable = "so_service_type";
                sStoredProc = "dp_manage_so_service_type";
                sInsertSQL = string.Format("insert into session.{0} (inst_uid_no, action_cd) values ({1},'{2}')", sTempTable, lInstallerUidNo, cACTION_CODE);
                sSelectSql = "*";
                sFilter = "1=1 order by seq_no";

                if (lynxAccount4.modCommon.bActivateLog)
                {
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                }

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "servicetype");
                oOutputRs.Sort = "modify_dt DESC";
                oOutputRs.MoveFirst();
                sMaxDate = Convert.ToString(oOutputRs.Fields["modify_dt"].Value);
                sModifyFirst = Convert.ToString(oOutputRs.Fields["modify_user_first_nm"].Value);
                sModifyLast = Convert.ToString(oOutputRs.Fields["modify_user_last_nm"].Value);
                sOutput = sOutput.Replace("</servicetype>", string.Concat("<lastchange><date>", sMaxDate, "</date><firstname>", sModifyFirst.Trim(), "</firstname><lastname>", sModifyLast.Trim(), "</lastname></lastchange></servicetype>"));

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string GetOperatingHours(long lInstallerUidNo, string sStylesheet = "")
        {
            string cACTION_CODE = "GET";
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sInsertSQL = string.Empty,
                    sSelectSql = string.Empty,
                    sOutput = string.Empty,
                    sFilter = string.Empty;
            Recordset oOutputRs = null;
            try
            {
                sTempTable = "so_manage_operating_hours";
                sStoredProc = "dp_manage_so_operating_hours";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no, action_cd)",
                              " values (", lInstallerUidNo, ",'", cACTION_CODE, "')");
                sSelectSql = "*";
                sFilter = string.Empty;

                if (lynxAccount4.modCommon.bActivateLog)
                {
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                }

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "operatinghours");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string GetHeaderInfo(long lInstallerUidNo, string sUserID, string sStylesheet = "")
        {
            string cACTION_CODE = "GET";
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty;
            Recordset oOutputRs = null;
            try
            {
                sTempTable = "so_service_center";
                sStoredProc = "dp_manage_so_service_center";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no, action_cd, user_id_no)",
                                          " values (", lInstallerUidNo, ",'", cACTION_CODE, "', '", sUserID, "')");
                sSelectSql = "*";
                sFilter = "1=1";
                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "headerInfo");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        /// <summary>
        /// This method is used to replace the methods of GetPhonenumbers() and GetServiceVehicles().
        /// </summary>
        /// <param name="lInstallerUidNo"></param>
        /// <param name="sTempTable"></param>
        /// <param name="sStoredProc"></param>
        /// <param name="sFilter"></param>
        /// <param name="sSelectSql"></param>
        /// <param name="method">servicevehicles,phoneNumbers</param>
        /// <param name="sStylesheet"></param>
        /// <returns></returns>
        public string GetDetails(long lInstallerUidNo, string sTempTable, string sStoredProc, string sFilter, string sSelectSql, string method, [Optional]string sStylesheet = "")
        {
            string cACTION_CODE = "GET";
            string sInsertSQL = string.Empty,
                     sOutput = string.Empty;

            Recordset oOutputRs = null;
            try
            {
                //sTempTable = "so_service_center_phone";
                //sStoredProc = "dp_get_so_service_center_phone";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no)",
                              " values (", lInstallerUidNo, ")");
                sSelectSql = "*";
                sFilter = "1=1";
                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, method);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        #region "Dump Methods commented"
        //public string GetPhoneNumbers(long lInstallerUidNo, [Optional]string sStylesheet = "")
        //{
        //    string cACTION_CODE = "GET";
        //    string sStoredProc = string.Empty,
        //             sTempTable = string.Empty,
        //             sInsertSQL = string.Empty,
        //             sSelectSql = string.Empty,
        //             sOutput = string.Empty,
        //             sFilter = string.Empty;
        //    Recordset oOutputRs = null;
        //    try
        //    {
        //        sTempTable = "so_service_center_phone";
        //        sStoredProc = "dp_get_so_service_center_phone";
        //        sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no)",
        //                      " values (", lInstallerUidNo,")");
        //        sSelectSql = "*";
        //        sFilter = "1=1";
        //        if (bActivateLog)
        //            objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"",DateTime.Now,"\"/>");

        //         oOutputRs = GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sIngresDSN, sFilter, sSelectSql);

        //        if (bActivateLog)
        //            objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"" ,DateTime.Now,"\"/>");

        //        if(bActivateLog)
        //            objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"",DateTime.Now, "\"/>");

        //            sOutput = RsToXML(oOutputRs, true, , "phoneNumbers");//moddata acess have acess method

        //        if (bActivateLog)
        //            objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime,"<detail function=\"RsToXML\" type=\"end\" time=\"",DateTime.Now,"\"/>");

        //        if(sStylesheet!=string.Empty)
        //            sOutput = TransformXML(sOutput, string.Concat(sStylesheetPath,sStylesheet));//moddata acess have method
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        oOutputRs.Close();
        //        oOutputRs=null;
        //    }
        //    return sOutput;
        //}

        //public string GetServiceVehicles(long lInstallerUidNo, [Optional]string sStylesheet = "")
        //{
        //    string sStoredProc = string.Empty,
        //             sTempTable = string.Empty,
        //             sInsertSQL = string.Empty,
        //             sSelectSql = string.Empty,
        //             sOutput = string.Empty,
        //             sFilter = string.Empty;
        //    Recordset oOutputRs = null;
        //    try
        //    {
        //        sTempTable = "so_service_vehicles";
        //        sStoredProc = "dp_get_so_service_vehicles";
        //        sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no)",
        //                      " values (",lInstallerUidNo, ")");
        //        sSelectSql = "*";
        //        sFilter = string.Empty;

        //        if(bActivateLog)
        //            objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime,"<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

        //        oOutputRs = GetSessionRows(sTempTable, sStoredProc, sInsertSQL, sIngresDSN, sFilter, sSelectSql);

        //        if(bActivateLog)
        //            objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"",DateTime.Now,"\"/>");

        //        if(bActivateLog)
        //            objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime,"<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

        //        sOutput = RsToXML(oOutputRs, true, , "servicevehicles");

        //        if(bActivateLog)
        //            objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime,"<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

        //        if(sStylesheet!=string.Empty)
        //            sOutput = TransformXML(sOutput, string.Concat(sStylesheetPath,sStylesheet));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        oOutputRs.Close();
        //        oOutputRs=null;
        //    }
        //    return sOutput;
        //}
        #endregion

        public string GetTechnicians(long lInstallerUidNo, [Optional]string sXML = "", [Optional]string sUserID = "", [Optional]string sStylesheet = "")
        {
            string cACTION_CODE = "GET";
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sInsertSQL = string.Empty,
                    sSelectSql = string.Empty,
                    sOutput = string.Empty,
                    sFilter = string.Empty,
                    sCertXML = string.Empty,
                    sCertificationXML = string.Empty,
                    sLicenseXML = string.Empty,
                    sTrainingXML = string.Empty,
                    sTrainingXMLLookup = string.Empty,
                    sLicenseXMLLookup = string.Empty,
                    sCertificationXMLLookup = string.Empty,
                    stechnician_licensesXMLLookup = string.Empty,
                    stechnician_licensesXMLChildLookup = string.Empty,
                    sCompany_licensesXMLLookup = string.Empty,
                    strParentID = string.Empty,
                    strLicenseID = string.Empty,
                    strServiceCenterStateCode = string.Empty;
            string[] arrParentID;
            int intLicenseCount, intValidSCLicenseCount, i;
            long iTechUidNo;
            Recordset oOutputRs1 = null;
            DOMDocument40 oXMLDom = null;
            DOMDocument40 oXMLDomChild = null;
            IXMLDOMNodeList oXmlNodeList = null;
            IXMLDOMNodeList oXmlNodeListChild = null;
            IXMLDOMNode ixmlDOMNode = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                oOutputRs1 = new Recordset();
                mobjCommon = new lynxAccount4.modCommon();
                oXMLDom = new DOMDocument40();
                oXMLDomChild = new DOMDocument40();
                sXML = sXML.Replace("</GetServiceCenter>", "<ActionCode>GET</ActionCode></GetServiceCenter>");
                sTrainingXML = string.Empty;
                sLicenseXML = string.Empty;
                sCertificationXML = string.Empty;
                sTempTable = "so_service_technician";
                sStoredProc = "dp_get_so_technician";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no)",
                              " values (", lInstallerUidNo, ")");
                sSelectSql = "*";
                sFilter = string.Empty;

                if (lynxAccount4.modCommon.bActivateLog)
                {
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", DateTime.Now, "\"/>");
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");
                }

                oOutputRs1 = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (oOutputRs1.RecordCount != 0)
                {
                    while (!oOutputRs1.EOF)
                    {
                        iTechUidNo = Convert.ToInt64(oOutputRs1.Fields["technician_uid_no"].ToString().Trim());
                        sTrainingXML = string.Concat(sTrainingXML, ManageTechniciansTraining(sXML, iTechUidNo, sUserID));
                        sLicenseXML = string.Concat(sLicenseXML, ManageTechniciansLicenses(sXML, iTechUidNo, sUserID));
                        sCertificationXML = string.Concat(sCertificationXML, ManageTechniciansCertification(sXML, iTechUidNo, sUserID));
                        oOutputRs1.MoveNext();
                    }
                    oOutputRs1.MoveFirst();
                }

                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                ixmlDOMNode = oXMLDom.selectSingleNode("//headerInfo/record/@state_cd");

                if (ixmlDOMNode != null)
                    strServiceCenterStateCode = ixmlDOMNode.text;

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetCertXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                oXMLDom = new DOMDocument40();
                oXMLDom.load(string.Concat(sXMLPath, "lookup.xml"));

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                oXmlNodeList = oXMLDom.selectNodes("//ObjectGroup[@ObjectGroupName='technician_training']/Object[@StatusID = '1']");

                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    if (oXMLNode != null)
                        sTrainingXMLLookup = string.Concat(sTrainingXMLLookup, "<technician_training>", oXMLNode.xml, "</technician_training>");

                }

                oXmlNodeList = oXMLDom.selectNodes("//ObjectGroup[@ObjectGroupName='technician_licenses']/Object[@StatusID = '1']");

                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    intLicenseCount = oXmlNodeList.length;
                    if (oXMLNode != null)
                    {
                        strParentID = string.Concat(strParentID, oXMLNode.attributes[1].text, "^");
                        sLicenseXMLLookup = string.Concat(sLicenseXMLLookup, "<technician_licenses>", oXMLNode.xml, "</technician_licenses>");
                    }
                }

                arrParentID = strParentID.Split('^');
                oXmlNodeList = oXMLDom.selectNodes("//ObjectGroup[@ObjectGroupName='technician_certification']/Object[@StatusID = '1']");

                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    if (oXMLNode != null)
                        sCertificationXMLLookup = string.Concat(sCertificationXMLLookup, "<technician_certification>", oXMLNode.xml, "</technician_certification>");

                }

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetCertXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                oXMLDom = new DOMDocument40();
                oXMLDom.load(string.Concat(sXMLPath, "lookup_xref.xml"));

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);//modcommon have these method
                }
                intLicenseCount = oXmlNodeList.length;
                for (i = 0; i != intLicenseCount; i++)
                {
                    oXmlNodeList = oXMLDom.selectNodes(string.Concat("//XrefObjectGroup/ParentObjectGroup[@ParentObjectGroupName= 'technician_licenses']/ChildObjectGroup[@CHildParentObjectID= '", arrParentID[i], "' and @StatusID = '1']"));
                    foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                    {
                        if (oXMLNode != null)
                            stechnician_licensesXMLChildLookup = string.Concat(stechnician_licensesXMLChildLookup, oXMLNode.xml);

                    }
                }

                stechnician_licensesXMLChildLookup = string.Format("<getStateCodes>{0}</getStateCodes>", stechnician_licensesXMLChildLookup);
                oXMLDomChild.loadXML(stechnician_licensesXMLChildLookup);

                if (oXMLDomChild.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDomChild.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                stechnician_licensesXMLChildLookup = string.Empty;
                strParentID = string.Empty;
                oXmlNodeListChild = oXMLDomChild.selectNodes(string.Concat("//ChildObjectGroup[@ChildObjectCode= '", strServiceCenterStateCode, "' and @StatusID = '1']"));

                foreach (IXMLDOMNode oXMLNodeChild in oXmlNodeListChild)
                {
                    if (oXMLNodeChild != null)
                    {
                        strParentID = string.Concat(strParentID, oXMLNodeChild.attributes[1].text, "^");
                        stechnician_licensesXMLChildLookup = string.Concat(stechnician_licensesXMLChildLookup, oXMLNodeChild.xml);
                    }

                }

                stechnician_licensesXMLChildLookup = string.Concat("<stechnician_licensesXMLChildLookup>", stechnician_licensesXMLChildLookup, "</stechnician_licensesXMLChildLookup>");
                oXMLDomChild = new DOMDocument40();
                oXMLDomChild.loadXML(stechnician_licensesXMLChildLookup);

                if (oXMLDomChild.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDomChild.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                oXmlNodeListChild = oXMLDomChild.selectNodes("//ChildObjectGroup[not(@CHildParentObjectID=preceding-sibling::node()/attribute::CHildParentObjectID)]");
                intValidSCLicenseCount = oXmlNodeListChild.length;
                strParentID = string.Empty;

                foreach (IXMLDOMNode oXMLNodeChild in oXmlNodeListChild)
                {
                    if (oXMLNodeChild != null)
                        strParentID = string.Concat(strParentID, oXMLNodeChild.attributes[6].text, "^");

                }

                arrParentID = strParentID.Split('^');
                sLicenseXMLLookup = string.Concat("<sLicenseXMLLookup1>", sLicenseXMLLookup, "</sLicenseXMLLookup1>");
                oXMLDom = new DOMDocument40();
                oXMLDom.loadXML(sLicenseXMLLookup);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                sLicenseXMLLookup = string.Empty;

                for (i = 0; i != intValidSCLicenseCount; i++)
                {
                    oXmlNodeList = oXMLDom.selectNodes(string.Concat("//Object[@ObjectID = '", arrParentID[i], "']"));
                    foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                    {
                        if (oXMLNode != null)
                            sLicenseXMLLookup = string.Concat(sLicenseXMLLookup, "<sLicenseXMLLookup>", oXMLNode.xml, "</sLicenseXMLLookup>");
                    }
                }

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                sOutput = mobjDataAccess.RsToXML(oOutputRs1, true);
                sOutput = string.Concat("<technicians>", sOutput, sTrainingXMLLookup, "<technician_licenses>", sLicenseXMLLookup, "</technician_licenses>", sCertificationXMLLookup, "<strLicenseXML>", stechnician_licensesXMLChildLookup, "</strLicenseXML>", sTrainingXML, sLicenseXML, sCertificationXML, "</technicians>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oOutputRs1 = null;
                oXMLDom = null;
                oXmlNodeList = null;
                oXmlNodeListChild = null;

            }
            return sOutput;
        }

        public string GetServiceCenterProfileItems(long lInstallerUidNo, [Optional]string sStylesheet, [Optional]string sUserID)
        {
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sInsertSQL = string.Empty,
                    sSelectSql = string.Empty,
                    sOutput = string.Empty,
                    sFilter = string.Empty;
            Recordset oOutputRs = null;
            try
            {
                sTempTable = "so_profile_item";
                sStoredProc = "dp_get_so_profile_item";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (profile_object,profile_uid_no, user_id_no, user_session_no)",
                              " values ('inst',", lInstallerUidNo, ", '", sUserID, "', '", sSessionID, "')");
                sSelectSql = "*";
                sFilter = "attrib_type_cd=5 order by attrib_desc ";

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"begin\" time=\"", Convert.ToString(DateTime.Now), "\"/>");

                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"GetSessionRows\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"begin\" time=\"", DateTime.Now, "\"/>");

                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "item", "profile");

                if (lynxAccount4.modCommon.bActivateLog)
                    objmodcommon.sLogTime = string.Concat(objmodcommon.sLogTime, "<detail function=\"RsToXML\" type=\"end\" time=\"", DateTime.Now, "\"/>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string ManageServiceVehicles(string sXML, [Optional]string sStylesheet)
        {
            string sStoredProc = string.Empty,
                   sOutput = string.Empty,
                   sActionCode = string.Empty,
                   sLicenseNo = string.Empty,
                   sLicenseState = string.Empty,
                   sUserID = string.Empty,
                   sUserSessionID = string.Empty,
                   sVehicleDescription = string.Empty,
                   sVIN = string.Empty,
                   sOLDVIN = string.Empty;
            int sInd;
            bool bLogError;
            long lInstallerID;
            object objExecute;
            Connection oConnection = null;
            Command oCmd = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMElement oRoot = null;
            IXMLDOMNode oNode = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                oXMLDom = new DOMDocument40();
                oConnection = new Connection();
                oCmd = new Command();
                oXMLDom.async = false;
                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                sActionCode = oXMLDom.selectSingleNode("//ActionCode").text;
                lInstallerID = Convert.ToInt64(oXMLDom.selectSingleNode("//InstallerId").text);
                sLicenseNo = oXMLDom.selectSingleNode("//LicenseNo").text;
                sLicenseState = oXMLDom.selectSingleNode("//LicenseState").text;
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//UserId").text);
                sUserSessionID = oXMLDom.selectSingleNode("//UserSessionId").text;
                sVehicleDescription = oXMLDom.selectSingleNode("//VehicleDescription").text;
                sVIN = oXMLDom.selectSingleNode("//VIN").text;
                sOLDVIN = oXMLDom.selectSingleNode("//OLDVIN").text;
                sStoredProc = "dp_manage_service_vehicles";
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);
                oCmd.ActiveConnection = oConnection;
                oCmd.CommandType = CommandTypeEnum.adCmdText;
                oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }");
                oCmd.CommandTimeout = 90;
                oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sActionCode));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, lInstallerID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_license_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sLicenseNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_license_state_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 2, sLicenseState));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_dt", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 25, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 7, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_first_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_modify_user_last_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 30, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, sUserID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserSessionID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_veh_desc", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 80, sVehicleDescription));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_orig_vin_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 17, sOLDVIN));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_vin_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 17, sVIN));
                oCmd.Execute(out objExecute);

                if (Convert.ToInt32(oCmd.Parameters["retval"]) != 0)
                {
                    if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0)
                        sInd = 1;
                    else
                        bLogError = true;
                    throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError.ToString(), sStoredProc, oCmd.Parameters["h_msg_text"]));
                }

                sOutput = mobjDataAccess.ParametersToXML(ref oCmd, true, "response");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCmd = null;
                oConnection = null;
            }
            return sOutput;
        }

        public string ManageTechnicians(string sXML, [Optional]string sFlagUpdate, [Optional]string sDeleteItem, [Optional]string sStylesheet)
        {
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sSelectSql = string.Empty,
                    sInsertSQL = string.Empty,
                    sFieldsSQL = string.Empty,
                    sValuesSQL = string.Empty,
                    sField = string.Empty,
                    sValue = string.Empty,
                    sValues = string.Empty,
                    sOutput = string.Empty,
                    sTechOutput = string.Empty,
                    sCertOutput = string.Empty,
                    sFilter = string.Empty,
                    sDropDownXML = string.Empty,
                    sCertXML = string.Empty,
                    sLicenseXML = string.Empty,
                    sTrainingXML = string.Empty,
                    strCheckCertPath = string.Empty,
                    strCheckLicensePath = string.Empty,
                    sActionCode = string.Empty,
                    sUserID = string.Empty,
                    sFirstName = string.Empty,
                    sInstUidNo = string.Empty,
                    sLastName = string.Empty,
                    sSSSuffixNo = string.Empty,
                    sCertCode = string.Empty,
                    sLicenseState = string.Empty,
                    sLicenseNumber = string.Empty,
                    sLicenseExpDate = string.Empty,
                    sUserSessionID = string.Empty,
                    strParam = string.Empty,
                    strErrMsg = string.Empty,
                    strResult = string.Empty,
                    strCheckTrainingPath = string.Empty;
            Connection oConnection = null;
            Command oCmd = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNode oXMLNodeCert = null;
            IXMLDOMNode oXMLNodeLic = null;
            IXMLDOMNode oXMLNodeTrain = null;
            IXMLDOMNodeList oXmlNodeList = null;
            Recordset oOutputRs = null;
            IXMLDOMParseError tmpParseError = null;
            int sInd;
            bool bLogError;
            long iTechUidNo;
            long lInstallerUID;
            object objExecute;
            try
            {
                oXMLDom = new DOMDocument40();
                oConnection = new Connection();
                oCmd = new Command();
                mobjCommon = new lynxAccount4.modCommon();
                objmodcommon = new modAGC2Common();

                if (sFlagUpdate != string.Empty)
                {
                    if (Left(sFlagUpdate, 2) == "Y*")
                    {
                        sDeleteItem = Right(sFlagUpdate, sFlagUpdate.Length - 2);
                        sFlagUpdate = Left(sFlagUpdate, 1);
                    }
                    else
                        sDeleteItem = string.Empty;
                }

                oXMLDom.async = false;
                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                sActionCode = oXMLDom.selectSingleNode("//ActionCode").text;
                strParam = "first_nm";
                sFirstName = objmodcommon.getNodeValue(ref oXMLDom, ref strParam);
                strParam = "last_nm";
                sLastName = objmodcommon.getNodeValue(ref oXMLDom, ref strParam);
                strParam = "soc_sec_suffix_no";
                sSSSuffixNo = objmodcommon.getNodeValue(ref oXMLDom, ref strParam);
                sInstUidNo = oXMLDom.selectSingleNode("//inst_uid_no").text;
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//UserID").text);//this function already exists in mod common class file
                sUserSessionID = sUserID;
                iTechUidNo = Convert.ToInt64(oXMLDom.selectSingleNode("//technician_uid_no").text);
                strParam = "license_no";
                sLicenseNumber = Right((objmodcommon.getNodeValue(ref oXMLDom, ref strParam)).Trim(), 4);
                strParam = "license_state_cd";
                sLicenseState = objmodcommon.getNodeValue(ref oXMLDom, ref strParam);

                if (sActionCode == "TRANSFER")
                {
                    sOutput = TechServiceCenterSwitch(sXML);
                    oXMLDom = null;
                    return sOutput;

                }

                if (sFlagUpdate != "Y")
                {
                    oXMLNodeCert = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/certification");
                    if (oXMLNodeCert == null)
                        strCheckCertPath = string.Empty;
                    else
                        strCheckCertPath = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/certification").text;

                    if (strCheckCertPath != string.Empty)
                        sCertXML = ManageTechniciansCertification(sXML, iTechUidNo, sUserID);

                    oXMLNodeLic = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/license");

                    if (oXMLNodeLic == null)
                        strCheckLicensePath = string.Empty;
                    else
                        strCheckLicensePath = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/license").text;

                    if (strCheckLicensePath != string.Empty)
                        sLicenseXML = ManageTechniciansLicenses(sXML, iTechUidNo, sUserID);

                    oXMLNodeTrain = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/training");

                    if (oXMLNodeTrain == null)
                        strCheckTrainingPath = string.Empty;
                    else
                        strCheckTrainingPath = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/training").text;

                    if (strCheckTrainingPath != string.Empty)
                        sTrainingXML = ManageTechniciansTraining(sXML, iTechUidNo, sUserID);

                    sTechOutput = string.Concat("<ManageTechnicians>", sCertXML, sLicenseXML, sTrainingXML, "<InstallerID>", sInstUidNo, "</InstallerID><UserID>", sUserID, "</UserID><ActionCode>", sActionCode, "</ActionCode></ManageTechnicians>");

                    sOutput = string.Concat("<data>", sTechOutput, "</data>");

                    if (sDeleteItem != string.Empty)
                    {
                        oXMLDom = null;
                        oXmlNodeList = null;
                        if (oOutputRs != null)
                            oOutputRs.Close();

                        oOutputRs = null;
                        return sOutput;
                    }
                }
                if (sDeleteItem != string.Empty)
                {
                    switch (sDeleteItem)
                    {
                        case "Certification":
                            oXMLNodeCert = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/certification");
                            if (oXMLNodeCert == null)
                                strCheckCertPath = string.Empty;
                            else
                                strCheckCertPath = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/certification").text;

                            if (strCheckCertPath != string.Empty)
                                sCertXML = ManageTechniciansCertification(sXML, iTechUidNo, sUserID);
                            break;
                        case "License":
                            oXMLNodeLic = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/license");
                            if (oXMLNodeLic == null)
                                strCheckLicensePath = string.Empty;
                            else
                                strCheckLicensePath = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/license").text;
                            if (strCheckLicensePath != string.Empty)
                                sLicenseXML = ManageTechniciansLicenses(sXML, iTechUidNo, sUserID);
                            break;
                        case "Training":
                            oXMLNodeTrain = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/training");

                            if (oXMLNodeTrain == null)
                                strCheckTrainingPath = string.Empty;
                            else
                                strCheckTrainingPath = oXMLDom.selectSingleNode("//ServiceCenterTechnicians/data/TechnicianData/training").text;

                            if (strCheckTrainingPath != string.Empty)
                                sTrainingXML = ManageTechniciansTraining(sXML, iTechUidNo, sUserID);
                            break;
                    }

                    sTechOutput = sTechOutput.Replace("</ManageTechnicians>", string.Concat(sCertXML, sLicenseXML, sTrainingXML, "</ManageTechnicians>"));
                    sOutput = string.Concat("<data>", sTechOutput, "</data>");
                    oXMLDom = null;
                    oXMLNode = null;
                    oXmlNodeList = null;

                    if (oOutputRs != null)
                    {
                        oOutputRs.Close();
                        oOutputRs = null;
                    }

                    sDeleteItem = string.Empty;
                    return sOutput;
                }
                sStoredProc = "dp_manage_technician";
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);
                oCmd.ActiveConnection = oConnection;
                oCmd.CommandType = CommandTypeEnum.adCmdText;
                oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(?,?,?,?,?,?,?,?,?,?,?,?,?) }");
                oCmd.CommandTimeout = 90;
                oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sActionCode));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                //oCmd.Parameters.Append .CreateParameter("h_driver_license_exp_dt", adVarChar, adParamInputOutput, 19, sLicenseExpDate)
                oCmd.Parameters.Append(oCmd.CreateParameter("h_driver_license_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 4, sLicenseNumber));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_driver_license_state_cd", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 2, sLicenseState));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_first_name", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 20, sFirstName));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 10, sInstUidNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_last_name", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 20, sLastName));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_soc_sec_suffix_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 4, sSSSuffixNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_technician_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 10, iTechUidNo));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, sUserID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserSessionID));
                oCmd.Execute(out objExecute);

                if (Convert.ToInt32(oCmd.Parameters["retval"]) != 0)
                {
                    bLogError = true;
                    strErrMsg = Convert.ToString(oCmd.Parameters["h_msg_text"]);
                    if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0 || strErrMsg.Contains("Cannot remove this technician") || strErrMsg.Contains("Removing the current technician will result in insufficient technicians") || strErrMsg.Contains("is already assigned to"))
                        sInd = 1;
                    throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError.ToString(), sStoredProc, oCmd.Parameters["h_msg_text"]));
                }

                if (sActionCode != "DELETE")
                {
                    iTechUidNo = Convert.ToInt64(oCmd.Parameters["h_technician_uid_no"].Value);
                    sTechOutput = mobjDataAccess.ParametersToXML(ref oCmd, true, "ManageTechnicians");
                    if (Convert.ToInt32(oCmd.Parameters["retval"]) == 0)
                    {
                        sCertXML = ManageTechniciansCertification(sXML, iTechUidNo, sUserID);
                        sLicenseXML = ManageTechniciansLicenses(sXML, iTechUidNo, sUserID);
                        sTrainingXML = ManageTechniciansTraining(sXML, iTechUidNo, sUserID);
                    }
                }

                sTechOutput = sTechOutput.Replace("</ManageTechnicians>", string.Concat(sCertXML, sLicenseXML, sTrainingXML, "</ManageTechnicians>"));
                sOutput = string.Concat("<data>", sTechOutput, "</data>");

                if (sStylesheet != string.Empty)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXMLNode = null;
                oXmlNodeList = null;
                if (oOutputRs != null)
                {
                    oOutputRs.Close();
                    oOutputRs = null;
                }
                oCmd = null;
                oConnection.Close();
                oConnection = null;

            }
            return sOutput;
        }

        public string TechServiceCenterSwitch(string sInputXML)
        {
            string sTechID = string.Empty,
                   sNewSC = string.Empty,
                   sOLDSC = string.Empty,
                   sTechCnt = string.Empty,
                   sActionCode = string.Empty,
                   sUserID = string.Empty,
                   strErrMsg = string.Empty,
                   strParam = string.Empty,
                   sStoredProc = string.Empty;
            Command oCmd = null;
            Connection oConnection = null;
            FreeThreadedDOMDocument40 oXMLIn = null;
            IXMLDOMParseError tmpParseError = null;
            DOMDocument40 objDomdocument = null;
            int sInd;
            try
            {
                objmodcommon = new modAGC2Common();
                oXMLIn = new FreeThreadedDOMDocument40();
                mobjCommon = new lynxAccount4.modCommon();
                oCmd = new Command();
                oConnection = new Connection();
                oXMLIn.loadXML(sInputXML);

                if (oXMLIn.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLIn.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                objDomdocument = (DOMDocument40)oXMLIn;
                strParam = "TechID";
                sTechID = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "NewServiceCenter";
                sNewSC = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "OldServiceCenter";
                sOLDSC = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "TechCnt";
                sTechCnt = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "ActionCode";
                sActionCode = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "UserID";
                sUserID = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                sStoredProc = "dp_manage_technician";
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);
                oCmd.ActiveConnection = oConnection;
                oCmd.CommandType = CommandTypeEnum.adCmdText;
                oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(?,?,?,?,?,?,?,?,?,?,?,?,?) }");
                oCmd.CommandTimeout = 90;
                oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sActionCode));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_driver_license_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 4, ""));  //'sLicenseNumber
                oCmd.Parameters.Append(oCmd.CreateParameter("h_driver_license_state_cd", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 2, ""));  //'sLicenseState
                oCmd.Parameters.Append(oCmd.CreateParameter("h_first_name", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 20, "")); //'sFirstName
                oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 10, sNewSC));  // 'sInstUidNo
                oCmd.Parameters.Append(oCmd.CreateParameter("h_last_name", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 20, ""));  //'sLastName
                oCmd.Parameters.Append(oCmd.CreateParameter("h_soc_sec_suffix_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 4, 0));//'sSSSuffixNo
                oCmd.Parameters.Append(oCmd.CreateParameter("h_technician_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 10, sTechID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, sUserID));
                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));//'sUserSessionID

                if (Convert.ToBoolean(oCmd.Parameters["retval"]) != true)
                {
                    //'bLogError = True
                    strErrMsg = Convert.ToString(oCmd.Parameters["h_msg_text"]);
                    if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0 || strErrMsg.Contains("Cannot remove this technician") || strErrMsg.Contains("Removing the current technician will result in insufficient technicians") || strErrMsg.Contains("is already assigned to"))
                        sInd = 1;
                    throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError, sStoredProc, oCmd.Parameters["h_msg_text"]));
                }
                else
                    strErrMsg = "Technician was successfully transfered to new the new Service Center";
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oCmd = null;
                oConnection = null;
                oXMLIn = null;
            }
            return strErrMsg;
        }

        private string ManageTechniciansCertification(string sXML, long iTechUidNo, string sUserID)
        {
            string sStoredProc = string.Empty,
                     sTempTable = string.Empty,
                     sSelectSql = string.Empty,
                     sInsertSQL = string.Empty,
                     sFieldsSQL = string.Empty,
                     sValuesSQL = string.Empty,
                     sField = string.Empty,
                     sValue = string.Empty,
                     sValues = string.Empty,
                     sOutput = string.Empty,
                     sTechOutput = string.Empty,
                     sCertOutput = string.Empty,
                     sFilter = string.Empty,
                     sActionCode = string.Empty,
                     sDropDownXMLng = string.Empty,
                     sUserSessionID = string.Empty,
                     sCertCode = string.Empty,
                     sInstUidNo = string.Empty,
                     strTech_cert_nm = string.Empty,
                     sCertificationNm = string.Empty,
                     sCertificationNo = string.Empty,
                     sCertificationDate = string.Empty,
                     sCertExpDate = string.Empty,
                     strCertificationNm = string.Empty,
                     strErrMsg = string.Empty;
            string[] vCertification;
            int sInd;
            long lgCertificationNm, lInstallerUID;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNodeList oXmlNodeList = null;
            Recordset oOutputRs = null;
            IXMLDOMParseError tmpParseError = null;
            bool bLogError;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                oXMLDom = new DOMDocument40();
                oXMLDom.async = false;
                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//UserID").text);
                sUserSessionID = sUserID;
                sActionCode = oXMLDom.selectSingleNode("//ActionCode").text;
                sUserSessionID = sUserID;

                switch (sActionCode.ToUpper())
                {
                    case "GET":
                        sTempTable = "so_technician_certification";
                        sStoredProc = "dp_manage_so_technician_certs";
                        sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, technician_uid_no, commit_ind, msg_ind, ",
                                     "msg_text, user_id_no, user_session_no) VALUES");
                        sValuesSQL = string.Concat("('GET', ", iTechUidNo, ", 'Y', 'N', '', '", sUserID, "','", sUserID, "')~");
                        sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                    case "DELETE":
                        sTempTable = "so_technician_certification";
                        sStoredProc = "dp_manage_so_technician_certs";
                        oXMLDom.loadXML(sXML);
                        oXmlNodeList = oXMLDom.selectNodes("//ServiceCenterTechnicians/data/TechnicianData/certification");

                        foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                        {
                            sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, certification_no, certification_uid_no, technician_uid_no, commit_ind, msg_ind, ",
                                                      "msg_text, user_id_no, user_session_no) VALUES");
                            lgCertificationNm = Convert.ToInt64(oXMLNode.selectSingleNode("Tech_cert_id").text);
                            sCertificationNo = oXMLNode.selectSingleNode("Tech_cert_no").text;
                            sValuesSQL = string.Concat("('DELETE', '", sCertificationNo, "', ", lgCertificationNm, ", ", iTechUidNo, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                            sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        }

                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                    case "UPDATE":
                        sTempTable = "so_technician_certification";
                        sStoredProc = "dp_manage_so_technician_certs";
                        oXMLDom.loadXML(sXML);
                        oXmlNodeList = oXMLDom.selectNodes("//ServiceCenterTechnicians/data/TechnicianData/certification");

                        foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                        {
                            if (oXMLDom.text != string.Empty)
                            {
                                sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, certification_dt,  certification_no, certification_uid_no, exp_dt, technician_uid_no, commit_ind, msg_ind, ",
                                                    "msg_text, user_id_no, user_session_no) VALUES");
                                strTech_cert_nm = oXMLNode.selectSingleNode("Tech_cert_nm").text;
                                vCertification = strTech_cert_nm.Split('^');
                                lgCertificationNm = Convert.ToInt64(vCertification[0]);
                                sCertificationNm = vCertification[1];
                                sCertificationNo = oXMLNode.selectSingleNode("Tech_cert_id").text;
                                sCertificationDate = oXMLNode.selectSingleNode("Tech_cert_dt").text;
                                sCertExpDate = oXMLNode.selectSingleNode("Tech_exp_dt").text;
                                sValuesSQL = string.Concat("('UPDATE', '", sCertificationDate, "', '", sCertificationNo, "', ", lgCertificationNm, ", '", sCertExpDate, "', ", iTechUidNo, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                                sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);

                            }
                            else
                            {
                                oXMLDom = null;
                                oXmlNodeList = null;
                                if (oOutputRs != null)
                                    oOutputRs.Close();
                                oOutputRs = null;
                            }
                        }

                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                    case "INSERT":
                        sInsertSQL = string.Empty;
                        sTempTable = "so_technician_certification";
                        sStoredProc = "dp_manage_so_technician_certs";
                        oXMLDom.loadXML(sXML);
                        oXmlNodeList = oXMLDom.selectNodes("//ServiceCenterTechnicians/data/TechnicianData/certification");

                        foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                        {
                            if (oXMLNode.text != string.Empty)
                            {
                                sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, certification_dt, certification_nm, certification_no, certification_uid_no, exp_dt, technician_uid_no, commit_ind, msg_ind, ",
                                                    "msg_text, user_id_no, user_session_no) VALUES");
                                strTech_cert_nm = oXMLNode.selectSingleNode("Tech_cert_nm").text;
                                vCertification = strTech_cert_nm.Split('^');
                                lgCertificationNm = Convert.ToInt64(vCertification[0]);
                                sCertificationNm = vCertification[1];
                                sCertificationNo = oXMLNode.selectSingleNode("Tech_cert_id").text;
                                sCertificationDate = oXMLNode.selectSingleNode("Tech_cert_dt").text;
                                sCertExpDate = oXMLNode.selectSingleNode("Tech_exp_dt").text;
                                sValuesSQL = string.Concat("('INSERT', '", sCertificationDate, "', '", sCertificationNm, "', '", sCertificationNo, "', ", lgCertificationNm, ", '", sCertExpDate, "', ", iTechUidNo, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                                sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                            }
                            else
                            {
                                oXMLDom = null;
                                oXmlNodeList = null;
                                if (oOutputRs != null)
                                    oOutputRs.Close();
                                oOutputRs = null;
                            }
                        }

                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        sCertOutput = mobjDataAccess.RsToXML(oOutputRs, true, "Certification");
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXmlNodeList = null;
                if (oOutputRs != null)
                    oOutputRs.Close();
                oOutputRs = null;
            }
            return sCertOutput;
        }

        private string ManageTechniciansLicenses(string sXML, long iTechUidNo, string sUserID)
        {
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sSelectSql = string.Empty,
                   sInsertSQL = string.Empty,
                   sFieldsSQL = string.Empty,
                   sValuesSQL = string.Empty,
                   sField = string.Empty,
                   sValue = string.Empty,
                   sValues = string.Empty,
                   sOutput = string.Empty,
                   sTechOutput = string.Empty,
                   sCertOutput = string.Empty,
                   sFilter = string.Empty,
                   sDropDownXML = string.Empty,
                   sActionCode = string.Empty,
                   sUserSessionID = string.Empty,
                   sInstUidNo = string.Empty,
                   strTech_StateInfo = string.Empty,
                   strTech_licenses_nm = string.Empty,
                   sLicenseStateCD = string.Empty,
                   sLicenseNm = string.Empty,
                   sLicenseNo = string.Empty,
                   sLicenseExpDate = string.Empty;
            long lInstallerUID, lgLicenseStateID, lgLicenseNm;
            string[] vStateCode;
            string[] vLicense;
            bool bLogError;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNodeList oXmlNodeList = null;
            IXMLDOMParseError tmpParseError = null;
            Recordset oOutputRs = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                oOutputRs = new Recordset();
                oXMLDom = new DOMDocument40();
                oXMLDom.async = false;
                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                sActionCode = oXMLDom.selectSingleNode("//ActionCode").text;
                sUserSessionID = sUserID;

                switch (sActionCode.ToUpper())
                {
                    case "GET":
                        sTempTable = "so_technician_license";
                        sStoredProc = "Dp_manage_so_technician_license";
                        sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, technician_uid_no, commit_ind, msg_ind, ",
                                      "msg_text, user_id_no, user_session_no) VALUES");
                        sValuesSQL = string.Concat("('GET', ", iTechUidNo, ", 'Y', 'N', '', '", sUserID, "','", sUserID, "')~");
                        sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                    case "DELETE":
                        sTempTable = "so_technician_license";
                        sStoredProc = "Dp_manage_so_technician_license";
                        oXMLDom.loadXML(sXML);
                        oXmlNodeList = oXMLDom.selectNodes("//ServiceCenterTechnicians/data/TechnicianData/license");

                        foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                        {
                            sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, technician_license_id, license_state_id, technician_uid_no, commit_ind, msg_ind, ",
                                          "msg_text, user_id_no, user_session_no) VALUES");
                            lgLicenseStateID = Convert.ToInt64(oXMLNode.selectSingleNode("license_state_id").text);
                            lgLicenseNm = Convert.ToInt64(oXMLNode.selectSingleNode("technician_license_id").text);
                            sValuesSQL = string.Concat("('DELETE', ", lgLicenseNm, ", ", lgLicenseStateID, ", ", iTechUidNo, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                            sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        }

                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;

                    case "UPDATE":
                        sTempTable = "so_technician_license";
                        sStoredProc = "Dp_manage_so_technician_license";
                        oXMLDom.loadXML(sXML);
                        oXmlNodeList = oXMLDom.selectNodes("//ServiceCenterTechnicians/data/TechnicianData/license");

                        foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                        {
                            if (oXMLNode.text != string.Empty)
                            {
                                sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, exp_dt, technician_license_id, technician_license_no,  technician_license_nm, license_state_id, license_state_cd, technician_uid_no, commit_ind, msg_ind, ",
                                          "msg_text, user_id_no, user_session_no) VALUES");
                                strTech_StateInfo = oXMLNode.selectSingleNode("Tech_state_cd").text;
                                lgLicenseStateID = Convert.ToInt64(oXMLNode.selectSingleNode("License_state_id").text);
                                vStateCode = strTech_StateInfo.Split('^');
                                sLicenseStateCD = vStateCode[0];
                                strTech_licenses_nm = oXMLNode.selectSingleNode("Tech_licenses_nm").text;
                                vLicense = strTech_licenses_nm.Split('^');
                                lgLicenseNm = Convert.ToInt64(vLicense[0]);
                                sLicenseNo = oXMLNode.selectSingleNode("Tech_license_num").text;
                                sLicenseExpDate = oXMLNode.selectSingleNode("Tech_license_exp_dt").text;
                                sValuesSQL = string.Concat("('UPDATE', '", sLicenseExpDate, "', ", lgLicenseNm, ", '", sLicenseNo, "', '", sLicenseNm, "', ", lgLicenseStateID, ", '", sLicenseStateCD, "', ", iTechUidNo, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                                sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                            }
                            else
                            {
                                oXMLDom = null;
                                oXmlNodeList = null;
                                oXmlNodeList = null;
                                if (oOutputRs != null)
                                    oOutputRs.Close();
                                oOutputRs = null;
                            }

                        }

                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                    case "INSERT":
                        sInsertSQL = string.Empty;
                        sTempTable = "so_technician_license";
                        sStoredProc = "Dp_manage_so_technician_license";
                        oXMLDom.loadXML(sXML);
                        oXmlNodeList = oXMLDom.selectNodes("//ServiceCenterTechnicians/data/TechnicianData/license");

                        foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                        {
                            if (oXMLNode.text != string.Empty)
                            {
                                sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, exp_dt, technician_license_id, technician_license_no,  technician_license_nm, license_state_id, license_state_cd, technician_uid_no, commit_ind, msg_ind, ",
                                          "msg_text, user_id_no, user_session_no) VALUES");
                                strTech_StateInfo = oXMLNode.selectSingleNode("Tech_state_cd").text;
                                vStateCode = strTech_StateInfo.Split('^');
                                lgLicenseStateID = Convert.ToInt64((vStateCode[0]));
                                sLicenseStateCD = vStateCode[1].Trim();
                                strTech_licenses_nm = oXMLNode.selectSingleNode("Tech_licenses_nm").text;
                                vLicense = strTech_licenses_nm.Split('^');
                                lgLicenseNm = Convert.ToInt64(vLicense[0]);
                                sLicenseNm = Convert.ToString(vLicense[1]);
                                sLicenseNo = oXMLNode.selectSingleNode("Tech_license_num").text;
                                sLicenseExpDate = oXMLNode.selectSingleNode("Tech_license_exp_dt").text;
                                sValuesSQL = string.Concat("('INSERT', '", sLicenseExpDate, "', ", lgLicenseNm, ", '", sLicenseNo, "', '", sLicenseNm, "', ", lgLicenseStateID, ", '", sLicenseStateCD, "', ", iTechUidNo, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                                sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                            }
                            else
                            {
                                oXMLDom = null;
                                oXmlNodeList = null;
                                if (oOutputRs != null)
                                    oOutputRs.Close();
                                oOutputRs = null;
                            }
                        }

                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                }
                sCertOutput = mobjDataAccess.RsToXML(oOutputRs, true, "license");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXmlNodeList = null;
                if (oOutputRs != null)
                    oOutputRs.Close();
                oOutputRs = null;
            }
            return sCertOutput;
        }

        private string ManageTechniciansTraining(string sXML, long iTechUidNo, string sUserID)
        {
            string sStoredProc = string.Empty,
                     sTempTable = string.Empty,
                     sSelectSql = string.Empty,
                     sInsertSQL = string.Empty,
                     sFieldsSQL = string.Empty,
                     sValuesSQL = string.Empty,
                     sField = string.Empty,
                     sValue = string.Empty,
                     sValues = string.Empty,
                     sOutput = string.Empty,
                     sTechOutput = string.Empty,
                     sCertOutput = string.Empty,
                     sFilter = string.Empty,
                     sDropDownXML = string.Empty,
                     sActionCode = string.Empty,
                     sUserSessionID = string.Empty,
                     sInstUidNo = string.Empty,
                     strTech_training_nm = string.Empty,
                     sTrainingComp = string.Empty,
                     sTrainingNm = string.Empty,
                     sTrainingCompDate = string.Empty;
            string[] vTraining;
            long lgTrainingNm;
            bool bLogError;
            long lInstallerUID;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNodeList oXmlNodeList = null;
            Recordset oOutputRs = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                oXMLDom = new DOMDocument40();
                oOutputRs = new Recordset();
                oXMLDom.async = false;
                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                sActionCode = oXMLDom.selectSingleNode("//ActionCode").text;
                sUserSessionID = sUserID;

                switch (sActionCode.ToUpper())
                {
                    case "GET":
                        sTempTable = "so_technician_training";
                        sStoredProc = "dp_manage_so_technician_training";
                        sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, technician_uid_no, commit_ind, msg_ind, ",
                                     "msg_text, user_id_no, user_session_no) VALUES");
                        sValuesSQL = string.Concat("('GET', ", iTechUidNo, ", 'Y', 'N', '', '", sUserID, "','", sUserID, "')~");

                        sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        //'MsgBox  sInsertSQL, vbOKOnly, "sInsertSQL"
                        sSelectSql = "*";

                        //' STEP IV : Sort the Results
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                    case "DELETE":
                        sTempTable = "so_technician_training";
                        sStoredProc = "dp_manage_so_technician_training";
                        oXMLDom.loadXML(sXML);
                        oXmlNodeList = oXMLDom.selectNodes("//ServiceCenterTechnicians/data/TechnicianData/training");

                        foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                        {
                            sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, technician_uid_no, training_uid_no, commit_ind, msg_ind, ",
                                                    "msg_text, user_id_no, user_session_no) VALUES");
                            strTech_training_nm = oXMLNode.selectSingleNode("Tech_training_nm").text;
                            sValuesSQL = string.Concat("('DELETE', ", iTechUidNo, ", ", strTech_training_nm, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                            sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                        }

                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                    case "UPDATE":
                        sTempTable = "so_technician_training";
                        sStoredProc = "dp_manage_so_technician_training";
                        oXMLDom.loadXML(sXML);
                        oXmlNodeList = oXMLDom.selectNodes("//ServiceCenterTechnicians/data/TechnicianData/training");

                        foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                        {
                            if (oXMLNode.text != string.Empty)
                            {
                                sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, technician_uid_no, training_completed_ind, training_dt, training_uid_no, commit_ind, msg_ind, ",
                                                    "msg_text, user_id_no, user_session_no) VALUES");
                                lgTrainingNm = Convert.ToInt32(oXMLNode.selectSingleNode("Tech_training_nm").text);
                                sTrainingCompDate = oXMLNode.selectSingleNode("Tech_complete_dt").text;
                                sValuesSQL = string.Concat("('UPDATE', ", iTechUidNo, ", 'Yes', '", sTrainingCompDate, "', ", lgTrainingNm, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                                sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                            }
                            else
                            {
                                oXMLDom = null;
                                oXmlNodeList = null;
                                if (oOutputRs != null)
                                    oOutputRs = null;
                            }
                        }

                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                    case "INSERT":
                        sInsertSQL = string.Empty;
                        sTempTable = "so_technician_training";
                        sStoredProc = "dp_manage_so_technician_training";
                        oXMLDom.loadXML(sXML);
                        oXmlNodeList = oXMLDom.selectNodes("//ServiceCenterTechnicians/data/TechnicianData/training");

                        foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                        {
                            if (oXMLNode.text != string.Empty)
                            {
                                sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, technician_uid_no, training_completed_ind, training_dt, training_desc, training_uid_no, commit_ind, msg_ind, ",
                                                    "msg_text, user_id_no, user_session_no) VALUES");
                                strTech_training_nm = oXMLNode.selectSingleNode("Tech_training_nm").text;
                                vTraining = strTech_training_nm.Split('^');
                                lgTrainingNm = Convert.ToInt64(vTraining[0]);
                                sTrainingNm = Convert.ToString(vTraining[1]);
                                sTrainingCompDate = oXMLNode.selectSingleNode("Tech_complete_dt").text;
                                sValuesSQL = string.Concat("('INSERT', ", iTechUidNo, ", '", sTrainingComp, "', '", sTrainingCompDate, "', '", sTrainingNm, "', ", lgTrainingNm, ", 'Y', 'N', '', '", sUserID, "', '", sUserID, "')~");
                                sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                            }
                            else
                            {
                                oXMLDom = null;
                                oXmlNodeList = null;
                                if (oOutputRs != null)
                                    oOutputRs = null;

                            }

                        }

                        sSelectSql = "*";
                        sFilter = string.Empty;
                        oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                        break;
                }
                sCertOutput = mobjDataAccess.RsToXML(oOutputRs, true, "Training");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXmlNodeList = null;
                if (oOutputRs != null)
                    oOutputRs.Close();
                oOutputRs = null;
            }
            return sCertOutput;
        }
        public string ManageServiceCenterRemitTo(string sXML, [Optional]string sStylesheet)
        {
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                oXMLDom = new DOMDocument40();
                oXMLDom.async = false;
                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                sOutput = ManageServiceCenterData(sXML);

                if (sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
            return sOutput;
        }

        public string ManageServiceType(string sXML, string sStylesheet)
        {
            string cACTION_CODE = "UPDATE";
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sSelectSql = string.Empty,
                   sInsertSQL = string.Empty,
                   sFieldsSQL = string.Empty,
                   sValuesSQL = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty,
                   sDriverInd = string.Empty,
                   sMobileInd = string.Empty,
                   sUserID = string.Empty,
                   sServiceInd = string.Empty;
            int iServiceType, iMobileMiles;
            long lInstallerUID;
            Recordset oOutputRs = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNodeList oXmlNodeList = null;
            try
            {
                oXMLDom = new DOMDocument40();
                sTempTable = "so_service_type";
                sStoredProc = "dp_manage_so_service_type";
                oXMLDom.loadXML(sXML);
                lInstallerUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);
                oXmlNodeList = oXMLDom.selectNodes("//data/row");

                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, inst_uid_no, user_id_no, user_session_no, ",
                                     "service_type_id, driver_view_ind, mobile_ind, service_type_ind, commit_ind) VALUES");
                    iServiceType = Convert.ToInt32(oXMLNode.attributes.getNamedItem("service_type_id").text);
                    sDriverInd = oXMLNode.attributes.getNamedItem("driver_view_ind").text;
                    sMobileInd = oXMLNode.attributes.getNamedItem("mobile_ind").text;
                    sServiceInd = oXMLNode.attributes.getNamedItem("service_type_ind").text;
                    sValuesSQL = string.Concat("('", cACTION_CODE, "',", lInstallerUID, ",'", sUserID, "','", sUserID, "',",
                                     iServiceType, ",'", sDriverInd, "','", sMobileInd, "','", sServiceInd, "','Y')~");
                    sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                }

                sSelectSql = "*";
                sFilter = "";
                oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "response");

                if (sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXmlNodeList = null;
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string ManageOperatingHours(string sXML, [Optional]string sStylesheet)
        {
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sSelectSql = string.Empty,
                    sInsertSQL = string.Empty,
                    sFieldsSQL = string.Empty,
                    sValuesSQL = string.Empty,
                    sOutput = string.Empty,
                    sFilter = string.Empty,
                    sUserID = string.Empty,
                    sActionCode = string.Empty;
            int iDayNo, iStartTime, iEndTime;
            long lInstallerUID;
            Recordset oOutputRs = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNodeList oXmlNodeList = null;
            try
            {
                oXMLDom = new DOMDocument40();
                sTempTable = "so_manage_operating_hours";
                sStoredProc = "dp_manage_so_operating_hours";
                oXMLDom.loadXML(sXML);
                sActionCode = oXMLDom.selectSingleNode("//header/ActionCode").text;
                lInstallerUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);
                oXmlNodeList = oXMLDom.selectNodes("//data/row");

                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, inst_uid_no, user_id_no, user_session_no, ",
                                              "dow_no, military_start_tm, military_end_tm, commit_ind) VALUES");
                    iDayNo = Convert.ToInt32(oXMLNode.attributes.getNamedItem("dow_no").text);
                    iStartTime = Convert.ToInt32(oXMLNode.attributes.getNamedItem("military_start_tm").text);
                    iEndTime = Convert.ToInt32(oXMLNode.attributes.getNamedItem("military_end_tm").text);
                    sValuesSQL = string.Concat("('", sActionCode, "',", lInstallerUID, ",'", sUserID, "','", sUserID, "',",
                                               iDayNo, ",", iStartTime, ",", iEndTime, ",'Y')~");
                    sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                }

                sSelectSql = "*";
                sFilter = "";
                oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "response");

                if (sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXmlNodeList = null;
            }
            return sOutput;
        }

        public string ManageServiceCenterData(string sXML, [Optional]string sStylesheet = "")
        {
            string sStoredProc = string.Empty,
                    sTempTable = string.Empty,
                    sSelectSql = string.Empty,
                    sInsertSQL = string.Empty,
                    sField = string.Empty,
                    sValue = string.Empty,
                    DisValue = string.Empty,
                    sValues = string.Empty,
                    sOutput = string.Empty,
                    sFilter = string.Empty,
                    sUserID = string.Empty,
                    sActionCode = string.Empty,
                    sFieldsSQL = string.Empty,
                    sValuesSQL = string.Empty;
            double num;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNodeList oXmlNodeList = null;
            Recordset oOutputRs = null;
            try
            {
                oXMLDom = new DOMDocument40();
                sTempTable = "so_service_center";
                sStoredProc = "dp_manage_so_service_center";
                oXMLDom.loadXML(sXML);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);
                sActionCode = oXMLDom.selectSingleNode("//header/ActionCode").text;
                oXmlNodeList = oXMLDom.selectNodes("//data/row");

                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    sField = string.Concat(sField, ", ", oXMLNode.attributes.getNamedItem("name").text);
                    if (double.TryParse(oXMLNode.attributes.getNamedItem("value").text, out num))
                    {
                        if (oXMLNode.attributes.getNamedItem("name").text.Contains("zip_cd") || oXMLNode.attributes.getNamedItem("name").text.Contains("op_hrs_toll_free_cd") || oXMLNode.attributes.getNamedItem("name").text.Contains("store_no") || oXMLNode.attributes.getNamedItem("name").text.Contains("addr_1st") || oXMLNode.attributes.getNamedItem("name").text.Contains("addr_2nd")
                            || oXMLNode.attributes.getNamedItem("name").text.Contains("af_hrs_toll_free_cd") || oXMLNode.attributes.getNamedItem("name").text.Contains("alias_nm"))
                            sOutput = string.Concat(sValue, ", '", oXMLNode.attributes.getNamedItem("value").text, "'");
                        else
                            sValue = string.Concat(sValue, ", ", oXMLNode.attributes.getNamedItem("value").text, "");

                    }
                    else
                        sValue = string.Concat(sValue, ", '", oXMLNode.attributes.getNamedItem("value").text, "'");
                }

                sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, user_id_no, user_session_no, commit_ind",
                             sField, ") VALUES");
                sValuesSQL = string.Concat("('", sActionCode, "','", sUserID, "','", sUserID, "','Y'",
                             sValue, ")~");

                sInsertSQL = string.Concat(sFieldsSQL, sValuesSQL);
                sSelectSql = "*";
                sFilter = "";
                oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "response");

                if (sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXmlNodeList = null;
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string ManageServiceCenterPhones(string sXML, [Optional]string sStylesheet)
        {
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sSelectSql = string.Empty,
                   sInsertSQL = string.Empty,
                   sFieldsSQL = string.Empty,
                   sValuesSQL = string.Empty,
                   sField = string.Empty,
                   sValue = string.Empty,
                   sValues = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty,
                   sDropDownXML = string.Empty,
                   sActionCode = string.Empty,
                   sUserSessionID = string.Empty,
                   sUserID = string.Empty,
                   sPhone = string.Empty,
                   sAreaCode = string.Empty,
                   sExchange = string.Empty,
                   sHousehold = string.Empty,
                   sOldAreaCode = string.Empty,
                   sOldExchange = string.Empty,
                   sOldHousehold = string.Empty,
                   sOldPhoneType = string.Empty,
                   sOldTollFreeCode = string.Empty,
                   sInsertType = string.Empty,
                   sStepNo = string.Empty;
            string[] arrAreaCode, arrExchange, arrHousehold, arrPhoneTypes, arrPhoneUIDs, arrTollFreeCodes;
            int i, sInd;
            bool bUpdate, bLogError;
            long lInstallerUID;
            Connection oConnection = null;
            Command oCmd = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNodeList oXmlNodeList = null;
            Recordset oOutputRs = null;
            object objExecute;
            int PHONE_TYPE_SERVICE_CENTER = 4;
            try
            {
                oXMLDom = new DOMDocument40();
                oCmd = new Command();
                oConnection = new Connection();
                oXMLDom.loadXML(sXML);
                lInstallerUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);
                sActionCode = oXMLDom.selectSingleNode("//header/ActionCode").text;
                sTempTable = "so_service_center";
                sStoredProc = "dp_manage_service_center_phone";

                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);

                if (sActionCode.ToUpper() == "DELETE")
                {
                    sPhone = oXMLDom.selectSingleNode("//data/Phone").text;
                    sAreaCode = Left(sPhone, 3);
                    sExchange = Mid(sPhone, 4, 3);
                    sHousehold = Right(sPhone, 4);

                    oCmd.ActiveConnection = oConnection;
                    oCmd.CommandType = CommandTypeEnum.adCmdText;
                    oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(?,?,?,?,?,?,?,?,?,?,?,?) }");
                    oCmd.CommandTimeout = 90;
                    oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, -1));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, "DELETE"));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, Convert.ToString(lInstallerUID)));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_area_code_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, sAreaCode));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_exchange_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, sExchange));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_household_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, sHousehold));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_phone_type_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, PHONE_TYPE_SERVICE_CENTER));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_toll_free_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, 0));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, "Y"));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sUserID));
                    oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sUserID));
                    oCmd.Execute(out objExecute);

                    if (Convert.ToInt32(oCmd.Parameters["retval"]) != 0)
                    {
                        bLogError = true;
                        if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0)
                            sInd = 1;
                        else
                            sInd = 4;

                        throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError, sStoredProc, oCmd.Parameters["h_msg_text"]));
                    }

                    sOutput = mobjDataAccess.ParametersToXML(ref oCmd, true, "response");
                }
                else
                {
                    arrAreaCode = (oXMLDom.selectSingleNode("//data/area_code_no").text).Split(',');
                    arrExchange = (oXMLDom.selectSingleNode("//data/exchange_no").text).Split(',');
                    arrHousehold = (oXMLDom.selectSingleNode("//data/household_no").text).Split(',');
                    arrTollFreeCodes = (oXMLDom.selectSingleNode("//data/toll_free_cd").text).Split(',');

                    for (i = 0; i <= arrAreaCode.GetUpperBound(0); i++)
                    {
                        if (arrAreaCode[i].Trim() != "" && arrHousehold[i].Trim() != "" && arrExchange[i].Trim() != "")
                        {
                            oXMLNode = (oXMLDom.selectSingleNode(string.Concat("//serviceCenter/data/phoneNumbers/record[position() = '", i + 1, "']")));
                            if (oXMLNode != null)
                            {
                                sOldAreaCode = oXMLNode.attributes.getNamedItem("area_code_no").text;
                                sOldExchange = oXMLNode.attributes.getNamedItem("exchange_no").text;
                                sOldHousehold = oXMLNode.attributes.getNamedItem("household_no").text;
                                sOldTollFreeCode = oXMLNode.attributes.getNamedItem("toll_free_cd").text;
                                bUpdate = true;
                            }
                            else
                            {
                                sOldAreaCode = "";
                                sOldExchange = "";
                                sOldHousehold = "";
                                sOldTollFreeCode = "";
                                bUpdate = false;
                            }
                            if (sActionCode == "IINSERT")
                                sInsertType = sActionCode;
                            else
                                sInsertType = "INSERT";

                            if (sOldAreaCode != arrAreaCode[i].Trim() || sOldTollFreeCode != arrTollFreeCodes[i].Trim() || sOldExchange != arrExchange[i].Trim() || sOldHousehold != arrHousehold[i].Trim())
                            {
                                if (bUpdate)
                                {
                                    oCmd = null;
                                    oCmd = new Command();
                                    oCmd.ActiveConnection = oConnection;
                                    oCmd.CommandType = CommandTypeEnum.adCmdText;
                                    oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(?,?,?,?,?,?,?,?,?,?,?,?) }");
                                    oCmd.CommandTimeout = 90;
                                    oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, -1));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, "DELETE"));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, Convert.ToInt64(lInstallerUID)));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_area_code_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, sOldAreaCode));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_exchange_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, sOldExchange));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_household_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, sOldHousehold));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_phone_type_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, PHONE_TYPE_SERVICE_CENTER));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_toll_free_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, sOldTollFreeCode));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, "Y"));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sUserID));
                                    oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sUserID));
                                    oCmd.Execute(out objExecute);

                                    if (Convert.ToInt32(oCmd.Parameters["retval"]) != 0)
                                    {
                                        bLogError = true;
                                        if (Convert.ToInt32(oCmd.Parameters["retval"]) < 0)
                                            sInd = 1;
                                        else
                                            sInd = 4;

                                        throw new Exception(string.Concat(oCmd.Parameters["retval"], Constants.vbObjectError, sStoredProc, oCmd.Parameters["h_msg_text"]));
                                    }

                                }

                                oCmd = null;
                                oCmd = new Command();
                                oCmd.ActiveConnection = oConnection;
                                oCmd.CommandType = CommandTypeEnum.adCmdText;
                                oCmd.CommandText = string.Concat("{? = call lynxdba.", sStoredProc, "(?,?,?,?,?,?,?,?,?,?,?,?) }");
                                oCmd.CommandTimeout = 90;
                                oCmd.Parameters.Append(oCmd.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 0, -1));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_action_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sInsertType));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, Convert.ToInt64(lInstallerUID)));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_area_code_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, Convert.ToInt32(arrAreaCode[i].Trim())));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_exchange_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, Convert.ToInt32(arrExchange[i].Trim())));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_household_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, Convert.ToInt32(arrHousehold[i].Trim())));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_phone_type_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 0, 4));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_toll_free_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, Convert.ToString(arrTollFreeCodes[i].Trim())));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, "Y"));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 10, sUserID));
                                oCmd.Parameters.Append(oCmd.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sUserID));
                                oCmd.Execute(out objExecute);

                                if (Convert.ToInt32(oCmd.Parameters["retval"]) > 0)
                                {
                                    bLogError = true;
                                    sInd = 4;
                                }
                                else
                                    sInd = 1;

                                sOutput = string.Concat(sOutput, mobjDataAccess.ParametersToXML(ref oCmd, true, "response"));
                            }
                        }
                    }
                }

                sOutput = string.Concat("<ManageServiceCenterPhones>", sOutput, "</ManageServiceCenterPhones>");

                if (sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXMLNode = null;
                oXmlNodeList = null;
                oOutputRs = null;
            }

            return sOutput;
        }

        public string ManageServiceArea(string sXML, [Optional]string sStylesheet)
        {
            string sActionCode = string.Empty,
                   sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sSelectSql = string.Empty,
                   sInsertSQL = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty,
                   sUserID = string.Empty,
                   sSessionID = string.Empty,
                   sServiceInd = string.Empty,
                   sRepairInd = string.Empty,
                   sInShopInd = string.Empty,
                   sZip = string.Empty,
                   sFieldsSQL = string.Empty,
                   sValuesSQL = string.Empty;
            long lInstallerUID;
            Recordset oOutputRs = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNodeList oXmlNodeList = null;
            IXMLDOMParseError tmpParseError;
            try
            {
                oXMLDom = new DOMDocument40();
                mobjCommon = new lynxAccount4.modCommon();
                sTempTable = "so_service_area";
                sStoredProc = "dp_manage_so_service_area";
                oXMLDom.async = false;
                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                lInstallerUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);
                sSessionID = oXMLDom.selectSingleNode("//SessionID").text;
                sActionCode = oXMLDom.selectSingleNode("//ActionCode").text;
                oXmlNodeList = oXMLDom.selectNodes("//data/row");

                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, inst_uid_no, user_id_no, user_session_no, ",
                                              "mobile_service_ind, mobile_repair_ind, service_ind,zip_cd, commit_ind) VALUES");
                    sServiceInd = oXMLNode.attributes.getNamedItem("mobile_service_ind").text;
                    sRepairInd = oXMLNode.attributes.getNamedItem("mobile_repair_ind").text;
                    if (oXMLNode.attributes.getNamedItem("service_ind") != null)
                        sInShopInd = oXMLNode.attributes.getNamedItem("service_ind").text;

                    sZip = oXMLNode.attributes.getNamedItem("zip_cd").text;
                    sValuesSQL = string.Concat("('", sActionCode, "',", lInstallerUID, ",'", sUserID, "','", sSessionID, "','",
                                               sServiceInd, "','", sRepairInd, "','", sInShopInd, "','", sZip, "','Y')~");

                    sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                }

                sSelectSql = "*";
                sFilter = "";
                oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "response");

                if (sStylesheet != null)
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXmlNodeList = null;
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string GetServiceArea(string sXML, [Optional]string sStylesheet)
        {
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty,
                   lInstallerID = string.Empty,
                   sDirection = string.Empty,
                   sRadius = string.Empty,
                   sSessionID = string.Empty,
                   sActionCode = string.Empty,
                   lParentId = string.Empty,
                   strService = string.Empty,
                   strParam = string.Empty,
                   sHistoryDate = string.Empty;
            string cActionCode = "GET";
            Recordset oOutputRs = null;
            DOMDocument40 oxmlDom = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                modAGC2Common mobjAGC2Common = new modAGC2Common();
                oxmlDom = new DOMDocument40();
                objmodcommon = new modAGC2Common();
                oxmlDom.loadXML(sXML);

                if (Convert.ToString(oxmlDom.parseError.errorCode) != "0")
                {
                    tmpParseError = oxmlDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                strParam = "InstallerId";
                lInstallerID = objmodcommon.getNodeValue(ref oxmlDom, ref strParam);
                strParam = "sHistoryDate";
                sHistoryDate = objmodcommon.getNodeValue(ref oxmlDom, ref strParam);
                strParam = "Action";
                sActionCode = objmodcommon.getNodeValue(ref oxmlDom, ref strParam);

                if (sActionCode == "COMPANYZIPS")
                {
                    strParam = "ParentId";
                    lParentId = objmodcommon.getNodeValue(ref oxmlDom, ref strParam);
                    sTempTable = "so_company_service_zip";
                    sStoredProc = "dp_get_so_company_service_zip";
                    sInsertSQL = mobjDataAccess.BuildInsertSQL(sTempTable, "inst_uid_no,parent_uid_no", Convert.ToString(lInstallerID), ",", Convert.ToString(lParentId));
                    strService = mobjDataAccess.GetSessionXML(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, string.Concat("inst_uid_no <> ", Convert.ToString(lInstallerID)), "zip_cd,inst_nm,store_no", "", "CompanyZips").Replace("'", "");
                }
                else
                {
                    strParam = "SessionID";
                    sSessionID = objmodcommon.getNodeValue(ref oxmlDom, ref strParam);
                    sTempTable = "so_service_area";
                    sStoredProc = "dp_manage_so_service_area";
                    sInsertSQL = string.Concat("insert into session.", sTempTable, " (action_cd, inst_uid_no, user_session_no, select_dt)",
                                 " values ('", cActionCode, "',", lInstallerID, ",'", sSessionID, "', '", sHistoryDate, "')");
                    sSelectSql = "zip_cd,mobile_service_ind,mobile_repair_ind,service_ind,locked_ind,co_ind,msg_text,select_dt,modify_dt";
                    oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                    sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "row", "ServicedZips");
                    sSelectSql = "sum(population) as sum_population, sum(square_miles) as sum_square_miles, max(distance) as distance";
                    sFilter = " mobile_service_ind = 'Y' or mobile_repair_ind = 'Y' ";
                    oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                    sOutput = string.Concat(sOutput, mobjDataAccess.RsToXML(oOutputRs, true, "row", "sumdata"));
                    sOutput = string.Concat(sOutput, mobjAGC2Common.GetItemFromCache("inshop_service_area_min_miles", "MinMiles"));
                    sOutput = string.Concat(sOutput, mobjAGC2Common.GetItemFromCache("inshop_service_area_max_miles", "MaxMiles"));

                    if (sStylesheet != "")
                        sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string ManageServiceCenterProfileItems(string sXML, [Optional]string sStylesheet)
        {
            string sOutput = string.Empty,
                   sUserID = string.Empty;
            long lInstallerUID;
            object objExecute;
            Connection oConnection = null;
            Command oCommand = null;
            DOMDocument40 oXMLDom = null;

            IXMLDOMNodeList oXmlNodeList = null;
            try
            {
                oConnection = new Connection();
                oCommand = new Command();
                oXMLDom = new DOMDocument40();

                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);

                oXMLDom.loadXML(sXML);

                lInstallerUID = Convert.ToInt64(oXMLDom.selectSingleNode("//header/InstallerUID").text);
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);

                oCommand.ActiveConnection = oConnection;
                oCommand.ActiveConnection.BeginTrans();
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandText = "{? = call lynxdba.dp_update_profile_item (?,?,?,?,?,?,?) }";
                oCommand.CommandTimeout = 90;
                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "N"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_uid_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, sUserID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_attrib_value", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 50, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_attrib_nm", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_profile_object", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, "inst"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_profile_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, lInstallerUID));
                oXmlNodeList = oXMLDom.selectNodes("//data/row");

                foreach (IXMLDOMNode oXMLNode in oXmlNodeList)
                {
                    oCommand.Parameters["h_attrib_nm"].Value = oXMLNode.attributes.getNamedItem("name").text;
                    oCommand.Parameters["h_attrib_value"].Value = oXMLNode.attributes.getNamedItem("value").text;
                    oCommand.Execute(out objExecute);
                }

                oCommand.ActiveConnection.CommitTrans();
                sOutput = mobjDataAccess.ParametersToXML(ref oCommand);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }

        public string ManageServiceCenterStatus(string sXML)
        {
            long lInstallerUID;
            string sStatus = string.Empty,
                   sUserID = string.Empty,
                   strParam = string.Empty,
                   sOutput = string.Empty;
            int sInd;
            object objExecute;
            Connection oConnection = null;
            Command oCommand = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNodeList oXmlNodeList = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                mobjCommon = new lynxAccount4.modCommon();
                mobjDataAccess = new lynxAccount4.modDataAccess();
                objmodcommon = new modAGC2Common();
                oXMLDom = new DOMDocument40();
                oConnection.ConnectionTimeout = 60;
                oConnection.Open(lynxAccount4.modCommon.sIngresDSN);
                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                strParam = "InstallerUID";
                lInstallerUID = Convert.ToInt64(objmodcommon.getNodeValue(ref oXMLDom, ref strParam));
                strParam = "StatusCode";
                sStatus = objmodcommon.getNodeValue(ref oXMLDom, ref strParam);

                if (sStatus == "1")
                    sStatus = "A";

                if (sStatus != "A")
                    sStatus = "D";

                strParam = "UserID";
                sUserID = mobjCommon.PreFixUserId(objmodcommon.getNodeValue(ref oXMLDom, ref strParam));

                if (sUserID == "")
                    sUserID = "lynxweb";
                oCommand.ActiveConnection = oConnection;
                oCommand.CommandType = CommandTypeEnum.adCmdText;
                oCommand.CommandTimeout = 90;
                oCommand.Parameters.Append(oCommand.CreateParameter("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 0, lInstallerUID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_inst_stat_cd", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, Left((sStatus).Trim(), 1)));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y"));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, ""));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_id_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 32, sUserID));
                oCommand.Parameters.Append(oCommand.CreateParameter("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 32, sUserID));
                oCommand.CommandText = string.Concat("{? = call lynxdba.dp_activate_service_center (", mobjDataAccess.getQuestionMarks(oCommand.Parameters.Count), ") }");
                oCommand.Execute(out objExecute);

                if (Convert.ToInt32(oCommand.Parameters["retval"]) > 0)
                {
                    sInd = 4;
                    throw new Exception(string.Concat(oCommand.Parameters["retval"], "clsServiceCenter.ManageServiceCenterStatus", "Error from stored proc:", oCommand.Parameters["h_msg_text"]));
                }
                else
                {
                    sInd = 1;
                    sOutput = Convert.ToString(oCommand.Parameters["h_msg_text"].Value);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oCommand = null;
                oConnection = null;
            }
            return sOutput;
        }

        public string GetServiceCenterComments(string sXML, string sStylesheet)
        {
            string cACTION_CODE = "GET";
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sInsertSQL = string.Empty,
                   sSelectSql = string.Empty,
                   sOutput = string.Empty,
                   strParam = string.Empty,
                   sFilter = string.Empty,
                   sDropDownInfo = string.Empty;
            long lInstallerUID;
            int iGlassPgmID;
            Recordset oOutputRs = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                objmodcommon = new modAGC2Common();
                oXMLDom = new DOMDocument40();
                mobjCommon = new lynxAccount4.modCommon();
                oXMLDom.async = false;
                oXMLDom.loadXML(sXML);

                if (oXMLDom.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                strParam = "InstallerUID";
                lInstallerUID = Convert.ToInt64(objmodcommon.getNodeValue(ref oXMLDom, ref strParam));
                strParam = "GlassProgramID";
                iGlassPgmID = Convert.ToInt32(objmodcommon.getNodeValue(ref oXMLDom, ref strParam));
                sTempTable = "so_icmt";
                sStoredProc = "dp_manage_so_comments";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (inst_uid_no, action_cd,glass_admin_no )",
                                          " values (", lInstallerUID, ",'", cACTION_CODE, "',", iGlassPgmID, ")");
                sSelectSql = "*";
                sFilter = "1=1 order by modify_dt desc";
                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "comments");
                sDropDownInfo = objmodcommon.GetItemFromCache("comment_type_no", "comment_type");
                sOutput = string.Concat("<ServiceCenterComments>", sOutput, sDropDownInfo, "</ServiceCenterComments>");

                if (sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oXMLDom = null;

            }
            return sOutput;
        }

        public string ManageServiceCenterComments(string sXML, string sStylesheet = "")
        {
            string cACTION_CODE = "INSERT";
            string sStoredProc = string.Empty,
                   sTempTable = string.Empty,
                   sSelectSql = string.Empty,
                   sInsertSQL = string.Empty,
                   sFieldsSQL = string.Empty,
                   sValuesSQL = string.Empty,
                   sOutput = string.Empty,
                   sFilter = string.Empty,
                   lInstallerUID = string.Empty,
                   sUserID = string.Empty,
                   sCommentText = string.Empty;
            int iCommentNo, iGlassPgmID;
            Recordset oOutputRs = null;
            DOMDocument40 oXMLDom = null;
            IXMLDOMNode oXMLNode = null;
            IXMLDOMNodeList oXmlNodeList = null;
            try
            {
                oXMLDom = new DOMDocument40();
                sTempTable = "so_icmt";
                sStoredProc = "dp_manage_so_comments";
                oXMLDom.loadXML(sXML);
                lInstallerUID = oXMLDom.selectSingleNode("//header/InstallerUID").text;
                sUserID = mobjCommon.PreFixUserId(oXMLDom.selectSingleNode("//header/UserID").text);
                iGlassPgmID = Convert.ToInt32(oXMLDom.selectSingleNode("//GlassProgramID").text);
                sFieldsSQL = string.Concat("insert into session.", sTempTable, " (action_cd, inst_uid_no, user_id_no, user_session_no, ",
                                          "comment_text, comment_type_no, glass_admin_no, commit_ind) VALUES");
                sCommentText = oXMLDom.selectSingleNode("//data/comment_text").text;
                iCommentNo = Convert.ToInt32(oXMLDom.selectSingleNode("//data/comment_type_no").text);
                sValuesSQL = string.Concat("('", cACTION_CODE, "',", lInstallerUID, ",'", sUserID, "','", sUserID, "','",
                                  sCommentText, "',", iCommentNo, ",", iGlassPgmID, ",'Y')~");
                sInsertSQL = string.Concat(sInsertSQL, sFieldsSQL, sValuesSQL);
                sSelectSql = "*";
                sFilter = "";
                oOutputRs = mobjDataAccess.UpdateSessionRows2(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "response");
                if (sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
                oXMLNode = null;
                oXmlNodeList = null;
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        private string ConvertToIngresDate(string sDate)
        {
            string strOutput = string.Empty;
            try
            {
                DateTime now = DateTime.Now;
                strOutput = string.Concat(Right(sDate, 2), "-", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(Mid(sDate, 5, 2))), "-", Left(sDate, 4));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strOutput;
        }

        private void ObjectControl_Activate()
        {
            string sOutput = string.Empty;
            DOMDocument40 oXMLDom = null;
            IXMLDOMParseError tmpParseError = null;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                oXMLDom = new DOMDocument40();
                sOutput = mobjCommon.GetConfiguration("lynxAGC2");
                oXMLDom.async = false;
                oXMLDom.loadXML(sOutput);

                if (oXMLDom.parseError.errorCode == 0)
                {
                    //'retrieve the values here
                    lynxAccount4.modCommon.sCairDSN = oXMLDom.selectSingleNode("//DataSources/Cair").text;
                    lynxAccount4.modCommon.sIngresDSN = oXMLDom.selectSingleNode("//DataSources/Ingres").text;
                    lynxAccount4.modCommon.sStylesheetPath = oXMLDom.selectSingleNode("//Stylesheets").text;
                    lynxAccount4.modCommon.bActivateLog = Convert.ToBoolean(oXMLDom.selectSingleNode("//ActivateLog").text);
                    sXMLPath = oXMLDom.selectSingleNode("//Cache").text;
                }
                else
                {
                    tmpParseError = oXMLDom.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXMLDom = null;
            }
        }

        private bool ObjectControl_CanBePooled()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return true;
        }

        private void ObjectControl_Deactivate()
        {
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string GetServiceCenterDiscounts(string sRequestXML, string sStylesheet)
        {
            string sStoredProc = string.Empty,
                sTempTable = string.Empty,
                sInsertSQL = string.Empty,
                sSelectSql = string.Empty,
                sOutput = string.Empty,
                sFilter = string.Empty,
                sActionCode = string.Empty,
                sEffectiveDate = string.Empty,
                sUserID = string.Empty,
                sUserSession = string.Empty,
                strParam = string.Empty,
                sRootNode = string.Empty;
            Recordset oOutputRs = null;
            FreeThreadedDOMDocument40 oXMLIn = null;
            IXMLDOMParseError tmpParseError = null;
            DOMDocument40 objDomdocument = null;
            long lRegionID, lInstallerID;
            try
            {
                mobjDataAccess = new lynxAccount4.modDataAccess();
                mobjCommon = new lynxAccount4.modCommon();
                oXMLIn = new FreeThreadedDOMDocument40();
                objmodcommon = new modAGC2Common();
                oXMLIn.loadXML(sRequestXML);
                if (oXMLIn.parseError.errorCode != 0)
                {
                    tmpParseError = oXMLIn.parseError;
                    mobjCommon.InvalidXML(ref tmpParseError);
                }

                objDomdocument = (DOMDocument40)oXMLIn;
                strParam = "ActionCode";
                sActionCode = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "EffectiveDate";
                sEffectiveDate = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "RegionID";
                lRegionID = Convert.ToInt64(objmodcommon.getNodeValue(ref objDomdocument, ref strParam));
                strParam = "InstallerID";
                lInstallerID = Convert.ToInt64(objmodcommon.getNodeValue(ref objDomdocument, ref strParam));
                strParam = "UserID";
                sUserID = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "UserSession";
                sUserSession = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "Styelsheet";
                sStylesheet = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                strParam = "RootNode";
                sRootNode = objmodcommon.getNodeValue(ref objDomdocument, ref strParam);
                sTempTable = "so_retailer_discounts";
                sStoredProc = "dp_get_so_retailer_discounts";
                sInsertSQL = string.Concat("insert into session.", sTempTable, " (action_cd, commit_ind, eff_dt, ins_reg_uid_no, inst_uid_no, msg_ind, user_id_no, user_session_no)",
                             " values ('", sActionCode, "', 'Y', '", sEffectiveDate, "', ", lRegionID, ", ", lInstallerID, ", 'N', '", sUserID, "', '", sUserSession, "')");
                mobjCommon.LogToAppLog("lynxAGC2.GetServiceCenterDiscounts", sInsertSQL, "", 1, 21);
                sSelectSql = "*";
                sFilter = " 1=1 order by county_type_cd, pricing_type_cd ";
                oOutputRs = mobjDataAccess.GetSessionRows(sTempTable, sStoredProc, sInsertSQL, lynxAccount4.modCommon.sIngresDSN, sFilter, sSelectSql);
                sOutput = mobjDataAccess.RsToXML(oOutputRs, true, "record", sRootNode);

                if (sStylesheet != "")
                    sOutput = mobjDataAccess.TransformXML(sOutput, string.Concat(lynxAccount4.modCommon.sStylesheetPath, sStylesheet));

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oOutputRs.Close();
                oOutputRs = null;
            }
            return sOutput;
        }

        public string Left(string strParam, int intLength)
        {
            string strResult = strParam.Substring(0, intLength);
            return strResult;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string strParam, int intLength)
        {
            string strResult = strParam.Substring(strParam.Length - intLength, intLength);
            return strResult;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = strParam.Substring(startIndex, intLength);
            return strResult;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = strParam.Substring(intstartIndex);
            return strResult;
        }
    }
}
