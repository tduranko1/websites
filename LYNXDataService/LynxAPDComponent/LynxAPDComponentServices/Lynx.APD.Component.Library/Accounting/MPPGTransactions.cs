﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lynx.APD.Component.Library.PartnerDataMgr;
using MSXML2;
using System.Threading;

namespace Lynx.APD.Component.Library.Accounting
{
    /// <summary>
    ///  Module MPPGTransactions
    /// 
    ///  Code common to PPG transactions.
    /// </summary>
    class MPPGTransactions
    {
        #region Declarations
        private const string APP_NAME = "LAPDAccounting.";
        private string MODULE_NAME = string.Concat(APP_NAME, "MPPGTransactions.");
        MLAPDAccounting objMLAPDAccounting = null;
        #endregion
        #region Enumerators
        //Internal error codes for this module.
        private enum EventCodes : ulong
        {
            eElectronicPostBadStatus = 0x80065000 + 0x300
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// Syntax:      BuildTransactionForPPG
        /// 
        /// Parameters:  strTransactionType - A token identifying the transaction type.
        ///              strMessageBody - The XML to put within the wrapper.
        ///              strDestination - A token indentfying the message destination.
        ///              strReferenceId - An internal identifyer for the message.
        /// 
        /// Purpose:     Wraps the passed XML with a PPG SOAP style wrapper.
        /// 
        /// Returns:     The completed XML.
        /// 
        /// Requires:    Settings in the config.xml file.
        /// </summary>
        /// <param name="strTransactionType"></param>
        /// <param name="strMessageBody"></param>
        /// <param name="strDestination"></param>
        /// <param name="strReferenceId"></param>
        /// <returns></returns>
        public string BuildTransactionForPPG(string strTransactionType, string strMessageBody, string strDestination, string strReferenceId)
        {
            string PROC_NAME = string.Empty;
            string strTrans = string.Empty,
                 strEnvelopeId = string.Empty,
             strOriginationId = string.Empty,
             strDestinationId = string.Empty,
             strBuildTransaction = string.Empty;

            bool blnUseSOAP;
            bool blnUseCDATA;

            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "BuildTransactionForPPG: ");

                objMLAPDAccounting = new MLAPDAccounting();
                //Add parameters to trace data.
                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat("TransactionType = '" + strTransactionType + "'  Dest = '" + strDestination
                    + "'  RefId = '" + strReferenceId + "'"), string.Concat(PROC_NAME, "Started"));

                //Get SOAP and CDATA settings from configuration.
                blnUseSOAP = Convert.ToBoolean(objMLAPDAccounting.GetConfig("PPG_SOAP_Header/UseSOAP") == "True");
                blnUseCDATA = Convert.ToBoolean(objMLAPDAccounting.GetConfig("PPG_SOAP_Header/UseCDATA") == "True");

                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat("SOAP = '", blnUseSOAP, "'  CDATA = '", blnUseCDATA), string.Concat(PROC_NAME, "Config"));

                //This configuration setting allows us to disable the SOAP envelope if need be.
                if (!blnUseSOAP)
                    strBuildTransaction = strMessageBody;
                else
                {
                    //Get source and dest PIDs from the config file.

                    strOriginationId = objMLAPDAccounting.GetConfig("PPG_SOAP_Header/OriginationPID");
                    strDestinationId = objMLAPDAccounting.GetConfig("PPG_SOAP_Header/DestinationPID[@name='" + strDestination + "']");

                    //Generate the envelope id.
                    strEnvelopeId = string.Concat(strOriginationId, "#", strDestinationId, "#", strReferenceId, "#", DateTime.Now);

                    //Start building the transaction XML.
                    strTrans = "<StdEnvelope EnvelopeID='" + strEnvelopeId + "' Version='1.0'>";

                    //Add in the environment code.
                    strTrans = string.Concat(strTrans, "<Header><Delivery><Environment>", objMLAPDAccounting.GetConfig("@EnvironmentCode"), "</Environment>");

                    //Add in the origination company.
                    strTrans = string.Concat(strTrans, "<OriginationCompany>", objMLAPDAccounting.GetConfig("PPG_SOAP_Header/OriginationCompany"), "</OriginationCompany>");

                    //Add in the origination ID.
                    strTrans = string.Concat(strTrans, "<OriginationPID>", strOriginationId, "</OriginationPID>");

                    //Add in the destination company and Id.
                    strTrans = string.Concat(strTrans, "<DestinationCompany>", strDestination, "</DestinationCompany>"
                        , "<DestinationPID>", strDestinationId, "</DestinationPID>");

                    //Add in the transaction type.
                    strTrans = string.Concat(strTrans, "<TransactionType>", strTransactionType, "</TransactionType>");

                    //Add in the sender's reference Id.
                    strTrans = string.Concat(strTrans, "<SendersReferenceID>", strReferenceId, "</SendersReferenceID>");

                    //Add in the current time and date.
                    strTrans = string.Concat(strTrans, "<TransactionDateTime>", DateTime.Now, "</TransactionDateTime>");

                    //Close out the header, begin the message body.
                    strTrans = string.Concat(strTrans, "</Delivery></Header><MessageBody>");

                    //Add in CDATA tag if specified in configuration.
                    if (blnUseCDATA)
                        strTrans = string.Concat(strTrans, "<![CDATA[");

                    //Add in the message body.
                    strTrans = string.Concat(strTrans, strMessageBody);

                    //Close CDATA tag if specified in configuration.
                    if (blnUseCDATA)
                        strTrans = string.Concat(strTrans, "]]>");

                    //Close the message and envelope tags - we are done!
                    strTrans = string.Concat(strTrans, "</MessageBody></StdEnvelope>");

                    if (objMLAPDAccounting.g_blnDebugMode)
                        objMLAPDAccounting.g_objEvents.Trace(string.Concat("XML = '", strTrans), string.Concat(PROC_NAME, "Envelope"));

                    strBuildTransaction = strTrans;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strBuildTransaction;
        }

        /// <summary>
        /// Syntax:      XmlHttpPost
        /// Parameters:  strUrl - The URL to post to.
        ///              strXml - The XML data to POST.
        ///              g_objEvents - An instance of SiteUtilities::CEvents to use.
        /// Purpose:     Posts the passed data to a url.
        /// Returns:     The status.
        /// </summary>
        /// <param name="strUrl"></param>
        /// <param name="strXML"></param>
        /// <returns></returns>
        public long XmlHttpPost(string strUrl, string strXML)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "XmlHttpPost: ");

            XMLHTTP objXmlHttp = null;
            long lngStatus = 0;
            //Failover vars and constants
            int intFailOverCount = 0;
            int MaxFailoverAttempts = 10;
            int FailoverSleepPeriod = 1000;

            try
            {
                objMLAPDAccounting = new MLAPDAccounting();
                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace("URL = '" + strUrl + "'", PROC_NAME);

                objXmlHttp = new XMLHTTP();

                objXmlHttp.open("POST", strUrl, false);

                objXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                objXmlHttp.send(strXML);

                if (objXmlHttp.status != 200)
                    throw new Exception(string.Concat(EventCodes.eElectronicPostBadStatus, PROC_NAME, "HTTP Send",
                        "Bad status returned from post to ", strUrl));

                //Retry loop count below max so far?
                if (intFailOverCount < MaxFailoverAttempts)
                {
                    if (intFailOverCount == 0)
                    {
                        //Per TestTrack 1782 - Disable all error and warning emails that no longer hold relevance.
                        //Thus Reduced the following message from an event to a trace message.

                        if (objMLAPDAccounting.g_blnDebugMode)
                            objMLAPDAccounting.g_objEvents.Trace(string.Concat("A CRITICAL HTTP POSTING ERROR HAS OCCURRED", Environment.NewLine,
                                   "    ENTERING FAILOVER RETRY LOOP"), PROC_NAME);

                        intFailOverCount = 1;
                    }
                    else
                    {
                        intFailOverCount = intFailOverCount + 1;
                    }

                    //Pause to give time for cluster failover to complete.
                    Thread.Sleep(FailoverSleepPeriod);
                }
                else    // Give up!
                {
                    //Add note to log that the retry count has been exceeded.
                    objMLAPDAccounting.g_objEvents.Env(string.Concat("XmlHttpPost: MAXIMUM RETRY COUNT OF ", MaxFailoverAttempts, " HAS BEEN EXCEEDED!"));
                }

                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Empty, string.Concat(PROC_NAME, "HTTP Submit Successful!"));

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lngStatus;
        }
        #endregion

    }
}
