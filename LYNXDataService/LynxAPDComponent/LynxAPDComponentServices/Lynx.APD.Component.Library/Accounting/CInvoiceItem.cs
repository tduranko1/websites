﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSXML2;
using System.Collections;
using VBA;
using System.Runtime.InteropServices;

namespace Lynx.APD.Component.Library.Accounting
{
    class CInvoiceItem
    {
        Collection mcolInvoiceIDs = null;
        private string MODULE_NAME = "CInvoiceItem.";
        private long lngInvoiceID;
        private int mintInvoiceItemID;
        private long mlngClaimAspectID, mlngInvoiceID;
        private string mstrAuthorizingClientUserID = string.Empty,
                        mstrAmount = string.Empty,
                        mstrClientFeeCode = string.Empty,
                        mstrDispatchNumber = string.Empty,
                        mstrDeductible = string.Empty,
                        mstrInitialPayment = string.Empty,
                        mstrInvoiceDate = string.Empty,
                        mstrInvoiceDescription = string.Empty,
                        mstrItemType = string.Empty,
                        mstrPayeeID = string.Empty,
                        mstrPayeeName = string.Empty,
                        mstrPayeeCity = string.Empty,
                        mstrPayeeState = string.Empty,
                        mstrPayeeType = string.Empty,
                        mstrPayment = string.Empty,
                        mstrFee = string.Empty,
                        mstrAdminFee = string.Empty,
                        mstrTaxTotalAmt = string.Empty,
                        mstrLynxID = string.Empty,
                        mstrCarrierClaimNumber = string.Empty,
                        mstrComment = string.Empty,
                        mstrCarrierRepNameFirst = string.Empty,
                        mstrCarrierRepnameLast = string.Empty,
                        mstrInsuredNameFirst = string.Empty,
                        mstrInsuredNameLast = string.Empty,
                        mstrInsuredBusinessName = string.Empty,
                        mstrClaimantNameFirst = string.Empty,
                        mstrClaimantNameLast = string.Empty,
                        mstrClaimantBusinessName = string.Empty,
                        mstrServiceChannel = string.Empty;
        private bool mblnFirstIndemnityAssignment;

        public enum AmountType
        {
            ePayment,
            eFee
        }

        public int InvoiceItemID
        {
            get
            {
                return mintInvoiceItemID;
            }
            set
            {
                mintInvoiceItemID = value;
            }
        }
        public Collection InvoiceIDs
        {
            get
            {
                return mcolInvoiceIDs;
            }

        }
        public string Claimant
        {
            get
            {
                if (mstrClaimantBusinessName.Length > 0)
                    return mstrClaimantBusinessName;
                else
                    return string.Concat(mstrClaimantNameFirst.Trim(), "", mstrClaimantNameLast.Trim());
            }

        }


        public string AuthorizingClientUserID
        {
            get
            {
                return mstrAuthorizingClientUserID;
            }
            set
            {
                mstrAuthorizingClientUserID = value;
            }
        }

        public string Amount
        {
            get
            {
                return mstrAmount;
            }
            set
            {
                mstrAmount = value;
            }
        }

        public string ClientFeeCode
        {
            get
            {
                return mstrClientFeeCode;
            }
            set
            {
                mstrClientFeeCode = value;

            }
        }

        public string InvoiceDate
        {
            get
            {
                return mstrInvoiceDate;
            }
            set
            {
                mstrInvoiceDate = value;

            }
        }

        public string InvoiceDescription
        {
            get
            {
                if (mstrItemType == "Fee" || mstrPayeeName.Length == 0)
                    return mstrInvoiceDescription;
                else
                    return string.Concat(mstrPayeeName, " - ", mstrInvoiceDescription, " Claim");

            }
            set
            {
                mstrInvoiceDescription = value;
            }
        }

        public string LynxID
        {
            get
            {
                return mstrLynxID;
            }
            set
            {
                mstrLynxID = value;
            }
        }
        public string PayeeID
        {
            get
            {
                return mstrPayeeID;
            }
            set
            {
                mstrPayeeID = value;
            }
        }

        public string Payment
        {
            get
            {
                return mstrPayment;
            }
            set
            {
                mstrPayment = value;

            }
        }

        public string ClaimNumber
        {
            get
            {
                return mstrCarrierClaimNumber;
            }
            set
            {
                mstrCarrierClaimNumber = value;
            }
        }

        public string Comment
        {
            get
            {
                return mstrComment;
            }
            set
            {
                mstrComment = value;
            }
        }

        public string PayeeName
        {
            get
            {
                return mstrPayeeName;
            }
            set
            {
                mstrPayeeName = value;

            }
        }
        public string PayeeCity
        {
            get
            {
                return mstrPayeeCity;
            }
            set
            {
                mstrPayeeCity = value;
            }
        }
        public string PayeeState
        {
            get
            {
                return mstrPayeeState;
            }
            set
            {
                mstrPayeeState = value;
            }
        }

        public string PayeeType
        {
            get
            {
                return mstrPayeeType;
            }
            set
            {
                mstrPayeeType = value;

            }
        }
        public string Fee
        {
            get
            {
                return mstrFee;
            }
            set
            {
                mstrFee = value;
            }
        }
        public string adminFee
        {
            get
            {
                return mstrAdminFee;
            }
            set
            {
                mstrAdminFee = value;
            }
        }
        public string DeductibleAmt
        {
            get
            {
                return mstrDeductible;
            }

        }
        public string TaxTotalAmt
        {
            get
            {
                return mstrTaxTotalAmt;
            }
            set
            {
                mstrTaxTotalAmt = value;

            }
        }
        public string InsuredNameFirst
        {
            get
            {
                return mstrInsuredNameFirst;
            }
            set
            {
                mstrInsuredNameFirst = value;
            }
        }
        public string InsuredNameLast
        {
            get
            {
                return mstrInsuredNameLast;
            }
            set
            {
                mstrInsuredNameLast = value;
            }
        }
        public string InsuredBusinessName
        {
            get
            {
                return mstrInsuredBusinessName;
            }
            set
            {
                mstrInsuredBusinessName = value;

            }
        }
        public string CarrierRepNameFirst
        {
            get
            {
                return mstrCarrierRepNameFirst;
            }
            set
            {
                mstrCarrierRepNameFirst = value;
            }
        }
        public string CarrierRepNameLast
        {
            get
            {
                return mstrCarrierRepnameLast;
            }
            set
            {
                mstrCarrierRepnameLast = value;
            }
        }
        public string DispatchNumber
        {
            get
            {
                return mstrDispatchNumber;
            }
            set
            {
                mstrDispatchNumber = value;

            }
        }
        public long FirstInvoiceID
        {
            get
            {
                return Convert.ToInt64(mcolInvoiceIDs.Item(1));
            }

        }
        public long AddInvoiceID
        {
            set
            {
                mcolInvoiceIDs.Add(lngInvoiceID);
            }
        }
        public long ClaimAspectID
        {
            get
            {
                return mlngClaimAspectID;
            }
            set
            {
                mlngClaimAspectID = value;
            }
        }

        public bool HandlingFeeGoesHere
        {
            get
            {
                return mblnFirstIndemnityAssignment;
            }
        }


        private void Class_Initialize()
        {
            try
            {
                mcolInvoiceIDs = new Collection();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Class_Terminate()
        {
            try
            {
                mcolInvoiceIDs = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Store the initial property values for the InvoiceItem.
        /// </summary>
        public void Initialize(int intInvoiceItemID, long lngClaimAspectID, string strItemType, string strLynxID, string strCarrierClaimNumber,
                               string strCarrierRepNameFirst, string strCarrierRepNameLast, string strComment, string strDeductible,
                               string strInsuredNameFirst, string strInsuredNameLast, string strInsuredBusinessName, string strClaimantNameFirst,
                               string strClaimantNameLast, string strClaimantBusinessName, string strAuthorizingClientUserID, string strAmount, string strAdminAmount,
                               string strPayeeID, string strPayeeName, string strPayeeCity, string strPayeeState, string strDispatchNumber, string strInvoiceDescription,
                               long lngInvoiceID, string strClientFeeCode, string strInvoiceDate, string strPayeeType, string strServiceChannel,
                               string strInitialPayment, string strTaxTotalAmt, [Optional]bool blnLatestIndemnityAssignment)
        {
            string PROC_NAME = "Initialize";
            mintInvoiceItemID = intInvoiceItemID;
            mlngClaimAspectID = lngClaimAspectID;
            mlngInvoiceID = lngInvoiceID;
            mstrAuthorizingClientUserID = strAuthorizingClientUserID;
            mstrAmount = strAmount;
            mstrDeductible = strDeductible;
            mstrDispatchNumber = strDispatchNumber;
            mstrLynxID = strLynxID;
            mstrCarrierClaimNumber = strCarrierClaimNumber;
            mstrComment = strComment;
            mstrInsuredNameFirst = strInsuredNameFirst;
            mstrInsuredNameLast = strInsuredNameLast;
            mstrInsuredBusinessName = strInsuredBusinessName;
            mstrClaimantNameLast = strClaimantNameLast;
            mstrClaimantNameFirst = strClaimantNameFirst;
            mstrClaimantBusinessName = strClaimantBusinessName;
            mstrCarrierRepNameFirst = strCarrierRepNameFirst;
            mstrCarrierRepnameLast = strCarrierRepNameLast;
            mstrInvoiceDescription = strInvoiceDescription;
            mstrClientFeeCode = strClientFeeCode;
            mblnFirstIndemnityAssignment = blnLatestIndemnityAssignment;
            mstrPayeeID = strPayeeID;
            mstrInvoiceDate = strInvoiceDate;
            mstrItemType = strItemType;
            mstrPayeeType = strPayeeType;
            mstrServiceChannel = strServiceChannel;
            mstrTaxTotalAmt = strTaxTotalAmt;

            //Deductible is applied on initial payment only.  This is a fix for WAG but should apply to all other clients as well.

            try
            {
                if (strItemType == "Fee")
                {
                    mstrPayment = "0.00";
                    mstrFee = strAmount;
                    mstrAdminFee = strAdminAmount;
                }
                else
                {
                    mstrFee = "0.00";
                    mstrAdminFee = "0.00";
                    mstrPayment = strAmount;
                    mstrPayeeName = strPayeeName;
                    mstrPayeeCity = strPayeeCity;
                    mstrPayeeState = strPayeeState;
                }
                mcolInvoiceIDs.Add(lngInvoiceID);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        ///  Add the passed amount to this InvoiceItem and add the InvoiceID to the collection.
        /// </summary>
        /// <param name="strAmount"></param>
        public bool Add(string strAmount, string strAdminAmount, string strDeductibleAmt, string strTaxTotalAmt, AmountType sType, long lngInvoiceID, string strInitialPayment)
        {
            //Initialize return value.
            bool blnAdd = false;
            int lenmstrpayment;
            try
            {
                // Set the Fee or Payment amount.
                if (sType == AmountType.eFee)
                {
                    mstrFee = strAmount;
                    mstrAdminFee = strAdminAmount;
                    mstrDeductible = strDeductibleAmt;
                    mstrTaxTotalAmt = strTaxTotalAmt;
                }
                else
                {
                    //Format the Payment amount for display.
                    mstrPayment = Convert.ToString(Convert.ToDouble(mstrPayment) + Convert.ToDouble(strAmount));
                    lenmstrpayment = mstrPayment.Length - 1;

                    if (mstrPayment.IndexOf('.') == 0)
                        mstrPayment = string.Concat(mstrPayment, ".00");
                    else if (mstrPayment.IndexOf('.') == lenmstrpayment)
                        mstrPayment = string.Concat(mstrPayment, "0");
                    //switch (mstrPayment.IndexOf('.'))
                    //{
                    //    case 0:
                    //        mstrPayment = string.Concat(mstrPayment,".00");
                    //        break;
                    //    case 1:
                    //        mstrPayment = string.Concat(mstrPayment,"0");
                    //        break;
                    //    default:
                    //        break;
                    //}
                }

                if (strInitialPayment == "true")
                    mstrInitialPayment = Convert.ToString(true);

                //Redundant value used by some functions.
                mstrAmount = mstrPayment;

                //Add the passed invoice id to the collection.
                mcolInvoiceIDs.Add(lngInvoiceID);

                //Set the return to success.
                blnAdd = true;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return blnAdd;
        }

        /// <summary>
        /// Construct an XML node from this InvoiceItem's property values appopriate for
        ///  use in the XML source that will be sent to GLAXIS for electronic billng.
        /// </summary>
        /// <returns></returns>
        public string GetBillingFileXML()
        {
            string strXML = string.Empty;
            DOMDocument oXML = null;
            IXMLDOMElement oInvNode = null;
            try
            {
                oXML.async = false;
                oXML.loadXML("<Invoice/>");
                strXML = "";

                oInvNode = (IXMLDOMElement)(oXML.selectSingleNode("/Invoice"));

                if (oInvNode != null)
                {
                    oInvNode.setAttribute("AuthorizingClientUserID", mstrAuthorizingClientUserID);
                    oInvNode.setAttribute("AmountFee", mstrFee);
                    oInvNode.setAttribute("AdminAmountFee", mstrAdminFee);
                    oInvNode.setAttribute("AmountPayment", mstrPayment);
                    oInvNode.setAttribute("ClientFeeCode", mstrClientFeeCode);
                    oInvNode.setAttribute("Deductible", mstrDeductible);
                    oInvNode.setAttribute("TaxTotalAmt", mstrTaxTotalAmt);
                    oInvNode.setAttribute("InitialPayment", mstrInitialPayment);
                    oInvNode.setAttribute("DispatchNumber", mstrDispatchNumber);
                    oInvNode.setAttribute("InvoiceDate", DateTime.Today.ToString());
                    oInvNode.setAttribute("InvoiceDescription", this.InvoiceDescription);
                    oInvNode.setAttribute("PayeeName", mstrPayeeName);
                    oInvNode.setAttribute("PayeeCity", mstrPayeeCity);
                    oInvNode.setAttribute("PayeeState", mstrPayeeState);
                    oInvNode.setAttribute("ServiceChannel", mstrServiceChannel);
                    strXML = oXML.xml;

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXML = null;
                oInvNode = null;
            }
            return strXML;
        }

        /// <summary>
        /// Construct an XML node from this InvoiceItem's property values appopriate for
        /// creating a paper Invoice document.
        /// </summary>
        /// <returns></returns>
        public string GetInvoiceDocumentXML()
        {
            string strXML = string.Empty;
            DOMDocument oXML = null;
            IXMLDOMElement oInvNode = null;
            try
            {
                oXML.async = false;
                oXML.loadXML("<Invoice/>");
                strXML = "";
                oInvNode = (MSXML2.IXMLDOMElement)oXML.selectSingleNode("/Invoice");
                if (oInvNode != null)
                {
                    oInvNode.setAttribute("AuthorizingClientUserID", mstrAuthorizingClientUserID);
                    oInvNode.setAttribute("Amount", mstrAmount);
                    oInvNode.setAttribute("ClientFeeCode", mstrClientFeeCode);
                    oInvNode.setAttribute("DispatchNumber", mstrDispatchNumber);
                    oInvNode.setAttribute("InvoiceDate", DateTime.Today.ToString());
                    oInvNode.setAttribute("InvoiceDescription", this.InvoiceDescription);
                    oInvNode.setAttribute("PayeeName", mstrPayeeName);
                    oInvNode.setAttribute("PayeeCity", mstrPayeeCity);
                    oInvNode.setAttribute("PayeeState", mstrPayeeState);
                    strXML = oXML.xml;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oXML = null;
                oInvNode = null;
            }
            return strXML;
        }
    }
}

