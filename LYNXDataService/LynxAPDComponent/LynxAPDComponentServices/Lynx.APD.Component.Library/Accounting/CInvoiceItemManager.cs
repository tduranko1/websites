﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MSXML2;
using System.Xml;
using System.Collections;
using VBA;
using System.Text.RegularExpressions;
using DataAccessor;
using System.Reflection;
using ADODB;
using System.Configuration;

namespace Lynx.APD.Component.Library.Accounting
{
    /// <summary>
    /// Module    : CInvoiceItemManager
    /// DateTime  : 3/21/2005 14:19
    /// Author    : Dan Price
    /// Purpose   : This is a collection object whose purpose is to build and maintain a collection
    ///             of InvoiceItem objects. This object implements the business rules governing how
    ///             InvoiceItems are constructed from Invoice records from the APD database.  A
    ///             typical InvoiceItem object may be composed of several Invoice records including
    ///             multiple Payment records and a Handling Fee record.  This object also provides
    ///             the ability to ship the InvoiceItems, once constructed, to Ingres and sync the
    ///             APD database to Ingres with the returned Dispatch numbers.
    /// </summary>
    class CInvoiceItemManager
    {
        #region Declarations
        private string MODULE_NAME = "clsInvoiceItemManager.";

        private Collection mcolInvoiceItems;
        private DOMDocument40 mobjParamDOM;
        private CInvoiceItem mobjHandlingFeeInvoiceItem;

        private string mstrInitialPayment;
        private string mstrInsuranceCompanyID;
        private string mstrEarlyBillFlag;
        private string mstrReleaseFundInd;
        private string mstrWarrantyExistsFlag;
        private string mstrAmount;
        private string mstrAdminAmount;
        private string mstrIngresAccountingID;
        private long mlngInvoiceID;
        private string mstrPayeeID;
        private string mstrPayeeName;
        private string mstrPayeeType;
        private string mstrItemType;
        private string mstrNTUserID;
        private string mstrUserID;
        private string mstrLynxID;
        private string mstrCarrierClaimNumber;
        private string mstrComment;
        private string mstrInsuredNameFirst;
        private string mstrInsuredNameLast;
        private string mstrInsuredBusinessName;
        private string mstrClaimantNameFirst;
        private string mstrClaimantNameLast;
        private string mstrClaimantBusinessName;
        private string mstrCarrierRepNameFirst;
        private string mstrCarrierRepnameLast;
        private string mstrHandlingPayeeID;
        private string mstrAPDPayeeID;
        private long mlngClaimAspectID;
        private double mdblInvoiceTotal;
        private int mintInvoiceItemID;
        private int mintInvoiceType;
        private bool mblnHandlingFeeIndemnityFlagged;
        private string mstrDeductibleAmt;
        private string mstrTaxTotalAmt;

        MDomUtils objMdomUtils = null;
        CDataAccessor objIngresAccessor = null;
        MLAPDAccounting objMLAPDAccounting = null;
        #endregion

        #region Enumerators
        public enum eInvoiceType : ulong
        {
            eFile,
            ePaper
        }
        #endregion

        #region Public Functions


        /// <summary>
        /// 
        /// </summary>
        /// <param name="strParamXML"></param>
        /// <param name="intInvoiceType"></param>
        public void Initialize(string strParamXML, eInvoiceType intInvoiceType)
        {
            string PROC_NAME = string.Empty;

            // Objects that need to be destroyed.
            XmlNodeList objExposureList = null;
            XmlNodeList objInvoiceList = null;

            CInvoiceItem objInvoiceItem = null;
            IXMLDOMElement objElem = null;
            // Primitives
            string strDispatchNumber;
            string strPayeeName;
            string strPayeeAddress;
            string strPayeeCity;
            string strPayeeState;
            bool blnPaymentAdded;
            bool blnLastIndemnityAssignment;
            bool blnHandlingFeeAdded;
            bool blnClaimAspectPaymentExists;

            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "Initialize");
                objMdomUtils = new MDomUtils();
                objMLAPDAccounting = new MLAPDAccounting();
                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat(
            " Param XML = ", strParamXML, Environment.NewLine,
            PROC_NAME, " Started"));

                // Initialization.
                mintInvoiceType = Convert.ToInt32(intInvoiceType);
                mdblInvoiceTotal = 0;
                mintInvoiceItemID = 0;

                // Load the parameter XML into a DOM object
                objMdomUtils.LoadXml(ref mobjParamDOM, strParamXML, PROC_NAME, "Invoice");

                // Retrieve Root level attributes.
                mstrNTUserID = mobjParamDOM.selectSingleNode("//@NTUserID").text;
                mstrUserID = mobjParamDOM.selectSingleNode("//@UserID").text;
                mstrIngresAccountingID = mobjParamDOM.selectSingleNode("//@IngresAccountingID").text;
                mstrInsuranceCompanyID = mobjParamDOM.selectSingleNode("//@InsuranceCompanyID").text;
                mstrEarlyBillFlag = mobjParamDOM.selectSingleNode("//@EarlyBillFlag").text;
                mstrWarrantyExistsFlag = mobjParamDOM.selectSingleNode("//@WarrantyExistsFlag").text;
                mstrReleaseFundInd = mobjParamDOM.selectSingleNode("//@ReleaseFundsFlag").text;
                mstrReleaseFundInd = (mstrReleaseFundInd == "1" ? "Y" : "N");

                // Loop through each Claim node.
                foreach (XmlNode objClaimNode in mobjParamDOM.selectNodes("/Root/Claim"))
                {
                    // Retrieve Claim level attributes.
                    mstrLynxID = objClaimNode.SelectSingleNode("./@LynxID").InnerText;
                    mstrCarrierClaimNumber = objClaimNode.SelectSingleNode("./@CarrierClaimNumber").InnerText;
                    mstrInsuredNameFirst = objClaimNode.SelectSingleNode("./@InsuredNameFirst").InnerText;
                    mstrInsuredNameLast = objClaimNode.SelectSingleNode("./@InsuredNameLast").InnerText;
                    mstrInsuredBusinessName = objClaimNode.SelectSingleNode("./@InsuredBusinessName").InnerText;
                    mstrCarrierRepNameFirst = objClaimNode.SelectSingleNode("./@CarrierRepNameFirst").InnerText;
                    mstrCarrierRepnameLast = objClaimNode.SelectSingleNode("./@CarrierRepNameLast").InnerText;

                    // Retrieve Exposure nodes.
                    objExposureList = objClaimNode.SelectNodes("./Exposure");

                    // Loop through each Exposure node.
                    foreach (XmlNode objExposureNode in objExposureList)
                    {
                        // Ensure no Handling Fee is hanging around from a previous Exposure.
                        mobjHandlingFeeInvoiceItem = null;

                        // Retrieve Exposure level properties.
                        mstrClaimantNameFirst = objExposureNode.SelectSingleNode("./@ClaimantNameFirst").InnerText;
                        mstrClaimantNameLast = objExposureNode.SelectSingleNode("./@ClaimantNameLast").InnerText;
                        mstrClaimantBusinessName = objExposureNode.SelectSingleNode("./@ClaimantBusinessName").InnerText;
                        mstrComment = objExposureNode.SelectSingleNode("./@Vehicle").InnerText.ToUpper();
                        mstrHandlingPayeeID = objExposureNode.SelectSingleNode("./@HandlingShopLocationID").InnerText;
                        mstrAPDPayeeID = objExposureNode.SelectSingleNode("./@APDShopLocationID").InnerText;
                        mlngClaimAspectID = Convert.ToInt64(objExposureNode.SelectSingleNode("./@ClaimAspectID").InnerText);

                        // Reinitialize for each exposure. Indicates whether at least 1 indemnity payment exists for this exposure.
                        blnClaimAspectPaymentExists = false;

                        // Reinitialize for each exposure. Indicates whether an indemnity to which the handling fee should be attached has been located and flagged.
                        mblnHandlingFeeIndemnityFlagged = false;

                        // Retrieve the Invoice nodes.
                        objInvoiceList = objExposureNode.SelectNodes("./Invoice");

                        // Loop through the Invoice nodes.
                        foreach (XmlNode objInvoiceNode in objInvoiceList)
                        {
                            // Retrieve Invoice level properties.
                            mstrItemType = objInvoiceNode.SelectSingleNode("./@ItemType").InnerText;
                            mstrAmount = objInvoiceNode.SelectSingleNode("./@Amount").InnerText;
                            mstrAdminAmount = objInvoiceNode.SelectSingleNode("./@AdminFeeAmount").InnerText;
                            mstrDeductibleAmt = objInvoiceNode.SelectSingleNode("./@DeductibleAmt").InnerText;
                            mstrTaxTotalAmt = objInvoiceNode.SelectSingleNode("./@TaxTotalAmt").InnerText;
                            mlngInvoiceID = Convert.ToInt64(objInvoiceNode.SelectSingleNode("./@InvoiceID").InnerText);
                            mstrPayeeID = objInvoiceNode.SelectSingleNode("./@PayeeID").InnerText;
                            mstrPayeeName = objInvoiceNode.SelectSingleNode("./@PayeeName").InnerText;
                            mstrPayeeType = objInvoiceNode.SelectSingleNode("./@PayeeType").InnerText;
                            mstrInitialPayment = objInvoiceNode.SelectSingleNode("./@InitialPayment").InnerText;

                            // Process Payments.
                            if (mstrItemType != "Fee")
                            {
                                // Initialize value that determines whether payment was added to an existing InvoiceItem or should constitute a new InvoiceItem.
                                blnPaymentAdded = false;

                                // If there is an existing InvoiceItem for this Payee add the payment amount to that InvoiceItem.
                                blnPaymentAdded = AddToExistingInvoiceItem();

                                // If the Payment amount has not already been added to an existing InvoiceItem.
                                if (!blnPaymentAdded)
                                {
                                    if (objMLAPDAccounting.g_blnDebugMode)
                                        objMLAPDAccounting.g_objEvents.Trace(string.Concat("Adding Payment as new line item/dispatch for Claim Aspect: ", Convert.ToString(mlngClaimAspectID)));

                                    // Create a new InvoiceItem from the InvoiceNode
                                    objInvoiceItem = CreateInvoiceItem(objInvoiceNode);

                                    // Add the new InvoiceItem to the InvoiceItems collection.
                                    AppendInvoiceItem(objInvoiceItem);

                                }

                                // This indicates at least one payment exists for this exposure.
                                blnClaimAspectPaymentExists = true;
                            }
                            // Process Fees.
                            else
                            {
                                // Create a new InvoiceItem from the InvoiceNode
                                objInvoiceItem = CreateInvoiceItem(objInvoiceNode);

                                // Determine if this Fee is a Handling Fee.
                                if (objInvoiceNode.SelectSingleNode("./@FeeCategoryCD").InnerText == "H")
                                {
                                    if (objMLAPDAccounting.g_blnDebugMode)
                                        objMLAPDAccounting.g_objEvents.Trace(string.Concat(" Handling Fee found for Claim Aspect: ", objInvoiceItem.ClaimAspectID, ".  Making copy to be attached to indemnity."));

                                    // Save a copy of the Handling Fee to be added later to most recent shop payee's Indemnity Payment.
                                    // Exactly 1 handling fee is required.
                                    mobjHandlingFeeInvoiceItem = CopyInvoiceItem(objInvoiceItem);
                                }
                                else
                                {
                                    if (objMLAPDAccounting.g_blnDebugMode)
                                        objMLAPDAccounting.g_objEvents.Trace(string.Concat("Non-Handling Fee found for Claim Aspect: ", objInvoiceItem.ClaimAspectID,
                                                                            ".  Adding fee as separate line item/dispatch."));
                                    //' Add the new InvoiceItem to the InvoiceItems collection.
                                    AppendInvoiceItem(objInvoiceItem);
                                }
                            }
                        }
                        // This attribute is required when creating a paper Invoice document.
                        if (objInvoiceItem != null)
                        {
                            IXMLDOMElement objXmlElemAppend = (IXMLDOMElement)objExposureNode;
                            objMdomUtils.AddAttribute(ref objXmlElemAppend, "Claimant", objInvoiceItem.Claimant);
                        }
                        // If there is a handling fee, it must be added to an existing payment if appropriate or added to
                        // the collection as a separate InvoiceItem.
                        if (mobjHandlingFeeInvoiceItem != null)
                        {
                            blnHandlingFeeAdded = false;

                            // Find last indemnity payee and add the handling fee info.
                            foreach (CInvoiceItem objInvoice in mcolInvoiceItems)
                            {
                                // Limit this search to InvoiceItems for the current ClaimAspectID.
                                if (objInvoice.ClaimAspectID == mlngClaimAspectID)
                                {
                                    // One payee should be flagged as the last Shop to be payed an Indemnity.
                                    if (objInvoiceItem.HandlingFeeGoesHere)
                                    {
                                        if (objMLAPDAccounting.g_blnDebugMode)
                                            objMLAPDAccounting.g_objEvents.Trace(string.Concat("Adding handling fee to line item containing payment to Payee ID: ", objInvoice.PayeeID));

                                        // If found add the Handling fee.
                                        objInvoice.Fee = mobjHandlingFeeInvoiceItem.Fee;
                                        objInvoice.adminFee = mobjHandlingFeeInvoiceItem.adminFee;
                                        objInvoice.TaxTotalAmt = mobjHandlingFeeInvoiceItem.TaxTotalAmt;

                                        // Also add the Handling Fee's InvoiceID to the InvoiceItems's InvoiceIDs collection.
                                        objInvoice.AddInvoiceID = mobjHandlingFeeInvoiceItem.FirstInvoiceID;

                                        // Set the ClientFeeCode from the fee item
                                        objInvoice.ClientFeeCode = mobjHandlingFeeInvoiceItem.ClientFeeCode;

                                        // Set the flag to indicate the Handling fee has been handled.
                                        blnHandlingFeeAdded = true;

                                        // This ride is making me dizzy, let me off.
                                    }
                                }
                            }

                            // If no indemnity payment could be found to attach this Handling fee to...
                            if (!blnHandlingFeeAdded)
                            {
                                if (objMLAPDAccounting.g_blnDebugMode)
                                    objMLAPDAccounting.g_objEvents.Trace(string.Concat("Handling fee being added as separate line item/dispatch.  ", "InvoiceID: ", mobjHandlingFeeInvoiceItem.FirstInvoiceID));

                                // ... then add the Handling fee as a separate item.
                                AppendInvoiceItem(mobjHandlingFeeInvoiceItem);

                                // If there is at least 1 payment for this exposure, then the handling fee should have been attached to it.  Find out why it was not.
                                if (blnClaimAspectPaymentExists)
                                    objMLAPDAccounting.g_objEvents.HandleEvent(Convert.ToInt32(MLAPDAccounting.LAPDAccounting_ErrorCodes.eHandlingFeeError), PROC_NAME, string.Concat("Handling fee was not attached to an indemnity payment. ",
                                                    "Developer Note:  This can occur when there are no payments or only Expense payments, or when an indemnity payment ",
                                                    "was not made to the shop marked as the first shop to receive an indemnity payment for this vehicle. ", Environment.NewLine,
                                                    "LynxID: ", mobjHandlingFeeInvoiceItem.LynxID, " ClaimAspectID: ", mobjHandlingFeeInvoiceItem.ClaimAspectID,
                                                    " InvoiceID: ", mobjHandlingFeeInvoiceItem.FirstInvoiceID, " InvoiceDescription: ",
                                                    mobjHandlingFeeInvoiceItem.InvoiceDescription), "", "", false);
                            }
                        }
                    }

                }
                // This attribute is required when creating a paper Invoice document.
                objElem = mobjParamDOM.documentElement;
                objMdomUtils.AddAttribute(ref objElem, "TotalInvoiceAmount", GetCurrencyFormattedString(mdblInvoiceTotal));
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        /// <summary>
        ///  Procedure : SendToIngres
        ///  DateTime  : 3/16/2005 09:29
        ///  Author    : Dan Price
        ///  Purpose   : Send each InvoiceItem in the collection to Ingres, retrieve a Dispatch number,
        ///              update the APD Invoice records with the Dispatch number, and update the
        ///              InvoiceItem object with the Dispatch number.  This ensures that the APD
        ///              database is in sync with Ingres and that the Dispatch number will be available
        ///              when an Invoice file or document is created.
        /// </summary>
        /// <returns></returns>
        internal int SendToIngres()
        {
            string PROC_NAME = string.Empty;

            // Objects that must be destroyed.
            Collection colInvoiceIDs = null;
            // Primitives
            long lngNum;
            string strSrc = string.Empty;
            string strDesc = string.Empty;
            string strReturnValue = string.Empty;
            string strDispatchTemp = string.Empty;
            string strMessageText = string.Empty;
            string strMessageInd = string.Empty;
            string strUpdateDispatchProc = string.Empty;
            string strReleaseFundsUpdateDispatchProc = string.Empty;
            string strNowSqlDateTime = string.Empty;
            string strLynxID = string.Empty;
            string strPayeeID = string.Empty;
            string strPayeeName = string.Empty;
            string strPayment = string.Empty;
            string strClaimNumber = string.Empty;
            string strComment = string.Empty;
            string strDispatchNumber = string.Empty;
            string strFee = string.Empty;
            string strRetailerFee = string.Empty;
            string strInsuredNameFirst = string.Empty;
            string strInsuredNameLast = string.Empty;
            string strInsuredBusinessName = string.Empty;
            string strCarrierRepName = string.Empty;
            string strIngresProc = string.Empty;
            string strDeductibleAmt = string.Empty;
            string strTaxTotalAmt = string.Empty;
            string strReleaseFundAmt = string.Empty;
            string strWarrantyInd = string.Empty;
            string strHistoryComments = string.Empty;
            string strIngresErrorCode = string.Empty;
            string strIngresMessageText = string.Empty;
            long lngIngresErrorNumber;

            bool blnIngresTransaction = false;
            bool blnAPDTransaction = false;
            int intReturnVal = 0;
            bool blnIngresErrorElectronicBilling = false;

            DOMDocument40 objIngresParams = null;
            IXMLDOMElement objIngresParamsRoot = null;
            string strImageRoot = string.Empty;
            string strFileName = string.Empty;
            double dblResult;
            bool isValid = false;
            AppSettingsReader objAppSettingReader = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "SendToIngres: ");
                objMLAPDAccounting = new MLAPDAccounting();

                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat(PROC_NAME, " Started"));

                //Get values required by this process.
                strUpdateDispatchProc = objMLAPDAccounting.GetConfig("Accounting/Billing/UpdateDispatchSP");
                strIngresProc = objMLAPDAccounting.GetConfig("Accounting/IngressBillingSP");
                strNowSqlDateTime = "'" + objMLAPDAccounting.ConstructSqlDateTime(string.Empty, string.Empty, false) + "'";
                strImageRoot = objMLAPDAccounting.GetConfig("Document/RootDirectory");

                if (strImageRoot.Substring(0, 4) == "\\\\")
                    strImageRoot = Regex.Replace(strImageRoot, "\\\\", "\\", RegexOptions.IgnoreCase);

                // A bad Ingres Accounting ID will lock Ingres up for a few minutes and then cause APD to crash.
                if ((mstrIngresAccountingID.Length) == 0 || mstrIngresAccountingID == "0")
                    throw new Exception(string.Concat(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresRequiresIngresAccountingID, PROC_NAME));

                // Create and initialize connection to Ingres
                objIngresAccessor = new CDataAccessor();
                objAppSettingReader = new AppSettingsReader();
                objIngresAccessor.mEvents.ComponentInstance = "Accounting";
                objIngresAccessor.InitEvents(string.Concat(objAppSettingReader.GetValue("", typeof(string)).ToString(), "\\..\\config\\config.xml"), "Debug", false);
                objIngresAccessor.SetConnectString(objMLAPDAccounting.g_strIngresConnect);

                objMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_Recordset);

                blnIngresTransaction = true;
                objIngresAccessor.BeginTransaction();

                blnAPDTransaction = true;
                objMLAPDAccounting.g_objApdRsAccessor.BeginTransaction();

                // Loop through the InvoiceItems collection.
                foreach (CInvoiceItem objInvoiceItem in mcolInvoiceItems)
                {
                    // Retrieve the properties from the current InvoiceItem that are required for the Ingres call.
                    strLynxID = objInvoiceItem.LynxID;
                    strPayeeID = objInvoiceItem.PayeeID;
                    strPayeeName = objInvoiceItem.PayeeName;
                    strPayment = objInvoiceItem.Payment;
                    strClaimNumber = objInvoiceItem.ClaimNumber.Substring(0, 20);
                    strComment = objInvoiceItem.Comment.Substring(0, 300);
                    strDispatchNumber = "0";     // This will need to be pulled from an InvoiceItem property when updates are implemented.
                    strFee = objInvoiceItem.Fee;
                    strRetailerFee = objInvoiceItem.adminFee;
                    strDeductibleAmt = objInvoiceItem.DeductibleAmt;
                    strTaxTotalAmt = objInvoiceItem.TaxTotalAmt;
                    strCarrierRepName = (string.Concat(objInvoiceItem.CarrierRepNameFirst, " ", objInvoiceItem.CarrierRepNameLast).Trim().Substring(0, 200));

                    // Use Insured Business Name if available, otherwise use Insured first and last names.
                    strInsuredNameLast = objInvoiceItem.InsuredBusinessName.Substring(0, 20);
                    if ((strInsuredNameLast.Length) == 0)
                    {
                        strInsuredNameLast = (objInvoiceItem.InsuredNameLast.Substring(0, 20));
                        strInsuredNameFirst = (objInvoiceItem.InsuredNameFirst.Substring(0, 20));
                    }
                    else
                    {
                        strInsuredNameFirst = string.Empty;
                    }

                    if (strRetailerFee == string.Empty)
                        strRetailerFee = "0.00";

                    if (mstrEarlyBillFlag == "1")
                        if (mstrReleaseFundInd == "Y")
                            strReleaseFundAmt = strPayment;
                        else
                            strReleaseFundAmt = "0";
                    else
                        strReleaseFundAmt = strPayment;

                    strWarrantyInd = "N";
                    if (mstrWarrantyExistsFlag == "1")
                        strWarrantyInd = "Y";

                    //g_objEvents.Trace "", PROC_NAME & "Ingres parameters: retVal=" & CStr(adParamReturnValue) & _
                    //                       " h_apd_lynx_id=" & strLynxID & " h_apd_shop_id=" & strPayeeID & _
                    //                       " h_claim_amt=" & strPayment & " h_claim_no=" & strClaimNumber & _
                    //                       " h_comment_text=" & strComment & " h_comment_ind=  h_dispatch_no=" & strDispatchNumber & _
                    //                       " h_fee_amt=" & strFee & " h_insrd_first_nm=" & strInsuredNameFirst & _
                    //                       " h_insrd_last_nm=" & strInsuredNameLast & " h_ins_pc_uid_no=" & mstrIngresAccountingID & _
                    //                       " h_job_type_cd=15 h_msg_ind=  h_msg_text=" & strCarrierRepName & _
                    //                       " h_tries=1  h_user_id_no=" & mstrNTUserID & " h_user_session_no="

                    objMLAPDAccounting.g_objEvents.Trace(string.Empty, string.Concat(PROC_NAME, "Ingres parameters: retVal=", Convert.ToString(ParameterDirectionEnum.adParamReturnValue),
                                          " h_apd_lynx_id=", strLynxID, " h_apd_shop_id=", strPayeeID,
                                          " h_claim_amt=", strPayment, " h_claim_no=", strClaimNumber,
                                          " h_comment_text=", strComment, " h_commit_ind=1  h_dispatch_no=", strDispatchNumber,
                                          " h_fee_amt=", strFee, " h_rtlr_fee_amt=", strRetailerFee, " h_insrd_first_nm=", strInsuredNameFirst,
                                          " h_insrd_last_nm=", strInsuredNameLast, " h_ins_pc_uid_no=", mstrIngresAccountingID,
                                          " h_job_type_cd=15 h_msg_ind=  h_msg_text=", strCarrierRepName,
                                          " h_tries=1  h_user_id_no=", mstrNTUserID, " h_user_session_no=",
                                          " h_release_fund_amt=", strReleaseFundAmt, " h_release_fund_ind=", mstrReleaseFundInd,
                                          " h_warranty_ind=", strWarrantyInd));

                    //not implemented yet on AGC side
                    //" h_deductible_amt = " & strDeductibleAmt & " h_tax_total_amt = " & strTaxTotalAmt

                    // Do not send items to Ingres if both amounts are zero.  These do not need to appear on the invoice, and Ingres will blow.
                    if (Convert.ToDouble(strPayment) + Convert.ToDouble(strFee) == 0)
                    {
                        objMLAPDAccounting.g_objEvents.Trace(string.Empty, string.Concat(PROC_NAME, "Skipping Ingres call because both payment and fee amounts are zero."));

                        //change the status of invoice items to FS (invoiced). We need to stuff a dummy dispatch number too
                        strDispatchNumber = "0";

                        // Retrieve the InvoiceIDs associated with this InvoiceItem.
                        colInvoiceIDs = (Collection)objInvoiceItem.InvoiceIDs;

                        objMLAPDAccounting.g_objEvents.Trace(string.Empty, string.Concat(PROC_NAME, "Setting invoice items as invoiced (StatusCD=FS)."));

                        // Loop through the InvoiceItem's InvoiceIDs collection.
                        foreach (object vInvoiceID in colInvoiceIDs)
                            // Update the corresponding Invoice records in the APD database with the newly retrieved DispatchNumber.
                            objMLAPDAccounting.g_objApdRsAccessor.ExecuteSp(strUpdateDispatchProc, Convert.ToInt64(vInvoiceID), strNowSqlDateTime, strDispatchNumber, 0);

                        // Remove this InvoiceItem from the collection so that it will not appear in the output.
                        RemoveInvoiceItem(objInvoiceItem.InvoiceItemID);

                        // Continue processing normally.
                    }
                    else
                    {
                        //create the Ingres Parameter XML so we can save it later.
                        if (mstrEarlyBillFlag == "1")
                        {
                            objIngresParams = new DOMDocument40();
                            objMdomUtils.LoadXml(ref objIngresParams, "<Root/>", PROC_NAME, "SendToIngres");
                            objIngresParamsRoot = objIngresParams.documentElement;
                            objIngresParamsRoot.setAttribute("retval", "0");
                            objIngresParamsRoot.setAttribute("h_apd_lynx_id", strLynxID);
                            objIngresParamsRoot.setAttribute("h_apd_shop_id", strPayeeID);
                            objIngresParamsRoot.setAttribute("h_claim_amt", strPayment);
                            objIngresParamsRoot.setAttribute("h_claim_no", strClaimNumber);
                            objIngresParamsRoot.setAttribute("h_comment_text", strComment);
                            //objIngresParamsRoot.SetAttributee "h_comment_ind", ""
                            objIngresParamsRoot.setAttribute("h_commit_ind", "");
                            objIngresParamsRoot.setAttribute("h_dispatch_no", strDispatchNumber);
                            objIngresParamsRoot.setAttribute("h_fee_amt", strFee);
                            objIngresParamsRoot.setAttribute("h_rtlr_fee_amt", strRetailerFee);
                            objIngresParamsRoot.setAttribute("h_insrd_first_nm", strInsuredNameFirst);
                            objIngresParamsRoot.setAttribute("h_insrd_last_nm", strInsuredNameLast);
                            objIngresParamsRoot.setAttribute("h_ins_pc_uid_no", mstrIngresAccountingID);
                            objIngresParamsRoot.setAttribute("h_job_type_cd", "15");   // 15 indicates an APD claim.  Later WAG and OCG will require this value to become dynamic.
                            objIngresParamsRoot.setAttribute("h_msg_ind", "");

                            // When strReturnValue is -1 the first 4 chars of h_msg_text represent the Ingres error code; resolve in message store.
                            objIngresParamsRoot.setAttribute("h_msg_text", strCarrierRepName);

                            //Early Billing
                            objIngresParamsRoot.setAttribute("h_release_fund_amt", strReleaseFundAmt);
                            objIngresParamsRoot.setAttribute("h_release_fund_ind", mstrReleaseFundInd);
                            objIngresParamsRoot.setAttribute("h_warranty_ind", strWarrantyInd);
                            objIngresParamsRoot.setAttribute("h_tries", "1");

                            // Pass 'lynxdba' for system batch jobs.
                            objIngresParamsRoot.setAttribute("h_user_id_no", mstrNTUserID);
                            objIngresParamsRoot.setAttribute("h_user_session_no", "");
                        }

                        // Send each InvoiceItem to Ingres and retreive the returned Dispatch number.
                        objIngresAccessor.FpCreateCommand(strIngresProc);
                        objIngresAccessor.FpAddParam("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 4, 0);
                        objIngresAccessor.FpAddParam("h_apd_lynx_id", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, strLynxID);
                        objIngresAccessor.FpAddParam("h_apd_shop_id", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, Convert.ToInt64(strPayeeID));
                        objIngresAccessor.FpAddParam("h_claim_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, Convert.ToDouble(strPayment));
                        objIngresAccessor.FpAddParam("h_claim_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, strClaimNumber);
                        objIngresAccessor.FpAddParam("h_comment_text", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 300, strComment);
                        //objIngresAccessor.FpAddParam "h_comment_ind", adChar, adParamInput, 1, ""
                        objIngresAccessor.FpAddParam("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "");
                        objIngresAccessor.FpAddParam("h_dispatch_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 4, Convert.ToInt64(strDispatchNumber));
                        objIngresAccessor.FpAddParam("h_fee_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, Convert.ToDouble(strFee));
                        objIngresAccessor.FpAddParam("h_rtlr_fee_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, Convert.ToDouble(strRetailerFee));
                        objIngresAccessor.FpAddParam("h_insrd_first_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, strInsuredNameFirst);
                        objIngresAccessor.FpAddParam("h_insrd_last_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, strInsuredNameLast);
                        objIngresAccessor.FpAddParam("h_ins_pc_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, Convert.ToInt64(mstrIngresAccountingID));
                        objIngresAccessor.FpAddParam("h_job_type_cd", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 1, 15);  // 15 indicates an APD claim.  Later WAG and OCG will
                        // require this value to become dynamic.
                        objIngresAccessor.FpAddParam("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, "");

                        // When strReturnValue is -1 the first 4 chars of h_msg_text represent the Ingres error code; resolve in message store.
                        objIngresAccessor.FpAddParam("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, strCarrierRepName);

                        //Early Billing
                        objIngresAccessor.FpAddParam("h_release_fund_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, Convert.ToDouble(strReleaseFundAmt));
                        objIngresAccessor.FpAddParam("h_release_fund_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, mstrReleaseFundInd);
                        objIngresAccessor.FpAddParam("h_warranty_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, strWarrantyInd);
                        objIngresAccessor.FpAddParam("h_tries", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, 1);

                        // Pass 'lynxdba' for system batch jobs.
                        objIngresAccessor.FpAddParam("h_user_id_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, mstrNTUserID);
                        objIngresAccessor.FpAddParam("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, "");

                        // strReturnValue:  0 for success, positive for dbms error, negative for logical error (see h_msh_text).
                        strReturnValue = Convert.ToString(objIngresAccessor.FpExecute());
                        strDispatchNumber = Convert.ToString(objIngresAccessor.FpGetParameter("h_dispatch_no"));
                        strMessageText = Convert.ToString(objIngresAccessor.FpGetParameter("h_msg_text"));
                        strMessageInd = Convert.ToString(objIngresAccessor.FpGetParameter("h_msg_ind"));

                        objIngresAccessor.FpCleanUp();

                        objMLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Ingres return:  strReturnValue=", strReturnValue, " h_dispatch_no=", strDispatchNumber,
                                                            " h_msg_text=", strMessageText, " h_msg_ind=", strMessageInd));

                        if (mstrEarlyBillFlag == "1")
                        {
                            if (objIngresParamsRoot != null)
                            {
                                objIngresParamsRoot.setAttribute("h_dispatch_no", strDispatchNumber);
                                objIngresParamsRoot.setAttribute("h_msg_text", strMessageText);
                                objIngresParamsRoot.setAttribute("h_msg_ind", strMessageInd);
                            }
                        }

                        //Verify what we got back from Ingres.
                        if (strReturnValue == "-1")
                        {
                            // Extract the code and text portions of the Ingres error message.
                            strIngresErrorCode = strMessageText.Substring(0, 4);
                            strIngresMessageText = strMessageText.Substring((strMessageText.Length - ((strMessageText).Length - 4)), (strMessageText).Length - 4);

                            // Ensure a valid Ingres error code was retrieved
                            isValid = double.TryParse(strIngresErrorCode, out dblResult);
                            if (isValid)
                            {
                                // Convert the Ingres error code to an APD error code that can be resolved in the message store (error.xml).
                                lngIngresErrorNumber = Convert.ToInt64(strIngresErrorCode);
                                if (lngIngresErrorNumber != 0)
                                    lngIngresErrorNumber = MLAPDAccounting.LAPDAccounting_FirstError + lngIngresErrorNumber;

                                // Raise the error normally for invoice methods other than 'file'.
                                if (mintInvoiceType != Convert.ToInt32(eInvoiceType.eFile))
                                    throw new Exception(string.Concat(lngIngresErrorNumber, "Ingres Billing SP", strIngresErrorCode, "|", strIngresMessageText));
                                // For 'file' (electronic) billing, handle the error and continue.
                                else
                                {
                                    blnIngresErrorElectronicBilling = true;

                                    objMLAPDAccounting.g_objEvents.Env("INGRES: Return Value = '" + strReturnValue + "' Message Text = '" + strMessageText + "' Message Indicator = '" + strMessageInd + "'");
                                    objMLAPDAccounting.g_objEvents.HandleEvent(Convert.ToInt32(lngIngresErrorNumber), "Ingres Billing SP", string.Concat(strIngresErrorCode, "|", strIngresMessageText),
                                       string.Concat("' InsCo='" + mstrInsuranceCompanyID + "' LynxID='" + strLynxID +
                                        "' UserID='0' Ingres Accounting ID='" + mstrIngresAccountingID +
                                        "' PayeeID='" + strPayeeID + "' Dispatch Number='" + strDispatchNumber +
                                        "' Total Payment='" + strPayment + "' Total Billing='"), strFee, false);
                                }
                            }
                            else
                            {
                                // Raise the generic Ingres error.
                                if (mintInvoiceType != Convert.ToInt32(eInvoiceType.eFile))
                                    throw new Exception(string.Concat(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresReturnedUnknownError, "IngresBillingSP", strIngresErrorCode, "|", strMessageText));

                                else
                                {
                                    // For electronic billing handle the error and continue.
                                    blnIngresErrorElectronicBilling = true;

                                    objMLAPDAccounting.g_objEvents.Env(string.Concat("INGRES: Return Value = '" + strReturnValue + "' Message Text = '" + strMessageText
                                      + "' Message Indicator = '" + strMessageInd + "'"));

                                    objMLAPDAccounting.g_objEvents.HandleEvent(Convert.ToInt32(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresReturnedUnknownError), "Ingres Billing SP", string.Concat(strReturnValue, "|", strIngresMessageText),
                                        string.Concat("' InsCo='" + mstrInsuranceCompanyID + "' LynxID='" + strLynxID +
                                        "' UserID='0' Ingres Accounting ID='" + mstrIngresAccountingID +
                                        "' PayeeID='" + strPayeeID + "' Dispatch Number='" + strDispatchNumber +
                                        "' Total Payment='" + strPayment + "' Total Billing='"), strFee, false);
                                }
                            }
                        }
                        else if (strReturnValue != "0")
                        {
                            // Raise the generic Ingres error.
                            if (mintInvoiceType != Convert.ToInt32(eInvoiceType.eFile))
                                throw new Exception(string.Concat(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresReturnedUnknownError, "IngresBillingSP", strReturnValue, "|", strMessageText));

                            else
                            {
                                // For electronic billing handle the error and continue.
                                blnIngresErrorElectronicBilling = true;

                                objMLAPDAccounting.g_objEvents.Env("INGRES: Return Value = '" + strReturnValue
                                    + "' Message Text = '" + strMessageText
                                    + "' Message Indicator = '" + strMessageInd + "'");

                                objMLAPDAccounting.g_objEvents.HandleEvent(Convert.ToInt32(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresReturnedUnknownError), "Ingres Billing SP", string.Concat(strReturnValue, "|", strIngresMessageText),
                                   string.Concat("' InsCo='" + mstrInsuranceCompanyID + "' LynxID='" + strLynxID +
                                    "' UserID='0' Ingres Accounting ID='" + mstrIngresAccountingID +
                                    "' PayeeID='" + strPayeeID + "' Dispatch Number='" + strDispatchNumber +
                                    "' Total Payment='" + strPayment + "' Total Billing='"), strFee, false);
                            }
                        }

                        //If an Ingres error occurred during electronic billing, skip the updates to the InvoiceItem and the APD database.
                        if (blnIngresErrorElectronicBilling)
                        {
                            objMLAPDAccounting.g_objEvents.Trace(string.Empty, string.Concat(PROC_NAME, "Removing invoice item due to Ingres error."));

                            // Remove this InvoiceItem from the collection so that it will not appear in the output.
                            RemoveInvoiceItem(objInvoiceItem.InvoiceItemID);

                            // Reset flag.
                            blnIngresErrorElectronicBilling = false;
                        }
                        else
                        {
                            // Ensure Dispatch number has a value.
                            if ((strDispatchNumber.Length) > 0)
                            {
                                // Increment value indicating how many dispatches have been created in Ingres.
                                intReturnVal = intReturnVal + 1;

                                // Retrieve the InvoiceIDs associated with this InvoiceItem.
                                colInvoiceIDs = (Collection)objInvoiceItem.InvoiceIDs;

                                // Loop through the InvoiceItem's InvoiceIDs collection.
                                foreach (object vInvoiceID in colInvoiceIDs)
                                    // Update the corresponding Invoice records in the APD database with the newly retrieved DispatchNumber.
                                    objMLAPDAccounting.g_objApdRsAccessor.ExecuteSp(strUpdateDispatchProc, Convert.ToInt64(vInvoiceID), strNowSqlDateTime, strDispatchNumber, 0);

                                // Set the InvoiceItem's DispatchNumber property so the DispatchNumber will appear on the invoice or in the billing file source
                                // XML as appropriate.  IMPORTANT -- ALWAYS perform this step prior to generating a paper invoice or sending the billing file source
                                // to GLAXIS.
                                objInvoiceItem.DispatchNumber = strDispatchNumber;

                                //save the Ingres transaction so we can use it for early bill release funds
                                if (mstrEarlyBillFlag == "1")
                                {
                                    //Save the release amount
                                    if ((Convert.ToDouble(strReleaseFundAmt)) > 0)
                                    {
                                        strReleaseFundsUpdateDispatchProc = objMLAPDAccounting.GetConfig("Accounting/Payment/UpdateReleaseFundsDispatchSP");
                                        strHistoryComments = string.Concat(Regex.Replace(strPayeeName, "'", "", RegexOptions.IgnoreCase), " [", strPayeeID, "]");
                                        objMLAPDAccounting.g_objApdRsAccessor.ExecuteSp(strReleaseFundsUpdateDispatchProc, strDispatchNumber, strDispatchNumber, strReleaseFundAmt, "'System: Payment Released to " + strHistoryComments + "'", mstrUserID);

                                    }
                                    if (objIngresParamsRoot != null)
                                    {
                                        strFileName = strLynxID.Substring((strLynxID.Substring(1, strLynxID.IndexOf("-", 1) - 1)).Length - 4, 4);
                                        strFileName = string.Concat(strFileName.Substring(1, 1), "\\",
                                                      strFileName.Substring(2, 1), "\\",
                                                      strFileName.Substring(3, 1), "\\",
                                                      strFileName.Substring(4, 1), "\\",
                                                      "IngresParams", strDispatchNumber, ".xml");

                                        if ((strImageRoot.Substring(strImageRoot.Length - 1, 1) != "\\"))
                                            strFileName = string.Concat(strImageRoot, "\\", strFileName);
                                        else
                                            strFileName = string.Concat(strImageRoot, strFileName);

                                        objIngresParams.save(strFileName);

                                        if (objMLAPDAccounting.g_blnDebugMode)
                                            objMLAPDAccounting.g_objEvents.Trace(string.Empty, string.Concat(PROC_NAME, "Saved the Ingres Parameters to ", strFileName));
                                    }
                                }

                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return intReturnVal;
        }

        /// <summary>
        /// Procedure : CreateInvoiceSourceXML
        /// DateTime  : 3/16/2005 11:19
        /// Author    : Dan Price
        /// Purpose   : Alter the XML retrieved from the APD database by replacing the original
        ///             Invoice nodes, which represent APD Invoice records, with new Invoice nodes,
        ///             which represent InvoiceItem objects.
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        public string CreateInvoiceSourceXML(eInvoiceType iType)
        {
            //Primitives
            long lngClaimAspectID;
            string strDispatchNumber = string.Empty;
            string PROC_NAME = string.Empty;
            string strXslFile;
            string strReturn = string.Empty;
            int intInvoiceNodeCount;
            int intExposureNodeCount;

            DOMDocument40 objXmlDoc = null;
            DOMDocument40 objInvoiceDoc = null;
            IXMLDOMNodeList objExposureList = null;
            IXMLDOMNodeList objInvoiceList = null;
            XmlNode objParentNode = null;
            IXMLDOMNodeList objClaimList = null;
            XmlNode objChildNode = null;
            IXMLDOMNode objNodeAppend = null;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "CreateInvoiceSourceXML");
                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat(PROC_NAME, " Started", Environment.NewLine, "Invoice Type: ", mintInvoiceType));

                // Initialization
                objXmlDoc = new DOMDocument40();
                objInvoiceDoc = new DOMDocument40();
                objMdomUtils = new MDomUtils();
                objMLAPDAccounting = new MLAPDAccounting();

                intInvoiceNodeCount = 0;
                intExposureNodeCount = 0;

                // Create a new document so that this process does not alter the original.
                objMdomUtils.LoadXml(ref objXmlDoc, mobjParamDOM.xml, PROC_NAME, "Electronic Billing");

                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat("Invoice source XML before altering to invoice format:", Environment.NewLine, mobjParamDOM.xml));

                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat("NOTE: This function will remove all invoice nodes and replace them with invoice nodes ", Environment.NewLine,
                                        "specifically formatted for the invoice process."));

                // Loop thru exposures
                objExposureList = objXmlDoc.selectNodes("/Root/Claim/Exposure");

                foreach (IXMLDOMNode objNode in objExposureList)
                {
                    // Grab ClaimAspectID
                    lngClaimAspectID = Convert.ToInt64(objNode.selectSingleNode("./@ClaimAspectID").text);

                    // Remove the current Exposure's Invoice nodes.
                    objInvoiceList = objNode.selectNodes("./Invoice");

                    if (objMLAPDAccounting.g_blnDebugMode)
                        objMLAPDAccounting.g_objEvents.Trace("Removing invoice nodes for Claim Aspect ID: ", Convert.ToString(lngClaimAspectID));

                    foreach (IXMLDOMNode objInvoiceNode in objInvoiceList)
                        objNode.removeChild(objInvoiceNode);

                    // Loop thru the InvoiceItems collection
                    foreach (CInvoiceItem objInvoiceItem in mcolInvoiceItems)
                    {
                        // Locate the InvoiceItems for the current Claim Aspect (Exposure).
                        if (objInvoiceItem.ClaimAspectID == lngClaimAspectID)
                        {
                            intInvoiceNodeCount = intInvoiceNodeCount + 1;

                            // Add a new Invoice node to the Exposure node with the InvoiceItem xml.
                            if (mintInvoiceType == Convert.ToInt32(eInvoiceType.eFile))
                            {
                                // Get Invoice node xml formatted for electronic billing.
                                objMdomUtils.LoadXml(ref objInvoiceDoc, objInvoiceItem.GetBillingFileXML(), "ElectronicInvoicing", "InvoiceItem");
                            }
                            else
                            {
                                // Get Invoice node xml formatted for 'paper' Invoice document.
                                objMdomUtils.LoadXml(ref objInvoiceDoc, objInvoiceItem.GetInvoiceDocumentXML(), "ElectronicInvoicing", "InvoiceItem");

                                // Grab the Dispatch number from the InvoiceItem with the handling fee.  For the 'paper' Invoice document, the
                                // handling fee will be a separate line item and we need to stick the correct Dispatch number in the Handling fee
                                // InvoiceItem.
                                if (objInvoiceItem.HandlingFeeGoesHere)
                                    strDispatchNumber = objInvoiceItem.DispatchNumber;
                            }

                            if (objMLAPDAccounting.g_blnDebugMode)
                                objMLAPDAccounting.g_objEvents.Trace(string.Concat("Replacing invoice node for Claim Aspect ID: ", Convert.ToString(lngClaimAspectID), Environment.NewLine, objInvoiceDoc.xml));

                            objNode.appendChild(objInvoiceDoc.ownerDocument);
                        }
                    }

                    // Hack for paper invoices.
                    if ((mintInvoiceType == Convert.ToInt32(eInvoiceType.ePaper)) && (strDispatchNumber.Length) > 0)
                    {
                        if (mobjHandlingFeeInvoiceItem != null)
                        {
                            //Add the Dispatch number to the Handling fee InvoiceItem.
                            mobjHandlingFeeInvoiceItem.DispatchNumber = strDispatchNumber;

                            // Get the InvoiceItem objects xml formatted for the paper invoice.
                            objMdomUtils.LoadXml(ref objInvoiceDoc, mobjHandlingFeeInvoiceItem.GetInvoiceDocumentXML(), "ElectronicInvoicing", "InvoiceItem");

                            // Add the Invoice line item xml to the document.
                            objNode.appendChild(objInvoiceDoc.documentElement);
                        }
                    }
                }

                // Clean up XML This to ensure no dud nodes are forwarded  through the invoice process. -----------------------------------

                // Some InvoiceItems might have been removed during the SendToIngresProcess due to zero billing
                // and zero payment or because of an Ingres error.  This could leave some exposure nodes with no child invoice
                // nodes.  These exposure nodes should be removed.

                objExposureList = objXmlDoc.selectNodes("/Root/Claim/Exposure[count(Invoice) = 0]");
                foreach (XmlNode objExposureNode in objExposureList)
                {
                    objParentNode = objExposureNode.ParentNode;

                    if (objMLAPDAccounting.g_blnDebugMode)
                        objMLAPDAccounting.g_objEvents.Trace(string.Concat("Removing empty Exposure node: ", Environment.NewLine, objExposureNode.OuterXml));

                    objChildNode = objParentNode.RemoveChild(objExposureNode);
                    objChildNode = null;
                }

                // Removing exposure nodes could leave some claim nodes with no child exposure nodes.  These claim nodes shold be removed.
                objClaimList = objXmlDoc.selectNodes("/Root/Claim[count(Exposure) = 0]");
                foreach (XmlNode objClaimNode in objClaimList)
                {
                    objParentNode = objClaimNode.ParentNode;

                    if (objMLAPDAccounting.g_blnDebugMode)
                        objMLAPDAccounting.g_objEvents.Trace(string.Concat("Removing empty Claim node: ", Environment.NewLine, objClaimNode));

                    objChildNode = objParentNode.RemoveChild(objClaimNode);
                    objChildNode = null;
                }

                // Initialize return value
                strReturn = string.Empty;

                // Electronic files may need to be altered to conform to GLAXIS' Biztalk map.  This will be accomplished through an XSLT.
                if (iType == eInvoiceType.eFile)
                {
                    // Check config to determine if any implementation specific xml file structure is required.  ------------------------------
                    strXslFile = objMLAPDAccounting.GetConfig(string.Concat("Accounting/ElectronicInvoice/Insurance[@ID=", mstrInsuranceCompanyID, "]/@xsl"));

                    if (objMLAPDAccounting.g_blnDebugMode)
                        objMLAPDAccounting.g_objEvents.Trace(string.Concat(PROC_NAME, Environment.NewLine, " XSL File: ", strXslFile));

                    // If an xsl file is found do the transformation.
                    if ((strXslFile).Trim().Length > 0)
                        strXslFile = string.Concat(objMLAPDAccounting.g_strSupportDocPath, "\\", strXslFile);

                    if (objMLAPDAccounting.g_blnDebugMode)
                        objMLAPDAccounting.g_objEvents.Trace(string.Concat(PROC_NAME, Environment.NewLine, " XSL found.  Performing transformation."));

                    objInvoiceDoc = objMdomUtils.TransformDomAsDom(ref objXmlDoc, strXslFile, objMLAPDAccounting.g_objEvents);

                    // Grab the first child so we can determine if it is a processing instruction.
                    objNodeAppend = objInvoiceDoc.firstChild;

                    // Remove processing instruction if present.
                    if (objNodeAppend.nodeType.ToString() == "NODE_PROCESSING_INSTRUCTION")
                    {
                        if (objMLAPDAccounting.g_blnDebugMode)
                            objMLAPDAccounting.g_objEvents.Trace(string.Concat("Removing processing instruction from XML: ", Environment.NewLine, objInvoiceDoc.firstChild.xml));

                        objInvoiceDoc.removeChild(objNodeAppend);
                        objNodeAppend = objInvoiceDoc.firstChild;

                        // Remove the formatiing object attribute if present.
                        objNodeAppend.attributes.removeNamedItem("xmlns:fo");

                        // Set the return value.  An XSLT was performed.
                        strReturn = objInvoiceDoc.xml;
                    }
                    else
                    {
                        // Set the return value.  There was no XSLT required for this client.
                        strReturn = objXmlDoc.xml;
                    }
                }
                else
                {
                    // Set the return value.  No additional XSLT required for paper billing.
                    strReturn = objXmlDoc.xml;
                }

                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat(PROC_NAME, Environment.NewLine, " InvoiceSourceXML: ", strReturn));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // Return the reformatted XML.
            return strReturn;
        }

        #endregion

        #region Private Helper Functions
        private void Class_Initialize()
        {
            try
            {
                mcolInvoiceItems = new Collection();
                mobjParamDOM = new DOMDocument40();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// Procedure : Class_Terminate
        /// DateTime  : 3/16/2005 11:26
        /// Author    : Dan Price
        /// Purpose   : Destroy the InvoiceItems collection and all of the InvoiceItem objects
        ///             it contains.
        /// </summary>
        private void Class_Terminate()
        {
            //item As CInvoiceItem

            mobjParamDOM = null;
            mobjHandlingFeeInvoiceItem = null;
            CInvoiceItem itemClear = null;
            try
            {
                foreach (CInvoiceItem item in mcolInvoiceItems)
                {
                    itemClear = item;
                    itemClear = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                mcolInvoiceItems = null;
            }
        }

        /// <summary>
        /// Procedure : CreateInvoiceItem
        /// DateTime  : 3/16/2005 11:15
        /// Author    : Dan Price
        /// Purpose   : Build a new InvoiceItem object that contains all the properties needed to
        ///             support paper or electronic invoicing.
        /// </summary>
        /// <param name="objInvoiceNode"></param>
        /// <returns></returns>
        private CInvoiceItem CreateInvoiceItem(XmlNode objInvoiceNode)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "CreateInvoiceItem");

            // Objects that must be destroyed.
            CInvoiceItem objInvoiceItem = null;

            // Primitives
            string strAuthorizingClientUserID = string.Empty;
            string strPayeeName = string.Empty;
            string strPayeeCity = string.Empty;
            string strPayeeState = string.Empty;
            string strDispatchNumber = string.Empty;
            string strInvoiceDescription = string.Empty;
            string strClientFeeCode = string.Empty;
            string strInvoiceDate = string.Empty;
            string strDeductible = string.Empty;
            string strServiceChannel = string.Empty;
            bool blnHandlingFeeGoesHere = false;
            string strTaxTotalAmt = string.Empty;
            double outNumeric;
            bool isNumeric = false;
            try
            {
                objMdomUtils = new MDomUtils();
                objMLAPDAccounting = new MLAPDAccounting();
                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat(" Invoice Node XML = ", objInvoiceNode.OuterXml, Environment.NewLine,
                        PROC_NAME, "Started"));

                // Retrieve properties from Invoice node.
                strAuthorizingClientUserID = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@AuthorizingClientUserID", false);
                strPayeeName = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@PayeeName", true);
                strPayeeCity = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@PayeeCity", true);
                strPayeeState = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@PayeeState", true);
                strDispatchNumber = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@DispatchNumber");
                strInvoiceDescription = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@InvoiceDescription");
                strClientFeeCode = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@ClientFeeCode");
                strInvoiceDate = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@InvoiceDate");
                strDeductible = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@DeductibleAmt");
                strTaxTotalAmt = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@TotalTaxAmt");
                strServiceChannel = objMdomUtils.GetChildNodeText(ref objInvoiceNode, "./@ServiceChannel");

                // For indemnities, determine if the Payment is to the first assigned Shop to receive an indemnity payment.  The Handling Fee will be added to the shop
                // where this is true.
                if (mstrItemType == "Indemnity" && mstrPayeeType == "Shop" && !mblnHandlingFeeIndemnityFlagged)
                {
                    isNumeric = double.TryParse(mstrHandlingPayeeID, out outNumeric);
                    if (isNumeric)
                    {
                        if (mstrHandlingPayeeID == mstrPayeeID)
                            blnHandlingFeeGoesHere = true;

                        // Indicate that the indemnity to which the handling fee will be attached has been located and flagged.  This will prevent the method
                        // from flagging any other indemnity.
                        mblnHandlingFeeIndemnityFlagged = true;
                    }
                    else
                        blnHandlingFeeGoesHere = false;
                }
                else
                    blnHandlingFeeGoesHere = false;

                // Add Amount of current Invoice record to the Invoice total.
                mdblInvoiceTotal = mdblInvoiceTotal + Convert.ToDouble(mstrAmount);

                // Increment InvoiceItemID.  This value will can be used to explicitly identify an InvoiceItem without trudging through
                // all the other crap in the InvoiceItem.
                mintInvoiceItemID = mintInvoiceItemID + 1;

                // Retrieve Invoice level properties.
                objInvoiceItem = new CInvoiceItem();

                // Call to the InvoiceItem 'constructor'.
                objInvoiceItem.Initialize(mintInvoiceItemID, mlngClaimAspectID, mstrItemType, mstrLynxID, mstrCarrierClaimNumber, mstrCarrierRepNameFirst,
                                         mstrCarrierRepnameLast, mstrComment, strDeductible, mstrInsuredNameFirst, mstrInsuredNameLast, mstrInsuredBusinessName,
                                         mstrClaimantNameFirst, mstrClaimantNameLast, mstrClaimantBusinessName, strAuthorizingClientUserID,
                                         mstrAmount, mstrAdminAmount, mstrPayeeID, strPayeeName, strPayeeCity, strPayeeState, strDispatchNumber,
                                         strInvoiceDescription, mlngInvoiceID, strClientFeeCode, strInvoiceDate, mstrPayeeType, strServiceChannel,
                                         mstrInitialPayment, strTaxTotalAmt, blnHandlingFeeGoesHere);

                // Return the newly created object.
                return objInvoiceItem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Procedure : AppendInvoiceItem
        /// DateTime  : 3/16/2005 11:25
        /// Author    : Dan Price
        /// Purpose   : Add an existing InvoiceItem to the InvoiceItems collection.
        /// </summary>
        private void AppendInvoiceItem(CInvoiceItem objInvoiceItem)
        {
            try
            {
                mcolInvoiceItems.Add(objInvoiceItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Procedure : RemoveInvoiceItem
        ///  DateTime  : 4/1/2005 11:50
        ///  Author    : Dan Price
        ///  Purpose   : Remove the InvoiceItem identified by the passed InvoiceItemID from the
        ///              InvoiceItems collection.
        /// </summary>
        /// <param name="intInvoiceItemID"></param>
        private void RemoveInvoiceItem(int intInvoiceItemID)
        {
            string PROC_NAME = "RemoveInvoiceItem";
            CInvoiceItem objTemp = null;

            // Primitives
            int intIndex;
            try
            {
                objMLAPDAccounting = new MLAPDAccounting();
                // Initialize index for 1 based collection.
                intIndex = 1;

                // Loop through the InvoiceItems collection.
                foreach (CInvoiceItem objInvoiceItem in mcolInvoiceItems)
                {
                    // Locate the InvoiceItem with the passed InvoiceItemID
                    if (objInvoiceItem.InvoiceItemID == intInvoiceItemID)
                        objTemp = new CInvoiceItem();

                    // Added objTemp because there were some items being errantly removed.
                    objTemp = (CInvoiceItem)mcolInvoiceItems.Item(intIndex);

                    if (objTemp.InvoiceItemID == intInvoiceItemID)
                    {
                        if (objMLAPDAccounting.g_blnDebugMode)
                            objMLAPDAccounting.g_objEvents.Trace(string.Concat("Removing invoice item.  InvoiceItemID: ", objTemp.InvoiceItemID,
                                                                  "InvoiceID: ", objTemp.FirstInvoiceID, " ClaimAspectID: ", objTemp.ClaimAspectID,
                                                                  " Fee: ", objTemp.Fee, " Payment: ", objTemp.Payment));

                        // Remove the indicated item from the collection.
                        mcolInvoiceItems.Remove(intIndex);
                    }
                    // Bail out of the loop.
                    break;
                }

                // Increment the index.
                intIndex = intIndex + 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Procedure : AddToExistingInvoiceItem
        /// DateTime  : 3/16/2005 11:23
        /// Author    : Dan Price
        /// Purpose   : Determine if an InvoiceItem already exists for the current Payee.  If so,
        ///             add the current Payment amount to that InvoiceItem and return True.  Otherwise,
        ///             return False signaling that a new InvoiceItem should be created.
        /// </summary>
        /// <returns></returns>
        private bool AddToExistingInvoiceItem()
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "AddToExistingInvoiceItem");

            //Dim objInvoiceItem As CInvoiceItem
            bool blnPaymentAdded = false;
            try
            {
                objMLAPDAccounting = new MLAPDAccounting();
                // Loop thru existing InvoiceItems to determine if one has already been created for the current Payee.
                foreach (CInvoiceItem objInvoiceItem in mcolInvoiceItems)
                {
                    // Limit this search to InvoiceItems for the current ClaimAspectID.
                    if (objInvoiceItem.ClaimAspectID == mlngClaimAspectID)
                    {
                        // Determine if an InvoiceItem already exists for this Payee for this vehicle. The name is factored in here in case payments were made
                        // to more than 1 CEI shop.  Both would use the default CEI Shop ID and could not therefore be differentiated on the basis of ID.
                        if (objInvoiceItem.PayeeID == mstrPayeeID && objInvoiceItem.PayeeType == mstrPayeeType && objInvoiceItem.PayeeName == mstrPayeeName)
                        {
                            if (objMLAPDAccounting.g_blnDebugMode)
                                objMLAPDAccounting.g_objEvents.Trace(string.Concat(
                               PROC_NAME, Environment.NewLine, " Amount: ", mstrAmount, "  AmountType: ", CInvoiceItem.AmountType.eFee, "  InvoiceID: ", mlngInvoiceID));

                            // Add Payment Amount to the existing InvoiceItem and add the InvoiceID to the InvoiceItem's InvoiceIDs collection.
                            blnPaymentAdded = objInvoiceItem.Add(mstrAmount, mstrAdminAmount, mstrDeductibleAmt, mstrTaxTotalAmt, CInvoiceItem.AmountType.ePayment, mlngInvoiceID, mstrInitialPayment);

                            if (objMLAPDAccounting.g_blnDebugMode)
                                objMLAPDAccounting.g_objEvents.Trace(string.Concat(
                                    PROC_NAME, Environment.NewLine, " Payment ", (blnPaymentAdded ? "ADDED" : "NOT ADDED"), " to existing line item/dispatch for Claim Aspect: ",
                                                      objInvoiceItem.ClaimAspectID));
                        }

                        // Check to see if this Payment has been added to an existing InvoiceItem.
                        if (blnPaymentAdded)
                        {
                            // If so, add Amount of current Invoice record to the Invoice Total...
                            mdblInvoiceTotal = mdblInvoiceTotal + Convert.ToDouble(mstrAmount);

                            // ... and bail out.
                            break;
                        }
                    }
                }

                // Set return value.
                return blnPaymentAdded;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Procedure : CopyInvoiceItem
        /// Da teTime  : 3/18/2005 14:03
        /// Author    : Dan Price
        /// Purpose   : Make a copy of an existing InvoiceItem and return the new object to the caller.
        ///             This is necessary because direct assignment of a reference object simply creates
        ///             an additional pointer to the existing object.  We actually require a new object here.
        /// </summary>
        /// <param name="objInvoiceItem"></param>
        /// <returns></returns>

        private CInvoiceItem CopyInvoiceItem(CInvoiceItem objInvoiceItem)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "CopyInvoiceItem");

            CInvoiceItem objNew = null;
            Collection col = null;

            try
            {
                objMLAPDAccounting = new MLAPDAccounting();
                if (objMLAPDAccounting.g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat(PROC_NAME, " Started ", Environment.NewLine, "ClaimAspectID: ", objInvoiceItem.ClaimAspectID,
        "  Invoice Description: ", objInvoiceItem.InvoiceDescription, "  Payee Name: ", objInvoiceItem.PayeeName));

                objNew = new CInvoiceItem();

                objNew.CarrierRepNameFirst = objInvoiceItem.CarrierRepNameFirst;
                objNew.CarrierRepNameLast = objInvoiceItem.CarrierRepNameLast;
                objNew.ClaimAspectID = objInvoiceItem.ClaimAspectID;
                objNew.ClaimNumber = objInvoiceItem.ClaimNumber;
                objNew.Comment = objInvoiceItem.Comment;
                objNew.DispatchNumber = objInvoiceItem.DispatchNumber;
                objNew.Fee = objInvoiceItem.Fee;
                objNew.adminFee = objInvoiceItem.adminFee;
                objNew.InsuredBusinessName = objInvoiceItem.InsuredBusinessName;
                objNew.InsuredNameFirst = objInvoiceItem.InsuredNameFirst;
                objNew.InsuredNameLast = objInvoiceItem.InsuredNameLast;
                objNew.LynxID = objInvoiceItem.LynxID;
                objNew.PayeeID = objInvoiceItem.PayeeID;
                objNew.Payment = objInvoiceItem.Payment;
                objNew.AuthorizingClientUserID = objInvoiceItem.AuthorizingClientUserID;
                objNew.Amount = objInvoiceItem.Amount;
                objNew.ClientFeeCode = objInvoiceItem.ClientFeeCode;
                objNew.InvoiceDate = objInvoiceItem.InvoiceDate;
                objNew.InvoiceDescription = objInvoiceItem.InvoiceDescription;
                objNew.PayeeName = objInvoiceItem.PayeeName;
                objNew.PayeeCity = objInvoiceItem.PayeeCity;
                objNew.PayeeState = objInvoiceItem.PayeeState;
                objNew.PayeeType = objInvoiceItem.PayeeType;
                objNew.InvoiceItemID = objInvoiceItem.InvoiceItemID;

                col = (Collection)objInvoiceItem.InvoiceIDs;

                foreach (object item in col)
                    objNew.AddInvoiceID = Convert.ToInt64(item.ToString());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objNew;
        }

        /// <summary>
        ///  Procedure : GetCurrencyFormattedString
        ///  DateTime  : 3/18/2005 14:03
        ///  Author    : Dan Price
        ///  Purpose   : Ensure a string representing a dollar amount has a precision of 2 decimal
        ///              places.
        /// </summary>
        /// <param name="dblAmount"></param>
        /// <returns></returns>
        private string GetCurrencyFormattedString(double dblAmount)
        {
            string strAmount = string.Empty;
            long lngPos;
            try
            {
                strAmount = Convert.ToString(dblAmount);
                lngPos = strAmount.IndexOf(".");

                if (lngPos == 0)
                    strAmount = string.Concat(strAmount, ".00");
                else if (lngPos == ((strAmount).Length - 1))
                    strAmount = string.Concat(strAmount, "0");

                return strAmount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
