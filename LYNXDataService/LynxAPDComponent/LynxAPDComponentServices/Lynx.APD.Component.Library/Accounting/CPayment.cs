﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using SiteUtilities;
using ADODB;

namespace Lynx.APD.Component.Library.Accounting
{
    class CPayment
    {
        string MODULE_NAME = "LAPDAccounting.CPayment.";
        MLAPDAccounting mLAPDAccounting = null;
        DataAccessor.CDataAccessor g_objIngresAccessor = null;
        private enum EventCodes
        {
            //eNull = CPayment_CLS_ErrorCodes
        }
        private void Class_Terminate()
        {
            mLAPDAccounting = new MLAPDAccounting();
            try
            {
                mLAPDAccounting.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string MakePayment(long lngClaimAspectID, long lngPayeeID, string strCurrentInvoiceNbr, string strOriginalInvoiceNbr, string strDescription, string strInsuredFirstName, string strInsuredLastName, string strInsuredBusinessName,
                                  string strVehicleYear, string strVehicleMake, string strVehicleModel, string strVehicleBody, string strPayeeAddress1, string strPayeeAddress2, string strPayeeCity,
                                  string strPayeeState, string strPayeeZip, string strPayeeName, string strPayeeTypeCD, string strClaimAmount, string strFeeAmount, string strPaymentTypeCD, int intRentalDays,
                                  int intUserID, string strClaimRepName, long lngLynxID, string strCarrierClaimNumber, long lngInsuranceCompanyID, long lngIngresAccountingID, bool blnIsCeiShop, string strCarrierRepName)
        {

            string PROC_NAME = string.Concat(MODULE_NAME, "MakePayment: ");
            bool blnIngresEnabled;
            string strComment = string.Empty,
                    strVehicleDesc = string.Empty,
                    strMessageInd = string.Empty,
                    strReturnValue = string.Empty,
                    strDispatchNumber = string.Empty,
                    strMessageText = string.Empty,
                    strMakePayment = string.Empty,
                    strSubject = string.Empty,
                    strBody = string.Empty,
                    strFrom = string.Empty,
                    strTo = string.Empty;
            try
            {
                mLAPDAccounting = new MLAPDAccounting();
                g_objIngresAccessor = new DataAccessor.CDataAccessor();
                mLAPDAccounting.InitializeGlobals();

                if (mLAPDAccounting.g_blnDebugMode)
                    mLAPDAccounting.g_objEvents.Trace(string.Concat("lngClaimAspectID = ", lngClaimAspectID, Environment.NewLine, "  Lynx ID = ",
                        lngLynxID, " Carrier Claim Number = ", strCarrierClaimNumber, Environment.NewLine,
                      "  Ins. Co. ID = ", lngInsuranceCompanyID, Environment.NewLine, " Ingres Accounting ID = ", lngIngresAccountingID,
                      "  Ingres Accounting ID = ", lngIngresAccountingID, " CEI = ", blnIsCeiShop, Environment.NewLine,
                      "  Payee ID = ", lngPayeeID, Environment.NewLine,
                      "  CurrentInvoiceNbr = ", strCurrentInvoiceNbr, Environment.NewLine,
                      "  OriginalInvoiceNbr = ", strOriginalInvoiceNbr, Environment.NewLine,
                      "  Description = ", strDescription, Environment.NewLine,
                      "  Insured Name = ", strInsuredFirstName, " ", strInsuredLastName, Environment.NewLine,
                      "  Insured Business Name = ", strInsuredBusinessName, Environment.NewLine,
                      "  Vehicle = ", strVehicleYear, " ", strVehicleMake, " ", strVehicleModel, " ", strVehicleBody, Environment.NewLine,
                      "  Payee Name = ", strPayeeName, Environment.NewLine,
                      "  Payee Address 1 = ", strPayeeAddress1, Environment.NewLine,
                      "  Payee Address 2 = ", strPayeeAddress2, Environment.NewLine,
                      "  Payee City/st/zip = ", strPayeeCity, ", ", strPayeeState, "  ", strPayeeZip, Environment.NewLine,
                      "  Payee Type CD = ", strPayeeTypeCD, Environment.NewLine,
                      "  CLAIM AMOUNT = ", strClaimAmount, "  FEE AMOUNT = ", strFeeAmount, Environment.NewLine,
                      "  Payment Type CD = ", strPaymentTypeCD, "  Rental Days = ", intRentalDays, Environment.NewLine,
                      "  User ID = ", intUserID, " Claim Rep Name = ", strClaimRepName, " Carrier Rep Name = ", strCarrierRepName, Environment.NewLine),
                      string.Concat(PROC_NAME, "Started"));

                //'Check passed parameters.

                mLAPDAccounting.g_objEvents.Assert(lngClaimAspectID > 0, "Nothing passed for Claim Aspect ID.");

                mLAPDAccounting.g_objEvents.Assert(strPaymentTypeCD.Length > 0, "Nothing passed for Payment Type Code.");

                mLAPDAccounting.g_objEvents.Assert(intUserID > 0, "Nothing passed for User ID.");

                mLAPDAccounting.g_objEvents.Assert(strClaimRepName.Length > 0, "Nothing passed for Claim Rep Name.");

                mLAPDAccounting.g_objEvents.Assert(lngLynxID > 0, "Nothing passed for Lynx ID.");

                mLAPDAccounting.g_objEvents.Assert(strPayeeName.Length > 0, "Nothing passed for Payee Name.");

                mLAPDAccounting.g_objEvents.Assert(lngIngresAccountingID > 0, "Nothing passed for Ingres Accounting ID.");

                mLAPDAccounting.g_objEvents.Assert(strCarrierRepName.Length > 0, "Nothing passed for Carrier Rep Name.");

                blnIngresEnabled = (mLAPDAccounting.GetConfig("Accounting/IngresEnabled") == "True" ? true : false);

                //      'if no payment company id was passed then use the default from the config file.
                //'this is a hack to save major changes to the db in the near term.
                //'this hack has been tur-muh-na-ted as the major changes to the db have been implemented.
                //'if lngpaymentcompanyid = 0 then
                //'    lngpaymentcompanyid = clng(getconfig("accounting/defaultpaymentcompanyid"))
                //'end if

                //' if we have a business name for the insured, use it versus first/last

                if (strInsuredBusinessName.Length > 0)
                {
                    strInsuredLastName = strInsuredBusinessName;
                    strInsuredFirstName = string.Empty;
                }
                if (strCurrentInvoiceNbr.Length > 0)
                    strCurrentInvoiceNbr = "0";


                strVehicleDesc = mLAPDAccounting.BuildVehicleString(strVehicleYear, strVehicleMake, strVehicleModel, strVehicleBody);

                if (strVehicleDesc == "")
                    strVehicleDesc = "No vehicle description available";

                strComment = strVehicleDesc;

                if (strOriginalInvoiceNbr.Length > 0)
                    strComment = string.Concat(strComment, " : Original dispatch #", strOriginalInvoiceNbr);


                if (mLAPDAccounting.g_blnDebugMode)
                    mLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Comment = |", strComment, "|"));

                //Insert/Update the Ingres DB with the payment info.

                if (mLAPDAccounting.g_blnDebugMode)
                    mLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Insert/Update Ingres Database"));

                if (blnIsCeiShop)
                    lngPayeeID = Convert.ToInt64(mLAPDAccounting.GetConfig("Accounting/Payment/CEI/@ShopID"));

                if (blnIngresEnabled)
                {
                    //'Enforce presence of a vehicle description.
                    //'Ingres will thrown an unintelligible error message otherwise.


                    if (strVehicleDesc == "")
                        throw new Exception(string.Concat(MLAPDAccounting.LAPDAccounting_ErrorCodes.eIngresRequiresVehicleDesc.ToString(), "Call to Ingres"));

                    mLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eIngres_Recordset);

                    mLAPDAccounting.g_objIngresAccessor.BeginTransaction();

                    mLAPDAccounting.g_objIngresAccessor.FpCreateCommand(mLAPDAccounting.GetConfig("Accounting/IngressBillingSP"), 0);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("retval", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 4, 0);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_claim_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, strClaimAmount);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_claim_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, Left(strCarrierClaimNumber, 20));

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_comment", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 80, Left(strComment, 80));

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_comment_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "");

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_create_dt", DataTypeEnum.adDBTimeStamp, ParameterDirectionEnum.adParamInput, 0, (DateTime.Now.ToString("mm/dd/yyyy")));

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_dispatch_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 4, Convert.ToInt64(strCurrentInvoiceNbr));

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_fee_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, strFeeAmount);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_insrd_first_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, Left(strInsuredFirstName, 20));

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_insrd_last_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, Left(strInsuredLastName, 20));

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_ins_co_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, lngInsuranceCompanyID);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_ins_pc_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, lngIngresAccountingID);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_lcmt_id", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, 0);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_lynx_id", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 10, Convert.ToString(lngLynxID));

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, "");

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, strCarrierRepName);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_shop_location_id", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, lngPayeeID);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_tries", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, 1);

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_user_id_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, Convert.ToString(intUserID));

                    mLAPDAccounting.g_objIngresAccessor.FpAddParam("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, "");

                    strReturnValue = Convert.ToString(mLAPDAccounting.g_objIngresAccessor.FpExecute());

                    strDispatchNumber = Convert.ToString(mLAPDAccounting.g_objIngresAccessor.FpGetParameter("h_dispatch_no"));

                    strMessageText = Convert.ToString(mLAPDAccounting.g_objIngresAccessor.FpGetParameter("h_msg_text"));

                    strMessageInd = Convert.ToString(mLAPDAccounting.g_objIngresAccessor.FpGetParameter("h_msg_ind"));//No longer used

                    mLAPDAccounting.g_objIngresAccessor.FpCleanUp();
                }

                strMakePayment = strDispatchNumber;

                if (mLAPDAccounting.g_blnDebugMode)
                    mLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Insert/Update APD Database"));

                mLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_Recordset);

                mLAPDAccounting.g_objApdRsAccessor.BeginTransaction();

                mLAPDAccounting.g_objApdRsAccessor.ExecuteSpNamedParams(mLAPDAccounting.GetConfig("Accounting/Payment/InsertDetailSP"), "@ClaimAspectID", lngClaimAspectID,
                                                        "@PayeeID", lngPayeeID, "@Description", strDescription, "@DispatchNumber", strDispatchNumber,
                                                        "@PayeeAddress1", strPayeeAddress1, "@PayeeAddress2", strPayeeAddress2, "@PayeeCity", strPayeeCity,
                                                        "@PayeeState", strPayeeState, "@PayeeZip", strPayeeZip, "@PayeeTypeCD", strPayeeTypeCD, "@PayeeName", strPayeeName,
                                                        "@Amount", strClaimAmount, "@PaymentTypeCD", strPaymentTypeCD, "@RentalDays", intRentalDays, "@UserID", intUserID);
                strTo = mLAPDAccounting.GetConfig("Accounting/Payment/CEI/@ToEmail");

                if (strTo != "")
                {
                    strFrom = mLAPDAccounting.GetConfig("Accounting/Payment/CEI/@FromEmail");
                    strSubject = string.Concat("APD Payment of $", strClaimAmount, " for Dispatch #", strDispatchNumber);
                    strBody = string.Concat("Dispatch Number: ", strDispatchNumber, Environment.NewLine,
                              "Payment Amount: $", strClaimAmount, Environment.NewLine,
                              "Shop Name: ", strPayeeName, Environment.NewLine,
                              "APD LYNX ID: ", lngLynxID, Environment.NewLine,
                              "Claims Representative: ", strClaimRepName, Environment.NewLine);

                    if (mLAPDAccounting.g_blnDebugMode)
                        mLAPDAccounting.g_objEvents.Trace(string.Concat(strSubject, Environment.NewLine, strBody), string.Concat(PROC_NAME, "Sending email to ", strTo));

                    mLAPDAccounting.g_objEvents.SendEmail(strFrom, strTo, strSubject, strBody, false, 2);

                }
                if (mLAPDAccounting.g_blnDebugMode)
                    mLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Successful Finish!"));



            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                mLAPDAccounting.TerminateGlobals();
            }
            return strMakePayment;

        }
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

    }
}
