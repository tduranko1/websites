﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using LAPDEcadAccessorMgr;
using DataAccessor = Lynx.APD.Component.Library.DataAccessor;
using SiteUtilities;
using PartnerDataMgr = Lynx.APD.Component.Library.PartnerDataMgr;

namespace Lynx.APD.Component.Library.Accounting
{
    class MLAPDAccounting
    {
        #region Variable Declaration
        public const string APP_NAME = "LAPDAccounting.";
        public string MODULE_NAME = string.Concat(APP_NAME, "MLAPDAccounting.");
        public const long LAPDAccounting_FirstError = 0x80066800;

        //Global events and data access objects.
        public CEvents g_objEvents = null;
        public string g_strSupportDocPath = string.Empty;
        public bool g_blnDebugMode = false;
        PartnerDataMgr.MLAPDPartnerDataMgr objMLAPDPartnerDataMgr = null;
        public DataAccessor.CDataAccessor g_objApdRsAccessor = null;
        public DataAccessor.CDataAccessor g_objApdXmlAccessor = null;
        public DataAccessor.CDataAccessor g_objIngresAccessor = null;

        //I used multiple DataAccessor components here because we have two separate
        //databases to hit, one with two different providers.  All of this has to be
        //wrapped in the same transaction.  You cannot switch provider or database
        //in the middle of a transaction.  Ugly, ugly.

        public string g_strIngresConnect = string.Empty;
        #endregion

        #region Enum Declaration
        //Error codes specific to LAPDAccounting
        public enum LAPDAccounting_ErrorCodes : ulong
        {
            //General error codes applicable to all modules of this component
            eCreateObjectExError = LAPDAccounting_FirstError,
            eIngresReturnedUnknownError = LAPDAccounting_FirstError + 0x50,
            eIngresRequiresVehicleDesc = LAPDAccounting_FirstError + 0x60,
            eIngresRequiresIngresAccountingID = LAPDAccounting_FirstError + 0x70,

            //Module specific error ranges.
            CBilling_CLS_ErrorCodes = LAPDAccounting_FirstError + 0x100,
            eHandlingFeeError = LAPDAccounting_FirstError + 0x110,

            MRecordsetUtils_BAS_ErrorCodes = LAPDAccounting_FirstError + 0x180
        }

        public enum EDatabase
        {
            eAPD_Recordset,
            eAPD_XML,
            eIngres_Recordset
        }
        #endregion

        #region Public Methods

        //API declares

        ///Syntax:      Set obj = CreateObjectEx("DataAccessor.CDataAccessor")
        ///Parameters:  strObjectName = The object to call CreateObject for.
        ///Purpose:     Wraps CreateObject() with better debug information.
        ///Returns:     The Object created or Nothing.

        public object CreateObjectEx(string strObjectName)
        {
            object mobjCreateObject = null;
            Type createdObj = null;
            try
            {
                createdObj = Type.GetTypeFromProgID(strObjectName);
                if (createdObj == null)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(LAPDAccounting_ErrorCodes.eCreateObjectExError), ",", "", ",", "CreateObject('", strObjectName, "') returned Nothing." }));

                mobjCreateObject = Activator.CreateInstance(createdObj);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                createdObj = null;
            }

            return mobjCreateObject;
        }

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string strParam, int intLength)
        {
            string strResult = strParam.Substring(0, intLength);
            return strResult;
        }


        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string strParam, int intLength)
        {
            string strResult = strParam.Substring(strParam.Length - intLength, intLength);
            return strResult;
        }


        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = strParam.Substring(startIndex, intLength);
            return strResult;
        }


        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = strParam.Substring(intstartIndex);
            return strResult;
        }


        //Initializes global objects and variables
        public void InitializeGlobals()
        {
            //Create DataAccessor.

            try
            {
                g_objEvents = new CEvents();
                g_objApdRsAccessor = new DataAccessor.CDataAccessor();
                g_objApdXmlAccessor = new DataAccessor.CDataAccessor();
                g_objIngresAccessor = new DataAccessor.CDataAccessor();

                //Share DataAccessor's events object.
                g_objEvents = g_objApdRsAccessor.mobjEvents;

                //Get path to support document directory
                g_strSupportDocPath = string.Concat(new string[]{System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetEntryAssembly().Location) , "\\" , Left(APP_NAME, (APP_NAME.Length) - 1)});

                //This will give LAPDAccounting its own log file.
                g_objEvents.ComponentInstance = "Accounting";
                g_objApdXmlAccessor.mobjEvents.ComponentInstance = "Accounting";
                g_objIngresAccessor.mobjEvents.ComponentInstance = "Accounting";

                //Initialize our member components first, so we have logging set up.
                g_objApdRsAccessor.InitEvents(string.Concat(new string[]{System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetEntryAssembly().Location), @"\..\config\config.xml"}), "Debug", true);
                g_objApdXmlAccessor.InitEvents(string.Concat(new string[]{System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetEntryAssembly().Location) , @"\..\config\config.xml"}), "Debug", false);
                g_objIngresAccessor.InitEvents(string.Concat(new string[]{System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetEntryAssembly().Location) , @"\..\config\config.xml"}), "Debug", false);

                g_strIngresConnect = GetConfig("Accounting/ConnectionString");
                g_objEvents.Assert(g_strIngresConnect.Length > 0, string.Concat(new string[] { "Blank connection string returned from config file: ", g_strIngresConnect }));

                //Get config debug mode status.
                g_blnDebugMode = g_objEvents.IsDebugMode;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //Terminates global objects and variables
        public void TerminateGlobals()
        {
            try
            {
                g_objApdRsAccessor = null;
                g_objEvents = null;
                g_objIngresAccessor = null;
                g_objApdXmlAccessor = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        ///Syntax:      strSqlDateTime = ConstructSqlDateTime("12032003","013425")
        ///Parameters:  strXmlDate = Date in format "mmddyyyy"
        ///             strXmlTime = Time in format "hhmmss"
        ///             blnNowAsDefault = If true, will use Now if no values passed.
        ///Purpose:     Builds a specifically formatted time string, according to our
        ///             SQL specs, as "yyyy-mm-ddThh:mm:ss"
        ///   Returns:     The date-time string.

        public string ConstructSqlDateTime([Optional] string strXmlDate, [Optional] string strXmlTime, [Optional] bool blnNowAsDefault)
        {
            strXmlTime = null;
            strXmlDate = null;
            blnNowAsDefault = false;
            string strDateTime = string.Empty;
            string strTemp = string.Empty;
            try
            {
                //Should be in format 'mmddyyyy' as per spec.
                if (strXmlDate.Length == 8)
                    strDateTime = string.Concat(new string[] { Right(strXmlDate, 4), "-", Left(strXmlDate, 2), "-", Mid(strXmlDate, 3, 2) });
                else if (blnNowAsDefault)
                    strDateTime = DateTime.Now.ToString("yyyy-mm-dd");

                //Dont bother with time if we dont have date.
                if (strDateTime.Length > 0)
                {
                    //Should be in format 'hhmmss' as per spec.
                    if (strXmlTime.Length == 6)
                        strDateTime = string.Concat(new string[] { strDateTime, "T", Left(strXmlTime, 2), ":", Mid(strXmlTime, 3, 2), ":", Right(strXmlTime, 2) });

                        //If date was passed but no time then use default of 0 time.
                    else if (strXmlDate.Length > 0)
                        strDateTime = string.Concat(strDateTime, "T00:00:00");
                    //If no date or time were passed then use now if allowed.
                    else if (blnNowAsDefault)
                        strDateTime = string.Concat(strDateTime, DateTime.Now.ToString("Thh:mm:ss"));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strDateTime;
        }

        //Returns the requested configuration setting.
        public string GetConfig(string strSetting)
        {
            return g_objEvents.mSettings.GetParsedSetting(strSetting);
        }

        //Points the global data accessor object at the proper database.
        public void ConnectToDB(EDatabase edatabase)
        {
            string strConnect = string.Empty;
            string strConfig = string.Empty;
            try
            {
                //Get configuration settings for FNOL database connect string.
                switch (edatabase)
                {
                    case EDatabase.eIngres_Recordset:
                        strConfig = "Accounting/ConnectionString";
                        break;
                    case EDatabase.eAPD_Recordset:
                        strConfig = "ConnectionStringStd";
                        break;
                    case EDatabase.eAPD_XML:
                        strConfig = "ConnectionStringXml";
                        break;
                }

                strConnect = GetConfig(strConfig);
                g_objEvents.Assert(strConnect.Length > 0, string.Concat(new string[] { "Blank connection string returned from config file: ", strConfig }));

                //Set the connect string.
                switch (edatabase)
                {
                    case EDatabase.eAPD_Recordset:
                        g_objApdRsAccessor.SetConnectString(strConnect);
                        break;
                    case EDatabase.eAPD_XML:
                        g_objApdXmlAccessor.SetConnectString(strConnect);
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //Construct for the invoice the actual filename as it will be stored on disk.
        public string BuildFileName(long lngLynxID)
        {
            int fileCount = 0;
            string strImageFolder = string.Empty;
            string strLynxID = string.Empty;
            string strDateTime = string.Empty;
            string strBuildFileName = string.Empty;
            try
            {
                //Use the current date time to make the file name unique.
                strDateTime = DateTime.Now.ToString("yyyymmddhhmmss");

                //Get the last 4 digits of the lynx id for the file folder hierarchy.
                strLynxID = Right(Convert.ToString(lngLynxID), 4);

                for (fileCount = 1; fileCount <= 4; fileCount++)
                {
                    strImageFolder = string.Concat(strImageFolder, @"\", Mid(strLynxID, fileCount, 1));
                }
                strImageFolder = string.Concat(strImageFolder, @"\");

                //Put it all together now...
                strBuildFileName = string.Concat(new string[] { strImageFolder, "INV", strDateTime, "LYNXID", Convert.ToString(lngLynxID), "Doc.doc" });
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strBuildFileName;
        }

        //Builds the year/make/model/body style string
        public string BuildVehicleString(string strYear, string strMake, string strModel, string strBody)
        {
            string strTemp = string.Empty;
            string strBuildVehicleString = string.Empty;
            try
            {
                strTemp = strYear;
                if (strMake != null)
                    strTemp = string.Concat(strTemp, " ", strMake);
                if (strModel != null)
                    strTemp = string.Concat(strTemp, " ", strModel);
                if (strBody != null)
                    strTemp = string.Concat(strTemp, " ", strBody);

                strBuildVehicleString = strTemp.ToUpper();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strBuildVehicleString;
        }

        ///Syntax:      DoSleep( 500 )
        ///Parameters:  lngMS - milliseconds to sleep.
        ///Purpose:     Wraps Sleep() with calls to VB DoEvents().
        ///Returns:     Nothing.
        public void DoSleep(long lngMS)
        {
            long lCount = 0;
            try
            {
                for (lCount = 1; lCount <= lngMS / 10; lCount++)
                {
                    Thread.Sleep(10);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion
    }
}
