﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Runtime.InteropServices;
using MSXML2;

namespace Lynx.APD.Component.Library.Accounting
{
    /// <summary>
    /// Component EcadAccessor : Module MDomUtils
    /// 
    /// Utility methods used with DOM Documents.
    /// </summary>
    class MDomUtils
    {
        #region Declarations
        private const string APP_NAME = "LAPDAccounting.";
        private string MODULE_NAME = string.Concat(APP_NAME, "MDomUtils.");
        MLAPDAccounting objMLAPDAccounting = null;
        #endregion

        #region Enumerators
        //Internal error codes for this module.
        public enum EventCodes : ulong
        {
            eDomDataValidationFailed = 0x80065000 + 0x100,
            eXmlInvalid,
            eXmlMissingAttribute,
            eXmlMissingElement,
            eXmlEmptyNodeSet
        }
        #endregion

        #region Public Functions

        /// <summary>
        /// oads the passed Dom from the passed string and reports any errors.
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="strXml"></param>
        /// <param name="strCallerName"></param>
        /// <param name="strXmlDescription"></param>
        public void LoadXml(ref DOMDocument40 objDom, string strXml, [Optional] string strCallerName, [Optional] string strXmlDescription)
        {
            bool blnValidXml;
            try
            {
                //loadXml returns true if the load succeeded.
                blnValidXml = objDom.loadXML(strXml);

                //We can double check for problems by looking at the parse error code.
                blnValidXml = (blnValidXml && Convert.ToBoolean(objDom.parseError.errorCode == 0));

                //If either test proved problematic, raise an error.
                if (!blnValidXml)
                    throw new Exception(string.Concat(EventCodes.eXmlInvalid, strCallerName, ":LoadXml",
                        "The ", strXmlDescription, " xml could not be loaded because:"
                        , Environment.NewLine, FormatDomParseError(objDom.parseError)));


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Loads the passed Dom from the passed file name string and reports any errors.
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="strFileName"></param>
        /// <param name="strCallerName"></param>
        /// <param name="strXmlDescription"></param>
        public void LoadXmlFile(ref DOMDocument40 objDom, string strFileName, [Optional]string strCallerName, [Optional]string strXmlDescription)
        {
            bool blnValidXml;

            try
            {
                //loadXml returns true if the load succeeded.
                blnValidXml = objDom.load(strFileName);

                //We can double check for problems by looking at the parse error code.
                blnValidXml = (blnValidXml && Convert.ToBoolean(objDom.parseError.errorCode == 0));

                //If either test proved problematic, raise an error.
                if (!blnValidXml)
                    throw new Exception(string.Concat(EventCodes.eXmlInvalid, strCallerName, ":LoadXmlFile",
                        "The ", strXmlDescription, " xml could not be loaded because:"
                        , Environment.NewLine, FormatDomParseError(objDom.parseError), "      File: ", strFileName));

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Returns the passed XML escaped of any bad characters.
        ///  Cheese alert - we do this just by loading it into
        ///  a DOM and then converting it back into a string.
        /// </summary>
        /// <param name="strXml"></param>
        /// <returns></returns>
        public string EscapeXml(ref string strXml)
        {
            DOMDocument40 objDom = null;
            try
            {
                objDom = new DOMDocument40();
                LoadXml(ref objDom, strXml, "CleanUpXml", "EscapeXml");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDom = null;
            }
            return objDom.xml;
        }

        /// <summary>
        /// Returns the specified child node of the passed node.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public XmlNode GetChildNode(XmlNode objNode, string strPath, [Optional]bool blnMustExist)
        {
            XmlNode objChild = null;
            XmlNode objChildNode = null;

            blnMustExist = false;
            try
            {
                objChild = objNode.SelectSingleNode(strPath);
                if (objChild == null)
                {
                    if (blnMustExist == true)
                        throw new Exception(string.Concat(EventCodes.eXmlMissingElement, "GetChildNode('" + strPath + "')", string.Concat(objNode.Name, " is missing a '" + strPath + "' child node.")));
                }
                else
                    objChildNode = objChild;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objChild = null;
            }
            return objChildNode;
        }

        /// <summary>
        ///  Returns the specified child node text of the passed node.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public string GetChildNodeText(ref XmlNode objNode, string strPath, [Optional]bool blnMustExist)
        {
            XmlNode objChild = null;
            string strChildNode = string.Empty;

            blnMustExist = false;
            try
            {
                objChild = GetChildNode(objNode, strPath, blnMustExist);

                if (objChild == null)
                    strChildNode = string.Empty;
                else
                    strChildNode = objChild.InnerText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strChildNode;
        }

        /// <summary>
        /// Returns a node list for specified XPATH of the passed node.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public XmlNodeList GetChildNodeList(ref XmlNode objNode, string strPath, [Optional]bool blnMustExist)
        {
            XmlNodeList objChildList = null;
            XmlNodeList objSelectedChild = null;

            blnMustExist = false;
            try
            {
                objChildList = objNode.SelectNodes(strPath);

                if (objChildList == null)
                {
                    if (blnMustExist)
                        throw new Exception(string.Concat(EventCodes.eXmlEmptyNodeSet, "GetChildNodeList('" + strPath + "')", objNode.Name, " is missing a '" + strPath + "' child nodes."));

                    else if (objChildList.ToString().Length == 0 && blnMustExist)
                        throw new Exception(string.Concat(EventCodes.eXmlEmptyNodeSet, "GetChildNodeList('" + strPath + "')", objNode.Name, " is missing a '" + strPath + "' child nodes."));
                    else
                        objSelectedChild = objChildList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objChildList = null;
            }
            return objSelectedChild;
        }

        /// <summary>
        /// Syntax:      ValidateDomData( objDom, "Validate.xsl" )
        /// Parameters:  domXML - the xml to validate.
        ///              strXslFileName - the xsl file to validate with.
        ///              objEvents - events object borrowed from caller.
        /// Purpose:     Uses a style sheet to validate the passed Dom.
        ///              This style sheet will transform to an empty string if everything
        ///              validates just fine, or will return error strings compatible
        ///              with SiteUtilities.CEvents.HandleMultiEvents().
        /// Returns:     Nothing.  Will Raise if the validation failed.
        /// 
        /// </summary>
        /// <param name="domXML"></param>
        /// <param name="strXslFileName"></param>
        /// <param name="objEvents"></param>
        /// <param name="blnRaise"></param>
        /// <returns></returns>
        public string ValidateDomData(ref DOMDocument domXML, ref string strXslFileName, object objEvents, [Optional]bool blnRaise)
        {
            string PROC_NAME = string.Empty;
            string strValid = string.Empty;
            DOMDocument40 domXSL = null;

            blnRaise = true;

            string strResult = string.Empty;
            bool g_blnDebugMode = false;
            try
            {
                objMLAPDAccounting = new MLAPDAccounting();
                PROC_NAME = string.Concat(MODULE_NAME, "ValidateDomData: ");

                //Get debug mode status.
                g_blnDebugMode = objMLAPDAccounting.g_objEvents.IsDebugMode;

                if (g_blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat("XML = ", domXML.xml), string.Concat(PROC_NAME, "Validating XML versus file ", strXslFileName));

                //Load the XSL to validate with.
                domXSL = new DOMDocument40();

                LoadXmlFile(ref domXSL, strXslFileName, PROC_NAME, "Validation XSL");

                //Transform the XML with the XSL style sheet.
                strResult = domXML.transformNode(domXSL);

                if (strResult.Substring(0, 1) == "|")
                    strResult = strResult.Substring(Convert.ToInt32(strResult), 2);


                strValid = strResult;

                //If the result has contents then pass along then raise.
                if ((strResult.Length) > 0)
                    objMLAPDAccounting.g_objEvents.HandleMultiEvents(Convert.ToInt32(EventCodes.eDomDataValidationFailed), PROC_NAME, strResult);
                else
                    if (g_blnDebugMode)
                        objMLAPDAccounting.g_objEvents.Trace(string.Empty, string.Concat(PROC_NAME, "Passed Data Validation"));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                domXSL = null;
            }
            return strValid;
        }

        /// <summary>
        /// Adds an attribute to the passed dom element
        /// </summary>
        /// <param name="objElem"></param>
        /// <param name="strName"></param>
        /// <param name="strValue"></param>
        public void AddAttribute(ref IXMLDOMElement objElem, string strName, string strValue)
        {
            IXMLDOMAttribute objAtt = null;
            try
            {
                objAtt = objElem.ownerDocument.createAttribute(strName);
                objAtt.value = strValue;
                objElem.setAttributeNode(objAtt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objAtt = null;
            }
        }

        /// <summary>
        /// Adds an attribute to the passed dom element
        /// </summary>
        /// <param name="objElem"></param>
        /// <param name="strName"></param>
        /// <returns></returns>
        public IXMLDOMElement AddElement(IXMLDOMElement objElem, string strName)
        {
            IXMLDOMElement objNewElem = null;
            try
            {
                objNewElem = objElem.ownerDocument.createElement(strName);
                objElem.appendChild(objNewElem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objNewElem;
        }

        /// <summary>
        /// Renames an attribute of the passed dom element
        /// </summary>
        /// <param name="objElem"></param>
        /// <param name="strPath"></param>
        /// <param name="strNewName"></param>
        public void RenameAttribute(ref IXMLDOMElement objElem, string strPath, string strNewName)
        {
            IXMLDOMAttribute objAtt = null;
            string strValue = string.Empty;
            try
            {
                objAtt = (IXMLDOMAttribute)objElem.selectNodes(strPath); //(IXMLDOMAttribute)GetChildNode(objElem, strPath, false); 

                if (objAtt != null)
                {
                    strValue = objAtt.text;
                    objElem.removeAttribute(objAtt.name);
                    AddAttribute(ref objElem, strNewName, strValue);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Renames all matching attributes below in the passed dom element.
        /// </summary>
        /// <param name="objElem"></param>
        /// <param name="strOldName"></param>
        /// <param name="strNewName"></param>
        public void RenameAllAttributes(ref IXMLDOMElement objElem, string strOldName, string strNewName)
        {
            IXMLDOMElement objDomElemAppend = null;
            try
            {
                //Rename any child attribute that matches.
                RenameAttribute(ref objElem, string.Concat("@", strOldName), strNewName);

                //Now recursively do the same through all children.
                if (objElem.childNodes.length > 0)
                {
                    foreach (IXMLDOMNode objChild in objElem.childNodes)
                    {
                        if (objChild != null)
                        {
                            objDomElemAppend = (IXMLDOMElement)objChild;
                            RenameAllAttributes(ref objDomElemAppend, strOldName, strNewName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Removes all blank attributes of the passed element.
        /// </summary>
        /// <param name="objElem"></param>
        public void RemoveBlankAttributes(IXMLDOMElement objElem)
        {
            IXMLDOMAttribute objAtt = null;
            int intIdx = 0;
            try
            {
                while (intIdx < objElem.attributes.length)
                {
                    objAtt = (IXMLDOMAttribute)objElem.attributes[intIdx];
                    if (objAtt.text == string.Empty)
                        objElem.removeAttribute(objAtt.nodeName);
                    else
                        intIdx = intIdx + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Removes all blank attributes below in the passed dom element.
        /// </summary>
        /// <param name="objElem"></param>
        public void RemoveAllChildBlankAttributes(IXMLDOMElement objElem)
        {
            IXMLDOMElement objXmlElemAppend = null;
            try
            {
                //Remove any blank child attributes.
                RemoveBlankAttributes(objElem);

                //Now recursively do the same through all children.
                if (objElem.childNodes.length > 0)
                {
                    foreach (IXMLDOMNode objChild in objElem.childNodes)
                    {
                        if (objChild != null)
                        {
                            objXmlElemAppend = (IXMLDOMElement)objChild;
                            RemoveBlankAttributes(objXmlElemAppend);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Syntax:      MapAPDReferenceData( objData, "DataMap.xml" )
        /// Parameters:  objDom - the DOM document to convert.
        ///              strMapFile - the absolute path to the map file.
        /// Purpose:     Uses the map file to convert codes to reference values.
        ///              Requires that the DOM be formatted to the APD DB standard.
        /// Returns:     Nothing.
        /// </summary>
        /// <param name="objDom"></param>
        /// <param name="strMapFile"></param>
        /// <param name="objEvents"></param>
        public void MapAPDReferenceData(ref DOMDocument40 objDom, string strMapFile, object objEvents)
        {
            string PROC_NAME = string.Empty;

            //Object variable declarations
            DOMDocument40 objMapDom = null;
            IXMLDOMNodeList objMapList = null;
            IXMLDOMNodeList objAttParentList = null;
            IXMLDOMNodeList objReplaceList = null;
            IXMLDOMElement objXmlDomElem = null;

            string strCodeAtt = string.Empty,
                strParent = string.Empty,
                strCode = string.Empty,
                strList = string.Empty,
                strMapAtt = string.Empty,
                strValue = string.Empty;
            try
            {
                PROC_NAME = string.Concat(MODULE_NAME, "MapAPDReferenceData: ");
                objMapDom = new DOMDocument40();
                objMLAPDAccounting = new MLAPDAccounting();

                //Load up the map file into a dom.
                LoadXmlFile(ref objMapDom, strMapFile, PROC_NAME, "Map");

                //Get all Map elements in the map file.
                objMapList = objMapDom.selectNodes("//Map");

                //Loop through map elements.
                foreach (IXMLDOMNode objMapNode in objMapList)
                {
                    //Get a list of Replace elements under the map element.
                    objReplaceList = objMapNode.selectNodes("Replace");

                    //Extract map attributes.
                    strCodeAtt = objMapNode.selectSingleNode("@Att").text;
                    strParent = objMapNode.selectSingleNode("@Parent").text;
                    strList = objMapNode.selectSingleNode("@List").text;
                    strMapAtt = objMapNode.selectSingleNode("@MapAtt").text;


                    //Enforce some kind of parential presence.
                    if (strParent == string.Empty)
                        strParent = "Root";

                    //Get a list of attributes matching the code for this map element.
                    objAttParentList = objDom.selectNodes(string.Concat("//", strParent, "//*[@", strCodeAtt, "]"));

                    //Loop through attributes list.
                    foreach (IXMLDOMNode objAttParentNode in objAttParentList)
                    {
                        //Get the code value from the code attribute.
                        strCode = objAttParentNode.selectSingleNode(string.Concat("@", strCodeAtt)).text;

                        //Convert codes to upper, to cover for Test Track 874.
                        //strCode = UCase(strCode)

                        //If strList is blank then just copy the value of Att.
                        if (strList == string.Empty)
                            strValue = strCode;
                        //Otherwise go look up the reference value.
                        else
                            //Get a reference value for this code.
                            if ((strCode.Length) > 0)
                                strValue = objDom.selectSingleNode(string.Concat("/Root/Reference", "[@List='" + strList + "']"
                                    , "[@ReferenceID='" + strCode + "']"
                                    , "/@Name")).text;
                            else
                                strValue = string.Empty;

                        //Loop through the replace elements and do any search-and-replace.
                        foreach (XmlNode objReplaceNode in objReplaceList)
                            strValue = (strValue.Replace(objReplaceNode.SelectSingleNode("@Find").InnerText, objReplaceNode.SelectSingleNode("@Insert").InnerText));


                        //Because different stored procedures return the reference data
                        //in different formats (i.e. List="Gender" vs. List="GenderCD")
                        //we have to allow for more than one way to convert a code.
                        //Grrrr... so only add an attribute if it doesn't already exist
                        //AND have a non-blank value.
                        if (objAttParentNode.selectSingleNode(string.Concat("@", strMapAtt)).text == string.Empty)
                        {
                            //Create a new attribute and add it to the parent element.
                            objXmlDomElem = (IXMLDOMElement)objAttParentNode;
                            AddAttribute(ref objXmlDomElem, strMapAtt, strValue);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Formats a friendly error string from the passed dom parse error object.
        /// </summary>
        /// <param name="objErr"></param>
        /// <returns></returns>
        public string FormatDomParseError(IXMLDOMParseError objErr)
        {
            try
            {
                return string.Concat("      Error code (", objErr.errorCode, ") was raised during parsing.", Environment.NewLine, "      File location: line = ", objErr.line, " and column = ", objErr.linepos, ".", Environment.NewLine,
                      "      Bad XML: '", objErr.srcText, "'", Environment.NewLine
                     , "      Reason given: ", objErr.reason);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Transforms the passed XML DOM to an XML DOM via the passed XSL file.
        ///  Validates the original XML and XSL documents and raises intelligent errors.
        ///  Does not validate the returned DOM beyond that done by MSXML.
        /// </summary>
        /// <param name="domXML"></param>
        /// <param name="strXslPath"></param>
        /// <param name="objEvents"></param>
        /// <returns></returns>
        public DOMDocument40 TransformDomAsDom(ref DOMDocument40 domXML, string strXslPath, object objEvents)
        {
            string PROC_NAME = string.Concat(MODULE_NAME, "TransformXmlAsDom: ");

            //Objects that need clean-up.
            DOMDocument40 domXSL = null;
            DOMDocument40 domResult = null;

            //Primitives that dont.
            bool blnDebugMode;

            try
            {
                objMLAPDAccounting = new MLAPDAccounting();
                objMLAPDAccounting.g_objEvents.Assert((domXML != null), "Passed XML DOM was Nothing.");
                objMLAPDAccounting.g_objEvents.Assert(strXslPath != string.Empty, "Passed XSL file path string was blank.");

                //Get debug mode status.
                blnDebugMode = objMLAPDAccounting.g_objEvents.IsDebugMode;

                domXSL = new DOMDocument40();
                domResult = new DOMDocument40();

                if (blnDebugMode)
                    objMLAPDAccounting.g_objEvents.Trace(string.Concat("StyleSheet = ", strXslPath), PROC_NAME);

                //Load and validate the style sheet.
                LoadXmlFile(ref domXSL, strXslPath, PROC_NAME, "XSL Style Sheet");

                //Transform the XML with the XSL style sheet.
                domXML.transformNodeToObject(domXSL, domResult);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return domResult;
        }


        /// <summary>
        /// Transforms the passed XML string to an XML DOM via the passed XSL file.
        /// Validates the original XML and XSL documents and raises intelligent errors.
        /// Does not validate the returned DOM beyond that done by MSXML.
        /// </summary>
        /// <param name="strXml"></param>
        /// <param name="strXslPath"></param>
        /// <param name="objEvents"></param>
        /// <returns></returns>
        public DOMDocument40 TransformXmlAsDom(ref string strXml, string strXslPath, ref object objEvents)
        {
            DOMDocument40 domXML = null;
            string PROC_NAME = string.Empty;
            try
            {

                PROC_NAME = string.Concat(MODULE_NAME, "TransformXmlAsDom: ");
                objMLAPDAccounting.g_objEvents.Assert(strXml != string.Empty, "Passed XML string was blank.");

                //Load and validate the param XML
                LoadXml(ref domXML, strXml, PROC_NAME, "Source XML");

                return TransformDomAsDom(ref domXML, strXslPath, objEvents);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Transforms the passed XML string to an XML DOM via the passed XSL file.
        /// Validates all three documents and raises intelligent errors.
        /// </summary>
        /// <param name="domXML"></param>
        /// <param name="strXslPath"></param>
        /// <param name="objEvents"></param>
        /// <returns></returns>
        public string TransformDomAsXml(DOMDocument40 domXML, string strXslPath, object objEvents)
        {
            try
            {
                return TransformDomAsDom(ref domXML, strXslPath, objEvents).xml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Transforms the passed XML string to an XML DOM via the passed XSL file.
        /// Validates all three documents and raises intelligent errors.
        /// </summary>
        /// <param name="strXml"></param>
        /// <param name="strXslPath"></param>
        /// <param name="objEvents"></param>
        /// <returns></returns>
        public string TransformXmlAsXml(ref string strXml, string strXslPath, ref object objEvents)
        {
            DOMDocument40 domXML = null;
            string PROC_NAME = string.Empty;
            try
            {
                objMLAPDAccounting = new MLAPDAccounting();
                PROC_NAME = string.Concat(MODULE_NAME, "TransformXmlAsDom: ");
                objMLAPDAccounting.g_objEvents.Assert(strXml != string.Empty, "Passed XML string was blank.");

                //Load and validate the param XML
                LoadXml(ref domXML, strXml, PROC_NAME, "Source XML");

                return TransformDomAsXml(domXML, strXslPath, objEvents);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
