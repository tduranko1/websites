﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;
using System.IO;
using MSXML2;
using DataAccessor = Lynx.APD.Component.Library.DataAccessor;
using ADODB;
using DocumentMgr;
using System.Configuration;

namespace Lynx.APD.Component.Library.Accounting
{
    class CBilling
    {
        private string MODULE_NAME = "LAPDAccounting.CBilling.";

        MLAPDAccounting mobjMLAPDAccounting = null;
        CInvoiceItemManager mobjInvoiceItemManager = null;
        MDomUtils mobjMDomUtils = null;
        MPPGTransactions mobjMPPGTransactions = null;
        public DataAccessor.CDataAccessor objIngresAccessor = null;

        string m_strRevisedInvoiceXML = string.Empty;
        bool m_blnReleaseFundsMode = false;

        //Internal error codes for this module.
        private enum EventCodes : ulong
        {
            eNoOpenBillingOrPayment = MLAPDAccounting.LAPDAccounting_ErrorCodes.CBilling_CLS_ErrorCodes,
            eMissingDispatchNumber,
            eNoOpenPayment,
            eNoOpenBilling,
            eBulkBillingAndPayment
        }

        //Backup global termination
        private void Class_Terminate()
        {
            mobjMLAPDAccounting = new MLAPDAccounting();
            mobjMLAPDAccounting.TerminateGlobals();
        }

        /// Procedure : BillAndCreateInvoice
        /// DateTime  : 3/16/2005 10:03
        /// Author    : CSR0901
        /// Purpose   : 1 - Pull APD Invoice records for the passed ClaimAspectID.
        /// 2 - Apply business rules for totaling payments and attaching fees.
        /// 3 - Send the items to Ingres.
        /// 4 - Retrieve a Dispatch number for each item sent to Ingres..
        /// 5 - Apply the Dispatch number to the APD Invoice records that comprise the Dispatch
        ///     and update the Status of those records.
        /// 6 - Create a 'paper' Invoice document.
        /// 7 - Attach the document to the claim.
        public long BillAndCreateInvoice(long lngInsuranceCompanyID, long lngClaimAspectID, long intUserID)
        {
            DOMDocument40 objXmlDoc = null;

            //Primitives
            string strXML = string.Empty;
            bool blnIngresEnabled = false;
            int intSentToIngresCount = 0;
            string strParamXML = string.Empty;
            CDocMgr objDocMgr = null;
            try
            {
                //Objects that must be destroyed.

                mobjMDomUtils = new MDomUtils();
                mobjMLAPDAccounting = new MLAPDAccounting();

                string PROC_NAME = string.Concat(MODULE_NAME, "BillAndCreateInvoice: ");

                mobjMLAPDAccounting.InitializeGlobals();

                if (mobjMLAPDAccounting.g_blnDebugMode)
                    mobjMLAPDAccounting.g_objEvents.Trace(string.Concat(new string[] { "  Claim Aspect ID = ", Convert.ToString(lngClaimAspectID), Environment.NewLine, "  User ID = ", Convert.ToString(intUserID), Environment.NewLine, PROC_NAME, "Started" }));


                //Check passed parameters.
                mobjMLAPDAccounting.g_objEvents.Assert(lngInsuranceCompanyID > 0, "Nothing passed for Insurance Company ID.");
                mobjMLAPDAccounting.g_objEvents.Assert(lngClaimAspectID > 0, "Nothing passed for Claim Aspect ID.");
                mobjMLAPDAccounting.g_objEvents.Assert(intUserID > 0, "Invalid Date passsed.");


                //Are the Ingres DB calls enabled?
                blnIngresEnabled = Convert.ToBoolean(mobjMLAPDAccounting.GetConfig("Accounting/IngresEnabled=true")) ? true : false;


                // Extract all non-billed billing and payment records from APD as XML.
                if (mobjMLAPDAccounting.g_blnDebugMode)
                    mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Extracting APD Records"));

                //Load accounting pickup info into a DOM.
                objXmlDoc = new DOMDocument40();

                //Flip this config to True to load the accounting xml from disk.
                if (mobjMLAPDAccounting.GetConfig("Accounting/LoadFromFile") == "true")
                    mobjMDomUtils.LoadXmlFile(ref objXmlDoc, mobjMLAPDAccounting.GetConfig("Accounting/LoadFromFile/@file"), PROC_NAME, "Test Billing Pickup Data");

                //Get the accounting xml from the database.
                else
                {
                    mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_XML);

                    // TODO:  proc should pull all billing and payment records billed or otherwise and compute new total.
                    strXML = mobjMLAPDAccounting.g_objApdXmlAccessor.OpenRecordsetAsClientSideXML(mobjMLAPDAccounting.GetConfig("Accounting/Billing/PickupBillingSP"), "P", lngInsuranceCompanyID, lngClaimAspectID, string.Concat(new string[] { "'", mobjMLAPDAccounting.ConstructSqlDateTime("", "", true), "'" }), intUserID);
                    mobjMDomUtils.LoadXml(ref objXmlDoc, strXML, PROC_NAME, "Billing");
                }

                // Create the Invoice Item Manager.
                mobjInvoiceItemManager = new CInvoiceItemManager();

                // Initialize the InvoiceItemManager which will in turn construct the final InvoiceItems that will be sent to Ingres and
                // appear on the Invoice document or replace the raw Invoice nodes in the XML source for the billing file.
                mobjInvoiceItemManager.Initialize(objXmlDoc.xml, CInvoiceItemManager.eInvoiceType.ePaper);

                // Send InvoiceItems to Ingres and update the retrieved Dispatch number to the APD database and the InvoiceItem objects.
                if (blnIngresEnabled)
                    intSentToIngresCount = mobjInvoiceItemManager.SendToIngres();


                // If items were sent to Ingres, indicated by a returned count, an invoice document must be created.
                if (intSentToIngresCount > 0)
                {
                    // Generate the invoice and save to disk.
                    // Create the XML to be used to generate Invoice document.
                    strXML = mobjInvoiceItemManager.CreateInvoiceSourceXML(CInvoiceItemManager.eInvoiceType.ePaper);

                    //objDocMgr = CreateObjectEx("DocumentMgr.CDocMgr")
                    objDocMgr = new CDocMgr();

                    // Build xml input required by the DocumentMgr.GenerateInvoiceDocument function.
                    //strParamXML = "<Invoice NTUserID='" & strCSRNo & "' ProtectDocument='True' DocumentType='Invoice' />"
                    strParamXML = "<Invoice ProtectDocument='true' DocumentType='Invoice' />";


                    if (mobjMLAPDAccounting.g_blnDebugMode)
                        mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Calling DocumentMgr to create invoice document."));

                    // Call DocumentMgr to create invoice document and attach that document to the claim.
                    objDocMgr.GenerateInvoiceDocument(strParamXML, strXML);
                }


                if (mobjMLAPDAccounting.g_blnDebugMode)
                    mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Successful Finish!"));



                mobjMLAPDAccounting.TerminateGlobals();

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objXmlDoc = null;
                mobjInvoiceItemManager = null;
                objDocMgr = null;
            }
            return 0;
        }


        ///Procedure : ElectronicInvoicing
        ///DateTime  : 3/16/2005 10:03
        ///Author    : CSR0901
        ///Purpose   : 1 - Pull APD Invoice records for the passed ClaimAspectID.
        /// 2 - Apply business rules for totaling payments and attaching fees.
        /// 3 - Send the items to Ingres.
        /// 4 - Retrieve a Dispatch number for each item sent to Ingres.
        /// 5 - Apply the Dispatch number to the APD Invoice records that comprise the Dispatch
        ///     and update the Status of those records.
        /// 6 - Create the source XML that will be sent to GLAXIS and used to create the billing file
        ///     which will be transmitted to the client.
        /// 7 - Post the XML to GLAXIS.
        public long ElectronicInvoicing(long lngInsuranceCompanyID, DateTime dtToDate)
        {
            //Objects that must be destroyed.
            DOMDocument40 objXmlDoc = null;
            mobjInvoiceItemManager = new CInvoiceItemManager();

            //Primitives
            string strEnabled = string.Empty;
            string strPPGDestCode = string.Empty;
            string strPPGXML = string.Empty;
            string strXML = string.Empty;
            string strXmitUrl = string.Empty;
            bool blnIngresEnabled = false;
            int intSentToIngresCount = 0;
            DateTime Expression;
            try
            {
                mobjMLAPDAccounting = new MLAPDAccounting();
                mobjMDomUtils = new MDomUtils();
                mobjMLAPDAccounting = new MLAPDAccounting();
                mobjMPPGTransactions = new MPPGTransactions();

                string PROC_NAME = string.Concat(MODULE_NAME, "ElectronicInvoicing: ");

                mobjMLAPDAccounting.InitializeGlobals();

                if (mobjMLAPDAccounting.g_blnDebugMode)
                    mobjMLAPDAccounting.g_objEvents.Trace(string.Concat(new string[] { "  Ins. Co. ID = ", Convert.ToString(lngInsuranceCompanyID), Environment.NewLine, "  To Date = ", Convert.ToString(dtToDate), Environment.NewLine, PROC_NAME, "Started" }));


                //Check passed parameters.
                mobjMLAPDAccounting.g_objEvents.Assert(lngInsuranceCompanyID > 0, "Nothing passed for Insurance Company ID.");

                mobjMLAPDAccounting.g_objEvents.Assert(DateTime.TryParse(Convert.ToString(dtToDate), out Expression), "Invalid Date passsed.");


                //Are the Ingres DB calls enabled?
                blnIngresEnabled = Convert.ToBoolean(mobjMLAPDAccounting.GetConfig("Accounting/IngresEnabled = true")) ? true : false;


                // Extract all non-billed billing and payment records from APD as XML.


                if (mobjMLAPDAccounting.g_blnDebugMode)
                    mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Extracting APD Records"));

                //Load accounting pickup info into a DOM.
                objXmlDoc = new DOMDocument40();

                //Flip this config to True to load the accounting xml from disk.
                if (mobjMLAPDAccounting.GetConfig("Accounting/LoadFromFile") == "true")
                    mobjMDomUtils.LoadXmlFile(ref objXmlDoc, mobjMLAPDAccounting.GetConfig("Accounting/LoadFromFile/@file"), PROC_NAME, "Test Billing Pickup Data");
                //Get the accounting xml from the database.
                else
                {
                    mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_XML);

                    // TODO:  proc should pull all billing and payment records billed or otherwise and compute new total.
                    strXML = mobjMLAPDAccounting.g_objApdXmlAccessor.OpenRecordsetAsClientSideXML(mobjMLAPDAccounting.GetConfig("Accounting/Billing/PickupBillingElectronicSP"), "F", lngInsuranceCompanyID, string.Concat(new string[] { "'", Convert.ToString(dtToDate), "'" }), 0);
                    mobjMDomUtils.LoadXml(ref objXmlDoc, strXML, PROC_NAME, "Billing");
                }

                // Create the Invoice Item Manager.
                mobjInvoiceItemManager = new CInvoiceItemManager();

                // Initialize the InvoiceItemManager which will in turn construct the final InvoiceItems that will be sent to Ing
                // appear on the Invoice document or replace the raw Invoice nodes in the XML source for the billing file.
                mobjInvoiceItemManager.Initialize(objXmlDoc.xml, CInvoiceItemManager.eInvoiceType.ePaper);

                //Send InvoiceItems to Ingres and update the retrieved Dispatch number to the APD database and the InvoiceItem o
                if (blnIngresEnabled)
                    intSentToIngresCount = mobjInvoiceItemManager.SendToIngres();

                // If Ingres calls are enabled in this environment but there was nothing to send or nothing was sent successfully
                if (intSentToIngresCount == 0 && blnIngresEnabled == true)
                    if (mobjMLAPDAccounting.g_blnDebugMode)
                        mobjMLAPDAccounting.g_objEvents.Trace("", "Nothing to invoice. Terminating Process.");
                    else
                    {
                        // Create the Billing file XML to be sent to GLAXIS.
                        strXML = mobjInvoiceItemManager.CreateInvoiceSourceXML(CInvoiceItemManager.eInvoiceType.eFile);


                        //Get the 'nickname' used to identify the current company in the SOAP header.
                        strPPGDestCode = mobjMLAPDAccounting.GetConfig(string.Concat(new string[] { "Accounting/ElectronicInvoice/Insurance[@ID='", Convert.ToString(lngInsuranceCompanyID), "']/@PPGDe" }));

                        //Do we override the electronic invoice in this environment?
                        strEnabled = mobjMLAPDAccounting.GetConfig(string.Concat(new string[] { "PPG_SOAP_Header/DestinationPID[@name='", strPPGDestCode, "']/@enabled" }));

                        //Get the submit url to GO from the config xml file.  The electronic invoicing process will use the same URLs a
                        strXmitUrl = mobjMLAPDAccounting.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                        //Add the PPG SOAP wrapper to the Invoice XML.
                        strPPGXML = mobjMPPGTransactions.BuildTransactionForPPG("Invoice", strXML, strPPGDestCode, strPPGDestCode);

                        if (strEnabled != "true")
                        {
                            if (mobjMLAPDAccounting.g_blnDebugMode)
                                mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "ELECTRONIC TRANSMISSION DISABLED TO ", strPPGDestCode, " IN THE ", mobjMLAPDAccounting.g_objEvents.mSettings.Environment }));
                            else
                            {
                                //Electronic transmission is now reduced to just and http post to GO.
                                mobjMPPGTransactions.XmlHttpPost(strXmitUrl, strPPGXML);
                            }
                        }
                    }

                if (mobjMLAPDAccounting.g_blnDebugMode)
                    mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Successful Finish!"));

                mobjMLAPDAccounting.TerminateGlobals();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objXmlDoc = null;
                mobjInvoiceItemManager = null;
            }
            return 0;
        }


        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }


        public int ReleaseFunds(string strXML)
        {
            DOMDocument40 objReleasePaymentXML = null;
            DOMDocument40 objOriginalTxXML = null;
            //object objIngresAccessor=null;
            IXMLDOMElement objRoot = null;
            DOMDocument40 objRevisedInvoiceXML = null;
            IXMLDOMElement objRevisedInvoiceFeeItem = null;

            string strInsuranceCompanyName = string.Empty;
            string strLynxID = string.Empty;
            string strClaimAspectNumber = string.Empty;
            string strDispatchNumber = string.Empty;
            string strOriginalDispatchNumber = string.Empty;
            string strNewDispatchNumber = string.Empty;
            string strInvoiceDate = string.Empty;
            string strInvoiceSubmitDate = string.Empty;
            string strInvoiceAmount = string.Empty;
            string strPayeeID = string.Empty;
            string strPayeeName = string.Empty;
            string strPayeePhone = string.Empty;
            string strPayeeWarrantyFlag = string.Empty;
            string strDescription = string.Empty;
            string strHistoryComments = string.Empty;
            string strReleaseAmount = string.Empty;
            string strDeductibleAmount = string.Empty;
            string strTaxTotalAmount = string.Empty;
            string strWarrantyFlag = string.Empty;
            string strFileName = string.Empty;
            string strImageRoot = string.Empty;
            string strMessageText = string.Empty;
            string strMessageInd = string.Empty;
            string strReturnValue = string.Empty;
            string strIngresProc = string.Empty;
            string strUpdateDispatchProc = string.Empty;
            string strUserID = string.Empty;
            string strOldInvoiceIDFee = string.Empty;
            string strNewFeeID = string.Empty;
            string strNewFeeServices = string.Empty;
            string strDispositionCD = string.Empty;
            string strFeeAmount = string.Empty;
            bool blnIngresEnabled = false;
            string strManualFormRequestTo = string.Empty;
            string h_apd_lynx_id = string.Empty;
            string h_apd_shop_id = string.Empty;
            string h_claim_amt = string.Empty;
            string h_claim_no = string.Empty;
            string h_comment_text = string.Empty;
            string h_commit_ind = string.Empty;
            string h_dispatch_no = string.Empty;
            string h_fee_amt = string.Empty;
            string h_rtlr_fee_amt = string.Empty;
            string h_insrd_first_nm = string.Empty;
            string h_insrd_last_nm = string.Empty;
            string h_ins_pc_uid_no = string.Empty;
            string h_job_type_cd = string.Empty;
            string h_msg_ind = string.Empty;
            string h_msg_text = string.Empty;
            string h_release_fund_amt = string.Empty;
            string h_release_fund_ind = string.Empty;
            string h_warranty_ind = string.Empty;
            string h_tries = string.Empty;
            string h_user_id_no = string.Empty;
            string h_user_session_no = string.Empty;
            string strEmailSubject = string.Empty;
            string strEmailFrom = string.Empty;
            string strEmailBody = string.Empty;
            string strReviseInvoice = string.Empty;
            string strIngresErrorCode = string.Empty;
            string lngIngresErrorNumber = string.Empty;
            string strIngresMessageText = string.Empty;
            string strSrc = string.Empty;
            string strDesc = string.Empty;
            string diPath = string.Empty;

            long lngNum = 0;
            int strReleaseFunds = 0;
            int intReturnVal = 0;

            bool blnIngresTransaction = false;
            bool blnAPDTransaction = false;
            bool blnIngresErrorElectronicBilling = false;

            AppSettingsReader appReader = null;
            try
            {
                appReader = new AppSettingsReader();

                string PROC_NAME = string.Concat(MODULE_NAME, "ReleasePayment: ");
                m_blnReleaseFundsMode = true;
                mobjMLAPDAccounting.InitializeGlobals();

                if (mobjMLAPDAccounting.g_blnDebugMode)
                    mobjMLAPDAccounting.g_objEvents.Trace(string.Concat("  strXML = ", strXML, "Check passed parameters."));

                mobjMLAPDAccounting.g_objEvents.Assert(strXML != null, "Nothing passed.");

                objReleasePaymentXML = new DOMDocument40();
                mobjMDomUtils.LoadXml(ref objReleasePaymentXML, strXML, string.Empty, string.Empty);

                strInsuranceCompanyName = objReleasePaymentXML.documentElement.getAttribute("insuranceCompanyName").ToString();
                strLynxID = objReleasePaymentXML.documentElement.getAttribute("lynxID").ToString();
                strClaimAspectNumber = objReleasePaymentXML.documentElement.getAttribute("claimAspectNumber").ToString();
                strDispatchNumber = objReleasePaymentXML.documentElement.getAttribute("dispatchNumber").ToString();
                strOriginalDispatchNumber = objReleasePaymentXML.documentElement.getAttribute("originalDispatchNumber").ToString();
                strInvoiceDate = objReleasePaymentXML.documentElement.getAttribute("invoiceDate").ToString();
                strInvoiceSubmitDate = objReleasePaymentXML.documentElement.getAttribute("invoiceSubmitDate").ToString();
                strInvoiceAmount = objReleasePaymentXML.documentElement.getAttribute("invoiceAmount").ToString();
                strPayeeID = objReleasePaymentXML.documentElement.getAttribute("payeeID").ToString();
                strPayeeName = objReleasePaymentXML.documentElement.getAttribute("payeeName").ToString();
                strPayeePhone = objReleasePaymentXML.documentElement.getAttribute("payeePhone").ToString();
                strPayeeWarrantyFlag = objReleasePaymentXML.documentElement.getAttribute("payeeWarrantyFlag").ToString();
                strDescription = objReleasePaymentXML.documentElement.getAttribute("description").ToString();
                strHistoryComments = objReleasePaymentXML.documentElement.getAttribute("historyComment").ToString();
                strReleaseAmount = objReleasePaymentXML.documentElement.getAttribute("releaseAmt").ToString();
                strDeductibleAmount = objReleasePaymentXML.documentElement.getAttribute("deductibleAmt").ToString();
                strTaxTotalAmount = objReleasePaymentXML.documentElement.getAttribute("taxTotal").ToString();
                strWarrantyFlag = objReleasePaymentXML.documentElement.getAttribute("warrantyFlag").ToString();
                strOldInvoiceIDFee = objReleasePaymentXML.documentElement.getAttribute("oldInvoiceIDFee").ToString();
                strNewFeeID = objReleasePaymentXML.documentElement.getAttribute("newFeeID").ToString();
                strNewFeeServices = objReleasePaymentXML.documentElement.getAttribute("newFeeServices").ToString();
                strDispositionCD = objReleasePaymentXML.documentElement.getAttribute("dispositionCD").ToString();
                strUserID = objReleasePaymentXML.documentElement.getAttribute("userID").ToString();

                strWarrantyFlag = (Convert.ToBoolean(strWarrantyFlag = "1")) ? "Y" : "N";
                if (strOriginalDispatchNumber == string.Empty)
                {
                    strOriginalDispatchNumber = strDispatchNumber;
                    strUpdateDispatchProc = mobjMLAPDAccounting.GetConfig("Accounting/Payment/UpdateReleaseFundsDispatchSP");
                }

                //Check passed parameters.
                mobjMLAPDAccounting.g_objEvents.Assert(strLynxID != null, "No LYNX ID.");
                mobjMLAPDAccounting.g_objEvents.Assert(strDispatchNumber != null, "No Dispatch Number.");
                mobjMLAPDAccounting.g_objEvents.Assert(strPayeeID != null, "No Payee ID.");
                mobjMLAPDAccounting.g_objEvents.Assert(strReleaseAmount != null, "No Release Funds Amount.");

                //g_objEvents.Assert (strPayeeWarrantyFlag = "1") And (strDispositionCD <> "TL" And strDispositionCD <> "CO"), "Cannot release funds for warranty payee for a TL/CO vehicle."
                //g_objEvents.Assert (CCur(strReleaseAmount) > 0) And (strDispositionCD <> "TL" And strDispositionCD <> "CO"), "Release funds cannot be greater than zero for a TL/CO vehicle."

                if (strDispositionCD == "TL" || strDispositionCD == "CO" || strDispositionCD == "ST")
                {
                    if (strPayeeWarrantyFlag == "1")
                        throw new Exception(string.Concat(new string[] { Convert.ToString(Microsoft.VisualBasic.Constants.vbObjectError), Convert.ToString(101), ",", PROC_NAME, ",", "Cannot release funds for warranty payee for a TL/CO vehicle." }));

                    if (Convert.ToDecimal(strReleaseAmount) > 0)
                        throw new Exception(string.Concat(new string[] { Convert.ToString(Microsoft.VisualBasic.Constants.vbObjectError), Convert.ToString(101), ",", PROC_NAME, ",", "Release funds cannot be greater than zero for a TL/CO vehicle." }));
                }

                //Are the Ingres DB calls enabled?
                blnIngresEnabled = Convert.ToBoolean(mobjMLAPDAccounting.GetConfig("Accounting/IngresEnabled=true")) ? true : false;

                if (blnIngresEnabled)
                {
                    if (strPayeeWarrantyFlag == "0")
                    {
                        if (strDispositionCD == "TL" || strDispositionCD == "CO" || strDispositionCD == "ST")
                        {
                            //need to revise the invoice
                            if (ReviseInvoice(string.Concat(new string[]{"<Root claimAspectID='' dispatchNumber='" , strDispatchNumber ,
                                             "' revisedAmount='" , strReleaseAmount,
                                             "' oldInvoiceIDFee='" , strOldInvoiceIDFee,
                                             "' newFeeID='" , strNewFeeID,
                                             "' newFeeServices='" , strNewFeeServices,
                                             "' userID='" , strUserID , "'/>"})) != 0)
                                throw new Exception(string.Concat(new string[] { Convert.ToString(Microsoft.VisualBasic.Constants.vbObjectError), Convert.ToString(101), ",", PROC_NAME, ",", "Error revising the invoice." }));

                            //read the new invoice line items
                            objRevisedInvoiceXML = new DOMDocument40();
                            mobjMDomUtils.LoadXml(ref objRevisedInvoiceXML, m_strRevisedInvoiceXML, string.Empty, string.Empty);

                            objRevisedInvoiceFeeItem = (IXMLDOMElement)objRevisedInvoiceXML.selectSingleNode(string.Concat(new string[] { "//Invoice[@DispatchNumber='", strDispatchNumber, "' and @ItemType='Fee']" }));
                            if (objRevisedInvoiceFeeItem != null)
                                strFeeAmount = Convert.ToString(objRevisedInvoiceFeeItem.getAttribute("Amount"));
                        }

                        strImageRoot = mobjMLAPDAccounting.GetConfig("Document/RootDirectory");
                        if (Left(strImageRoot, 4) == "\\\\")
                            strImageRoot = Regex.Replace(strImageRoot, @"\\", @"\", RegexOptions.IgnoreCase);

                        //check if the original transaction xml
                        strFileName = Right(strLynxID, 4);
                        strFileName = string.Concat(new string[]{Mid(strFileName, 1, 1) , @"\",
                                      Mid(strFileName, 2, 1) , @"\",
                                      Mid(strFileName, 3, 1) , @"\",
                                      Mid(strFileName, 4, 1) , @"\",
                                      "IngresParams" , strOriginalDispatchNumber , ".xml"});
                        if (Right(strImageRoot, 1) != @"\")
                            strFileName = string.Concat(strImageRoot, @"\", strFileName);
                        else
                            strFileName = string.Concat(strImageRoot, strFileName);

                        if (new FileInfo(strFileName).Length > 0)
                        {
                            objOriginalTxXML = new DOMDocument40();
                            mobjMDomUtils.LoadXmlFile(ref objOriginalTxXML, strFileName, string.Empty, string.Empty);

                            objRoot = objOriginalTxXML.documentElement;

                            //load the values from the original transaction
                            h_apd_lynx_id = objRoot.getAttribute("h_apd_lynx_id").ToString();
                            h_apd_shop_id = objRoot.getAttribute("h_apd_shop_id").ToString();
                            h_claim_amt = objRoot.getAttribute("h_claim_amt").ToString();
                            h_claim_no = objRoot.getAttribute("h_claim_no").ToString();
                            h_comment_text = strDescription; //objRoot.getAttribute("h_comment_text")
                            h_commit_ind = objRoot.getAttribute("h_commit_ind").ToString();
                            h_dispatch_no = strDispatchNumber;
                            h_fee_amt = objRoot.getAttribute("h_fee_amt").ToString();
                            h_rtlr_fee_amt = objRoot.getAttribute("h_rtlr_fee_amt").ToString();
                            h_insrd_first_nm = objRoot.getAttribute("h_insrd_first_nm").ToString();
                            h_insrd_last_nm = objRoot.getAttribute("h_insrd_last_nm").ToString();
                            h_ins_pc_uid_no = objRoot.getAttribute("h_ins_pc_uid_no").ToString();
                            h_job_type_cd = objRoot.getAttribute("h_job_type_cd").ToString();
                            h_msg_ind = objRoot.getAttribute("h_msg_ind").ToString();
                            h_msg_text = objRoot.getAttribute("h_msg_text").ToString();
                            h_release_fund_amt = strReleaseAmount;
                            h_release_fund_ind = "Y";
                            h_warranty_ind = strWarrantyFlag;
                            h_tries = objRoot.getAttribute("h_tries").ToString();
                            h_user_id_no = objRoot.getAttribute("h_user_id_no").ToString();
                            h_user_session_no = objRoot.getAttribute("h_user_session_no").ToString();

                            if (strDispositionCD == "TL" || strDispositionCD == "CO" || strDispositionCD == "ST")
                                mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(new string[]{ " Ingres parameter overrides for TL/CO: OLD Values: ",
                                                        "h_rtlr_fee_amt=" , h_rtlr_fee_amt , " h_fee_amt=" , h_fee_amt,
                                                        "  NEW Values: h_rtlr_fee_amt=0 h_fee_amt=" , strFeeAmount}));
                            {
                                h_rtlr_fee_amt = Convert.ToString(0);
                                h_fee_amt = Convert.ToBoolean(Convert.ToString(Convert.ToInt32(strFeeAmount))) ? strFeeAmount : "0";
                            }

                            mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(new string[]{ " Ingres parameters: retVal=" , Convert.ToString(ParameterDirectionEnum.adParamReturnValue),
                                                                " h_apd_lynx_id=" , h_apd_lynx_id , " h_apd_shop_id=" ,h_apd_shop_id,
                                                                " h_claim_amt=" , h_claim_amt , " h_claim_no=" , h_claim_no,
                                                                " h_comment_text=" , h_comment_text , " h_commit_ind=1  h_dispatch_no=" , h_dispatch_no,
                                                                " h_fee_amt=" , h_fee_amt , " h_rtlr_fee_amt=" , h_rtlr_fee_amt , " h_insrd_first_nm=" , h_insrd_first_nm,
                                                                " h_insrd_last_nm=" , h_insrd_last_nm , " h_ins_pc_uid_no=" , h_ins_pc_uid_no ,
                                                                " h_job_type_cd=15 h_msg_ind=  h_msg_text=" , h_msg_text,
                                                                " h_tries=1  h_user_id_no=" , h_user_id_no , " h_user_session_no=",
                                                                " h_release_fund_amt=" , h_release_fund_amt , " h_release_fund_ind=",h_release_fund_ind,
                                                                " h_warranty_ind=" , h_warranty_ind}));


                            // Create and initialize connection to Ingres
                            objIngresAccessor = new DataAccessor.CDataAccessor();

                            diPath = (string)appReader.GetValue("LAPDPartnerDataMgr", typeof(string));

                            objIngresAccessor.mobjEvents.ComponentInstance = "Accounting";
                            objIngresAccessor.InitEvents(diPath, "Debug", false);
                            objIngresAccessor.SetConnectString(mobjMLAPDAccounting.g_strIngresConnect);

                            mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_Recordset);

                            blnIngresTransaction = true;
                            objIngresAccessor.BeginTransaction();

                            blnAPDTransaction = true;
                            objIngresAccessor.BeginTransaction();

                            strIngresProc = mobjMLAPDAccounting.GetConfig("Accounting/IngressBillingSP");

                            //Send each InvoiceItem to Ingres and retreive the returned Dispatch number.
                            objIngresAccessor.FpCreateCommand(strIngresProc, 0);
                            objIngresAccessor.FpAddParam("retval", ADODB.DataTypeEnum.adInteger, ParameterDirectionEnum.adParamReturnValue, 4, 0);
                            objIngresAccessor.FpAddParam("h_apd_lynx_id", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 25, h_apd_lynx_id);
                            objIngresAccessor.FpAddParam("h_apd_shop_id", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, h_apd_shop_id);
                            objIngresAccessor.FpAddParam("h_claim_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, h_claim_amt);
                            objIngresAccessor.FpAddParam("h_claim_no", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, h_claim_no);
                            objIngresAccessor.FpAddParam("h_comment_text", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 300, h_comment_text);
                            //objIngresAccessor'.FpAddParam "h_comment_ind", adChar, adParamInput, 1, h_comment_ind
                            objIngresAccessor.FpAddParam("h_commit_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, h_commit_ind);
                            objIngresAccessor.FpAddParam("h_dispatch_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInputOutput, 4, Convert.ToInt64(strDispatchNumber));
                            objIngresAccessor.FpAddParam("h_fee_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, h_fee_amt);
                            objIngresAccessor.FpAddParam("h_rtlr_fee_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, h_rtlr_fee_amt);
                            objIngresAccessor.FpAddParam("h_insrd_first_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, h_insrd_first_nm);
                            objIngresAccessor.FpAddParam("h_insrd_last_nm", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 20, h_insrd_last_nm);
                            objIngresAccessor.FpAddParam("h_ins_pc_uid_no", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, h_ins_pc_uid_no);
                            objIngresAccessor.FpAddParam("h_job_type_cd", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 1, h_job_type_cd);
                            objIngresAccessor.FpAddParam("h_msg_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInputOutput, 1, h_msg_ind);

                            // When strReturnValue is -1 the first 4 chars of h_msg_text represent the Ingres error code; resolve in message store.
                            objIngresAccessor.FpAddParam("h_msg_text", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInputOutput, 200, h_msg_text);

                            //Early Billing
                            objIngresAccessor.FpAddParam("h_release_fund_amt", DataTypeEnum.adDouble, ParameterDirectionEnum.adParamInput, -1, Convert.ToDecimal(strReleaseAmount));
                            objIngresAccessor.FpAddParam("h_release_fund_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, "Y");
                            objIngresAccessor.FpAddParam("h_warranty_ind", DataTypeEnum.adChar, ParameterDirectionEnum.adParamInput, 1, strWarrantyFlag);

                            objIngresAccessor.FpAddParam("h_tries", DataTypeEnum.adInteger, ParameterDirectionEnum.adParamInput, 4, h_tries);

                            // Pass 'lynxdba' for system batch jobs.
                            objIngresAccessor.FpAddParam("h_user_id_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, h_user_id_no);
                            objIngresAccessor.FpAddParam("h_user_session_no", DataTypeEnum.adVarChar, ParameterDirectionEnum.adParamInput, 32, h_user_session_no);

                            // strReturnValue:  0 for success, positive for dbms error, negative for logical error (see h_msh_text).
                            strReturnValue = Convert.ToString(objIngresAccessor.FpExecute());
                            strNewDispatchNumber = Convert.ToString(objIngresAccessor.FpGetParameter("h_dispatch_no"));
                            strMessageText = Convert.ToString(objIngresAccessor.FpGetParameter("h_msg_text"));
                            strMessageInd = Convert.ToString(objIngresAccessor.FpGetParameter("h_msg_ind"));

                            objIngresAccessor.FpCleanUp();


                            mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(new string[]{PROC_NAME , "Ingres return:  strReturnValue=" , strReturnValue , " h_dispatch_no=" ,strDispatchNumber,
                                                                  " h_msg_text=" , strMessageText , " h_msg_ind=" , strMessageInd}));


                            //Verify what we got back from Ingres.
                            if (strReturnValue == "-1")
                            {
                                // Extract the code and text portions of the Ingres error message.
                                strIngresErrorCode = Left(strMessageText, 4);
                                strIngresMessageText = Right(strMessageText, strMessageText.Length - 4);

                                //Ensure a valid Ingres error code was retrieved
                                if (strIngresErrorCode == "true")
                                {
                                    // Convert the Ingres error code to an APD error code that can be resolved in the message store (error.xml).
                                    lngIngresErrorNumber = Convert.ToString(Convert.ToInt64(strIngresErrorCode));
                                    if (Convert.ToInt32(lngIngresErrorNumber) != 0)
                                        lngIngresErrorNumber = MLAPDAccounting.LAPDAccounting_FirstError + lngIngresErrorNumber;

                                    blnIngresErrorElectronicBilling = true;

                                    mobjMLAPDAccounting.g_objEvents.Env(string.Concat(new string[]{"INGRES: Return Value = '" , strReturnValue,"' Message Text = '" , strMessageText,
                                        "' Message Indicator = '" , strMessageInd, "'"}));


                                    mobjMLAPDAccounting.g_objEvents.HandleEvent(Convert.ToInt32(lngIngresErrorNumber), "Ingres Billing SP", string.Concat(new string[]{ strIngresErrorCode , "|" , strIngresMessageText,
                                        "' LynxID='" , strLynxID,
                                        "' Dispatch='" , strDispatchNumber , "' ",Convert.ToString(true)}));
                                }
                                else
                                {
                                    // For electronic billing handle the error and continue.
                                    blnIngresErrorElectronicBilling = true;

                                    mobjMLAPDAccounting.g_objEvents.Env(string.Concat(new string[]{"INGRES: Return Value = '" , strReturnValue,
                                      "' Message Text = '" , strMessageText,
                                      "' Message Indicator = '" , strMessageInd , "'"}));


                                    mobjMLAPDAccounting.g_objEvents.HandleEvent(Convert.ToInt32(lngIngresErrorNumber), "Ingres Billing SP", string.Concat(new string[]{ strIngresErrorCode , "|" , strIngresMessageText,"' LynxID='", strLynxID,
                                        "' Dispatch='" , strDispatchNumber , "' ", Convert.ToString(true)}));

                                }
                            }
                            else if (strReturnValue != "0")
                            {
                                // For electronic billing handle the error and continue.
                                blnIngresErrorElectronicBilling = true;

                                mobjMLAPDAccounting.g_objEvents.Env(string.Concat(new string[]{"INGRES: Return Value = '" , strReturnValue,
                                     "' Message Text = '" , strMessageText,
                                     "' Message Indicator = '" , strMessageInd , "'"}));



                                mobjMLAPDAccounting.g_objEvents.HandleEvent(Convert.ToInt32(lngIngresErrorNumber), "Ingres Billing SP", string.Concat(new string[]{ strIngresErrorCode , "|" , strIngresMessageText,
                                    "' LynxID='" , strLynxID ,
                                    "' Dispatch='" , strDispatchNumber , "' ",Convert.ToString(true)}));
                            }

                            // Ensure Dispatch number has a value.
                            if ((blnIngresErrorElectronicBilling == false) && (strNewDispatchNumber.Length > 0))
                            {
                                //Update the corresponding Invoice records in the APD database with the newly retrieved DispatchNumber.
                                objIngresAccessor.ExecuteSp(strUpdateDispatchProc, strDispatchNumber, strNewDispatchNumber, strReleaseAmount, string.Concat(new string[] { "'", Regex.Replace(strHistoryComments, "'", "", RegexOptions.IgnoreCase), "'" }), strUserID);

                                //g_objApdRsAccessor.ExecuteSpNp
                            }
                        }
                    }
                    else
                    {
                        //strPayeeWarrantyFlag = "0"
                        //get the email destinations for the manual request
                        strManualFormRequestTo = mobjMLAPDAccounting.GetConfig("Accounting/Payment/ManualPayment/@ToEmail");

                        if (strManualFormRequestTo != string.Empty)
                        {
                            strEmailFrom = mobjMLAPDAccounting.GetConfig("Accounting/ElectronicInvoice/Notification/@From");
                            strEmailSubject = string.Concat(new string[] { "APD Manual Check Processing Request for LYNX ID: ", strLynxID, "-", strClaimAspectNumber });
                            strEmailBody = string.Concat(new string[]{ "             APD MANUAL CHECK PROCESSING REQUEST             " , Environment.NewLine,
                                            "             ===================================             " , Environment.NewLine , Environment.NewLine,
                                            "                Date: " , DateTime.Now.ToString("mm/dd/yyyy") , Environment.NewLine,
                                            "           Installer: " , strPayeeName , Environment.NewLine,
                                            "UID Number (LYNX ID): " , strLynxID , "-" , strClaimAspectNumber , Environment.NewLine,
                                            "    Telephone Number: " , strPayeePhone , Environment.NewLine,
                                            "     Dispatch Number: " , strDispatchNumber , Environment.NewLine,
                                            "           Insurance: " , strInsuranceCompanyName , Environment.NewLine ,
                                            "      Invoice Number: " , strLynxID ,"-" , strClaimAspectNumber , Environment.NewLine,
                                            "        Created Date: " , strInvoiceDate , Environment.NewLine,
                                            "      Statement Date: " , strInvoiceSubmitDate , Environment.NewLine,
                                            "       Resubmit Date: " , Environment.NewLine,
                                            "      Invoice Amount: " , strInvoiceAmount , Environment.NewLine,
                                            "Release Funds Amount: " , strReleaseAmount , Environment.NewLine,
                                            "            Comments: " , strDescription});

                            //generate the custom form request
                            mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(new string[]{ PROC_NAME , "Manual Payment Request:",
                                                                " From: " , strEmailFrom,
                                                                " To: " , strManualFormRequestTo , Environment.NewLine,
                                                                " Subject: " ,strEmailSubject , Environment.NewLine,
                                                                " Body: " , strEmailBody}));

                            mobjMLAPDAccounting.g_objEvents.SendEmail(strEmailFrom, strManualFormRequestTo, strEmailSubject, strEmailBody, false, 2);

                            // Update the corresponding Invoice records in the APD database with the newly retrieved DispatchNumber.
                            strNewDispatchNumber = strDispatchNumber;

                            //connect to the database
                            mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_Recordset);

                            //'start a transaction
                            //'blnAPDTransaction = True
                            //'g_objApdRsAccessor.BeginTransaction

                            //'record the release funds
                            objIngresAccessor.ExecuteSp(strUpdateDispatchProc, strDispatchNumber, strNewDispatchNumber, strReleaseAmount, string.Concat(new string[] { "'", Regex.Replace(strHistoryComments, "'", "", RegexOptions.IgnoreCase), "'" }), strUserID);

                        }
                        else //no manual form destination
                        {
                            mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "No Manual Payment Request destination."));

                            objReleasePaymentXML = null;
                        }
                    }
                    if (blnIngresTransaction)
                        objIngresAccessor.RollbackTransaction();
                    if (blnAPDTransaction)
                        objIngresAccessor.RollbackTransaction();


                    if (blnIngresTransaction)
                        objIngresAccessor.CommitTransaction();
                    blnIngresTransaction = false;

                    if (blnAPDTransaction)
                        objIngresAccessor.CommitTransaction();
                    blnAPDTransaction = false;

                    //Problems committing?
                    if (mobjMLAPDAccounting.g_blnDebugMode)
                        mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Transactions Committed Successfully."));

                    strReleaseFunds = 0;

                }


                mobjMLAPDAccounting.TerminateGlobals();

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objIngresAccessor = null;
                m_blnReleaseFundsMode = false;
            }
            return strReleaseFunds;
        }

        public int ReviseInvoice(string strReleaseXML)
        {
            DOMDocument40 objParamXML = null;
            DOMDocument40 objXML = null;
            XmlNodeList objInvoiceNodes = null;
            XmlElement objInvoiceNode = null;
            XmlNode passNodeClaimAspectID = null;
            XmlNode passRevisedAmount = null;
            XmlNode passUserID = null;
            XmlNode passDispatchNumber = null;
            XmlNode passOldInvoiceIDFee = null;
            XmlNode passNewFeeID = null;
            XmlNode passNewFeeServices = null;
            XmlNode sendLynxID = null;
            XmlNode sendDispatchNumber = null;
            XmlNode sendTotalAmount = null;
            XmlNode sendFeeAmount = null;
            XmlNode sendInvoiceNodes = null;
            DocumentMgr.CDocMgr objDocMgr = null;

            string strXML = string.Empty;
            string strParamXML = string.Empty;
            string strFeeAmount = string.Empty;
            string strNewFeeServices = string.Empty;
            string strNewFeeID = string.Empty;
            string strOldInvoiceIDFee = string.Empty;
            string strUserID = string.Empty;
            string strRevisedAmount = string.Empty;
            string strClaimAspectID = string.Empty;
            string strEmailBody = string.Empty;
            string strEmailSubject = string.Empty;
            string strEmailFrom = string.Empty;
            string strManualFormRequestTo = string.Empty;
            string strTotalAmount = string.Empty;
            string strDispatchNumber = string.Empty;
            string strLynxID = string.Empty;

            string strInvoiceNode = null;
            try
            {
                objIngresAccessor = new DataAccessor.CDataAccessor();
                string PROC_NAME = string.Concat(MODULE_NAME, "ReviseInvoice: ");

                if (m_blnReleaseFundsMode == false)
                    mobjMLAPDAccounting.InitializeGlobals();

                if (mobjMLAPDAccounting.g_blnDebugMode)
                    mobjMLAPDAccounting.g_objEvents.Trace(string.Concat("  strReleaseXML = ", strReleaseXML));

                //Check passed parameters.
                mobjMLAPDAccounting.g_objEvents.Assert(strReleaseXML != null, "Nothing passed.");

                objParamXML = new DOMDocument40();
                mobjMDomUtils.LoadXml(ref objParamXML, strReleaseXML, string.Empty, string.Empty);

                strClaimAspectID = objParamXML.documentElement.getAttribute("claimAspectID").ToString();
                strRevisedAmount = objParamXML.documentElement.getAttribute("revisedAmount").ToString();
                strUserID = objParamXML.documentElement.getAttribute("userID").ToString();
                strDispatchNumber = objParamXML.documentElement.getAttribute("dispatchNumber").ToString();
                strOldInvoiceIDFee = objParamXML.documentElement.getAttribute("oldInvoiceIDFee").ToString();
                strNewFeeID = objParamXML.documentElement.getAttribute("newFeeID").ToString();
                strNewFeeServices = objParamXML.documentElement.getAttribute("userID").ToString();


                passNodeClaimAspectID = (XmlNode)objParamXML.selectSingleNode("/Root/@claimAspectID");
                passRevisedAmount = (XmlNode)objParamXML.selectSingleNode("/Root/@revisedAmount");
                passUserID = (XmlNode)objParamXML.selectSingleNode("/Root/@userID");
                passDispatchNumber = (XmlNode)objParamXML.selectSingleNode("/Root/@dispatchNumber");
                passOldInvoiceIDFee = (XmlNode)objParamXML.selectSingleNode("/Root/@oldInvoiceIDFee");
                passNewFeeID = (XmlNode)objParamXML.selectSingleNode("/Root/@newFeeID");
                passNewFeeServices = (XmlNode)objParamXML.selectSingleNode("/Root/@newFeeServices");

                strClaimAspectID = mobjMDomUtils.GetChildNodeText(ref passNodeClaimAspectID, "/Root/@claimAspectID", false);
                strRevisedAmount = mobjMDomUtils.GetChildNodeText(ref passRevisedAmount, "/Root/@revisedAmount", false);
                strUserID = mobjMDomUtils.GetChildNodeText(ref passUserID, "/Root/@userID", false);
                strDispatchNumber = mobjMDomUtils.GetChildNodeText(ref passDispatchNumber, "/Root/@dispatchNumber", false);
                strOldInvoiceIDFee = mobjMDomUtils.GetChildNodeText(ref passOldInvoiceIDFee, "/Root/@oldInvoiceIDFee", false);
                strNewFeeID = mobjMDomUtils.GetChildNodeText(ref passNewFeeID, "/Root/@newFeeID", false);
                strNewFeeServices = mobjMDomUtils.GetChildNodeText(ref passNewFeeServices, "/Root/@newFeeServices", false);

                if (mobjMLAPDAccounting.g_blnDebugMode)
                    mobjMLAPDAccounting.g_objEvents.Trace(string.Concat(new string[]{"  strClaimAspectID = " , strClaimAspectID,
                 "  strRevisedAmount = " ,strRevisedAmount,
                 "  strUserID = " , strUserID,
                 "  strDispatchNumber = " , strDispatchNumber,
                 "  strOldInvoiceIDFee = " , strOldInvoiceIDFee,
                 "  strNewFeeID = " , strNewFeeID,
                 "  strNewFeeServices = " , strNewFeeServices}));

                //Check passed parameters.
                mobjMLAPDAccounting.g_objEvents.Assert(strClaimAspectID != string.Empty, "Invalid Claim Aspect ID.");
                //g_objEvents.Assert strDispatchNumber <> "", "Invalid Dispatch Number."
                mobjMLAPDAccounting.g_objEvents.Assert(strRevisedAmount != string.Empty, "Invalid Revised Amount.");
                mobjMLAPDAccounting.g_objEvents.Assert(strUserID != string.Empty, "Invalid User ID.");

                mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_Recordset);

                //revise the recent invoice amount
                objIngresAccessor.ExecuteSp(mobjMLAPDAccounting.GetConfig("Accounting/ReviseInvoiceSP"), strClaimAspectID, strDispatchNumber, strRevisedAmount, strOldInvoiceIDFee,
                                                strNewFeeID, string.Concat(new string[] { "'", strNewFeeServices }), "'", strUserID);

                mobjMLAPDAccounting.ConnectToDB(MLAPDAccounting.EDatabase.eAPD_XML);

                //create a revised invoice
                strXML = objIngresAccessor.OpenRecordsetAsClientSideXML(mobjMLAPDAccounting.GetConfig("Accounting/Billing/RevisedPickupBillingSP"), strClaimAspectID, strDispatchNumber, strUserID);

                if (strXML != string.Empty)
                {
                    m_strRevisedInvoiceXML = strXML;
                    objDocMgr = new CDocMgr();

                    // Build xml input required by the DocumentMgr.GenerateInvoiceDocument function.
                    strParamXML = "<RevisedInvoice ProtectDocument='true' DocumentType='Invoice' />";


                    if (mobjMLAPDAccounting.g_blnDebugMode)
                        mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(PROC_NAME, "Calling DocumentMgr to create revised invoice document."));

                    //Call DocumentMgr to create invoice document and attach that document to the claim.
                    objDocMgr.GenerateInvoiceDocument(strParamXML, strXML);

                    //Send Email notification
                    strEmailFrom = mobjMLAPDAccounting.GetConfig("Accounting/ElectronicInvoice/Notification/@From");
                    strManualFormRequestTo = mobjMLAPDAccounting.GetConfig("Accounting/Payment/ManualPayment/@ToEmail");

                    objXML = new DOMDocument40();
                    mobjMDomUtils.LoadXml(ref objXML, strXML, string.Empty, string.Empty);
                    sendLynxID = (XmlNode)objXML.selectSingleNode("/Root/Claim/@LynxID");
                    sendDispatchNumber = (XmlNode)objXML.selectSingleNode("/Root/Claim/Exposure/Invoice/@DispatchNumber");
                    sendTotalAmount = (XmlNode)objXML.selectSingleNode("/Root/@TotalInvoiceAmount");
                    sendFeeAmount = (XmlNode)objXML.selectSingleNode("//Invoice[@ItemType='Fee']/@Amount");

                    strLynxID = mobjMDomUtils.GetChildNodeText(ref sendLynxID, "/Root/Claim/@LynxID", true);
                    strDispatchNumber = mobjMDomUtils.GetChildNodeText(ref sendDispatchNumber, "/Root/Claim/Exposure/Invoice/@DispatchNumber", true);
                    strTotalAmount = mobjMDomUtils.GetChildNodeText(ref sendTotalAmount, "/Root/@TotalInvoiceAmount", true);

                    if (strNewFeeID != string.Empty)
                        strFeeAmount = mobjMDomUtils.GetChildNodeText(ref sendFeeAmount, "//Invoice[@ItemType='Fee']/@Amount", false);
                    else
                        strFeeAmount = "NO CHANGE";


                    if (strManualFormRequestTo != string.Empty)
                    {
                        strEmailFrom = mobjMLAPDAccounting.GetConfig("Accounting/ElectronicInvoice/Notification/@From");
                        strEmailSubject = string.Concat(new string[] { "APD Revised Invoice for LYNX ID: ", strLynxID });
                        strEmailBody = string.Concat(new string[]{"             APD REVISED INVOICE CREATED            " , Environment.NewLine,
                                        "             ===========================             " , Environment.NewLine , Environment.NewLine,
                                        "             LYNX ID: " , strLynxID , Environment.NewLine ,
                                        "     Dispatch Number: " , strDispatchNumber , Environment.NewLine,
                                        "      Revised Amount: " , strTotalAmount , Environment.NewLine,
                                        "  Revised Fee Amount: " , strFeeAmount , Environment.NewLine , Environment.NewLine , Environment.NewLine ,
                                        "     AMOUNT DESCRIPTION" , Environment.NewLine,
                                        "----------- -----------------------------------------------------------" , Environment.NewLine});
                        sendInvoiceNodes = (XmlNode)objXML.selectSingleNode("//Invoice");

                        objInvoiceNodes = mobjMDomUtils.GetChildNodeList(ref sendInvoiceNodes, "//Invoice", true);

                        foreach (IXMLDOMNode tempobjInvoiceNode in objInvoiceNodes)
                        {
                            objInvoiceNode = (XmlElement)tempobjInvoiceNode;

                            strInvoiceNode = Convert.ToString(objInvoiceNode.GetAttribute("Amount"));
                            strEmailBody = string.Concat(strEmailBody, String.Format(String.Format(strInvoiceNode, "$#####0.00"), "@@@@@@@@@@@ "));

                            if (objInvoiceNode.GetAttribute("PayeeName") != string.Empty)
                                strEmailBody = string.Concat(new string[] { strEmailBody, Convert.ToString(objInvoiceNode.GetAttribute("PayeeName")), Environment.NewLine });
                            else
                                strEmailBody = string.Concat(new string[] { strEmailBody, Convert.ToString(objInvoiceNode.GetAttribute("InvoiceDescription")), Environment.NewLine });
                        }

                        strEmailBody = string.Concat(new string[]{ strEmailBody,
                                        "===========" , Environment.NewLine,
                                       String.Format(String.Format(strTotalAmount, "$#####0.00"), "@@@@@@@@@@@ ") , "TOTAL" , Environment.NewLine,
                                        "===========" , Environment.NewLine});


                        //generate the custom form request
                        mobjMLAPDAccounting.g_objEvents.Trace("", string.Concat(new string[]{ PROC_NAME , "Revised Invoice Notification:",
                                                            " From: " , strEmailFrom ,
                                                            " To: " , strManualFormRequestTo , Environment.NewLine ,
                                                            " Subject: " , strEmailSubject , Environment.NewLine,
                                                            " Body: " , strEmailBody}));

                        mobjMLAPDAccounting.g_objEvents.SendEmail(strEmailFrom, strManualFormRequestTo, strEmailSubject, strEmailBody, false, 2);
                    }
                }
                if (m_blnReleaseFundsMode == false)
                    mobjMLAPDAccounting.TerminateGlobals();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objDocMgr = null;
                objXML = null;
                objInvoiceNodes = null;
                objInvoiceNode = null;
            }
            return 0;
        }
    }
}
