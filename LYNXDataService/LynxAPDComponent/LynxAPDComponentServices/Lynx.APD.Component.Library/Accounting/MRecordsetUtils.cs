﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ADODB;

namespace Lynx.APD.Component.Library.Accounting
{
    /// <summary>
    ///  Module MRecordsetUtils
    /// 
    ///  Utility methods for dealing with recordsets.
    /// </summary>
    class MRecordsetUtils
    {
        #region Declarations
        private const string APP_NAME = "LAPDAccounting.";
        private string MODULE_NAME = string.Concat(APP_NAME, "MDomUtils.");
        MLAPDAccounting objMLAPDAccounting = null;
        #endregion

        #region Private Helper Functions

        /// <summary>
        /// Spits recordset contents out to the debug log.
        /// </summary>
        /// <param name="rsDebuglog"></param>
        private void DebugRS(ref Recordset rsDebuglog)
        {
            string strAppend = string.Empty;
            string strValue = string.Empty;
            try
            {
                rsDebuglog.MoveFirst();

                strAppend = string.Concat("Record Count = ", rsDebuglog.RecordCount, Environment.NewLine);
                strAppend = string.Concat(strAppend, "  Fields = ");

                objMLAPDAccounting = new MLAPDAccounting();

                foreach (Field fldCheck in rsDebuglog.Fields)
                    strAppend = string.Concat(strAppend, fldCheck.Name, ", ");

                strAppend = string.Concat(strAppend, Environment.NewLine);

                while (!rsDebuglog.EOF)
                {
                    strAppend = string.Concat(strAppend, "  Data: ");
                    foreach (Field fldCheckRecordset in rsDebuglog.Fields)
                    {
                        if ((fldCheckRecordset.Value) != null)
                        {
                            strValue = ((fldCheckRecordset.Value).ToString().Replace(Environment.NewLine, string.Empty)).Trim();
                            if (strValue.Length > 0)
                            {
                                if (strValue.Substring(0, 1) == "<" && (strValue.Substring(strValue.Length - 1, 1) == ">"))
                                    strAppend = string.Concat(strAppend, "(XML NOT LOGGED), ");
                                else
                                    strAppend = string.Concat(strAppend, "'" + fldCheckRecordset.Value + "', ");
                            }
                            else
                                strAppend = string.Concat(strAppend, "'' , ");
                        }
                        else
                            strAppend = string.Concat(strAppend, "NULL, ");

                    }
                    strAppend = string.Concat(strAppend, Environment.NewLine);

                    rsDebuglog.MoveNext();
                }

                objMLAPDAccounting.g_objEvents.Trace(strAppend, string.Concat(MODULE_NAME, "Recordset Contents"));
                rsDebuglog.MoveFirst();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
