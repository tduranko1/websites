﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Lynx.APD.Component.Library.Common;
using Microsoft.VisualBasic;
using Scripting;
namespace Lynx.APD.Component.Library.PartnerDataMgr
{

    public class CCCC
    {
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        string MODULE_NAME = string.Concat(new string[] {APP_NAME, "CCCC." });
        private CPartner mobjPartner = null;
        private MLAPDPartnerDataMgr mLAPDPartnerDataMgr = null;
        private MPPGTransactions mppgTransactions = null;
        private MBase64Utils mBase64utils = null;
        private MFlatfile mFlatfile = null;
        private MDomUtils mDomDomUtils = null;
        //private MLAPDEcadAccessorMgr mLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();

        /// <summary>
        /// Error codes private to this module.
        /// </summary>
        private enum CCC_Error_Codes : ulong
        {
            eFailedAddElement =MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CCCC_CLS_ErrorCodes
        }


        /// <summary>
        /// These are error codes that will come
        ///  back in negative acknowledgements from CCC.
        /// </summary>
        private enum CCC_NACK_ErrorCodes : ulong
        {
            Invalid_Data_Assignment = 00102,
            Invalid_External_Alias_1 = 00103,
            Invalid_External_Alias_2 = 00213,
            Processing_Exception_Occurred = 00200,
            Invalid_Data_Exception = 00202
        }

        /// <summary>
        /// This method transmits documents to the partner.
        /// </summary>
        /// <returns></returns>
        public string CPartner_TransferToPartner()
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), "TransferToPartner()", "Method not implemented" }));
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        /// <summary>
        /// This method initializes processing of the documents.
        /// Returns True if processing should continue.
        /// </summary>
        /// <returns></returns>
        public bool CPartner_InitProcessDocument()
        {
            mobjPartner = new CPartner();
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            bool isInitProcessDocument;
            try
            {
                //'Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = mLAPDPartnerDataMgr.g_objPassedDocument.DocumentElement.Name;

                //'Extract the LYNXID and Vehicle number for reference by any procedure
                ExtractLynxIDAndVehNum(mobjPartner.DocTypeEnum);
                isInitProcessDocument = mobjPartner.CanProcessDocument();
            }
            catch (Exception exception)
            {
                throw exception;
            }

            //'Are we configured to process this document at this time?
            return isInitProcessDocument;
        }

        /// <summary>
        /// This method processing documents to the partner database.
        /// </summary>
        /// <returns></returns>
        public bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            mobjPartner = new CPartner();
            string PROC_NAME = string.Concat(new string[] {MODULE_NAME, "PartnerDatabaseTransfer: "});
            bool isPartnerDatabaseTransfer=true;
            XmlElement objElem = null;
            try
            {
               // objElem = new XmlElement();

                //'Backwards compatibility and cover up hack for Business Events:
                //'If the CCC "Super Secret Echo Field" does not come back then we
                //'hit the APD DB to get the AssignmentID back.  Then the LynxID,
                //'VehNum and Suffix are used to create the echo field here.
                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt();
                        break;
                    case CPartner.EDocumentType.ePrintImage:
                        ExtractSequenceNumber();
                        mobjPartner.StorePrintImageInPartnerDB();
                        if (blnSendReceipt)
                            SendAReceipt();
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                        ExtractSequenceNumber();
                        mobjPartner.StoreEstimateDataInPartnerDB();
                        if (blnSendReceipt)
                            SendAReceipt();
                        break;
                    case CPartner.EDocumentType.eBusinessEvent:
                        mobjPartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt();
                        break;
                    default:
                        mobjPartner.StoreDocumentInPartnerDb();
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                objElem = null;
            }
            return isPartnerDatabaseTransfer;
        }


       /// <summary>
        /// Used for backwards compatibility problems with partner_db
       /// </summary>
        private void AddEchoField(string strElement, string strParent)
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mDomDomUtils = new MDomUtils();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "AddEchoField:" });
            mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Looking for ", strElement, " within ", strParent }), PROC_NAME);
            XmlElement objElement = null;
            XmlElement objParent = null;
            try
            {
               // objElement = new XmlElement();
               // objParent = new XmlElement();
                objElement = (XmlElement)mLAPDPartnerDataMgr.GetChildNode(ref mLAPDPartnerDataMgr.g_objPassedDocument, string.Concat(new string[] { "//", strElement }), false);

                if (objElement == null)
                {
                    objParent = (XmlElement)mLAPDPartnerDataMgr.GetChildNode(ref mLAPDPartnerDataMgr.g_objPassedDocument, string.Concat(new string[] { "//", strParent }), true);
                    objElement = mDomDomUtils.AddElement(ref objParent, strElement);
                    if (objElement == null)
                    {
                        throw new Exception(string.Concat(new string[] { CCC_Error_Codes.eFailedAddElement.ToString(), PROC_NAME, "Failed to add element ", "Method not implemented", strElement, " to ", strParent }));
                    }
                    objElement.InnerText = string.Concat(new string[] { mLAPDPartnerDataMgr.g_strLynxID, "-", mLAPDPartnerDataMgr.g_strVehicleNumber, "-", mLAPDPartnerDataMgr.g_strAssignmentID });
                }
                
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                objElement = null;
                objParent = null;
            }
        }

        /// <summary>
        /// This method processing documents to the APD database.
        /// </summary>
        /// <returns></returns>
        public string CPartner_ApdDatabaseTransfer()//Todo return value
        {
            mobjPartner = new CPartner();
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransfer:" });
            string strApdDatabaseTransfer = string.Empty;
            try
            {
                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        mobjPartner.ProcessReceipt("//ErrorCode", "//ErrorDescription");
                        break;
                    case CPartner.EDocumentType.eBusinessEvent:
                        //There are no specific handlers for business events required
                        //because we only receive one business event from CCC.
                        mobjPartner.SendWorkflowNotification("AssignmentDownloadedByShop", string.Concat(new string[] { "SHOP (CCC): Assignment for Vehicle ", (mLAPDPartnerDataMgr.g_strVehicleNumber), " ", (mLAPDPartnerDataMgr.GetChildNode(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//EventNotes", true).InnerText)}));
                        break;
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.ProcessBinaryFiles(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//DigitalImageFile", "//CreateDate", "//CreateTime", "JPG", mobjPartner.DocTypeString, CPartner.EDocumentType.eDigitalImage);
                        mobjPartner.SendWorkflowNotification("ElectronicDocumentReceived",  string.Concat(new string[] {"SHOP (" ,mLAPDPartnerDataMgr.g_strPartner,"): Digital images received for Vehicle ",mLAPDPartnerDataMgr.g_strVehicleNumber}));
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ProcessEstimate(ref mobjPartner);  
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return strApdDatabaseTransfer;
        }


        /// <summary>
        /// Purpose: Inserts an estimate print image into the APD document system.
        ///  Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        /// <returns></returns>
        public long CPartner_InsertPrintImageIntoAPD(string strXml, long lngPartnerTransID)
        {
            mobjPartner = new CPartner();
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mBase64utils = new MBase64Utils();
            mFlatfile = new MFlatfile();
            mDomDomUtils = new MDomUtils();
            string PROC_NAME = string.Concat(new string[] {MODULE_NAME, "CPartner_InsertPrintImageIntoAPD:" });
            MSXML2.IXMLDOMDocument2 objPrintImage = null;
            XmlDocument xmlDocumentTemp = null;
            XmlElement objImageFile = null;
            DateTime currentDate;
            string strImageType = string.Empty;
            string strPrintImage = string.Empty;
            string strFileName = string.Empty;
            long lngInsertPrintImageIntoAPD;
            long lngStart = 0;
            long lngEnd = 0;
            try
            {
                currentDate = DateTime.Now;
                lngStart = 0;
                lngEnd = 0;
                //objPrintImage = new XmlDocument();
                //objImageFile = new XmlElement();
                xmlDocumentTemp = new XmlDocument();

                mLAPDPartnerDataMgr.g_objEvents.Trace("Loading Print Image XML", PROC_NAME);
                objPrintImage.async = false;//Todo
               
                xmlDocumentTemp = (XmlDocument)objPrintImage;
                mDomDomUtils.LoadXml(ref xmlDocumentTemp, ref strXml, string.Empty, string.Empty);

                objImageFile = (XmlElement)(xmlDocumentTemp.SelectSingleNode("//ImageFile"));

                if ((objImageFile!= null))
                {
                    strPrintImage = objImageFile.InnerText;
                }
                else
                {
                    mLAPDPartnerDataMgr.g_objEvents.Trace("Cannot find the ImageFile node in the print image. Will use the old method (string comparison) now.", PROC_NAME);
                    //lngStart = string.Intern(1, strXml, "CDATA[", Constants.vbBinaryCompare) + 6;
                    //lngEnd =  string.Intern(1, strXml, "]]", Constants.vbBinaryCompare);
                    lngStart = strXml.IndexOf("CDATA[",1) + 6;
                    lngEnd = strXml.IndexOf("]]",1);

                    if (lngStart <= 0|| lngEnd <= 0 ||lngEnd <= lngStart)
                    {
                        throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eCDataTagNotFound.ToString(), "PrintImage()", "CDATA tag not found in print image." }));
                    }

                    strPrintImage = Mid(strXml, Convert.ToInt32(lngStart), Convert.ToInt32(lngEnd - lngStart));
                }
                objPrintImage = null;

                //'Get the document file name.
                strFileName = mobjPartner.BuildFinalFileName(1,DateTime.Now.ToString("yyyymmddhhmmss"), "PrintImage");
                mFlatfile.ConvertFlatfileToXML(CPartner.EDocumentType.ePrintImage.ToString(), strXml.Substring(Convert.ToInt32(lngStart),Convert.ToInt32((lngEnd - lngStart))), "PrintImage", "66", "Estimate", strFileName);
                strImageType = "XML";
                if (strPrintImage.IndexOf("Job Number",1,StringComparison.CurrentCultureIgnoreCase) > 0)
                {
                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(" Processing CCC XML based print image ", string.Concat(new string[] { PROC_NAME, "Started" }));
                    

                    //content is in the old ez-net format with plain text.
                    mFlatfile.ConvertFlatfileToXML(mLAPDPartnerDataMgr.g_objDataAccessor.ToString(), CPartner.EDocumentType.ePrintImage.ToString(), strPrintImage, "PrintImage", "Estimate", strFileName);

                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(" Processing CCC XML based print image ", string.Concat(new string[] { PROC_NAME, "Completed" }));
                    
                }
                else
                {
                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(" Processing CCC PDF based print image ", string.Concat(new string[]{PROC_NAME,"Started"}));
                    

                    //this could be from CCC One
                    object obj = null;
                    string strPath = null;
                    obj = mBase64utils.ReadBinaryDataFromDomElement(objImageFile);
                    strFileName = strFileName.Replace(".xml", ".pdf");
                    //strPath = Strings.Replace((GetConfig("Document/RootDirectory") + strFileName), "\\\\", "\\");
                    strPath = (mLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "Document/RootDirectory", strFileName })).Replace("\\\\", "\\"));
                    
                    mBase64utils.WriteBinaryDataToFile(strPath, (byte[])obj);

                    strImageType = "PDF";
                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(" Processing CCC PDF based print image ", string.Concat(new string[] { PROC_NAME, "Completed" }));
                    
                }
                lngInsertPrintImageIntoAPD = mobjPartner.InsertLAPDDocument(ref mLAPDPartnerDataMgr.g_objDataAccessor, CPartner.EDocumentType.ePrintImage, Convert.ToInt64(mLAPDPartnerDataMgr.g_strAssignmentID), Convert.ToInt64(mLAPDPartnerDataMgr.g_strLynxID), Convert.ToInt32(mLAPDPartnerDataMgr.g_strVehicleNumber), mobjPartner.EstimateSequenceNumber, mobjPartner.BuildPartnerTransID(), strFileName, strImageType, "", false);

            }
            catch (Exception exception)
            {
                throw exception;
            }

            //'Insert new document record into APD database
            return lngInsertPrintImageIntoAPD;
        }


        /// <summary>
        /// private void ExtractLynxIDAndVehNum()
        ///Extracts the LynxID and Vehicle Number from the Lynx specified reference ID.
        ///This routine is now modified to handle two versions of CCC transactions:
        ///those that include the echo field and those that don't.  This makes the logic
        ///rather messy, but it will save resubmission of docs by hand in PRD.
        /// </summary>
        /// <param name="enmDocType"></param>
        private void ExtractLynxIDAndVehNum(CPartner.EDocumentType enmDocType)
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            string PROC_NAME = string.Concat(new string[] {MODULE_NAME, "ExtractLynxIDAndVehNum:" });
            bool blnNeedAssignmentID;
            string strResults = string.Empty;
            string[] strValues = null;
            string strDate = string.Empty;
            string strTime = string.Empty;
            int intLynxIDStart;
           
            try
            {
                intLynxIDStart = 0;
                blnNeedAssignmentID = false;//' This is where the LynxID begins

                //New "Secret Echo Field" extraction.
                if (enmDocType == CPartner.EDocumentType.eReceipt)
                {
                    mLAPDPartnerDataMgr.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//PartnerUniqueId");
                }
                else if (enmDocType == CPartner.EDocumentType.eBusinessEvent)
                {
                    blnNeedAssignmentID = true;
                    //Flag that we dont have the assignment ID.
                    mLAPDPartnerDataMgr.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//CustomerRefId", true);
                    //Old version still had the colon.  New version does not.  See Test Track 2037.
                    if (mLAPDPartnerDataMgr.g_strActualLynxID.IndexOf(":", 1) > 0)
                    {
                        mLAPDPartnerDataMgr.g_strActualLynxID = Mid(mLAPDPartnerDataMgr.g_strActualLynxID, mLAPDPartnerDataMgr.g_strActualLynxID.IndexOf(":", 1) + 2);
                    }
                    else if (mLAPDPartnerDataMgr.g_strActualLynxID.IndexOf("ID", 1) > 0)
                    {
                        mLAPDPartnerDataMgr.g_strActualLynxID = Mid(mLAPDPartnerDataMgr.g_strActualLynxID, mLAPDPartnerDataMgr.g_strActualLynxID.IndexOf("ID", 1) + 3);
                    }
                }
                else if (enmDocType == CPartner.EDocumentType.eDigitalImage || enmDocType == CPartner.EDocumentType.ePrintImage)
                {
                    mLAPDPartnerDataMgr.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//InsuranceUniqueID");
                }
                else if (enmDocType == CPartner.EDocumentType.eEstimateData)
                {
                    mLAPDPartnerDataMgr.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//SecretField");
                }
                else
                {
                    throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), " ", "Unknown document type: '", enmDocType.ToString(), "'." }));
                }
                //'Backwards compatibility to old legacy documents
                if (mLAPDPartnerDataMgr.g_strActualLynxID.Length == 0)
                {
                    blnNeedAssignmentID = true;
                    //Flag that we dont have the assignment ID.
                    if (enmDocType == CPartner.EDocumentType.eReceipt)
                    {
                        mLAPDPartnerDataMgr.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//CustomerRefId", true);
                    }
                    else if (enmDocType == CPartner.EDocumentType.eDigitalImage || enmDocType == CPartner.EDocumentType.ePrintImage)
                    {
                        mLAPDPartnerDataMgr.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//ClaimNumber", true);
                    }
                    else if (enmDocType == CPartner.EDocumentType.eEstimateData)
                    {
                        mLAPDPartnerDataMgr.g_strActualLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//ReceiverTransactionId", true);
                        intLynxIDStart = mLAPDPartnerDataMgr.g_strActualLynxID.IndexOf("#");
                    }
                }

                //'Split up the values by the dashes
                strValues = mLAPDPartnerDataMgr.g_strActualLynxID.Split('-');
                mLAPDPartnerDataMgr.g_objEvents.Assert((strValues.Length - 1) > 0, string.Concat(new string[]{"Invalid Document Ref ID (", mLAPDPartnerDataMgr.g_strActualLynxID,")"}));

                //Extract the LynxID
                mLAPDPartnerDataMgr.g_strLynxID = Convert.ToString(mLAPDPartnerDataMgr.AsNumber(strValues[0]));

                //Extracts and zero fill the vehicle number
                mLAPDPartnerDataMgr.g_strVehicleNumber = mLAPDPartnerDataMgr.ZeroPad(mLAPDPartnerDataMgr.AsNumber(strValues[1]), 3);

                //'Extract the assignment id.
                //'Do we need to get it from the database using LynxID, VehNum and Suffix?
                if((strValues.Length - 1) < 2)
                {
                    string strEnd = string.Empty;
                    int intSuffixLen = 0;
                    strEnd = strValues[1];
                    intSuffixLen = (strEnd.Length) - (mLAPDPartnerDataMgr.g_strVehicleNumber.Length);

                    if (intSuffixLen >= 0)
                    {
                        strEnd = Right(strEnd, intSuffixLen);

                        //Take only the first character, and cover for estimates with their '#' character.
                        strEnd = ((Left(strEnd, 1) == "#") ? "" : Left(strEnd, 1));
                    }

                    mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strLYNXConnStringStd);

                    mLAPDPartnerDataMgr.g_strAssignmentID = Convert.ToString(mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/AssignmentDecodeSP"),new object[] { "LynxID", Convert.ToInt64(mLAPDPartnerDataMgr.g_strLynxID), "VehicleNumber", Convert.ToInt32(mLAPDPartnerDataMgr.g_strVehicleNumber), "AssignmentSuffix", strEnd}));
                }
                else
                {
                    mLAPDPartnerDataMgr.g_strAssignmentID = Convert.ToString(mLAPDPartnerDataMgr.AsNumber(strValues[2]));
                }

                //'Extract the assignment / cancel character.
                //'Do we need to get it from the database using LynxID, VehNum and Suffix?
                if ((strValues.Length - 1) > 2)
                {
                    mLAPDPartnerDataMgr.g_strActionCode = Left(strValues[3], 1);
                }
                if (mLAPDPartnerDataMgr.g_strActionCode != "C")
                {
                    mLAPDPartnerDataMgr.g_strActionCode = "N";
                }

                //Extract the transaction sent date/time.
                strDate = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//TransactionDate", true);
                strTime = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//TransactionTime", true);
                mLAPDPartnerDataMgr.g_varTransactionDate = string.Concat(new string[] { Left(strDate, 2), "/", Mid(strDate, 3, 2), "/", Right(strDate, 4), " ", Left(strTime, 2), ":", Right(strTime, 2) }); 
                
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }


        /// <summary>
        /// Extracts the Estimate Sequence Number from the Print Image or Estimate Data and stores it in intEstimateSequenceNumber.
        /// The function returns true if a value is found, false if not
        /// </summary>
        private void ExtractSequenceNumber()
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mobjPartner = new CPartner();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractSequenceNumber: " });
            string strNodeText = string.Empty;// Text value of node we're looking at
            bool blnExtractSequenceNumber;
            double dNum;
            bool isNum;
            XmlNodeList nodCurrent = null;// The nodes we're looking at
            try
            {

                //' Set default return value
                blnExtractSequenceNumber = false;

                //'CCC Print Image
                if (mobjPartner.DocTypeEnum == CPartner.EDocumentType.ePrintImage)
                {
                    mobjPartner.ExtractSequenceNumber("//EstimateIdentifier", string.Empty);
                    blnExtractSequenceNumber = true;
                }
                //Estimate Data
                else if (mobjPartner.DocTypeEnum == CPartner.EDocumentType.eEstimateData)
                {
                    nodCurrent = mLAPDPartnerDataMgr.g_objPassedDocument.GetElementsByTagName("TransactionType");
                    strNodeText = nodCurrent.Item(0).NodeType.ToString();

                    // This is the value as it comes from the XML document
                    // In a ED, the estimate Supplement Identifier comes from a combination of two elements, one which designates
                    // E or S (for estimate and supplement), and a second which designates the number.  If the value of the first
                    // element is "E", just set the sequence number to 0.  Other, we'll need to get the sequence number from
                    // the second element.
                    if (strNodeText == "E")
                    {
                        mobjPartner.EstimateSequenceNumber = 0;
                        blnExtractSequenceNumber = true;
                    }

                    if (strNodeText == "S")
                    {
                        // This is a supplement, get the Supplement Sequence Nubmber
                        nodCurrent = mLAPDPartnerDataMgr.g_objPassedDocument.GetElementsByTagName("EstimateSupplementNmbr");
                        strNodeText = nodCurrent.Item(0).NodeType.ToString();

                        // This is the value as it comes from the XML document
                        isNum = double.TryParse(strNodeText, out dNum);

                        if (isNum)
                        {
                            mobjPartner.EstimateSequenceNumber = Convert.ToInt32(strNodeText);
                            blnExtractSequenceNumber = true;
                        }
                    }

                    //Unknown
                }

                else
                {
                    throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType.ToString(), " ", " Unknown document type : '", mobjPartner.DocTypeString, "'." }));
                }

                if (!blnExtractSequenceNumber)
                {
                    throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber.ToString(), PROC_NAME, "Unable to extract Estimate Sequence Number" }));
                }
            }

            catch (Exception exception)
            {
                throw exception;
            }
        }


        private void SendAReceipt()
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mobjPartner = new CPartner();
            mppgTransactions = new MPPGTransactions();
            mDomDomUtils = new MDomUtils();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendAReceipt: " });
            string strWrappedReceipt = string.Empty;
            string strPost2URL = string.Empty;
            string strTransType = string.Empty;
            XmlDocument objLYNXReceipt = null;
            try
            {
                objLYNXReceipt = new XmlDocument();

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Preparing receipt" }));


                //First load the receipt template from disk.
                objLYNXReceipt = new XmlDocument();
                string strPath = string.Concat(new string[] {mLAPDPartnerDataMgr.g_strSupportDocPath,"\\CCCReceipt.XML" });
                mDomDomUtils.LoadXmlFile(ref objLYNXReceipt, ref strPath, "SendAReceipt", "Receipt");

                //Fill out the template with values from the submitted document.
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//SenderTransactionId", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//SenderTransactionId", true);
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//ReceiverTransactionId", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//ReceiverTransactionId", true);
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//TransactionDate", true).InnerText = mLAPDPartnerDataMgr.ConstructXmlDate(DateTime.Now.ToString());
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//TransactionTime", true).InnerText = mLAPDPartnerDataMgr.ConstructXmlTime(DateTime.Now.ToString());
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//CustomerRefId", true).InnerText = string.Concat(new string[] {mLAPDPartnerDataMgr.g_strLynxID , "-",mLAPDPartnerDataMgr.g_strVehicleNumber});

                //The CCC transaction type is the root node name minus the 4 digit version number.
                strTransType = mLAPDPartnerDataMgr.g_objPassedDocument.DocumentElement.Name;
                strTransType = Left(strTransType, (strTransType.Length) - 4);
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//TransactionType", true).InnerText = strTransType;
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//ErrorCode", true).InnerText = "00000";
                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//ErrorDescription", true).InnerText = string.Concat(new string[] { mobjPartner.DocTypeString, " processed" });

                mLAPDPartnerDataMgr.GetChildNode(ref objLYNXReceipt, "//EventCreatedDateTime/DlDate/Date", true).InnerText = string.Concat(new string[] { mLAPDPartnerDataMgr.ConstructXmlDate(DateTime.Now.ToString()), " ", mLAPDPartnerDataMgr.ConstructXmlTime(DateTime.Now.ToString()) });
                
                //Build the SOAP Envelope and put the receipt into the envelope
                strWrappedReceipt = mppgTransactions.BuildTransactionForPPG("Receipt", objLYNXReceipt.OuterXml, mLAPDPartnerDataMgr.g_strPartner, string.Concat(new string[] { mLAPDPartnerDataMgr.g_strLynxID, "-", mLAPDPartnerDataMgr.g_strVehicleNumber }));


                //Determine the URL for posting depending on the environment
                strPost2URL = mLAPDPartnerDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mppgTransactions.XmlHttpPost(strPost2URL, strWrappedReceipt);

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace("", PROC_NAME);


            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                objLYNXReceipt = null;
            }
        }


        /// <summary>
        /// Send a HeartBeat request to Partner.
        /// </summary>
        public void CPartner_SendHeartBeatRequest()
        {
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), MODULE_NAME, "SendHeartBeatRequest()", "Method not implemented" }));

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        private bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CPartner_PartnerDatabaseTransferV2:" });
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));

            }
            catch (Exception exception)
            {
                throw exception;
            }

        }


        private string CPartner_ApdDatabaseTransferV2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CPartner_ApdDatabaseTransferV2:" });
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }


        private bool CPartner_InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CPartner_InitProcessDocumentV2:" });
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception exception)
            {
                throw exception;
            }
             
        }


        private long CPartner_InsertPrintImageIntoAPDV2(string strXml, long lngPartnerTransID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CPartner_InsertPrintImageIntoAPDV2:" });
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }


        public string Left(string param, int length)
        {
            try
            {
                string strLeft = param.Substring(0, length);
                return strLeft;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string Right(string param, int length)
        {
            try
            {
                string strRight = param.Substring(param.Length - length, length);
                return strRight;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string Mid(string param, int startIndex, int length)
        {
            try
            {
                string strMid = param.Substring(startIndex, length);
                return strMid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string Mid(string param, int startIndex)
        {
            try
            {
                string strMid = param.Substring(startIndex);
                return strMid;
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

    }
}
