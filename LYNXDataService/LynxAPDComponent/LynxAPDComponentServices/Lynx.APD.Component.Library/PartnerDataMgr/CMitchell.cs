﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    /// <summary>
    /// Component LAPDPartnerDataMgr : Class CMitchell
    /// Processing methods specific to Mitchell
    /// </summary>
    class CMitchell
    {
        #region Declarations
        public const string APP_NAME = "LAPDPartnerDataMgr.";
        private string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CMitchell." });
        #endregion

        #region Public Function

        /// <summary>
        /// This method initializes processing of the documents.
        /// Returns True if processing should continue.
        /// </summary>
        public bool CPartner_InitProcessDocument()
        {
            try
            {
                return false;
                throw new Exception(string.Concat(new string[] { MODULE_NAME, "InitProcessDocument()", "Method not implemented" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method processing documents to the partner database.
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            try
            {
                return false;
                throw new Exception(string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransfer()", "Method not implemented" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// This method processing documents to the APD database.
        /// </summary>
        /// <returns></returns>
        public string CPartner_ApdDatabaseTransfer()
        {
            try
            {
                throw new Exception(string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransfer()", "Method not implemented" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This method transmits documents to the partner.
        /// </summary>
        /// <returns></returns>
        public string CPartner_TransferToPartner()
        {
            try
            {
                throw new Exception(string.Concat(new string[] { MODULE_NAME, "TransferToPartner()", "Method not implemented" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public long CPartner_InsertPrintImageIntoAPD(string strXML, long lngPartnerTransID)
        {
            try
            {
                throw new Exception(string.Concat(new string[] { MODULE_NAME, "InsertPrintImageIntoAPD()", "Method not implemented" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CPartner_SendHeartBeatRequest()
        {
            try
            {
                throw new Exception(string.Concat(new string[] { MODULE_NAME, "SendHeartBeatRequest()", "Method not implemented" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Private Helper Functions

        private bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransferV2: " });

            try
            {
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string CPartner_ApdDatabaseTransferV2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransferV2: " });
            try
            {
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool CPartner_InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocumentV2: " });
            try
            {
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private long CPartner_InsertPrintImageIntoAPDV2(string strXML, long lngPartnerTransID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertPrintImageIntoAPDV2: " });
            try
            {
                throw new Exception(string.Concat(new string[] { PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
