﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using MS.Internal.Xml;
using System.Diagnostics;
using System.Threading;
using MSXML2;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    class MPPGTransactions
    {
        /// <summary>
        /// Module MPPGTransactions
        /// Code common to PPG transactions.
        /// </summary>

        #region Declarations
        public const string APP_NAME = "LAPDPartnerDataMgr.";
        public string MODULE_NAME = string.Concat(new string[] { APP_NAME, "MPPGTransactions." });
        MLAPDPartnerDataMgr mlapdPartnerDataMgr = null;
        #endregion

        #region Constructor

        public MPPGTransactions()
        {
          
        }
        #endregion

        #region Enumerator
        /// <summary>
        /// Internal error codes for this module.
        /// </summary>

        private enum EventCodes : ulong
        {
            eElectronicPostBadStatus = 0x80065000 + 0x300
        }
        #endregion

        #region Public Fuctions

        /// <summary>
        /// Syntax:      BuildTransactionForPPG
        /// Parameters:  strTransactionType - A token identifying the transaction type.
        ///              strMessageBody - The XML to put within the wrapper.
        ///              strDestination - A token indentfying the message destination.
        ///               strReferenceId - An internal identifyer for the message.
        ///               Wraps the passed XML with a PPG SOAP style wrapper.
        ///                The completed XML.
        ///                Settings in the config.xml file.
        ///              </summary>

        public string BuildTransactionForPPG(string strTransactionType, string strMessageBody, string strDestination, string strReferenceId)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "BuildTransactionForPPG:" });
            string strTrans = string.Empty,
                strEnvelopeId = string.Empty,
                strOriginationId = string.Empty,
                strDestinationId = string.Empty,
                strReturnTrans = string.Empty;
            bool blnUseSOAP, blnUseCDATA;
            try
            {
                mlapdPartnerDataMgr = new MLAPDPartnerDataMgr();
                //Get SOAP and CDATA settings from configuration.
                blnUseSOAP = Convert.ToBoolean((mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/UseSOAP") == "True"));   //TODO: Need to verify assigned function return value is correct?
                blnUseCDATA = (mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/UseCDATA") == "True");

                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    mlapdPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "TransactionType = '", strTransactionType, "'  Dest = '", strDestination, "'  RefId = '", strReferenceId, "'", PROC_NAME, "Started" }));

                //This configuration setting allows us to disable the SOAP envelope if need be.
                if (!blnUseSOAP)
                    return strMessageBody;

                else
                {
                    //Get source and dest PIDs from the config file.
                    strOriginationId = mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/OriginationPID");
                    strDestinationId = mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/DestinationPID[@name='" + strDestination + "']");
                    //Generate the envelope id.
                    strEnvelopeId = string.Concat(new string[] { strOriginationId, "#", strDestinationId, "#", strReferenceId, "#" });

                    //Start building the transaction XML.
                    strTrans = string.Concat(new string[] { "<StdEnvelope EnvelopeID='", strEnvelopeId, "' Version='1.0'>" });

                    //Add in the environment code.
                    strTrans = string.Concat(new string[] { strTrans, "<Header><Delivery><Environment>", mlapdPartnerDataMgr.GetConfig("@EnvironmentCode"), "</Environment>" });

                    //Add in the origination company.
                    strTrans = string.Concat(new string[] { strTrans, "<OriginationCompany>", mlapdPartnerDataMgr.GetConfig("PPG_SOAP_Header/OriginationCompany"), "</OriginationCompany>" });

                    //Add in the origination ID.
                    strTrans = string.Concat(new string[] { strTrans, "<OriginationPID>", strOriginationId, "</OriginationPID>" });

                    //Add in the destination company and Id.
                    strTrans = string.Concat(new string[] { strTrans , "<DestinationCompany>" , strDestination , "</DestinationCompany>"
                        , "<DestinationPID>" , strDestinationId , "</DestinationPID>"});

                    //Add in the transaction type.
                    strTrans = string.Concat(new string[] { strTrans, "<TransactionType>", strTransactionType, "</TransactionType>" });

                    //Add in the sender's reference Id.
                    strTrans = string.Concat(new string[] { strTrans, "<SendersReferenceID>", strReferenceId, "</SendersReferenceID>" });

                    //Add in the current time and date.
                    strTrans = string.Concat(new string[] { strTrans, "<TransactionDateTime>", DateTime.Now.ToString(), "</TransactionDateTime>" });

                    //Close out the header, begin the message body.
                    strTrans = string.Concat(new string[] { strTrans, "</Delivery></Header><MessageBody>" });

                    //Add in CDATA tag if specified in configuration.
                    if (blnUseCDATA)
                        strTrans = string.Concat(new string[] { strTrans, "<![CDATA[" });

                    //Add in the message body.
                    strTrans = string.Concat(new string[] { strTrans, strMessageBody });

                    //Close CDATA tag if specified in configuration.
                    if (blnUseCDATA)
                        strTrans = string.Concat(new string[] { strTrans, "]]>" });

                    //Close the message and envelope tags - we are done!
                    strReturnTrans = string.Concat(new string[] { strTrans, "</MessageBody></StdEnvelope>" });

                    if (mlapdPartnerDataMgr.g_blnDebugMode)
                    {
                        mlapdPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "XML = '", strTrans }), string.Concat(new string[] { PROC_NAME, "Envelope" }));
                        strReturnTrans = strTrans;
                    }
                }
            }
            catch (Exception ex)
            {
                mlapdPartnerDataMgr.g_objEvents.Env(string.Concat(new string[] { "Message = ", strMessageBody }));
                mlapdPartnerDataMgr.g_objEvents.Env(string.Concat(new string[] { "Result = ", strTrans }));
                throw ex;
            }
            return strReturnTrans;
        }

        /// <summary>
        /// Syntax:      XmlHttpPost
        /// Parameters:  strUrl - The URL to post to.
        ///              strXml - The XML data to POST.
        ///              g_objEvents - An instance of SiteUtilities::CEvents to use.
        /// Purpose:     Posts the passed data to a url.         
        /// Returns:     The status.
        /// </summary>
        public void XmlHttpPost(string strUrl, string strXML)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "XmlHttpPost: " });

            Int32 intFailOverCount = 0;
            const Int32 MaxFailoverAttempts = 10;
            const Int32 FailoverSleepPeriod = 1000;
            MSXML2.XMLHTTP objXmlHttp = null;
            try
            {
                mlapdPartnerDataMgr = new MLAPDPartnerDataMgr();
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    mlapdPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "URL = '", strUrl, "'", PROC_NAME }));
              
                objXmlHttp = new XMLHTTP();
                objXmlHttp.open("POST", strUrl, false);

                objXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                objXmlHttp.send(strXML);

                if (objXmlHttp.status != 200)
                    throw new Exception(string.Concat(new string[] { PROC_NAME, "HTTP Send", "Bad status returned from post to", strUrl }));

                //Retry loop count below max so far?
                if (intFailOverCount < MaxFailoverAttempts)
                {
                    //Per TestTrack 1782 - Disable all error and warning emails that no longer hold relevance.
                    //Thus Reduced the following message from an event to a trace message.

                    if (intFailOverCount == 0)
                    {
                        if (mlapdPartnerDataMgr.g_blnDebugMode)
                            mlapdPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "A CRITICAL HTTP POSTING ERROR HAS OCCURRED", Environment.NewLine, "ENTERING FAILOVER RETRY LOOP", PROC_NAME }));

                        //g_objEvents.HandleEvent Err.Number, PROC_NAME & Err.Source, Err.Description, _
                        //   "XmlHttpPost: A CRITICAL ERROR HAS OCCURRED HTTP POSTING - " & _
                        //  "ENTERING FAILOVER RETRY LOOP", "", False

                        intFailOverCount = 1;
                    }
                    else
                    {
                        intFailOverCount = intFailOverCount + 1;
                    }
                    //Protect against errors in this code

                    //Clear the error object.

                    //Err.Clear();

                    //Pause to give time for cluster failover to complete.
                    Thread.Sleep(FailoverSleepPeriod);

                    //Resume FailOverLoop;
                }
                else    // Give up!
                {
                    //Add note to log that the retry count has been exceeded.
                    mlapdPartnerDataMgr.g_objEvents.Env(string.Concat(new string[] { "XmlHttpPost: MAXIMUM RETRY COUNT OF ", MaxFailoverAttempts.ToString(), " HAS BEEN EXCEEDED!" }));
                }

                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    mlapdPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "HTTP Submit Successful!" }));
            }
            catch (Exception ex)
            {
                objXmlHttp = null;
                mlapdPartnerDataMgr.g_objEvents.Env(string.Concat(new string[] { "URL = ", strUrl }));
                mlapdPartnerDataMgr.g_objEvents.Env(string.Concat(new string[] { "XML = ", strXML }));
                throw ex;
            }
        }

        #endregion
    }
}
