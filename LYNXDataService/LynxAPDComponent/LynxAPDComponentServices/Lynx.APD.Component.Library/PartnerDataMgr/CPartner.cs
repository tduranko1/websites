﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Xml;
using ADODB;
using Lynx.APD.Component.Library.Common;
using Scripting;
using SiteUtilities;
using System.Threading;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    public class CPartner
    {
        #region Variables
        private const string MODULE_NAME = "LAPDPartnerDataMgrCPartner";

        private MDomUtils mobjMDomUtils = null;
        private MLAPDPartnerDataMgr mobjLAPDPartner = null;///mLAPDEcadAccessorMgr
        private MEscapeUtilities mobjMEscapeUtilities = null;
        private MBase64Utils mobjBase64utils = null;
        MPPGTransactions mobjMPPGTransactions = null;

        private EDocumentType menmDocType;
        private string mstrDocType = string.Empty,
        mstrRawDocType = string.Empty,
        mstrPartnerConfig = string.Empty,
        mstrExistingEstimateDataXml = string.Empty,
        mstrExistingPrintImageXml = string.Empty,
        mstrPartner = string.Empty,
        mstrActualLynxID = string.Empty,
        mstrLynxID = string.Empty,
        mstrAssignmentID = string.Empty,
        mstrTaskID = string.Empty,
        mstrVehicleNumber = string.Empty,
        mstrActionCode = string.Empty,
        mstrPassedDocument = string.Empty,
        mstrPartnerSmallID = string.Empty;

        long mlngPartnerTransID = 0,
            mlngExistingEstimateDataID = 0,
            mlngExistingPrintImageID = 0,
            mintEstimateSequenceNumber = 0;

        object mvarTransactionDate = null;

        XmlDocument mobjPassedDocument = null;
        #endregion

        #region Enum
        /// <summary>  
        /// 'Error codes private to this module.
        /// </summary>
        private enum CPartner_Error_Codes : ulong
        {
            eNotCreateable = 0x80067400,
            eShopSearchMissingParams
        }

        
        public enum EDocumentType
        {
            eAssignment,
            eEstimateData,
            ePrintImage,
            eDigitalImage,
            eBusinessEvent,
            eReceipt,
            eHeartBeat,
            eShopSearch,
            eFolderUpload,
            eClaimMessage
        }
        #endregion

        #region Properties
        /// <summary>
        /// Sets the Document Type Enum and String
        /// </summary>
        internal EDocumentType DocTypeEnum
        {
            get
            {
                return menmDocType;
            }
            set
            {
                menmDocType = value;
                SetDocTypeEnumProperty(menmDocType);
            }
        }


        /// <summary>
        /// Gets the Document Type String
        /// </summary>
        internal string DocTypeString
        {
            get
            {
                return mstrDocType;
            }
        }


        /// <summary>
        /// Gets the document type specific configuration XPath
        /// </summary>
        internal string DocTypeConfig
        {
            get
            {
                return string.Concat(new string[] { mstrPartnerConfig, mstrDocType, "/" });
            }

        }


        /// <summary>
        /// Gets the raw document type.
        /// </summary>
        public string RawDocTypeString
        {
            get
            {
                return mstrDocType;
            }
            set
            {
                mstrDocType = value;
                RawDocTypeStringEncode(mstrDocType);
            }
        }


        /// <summary>
        /// Gets the document ID of any document added to the partner database.
        /// </summary>
        internal long PartnerTransID
        {
            get
            {
                return mlngPartnerTransID;
            }
        }


        /// <summary>
        /// The estimate sequence number.
        /// </summary>
        internal int EstimateSequenceNumber
        {
            get
            {
                return Convert.ToInt16(mintEstimateSequenceNumber);
            }
            set
            {
                EstimateSequenceNumber = Convert.ToInt16(mintEstimateSequenceNumber);
            }
        }


        internal string DocTypeStringForDatabase
        {
            get
            {
                return DocTypeStringForDB(menmDocType);
            }
        }


        //Gets the Actual LynxID
        internal string ActualLynxID
        {
            get
            {
                return mstrActualLynxID;
            }
        }


        //Gets the LynxID
        internal string LynxID
        {
            get
            {
                return mstrLynxID;
            }
        }


        //Gets the AssignmentID
        internal string AssignmentID
        {
            get
            {
                return mstrAssignmentID;
            }
        }


        // Gets the TaskID
        internal string TaskID
        {
            get
            {
                return mstrTaskID;
            }
        }


        //Gets the Vehicle Number
        internal string VehicleNumber
        {
            get
            {
                return mstrVehicleNumber;
            }
        }


        //Gets the Action Code
        internal string ActionCode
        {
            get
            {
                return mstrActionCode;
            }
        }


        //Gets the Transaction Date
        internal object TransactionDate
        {
            get
            {
                return mvarTransactionDate;
            }
        }


        //Gets the Passed Document
        internal string PassedDocument
        {
            get
            {
                return mstrPassedDocument;
            }
            set
            {
                mstrPassedDocument = value;
                XmlDocument mobjPassedDocument = new XmlDocument();
                mobjMDomUtils = new MDomUtils();
                mobjMDomUtils.LoadXml(ref mobjPassedDocument, ref mstrPassedDocument, string.Empty, string.Empty);
            }
        }


        //Gets the Partner
        internal string TradingPartner
        {
            get
            {
                return mstrPartner;
            }
            set
            {
                mstrPartner = value;
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                mstrPartnerSmallID = mobjLAPDPartner.GetConfig(string.Concat(new string[] { "PartnerSettings/", mstrPartner, "/@SmallID" }));
            }
        }
        #endregion

        #region Methods
        //Internal Document Type Properties (string and enumerated versions)

        /// <summary>
        /// This method initializes processing of the documents
        /// </summary>
        /// <returns></returns>
        public bool InitProcessDocument()
        {
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "InitProcessDocument() Method not implemented" }));
        }


        /// <summary>
        /// This method initializes processing of the documents.
        /// </summary>
        /// <param name="strTradingPartner"></param>
        /// <param name="strPassedDocument"></param>
        /// <returns></returns>
        public bool InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            bool isInitProcessDocumentV2 = false;
            try
            {
                return isInitProcessDocumentV2;
                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "InitProcessDocumentV2() Method not implemented" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// This method processes documents to the partner database.
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            bool isPartnerDatabaseTransfer = false;
            try
            {
                return isPartnerDatabaseTransfer;
                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "PartnerDatabaseTransfer()", ",", "Method not implemented" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        /// <summary>
        /// This method processes documents to the partner database.
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            bool isPartnerDatabaseTransferV2 = false;
            try
            {
                return isPartnerDatabaseTransferV2;
                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "PartnerDatabaseTransferV2()", ",", "Method not implemented" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// This method processes documents to the APD database.
        /// </summary>
        /// <returns></returns>
        public string ApdDatabaseTransfer()
        {
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "ApdDatabaseTransfer()", ",", "Method not implemented" }));
        }


        /// <summary>
        /// This method processes documents to the APD database.
        /// </summary>
        /// <returns></returns>
        public string ApdDatabaseTransferV2()
        {
            string strApdDatabaseTransferV2 = string.Empty;
            try
            {
                return strApdDatabaseTransferV2;
                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "ApdDatabaseTransferV2()", ",", "Method not implemented" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// This method transmits documents to the partner. 
        /// </summary>
        /// <returns></returns>
        public string TransferToPartner()
        {
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "TransferToPartner()", ",", "Method not implemented" }));
        }


        /// <summary>
        /// Purpose: Inserts an estimate print image into the APD document system.
        /// Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        /// <param name="strXML"></param>
        /// <param name="lngPartnerTransID"></param>
        /// <returns></returns>
        public long InsertPrintImageIntoAPD(string strXML, long lngPartnerTransID)
        {
            long lngInsertPrintImageIntoAPD = 0;
            XmlDocument objXmlDoc = null;
            try
            {
                objXmlDoc = new XmlDocument();
                mobjMDomUtils = new MDomUtils();

                mobjMDomUtils.LoadXml(ref objXmlDoc, ref strXML, "InsertPrintImageIntoAPD", "Print Image XML");
                lngInsertPrintImageIntoAPD = ProcessBinaryFiles(ref objXmlDoc, "//ImageFile", "", "", "PDF", "PrintImage", EDocumentType.ePrintImage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objXmlDoc = null;
            }
            return lngInsertPrintImageIntoAPD;
        }


        /// <summary>
        ///Purpose: Inserts an estimate print image into the APD document system.
        /// Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        /// <param name="strXML"></param>
        /// <param name="lngPartnerTransID"></param>
        /// <returns></returns>
        public long InsertPrintImageIntoAPDV2(string strXML, long lngPartnerTransID)
        {
            long lnginsertPrintImageIntoAPDV2 = 0;
            XmlDocument objXmlDocument = null;

            try
            {
                objXmlDocument = new XmlDocument();
                mobjMDomUtils = new MDomUtils();

                mobjMDomUtils.LoadXml(ref objXmlDocument, ref strXML, "InsertPrintImageIntoAPDV2", "Print Image XML");

                lnginsertPrintImageIntoAPDV2 = ProcessBinaryFiles_v2(ref objXmlDocument, "//ImageFile", "", "", "PDF", "PrintImage", EDocumentType.ePrintImage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lnginsertPrintImageIntoAPDV2;
        }


        /// <summary>
        /// Send a HeartBeat request to Partner.
        /// </summary>
        public void SendHeartBeatRequest()
        {
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "SendHeartBeatRequest()", ",", "Method not implemented" }));
        }


        internal void SetDocTypeEnumProperty(EDocumentType enmDocType)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SetDocTypeEnumPropertyDocTypeEnum: " });

            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                mstrPartnerConfig = string.Concat(new string[] { "PartnerSettings/", mobjLAPDPartner.g_strPartner, "/" });
                switch (enmDocType)
                {
                    case EDocumentType.eAssignment:
                        mstrDocType = "Assignment";
                        break;
                    case EDocumentType.eEstimateData:
                        mstrDocType = "EstimateData";
                        break;
                    case EDocumentType.ePrintImage:
                        mstrDocType = "PrintImage";
                        break;
                    case EDocumentType.eDigitalImage:
                        mstrDocType = "DigitalImage";
                        break;
                    case EDocumentType.eBusinessEvent:
                        mstrDocType = "BusinessEvent";
                        break;
                    case EDocumentType.eReceipt:
                        mstrDocType = "Receipt";
                        break;
                    case EDocumentType.eHeartBeat:
                        mstrDocType = "HeartBeat";
                        break;
                    case EDocumentType.eShopSearch:
                        mstrDocType = "ShopSearch";
                        break;
                    case EDocumentType.eFolderUpload:
                        mstrDocType = "FolderUpload";
                        break;
                    case EDocumentType.eClaimMessage:
                        mstrDocType = "ClaimMessage";
                        break;
                    default:
                        throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType), ",", PROC_NAME, ",", "Unknown document type : '", Convert.ToString(enmDocType), "'." }));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void RawDocTypeStringEncode(string strRawDocType)
        {
            string PROC_NAME = string.Empty;
            mstrPartnerConfig = string.Concat(new string[] { "PartnerSettings/", mobjLAPDPartner.g_strPartner, "/" });
            mstrRawDocType = strRawDocType;
            mstrDocType = "";

            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, " Let RawDocTypeString: " });

                if ((strRawDocType.Length) > 0)
                    if (mobjLAPDPartner.g_blnUsePartnerTypesForDocTypes)
                    {
                        if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "EstimateData/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eEstimateData;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "PrintImage/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.ePrintImage;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "DigitalImage/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eDigitalImage;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "HeartBeat/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eHeartBeat;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "Receipt/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eReceipt;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "BusinessEvent/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eBusinessEvent;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "ShopSearch/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eShopSearch;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "Assignment/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eAssignment;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "FolderUpload/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eFolderUpload;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "ClaimMessage/@PartnerType" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eClaimMessage;
                    }
                    else
                    {
                        if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "EstimateData/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eEstimateData;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "PrintImage/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.ePrintImage;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "DigitalImage/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eDigitalImage;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "HeartBeat/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eHeartBeat;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "Receipt/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eReceipt;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "BusinessEvent/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eBusinessEvent;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "ShopSearch/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eShopSearch;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "Assignment/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eAssignment;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "FolderUpload/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eFolderUpload;
                        else if (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "ClaimMessage/@Type" })) == strRawDocType)
                            DocTypeEnum = EDocumentType.eClaimMessage;
                    }
            }
            catch (Exception)
            {

                throw;
            }
        }


        internal string DocTypeStringForDB(EDocumentType menmdoctype)
        {
            string strDocType = string.Empty;
            try
            {
                switch (menmdoctype)
                {
                    case EDocumentType.eAssignment:
                        strDocType = "Assignment";
                        break;
                    case EDocumentType.eEstimateData:
                        strDocType = "EstimateTransaction";
                        break;
                    case EDocumentType.ePrintImage:
                        strDocType = "EstimatePI";
                        break;
                    case EDocumentType.eDigitalImage:
                        strDocType = "EstimateDI";
                        break;
                    case EDocumentType.eBusinessEvent:
                        strDocType = "BusinessEvent";
                        break;
                    case EDocumentType.eReceipt:
                        strDocType = "ExternalAck";
                        break;
                    case EDocumentType.eHeartBeat:
                        strDocType = "HeartBeat";
                        break;
                    case EDocumentType.eShopSearch:
                        strDocType = "ShopSearch";
                        break;
                    case EDocumentType.eFolderUpload:
                        strDocType = "FolderUpload";
                        break;
                    case EDocumentType.eClaimMessage:
                        strDocType = "ClaimMessage";
                        break;
                    default:
                        throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType), "Unknown document type : ", Convert.ToString(menmdoctype) }));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strDocType;
        }


        /// <summary>
        ///  Returns True if we are supposed to process this document type.
        /// </summary>
        internal bool CanProcessDocument()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CanProcessDocument: " });
            bool isProcessDocument = false;

            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                //'Ignore any transaction with LynxID = 'TEST_LYNX_ID'
                if (mobjLAPDPartner.g_strLynxID == "TEST_LYNX_ID")
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { mstrDocType, " Received. ", "Ignoring transaction with LynxID = 'TEST_LYNX_ID'", PROC_NAME }));
                }
                //Check to see if we are supposed to process this document type.
                else if ("True" != mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "@Process" })))
                {
                    //If not, log this fact and exit.
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "We are not configured to process ", mobjLAPDPartner.g_strPartner, " document type ", mstrDocType, PROC_NAME }));
                }
                //Otherwise return True for normal document consumption.
                else
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                    {
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { mstrDocType, " Received ", PROC_NAME }));
                        isProcessDocument = true;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return isProcessDocument;
        }


        /// <summary>
        ///  Returns True if we are supposed to process this document type.
        /// </summary>
        /// 
        internal bool CanProcessDocument_v2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "CanProcessDocument_v2: " });
            bool isCanProcessDocument_v2 = false;
            try
            {
                //Ignore any transaction with LynxID = 'TEST_LYNX_ID'
                if (mstrLynxID == "TEST_LYNX_ID")
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { mstrDocType, " Received. ", "Ignoring transaction with LynxID = 'TEST_LYNX_ID'", PROC_NAME }));
                }
                //Check to see if we are supposed to process this document type.
                else if ("True" != mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "@Process" })))
                {
                    //If not, log this fact and exit.
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "We are not configured to process ", mobjLAPDPartner.g_strPartner, " document type ", mstrDocType, PROC_NAME }));
                }
                //Otherwise return True for normal document consumption.
                else
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                    {
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { mstrDocType, " Received", PROC_NAME }));
                        isCanProcessDocument_v2 = true;
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return isCanProcessDocument_v2;
        }


        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string strParam, int intLength)
        {
            string strResult = strParam.Substring(0, intLength);
            return strResult;
        }


        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string strParam, int intLength)
        {
            string strResult = strParam.Substring(strParam.Length - intLength, intLength);
            return strResult;
        }


        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="startIndex"></param>
        /// <param name="intLength"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int startIndex, int intLength)
        {
            string strResult = strParam.Substring(startIndex, intLength);
            return strResult;
        }


        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="strParam"></param>
        /// <param name="intstartIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string strParam, int intstartIndex)
        {
            string strResult = strParam.Substring(intstartIndex);
            return strResult;
        }


        /// <summary>
        /// BuildFinalFileName
        /// </summary>
        /// <returns></returns>
        internal string BuildFinalFileName(int intSuffix, string strDate, string strDocType)
        {
            // Construct for the Digital Image's actual filename as it will be stored on disk
            int intImageCount = 0;
            string strImageFolder = string.Empty;
            string strLynxID = string.Empty;
            string strPrefix = string.Empty;
            string strSuffix = string.Empty;
            string strfinalFileName = string.Empty;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                strLynxID = Right(mobjLAPDPartner.g_strLynxID, 4);
                for (intImageCount = 1; intImageCount <= 4; intImageCount++)
                {
                    strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\", Mid(strLynxID, intImageCount, 1) });
                }
                strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\" });

                // The first letter in the name of the file for imaging purposes
                strPrefix = mobjLAPDPartner.GetConfig(string.Concat(new string[] { "PartnerSettings/", mobjLAPDPartner.g_strPartner, "/", strDocType, "/", "@ImageFilenamePrefix" }));
                strSuffix = mobjLAPDPartner.GetConfig(string.Concat(new string[] { "PartnerSettings/", mobjLAPDPartner.g_strPartner, "/", strDocType, "/", "@ImageFilenameSuffix" }));

                strfinalFileName = string.Concat(new string[] { strImageFolder, strPrefix, strDate, "LYNXID", mobjLAPDPartner.g_strLynxID, "V", mobjLAPDPartner.g_strVehicleNumber, "Doc", mobjLAPDPartner.ZeroPad(intSuffix, 3), ".", strSuffix });
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strfinalFileName;
        }


        /// <summary>
        /// Build Final File Name
        /// </summary>
        internal string BuildFinalFileName_v2(int intSuffix, string strDate, string strDocType)
        {
            int intImageCount = 0;
            string strImageFolder = null;
            string strLynxID = null;
            string strPrefix = null;
            string strSuffix = null;
            string strGUID = null;
            string strfinalFileName_v2 = string.Empty;

            Guid generateGUID = default(Guid);
            try
            {
                // Construct for the Digital Image's actual filename as it will be stored on disk
                strLynxID = Right(mstrLynxID, 4);

                for (intImageCount = 1; intImageCount <= 4; intImageCount++)
                {
                    strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\", Mid(strLynxID, intImageCount, 1) });
                }
                strImageFolder = string.Concat(new string[] { strImageFolder, "\\\\" });

                // The first letter in the name of the file for imaging purposes
                strPrefix = mobjLAPDPartner.GetConfig(string.Concat(new string[] { "PartnerSettings/", mobjLAPDPartner.g_strPartner, "/", strDocType, "/", "@ImageFilenamePrefix" }));
                strSuffix = mobjLAPDPartner.GetConfig(string.Concat(new string[] { "PartnerSettings/", mobjLAPDPartner.g_strPartner, "/", strDocType, "/", "@ImageFilenameSuffix" }));

                generateGUID = Guid.NewGuid();
                strGUID = generateGUID.ToString();
                strGUID = strGUID.Replace("{", "");
                strGUID = strGUID.Replace("}", "");
                strGUID = strGUID.Replace("-", "");

                strfinalFileName_v2 = string.Concat(new string[] { strImageFolder, strPrefix, strGUID, "LYNXID", mstrLynxID, "V", mstrVehicleNumber, "Doc", mobjLAPDPartner.ZeroPad(intSuffix, 3), ".", strSuffix });
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strfinalFileName_v2;
        }


        /// <summary>
        /// Insert LAPD Document
        /// Calls a DB stored procedure to add or update an APD document record.
        /// Parameters:  objDataAccessor   - The instance of Data Accessor to use
        /// strDocType        - Document Type
        /// lngLynxID         - The LynxID to attach the document to
        /// intVehicleNumber  - Vehicle Number to attach the document to
        /// intSequenceNumber - Estimate/Supplement Sequence Number
        /// strpartner        - Partner
        /// lngPartnerTransID - Identity ID of document in the partner database
        /// strImagePath      - The image file path to be stored with the document record
        /// strImageType      - The image type being stored (jpg, txt, xml, etc)
        /// lngAPDDocumentID  - Identity of associated record in APD Database (if available)
        ///                    (The resulting APD Document ID will be here when complete)
        /// </summary>
        internal long InsertLAPDDocument(ref DataAccessor.CDataAccessor objDataAccessor, EDocumentType enmDocType, long lngAssignmentID,
                                         long lngLynxID, long intVehicleNumber, int intSequenceNumber, string strPartnerTransID, string strImagePath,
                                         string strImageType, string strCreatedDate, bool blnNotifyEvent)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertLAPDDocument: " });
            string strDBDocumentType = string.Empty;
            string strDBImagePath = string.Empty;
            string strDBImageDate = string.Empty;
            string strDBDocumentSource = string.Empty;
            long lngLAPDDocument = 0;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                //Determine the DBDocumentType
                switch (enmDocType)
                {
                    case EDocumentType.ePrintImage:
                    case EDocumentType.eEstimateData:
                        if (intSequenceNumber == 0)
                            strDBDocumentType = "Estimate";
                        else
                            strDBDocumentType = "Supplement";
                        break;
                    case EDocumentType.eDigitalImage:
                        strDBDocumentType = "Photograph";
                        break;
                }

                strDBDocumentSource = mobjLAPDPartner.GetConfig(string.Concat(new string[] { "PartnerSettings/", mobjLAPDPartner.g_strPartner, "/@SourceID" }));
                //Determine the DBImagePath and Date
                if (strImagePath == "")
                {
                    strDBImagePath = "";
                    strDBImageDate = "";
                }
                else
                {
                    strDBImagePath = strImagePath;
                    strDBImageDate = DateTime.Now.ToString("yyyy-mm-ddThh:mm:ss");
                }

                //Use Now for the created date if not passed.
                if (string.IsNullOrEmpty(strCreatedDate))
                    strCreatedDate = DateTime.Now.ToString("yyyy-mm-ddThh:mm:ss");
                //Set the connect string to APD
                objDataAccessor.SetConnectString(mobjLAPDPartner.g_strLYNXConnStringStd);

                // Perform the insert
                lngLAPDDocument = objDataAccessor.ExecuteSpNamedParams(mobjLAPDPartner.GetConfig("PartnerSettings/InsertSP"), new object[] { "@NTUserID", mobjLAPDPartner.GetConfig("WorkFlow/NTUserID"), "@PertainsTo", string.Concat(new string[] { "VEH", Convert.ToString(intVehicleNumber) }), "@AssignmentID", lngAssignmentID, "@LynxID", lngLynxID, "@NotifyEvent",
               (blnNotifyEvent ? 1 : 0), "@CreatedDate", strCreatedDate, "@ImageDate", strDBImageDate, "@ImageLocation", strDBImagePath, "@DocumentSource", strDBDocumentSource, "@DocumentType",
               strDBDocumentType, "@SupplementSeqNumber", intSequenceNumber, "@ExternalReference", strPartnerTransID, "@ImageType", strImageType});
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lngLAPDDocument;
        }


        /// <summary>
        /// Insert LAPD Document

        /// Calls a DB stored procedure to add or update an APD document record.

        /// Parameters:  objDataAccessor   - The instance of Data Accessor to use
        /// strDocType        - Document Type
        /// lngLynxID         - The LynxID to attach the document to
        /// intVehicleNumber  - Vehicle Number to attach the document to
        /// intSequenceNumber - Estimate/Supplement Sequence Number
        /// strpartner        - Partner
        /// lngPartnerTransID - Identity ID of document in the partner database
        /// strImagePath      - The image file path to be stored with the document record
        /// strImageType      - The image type being stored (jpg, txt, xml, etc)
        /// lngAPDDocumentID  - Identity of associated record in APD Database (if available)
        ///                    (The resulting APD Document ID will be here when complete)
        /// </summary>
        internal long InsertLAPDDocument_v2(ref DataAccessor.CDataAccessor objDataAccessor, EDocumentType enmDocType, long lngAssignmentID,
                                            long lngLynxID, long intVehicleNumber, int intSequenceNumber, string strPartnerTransID, string strImagePath,
                                            string strImageType, string strCreatedDate, bool blnNotifyEvent)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertLAPDDocument_v2: " });
            string strDBDocumentType = string.Empty;
            string strDBImagePath = string.Empty;
            string strDBImageDate = string.Empty;
            string strDBDocumentSource = string.Empty;
            long lngLAPDDocument_v2 = 0;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                //Determine the DBDocumentType
                switch (enmDocType)
                {
                    case EDocumentType.ePrintImage:
                    case EDocumentType.eEstimateData:

                        if (intSequenceNumber == 0)
                            strDBDocumentType = "Estimate";
                        else
                            strDBDocumentType = "Supplement";
                        break;
                    case EDocumentType.eDigitalImage:
                        strDBDocumentType = "Photograph";
                        break;
                }
                strDBDocumentSource = mobjLAPDPartner.GetConfig(string.Concat(new string[] { "PartnerSettings/", mstrPartner, "/@SourceID" }));

                // Determine the DBImagePath and Date
                if (string.IsNullOrEmpty(strImagePath))
                {
                    strDBImagePath = "";
                    strDBImageDate = "";
                }
                else
                {
                    strDBImagePath = strImagePath;
                    strDBImageDate = DateTime.Now.ToString("yyyy-mm-ddThh:mm:ss");
                }

                //Use Now for the created date if not passed.
                if (string.IsNullOrEmpty(strCreatedDate))
                    strCreatedDate = DateTime.Now.ToString("yyyy-mm-ddThh:mm:ss");

                //Set the connect string to APD
                objDataAccessor.SetConnectString(mobjLAPDPartner.g_strLYNXConnStringStd);

                // Perform the insert
                lngLAPDDocument_v2 = objDataAccessor.ExecuteSpNamedParams(mobjLAPDPartner.GetConfig("PartnerSettings/InsertSP"), new object[] { "@NTUserID", mobjLAPDPartner.GetConfig("WorkFlow/NTUserID"), "@PertainsTo", string.Concat(new string[] { "VEH", Convert.ToString(intVehicleNumber) }), "@AssignmentID", lngAssignmentID, "@LynxID", lngLynxID, "@NotifyEvent",
                (blnNotifyEvent ? 1 : 0), "@CreatedDate", strCreatedDate, "@ImageDate", strDBImageDate, "@ImageLocation", strDBImagePath, "@DocumentSource", strDBDocumentSource, "@DocumentType",
                strDBDocumentType, "@SupplementSeqNumber", intSequenceNumber, "@ExternalReference", strPartnerTransID, "@ImageType", strImageType});
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return lngLAPDDocument_v2;
        }

        /// <summary>
        /// Sends a workflow notification.
        /// </summary>
        internal void SendWorkflowNotification(string strCodeName, string strDescription, string strKey = "", string strPertainsTo = "VEH", string strConditionValue = "")
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendWorkflowNotification: " });

            string strEventID = string.Empty;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "Code: ", strCodeName, " Desc: '", strDescription, "' Key: '", strKey, "'", PROC_NAME, "Initiate Workflow call" }));

                strEventID = mobjLAPDPartner.GetConfig(string.Concat(new string[] { "WorkFlow/EventCodes/Code[@name='", strCodeName, "']" }));

                if (string.IsNullOrEmpty(strKey))
                {
                    //this could be an RRP assignment. We will formulate a special key and pass it in
                    //  we don't want to make another call to figure out the assignment sequence number
                    //  but let the workflowNotifyEvent proc to figure it out.
                    strKey = string.Concat(new string[] { "$AssignmentID$=", mobjLAPDPartner.g_strAssignmentID });
                }

                //Add vehicle number to pertains to if needed.
                if (strPertainsTo == "VEH")
                {
                    strPertainsTo = string.Concat(new string[] { strPertainsTo, Convert.ToString(mobjLAPDPartner.g_strVehicleNumber) });
                }

                if (!string.IsNullOrEmpty(strEventID))
                {
                    strDescription = strDescription.Replace("'", "''");

                    mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strLYNXConnStringStd);
                    mobjLAPDPartner.g_objDataAccessor.ExecuteSpNamedParams(mobjLAPDPartner.GetConfig("WorkFlow/SPName"), new object[]{ "@EventID", Convert.ToInt64(strEventID), "@LynxID", mobjLAPDPartner.g_strLynxID, "@PertainsTo", strPertainsTo, "@Description", strDescription, "@Key",
                    strKey, "@UserID", mobjLAPDPartner.GetConfig("WorkFlow/SysUserID"), "@ConditionValue", strConditionValue});

                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "", "Workflow Notification Sent : ", strCodeName }));
                }
                else
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "", PROC_NAME, "EventID For ", strCodeName, " not found in the config file.  Bypassing workflow call." }));
                }
            }
            catch (Exception ex)
            {
                mobjLAPDPartner.g_objEvents.Env(string.Concat(new string[] { "Description = ", strDescription }));
                mobjLAPDPartner.g_objEvents.Env(string.Concat(new string[] { "Key = ", strKey }));
                throw ex;
            }
        }


        /// <summary>
        /// Sends a workflow notification.
        /// </summary>
        internal void SendWorkflowNotification_v2(string strCodeName, string strDescription, string strKey = "", string strPertainsTo = "VEH", string strConditionValue = "")
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendWorkflowNotification_v2: " });

            string strEventID = null;

            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "Code: ", strCodeName, " Desc: '", strDescription, "' Key: '", strKey, "'", PROC_NAME, "Initiate Workflow call" }));

                strEventID = mobjLAPDPartner.GetConfig(string.Concat(new string[] { "WorkFlow/EventCodes/Code[@name='", strCodeName, "']" }));

                //Add vehicle number to pertains to if needed.
                if (strPertainsTo == "VEH")
                {
                    strPertainsTo = string.Concat(new string[] { strPertainsTo, Convert.ToString(mstrVehicleNumber) });
                }

                if (!string.IsNullOrEmpty(strEventID))
                {
                    strDescription = strDescription.Replace("'", "''");

                    mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strLYNXConnStringStd);
                    mobjLAPDPartner.g_objDataAccessor.ExecuteSpNamedParams((mobjLAPDPartner.GetConfig("WorkFlow/SPName")), new object[] { "@EventID", Convert.ToInt64(strEventID), "@LynxID", mstrLynxID, "@PertainsTo", strPertainsTo, "@Description", strDescription, "@Key", strKey, "@UserID", mobjLAPDPartner.GetConfig("WorkFlow/SysUserID"), "@ConditionValue", strConditionValue });

                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "", "Workflow Notification Sent : ", strCodeName }));
                }
                else
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "", PROC_NAME, "EventID For ", strCodeName, " not found in the config file.  Bypassing workflow call." }));
                }
            }
            catch (Exception ex)
            {
                mobjLAPDPartner.g_objEvents.Env(string.Concat(new string[] { "Description = ", strDescription }));
                mobjLAPDPartner.g_objEvents.Env(string.Concat(new string[] { "Key = ", strKey }));
                throw ex;
            }
        }


        /// <summary>
        /// Stores a estimate print image transaction in the partner database.
        /// </summary>
        internal void StorePrintImageInPartnerDB()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StorePrintImageInPartnerDB: " });

            XmlDocument objBinaryNode = null;
            ADODB.Recordset objRS = null;
            try
            {
                objBinaryNode = new XmlDocument();
                objRS = new ADODB.Recordset();
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                //Store the doc in the partner DB, returning results as a recordset.
                objRS = StoreDocumentInPartnerDb();

                if (objRS.EOF && objRS.BOF)
                {
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned), PROC_NAME, "No Records returned from partner DB for Print Image submission." }));
                }

                mstrExistingPrintImageXml = mobjLAPDPartner.g_objPassedDocument.OuterXml;
                mlngExistingPrintImageID = mlngPartnerTransID;

                //If the document exists then clear the XML string pointers
                //so that nothing will be submitted to the APD database.
                if (objRS.Fields["DocumentExists"].Value == "true")
                {
                    mstrExistingPrintImageXml = "";
                    mstrExistingEstimateDataXml = "";

                    //COMMENTED OUT - This message no longer holds value.
                    //We get plenty of dupes, and they no longer represent an error case.

                    //Send internal informational message.
                    //g_objEvents.HandleEvent eDuplicateDocument, PROC_NAME, _
                    //    mlngExistingPrintImageID, "", "", False

                    //Otherwise, if available, extract the 'Other' document so that we have both ED and PI.
                }
                else
                {
                    if (objRS.Fields["OtherTransXML"] != null)
                    {
                        mstrExistingEstimateDataXml = Convert.ToString(objRS.Fields["OtherTransXML"]);
                        mlngExistingEstimateDataID = Convert.ToInt64(objRS.Fields["OtherPartnerTransID"]);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objBinaryNode = null;

                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                        objRS.Close();
                    objRS = null;
                }
            }
        }


        /// <summary>
        /// Stores a estimate print image transaction in the partner database.
        /// </summary>
        internal void StorePrintImageInPartnerDB_v2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StorePrintImageInPartnerDB_v2: " });

            XmlDocument objBinaryNode = null;
            ADODB.Recordset objRS = null;

            try
            {
                objBinaryNode = new XmlDocument();
                objRS = new ADODB.Recordset();

                //Store the doc in the partner DB, returning results as a recordset.
                objRS = StoreDocumentInPartnerDb_v2();

                if (objRS.EOF && objRS.BOF)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned), ",", PROC_NAME, ",", "No Records returned from partner DB for Print Image submission." }));

                mstrExistingPrintImageXml = mobjPassedDocument.OuterXml;
                mlngExistingPrintImageID = mlngPartnerTransID;

                //If the document exists then clear the XML string pointers
                //so that nothing will be submitted to the APD database.

                if (objRS.Fields["DocumentExists"].Value == "true")
                {
                    mstrExistingPrintImageXml = "";
                    mstrExistingEstimateDataXml = "";

                    //COMMENTED OUT - This message no longer holds value.
                    //We get plenty of dupes, and they no longer represent an error case.

                    //Send internal informational message.
                    //g_objEvents.HandleEvent eDuplicateDocument, PROC_NAME, _
                    //    mlngExistingPrintImageID, "", "", False

                    //Otherwise, if available, extract the 'Other' document so that we have both ED and PI.
                }
                else
                {
                    if (objRS.Fields["OtherTransXML"] != null)
                    {
                        mstrExistingEstimateDataXml = Convert.ToString(objRS.Fields["OtherTransXML"]);
                        mlngExistingEstimateDataID = Convert.ToInt64(objRS.Fields["OtherPartnerTransID"]);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objBinaryNode = null;

                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.Close();
                        objRS = null;
                    }
                }
            }
        }


        /// <summary>
        ///Stores estimate data in the partner database. 
        /// </summary>
        internal void StoreEstimateDataInPartnerDB()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StoreEstimateDataInPartnerDB: " });
            ADODB.Recordset objRS = null;
            try
            {
                objRS = new ADODB.Recordset();
                //Store the doc in the partner DB, returning results as a recordset.
                objRS = StoreDocumentInPartnerDb();
                if (objRS.EOF && objRS.BOF)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned), ",", PROC_NAME, ",", "No Records returned from partner DB for Estimate Data submission." }));

                mstrExistingEstimateDataXml = mobjLAPDPartner.g_objPassedDocument.OuterXml;
                mlngExistingEstimateDataID = mlngPartnerTransID;

                //If the document exists then clear the XML string pointers
                //so that nothing will be submitted to the APD database.
                if (objRS.Fields["DocumentExists"].Value == "true")
                {
                    mstrExistingPrintImageXml = "";
                    mstrExistingEstimateDataXml = "";

                    //COMMENTED OUT - This message no longer holds value.
                    //We get plenty of dupes, and they no longer represent an error case.
                    //Test Track 1782 - "Disable all error and warning emails that no longer hold relevance"

                    //Send internal informational message.
                    //g_objEvents.HandleEvent eDuplicateDocument, PROC_NAME, _
                    //    mlngExistingEstimateDataID, "", "", False
                }
                else
                {
                    if (objRS.Fields["OtherTransXML"] != null)
                    {
                        mstrExistingPrintImageXml = Convert.ToString(objRS.Fields["OtherTransXML"]);
                        mlngExistingPrintImageID = Convert.ToInt64(objRS.Fields["OtherPartnerTransID"]);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.Close();
                        objRS = null;
                    }
                }
            }
        }


        /// <summary>
        ///  Stores estimate data in the partner database.
        /// </summary>
        internal void StoreEstimateDataInPartnerDB_v2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StoreEstimateDataInPartnerDB_v2: " });

            ADODB.Recordset objRS = null;

            try
            {
                objRS = new ADODB.Recordset();

                //Store the doc in the partner DB, returning results as a recordset.
                objRS = StoreDocumentInPartnerDb_v2();

                if (objRS.EOF && objRS.BOF)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned), ",", PROC_NAME, ",", "No Records returned from partner DB for Estimate Data submission." }));
                mstrExistingEstimateDataXml = mobjPassedDocument.OuterXml;
                mlngExistingEstimateDataID = mlngPartnerTransID;

                //If the document exists then clear the XML string pointers
                //so that nothing will be submitted to the APD database.

                if (objRS.Fields["DocumentExists"].Value == "true")
                {
                    mstrExistingPrintImageXml = "";
                    mstrExistingEstimateDataXml = "";

                    //COMMENTED OUT - This message no longer holds value.
                    //We get plenty of dupes, and they no longer represent an error case.
                    //Test Track 1782 - "Disable all error and warning emails that no longer hold relevance"

                    //Send internal informational message.
                    //g_objEvents.HandleEvent eDuplicateDocument, PROC_NAME, _
                    //    mlngExistingEstimateDataID, "", "", False
                }
                else
                {
                    if (objRS.Fields["OtherTransXML"] != null)
                    {
                        mstrExistingPrintImageXml = Convert.ToString(objRS.Fields["OtherTransXML"]);
                        mlngExistingPrintImageID = Convert.ToInt64(objRS.Fields["OtherPartnerTransID"]);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.Close();
                        objRS = null;
                    }
                }
            }
        }


        /// <summary>
        /// Utility method used to wrap call to udb_partner
        /// </summary>
        internal ADODB.Recordset StoreDocumentInPartnerDb()
        {
            ADODB.Recordset objRS = null;
            ADODB.Recordset StoreDocumentInPartnerDb = null;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                objRS = new ADODB.Recordset();
                StoreDocumentInPartnerDb = new ADODB.Recordset();
                //Initialize the connect string for std output
                mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strPartnerConnStringStd);

                //Store the estimate data, getting the stored proc to call from the config file.
                objRS = mobjLAPDPartner.g_objDataAccessor.OpenRecordsetSp(mobjLAPDPartner.GetConfig("PartnerSettings/PartnerLoadSP"), new object[] {
                    mobjLAPDPartner.g_strAssignmentID, mobjLAPDPartner.g_strLynxID, mobjLAPDPartner.g_strVehicleNumber,
                    string.Concat(new string[] { "'", Convert.ToString(mintEstimateSequenceNumber), "'", ",", "'", 
                        Convert.ToString(mobjLAPDPartner.g_varTransactionDate), "'", ",", "'", mobjLAPDPartner.g_strPartnerSmallID,
                        "'", ",", "'", DocTypeStringForDatabase, "'", ",", "'", mobjLAPDPartner.g_objPassedDocument.OuterXml.Replace("'", "''"), "'" })});

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.DebugRS(ref objRS);

                mlngPartnerTransID = Convert.ToInt64(objRS.Fields["PartnerTransID"]);
                StoreDocumentInPartnerDb = objRS;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                        objRS.Close();
                }
                objRS = null;
            }
            return StoreDocumentInPartnerDb;
        }


        /// <summary>
        /// Utility method used to wrap call to udb_partner
        /// </summary>
        internal ADODB.Recordset StoreDocumentInPartnerDb_v2()
        {
            ADODB.Recordset RSStoreDocument = null;
            ADODB.Recordset objRS = null;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                RSStoreDocument = new ADODB.Recordset();
                objRS = new ADODB.Recordset();
                //Initialize the connect string for std output
                mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strPartnerConnStringStd);

                //Store the estimate data, getting the stored proc to call from the config file.
                objRS = mobjLAPDPartner.g_objDataAccessor.OpenRecordsetSp(mobjLAPDPartner.GetConfig("PartnerSettings/PartnerLoadSP"), new object[] { mstrAssignmentID, mstrLynxID, mstrVehicleNumber, string.Concat(new string[] { "'", Convert.ToString(mintEstimateSequenceNumber), "'", ",", "'", Convert.ToString(mvarTransactionDate), "'", ",", "'", mstrPartnerSmallID, "'", ",", "'", DocTypeStringForDatabase, "'", ",", "'", mobjPassedDocument.OuterXml.Replace("'", "''"), "'" }) });

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.DebugRS(ref objRS);

                mlngPartnerTransID = Convert.ToInt64(objRS.Fields["PartnerTransID"]);

                RSStoreDocument = objRS;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                        objRS.Close();
                }
                objRS = null;
            }

            return RSStoreDocument;
        }


        /// <summary>
        /// Utility method used to wrap call to udb_partner
        /// </summary>
        private void StoreCefInPartnerDb(long lngPartnerTransID, ref string strCEF)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StoreCefInPartnerDb: " });

            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                mobjMEscapeUtilities = new MEscapeUtilities();

                //Assert what we were passed.
                mobjLAPDPartner.g_objEvents.Assert(lngPartnerTransID != 0, string.Concat(new string[] { PROC_NAME, "Partner Trans ID was zero." }));
                mobjLAPDPartner.g_objEvents.Assert(!string.IsNullOrEmpty(strCEF), string.Concat(new string[] { PROC_NAME, "CEF string was blank." }));

                //Initialize the connect string for std output
                mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strPartnerConnStringStd);

                //Store the CEF data, getting the stored proc to call from the config file.
                mobjLAPDPartner.g_objDataAccessor.ExecuteSp(mobjLAPDPartner.GetConfig("PartnerSettings/CefUpdateSP"), new object[] { lngPartnerTransID, string.Concat(new string[] { "'", mobjMEscapeUtilities.SQLQueryString(ref strCEF), "'" }) });

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Utility method used to wrap call to udb_partner
        /// </summary>
        internal string RetrieveCefFromPartnerDb(long lngPartnerTransID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "RetrieveCefFromPartnerDb: " });

            string strRetrieveCef = null;
            ADODB.Recordset objRS = null;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                objRS = new ADODB.Recordset();
                //Assert what we were passed.
                mobjLAPDPartner.g_objEvents.Assert(lngPartnerTransID != 0, string.Concat(new string[] { PROC_NAME, "Partner Trans ID was zero." }));

                //Initialize the connect string for std output
                mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strPartnerConnStringStd);

                //Retrieve the CEF data, getting the stored proc to call from the config file.
                objRS = mobjLAPDPartner.g_objDataAccessor.OpenRecordsetSp(mobjLAPDPartner.GetConfig("PartnerSettings/CefRetrieveSP"), new object[] { lngPartnerTransID });

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.DebugRS(ref objRS);

                strRetrieveCef = Convert.ToString(objRS.Fields["CEFXML"]);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                        objRS.Close();
                }
                objRS = null;
            }
            return strRetrieveCef;
        }


        /// <summary>
        /// Extracts the Estimate Sequence Number from the passed XML and stores it
        /// in intEstimateSequenceNumber.
        /// The estimate Identifier comes in the form [E|S]#, the first character
        /// designating Estimate or Supplement, the second denoting the number.
        /// As a special case, the first estimate is represented as E01.
        /// We are primarily interested in the number, which we'll extract as is.
        /// In the special case, we change this to 0 for business logic reasons. 
        /// </summary>
        internal void ExtractSequenceNumber(string strIdentPath, [Optional]string strNumPath)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractSequenceNumber: " });

            string strNodeText = string.Empty;
            string strNodeTextLen = string.Empty;
            int intResult = 0;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                // Text value of node we're looking at
                strNodeText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, strNumPath, true);

                //Validate value returned
                if ((strNodeText.Length > 0) && ((Left(strNodeText, 1) == "E") || (Left(strNodeText, 1) == "S")))
                {
                    if (Left(strNodeText, 1) == "E")
                        mintEstimateSequenceNumber = 0;
                    else
                    {
                        if (strNumPath == "")
                        {
                            //strNodeTextLen = Right(strNodeText, strNodeText.Length - 1);
                            if (int.TryParse(Right(strNodeText, strNodeText.Length - 1), out intResult))
                                mintEstimateSequenceNumber = Convert.ToInt32(Right(strNodeText, strNodeText.Length - 1));
                            else
                                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Supplement Number value was non-numeric." }));
                        }
                        else
                        {
                            strNodeText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, strNumPath, true);

                            if (int.TryParse(Right(strNodeText, strNodeText.Length - 1), out intResult))
                                mintEstimateSequenceNumber = Convert.ToInt32(Right(strNodeText, strNodeText.Length - 1));
                            else
                                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Supplement Number value was non-numeric." }));
                        }
                    }
                }
                else
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Estimate Identifier began with neither 'S' nor 'E'." }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Extracts the Estimate Sequence Number from the passed XML and stores it
        /// in intEstimateSequenceNumber.
        /// The estimate Identifier comes in the form [E|S]#, the first character
        /// designating Estimate or Supplement, the second denoting the number.
        /// As a special case, the first estimate is represented as E01.
        /// We are primarily interested in the number, which we'll extract as is.
        /// In the special case, we change this to 0 for business logic reasons.
        /// </summary>
        internal void ExtractSequenceNumber_v2(string strIdentPath, [Optional]string strNumPath)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractSequenceNumber_v2: " });

            string strNodeText = string.Empty;
            string strNodeTextLen = string.Empty;
            int intResult = 0;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                strNodeText = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, strIdentPath, true);

                // Validate value returned
                if ((strNodeText.Length) > 0 && ((Left(strNodeText, 1) == "E") || (Left(strNodeText, 1) == "S")))
                {
                    // Value is valid, parse it
                    if (Left(strNodeText, 1) == "E")
                        mintEstimateSequenceNumber = 0;
                    else
                    {
                        if (strNumPath == "")
                        {
                            if (int.TryParse(Right(strNodeText, strNodeText.Length - 1), out intResult))
                                mintEstimateSequenceNumber = Convert.ToInt32(Right(strNodeText, strNodeText.Length - 1));
                            else
                                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Supplement Number value was non-numeric." }));
                        }
                        else
                        {
                            strNodeText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, strNumPath, true);

                            if (int.TryParse(Right(strNodeText, strNodeText.Length - 1), out intResult))
                                mintEstimateSequenceNumber = Convert.ToInt32(Right(strNodeText, strNodeText.Length - 1));
                            else
                                throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Supplement Number value was non-numeric." }));
                        }
                    }
                }
                else
                {
                    throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadSequenceNumber), ",", PROC_NAME, ",", "Estimate Identifier began with neither 'S' nor 'E'." }));
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Processes a receipt (acknowledgment).
        /// Sends workflow notifications and any required events. 
        /// </summary>
        //TODO
        internal void ProcessReceipt(string strErrorCodePath, string strErrorDescPath)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessReceipt: " });
            // Now decide if there was error or not in the outbound shop assingment
            // based on the error code contained in the inbound Xml we are playing with here.
            string strErrorCode = string.Empty;
            string strMonitor = string.Empty;
            string strErrorDesc = string.Empty;
            string strErrorKey = string.Empty;
            string strWorkFlow = string.Empty;
            int intErrorCode = 0;

            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                strErrorCode = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, strErrorCodePath);
                if (string.IsNullOrEmpty(strErrorCode))
                    strErrorCode = "0";
                //  This is to identify the partner's success code whether it is the string '0' or '00000'.
                // All our current success codes, whether decimal, hex, or string equate to 0.  If this conversion fails
                // the code is something other than 0 and therefore not a success.

                ////TODO
                //intErrorCode = Convert.ToInt32(strErrorCode);
                //if (Err().Number != 0)
                //{
                //    intErrorCode = 1;
                //    Err().Clear();
                //}

                //Receipt for cancellation.
                if (mobjLAPDPartner.g_strActionCode == "C")
                {
                    //Log that we do Nothing
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "Receipt for Cancellation received: Ignoring.");
                }
                //Receipt for successful assignment.
                else if (intErrorCode == 0)
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "Receipt for Assignment received: Sending Workflow Notification.");

                    SendWorkflowNotification("AssignmentReceiptReceivedFromVAN", string.Concat(new string[] { "SHOP (", mobjLAPDPartner.g_strPartner, "): Assignment for Vehicle ", Convert.ToString(mobjLAPDPartner.g_strVehicleNumber), " succeeded." }));
                    //Errors at Partner!
                }
                else
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "NEGATIVE Receipt for Assignment received: Sending Workflow Notification.");

                    strErrorDesc = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, strErrorDescPath);
                    //Allow unknown codes from partner.  An email will get sent from SiteUtilities and that
                    //is enough to allow us to get any new codes from partner.  Fall into the default task key.

                    strErrorKey = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']" }));
                    strWorkFlow = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']/@WorkFlow" }));
                    strMonitor = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']/@Monitor" }));

                    if (strErrorKey.Length == 0)
                    {
                        strErrorKey = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']" }));
                        strWorkFlow = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']/@WorkFlow" }));
                        strMonitor = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']/@Monitor" }));
                    }
                    SendWorkflowNotification(strWorkFlow, string.Concat(new string[] { "SHOP (", mobjLAPDPartner.g_strPartner, "): Assignment for Vehicle ", Convert.ToString(mobjLAPDPartner.g_strVehicleNumber), " FAILED!  ", strErrorDesc }), strErrorKey);

                    //Added the Monitor attribute to config.xml to allow selectivity of internal emails.
                    //Test Track 1782 - "Disable all error and warning emails that no longer hold relevance"
                    if (strMonitor == "True")
                        mobjLAPDPartner.g_objEvents.HandleEvent(Convert.ToInt32(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNackReceived), PROC_NAME, string.Concat(new string[] { strErrorCode, "|", strErrorDesc, "|", strErrorKey }), string.Concat(new string[] { "XML = ", mobjLAPDPartner.g_strPassedDocument }), "", false);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Processes a receipt (acknowledgment).
        /// Sends workflow notifications and any required events.
        /// </summary>
        //TODO
        internal void ProcessReceipt_v2(string strErrorCodePath, string strErrorDescPath)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessReceipt_v2: " });
            // Now decide if there was error or not in the outbound shop assingment
            // based on the error code contained in the inbound Xml we are playing with here.
            string strErrorCode = string.Empty;
            string strMonitor = string.Empty;
            string strErrorDesc = string.Empty;
            string strErrorKey = string.Empty;
            string strWorkFlow = string.Empty;
            int intErrorCode = 0;

            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                strErrorCode = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, strErrorCodePath);
                if (string.IsNullOrEmpty(strErrorCode))
                    strErrorCode = "0";
                //  This is to identify the partner's success code whether it is the string '0' or '00000'.
                // All our current success codes, whether decimal, hex, or string equate to 0.  If this conversion fails
                // the code is something other than 0 and therefore not a success.

                //TODO
                //intErrorCode = Convert.ToInt32(strErrorCode);
                //if (Err().Number != 0)
                //{
                //    intErrorCode = 1;
                //    Err().Clear();
                //}
                //Receipt for cancellation.
                if (mstrActionCode == "C")
                {
                    //Log that we do Nothing
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "Receipt for Cancellation received: Ignoring.");
                }
                //Receipt for successful assignment.
                else if (intErrorCode == 0)
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "Receipt for Assignment received: Sending Workflow Notification.");

                    SendWorkflowNotification_v2("AssignmentReceiptReceivedFromVAN", string.Concat(new string[] { "SHOP (", mstrPartner, "): Assignment for Vehicle ", Convert.ToString(mstrVehicleNumber), " succeeded." }));
                    //Errors at Partner!
                }
                else
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "NEGATIVE Receipt for Assignment received: Sending Workflow Notification.");

                    strErrorDesc = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, strErrorDescPath);
                    //Allow unknown codes from partner.  An email will get sent from SiteUtilities and that
                    //is enough to allow us to get any new codes from partner.  Fall into the default task key.

                    strErrorKey = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']" }));
                    strWorkFlow = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']/@WorkFlow" }));
                    strMonitor = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='", strErrorCode, "']/@Monitor" }));

                    if (strErrorKey.Length == 0)
                    {
                        strErrorKey = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']" }));
                        strWorkFlow = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']/@WorkFlow" }));
                        strMonitor = mobjLAPDPartner.GetConfig(string.Concat(new string[] { DocTypeConfig, "NACK[@Code='Default']/@Monitor" }));
                    }
                    SendWorkflowNotification_v2(strWorkFlow, string.Concat(new string[] { "SHOP (", mstrPartner, "): Assignment for Vehicle ", Convert.ToString(mstrVehicleNumber), " FAILED!  ", strErrorDesc, ",", strErrorKey }));

                    //Added the Monitor attribute to config.xml to allow selectivity of internal emails.
                    //Test Track 1782 - "Disable all error and warning emails that no longer hold relevance"
                    if (strMonitor == "True")
                        mobjLAPDPartner.g_objEvents.HandleEvent(Convert.ToInt32(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNackReceived), PROC_NAME, string.Concat(new string[] { strErrorCode, "|", strErrorDesc, "|", strErrorKey }), string.Concat(new string[] { "XML = ", mstrPassedDocument }), "", false);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Processes a receipt (acknowledgment).
        /// Sends workflow notifications and any required events.
        /// </summary>
        internal void ProcessEstimate(ref CPartner objPartner)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessEstimate: " });
            //Objects that need clean up.
            XmlDocument objEstimateDataDom = null;
            CPartnerDataXfer objDataXfer = null;
            //Primitives that don't
            long lngAPDDocumentID = 0;
            string strCEF = string.Empty;
            bool blnDAAssignment = false;
            string strConditionValue = string.Empty;
            bool blnProcessEstimateData = false;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                blnProcessEstimateData = (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "@ProcessEstimateData" })) == "False" ? false : true);

                //If we now have both the estimate data and the print image for this estimate,
                //submit them both into the APD database and make the appropriate workflow calls.

                if (((mstrExistingPrintImageXml.Length) != 0 && (mstrExistingEstimateDataXml.Length) != 0) || ((mstrExistingPrintImageXml.Length) != 0 && !blnProcessEstimateData))
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "Beginning processing of estimate");

                    //Inserts an estimate print image into the APD document system.
                    //Returns the APD document ID through lngAPDDocumentID.  Will be new if not passed in.
                    lngAPDDocumentID = objPartner.InsertPrintImageIntoAPD(mstrExistingPrintImageXml, mlngExistingPrintImageID);

                    if ((mstrExistingEstimateDataXml.Length) != 0 && blnProcessEstimateData)
                    {
                        //If the passed doc was the estimate data we can use it.
                        //Otherwise load up what we extracted from the partner database.
                        if (menmDocType == CPartner.EDocumentType.eEstimateData)
                            objEstimateDataDom = mobjLAPDPartner.g_objPassedDocument;
                        else
                        {
                            objEstimateDataDom = new XmlDocument();
                            mobjLAPDPartner.LoadXml(ref objEstimateDataDom, ref mstrExistingEstimateDataXml, PROC_NAME, "Existing Estimate");
                        }

                        objDataXfer = new CPartnerDataXfer();

                        //Handles the insertion of the estimate data in APD.
                        //Returns the transformed estimate in Common Estimate Format (CEF).
                        strCEF = objDataXfer.TransferDataToAPD(objEstimateDataDom, lngAPDDocumentID);
                        objDataXfer = null;

                        //Save the CEF back into the partner database.
                        StoreCefInPartnerDb(mlngExistingEstimateDataID, ref strCEF);
                    }

                    mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strLYNXConnStringStd);

                    blnDAAssignment = Convert.ToBoolean(mobjLAPDPartner.g_objDataAccessor.ExecuteSpNamedParams(mobjLAPDPartner.GetConfig("WorkFlow/DeskAuditAssignmentSP"), new object[] { "@AssignmentID", mobjLAPDPartner.g_strAssignmentID }));

                    strConditionValue = (blnDAAssignment ? string.Empty : "NONUSERSOURCE");

                    //Send a workflow event notification to the database.
                    SendWorkflowNotification("EstimateDataTransferredToAPD", string.Concat(new string[] { "SHOP (", mobjLAPDPartner.g_strPartner, "): Estimate data received for Vehicle ", mobjLAPDPartner.g_strVehicleNumber }), strConditionValue);
                    SendWorkflowNotification("EstimatePrintImageFileReceived", string.Concat(new string[] { "SHOP (", mobjLAPDPartner.g_strPartner, "): Estimate print image received for Vehicle ", mobjLAPDPartner.g_strVehicleNumber }));
                }
                else
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "We do not have both the PI and the ED yet - processing halted.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objEstimateDataDom = null;
                objDataXfer = null;
            }
        }


        /// <summary>
        /// Processes a receipt (acknowledgment).
        /// Sends workflow notifications and any required events.
        /// </summary>
        internal void ProcessEstimate_v2(ref CPartner objPartner)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessEstimate_v2: " });

            //Objects that need clean up.
            XmlDocument objEstimateDataDom = null;
            CPartnerDataXfer objDataXfer = null;

            //Primitives that don't
            long lngAPDDocumentID = 0;
            string strCEF = string.Empty;
            bool blnDAAssignment = false;
            string strConditionValue = string.Empty;
            bool blnProcessEstimateData = false;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                blnProcessEstimateData = (mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "@ProcessEstimateData" })) == "False" ? false : true);

                //If we now have both the estimate data and the print image for this estimate,
                //submit them both into the APD database and make the appropriate workflow calls.

                if (((mstrExistingPrintImageXml.Length) != 0 && (mstrExistingEstimateDataXml.Length) != 0) || ((mstrExistingPrintImageXml.Length) != 0 && !blnProcessEstimateData))
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "Beginning processing of estimate");

                    //Inserts an estimate print image into the APD document system.
                    //Returns the APD document ID through lngAPDDocumentID.  Will be new if not passed in.
                    lngAPDDocumentID = objPartner.InsertPrintImageIntoAPDV2(mstrExistingPrintImageXml, mlngExistingPrintImageID);

                    if ((mstrExistingEstimateDataXml.Length) != 0 && blnProcessEstimateData)
                    {
                        //If the passed doc was the estimate data we can use it.
                        //Otherwise load up what we extracted from the partner database.
                        if (menmDocType == CPartner.EDocumentType.eEstimateData)
                            objEstimateDataDom = mobjLAPDPartner.g_objPassedDocument;
                        else
                        {
                            objEstimateDataDom = new XmlDocument();
                            mobjLAPDPartner.LoadXml(ref objEstimateDataDom, ref mstrExistingEstimateDataXml, PROC_NAME, "Existing Estimate");
                        }

                        objDataXfer = new CPartnerDataXfer();

                        //Handles the insertion of the estimate data in APD.
                        //Returns the transformed estimate in Common Estimate Format (CEF).
                        strCEF = objDataXfer.TransferDataToAPD_v2(objEstimateDataDom, lngAPDDocumentID, mstrPassedDocument, mstrPartnerSmallID);
                        objDataXfer = null;

                        //Save the CEF back into the partner database.
                        StoreCefInPartnerDb(mlngExistingEstimateDataID, ref strCEF);
                    }

                    mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strLYNXConnStringStd);

                    blnDAAssignment = Convert.ToBoolean(mobjLAPDPartner.g_objDataAccessor.ExecuteSpNamedParams(mobjLAPDPartner.GetConfig("WorkFlow/DeskAuditAssignmentSP"), new object[] { "@AssignmentID", mstrAssignmentID }));

                    strConditionValue = (blnDAAssignment ? string.Empty : "NONUSERSOURCE");

                    //Send a workflow event notification to the database.
                    SendWorkflowNotification_v2("EstimateDataTransferredToAPD", string.Concat(new string[] { "SHOP (", mstrPartner, "): Estimate data received for Vehicle ", mstrVehicleNumber }), strConditionValue);
                    SendWorkflowNotification_v2("EstimatePrintImageFileReceived", string.Concat(new string[] { "SHOP (", mstrPartner, "): Estimate print image received for Vehicle ", mstrVehicleNumber }));
                }
                else
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", "We do not have both the PI and the ED yet - processing halted.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objEstimateDataDom = null;
                objDataXfer = null;
            }
        }


        /// <summary>
        /// Decodes digital images and adds them to the document system one at a time.
        /// Note that it does not deal with Thumbnails even though the document contains
        /// them.  We generate thumbnails on the fly in the APD application.
        /// Returns the APD Document ID of the added document.
        /// </summary>
        internal long ProcessBinaryFiles(ref XmlDocument objDom, string strNodeNames, string strCreateDateNodeNames, string strCreateTimeNodeNames, string strFileExtension, string strDocType, EDocumentType enmDocType)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessBinaryFiles: " });

            XmlNodeList objNodeList = null;
            XmlNodeList objCreateDateNodeList = null;
            XmlNodeList objCreateTimeNodeList = null;
            XmlElement objTempElement = null;
            object objTempArrayObject = null;

            int intImageCount = 0;
            long lngProcessBinaryFiles = 0;
            string strDate = string.Empty; // Holds a date and time used to make filenames unique
            string strFileName = string.Empty;
            string strCreatedDate = string.Empty;
            byte[] arrBuffer = null;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                mobjBase64utils = new MBase64Utils();

                //Add a trace message to note start of method.
                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace("", "Processing binary file(s).");

                //Build the current date for dissection by the app.
                strDate = mobjLAPDPartner.BuildFileNameDate();

                // Create the object collection containing the encoded pics
                objNodeList = objDom.SelectNodes(strNodeNames);

                if (!string.IsNullOrEmpty(strCreateDateNodeNames))
                    objCreateDateNodeList = objDom.SelectNodes(strCreateDateNodeNames);

                if (!string.IsNullOrEmpty(strCreateTimeNodeNames))
                    objCreateTimeNodeList = objDom.SelectNodes(strCreateTimeNodeNames);


                //Loop through each image and store them in separate files.
                for (intImageCount = 0; intImageCount <= objNodeList.Count - 1; intImageCount++)
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Document ", Convert.ToString(intImageCount) }));

                    //Create a unique file name for the document.\
                    strFileName = BuildFinalFileName(intImageCount, strDate, strDocType);
                    strFileName = (mobjLAPDPartner.GetConfig(string.Concat(new string[] { ("Document/RootDirectory"), strFileName })).Replace("\\\\", "\\"));


                    //Create binary data from the Base64 encoded info.
                    objTempElement = (XmlElement)objNodeList.Item(intImageCount);
                    objTempArrayObject = mobjBase64utils.ReadBinaryDataFromDomElement(objTempElement);

                    if (objTempArrayObject != null)
                    {
                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                        binaryFormatter.Serialize(memoryStream, objTempArrayObject);
                        arrBuffer = memoryStream.ToArray();
                    }


                    //Build created date in format "yyyy-mm-ddThh:mm:ss"
                    if (((objCreateDateNodeList != null)) && ((objCreateTimeNodeList != null)))
                    {
                        if ((intImageCount < objCreateDateNodeList.Count) && (intImageCount < objCreateTimeNodeList.Count))
                            strCreatedDate = mobjLAPDPartner.ConstructSqlDateTime(objCreateDateNodeList.Item(intImageCount).InnerText, objCreateTimeNodeList.Item(intImageCount).InnerText);
                    }

                    //Write the binary data to a file in the document structure.
                    mobjBase64utils.WriteBinaryDataToFile(strFileName, arrBuffer);

                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "File Name = ", strFileName, ",", PROC_NAME, "Document Created" }));

                    //Insert new document record into APD database
                    lngProcessBinaryFiles = InsertLAPDDocument(ref mobjLAPDPartner.g_objDataAccessor, enmDocType, Convert.ToInt64(mobjLAPDPartner.g_strAssignmentID), Convert.ToInt64(mobjLAPDPartner.g_strLynxID), Convert.ToInt32(mobjLAPDPartner.g_strVehicleNumber), Convert.ToInt32(mintEstimateSequenceNumber), BuildPartnerTransID(), BuildFinalFileName(intImageCount, strDate, strDocType), strFileExtension, strCreatedDate,
                   false);

                    objTempElement = null;
                    objTempArrayObject = null;
                }

                //Add another trace message to note exit
                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "", ",", PROC_NAME, "Finished!" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objNodeList = null;
                objCreateDateNodeList = null;
                objCreateTimeNodeList = null;
            }
            return lngProcessBinaryFiles;
        }


        /// <summary>
        /// Decodes digital images and adds them to the document system one at a time.
        /// Note that it does not deal with Thumbnails even though the document contains
        /// them.  We generate thumbnails on the fly in the APD application.
        /// Returns the APD Document ID of the added document. 
        /// </summary>
        internal long ProcessBinaryFiles_v2(ref XmlDocument objDom, string strNodeNames, string strCreateDateNodeNames, string strCreateTimeNodeNames, string strFileExtension, string strDocType, EDocumentType enmDocType)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessBinaryFiles_v2: " });

            XmlNodeList objNodeList = null;
            XmlNodeList objCreateDateNodeList = null;
            XmlNodeList objCreateTimeNodeList = null;
            FileSystemObject objFSO = null;
            XmlElement objTempElement = null;
            object objTempArrayObject = null;

            int intImageCount = 0;
            long lngProcessBinaryFiles = 0;
            string strDate = string.Empty; // Holds a date and time used to make filenames unique
            string strDocumentRoot = string.Empty;
            string strFileName = string.Empty;
            string strCreatedDate = string.Empty;
            byte[] arrBuffer = null;

            //check if the recommended file is unique. else find a unique file
            string strOutFilePath = string.Empty;
            string strOutFileName = string.Empty;
            string strOutFileExt = string.Empty;
            string strOutFileDBPath = string.Empty;
            int intDupCount = 0;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                mobjBase64utils = new MBase64Utils();

                //Add a trace message to note start of method.
                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace("", "Processing binary file(s).");

                //Build the current date for dissection by the app.
                strDate = mobjLAPDPartner.BuildFileNameDate();

                // Create the object collection containing the encoded pics
                objNodeList = objDom.SelectNodes(strNodeNames);

                if (!string.IsNullOrEmpty(strCreateDateNodeNames))
                    objCreateDateNodeList = objDom.SelectNodes(strCreateDateNodeNames);

                if (!string.IsNullOrEmpty(strCreateTimeNodeNames))
                    objCreateTimeNodeList = objDom.SelectNodes(strCreateTimeNodeNames);


                //Loop through each image and store them in separate files.
                for (intImageCount = 0; intImageCount <= objNodeList.Count - 1; intImageCount++)
                {
                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Document ", Convert.ToString(intImageCount) }));

                    //Create a unique file name for the document.\
                    strFileName = BuildFinalFileName_v2(intImageCount, strDate, strDocType);
                    strDocumentRoot = mobjLAPDPartner.GetConfig("Document/RootDirectory");
                    strFileName = (mobjLAPDPartner.GetConfig(string.Concat(new string[] { strDocumentRoot, strFileName })).Replace("\\\\", "\\"));

                    //Create binary data from the Base64 encoded info.
                    objTempElement = (XmlElement)objNodeList.Item(intImageCount);
                    objTempArrayObject = mobjBase64utils.ReadBinaryDataFromDomElement(objTempElement);

                    if (objTempArrayObject != null)
                    {
                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
                        binaryFormatter.Serialize(memoryStream, objTempArrayObject);
                        arrBuffer = memoryStream.ToArray();
                    }

                    //Build created date in format "yyyy-mm-ddThh:mm:ss"
                    if (((objCreateDateNodeList != null)) && ((objCreateTimeNodeList != null)))
                    {
                        if ((intImageCount < objCreateDateNodeList.Count) && (intImageCount < objCreateTimeNodeList.Count))
                            strCreatedDate = mobjLAPDPartner.ConstructSqlDateTime(objCreateDateNodeList.Item(intImageCount).InnerText, objCreateTimeNodeList.Item(intImageCount).InnerText);
                    }

                    objFSO = new FileSystemObject();

                    strOutFilePath = objFSO.GetParentFolderName(strFileName);
                    strOutFileName = objFSO.GetBaseName(strFileName);
                    strOutFileExt = objFSO.GetExtensionName(strFileName);
                    intDupCount = 1;
                    while (objFSO.FileExists(strFileName) == true)
                    {
                        strFileName = objFSO.BuildPath(strOutFilePath, string.Concat(new string[] { strOutFileName, "_", Convert.ToString(intDupCount), ".", strOutFileExt }));
                        intDupCount = intDupCount + 1;
                    }
                    objFSO = null;
                    strOutFileDBPath = Mid(strFileName, (strDocumentRoot.Replace("\\\\", "\\")).Length + 1);
                    strOutFileDBPath = strOutFileDBPath.Replace("\\", "\\\\");

                    //Write the binary data to a file in the document structure.
                    mobjBase64utils.WriteBinaryDataToFile(strFileName, arrBuffer);

                    if (mobjLAPDPartner.g_blnDebugMode)
                        mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "File Name = ", strFileName, ",", PROC_NAME, "Document Created" }));

                    //Insert new document record into APD database
                    lngProcessBinaryFiles = InsertLAPDDocument_v2(ref mobjLAPDPartner.g_objDataAccessor, enmDocType, Convert.ToInt64(mstrAssignmentID), Convert.ToInt64(mstrLynxID), Convert.ToInt32(mstrVehicleNumber), Convert.ToInt32(mintEstimateSequenceNumber), BuildPartnerTransID(), strOutFileDBPath, strFileExtension, strCreatedDate, false);

                    objTempElement = null;
                    objTempArrayObject = null;
                }

                //Add another trace message to note exit
                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "", ",", PROC_NAME, "Finished!" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objNodeList = null;
                objCreateDateNodeList = null;
                objCreateTimeNodeList = null;
                objFSO = null;
            }
            return lngProcessBinaryFiles;
        }


        /// <summary>
        /// Send a receipt back to Partner.
        /// </summary>
        internal void SendAReceipt(string strTemplateFile, string strSuccess = "Y", long lngErrorCode = 0, string strErrorDesc = "")
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendAReceipt: " });
            XmlDocument objLYNXReceipt = null;
            XmlNode objNode = null;

            string strWrappedReceipt = string.Empty;
            string strPost2URL = string.Empty;
            string strTransactionID = string.Empty;
            string strPath = string.Empty;
            try
            {
                mobjMDomUtils = new MDomUtils();
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                mobjMPPGTransactions = new MPPGTransactions();

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Preparing receipt" }));

                //First load the receipt template from disk.
                objLYNXReceipt = new XmlDocument();

                strPath = string.Concat(new string[] { mobjLAPDPartner.g_strSupportDocPath, "\\", strTemplateFile });
                mobjMDomUtils.LoadXmlFile(ref objLYNXReceipt, ref strPath, "SendAReceipt", "Receipt");

                //temporary variable.
                strPath = string.Empty;

                strTransactionID = (mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@TransactionID").Trim());
                if (strTransactionID.Length == 0)
                    strTransactionID = (mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//SenderTransactionId").Trim());

                //Fill out the template with values from the submitted document.
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@TransactionID", true).InnerText = string.Concat(new string[] { "LR-", strTransactionID });
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@TransactionDate", true).InnerText = mobjLAPDPartner.ConstructXmlDate(Convert.ToString(DateTime.Now));
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@TransactionTime", true).InnerText = mobjLAPDPartner.ConstructXmlTime(Convert.ToString(DateTime.Now));
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@AssignmentID", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@AssignmentID");
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@LynxID", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@LynxID");
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@VehicleID", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@VehicleID");

                //TaskID field is optional (specific to SG).
                objNode = mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@TaskID");
                if ((objNode != null))
                    objNode.InnerText = mobjLAPDPartner.g_strTaskID;

                //ClaimNumber field is optional (specific to Grange).
                objNode = mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//ClaimNumber");
                if ((objNode != null))
                    objNode.InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//ClaimNumber", false);

                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//ReceiptFor", true).InnerText = strTransactionID;
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//OriginalTransactionDateTime/DateTime/@Date", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@TransactionDate");
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//OriginalTransactionDateTime/DateTime/@Time", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@TransactionTime");
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//SuccessFlag", true).InnerText = strSuccess;
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//FailureReasonCode", true).InnerText = Convert.ToString(lngErrorCode);
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//FailureReasonText", true).InnerText = strErrorDesc;

                //Build the SOAP Envelope and put the receipt into the envelope
                strWrappedReceipt = mobjMPPGTransactions.BuildTransactionForPPG("Receipt", objLYNXReceipt.OuterXml, mobjLAPDPartner.g_strPartner, string.Concat(new string[] { mobjLAPDPartner.g_strLynxID, "-", mobjLAPDPartner.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjLAPDPartner.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedReceipt);
                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL, PROC_NAME, "Receipt sent via HTTP" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objNode = null;
                objLYNXReceipt = null;
            }
        }


        /// <summary>
        /// Send a receipt back to Partner.
        /// </summary>
        internal void SendAReceipt_v2(string strTemplateFile, string strSuccess = "Y", long lngErrorCode = 0, string strErrorDesc = "")
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendAReceipt_v2: " });

            XmlDocument objLYNXReceipt = null;
            XmlNode objNode = null;

            string strWrappedReceipt = string.Empty;
            string strPost2URL = string.Empty;
            string strTransactionID = string.Empty;
            string strPath = string.Empty;
            try
            {
                mobjMDomUtils = new MDomUtils();
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                mobjMPPGTransactions = new MPPGTransactions();

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Preparing receipt" }));

                //First load the receipt template from disk.
                objLYNXReceipt = new XmlDocument();
                strPath = string.Concat(new string[] { mobjLAPDPartner.g_strSupportDocPath, "\\", strTemplateFile });
                mobjMDomUtils.LoadXmlFile(ref objLYNXReceipt, ref strPath, "SendAReceipt", "Receipt");

                //temporary variable.
                strPath = string.Empty;

                strTransactionID = (mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@TransactionID").Trim());
                if (strTransactionID.Length == 0)
                    strTransactionID = (mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//SenderTransactionId").Trim());

                //Fill out the template with values from the submitted document.
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@TransactionID", true).InnerText = string.Concat(new string[] { "LR-", strTransactionID });
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@TransactionDate", true).InnerText = mobjLAPDPartner.ConstructXmlDate(Convert.ToString(DateTime.Now));
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@TransactionTime", true).InnerText = mobjLAPDPartner.ConstructXmlTime(Convert.ToString(DateTime.Now));
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@AssignmentID", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@AssignmentID");
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@LynxID", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@LynxID");
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@VehicleID", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@VehicleID");

                //TaskID field is optional (specific to SG).
                objNode = mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//@TaskID");
                if ((objNode != null))
                    objNode.InnerText = mobjLAPDPartner.g_strTaskID;

                //ClaimNumber field is optional (specific to Grange).
                objNode = mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//ClaimNumber");
                if ((objNode != null))
                    objNode.InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//ClaimNumber", false);

                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//ReceiptFor", true).InnerText = strTransactionID;
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//OriginalTransactionDateTime/DateTime/@Date", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@TransactionDate");
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//OriginalTransactionDateTime/DateTime/@Time", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@TransactionTime");
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//SuccessFlag", true).InnerText = strSuccess;
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//FailureReasonCode", true).InnerText = Convert.ToString(lngErrorCode);
                mobjLAPDPartner.GetChildNode(ref objLYNXReceipt, "//FailureReasonText", true).InnerText = strErrorDesc;

                //Build the SOAP Envelope and put the receipt into the envelope
                strWrappedReceipt = mobjMPPGTransactions.BuildTransactionForPPG("Receipt", objLYNXReceipt.OuterXml, mstrPartner, string.Concat(new string[] { mstrLynxID, "-", mstrVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjLAPDPartner.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedReceipt);
                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL, PROC_NAME, "Receipt sent via HTTP" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objNode = null;
                objLYNXReceipt = null;
            }
        }


        /// <summary>
        /// Send a HeartBeat back to Partner.
        /// </summary>
        internal void SendHeartBeatResponse(string strTemplateFile)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendHeartBeatResponse: " });

            //Object vars that need clean up.
            XmlDocument objHeartBeatDom = null;

            //Primitive vars that dont need clean up.
            string strWrappedHeartBeat = string.Empty;
            string strPost2URL = string.Empty;
            string strPath = string.Empty;
            try
            {
                mobjMDomUtils = new MDomUtils();
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                mobjMPPGTransactions = new MPPGTransactions();

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Preparing HeartBeat" }));

                //First load the HeartBeat template from disk.
                objHeartBeatDom = new XmlDocument();
                strPath = string.Concat(new string[] { mobjLAPDPartner.g_strSupportDocPath, "\\", strTemplateFile });
                mobjMDomUtils.LoadXmlFile(ref objHeartBeatDom, ref strPath, "SendAHeartBeat", "HeartBeat");

                //temporary variable.
                strPath = string.Empty;

                //Fill out the template with values from the submitted document.
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//@TransactionID", true).InnerText = string.Concat(new string[] { "LR-", mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@TransactionID") });
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//@TransactionDate", true).InnerText = mobjLAPDPartner.ConstructXmlDate(Convert.ToString(DateTime.Now));
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//@TransactionTime", true).InnerText = mobjLAPDPartner.ConstructXmlTime(Convert.ToString(DateTime.Now));
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//@AssignmentID", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@AssignmentID");

                //Copy the origination date/time
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//OriginationDate/DateTime/@Date", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//OriginationDate/DateTime/@Date");
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//OriginationDate/DateTime/@Time", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//OriginationDate/DateTime/@Time");
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//OriginationDate/DateTime/@TimeZone", true).InnerText = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//OriginationDate/DateTime/@TimeZone");

                //Set the received date/time
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//ReceivedDate/DateTime/@Date", true).InnerText = mobjLAPDPartner.ConstructXmlDate(Convert.ToString(mobjLAPDPartner.g_varDocumentReceivedDate));
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//ReceivedDate/DateTime/@Time", true).InnerText = mobjLAPDPartner.ConstructXmlTime(Convert.ToString(mobjLAPDPartner.g_varDocumentReceivedDate));

                //Set the reply date/time
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//ReplyDate/DateTime/@Date", true).InnerText = mobjLAPDPartner.ConstructXmlDate(Convert.ToString(mobjLAPDPartner.g_varDocumentReceivedDate));
                mobjLAPDPartner.GetChildNode(ref objHeartBeatDom, "//ReplyDate/DateTime/@Time", true).InnerText = mobjLAPDPartner.ConstructXmlTime(Convert.ToString(mobjLAPDPartner.g_varDocumentReceivedDate));

                //Build the SOAP Envelope and put the HeartBeat into the envelope
                strWrappedHeartBeat = mobjMPPGTransactions.BuildTransactionForPPG("NetworkStatusTest", objHeartBeatDom.OuterXml, mobjLAPDPartner.g_strPartner, string.Concat(new string[] { mobjLAPDPartner.g_strLynxID, "-", mobjLAPDPartner.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjLAPDPartner.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedHeartBeat);

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL, ",", PROC_NAME, "HeartBeat sent via HTTP" }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objHeartBeatDom = null;
            }
        }


        /// <summary>
        /// Extracts the LynxID and Vehicle Number from the Lynx specified reference ID.
        /// </summary>
        internal void ExtractLynxIDAndVehNum()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractLynxIDAndVehNum: " });

            string strDate = string.Empty;
            string strTime = string.Empty;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                mstrActualLynxID = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@LynxID");
                mobjLAPDPartner.g_strActualLynxID = mstrActualLynxID;      //GetChildNodeText(g_objPassedDocument, "//@LynxID")

                mstrLynxID = mstrActualLynxID;
                mobjLAPDPartner.g_strLynxID = mstrLynxID;                  // g_strActualLynxID

                mstrAssignmentID = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@AssignmentID");
                mobjLAPDPartner.g_strAssignmentID = mstrAssignmentID;      //GetChildNodeText(g_objPassedDocument, "//@AssignmentID")

                mstrTaskID = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@TaskID");
                mobjLAPDPartner.g_strTaskID = mstrTaskID;                  // GetChildNodeText(g_objPassedDocument, "//@TaskID")

                mstrVehicleNumber = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@VehicleID");
                mobjLAPDPartner.g_strVehicleNumber = mstrVehicleNumber;    //GetChildNodeText(g_objPassedDocument, "//@VehicleID")

                mstrActionCode = Left(mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//AssignmentCode"), 1);
                mobjLAPDPartner.g_strActionCode = mstrActionCode;          // Left(GetChildNodeText(g_objPassedDocument, "//AssignmentCode"), 1)

                //Special case for ADP - Glaxis was missing the pass through data.
                //In these cases Glaxis sets the LynxID and VehicleNumber to "UNK"
                //and the AssignmentID to "0".  See Test Track #1785.
                //If g_strVehicleNumber <> "UNK" Then
                if (mstrVehicleNumber != "UNK")
                {
                    mstrVehicleNumber = mobjLAPDPartner.ZeroPad((string.IsNullOrEmpty(mobjLAPDPartner.g_strVehicleNumber) ? 0 : Convert.ToInt32(mobjLAPDPartner.g_strVehicleNumber)), 3);
                    mobjLAPDPartner.g_strVehicleNumber = mstrVehicleNumber; //ZeroPad(IIf(g_strVehicleNumber = "", 0, g_strVehicleNumber), 3)
                }
                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "LynxID=", mstrLynxID, " VehNum=", mstrVehicleNumber, " AssignmentID=", mstrAssignmentID, ",", PROC_NAME, mstrActualLynxID }));

                //Extract the assignment / cancel character.
                //If g_strActionCode <> "C" Then
                if (mstrActionCode != "C")
                {
                    mstrActionCode = "N";
                    mobjLAPDPartner.g_strActionCode = mstrActionCode;         //"N"
                }

                //Extract the transaction sent date/time.
                if ((mobjLAPDPartner.GetChildNode(ref mobjLAPDPartner.g_objPassedDocument, "//@TransactionDate") == null) || (mobjLAPDPartner.GetChildNode(ref mobjLAPDPartner.g_objPassedDocument, "//@TransactionTime") == null))
                {
                    strDate = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//TransactionDate", true);
                    strTime = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//TransactionTime", true);
                }
                else
                {
                    strDate = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@TransactionDate", false);
                    strTime = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, "//@TransactionTime", false);
                }
                mvarTransactionDate = string.Concat(new string[] { Left(strDate, 2), "/", Mid(strDate, 3, 2), "/", Right(strDate, 4), " ", Left(strTime, 2), ":", Mid(strTime, 3, 2), ":", Right(strTime, 2) });
                mobjLAPDPartner.g_varTransactionDate = mvarTransactionDate;
                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "TransactionDate = ", Convert.ToString(mvarTransactionDate) }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Extracts the LynxID and Vehicle Number from the Lynx specified reference ID.
        /// </summary>
        internal void ExtractLynxIDAndVehNum_v2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ExtractLynxIDAndVehNum_v2: " });

            string strDate = string.Empty;
            string strTime = string.Empty;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                mstrActualLynxID = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@LynxID");
                mstrLynxID = mstrActualLynxID;

                mstrAssignmentID = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@AssignmentID");
                mstrTaskID = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@TaskID");
                mstrVehicleNumber = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@VehicleID");
                mstrActionCode = Left(mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//AssignmentCode"), 1);

                //Special case for ADP - Glaxis was missing the pass through data.
                //In these cases Glaxis sets the LynxID and VehicleNumber to "UNK"
                //and the AssignmentID to "0".  See Test Track #1785.
                if (mstrVehicleNumber != "UNK")
                    mstrVehicleNumber = mobjLAPDPartner.ZeroPad((string.IsNullOrEmpty(mstrVehicleNumber) ? 0 : Convert.ToInt32(mstrVehicleNumber)), 3);

                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "LynxID=", mstrLynxID, " VehNum=", mstrVehicleNumber, " AssignmentID=", mstrAssignmentID, ",", PROC_NAME, mstrActualLynxID }));

                //Extract the assignment / cancel character.
                if (mstrActionCode != "C")
                    mstrActionCode = "N";

                //Extract the transaction sent date/time.
                if ((mobjLAPDPartner.GetChildNode(ref mobjLAPDPartner.g_objPassedDocument, "//@TransactionDate") == null) || (mobjLAPDPartner.GetChildNode(ref mobjPassedDocument, "//@TransactionTime") == null))
                {
                    strDate = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//TransactionDate", true);
                    strTime = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//TransactionTime", true);
                }
                else
                {
                    strDate = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@TransactionDate", false);
                    strTime = mobjLAPDPartner.GetChildNodeText(ref mobjPassedDocument, "//@TransactionTime", false);
                }
                mvarTransactionDate = string.Concat(new string[] { Left(strDate, 2), "/", Mid(strDate, 3, 2), "/", Right(strDate, 4), " ", Left(strTime, 2), ":", Mid(strTime, 3, 2), ":", Right(strTime, 2) });
                mobjLAPDPartner.g_varTransactionDate = mvarTransactionDate;
                if (mobjLAPDPartner.g_blnDebugMode)
                    mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { "TransactionDate = ", Convert.ToString(mvarTransactionDate) }));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Builds a friendly description string for the database.
        /// </summary>
        internal string BuildPartnerTransID()
        {
            string strBuildPartnerTransID = string.Empty;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                if (menmDocType == CPartner.EDocumentType.eEstimateData || menmDocType == CPartner.EDocumentType.ePrintImage)
                    strBuildPartnerTransID = string.Concat(new string[] { mobjLAPDPartner.ZeroPad(mlngExistingPrintImageID, 3), ";ED-ID:", mobjLAPDPartner.ZeroPad(mlngExistingEstimateDataID, 3) });
                else
                    strBuildPartnerTransID = string.Concat(new string[] { mobjLAPDPartner.ZeroPad(mlngPartnerTransID, 3), ";ED-ID:000" });
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strBuildPartnerTransID;
        }


        //Conducts a shop search based upon the passed document search parameters
        // strSearchElemPath is the path to the search element that contains the
        //shop search parameters.
        internal string ProcessShopSearchRequest(string strSearchElemPath, string strSourceCodePath = "//@Originator", string strZipCodePath = "//Zip", string strAreaCodePath = "//Area", string strExchangePath = "//Exchange")
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessShopSearchRequest: " });
            //Object vars that need clean up.
            XmlElement objSearchElem = null;

            //Primitive vars that dont need clean up.
            string strShopListXml = string.Empty;
            string strSourceCode = string.Empty;
            int intCompanyID = 0;
            string strAreaCode = string.Empty;
            string strExchange = string.Empty;
            string strZipCode = string.Empty;
            string strShopSearchRequest = string.Empty;
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                //Get the carrier specific company code.
                strSourceCode = mobjLAPDPartner.GetChildNodeText(ref mobjLAPDPartner.g_objPassedDocument, strSourceCodePath, true);

                //Locate the shop search parameter element.
                objSearchElem = (XmlElement)mobjLAPDPartner.GetChildNode(ref mobjLAPDPartner.g_objPassedDocument, strSearchElemPath, true);

                //Extract some parameters for the shop search.

                strAreaCode = objSearchElem.SelectSingleNode(strAreaCodePath).InnerText;
                strExchange = objSearchElem.SelectSingleNode(strExchangePath).InnerText;
                strZipCode = objSearchElem.SelectSingleNode(strZipCodePath).InnerText;

                //Translate the partner company source code to the APD company id.
                intCompanyID = Convert.ToInt32(mobjLAPDPartner.GetConfig(string.Concat(new string[] { mstrPartnerConfig, "Carrier[@SourceCode='", strSourceCode, "']/@CompanyID" })));

                //Set the connect string to APD for XML retrieval.
                mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strLYNXConnStringXML);

                //Zip code only search.
                if (!string.IsNullOrEmpty(strZipCode))
                    strShopListXml = mobjLAPDPartner.g_objDataAccessor.ExecuteSpNamedParamsXML(mobjLAPDPartner.GetConfig("PartnerSettings/ShopSearchSP"), new object[] { "@InsuranceCompanyID", intCompanyID, "@ShopTypeCode", "P", "@Zip", strZipCode });

                //Area code and exchange search.
                else if (!string.IsNullOrEmpty(strAreaCode) && !string.IsNullOrEmpty(strExchange))
                    strShopListXml = mobjLAPDPartner.g_objDataAccessor.ExecuteSpNamedParamsXML(mobjLAPDPartner.GetConfig("PartnerSettings/ShopSearchSP"), new object[] { "@InsuranceCompanyID", intCompanyID, "@ShopTypeCode", "P", "@AreaCode", strAreaCode, "@ExchangeNumber", strExchange });

                    //Not enough information to do the search - Kaboom!
                else
                    throw new Exception(string.Concat(new string[] { Convert.ToString(CPartner_Error_Codes.eShopSearchMissingParams), PROC_NAME, "Shop Search XML missing required parameters." }));

                //Return the results.
                strShopSearchRequest = strShopListXml;
            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objSearchElem = null;
            }
            return strShopSearchRequest;
        }


        /// <summary>
        ///  Converts a byte array buffer containing the binary data for an EMS DBF package file
        /// (zip file containing files for EMS) and returns the data in XML format.
        /// </summary>
        internal XmlDocument ConvertEmsDBFToXml(ref byte[] buffer)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ConvertEmsDBFToXml: " });

            XmlDocument mobjXmlDoc = null;
            FileSystemObject mobjFileSystemObject = null;
            File objFile = null;
            Folder objFolder = null;
            CUnZip objUnzip = null;
            Guid GenerateGUID = default(Guid);
            XmlDocument ConvertEmsDBFToXml = null;

            string strWorkRootPath = string.Empty;
            string strGUID = string.Empty;
            string strWorkPath = string.Empty;
            string strPackageFile = string.Empty;
            string strFileType = string.Empty;
            try
            {
                mobjXmlDoc = new XmlDocument();
                mobjFileSystemObject = new FileSystemObject();
                objUnzip = new CUnZip();
                mobjLAPDPartner = new MLAPDPartnerDataMgr();
                mobjBase64utils = new MBase64Utils();

                mobjLAPDPartner.g_objEvents.Trace(string.Concat(new string[] { PROC_NAME, "executed." }));
                strWorkRootPath = mobjLAPDPartner.g_objEvents.mSettings.GetParsedSetting("PartnerSettings/TempWorkSpace");
                mobjLAPDPartner.g_objEvents.Assert(!string.IsNullOrEmpty(strWorkRootPath), "Temp Work Space specified.");
                mobjLAPDPartner.g_objEvents.Assert(mobjFileSystemObject.FolderExists(strWorkRootPath), "Temp Work Space specified does not exist.");

                // Generate the temporary work folder for this transaction
                GenerateGUID = Guid.NewGuid();
                strWorkPath = mobjFileSystemObject.BuildPath(strWorkRootPath, GenerateGUID.ToString());
                strPackageFile = mobjFileSystemObject.BuildPath(strWorkPath, "\\ems.zip");
                // First we need to write the buffer out to a file.
                mobjFileSystemObject.CreateFolder(strWorkPath);
                mobjBase64utils.WriteBinaryDataToFile(strPackageFile, buffer);

                //Extract the individual files from package
                objUnzip.ZipFile = strPackageFile;
                objUnzip.UnzipFolder = strWorkPath;
                objUnzip.Unzip();

                objUnzip = null;
                mobjFileSystemObject.DeleteFile(strPackageFile, true);

                // Now cycle through the files and import each file's information into XML document
                objFolder = mobjFileSystemObject.GetFolder(strWorkPath);
                mobjXmlDoc.LoadXml("<EMS/>");
                foreach (File varobjFile in objFolder.Files)
                {
                    objFile = varobjFile;
                    strFileType = (Right(objFile.ShortName, 3)).ToUpper();
                    switch (strFileType)
                    {
                        case "AD1":
                        case "AD2":
                        case "ENV":
                        case "VEH":
                        case "TTL":
                        case "VEN":
                        case "PFP":
                        case "PFT":
                        case "PFM":
                        case "PFL":
                        case "PFH":
                        case "LIN":
                        case "STL":
                            AddDBFTableDataToXml(strFileType, strWorkPath, ref objFile, ref mobjXmlDoc);
                            break;
                        default:
                            break;
                        // Do Nothing.
                    }
                }
                // objFile.Delete True
                ConvertEmsDBFToXml = mobjXmlDoc;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objFile = null;
                objFolder = null;
                mobjFileSystemObject = null;
                mobjXmlDoc = null;
            }
            return ConvertEmsDBFToXml;
        }


        //TODO
        private void AddDBFTableDataToXml(string strElementName, string strWorkPath, ref File objDBFFile, ref XmlDocument objDoc)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "AddDBFTableDataToXml: " });

            ADODB.Recordset objRS = null;
            ADODB.Connection adodbConnection = null;
            XmlElement objRecord = null;
            XmlElement objField = null;

            int intRecord = 0;
            int intRecordcolumn = 0;
            string strFileName = string.Empty;
            string strDeletePath = string.Empty;
            string strPattern = string.Empty;
            string strSubstitution = string.Empty;

            try
            {
                adodbConnection = new ADODB.Connection();
                objRS = new ADODB.Recordset();
                objRecord = null;
                objField = null;
                mobjMDomUtils = new MDomUtils();

                strPattern = string.Concat(new string[] { ".", strElementName });
                strSubstitution = ".dbf";

                strFileName = Regex.Replace(Convert.ToString(objDBFFile), strPattern, strSubstitution, RegexOptions.IgnoreCase);
                objDBFFile.Copy(string.Concat(new string[] { objDBFFile.ParentFolder.Path, "\\", strFileName }), true);
                adodbConnection.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
                adodbConnection.Open(string.Concat(new string[] { "Driver={Microsoft dBASE Driver (*.dbf)};Dbq=", strWorkPath }));

                objRS.Open(string.Concat(new string[] { "select * from ", strFileName }), adodbConnection, CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly, Convert.ToInt32(CommandTypeEnum.adCmdText));

                if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                {
                    while (!objRS.EOF)
                    {
                        // objRecord = mDomUtils.AddElement(ref objDoc.DocumentElement, strElementName);
                        objRecord = objDoc.OwnerDocument.CreateElement(strElementName);
                        objDoc.AppendChild(objRecord);

                        for (intRecordcolumn = 0; intRecordcolumn <= objRS.Fields.Count - 1; intRecordcolumn++)
                        {
                            objField = mobjMDomUtils.AddElement(ref objRecord, objRS.Fields[intRecordcolumn].Name);

                            if ((objRS.Fields[intRecordcolumn].Value) != null)
                            {
                                switch (objRS.Fields[intRecordcolumn].Type)
                                {
                                    case DataTypeEnum.adDate:
                                    case DataTypeEnum.adDBDate:
                                        objField.InnerText = DateTime.Parse(Convert.ToString(objRS.Fields[intRecordcolumn].Value)).ToString("MM/dd/yyyy h:mm:ss tt");
                                        break;
                                    default:
                                        objField.InnerText = Convert.ToString(objRS.Fields[intRecordcolumn].Value);
                                        break;
                                }
                            }
                            else
                                objField.InnerText = "";
                        }
                        objRS.MoveNext();
                        //TODO
                        //DoEvents();
                        Thread.Sleep(1000);

                    }
                }
                objRS.Close();

                strDeletePath = string.Concat(new string[] { Convert.ToString(objDBFFile.ParentFolder.Path), "\\", strFileName });
                System.IO.Directory.Delete(strDeletePath);

                if ((objRS != null))
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                        objRS.Close();
                }
                objRS = null;
                if ((adodbConnection != null))
                {
                    if (adodbConnection.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                        adodbConnection.Close();
                }
                adodbConnection = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                if (objRS != null)
                {
                    if (objRS.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        objRS.Close();
                        objRS = null;
                    }
                }
                if (adodbConnection != null)
                {
                    if (adodbConnection.State == (int)ADODB.ObjectStateEnum.adStateOpen)
                    {
                        adodbConnection.Close();
                        adodbConnection = null;
                    }
                }
            }
        }


        internal void SetDocumentSendTransactionID(string DocumentID, string TransactionID)
        {
            const string PROC_NAME = "SetDocumentSendTransactionID:";
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strLYNXConnStringStd);
                mobjLAPDPartner.g_objDataAccessor.ExecuteSpNamedParams(mobjLAPDPartner.GetConfig("PartnerSettings/SetDocumentSendTransactionID"), new object[] { "@DocumentID", DocumentID, "@TransactionID", TransactionID });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        internal void UpdateDocumentSendStatus(string DocumentID, string SendStatus)
        {
            const string PROC_NAME = "UpdateDocumentSendStatus:";
            try
            {
                mobjLAPDPartner = new MLAPDPartnerDataMgr();

                mobjLAPDPartner.g_objDataAccessor.SetConnectString(mobjLAPDPartner.g_strLYNXConnStringStd);
                mobjLAPDPartner.g_objDataAccessor.ExecuteSpNamedParams(mobjLAPDPartner.GetConfig("PartnerSettings/UpdateDocumentSendStatus"), new object[] { "@DocumentID", DocumentID, "@SendStatusCD", SendStatus });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
    }
}
