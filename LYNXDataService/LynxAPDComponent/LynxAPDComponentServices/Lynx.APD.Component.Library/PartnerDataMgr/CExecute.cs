﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Lynx.APD.Component.Library.PartnerData;
using Lynx.APD.Component.Library.Common;
using LAPDEcadAccessorMgr;
using SiteUtilities;
using DataAccessor;
using DataPresenter;
using System.Data;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    public class CExecute
    {

        #region Declarations
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CExecute." });
        private bool mblnPartnerDbXfer;
        private bool mblnApdDbXfer;
        private bool mblnSendReceipt;
        private MLAPDPartnerDataMgr mLAPDPartnerDataMgr = null;
        private MDomUtils mDomUtils = null;
        

        // private MLAPDEcadAccessorMgr mLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();
        #endregion

        /// <summary>
        /// Error codes private to this module.
        /// </summary>
        private enum CExecute_Error_Codes : ulong
        {
            eMethodObjsolete = MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CExecute_CLS_ErrorCodes
        }


        private void Class_Initialize()
        {
            try
            {
                //Default is to submit to both databases.
                //They will only be overrid by backoffice document submission.
                mblnPartnerDbXfer = true;
                mblnApdDbXfer = true;
                mblnSendReceipt = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void Class_Terminate()
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            try
            {
                //Ensure that object clean-up happens in spite of errors.
                mLAPDPartnerDataMgr.TerminateGlobals();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Obsoleted - call ConsumePartnerXmlTransaction below instead.
        /// Added a new method rather than break binary compatability.
        /// </summary>
        /// <returns></returns>
        public long SendPartnerDocAsXml(string strXMLDoc, string strTradingPartner)
        {
            try
            {
                throw new Exception(string.Concat(new string[] { CExecute_Error_Codes.eMethodObjsolete.ToString(), "SendPartnerDocAsXml", "Method Obsolete - call ConsumePartnerXmlTransaction instead." }));
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// This function processes an Xml document sent from outside of PPG,
        /// Specifically from one of our partners (CCC, ADP, SG, etc.)
        /// </summary>
        /// <returns></returns>
        public string ConsumePartnerXmlTransaction(string strXMLDoc, string strTradingPartner, bool blnReturnXml)//Todo return value
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            string PROC_NAME = string.Concat(new string[] {MODULE_NAME, "ConsumePartnerXmlTransaction: " });
            string strConsumePartnerXmlTransaction = string.Empty;
            bool blnRaise = false;
            bool blnContinue;
            CPartner objPartner = null;

            try
            {
                //This bool is used to add additional control over when we raise to the caller.
                blnRaise = false;
                blnContinue = false;
                objPartner = new CPartner();

                //Raise to the caller for any problems with the passed parameters.
                blnRaise = true;

                //Store the received date/time for later.
                mLAPDPartnerDataMgr.g_varDocumentReceivedDate = DateTime.Now;

                // Put these in global scope for access by all procedures
                // including "InitalizeGlobals"
                mLAPDPartnerDataMgr.g_strPartner = strTradingPartner;
                mLAPDPartnerDataMgr.g_strPassedDocument = strXMLDoc;

                mLAPDPartnerDataMgr.InitializeGlobals();

                //Add parameters to trace data.
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Partner Document Received from ", strTradingPartner, "at", DateTime.Now.ToString(), Environment.NewLine, strXMLDoc }), string.Concat(new string[] { PROC_NAME, "Started" }));

                //Validate our parameters and trace them IF they are invalid.
                mLAPDPartnerDataMgr.g_objEvents.Assert((strTradingPartner.Length) > 0, "A blank Trading Partner string received");
                mLAPDPartnerDataMgr.g_strPartnerSmallID = Convert.ToString(mLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", strTradingPartner, "/@SmallID" })));
                mLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", strTradingPartner, "/@SmallID" }));

                //Get a partner object to work with.
                // objPartner = GetPartnerObject();

                if (strTradingPartner == "ADP")
                {
                    //Processing of document by partner specific class...
                    if (objPartner.InitProcessDocumentV2(strTradingPartner, strXMLDoc))
                    {
                        //Do the partner database submission?
                        if (mblnPartnerDbXfer)
                        {
                            blnContinue = objPartner.PartnerDatabaseTransferV2(mblnSendReceipt);
                        }

                        else
                        {
                            blnContinue = true;
                        }

                        //Once the document has successfully validated and stored in the
                        //partner database then we consider any issues to be internal.
                        blnRaise = false;

                        //Do the APD database submission?
                        if (mblnApdDbXfer && blnContinue)
                        {
                            strConsumePartnerXmlTransaction = objPartner.ApdDatabaseTransferV2();//Todo
                        }

                    }
                }

                else
                {
                    // Dump the passed in values into the bit bucket
                    strXMLDoc = "";
                    strTradingPartner = "";

                    //Processing of document by partner specific class...
                    if (objPartner.InitProcessDocument())
                    {
                        //Do the partner database submission?
                        if (mblnPartnerDbXfer)
                        {
                            blnContinue = objPartner.PartnerDatabaseTransfer(mblnSendReceipt);
                        }

                        else
                        {
                            blnContinue = true;
                        }

                        //Once the document has successfully validated and stored in the
                        //partner database then we consider any issues to be internal.
                        blnRaise = false;

                        //Do the APD database submission?
                        if (mblnApdDbXfer && blnContinue)
                        {
                            strConsumePartnerXmlTransaction = objPartner.ApdDatabaseTransfer();//Todo
                        }


                    }

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objPartner = null;
                mLAPDPartnerDataMgr.TerminateGlobals();
            }
            return strConsumePartnerXmlTransaction;
        }


        /// <summary>
        /// Syntax:      object.SendHeartBeatRequest()
        /// Purpose:     Network Status Test
        /// </summary>
        /// <returns></returns>
        public void SendHeartBeatRequest(string strTradingPartner)//Todo returnvalue
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendHeartBeatRequest: " });
            CPartner objPartner = null;

            try
            {
                objPartner = new CPartner();
                mLAPDPartnerDataMgr.g_strPartner = strTradingPartner;
                mLAPDPartnerDataMgr.g_strPassedDocument = "<Root/>";

                mLAPDPartnerDataMgr.InitializeGlobals();

                //Add parameters to trace data.
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "HeartBeat outbound to ", mLAPDPartnerDataMgr.g_strPartner, "at", DateTime.Now.ToString(), Environment.NewLine }), string.Concat(new string[] { PROC_NAME, "Started" }));


                //Validate our parameters and trace them IF they are invalid.
                mLAPDPartnerDataMgr.g_objEvents.Assert(Convert.ToBoolean((mLAPDPartnerDataMgr.g_strPartner.Length) > 0), "No Trading Partner value received");

                //Get a partner object to work with.
                // objPartner = GetPartnerObject(); //TODO
                objPartner.SendHeartBeatRequest();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objPartner = null;
                mLAPDPartnerDataMgr.TerminateGlobals();
            }
        }


        /// <summary>
        /// Backoffice utility for submitting to Partner database only.
        /// </summary>
        /// <returns></returns>
        public long SubmitDocumentToPartnerDbOnly(string strXMLDoc, string strTradingPartner, bool blnSendReceipt)
        {
            long lngSubmitDocumentToPartnerDbOnly;
            string strResult = string.Empty;

            try
            {
                lngSubmitDocumentToPartnerDbOnly = 0;
                strResult = null;
                mblnPartnerDbXfer = true;
                mblnApdDbXfer = false;
                mblnSendReceipt = blnSendReceipt;
                strResult = ConsumePartnerXmlTransaction(strXMLDoc, strTradingPartner, false);

                if (Left(strResult, 6) == "<Error")
                {
                    lngSubmitDocumentToPartnerDbOnly = 0;
                }
                else
                {
                    lngSubmitDocumentToPartnerDbOnly = 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return lngSubmitDocumentToPartnerDbOnly;
        }

        /// <summary>
        /// Backoffice utility for submitting to APD database only.
        /// </summary>
        /// <returns></returns>
        public long SubmitDocumentToApdDbOnly(string strXMLDoc, string strTradingPartner, bool blnSendReceipt)
        {
            long lngSubmitDocumentToApdDbOnly;
            string strResult = string.Empty;

            try
            {
                lngSubmitDocumentToApdDbOnly = 0;
                strResult = null;
                mblnPartnerDbXfer = false;
                mblnApdDbXfer = true;
                mblnSendReceipt = blnSendReceipt;
                strResult = ConsumePartnerXmlTransaction(strXMLDoc, strTradingPartner, false);

                if (Left(strResult, 6) == "<Error")
                {
                    lngSubmitDocumentToApdDbOnly = 0;
                }

                else
                {
                    lngSubmitDocumentToApdDbOnly = 1;
                }


            }

            catch (Exception ex)
            {
                throw ex;
            }
            return lngSubmitDocumentToApdDbOnly;
        }

        /// <summary>
        /// Returns a CPartner based upon g_strPartner
        /// </summary>
        /// <returns></returns>
        private object GetPartnerObject()//Todo
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            object objGetPartnerObject = null;
            try
            {
                switch ((mLAPDPartnerDataMgr.g_strPartner.ToUpper()))
                {
                    case "CCC":
                        objGetPartnerObject = new CCCC();
                        break;
                    case "ADP":
                        objGetPartnerObject = new CADP();
                        break;
                    case "CCAV":
                        objGetPartnerObject = new CAutoverse();
                        break;
                    case "GRANGEOHIO":
                        objGetPartnerObject = new CGrange();
                        break;
                    case "MITCHELL":
                        objGetPartnerObject = new CMitchell();
                        break;
                    case "SCENEGENESIS":
                        objGetPartnerObject = new CSceneGenesis();
                        break;
                    case "FCS":
                        objGetPartnerObject = new CFCS();
                        break;
                    default:
                        throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eInvalidTradingPartner.ToString(), "GetPartnerObject()", "Trading partner", mLAPDPartnerDataMgr.g_strPartner, " passed was unknown." }));

                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            return objGetPartnerObject;

        }

        /// <summary>
        /// Syntax:      object.InitiatePartnerXmlTransaction strXmlDoc
        /// Parameters:  strXmlDoc = Xml document received from calling application
        /// Purpose:     Receive an Xml from an internal application and then kick off
        /// a transaction with an external (partner) application.
        /// Returns:     Blank or any response XML.
        /// </summary>
        /// <returns></returns>
        public string InitiatePartnerXmlTransaction(string strXMLDoc)//todo return value
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitiatePartnerXmlTransaction: " });
            string strInitiatePartnerXmlTransaction = string.Empty;

            //Object vars that need clean up
            CPartner objPartner = null;

            try
            {
                objPartner = new CPartner();

                //Store the received date/time for later.
                mLAPDPartnerDataMgr.g_varDocumentReceivedDate = DateTime.Now;

                // Put these in global scope for access by all procedures
                // including "InitalizeGlobals"
                mLAPDPartnerDataMgr.g_strPassedDocument = strXMLDoc;

                // Dump the passed in values into the bit bucket
                strXMLDoc = "";
                mLAPDPartnerDataMgr.InitializeGlobals();

                //Add parameter to trace data.
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(mLAPDPartnerDataMgr.g_strPassedDocument, string.Concat(new string[] { PROC_NAME, "Started" }));

                //Validate our parameters and trace them IF they are invalid.
                mLAPDPartnerDataMgr.g_objEvents.Assert((mLAPDPartnerDataMgr.g_strPassedDocument.Length) > 0, "A blank Xml Document was received");

                //Extract the partner from the XML, i.e. "SceneGenesis".
                mLAPDPartnerDataMgr.g_strPartner = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//@Partner", true);

                //Get a little partner string from the config, i.e. "SG".
                mLAPDPartnerDataMgr.g_strPartnerSmallID = Convert.ToString(mLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", mLAPDPartnerDataMgr.g_strPartner, "/@SmallID" })));

                //Get a partner object to work with.
                //objPartner = GetPartnerObject();

                //Processing of document by partner specific class...
                strInitiatePartnerXmlTransaction = objPartner.TransferToPartner();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objPartner = null;
                mLAPDPartnerDataMgr.TerminateGlobals();
            }

            return strInitiatePartnerXmlTransaction;
        }

        /// <summary>
        /// Procedure : GetUnprocessedPartnerData
        /// DateTime  : 11/23/2004 14:33
        /// Author    : csr0901
        /// Purpose   : Gets a list of udb_partner.transaction_header records with 'unk' LynxIDs.These records represent data sent to Lynx by a shop before that shop
        /// received an electronic assignment from Lynx.  Due to that the incoming
        /// data contained no LynxID, VehicleNumber, nor AssignmentID.  That data is
        /// now stuck in the partner database until someone manually supplies the
        /// Missing required data listed above.
        /// </summary>
        /// <returns></returns>
        public string GetUnprocessedPartnerData(string strWebRoot, string strSessionKey, string strWindowID, string strUserID, string strErrorXML = "")
        {
            mDomUtils = new MDomUtils();
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "GetUnprocessedPartnerData: " });
            string strGetUnprocessedPartnerData = string.Empty,
            strConnect = string.Empty,
            strProcName = string.Empty,
            strDataSource = string.Empty,
            strDataSourceCode = string.Empty,
            strPartnerTransID = string.Empty,
            strTransactionDate = string.Empty,
            strTransactionSource = string.Empty,
            strTransactionType = string.Empty,
            strClaimNo = string.Empty,
            strInsdLN = string.Empty,
            strOwnerLN = string.Empty,
            strModelYear = string.Empty,
            strMake = string.Empty,
            strModel = string.Empty,
            strBodyStyle = string.Empty,
            strpath = string.Empty;
            XmlDocument objDataDOM = null;
            XmlDocument objReturnDOM = null;
            XmlDocument objErrorDOM = null;
            XmlElement objRootElement = null;
            XmlAttribute objAttribute = null;
            XmlNodeList objNodeList = null;
            XmlNode objNode = null;
            XmlElement objxmlElement;
            //object objExe = null;
            ADODB.Recordset objRS = null;
            long lngLynxID = 0;
            string strAppend = string.Empty;

            try
            {
                //Objects that need cleaning up
                strpath = "<Root></Root>";
                objDataDOM = new XmlDocument();
                objReturnDOM = new XmlDocument();
                objErrorDOM = new XmlDocument();
                //objRootElement = new XmlElement();
                //objAttribute = new XmlAttribute();
                objRS = new ADODB.Recordset();
                lngLynxID = 0;

                //Primitives that do not
                // ERROR: Not supported in C#: OnErrorStatement
                mLAPDPartnerDataMgr.InitializeGlobals();

                //Initialize DOM objects.
                objDataDOM = new XmlDocument();
                objReturnDOM = new XmlDocument();
                mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strPartnerConnStringStd);

                //Get the procedure name from the config XML.
                strProcName = "uspTransactionHeaderUnprocessedGetDetail";

                //Get the XML from the database.
                objRS = mLAPDPartnerDataMgr.g_objDataAccessor.OpenRecordsetSp(strProcName,null);

                //Add a trace mesage to the debug log.
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { PROC_NAME, "After DB Execute." }));


                //Creates an emp.
                mDomUtils.LoadXml(ref objReturnDOM, ref strpath, PROC_NAME, "Parameter");

                //Get the root element of the return XML.
                objRootElement = objReturnDOM.DocumentElement;

                while (!objRS.EOF)
                {

                    //Loads and validates the xml.
                    strAppend = objRS.Fields["XML"].Value.ToString();
                    mDomUtils.LoadXml(ref objDataDOM, ref strAppend, PROC_NAME, "data");

                    // Get the values from the recordset and embedded XML that will be returned to the caller.
                    strPartnerTransID = Convert.ToString(objRS.Fields["XML"].Value);
                    strTransactionDate = Convert.ToString(objRS.Fields["TransactionDate"].Value);
                    strTransactionSource = Convert.ToString(objRS.Fields["TransactionSource"].Value);
                    strTransactionType = Convert.ToString(objRS.Fields["TransactionType"].Value);
                    strClaimNo = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//CLM_NO");
                    strInsdLN = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//INSD_LN");
                    strOwnerLN = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//OWNER_LN");
                    strModelYear = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//V_MODEL_YR");
                    strMake = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//V_MAKEDESC");
                    strModel = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//V_MODEL");
                    strBodyStyle = mLAPDPartnerDataMgr.GetChildNodeText(ref objDataDOM, "//V_BSTYLE");

                    // Create a node to hold the retrieved values for this record and add it to the Root.
                    //ToDo: Verify
                    //objNode = mDomUtils.AddElement(ref objNode, "PartnerRecord");
                    objxmlElement = objNode.OwnerDocument.CreateElement("PartnerRecord");
                    objNode.AppendChild(objxmlElement);

                    // Add the retrieved values as attributes of the newly created node.
                    mDomUtils.AddAttribute(ref objxmlElement, "PartnerTransID", strPartnerTransID);
                    objNode.OwnerDocument.CreateAttribute("");
                    mDomUtils.AddAttribute(ref objxmlElement, "TransactionDate", strTransactionDate);
                    mDomUtils.AddAttribute(ref objxmlElement, "TransactionSource", strTransactionSource);
                    mDomUtils.AddAttribute(ref objxmlElement, "TransactionType", strTransactionType);
                    mDomUtils.AddAttribute(ref objxmlElement, "ClaimNo", strClaimNo);
                    mDomUtils.AddAttribute(ref objxmlElement, "InsdLN", strInsdLN);
                    mDomUtils.AddAttribute(ref objxmlElement, "OwnerLN", strOwnerLN);
                    mDomUtils.AddAttribute(ref objxmlElement, "Year", strModelYear);
                    mDomUtils.AddAttribute(ref objxmlElement, "Make", strMake);
                    mDomUtils.AddAttribute(ref objxmlElement, "Model", strModel);
                    mDomUtils.AddAttribute(ref objxmlElement, "BodyStyle", strBodyStyle);
                    objRS.MoveNext();
                }

                // If there are errors append them to the xml to be returned.
                if (strErrorXML != null)
                {
                    objErrorDOM = new XmlDocument();

                    // Load and validate the passed xml error string.
                    mDomUtils.LoadXml(ref objErrorDOM, ref strErrorXML, PROC_NAME, "errors");

                    // Grab the Errors node which contains the individual Error nodes.
                    objNode = objErrorDOM.SelectSingleNode("/Root/Errors");

                    // Append the Errrors node to the Root node of the XML to be transformed
                    objRootElement.AppendChild(objNode);
                }

                // Create an instance of DataPresenter to perform the XML to HTML transformation.
                //  objExe = mLAPDPartnerDataMgr.CreateObjectEx("DataPresenter.CExecute");
                DataPresenter.CExecute objExe = new DataPresenter.CExecute();
                // Initialize DataPresenter.
                objExe.InitializeEx(strWebRoot, strSessionKey, strWindowID, strUserID);

                //Perform the transformation and return the resultant HTML.
                strGetUnprocessedPartnerData = objExe.TransformXML(objReturnDOM.OuterXml, "UnprocessedPartnerData.xsl");

                //Add another trace message to note exit?
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Return = ", strGetUnprocessedPartnerData }), string.Concat(new string[] { PROC_NAME, "Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objRS.Close();
                objRS = null;
                mLAPDPartnerDataMgr.TerminateGlobals();
            }

            return strGetUnprocessedPartnerData;
        }


        /// <summary>
        /// Procedure : FixUnprocessedPartnerData       
        /// Parameters: strData - delimited string of the format: PartnerTransID,LynxID,VehicleNumber;...
        ///           : strWebRoot - required because
        /// Purpose   : This function parse strData resolving the AssignmentID associated with the each passed
        ///            LynxID/VehicleNumber combination, pulling the partner XML assocaited with PartnerTransID,
        ///            inserting the LynxID, VehicleNumber, and AssignmentID appropriately into the partner XML,
        ///            submit the corrected partnerXML to the APD database, and finally update the Partner record,
        ///            with the appropriate values.
        /// </summary>
        /// <returns></returns>
        public string FixUnprocessedPartnerData(string strData, string strWebRoot, string strSessionKey, string strWindowID, string strUserID)
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mDomUtils = new MDomUtils();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "FixUnprocessedPartnerData:" });
            string strRoot = string.Empty,
            strReturnData = string.Empty,
            strFixUnprocessedPartnerData = string.Empty,
            strAssignmentID = string.Empty,
            strErrorXML = string.Empty,
            strPartner = string.Empty,
            strPartnerTransID = string.Empty,
            strResult = string.Empty,
            strTransactionType = string.Empty,
            strXML = string.Empty,
            strPath = string.Empty;
            long lngIte,
            lngUBound,
            strLynxID;
            int intErrorCount,
            strVehicleNumber;
            XmlDocument xmlDocument, objReturnDOM, objDataDOM;
            XmlNode objNode, objErrorNode;
            XmlAttribute objAttribute;
            object varrData = null;
            string[] varrDataRow = null;
            ADODB.Recordset objRS = null;
            DataRow drDataRow = null;
            string[] strSplitarray = null;
            try
            {
                // Objects that require cleaning up.
                strRoot = "<Root><Errors></Errors></Root>";
                strReturnData = "ReturnData";
                lngIte = 0;
                lngUBound = 0;
                strLynxID = 0;
                intErrorCount = 0;
                strVehicleNumber = 0;
                xmlDocument = new XmlDocument();
                objReturnDOM = new XmlDocument();
                objDataDOM = new XmlDocument();
                //objAttribute = new XmlAttribute();
                objRS = new ADODB.Recordset();
                mLAPDPartnerDataMgr.InitializeGlobals();
                drDataRow = null;

                // Initialize local objects and variables.
                intErrorCount = 0;
                mDomUtils.LoadXml(ref objReturnDOM, ref strRoot, PROC_NAME, "ReturnData");
                objErrorNode = objReturnDOM.SelectSingleNode("/Root/Errors");

                // Remove trailing semicolon (;) if it exists.
                if (Right(strData, 1) == ";")
                    strData = Left(strData, (strData.Length - 1));

                // Break the data into individual PartnerTransID,LynxID,VehicleNumber strings.
                varrDataRow = strData.Split(';');
                strSplitarray = strData.Split(';');

                // Get the upper bound of the newly created array.
                //lngUBound = strData.varrDataRow;
                lngUBound = varrDataRow.Length - 1;

                // Loop through the array.
                for (lngIte = 0; lngIte <= lngUBound; lngIte++)
                {
                    // Break the data row into individual data items.                   
                    varrData = strSplitarray[lngIte].Split(',');

                    // Extract the individual data items.
                    strPartnerTransID = strSplitarray[0].ToString();
                    strLynxID = Convert.ToInt64(strSplitarray[1]);
                    strVehicleNumber = Convert.ToInt32(strSplitarray[2]);

                    // Point data accessor to the APD database.
                    mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strLYNXConnStringStd);

                    // Temporarily turn off error handling
                    // ERROR: Not supported in C#: OnErrorStatement
                    //Get the assignment ID of the assignment for this vehicle.
                    strAssignmentID = mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig(("ClaimPoint/SPMap/AssignmentDecodeSP").ToString()),new object[] {"@LynxID", Convert.ToInt64(strLynxID), "@VehicleNumber", Convert.ToInt32(strVehicleNumber), "@AssignmentSuffix", ""}).ToString();

                    // If any error occured, assume an AssignmentID could not be found for the passed LynxID/Vehicle Number.
                    // This will be handled below due to the fact that strAssignmentID is still empty.
                    //Err.Clear();
                    // Resume normal error handling.
                    // ERROR: Not supported in C#: OnErrorStatement
                    // Continue processing this partner record only if an AssignmentID was found.
                    if (strAssignmentID != null)
                    {
                        // Point data accessor to the Partner database.
                        mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strPartnerConnStringStd);

                        // Get the transaction header record.
                        objRS = mLAPDPartnerDataMgr.g_objDataAccessor.OpenRecordsetNamedParams("uspTransactionHeaderUnprocessedGetDetail",new object[] { "@PartnerTransID", Convert.ToInt64(strPartnerTransID)});

                        // Make sure a record still exists for this PartnerTransID.                        
                        if (!objRS.EOF)
                        {
                            // Load xml string into the DOM.
                            strPath = objRS.Fields["XML"].Value.ToString();
                            mDomUtils.LoadXml(ref objDataDOM, ref strPath, PROC_NAME, "utb_transaction_header.xml");
                            strPath = string.Empty;

                            // Get some values from the retrieved record.
                            strPartner = Convert.ToString(objRS.Fields["TransactionSource"].Value);
                            strTransactionType = Convert.ToString(objRS.Fields["TransactionType"].Value);

                            if (strPartner == "ADP")
                            {
                                // Set the missing values in the XML.
                                objDataDOM.DocumentElement.Attributes.GetNamedItem("LynxID").InnerText = strLynxID.ToString();
                                objDataDOM.DocumentElement.Attributes.GetNamedItem("VehicleID").InnerText = strVehicleNumber.ToString();
                                objDataDOM.DocumentElement.Attributes.GetNamedItem("AssignmentID").InnerText = strAssignmentID.ToString();

                                // Hack neceesary because InitializeGlobals() assumes any XML was passed in original call,
                                // however here we pull the XML from the database after InitializeGlobals has already been called.
                                if (mLAPDPartnerDataMgr.g_objPassedDocument == null)
                                    mLAPDPartnerDataMgr.g_objPassedDocument = new XmlDocument();

                                string str = objDataDOM.ToString();
                                mDomUtils.LoadXml(ref mLAPDPartnerDataMgr.g_objPassedDocument, ref str, PROC_NAME, string.Concat(new string[] { "PartnerTransID=", strPartnerTransID }));

                                // Submit the xml back thru the system
                                strResult = ConsumePartnerXmlTransaction(objDataDOM.ToString(), strPartner, false);

                                // Handle any error return from the data submission process.
                                if (Left(strResult, 6) == "<Error")
                                {
                                    // PartnerTransID not found.  Create a node to hold the error text and add it to the Root.
                                    //ToDo: Verify
                                    //objNode = mDomUtils.AddElement(ref objErrorNode, "Error");
                                    objNode = objErrorNode.OwnerDocument.CreateElement("Error");
                                    objErrorNode.AppendChild(objNode);
                                    objNode.InnerText = string.Concat(new string[] { "The specified ", strTransactionType, " (PartnerTransID=", strPartnerTransID, ") could ", "not be successfully loaded into the APD database." });
                                    intErrorCount = intErrorCount + 1;
                                    //g_objevents.HandleEvent

                                }

                                else
                                {
                                    // Point data accessor to the Partner database.
                                    mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strPartnerConnStringStd);

                                    // Delete the original Partner record.
                                    mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams("uspTransactionHeaderDel",new object[] { "@PartnerTransID", strPartnerTransID});

                                }

                            }

                            else
                            {
                                // Not ADP data.
                                //ToDo: Verify
                                // objNode = mDomUtils.AddElement(ref(XmlElement)(objErrorNode), "Error");
                                objNode = objErrorNode.OwnerDocument.CreateElement("Error");
                                objErrorNode.AppendChild(objNode);
                                objNode.InnerText = string.Concat(new string[] { "This process is currently only supported for ADP estimates.  Could not attach this ", strTransactionType, " to LynxID: ", strLynxID.ToString(), "-", strVehicleNumber.ToString(), "." });

                                intErrorCount = intErrorCount + 1;
                                //g_objevents.HandleEvent
                            }

                        }

                        else
                        {
                            // PartnerTransID not found.  Create a node to hold the error text and add it to the Root.
                            //objNode = mDomUtils.AddElement(ref objErrorNode, "Error");
                            objNode = objErrorNode.OwnerDocument.CreateElement("Error").SelectSingleNode("Error");
                            objNode.InnerText = string.Concat(new string[] { "The specified ", strTransactionType, " (PartnerTransID=", strPartnerTransID, ") could not be found in the ", "Partner database.  Check your claim, someone may have already fixed the data you are attempting to fix." });
                            intErrorCount = intErrorCount + 1;

                        }

                    }
                    else
                    {
                        // AssignmentID not found.  Create a node to hold the error text and add it to the Root.
                        //ToDo: Verify
                        //objNode = mDomUtils.AddElement(ref objErrorNode, "Error");
                        objNode = objErrorNode.OwnerDocument.CreateElement("Error");
                        objErrorNode.AppendChild(objNode);
                        objNode.InnerText = string.Concat(new string[] { "No Assignment ID could be found for claim, ", strLynxID.ToString(), "-", strVehicleNumber.ToString(), ".  Please ensure you ", "entered a valid LynxID/Vehicle Number combination and that that vehicle's assignment is still valid." });
                        intErrorCount = intErrorCount + 1;
                        //g_objevents.HandleEvent
                    }

                }

                // Grab error XML.  If there were no errors set the value to null
                strErrorXML = (intErrorCount == 0 ? null : objReturnDOM.OuterXml);

                // Call GetUnprocessedPartnerData to get the updated list of partner records an perorm the transformation to HTML.  This is a
                // little hokey but these are highly specalized procedures, this provides a way of including any error xml in the
                // transformation, and saves the web page a round trip.  Nice rationalization, huh?
                strFixUnprocessedPartnerData = GetUnprocessedPartnerData(strWebRoot, strSessionKey, strWindowID, strUserID, strErrorXML);

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objRS.Close();
                objRS = null;
                mLAPDPartnerDataMgr.TerminateGlobals();
            }

            return strFixUnprocessedPartnerData;
        }

        /// <summary>
        /// String Left Process
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Left(string param, int length)
        {
            try
            {
                string strLeft = param.Substring(0, length);
                return strLeft;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// String Right Process
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Right(string param, int length)
        {
            try
            {
                string strRight = param.Substring(param.Length - length, length);
                return strRight;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// String Mid
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string Mid(string param, int startIndex, int length)
        {
            try
            {
                string strMid = param.Substring(startIndex, length);
                return strMid;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// String Mid - Overloaded
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public string Mid(string param, int startIndex)
        {
            try
            {
                string strMid = param.Substring(startIndex);
                return strMid;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
