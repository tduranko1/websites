﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using SiteUtilities;
using Scripting;
using LAPDEcadAccessorMgr;
using DataAccessor;
using System.Xml.Xsl;
using System.Runtime.Serialization.Formatters.Binary;
using MSXML2;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{

    class CAutoverse
    {

        #region Public Variables
        string MODULE_NAME = string.Concat(new string[] { "LAPDPartnerDataMgr.", "CAutoverse." });
        public CPartner mobjCpartner = null;
        public CEvents mobjCEvents = null;
        public Common.MDomUtils mobjMdomUtils = null;
        public PartnerData.MLAPDPartnerData mobjMLAPDPartnerData = null;
        public MPPGTransactions mobjMPPGTransactions = null;
        public CDataAccessor mobjDataAccessor = null;
        public MLAPDPartnerDataMgr mobjMLAPDParterDataMgr = null;
        public MBase64Utils mobjMBase64Utils = null;
        LAPDEcadAccessorMgr.CWebAssignment mobjECAD = null;

        string cstrTransactionHeaderTag = string.Empty,
            cstrAssignmentTag = string.Empty,
            cstrClaimMessageTag = string.Empty,
            cstrLynxAPDInsCoID = string.Empty,
            cstrClaimNumber = string.Empty,
            cstrSenderName = string.Empty,
            cstrLynxAPDInsCoDefaultAdjLogon = string.Empty,
            clngAutoverseUserID = string.Empty,
            cstrSourceApplicationPassThruData = string.Empty;
        VehData objVehData;
        #endregion

        #region Struct
        public struct VehData
        {
            public string Year;
            public string Make;
            public string Model;
        };
        #endregion

        #region Enums
        public enum Autoverse_Error_Codes : ulong
        {
            eBadAssignmentCode = 0x80067480,
            eAssignmentInvalid,
            eAttachmentIdNotFound,
            eClaimNumberInvalid,
            eNoPassThruData,
            eBadPassThruDataFormat,
            eUnhandledAutoverseNack,
            eBlankCefReturnedForEstimate,
            eXmlTransactionTooLarge,
            eInternalError,
            eInvalidEventCode,
            eMissingMessageOrDocumentNode,
            eUnexpectedReceipt,
            eInvalidInsuranceCompanyCode,
        }
        #endregion

        #region Methods
        public void SendAReceipt(ref string strTemplateFile, [Optional] string strSuccess, [Optional] string lngErrorCode, [Optional] string strErrorDesc)
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendAReceipt:" });
            XmlDocument xmlDocumentLYNXReceipt;
            string strWrappedReceipt = string.Empty,
                strPost2URL = string.Empty,
                strTransactionID = string.Empty;
            Boolean g_blnDebugMode = false;

            try
            {
                mobjCEvents = new CEvents();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMPPGTransactions = new MPPGTransactions();

                g_blnDebugMode = mobjCEvents.IsDebugMode;

                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Preparing receipt" }));

                //First load the receipt template from disk.
                xmlDocumentLYNXReceipt = new XmlDocument();

                mobjMdomUtils.LoadXml(ref xmlDocumentLYNXReceipt, ref strPost2URL, "SendAReceipt", "Receipt");

                strTransactionID = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//SenderTransactionId").InnerText;

                SetHeaderSchtuff(xmlDocumentLYNXReceipt, "");
                xmlDocumentLYNXReceipt.SelectSingleNode("//ReceiptFor").InnerText = strTransactionID;
                xmlDocumentLYNXReceipt.SelectSingleNode("///SuccessFlag").InnerText = strSuccess;
                xmlDocumentLYNXReceipt.SelectSingleNode("/FailureReasonCode").InnerText = lngErrorCode;
                xmlDocumentLYNXReceipt.SelectSingleNode("//FailureReasonText").InnerText = strErrorDesc;

                strWrappedReceipt = mobjMPPGTransactions.BuildTransactionForPPG("Receipt", xmlDocumentLYNXReceipt.OuterXml, "", string.Concat(new string[] { cstrClaimNumber, "-", cstrLynxAPDInsCoID }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedReceipt);

                if (g_blnDebugMode)
                    mobjCEvents.Trace(string.Concat(new string[] { "URL=", strPost2URL, PROC_NAME, "Receipt sent via HTTP" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        /// <summary>
        /// Send a business event to Partner.
        /// </summary>
        /// <param name="strEventCode"></param>
        /// <param name="strEventNote"></param>
        /// <param name="strSuccess"></param>
        /// <param name="strEventOpcode"></param>
        public void SendBusinessEvent(string strEventCode, string strEventNote, string strSuccess, string strEventOpcode)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendBusinessEvent:" });

            XmlDocument xmlDocumentObj, xmlDocumentSession;
            string strEventText = string.Empty,
             strAreaCode = string.Empty,
             strExchange = string.Empty,
             strUnitNo = string.Empty,
             strExt = string.Empty,
             strEmail = string.Empty,
             strWrappedXml = string.Empty,
             strPost2URL = string.Empty;
            Boolean g_blnDebugMode = false;

            try
            {
                mobjCEvents = new CEvents();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMPPGTransactions = new MPPGTransactions();
                mobjDataAccessor = new CDataAccessor();

                g_blnDebugMode = mobjCEvents.IsDebugMode;

                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Preparing Business Event." }));

                //First Load Receipt template from disk
                xmlDocumentObj = new XmlDocument();
                string strPath = string.Concat(new string[] { mobjMLAPDParterDataMgr.g_strSupportDocPath, "\\AutoverseBusinessEvent.XML" });
                mobjMdomUtils.LoadXmlFile(ref xmlDocumentObj, ref strPath, PROC_NAME, "Business Event");


                SetHeaderSchtuff(xmlDocumentObj, "");

                xmlDocumentObj.SelectSingleNode("//InsuranceCarrierID").InnerText = cstrLynxAPDInsCoID;
                xmlDocumentObj.SelectSingleNode("//ClaimNumber").InnerText = cstrClaimNumber;
                xmlDocumentObj.SelectSingleNode("//EventCode").InnerText = strEventCode;

                switch (strEventCode)
                {
                    case "Claim Assigned":
                        {
                            xmlDocumentSession = new XmlDocument();
                            //Get the claim session detail from database so we can populate adjuster info
                            mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);
                            string strxmlPath = mobjDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/SessionClaimDetailSP"), new object[] { "@LynxID", mobjMLAPDParterDataMgr.g_strLynxID });
                            mobjMdomUtils.LoadXml(ref xmlDocumentSession, ref strxmlPath, PROC_NAME, "Session Claim Detail");

                            strAreaCode = xmlDocumentSession.SelectSingleNode("//@OwnerUserPhoneAreaCode").InnerText;
                            strExchange = xmlDocumentSession.SelectSingleNode("//@OwnerUserPhoneExchangeNumber").InnerText;
                            strUnitNo = xmlDocumentSession.SelectSingleNode("//@OwnerUserPhoneUnitNumber").InnerText;
                            strExt = xmlDocumentSession.SelectSingleNode("//@OwnerUserExtensionNumber").InnerText;
                            strEmail = xmlDocumentSession.SelectSingleNode("//@OwnerUserEmail").InnerText;

                            xmlDocumentObj.SelectSingleNode("//VehicleYr").InnerText = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//V_MODEL_YR").InnerText;
                            xmlDocumentObj.SelectSingleNode("//VehicleYr").InnerText = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//V_MAKECODE").InnerText;
                            xmlDocumentObj.SelectSingleNode("//VehicleYr").InnerText = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//V_MODEL").InnerText;

                            strEventText = string.Concat(new string[] { cstrClaimNumber, " was received by LYNX.", "Lynx ID:", mobjMLAPDParterDataMgr.g_strLynxID, "Assigned To:" });

                            strEventText = string.Concat(new string[] { strEventText, xmlDocumentSession.SelectSingleNode("//@OwnerUserNameFirst").InnerText, xmlDocumentSession.SelectSingleNode("//@OwnerUserNameLast").InnerText });

                            //Space(13) equivalent to PadRight(13)
                            strEventText = string.Concat(new string[] { strEventText, strEventText.PadRight(13, ' '), "(", strAreaCode, ")", strExchange, "-", strUnitNo });

                            if (strEventText.Length > 0)
                                strEventText = string.Concat(new string[] { strEventText, "Ext.", strExt });

                            strEventText = string.Concat(new string[] { strEventText, Environment.NewLine, strEmail });

                            break;
                        }
                    case "Claim Cancel":
                        {
                            objVehData = new VehData();
                            xmlDocumentObj.SelectSingleNode("//VehicleYr").InnerText = objVehData.Year;
                            xmlDocumentObj.SelectSingleNode("//VehicleMake").InnerText = objVehData.Make;
                            xmlDocumentObj.SelectSingleNode("//VehicleModel").InnerText = objVehData.Model;

                            strEventText = "Cancellation Request Received by LYNX";
                            break;
                        }
                    default:
                        throw new Exception(string.Concat(new string[] { Convert.ToString(Autoverse_Error_Codes.eInvalidEventCode), "", "Invalid Event Code for Autoverse Business Event" }));
                }
                xmlDocumentObj.SelectSingleNode("//EventText").InnerText = strEventText;

                //Build the SOAP Envelope and put the HeartBeat into the envelope
                strWrappedXml = mobjMPPGTransactions.BuildTransactionForPPG("BusinessEvent", xmlDocumentObj.OuterXml, mobjMLAPDParterDataMgr.g_strPartner, string.Concat(new string[] { mobjMLAPDParterDataMgr.g_strLynxID, "-", mobjMLAPDParterDataMgr.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);

                if (g_blnDebugMode)
                    mobjCEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL, PROC_NAME, ":Business Event Sent via HTTP " }));

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlDocumentObj = null;
                xmlDocumentSession = null;
            }
        }


        /// <summary>
        /// Fills out some generic header schtuff.
        /// </summary>
        /// <param name="xmlLYNXReceipt"></param>
        /// <param name="p"></param>
        private void SetHeaderSchtuff(XmlDocument xmlDocumentObj, string strIdent)
        {
            string strDate = string.Empty,
                strTime = string.Empty,
                strLynxID = string.Empty,
                    strVehNum = string.Empty;
            Int32 intPtr;
            DateTime dtNow;
            XmlNode SourcePassPassedDoc = null, xmlNodeSourcePassDom = null;

            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                dtNow = DateTime.Now;
                for (intPtr = 1; intPtr <= (7 - mobjMLAPDParterDataMgr.g_strLynxID.Length); intPtr++)
                    strLynxID = string.Concat(new string[] { strLynxID, "0" });

                strLynxID = string.Concat(new string[] { strLynxID, mobjMLAPDParterDataMgr.g_strLynxID });

                if (mobjMLAPDParterDataMgr.g_strVehicleNumber == string.Empty)
                    strVehNum = "0";
                else
                    strVehNum = mobjMLAPDParterDataMgr.g_strVehicleNumber;
                xmlDocumentObj.SelectSingleNode("//@TransactionID").InnerText = string.Concat(new string[] { dtNow.ToString("yyyyMMddhhnnssfff"), "#", strLynxID, "-", strVehNum, "#" });

                xmlDocumentObj.SelectSingleNode("//@TransactionDate").InnerText = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                xmlDocumentObj.SelectSingleNode("//@TransactionTime").InnerText = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));

                switch (xmlDocumentObj.SelectSingleNode("@TransactionType").InnerText.ToUpper())
                {
                    case "ACK":
                        dtNow = Convert.ToDateTime(mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//head:TransactionDateTime").InnerText);
                        break;
                }

                xmlDocumentObj.SelectSingleNode("//DateTime/@Date").InnerText = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                xmlDocumentObj.SelectSingleNode("//DateTime/@Time").InnerText = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));
                xmlDocumentObj.SelectSingleNode("//DateTime/@TimeZone").InnerText = "EST";

                SourcePassPassedDoc = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//SourcePassThruData").CloneNode(true);
                xmlNodeSourcePassDom = xmlDocumentObj.SelectSingleNode("//SourcePassThruData");
                xmlNodeSourcePassDom.ParentNode.ReplaceChild(SourcePassPassedDoc, xmlNodeSourcePassDom);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool CPartner_InitProcessDocument()
        {
            string PROC_NAME = "InitProcessDocument:";
            XmlDocument xmlDocumentObj;
            XmlNode xmlNodeObj = null;
            XmlCDataSection xmlCDataSecObj;

            string strCRMId = string.Empty,
                strInsuranceCompanyID = string.Empty,
                strProc = string.Empty,
                strNamespaces = string.Empty,
                strTemplateField = string.Empty;

            Boolean g_blnDebugMode;

            try
            {
                mobjCpartner = new CPartner();
                mobjCEvents = new CEvents();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjDataAccessor = new CDataAccessor();

                g_blnDebugMode = mobjCEvents.IsDebugMode;
                if (g_blnDebugMode)
                    mobjCEvents.Trace(string.Concat(new string[] { "", PROC_NAME + "Started" }));

                //Extract the transaction type from transaction header
                foreach (XmlAttribute xmlObjAttrNode in mobjMLAPDParterDataMgr.g_objPassedDocument.DocumentElement.Attributes)
                {
                    if ((xmlObjAttrNode.Name.Length) >= 6)
                    {
                        if (Left(xmlObjAttrNode.Name, 6) == "xmlns:")
                            strNamespaces = string.Concat(new string[] { strNamespaces, xmlObjAttrNode.Name, "='", xmlObjAttrNode.Value, "'" });
                    }
                }

                //Todo for Set Property using XMLNameSpaceManager
                XmlNamespaceManager xmlNameSpace = new XmlNamespaceManager(mobjMLAPDParterDataMgr.g_objPassedDocument.NameTable);
                xmlNameSpace.AddNamespace("SelectionNamespaces", strNamespaces);

                //mobjMLAPDParterDataMgr.g_objPassedDocument.s("SelectionNamespaces", strNamespaces);
                mobjCpartner.RawDocTypeString = mobjMLAPDParterDataMgr.g_objPassedDocument.DocumentElement.Name;
                mobjMLAPDParterDataMgr.g_blnUsePartnerTypesForDocTypes = true;

                strCRMId = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//head:TransactionHeader/head:InsCoCode").InnerText;

                cstrClaimNumber = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//head:TransactionHeader/head:ClaimNumber").InnerText;

                TranslateCCCInsCoToAPDInsCo(strCRMId);
                xmlNodeObj = mobjMLAPDParterDataMgr.g_objPassedDocument.CreateElement("SourcePassThruData");
                xmlCDataSecObj = mobjMLAPDParterDataMgr.g_objPassedDocument.CreateCDataSection(mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//head:TransactionHeader").InnerXml);
                xmlNodeObj.AppendChild(xmlCDataSecObj);
                mobjMLAPDParterDataMgr.g_objPassedDocument.DocumentElement.AppendChild(xmlNodeObj);

                if (cstrLynxAPDInsCoID == string.Empty)
                {
                    strTemplateField = "AutoverseReceipt.XML";
                    SendAReceipt(ref strTemplateField, "N", Convert.ToString(Autoverse_Error_Codes.eInvalidInsuranceCompanyCode), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/InvalidInsuranceCompanyCode"));
                    mobjCEvents.HandleEvent(Convert.ToInt32(Autoverse_Error_Codes.eInvalidInsuranceCompanyCode), PROC_NAME, "");
                }

                cstrLynxAPDInsCoDefaultAdjLogon = mobjMLAPDParterDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/CCAV/InsuranceCompany[@CRMId='", strCRMId, "']/@DefaultUser" }));

                strProc = mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp");

                //Set Database connection
                mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);

                xmlDocumentObj = new XmlDocument();
                //Call stored procedure to retrieve claim info based on client claim number.  Load results into a DOM object.
                string strSpNamedParamsXML = mobjDataAccessor.ExecuteSpNamedParamsXML(strProc, new object[] { "@ClientClaimNumber", cstrClaimNumber, "@InsuranceCompanyID", cstrLynxAPDInsCoID, "@SourceApplicationID", 6 });
                mobjMdomUtils.LoadXml(ref xmlDocumentObj, ref strSpNamedParamsXML, string.Empty, string.Empty);

                mobjMLAPDParterDataMgr.g_strLynxID = xmlDocumentObj.SelectSingleNode("//@LynxID").InnerText;
                mobjMLAPDParterDataMgr.g_strVehicleNumber = xmlDocumentObj.SelectSingleNode("//@VehicleNumber").InnerText;
                mobjMLAPDParterDataMgr.g_strStatus = xmlDocumentObj.SelectSingleNode("//@Status").InnerText;

                objVehData = new VehData();
                objVehData.Year = xmlDocumentObj.SelectSingleNode("//@VehicleYear").InnerText;
                objVehData.Make = xmlDocumentObj.SelectSingleNode("//@Make").InnerText;
                objVehData.Model = xmlDocumentObj.SelectSingleNode("//@Model").InnerText;

                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new string[] { PROC_NAME, " Finished" }));


            }
            catch (Exception ex)
            {
                throw ex;
            }


            return mobjCpartner.CanProcessDocument();
        }

        /// <summary>
        /// it set cstrLynxAPDInsCoID ID
        /// </summary>
        /// <param name="strCCCInsCo"></param>
        private void TranslateCCCInsCoToAPDInsCo(string strCCCInsCo)
        {
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                cstrLynxAPDInsCoID = mobjMLAPDParterDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/CCAV/InsuranceCompany[@CRMId='", strCCCInsCo, "']/@ApdID" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cstrLynxAPDInsCoID = string.Empty;
            }
        }

        /// <summary>
        /// It call InsertPrintImageIntoAPD methods
        /// </summary>
        /// <param name="strXML"></param>
        /// <param name="lngPartnerTransID"></param>
        /// <returns></returns>
        private long CPartner_InsertPrintImageIntoAPD(string strXML, long lngPartnerTransID)
        {
            try
            {
                mobjCpartner = new CPartner();
                return mobjCpartner.InsertPrintImageIntoAPD(strXML, lngPartnerTransID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // ToDO Partially:
        private bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransfer: " });
            bool g_blnDebugMode = false;
            try
            {
                mobjCpartner = new CPartner();
                mobjCEvents = new CEvents();

                g_blnDebugMode = mobjCEvents.IsDebugMode;

                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new string[] { PROC_NAME, " Started " }));
                string strTemplateFile = "AutoverseReceipt.XML";
                switch (mobjCpartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjCpartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt(ref strTemplateFile, null, null, null);
                        break;
                    case CPartner.EDocumentType.ePrintImage:
                        mobjCpartner.ExtractSequenceNumber("//EstimateIdentifier");
                        mobjCpartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt(ref strTemplateFile, null, null, null);
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                        mobjCpartner.ExtractSequenceNumber("//EstimateIdentifier");
                        mobjCpartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            SendAReceipt(ref strTemplateFile, null, null, null);
                        break;
                    case CPartner.EDocumentType.eHeartBeat:
                        break;
                    case CPartner.EDocumentType.eReceipt:
                        mobjCpartner.StoreDocumentInPartnerDb();
                        break;
                    case CPartner.EDocumentType.eShopSearch:
                        mobjCpartner.StoreDocumentInPartnerDb();
                        break;
                    case CPartner.EDocumentType.eBusinessEvent:
                        string strRaiseError = string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType.ToString(), PROC_NAME, "We don't process doc type '", mobjCpartner.DocTypeString, "' [", Convert.ToString(mobjCpartner.DocTypeEnum), "] for partner ", mobjMLAPDParterDataMgr.g_strPartner });
                        throw new Exception(strRaiseError);
                    case CPartner.EDocumentType.eAssignment:
                        mobjCpartner.StoreDocumentInPartnerDb();
                        break;
                }

                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new object[] { PROC_NAME, " Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        private void CPartner_SendHeartBeatRequest()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendHeartBeatRequest: " });
            XmlDocument xmlDocumentObj;
            XmlNode xmlNodeNetStat = null;
            string strWrappedXml = string.Empty,
                strPost2URL = string.Empty;
            bool g_blnDebugMode;
            try
            {
                mobjCEvents = new CEvents();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMPPGTransactions = new MPPGTransactions();

                g_blnDebugMode = mobjCEvents.IsDebugMode;
                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Preparing HeartBeat" }));

                //First load the HeartBeat template from disk.
                xmlDocumentObj = new XmlDocument();
                string strPath = string.Concat(new string[] { mobjMLAPDParterDataMgr.g_strSupportDocPath, "\\SGHeartBeat.XML" });
                mobjMdomUtils.LoadXmlFile(ref xmlDocumentObj, ref strPath, "SendAHeartBeat", "HeartBeat");
                SetHeaderSchtuff(xmlDocumentObj, "HB");

                xmlNodeNetStat = xmlDocumentObj.SelectSingleNode("//NetworkStatusTest");

                //Remove the received date/time - to be filled in by SG.
                xmlNodeNetStat.RemoveChild(xmlDocumentObj.SelectSingleNode("//ReceivedDate"));

                //Remove the reply date/time - to be filled in by SG.
                xmlNodeNetStat.RemoveChild(xmlDocumentObj.SelectSingleNode("//ReplyDate"));

                //Build the SOAP Envelope and put the HeartBeat into the envelope
                strWrappedXml = mobjMPPGTransactions.BuildTransactionForPPG("", xmlDocumentObj.OuterXml, mobjMLAPDParterDataMgr.g_strPartner, string.Concat(new string[] { mobjMLAPDParterDataMgr.g_strLynxID, "-", mobjMLAPDParterDataMgr.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mobjMPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);

                if (g_blnDebugMode)
                    mobjCEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL }), string.Concat(new string[] { PROC_NAME, "HeartBeat sent via HTTP" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                xmlDocumentObj = null;
                xmlNodeNetStat = null;
            }
        }

        /// <summary>
        /// This method transmits documents to the partner.
        /// </summary>
        /// <returns></returns>
        private string CPartner_TransferToPartner()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferToPartner: " });
            XmlNodeList xmlNodeListObj = null;
            XmlNode xmlNodeObj = null, xmlNodeVeh = null;
            XmlDocument xmlDocumentClaimVeh;
            bool g_blnDebugMode;
            long lngClaimAspectID;
            string strPostingUrl = string.Empty,
                strXML = string.Empty,
                strXmlPath = string.Empty;
            try
            {
                mobjCpartner = new CPartner();
                mobjCEvents = new CEvents();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();

                g_blnDebugMode = mobjCEvents.IsDebugMode;
                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new string[] { PROC_NAME, " Started." }));

                //Todo:
                //return "";

                if ((mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//ClaimMessage") == null) && (mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("////Document") == null))
                {
                    mobjCEvents.HandleEvent(Convert.ToInt32(Autoverse_Error_Codes.eMissingMessageOrDocumentNode), PROC_NAME, "Transaction must contain a ClaimMessage node or at least 1 Document node", "", "", false);
                    //ToDo
                }

                //Extract Header Schtuff(to quote carpenter)
                cstrSenderName = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//Header/SenderName").InnerText;
                mobjMLAPDParterDataMgr.g_strLynxID = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//Header/LynxID").InnerText;
                cstrClaimNumber = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//Header/ClaimNumber").InnerText;
                cstrLynxAPDInsCoID = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//Header/InsuranceCarrierID").InnerText;
                mobjMLAPDParterDataMgr.g_strVehicleNumber = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//Header/VehicleNumber").InnerText;
                strPostingUrl = mobjMLAPDParterDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                // objVehData = new VehData();
                objVehData.Year = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//Header/VehicleYear").InnerText;
                objVehData.Make = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//Header/VehicleMake").InnerText;
                objVehData.Model = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//Header/VehicleModel").InnerText;

                //We need to obtain the ClaimAspectID
                mobjMLAPDParterDataMgr.g_objDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);

                xmlDocumentClaimVeh = new XmlDocument();
                strXmlPath = mobjMLAPDParterDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp"), new object[] { "@ClientClaimNumber", cstrClaimNumber, "@InsuranceCompanyID", cstrLynxAPDInsCoID, "@SourceApplicationID", 6 });
                mobjMLAPDParterDataMgr.LoadXml(ref xmlDocumentClaimVeh, ref strXmlPath, string.Empty, string.Empty);
                strXmlPath = string.Empty;

                xmlNodeVeh = xmlDocumentClaimVeh.SelectSingleNode("//Vehicle");

                if (xmlNodeObj == null)
                    lngClaimAspectID = 0;
                else
                    lngClaimAspectID = Convert.ToInt64(xmlDocumentClaimVeh.SelectSingleNode("@ClaimAspectID").InnerText);

                xmlNodeVeh = null;
                xmlNodeVeh = null;

                if (lngClaimAspectID > 0)
                {
                    xmlDocumentClaimVeh = new XmlDocument();
                    strXmlPath = mobjMLAPDParterDataMgr.g_objDataAccessor.OpenRecordsetAsClientSideXML(mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/PassThruSP"), new object[] { mobjMLAPDParterDataMgr.g_strLynxID });
                    mobjMLAPDParterDataMgr.LoadXml(ref xmlDocumentClaimVeh, ref strXmlPath, string.Empty, string.Empty);
                    strXmlPath = string.Empty;

                    cstrSourceApplicationPassThruData = xmlDocumentClaimVeh.SelectSingleNode(string.Concat(new string[] { "//ClaimAspect[@ClaimAspectID='", Convert.ToString(lngClaimAspectID), "']/@SourceApplicationPassThruData" })).InnerText;
                }
                //Is there a Claim Message to send?

                if (mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//ClaimMessage").InnerText.Trim() != string.Empty)
                {
                    SendMessageToPartner(strPostingUrl, mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//ClaimMessage"));
                    mobjCpartner.SendWorkflowNotification("External Claim Message Sent", "Extenal Claim Message Sent to Partner");
                }

                xmlNodeListObj = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectNodes("//Document");

                if (xmlNodeListObj.Count > 0)
                {
                    foreach (XmlNode xmlDocNode in xmlNodeListObj)
                    {
                        TransferDocumentToPartner(strPostingUrl, xmlDocNode);
                    }

                    //Fire off event indicating that Documents were sent.
                    mobjCpartner.SendWorkflowNotification("Documents Sent To Carrier", "Documents Sent to Carrier");
                }

                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new string[] { PROC_NAME, " Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
            return "";
        }


        /// <summary>
        /// For assignment transaction CCC did not put a qualified namespace for the
        /// Assignment Root Element like they did for all other transactions rather they
        /// namespaced the document itself.  This caused havoc with styling
        /// </summary>
        /// <param name="strPostingUrl"></param>
        /// <param name="xmlDocNode"></param>
        private void StripDocNamespace()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "StripDocNameSpace:" }),
                strXML = string.Empty,
                strPartToRemove = string.Empty,
                strNamespaces = string.Empty;
            Int32 intStartPtr, intEndPtr;
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();

                strXML = mobjMLAPDParterDataMgr.g_objPassedDocument.OuterXml;

                intStartPtr = strXML.IndexOf(string.Concat(new string[] { "xmlns=", string.Empty }), 1, StringComparison.CurrentCulture);

                if (intStartPtr < 1)
                    return;

                intEndPtr = strXML.IndexOf(string.Empty, (intStartPtr + 7), StringComparison.CurrentCulture);

                strPartToRemove = this.Mid(strXML, intStartPtr, (intEndPtr - intStartPtr + 1));

                strXML = strXML.Replace(strPartToRemove, "");

                mobjMLAPDParterDataMgr.g_objPassedDocument = new XmlDocument();

                mobjMLAPDParterDataMgr.g_objPassedDocument.LoadXml(strXML);

                foreach (XmlAttribute objxmlAttr in mobjMLAPDParterDataMgr.g_objPassedDocument.Attributes)
                {
                    if (objxmlAttr.Name.Length >= 6)
                    {
                        if (this.Left(objxmlAttr.Name, 6) == "xmlns:")
                            strNamespaces = string.Concat(new string[] { strNamespaces, objxmlAttr.Name, "='", objxmlAttr.Value, "' " });
                    }
                }

                // TODO Set Property using XmlNameSpaceManager

                XmlNamespaceManager xmlNameSpaceManager = new XmlNamespaceManager(mobjMLAPDParterDataMgr.g_objPassedDocument.NameTable);
                xmlNameSpaceManager.AddNamespace("SelectionNamespaces", strNamespaces);
                // mobjMLAPDParterDataMgr.g_objPassedDocument.SetProperty("SelectionNamespaces", strNamespaces);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDocumentEMSData"></param>
        /// <returns></returns>
        private bool ValidateEMSData(XmlDocument xmlDocumentEMSData)
        {

            XmlDocument xmlDocumentValidateXsl;
            bool blnValidEMS = false;
            string strEMSIssues = string.Empty,
                strValue = string.Empty,
                strTrace = string.Empty,
                strResult = string.Empty,
                PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ValidateEMSData: " });
            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                mobjCEvents = new CEvents();
                //EMS only passes a 2 digit vehicle year -- this needs to be changed to a 4 digit year
                if (xmlDocumentEMSData.SelectSingleNode("//V_MODEL_YR").InnerText.Length > 0)
                {
                    strValue = xmlDocumentEMSData.SelectSingleNode("//V_MODEL_YR").InnerText.Trim();
                    double dNum;
                    bool isNum = double.TryParse(strValue, out dNum);
                    if (isNum)
                    {
                        if (Convert.ToInt32(strValue) > 50)
                            strValue = string.Concat(new string[] { "19", strValue });
                        else
                            strValue = string.Concat(new string[] { "20", strValue });

                        xmlDocumentEMSData.SelectSingleNode("//V_MODEL_YR").InnerText = strValue;
                    }
                }

                //Insure the Claim Number specified in EMS Payload is the same as the value in header
                if (cstrClaimNumber.Trim() != xmlDocumentEMSData.SelectSingleNode("//AD1/CLM_NO").InnerText.Trim())
                {
                    strEMSIssues = string.Concat(new string[] { strEMSIssues, mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/MismatchedClaimNumbers").Replace("%headerclaimnumber%", cstrClaimNumber).Replace("%emsclaimnumber%", xmlDocumentEMSData.SelectSingleNode("//AD1/CLM_NO").InnerText.Trim()) });
                    blnValidEMS = false;
                }
                string strPath = string.Concat(new string[] { mobjMLAPDParterDataMgr.g_strSupportDocPath, "\\AutoverseEMSValidate.xsl" });
                xmlDocumentValidateXsl = new XmlDocument();
                mobjMdomUtils.LoadXmlFile(ref xmlDocumentValidateXsl, ref strPath, PROC_NAME, "");

                //Todo: xsl

                if (xmlDocumentEMSData != null)
                {
                    using (StringReader sReader = new StringReader(xmlDocumentEMSData.OuterXml))
                    using (XmlReader xReader = XmlReader.Create(sReader))
                    using (StringWriter sWriter = new StringWriter())
                    using (XmlWriter xWriter = XmlWriter.Create(sWriter))
                    {
                        XslCompiledTransform objTransformXSL = new XslCompiledTransform();
                        objTransformXSL.Load(xmlDocumentValidateXsl);
                        objTransformXSL.Transform(xReader, xWriter);
                        strResult = sWriter.ToString();
                    }
                }

                strEMSIssues = string.Concat(new string[] { strEMSIssues, strResult });

                if (!strEMSIssues.Equals(string.Empty))
                {
                    string strTemplate = "AutoverseReceipt.XML";
                    SendAReceipt(ref strTemplate, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), strEMSIssues);
                    mobjCEvents.HandleEvent(Convert.ToInt32(Autoverse_Error_Codes.eAssignmentInvalid), PROC_NAME, strEMSIssues, "", "", false);
                    blnValidEMS = false;
                }

                goto ValidateEMSExit;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }

        ValidateEMSExit:
            return blnValidEMS;
        }

        private void TransferDocumentToPartner(string strPostingUrl, XmlNode xmlNodeObj)
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferToPartner: " });
            string strPartnerXml = string.Empty,
                  strTransactionID = string.Empty,
                  strInsCo = string.Empty,
                  strVehYr = string.Empty,
                  strVehMake = string.Empty,
                  strVehMode = string.Empty,
                  strXML = string.Empty,
                  strTemplate = string.Empty,
                  strDocType = string.Empty,
                  strDocumentID = string.Empty,
                  strTransType = string.Empty,
                  strEncodedFile = string.Empty;
            XmlDocument xmlDocumentPartner;
            DateTime dtNow;
            XmlCDataSection xmlCDataSectionObj;
            bool g_blnDebugMode;

            try
            {
                mobjCEvents = new CEvents();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMPPGTransactions = new MPPGTransactions();
                mobjCpartner = new CPartner();

                g_blnDebugMode = mobjCEvents.IsDebugMode;


                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new string[] { PROC_NAME, "-Preparing to send documents." }));

                dtNow = DateTime.Now;

                strDocumentID = xmlNodeObj.SelectSingleNode("@DocumentID").InnerText;
                strDocType = xmlNodeObj.SelectSingleNode("DocumentType").InnerText;
                strTransType = strDocType;

                switch (strDocType)
                {
                    case "DigitalImage":
                        strTemplate = "AutoverseDigitalImage.xml";
                        break;
                    case "EstimatePI":
                        strTemplate = "AutoversePrintImage.xml";
                        break;
                    default:
                        if (this.Left(strDocType, 8) == "Document")
                        {
                            strTransType = "Document";
                            strDocType = this.Mid(strDocType, 10, strDocType.Length);
                            strTemplate = "AutoverseDocumentXfer.xml";
                        }
                        else
                        {
                            //Todo:
                            strDocType = string.Empty;
                        }
                        break;
                }

                if (strDocType != string.Empty)
                {
                    mobjCEvents.Trace(string.Concat(new string[] { "Preparing to send Document ID#", strDocumentID, " {", strDocType, "}" }));

                    //Instantiate and Load Template from disk
                    xmlDocumentPartner = new XmlDocument();
                    string strPath = string.Concat(new string[] { mobjMLAPDParterDataMgr.g_strSupportDocPath, "\" & strTemplate" });
                    mobjMdomUtils.LoadXmlFile(ref xmlDocumentPartner, ref strPath, "TransferDocumentsToPartner", "Document Xfer");
                    strTransactionID = string.Concat(new string[] { dtNow.ToString("yyyyMMddhhnnssfff"), "#", strDocumentID, "#" });

                    //Populate all common elements
                    xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionID").Value = strTransactionID;
                    xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionDate").Value = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                    xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionTime").Value = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));
                    xmlDocumentPartner.SelectSingleNode("//InsuranceCarrierID").InnerText = strInsCo;
                    xmlDocumentPartner.SelectSingleNode("//ClaimNumber").InnerText = cstrClaimNumber;

                    objVehData = new VehData();

                    xmlDocumentPartner.SelectSingleNode("//VehicleYr").InnerText = objVehData.Year;
                    xmlDocumentPartner.SelectSingleNode("//VehicleMake").InnerText = objVehData.Make;
                    xmlDocumentPartner.SelectSingleNode("//VehicleModel").InnerText = objVehData.Model;

                    xmlDocumentPartner.SelectSingleNode("//DateTime/@Date").Value = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                    xmlDocumentPartner.SelectSingleNode("//DateTime/@Time").Value = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));

                    //Add in Souce Pass Thru Info
                    xmlCDataSectionObj = xmlDocumentPartner.CreateCDataSection(cstrSourceApplicationPassThruData);
                    xmlDocumentPartner.SelectSingleNode("//SourcePassThruData").AppendChild(xmlCDataSectionObj);

                    strEncodedFile = xmlNodeObj.SelectSingleNode("FilePath").InnerText;
                    // Now populate specific elements based on document type

                    switch (strDocType)
                    {
                        case "DigitalImage":
                            xmlDocumentPartner.SelectSingleNode("//ImageLength").InnerText = Convert.ToString(strEncodedFile.Length);
                            xmlDocumentPartner.SelectSingleNode("//FileName").InnerText = string.Concat(new string[] { "LYNX_", strDocumentID, ".jpg" });
                            xmlDocumentPartner.SelectSingleNode("//ImageFile").InnerText = strEncodedFile;
                            xmlDocumentPartner.SelectSingleNode("//EstimateIdentifier").InnerText = xmlNodeObj.SelectSingleNode("SequenceNumber").InnerText;
                            break;
                        case "EstimatePI":
                            xmlDocumentPartner.SelectSingleNode("//FileLength").InnerText = Convert.ToString(strEncodedFile.Length);
                            xmlDocumentPartner.SelectSingleNode("//ImageFile").InnerText = strEncodedFile;
                            xmlDocumentPartner.SelectSingleNode("//EstimateIdentifier").InnerText = xmlNodeObj.SelectSingleNode("SequenceNumber").InnerText;
                            break;
                        default:
                            xmlDocumentPartner.SelectSingleNode("//FileLength").InnerText = Convert.ToString(strEncodedFile.Length);
                            xmlDocumentPartner.SelectSingleNode("//DocumentFile").InnerText = strEncodedFile;
                            xmlDocumentPartner.SelectSingleNode("//DocumentType").InnerText = strDocType;
                            break;
                    }
                    //Wrap in SOAP
                    strXML = mobjMPPGTransactions.BuildTransactionForPPG(strTransType, xmlDocumentPartner.OuterXml, mobjMLAPDParterDataMgr.g_strPartner, string.Concat(new string[] { "D-", strDocumentID }));

                    //Smellin like Irish Spring off to PPG we go
                    mobjMPPGTransactions.XmlHttpPost(strPostingUrl, strXML);

                    mobjCpartner.SetDocumentSendTransactionID(strDocumentID, strTransactionID);

                    if (g_blnDebugMode)
                        mobjCEvents.Trace(string.Concat(new string[] { "URL = ", strPostingUrl }), string.Concat(new string[] { PROC_NAME, "-Document sent via HTTP" }));

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                xmlCDataSectionObj = null;
                xmlDocumentPartner = null;
                xmlNodeObj = null;
            }
        }

        private string EncodeFile(string strFileToEncode)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "EncodeFile:" });
            ADODB.Stream objStream = null;

            Byte[] buffer = null;
            int bufferSize;
            FileSystemObject fso;
            try
            {
                mobjCEvents = new CEvents();
                mobjMBase64Utils = new MBase64Utils();
                fso = new FileSystemObject();
                objStream = new ADODB.Stream();
                mobjCEvents.Assert(fso.FileExists(strFileToEncode), string.Concat(new string[] { "File specified, '", strFileToEncode, "', does not exist or can not be accessed." }));


                objStream.Type = ADODB.StreamTypeEnum.adTypeBinary;
                objStream.Open();

                objStream.LoadFromFile(strFileToEncode);
                bufferSize = objStream.Size;

                Array.Resize(ref buffer, bufferSize);

                BinaryFormatter binaryFormatter = new BinaryFormatter();
                MemoryStream memoryStream = new MemoryStream();
                binaryFormatter.Serialize(memoryStream, objStream.Read());
                buffer = memoryStream.ToArray();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (objStream == null)
                {
                    if (objStream.State != ADODB.ObjectStateEnum.adStateClosed)
                        objStream.Close();

                    objStream = null;
                }
            }
            return mobjMBase64Utils.EncodeHexBin(bufferSize, buffer);
        }

        private string ExtractDocumentIDFromTransactionID(string TransactionID)
        {
            //string PROC_NAME = "ExtractDocumentIDFromTransactionID:";
            Int32 intStartPtr, intEndPtr;
            string strDocumentID = string.Empty;
            try
            {
                intStartPtr = TransactionID.IndexOf("#", 1);

                if (intStartPtr > 0)
                {
                    intStartPtr = intStartPtr + 1;
                    if (intStartPtr < TransactionID.Length)
                    {
                        intEndPtr = TransactionID.IndexOf("#", intStartPtr);
                        return this.Mid(TransactionID, intStartPtr, (intEndPtr - intStartPtr)).Trim();
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return "";
        }

        private void HandleDocumentXferReceipt()
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "HandleDocumentXferReceipt:" });

            string strErrCode = string.Empty,
             strDocumentID = string.Empty,
             strTransactionID = string.Empty,
             strErrDesc = string.Empty,
             strDocSendStatus = string.Empty,
             strTrace = string.Empty;

            try
            {
                mobjCpartner = new CPartner();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjCEvents = new CEvents();

                strErrCode = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//ErrorCode").InnerText;
                strTransactionID = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//head:SenderTransactionID").InnerText;
                mobjCEvents.Assert((strTransactionID.Length > 0), "Header for NACK did not contain SenderTransactionID");

                strDocumentID = ExtractDocumentIDFromTransactionID(strTransactionID);

                if (strDocumentID.IndexOf("-") > 0)
                {
                    //this is an ack for claim msg that glaxis made from business event.

                }

                double dNum;
                bool isNum = double.TryParse(strDocumentID, out dNum);

                if (strDocumentID == string.Empty || !isNum)
                    throw new Exception(string.Concat(new string[] { Convert.ToString(Autoverse_Error_Codes.eInternalError), "Invalid Document ID returned in NACK SenderTransactionID" }));

                switch (Convert.ToInt16(strErrCode))
                {
                    case 0:
                        //ASK
                        strDocSendStatus = "S";
                        break;
                    default:
                        //NASK
                        strDocSendStatus = "NS";
                        strTrace = string.Concat(new string[] { "LynxID=", mobjMLAPDParterDataMgr.g_strLynxID, "  DocumentID=", strDocumentID });
                        //Send out notification to APD Production Monitoring
                        mobjCEvents.HandleEvent(Convert.ToInt16(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNackReceived), PROC_NAME, "Document/Message Failed to Send", strTrace, string.Empty, false);
                        mobjCpartner.UpdateDocumentSendStatus(strDocumentID, strDocSendStatus);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SendMessageToPartner(string strPostingUrl, XmlNode xmlMsgNode)
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendMessageToPartner:" });
            string strMessageText = string.Empty,
                objPartnerXml = string.Empty,
                strTransactionID = string.Empty,
                strSenderName = string.Empty,
                strXML = string.Empty,
                strDocumentID = string.Empty,
                  strPath = string.Empty;
            DateTime dtNow;
            XmlDocument xmlDocumentPartner = null;
            XmlCDataSection xmlCDataSectionObj;
            bool g_blnDebugMode;
            try
            {
                mobjCEvents = new CEvents();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                mobjMPPGTransactions = new MPPGTransactions();

                g_blnDebugMode = mobjCEvents.IsDebugMode;
                strMessageText = xmlMsgNode.InnerText;
                strDocumentID = xmlMsgNode.SelectSingleNode("@DocumentID").InnerText;

                dtNow = DateTime.Now;
                if (g_blnDebugMode)
                    mobjCEvents.Trace("", string.Concat(new string[] { PROC_NAME, "-Preparing Message Transaction" }));

                //Load Message Template from disk
                strPath = string.Concat(new string[] { mobjMLAPDParterDataMgr.g_strSupportDocPath, "\\AutoverseMessage.xml" });
                xmlDocumentPartner = new XmlDocument();
                mobjMdomUtils.LoadXmlFile(ref xmlDocumentPartner, ref strPath, "SendMessageToPartner", "Message");

                strTransactionID = string.Concat(new string[] { dtNow.ToString(""), strDocumentID });

                xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionID").Value = strTransactionID;
                xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionDate").Value = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                xmlDocumentPartner.DocumentElement.SelectSingleNode("@TransactionTime").Value = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));
                xmlDocumentPartner.SelectSingleNode("//InsuranceCarrierID").InnerText = cstrLynxAPDInsCoID;
                xmlDocumentPartner.SelectSingleNode("//ClaimNumber").InnerText = cstrClaimNumber;
                xmlDocumentPartner.SelectSingleNode("//VehicleYr").InnerText = objVehData.Year;
                xmlDocumentPartner.SelectSingleNode("//VehicleMake").InnerText = objVehData.Make;
                xmlDocumentPartner.SelectSingleNode("//VehicleModel").InnerText = objVehData.Model;
                xmlDocumentPartner.SelectSingleNode("//SenderName").InnerText = cstrSenderName;
                xmlDocumentPartner.SelectSingleNode("//MessageText").InnerText = strMessageText;
                xmlDocumentPartner.SelectSingleNode("//MessageLength").InnerText = Convert.ToString(strMessageText.Length);
                xmlDocumentPartner.SelectSingleNode("//DateTime/@Date").Value = mobjMLAPDParterDataMgr.ConstructXmlDate(Convert.ToString(dtNow));
                xmlDocumentPartner.SelectSingleNode("//DateTime/@Time").Value = mobjMLAPDParterDataMgr.ConstructXmlTime(Convert.ToString(dtNow));

                //Insert Pass Thru data into Transaction
                xmlCDataSectionObj = xmlDocumentPartner.CreateCDataSection(cstrSourceApplicationPassThruData);

                xmlDocumentPartner.SelectSingleNode("//SourcePassThruData").AppendChild(xmlCDataSectionObj);

                //Build SOAP Envelope
                strXML = mobjMPPGTransactions.BuildTransactionForPPG("Message", xmlDocumentPartner.OuterXml, mobjMLAPDParterDataMgr.g_strPartner, string.Concat(new string[] { mobjMLAPDParterDataMgr.g_strLynxID, "-", mobjMLAPDParterDataMgr.g_strVehicleNumber }));

                //Post da puppy
                mobjMPPGTransactions.XmlHttpPost(strPostingUrl, strXML);

                mobjCpartner.SetDocumentSendTransactionID(strDocumentID, strTransactionID);

                if (g_blnDebugMode)
                    mobjCEvents.Trace(string.Concat(new string[] { "URL = ", strPostingUrl }), string.Concat(new string[] { PROC_NAME, "-Message sent via HTTP" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                xmlDocumentPartner = null;
                strPath = string.Empty;
            }
        }

        private void ProcessNewAssignmentTransaction()
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessNewAssignmentTransaction:" });
            object objECAD = null,
             objTempEvents = null;
            bool blnValidShop, blnPossibleTotalLoss = false;
            long lngLynxShopID, lngClaimAspectID, lngPackageSize, lngNumber;
            byte[] buffer = null;
            XmlNode xmlNodeElem1 = null, xmlNodeVeh = null;
            XmlCDataSection xmlCDataSectionObj = null;
            XmlDocument xmlDocumentWithEmsXml = null,
                xmlDocumentEms = null,
                xmlDocumentAssignment = null,
                xmlDocumentAssignmentVerify = null, xmlDocumentClaimAspect = null, objTempPassedDocument = null;
            string strCRMId = string.Empty,
                strCCCShopID = string.Empty,
                strCarrierRepInfo = string.Empty,
                strHexEncodedEmsPackage = string.Empty,
                strAssignmentType = string.Empty,
                strLossState = string.Empty,
                strRFWarningMessage = string.Empty,
                strAssignVerify = string.Empty,
                strRFWarningNote = string.Empty,
                strError = string.Empty,
                strSource = string.Empty,
                strXmlPath = string.Empty,
                strTempPath = string.Empty;

            try
            {
                mobjCpartner = new CPartner();
                mobjCEvents = new CEvents();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMBase64Utils = new MBase64Utils();
                mobjMdomUtils = new Common.MDomUtils();
                mobjDataAccessor = new CDataAccessor();

                mobjCEvents.Trace("Processing 'New Assignment' transaction.");

                // mobjCEvents.Trace("Processing 'New Assignment' transaction.");
                // he assignment "data" comes in as a HEXBinary EMS Package embedded inside an XML transaction
                // In order for us to process this we'll need to extract that information, decode it, and convert
                // the resulting ems package into XML.

                strHexEncodedEmsPackage = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//EMSPayload").InnerText;
                mobjCEvents.Assert((strHexEncodedEmsPackage.Length > 0), "EMS Payload for Transaction was empty");

                lngPackageSize = mobjMBase64Utils.DecodeHexBin(strHexEncodedEmsPackage, buffer);

                xmlDocumentEms = mobjCpartner.ConvertEmsDBFToXml(ref buffer);

                if (mobjCEvents.IsDebugMode)
                    mobjCEvents.Trace(string.Concat(new string[] { "Ems Xml: ", xmlDocumentEms.OuterXml }));

                if (!ValidateEMSData(xmlDocumentEms))
                    return;

                switch (xmlDocumentEms.SelectSingleNode("//TLOS_IND").InnerText.ToLower())
                {
                    case "true":
                        blnPossibleTotalLoss = true;
                        break;
                    case "yes":
                        blnPossibleTotalLoss = true;
                        break;
                }
                //Now we need to replace the EMSPayLoad hexbin data with xml data
                xmlNodeElem1 = mobjMLAPDParterDataMgr.g_objPassedDocument.CreateElement(xmlDocumentEms.DocumentElement.Name);
                xmlDocumentEms.SelectSingleNode("//EMSPayload").InnerText = string.Empty;
                xmlDocumentEms.SelectSingleNode("//EMSPayload").AppendChild(xmlNodeElem1);
                xmlDocumentEms.SelectSingleNode("//EMSPayload").ReplaceChild(xmlDocumentEms.DocumentElement.CloneNode(true), xmlNodeElem1);

                xmlDocumentEms = null;

                //Add element indicating mapped APD Client ID
                xmlNodeElem1 = mobjMLAPDParterDataMgr.g_objPassedDocument.CreateElement("LynxInsCoID");
                xmlNodeElem1.InnerText = cstrLynxAPDInsCoID;
                mobjMLAPDParterDataMgr.g_objPassedDocument.AppendChild(xmlNodeElem1);

                //Prepare Assignment
                objTempPassedDocument = mobjMLAPDParterDataMgr.g_objPassedDocument;
                objTempEvents = mobjMLAPDParterDataMgr.g_objEvents;
                strTempPath = string.Concat(new string[] { mobjMLAPDParterDataMgr.g_strSupportDocPath, "\\AutoverseAssignmentXfer.xsl" });
                xmlDocumentAssignment = mobjMdomUtils.TransformDomAsDom(ref objTempPassedDocument, strTempPath, ref objTempEvents);// mobjMdomUtils.TransformDomAsDom(ref objTempPassedDocument, strTempPath, ref objTempEvents);

                mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);

                xmlDocumentAssignmentVerify = new XmlDocument();
                strXmlPath = mobjMLAPDParterDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/VerifyAssignmentType"), new object[] { "@InsuranceCompanyID", cstrLynxAPDInsCoID, "@AssignmentTypeID", xmlDocumentAssignment.SelectSingleNode("//Vehicle/@AssignmentTypeID").InnerText });
                mobjMdomUtils.LoadXml(ref xmlDocumentAssignmentVerify, ref strXmlPath, string.Empty, string.Empty);
                strAssignVerify = xmlDocumentAssignmentVerify.SelectSingleNode("//@result").InnerText;
                strXmlPath = string.Empty;

                if (strAssignVerify.ToLower() == "ok")
                {
                    //Assignment Type is not valid for this client
                    mobjCEvents.HandleEvent(Convert.ToInt32(Autoverse_Error_Codes.eAssignmentInvalid), PROC_NAME, strAssignVerify, string.Empty, string.Empty, false);
                    string strTemplateFile = "AutoverseReceipt.XML";
                    SendAReceipt(ref strTemplateFile, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), strAssignVerify);
                    return;
                }

                //Extract CCC's Shop ID from header of passed documents, retrieve Lynx's Shop Information from AutoverseID
                // Since CCC Does not pass us Loss State will assume the shop's state is loss state

                strLossState = string.Empty;

                strCCCShopID = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//head:AssignedToID").InnerText;
                if (strCCCShopID != string.Empty)
                {
                    XmlDocument objShopXml = new XmlDocument();
                    mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);
                    strXmlPath = mobjMLAPDParterDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/ShopInfoFromAutoverseID"), new object[] { "@AutoverseID", strCCCShopID, "@InsuranceCompanyID", cstrLynxAPDInsCoID });
                    mobjMdomUtils.LoadXml(ref objShopXml, ref strXmlPath, string.Empty, string.Empty);
                    strXmlPath = string.Empty;

                    if (objShopXml.DocumentElement.ChildNodes.Count == 0)
                        blnValidShop = false;
                    else
                        blnValidShop = (objShopXml.SelectSingleNode("//Shop/@AllowSelection").InnerText == "1");

                    if (!blnValidShop)
                    {
                        //Can not match the AutoverseID to a Lynx DRP
                        strRFWarningNote = string.Concat(new string[]
                       {
                           "Assignment was received with a shop selected that could not be mapped to a shop in Lynx DRP: ",
                           Environment.NewLine,
                           Environment.NewLine,
                           "Repair Facility Information Supplied with Assignment: ",
                           "   Autoverse ID: ",strCCCShopID,Environment.NewLine,
                           "           Name: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_CO_NM").InnerText,Environment.NewLine,
                           "        Address: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_ADDR1").InnerText,Environment.NewLine,
                           "City, State Zip: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_CITY").InnerText,", ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_ST").InnerText,", ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_ZIP").InnerText,Environment.NewLine,
                           "        Phone #: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_PH1").InnerText,Environment.NewLine,
                           "          Fax #: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_FAX").InnerText,Environment.NewLine,
                           "   Contact Name: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_CT_FN").InnerText," ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_CT_LN").InnerText
                       }
                        );

                        strRFWarningMessage = string.Concat(new string[]
                       {
                           "Received an Assignment from Autoverse for Insurance Company = ",cstrLynxAPDInsCoID," in which the Repair Facility could not be mapped to a Lynx DRP.",Environment.NewLine,
                           "Pertaining to Lynx ID#%lynxid%",
                           Environment.NewLine,
                           Environment.NewLine,
                           "Repair Facility Information: ",
                           Environment.NewLine,
                           "   Autoverse ID: ",strCCCShopID,Environment.NewLine,
                           "           Name: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_CO_NM").InnerText,Environment.NewLine,
                           "        Address: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_ADDR1").InnerText,Environment.NewLine,
                           "City, State Zip: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_CITY").InnerText,", ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_ST").InnerText,", ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_ZIP").InnerText,Environment.NewLine,
                           "        Phone #: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_PH1").InnerText,Environment.NewLine,
                           "          Fax #: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_FAX").InnerText,Environment.NewLine,
                           "   Contact Name: ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_CT_FN").InnerText," ",mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_CT_LN").InnerText
                       }
                        );

                        strLossState = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/RF_ST").InnerText;
                        xmlDocumentAssignment.SelectSingleNode("//Vehicle/@ShopLocationID").Value = mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/DefaultDRPShopID");
                    }
                    else
                    {
                        //Set the Shop Location ID
                        xmlDocumentAssignment.SelectSingleNode("//Vehicle/@ShopLocationID").Value = objShopXml.SelectSingleNode("//Shop/@ShopLocationID").InnerText;
                        strLossState = objShopXml.SelectSingleNode("//Shop/@AddressState").InnerText;
                    }
                    objShopXml = null;
                }

                if (strLossState == string.Empty)
                {
                    //Either this is not a program shop assignment or shop information did not have a state?
                    if (mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/LOC_ST").InnerText.Trim() != string.Empty)
                    {
                        //Use vehicle's current location state as loss state
                        strLossState = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD2/LOC_ST").InnerText.Trim();
                    }
                    else if (mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD1/INSD_ST").InnerText.Trim() != string.Empty)
                    {
                        //Use insured's state as loss state
                        strLossState = mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//AD1/INSD_ST").InnerText.Trim();
                    }
                    else
                    {
                        //throw nack
                        string strTemplateFile = "AutoverseReceipt.XML";
                        SendAReceipt(ref strTemplateFile, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/UndeterminableLossState"));
                        mobjCEvents.HandleEvent(Convert.ToInt32(Autoverse_Error_Codes.eAssignmentInvalid), PROC_NAME, "Received assignment where Loss State could not be determined.  NACK sent.");
                        return;
                    }

                }

                xmlDocumentAssignment.SelectSingleNode("//Claim/@LossAddressState").Value = strLossState;

                // Apparently one possible impact code EMS has is a total loss code.
                // if after translations we have this then we need to NAK

                if (xmlDocumentAssignment.SelectSingleNode("//Vehicle/@ImpactLocations").InnerText.IndexOf("TL", 1) > 0)
                {
                    //TL was indicated -- we will still accept the assignment but we need to indicate that they indicate this could be a total loss
                    blnPossibleTotalLoss = true;

                    //Need to strip out the 'TL'
                    xmlDocumentAssignment.SelectSingleNode("//Vehicle/@ImpactLocations").Value = StripTotalLossImpactPoint(xmlDocumentAssignment.SelectSingleNode("//Vehicle/ImpactLocations").InnerText);
                }

                if (xmlDocumentAssignment.SelectSingleNode("//Claim/@CarrierRepLogin").InnerText.ToUpper() == string.Empty ||
                    xmlDocumentAssignment.SelectSingleNode("/Claim/@CarrierRepNameFirst").InnerText.ToUpper() == string.Empty ||
                    xmlDocumentAssignment.SelectSingleNode("//Claim/@CarrierRepNameLast").InnerText.ToUpper() == string.Empty ||
                    xmlDocumentAssignment.SelectSingleNode("//Claim/@CarrierRepEmailAddress").InnerText.ToUpper() == string.Empty)
                {
                    //no claim rep information sent. Build a string so we can add a note/notify
                    strCarrierRepInfo = BuildCarrierRepInfo();

                    //now lets use the default user as the source of this claim.
                    xmlDocumentAssignment.SelectSingleNode("//Claim/@CarrierRepLogin").InnerText = cstrLynxAPDInsCoDefaultAdjLogon;
                }

                xmlDocumentAssignment.SelectSingleNode("//Claim/@FNOLUserID").InnerText = Convert.ToString(clngAutoverseUserID);

                // Error             
                mobjMLAPDParterDataMgr.g_strLynxID = Convert.ToString((mobjECAD.ApdLoadAssignmentExternal(xmlDocumentAssignment.OuterXml, mobjMLAPDParterDataMgr.g_strPartner)));
                // line no 820 to 940

                mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);
                xmlDocumentClaimAspect = new XmlDocument();
                strXmlPath = mobjMLAPDParterDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp"), new object[] { "@ClientClaimNumber", cstrClaimNumber, "@InsuranceCompanyID", cstrLynxAPDInsCoID, "@SourceApplicationID", 6 });
                mobjMdomUtils.LoadXml(ref xmlDocumentClaimAspect, ref strXmlPath, string.Empty, string.Empty);
                xmlNodeVeh = xmlDocumentClaimAspect.SelectSingleNode("//Vehicle");
                strXmlPath = string.Empty;

                if (strCarrierRepInfo.Length > 0)
                {
                    //Error
                    if (xmlNodeVeh != null)
                    {
                        //Insert Note into claim indicating that claim may be possible total
                        mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);
                        mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), new object[] {
                               "@ClaimAspectID", xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText,
                               "@NoteTypeID", 1,
                               "@StatusID", xmlNodeVeh.SelectSingleNode("@StatusID").InnerText,
                               "@Note", strCarrierRepInfo,
                               "@UserID", clngAutoverseUserID});

                        // Error line 1020 to1070
                    }
                }

                mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);
                if (blnPossibleTotalLoss)
                {
                    //Insert Note into claim indicating that claim may be possible total
                    mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), new object[] {
                                 "@ClaimAspectID", xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText,
                                 "@NoteTypeID", 1,
                                 "@StatusID", xmlNodeVeh.SelectSingleNode("@StatusID").InnerText,
                                 "@Note", strCarrierRepInfo,
                                 "@UserID", clngAutoverseUserID});
                }

                if (strRFWarningMessage.Trim().Length > 0)
                {
                    mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), new object[]{
                                "@ClaimAspectID", xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText,
                                "@NoteTypeID", 1,
                                "@StatusID", xmlNodeVeh.SelectSingleNode("@StatusID").InnerText,
                                "@Note", strCarrierRepInfo,
                                "@UserID", clngAutoverseUserID});
                }

                //Send back and AdjAssigned business event
                SendBusinessEvent("Claim Assigned", "", "Y", "");

                //Repair Facility specified could not be mapped.  Sent out Notification
                if (strRFWarningMessage != string.Empty)
                    mobjCEvents.HandleEvent(Convert.ToInt32(Autoverse_Error_Codes.eInternalError), PROC_NAME, strRFWarningMessage.Replace("%lynxid%", mobjMLAPDParterDataMgr.g_strLynxID), string.Empty, string.Empty, false);


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }

        private void ProcessCancelAssignmentRequest()
        {

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessCancelAssignmentRequest:" });
            XmlDocument xmlDocumentClaimVeh;
            XmlNode xmlNodeVeh;
            string strError = string.Empty,
                strSource = string.Empty,
                strFrom = string.Empty,
                strSubject = string.Empty,
                strBody = string.Empty,
                strXmlPath = string.Empty;
            long lngClaimAspectID, lngStatusID;

            try
            {
                mobjCEvents = new CEvents();
                mobjDataAccessor = new CDataAccessor();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();

                mobjCEvents.Trace(string.Concat(new string[] { "Processing Cancellation request for LynxID: ", mobjMLAPDParterDataMgr.g_strLynxID }));
                strSubject = "Received Cancellation Request through Autoverse for Claim";
                strBody = string.Concat(new string[] { "LYNX has received the cancellation request for Claim #:", cstrClaimNumber });

                //For cancellation we'll firs
                mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);

                //Retrieve Claim Aspect
                strXmlPath = mobjMLAPDParterDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp"), new object[] { "@ClientClaimNumber", cstrClaimNumber, "@InsuranceCompanyID", cstrLynxAPDInsCoID, "@SourceApplicationID", 6 });
                xmlDocumentClaimVeh = new XmlDocument();
                mobjMdomUtils.LoadXml(ref xmlDocumentClaimVeh, ref strXmlPath, string.Empty, string.Empty);
                xmlNodeVeh = xmlDocumentClaimVeh.SelectSingleNode("//Vehicle");
                lngClaimAspectID = Convert.ToInt64(xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText);

                if (lngClaimAspectID == 0)
                {
                    string strTemplateFile = "AutoverseReceipt.XML";
                    SendAReceipt(ref strTemplateFile, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/CancelClaimNoClaimFound"));
                    mobjCEvents.Trace("Received cancellation for claim which does not exist.", PROC_NAME);
                    return;
                }
                lngStatusID = Convert.ToInt64(xmlNodeVeh.SelectSingleNode("@StatusID").InnerText);
                mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringStd);

                // Error
                mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), new object[] {
                                 "@ClaimAspectID", Convert.ToString(lngClaimAspectID),
                                 "@NoteTypeID", 1,
                                 "@StatusID", Convert.ToString(lngStatusID),
                                 "@Note", strSubject,
                                 "@UserID", clngAutoverseUserID});

                mobjCpartner.SendWorkflowNotification("ExternalClaimMessageReceived", strSubject, "", "VEH");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ProcessClaimMessage()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ProcessClaimMessage:" });
            XmlDocument xmlDocumentClaimVeh;
            XmlNode xmlNodeVeh;
            string strError = string.Empty,
                strSource = string.Empty,
                strFrom = string.Empty,
                strSubject = string.Empty,
                strBody = string.Empty,
                strXmlPath = string.Empty;
            long lngClaimAspectID, lngStatusID;

            try
            {
                mobjCpartner = new CPartner();
                mobjDataAccessor = new CDataAccessor();
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                mobjMdomUtils = new Common.MDomUtils();
                mobjCEvents = new CEvents();

                mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringXML);

                //Retrieve Claim Aspect
                strXmlPath = mobjMLAPDParterDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/ClaimNumberCheckByApp"), new object[] { "@ClientClaimNumber", cstrClaimNumber, "@InsuranceCompanyID", cstrLynxAPDInsCoID, "@SourceApplicationID", 6 });
                xmlDocumentClaimVeh = new XmlDocument();
                mobjMdomUtils.LoadXml(ref xmlDocumentClaimVeh, ref strXmlPath, string.Empty, string.Empty);
                strXmlPath = string.Empty;

                xmlNodeVeh = xmlDocumentClaimVeh.SelectSingleNode("//Vehicle");

                lngClaimAspectID = Convert.ToInt64(xmlNodeVeh.SelectSingleNode("@ClaimAspectID").InnerText);

                if (lngClaimAspectID == 0)
                {
                    string strTemplateFile = "AutoverseReceipt.XML";
                    SendAReceipt(ref strTemplateFile, "N", Convert.ToString(Autoverse_Error_Codes.eAssignmentInvalid), mobjMLAPDParterDataMgr.GetConfig("PartnerSettings/CCAV/NACKMessages/CancelClaimNoClaimFound"));
                    mobjCEvents.Trace("Received Claim Message for claim which does not exist.", PROC_NAME);
                    return;
                }
                lngStatusID = Convert.ToInt64(xmlNodeVeh.SelectSingleNode("@StatusID").InnerText);

                //Build Note text
                strBody = string.Concat(new string[]{
                    "Sender: " ,mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//SenderName").InnerText,
                    Environment.NewLine ,
                    "Subject: " ,mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//SubjectLine").InnerText,
                    Environment.NewLine,Environment.NewLine,
                    mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//MessageText").InnerText});


                // Error
                mobjDataAccessor.SetConnectString(mobjMLAPDParterDataMgr.g_strLYNXConnStringStd);
                mobjDataAccessor.ExecuteSpNamedParams(mobjMLAPDParterDataMgr.GetConfig("ClaimPoint/SPMap/NoteInsDetail"), new object[] {
                                "@ClaimAspectID", Convert.ToString(lngClaimAspectID),
                                "@NoteTypeID", 1,
                                "@StatusID", Convert.ToString(lngStatusID),
                                "@Note", strBody,
                                "@UserID", clngAutoverseUserID});

                mobjCpartner.SendWorkflowNotification("ExternalClaimMessageReceived", string.Concat(new string[]{"External Message with subject '",
                       mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//SubjectLine").InnerText,"' received"}), "", "VEH");


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private string StripTotalLossImpactPoint(string strImpactPoints)
        {
            string[] strImpactPointsList;
            string strNewImpactPoints = string.Empty;
            Int32 intPtr;
            try
            {
                strImpactPointsList = strImpactPoints.Split(',');

                for (intPtr = strImpactPointsList.GetLowerBound(0); intPtr < strImpactPointsList.GetUpperBound(0); intPtr++)
                {
                    if (strImpactPointsList[intPtr] != "TL")
                    {
                        strNewImpactPoints = string.Concat(new string[] { strNewImpactPoints, strImpactPointsList[intPtr] });

                        if (intPtr < strImpactPointsList.GetUpperBound(0))
                            strNewImpactPoints = string.Concat(new string[] { strNewImpactPoints, "," });
                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strNewImpactPoints;
        }

        private string BuildCarrierRepInfo()
        {
            string strCarrierRepInfo = string.Empty;

            try
            {
                mobjMLAPDParterDataMgr = new MLAPDPartnerDataMgr();
                strCarrierRepInfo = string.Concat(new string[]{"Adjuster Information: ",Environment.NewLine,Environment.NewLine,
                  mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//CLM_CT_FN").InnerText,Environment.NewLine,
                  mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//CLM_CT_ADDR1").InnerText,Environment.NewLine,
                  mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("/CLM_CT_ADDR2").InnerText,Environment.NewLine,
                  mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//CLM_CITY").InnerText,", ",
                  mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//CLM_ST").InnerText,"  ",
                  mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//CLM_ZIP").InnerText,Environment.NewLine,
                  mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//CLM_CT_PH").InnerText});

                if (mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//CLM_CT_PHX").InnerText.Length > 0)
                    strCarrierRepInfo = string.Concat(new string[] { strCarrierRepInfo, " x", mobjMLAPDParterDataMgr.g_objPassedDocument.SelectSingleNode("//CLM_CT_PHX").InnerText });

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strCarrierRepInfo;
        }


        private bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransferV2:" });
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private string CPartner_ApdDatabaseTransferV2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransferV2:" });

            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private bool CPartner_PartnerDatabaseTransferV2(string strTradingPartner, string strPassedDocument)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocumentV2:" });
            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private long CPartner_PartnerDatabaseTransferV2(string strXML, long lngPartnerTransID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertPrintImageIntoAPDV2:" });

            try
            {
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Common Methods

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }
        #endregion

    }
}
