﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    class MFlatfile
    {
        #region Declarations


        /// <summary>
        /// Component PartnerDataMgr: Module MFlatfile
        /// 
        /// Flatfile utility functions.
        /// </summary>

        private const string APP_NAME = "LAPDPartnerDataMgr";
        string MODULE_NAME = string.Concat(new string[] { APP_NAME, "MFlatfile." });
        MLAPDPartnerDataMgr mlapdPartnerDataMgr = null;
        MBase64Utils objMBase64Utils = null;
        #endregion

        #region Public Functions

        /// <summary>
        /// Syntax:      ConvertTextToXML "PrintImage", 66, g_objEvents
        /// 
        /// Parameters:  strFlatfile - The flatfile data to convert.
        ///              strRootNode - The node name for the root node.
        ///              intLinesPerPage - lines per page in the flat file.
        ///              objEvents - the SiteUtilities.CEvents class from the parent.
        /// 
        /// Purpose:     Chops up a flatfile by pages and saves the pages as XML.
        /// 
        /// Returns:     Nothing.
        /// </summary>

        public void ConvertFlatfileToXML(string enmDocType, string strFlatfile, string strRootNode, string intLinesPerPage, string strDocumentType, string strFileName)
        {
            string strXML, strPath, strTemp, iCurPagePos, strCurChar, strCurLine;
            Int32 iPos = 0, iLineLen = 0, iCurLinePos = 0;

            try
            {
                string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ConvertFlatfileToXML: " });

                mlapdPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMBase64Utils = new MBase64Utils();

                //Add a trace message to note start of method.
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    mlapdPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { string.Empty, PROC_NAME, "Started", Environment.NewLine }));

                strPath = (mlapdPartnerDataMgr.GetConfig(string.Concat(new string[] { ("Document/RootDirectory"), strFileName })).Replace("\\\\", "\\"));

                strTemp = strFlatfile.Trim();
                iLineLen = 79;
                iCurPagePos = "1";
                iCurLinePos = 1;
                strCurLine = "";

                //Add root document element.
                strXML = string.Concat(new string[] { "<", strRootNode, ">", Environment.NewLine });
                strXML = string.Concat(new string[] { strXML, "<Page Num='", iCurPagePos, "'><![CDATA[", Environment.NewLine });

                for (iPos = 1; iPos <= strTemp.Length; iPos++)
                {
                    strCurChar = strTemp.Substring(iPos, 1);

                    if (Convert.ToInt32(strCurChar) >= 32)      //TODO:Need to verify Ascii Conversion..
                    {
                        strCurLine = strCurLine + strCurChar;
                        if (strCurLine.Length >= iLineLen)
                        {
                            strXML = string.Concat(new string[] { strXML, strCurLine, Environment.NewLine });
                            strCurLine = string.Empty;
                            iCurLinePos = iCurLinePos + 1;

                            if (iCurLinePos >= Convert.ToInt32(intLinesPerPage))
                            {
                                iCurPagePos = iCurPagePos + 1;
                                strXML = string.Concat(new string[] { strXML, Environment.NewLine, "]]></Page><Page Num=\"", iCurPagePos, "\"><![CDATA[", Environment.NewLine });
                                iCurLinePos = 0;
                            }
                        }
                    }
                }

                strXML = string.Concat(new string[] { strXML, "</", strRootNode, ">", Environment.NewLine });

                //Now write the data out to disk.
                objMBase64Utils.WriteTextDataToFile(strPath, strXML);

                //Add another trace message to note exit?
                if (mlapdPartnerDataMgr.g_blnDebugMode)
                    mlapdPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Return = ", MODULE_NAME, " - ", ",", PROC_NAME, " finished!" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Syntax:      ConvertXMLToFlatfile "PrintImage", 66, g_objEvents
        /// 
        /// Parameters:  strFlatfile - The flatfile data to convert.
        ///              strRootNode - The node name for the root node.
        ///              intLinesPerPage - lines per page in the flat file.
        ///              objEvents - the SiteUtilities.CEvents class from the parent.
        /// 
        /// Purpose:     Chops up a flatfile by pages and saves the pages as XML.
        /// </summary>
        /// Returns:     Nothing.
        /// <param name="strXML"></param>
        /// <returns></returns>
        public string ConvertXMLToFlatfile(string strXML)
        {
            string strResult = string.Empty;
            int intStart, lngEnd;

            try
            {
                intStart = 1;
                const string strStart = "<![CDATA[";
                const string strEnd = "]]>";

                intStart = strXML.IndexOf(strStart, 1);
                while (intStart != 0)
                {
                    lngEnd = strXML.IndexOf(strEnd, intStart);
                    intStart = intStart + strStart.Length;

                    strResult = string.Concat(new string[] { strResult, strXML.Substring(intStart, Convert.ToInt32(lngEnd) - intStart) });
                    intStart = strXML.IndexOf(strStart);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strResult;
        }

        #endregion
    }
}
