﻿using System;
using System.Xml;
using SiteUtilities;


namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    public class CADP
    {
        #region Variables
        private string MODULE_NAME = "LAPDPartnerDataMgr.CADP.";
        CPartner mobjPartner = null;
        private Common.MDomUtils mobjMDomUtils = null;
        private MLAPDPartnerDataMgr mobjPartnerDataMgr = null;
        private CEvents mobjCEvents = null;
        XmlDocument mobjPassedDocument;
        #endregion

        #region Enum
        //Error codes private to this module.
        private enum APD_Error_Codes : ulong
        {
            eNull = MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CAPD_CLS_ErrorCodes
        }

        //These are error codes that will come
        //back in negative acknowledgements from APD.
        private enum CAPD_NACK_ErrorCodes
        {
            eNullNack
        }
        #endregion

        #region Methods
        private long CPartner_InsertPrintImageIntoAPDV2(string strXML, long lngPartnerTransID)
        {
            long InsertPrintImage = 0;
            try
            {
                mobjPartner = new CPartner();
                InsertPrintImage = mobjPartner.InsertPrintImageIntoAPDV2(strXML, lngPartnerTransID);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return InsertPrintImage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransferV2:" });

            bool isPartnerDatabaseTransfer = false;
            try
            {
                mobjPartner = new CPartner();
                mobjCEvents = new CEvents();

                switch (mobjPartner.DocTypeEnum)
                {

                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.StoreDocumentInPartnerDb_v2();
                        break;
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ExtractSequenceNumber_v2("//EstimateIdentifier", string.Empty);
                        mobjPartner.StorePrintImageInPartnerDB_v2();
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                        mobjPartner.ExtractSequenceNumber_v2("//TRANS_TYPE", "//SUPP_NO");
                        mobjPartner.StoreEstimateDataInPartnerDB_v2();
                        if (blnSendReceipt)
                            mobjPartner.SendAReceipt_v2("ADPReceipt.XML");
                        break;
                    default:
                        mobjPartner.StoreDocumentInPartnerDb_v2();
                        break;
                }

                //An ADP transaction was missing CLIENT_PASSTHRU_DATA.  To ahead and store
                //it in udb_partner and notify development to correct the XML and resubmit it.
                if (mobjPartner.LynxID == "UNK" && mobjPartner.VehicleNumber == "UNK" && mobjPartner.AssignmentID == "0")
                {
                    mobjCEvents.HandleEvent(Convert.ToInt32(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eLynxIdUnknown), PROC_NAME, Convert.ToString(mobjPartner.PartnerTransID), "", "", false);
                    isPartnerDatabaseTransfer = false;
                    //Do not process in APD.
                }
                else
                {
                    isPartnerDatabaseTransfer = true;
                    //Process in APD.
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isPartnerDatabaseTransfer;
        }

        //TODO
        public string CPartner_ApdDatabaseTransferV2()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransferV2: " });
            string strApdDatabaseTransfer = string.Empty;
            try
            {
                mobjPartner = new CPartner();
                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        mobjPartner.ProcessReceipt_v2("//FailureReasonCode", "//FailureReasonText");
                        break;
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.ProcessBinaryFiles_v2(ref mobjPassedDocument, "//DigitalImageFile", "//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", "PDF", mobjPartner.DocTypeString, CPartner.EDocumentType.eDigitalImage);
                        mobjPartner.SendWorkflowNotification_v2("ElectronicDocumentReceived", string.Concat(new string[] { "SHOP (", mobjPartner.TradingPartner, "): Digital images received for Vehicle ", Convert.ToString(mobjPartner.VehicleNumber) }));
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ProcessEstimate_v2(ref mobjPartner);
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return strApdDatabaseTransfer;
        }

        public bool CPartner_InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            bool islInitProcessDocument = false;
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocumentV2: " });

            try
            {
                mobjPartner = new CPartner();
                mobjMDomUtils = new Common.MDomUtils();
                mobjPartnerDataMgr = new MLAPDPartnerDataMgr();

                mobjPartner.TradingPartner = strTradingPartner;
                mobjPartner.PassedDocument = strPassedDocument;

                mobjPassedDocument = new XmlDocument();
                mobjMDomUtils.LoadXml(ref mobjPassedDocument, ref strPassedDocument, string.Empty, string.Empty);

                //Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = mobjPartnerDataMgr.GetChildNodeText(ref mobjPassedDocument, "/ADPTransaction/@TransactionType", true);

                //Extract the LYNXID and Vehicle number
                mobjPartner.ExtractLynxIDAndVehNum_v2();

                //Are we configured to process this document at this time?
                islInitProcessDocument = mobjPartner.CanProcessDocument_v2();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            return islInitProcessDocument;
        }

        /// <summary>
        /// This method transmits documents to the partner.
        /// </summary>
        public string CPartner_TransferToPartner()
        {
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "TransferToPartner()", "Method not implemented" }));
        }

        /// <summary>
        /// This method initializes processing of the documents.
        /// Returns True if processing should continue.
        /// </summary>
        public bool CPartner_InitProcessDocument()
        {
            bool isInitProcessDocument = false;
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocument: " });
            try
            {
                mobjPartner = new CPartner();
                mobjPartnerDataMgr = new MLAPDPartnerDataMgr();

                //Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = mobjPartnerDataMgr.GetChildNodeText(ref mobjPartnerDataMgr.g_objPassedDocument, "/ADPTransaction/@TransactionType", true);

                //Extract the LYNXID and Vehicle number
                mobjPartner.ExtractLynxIDAndVehNum();

                //Are we configured to process this document at this time?
                isInitProcessDocument = mobjPartner.CanProcessDocument();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return isInitProcessDocument;
        }

        /// <summary>
        /// This method processing documents to the partner database.
        /// </summary>
        /// <param name="blnSendReceipt"></param>
        /// <returns></returns>
        public bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)
        {
            bool isPartnerDatabaseTransfer = false;
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransfer: " });

            try
            {
                mobjPartner = new CPartner();
                mobjPartnerDataMgr = new MLAPDPartnerDataMgr();

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.StoreDocumentInPartnerDb();
                        break;
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ExtractSequenceNumber("//EstimateIdentifier", string.Empty);
                        mobjPartner.StorePrintImageInPartnerDB();
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                        mobjPartner.ExtractSequenceNumber("//TRANS_TYPE", "//SUPP_NO");
                        mobjPartner.StoreEstimateDataInPartnerDB();
                        if (blnSendReceipt)
                            mobjPartner.SendAReceipt("ADPReceipt.XML");
                        break;
                    default:
                        mobjPartner.StoreDocumentInPartnerDb();
                        break;
                }

                //An ADP transaction was missing CLIENT_PASSTHRU_DATA.  To ahead and store
                //it in udb_partner and notify development to correct the XML and resubmit it.

                if (mobjPartnerDataMgr.g_strLynxID == "UNK" && mobjPartnerDataMgr.g_strVehicleNumber == "UNK" && mobjPartnerDataMgr.g_strAssignmentID == "0")
                {
                    mobjPartnerDataMgr.g_objEvents.HandleEvent(Convert.ToInt32(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eLynxIdUnknown), PROC_NAME, Convert.ToString(mobjPartner.PartnerTransID), "", "", false);
                    isPartnerDatabaseTransfer = false;
                    //Do not process in APD.
                }
                else
                {
                    isPartnerDatabaseTransfer = true;
                    //Process in APD.
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isPartnerDatabaseTransfer;
        }

        /// <summary>
        /// This method processing documents to the APD database.
        /// </summary>
        //TODO
        public string CPartner_ApdDatabaseTransfer()
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransfer: " });
            string strApdDatabaseTransfer = string.Empty;
            try
            {
                mobjPartner = new CPartner();
                mobjPartnerDataMgr = new MLAPDPartnerDataMgr();

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        mobjPartner.ProcessReceipt("//FailureReasonCode", "//FailureReasonText");
                        break;
                    case CPartner.EDocumentType.eDigitalImage:
                        //mobjPartner.ProcessBinaryFiles g_objPassedDocument, "//DigitalImageFile", _
                        //"//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", _
                        //"PDF", mobjPartner.DocTypeString, eDigitalImage

                        mobjPartner.ProcessBinaryFiles(ref mobjPassedDocument, "//DigitalImageFile", "//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", "PDF", mobjPartner.DocTypeString, CPartner.EDocumentType.eDigitalImage);
                        mobjPartner.SendWorkflowNotification("ElectronicDocumentReceived", string.Concat(new string[] { "SHOP (", mobjPartnerDataMgr.g_strPartner, "): Digital images received for Vehicle ", Convert.ToString(mobjPartnerDataMgr.g_strVehicleNumber) }));
                        break;
                    case CPartner.EDocumentType.eEstimateData:
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ProcessEstimate(ref mobjPartner);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strApdDatabaseTransfer;
        }

        /// <summary>
        /// Purpose: Inserts an estimate print image into the APD document system.
        /// Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        public long CPartner_InsertPrintImageIntoAPD(string strXML, long lngPartnerTransID)
        {
            mobjPartner = new CPartner();
            return mobjPartner.InsertPrintImageIntoAPD(strXML, lngPartnerTransID);
        }

        /// <summary>
        /// Send a HeartBeat request to Partner.
        /// </summary>
        public void CPartner_SendHeartBeatRequest()
        {
            throw new Exception(string.Concat(new string[] { Convert.ToString(MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented), ",", MODULE_NAME, "SendHeartBeatRequest()", "Method not implemented" }));
        }
        #endregion
    }
}
