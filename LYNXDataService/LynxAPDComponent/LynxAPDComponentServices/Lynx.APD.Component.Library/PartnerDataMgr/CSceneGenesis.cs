﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Lynx.APD.Component.Library;
using MSXML2;
using LAPDEcadAccessorMgr;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    class CSceneGenesis
    {
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        private string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CSceneGenesis." });
        private CPartner mobjPartner = null;
        private MLAPDPartnerDataMgr mLAPDPartnerDataMgr = null;
        private MPPGTransactions mPPGTransactions = null;
        private MEscapeUtilities mEscapeUtilities = null;
        private MBase64Utils mBase64Utils = null;
        private MFlatfile mFlatfile = null;
        private Common.MDomUtils mDomUtils = null;
        LAPDEcadAccessorMgr.CWebAssignment mCWebAssignment = null;
        // private EcadAccessorMgr.CWebAssignment mCWebAssignment = new EcadAccessorMgr.CWebAssignment();

        //  private MLAPDEcadAccessorMgr mLAPDEcadAccessorMgr = new MLAPDEcadAccessorMgr();

        /// <summary>
        /// Error codes private to this module.
        /// </summary>
        private enum SceneGenesis_Error_Codes : ulong
        {
            eBadAssignmentCode = MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CSceneGenesis_CLS_ErrorCodes,
            eAssignmentInvalid,
            eAttachmentIdNotFound,
            eNoPassThruData,
            eBadPassThruDataFormat,
            eUnhandledSceneGenesisNack,
            eBlankCefReturnedForEstimate,
            eXmlTransactionTooLarge
        }

        /// <summary>
        /// These are error codes that will come back in negative acknowledgements from SceneGenesis. 
        /// </summary>
        private enum CSceneGenesis_NACK_ErrorCodes
        {
            eNullNack,
            eUnknownError = 1,
            eXmlParsingError = 2,
            eXmlDocNotFound = 3,
            eXmlAttributeError = 4,
            eXmlNodeTextError = 5,
            eTransRecordingError = 6,
            eTaskLoadError = 7,
            eTransIdNotFound = 8,
            eReceiptLoggingError = 9,
            eStatusTestError = 10,
            eBizEventError = 11,
            eResetTaskError = 12,
            eXmlCDataWriteError = 13,
            eXmlBase64WriteError = 14,
            eItemNotFoundInFolder = 15,
            eEmsDataError = 16,
            eUserDataError = 17,
            eDocumentSendError = 101,
            eServiceTableError = 102,
            eFolderCreationError = 103,
            ePackMessageError = 104,
            eUnPackMessageError = 105,
            eCommonNodeCreationError = 106,
            eCommonNodeParseError = 107,
            eMissingRequiredElement = 108,
            eSystemTableError = 109,
            eFolderUploadError = 110,
            eUnassignedError = 111
        }


        /// <summary>
        /// This method initializes processing of the documents.Returns True if processing should continue.
        /// </summary>
        /// <returns></returns>
        public bool CPartner_InitProcessDocument()//Todo return value
        {
            mobjPartner = new CPartner();
           mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocument:" });
            bool isInitProcessDocument;
            try
            {

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Started" }));

                //Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "/SGTransaction/@TransactionType", true);

                //Extract the LYNXID and Vehicle number.
                mobjPartner.ExtractLynxIDAndVehNum();

                //Are we configured to process this document at this time?
                isInitProcessDocument = mobjPartner.CanProcessDocument();

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Finished" }));


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isInitProcessDocument;
        }


        /// <summary>
        /// This method processing documents to the partner database.
        /// </summary>
        /// <returns></returns>
        public bool CPartner_PartnerDatabaseTransfer(bool blnSendReceipt)//Todo return value
        {
            mobjPartner = new CPartner();
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mDomUtils = new Common.MDomUtils();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransfer:" });
            string strError = string.Empty,
            strPath = string.Empty;
            bool isPartnerDatabaseTransfer = true;
            XmlDocument xmldocument = null;
            object objTempEvents = null;
            try
            {
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                {
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started" }));
                }
                //return true;Todo

                switch (mobjPartner.DocTypeEnum)
                {

                    case CPartner.EDocumentType.eDigitalImage:

                        mobjPartner.StoreDocumentInPartnerDb();
                        if (blnSendReceipt)
                            mobjPartner.SendAReceipt("SGReceipt.XML");
                        break;
                    case CPartner.EDocumentType.ePrintImage:

                        mobjPartner.ExtractSequenceNumber("//EstimateIdentifier", string.Empty);
                        mobjPartner.StorePrintImageInPartnerDB();
                        if (blnSendReceipt)
                            mobjPartner.SendAReceipt("SGReceipt.XML");
                        break;
                    case CPartner.EDocumentType.eEstimateData:

                        mobjPartner.ExtractSequenceNumber("//EstimateIdentifier", string.Empty);
                        mobjPartner.StoreEstimateDataInPartnerDB();
                        if (blnSendReceipt)
                            mobjPartner.SendAReceipt("SGReceipt.XML");
                        break;
                    case CPartner.EDocumentType.eHeartBeat:
                        //These are not stored.
                        break;

                    case CPartner.EDocumentType.eReceipt:
                    case CPartner.EDocumentType.eShopSearch:

                        mobjPartner.StoreDocumentInPartnerDb();
                        break;
                    case CPartner.EDocumentType.eBusinessEvent:
                        throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType.ToString(), PROC_NAME, "We don't process doc type'", mobjPartner.DocTypeString, "' [", mobjPartner.DocTypeEnum.ToString(), "] for partner ", mLAPDPartnerDataMgr.g_strPartner }));

                    case CPartner.EDocumentType.eAssignment:
                        mobjPartner.StoreDocumentInPartnerDb();

                        //Validate the assignment XML for required information.
                        strPath = string.Concat(new string[] { "\\SGAssignmentValidate.xsl", mLAPDPartnerDataMgr.g_strSupportDocPath });
                        objTempEvents = mLAPDPartnerDataMgr.g_objEvents;
                        xmldocument = mLAPDPartnerDataMgr.g_objPassedDocument;
                        strError = mDomUtils.ValidateDomData(ref xmldocument, ref strPath, ref objTempEvents, false);
                        strPath = string.Empty;
                        if (!string.IsNullOrEmpty(strError))
                        {
                            //Halt further processing
                            isPartnerDatabaseTransfer = false;

                            //Negative receipt
                            mobjPartner.SendAReceipt("SGReceipt.XML", "N", Convert.ToInt64(SceneGenesis_Error_Codes.eAssignmentInvalid), strError);

                            //Send notification to development staff.
                            mLAPDPartnerDataMgr.g_objEvents.HandleEvent(Convert.ToInt32(SceneGenesis_Error_Codes.eAssignmentInvalid), PROC_NAME, "Scene Genesis Assignment failed validation.  A negative receipt has been sent.", string.Concat(new string[] { "Error: ", strError }), "", false);
                        }
                        break;
                }

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, " Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isPartnerDatabaseTransfer;
        }


        /// <summary>
        /// This method processing documents to the APD database.
        /// </summary>
        /// <returns></returns>
        public string CPartner_ApdDatabaseTransfer()
        {
            mobjPartner = new CPartner();
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mCWebAssignment = new LAPDEcadAccessorMgr.CWebAssignmentClass();
            mDomUtils = new Common.MDomUtils();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransfer:" });
            string strApdDatabaseTransfer = string.Empty,
            strCode = string.Empty,
            strError = string.Empty,
            strSource = string.Empty,
            strPath = string.Empty;
            object gobj = null;
            long lngNumber = 0;
            XmlDocument xmlApdLoadDocument = null;
         
            try
            {
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started" }));

                //CPartner_ApdDatabaseTransfer = "";//Todo
                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eReceipt:
                        //TODO - Put positive ack handling in place for other types besides Assignments.
                        if (mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//ReceiptForType") == "Assignment")
                        {
                            //Handle assignment receipt.
                            mobjPartner.ProcessReceipt("//FailureReasonCode", "//FailureReasonText");
                        }
                        else if (mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//SuccessFlag") == "N")
                        {
                            //Send developer notification of nack from SG.
                            mLAPDPartnerDataMgr.g_objEvents.HandleEvent((Convert.ToInt32(SceneGenesis_Error_Codes.eUnhandledSceneGenesisNack)), PROC_NAME, string.Concat(new string[] { "Negative Acknowledgement received from Scene Genesis: ", " Code = '", mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//FailureReasonCode"), "' Desc = '", mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//FailureReasonText"), "'" }), string.Concat(new string[] { "XML = ", mLAPDPartnerDataMgr.g_strPassedDocument }), string.Empty, false);
                        }

                        break;

                    case CPartner.EDocumentType.eDigitalImage:
                        mobjPartner.ProcessBinaryFiles(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//DigitalImageFile", "//CreateDateTime/DateTime/@Date", "//CreateDateTime/DateTime/@Time", "JPG", mobjPartner.DocTypeString, CPartner.EDocumentType.eDigitalImage);
                        mobjPartner.SendWorkflowNotification("ElectronicDocumentReceived", string.Concat(new string[] { "SHOP (", mLAPDPartnerDataMgr.g_strPartner, "): Digital images received for Vehicle ", mLAPDPartnerDataMgr.g_strVehicleNumber }));
                        break;

                    case CPartner.EDocumentType.eEstimateData:
                    case CPartner.EDocumentType.ePrintImage:
                        mobjPartner.ProcessEstimate(ref mobjPartner);
                        break;

                    case CPartner.EDocumentType.eHeartBeat:
                        //If this was a heartbeat request, then it will not have a
                        //Reply\DateTime element.  We want to respond to requests,
                        //but ignore responses for now.
                        if ((mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//ReplyDate/DateTime/@Date").Length) == 0)
                        {
                            mobjPartner.SendHeartBeatResponse("SGHeartBeat.XML");
                        }

                        break;

                    case CPartner.EDocumentType.eShopSearch:
                        //Get the shop search XML from the APD database
                        //and Transform into the SG shop list format.
                        gobj = mLAPDPartnerDataMgr.g_objEvents;
                        strPath = string.Concat(new string[] { "<ShopList>", mLAPDPartnerDataMgr.g_strPassedDocument, mobjPartner.ProcessShopSearchRequest("//ShopSearch"), "</ShopList>" });
                        strApdDatabaseTransfer = mDomUtils.TransformXmlAsXml(ref strPath, string.Concat(new string[] { mLAPDPartnerDataMgr.g_strSupportDocPath, "\\SGShopList.xsl" }), ref gobj);
                        return strApdDatabaseTransfer;


                    case CPartner.EDocumentType.eAssignment:
                        //Get the assignment code of the passed assignment.
                        strCode = (mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//Assignment/AssignmentCode").ToUpper());
                        switch (strCode)
                        {
                            case "NEW":
                                //objECAD = mLAPDPartnerDataMgr.CreateObjectEx("LAPDEcadAccessorMgr.CWebAssignment");

                                gobj = mLAPDPartnerDataMgr.g_objEvents;
                                xmlApdLoadDocument = new XmlDocument();

                                //Process the assignment.
                                strPath = string.Concat(new string[] { mLAPDPartnerDataMgr.g_strSupportDocPath, "\\SGAssignmentXfer.xsl" });

                                xmlApdLoadDocument = mDomUtils.TransformXmlAsDom(ref mLAPDPartnerDataMgr.g_strPassedDocument, strPath, ref gobj);                               

                                mLAPDPartnerDataMgr.g_strLynxID = Convert.ToString(mCWebAssignment.ApdLoadAssignment(xmlApdLoadDocument.OuterXml,0));

                                //Positive receipt.
                                mobjPartner.SendAReceipt("SGReceipt.XML");

                                // ERROR: Not supported in C#: OnErrorStatement
                                //Go ahead and raise now if an error happened in the ApdLoadAssignment call.
                                if (lngNumber != 0)
                                {
                                    throw new Exception(string.Concat(new string[] { lngNumber.ToString(), "strSource", "strError" }));

                                }

                                //Send back an AsgnDown business event.
                                SendBusinessEvent("AsgnDown", string.Concat(new string[] { "Assignment '", mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//AssignmentID"), "' was successfully downloaded." }), "-AD");

                                //Send back an AdjAssigned business event.
                                SendBusinessEvent("AdjAssigned", string.Concat(new string[] { "Assignment '", mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//AssignmentID"), "' was successfully assigned to an adjuster." }), "-AA");

                                break;
                            case "CANCEL":
                                if (string.IsNullOrEmpty(mLAPDPartnerDataMgr.g_strLynxID))
                                {
                                    throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eLynxIdInvalid.ToString(), PROC_NAME, "Cannot cancel a claim without a LynxID" }));
                                }
                                else
                                {
                                    mobjPartner.SendWorkflowNotification("ClaimCancelled", string.Concat(new string[] { "Cancellation received for LYNXID: ", mLAPDPartnerDataMgr.g_strLynxID }), "", "CLM");
                                }
                                break;
                            default:
                                throw new Exception(string.Concat(new string[] { SceneGenesis_Error_Codes.eBadAssignmentCode.ToString(), PROC_NAME, "Bad AssignmentCode = '", strCode, "'" }));

                        }
                        break;
                    case CPartner.EDocumentType.eBusinessEvent:
                        throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType.ToString(), PROC_NAME, "We don't process doc type '", mobjPartner.DocTypeString, "' [", mobjPartner.DocTypeEnum.ToString(), "] for partner ", mLAPDPartnerDataMgr.g_strPartner }));

                }

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, " Finished" }));
            }
            catch (Exception ex)
            {
                mobjPartner.SendAReceipt("SGReceipt.XML", "N", Convert.ToInt64(SceneGenesis_Error_Codes.eAssignmentInvalid), "B1|An error occurred while processing your assignment.  A LYNX representative will be in contact with you shortly.");
            }
            finally
            {

            }
            return strApdDatabaseTransfer;
        }


        /// <summary>
        /// This method transmits documents to the partner.
        /// </summary>
        /// <returns></returns>
        public string CPartner_TransferToPartner()//Todo
        {
            mobjPartner = new CPartner();
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferToPartner:" });
            string strTransferToPartner = string.Empty;
            try
            {

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, " Started" }));

                //Extract the doc type from the passed root element.
                mobjPartner.RawDocTypeString = mLAPDPartnerDataMgr.g_objPassedDocument.DocumentElement.NodeType.ToString();

                switch (mobjPartner.DocTypeEnum)
                {
                    case CPartner.EDocumentType.eFolderUpload:
                        DocumentExchange();
                        break;
                    default:
                        throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eBadDocumentType.ToString(), PROC_NAME, "We don't process doc type '", mobjPartner.DocTypeString, "' [", mobjPartner.DocTypeEnum.ToString(), "] for partner ", mLAPDPartnerDataMgr.g_strPartner }));

                }

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, " Finished" }));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strTransferToPartner;
        }


        /// <summary>
        /// Purpose: Inserts an estimate print image into the APD document system.
        /// Returns: The APD document ID through lngAPDDocumentID
        /// </summary>
        /// <returns></returns>
        public long CPartner_InsertPrintImageIntoAPD(string strXml, long lngPartnerTransID)
        {
            mobjPartner = new CPartner();
            long lngInsertPrintImageIntoAPD;
            try
            {
                lngInsertPrintImageIntoAPD = mobjPartner.InsertPrintImageIntoAPD(strXml, lngPartnerTransID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lngInsertPrintImageIntoAPD;
        }


        /// <summary>
        /// Fills out some generic header schtuff.
        /// </summary>
        public void SetHeaderSchtuff(XmlDocument objDom, string strIdent)
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            string strDate = string.Empty,
            strTime = string.Empty,
            strIdentity = string.Empty;
            try
            {
                strDate = mLAPDPartnerDataMgr.ConstructXmlDate(DateTime.Now.ToString());
                strTime = mLAPDPartnerDataMgr.ConstructXmlTime(DateTime.Now.ToString());
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@TransactionID", true).InnerText = string.Concat(new string[] { strIdentity, "-", strDate, "-", strTime, "-", mLAPDPartnerDataMgr.g_strLynxID, "-", mLAPDPartnerDataMgr.g_strVehicleNumber, "-", mLAPDPartnerDataMgr.g_strAssignmentID });
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@LynxID", true).InnerText = mLAPDPartnerDataMgr.g_strLynxID;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@VehicleID", true).InnerText = mLAPDPartnerDataMgr.g_strVehicleNumber;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@AssignmentID", true).InnerText = mLAPDPartnerDataMgr.g_strAssignmentID;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@TaskID", true).InnerText = mLAPDPartnerDataMgr.g_strTaskID;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@TransactionDate", true).InnerText = strDate;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@TransactionTime", true).InnerText = strTime;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//DateTime/@Date", true).InnerText = strDate;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//DateTime/@Time", true).InnerText = strTime;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//DateTime/@TimeZone", true).InnerText = "Local";

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        /// <summary>
        /// Send a HeartBeat request to Partner.
        /// </summary>
        public void CPartner_SendHeartBeatRequest()
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mPPGTransactions = new MPPGTransactions();
            mDomUtils = new Common.MDomUtils();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendHeartBeatRequest:" });
            XmlDocument objDom = null;
            XmlNode objNetStatNode = null;
            string strWrappedXml = string.Empty,
            strPost2URL = string.Empty,
            strPath = string.Empty;
            try
            {
                objDom = new XmlDocument();
                strPath = string.Concat(new string[] { "\\SGHeartBeat.XML", mLAPDPartnerDataMgr.g_strSupportDocPath });

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Preparing HeartBeat" }));

                mDomUtils.LoadXmlFile(ref objDom, ref strPath, "SendAHeartBeat", "HeartBeat");
                strPath = string.Empty;
                SetHeaderSchtuff(objDom, "HB");
                objNetStatNode = mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//NetworkStatusTest", true);

                //Remove the received date/time - to be filled in by SG.
                objNetStatNode.RemoveChild(mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//ReceivedDate", true));

                //Remove the reply date/time - to be filled in by SG.
                objNetStatNode.RemoveChild(mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//ReplyDate", true));
                strWrappedXml = mPPGTransactions.BuildTransactionForPPG("NetworkStatusTest", objDom.OuterXml, mLAPDPartnerDataMgr.g_strPartner, string.Concat(new string[] { mLAPDPartnerDataMgr.g_strLynxID, "-", mLAPDPartnerDataMgr.g_strVehicleNumber }));
                strPost2URL = mLAPDPartnerDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");
                mPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL }, string.Concat(new string[] { PROC_NAME, "HeartBeat sent via HTTP" })));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objNetStatNode = null;
                objDom = null;
            }

        }


        /// <summary>
        /// Send a business event to Partner.
        /// </summary>
        public void SendBusinessEvent(string strEventCode, string strEventNote, string strEventOpcode)
        {
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mPPGTransactions = new MPPGTransactions();
            mDomUtils = new Common.MDomUtils();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "SendBusinessEvent:" });
            XmlDocument objDom, objSession = null;
            string strWrappedXml = string.Empty,
            strPost2URL = string.Empty,
            strPath = string.Empty,
            strAssign = string.Empty;
            try
            {
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Preparing Business Event" }));

                objDom = new XmlDocument();
                strPath = string.Concat(new string[] { mLAPDPartnerDataMgr.g_strSupportDocPath, "\\SGBusinessEvent.XML" });
                mDomUtils.LoadXmlFile(ref objDom, ref strPath, PROC_NAME, "Business Event");
                SetHeaderSchtuff(objDom, string.Concat(new string[] { "BE", strEventOpcode }));

                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//@EventCode", true).InnerText = strEventCode;
                mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//EventNotes", true).InnerText = strEventNote;
                if (strEventCode.ToUpper() == "ADJASSIGNED")
                {
                    //Remove the Assignment Download data.
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//EventData", true).RemoveChild(mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//DateTime", true));

                    //Add the assigned adjuser information.
                    objSession = new XmlDocument();

                    //Get the claim session detail from the database.
                    mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strLYNXConnStringXML);
                    strAssign = mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/SessionClaimDetailSP"), new object[] { "LynxID", mLAPDPartnerDataMgr.g_strLynxID });
                    mDomUtils.LoadXml(ref objSession, ref strAssign, PROC_NAME, "Session Claim Detail");

                    //Set the LYNX rep contact info.
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Person/@FirstName", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserNameFirst", true);
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Person/@LastName", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserNameLast", true);
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Telecom/AreaCode", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserPhoneAreaCode", true);
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Telecom/TelecomNum", true).InnerText = string.Concat(new string[] { mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserPhoneExchangeNumber", true), mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserPhoneUnitNumber", true) });
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//Telecom/Extn", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserExtensionNumber", true);
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//ContactEmail", true).InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objSession, "//@OwnerUserEmail", true);
                    //Assignment Download
                }
                else
                {
                    //Remove the Adjuster Assigned data.
                    mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//EventData", true).RemoveChild(mLAPDPartnerDataMgr.GetChildNode(ref objDom, "//ContactPerson", true));
                }
                //Build the SOAP Envelope and put the HeartBeat into the envelope
                strWrappedXml = mPPGTransactions.BuildTransactionForPPG("BusinessEvent", objDom.ToString(), mLAPDPartnerDataMgr.g_strPartner, string.Concat(new string[] { mLAPDPartnerDataMgr.g_strLynxID, "-", mLAPDPartnerDataMgr.g_strVehicleNumber }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mLAPDPartnerDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");

                //Do the post to PPG
                mPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);
                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL }), string.Concat(new string[] { PROC_NAME, "Business Event sent via HTTP" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDom = null;
                objSession = null;
            }

        }


        /// <summary>
        /// Creates a Folder Upload document based upon the passed XML document.
        /// The passed XML points us to the claim and gives a list of attachments.
        /// This method then takes that list of attachments and decides how many
        /// EstimateUpload and how many EstimateDI transactions are needed.
        /// </summary>
        public void DocumentExchange()
        {
            mDomUtils = new Common.MDomUtils();
            mEscapeUtilities = new MEscapeUtilities();
            mLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
            mobjPartner = new CPartner();
            mPPGTransactions = new MPPGTransactions();
            mBase64Utils = new MBase64Utils();
            mFlatfile = new MFlatfile();
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "DocumentExchange:" });
            XmlDocument objDocs, objTrans, objCEF, objPassThru, objVehicleList = null;
            XmlElement objElem, objDocElem, objNewDocElem = null;
            XmlNodeList objList;
            XmlCDataSection objCdata = null;
            ADODB.Recordset objRS = null;
            string strVehicleNumber = string.Empty,
            strWrappedXml = string.Empty,
            strPost2URL = string.Empty,
            strPartnerTransID = string.Empty,
            strClaimAspectID = string.Empty,
            strDocList = string.Empty,
            strCEF = string.Empty,
            strXML = string.Empty,
            strTaskFields = string.Empty,
            strTaskID = string.Empty,
            strAssign = string.Empty,
            strValues = string.Empty,
            strInsCoID = string.Empty,
            strUserID = string.Empty,
            strPath = string.Empty,
            strFileName = string.Empty,
            strExtension = string.Empty,
            strTemp = string.Empty;
            string[] strAttachments = null,
            varData = null;
            object varAttach = null;
            System.DateTime objDate;
            int intIdx = 0;
            long lngDocID = 0;
            bool isNum = false;
            double dNum;
            try
            {

                objDocs = new XmlDocument();
                objTrans = new XmlDocument();
                objCEF = new XmlDocument();
                objVehicleList = new XmlDocument();
                objPassThru = new XmlDocument();
                //objElem = new XmlElement();
                //objDocElem = new XmlElement();
                //objNewDocElem = new XmlElement();
                //objCdata = new XmlCDataSection();
                objDate = new System.DateTime();

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Preparing ", mobjPartner.DocTypeString }));

                strAttachments = (mLAPDPartnerDataMgr.g_objPassedDocument.SelectSingleNode("//@Attachments").InnerText).Split(',');
                strInsCoID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//@InsuranceCompanyID", true);
                strUserID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//@UserID", true);
                mLAPDPartnerDataMgr.g_strLynxID = mLAPDPartnerDataMgr.GetChildNodeText(ref mLAPDPartnerDataMgr.g_objPassedDocument, "//@LynxID", true);
                mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strLYNXConnStringXML);
                strXML = mLAPDPartnerDataMgr.g_objDataAccessor.OpenRecordsetAsClientSideXML(mLAPDPartnerDataMgr.GetConfig("ClaimPoint/SPMap/VehicleList"), new object[] { mLAPDPartnerDataMgr.g_strLynxID, strInsCoID });
                objVehicleList = new XmlDocument();
                objVehicleList.LoadXml(strXML);
                strVehicleNumber = mLAPDPartnerDataMgr.GetChildNodeText(ref objVehicleList, "//Vehicle/@VehicleNumber");
                mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "VehicleNumber = ", strVehicleNumber }));
                objVehicleList = null;
                strXML = string.Empty;
                strXML = mLAPDPartnerDataMgr.g_objDataAccessor.OpenRecordsetAsClientSideXML(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PassThruSP"), new object[] { mLAPDPartnerDataMgr.g_strLynxID });
                objPassThru = new XmlDocument();
                objPassThru.LoadXml(strXML);

                if (objPassThru == null)
                {
                    throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned.ToString(), PROC_NAME, "No Records returned from ", mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PassThruSP"), " for ", mLAPDPartnerDataMgr.g_strLynxID }));
                }
                else if (objPassThru.SelectNodes("//ClaimAspect").ToString().Length == 0)
                {
                    throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNoRecordsReturned.ToString(), PROC_NAME, "No Records returned from ", mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PassThruSP"), " for ", mLAPDPartnerDataMgr.g_strLynxID }));
                }

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(objPassThru.OuterXml, string.Concat(new string[] { PROC_NAME, "SourceApplicationPassThruData" }));

                objDocs = new XmlDocument();
                strAssign = mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/DocumentListSP"), new object[] { "@LynxID", mLAPDPartnerDataMgr.g_strLynxID, "@DocumentClassCD", "A", "@InsuranceCompanyID", strInsCoID });
                mLAPDPartnerDataMgr.LoadXml(ref objDocs, ref strAssign, PROC_NAME, "Document List");

                foreach (string varAttach_loopVariable in strAttachments)
                {
                    varAttach = varAttach_loopVariable;

                    //Match each passed attachment to the list retrieved from the DB.
                    objElem = (XmlElement)objDocs.SelectSingleNode(string.Concat(new string[] { "//Document[@DocumentID=", Convert.ToString(varAttach), "]" }));

                    //Pitch a fit if one was passed in that could not be found.
                    if (objElem == null)
                    {
                        throw new Exception(string.Concat(new string[] { ToString(), "Attachment Check", "Attachment '", varAttach.ToString(), "' not found for LynxID '", mLAPDPartnerDataMgr.g_strLynxID, "' and InsuranceCompanyID '", strInsCoID, "'" }));
                    }
                    else
                    {
                        //Set some useful attributes back into the document elements.
                        mDomUtils.AddAttribute(ref objElem, "Selected", "True");
                        mDomUtils.AddAttribute(ref objElem, "EstimateTypeFlag", mLAPDPartnerDataMgr.GetChildNodeText(ref objDocs, string.Concat(new string[] { "//Reference[@List='DocumentType'][@ReferenceID='", objElem.SelectSingleNode("@DocumentTypeID").InnerText, "']/@EstimateTypeFlag" }), true));
                    }

                    // Get the PassThruData from the first selected document.  This data will then be added to each transaction sent to the carrier.
                    if ((strTaskID.Length) == 0 && strTaskFields.Length == 0)
                    {
                        // Get the ClaimAspectID associated with this document.
                        strClaimAspectID = objElem.SelectSingleNode("@ClaimAspectID").InnerText;

                        // Pull the Claim Aspect specific Pass Thru data.
                        strTaskFields = mLAPDPartnerDataMgr.GetChildNodeText(ref objPassThru, string.Concat(new string[] { "//ClaimAspect[@ClaimAspectID='", strClaimAspectID, "']/@SourceApplicationPassThruData" }), true);

                        if (mLAPDPartnerDataMgr.g_blnDebugMode)
                            mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "ClaimAspectID: ", strClaimAspectID, "  TaskFields: ", strTaskFields, PROC_NAME, "SourceApplicationPassThruData" }));

                        //Split the pass through data into the TaskID and the TaskFields.
                        intIdx = strTaskFields.IndexOf("^", 1);

                        if (intIdx < 1)
                        {
                            throw new Exception(string.Concat(new string[] { ToString(), PROC_NAME, "Bad Pass Thru data from ", mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PassThruSP"), " for ", mLAPDPartnerDataMgr.g_strLynxID, " - missing '|'" }));
                        }
                        strTaskID = Left(strTaskFields, intIdx - 1);
                        strValues = Mid(strTaskFields, intIdx + 1);
                        strTaskFields = mEscapeUtilities.UnEscapeXmlMarkup(ref strValues);
                    }
                }

                if (mLAPDPartnerDataMgr.g_blnDebugMode)
                    mLAPDPartnerDataMgr.g_objEvents.Trace(objDocs.OuterXml, string.Concat(new string[] { PROC_NAME, "Document list with selected attributes." }));

                //Determine the URL for posting depending on the environment
                strPost2URL = mLAPDPartnerDataMgr.GetConfig("ShopAssignment/ElectronicTransmissionURL");
                objCEF = new XmlDocument();

                // Launch EstimateUpload transactions for the selected estimates.
                //Get a list of all the selected estimates.
                objList = objDocs.SelectNodes("//Document[@Selected='True'][@EstimateTypeFlag='1']");

                //Loop through each one and send a transaction to SG for it.
                foreach (XmlElement objElem_loopVariable in objList)
                {
                    objElem = objElem_loopVariable;
                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(objElem.OuterXml, string.Concat(new string[] { PROC_NAME, "Estimate Upload Started" }));

                    //Load up a fresh estimate template from disk.
                    objTrans = new XmlDocument();
                    strPath = string.Concat(new string[] { mLAPDPartnerDataMgr.g_strSupportDocPath, "\\SGEstimateUpload.XML" });
                    mDomUtils.LoadXmlFile(ref objTrans, ref strPath, PROC_NAME, "Estimate Load");
                    strPath = string.Empty;

                    //Set some basic header values.
                    SetHeaderSchtuff(objTrans, "EU");

                    //Set the scene genesis specific fields.
                    mBase64Utils.WriteTextToCDATA((XmlCDataSection)mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//TaskFields", true).FirstChild, strTaskFields);

                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//@TaskID", true).InnerText = strTaskID;

                    //Get the file path and trim off the path info.
                    strPath = objElem.SelectSingleNode("@ImageLocation").InnerText;

                    strTemp = strPath;

                    while (strTemp.IndexOf("\\\\", 1) > 0)
                    {
                        strTemp = Mid(strTemp, (strTemp.IndexOf("\\\\", 1) + 2));
                    }
                    strFileName = strTemp;

                    //Now trim off everything but the extension.
                    while (strTemp.IndexOf(".", 1) > 0)
                    {
                        strTemp = Mid(strTemp, (strTemp.IndexOf(".", 1) + 1));
                    }

                    strExtension = strTemp.ToUpper();

                    //Get the full path across to the document storage.
                    strPath = string.Concat(new string[] { (mLAPDPartnerDataMgr.GetConfig("Document/RootDirectory")), strPath }).Replace("\\\\", "\\");

                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Estimate Upload PI" }));


                    //If the file type is XML then we use CDATA.
                    if (strExtension == "XML")
                    {
                        //Fix the file name and extension to be TXT
                        strExtension = "TXT";
                        strFileName = Left(strFileName, (strFileName.Length - 3)) + strExtension;

                        //Set the image encoding element to CDATA.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageEncoding", true).InnerText = "Cdata";

                        //Read in the text data from the file.
                        strTemp = mBase64Utils.ReadTextDataFromFile(strPath);

                        //Strip out the XML and just get the flatfile.
                        strTemp = mFlatfile.ConvertXMLToFlatfile(strTemp);

                        //Set the image size element.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageLength", true).InnerText = strTemp.Length.ToString();

                        //Create a new CDATA section and add it to the image file element.
                        objCdata = objTrans.CreateCDataSection(strTemp);
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageFile", true).AppendChild(objCdata);
                        objCdata = null;
                        //Otherwise we Base64 encode it.

                    }
                    else
                    {
                        //Set the image encoding element to Base64.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageEncoding", true).InnerText = "Base64";

                        //Read in the binary data from the file.
                        varData = (string[])(mBase64Utils.ReadBinaryDataFromFile(strPath));

                        //Set the image size element.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageLength", true).InnerText = (varData.Length - 1).ToString();

                        //Write the binary data to the DOM
                        mBase64Utils.WriteBinaryDataToDomElement((XmlElement)mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageFile", true), varData);
                    }

                    //Stuff file name into the ImageFileName element.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageFileName", true).InnerText = strFileName;

                    //Stuff the file extension into the ImageFileType element.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/ImageFileType", true).InnerText = strExtension;

                    //Get and set the image date time info.
                    strTemp = objElem.SelectSingleNode("@ReceivedDate").InnerText;

                    objDate = Convert.ToDateTime(string.Concat(new string[] { (Left(strTemp, 10)), " ", Right(strTemp, 8) }));
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/CreateDateTime/DateTime/@Date", true).InnerText = mLAPDPartnerDataMgr.ConstructXmlDate(objDate.ToString());
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//PrintImage/CreateDateTime/DateTime/@Time", true).InnerText = mLAPDPartnerDataMgr.ConstructXmlTime(objDate.ToString());

                    //Set the AssignmentID for this estimate.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//@AssignmentID", true).InnerText = objElem.SelectSingleNode("//@AssignmentID").InnerText;

                    //Get the partner trans id for this estimate.
                    strPartnerTransID = objElem.SelectSingleNode("//@ExternalReferenceDocumentID").InnerText;

                    strPartnerTransID = Mid(strPartnerTransID, strPartnerTransID.IndexOf(":", 1) + 1);
                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Partner Trans ID = '", strPartnerTransID, "'" }), string.Concat(new string[] { PROC_NAME, "Estimate Upload CEF data" }));

                    //Retrieve the CEF data for this estimate from the partner database.
                    isNum = double.TryParse(strPartnerTransID, out dNum);
                    if (isNum)
                    {
                        strCEF = mobjPartner.RetrieveCefFromPartnerDb(Convert.ToInt64(strPartnerTransID));
                    }
                    else
                    {
                        strCEF = string.Empty;
                    }

                    //Was CEF data found?
                    if (!string.IsNullOrEmpty(strCEF))
                    {
                        //Stuff it into a DOM.
                        mLAPDPartnerDataMgr.LoadXml(ref objCEF, ref strCEF, PROC_NAME, "CEF");

                        //Stuff it into the estimate upload DOM.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//TransactionData", true).AppendChild(mLAPDPartnerDataMgr.GetChildNode(ref objCEF, "//APDEstimate", true));
                    }
                    else
                    {
                        //Log the fact that no CEF was retrieved.
                        //g_objEvents.HandleEvent eBlankCefReturnedForEstimate, _
                        //PROC_NAME & " Estimate Upload CEF Retrieval", _
                        //"Blank CEF! Partner Trans ID = '" & strPartnerTransID & "'", "", "", False
                    }

                    //Build the SOAP Envelope and put the HeartBeat into the envelope
                    strWrappedXml = mPPGTransactions.BuildTransactionForPPG("EstimateUpload", objTrans.OuterXml, mLAPDPartnerDataMgr.g_strPartner, mLAPDPartnerDataMgr.g_strLynxID);

                    //Do the post to PPG
                    mPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);//Todo

                    //Build a description of the event.
                    strTemp = objElem.SelectSingleNode("//@SupplementSeqNumber").InnerText;

                    if (strTemp == "0")
                    {
                        strTemp = "Estimate";
                    }
                    else
                    {
                        strTemp = string.Concat(new string[] { "Supplement ", strTemp });
                    }

                    strTemp = string.Concat(new string[] { strTemp, " uploaded electronically to the insurance carrier through ", mLAPDPartnerDataMgr.g_strPartner, "." });

                    //Send a WorkFlow Event for the electronic transmission.
                    mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strLYNXConnStringStd);
                    mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("WorkFlow/SPName"), new object[]{ "@EventID", mLAPDPartnerDataMgr.GetConfig("WorkFlow/EventCodes/Code[@name='EstimateSentToCarrier']"), "@LynxID", mLAPDPartnerDataMgr.g_strLynxID, "@PertainsTo", string.Concat(new string[] { "VEH", strVehicleNumber }), "@Description", strTemp, "@UserID",
                    strUserID, "@ReferenceData", objElem.SelectSingleNode("@DocumentID").InnerText});

                    //Have the database set the uploaded indicator on the document
                    mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("Document/UploadSetSPName"), new object[] { "@DocList", objElem.SelectSingleNode("@DocumentID"), "@UserID", strUserID });

                    //Stuff the electronic transmission XML in the partner specific database.
                    mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strPartnerConnStringStd);
                    mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PartnerLoadSP"), new object[] { "@AssignmentID", mLAPDPartnerDataMgr.g_strAssignmentID, "@LynxID", mLAPDPartnerDataMgr.g_strLynxID, "@VehicleNumber", strVehicleNumber, "@SequenceNumber", objElem.SelectSingleNode("//@SupplementSeqNumber"), "@TransactionDate",
                    string.Concat(new string[] { DateTime.Now.ToString("yyyy-mm-dd"), " ", DateTime.Now.ToString("hh:mm:ss") }), "@TransactionSource", "LYNX", "@TransactionType", "EstimateUpload", "@PartnerXML", mEscapeUtilities.SQLQueryString(ref strWrappedXml)});
                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL, PROC_NAME, mobjPartner.DocTypeString, " sent via HTTP" }));

                }



                //Launch one EstimateDI transaction for all other selected documents.


                //Get a list of all the selected estimates.
                objList = objDocs.SelectNodes("//Document[@Selected='True'][@EstimateTypeFlag='0']");

                //Only need to go through the following mess if at least one DI exists to upload.
                if (objList.ToString().Length > 0)
                {
                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "Num Docs = ", objList.ToString() }), string.Concat(new string[] { PROC_NAME, "Document Upload Started" }));

                    //Load up a fresh estimate template from disk.
                    objTrans = new XmlDocument();
                    strPath = string.Concat(new string[] { mLAPDPartnerDataMgr.g_strSupportDocPath, "\\SGEstimateDI.XML" });
                    mDomUtils.LoadXmlFile(ref objTrans, ref strPath, PROC_NAME, "Estimate Load");

                    //Set some basic header values.
                    SetHeaderSchtuff(objTrans, "EU");

                    //Set the scene genesis specific fields.
                    mBase64Utils.WriteTextToCDATA((XmlCDataSection)mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//TaskFields", true).FirstChild, strTaskFields);
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//@TaskID", true).InnerText = strTaskID;

                    //Set the numeric insurance carrier ID.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//InsuranceCarrierID", true).InnerText = strInsCoID;

                    //Set the number of images.
                    mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//NumberOfImages", true).InnerText = (objList.ToString().Length).ToString();

                    //Get the mock-up DigitalImage element in the DI XML.
                    objDocElem = (XmlElement)objTrans.SelectSingleNode("//DigitalImage");


                    //Remove the blank DigitalImage element from the document.
                    objDocElem = (XmlElement)objDocElem.ParentNode.RemoveChild(objDocElem);
                    strDocList = string.Empty;

                    //Loop through each document and add a DigitalImage element for it.
                    foreach (XmlElement objElem_loopVariable in objList)
                    {
                        objElem = objElem_loopVariable;

                        //Duplicate the document element.
                        objNewDocElem = (XmlElement)objDocElem.CloneNode(true);

                        //Get the file path and trim off the path info.
                        strPath = objElem.SelectSingleNode("@ImageLocation").InnerText;

                        strTemp = strPath;
                        while (strTemp.IndexOf("\\\\", 1) > 0)
                        {
                            strTemp = Mid(strTemp, strTemp.IndexOf("\\\\", 1) + 2);
                        }

                        //Stuff file name into the ImageFileName element.
                        objNewDocElem.SelectSingleNode("ImageFileName").InnerText = strTemp;


                        //Now trim off everything but the extension.
                        while (strTemp.IndexOf(".", 1) > 0)
                        {
                            strTemp = Mid(strTemp, (strTemp.IndexOf(".", 1) + 1));
                        }

                        //Stuff the file extension into the ImageFileType element.
                        objNewDocElem.SelectSingleNode("ImageFileType").InnerText = strTemp;


                        //Stuff the document type.
                        objNewDocElem.SelectSingleNode("DocumentType").InnerText = mLAPDPartnerDataMgr.GetChildNodeText(ref objDocs, string.Concat(new string[] { "//Reference[@List='DocumentType'][@ReferenceID='", objElem.SelectSingleNode("@DocumentTypeID").ToString(), "']/@Name" }), true);

                        //Get the full path across to the document storage.
                        strPath = string.Concat(new string[] { (mLAPDPartnerDataMgr.GetConfig("Document/RootDirectory")), strPath }).Replace("\\\\", "\\");

                        //Read in the binary data from the file.
                        varData = (string[])(mBase64Utils.ReadBinaryDataFromFile(strPath));

                        //Set the image size element.
                        objNewDocElem.SelectSingleNode("ImageFileLength").InnerText = varData.ToString();

                        //Write the binary data to the DOM.
                        mBase64Utils.WriteBinaryDataToDomElement((XmlElement)objNewDocElem.SelectSingleNode("DigitalImageFile"), varData);


                        //Get and set the image date time info.
                        strTemp = objElem.SelectSingleNode("@ReceivedDate").InnerText;
                        objDate = Convert.ToDateTime(string.Concat(new string[] { Left(strTemp, 10), " ", Right(strTemp, 8) }));
                        objNewDocElem.SelectSingleNode("CreateDateTime/DateTime/@Date").InnerText = mLAPDPartnerDataMgr.ConstructXmlDate(objDate.ToString());
                        objNewDocElem.SelectSingleNode("CreateDateTime/DateTime/@Time").InnerText = mLAPDPartnerDataMgr.ConstructXmlTime(objDate.ToString());

                        //Set the AssignmentID for this estimate.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//@AssignmentID", true).InnerText = objElem.SelectSingleNode("//@AssignmentID").InnerText;
                        //Now append our newly created digital image element to the primery doc.
                        mLAPDPartnerDataMgr.GetChildNode(ref objTrans, "//TransactionData", true).AppendChild(objNewDocElem);

                        //Build a reference document ID list
                        strDocList = string.Concat(new string[] { strDocList, objElem.SelectSingleNode("@DocumentID").InnerText, "," });


                    }

                    //Check the size of the digital image transaction.
                    //Glaxis has a 4M limit on these due to their use of MSMQ.
                    //Compare to 4M - 1K to give a little wiggle room.
                    strTemp = objTrans.OuterXml;
                    if (strTemp.Length > ((1048576 * 4) - 1024))
                    {
                        throw new Exception(string.Concat(new string[] { CSceneGenesis.SceneGenesis_Error_Codes.eXmlTransactionTooLarge.ToString(), "4M Size Check", "NOT IMPLEMENTED", strTemp.Length.ToString(), "|", Convert.ToString((1048576 * 4) - 1024) }));
                    }

                    //Build the SOAP Envelope and put the HeartBeat into the envelope
                    strWrappedXml = mPPGTransactions.BuildTransactionForPPG("EstimateDI", strTemp, mLAPDPartnerDataMgr.g_strPartner, mLAPDPartnerDataMgr.g_strLynxID);

                    //Do the post to PPG
                    mPPGTransactions.XmlHttpPost(strPost2URL, strWrappedXml);

                    //Build a description of the event.
                    strTemp = string.Concat(new string[] { "Digital documents uploaded electronically to the insurance carrier through ", mLAPDPartnerDataMgr.g_strPartner, "." });

                    //Strip off trailing comma from the doc list.
                    if (!string.IsNullOrEmpty(strDocList))
                    {
                        strDocList = Left(strDocList, strDocList.Length - 1);
                    }

                    //Send a WorkFlow Event for the electronic transmission.
                    mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strLYNXConnStringStd);
                    mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("WorkFlow/SPName"), new object[] { "@EventID", mLAPDPartnerDataMgr.GetConfig("WorkFlow/EventCodes/Code[@name='ImagesSentToCarrier']"), "@LynxID", mLAPDPartnerDataMgr.g_strLynxID, "@PertainsTo", string.Concat(new string[] { "VEH", strVehicleNumber }), "@Description", strTemp, "@UserID",
                    strUserID, "@ReferenceData", strDocList});

                    //Have the database set the uploaded indicator on the document
                    mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("Document/UploadSetSPName"), new object[] { "@DocList", strDocList, "@UserID", strUserID });

                    //Stuff the electronic transmission XML in the partner specific database.
                    mLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(mLAPDPartnerDataMgr.g_strPartnerConnStringStd);
                    mLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(mLAPDPartnerDataMgr.GetConfig("PartnerSettings/PartnerLoadSP"), new object[] { "@AssignmentID", mLAPDPartnerDataMgr.g_strAssignmentID, "@LynxID", mLAPDPartnerDataMgr.g_strLynxID, "@VehicleNumber", strVehicleNumber, "@SequenceNumber", "0", "@TransactionDate",
                    string.Concat(new string[] { DateTime.Now.ToString("yyyy-mm-dd"), " ", DateTime.Now.ToString("hh:mm:ss") }), "@TransactionSource", "LYNX", "@TransactionType", "EstimateDI", "@PartnerXML", mEscapeUtilities.SQLQueryString(ref strWrappedXml)});

                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "URL = ", strPost2URL }), string.Concat(new string[] { PROC_NAME, mobjPartner.DocTypeString, " sent via HTTP" }));

                    if (mLAPDPartnerDataMgr.g_blnDebugMode)
                        mLAPDPartnerDataMgr.g_objEvents.Trace("", string.Concat(new string[] { PROC_NAME, "Document Upload Finished" }));

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objDocs = null;
                objTrans = null;
                objCEF = null;
                objList = null;
                objElem = null;
                objDocElem = null;
                objNewDocElem = null;
                objCdata = null;
                objPassThru = null;
                objRS.Close();
                objRS = null;
            }

        }


        public bool CPartner_PartnerDatabaseTransferV2(bool blnSendReceipt)
        {
            try
            {
                string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "PartnerDatabaseTransferV2:" });
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public string CPartner_ApdDatabaseTransferV2()
        {
            try
            {
                string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "ApdDatabaseTransferV2: " });
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public bool CPartner_InitProcessDocumentV2(string strTradingPartner, string strPassedDocument)
        {
            try
            {
                string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InitProcessDocumentV2: " });
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public long CPartner_InsertPrintImageIntoAPDV2(string strXML, long lngPartnerTransID)
        {
            try
            {
                string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "InsertPrintImageIntoAPDV2: " });
                throw new Exception(string.Concat(new string[] { MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.eNotImplemented.ToString(), PROC_NAME, "NOT IMPLEMENTED" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public string Left(string param, int length)
        {
            try
            {
                string result = param.Substring(0, length);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string Right(string param, int length)
        {
            try
            {
                string result = param.Substring(param.Length - length, length);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string Mid(string param, int startIndex, int length)
        {
            try
            {
                string result = param.Substring(startIndex, length);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string Mid(string param, int startIndex)
        {
            try
            {
                string result = param.Substring(startIndex);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
