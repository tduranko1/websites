﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    class MEscapeUtilities
    {
        private const string MODULE_NAME = "MstrEscapeUtils.";


        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }

        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }

        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }
        /// <summary>
        /// Returns string suitable for SQL queries by replacing all single quotes
        /// with 2 single quotes so that they will be accurately interpreted by the
        /// SQL engine as a literal single quote rather than a string delimiter
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public string SQLQueryString(ref string strInput)
        {
            return strInput.Replace("'", "''");
        }

        /// <summary>
        /// Escapes a string using URL type escaping (i.e. '%20' for a space ).
        /// Replaces all non-alphanumeric characters in a string as strEscaped sequences
        /// compatible with strEscaped URL strings.  That is, all special characters are
        /// replaced with %HH where HH is the two digit hex value of the character.
        ///  Note that although we use the same mechanism as URL escaping, we are more
        ///  regiorous in our interpretation of "special" characters.  For example,
        /// dashes and underscores will be strEscaped although they are not typically
        ///  strEscaped in URL strings.
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public string EscapeString(ref string strInput)
        {
            string strOut = string.Empty,
                strEscape = string.Empty;
            int intChar, count;
            try
            {
                strOut = strInput;

                for (count = strInput.Length; count >= 1; count += -1)
                {
                    //char to Ascii format
                    intChar = Convert.ToInt32(Mid(strInput, count, 1));

                    if (((intChar < 48) & (intChar != 32)) | ((intChar > 57) & (intChar < 65)) | ((intChar > 90) & (intChar < 97)) | (intChar > 122))
                    {
                        //Ascii to Hexa Decimal format
                        strEscape = String.Format("{0:X}", intChar);

                        if (strEscape.Length < 2)
                            strEscape = string.Concat(new string[] { "%0", strEscape });
                        else
                            strEscape = string.Concat(new string[] { "%" + strEscape });

                        strOut = string.Concat(new string[] { Left(strOut, count - 1), strEscape, Right(strOut, strOut.Length - count) });
                    }
                }
                return strOut.Replace(" ", "+");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        ///  Un-Escapes a string of URL type escaping (i.e. '%20' for a space ).
        ///Replaces all strEscape sequences within a string with the character represented
        /// by the strEscape sequence.  An strEscape sequence has the form %HH where HH is a
        ///two-digit hexidecimal number representing the ASCII code of the original
        ///character.  Note that this function does not properly check if the string
        ///was validly strEscaped in the first place.  When processing a string that
        ///contains literal %'s this function will behave unpredictably.
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public string UnEscapeString(ref string strInput)
        {
            string strOut = string.Empty;
            int count;
            try
            {
                strOut = strInput.Replace("+", " ");
                for (count = 1; count < strInput.Length; count++)
                {
                    if (Mid(strOut, count, 1) == "%")
                    {
                        if (Mid(strOut, count + 1, 1) == "u")
                        {
                            strOut = Left(strOut, count - 1) + Convert.ToChar(Convert.ToInt32("&H" + Mid(strOut, count + 2, 4))) + Right(strOut, strOut.Length - count - 5);
                        }
                        else
                        {
                            strOut = Left(strOut, count - 1) + Convert.ToChar(Convert.ToInt32("&H" + Mid(strOut, count + 1, 2))) + Right(strOut, strOut.Length - count - 2);
                        }
                    }
                }
                return strOut;

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        ///  Escapes a string of URL type escaping (i.e. '%20' for a space )
        ///  except this does NOT un-escape those characters not allowed in XML.
        ///  Thus this function is the nearly the same as UnEscapeString.
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public string UnEscapeStringAsXmlCompliant(ref string strInput)
        {
            string strOut = string.Empty, strCharacter = string.Empty;
            int count, iLen;

            try
            {
                strOut = strInput.Replace("+", " ");
                for (count = 1; count < strOut.Length; count++)
                {
                    if (Mid(strOut, count, 1) == "%")
                    {
                        if (Mid(strOut, count + 1, 1) == "u")
                        {
                            strCharacter = Convert.ToString(Convert.ToInt16("&H" + Mid(strOut, count + 2, 4)));
                            iLen = 5;
                        }
                        else
                        {
                            strCharacter = Convert.ToString(Convert.ToInt16("&H" + Mid(strOut, count + 1, 2)));
                            iLen = 5;
                        }

                        switch (strCharacter)
                        {
                            case "<":
                            case ">":
                            case "&":
                            default:
                                {
                                    strOut = Left(strOut, count - 1) + strCharacter + Right(strOut, strOut.Length - count - iLen);
                                    break;
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return strOut;
        }

        /// <summary>
        /// Removes strange formatting characters from XML.
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public string StripNonStdChars(ref string strInput)
        {
            string strOut = string.Empty;
            int count, intChar;

            try
            {
                strOut = strInput;
                for (count = 1; count <= strOut.Length; count++)
                {
                    intChar = Convert.ToInt16(Mid(strOut, count, 1));

                    if (intChar < 26 || intChar > 126)
                    {
                        strOut = string.Concat(new string[] { Left(strOut, count - 1), " ", Mid(strOut, count + 1) });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strOut;
        }

        /// <summary>
        /// Escapes an XML string of any markup characters (i.e. '&gt;' for '>')
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public string EscapeXmlMarkup(ref string strInput)
        {
            return strInput.Replace("&", "&amp;").Replace(">", "&gt;").Replace("", "&quot;").Replace("'", "&apos;").Replace("<", "&lt;");
        }

        /// <summary>
        /// UnEscapes an XML string of any markup characters (i.e. '>' for '&gt;')
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public string UnEscapeXmlMarkup(ref string strInput)
        {
            return strInput.Replace("<", "&lt;").Replace("'", "&apos;").Replace("", "&quot;").Replace(">", "&gt;").Replace("&", "&amp;");
        }

    }
}
