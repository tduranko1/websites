﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using DataAccessor;
using SiteUtilities;
using Lynx.APD.Component.Library.Common;
using System.Threading;

 
namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    class MLAPDPartnerDataMgr
    {
        string APP_NAME = "LAPDPartnerDataMgr.";
        string MODULE_NAME = "LAPDPartnerDataMgr.MLAPDPartnerDataMgr.cs";        
        //clsHelper mobjclsHelper = new clsHelper();
        //TODO
        public long LAPDPartnerDataMgr_FirstError = 0x80067000;

        public enum MLAPDPartnerDataMgr_ErrorCodes : ulong
        {
            eInvalidTradingPartner = 0x80067000 + 0x80,
            eCDataTagNotFound,
            eLynxIdInvalid,
            eBadTradingPartner,
            eBadDocumentType,
            eDuplicateDocument,
            eNackReceived,
            eNotImplemented,
            eBadSequenceNumber,
            eUnableToInitializeTrans,
            eNoRecordsReturned,
            eLynxIdUnknown,
            eShopSearchMissingParams,
            //Module specific error ranges.
            MPartnerDataMgr_BAS_ErrorCodes = 0x80067000 + 0x100,
            CPartnerDataXfer_CLS_ErrorCodes = 0x80067000 + 0x180,
            CAPD_CLS_ErrorCodes = 0x80067200 + 0x200,
            CCCC_CLS_ErrorCodes = 0x80067280 + 0x280,
            CExecute_CLS_ErrorCodes = 0x80067300 + 0x300,
            CSceneGenesis_CLS_ErrorCodes = 0x80067300 + 0x380,
            CPartner_CLS_ErrorCodes = 0x80067000 + 0x400,
            CAutoVerse_CLS_ErrorCodes = 0x80067000 + 0x480,
            CGrange_CLS_ErrorCodes = 0x80067000 + 0x560
        }


        public SiteUtilities.CEvents g_objEvents;
        public DataAccessor.CDataAccessor g_objDataAccessor;

        public bool g_blnDebugMode;
        public bool g_blnUsePartnerTypesForDocTypes;
        public string g_strSupportDocPath = string.Empty,
        g_strPartnerConnStringXml,     //Current strConn to DataAccesor for PARTNER XML data
        g_strPartnerConnStringStd,    //Current strConn to DataAccesor for PARTNER recordset data
        g_strLYNXConnStringXML,       //Current strConn to DataAccessor for LYNX XML data
        g_strLYNXConnStringStd,        //Current strConn to DataAccessor for LYNX recordset data
        g_strPartner,        //Trading partner name
        g_strPartnerSmallID,  //Small version of partner name (i.e. "SG" instead of "SceneGenesis")
         g_strPassedDocument,
        g_strLynxID,       //'LynxID associated with the inbound document as in 6008 only
        g_strActualLynxID,   //'Concatenated version from the Xml as in : 6008-1
        g_strVehicleNumber,  //'Vehicle Number
        g_strAssignmentID,   //'Assignment ID
        g_strTaskID,
        g_strActionCode,     //'Action Code: "N" for new assignment, "C" for cancellation.
        g_strStatus;

        public XmlDocument g_objPassedDocument;

        public object g_varDocumentReceivedDate;  //'The date/time that the document was received.
        public object g_varTransactionDate;       //'The date/time that the document was sent.

        //'This counter used to ensure the globals get intialized
        //'and desposed of only once.  This is required because various
        //'parts of the ECAD stuff have different entry points.
        private int mintInitCount;

        //'API declares
       // Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

        //[DllImport("kernel32")]
        //public void sleep(long dwMilliseconds);


        #region Struct
        struct typError
        {
            public long Number;
            public string Description;
            public string Source;
        }

        private typError objError;

        #endregion


        #region Methods
        // PushErr and PopErr function not been used

        public bool IsInitialized
        {
            get { return Convert.ToBoolean(mintInitCount != 0); }
        }



        /// <summary>
        /// Initialize Globals
        /// </summary>
        public void InitializeGlobals()
        {

            if (!IsInitialized)
            {
                //Create private member DataAccessor.:     g_objDataAccessor = New DataAccessor.CDataAccessor

                //Share DataAccessor's events object.
                g_objEvents = g_objDataAccessor.mobjEvents;

                //Get path to support document directory
                g_strSupportDocPath = string.Concat(new string[] { AppDomain.CurrentDomain.BaseDirectory, "\\", Left(APP_NAME, APP_NAME.Length - 1) });
                //  g_strSupportDocPath = App.Path + "\\" + Strings.Left(APP_NAME, Strings.Len(APP_NAME) - 1);

                //This will give partner data its own log file.
                g_objEvents.ComponentInstance = "Partner Data";

                //Initialize our member components first, so we have logging set up.
                g_objDataAccessor.InitEvents(string.Concat(new string[] { AppDomain.CurrentDomain.BaseDirectory, "\\..\\config\\config.xml" }),string.Empty,false);

                //Get debug mode status.
                g_blnDebugMode = g_objEvents.IsDebugMode;

                //g_objEvents.Assert g_strPassedDocument <> "", "Passed XML document string was blank."

                if (!string.IsNullOrEmpty(g_strPassedDocument))
                {
                    //Create a DOM document to hold the passed in document for reference
                    // g_objPassedDocument = new MSXML2.DOMDocument40();
                    g_objPassedDocument = new XmlDocument();
                    LoadXml(ref g_objPassedDocument, ref g_strPassedDocument, "InitializeGlobals()", "Passed");

                }

                //This connection points the component at the appropriate partner dBase
                g_strPartnerConnStringStd = GetConfig("PartnerSettings/ConnectionStringStd");
                g_strPartnerConnStringXml = GetConfig("PartnerSettings/ConnectionStringXml");

                //Lookup connection strings for the LYNX APD database
                g_strLYNXConnStringStd = GetConfig("ConnectionStringStd");
                g_strLYNXConnStringXML = GetConfig("ConnectionStringXml");

                if (g_blnDebugMode)
                    g_objEvents.Trace(g_strSupportDocPath, "DocPath");

            }

            mintInitCount = mintInitCount + 1;

        }


        /// <summary>
        /// Terminate Globals
        /// </summary>
        public void TerminateGlobals()
        {
            try
            {
                if (mintInitCount <= 1)
                {
                    g_objEvents = null;
                    g_objDataAccessor = null;
                    g_objPassedDocument = null;

                    mintInitCount = 0;
                }
                else
                {
                    mintInitCount = mintInitCount - 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Wraps CreateObject() with better debug information.
        /// </summary>
        /// <param name="strObjectName"></param>
        /// <returns>The Object created or Nothing.</returns>
        public object CreateObjectEx(string strObjectName)
        {
            object createdObject = null;
            Type CreateObj;
            try
            {
                //object obj = null;
                //obj = 
                CreateObj = Type.GetTypeFromProgID(strObjectName);
                createdObject = Activator.CreateInstance(CreateObj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return createdObject;
        }

        public long GetDefaultPartnerUserID(string strLogonID, string strApplicationCD = "APD")
        {
            //TODO Confirm the long Intialization
            long DefaultPartnerUserID = 0;
            string returnExecuteSpNamedParamsXML = string.Empty,
                tempDefaultPartnerUserID = string.Empty;
            XmlDocument objResults = null;
            try
            {
                objResults = new XmlDocument();
                g_objDataAccessor.SetConnectString(g_strLYNXConnStringXML);
                returnExecuteSpNamedParamsXML = g_objDataAccessor.ExecuteSpNamedParamsXML(GetConfig("ClaimPoint/WebAssignment/GetUserDetailSP"),new object[] { "@Login", strLogonID, "@ApplicationCD", strApplicationCD});
                if (!string.IsNullOrEmpty(returnExecuteSpNamedParamsXML))
                    objResults.LoadXml(returnExecuteSpNamedParamsXML);
                else
                    //TODO Need to confirm. because the LoadXML jus load the string into XML object
                    LoadXml(ref objResults, ref returnExecuteSpNamedParamsXML, "GetDefaultPartnerUserID", "Partner User Detail");

                tempDefaultPartnerUserID = GetChildNodeText(ref objResults, "/Root/User/@UserID");
                DefaultPartnerUserID = Convert.ToInt64(tempDefaultPartnerUserID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DefaultPartnerUserID;
        }

        /// <summary>
        /// Returns the requested configuration setting.
        /// </summary>
        /// <param name="strSetting"></param>
        /// <returns></returns>
        public string GetConfig(string strSetting)
        {
            string configSettings = string.Empty;
            try
            {
                configSettings = g_objEvents.mSettings.GetParsedSetting(strSetting);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return configSettings;
        }

        /// <summary>
        /// Builds up a date string to be used in document file names.
        /// </summary>
        /// <returns></returns>
        public string BuildFileNameDate()
        {
            string strBuildFileNameDate = string.Empty;
            try
            {
                // strBuildFileNameDate = 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strBuildFileNameDate;
        }

        /// <summary>
        /// Builds a specifically formatted time string, according to our XML docs, as "mmddyyyy".
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public string ConstructXmlDate(string date)
        {
            string
                //strMonth = string.Empty,
                //strDay = string.Empty,
                //strYear = string.Empty,
                strCurrDate = string.Empty;
            DateTime strBaseDate;
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    strBaseDate = Convert.ToDateTime(date);
                    strCurrDate = strBaseDate.ToString("MMddyyyy");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strCurrDate;
        }

        /// <summary>
        /// Builds a specifically formatted time string, according to our XML docs, as "hhmmss"
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public string ConstructXmlTime(string time)
        {
            string strCurrTime = string.Empty;
            DateTime strBaseTime;
            try
            {
                if (!string.IsNullOrEmpty(time))
                {
                    strBaseTime = Convert.ToDateTime(time);
                    strCurrTime = strBaseTime.ToString("HHmmss");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strCurrTime;
        }

        public string ConstructSqlDateTime(string strXmlDate, string strXmlTime, bool blnNowAsDefault = true)
        {
            string strDateTime = string.Empty,
                strTemp = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(strXmlDate))
                {
                    if (strXmlDate.Length == 8)
                        strDateTime = string.Concat(new string[] { Right(strXmlDate, 4), "-", Left(strXmlDate, 2), "-", Mid(strXmlDate, 3, 2) });
                    else if (blnNowAsDefault)
                        strDateTime = DateTime.Now.ToString("yyyy-MM-dd");
                }

                if (!string.IsNullOrEmpty(strXmlTime))
                {
                    if (strDateTime.Length > 0)
                    {
                        if (strDateTime.Length > 6)
                            strDateTime = Convert.ToString(new string[] { strDateTime, "T", Left(strXmlTime, 2), ":", Mid(strXmlTime, 3, 2), ":", Right(strXmlTime, 2) });
                        else if (strXmlDate.Length > 0)
                            strDateTime = string.Concat(new string[] { strDateTime, "T00:00:00" });
                        else if (blnNowAsDefault)
                            strDateTime = string.Concat(new string[] { strDateTime, DateTime.Now.ToString("THH:mm:ss") });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strDateTime;
        }


        /// <summary>
        /// Spits recordset contents out to the debug log.
        /// </summary>
        /// <param name="RS"></param>

        public void DebugRS(ref ADODB.Recordset RS)
        {
            string strRecord = string.Empty,
                strvalue = string.Empty;
            //ADODB.Field strField;
            try
            {
                RS.MoveFirst();

                strRecord = string.Concat(new string[] { "Record Count = ", Convert.ToString(RS.RecordCount), System.Environment.NewLine });
                strRecord = string.Concat(new string[] { strRecord, " Fields ", "," });

                foreach (ADODB.Field strField in RS.Fields)
                {
                    strRecord = Convert.ToString(new string[] { strRecord, strField.Name });
                }

                strRecord = string.Concat(new string[] { strRecord, System.Environment.NewLine });
                if (RS.RecordCount > 0)
                {
                    while (!RS.EOF)
                    {
                        strRecord = string.Concat(new string[] { strRecord, "  Data:" }); ;
                        foreach (ADODB.Field strField in RS.Fields)
                        {
                            if (strField.Value != null)
                            {
                                strvalue = Convert.ToString(strField.Value);
                                strvalue = strvalue.Replace(System.Environment.NewLine, "");
                                strvalue = strvalue.TrimEnd();
                                strvalue = strvalue.TrimStart();
                                if (!string.IsNullOrEmpty(strvalue) && strvalue.Length > 0)
                                {
                                    if ((Left(strvalue, 1) == "<") && (Right(strvalue, 1) == ">"))
                                        strRecord = string.Concat(new string[] { strRecord, "(XML NOT LOGGED)" });
                                    else
                                        strRecord = string.Concat(new string[] { strRecord, "'", Convert.ToString(strField.Value) });
                                }
                                else
                                    strRecord = string.Concat(new string[] { strRecord, "'' ," });
                            }
                            else
                                strRecord = string.Concat(new string[] { strRecord, "Null, " });
                        }
                        strRecord = string.Concat(new string[] { strRecord, System.Environment.NewLine });
                        RS.MoveNext();
                        strvalue = string.Empty;
                    }
                    g_objEvents.Trace(strRecord, string.Concat(new string[] { MODULE_NAME, "Recordset Contents" }));
                    RS.MoveFirst();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Pads a number out with zeros to make the requested number of digits.
        /// </summary>
        /// <param name="lngValue"></param>
        /// <param name="intDigits"></param>
        /// <returns></returns>
        public string ZeroPad(long lngValue, int intDigits)
        {
            string strValue = string.Empty,
                strZeroPad = string.Empty;
            try
            {
                strValue = Convert.ToString(lngValue);
                while (strValue.Length < intDigits)
                {
                    strValue = string.Concat(new string[] { "0", strValue });
                }
                strZeroPad = strValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return strZeroPad;
        }

        /// <summary>
        /// AsNumber - returns a Long based upon the first numeric portion of str.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public long AsNumber(string str)
        {
            long lngAsNumebr = 0;
            int countNumber,
                tempint;
            try
            {
                for (countNumber = 1; countNumber <= str.Length; countNumber++)
                {
                    if (int.TryParse(Left(str, countNumber), out tempint))
                        lngAsNumebr = Convert.ToInt64(Left(str, countNumber));
                    else
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lngAsNumebr;
        }

        public void DoSleep(long lngMS)
        {
            long lCount = 0;
            try
            {
                for (lCount = 1; lCount <= lngMS / 10; lCount++)
                {
                    Thread.Sleep(10);
                    //TODO
                    //DoEvents();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //TODO Need to remove the becasue this functionalities from MDomUtils .bas file
        // Need to confirm
        public void LoadXml(ref XmlDocument objDom, ref string strXml, string strCallerName, string strXmlDescription)
        {
            try
            {
                //loadXml returns true if the load succeeded.
                objDom.LoadXml(strXml);
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Returns the specified child node text of the passed node.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public string GetChildNodeText(ref XmlDocument objXml, string strPath, Boolean blnMustExist = false)
        {
            string childNode = null;
            XmlNode objChild = null;
            try
            {

                objChild = GetChildNode(ref objXml, strPath, blnMustExist);

                if (objChild == null)
                {
                    childNode = "";
                }
                else
                {
                    childNode = objChild.InnerText;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return childNode;

        }
        /// <summary>
        /// Returns the specified child node of the passed node.
        /// </summary>
        /// <param name="objNode"></param>
        /// <param name="strPath"></param>
        /// <param name="blnMustExist"></param>
        /// <returns></returns>
        public XmlNode GetChildNode(ref XmlDocument objXml, string strPath, bool blnMustExist = false)
        {
            XmlNode xmlNodeChild = null;
            try
            {
                XmlNode objChild;
                objChild = objXml.SelectSingleNode(strPath);

                if (objChild == null)
                {
                    if (blnMustExist == true)
                    {
                        throw new Exception(string.Concat(new string[] { Convert.ToString(MDomUtils.EventCodes.eXmlMissingElement), "GetChildNode('", strPath, "')" }));
                        // Err().Raise(eXmlMissingElement, "GetChildNode('" + strPath + "')", objNode.nodeName + " is missing a '" + strPath + "' child node.");
                    }
                }
                else
                {
                    xmlNodeChild = objChild;
                }

                objChild = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return xmlNodeChild;
        }


        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }


        /// <summary>
        ///  start at the index based on the lenght of the sting minus
        ///  the specified lenght and assign it a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Right(string param, int length)
        {
            string result = param.Substring(param.Length - length, length);
            return result;
        }


        /// <summary>
        /// start at the specified index in the string ang get N number of
        /// characters depending on the lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex, int length)
        {
            string result = param.Substring(startIndex, length);
            return result;
        }


        /// <summary>
        ///   start at the specified index and return all characters after it
        ///   and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="startIndex"></param>
        /// <returns> return the result of the operation</returns>
        public string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }
        #endregion

    }

}
