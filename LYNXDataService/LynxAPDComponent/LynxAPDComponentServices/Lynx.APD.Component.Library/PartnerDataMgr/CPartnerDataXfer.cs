﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Lynx.APD.Component.Library.Common;
using RulesEngine;
using NadaWrapper;

namespace Lynx.APD.Component.Library.PartnerDataMgr
{
    public class CPartnerDataXfer
    {
        /// <summary>
        /// Component PartnerDataXferManager : ClassModule CPartnerDataXfer
        /// 
        /// Routines for transferring data from partner db to apd db.
        /// </summary> 

        #region Declarations
        private const string APP_NAME = "LAPDPartnerDataMgr.";
        private string MODULE_NAME = string.Concat(new string[] { APP_NAME, "CPartnerDataXfer." });
        private long NADA_INVALID_VIN_ERROR = Convert.ToInt64("&H80064100");             //Error possibly returned from NADA Wrapper
        MEscapeUtilities objMEscapeUtilities = null;
        MLAPDPartnerDataMgr objMLAPDPartnerDataMgr = null;
        MDomUtils objMdomUtilis = null;
        CRulesEngine mobjRulesEngine = null;
        CNadaWrapper objCNadaWrapper = null;
        //Private vars used to save passing vars to private utility functions

        //private XmlNode mobjCurNode;       // Working node for data extract
        private XmlNode mobjRootNode;       //Root node of objAPDEstimateDataDom
        #endregion

        /// <summary>
        /// Error codes private to this module.
        /// </summary>

        #region Enumerators
        private enum ErrorCodes : ulong
        {
            ePartnerXferError = MLAPDPartnerDataMgr.MLAPDPartnerDataMgr_ErrorCodes.CPartnerDataXfer_CLS_ErrorCodes,
            eEstimateDataAuditFailed
        }

        //These enumerated types mirror the arguments to uspEstimateDetailUpdDetail.
        // They are used to simplify reading and updating the code that calls this proc.

        private enum EEstimateDetailUpdDetail
        {
            eDocumentID,                          // DocumentID of estimate
            eDetailNumber,                                                    // Line number from detail
            eAPDPartCategoryAgreedCD,                                         //The Agreed APD part category "bucket" this part belongs to "PAT", "PAN", "POT", "PON", etc
            eAPDPartCategoryOriginalCD,                                       //The Original APD part category "bucket" this part belongs to "PAT", "PAN", "POT", "PON", etc
            eComment,                                                         // Comment entered by shop for this line
            eDBLaborHoursOriginal,                                            //The Labor Hours from the estimate package's database
            eDBPriceOriginal,                                                 // The part price from the estimate package's database
            eDBRefinishLaborHoursOriginal,                                    // The Refinish Labor Hours from the estimate package's database
            eDescription,                                                     //Description
            eLaborHoursAgreed,                                                // Labor hours - agreed value
            eLaborHoursChangedFlag,                                           // Labor hours changed on estimate (this has nothing to do with original/agreed)
            eLaborHoursIncludedAgreedFlag,                                    // Labor hours included somewhere else - agreed value
            eLaborHoursIncludedOriginalFlag,                                  // Labor hours included somewhere else - original value
            eLaborHoursOriginal,                                              // Labor hours - original value
            eLaborTypeAgreedCD,                                               // (B)ody, (F)rame, (M)echanical - agreed value
            eLaborTypeOriginalCD,                                             // (B)ody, (F)rame, (M)echanical - original value
            eLynxComment,                                                     // Comment entered by Lynx Rep for this line
            eManualLineFlag,                                                  // Indicates whether this line was entered manually by the shop
            eOperationCD,                                                     // CIECA standard operation code "Op#"
            ePartTypeAgreedCD,                                               // CIECA standard part category code "PAL", "PAN", "PAS", etc - agreed value
            ePartTypeOriginalCD,                                              // CIECA standard part category code "PAL", "PAN", "PAS", etc - original value
            ePriceAgreed,                                                     // Agreed Price
            ePriceChangedFlag,                                                // Price changed on estimate (again, nothing to do with original/agreed)
            ePriceIncludedAgreedFlag,                                         // Part price included somewhere else - agreed value
            ePriceIncludedOriginalFlag,                                       // Part price included somewhere else - original value
            ePriceOriginal,                                                   // Price on estimate
            eQuantityAgreed,                                                  // Quantity
            eQuantityOriginal,                                                // Quantity on estimate
            eRefinishLaborHoursAgreed,                                        // Refinish hours
            eRefinishLaborHoursChangedFlag,                                   // Refinish hours changed on estimate (again, nothing to do with original/agreed)
            eRefinishLaborHoursOriginal,                                      // Refinish hours on estimate
            eRefinishLaborIncludedAgreedFlag,                                 // Refinsih Labor hours included somewhere else - agreed value
            eRefinishLaborIncludedOriginalFlag,                              // Refinish Labor hours included somewhere else - original value
            eUserID,                                                          // User ID (use either this OR @UserName)
            eUserName,                                                        // NT User Name ('System' is also acceptable)(use either this OR @UserID)
            eSysLastUpdatedDate,                                              // If update, syslastupdatedate.  Ignored on inserts
            eEstimateDetailUpdDetail_ParameterCount
        }

        #endregion

        #region Public Functions

        // ********************************************************************************
        //* Handles the insertion of the estimate data in APD.
        //*
        //* objEstimateDataDom   - Reference to a DOM document containing the Estimate Data XML
        //* lngApdDocumentID  - ID of related document in APD Database.
        //*                      Updated value is stored here once completed.
        //*
        //* Returns: the transformed Common Estimate Format (CEF) for debug purposes.
        //********************************************************************************
        public string TransferDataToAPD_v2(XmlDocument objEstimateDataDom, long lngAPDDocumentID, string strPassedDocument, string strPartnerSmallID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferDataToApd: " });

            //Objects that need clean up.
            XmlDocument objStyleSheetDom = null;             // Holds loaded stylesheet
            XmlDocument objAPDEstimateDataDom = null;       // Holds transformed "APD" formatted estimate data
            XmlNodeList objNodeList = null;               // Working Nodelist for data extraction
            object objNada = null;                            // This block of variables is used to decode the VIN through NADA
            XmlDocument objClaimXML = null;                 // This hold the claim information for the given assignment id
            XmlElement objAssignmentVehNode = null;         // Holds vehicle node in the claim information XML
            object objRulesEngine = null;                 // Rule Engine COM component
            XmlDocument objAPDEstimateRules = null;   // APD Estimate Rules
            XmlDocument objRulesOutputXML = null;    // Rules Engine output XML object
            XmlNodeList objRulesFiredList = null;       // The list of rules fired
            XmlElement objRuleFired = null;      // Rule Fired item

            //Primitives that dont.
            string strAction = string.Empty;           // Specific area of proc being executed (used for error tracing)
            string strAPDEstimateXML = string.Empty;          // String representation of transformed Estimate Data
            string[] strParameters = null;        // Working Parameter array for header
            int intTaxableFlag = 0;          // Transalated Database insertion value for Taxable flag In Summary
            string strTaxTypeCD = string.Empty;         // Translated Database insertion value for TaxTypeCD in Summary

            object varResults = null;        // and store
            string strYear = string.Empty;
            string strMake = string.Empty;
            string strModel = string.Empty;
            bool blnInTrans = false;
            string strAssignmentID = string.Empty;
            string strRulesOutput = string.Empty;
            int iAuditWeight = 0;
            int i = 0;
            string strInsertSummarySPName = string.Empty;
            string strInsertDetailSPName = string.Empty;
            string strInsertAuditResultSPName = string.Empty;
            string strNTUserID = string.Empty;
            string strVin = string.Empty;
            bool blnDAAssignment;
            string strfileAppend = string.Empty;
            object objAppend = null;
            try
            {
                objMEscapeUtilities = new MEscapeUtilities();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMdomUtilis = new MDomUtils();
                mobjRulesEngine = new CRulesEngine();
                objCNadaWrapper = new CNadaWrapper();

                objMLAPDPartnerDataMgr.g_objEvents.Assert(objEstimateDataDom != null, "Passed XML DOM was Nothing.");
                objMLAPDPartnerDataMgr.g_objEvents.Assert(lngAPDDocumentID != 0, "Passed APD Document ID was zero.");

                //***  Transform the Partner Data's XML into the "standard" APD format  ***
                strAction = "Transforming Partner Estimate Data";
                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "APD Document ID = ", lngAPDDocumentID.ToString() }), string.Concat(new string[] { PROC_NAME, strAction }));

                //Do the transfer.  Note that each partner has its own XSL.
                objAppend = objMLAPDPartnerDataMgr.g_objEvents;
                objAPDEstimateDataDom = objMdomUtilis.TransformDomAsDom(ref objEstimateDataDom, string.Concat(new string[] { objMLAPDPartnerDataMgr.g_strSupportDocPath, "/", strPartnerSmallID, "EstimateXfer.xsl" }), ref objAppend);

                strAPDEstimateXML = objAPDEstimateDataDom.OuterXml;

                //Return the transformed XML for debug purposes.
                return strAPDEstimateXML;

                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { " Results from Transform: ", Environment.NewLine, strAPDEstimateXML, "TransformDomToDom" }));

                //***  The transformed XML is now awaiting processing in objAPDEstimateDataDom  ***
                mobjRootNode = objAPDEstimateDataDom.DocumentElement;

                //Get the assignment id from the APDEstimate. We will use this later to obtain the claim information and
                // use it for estimate audit
                strAssignmentID = RootChild("Header/@AssignmentID");

                strNTUserID = objMLAPDPartnerDataMgr.GetConfig("WorkFlow/NTUserID");

                //Set the connect string to APD
                objMLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(objMLAPDPartnerDataMgr.g_strLYNXConnStringStd);

                //Everything that follows gets wrapped in a transaction.
                objMLAPDPartnerDataMgr.g_objDataAccessor.BeginTransaction();
                blnInTrans = true;   //Flag new transaction state.

                // Insert the estimate header information
                strAction = "Inserting APD Estimate Header";
                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, strAction }));

                // Dimension parameters array
                Array.Resize(ref strParameters, 65);

                // Compile parameter information into a variant array.  We have to do it this way or we will break VB's
                // line continuation limit.

                strParameters[0] = lngAPDDocumentID.ToString();
                strParameters[1] = string.Concat(new string[] { "'", RootChild("Header/Appraiser/@AppraiserLicenseNumber"), "'" });
                strParameters[2] = string.Concat(new string[] { "'", RootChild("Header/Appraiser/@AppraiserName"), "'" });
                strParameters[3] = string.Concat(new string[] { "'", RootChild("Header/@ClaimantName"), "'" });
                strParameters[4] = string.Concat(new string[] { "'", RootChild("Header/@ClaimNumber"), "'" });
                strParameters[5] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyAddress"), "'" });
                strParameters[6] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyAreaCode"), "'" });
                strParameters[7] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyCity"), "'" });
                strParameters[8] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyExchangeNumber"), "'" });
                strParameters[9] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyName"), "'" });
                strParameters[10] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyState"), "'" });
                strParameters[11] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyUnitNumber"), "'" });
                strParameters[12] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyZip"), "'" });

                // Detail Editable?
                // In an effort to protect the user from using bad data which we may receive from our VAN, a flag is used to indicate
                // whether the detail is editable or not.  If the data passes our audit tests, we set this flag to yes, allowing the
                // user to access the data.  If it doesn't, this flag is turned off to prevent write access.  In order to pass this test,
                // there must be no variance and all stylesheet audits must have passed.

                strParameters[13] = "0";

                if (RootChild("Summary/LineItem[@Type='Variance']/@ExtendedAmount") != "0")
                    objMLAPDPartnerDataMgr.g_objEvents.HandleEvent(Convert.ToInt32(ErrorCodes.eEstimateDataAuditFailed), string.Concat(new string[] { PROC_NAME, "Estimate Audit" }),
                         "Estimate Audit Failed: There must be no variance.",
                        string.Concat(new string[] {  "Partner document = " , Environment.NewLine , strPassedDocument , Environment.NewLine , Environment.NewLine ,
                         "After APD Transform = " , Environment.NewLine , strAPDEstimateXML}), Environment.NewLine, false);

                else if (mobjRootNode.SelectNodes("Audit/AuditItem[@Passed='No']").ToString().Length > 0)
                    objMLAPDPartnerDataMgr.g_objEvents.HandleEvent(Convert.ToInt32(ErrorCodes.eEstimateDataAuditFailed), string.Concat(new string[] { PROC_NAME, "Estimate Audit" }),
                         "Estimate Audit Failed: A stylesheet audit has failed.",
                        string.Concat(new string[] {"Partner document = " , Environment.NewLine , strPassedDocument , Environment.NewLine , Environment.NewLine ,
                         "After APD Transform = " , Environment.NewLine , strAPDEstimateXML}), Environment.NewLine, false);
                else
                    strParameters[13] = "1";

                strParameters[14] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionAddress"), "'" });
                strParameters[15] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionAreaCode"), "'" });
                strParameters[16] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionCity"), "'" });
                strParameters[17] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionExchangeNumber"), "'" });
                strParameters[18] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionLocation"), "'" });
                strParameters[19] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionState"), "'" });
                strParameters[20] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionType"), "'" });
                strParameters[21] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionUnitNumber"), "'" });
                strParameters[22] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionZip"), "'" });
                strParameters[23] = string.Concat(new string[] { "'", RootChild("Header/@InsuranceCompanyName"), "'" });
                strParameters[24] = string.Concat(new string[] { "'", RootChild("Header/@InsuredName"), "'" });
                strParameters[25] = string.Concat(new string[] { (RootChild("Header/@LossDate")).Length == 0 ? "NULL" : "'", RootChild("Header/@LossDate"), "'" });
                strParameters[26] = string.Concat(new string[] { "'", RootChild("Header/@LossType"), "'" });
                strParameters[27] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerAddress"), "'" });
                strParameters[28] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerCity"), "'" });
                strParameters[29] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerDayAreaCode"), "'" });
                strParameters[30] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerDayExchangeNumber"), "'" });
                strParameters[31] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerDayExtensionNumber"), "'" });
                strParameters[32] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerDayUnitNumber"), "'" });
                strParameters[33] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerEveningAreaCode"), "'" });
                strParameters[34] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerEveningExchangeNumber"), "'" });
                strParameters[35] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerEveningExtensionNumber"), "" });
                strParameters[36] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerEveningUnitNumber"), "'" });
                strParameters[37] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerName"), "'" });
                strParameters[38] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerState"), "'" });
                strParameters[39] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerZip"), "'" });
                strParameters[40] = string.Concat(new string[] { "'", RootChild("Header/@PolicyNumber"), "'" });
                strParameters[41] = string.Concat(new string[] { "'", RootChild("Header/@Remarks"), "'" });
                strParameters[42] = (RootChild("Header/@RepairDays")).Length == 0 ? "NULL" : RootChild("Header/@RepairDays");
                strParameters[43] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopAddress"), "'" });
                strParameters[44] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopAreaCode"), "'" });
                strParameters[45] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopCity"), "'" });
                strParameters[46] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopExchangeNumber"), "'" });
                strParameters[47] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopName"), "'" });
                strParameters[48] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopRegistrationNumber"), "'" });
                strParameters[49] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopState"), "'" });
                strParameters[50] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopUnitNumber"), "'" });
                strParameters[51] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopZip"), "'" });
                strParameters[52] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleBodyStyle"), "'" });
                strParameters[53] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleColor"), "'" });
                strParameters[54] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleEngineType"), "'" });

                // Impact Points - These are made up of 0 or more elements.  the Values of these elements need to be
                // concatenated together.

                //The primary impact point is listed first...
                strParameters[55] = string.Concat(new string[] { "'", RootChild("Header/Vehicle//ImpactPoint[@PrimaryImpactFlag='Yes']/@ImpactArea") });

                // Enumerate any additional impact points
                objNodeList = mobjRootNode.SelectNodes("Header/Vehicle//ImpactPoint[@PrimaryImpactFlag='No']");

                foreach (XmlNode mobjCurNode in objNodeList)
                    strParameters[55] = string.Concat(new string[] { strParameters[55], ", ", CurChild("@ImpactArea") });

                strParameters[55] = string.Concat(new string[] { strParameters[55], "'" });
                strParameters[56] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleMake"), "'" });
                strParameters[57] = (RootChild("Header/Vehicle/@VehicleMileage")).Length == 0 ? "NULL" : RootChild("Header/Vehicle/@VehicleMileage");
                strParameters[58] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleModel"), "'" });

                // Options - Like impact points, there may be 0 or more elements.  These also must be concatenated together
                strParameters[59] = "'";
                objNodeList = mobjRootNode.SelectNodes("Header/Vehicle//VehicleOption");

                foreach (XmlNode mobjCurNode in objNodeList)
                    strParameters[59] = string.Concat(new string[] { strParameters[59], CurChild("@Option"), ", " });

                if (strParameters[59].Length > 2) //Trim off last ", "
                    strParameters[59] = string.Concat(new string[] { strParameters[59].Substring(strParameters[59].Length - 2), "'" });
                else
                    strParameters[59] = "''";

                strParameters[60] = (RootChild("Header/Vehicle/@VehicleYear")).Length == 0 ? "NULL" : RootChild("Header/Vehicle/@VehicleYear");
                strParameters[61] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@Vin"), "'" });
                strParameters[62] = ((RootChild("Header/@WrittenDate")).Length < 6 ? "NULL" : string.Concat(new string[] { "'", RootChild("Header/@WrittenDate"), "'" }));
                strParameters[63] = "DEFAULT";
                strParameters[64] = string.Concat(new string[] { "'", strNTUserID, "'" });
                strParameters[65] = "DEFAULT";

                // OK, time to insert the estimate header record
                objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertHeaderSPName"), strParameters);

                // Insert Estimate Detail Records
                strAction = "Inserting APD Estimate Details";
                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, strAction }));

                // We'll need to use the parameters array again, reset it first to 0 to clear it, then back to the
                // length we need
                Array.Resize(ref strParameters, 0);
                Array.Resize(ref strParameters, (int)EEstimateDetailUpdDetail.eEstimateDetailUpdDetail_ParameterCount - 1);

                strInsertDetailSPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertDetailSPName");

                // Get a list of detail lines for for details.
                objNodeList = mobjRootNode.SelectNodes("Detail/DetailLine");

                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { "Detail Lines = ", objNodeList.ToString() }));

                foreach (XmlNode mobjCurNode in objNodeList)
                {
                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(mobjCurNode.OuterXml, "Current Line");

                    // Set Parameter array values
                    strParameters[(int)EEstimateDetailUpdDetail.eDocumentID] = lngAPDDocumentID.ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eDetailNumber] = CurChild("@DetailNumber");

                    switch (CurChild("Part/@APDPartCategory"))
                    {
                        case "TaxableParts":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'PAT'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'PAT'";
                            break;
                        case "NonTaxableParts":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'PAN'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'PAN'";
                            break;
                        case "OtherTaxableParts":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'POT'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'POT'";
                            break;
                        case "OtherNonTaxableParts":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'PON'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'PON'";
                            break;
                        case "Storage":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'STG'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'STG'";
                            break;
                        case "Sublet":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'SUB'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'SUB'";
                            break;
                        case "Towing":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'TOW'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'TOW'";
                            break;
                        case "Other":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'OTH'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'OTH'";
                            break;
                        default:
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "";
                            break;
                    }

                    strParameters[(int)EEstimateDetailUpdDetail.eComment] = string.Concat(new string[] { "'", CurChild("@Comment"), "'" });
                    strParameters[(int)EEstimateDetailUpdDetail.eDBLaborHoursOriginal] = CurChild("Labor[@LaborType!='Refinish']/@DBLaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eDBPriceOriginal] = CurChild("Part/@DBPartPrice");
                    strParameters[(int)EEstimateDetailUpdDetail.eDBRefinishLaborHoursOriginal] = CurChild("Labor[@LaborType='Refinish']/@DBLaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eDescription] = string.Concat(new string[] { "'", CurChild("@Description"), "'" });
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursAgreed] = CurChild("Labor[@LaborType!='Refinish']/@LaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursChangedFlag] = (CurChild("Labor[@LaborType!='Refinish']/@LaborHoursChangedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursIncludedAgreedFlag] = (CurChild("Labor[@LaborType!='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursIncludedOriginalFlag] = (CurChild("Labor[@LaborType!='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursOriginal] = CurChild("Labor[@LaborType!='Refinish']/@LaborHours");

                    switch (CurChild("Labor[@LaborType!='Refinish']/@LaborType"))
                    {
                        case "Body":
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeAgreedCD] = "'B'";
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeOriginalCD] = "'B'";
                            break;
                        case "Frame":
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeAgreedCD] = "'F'";
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeOriginalCD] = "'F'";
                            break;
                        case "Mechanical":
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeAgreedCD] = "'M'";
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeOriginalCD] = "'M'";
                            break;
                        default:
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeAgreedCD] = "";
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeOriginalCD] = "";
                            break;
                    }

                    strParameters[(int)EEstimateDetailUpdDetail.eLynxComment] = "DEFAULT";
                    strParameters[(int)EEstimateDetailUpdDetail.eManualLineFlag] = (CurChild("@ManualLineFlag") == "Yes" ? 1 : 0).ToString();

                    switch (CurChild("@OperationCode"))
                    {
                        case "":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "";
                            break;
                        case "Alignment":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'ALGN'";
                            break;
                        case "Comment":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'COM'";
                            break;
                        case "Other":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'OTH'";
                            break;
                        case "Refinish":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'REFN'";
                            break;
                        case "Remove/Install":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'R&I'";
                            break;
                        case "Repair":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'REPR'";
                            break;
                        case "Replace":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'REPL'";
                            break;
                        case "Sublet":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'SUB'";
                            break;
                        default:
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'!ND!'";
                            break;
                    }

                    switch (CurChild("Part/@PartType"))
                    {
                        case "Aftermarket":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'AFMK'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'AFMK'";
                            break;
                        case "Glass":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'GLS'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'GLS'";
                            break;
                        case "LKQ":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'LKQ'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'LKQ'";
                            break;
                        case "New":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'NEW'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'NEW'";
                            break;
                        case "Other":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'OTH'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'OTH'";
                            break;
                        case "Reconditioned":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'REC'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'REC'";
                            break;
                        case "Sublet":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'SUB'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'SUB'";
                            break;
                        default:
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "";
                            break;
                    }

                    strParameters[(int)EEstimateDetailUpdDetail.ePriceAgreed] = CurChild("Part/@PartPrice");
                    strParameters[(int)EEstimateDetailUpdDetail.ePriceChangedFlag] = (CurChild("Part/@PriceChangedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.ePriceIncludedAgreedFlag] = (CurChild("Part/@PartIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.ePriceIncludedOriginalFlag] = (CurChild("Part/@PartIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.ePriceOriginal] = CurChild("Part/@PartPrice");
                    strParameters[(int)EEstimateDetailUpdDetail.eQuantityAgreed] = CurChild("Part/@Quantity");
                    strParameters[(int)EEstimateDetailUpdDetail.eQuantityOriginal] = CurChild("Part/@Quantity");
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborHoursAgreed] = CurChild("Labor[@LaborType='Refinish']/@LaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborHoursChangedFlag] = (CurChild("Labor[@LaborType='Refinish']/@LaborHoursChangedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborHoursOriginal] = CurChild("Labor[@LaborType='Refinish']/@LaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborIncludedAgreedFlag] = (CurChild("Labor[@LaborType='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborIncludedOriginalFlag] = (CurChild("Labor[@LaborType='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eUserID] = "DEFAULT";
                    strParameters[(int)EEstimateDetailUpdDetail.eUserName] = string.Concat(new string[] { "'", strNTUserID, "'" });
                    strParameters[(int)EEstimateDetailUpdDetail.eSysLastUpdatedDate] = "DEFAULT";

                    // Insert the record
                    objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(strInsertDetailSPName, strParameters);

                    // Insert Estimate Summary records
                    // Estimate Summary records are inserted as individual line items (ie.  Parts Total, Refinish Labor Total,
                    // etc.)  We'll need to retrieve each of these out of the XML and insert them 1 at a time into summary
                    strAction = "Inserting APD Estimate Summary";
                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, strAction }));

                    // Get a list of line items for the summary.
                    objNodeList = mobjRootNode.SelectNodes("Summary/LineItem[@TransferLine='Yes']");

                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { "Line Items = ", (objNodeList.ToString().Length).ToString() }));

                    strInsertSummarySPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertSummarySPName");

                    foreach (XmlNode mobjCurNodes in objNodeList)
                    {
                        // Have to set the Database insertion values
                        intTaxableFlag = (CurChild("@Taxable") == "Yes" ? 1 : 0);

                        switch (CurChild("@TaxBase"))
                        {
                            case "Parts":
                                strTaxTypeCD = "PT";
                                break;
                            case "Materials":
                                strTaxTypeCD = "SP";
                                break;
                            case "PartsAndMaterials":
                                strTaxTypeCD = "MT";
                                break;
                            case "Labor":
                                strTaxTypeCD = "LB";
                            case "AdditionalCharges":
                                break;
                                strTaxTypeCD = "AC";
                                break;
                            case "Subtotal":
                                strTaxTypeCD = "ST";
                                break;
                            default:
                                strTaxTypeCD = "OT";
                                break;
                        }

                        // Insert the record
                        objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(strInsertSummarySPName, new object[] { 0, "DEFAULT", "'" + CurChild("@Type") + "'",
                             "'" + CurChild("@Category") + "'", lngAPDDocumentID, CurChild("@ExtendedAmount"),
                             CurChild("@Hours"), CurChild("@Percent"), intTaxableFlag,
                             CurChild("@UnitAmount"), "'" + CurChild("@Note") + "'", CurChild("@ExtendedAmount"),
                             CurChild("@Hours"), CurChild("@Percent"), intTaxableFlag, CurChild("@UnitAmount"),
                             "'" + strTaxTypeCD + "'", "DEFAULT", "'" + strNTUserID + "'",
                             "DEFAULT", "DEFAULT", "DEFAULT",
                             CurChild("@RepairHours"), CurChild("@ReplaceHours")});
                    }

                    // Insert Estimate Audit record
                    // The Estimate's audit record will contain data not already saved off in the estimate records in APD.
                    // It contains data used by APD to specifically audit the estimate.
                    strAction = "Inserting APD Estimate Audit Record";
                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, strAction }));

                    //Get the vehicle VIN.
                    strVin = RootChild("Header/Vehicle/@Vin");

                    //Do a VIN lookup if the VIN is not blank.
                    if (strVin != string.Empty)

                        // We need to run the VIN through NADA first.  We're not going to make the determination whether or not the
                        // VIN is valid here.  We're simply going to retrieve the info for determination later.
                        //objNada = objMLAPDPartnerDataMgr.CreateObjectEx("NadaWrapper.CNadaWrapper");

                        //Use lighter error handling here since this process isn't critical.
                        //The NADA Wrapper will log its own errors if they happen.

                        varResults = objCNadaWrapper.DecodeVin(strVin, 6);

                    //Strip out single quotes from results.
                    //Believe it or not, there are some bogus single quotes in the NADA database.
                    if (varResults != string.Empty)
                    {
                        strYear = varResults.ToString().Replace("'", "");
                        strMake = varResults.ToString().Replace("'", "");
                        strModel = varResults.ToString().Replace("'", "");
                    }

                    //objNada = null;



                    //Commit what we have in the transaction so far.  As per Test Track item 1430,
                    //the estimate audit which follows does not need to be included in the transaction,
                    objMLAPDPartnerDataMgr.g_objDataAccessor.CommitTransaction();
                    blnInTrans = false;  //Flag new transaction state.

                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Transaction Committed Successfully." }));

                    //New Estimate Auditing process
                    if (strAssignmentID != string.Empty)
                        if (Convert.ToInt64(strAssignmentID) > 0)
                            //get the claim detail for the assignment

                            blnDAAssignment = Convert.ToBoolean(objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(
                                 objMLAPDPartnerDataMgr.GetConfig("WorkFlow/DeskAuditAssignmentSP"), new object[] {
                                      "@AssignmentID", strAssignmentID}));

                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Estimate belongs to Desk Audit: ", blnDAAssignment.ToString() }));

                    if (blnDAAssignment == false)
                        if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                            objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Performing Rule based audit." }));

                    //Auditing needs to be done for program shop estimates only.
                    objMLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(objMLAPDPartnerDataMgr.g_strLYNXConnStringXML);

                    objClaimXML = new XmlDocument();
                    strfileAppend = objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(objMLAPDPartnerDataMgr.GetConfig("Estimate/GetAssignmentDetailSPName"), new object[]{
                                                                                    "@AssignmentID", strAssignmentID});
                    objMLAPDPartnerDataMgr.LoadXml(ref objClaimXML, ref strfileAppend, PROC_NAME, "TransferDataToAPD_v2");

                    //Set the connect string to APD
                    objMLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(objMLAPDPartnerDataMgr.g_strLYNXConnStringStd);

                    //get the vehicle node so we can add the nada values
                    objAssignmentVehNode = (XmlElement)objClaimXML.SelectSingleNode("/Root/Vehicle");

                    if (objAssignmentVehNode != null)
                    {
                        objAssignmentVehNode.SetAttribute("VehicleYearNADA", strYear);
                        objAssignmentVehNode.SetAttribute("VehicleMakeNADA", strMake);
                        objAssignmentVehNode.SetAttribute("VehicleModelNADA", strModel);
                    }

                    //load the APD Estimate audit rules
                    objAPDEstimateRules = new XmlDocument();
                    strfileAppend = string.Concat(new string[] { objMLAPDPartnerDataMgr.g_strSupportDocPath, "/", "APDEstimateRules.xml" });
                    objMdomUtilis.LoadXmlFile(ref objAPDEstimateRules, ref strfileAppend, PROC_NAME, "TransferDataToAPD_v2");

                    //create the Rules Engine object
                    //objRulesEngine = objMLAPDPartnerDataMgr.CreateObjectEx("RulesEngine.CRulesEngine");
                    //TODO

                    strRulesOutput = mobjRulesEngine.EngineExecute(objAPDEstimateDataDom.OuterXml, objAPDEstimateRules.OuterXml, objClaimXML.OuterXml);

                    if (strRulesOutput != string.Empty)
                        objRulesOutputXML = new XmlDocument();
                    objMLAPDPartnerDataMgr.LoadXml(ref objRulesOutputXML, ref strRulesOutput, PROC_NAME, "TransferDataToAPD_v2");

                    //get the audit weight
                    iAuditWeight = objRulesOutputXML.SelectSingleNode("/Root/@auditWeight").InnerText.Length;

                    Array.Resize(ref strParameters, 1);

                    strParameters[0] = lngAPDDocumentID.ToString();
                    strParameters[1] = iAuditWeight.ToString();

                    //update the document's audit weight
                    objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/UpdateAuditWeightSPName"), strParameters);

                    //now log the rules fired
                    objRulesFiredList = objRulesOutputXML.SelectNodes("/Root/RulesFired/Rule");

                    if (objRulesFiredList != null)
                    {
                        string sDetailNumber = string.Empty,
                            sKeyNames = string.Empty;

                        string[] aKeyName = null;
                        string sKeyValues = string.Empty;
                        string[] aKeyValue = null;

                        strInsertAuditResultSPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertAuditResultSPName");

                        foreach (XmlElement objRuleFiredlst in objRulesFiredList)
                        {
                            //insert the rule fired into the audit results table
                            Array.Resize(ref strParameters, 5);

                            sDetailNumber = "DEFAULT";

                            if (objRuleFired.GetAttribute("recursive") == "Y")
                            {
                                //get the detail number from the key values
                                sKeyNames = objRuleFired.GetAttribute("keyName");
                                sKeyValues = objRuleFired.GetAttribute("keyValue");

                                if (sKeyNames.IndexOf("DetailNumber", 1) > 0)
                                {
                                    aKeyName = sKeyNames.Split('|');
                                    for (i = aKeyName.GetLowerBound(0); i >= aKeyName.GetUpperBound(0); i++)
                                    {
                                        if (aKeyName[i].ToLower() == "detailnumber")
                                        {
                                            aKeyValue = sKeyValues.ToString().Split('|');
                                            if (i <= aKeyValue.GetUpperBound(0))
                                            {
                                                sDetailNumber = aKeyValue[i];
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            strParameters[0] = lngAPDDocumentID.ToString();
                            strParameters[1] = sDetailNumber;
                            strParameters[2] = string.Concat(new string[] { "'", (objRuleFired.GetAttribute("desc").Substring(0, 50).Replace("'", "''")), "'" });
                            strParameters[3] = string.Concat(new string[] { "'", (objRuleFired.SelectSingleNode("Action").InnerText).Replace("'", "''"), "'" });
                            strParameters[4] = objRuleFired.GetAttribute("weight");
                            strParameters[5] = string.Concat(new string[] { "'", strNTUserID, "'" });

                            objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(strInsertAuditResultSPName, strParameters);
                        }
                        if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                            objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Rule based audit complete." }));
                    }
                }
            }
            // *** Estimate Data Save is now complete ***

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //Clean up object refs.
                objStyleSheetDom = null;
                objAPDEstimateDataDom = null;
                mobjRootNode = null;
                objNodeList = null;
                objNada = null;
                objRulesEngine = null;
                objClaimXML = null;
                objAssignmentVehNode = null;
                objAPDEstimateRules = null;
                objRulesOutputXML = null;
                objRulesFiredList = null;
                objRuleFired = null;

                //Handle any errors that occur . . .

                //Need to roll back a transaction?
                if (blnInTrans)
                {
                    objMLAPDPartnerDataMgr.g_objDataAccessor.RollbackTransaction();
                }

            }

        }

        /// <summary>
        ///  "Back-end" utility method to allow attachment of estimate data to an
        ///  existing APD document.  Used to fix up estimates that have failed on insert.
        /// 
        ///  strXmlDoc = Estimate xml document received from calling application
        ///  lngAPDDocumentID = APD Document to attach estimate to.
        /// 
        ///  Returns: the transformed Common Estimate Format (CEF) for debug purposes.
        ///  </summary>

        public string AttachEstimateToAPDDocument(string strXMLDoc, string strTradingPartner, long lngAPDDocumentID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "AttachEstimateToApdDocument: " });
            try
            {
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();

                //Store the received date/time for later.
                objMLAPDPartnerDataMgr.g_varDocumentReceivedDate = DateTime.Now;

                //Put these in global scope for access by all procedures,
                //including "InitalizeGlobals" which does some initial work with it.
                objMLAPDPartnerDataMgr.g_strPassedDocument = strXMLDoc;
                objMLAPDPartnerDataMgr.g_strPartner = strTradingPartner;

                objMLAPDPartnerDataMgr.InitializeGlobals();

                objMLAPDPartnerDataMgr.g_objEvents.Assert(lngAPDDocumentID != 0, "Passed APD Document ID was zero.");

                objMLAPDPartnerDataMgr.g_objEvents.Assert(objMLAPDPartnerDataMgr.g_strPartner.Length > 0, "No Trading Partner value received");

                //Add parameter to trace data.
                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "APD Document ID = ", lngAPDDocumentID.ToString(), PROC_NAME, "Started" }));

                //Validate our parameters and trace them IF they are invalid.
                objMLAPDPartnerDataMgr.g_objEvents.Assert(objMLAPDPartnerDataMgr.g_strPassedDocument.Length > 0, "A blank Xml Document was received");

                //Get a little partner string from the config, i.e. "SG".
                objMLAPDPartnerDataMgr.g_strPartnerSmallID = objMLAPDPartnerDataMgr.GetConfig(string.Concat(new string[] { "PartnerSettings/", objMLAPDPartnerDataMgr.g_strPartner, "/@SmallID" }));


                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { objMLAPDPartnerDataMgr.g_strPassedDocument, PROC_NAME, "Finished" }));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //Now call the method below to do all the real work.
            return TransferDataToAPD(objMLAPDPartnerDataMgr.g_objPassedDocument, lngAPDDocumentID);
        }

        /// <summary>
        ///  Handles the insertion of the estimate data in APD.
        ///  
        ///  objEstimateDataDom   - Reference to a DOM document containing the Estimate Data XML
        ///  lngApdDocumentID  - ID of related document in APD Database.
        ///                       Updated value is stored here once completed.
        /// 
        ///  Returns: the transformed Common Estimate Format (CEF) for debug purposes.
        ///  </summary>

        public string TransferDataToAPD(XmlDocument objEstimateDataDom, long lngAPDDocumentID)
        {
            string PROC_NAME = string.Concat(new string[] { MODULE_NAME, "TransferDataToApd: " });
            //Objects that need clean up.

            object objNada = null;  //This block of variables is used to decode the VIN through NADA
            XmlDocument objStyleSheetDom = null;   //Holds loaded stylesheet
            XmlDocument objAPDEstimateDataDom = null;  // Holds transformed "APD" formatted estimate data
            XmlNodeList objNodeList = null;      // Working Nodelist for data extraction
            XmlDocument objClaimXML = null;     //This hold the claim information for the given assignment id\
            XmlElement objAssignmentVehNode = null;   // Holds vehicle node in the claim information XML
            object objRulesEngine = null;     //Rule Engine COM component
            XmlDocument objAPDEstimateRules = null;   // APD Estimate Rules
            XmlDocument objRulesOutputXML = null;   //Rules Engine output XML object
            XmlNodeList objRulesFiredList = null;      // The list of rules fired
            XmlElement objRuleFired = null;    // Rule Fired item

            //Primitives that dont.
            string strAction = string.Empty,   // Specific area of proc being executed (used for error tracing)
                strAPDEstimateXML = string.Empty, // String representation of transformed Estimate Data
                strTaxTypeCD = string.Empty;    //Translated Database insertion value for TaxTypeCD in Summary

            string[] strParameters = null;   //Working Parameter array for header

            int intTaxableFlag = 0;      // Transalated Database insertion value for Taxable flag In Summary

            object varResults = null;   // and store

            string strYear = string.Empty;
            string strMake = string.Empty;
            string strModel = string.Empty;
            bool blnInTrans = false;
            string strAssignmentID = string.Empty;
            string strRulesOutput = string.Empty;
            int iAuditWeight = 0;
            int i = 0;
            string strInsertSummarySPName = string.Empty;
            string strInsertDetailSPName = string.Empty;
            string strInsertAuditResultSPName = string.Empty;
            string strNTUserID = null;
            string strVin = string.Empty;
            bool blnDAAssignment = false;

            string sDetailNumber = string.Empty,
                                              sKeyNames = string.Empty,
                                              sKeyValues = string.Empty;
            string[] aKeyName = null,
                aKeyValue = null;
            string strappend = string.Empty;
            object objAppend = null;

            try
            {

                objMEscapeUtilities = new MEscapeUtilities();
                objMLAPDPartnerDataMgr = new MLAPDPartnerDataMgr();
                objMdomUtilis = new MDomUtils();
                mobjRulesEngine = new CRulesEngine();
                objCNadaWrapper = new CNadaWrapper();

                objMLAPDPartnerDataMgr.g_objEvents.Assert(objEstimateDataDom != null, "Passed XML DOM was Nothing.");
                objMLAPDPartnerDataMgr.g_objEvents.Assert(lngAPDDocumentID != 0, "Passed APD Document ID was zero.");

                //***  Transform the Partner Data's XML into the "standard" APD format  ***
                strAction = "Transforming Partner Estimate Data";
                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { "APD Document ID = ", lngAPDDocumentID.ToString() }), string.Concat(new string[] { PROC_NAME, strAction }));

                //Do the transfer.  Note that each partner has its own XSL.
                objAppend = objMLAPDPartnerDataMgr.g_objEvents;
                objAPDEstimateDataDom = objMdomUtilis.TransformDomAsDom(ref objEstimateDataDom, (string.Concat(new string[] { objMLAPDPartnerDataMgr.g_strSupportDocPath, "/", objMLAPDPartnerDataMgr.g_strPartnerSmallID, "EstimateXfer.xsl" })), ref objAppend);

                strAPDEstimateXML = objAPDEstimateDataDom.OuterXml;


                //Return the transformed XML for debug purposes.
                return strAPDEstimateXML;

                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Concat(new string[] { " Results from Transform: ", Environment.NewLine, strAPDEstimateXML }), "TransformDomToDom");

                //***  The transformed XML is now awaiting processing in objAPDEstimateDataDom  ***

                mobjRootNode = objAPDEstimateDataDom.DocumentElement;

                //Get the assignment id from the APDEstimate. We will use this later to obtain the claim information and
                // use it for estimate audit
                strAssignmentID = RootChild("Header/@AssignmentID");

                strNTUserID = objMLAPDPartnerDataMgr.GetConfig("WorkFlow/NTUserID");

                //Set the connect string to APD
                objMLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(objMLAPDPartnerDataMgr.g_strLYNXConnStringStd);

                //Everything that follows gets wrapped in a transaction.
                objMLAPDPartnerDataMgr.g_objDataAccessor.BeginTransaction();
                blnInTrans = true;   //Flag new transaction state.

                // Insert the estimate header information
                strAction = "Inserting APD Estimate Header";
                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, strAction }));

                // Dimension parameters array
                strParameters = new string[65];

                // Compile parameter information into a variant array.  We have to do it this way or we will break VB's
                // line continuation limit.

                strParameters[0] = lngAPDDocumentID.ToString();
                strParameters[1] = string.Concat(new string[] { "'", RootChild("Header/Appraiser/@AppraiserLicenseNumber"), "'" });
                strParameters[2] = string.Concat(new string[] { "'", RootChild("Header/Appraiser/@AppraiserName"), "'" });
                strParameters[3] = string.Concat(new string[] { "'", RootChild("Header/@ClaimantName"), "'" });
                strParameters[4] = string.Concat(new string[] { "'", RootChild("Header/@ClaimNumber"), "'" });
                strParameters[5] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyAddress"), "'" });
                strParameters[6] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyAreaCode"), "'" });
                strParameters[7] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyCity"), "'" });
                strParameters[8] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyExchangeNumber"), "'" });
                strParameters[9] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyName"), "'" });
                strParameters[10] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyState"), "'" });
                strParameters[11] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyUnitNumber"), "'" });
                strParameters[12] = string.Concat(new string[] { "'", RootChild("Header/Estimator/@CompanyZip"), "'" });

                // Detail Editable?
                // In an effort to protect the user from using bad data which we may receive from our VAN, a flag is used to indicate
                // whether the detail is editable or not.  If the data passes our audit tests, we set this flag to yes, allowing the
                // user to access the data.  If it doesn't, this flag is turned off to prevent write access.  In order to pass this test,
                //there must be no variance and all stylesheet audits must have passed.

                strParameters[13] = "0";

                if (RootChild("Summary/LineItem[@Type='Variance']/@ExtendedAmount") != "0")
                    objMLAPDPartnerDataMgr.g_objEvents.HandleEvent(Convert.ToInt32(ErrorCodes.eEstimateDataAuditFailed), string.Concat(new string[] { PROC_NAME, "Estimate Audit" }),
                         "Estimate Audit Failed: There must be no variance.",
                        string.Concat(new string[] {"Partner document = " , Environment.NewLine , objMLAPDPartnerDataMgr.g_strPassedDocument , Environment.NewLine , Environment.NewLine ,
                         "After APD Transform = " , Environment.NewLine,strAPDEstimateXML}), Environment.NewLine, false);

                else if (mobjRootNode.SelectNodes("Audit/AuditItem[@Passed='No']").ToString().Length > 0)
                    objMLAPDPartnerDataMgr.g_objEvents.HandleEvent(Convert.ToInt32(ErrorCodes.eEstimateDataAuditFailed), string.Concat(new string[] { PROC_NAME, "Estimate Audit" }), "Estimate Audit Failed: A stylesheet audit has failed.",
                        string.Concat(new string[] {"Partner document = " , Environment.NewLine , objMLAPDPartnerDataMgr.g_strPassedDocument , Environment.NewLine , Environment.NewLine ,
                         "After APD Transform = " , Environment.NewLine , strAPDEstimateXML}), Environment.NewLine, false);
                else
                    strParameters[13] = "1";

                strParameters[14] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionAddress"), "'" });
                strParameters[15] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionAreaCode"), "'" });
                strParameters[16] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionCity"), "'" });
                strParameters[17] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionExchangeNumber"), "'" });
                strParameters[18] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionLocation"), "'" });
                strParameters[19] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionState"), "'" });
                strParameters[20] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionType"), "'" });
                strParameters[21] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionUnitNumber"), "'" });
                strParameters[22] = string.Concat(new string[] { "'", RootChild("Header/Inspection/@InspectionZip"), "'" });
                strParameters[23] = string.Concat(new string[] { "'", RootChild("Header/@InsuranceCompanyName"), "'" });
                strParameters[24] = string.Concat(new string[] { "'", RootChild("Header/@InsuredName"), "'" });
                strParameters[25] = ((RootChild("Header/@LossDate")).Length == 0 ? "NULL" : string.Concat(new string[] { "'", RootChild("Header/@LossDate"), "'" }));
                strParameters[26] = string.Concat(new string[] { "'", RootChild("Header/@LossType"), "'" });
                strParameters[27] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerAddress"), "'" });
                strParameters[28] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerCity"), "'" });
                strParameters[29] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerDayAreaCode"), "'" });
                strParameters[30] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerDayExchangeNumber"), "'" });
                strParameters[31] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerDayExtensionNumber"), "'" });
                strParameters[32] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerDayUnitNumber"), "'" });
                strParameters[33] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerEveningAreaCode"), "'" });
                strParameters[34] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerEveningExchangeNumber"), "'" });
                strParameters[35] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerEveningExtensionNumber"), "'" });
                strParameters[36] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerEveningUnitNumber"), "'" });
                strParameters[37] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerName"), "'" });
                strParameters[38] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerState"), "'" });
                strParameters[39] = string.Concat(new string[] { "'", RootChild("Header/Owner/@OwnerZip"), "'" });
                strParameters[40] = string.Concat(new string[] { "'", RootChild("Header/@PolicyNumber"), "'" });
                strParameters[41] = string.Concat(new string[] { "'", RootChild("Header/@Remarks"), "'" });
                strParameters[42] = ((RootChild("Header/@RepairDays")).Length == 0 ? "NULL" : RootChild("Header/@RepairDays"));
                strParameters[43] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopAddress"), "'" });
                strParameters[44] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopAreaCode"), "'" });
                strParameters[45] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopCity"), "'" });
                strParameters[46] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopExchangeNumber"), "'" });
                strParameters[47] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopName"), "'" });
                strParameters[48] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopRegistrationNumber"), "'" });
                strParameters[49] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopState"), "'" });
                strParameters[50] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopUnitNumber"), "'" });
                strParameters[51] = string.Concat(new string[] { "'", RootChild("Header/Shop/@ShopZip"), "'" });
                strParameters[52] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleBodyStyle"), "'" });
                strParameters[53] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleColor"), "'" });
                strParameters[54] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleEngineType"), "'" });

                // Impact Points - These are made up of 0 or more elements.  the Values of these elements need to be
                // concatenated together.

                //The primary impact point is listed first...
                strParameters[55] = string.Concat(new string[] { "'", RootChild("Header/Vehicle//ImpactPoint[@PrimaryImpactFlag='Yes']/@ImpactArea") });

                // Enumerate any additional impact points
                objNodeList = mobjRootNode.SelectNodes("Header/Vehicle//ImpactPoint[@PrimaryImpactFlag='No']");

                foreach (XmlNode mobjCurNode in objNodeList)
                    strParameters[55] = string.Concat(new string[] { strParameters[55], ", ", CurChild("@ImpactArea") });

                strParameters[55] = string.Concat(new string[] { strParameters[55], "'" });

                strParameters[56] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleMake"), "'" });
                strParameters[57] = (RootChild("Header/Vehicle/@VehicleMileage")).Length == 0 ? "NULL" : RootChild("Header/Vehicle/@VehicleMileage");
                strParameters[58] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@VehicleModel"), "'" });

                // Options - Like impact points, there may be 0 or more elements.  These also must be concatenated together

                strParameters[59] = "'";
                objNodeList = mobjRootNode.SelectNodes("Header/Vehicle//VehicleOption");
                foreach (XmlNode mobjCurNode in objNodeList)
                    strParameters[59] = string.Concat(new string[] { strParameters[59], CurChild("@Option"), ", " });

                if ((strParameters[59]).Length > 2)   //Trim off last ", ";
                    strParameters[59] = string.Concat(new string[] { strParameters[59].Substring(0, (strParameters[59]).Length - 2), "'" });
                else
                    strParameters[59] = "''";

                strParameters[60] = ((RootChild("Header/Vehicle/@VehicleYear")).Length == 0 ? "NULL" : RootChild("Header/Vehicle/@VehicleYear"));
                strParameters[61] = string.Concat(new string[] { "'", RootChild("Header/Vehicle/@Vin"), "'" });
                strParameters[62] = ((RootChild("Header/@WrittenDate")).Length < 6 ? "NULL" : string.Concat(new string[] { "'", RootChild("Header/@WrittenDate"), "'" }));
                strParameters[63] = "DEFAULT";
                strParameters[64] = string.Concat(new string[] { "'", strNTUserID, "'" });
                strParameters[65] = "DEFAULT";

                // OK, time to insert the estimate header record
                objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertHeaderSPName"), strParameters);

                // Insert Estimate Detail Records
                strAction = "Inserting APD Estimate Details";
                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, strAction }));

                // We'll need to use the parameters array again, reset it first to 0 to clear it, then back to the
                // length we need
                Array.Resize(ref strParameters, 0);
                Array.Resize(ref strParameters, EEstimateDetailUpdDetail.eEstimateDetailUpdDetail_ParameterCount.ToString().Length - 1);

                strInsertDetailSPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertDetailSPName");

                // Get a list of detail lines for for details.
                objNodeList = mobjRootNode.SelectNodes("Detail/DetailLine");

                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { "Detail Lines = ", (objNodeList.ToString().Length).ToString() }));

                foreach (XmlNode mobjCurNode in objNodeList)
                {
                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(mobjCurNode.OuterXml, "Current Line");

                    // Set Parameter array values
                    strParameters[(int)EEstimateDetailUpdDetail.eDocumentID] = lngAPDDocumentID.ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eDetailNumber] = CurChild("@DetailNumber");

                    switch (CurChild("Part/@APDPartCategory"))
                    {
                        case "TaxableParts":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'PAT'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'PAT'";
                            break;
                        case "NonTaxableParts":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'PAN'";

                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'PAN'";
                            break;
                        case "OtherTaxableParts":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'POT'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'POT'";
                            break;
                        case "OtherNonTaxableParts":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'PON'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'PON'";
                            break;
                        case "Storage":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'STG'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'STG'";
                            break;
                        case "Sublet":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'SUB'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'SUB'";
                            break;
                        case "Towing":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'TOW'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'TOW'";
                            break;
                        case "Other":
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "'OTH'";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "'OTH'";
                            break;
                        default:
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryAgreedCD] = "";
                            strParameters[(int)EEstimateDetailUpdDetail.eAPDPartCategoryOriginalCD] = "";
                            break;
                    }

                    strParameters[(int)EEstimateDetailUpdDetail.eComment] = string.Concat(new string[] { "'", CurChild("@Comment"), "'" });
                    strParameters[(int)EEstimateDetailUpdDetail.eDBLaborHoursOriginal] = CurChild("Labor[@LaborType!='Refinish']/@DBLaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eDBPriceOriginal] = CurChild("Part/@DBPartPrice");
                    strParameters[(int)EEstimateDetailUpdDetail.eDBRefinishLaborHoursOriginal] = CurChild("Labor[@LaborType='Refinish']/@DBLaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eDescription] = string.Concat(new string[] { "'", CurChild("@Description"), "'" });
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursAgreed] = CurChild("Labor[@LaborType!='Refinish']/@LaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursChangedFlag] = (CurChild("Labor[@LaborType!='Refinish']/@LaborHoursChangedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursIncludedAgreedFlag] = (CurChild("Labor[@LaborType!='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursIncludedOriginalFlag] = (CurChild("Labor[@LaborType!='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eLaborHoursOriginal] = CurChild("Labor[@LaborType!='Refinish']/@LaborHours");

                    switch (CurChild("Labor[@LaborType!='Refinish']/@LaborType"))
                    {
                        case "Body":
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeAgreedCD] = "'B'";
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeOriginalCD] = "'B'";
                            break;
                        case "Frame":
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeAgreedCD] = "'F'";
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeOriginalCD] = "'F'";
                            break;
                        case "Mechanical":
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeAgreedCD] = "'M'";
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeOriginalCD] = "'M'";
                            break;
                        default:
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeAgreedCD] = "";
                            strParameters[(int)EEstimateDetailUpdDetail.eLaborTypeOriginalCD] = "";
                            break;
                    }

                    strParameters[(int)EEstimateDetailUpdDetail.eLynxComment] = "DEFAULT";
                    strParameters[(int)EEstimateDetailUpdDetail.eManualLineFlag] = (CurChild("@ManualLineFlag") == "Yes" ? 1 : 0).ToString();

                    switch (CurChild("@OperationCode"))
                    {
                        case "":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "";
                            break;
                        case "Alignment":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'ALGN'";
                            break;
                        case "Comment":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'COM'";
                            break;
                        case "Other":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'OTH'";
                            break;
                        case "Refinish":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'REFN'";
                            break;
                        case "Remove/Install":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'R&I'";
                            break;
                        case "Repair":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'REPR'";
                            break;
                        case "Replace":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'REPL'";
                            break;
                        case "Sublet":
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'SUB'";
                            break;
                        default:
                            strParameters[(int)EEstimateDetailUpdDetail.eOperationCD] = "'!ND!'";
                            break;
                    }

                    switch (CurChild("Part/@PartType"))
                    {
                        case "Aftermarket":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'AFMK'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'AFMK'";
                            break;
                        case "Glass":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'GLS'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'GLS'";
                            break;
                        case "LKQ":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'LKQ'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'LKQ'";
                            break;
                        case "New":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'NEW'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'NEW'";
                            break;
                        case "Other":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'OTH'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'OTH'";
                            break;
                        case "Reconditioned":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'REC'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'REC'";
                            break;
                        case "Sublet":
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "'SUB'";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "'SUB'";
                            break;
                        default:
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeAgreedCD] = "";
                            strParameters[(int)EEstimateDetailUpdDetail.ePartTypeOriginalCD] = "";
                            break;
                    }

                    strParameters[(int)EEstimateDetailUpdDetail.ePriceAgreed] = CurChild("Part/@PartPrice");
                    strParameters[(int)EEstimateDetailUpdDetail.ePriceChangedFlag] = (CurChild("Part/@PriceChangedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.ePriceIncludedAgreedFlag] = (CurChild("Part/@PartIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.ePriceIncludedOriginalFlag] = (CurChild("Part/@PartIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.ePriceOriginal] = CurChild("Part/@PartPrice");
                    strParameters[(int)EEstimateDetailUpdDetail.eQuantityAgreed] = CurChild("Part/@Quantity");
                    strParameters[(int)EEstimateDetailUpdDetail.eQuantityOriginal] = CurChild("Part/@Quantity");
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborHoursAgreed] = CurChild("Labor[@LaborType='Refinish']/@LaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborHoursChangedFlag] = (CurChild("Labor[@LaborType='Refinish']/@LaborHoursChangedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborHoursOriginal] = CurChild("Labor[@LaborType='Refinish']/@LaborHours");
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborIncludedAgreedFlag] = (CurChild("Labor[@LaborType='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eRefinishLaborIncludedOriginalFlag] = (CurChild("Labor[@LaborType='Refinish']/@LaborIncludedFlag") == "Yes" ? 1 : 0).ToString();
                    strParameters[(int)EEstimateDetailUpdDetail.eUserID] = "DEFAULT";
                    strParameters[(int)EEstimateDetailUpdDetail.eUserName] = "'" + strNTUserID + "'";
                    strParameters[(int)EEstimateDetailUpdDetail.eSysLastUpdatedDate] = "DEFAULT";

                    // Insert the record
                    objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(strInsertDetailSPName, strParameters);

                    // Insert Estimate Summary records
                    // Estimate Summary records are inserted as individual line items (ie.  Parts Total, Refinish Labor Total,
                    // etc.)  We'll need to retrieve each of these out of the XML and insert them 1 at a time into summary
                    strAction = "Inserting APD Estimate Summary";
                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, strAction }));

                    // Get a list of line items for the summary.
                    objNodeList = mobjRootNode.SelectNodes("Summary/LineItem[@TransferLine='Yes']");

                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { "Line Items = ", (objNodeList.ToString().Length).ToString() }));

                    strInsertSummarySPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertSummarySPName");

                    foreach (XmlNode mobjCurNod in objNodeList)
                        // Have to set the Database insertion values
                        intTaxableFlag = (CurChild("@Taxable") == "Yes" ? 1 : 0);

                    switch (CurChild("@TaxBase"))
                    {
                        case "Parts":
                            strTaxTypeCD = "PT";
                            break;
                        case "Materials":
                            strTaxTypeCD = "SP";
                            break;
                        case "PartsAndMaterials":
                            strTaxTypeCD = "MT";
                            break;
                        case "Labor":
                            strTaxTypeCD = "LB";
                        case "AdditionalCharges":
                            break;
                            strTaxTypeCD = "AC";
                        case "Subtotal":
                            strTaxTypeCD = "ST";
                            break;
                        default:
                            strTaxTypeCD = "OT";
                            break;
                    }

                    // Insert the record
                    objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(strInsertSummarySPName, new object[] { 0, "DEFAULT", "'" + CurChild("@Type") + "'",
                          "'" + CurChild("@Category") + "'", lngAPDDocumentID, CurChild("@ExtendedAmount"),
                          CurChild("@Hours"), CurChild("@Percent"), intTaxableFlag,
                          CurChild("@UnitAmount"), "'" + CurChild("@Note") + "'", CurChild("@ExtendedAmount"),
                          CurChild("@Hours"), CurChild("@Percent"), intTaxableFlag, CurChild("@UnitAmount"),
                          "'" + strTaxTypeCD + "'", "DEFAULT", "'" + strNTUserID + "'",
                          "DEFAULT", "DEFAULT", "DEFAULT",
                          CurChild("@RepairHours"), CurChild("@ReplaceHours")});

                    // Insert Estimate Audit record
                    // The Estimate's audit record will contain data not already saved off in the estimate records in APD.
                    // It contains data used by APD to specifically audit the estimate.
                    strAction = "Inserting APD Estimate Audit Record";
                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, strAction }));

                    //Get the vehicle VIN.
                    strVin = RootChild("Header/Vehicle/@Vin");

                    //Do a VIN lookup if the VIN is not blank.
                    if (strVin != string.Empty)
                    {

                        //We need to run the VIN through NADA first.  We're not going to make the determination whether or not the
                        // VIN is valid here.  We're simply going to retrieve the info for determination later.
                        // objNada = objMLAPDPartnerDataMgr.CreateObjectEx("NadaWrapper.CNadaWrapper");

                        //Use lighter error handling here since this process isn't critical.
                        //The NADA Wrapper will log its own errors if they happen.

                        varResults = objCNadaWrapper.DecodeVin(strVin, 6);

                        //Strip out single quotes from results.
                        //Believe it or not, there are some bogus single quotes in the NADA database.
                        if (varResults != string.Empty)
                        {
                            strYear = (varResults.ToString().Replace("'", ""));
                            strMake = (varResults.ToString().Replace("'", ""));
                            strModel = (varResults.ToString().Replace("'", ""));
                        }

                        //objNada = null;
                    }
                    //Commit what we have in the transaction so far.  As per Test Track item 1430,
                    //the estimate audit which follows does not need to be included in the transaction,
                    objMLAPDPartnerDataMgr.g_objDataAccessor.CommitTransaction();
                    blnInTrans = false; //Flag new transaction state.

                    if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                        objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Transaction Committed Successfully." }));

                    //New Estimate Auditing process
                    if (strAssignmentID != string.Empty)
                        if (strAssignmentID.Length > 0)
                        {
                            //get the claim detail for the assignment

                            blnDAAssignment = Convert.ToBoolean(objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParams(
                                 objMLAPDPartnerDataMgr.GetConfig("WorkFlow/DeskAuditAssignmentSP"), new object[] {
                                      "@AssignmentID", strAssignmentID}));

                            if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                                objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Estimate belongs to Desk Audit: ", blnDAAssignment.ToString() }));

                            if (blnDAAssignment == false)
                                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Performing Rule based audit." }));

                            //Auditing needs to be done for program shop estimates only.
                            objMLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(objMLAPDPartnerDataMgr.g_strLYNXConnStringXML);

                            objClaimXML = new XmlDocument();
                            strappend = objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSpNamedParamsXML(objMLAPDPartnerDataMgr.GetConfig("Estimate/GetAssignmentDetailSPName"), new object[] { "@AssignmentID", strAssignmentID });
                            objMLAPDPartnerDataMgr.LoadXml(ref objClaimXML, ref strappend, PROC_NAME, "TransferDataToAPD");

                            //Set the connect string to APD
                            objMLAPDPartnerDataMgr.g_objDataAccessor.SetConnectString(objMLAPDPartnerDataMgr.g_strLYNXConnStringStd);

                            //get the vehicle node so we can add the nada values
                            objAssignmentVehNode = (XmlElement)objClaimXML.SelectSingleNode("/Root/Vehicle");

                            if (objAssignmentVehNode != null)
                            {
                                objAssignmentVehNode.SetAttribute("VehicleYearNADA", strYear);
                                objAssignmentVehNode.SetAttribute("VehicleMakeNADA", strMake);
                                objAssignmentVehNode.SetAttribute("VehicleModelNADA", strModel);
                            }

                            //load the APD Estimate audit rules
                            objAPDEstimateRules = new XmlDocument();
                            strappend = string.Concat(new string[] { objMLAPDPartnerDataMgr.g_strSupportDocPath, "/", "APDEstimateRules.xml" });
                            objMdomUtilis.LoadXmlFile(ref objAPDEstimateRules, ref strappend, PROC_NAME, "TransferDataToAPD");

                            //create the Rules Engine object

                            strRulesOutput = mobjRulesEngine.EngineExecute(objAPDEstimateDataDom.OuterXml,
                                                                            objAPDEstimateRules.OuterXml,
                                                                            objClaimXML.OuterXml);

                            if (strRulesOutput != string.Empty)
                            {
                                objRulesOutputXML = new XmlDocument();
                                objMLAPDPartnerDataMgr.LoadXml(ref objRulesOutputXML, ref strRulesOutput, PROC_NAME, "TransferDataToAPD");

                                //get the audit weight
                                iAuditWeight = Convert.ToInt32(objRulesOutputXML.SelectSingleNode("/Root/@auditWeight").InnerText);

                                Array.Resize(ref strParameters, 1);   //Redim TODO:Need to verify..

                                strParameters[0] = lngAPDDocumentID.ToString();
                                strParameters[1] = iAuditWeight.ToString();

                                //update the document's audit weight
                                objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(objMLAPDPartnerDataMgr.GetConfig("Estimate/UpdateAuditWeightSPName"), strParameters);

                                //now log the rules fired
                                objRulesFiredList = objRulesOutputXML.SelectNodes("/Root/RulesFired/Rule");

                                if (objRulesFiredList != null)
                                {
                                    strInsertAuditResultSPName = objMLAPDPartnerDataMgr.GetConfig("Estimate/InsertAuditResultSPName");

                                    foreach (XmlElement objRuleFiredlist in objRulesFiredList)
                                    {
                                        //insert the rule fired into the audit results table
                                        Array.Resize(ref strParameters, 5);    //TODO:Need to verify 

                                        sDetailNumber = "DEFAULT";

                                        if (objRuleFired.GetAttribute("recursive") == "Y")
                                        {
                                            //get the detail number from the key values
                                            sKeyNames = objRuleFired.GetAttribute("keyName");
                                            sKeyValues = objRuleFired.GetAttribute("keyValue");

                                            if (sKeyNames.IndexOf("DetailNumber", 1) > 0)
                                            {
                                                aKeyName = sKeyNames.Split('|');

                                                for (i = aKeyName.GetLowerBound(0); i < aKeyName.GetUpperBound(0); i++)
                                                {
                                                    if ((aKeyName[i].ToLower()) == "detailnumber")
                                                    {
                                                        aKeyValue = sKeyValues.Split('|');
                                                        if (i <= aKeyValue.GetUpperBound(0))
                                                        {
                                                            sDetailNumber = aKeyValue[i];
                                                        }
                                                    }

                                                }
                                            }
                                        }

                                        strParameters[0] = lngAPDDocumentID.ToString();
                                        strParameters[1] = sDetailNumber;
                                        strParameters[2] = string.Concat(new string[] { "'", (Left(objRuleFired.GetAttribute("desc"), 50)).Replace("'", "''"), "'" });
                                        strParameters[3] = string.Concat(new string[] { "'", (objRuleFired.SelectSingleNode("Action").InnerText).Replace("'", "''"), "'" });
                                        strParameters[4] = objRuleFired.GetAttribute("weight");
                                        strParameters[5] = string.Concat(new string[] { "'", strNTUserID, "'" });

                                        objMLAPDPartnerDataMgr.g_objDataAccessor.ExecuteSp(strInsertAuditResultSPName, strParameters);
                                    }
                                }

                                if (objMLAPDPartnerDataMgr.g_blnDebugMode)
                                    objMLAPDPartnerDataMgr.g_objEvents.Trace(string.Empty, string.Concat(new string[] { PROC_NAME, "Rule based audit complete." }));
                            }
                        }
                }
                // *** Estimate Data Save is now complete ***
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objStyleSheetDom = null;
                objAPDEstimateDataDom = null;
                mobjRootNode = null;
                objNodeList = null;
                objNada = null;
                objRulesEngine = null;
                objClaimXML = null;
                objAssignmentVehNode = null;
                objAPDEstimateRules = null;
                objRulesOutputXML = null;
                objRulesFiredList = null;
                objRuleFired = null;
                //Need to roll back a transaction?
                if (blnInTrans)
                    objMLAPDPartnerDataMgr.g_objDataAccessor.RollbackTransaction();
            }
        }


        #endregion
        #region Private Helper Functions

        private string RootChild(string strPath)
        {
            string strFormatpath = string.Empty;
            try
            {
                objMEscapeUtilities = new MEscapeUtilities();
                strFormatpath = mobjRootNode.SelectSingleNode(strPath).InnerText;
                return objMEscapeUtilities.SQLQueryString(ref strFormatpath);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string CurChild(string strPath)
        {
            string strFormatpath = string.Empty;
            XmlNode mobjCurNode = null;
            try
            {
                objMEscapeUtilities = new MEscapeUtilities();
                strFormatpath = mobjCurNode.SelectSingleNode(strPath).InnerText;
                return objMEscapeUtilities.SQLQueryString(ref strFormatpath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// we start at 0 since we want to get the characters starting from the
        ///left and with the specified lenght and assign it to a variable
        /// </summary>
        /// <param name="param"></param>
        /// <param name="length"></param>
        /// <returns>return the result of the operation</returns>
        public string Left(string param, int length)
        {
            string result = param.Substring(0, length);
            return result;
        }

        #endregion

    }
}
