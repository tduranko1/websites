﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Lynx.APD.Component.Library.PartnerDataMgr;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PartnerDataMgr" in code, svc and config file together.
    [ServiceBehavior(Namespace = "http://LynxAPDComponentServices")]
    public class PartnerDataMgr : IPartnerDataMgr
    {
        CExecute objCExecute = null;
        local.pgw.dlynxdataservice.APDService wsAPDFoundation = new local.pgw.dlynxdataservice.APDService();

        public string GetDateTime()
        {
            try
            {
                return DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Procedure : FixUnprocessedPartnerData       
        /// Parameters: strData - delimited string of the format: PartnerTransID,LynxID,VehicleNumber;...
        ///           : strWebRoot - required because
        /// Purpose   : This function parse strData resolving the AssignmentID associated with the each passed
        ///            LynxID/VehicleNumber combination, pulling the partner XML assocaited with PartnerTransID,
        ///            inserting the LynxID, VehicleNumber, and AssignmentID appropriately into the partner XML,
        ///            submit the corrected partnerXML to the APD database, and finally update the Partner record,
        ///            with the appropriate values.
        /// </summary>
        /// <returns></returns>
        public string FixUnprocessedPartnerData(string strData, string strWebRoot, string strSessionKey, string strWindowID, string strUserID)
        {
            objCExecute = new CExecute();
            string strFixUnProcessed = string.Empty;

            try
            {
                strFixUnProcessed = objCExecute.FixUnprocessedPartnerData(strData, strWebRoot, strSessionKey, strWindowID, strUserID);
            }
            catch (Exception ex)
            {
                wsAPDFoundation.LogEvent("COM Conversion", "Error", "PartnerDataMgr.cs - FixUnprocessedPartnerData", ex.Message, "Function Parameters: strData -" + strData + " strWebRoot -" + strWebRoot + "strSessionKey -" + strSessionKey + "strWindowID -" + strWindowID + "strUserID -" + strUserID);
            }

            return strFixUnProcessed;
        }

        /// <summary>
        /// Procedure : FixUnprocessedPartnerData       
        /// Parameters: strData - delimited string of the format: PartnerTransID,LynxID,VehicleNumber;...
        ///           : strWebRoot - required because
        /// Purpose   : This function parse strData resolving the AssignmentID associated with the each passed
        ///            LynxID/VehicleNumber combination, pulling the partner XML assocaited with PartnerTransID,
        ///            inserting the LynxID, VehicleNumber, and AssignmentID appropriately into the partner XML,
        ///            submit the corrected partnerXML to the APD database, and finally update the Partner record,
        ///            with the appropriate values. 
        /// </summary>
        /// <returns></returns>
        public string GetUnprocessedPartnerData(string strWebRoot, string strSessionKey, string strWindowID, string strUserID, string strErrorXML = "")
        {
            objCExecute = new CExecute();
            string strGetUnprocessedPartnerData = string.Empty;
            try
            {
                strGetUnprocessedPartnerData = objCExecute.GetUnprocessedPartnerData(strWebRoot, strSessionKey, strWindowID, strUserID, strErrorXML);
            }
            catch (Exception ex)
            {
                wsAPDFoundation.LogEvent("COM Conversion", "Error", "PartnerDataMgr.cs - FixUnprocessedPartnerData", ex.Message, "Function Parameters: strWebRoot -" + strWebRoot + "strSessionKey -" + strSessionKey + "strWindowID -" + strWindowID + "strUserID -" + strUserID + "strErrorXML" + strErrorXML);
            }
            return strGetUnprocessedPartnerData;
        }

        /// <summary>
        /// Syntax:      object.InitiatePartnerXmlTransaction strXmlDoc
        /// Parameters:  strXmlDoc = Xml document received from calling application
        /// Purpose:     Receive an Xml from an internal application and then kick off
        /// a transaction with an external (partner) application.
        /// Returns:     Blank or any response XML.
        /// </summary>
        /// <returns></returns>
        public string InitiatePartnerXmlTransaction(string strXMLDoc)
        {
            objCExecute = new CExecute();
            string strInitiatePartnerXmlTrans = string.Empty;
            try
            {
               strInitiatePartnerXmlTrans= objCExecute.InitiatePartnerXmlTransaction(strXMLDoc);
            }
            catch (Exception ex)
            {

                wsAPDFoundation.LogEvent("COM Conversion", "Error", "PartnerDataMgr.cs - InitiatePartnerXmlTransaction", ex.Message, "Function Parameters: strXMLDoc -" + strXMLDoc);
            } 
            return strInitiatePartnerXmlTrans;

        }
    }
}
