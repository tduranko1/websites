﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PartnerData" in code, svc and config file together.
    [ServiceBehavior(Namespace = "http://LynxAPDComponentServices")]
    public class PartnerData : IPartnerData
    {

        local.pgw.dlynxdataservice.APDService wsAPDFoundation = new local.pgw.dlynxdataservice.APDService();

        /// <summary>
        /// Receive an Xml from an external application and then act on it
        /// based on the content itself.  Typical usage would be to send the
        /// Xml document to a stored proc for storage.
        /// </summary>
        /// <param name="strXmlDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <returns></returns>
        public long SendXmlDoc(string strXmlDoc, string strTradingPartner)
        {
            long returnXMLDoc = 0;
            PartnerData objPartnerData = new PartnerData();
            try
            {
                returnXMLDoc = objPartnerData.SendXmlDoc(strXmlDoc, strTradingPartner);
                wsAPDFoundation.LogEvent("COM Conversion", "Debugging", "PartnerData.cs - SendXmlDoc", "strTradingPartner" + strTradingPartner, "strXmlDoc" + strXmlDoc);
            }
            catch (Exception ex)
            {
                wsAPDFoundation.LogEvent("COM Conversion", "Error", "PartnerData.cs - SendXmlDoc", ex.Message, "Function Parameters: strXmlDoc -" + strXmlDoc + " strTradingPartner -" + strTradingPartner);
            }

            return returnXMLDoc;
        }


        /// <summary>
        /// Receive an Xml from an external application and then act on it
        /// based on the content itself.  Typical usage would be to send the
        /// Xml document to a stored proc for storage.
        /// </summary>
        /// <param name="strXmlDoc"></param>
        /// <param name="strTradingPartner"></param>
        /// <returns></returns>
        public string SendXmlRequest(string strXmlDoc, string strTradingPartner)
        {
            string strSendXmlRequest = string.Empty;
            PartnerData objPartnerData = new PartnerData();

            try
            {
                strSendXmlRequest = objPartnerData.SendXmlRequest(strXmlDoc, strTradingPartner);
                wsAPDFoundation.LogEvent("COM Conversion", "Debugging", "PartnerData.cs - SendXmlRequest", "strTradingPartner" + strTradingPartner, "strXmlDoc" + strXmlDoc);
            }
            catch (Exception ex)
            {
                wsAPDFoundation.LogEvent("COM Conversion", "Error", "PartnerData.cs - SendXmlRequest", ex.Message, "Function Parameters: strXmlDoc -" + strXmlDoc + " strTradingPartner -" + strTradingPartner);
            }

            return strSendXmlRequest;
        }


        public string GetDateTime()
        {
            try
            {
                return DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
