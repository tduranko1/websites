﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPartnerDataMgr" in both code and config file together.
    [ServiceContract(Namespace = "http://LynxAPDComponentServices")]
    public interface IPartnerDataMgr
    {
        [OperationContract]
        string GetDateTime();

        [OperationContract]
        string FixUnprocessedPartnerData(string strData, string strWebRoot, string strSessionKey, string strWindowID, string strUserID);

        [OperationContract]
        string GetUnprocessedPartnerData(string strWebRoot, string strSessionKey, string strWindowID, string strUserID, string strErrorXML = "");

        [OperationContract]
        string InitiatePartnerXmlTransaction(string strXMLDoc);
    }
}
