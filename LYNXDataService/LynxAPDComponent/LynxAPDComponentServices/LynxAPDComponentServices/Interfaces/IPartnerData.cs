﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace LynxAPDComponentServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPartnerData" in both code and config file together.
    [ServiceContract(Namespace = "http://LynxAPDComponentServices")]
    public interface IPartnerData
    {
        [OperationContract]
        long SendXmlDoc(string strXmlDoc, string strTradingPartner);

        [OperationContract]
        string SendXmlRequest(string strXmlDoc, string strTradingPartner);

        [OperationContract]
        string GetDateTime();
    } 
}
