-- ==========================================================================
-- Purpose : As per andrew's requirement we added new columns to utb_fnol_claim_vehicle_load.
-- Updated by : utb_fnol_claim_vehicle_load table updated by glsd452.
-- ==========================================================================

	Alter TABLE [dbo].[utb_fnol_claim_vehicle_load] Add
		[PrefMethodUpd] [udt_status_prefmethodupd] NULL,
		[CellPhoneCarrier] [udt_ph_cellphonecarrier] NULL,
		[ContactCellPhone] varchar(15) NULL,
		[ContactEmailAddress] [udt_web_email] NULL
	GO

-- ==========================================================================
-- Purpose : As per andrew's requirement we added new columns to utb_involved.
-- Updated by : utb_involved table updated by glsd451.
-- ==========================================================================

	Alter TABLE [dbo].[utb_involved] Add
		[PrefMethodUpd] [udt_status_prefmethodupd] Null,
		[CellPhoneCarrier] [udt_ph_cellphonecarrier] Null,
		[CellAreaCode] [udt_ph_area_code] NULL,
		[CellExchangeNumber] [udt_ph_exchange_number] NULL,
		[CellUnitNumber] [udt_ph_unit_number] NULL	
	GO
