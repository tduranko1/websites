-- Begin the transaction for dropping and creating the stored procedure 

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsuranceFeeUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmInsuranceFeeUpdDetail 
END
GO
/****** Object:  StoredProcedure [dbo].[uspAdmInsuranceFeeUpdDetail]    Script Date: 04/17/2014 09:13:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/************************************************************************************************************************
*
* PROCEDURE:    uspAdmInsuranceFeeUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Ramesh Vishegu]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @ClientFeeID          Client Fee ID
* (I) @EnabledFlag          EnabledFlag
* (I) @ClaimAspectTypeID    Claim Aspect Type ID
* (I) @InsuranceCompanyID   Insurance Company ID
* (I) @CategoryCD           Category Code
* (I) @Description          Description for the fee item that will appear on the billing screens
* (I) @DisplayOrder         Display Order
* (I) @FeeAmount            Fee Amount
* (I) @FeeInstructions      Fee Instructions
* (I) @ItemizeFlag          Can the fee item itemized in the invoice
* (I) @HideFromInvoiceFlag  Hide this fee from the invoice
* (I) @InvoiceDescription   The description that will appear on the invoice
* (I) @EffectiveStartDate   The fee effective from date
* (I) @EffectiveEndDate     The fee effective end date
* (I) @AppliesWhenCD        The Fee applies when code
* (I) @SysLastUserID        User that last updated the record
* (I) @SysLastUpdatedDate   The last updated date
* (I) @LowerLimit			Lower Limit Fee Range
* (I) @UpperLimit			Upper Limit Fee Range
* (I) @FeeRangeFlag			Apply Fee Range
* (I) @IncludedServices     A series of string comprising of ServiceID, ItemizeFlag, RequiredFlag and separated by ;
* (I) @AdminFees            A series of string comprising of AdminFeeCD, Amount, MSA and State separated by ;
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspAdmInsuranceFeeUpdDetail] 
(
    @ClientFeeID          udt_std_int,
    @EnabledFlag          udt_enabled_flag,
    @ClaimAspectTypeID    udt_std_int_tiny,
    @InsuranceCompanyID   udt_std_int_small,
    @CategoryCD           udt_std_cd,
    @Description          udt_std_name,
    @DisplayOrder         udt_std_int,
    @FeeAmount            varchar(50),
    @FeeInstructions      udt_std_desc_mid,
    @ItemizeFlag          udt_std_flag,
    @HideFromInvoiceFlag  udt_std_flag,
    @InvoiceDescription   udt_std_name,
    @EffectiveStartDate   varchar(10) = NULL,
    @EffectiveEndDate     varchar(10) = NULL,
    @AppliesWhenCD        varchar(1),
    @IncludedServices     varchar(1000),
    @AdminFees            varchar(1000),
    @SysLastUserID        udt_std_id,
    @SysLastUpdatedDate   VARCHAR(30),
    @ApplicationCD        udt_std_cd = 'APD',
    @LowerLimit           varchar(50),			--Audit Fee Range Changes
    @UpperLimit           varchar(50),			--Audit Fee Range Changes
    @FeeRangeFlag		  udt_std_flag			--Audit Fee Range Changes
)
AS
BEGIN
    
    SET NOCOUNT ON

    -- Declare internal variables

    --DECLARE @EnabledFlag              AS Bit
    DECLARE @error                    AS INT
    DECLARE @rowcount                 AS INT

    DECLARE @ProcName                 AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                      AS datetime

    DECLARE @Debug                    AS bit
    
    DECLARE @dbClaimAspectTypeID      AS udt_std_int_tiny     
    DECLARE @dbInsuranceCompanyID     AS udt_std_int_small
    DECLARE @dbCategoryCD             AS udt_std_cd
    DECLARE @dbDescription            AS udt_std_name
    DECLARE @dbFeeAmount              AS udt_std_money    
    DECLARE @dbFeeInstructions        AS udt_std_desc_mid       
    DECLARE @dbItemizeFlag            AS udt_std_flag
    DECLARE @dbInvoiceDescription     AS udt_std_name
    DECLARE @dbAppliesWhenCD          AS udt_std_cd     
    DECLARE @dbEffectiveStartDate     AS udt_std_datetime
    DECLARE @dbEffectiveEndDate       AS udt_std_datetime
    DECLARE @dbLowerLimit             AS udt_std_money			--Audit Fee Range Changes
    DECLARE @dbUpperLimit             AS udt_std_money			--Audit Fee Range Changes
    DECLARE @dbFeeRangeFlag           AS udt_std_flag			--Audit Fee Range Changes
    
    DECLARE @OldValue                 AS varchar(20)
    DECLARE @NewValue                 AS varchar(20)
    DECLARE @IncludedService          AS varchar(50)
    DECLARE @ServiceID                AS int
    DECLARE @ServiceItemizeFlag       AS bit
    DECLARE @ServiceRequiredFlag      AS bit    
    DECLARE @LogComment               AS udt_std_desc_long

    DECLARE @AdminFeeID               AS bigint
    DECLARE @AdminFee                 AS varchar(50)
    DECLARE @AdminFeeCD               AS varchar(1)    
    DECLARE @AdminFeeAmount           AS decimal(9, 2)
    DECLARE @AdminFeeMSA              AS varchar(5)
    DECLARE @AdminFeeState            AS varchar(5)    

    DECLARE @OldAdminFeeCD            AS varchar(1)    
    DECLARE @OldAdminFeeAmount        AS decimal(9, 2)
    DECLARE @NewAdminFeeCD            AS varchar(1)    
    DECLARE @NewAdminFeeAmount        AS decimal(9, 2)
    
    SET @Debug = 0

    SET @ProcName = 'uspAdmInsuranceFeeUpdDetail'
    SET @now = CURRENT_TIMESTAMP
    
    IF @Debug = 1
    BEGIN
        Print 'Input Parameters:'
        Print ''
        Print '@ClientFeeID= ' + convert(varchar, @ClientFeeID)
        Print '@ClaimAspectTypeID= ' + convert(varchar, @ClaimAspectTypeID)
        Print '@InsuranceCompanyID= ' + convert(varchar, @InsuranceCompanyID)
        Print '@CategoryCD= ' + convert(varchar, @CategoryCD)
        Print '@Description= ' + convert(varchar, @Description)
        Print '@DisplayOrder= ' + convert(varchar, @DisplayOrder)
        Print '@FeeAmount= ' + convert(varchar, @FeeAmount)
        Print '@FeeInstructions= ' + convert(varchar, @FeeInstructions)
        Print '@ItemizeFlag= ' + convert(varchar, @ItemizeFlag)
        Print '@HideFromInvoiceFlag= ' + convert(varchar, @HideFromInvoiceFlag)
        Print '@InvoiceDescription= ' + convert(varchar, @InvoiceDescription)
        Print '@IncludedServices= ' + convert(varchar, @IncludedServices)
        Print '@EffectiveStartDate= ' + convert(varchar, @EffectiveStartDate)
        Print '@EffectiveEndDate= ' + convert(varchar, @EffectiveEndDate)
        Print '@AppliesWhenCD= ' + convert(varchar, @AppliesWhenCD)
        Print '@SysLastUserID= ' + convert(varchar, @SysLastUserID)
        Print '@SysLastUpdatedDate= ' + convert(varchar, @SysLastUpdatedDate)
        Print '@LowerLimit= ' + convert(varchar, @LowerLimit)
        Print '@UpperLimit= ' + convert(varchar, @UpperLimit)
        Print '@FeeRangeFlag= ' + convert(varchar, @FeeRangeFlag)
    END
    
    -- Validate the input parameters
    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT u.UserID 
                       FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                           INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
                       WHERE a.Code = @ApplicationCD
                         AND u.UserID = @SysLastUserID
                         AND @SysLastUserID <> 0
                         AND ua.AccessBeginDate IS NOT NULL
                         AND ua.AccessBeginDate <= @now
                         AND ua.AccessEndDate IS NULL)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
        RETURN
    END

    IF @ClientFeeID IS NULL 
        OR 
        NOT EXISTS (SELECT ClientFeeID
                        FROM utb_client_fee
                        WHERE ClientFeeID = @ClientFeeID)
    BEGIN
        RAISERROR  ('1|Invalid ClientFeeID', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeID IS NULL 
        OR 
        NOT EXISTS (SELECT ClaimAspectTypeID
                        FROM utb_claim_aspect_type
                        WHERE ClaimAspectTypeID = @ClaimAspectTypeID)
    BEGIN
        RAISERROR  ('1|Invalid ClaimAspectTypeID', 16, 1, @ProcName)
        RETURN
    END

    IF @InsuranceCompanyID IS NULL 
        OR 
        NOT EXISTS (SELECT InsuranceCompanyID
                        FROM utb_insurance
                        WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        RAISERROR  ('1|Invalid InsuranceCompanyID', 16, 1, @ProcName)
        RETURN
    END

    IF @CategoryCD IS NULL OR @CategoryCD not in ('A', 'H')
        
    BEGIN
        RAISERROR  ('1|Invalid CategoryCD', 16, 1, @ProcName)
        RETURN
    END

    IF @Description IS NULL
        
    BEGIN
        RAISERROR  ('1|Description cannot be empty', 16, 1, @ProcName)
        RETURN
    END

    IF @FeeAmount IS NULL
        
    BEGIN
        RAISERROR  ('1|Fee Amount cannot be empty', 16, 1, @ProcName)
        RETURN
    END

    IF @ItemizeFlag IS NULL
    BEGIN
        RAISERROR  ('1|ItemizeFlag cannot be null', 16, 1, @ProcName)
        RETURN
    END
    ELSE
    BEGIN
        IF @InvoiceDescription IS NULL OR LEN(LTRIM(RTRIM(@InvoiceDescription))) = 0
        BEGIN
            SET @InvoiceDescription = @Description
        END
        /*IF @ItemizeFlag = 1 AND @InvoiceDescription IS NULL
        BEGIN
            RAISERROR  ('1|Invoice Description cannot be empty', 16, 1, @ProcName)
            RETURN
        END
        ELSE
        BEGIN
        END*/
    END

    IF @IncludedServices IS NULL OR LEN(RTrim(LTrim(@IncludedServices))) = 0
        
    BEGIN
        RAISERROR  ('1|Fee must include atleast one service.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @EffectiveStartDate IS NOT NULL
    BEGIN
        IF ISDATE(@EffectiveStartDate) = 0
        BEGIN
            RAISERROR  ('1|Invalid Fee Effective From Date.', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF @EffectiveEndDate IS NOT NULL
    BEGIN
        IF ISDATE(@EffectiveEndDate) = 0
        BEGIN
            RAISERROR  ('1|Invalid Fee Effective To Date.', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF @EffectiveStartDate IS NOT NULL AND @EffectiveEndDate IS NOT NULL
    BEGIN
        IF convert(datetime, @EffectiveEndDate) < convert(datetime, @EffectiveStartDate)
        BEGIN
            RAISERROR  ('1|The Fee Effective To date cannot be before the From date.', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF @AppliesWhenCD IS NULL OR @AppliesWhenCD NOT IN ('B', 'C')
    BEGIN
        RAISERROR  ('1|Invalid Applies When value.', 16, 1, @ProcName)
        RETURN
    END
    
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_client_fee', @ClientFeeID
    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END

    -- Done with validations, do the save operation.
    
    -- Extract the Included services
    DECLARE @tmpIncludedServices TABLE
    (
        ServiceInfo     varchar(50)
    )
    
    DECLARE @tmpClientFeeDefinition TABLE
    (
        ServiceID        int     not NULL,
        ItemizeFlag  bit     not NULL,
        RequiredFlag     bit     not NULL
    )
    
    INSERT INTO @tmpIncludedServices
    SELECT value from ufnUtilityParseString(@IncludedServices, ';', 1) -- @IncludedServices format: ServiceID,ItemizeFlag,RequiredFlag[;ServiceID,ItemizeFlag,RequiredFlag[...]]
    
    IF @Debug = 1
        SELECT * FROM  @tmpIncludedServices
    
    DECLARE csrIncludedServices CURSOR FOR
        SELECT ServiceInfo FROM @tmpIncludedServices
        
    

    OPEN csrIncludedServices

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrIncludedServices
    INTO @IncludedService

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        SET @ServiceID = NULL
        SET @ServiceItemizeFlag = NULL
        SET @ServiceRequiredFlag = NULL

        DECLARE csrIncludedService CURSOR FOR
            SELECT VALUE FROM ufnUtilityParseString(@IncludedService, ',', 1)
        
        OPEN csrIncludedService

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            CLOSE csrIncludedServices
            DEALLOCATE csrIncludedServices
            RETURN
        END    

        FETCH next
        FROM csrIncludedService
        INTO @ServiceID
        
        FETCH next
        FROM csrIncludedService
        INTO @ServiceRequiredFlag

        FETCH next
        FROM csrIncludedService
        INTO @ServiceItemizeFlag

        
        CLOSE csrIncludedService
        DEALLOCATE csrIncludedService
        
        IF NOT EXISTS(SELECT ServiceID from utb_service where ServiceID = @ServiceID)
        BEGIN
            RAISERROR  ('1|Invalid Service ID specified.', 16, 1, @ProcName)
            CLOSE csrIncludedServices
            DEALLOCATE csrIncludedServices
            RETURN
        END
        
        IF @ServiceItemizeFlag IS NULL
            SET @ServiceItemizeFlag = 0

        IF @ServiceRequiredFlag IS NULL
            SET @ServiceRequiredFlag = 0
            
        INSERT INTO @tmpClientFeeDefinition VALUES (@ServiceID, @ServiceItemizeFlag, @ServiceRequiredFlag)

        FETCH next
        FROM csrIncludedServices
        INTO @IncludedService

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    

    END

    CLOSE csrIncludedServices
    DEALLOCATE csrIncludedServices
    
    IF @Debug = 1
        SELECT * FROM @tmpClientFeeDefinition


    -- Extract the Included services
    DECLARE @tmpAdminFees TABLE
    (
        AdminFeeInfo     varchar(100)
    )
    
    DECLARE @tmpAdminFeeDefinition TABLE
    (
        PaidByCD    varchar(1)      not NULL,
        AdminAmount decimal(9,2)    not NULL,
        AdminMSA    varchar(5)      NULL,
        AdminState  varchar(5)      NULL
    )
    
    INSERT INTO @tmpAdminFees
    SELECT value from ufnUtilityParseString(@AdminFees, ';', 1) -- @IncludedServices format: ServiceID,ItemizeFlag,RequiredFlag[;ServiceID,ItemizeFlag,RequiredFlag[...]]
    
    IF @Debug = 1
        SELECT * FROM  @tmpAdminFees
    
    DECLARE csrAdminFees CURSOR FOR
        SELECT AdminFeeInfo FROM @tmpAdminFees
        
    

    OPEN csrAdminFees

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrAdminFees
    INTO @AdminFee

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        SET @AdminFeeCD = NULL
        SET @AdminFeeAmount = NULL
        SET @AdminFeeMSA = NULL
        SET @AdminFeeState = NULL

        DECLARE csrAdminFeeInfo CURSOR FOR
            SELECT VALUE FROM ufnUtilityParseString(@AdminFee, ',', 1)
        
        OPEN csrAdminFeeInfo

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            CLOSE csrIncludedServices
            DEALLOCATE csrIncludedServices
            RETURN
        END    

        FETCH next
        FROM csrAdminFeeInfo
        INTO @AdminFeeCD
        
        FETCH next
        FROM csrAdminFeeInfo
        INTO @AdminFeeAmount

        FETCH next
        FROM csrAdminFeeInfo
        INTO @AdminFeeMSA

        FETCH next
        FROM csrAdminFeeInfo
        INTO @AdminFeeState
        
        CLOSE csrAdminFeeInfo
        DEALLOCATE csrAdminFeeInfo
        
        INSERT INTO @tmpAdminFeeDefinition
        VALUES 
        (@AdminFeeCD, @AdminFeeAmount, @AdminFeeMSA, @AdminFeeState)

        FETCH next
        FROM csrAdminFees
        INTO @AdminFee

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    

    END

    CLOSE csrAdminFees
    DEALLOCATE csrAdminFees
    
    IF @Debug = 1
        SELECT * FROM @tmpAdminFeeDefinition
    
    -- Get the active admin fee id
    SELECT @AdminFeeID = AdminFeeID
    FROM dbo.utb_admin_fee
    WHERE ClientFeeID = @ClientFeeID

    --SET @EnabledFlag = 1
    
    
    -- Get db values for comparison with passed in parameters.  These comparisons will be used to determine
    -- fee audit logging
    SELECT  @dbClaimAspectTypeID   = ClaimAspectTypeID, 
            @dbInsuranceCompanyID  = InsuranceCompanyID,   
            @dbCategoryCD          = CategoryCD,           
            @dbDescription         = Description,          
            @dbFeeAmount           = FeeAmount,            
            @dbFeeInstructions     = FeeInstructions,      
            @dbItemizeFlag         = ItemizeFlag,      
            @dbInvoiceDescription  = InvoiceDescription,   
            @dbAppliesWhenCD       = AppliesToCD,        
            @dbEffectiveStartDate  = EffectiveStartDate,   
            @dbEffectiveEndDate    = EffectiveEndDate, 
            @dbLowerLimit		   = LowerLimit,					--Audit Fee Range Changes
            @dbUpperLimit		   = UpperLimit,					--Audit Fee Range Changes
            @dbFeeRangeFlag		   = ApplyFeeRange					--Audit Fee Range Changes
    FROM dbo.utb_client_fee   
    WHERE ClientFeeID = @ClientFeeID
                
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END   
    
        

    BEGIN TRANSACTION InsuranceFeeUpdTran
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END  
    
    -- Check parameters against existing db values for changes that need to be recorded in the audit log.  -----------------
    ------------------------------------------------------------------------------------------------------------------------
    
    
    -- EnabledFlag
    IF @EnabledFlag = 0
    BEGIN
      SET @LogComment = 'Fee was deleted'
    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    -- Fee Amount
    IF (@dbFeeAmount <> convert(money, @FeeAmount)) OR (@EnabledFlag = 0)
    BEGIN
      SET @LogComment = (SELECT CASE @EnabledFlag
                           WHEN 1 THEN
                             'Amount changed from ' + 
                             convert(varchar(50), @dbFeeAmount) + ' to ' + @FeeAmount
                           ELSE
                             'Amount at deletion - ' + convert(varchar(50), @dbFeeAmount)
                         END)
                        
    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF (@EnabledFlag = 1)
    BEGIN
      -- Effective From Date changed
      IF ((@dbEffectiveStartDate <> @EffectiveStartDate))
      BEGIN
        SET @LogComment = 'Effective From Date changed from ' + 
                          convert(varchar(30), @dbEffectiveStartDate, 101) + ' to ' +
                          convert(varchar(30), @EffectiveStartDate) 
    
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END  
      END
    
    
      -- Effective From Date added
      IF (@dbEffectiveStartDate IS NULL) AND (@EffectiveStartDate IS NOT NULL)
      BEGIN
        SET @LogComment = 'Effective From Date established - ' + 
                          convert(varchar(30), @EffectiveStartDate) 
      
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END  
      END
    
      -- Effective From Date removed
      IF (@dbEffectiveStartDate IS NOT NULL) AND (@EffectiveStartDate IS NULL)
      BEGIN
        SET @LogComment = 'Effective From Date - ' + 
                          convert(varchar(30), @dbEffectiveStartDate, 101) + ' - was removed.'
      
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END  
      END
    
    
      -- Effective To Date changed
      IF @dbEffectiveEndDate <> @EffectiveEndDate
      BEGIN
        SET @LogComment = 'Effective To Date changed from ' + 
                          convert(varchar(30), @dbEffectiveEndDate, 101) + ' to ' +
                          convert(varchar(30), @EffectiveEndDate) 
      
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END  
      END
    
      -- Effective To Date added
      IF (@dbEffectiveEndDate IS NULL) AND (@EffectiveEndDate IS NOT NULL)
      BEGIN
        SET @LogComment = 'Effective To Date established - ' + 
                          convert(varchar(30), @EffectiveEndDate) 
      
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END  
      END
    
      -- Effective To Date removed
      IF (@dbEffectiveEndDate IS NOT NULL) AND (@EffectiveEndDate IS NULL)
      BEGIN
        SET @LogComment = 'Effective To Date  - ' + 
                          convert(varchar(30), @dbEffectiveEndDate, 101) + ' - was removed.'
      
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END  
      END
    END
    ELSE
    BEGIN
         
      SET @LogComment = 'Effective From Date at deletion - ' + 
                        ISNULL(convert(varchar(30), @dbEffectiveStartDate, 101), 'Not Established')
      
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
             
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    
      SET @LogComment = 'Effective To Date at deletion - ' + 
                        ISNULL(convert(varchar(30), @dbEffectiveEndDate, 101), 'Not Established')
      
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
             
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    -- Check Claim Aspect
    IF (@dbClaimAspectTypeID <> @ClaimAspectTypeID) OR (@EnabledFlag = 0)
    BEGIN
      SET @LogComment = (SELECT CASE @EnabledFlag
                           WHEN 1 THEN 
                             'Aspect Type changed from ' + 
                             (SELECT Name FROM dbo.utb_claim_aspect_type WHERE ClaimAspectTypeID = @dbClaimAspectTypeID) + ' to ' +
                             (SELECT Name FROM dbo.utb_claim_aspect_type WHERE ClaimAspectTypeID = @ClaimAspectTypeID)
                           ELSE 
                             'Aspect Type at deletion - ' + 
                             (SELECT Name FROM dbo.utb_claim_aspect_type WHERE ClaimAspectTypeID = @dbClaimAspectTypeID)
                         END)
    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
                      
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    -- Category
    IF (@dbCategoryCD <> @CategoryCD) OR (@EnabledFlag = 0)
    BEGIN
      SET @LogComment = (SELECT CASE @EnabledFlag
                           WHEN 1 THEN
                             'Category changed from ' + 
                             (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'CategoryCD')
                                          WHERE Code = @dbCategoryCD) + ' to ' + 
                             (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'CategoryCD')
                                          WHERE Code = @CategoryCD)
                           ELSE
                             'Category at deletion - ' + 
                             (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'CategoryCD')
                                          WHERE Code = @dbCategoryCD)             
                         END)
                                     
    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    -- Description
    IF (UPPER(LTRIM(RTRIM(@dbDescription))) <> UPPER(LTRIM(RTRIM(@Description)))) OR (@EnabledFlag = 0)
    BEGIN
      SET @LogComment = (SELECT CASE @EnabledFlag
                           WHEN 1 THEN
                             'Description changed to - ' + @Description
                           ELSE
                             'Description at deletion - ' + ISNULL(@dbDescription, 'NONE')
                         END)
      
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
       
    
    -- Fee Instructions
    IF (UPPER(LTRIM(RTRIM(@dbFeeInstructions))) <> UPPER(LTRIM(RTRIM(@FeeInstructions)))) OR (@EnabledFlag = 0)
    BEGIN
      SET @LogComment = (SELECT CASE @EnabledFlag
                           WHEN 1 THEN
                             'Instructions changed from ' + ISNULL(@dbFeeInstructions, 'NONE') + ' to ' + @FeeInstructions
                           ELSE
                             'Instructions at deletion - ' + ISNULL(@dbFeeInstructions, 'NONE')
                         END)
                          
    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    -- ItemizeFlag
    IF (@dbItemizeFlag <> @ItemizeFlag) OR (@EnabledFlag = 0)
    BEGIN
      
      IF @dbItemizeFlag = 1
      BEGIN
        SET @OldValue = 'Itemize'
        SET @NewValue = 'Non-Itemize'
      END
      ELSE
      BEGIN
        SET @OldValue = 'Non-Itemize'
        SET @NewValue = 'Itemize'
      END
    
      SET @LogComment = (SELECT CASE @EnabledFlag
                           WHEN 1 THEN
                             'Changed from - ' + @OldValue + ' to ' + @NewValue
                           ELSE
                             'Fee at deletion was - ' + @OldValue
                         END)                         
                                  
    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    -- Invoice Description
    IF (UPPER(LTRIM(RTRIM(@dbInvoiceDescription))) <> UPPER(LTRIM(RTRIM(@InvoiceDescription)))) OR (@EnabledFlag = 0)
    BEGIN
      SET @LogComment = (SELECT CASE @EnabledFlag
                           WHEN 1 THEN
                             'Invoice Description changed from ' + 
                             ISNULL(@dbInvoiceDescription, 'NONE') + ' to ' + @InvoiceDescription   
                           ELSE
                             'Invoice Description at deletion - ' + ISNULL(@dbInvoiceDescription, 'NONE')
                         END)
    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    -- Applies To
    IF (@dbAppliesWhenCD <> @AppliesWhenCD) OR (@EnabledFlag = 0)
    BEGIN
      SET @LogComment = (SELECT CASE @EnabledFlag
                           WHEN 1 THEN
                             'Effective Date compared to changed from ' + 
                             (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'AppliesToCD') 
                                          WHERE Code = @dbAppliesWhenCD) + ' Creation to ' + 
                             (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'AppliesToCD') 
                                          WHERE Code = @AppliesWhenCD) + ' Creation'
                           ELSE
                             'Effective date was compared to ' + 
                             (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'AppliesToCD') 
                                          WHERE Code = @dbAppliesWhenCD) + ' Creation at deletion'
                         END)
    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    -- Services  -----------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------------
        
    -- Check for Services that were added to this fee
    DECLARE csrServiceAdd CURSOR FOR SELECT ServiceID
                                  FROM @tmpClientFeeDefinition
                                  WHERE ServiceID NOT IN (SELECT ServiceID 
                                                          FROM dbo.utb_client_fee_definition 
                                                          WHERE ClientFeeID = @ClientFeeID)
                                          
    OPEN csrServiceAdd

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    

    FETCH NEXT FROM csrServiceAdd INTO @ServiceID

    WHILE @@FETCH_STATUS = 0
    BEGIN

      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, @ProcName)
          ROLLBACK TRANSACTION
          RETURN
      END
     
      SET @LogComment = 'Service - ' + (select name from dbo.utb_service where ServiceID = @ServiceID)  + ' - was added'
    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID

      FETCH NEXT FROM csrServiceAdd INTO @ServiceID
    END   

    CLOSE csrServiceAdd
    DEALLOCATE csrServiceAdd


    -- Check for Services that were removed from this fee
    DECLARE csrServiceRem CURSOR FOR SELECT ServiceID
                                     FROM dbo.utb_client_fee_definition
                                     WHERE ClientFeeID = @ClientFeeID
                                       AND ServiceID NOT IN (SELECT ServiceID FROM @tmpClientFeeDefinition) 
                                                    
                                          
    OPEN csrServiceRem

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    

    FETCH NEXT FROM csrServiceRem INTO @ServiceID

    WHILE @@FETCH_STATUS = 0
    BEGIN

      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, @ProcName)
          ROLLBACK TRANSACTION
          RETURN
      END
     
      SET @LogComment = 'Service - ' + (select name from dbo.utb_service where ServiceID = @ServiceID)  + ' - was removed'

      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID

      FETCH NEXT FROM csrServiceRem INTO @ServiceID
    END   

    CLOSE csrServiceRem
    DEALLOCATE csrServiceRem

        
    -- Build a cursor with all services currently attached to this fee.
    DECLARE csrServiceAlt CURSOR FOR SELECT ServiceID, ItemizeFlag, RequiredFlag
                                     FROM dbo.utb_client_fee_definition
                                     WHERE ClientFeeID = @ClientFeeID
                                       AND ServiceID IN (SELECT ServiceID FROM @tmpClientFeeDefinition) 
                                                    
                                          
    OPEN csrServiceAlt

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    

    FETCH NEXT FROM csrServiceAlt INTO @ServiceID, @ServiceItemizeFlag, @ServiceRequiredFlag

    WHILE @@FETCH_STATUS = 0
    BEGIN

      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, @ProcName)
          ROLLBACK TRANSACTION
          RETURN
      END
  
  
      IF (@EnabledFlag = 1)
      BEGIN
        -- Check for Services that were altered for this fee
        
        -- Determine if itemize flag has changed and log whether it was turned on or off.
        IF @ServiceItemizeFlag <> (SELECT ItemizeFlag FROM @tmpClientFeeDefinition WHERE ServiceID = @ServiceID)
        BEGIN
          SET @LogComment = 'Service - ' + 
                            (SELECT Name FROM dbo.utb_service WHERE ServiceID = @ServiceID) + ' - ''Itemize'' was turned ' + 
                            (SELECT CASE @ServiceItemizeFlag WHEN 0 THEN 'ON' ELSE 'OFF' END)
    
          EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
                 
          IF @@ERROR <> 0
          BEGIN
             -- SQL Server Error

              RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
              ROLLBACK TRANSACTION
              RETURN
          END  
        END


        -- Determine if required flag has changed and log whether it was turned on or off.
        IF @ServiceRequiredFlag <> (SELECT TOP 1 RequiredFlag FROM @tmpClientFeeDefinition WHERE ServiceID = @ServiceID)
        BEGIN
          SET @LogComment = 'Service - ' + 
                            (select name from dbo.utb_service where ServiceID = @ServiceID) + ' - ''Required'' was turned ' + 
                            (SELECT CASE @ServiceRequiredFlag WHEN 0 THEN 'ON' ELSE 'OFF' END)
    
          EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
                 
          IF @@ERROR <> 0
          BEGIN
             -- SQL Server Error

              RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
              ROLLBACK TRANSACTION
              RETURN
          END  
        END
      END
      ELSE
      BEGIN
        -- This fee is being deleted.  Log all services that are currently attached and their states.
        SET @LogComment = 'Service included at Fee deletion - ' + 
                          (SELECT Name FROM dbo.utb_service where ServiceID = @ServiceID) + 
                          ' - ''Itemize'': ' + (SELECT CASE @ServiceItemizeFlag WHEN 1 THEN 'ON' ELSE 'OFF' END) + 
                          ' -- ''Required'': ' + (SELECT CASE @ServiceRequiredFlag WHEN 1 THEN 'ON' ELSE 'OFF' END)
                          
                          
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
               
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END  
      END  
      
      FETCH NEXT FROM csrServiceAlt INTO  @ServiceID, @ServiceItemizeFlag, @ServiceRequiredFlag
    END 
    
    CLOSE csrServiceAlt
    
    DEALLOCATE csrServiceAlt
    
    -- Admin Fees log  -----------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------------
    -- Check for flat admin fee
    SELECT @NewAdminFeeCD = PaidByCD,
           @NewAdminFeeAmount = AdminAmount
    FROM @tmpAdminFeeDefinition
    WHERE AdminMSA = 'Flat'
      AND AdminState = 'Flat'

    SELECT @OldAdminFeeCD = AdminFeeCD,
           @OldAdminFeeAmount = Amount
    FROM utb_admin_fee
    WHERE ClientFeeID = @ClientFeeID
      AND EnabledFlag = 1
      AND Amount IS NOT NULL

    IF @OldAdminFeeCD IS NULL AND @NewAdminFeeCD IS NOT NULL
    BEGIN
        -- Flat admin fee is added
        SET @LogComment = 'Flat Admin Fee was added. Paid by ' + CASE
                                                                    WHEN @NewAdminFeeCd = 'S' THEN 'Shop'
                                                                    WHEN @NewAdminFeeCd = 'I' THEN 'Insurance Company'
                                                                    ELSE 'Unknown'
                                                                 END + '; Amount: $' +
                                                                 CONVERT(varchar, @NewAdminFeeAmount)
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
    END

    IF @OldAdminFeeCD IS NOT NULL AND @NewAdminFeeCD IS NULL
    BEGIN
        -- Flat admin fee is deleted
        SET @LogComment = 'Flat Admin Fee was deleted. Old values: Paid by ' + CASE
                                                                    WHEN @OldAdminFeeCD = 'S' THEN 'Shop'
                                                                    WHEN @OldAdminFeeCD = 'I' THEN 'Insurance Company'
                                                                    ELSE 'Unknown'
                                                                 END + '; Amount $' +
                                                                 isNull(CONVERT(varchar, @OldAdminFeeAmount), '')
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
    END

    IF @OldAdminFeeCD IS NOT NULL AND @NewAdminFeeCD IS NOT NULL
    BEGIN
        -- Check if the fee paid by has changed
        IF @NewAdminFeeCD <> @OldAdminFeeCD
        BEGIN
            -- Admin Fee CD was updated
            SET @LogComment = 'Flat Admin Fee paid by was changed from '  
                                     + CASE
                                        WHEN @OldAdminFeeCD = 'S' THEN 'Shop'
                                        WHEN @OldAdminFeeCD = 'I' THEN 'Insurance Company'
                                        ELSE 'Unknown'
                                     END + ' to '
                                     + CASE
                                        WHEN @NewAdminFeeCD = 'S' THEN 'Shop'
                                        WHEN @NewAdminFeeCD = 'I' THEN 'Insurance Company'
                                        ELSE 'Unknown'
                                     END
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END

        -- Check if the fee amount has changed
        IF @NewAdminFeeAmount <> @OldAdminFeeAmount
        BEGIN
            -- Admin Fee Amount was updated
            SET @LogComment = 'Flat Admin Fee amount was changed from ' + convert(varchar, @OldAdminFeeAmount) + ' to ' + convert(varchar, @NewAdminFeeAmount)
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END
    END

    -- Check for Admin fee msa changes
    -- Admin fee msa additions
    DECLARE @tmpComments as varchar(1000)
    SET @tmpComments = ''

    IF @AdminFeeID IS NOT NULL
    BEGIN
        SELECT @tmpComments = @tmpComments + 'Admin fee for MSA: ' + t.AdminMSA + ' was added. ' + 
                                    'Paid by ' + 
                                    CASE
                                       WHEN t.PaidByCD = 'S' THEN 'Shop'
                                       WHEN t.PaidByCD = 'I' THEN 'Insurance Company'
                                       ELSE 'Unknown'
                                    END + '; Amount: $' +
                                    CONVERT(varchar, t.AdminAmount)
        FROM @tmpAdminFeeDefinition t
        WHERE AdminMSA not in (SELECT MSA
                                FROM utb_admin_fee_msa
                                WHERE AdminFeeID = @AdminFeeID
                                  AND EnabledFlag = 1)
          AND AdminMSA IS NOT NULL
          AND AdminMSA <> 'Flat'
          AND AdminMSA <> ''

        IF @Debug = 1
        BEGIN
            print 'Admin fee msa additions...' + @tmpComments
        END
        
        IF LEN(@tmpComments) > 0
        BEGIN
            SET @LogComment = @tmpComments
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END
    END 

    -- Admin fee msa deletions
    SET @tmpComments = ''

    IF @AdminFeeID IS NOT NULL
    BEGIN
        SELECT @tmpComments = @tmpComments + 'Admin fee for MSA: ' + MSA + ' was deleted. ' + 
                                    'Old values: Paid by ' + 
                                    CASE
                                       WHEN AdminFeeCD = 'S' THEN 'Shop'
                                       WHEN AdminFeeCD = 'I' THEN 'Insurance Company'
                                       ELSE 'Unknown'
                                    END + '; Amount: $' +
                                    CONVERT(varchar, Amount)
        FROM utb_admin_fee_msa
        WHERE AdminFeeID = @AdminFeeID
          AND MSA not in (SELECT AdminMSA
                            FROM @tmpAdminFeeDefinition
                            WHERE AdminMSA IS NOT NULL
                              AND AdminMSA <> 'Flat'
                              AND AdminMSA <> '')
        
        IF @Debug = 1
        BEGIN
            print 'Admin fee msa deletion...' + @tmpComments
        END
        
        IF LEN(@tmpComments) > 0
        BEGIN
            SET @LogComment = @tmpComments
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END
    END 

    -- Admin fee msa updates
    SET @tmpComments = ''

    IF @AdminFeeID IS NOT NULL
    BEGIN
        SELECT @tmpComments = @tmpComments + 'Admin fee for MSA: ' + t.AdminMSA + ' was updated.' +
                            CASE
                                WHEN afm.Amount <> t.AdminAmount THEN ' Amount updated from ' + convert(varchar, afm.Amount) + ' to ' + convert(varchar, t.AdminAmount) + '.'
                                ELSE ''
                            END +
                            CASE
                                WHEN afm.AdminFeeCD <> t.PaidByCD THEN ' Paid by updated from ' + 
                                    CASE
                                       WHEN afm.AdminFeeCD = 'S' THEN 'Shop'
                                       WHEN afm.AdminFeeCD = 'I' THEN 'Insurance Company'
                                       ELSE 'Unknown'
                                    END +
                                    ' to ' + 
                                    CASE
                                       WHEN t.PaidByCD = 'S' THEN 'Shop'
                                       WHEN t.PaidByCD = 'I' THEN 'Insurance Company'
                                       ELSE 'Unknown'
                                    END + '.'
                                ELSE ''
                            END
        FROM @tmpAdminFeeDefinition t
        LEFT JOIN utb_admin_fee_msa afm ON t.AdminMSA = afm.MSA
        WHERE afm.AdminFeeID = @AdminFeeID
          AND (afm.AdminFeeCD <> t.PaidByCD
           OR  afm.Amount <> t.AdminAmount)
        
        IF @Debug = 1
        BEGIN
            print 'Admin fee msa updates...' + @tmpComments
        END
        
        IF LEN(@tmpComments) > 0
        BEGIN
            SET @LogComment = @tmpComments
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END
    END 

    -- Check for Admin fee state changes
    -- Admin fee state additions
    SET @tmpComments = ''

    IF @AdminFeeID IS NOT NULL
    BEGIN
        SELECT @tmpComments = @tmpComments + 'Admin fee for State: ' + t.AdminState + ' was added. ' + 
                                    'Paid by ' + 
                                    CASE
                                       WHEN t.PaidByCD = 'S' THEN 'Shop'
                                       WHEN t.PaidByCD = 'I' THEN 'Insurance Company'
                                       ELSE 'Unknown'
                                    END + '; Amount: $' +
                                    CONVERT(varchar, t.AdminAmount)
        FROM @tmpAdminFeeDefinition t
        WHERE AdminState not in (SELECT StateCode
                                 FROM utb_admin_fee_state
                                 WHERE AdminFeeID = @AdminFeeID
                                   AND EnabledFlag = 1)
          AND AdminState IS NOT NULL
          AND AdminState <> 'Flat'
          AND AdminState <> ''
        
        IF @Debug = 1
        BEGIN
            print 'Admin fee state additions...' + @tmpComments
        END
        
        IF LEN(@tmpComments) > 0
        BEGIN
            SET @LogComment = @tmpComments
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END
    END 

    -- Admin fee state deletions
    SET @tmpComments = ''

    IF @AdminFeeID IS NOT NULL
    BEGIN
        SELECT @tmpComments = @tmpComments + 'Admin fee for State: ' + StateCode + ' was deleted. ' + 
                                    'Old values: Paid by ' + 
                                    CASE
                                       WHEN AdminFeeCD = 'S' THEN 'Shop'
                                       WHEN AdminFeeCD = 'I' THEN 'Insurance Company'
                                       ELSE 'Unknown'
                                    END + '; Amount: $' +
                                    CONVERT(varchar, Amount)
        FROM utb_admin_fee_state
        WHERE AdminFeeID = @AdminFeeID
          AND StateCode not in (SELECT AdminState
                                FROM @tmpAdminFeeDefinition
                                WHERE AdminState IS NOT NULL
                                  AND AdminState <> 'Flat'
                                  AND AdminState <> '')
        
        IF @Debug = 1
        BEGIN
            print 'Admin fee state deletion ...' + @tmpComments
        END
        
        IF LEN(@tmpComments) > 0
        BEGIN
            SET @LogComment = @tmpComments
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END
    END 

    -- Admin fee state updates
    SET @tmpComments = ''

    IF @AdminFeeID IS NOT NULL
    BEGIN
        SELECT @tmpComments = @tmpComments + 'Admin fee for State: ' + t.AdminState + ' was updated.' +
                            CASE
                                WHEN afm.Amount <> t.AdminAmount THEN ' Amount updated from ' + convert(varchar, afm.Amount) + ' to ' + convert(varchar, t.AdminAmount) + '.'
                                ELSE ''
                            END +
                            CASE
                                WHEN afm.AdminFeeCD <> t.PaidByCD THEN ' Paid by updated from ' + 
                                    CASE
                                       WHEN afm.AdminFeeCD = 'S' THEN 'Shop'
                                       WHEN afm.AdminFeeCD = 'I' THEN 'Insurance Company'
                                       ELSE 'Unknown'
                                    END +
                                    ' to ' + 
                                    CASE
                                       WHEN t.PaidByCD = 'S' THEN 'Shop'
                                       WHEN t.PaidByCD = 'I' THEN 'Insurance Company'
                                       ELSE 'Unknown'
                                    END + '.'
                                ELSE ''
                            END
        FROM @tmpAdminFeeDefinition t
        LEFT JOIN utb_admin_fee_state afm ON t.AdminState = afm.StateCode
        WHERE afm.AdminFeeID = @AdminFeeID
          AND (afm.AdminFeeCD <> t.PaidByCD
           OR  afm.Amount <> t.AdminAmount)
        
        IF @Debug = 1
        BEGIN
            print 'Admin fee state updates...' + @tmpComments
        END
        
        IF LEN(@tmpComments) > 0
        BEGIN
            SET @LogComment = @tmpComments
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END
    END       
    
    -- End of audit logging --------------------------------------------------------------------------------------------
    --------------------------------------------------------------------------------------------------------------------
    
    
    
    UPDATE utb_client_fee
    SET ClaimAspectTypeID   = @ClaimAspectTypeID,
        InsuranceCompanyID  = @InsuranceCompanyID,
        CategoryCD          = @CategoryCD,
        Description         = @Description,
        DisplayOrder        = @DisplayOrder,
        EnabledFlag         = @EnabledFlag,
        FeeAmount           = convert(money, @FeeAmount),
        FeeInstructions     = @FeeInstructions,
        ItemizeFlag         = @ItemizeFlag,
        HideFromInvoiceFlag = @HideFromInvoiceFlag,
        InvoiceDescription  = @InvoiceDescription,
        AppliesToCD         = @AppliesWhenCD,
        EffectiveStartDate  = @EffectiveStartDate,
        EffectiveEndDate    = @EffectiveEndDate,
        SysLastUserID       = @SysLastUserID,
        SysLastUpdatedDate  = @now,
        LowerLimit          = convert(money, @LowerLimit),				--Audit Fee Range Changes
        UpperLimit          = convert(money, @UpperLimit),				--Audit Fee Range Changes
        ApplyFeeRange       = @FeeRangeFlag								--Audit Fee Range Changes
    WHERE ClientFeeID = @ClientFeeID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('104|%s|utb_client_fee', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    -- delete all the included services for the client fee
    DELETE FROM utb_client_fee_definition
    WHERE ClientFeeID = @ClientFeeID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('106|%s|utb_client_fee_description', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    -- now insert the ones that were passed in
    INSERT INTO utb_client_fee_definition
    SELECT  @ClientFeeID,
            ServiceID,
            ItemizeFlag,
            RequiredFlag,
            @SysLastUserID,
            @now
    FROM @tmpClientFeeDefinition

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|utb_client_fee_description', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    IF @AdminFeeID IS NOT NULL
    BEGIN
        -- delete all the admin fees for the client fee
        DELETE FROM utb_admin_fee_msa
        WHERE AdminFeeID = @AdminFeeID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('106|%s|utb_admin_fee_msa', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        -- delete all the admin fees for the client fee
        DELETE FROM utb_admin_fee_state
        WHERE AdminFeeID = @AdminFeeID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('106|%s|utb_admin_fee_state', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- delete all the admin fees for the client fee
        UPDATE utb_admin_fee
        SET EnabledFlag = 0
        WHERE AdminFeeID = @AdminFeeID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('104|%s|utb_admin_fee', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END

    IF EXISTS(SELECT * FROM @tmpAdminFeeDefinition)
    BEGIN
        -- Add the fee to the utb_admin_fee
        IF @AdminFeeID IS NOT NULL
        BEGIN

            IF EXISTS (SELECT * 
                        FROM @tmpAdminFeeDefinition 
                        WHERE AdminMSA = 'Flat' 
                          AND AdminState = 'Flat')
            BEGIN
                UPDATE utb_admin_fee
                SET AdminFeeCD = PaidByCD,
                    Amount = AdminAmount,
                    EnabledFlag = 1
                FROM @tmpAdminFeeDefinition t
                WHERE AdminFeeID = @AdminFeeID
                  AND AdminMSA = 'Flat'
                  AND AdminState = 'Flat'

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error
                
                    RAISERROR  ('104|%s|utb_admin_fee', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END
            ELSE
            BEGIN
                UPDATE utb_admin_fee
                SET EnabledFlag = 1,
                    Amount = NULL,
                    AdminFeeCD = 'M'
                WHERE AdminFeeID = @AdminFeeID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error
                
                    RAISERROR  ('104|%s|utb_admin_fee', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END
        END
        ELSE
        BEGIN
            IF EXISTS (SELECT * 
                        FROM @tmpAdminFeeDefinition 
                        WHERE AdminMSA = 'Flat' 
                          AND AdminState = 'Flat')
            BEGIN
                INSERT INTO utb_admin_fee (
                    ClientFeeID, 
                    AdminFeeCD, 
                    AppliesToCD, 
                    Amount, 
                    EnabledFlag, 
                    SplitFlag, 
                    SysLastUserID, 
                    SysLastUpdatedDate)
                SELECT @ClientFeeID,
                    PaidByCD,
                    'F',
                    AdminAmount,
                    1,
                    0,
                    0,
                    @now
                FROM @tmpAdminFeeDefinition 
                WHERE AdminMSA = 'Flat' 
                  AND AdminState = 'Flat'

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error
                
                    RAISERROR  ('105|%s|utb_admin_fee', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END

                SELECT @AdminFeeID = SCOPE_IDENTITY()
            END
            ELSE
            BEGIN
                INSERT INTO utb_admin_fee (
                    ClientFeeID, 
                    AdminFeeCD, 
                    AppliesToCD, 
                    Amount, 
                    EnabledFlag, 
                    SplitFlag, 
                    SysLastUserID, 
                    SysLastUpdatedDate)
                SELECT @ClientFeeID,
                    'I',
                    'M',
                    NULL,
                    1,
                    0,
                    0,
                    @now
                FROM @tmpAdminFeeDefinition 

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error
                
                    RAISERROR  ('105|%s|utb_admin_fee', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END

                SELECT @AdminFeeID = SCOPE_IDENTITY()
            END
        END

        -- add the MSA
        INSERT INTO utb_admin_fee_msa (
            AdminFeeID,
            MSA,
            AdminFeeCD,
            Amount,
            EnabledFlag,
            SysLastUserID,
            SysLastUpdatedDate)
        SELECT @AdminFeeID,
            AdminMSA,
            PaidByCD,
            AdminAmount,
            1,
            0,
            @now
        FROM @tmpAdminFeeDefinition
        WHERE AdminMSA <> 'Flat'
          AND AdminMSA <> ''
          AND AdminMSA IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('105|%s|utb_admin_fee_msa', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- add the State
        INSERT INTO utb_admin_fee_state (
            AdminFeeID,
            StateCode,
            AdminFeeCD,
            Amount,
            EnabledFlag,
            SysLastUserID,
            SysLastUpdatedDate)
        SELECT @AdminFeeID,
            AdminState,
            PaidByCD,
            AdminAmount,
            1,
            0,
            @now
        FROM @tmpAdminFeeDefinition
        WHERE AdminState <> 'Flat'
          AND AdminState <> '' 
          AND AdminState IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('105|%s|utb_admin_fee_state', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF EXISTS(SELECT AdminFeeID
                    FROM utb_admin_fee_msa
                    WHERE AdminFeeID = @AdminFeeID
                      AND EnabledFlag = 1) OR
           EXISTS(SELECT AdminFeeID
                    FROM utb_admin_fee_msa
                    WHERE AdminFeeID = @AdminFeeID
                      AND EnabledFlag = 1)
        BEGIN
            UPDATE utb_admin_fee
            SET AppliesToCD = 'X',
                EnabledFlag = 1
            WHERE AdminFeeID = @AdminFeeID

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
            
                RAISERROR  ('104|%s|utb_admin_fee', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END
    END

    
    COMMIT TRANSACTION InsuranceFeeUpdTran

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Create XML Document to return new updated date time and updated vehicle involved list
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- New Involved Level
            NULL AS [ClientFee!2!ClientFeeID],
            NULL AS [ClientFee!2!SysLastUpdatedDate]

    UNION ALL

    SELECT  2,
            1,
            NULL,
            -- New Involved Level
            @ClientFeeID,
            dbo.ufnUtilityGetDateString( @now )

    ORDER BY tag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
    
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsuranceFeeUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmInsuranceFeeUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
