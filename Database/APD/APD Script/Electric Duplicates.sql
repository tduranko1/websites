/*Avoiding the Duplicates for Electric Bundle*/
If Exists(Select * from utb_bundling_document_type where BundlingID=3 and DocumentTypeID=8)
	update utb_bundling_document_type set DuplicateFlag=0 where BundlingID=3 and DocumentTypeID=8
If Exists(Select * from utb_bundling_document_type where BundlingID=4 and DocumentTypeID=8)
	update utb_bundling_document_type set DuplicateFlag=0 where BundlingID=4 and DocumentTypeID=8
If Exists(Select * from utb_bundling_document_type where BundlingID=22 and DocumentTypeID=8)
	update utb_bundling_document_type set DuplicateFlag=0 where BundlingID=22 and DocumentTypeID=8
	