/************************************************************************************************************************
* Table :		[utb_document]
* SYSTEM:       Lynx Services APD
* AUTHOR:       GLSD452
* FUNCTION:     Save Desk Audit Custom Froms values in Document Upload Page.
************************************************************************************************************************/


ALTER TABLE [dbo].[utb_document] ADD
			[ShopContacted] udt_std_flag NULL,
			[ShopContactName] udt_std_desc_mid NULL,
			[PrimaryReasonCodeText] udt_std_note NULL,
			[SecondaryReasonCodeText] udt_std_note NULL,
			[PrimaryReasonCode] udt_std_note NULL,
			[SecondaryReasonCode] udt_std_note NULL
GO

