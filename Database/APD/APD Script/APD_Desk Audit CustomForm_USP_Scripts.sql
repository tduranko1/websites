Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 2 and name = 'Vehicles') 
Insert into utb_form_field(formid,[Description],Name) values(2,'Vehicles','Vehicles')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 2 and name = 'DataSet') 
Insert into utb_form_field(formid,[Description],Name) values(2,'Estimate/Supplement','DataSet')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 2 and name = 'ClaimAspectID') 
Insert into utb_form_field(formid,[Description],Name) values(2,'ClaimAspectID','ClaimAspectID')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 2 and name = 'SequenceNumber') 
Insert into utb_form_field(formid,[Description],Name) values(2,'SequenceNumber','SequenceNumber')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 16 and name = 'Vehicles') 
Insert into utb_form_field(formid,[Description],Name) values(16,'Vehicles','Vehicles')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 16 and name = 'DataSet') 
Insert into utb_form_field(formid,[Description],Name) values(16,'Estimate/Supplement','DataSet')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 16 and name = 'ClaimAspectID') 
Insert into utb_form_field(formid,[Description],Name) values(16,'ClaimAspectID','ClaimAspectID')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 16 and name = 'SequenceNumber') 
Insert into utb_form_field(formid,[Description],Name) values(16,'SequenceNumber','SequenceNumber')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 17 and name = 'Vehicles') 
Insert into utb_form_field(formid,[Description],Name) values(17,'Vehicles','Vehicles')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 17 and name = 'DataSet') 
Insert into utb_form_field(formid,[Description],Name) values(17,'Estimate/Supplement','DataSet')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 17 and name = 'ClaimAspectID') 
Insert into utb_form_field(formid,[Description],Name) values(17,'ClaimAspectID','ClaimAspectID')
Go
if NOT Exists(Select Name,[Description] from utb_form_field where formid = 17 and name = 'SequenceNumber') 
Insert into utb_form_field(formid,[Description],Name) values(17,'SequenceNumber','SequenceNumber')
Go

GO

/****** Object:  StoredProcedure [dbo].[uspCFDASummaryReportGetDetailXML]    Script Date: 08/07/2012 12:23:58 ******/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[uspCFDASummaryReportGetDetailXML]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspCFDASummaryReportGetDetailXML]
GO

/****** Object:  StoredProcedure [dbo].[uspCFDASummaryReportGetDetailXML]    Script Date: 08/07/2012 12:23:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspCFDASummaryReportGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Ramesh Vishegu]
* FUNCTION:     [Stored procedure to retrieve Custom Forms as XML]
*
* PARAMETERS:  
* No Parameters
*
* RESULT SET:
* Users List as XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

-- exec uspCFDASummaryReportGetDetailXML 1600116,639816,'DA',5871,1,'Supplement'
CREATE PROCEDURE [dbo].[uspCFDASummaryReportGetDetailXML]
    @LynxID             udt_std_id_big,
    @ClaimAspectID      udt_std_id_big,
    @ServiceChannelCD   udt_std_cd,
    @UserID             udt_std_id_big,
    @SequenceNumber		udt_std_id_big		= NULL,
    @DataSet			udt_std_name,
    @FormID				udt_std_id_big   
   
AS
BEGIN
    -- Declare internal variables
    DECLARE @InsuranceCompanyName as varchar(100)
    DECLARE @ClaimNumber as varchar(50)
    DECLARE @CarrierRepName as varchar(100)
    DECLARE @OriginalEstimateDocumentID as bigint
    DECLARE @AuditedEstimateDocumentID as bigint
    DECLARE @OriginalEstimateAmount as decimal(9, 2)
    DECLARE @AuditedEstimateAmount as decimal(9, 2)
    DECLARE @AuditedDifference as decimal(9, 2)
    DECLARE @Deductible as decimal(9, 2)
    DECLARE @Adjustments as decimal(9, 2)
    DECLARE @Betterment as decimal(9, 2)
    DECLARE @NetAuditedEstimate as decimal(9, 2)
    DECLARE @AuditorName as varchar(100)
    DECLARE @AuditorTitle as varchar(100)
    DECLARE @AuditorPhone as varchar(15)
    DECLARE @AuditorEmail as varchar(75)
    DECLARE @ClaimAspectNumber as tinyint
    DECLARE @LastSupplementSeqNo as int
    DECLARE @Message as varchar(1000)
    DECLARE @VANFlag as bit
    DECLARE @LossState as varchar(2)
    DECLARE @LossStateName as varchar(100)
    DECLARE @ShopContacted as Varchar(1000)
	DECLARE @ShopContactName as Varchar(250)
	DECLARE @PrimaryReasonCode as Varchar(1000)
	DECLARE @PrimaryReasonCodeText as Varchar(1000)
	DECLARE @SecondaryReasonCode as Varchar(1000)
	DECLARE @SecondaryReasonCodeText as Varchar(1000)
	DECLARE @PriceAgreedCD as Varchar(250)
    DECLARE @EstimateSummaryRepairTotalID AS int
    DECLARE @EstimateSummaryAdjustmentsTotalID AS int
    DECLARE @EstimateSummaryBettermentID AS int
    DECLARE @EstimateSummaryDeductibleID AS int
    DECLARE @FormDataID as int
    DECLARE @OriginalDocumentID AS bigint
    DECLARE @AuditedDocumentID AS bigint
    
    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFDASummaryReportGetDetailXML'  
    
   -- SET @LastSupplementSeqNo = @SequenceNumber

    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    -- Check to make sure a valid user id was passed in
    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    -- Get the estimate summary ids
    SELECT @EstimateSummaryRepairTotalID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'TT'
      AND Name = 'RepairTotal'

    SELECT @EstimateSummaryAdjustmentsTotalID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'TT'
      AND Name = 'AdjustmentTotal'

    SELECT @EstimateSummaryBettermentID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'AJ'
      AND Name = 'Betterment'

    SELECT @EstimateSummaryDeductibleID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'AJ'
      AND Name = 'Deductible'

     --Get the latest supplement sequence number
    SELECT top 1 @LastSupplementSeqNo = d.SupplementSeqNumber
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and dt.EstimateTypeFlag = 1
      --and d.EstimateTypeCD = 'O'
      and d.EnabledFlag = 1
    order by d.SupplementSeqNumber desc, ds.VANFlag desc
   
    -- Get the original estimate document
    SELECT top 1 @OriginalEstimateDocumentID = d.DocumentID
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @SequenceNumber
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'O'
      and d.EnabledFlag = 1
    order by d.SupplementSeqNumber desc, ds.VANFlag desc

    -- Get the Audited Estimate
    SELECT top 1 @AuditedEstimateDocumentID = d.DocumentID,
			     @VANFlag = ds.VANFlag
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @SequenceNumber
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'A'
      and d.EnabledFlag = 1	 
    order by d.SupplementSeqNumber desc, ds.VANFlag desc
    
    -- Check Orginal Document Upload By Glsd451
     SELECT top 1 @OriginalDocumentID = d.DocumentID
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @LastSupplementSeqNo
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'O'
      and d.EnabledFlag = 1
    order by d.SupplementSeqNumber desc, ds.VANFlag desc
    
    -- Check Audited Document Upload By Glsd451
    SELECT top 1 @AuditedDocumentID = d.DocumentID,
			     @VANFlag = ds.VANFlag
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @LastSupplementSeqNo
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'A'
      and d.EnabledFlag = 1	 
    order by d.SupplementSeqNumber desc, ds.VANFlag desc

 -- Get the Reason code inforamtiom for Estimate/Supplement Audit
    SELECT top 1 @ShopContacted = d.ShopContacted,
				 @ShopContactName = d.ShopContactName,
				 @PrimaryReasonCode = d.PrimaryReasonCode,
				 @PrimaryReasonCodeText = d.PrimaryReasonCodeText,
				 @SecondaryReasonCode = d.SecondaryReasonCode,
				 @SecondaryReasonCodeText = d.SecondaryReasonCodeText,
				 @PriceAgreedCD = d.AgreedPriceMetCD
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @SequenceNumber
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'A'
      and d.EnabledFlag = 1
	 and  dt.Name = @DataSet
    order by d.SupplementSeqNumber desc, ds.VANFlag desc

    SET @Message = ''

    IF @OriginalEstimateDocumentID IS NOT NULL AND @AuditedEstimateDocumentID IS NOT NULL
    BEGIN
        SELECT @OriginalEstimateAmount = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @OriginalEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryRepairTotalID


        SELECT @AuditedEstimateAmount = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryRepairTotalID

        SELECT @Adjustments = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryAdjustmentsTotalID

        SELECT @Betterment = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryBettermentID

        SELECT @Deductible = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryDeductibleID

        SET @AuditedDifference = @OriginalEstimateAmount - @AuditedEstimateAmount

        SET @NetAuditedEstimate = @AuditedEstimateAmount - @Adjustments

        IF @VANFlag = 0
        BEGIN
            SET @NetAuditedEstimate = @NetAuditedEstimate - @Betterment
        END
    END
   
        IF @OriginalDocumentID IS NULL
        BEGIN
            IF @LastSupplementSeqNo = 0
            BEGIN
                SET @Message = 'Original Estimate not found.'
            END
            ELSE
            BEGIN
                SET @Message = 'Original Supplement ' + convert(varchar, @LastSupplementSeqNo) + ' not found.'
            END
        END

        IF @AuditedDocumentID IS NULL
        BEGIN
            IF @LastSupplementSeqNo = 0
            BEGIN
                SET @Message = 'Audited Estimate not found.'
            END
            ELSE
            BEGIN
                SET @Message = 'Audited Supplement ' + convert(varchar, @LastSupplementSeqNo) + ' not found.'
            END
        END
    

    SELECT @CarrierRepName = isNull(uc.NameFirst + ' ' + uc.NameLast, ''),
           @AuditorName = isNull(ua.NameFirst + ' ' + ua.NameLast, ''),
           @AuditorTitle = 'Quality Control Representative',
           @AuditorPhone = '(' + ua.PhoneAreaCode + ') ' + ua.PhoneExchangeNumber + ' ' + ua.PhoneUnitNumber,
           @AuditorEmail = LTRIM(isNull(ua.EmailAddress, '')),
           @ClaimAspectNumber = ca.ClaimAspectNumber,
           @LossState = c.LossState,
           @LossStateName = sc.StateValue
    FROM utb_claim_aspect ca 
    LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
    LEFT JOIN utb_user ua ON ca.AnalystUserID = ua.UserID
    LEFT JOIN utb_user uc ON c.CarrierRepUserID = uc.UserID
    LEFT JOIN utb_state_code sc ON c.LossState = sc.StateCode
    WHERE ca.ClaimAspectID = @ClaimAspectID

    SELECT @InsuranceCompanyName = i.Name,
           @ClaimNumber = c.ClientClaimNumber
    FROM utb_claim c 
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE c.LynxID = @LynxID   
       
    If @DataSet = 'Estimate'
    Begin
		select Top 1 @FormDataID = formdataid from utb_form_data_detail where formfieldid in
		(select formfieldid from utb_form_field where name='ClaimAspectID' and formid=@FormID ) and fielddata= Convert (varchar(50) ,@ClaimAspectID)
		and formdataid in(
		select formdataid from utb_form_data_detail where formfieldid in
		(select formfieldid from utb_form_field where name='DataSet' and formid=@FormID ) and fielddata= @DataSet and
		formdataid in(
		select formdataid from utb_form_data_detail where formfieldid in
		(select formfieldid from utb_form_field where name='lynxid' and formid=@FormID ) and fielddata= Convert(varchar(50),@LynxID)))
	End
	
	If @DataSet = 'Supplement'
	Begin
		select TOP 1 @FormDataID = formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='ClaimAspectID' and formid=@FormID ) and fielddata= Convert (varchar(50) ,@ClaimAspectID) and formdataid in(
			select formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='SequenceNumber' and formid=@FormID ) and fielddata= Convert (varchar(50) ,@SequenceNumber) and formdataid in(
			select formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='DataSet' and formid=@FormID ) and fielddata= @DataSet and formdataid in(
			select formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='lynxid' and formid=@FormID ) and fielddata= Convert(varchar(50),@LynxID))))
	End
	
    SELECT  1 as Tag,
            NULL as Parent,
            @InsuranceCompanyName AS [Root!1!InsuranceCompanyName],
            @LynxID AS [Root!1!LynxID],
            @ClaimAspectNumber AS [Root!1!ClaimAspectNumber],
            @ClaimNumber AS [Root!1!ClaimNumber],
            @LossState AS [Root!1!LossState],
            @LossStateName AS [Root!1!LossStateName],
            @CarrierRepName AS [Root!1!ClaimRepresentative],
            @OriginalEstimateAmount AS [Root!1!OriginalEstimateAmount],
            @AuditedDifference AS [Root!1!AuditedDifference],
            @AuditedEstimateAmount AS [Root!1!AuditedEstimateAmount],
            @Adjustments AS [Root!1!AdjustmentsTotal],
            @Betterment AS [Root!1!Betterment],
            @Deductible AS [Root!1!Deductible],
            @NetAuditedEstimate AS [Root!1!NetAuditedEstimate],
            @AuditorName AS [Root!1!AuditorName],
            @AuditorTitle AS [Root!1!AuditorTitle],
            @AuditorPhone AS [Root!1!AuditorPhone],
            @AuditorEmail AS [Root!1!AuditorEmail],
            @OriginalEstimateDocumentID AS [Root!1!OriginalDocumentID],
            @AuditedEstimateDocumentID AS [Root!1!AuditedDocumentID],
            @PriceAgreedCD AS [Root!1!PriceAgreedCD],
            @ShopContacted AS [Root!1!ShopContacted],
			@ShopContactName AS [Root!1!ShopContactName],
			@PrimaryReasonCode AS [Root!1!PrimaryReasonCode],
			@PrimaryReasonCodeText AS [Root!1!PrimaryReasonCodeText],
			@SecondaryReasonCode AS [Root!1!SecondaryReasonCode],
			@SecondaryReasonCodeText as [Root!1!SecondaryReasonCodeText],			
            @Message AS [Root!1!Message],		
            @FormDataID AS [Root!1!FormDataID], 
            (SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtSurplusOEMComments')as [Root!1!txtSurplusOEMComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkSurplusOEM') as [Root!1!chkSurplusOEM],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtRecComments') as [Root!1!txtRecComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkReconditioned') as [Root!1!chkReconditioned],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtAfterMarketComments') as [Root!1!txtAfterMarketComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkAftermarket') as [Root!1!chkAftermarket],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtLKQComments') as [Root!1!txtLKQComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkLKQ') as [Root!1!chkLKQ],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkAltParts') as [Root!1!chkAltParts],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='AdjustmentAmount') as [Root!1!AdjustmentAmount],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='PriceNotAgreedItemsChecked') as [Root!1!PriceNotAgreedItemsChecked],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='AltPartsLocatedItemsChecked') as [Root!1!AltPartsLocatedItemsChecked],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtNoShopContactOther') as [Root!1!txtNoShopContactOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkNoShopContactOther') as [Root!1!chkNoShopContactOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtProbableTLComments') as [Root!1!txtProbableTLComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkProbableTL') as [Root!1!chkProbableTL],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtProhibitedComments') as [Root!1!txtProhibitedComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkProhibited') as [Root!1!chkProhibited],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRecommendLowerOrgEstComments') as [Root!1!txtRecommendLowerOrgEstComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRecommendLowerOrgEst') as [Root!1!chkRecommendLowerOrgEst],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRepairCompleteComments') as [Root!1!txtRepairCompleteComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRepairComplete') as [Root!1!chkRepairComplete],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtThreeUnsuccessfulAttemptsComments') as [Root!1!txtThreeUnsuccessfulAttemptsComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkThreeUnsuccessfulAttempts') as [Root!1!chkThreeUnsuccessfulAttempts],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkNoAttemptToContactShop') as [Root!1!chkNoAttemptToContactShop],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAltPartsAvailableComments') as [Root!1!txtAltPartsAvailableComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkAltPartsAvailable') as [Root!1!chkAltPartsAvailable],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkShopContacted') as [Root!1!chkShopContacted],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAuditorCommentsUser') as [Root!1!txtAuditorCommentsUser],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAdverseSubroComments') as [Root!1!txtAdverseSubroComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkAdverseSubro') as [Root!1!chkAdverseSubro],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='DataSet') as [Root!1!DataSet],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='Vehicles') as [Root!1!Vehicles],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='SequenceNumber') as [Root!1!SequenceNumber],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='ClaimAspectID') as [Root!1!ClaimAspectID],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAuditorComments') as [Root!1!txtAuditorComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtPriceNotAgreedOther') as [Root!1!txtPriceNotAgreedOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkPriceNotAgreedOther') as [Root!1!chkPriceNotAgreedOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtSysDiffComments') as [Root!1!txtSysDiffComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkSystemDiff') as [Root!1!chkSystemDiff],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRateDiffComments') as [Root!1!txtRateDiffComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRateDiff') as [Root!1!chkRateDiff],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAgreedPriceObtainedWith') as [Root!1!txtAgreedPriceObtainedWith],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkAgreedPriceObtained') as [Root!1!chkAgreedPriceObtained],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkPriceNotAgreed') as [Root!1!chkPriceNotAgreed],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtOther') as [Root!1!txtOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkOther') as [Root!1!chkOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRateCorrComments') as [Root!1!txtRateCorrComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRateCorrection') as [Root!1!chkRateCorrection],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtEstGuidelinesComments') as [Root!1!txtEstGuidelinesComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkEstimateGuidelines') as [Root!1!chkEstimateGuidelines],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtCorrComments') as [Root!1!txtCorrComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkCorrection') as [Root!1!chkCorrection]
			

    --FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END
GO



/****** Object:  StoredProcedure [dbo].[uspCFSaveFormData]    Script Date: 08/07/2012 12:24:59 ******/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[uspCFSaveFormData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspCFSaveFormData]
GO

/****** Object:  StoredProcedure [dbo].[uspCFSaveFormData]    Script Date: 08/07/2012 12:24:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspCFSaveFormData
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Save form data one field at a time
*
* PARAMETERS:  
* (I) @FormID               Form ID to update
* (I) @SupplementID         Supplement ID 
* (I) @FieldName            Field Name as defined by the control
* (I) @FieldDesc            The friendly name of the field
* (I) @FieldValue           The value of the field
* (I) @UserID               The User ID creating this form
*
* RESULT SET:
*   The new last updated date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure
--exec [uspCFSaveFormData] 2,null,null,5871,NULL

CREATE PROCEDURE [dbo].[uspCFSaveFormData] 
    @FormID                 udt_std_id_big,
    @SupplementID           udt_std_id_big = NULL,
    @ParentFormDataID       udt_std_id_big = NULL,
    @UserID                 udt_std_id,
    @FormDataID				udt_std_id_big = NULL
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON

    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @KeyValue               AS varchar(40)
    DECLARE @now                    AS datetime
    DECLARE @FieldID                AS udt_std_int_big
    DECLARE @tmpFormDataID            AS udt_std_int_big

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFSaveFormData'

    
    -- Check to make sure a valid Form Id was passed
    IF  (@FormID IS NULL) OR
        (NOT EXISTS(SELECT FormID FROM dbo.utb_form WHERE FormID = @FormID))
    BEGIN
        -- Invalid Form ID
    
        RAISERROR('101|%s|@FormID|%u', 16, 1, @ProcName, @FormID)
        RETURN
    END

    -- Check to make sure a valid Form Id was passed
    IF  (@SupplementID IS NOT NULL) AND
        (NOT EXISTS(SELECT FormSupplementID FROM dbo.utb_form_supplement WHERE FormID = @FormID  AND FormSupplementID = @SupplementID))
    BEGIN
        -- Invalid Supplement ID
    
        RAISERROR('101|%s|@SupplementID|%u', 16, 1, @ProcName, @SupplementID)
        RETURN
    END

    -- Check to make sure a valid Form Id was passed
    IF  (@ParentFormDataID IS NOT NULL) AND
        (NOT EXISTS(SELECT FormDataID FROM dbo.utb_form_data WHERE FormDataID = @ParentFormDataID))
    BEGIN
        -- Invalid Supplement ID
    
        RAISERROR('101|%s|@ParentFormDataID|%u', 16, 1, @ProcName, @ParentFormDataID)
        RETURN
    END

    -- Check to make sure a valid Form Id was passed
    IF  (@SupplementID IS NOT NULL) AND
        (@ParentFormDataID IS NULL)
    BEGIN
        -- Supplement ID must have a parent form id
    
        RAISERROR('101|%s|@SupplementID, @ParentFormDataID|%u, %u', 16, 1, @ProcName, @SupplementID, @ParentFormDataID)
        RETURN
    END

    -- Check to make sure a valid Sys Last User id was passed in

    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid Sys last User ID
    
        RAISERROR('101|%s|UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Begin Update

    BEGIN TRANSACTION

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @SupplementID IS NULL
    BEGIN
        -- This is a parent form.
        IF @ParentFormDataID IS NULL
        BEGIN
           --This code for chekcing exisiting record for Desk audit for updation 
           -- This code implemented by glsd451
            Declare @DocumentTypeID AS udt_std_int_tiny			
	
			Select @DocumentTypeID = DocumentTypeID from utb_form where Formid=@Formid	
			
			IF @DocumentTypeID = 33 AND @FormDataID != ''
        
				Begin
	
					Update dbo.utb_form_data set SysLastUserID = @UserID , SysLastUpdatedDate = @now where FormDataID = @FormDataID
			
				IF @@error <> 0
				BEGIN
					-- SQL Server Error

					RAISERROR  ('105|%s|utb_form_data in update', 16, 1, @ProcName)
					ROLLBACK TRANSACTION 
					RETURN
				END
			
				SET @tmpFormDataID = @FormDataID
				 End
    
			Else 
    
			Begin 
	    
				-- Create a new parent form data record
				INSERT INTO utb_form_data (
					FormID,
					FormSupplementID,
					SysLastUserID,
					SysLastUpdatedDate )
				VALUES (
					@FormID,
					@SupplementID,
					@UserID,
					@now
				)

				IF @@error <> 0
				BEGIN
					-- SQL Server Error

					RAISERROR  ('105|%s|utb_form_data', 16, 1, @ProcName)
					ROLLBACK TRANSACTION 
					RETURN
				END
				
				SET @tmpFormDataID = SCOPE_IDENTITY()
            End
        END
        ELSE
        BEGIN
            SET @tmpFormDataID = @ParentFormDataID
        END
    END
    ELSE
    BEGIN
        -- Supplemental form
        SELECT @tmpFormDataID = FormDataID
        FROM utb_form_data
        WHERE FormID = @FormID
          AND FormSupplementID = @SupplementID
          AND ParentFormDataID = @ParentFormDataID

        IF @tmpFormDataID IS NULL
        BEGIN
            -- Create a new supplement form data record tied to the parent
            INSERT INTO utb_form_data (
                FormID,
                FormSupplementID,
                ParentFormDataID,
                SysLastUserID,
                SysLastUpdatedDate )
            VALUES (
                @FormID,
                @SupplementID,
                @ParentFormDataID,
                @UserID,
                @now
            )

            IF @@error <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR  ('105|%s|utb_form_data', 16, 1, @ProcName)
                ROLLBACK TRANSACTION 
                RETURN
            END

            SET @tmpFormDataID = SCOPE_IDENTITY()
        END
    END


    COMMIT TRANSACTION

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
	
    RETURN @tmpFormDataID 

END
GO


/****** Object:  StoredProcedure [dbo].[uspCFSaveFormDataDetail]    Script Date: 08/07/2012 12:25:58 ******/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[uspCFSaveFormDataDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspCFSaveFormDataDetail]
GO

/****** Object:  StoredProcedure [dbo].[uspCFSaveFormDataDetail]    Script Date: 08/07/2012 12:25:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspCFSaveFormDataDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Save form data one field at a time
*
* PARAMETERS:  
* (I) @FormDataID           Parent Form Data ID
* (I) @FieldName            Field Name as defined by the control
* (I) @FieldDesc            The friendly name of the field
* (I) @FieldValue           The value of the field
*
* RESULT SET:
*   The new last updated date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspCFSaveFormDataDetail]
    @FormDataID             udt_std_id_big = NULL,
    @FormID                 udt_std_id_big,
    @SupplementID           udt_std_id_big = NULL,
    @FieldName              udt_sys_login,
    @FieldDesc              udt_std_desc_mid,
    @FieldValue             udt_std_desc_huge
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON

    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @KeyValue               AS varchar(40)
    DECLARE @now                    AS datetime
    DECLARE @FieldID                AS udt_std_int_big    
--    DECLARE @FormDataID             AS udt_std_int_big

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFSaveFormDataDetail'

    
    -- Check to make sure a valid Form Data Id was passed
    IF  (@FormDataID IS NULL) OR
        (NOT EXISTS(SELECT FormDataID FROM dbo.utb_form_data WHERE FormDataID = @FormDataID))
    BEGIN
        -- Invalid Form Data ID
    
        RAISERROR('101|%s|@FormDataID|%u', 16, 1, @ProcName, @FormDataID)
        RETURN
    END

    -- Check to make sure a valid Form Id was passed
    IF  (@FormID IS NULL) OR
        (NOT EXISTS(SELECT FormID FROM dbo.utb_form WHERE FormID = @FormID))
    BEGIN
        -- Invalid Form ID
    
        RAISERROR('101|%s|@FormID|%u', 16, 1, @ProcName, @FormID)
        RETURN
    END

    -- Check to make sure a valid Form Id was passed
    IF  (@SupplementID IS NOT NULL) AND
        (NOT EXISTS(SELECT FormSupplementID FROM dbo.utb_form_supplement WHERE FormID = @FormID  AND FormSupplementID = @SupplementID))
    BEGIN
        -- Invalid Supplement ID
    
        RAISERROR('101|%s|@SupplementID|%u', 16, 1, @ProcName, @SupplementID)
        RETURN
    END

    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Begin Update

    BEGIN TRANSACTION

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Get the Field ID
    SELECT @FieldID = FormFieldID
    FROM utb_form_field
    WHERE FormID = @FormID
      AND ((@SupplementID IS NULL AND FormSupplementID IS NULL)
        OR (@SupplementID IS NOT NULL AND FormSupplementID = @SupplementID))
      AND Name = @FieldName
    IF @FieldID IS NULL
    BEGIN
        -- Field is not defined
        INSERT INTO utb_form_field (
        FormID,
        FormSupplementID,
        Name,
        Description
        ) VALUES (
        @FormID,
        @SupplementID,
        @FieldName,
        @FieldDesc
        )


        IF @@error <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR  ('105|%s|utb_form_field', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END

        SET @FieldID = SCOPE_IDENTITY()
    END
   
     -- For checking existance record for DA 
     
    --Desk audit form 
    Declare @DocumentTypeID AS udt_std_int_tiny
	Declare @FDDID AS udt_std_int_big
	
	Select @DocumentTypeID = DocumentTypeID from utb_form where Formid=@Formid
	
	Select @FDDID = FormDataID from utb_form_data_detail where FormDataID=@FormDataID and 
		FormFieldID in(Select FormFieldID from utb_form_field where FormID = @FormID and
						Name like @FieldName)

	IF @DocumentTypeID = 33 And @FDDID != ''
	Begin
	
		exec uspCFUpdateFormDataDetail @FDDID,@FieldName,@FieldValue 
		   
    End
    
    Else 
    
    Begin 
	    
	   INSERT INTO dbo.utb_form_data_detail
		(
			FormDataID,
			FormFieldID,
			FieldData
		)
		VALUES (@FormDataID,
				@FieldID,
				@FieldValue)
    End
    
    

    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_form_data_detail', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    COMMIT TRANSACTION

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @FormDataID 

END
GO


/****** Object:  StoredProcedure [dbo].[uspCFUpdateFormDataDetail]    Script Date: 08/07/2012 12:26:49 ******/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[uspCFUpdateFormDataDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspCFUpdateFormDataDetail]
GO

/****** Object:  StoredProcedure [dbo].[uspCFUpdateFormDataDetail]    Script Date: 08/07/2012 12:26:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspCFUpdateFormDataDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       glsd451
* FUNCTION:     Update form data one field at a time
*
* PARAMETERS:  
* (I) @FormDataID           Parent Form Data ID
* (I) @FieldName            Field Name as defined by the control
* (I) @FieldValue           The value of the field
*
* RESULT SET:
*   The new last updated date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure
--EXEC [uspCFUpdateFormDataDetail] 29964,'SequenceNumber',1

CREATE PROCEDURE [dbo].[uspCFUpdateFormDataDetail]
    @FormDataID             udt_std_id_big,
    @FieldName              udt_sys_login,
    @FieldValue             udt_std_desc_huge
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON

    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @KeyValue               AS varchar(40)
    DECLARE @now                    AS datetime
    DECLARE @FieldID                AS udt_std_int_big

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFUpdateFormDataDetail'

    
    -- Check to make sure a valid Form Data Id was passed
    IF  (@FormDataID IS NULL) OR
        (NOT EXISTS(SELECT FormDataID FROM dbo.utb_form_data WHERE FormDataID = @FormDataID))
    BEGIN
        -- Invalid Form Data ID
    
        RAISERROR('101|%s|@FormDataID|%u', 16, 1, @ProcName, @FormDataID)
        RETURN
    END

    -- Begin Update

    BEGIN TRANSACTION

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Get the Field ID
     SELECT @FieldID = FDD.FormFieldID
		FROM dbo.utb_form_field AS ff 
			INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  
			where formdataid = @FormDataID and Name =@FieldName
   
   
     -- Check to make sure a valid Form Data Id was passed
    IF  (@FieldID IS NULL) OR
        (NOT EXISTS(SELECT [FormFieldID] FROM dbo.utb_form_data_detail WHERE [FormDataID] =@FormDataID AND [FormFieldID] = @FieldID))
    BEGIN
        -- Invalid Field ID
    
        RAISERROR('101|%s|@FieldID|%u', 16, 1, @ProcName, @FieldID)
        RETURN
    END
   
   
   

    UPDATE dbo.utb_form_data_detail 
    SET FieldData = @FieldValue
    WHERE [FormDataID] =@FormDataID AND 
           [FormFieldID] = @FieldID
           
--update utb_form_data_detail set fielddata = '0' where formdataid = 29964 and formfieldid = 277
    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_form_data_detail', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    COMMIT TRANSACTION

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @FormDataID 

END
GO


/****** Object:  StoredProcedure [dbo].[uspCustomFormsGetListXML]    Script Date: 08/07/2012 12:27:33 ******/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[uspCustomFormsGetListXML]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspCustomFormsGetListXML]
GO

/****** Object:  StoredProcedure [dbo].[uspCustomFormsGetListXML]    Script Date: 08/07/2012 12:27:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
/************************************************************************************************************************  
*  
* PROCEDURE:    uspCustomFormsGetListXML  
* SYSTEM:       Lynx Services APD  
* AUTHOR:       [Ramesh Vishegu]  
* FUNCTION:     [Stored procedure to retrieve Custom Forms as XML]  
*  
* PARAMETERS:    
* No Parameters  
*  
* RESULT SET:  
* Users List as XML  
*  
*  
* VSS  
* $Workfile: $  
* $Archive:  $  
* $Revision: $  
* $Author:   $  
* $Date:     $  
*  
************************************************************************************************************************/  
  
-- Create the stored procedure  
  
  
CREATE PROCEDURE [dbo].[uspCustomFormsGetListXML]  --1600423,5844  
    @LynxID     udt_std_id_big,  
    @UserID     udt_std_id_big  
  
AS  
BEGIN  
    -- Declare internal variables  
    DECLARE @InsuranceCompanyID as int  
    DECLARE @InsuranceCompanyName as varchar(50)  
    DECLARE @ClaimAspectTypeIDVehicle as int  
    DECLARE @LossState as varchar(2)  
  DECLARE @UserName as varchar(100)  
  DECLARE @UserPhone as varchar(10)  
  DECLARE @UserEmail as varchar(100)  
  DECLARE @PolicyNumber as varchar(100)  
  DECLARE @ClientClaimNumber as varchar(100)  
      
    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts   
  
    SET @ProcName = 'uspCustomFormsGetListXML'  
  
    -- Check to make sure a valid Lynx id was passed in  
    IF  (@LynxID IS NULL) OR  
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))  
    BEGIN  
        -- Invalid Lynx ID  
      
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)  
        RETURN  
    END  
  
    -- Check to make sure a valid user id was passed in  
    IF  (@UserID IS NULL) OR  
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))  
    BEGIN  
        -- Invalid User ID  
      
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)  
        RETURN  
    END  
  
    -- Get the claim aspect type id for the vehicle  
    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID  
    FROM utb_claim_aspect_type  
    WHERE Name = 'Vehicle'  
  
    -- Get the Insurance Company   
    SELECT @InsuranceCompanyID = c.InsuranceCompanyID,  
           @InsuranceCompanyName = i.Name,  
           @LossState = c.LossState,  
           @PolicyNumber = c.PolicyNumber,  
           @ClientClaimNumber = c.ClientClaimNumber  
    FROM utb_claim c  
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID  
    WHERE LynxID = @LynxID  
      
    SELECT  @UserName = NameFirst + ' ' + NameLast,  
   @UserPhone = PhoneAreaCode + PhoneExchangeNumber + PhoneUnitNumber,  
   @UserEmail = EmailAddress  
    FROM utb_user  
 WHERE UserID = @UserID  
  
    -- Begin XML Select  
  
    -- Select Root  
     DECLARE @tmpReference TABLE  
    (  
        FormID                int           NOT NULL,  
        BundlingTaskID        int               NULL,    
        DocumentTypeID      int               NULL,  
        DocName               varchar(50)       NULL,  
        InsuranceCompanyID   int                NULL,  
        EventID              int                NULL,  
        LossStateCode         varchar(10)       NULL,  
        ShopStateCode        varchar(10)        NULL,      
        AutoBundlingFlag    INT                 NULL,  
        DataMiningFlag      int                 NULL,  
        HTMLPath           varchar(50)           NULL,  
        formName           varchar(220)         Not NULL,  
        PDFPath        varchar(220)           Not NULL,  
        PertainsToCD     varchar(4)             NULL,  
        ServiceChannelCD   varchar(4)           NULL,  
        SQLProcedure        varchar(50)          NULL,  
        fax                varchar(5)           Null  
          
    )  
      
      
    DECLARE @ClaimAspectID as int  
      
    /* SELECT    
        @ClaimAspectID = ca.ClaimAspectID    
    FROM utb_claim_aspect ca  
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID      
    WHERE ca.LynxID = @LynxID  
      AND ca.EnabledFlag = 1  
      AND casc.EnabledFlag = 1 */  
      
      
     
                
    IF NOT EXISTS (SELECT InvoiceID FROM utb_invoice I WHERE I.ClaimAspectID IN   
     ( SELECT    
        ca.ClaimAspectID    
    FROM utb_claim_aspect ca  
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID      
    WHERE ca.LynxID = @LynxID  
      AND ca.EnabledFlag = 1  
      AND casc.EnabledFlag = 1) AND EnabledFlag = 1)  
BEGIN  
  
 INSERT INTO @tmpReference ( FormID,                 
                                BundlingTaskID,    
                                DocumentTypeID,       
                                DocName,    
                                EventID,               
                                InsuranceCompanyID,                                             
                                LossStateCode,         
                                ShopStateCode,            
                                AutoBundlingFlag,     
                                DataMiningFlag,      
                                HTMLPath,             
                                FormName,            
                                PDFPath,         
                                PertainsToCD,      
                                ServiceChannelCD,    
                                SQLProcedure ,   
                                Fax)  
                                  
    SELECT  FormID,   
            BundlingTaskID,    
            f.DocumentTypeID,   
            dt.Name,  
            EventID,   
            InsuranceCompanyID,   
            LTrim(isNull(LossStateCode, '')),   
            LTrim(isNull(ShopStateCode, '')),   
            AutoBundlingFlag,   
            DataMiningFlag,   
            HTMLPath,  
            f.Name,   
            PDFPath,   
            LTrim(isNull(PertainsToCD, '')),   
            LTrim(isNull(ServiceChannelCD, '')),   
            SQLProcedure,  
   CASE   
    WHEN f.PertainsToCD = 'S' THEN 'FAX'  
    ELSE ''  
   END  
             
    FROM utb_form f  
    LEFT JOIN utb_document_type dt ON f.DocumentTypeID = dt.DocumentTypeID  
    WHERE (InsuranceCompanyID IS NULL   
       OR InsuranceCompanyID = @InsuranceCompanyID)  
      AND f.EnabledFlag = 1  
      AND f.SystemFlag = 0  
      --AND ServiceChannelCD <> 'RRP'   
      AND (f.Name <> 'QBE Memo Bill' OR ServiceChannelCD = 'DA')  
      --AND (f.Name <> 'BCIF Request' or InsuranceCompanyID <> 387)  
        
       
        
        
END  
ELSE  
BEGIN  
INSERT INTO @tmpReference ( FormID,                 
                                BundlingTaskID,    
                                DocumentTypeID,       
                                DocName,   
                                EventID,               
                                InsuranceCompanyID,  
                                LossStateCode,         
                                ShopStateCode,            
                                AutoBundlingFlag,     
                                DataMiningFlag,      
                                HTMLPath,             
                                FormName,            
                                PDFPath,         
                                PertainsToCD,      
                                ServiceChannelCD,    
                                SQLProcedure,   
                                Fax)  
  
    SELECT   
            -- Form  
            FormID,   
            BundlingTaskID,   
            f.DocumentTypeID,   
            dt.Name,  
            EventID,   
            InsuranceCompanyID,   
            LTrim(isNull(LossStateCode, '')),   
            LTrim(isNull(ShopStateCode, '')),   
            AutoBundlingFlag,   
            DataMiningFlag,   
            HTMLPath,  
            f.Name,   
            PDFPath,   
            LTrim(isNull(PertainsToCD, '')),   
            LTrim(isNull(ServiceChannelCD, '')),   
            SQLProcedure,  
   CASE   
    WHEN f.PertainsToCD = 'S' THEN 'FAX'  
    ELSE ''  
   END  
             
    FROM utb_form f  
    LEFT JOIN utb_document_type dt ON f.DocumentTypeID = dt.DocumentTypeID  
    WHERE (InsuranceCompanyID IS NULL   
       OR InsuranceCompanyID = @InsuranceCompanyID)  
      AND f.EnabledFlag = 1  
      AND f.SystemFlag = 0  
        
      --AND (@InvoiceExists > 0 AND ServiceChannelCD = 'RRP' AND f.Name = 'QBE Memo Bill' )  
END    
      
    SELECT  1 as Tag,  
            NULL as Parent,  
            @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],  
            @InsuranceCompanyName AS [Root!1!InsuranceCompanyName],  
            @LynxID AS [Root!1!LynxID],  
            @LossState AS [Root!1!LossState],  
            @ClientClaimNumber AS [Root!1!ClientClaimNumber],  
            @PolicyNumber AS [Root!1!PolicyNumber],  
            @UserName as [Root!1!UserName],  
            @UserPhone as [Root!1!UserPhone],  
            @UserEmail as [Root!1!UserEmail],  
            -- Vehicle List  
            NULL as [Vehicle!2!ClaimAspectID],  
            NULL as [Vehicle!2!ClaimAspectNumber],  
            NULL as [Vehicle!2!OwnerNameFirst],  
            NULL as [Vehicle!2!OwnerNameLast],  
            NULL as [Vehicle!2!OwnerBusinessName],  
            NULL as [Vehicle!2!ShopFaxNumber],  
            NULL as [Vehicle!2!AnalystUserID],  
            NULL as [Vehicle!2!InvoiceID],  
            -- Service Channel  
            NULL as [ServiceChannel!3!ClaimAspectServiceChannelID],  
            NULL as [ServiceChannel!3!ServiceChannelCD],  
            NULL as [ServiceChannel!3!ServiceChannelDesc],  
            NULL as [ServiceChannel!3!ShopState],  
            -- Form  
            NULL as [Form!4!FormID],  
            NULL as [Form!4!BundlingTaskID],  
            NULL as [Form!4!DocumentTypeID],  
            NULL as [Form!4!DocumentTypeName],  
            NULL as [Form!4!EventID],  
            NULL as [Form!4!InsuranceCompanyID],  
            NULL as [Form!4!LossStateCode],  
            NULL AS [Form!4!ShopStateCode],  
            NULL AS [Form!4!AutoBundlingFlag],  
            NULL AS [Form!4!DataMiningFlag],  
            NULL as [Form!4!HTMLPath],  
            NULL as [Form!4!Name],  
            NULL as [Form!4!PDFPath],  
            NULL AS [Form!4!PertainsToCD],  
            NULL AS [Form!4!ServiceChannelCD],  
            NULL AS [Form!4!SQLProcedure],  
            NULL AS [Form!4!DestinationType],  
     
            -- Form Supplement  
            NULL AS [Supplement!5!FormSupplementID],  
            NULL AS [Supplement!5!LossStateCode],  
            NULL AS [Supplement!5!ShopStateCode],  
            NULL AS [Supplement!5!HTMLPath],  
            NULL AS [Supplement!5!Name],  
            NULL AS [Supplement!5!PDFPath],  
            NULL AS [Supplement!5!ServiceChannelCD],  
            NULL AS [Supplement!5!SQLProcedure],  
              
              
            -- Added dataset for Estimate and Supplement glsd451  
            NULL AS [DataSet!6!SupplementSeqNumber],
            NULL AS [DataSet!6!Name],
            NULL AS [DataSet!6!ServiceChannelCD],
            NULL AS [DataSet!6!DuplicateFlag]            
              
              
  
    UNION ALL  
  
    SELECT  2,  
            1,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            ca.ClaimAspectID,  
            ca.ClaimAspectNumber,  
            IsNull((SELECT  Top 1 i.NameFirst  
                    FROM  dbo.utb_claim_aspect_involved cai  
                    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)  
                    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)  
                    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)  
                    WHERE cai.ClaimAspectID = cv.ClaimAspectID  
                      AND cai.EnabledFlag = 1  
                      AND irt.Name = 'Owner'), ''),  
            IsNull((SELECT  Top 1 i.NameLast  
                    FROM  dbo.utb_claim_aspect_involved cai  
                   LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)  
                    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)  
                    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)  
                    WHERE cai.ClaimAspectID = cv.ClaimAspectID  
                      AND cai.EnabledFlag = 1  
                      AND irt.Name = 'Owner'), ''),  
            IsNull((SELECT  Top 1 i.BusinessName  
                    FROM  dbo.utb_claim_aspect_involved cai  
                    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)  
                    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)  
                    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)  
                    WHERE cai.ClaimAspectID = cv.ClaimAspectID  
                      AND cai.EnabledFlag = 1  
                      AND irt.Name = 'Owner'), ''),  
            IsNull((SELECT sl.FaxAreaCode + FaxExchangeNumber + FaxUnitNumber  
      FROM utb_shop_location sl  
      LEFT JOIN utb_assignment a ON sl.ShopLocationID = a.ShopLocationID  
      LEFT JOIN utb_claim_aspect_service_channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID  
      LEFT JOIN utb_claim_aspect ca1 ON casc.ClaimAspectID = ca1.ClaimAspectID  
      WHERE a.assignmentsequencenumber = 1 AND a.CancellationDate IS NULL  
        AND casc.PrimaryFlag = 1  
        AND ca1.ClaimAspectID = ca.ClaimAspectID  
     ), ''),  
            ca.AnalystUserID,  
             inv.InvoiceID,  
            -- Service Channel  
            NULL, NULL, NULL, NULL,  
            -- Form  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Form Supplement  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
           -- Added dataset for Estimate and Supplement glsd451  
            NULL, NULL, NULL, NULL
    FROM utb_claim_aspect ca  
    LEFT JOIN utb_claim_vehicle cv on ca.ClaimAspectID = cv.ClaimAspectID  
    Left JOIN utb_invoice inv on inv.ClaimAspectID =  ca.ClaimAspectID and  
    inv.enabledflag = 1   
    WHERE ca.LynxID = @LynxID  
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle  
      AND ca.EnabledFlag = 1  
  
  
    UNION ALL  
  
    SELECT  3,  
            2,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            ca.ClaimAspectID,  
            NULL, NULL, NULL, NULL, NULL, NULL,NULL,  
            -- Service Channel  
            casc.ClaimAspectServiceChannelID,  
            casc.ServiceChannelCD,   
            tmp.Name,  
            LTRIM(isNULL((SELECT TOP 1 sl.AddressState  
                            FROM utb_assignment a  
                            LEFT JOIN utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID  
                            WHERE a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID  
                              AND a.CancellationDate is null AND a.assignmentsequencenumber = 1  
                              AND a.ShopLocationID is not null), '')),  
            -- Form  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Form Supplement  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
             -- Added dataset for Estimate and Supplement glsd451  
            NULL, NULL, NULL, NULL  
    FROM utb_claim_aspect ca  
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID  
    LEFT JOIN (SELECT Code, Name  
                FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')) tmp ON casc.ServiceChannelCD = tmp.Code  
    WHERE ca.LynxID = @LynxID  
      AND ca.EnabledFlag = 1  
      AND casc.EnabledFlag = 1  
  
    
UNION ALL  
    -- Get all forms that are applicable to all insurance companies and the ones that are specific  
  SELECT 4,  
            1,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,  
            -- Service Channel  
            NULL, NULL, NULL, NULL,  
            -- Form  
            FormID,   
            BundlingTaskID,   
            f.DocumentTypeID,   
            DocName,  
            EventID,   
            InsuranceCompanyID,   
            LTrim(isNull(LossStateCode, '')),   
            LTrim(isNull(ShopStateCode, '')),   
            AutoBundlingFlag,   
            DataMiningFlag,   
            HTMLPath,  
            FormName,   
            PDFPath,   
            LTrim(isNull(PertainsToCD, '')),   
            LTrim(isNull(ServiceChannelCD, '')),   
            SQLProcedure,  
   CASE   
    WHEN f.PertainsToCD = 'S' THEN 'FAX'  
    ELSE ''  
   END,  
            -- Form Supplement  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
             -- Added dataset for Estimate and Supplement glsd451 
            NULL, NULL, NULL, NULL
    FROM @tmpReference f  
      
  
    UNION ALL  
  
    -- Get all supplement forms  
    SELECT  5,  
            4,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,  
            -- Service Channel  
            NULL, NULL, NULL, NULL,  
            -- Form  
            fs.FormID,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Form Supplement  
            fs.FormSupplementID,   
            LTrim(isNull(fs.LossStateCode, '')),   
            LTrim(isNull(fs.ShopStateCode, '')),   
            fs.HTMLPath,   
            fs.Name,   
            fs.PDFPath,   
            LTrim(isNull(fs.ServiceChannelCD, '')),   
            fs.SQLProcedure,  
            -- Added dataset for Estimate and Supplement glsd451  
            NULL, NULL, NULL, NULL 
    FROM utb_form_supplement fs  
    LEFT JOIN utb_form f ON fs.FormID = f.FormID  
    WHERE (f.InsuranceCompanyID IS NULL   
       OR f.InsuranceCompanyID = @InsuranceCompanyID)  
      AND f.EnabledFlag = 1  
      AND f.SystemFlag = 0  
      AND fs.EnabledFlag = 1  
  
       
              
    UNION ALL  
    -- Added dataset for Estimate and Supplement glsd451 
    SELECT  6 ,  
            1 ,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            -- Vehicle List  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,  
            -- Service Channel  
            NULL, NULL, NULL, NULL,  
            -- Form  
            NULL,   
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
             -- Form Supplement  
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  
			SupplementSeqNumber,Name,ServiceChannelCD,duplicateflag 
        from (
			SELECT
			dt.Name,
			d.EstimateTypeCD,
			d.duplicateflag,
			d.SupplementSeqNumber,
			casc.ServiceChannelCD
			FROM dbo.utb_claim_aspect_Service_Channel_document cascd
			INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
			LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
			INNER JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
			INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
			WHERE ca.LynxID = @LynxID
			AND d.EnabledFlag = 1
			AND casc.EnabledFlag = 1
			AND casc.ServiceChannelCD = 'DA'
			AND dt.Name in('Estimate','Supplement')
			AND d.EstimateTypeCD = 'O'
			AND d.duplicateflag = 0
			group by dt.Name,
			d.EstimateTypeCD,
			d.duplicateflag,
			d.SupplementSeqNumber,casc.ServiceChannelCD,duplicateflag having count(d.EstimateTypeCD)=1
			UNION ALL

			SELECT
			dt.Name,
			d.EstimateTypeCD,
			d.duplicateflag,
			d.SupplementSeqNumber,
			casc.ServiceChannelCD
			FROM dbo.utb_claim_aspect_Service_Channel_document cascd
			INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
			LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
			INNER JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
			INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
			WHERE ca.LynxID = @LynxID
			AND d.EnabledFlag = 1
			AND casc.EnabledFlag = 1
			AND casc.ServiceChannelCD = 'DA'
			AND dt.Name in('Estimate','Supplement')
			AND d.EstimateTypeCD = 'A'
			AND d.duplicateflag = 0
			group by dt.Name,
			d.EstimateTypeCD,
			d.duplicateflag,
			d.SupplementSeqNumber,casc.ServiceChannelCD having count(d.EstimateTypeCD)=1
			)
			as t1 group by SupplementSeqNumber,Name,ServiceChannelCD,duplicateflag having count(SupplementSeqNumber) =2  
            
            
            
    ORDER BY [Vehicle!2!ClaimAspectID], [Form!4!FormID], Tag  
  
    --FOR XML EXPLICIT  
  
    IF @@ERROR <> 0  
    BEGIN  
       -- SQL Server Error  
      
        RAISERROR('99|%s', 16, 1, @ProcName)  
        RETURN(1)  
    END      
END

GO


/****** Object:  StoredProcedure [dbo].[uspEstimateQuickGetDetailXML]    Script Date: 08/07/2012 12:27:33 ******/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[uspEstimateQuickGetDetailXML]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspEstimateQuickGetDetailXML]
GO

/****** Object:  StoredProcedure [dbo].[uspEstimateQuickGetDetailXML]    Script Date: 08/07/2012 12:27:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateQuickGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Retreives estimate data for the estimate utility program
*
* PARAMETERS:
* (I) @DocumentID                   The Document ID to retrieve
* (I) @ClaimAspectID                The Claim Aspect ID of the document
* (I) @LynxID                       The Lynx ID of the document
* (I) @ClaimAspectServiceChannelID  The Claim Aspect Service Channel ID of the document
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspEstimateQuickGetDetailXML]
    @DocumentID                     udt_std_id_big,
    @ClaimAspectID                  udt_std_id_big,
    @LynxID                         udt_std_id = NULL,
    @ClaimAspectServiceChannelID    udt_std_id_big = NULL,
    @UserID							      udt_std_id = NULL
AS
BEGIN
    -- Declare internal variables

    SET NOCOUNT ON
    DECLARE @ProcName               varchar(30)

    SET @ProcName = 'uspEstimateQuickGetDetailXML'

    DECLARE @CoverageProfileCD      udt_std_cd
    DECLARE @DeductibleAmt          udt_std_money
    DECLARE @ShopLocationID         udt_std_id_big
    DECLARE @AssignmentDate         udt_std_datetime
    DECLARE @PricingAvailable       char(1)
    DECLARE @ActiveAssignment       bit
    DECLARE @ProgramShopAssignment  bit
    DECLARE @ClaimAspectStatus      int
    DECLARE @ReinspectionCompleted  bit
    DECLARE @ReinspectionPerformedBy varchar(100)
    DECLARE @ReinspectionDate       datetime
    DECLARE @UserIsSupervisor		   bit
    DECLARE @ServiceChannelCD       udt_std_cd
    DECLARE @EarlyBillFlag          bit
    DECLARE @SupervisorFlag         bit
    DECLARE @ApprovedDocumentCount  bit
    DECLARE @WarrantyExistsFlag     bit

    SET @ShopLocationID = 0

    -- Validate parameters

    IF @DocumentID > 0
    BEGIN
        IF NOT EXISTS(SELECT DocumentID FROM dbo.utb_document WHERE DocumentID = @DocumentID)
        BEGIN
            -- Invalid Document ID

            RAISERROR  ('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
            RETURN
        END

        --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        IF NOT EXISTS(
                        SELECT DocumentID
                        FROM dbo.utb_claim_aspect_service_channel_document cascd
                        Inner join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                        WHERE casc.ClaimAspectID = @ClaimAspectID
                        AND cascd.DocumentID = @DocumentID
                     )
        --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        BEGIN
            RAISERROR('%s: Invalid ClaimAspectID for this DocumentID', 16, 1, @ProcName)
        END
        ELSE
        BEGIN
            SELECT @ServiceChannelCD = casc.ServiceChannelCD,
                   @WarrantyExistsFlag = ca.WarrantyExistsFlag
            FROM dbo.utb_claim_aspect_service_channel_document cascd
            Inner join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
            Inner join utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
            WHERE casc.ClaimAspectID = @ClaimAspectID
            AND cascd.DocumentID = @DocumentID
        END
    END
    ELSE
    BEGIN
        -- IF DocumentID = 0, we need a LYNX ID

        IF @LynxID IS NULL OR
           NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID)
        BEGIN
            RAISERROR  ('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
            RETURN
        END
    END
    
    SELECT @UserIsSupervisor = SupervisorFlag
    FROM utb_user
    WHERE UserID = @UserID


    -- Gather metadata for all entities updatable columns

    DECLARE @tmpMetadata TABLE
    (
        GroupName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )

    INSERT INTO @tmpMetadata (GroupName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Estimate',
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE  ((Table_Name = 'utb_document' AND Column_Name IN
            ('DocumentTypeID',
             'SupplementSeqNumber',
             'AgreedPriceMetCD',
             'DuplicateFlag',
             'EstimateTypeCD',
             'SysLastUserID')))
      OR  ((Table_Name = 'utb_estimate_summary' AND Column_Name IN
            ('ExtendedAmt')))
        --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
      OR  ((Table_Name = 'utb_claim_aspect_service_channel_document' AND Column_Name IN
            ('ClaimAspectServiceChannelID')))
        --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END


    -- Get LYNX ID from ClaimAspectID if LynxID was not provided

    IF @LynxID IS NULL
    BEGIN
        SELECT  @LynxID = LynxID
          FROM  dbo.utb_claim_aspect
          WHERE ClaimAspectID = @ClaimAspectID

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END

    -- Create temporary Table to hold Approved estimates

    DECLARE @tmpApprovedEst TABLE
    (
        DocumentID          bigint NOT NULL,
        ClaimAspectID       bigint NULL
    )
    
    INSERT INTO @tmpApprovedEst
    SELECT d.DocumentID, ca.ClaimAspectID
    FROM utb_claim c
    LEFT JOIN utb_claim_aspect ca ON ca.LynxID = c.LynxID
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    WHERE c.LynxID = @LynxID
      AND d.EnabledFlag = 1
      AND dt.EstimateTypeFlag = 1
      AND d.ApprovedFlag = 1 

    -- Gather Reference Data

    DECLARE @tmpReference TABLE
    (
        ListName            varchar(50)     NOT NULL,
        DisplayOrder        int             NULL,
        ReferenceId         varchar(10)     NOT NULL,
        Name                varchar(50)     NOT NULL,
        EstimateTypeFlag    bit             NULL,
        ClaimAspectID       int             NULL,
        DeductibleAmt       money           NULL,
        ShowAudited         bit             NULL,
        ServiceChannelCD    varchar(4)      NULL,
        ApprovedDocumentCount int           NULL
    )

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceId, Name, EstimateTypeFlag, ClaimAspectID, DeductibleAmt, ShowAudited, ServiceChannelCD, ApprovedDocumentCount)

    SELECT  'DocumentType' AS ListName,
            DisplayOrder,
            convert(varchar, DocumentTypeID),
            Name,
            EstimateTypeFlag,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_document_type
    WHERE   EnabledFlag = 1
      AND   DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'AgreedPriceMetCD' AS ListName,
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes('utb_document', 'AgreedPriceMetCD')

    UNION ALL

    SELECT  'EstimateTypeCD' AS ListName,
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes('utb_document', 'EstimateTypeCD')

    UNION ALL

    SELECT  DISTINCT 'PertainsTo',
            NULL,
            EntityCode,
            cel.Name,
            NULL,
            cel.ClaimAspectID,
            CASE
              WHEN ca.CoverageProfileCD IS NULL THEN 0
		--*********************************************************************************
		--Project: 210474 APD - Enhancements to support multiple concurrent service channels
		--Note:	Verify the logic below with JR/JP
		--	M.A. 20061109
		--*********************************************************************************

	      else ISNULL(cc.DeductibleAmt,0.00)
		
        --      WHEN ca.CoverageProfileCD = 'COLL' THEN IsNull(cc.CollisionDeductibleAmt, 0.00)
        --      WHEN ca.CoverageProfileCD = 'COMP' THEN IsNull(cc.ComprehensiveDeductibleAmt, 0.00)
        --      WHEN ca.CoverageProfileCD = 'LIAB' THEN IsNull(cc.LiabilityDeductibleAmt, 0.00)
        --      WHEN ca.CoverageProfileCD = 'UIM' THEN IsNull(cc.UnderinsuredDeductibleAmt, 0.00)
        --      WHEN ca.CoverageProfileCD = 'UM' THEN IsNull(cc.UninsuredDeductibleAmt, 0.00)
		
            END,
            (SELECT CASE
                        WHEN casc.ServiceChannelCD = 'DA' or casc.ServiceChannelCD = 'DR' or casc.ServiceChannelCD = 'RRP' THEN 1
                        ELSE 0
                    END
             FROM dbo.utb_claim_aspect ca
             --Project:210474 APD Remarked-off the following to support the schema change M.A.20061218
             --LEFT JOIN dbo.utb_assignment_type cat ON (ca.CurrentAssignmentTypeID = cat.AssignmentTypeID)
             --Project:210474 APD Added the following to support the schema change M.A.20061218
             LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc
             on ca.ClaimAspectID = casc.ClaimAspectID
             and casc.Primaryflag = 1
             WHERE ca.ClaimAspectID = cel.ClaimAspectID),
            NULL,
            (SELECT COUNT(DocumentID)
             FROM @tmpApprovedEst t
             WHERE t.ClaimAspectID = ca.ClaimAspectID)
      FROM  dbo.ufnUtilityGetClaimEntityList(@LynxID, 1, 0) cel   -- Get All exposures
      LEFT JOIN dbo.utb_claim_aspect ca ON (cel.ClaimAspectID = ca.ClaimAspectID)
      Left Outer join   utb_Claim c
      on           c.lynxid = ca.Lynxid
      Left Outer join   utb_Client_Coverage_Type cct
      on           c.InsuranceCompanyID = cct.InsuranceCompanyID
      and           cct.ClientCoverageTypeID = ca.ClientCoverageTypeID
      and          cct.CoverageProfileCD = ca.CoverageProfileCD
      Left Outer join   utb_Claim_Coverage cc
      on           cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
      and          c.LynxID = cc.LynxID
      and          cc.CoverageTypeCD = cct.CoverageProfileCD
      and          cc.AddtlCoverageFlag = 0 -- Exclude the additional coverages


    UNION ALL

   SELECT  'ServiceChannelCD' AS ListName,
            NULL,
            code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes('utb_Claim_Aspect_Service_Channel', 'ServiceChannelCD')

    UNION ALL

    SELECT  DISTINCT 'ServiceChannel',
            NULL,
            convert(varchar(10),casc.ClaimAspectServiceChannelID),
            fn.Name,
            NULL,
            ca.ClaimAspectID,
            NULL,
            NULL,
            casc.ServiceChannelCD,
            NULL

      FROM  dbo.ufnUtilityGetClaimEntityList(@LynxID, 1, 0) cel   -- Get All exposures
      LEFT JOIN dbo.utb_claim_aspect ca ON (cel.ClaimAspectID = ca.ClaimAspectID)
      Left Outer join   utb_Claim c
      on           c.lynxid = ca.Lynxid
      Left Outer join   utb_Client_Coverage_Type cct
      on           c.InsuranceCompanyID = cct.InsuranceCompanyID
      and           cct.ClientCoverageTypeID = ca.ClientCoverageTypeID
      and          cct.CoverageProfileCD = ca.CoverageProfileCD
      Left Outer join   utb_Claim_Coverage cc
      on           cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
      and          c.LynxID = cc.LynxID
      and          cc.CoverageTypeCD = cct.CoverageProfileCD
      and          cc.AddtlCoverageFlag = 0 -- Exclude the additional coverages
      left outer join utb_Claim_Aspect_Service_Channel casc
      on    casc.ClaimAspectID = ca.ClaimAspectID
      left outer join dbo.ufnUtilityGetReferenceCodes('utb_Claim_Aspect_Service_Channel', 'ServiceChannelCD') fn
      on fn.code = casc.ServiceChannelCD

      where ca.ClaimAspectTypeID = 9 --for vehicle


    ORDER BY ListName,  EstimateTypeFlag DESC, DisplayOrder, Name

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END

    -- Shop pricing data as of assignmentdate
    DECLARE @tmpShopPricing TABLE (HourlyRateMechanical     money   NULL,
                                   HourlyRateRefinishing    money   NULL,
                                   HourlyRateSheetMetal     money   NULL,
                                   HourlyRateUnibodyFrame   money   NULL)


    -- Grab some info needed to locate the shop pricing data
    -- Use the document's AssignmentID if we have it.  Which we should.

    IF @DocumentID > 0
    BEGIN
        SELECT @ShopLocationID = a.ShopLocationID,
               @AssignmentDate = a.AssignmentDate
        FROM dbo.utb_assignment a
        INNER JOIN utb_document d
            ON d.AssignmentID = a.AssignmentID
        WHERE d.DocumentID = @DocumentID
        AND a.ShopLocationID IS NOT NULL -- changed by Venu
          AND  a.AssignmentDate IS NOT NULL           
            AND a.CancellationDate IS NULL
    END

    -- If we dont have the ShopLocationID but we have a ClaimAspectServiceChannelID,
    -- then attempt to use the latter to get the shop pricing.  This would
    -- be applicable for when a shop is selected but no assignment has been sent.

    IF ( @ShopLocationID IS NULL OR @ShopLocationID = 0 )
        AND ( NOT ( @ClaimAspectServiceChannelID IS NULL ) )
        AND ( @ClaimAspectServiceChannelID > 0 )
    BEGIN
        SELECT @ShopLocationID = a.ShopLocationID,
               @AssignmentDate = a.AssignmentDate
        FROM dbo.utb_assignment a
        WHERE a.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
            AND a.CancellationDate IS NULL
              AND a.ShopLocationID IS NOT NULL -- changed by Venu
    END

    -- Now fall back to the original method - usign the ClaimAspectID.

    IF ( @ShopLocationID IS NULL OR @ShopLocationID = 0 )
        AND ( ( @ClaimAspectServiceChannelID IS NULL )
        OR ( @ClaimAspectServiceChannelID = 0 ) )
    BEGIN
        SELECT @ShopLocationID = a.ShopLocationID,
               @AssignmentDate = a.AssignmentDate
        FROM dbo.utb_assignment a
        INNER JOIN utb_Claim_Aspect_Service_Channel casc
            ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        WHERE casc.ClaimAspectID = @ClaimAspectID
            AND a.CancellationDate IS NULL
              AND a.ShopLocationID IS NOT NULL -- changed by Venu
    END


    IF @@ERROR <> 0
    BEGIN

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Grap the shop pricing data
    IF EXISTS(SELECT ShopLocationID FROM dbo.utb_shop_location_pricing_history WHERE ShopLocationID = @ShopLocationID)
    BEGIN
      INSERT INTO @tmpShopPricing
      SELECT TOP 1 HourlyRateMechanical,
                   HourlyRateRefinishing,
                   HourlyRateSheetMetal,
                   HourlyRateUnibodyFrame
      FROM dbo.utb_shop_location_pricing_history
      WHERE ShopLocationID = @ShopLocationID
        AND SysLastUpdatedDate < @AssignmentDate
      ORDER BY SysLastUpdatedDate desc
    END
    ELSE
    BEGIN
      INSERT INTO @tmpShopPricing
      SELECT HourlyRateMechanical,
             HourlyRateRefinishing,
             HourlyRateSheetMetal,
             HourlyRateUnibodyFrame
      FROM dbo.utb_shop_location_pricing
      WHERE ShopLocationID = @ShopLocationID
    END

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpShopPricing', 16, 1, @ProcName)
        RETURN
    END

    SET @PricingAvailable = (SELECT COUNT(*) FROM @tmpShopPricing)

    IF @@ERROR <> 0
    BEGIN

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ClaimAspectStatus = cas.StatusID
    FROM dbo.utb_claim_aspect ca
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added utb_Claim_Aspect_Status to get to a ClaimAspectID row
		M.A. 20061109
	*********************************************************************************/
	INNER JOIN utb_Claim_Aspect_Status cas ON cas.ClaimAspectID = ca.ClaimAspectID
    WHERE ca.ClaimAspectID = @ClaimAspectID

    IF NOT EXISTS(SELECT *
                    FROM utb_status
                    WHERE StatusID = @ClaimAspectStatus
                      AND Name IN ('Vehicle Closed', 'Vehicle Cancelled', 'Vehicle Voided'))
    BEGIN
        -- Claim Aspect is active. Now see if there is an active assignment.

        SET @ActiveAssignment = (SELECT count(*)
                                    FROM dbo.utb_assignment a
					/*********************************************************************************
					Project: 210474 APD - Enhancements to support multiple concurrent service channels
					Note:	Added utb_Claim_Aspect_Service_Channel to get to a ClaimAspectID row
						M.A. 20061109
					*********************************************************************************/
					INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                                    WHERE casc.ClaimAspectID = @ClaimAspectID AND a.assignmentsequencenumber = 1
                                      AND a.CancellationDate IS NULL)

        -- Now get the program shop status of the most recent assignment
        SELECT @ProgramShopAssignment = ProgramFlag
        FROM dbo.utb_shop_location
        WHERE ShopLocationID = (SELECT top 1 ShopLocationID
                                FROM utb_assignment a
				/*********************************************************************************
				Project: 210474 APD - Enhancements to support multiple concurrent service channels
				Note:	Added utb_Claim_Aspect_Service_Channel to get to a ClaimAspectID row
					M.A. 20061109
				*********************************************************************************/
				INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                                WHERE casc.ClaimAspectID = @ClaimAspectID
                                  AND a.CancellationDate IS NULL
                                ORDER BY a.AssignmentDate DESC)
    END

    -- Check if a reinspection was completed
    SET @ReinspectionCompleted = 0
    SELECT @ReinspectionCompleted = isNull(LockedFlag, 0),
           @ReinspectionPerformedBy = isNull(u.NameLast, '') + ', ' + isNull(u.NameFirst, ''),
           @ReinspectionDate = ReinspectionDate
    FROM dbo.utb_reinspect r
    LEFT JOIN dbo.utb_user u ON (r.ReinspectorUserID = u.UserID)
    WHERE r.ClaimAspectID = @ClaimAspectID
      AND r.EstimateDocumentID = @DocumentID

    SELECT @EarlyBillFlag = i.EarlyBillFlag
    FROM utb_claim c 
    LEFT JOIN utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE c.LynxID = @LynxID

    IF @UserID IS NOT NULL
    BEGIN
      SELECT @SupervisorFlag = SupervisorFlag
      FROM dbo.utb_user
      WHERE UserID = @UserID
    END
    
    -- Count the number of approved estimates besides this one
    IF @DocumentID > 0
    BEGIN
       SELECT @ApprovedDocumentCount = COUNT(d.DocumentID)
       FROM utb_document d 
       LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
       LEFT JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
       WHERE cascd.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
         AND d.EnabledFlag = 1
         AND dt.EstimateTypeFlag = 1
         AND d.ApprovedFlag = 1
         AND d.DocumentID <> @DocumentID
    END

    SET @WarrantyExistsFlag = 0
    IF EXISTS(SELECT wa.AssignmentID
               FROM utb_warranty_assignment wa
               LEFT JOIN utb_claim_aspect_service_channel casc  ON wa.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
               WHERE (ca.ClaimAspectID = @ClaimAspectID OR ca.LynxID = @LynxID)
                AND wa.CancellationDate IS NULL)
    BEGIN
      SET @WarrantyExistsFlag = 1
    END
    
    -- Final Select

    SELECT  1    AS Tag,
            NULL AS Parent,
            @DocumentID AS [Root!1!DocumentID],
            @ClaimAspectID AS [Root!1!ClaimAspectID],
            @ClaimAspectServiceChannelID AS [Root!1!ClaimAspectServiceChannelID],
            @PricingAvailable AS [Root!1!PricingAvailable],
            @UserID AS [Root!1!UserID],
            @UserIsSupervisor AS [Root!1!UserIsSupervisor],
            @ServiceChannelCD AS [Root!1!ServiceChannelCD],
            @EarlyBillFlag as [Root!1!EarlyBillFlag],
            @SupervisorFlag as [Root!1!SupervisorFlag],
            @ApprovedDocumentCount as [Root!1!ApprovedDocumentCount],
            @WarrantyExistsFlag as [Root!1!WarrantyExistsFlag],
            -- Estimate Detail
            NULL AS [Estimate!2!DocumentID],
            NULL AS [Estimate!2!DocumentTypeID],
            NULL AS [Estimate!2!SupplementSeqNumber],
            NULL AS [Estimate!2!DocumentVANSourceFlag],
            NULL AS [Estimate!2!DuplicateFlag],
            NULL AS [Estimate!2!EstimateTypeCD],
            NULL AS [Estimate!2!RepairTotalAmt],
            NULL AS [Estimate!2!DeductibleAmt],
            NULL AS [Estimate!2!BettermentAmt],
            NULL AS [Estimate!2!OtherAdjustmentAmt],
            NULL AS [Estimate!2!NetTotalAmt],
            NULL AS [Estimate!2!TaxTotalAmt],
            NULL AS [Estimate!2!AgreedPriceMetCD],
            NULL AS [Estimate!2!ShopContacted],
            NULL AS [Estimate!2!ShopContactName],
            NULL AS [Estimate!2!PrimaryReasonCode],
            NULL AS [Estimate!2!PrimaryReasonCodeText],
            NULL AS [Estimate!2!SecondaryReasonCode],
            NULL AS [Estimate!2!SecondaryReasonCodeText],
            NULL AS [Estimate!2!FullSummaryExistsFlag],
            NULL AS [Estimate!2!ActiveAssignment],
            NULL AS [Estimate!2!ProgramShopAssignment],
            NULL AS [Estimate!2!ReinspectionRequestFlag],
            NULL AS [Estimate!2!ReinspectionCompleted],
            NULL AS [Estimate!2!ReinspectionPerformedBy],
            NULL AS [Estimate!2!ReinspectionDate],
            NULL AS [Estimate!2!EstimateLockedFlag],
            NULL AS [Estimate!2!ApprovedFlag],
            NULL AS [Estimate!2!ApprovedDate],
            NULL AS [Estimate!2!WarrantyFlag],
            NULL AS [Estimate!2!BilledFlag],
                      -- Shop Pricing
            NULL AS [ShopPricing!3!Mechanical],
            NULL AS [ShopPricing!3!Refinishing],
            NULL AS [ShopPricing!3!SheetMetal],
            NULL AS [ShopPricing!3!UnibodyFrame],
            -- Metadata Header
            NULL AS [Metadata!4!Entity],
            -- Columns
            NULL AS [Column!5!Name],
            NULL AS [Column!5!DataType],
            NULL AS [Column!5!MaxLength],
            NULL AS [Column!5!Precision],
            NULL AS [Column!5!Scale],
            NULL AS [Column!5!Nullable],
            -- Reference Data
            NULL AS [Reference!6!List],
            NULL AS [Reference!6!ReferenceID],
            NULL AS [Reference!6!Name],
            NULL AS [Reference!6!EstimateTypeFlag],
            NULL AS [Reference!6!ClaimAspectID],
            NULL AS [Reference!6!DeductibleAmt],
            NULL AS [Reference!6!ShowAudited],
            NULL AS [Reference!6!ServiceChannelCD],
            NULL AS [Reference!6!ApprovedDocumentCount]


    UNION ALL


    SELECT  2,
            1,
            NULL, NULL, NULL, NULL,
            @UserID,
            isNull(@UserIsSupervisor, 0),
            NULL, NULL, NULL, NULL, NULL,
            -- Estimate Detail
            d.DocumentID,
            IsNull(d.DocumentTypeID, 0),
            IsNull(d.SupplementSeqNumber, ''),
            IsNull(ds.VANFlag, 0),
            IsNull(d.DuplicateFlag, ''),
            IsNull(d.EstimateTypeCD, ''),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 27 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 28 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 30 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 39 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 32 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull((SELECT  IsNull(Convert(varchar(12), es.OriginalExtendedAmt),'')
               FROM  dbo.utb_document d
               LEFT JOIN dbo.utb_estimate_summary es ON (d.DocumentID = es.DocumentID AND 25 = es.EstimateSummaryTypeID)
               WHERE d.DocumentID = @DocumentID), 0),
            IsNull(d.AgreedPriceMetCD, ''),
            IsNull(d.ShopContacted, ''),  
            IsNull(d.ShopContactName, ''),
            IsNull(d.PrimaryReasonCode, ''),
            IsNull(d.PrimaryReasonCodeText, ''),
            IsNull(d.SecondaryReasonCode, ''),
            IsNull(d.SecondaryReasonCodeText, ''),          
            IsNull(d.FullSummaryExistsFlag, 0),
            IsNull(@ActiveAssignment, 0),
            IsNull(@ProgramShopAssignment, 0),
            d.ReinspectionRequestFlag,
            @ReinspectionCompleted,
            @ReinspectionPerformedBy,
            @ReinspectionDate,
            IsNull(d.EstimateLockedFlag, 0),
            d.ApprovedFlag, 
            d.ApprovedDate, 
            d.WarrantyFlag,
            isNull((SELECT CASE 
                              WHEN i.StatusCD = 'APD' OR 
                                   i.StatusCD = 'AC' OR
                                   i.StatusCD = 'NB' THEN 0
                              ELSE 1
                          END
                     FROM utb_invoice i
                     WHERE i.DocumentID = d.DocumentID
                       AND i.EnabledFlag = 1), 0),
            -- Shop Pricing
            NULL, NULL, NULL, NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
      FROM  (SELECT @DocumentID AS DocumentID) AS parms
      LEFT JOIN dbo.utb_document d ON (parms.DocumentID = d.DocumentID)
      LEFT JOIN dbo.utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)


    UNION ALL

    -- Select Shop Pricing Level

    SELECT DISTINCT 3,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Estimate Detail
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Pricing
            ISNULL(convert(varchar(15), HourlyRateMechanical), ''),
            ISNULL(convert(varchar(15), HourlyRateRefinishing), ''),
            ISNULL(convert(varchar(15), HourlyRateSheetMetal), ''),
            ISNULL(convert(varchar(15), HourlyRateUnibodyFrame), ''),

            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpShopPricing


    UNION ALL


    -- Select Metadata Header Level

    SELECT DISTINCT 4,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Estimate Detail
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Pricing
            NULL, NULL, NULL, NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT  5,
            4,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Estimate Detail
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Pricing
            NULL, NULL, NULL, NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM  @tmpMetadata


    UNION ALL


    -- Select Reference Data Level

    SELECT  6,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Estimate Detail
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Pricing
            NULL, NULL, NULL, NULL,
            -- Metadata Header
            'ZZ-Reference',
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
            EstimateTypeFlag,
            ClaimAspectID,
            DeductibleAmt,
            ShowAudited,
            ServiceChannelCD,
            ApprovedDocumentCount

    FROM    @tmpReference


    ORDER BY [Metadata!4!Entity], Tag
--    FOR XML EXPLICIT                  -- (Comment out for Client-side XML generation)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

/****** Object:  StoredProcedure [dbo].[uspEstimateQuickUpdDetail]    Script Date: 08/07/2012 12:27:33 ******/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[uspEstimateQuickUpdDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[uspEstimateQuickUpdDetail]
GO

/****** Object:  StoredProcedure [dbo].[uspEstimateQuickUpdDetail]    Script Date: 08/07/2012 12:27:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateQuickUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates the estimate data in utb_document and utb_estimate_summary
*
* PARAMETERS:
* (I) @DocumentID           The DocumentID to update
* (I) @ClaimAspectID        The claim aspect id
* (I) @AdjustmentTotalAmt   The total adjustment amount
* (I) @DeductibleAmt        The amount of the deductible
* (I) @DocumentTypeID       The document type id
* (I) @DuplicateFlag        Indicates whether the estimate is a duplicate
* (I) @EstimateTypeCD       Indicates whether the estimate is (O)riginal or (A)udited
* (I) @NetTotalAmt          The Net total of the estimate (counting adjustments)
* (I) @OtherAdjustmentAmt   The amount of other adjustments
* (I) @RepairTotalAmt       The repair total of the estimate without applied adjustments
* (l) @BettermentTotalAmt   The betterment amount
* (I) @SupplementSeqNumber  The supplement sequence number
* (I) @AgreedPriceMetCD     Was the price agreed to by the shop
* (I) @UserID               UserID performing the update
*
* New Parameters are Added By Glsd451
* (I) @ShopContacted		Indicates Whether the shopcontacted is Yes or No
* (I) @ShopContactName			Name of the ShopContacted
* (I) @PrimaryReasonCode	Values of PrimaryReasonCode Checkbox
* (I) @PrimaryReasonCodeText Values for PrimaryReasonCode Textbox
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspEstimateQuickUpdDetail]
    @DocumentID             udt_std_id_big,
    @ClaimAspectID          udt_std_id_big,
    @AdjustmentTotalAmt     decimal(9,2),
    @DeductibleAmt          decimal(9,2),
    @DocumentTypeID         udt_std_id,
    @DuplicateFlag          udt_std_flag,
    @EstimateTypeCD         udt_std_cd,
    @NetTotalAmt            decimal(9,2),
    @OtherAdjustmentAmt     decimal(9,2),
    @RepairTotalAmt         decimal(9,2),
    @BettermentTotalAmt     decimal(9,2),
    @SupplementSeqNumber    udt_std_int_tiny,
    @AgreedPriceMetCD       udt_std_cd,
    @TaxTotalAmt            decimal(9, 2),
    @ApprovedFlag           udt_std_flag,
    @WarrantyFlag           udt_std_flag,
    @DocumentEditFlag       udt_std_flag,
    @UserID                 udt_std_id,

-- New Parameters added by Glsd451
	@ShopContacted			udt_std_flag,
	@ShopContactName		udt_std_desc_mid,
	@PrimaryReasonCode		udt_std_note,
	@PrimaryReasonCodeText  udt_std_note,
	@SecondaryReasonCode	udt_std_note,
	@SecondaryReasonCodeText udt_std_note
AS	
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@EstimateTypeCD))) = 0 SET @EstimateTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@AgreedPriceMetCD))) = 0 SET @AgreedPriceMetCD = NULL


    -- Declare internal variables

    DECLARE @DocumentVANSourceFlag                  udt_std_flag
    DECLARE @DateLastUpdatedAdjustmentTotal         varchar(30)
    DECLARE @DateLastUpdatedContractPrice           varchar(30)
    DECLARE @DateLastUpdatedDeductible              varchar(30)
    DECLARE @DateLastUpdatedNetTotal                varchar(30)
    DECLARE @DateLastUpdatedOtherAdjustment         varchar(30)
    DECLARE @DateLastUpdatedRepairTotal             varchar(30)
    DECLARE @DateLastUpdatedBettermentTotal         varchar(30)
    DECLARE @DateLastUpdatedDeductiblesAppliedTotal varchar(30)
    DECLARE @DateLastUpdatedLimitEffectTotal        varchar(30)
    DECLARE @DateLastUpdatedNetTotalEffectTotal     varchar(30)
    DECLARE @DateLastUpdatedTaxTotal                varchar(30)
    
    DECLARE @EstimateSummaryIDAdjustmentTotal       udt_std_id
    DECLARE @EstimateSummaryIDContractPrice         udt_std_id
    DECLARE @EstimateSummaryIDDeductible            udt_std_id
    DECLARE @EstimateSummaryIDNetTotal              udt_std_id
    DECLARE @EstimateSummaryIDOtherAdjustment       udt_std_id
    DECLARE @EstimateSummaryIDRepairTotal           udt_std_id
    DECLARE @EstimateSummaryIDBetterment            udt_std_id
    DECLARE @EstimateSummaryIDDeductiblesApplied    udt_std_id
    DECLARE @EstimateSummaryIDLimitEffect           udt_std_id
    DECLARE @EstimateSummaryIDNetTotalEffect        udt_std_id
    DECLARE @EstimateSummaryIDTaxTotal              udt_std_id
    
    DECLARE @LimitEffectAmt                         udt_std_money
    DECLARE @NetTotalEffectAmt                      udt_std_money
    
    DECLARE @ClaimAspectServiceChannelID            bigint
    DECLARE @ClaimCoverageID                        bigint
    DECLARE @ExposureCD                             varchar(4)
    DECLARE @EventID                                int
    DECLARE @EventName                              varchar(50)
    DECLARE @PHNotificationDocumentTypeID           int

    DECLARE @error AS int
    DECLARE @rowcount AS int

    DECLARE @now               AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspEstimateQuickUpdDetail'


    -- Set Database options

    SET NOCOUNT ON


    -- Check to make sure a valid Document id was passed in

    IF  (@DocumentID IS NULL) OR
        (NOT EXISTS(SELECT DocumentID FROM dbo.utb_document WHERE DocumentID = @DocumentID))
    BEGIN
        -- Invalid Document ID

        RAISERROR('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
     RETURN
    END


    -- Check to make sure a valid Claim Aspect id was passed in

    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid ClaimAspect ID

        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure the document id belongs to the claim aspect id
    --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    IF  (NOT EXISTS(
                    SELECT ClaimAspectID 
                    FROM dbo.utb_claim_aspect_service_channel_document  cascd
                    INNER JOIN    utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                    WHERE DocumentID = @DocumentID 
                    AND ClaimAspectID = @ClaimAspectID
                   )
        )
    --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    BEGIN
        -- Invalid ClaimAspect ID for the Document id

        RAISERROR('%s|Claim Aspect ID invalid for Document', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure EstimateTypeCD is valid

    IF (@EstimateTypeCD IS NOT NULL) AND
       (@EstimateTypeCD NOT IN (SELECT code FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'EstimateTypeCD')))
    BEGIN
        -- Invalid Estimate Type CD

        RAISERROR('101|%s|@EstimateTypeCD|%s', 16, 1, @ProcName, @EstimateTypeCD)
        RETURN
    END


    -- Check to make sure AgreedPriceMetCD is valid

    IF (@AgreedPriceMetCD IS NOT NULL) AND
       (@AgreedPriceMetCD NOT IN (SELECT code FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'AgreedPriceMetCD')))
    BEGIN
        -- Invalid Agreed Price Met CD

        RAISERROR('101|%s|@AgreedPriceMetCD|%s', 16, 1, @ProcName, @AgreedPriceMetCD)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Validate APD Date State

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'AdjustmentTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"AdjustmentTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'CP' AND Name = 'ContractPrice')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"ContractPrice"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'AJ' AND Name = 'Deductible')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"Deductible"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'NetTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"NetTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'AJ' AND Name = 'Other')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"Other (Other Adjustment)"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'RepairTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'AJ' AND Name = 'Betterment')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"Betterment"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'OT' AND Name = 'DeductiblesApplied')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"DeductiblesApplied"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'OT' AND Name = 'LimitEffect')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"LimitEffect"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'OT' AND Name = 'NetTotalEffect')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"NetTotalEffect"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'TaxTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"TaxTotal"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF @ApprovedFlag = 1 AND @AgreedPriceMetCD = 'N'
    BEGIN
      -- Cannot set Approved Flag to Yes for an estimate that has Agreed Price Met as No
      SET @ApprovedFlag = 0
    END
    
    
   
    

    -- Check to see if we're updating a VAN sourced document.  If so, we'll ignore the amounts
    -- (the user shouldn't have edited those anyway)

    SELECT @DocumentVANSourceFlag = ds.VANFlag
      FROM dbo.utb_document d
      LEFT JOIN dbo.utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
      WHERE DocumentID = @DocumentID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    FROM utb_document d
    LEFT JOIN utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
    WHERE d.DocumentID = @DocumentID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Get the claim coverage based on the vehicle coverage.
    SELECT @ClaimCoverageID = cc.ClaimCoverageID,
           @ExposureCD = ExposureCD
    FROM dbo.utb_claim_aspect ca 
    LEFT JOIN dbo.utb_claim_coverage cc ON ca.LynxID = cc.LynxID and ca.CoverageProfileCD = cc.CoverageTypeCD
    WHERE ca.ClaimAspectID = @ClaimAspectID
      AND cc.EnabledFlag = 1
      AND cc.AddtlCoverageFlag = 0

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
   
    SELECT @ExposureCD = ExposureCD
    FROM dbo.utb_claim_aspect ca 
    WHERE ca.ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @DocumentVANSourceFlag = 0
    BEGIN
        -- Initialize all the estimate summary ids to 0

        SET @EstimateSummaryIDAdjustmentTotal = 0
        SET @EstimateSummaryIDContractPrice = 0
        SET @EstimateSummaryIDDeductible = 0
        SET @EstimateSummaryIDNetTotal = 0
        SET @EstimateSummaryIDOtherAdjustment = 0
        SET @EstimateSummaryIDRepairTotal = 0
        SET @EstimateSummaryIDBetterment = 0
        SET @EstimateSummaryIDTaxTotal = 0
        
        SET @EstimateSummaryIDDeductiblesApplied = 0
        SET @EstimateSummaryIDLimitEffect = 0
        SET @EstimateSummaryIDNetTotalEffect = 0
        -- Get the ids for any existing estimate summary records

        SELECT  @EstimateSummaryIDAdjustmentTotal = EstimateSummaryID,
                @DateLastUpdatedAdjustmentTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'AdjustmentTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDContractPrice = EstimateSummaryID,
                @DateLastUpdatedContractPrice = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'CP'
            AND est.Name = 'ContractPrice'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDDeductible = EstimateSummaryID,
                @DateLastUpdatedDeductible = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'AJ'
            AND est.Name = 'Deductible'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDNetTotal = EstimateSummaryID,
                @DateLastUpdatedNetTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'NetTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDOtherAdjustment = EstimateSummaryID,
                @DateLastUpdatedOtherAdjustment = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'AJ'
            AND est.Name = 'Other'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDRepairTotal = EstimateSummaryID,
                @DateLastUpdatedRepairTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'RepairTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        SELECT  @EstimateSummaryIDTaxTotal = EstimateSummaryID,
                @DateLastUpdatedTaxTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'TaxTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        
        SELECT  @EstimateSummaryIDBetterment = EstimateSummaryID,
                @DateLastUpdatedBettermentTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'AJ'
            AND est.Name = 'Betterment'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SELECT  @EstimateSummaryIDDeductiblesApplied = EstimateSummaryID,
                @DateLastUpdatedDeductiblesAppliedTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'OT'
            AND est.Name = 'DeductiblesApplied'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SELECT  @EstimateSummaryIDLimitEffect = EstimateSummaryID,
                @LimitEffectAmt = OriginalExtendedAmt,
                @DateLastUpdatedLimitEffectTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'OT'
            AND est.Name = 'LimitEffect'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SELECT  @EstimateSummaryIDNetTotalEffect = EstimateSummaryID,
                @DateLastUpdatedNetTotalEffectTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'OT'
            AND est.Name = 'NetTotalEffect'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SET @NetTotalEffectAmt = isNull(@RepairTotalAmt, 0) - isNull(@DeductibleAmt, 0) - isNull(@LimitEffectAmt, 0)
        
    END


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Begin Update

    BEGIN TRANSACTION DocumentEstimateUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

        -- Perform an update

        UPDATE  dbo.utb_document
           SET  DocumentTypeID          = @DocumentTypeID,
                DuplicateFlag           = @DuplicateFlag,
                EstimateTypeCD          = @EstimateTypeCD,
                EstimateReviewDate      = @now,
                AgreedPriceMetCD        = @AgreedPriceMetCD,
                SupplementSeqNumber     = @SupplementSeqNumber,
                WarrantyFlag            = @WarrantyFlag,
                SysLastUserID           = @UserID,
                SysLastUpdatedDate      = @now,
				ShopContacted			= @ShopContacted,
				ShopContactName			= @ShopContactName,			
				PrimaryReasonCode		= @PrimaryReasonCode,
				PrimaryReasonCodeText   = @PrimaryReasonCodeText,
				SecondaryReasonCode		= @SecondaryReasonCode,
				SecondaryReasonCodeText = @SecondaryReasonCodeText 
        WHERE DocumentID = @DocumentID

        SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

        -- Check error value

        IF @error <> 0
        BEGIN
            -- Update failed

            RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END



    IF @DocumentVANSourceFlag = 0
    BEGIN
        -- Let's update the estimate summary

        -- First, delete all existing estimate summary records that we're not going to change

        DELETE FROM dbo.utb_estimate_summary
          WHERE DocumentID = @DocumentID
            AND EstimateSummaryID NOT IN (@EstimateSummaryIDAdjustmentTotal,
                                          @EstimateSummaryIDContractPrice,
                                          @EstimateSummaryIDDeductible,
                                          @EstimateSummaryIDNetTotal,
                                          @EstimateSummaryIDOtherAdjustment,
                                          @EstimateSummaryIDRepairTotal,
                                          @EstimateSummaryIDBetterment,
                                          @EstimateSummaryIDDeductiblesApplied,
                                          @EstimateSummaryIDLimitEffect,
                                          @EstimateSummaryIDNetTotalEffect,
                                          @EstimateSummaryIDTaxTotal)

        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('106|%s|utb_estimate_summary', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDContractPrice,
                                              @EstimateSummaryType      = 'ContractPrice',
                                              @EstimateSummaryTypeCD    = 'CP',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @RepairTotalAmt,
                                              @AgreedUnitAmt            = @RepairTotalAmt,
                                              @OriginalExtendedAmt      = @RepairTotalAmt,
                                              @OriginalUnitAmt          = @RepairTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedContractPrice,
                                              @ReturnSysLastUpdate      = 0

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Contract Price)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDRepairTotal,
                                              @EstimateSummaryType      = 'RepairTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @RepairTotalAmt,
                                              @AgreedUnitAmt            = @RepairTotalAmt,
                                              @OriginalExtendedAmt      = @RepairTotalAmt,
                                              @OriginalUnitAmt          = @RepairTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedRepairTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Repair Total)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDDeductible,
                                              @EstimateSummaryType      = 'Deductible',
                                              @EstimateSummaryTypeCD    = 'AJ',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @DeductibleAmt,
                                              @AgreedUnitAmt            = @DeductibleAmt,
                                              @OriginalExtendedAmt      = @DeductibleAmt,
                                              @OriginalUnitAmt          = @DeductibleAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedDeductible,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Deductible)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDOtherAdjustment,
                                              @EstimateSummaryType      = 'Other',
                                              @EstimateSummaryTypeCD    = 'AJ',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @OtherAdjustmentAmt,
                                              @AgreedUnitAmt            = @OtherAdjustmentAmt,
                                              @OriginalExtendedAmt      = @OtherAdjustmentAmt,
                                              @OriginalUnitAmt          = @OtherAdjustmentAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedOtherAdjustment,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDAdjustmentTotal,
                                              @EstimateSummaryType      = 'AdjustmentTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @AdjustmentTotalAmt,
                                              @AgreedUnitAmt            = @AdjustmentTotalAmt,
                                              @OriginalExtendedAmt      = @AdjustmentTotalAmt,
                                              @OriginalUnitAmt          = @AdjustmentTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedAdjustmentTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDTaxTotal,
                                              @EstimateSummaryType      = 'TaxTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @TaxTotalAmt,
                                              @AgreedUnitAmt            = @TaxTotalAmt,
                                              @OriginalExtendedAmt      = @TaxTotalAmt,
                                              @OriginalUnitAmt          = @TaxTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedTaxTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDNetTotal,
                                              @EstimateSummaryType      = 'NetTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @NetTotalAmt,
                                              @AgreedUnitAmt            = @NetTotalAmt,
                                              @OriginalExtendedAmt      = @NetTotalAmt,
                                              @OriginalUnitAmt          = @NetTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedNetTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDBetterment,
                                              @EstimateSummaryType      = 'Betterment',
                                              @EstimateSummaryTypeCD    = 'AJ',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @BettermentTotalAmt,
                                              @AgreedUnitAmt            = @BettermentTotalAmt,
                                              @OriginalExtendedAmt      = @BettermentTotalAmt,
                                              @OriginalUnitAmt          = @BettermentTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedBettermentTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDDeductiblesApplied,
                                              @EstimateSummaryType      = 'DeductiblesApplied',
                                              @EstimateSummaryTypeCD    = 'OT',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @DeductibleAmt,
                                              @AgreedUnitAmt            = @DeductibleAmt,
                                              @OriginalExtendedAmt      = @DeductibleAmt,
                                              @OriginalUnitAmt          = @DeductibleAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedDeductiblesAppliedTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (DeductiblesApplied)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDLimitEffect,
                                              @EstimateSummaryType      = 'LimitEffect',
                                              @EstimateSummaryTypeCD    = 'OT',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @LimitEffectAmt,
                                              @AgreedUnitAmt            = @LimitEffectAmt,
                                              @OriginalExtendedAmt      = @LimitEffectAmt,
                                              @OriginalUnitAmt          = @LimitEffectAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedLimitEffectTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (LimitEffect)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDNetTotalEffect,
                                              @EstimateSummaryType      = 'NetTotalEffect',
                                              @EstimateSummaryTypeCD    = 'OT',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @NetTotalEffectAmt,
                                              @AgreedUnitAmt            = @NetTotalEffectAmt,
                                              @OriginalExtendedAmt      = @NetTotalEffectAmt,
                                              @OriginalUnitAmt          = @NetTotalEffectAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedNetTotalEffectTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (NetTotalEffect)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        
        IF @ClaimCoverageID IS NOT NULL AND @ClaimAspectServiceChannelID IS NOT NULL
        BEGIN
           -- Now check if the deductible applied record exist for the claim aspect service channel.
           IF EXISTS (SELECT ClaimAspectServiceChannelID
                      FROM dbo.utb_claim_aspect_service_channel_coverage
                      WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                        AND ClaimCoverageID = @ClaimCoverageID)
           BEGIN
               -- Deductible applied record exist. Update the deductible applied
               UPDATE dbo.utb_claim_aspect_service_channel_coverage
               SET DeductibleAppliedAmt = @DeductibleAmt
               WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                 AND ClaimCoverageID = @ClaimCoverageID
   
               IF @error <> 0
               BEGIN
                   -- Update failed
       
                   RAISERROR('104|%s|utb_claim_aspect_service_channel_coverage', 16, 1, @ProcName)
                   ROLLBACK TRANSACTION
                   RETURN
               END
           END
           ELSE
           BEGIN
               -- Deductible applied record exist. Update the deductible applied
               INSERT INTO dbo.utb_claim_aspect_service_channel_coverage
                  (ClaimAspectServiceChannelID, 
                   ClaimCoverageID, 
                   DeductibleAppliedAmt, 
                   SysLastUserID, 
                   SysLastUpdatedDate)
               VALUES
                  (@ClaimAspectServiceChannelID, 
                   @ClaimCoverageID, 
                   @DeductibleAmt, 
                   @UserID, 
                   @now)
   
               IF @error <> 0
               BEGIN
                   -- Insert failed
       
                   RAISERROR('105|%s|utb_claim_aspect_service_channel_coverage', 16, 1, @ProcName)
                   ROLLBACK TRANSACTION
                   RETURN
               END
           END
        END

        IF @ExposureCD = '1' AND @EstimateTypeCD = 'A' AND @AgreedPriceMetCD = 'Y'
        BEGIN
            -- Adding an price agreed audited estimate for a 1st party vehicle.
            
            -- Check if the PH Notifcation document exists
            SELECT @PHNotificationDocumentTypeID = DocumentTypeID
            FROM utb_document_type
            WHERE Name = 'PH Notification'

            IF NOT EXISTS(SELECT d.DocumentID
                           FROM utb_document d
                           LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
                           LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                           WHERE d.DocumentTypeID = @PHNotificationDocumentTypeID 
                             AND casc.ClaimAspectID = @ClaimAspectID
                             AND d.EnabledFlag = 1)
            BEGIN
               -- Raise the PH Notofication event
               SET @EventName = 'First Party Audited Estimate'

               SELECT  @EventID = EventID 
               FROM  utb_event 
               WHERE Name = @EventName

               IF @@ERROR <> 0
               BEGIN
                  -- SQL Server Error

                  RAISERROR('99|%s', 16, 1, @ProcName)
                  RETURN
               END              

               IF @EventID IS NULL
               BEGIN
                  -- Event Name not found

                  RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
                  ROLLBACK TRANSACTION
                  RETURN
               END


               -- Make the workflow call

               EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                           @ClaimAspectID = @ClaimAspectID,
                                           @Description = 'Audited Estimate with agreed price added to a First Party vehicle',
                                           @Key = NULL,
                                           @UserID = @UserID,
                                           @ConditionValue = 'DA',
                                           @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

            END
        END
    END


    COMMIT TRANSACTION DocumentEstimateUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    SELECT  1                   AS tag,
            NULL                AS parent,
            @DocumentID         AS [Root!1!DocumentID],
            @Now                AS [Root!1!SysLastUpdatedDate]

--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END
