-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDocumentGetDetailWSXML 
END
GO
/****** Object:  StoredProcedure [dbo].[uspDocumentGetDetailWSXML]    Script Date: 09/23/2014 02:19:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspDocumentGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Retrieves all documents
*
* PARAMETERS:
* (I) @LynxID               The Lynx ID
* (I) @ClaimAspectID        Optional claim aspect to request documents for
* (I) @ClaimAspectID        Optional claim aspect service channel to request documents for
* (I) @DocumentClassCD      The type of documents to return ('A' - All, 'E' - Estimates only, 'O' - Non-estimate documents)
* (I) @InsuranceCompanyID   The Insurance company to validate the request against.
* (I) @UserID               Optional User id to get the routing document information
*
* RESULT SET:
* An XML Data stream containing all documents for the claim
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspDocumentGetDetailWSXML]
    @LynxID                      udt_std_int_big,
    @ClaimAspectID               udt_std_id_big = NULL,
    @ClaimAspectServiceChannelID udt_std_id_big = NULL,
    @DocumentClassCD             char(1) = 'A',
    @InsuranceCompanyID          udt_std_id,
    @UserID                      udt_std_id = NULL  --Project:210474 APD Added the parameter when we did the code merge M.A.20061116
AS
BEGIN
    SET nocount on
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@DocumentClassCD))) = 0 SET @DocumentClassCD = NULL


    -- Declare local variables

    DECLARE @InsuranceCompanyIDClaim        udt_std_id
    DECLARE @InsuranceCompanyNameClaim      varchar(100)
    --Project:210474 APD Added the following 9 variables when we did the code merge M.A.20061116
    DECLARE @UserEmailAddress               varchar(50)
    DECLARE @SupervisorFlag                 bit
    DECLARE @ReturnDocDestinationCD         udt_std_cd
    DECLARE @ReturnDocPackageTypeCD         udt_std_cd
    DECLARE @ReturnDocRoutingCD             udt_std_cd
    DECLARE @ReturnDocDestinationValue      varchar(400) ---Project:210474 APD Modified the data type when we did the code merge M.A.20061215
    DECLARE @ClientClaimNumber              udt_cov_claim_number
    DECLARE @LossDate                       varchar(100)
    DECLARE @InsuredName                    varchar(100)
    DECLARE @ClaimAspectIDClaim             bigint
    --Project:210474 APD Added the five DECLARE below when we did the code merge M.A.20061215
    DECLARE @UserName                       varchar(100)
    DECLARE @VehicleNum                     tinyint
    DECLARE @VehicleMake                    varchar(100)
    DECLARE @VehicleYear                    varchar(5)
    DECLARE @VehicleModel                   varchar(100)
    DECLARE @SourceApplicationIDCCAV		tinyInt
    DECLARE @SourceApplicationIDWS			tinyInt
    DECLARE @ClientAdjusterName     		varchar(100)
    DECLARE @ClientAdjusterFax     		    varchar(50)
    DECLARE @ClientAdjusterEmail  		    varchar(100)
    DECLARE @ClientAdjusterActiveFlag       tinyint
    DECLARE @ClaimAspectTypeIDVehicle       int
    DECLARE @LossState                      varchar(2)
    DECLARE @ShopState                      varchar(2)
    DECLARE @ClientPolicyNumber             varchar(50)
    DECLARE @EarlyBillFlag                  bit
    DECLARE @WarrantyExistsFlag             bit

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspDocumentGetDetailWSXML'


    -- Check to make sure a valid Lynx ID was passed in

    IF  (@LynxID IS NULL) OR
        ((NOT @LynxID = 0) AND NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID

        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    -- Validate PertainsTo if included

    IF (@ClaimAspectID IS NOT NULL)
    BEGIN
        IF (@ClaimAspectID NOT IN (SELECT ClaimAspectID FROM dbo.ufnUtilityGetClaimEntityList ( @LynxID, 0, 0 )))  -- (0, 0) means all aspects, open or closed
        BEGIN
            -- Invalid Claim Aspect ID

            RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
            RETURN
        END
    END


    -- Validate DocumentClassCD
    IF (@DocumentClassCD NOT IN ('A', 'E', 'O'))
    BEGIN
        -- Invalid @DocumentClassCD

        RAISERROR('101|%s|@DocumentClassCD|%s', 16, 1, @ProcName, @DocumentClassCD)
        RETURN
    END


    -- Get the Insurance Company Id for the claim
    SELECT  @InsuranceCompanyIDClaim = c.InsuranceCompanyID,
            @InsuranceCompanyNameClaim = Name,
            @LossDate = convert(varchar, c.LossDate, 101),     --Project:210474 APD Added the column when we did the code merge M.A.20061116
            @LossState = LTrim(isNull(LossState, '')),
            @EarlyBillFlag = i.EarlyBillFlag
      FROM  dbo.utb_claim c
      LEFT JOIN dbo.utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
      WHERE LynxID = @LynxID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Validate it against what has been passed in

    IF (@InsuranceCompanyIDClaim <> @InsuranceCompanyID)
    BEGIN
        -- Insurance Company ID does not match

        RAISERROR('111|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClaim, @InsuranceCompanyID)
        RETURN
    END


    -- Validate APD data state

    IF NOT EXISTS(SELECT Name FROM dbo.utb_document_type WHERE Name = 'Note')
    BEGIN
       -- Note Document Type Not Found

        RAISERROR('102|%s|"Note"|utb_document_type', 16, 1, @ProcName)
        RETURN
    END


    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'NetTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"NetTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

	SELECT @SourceApplicationIDCCAV = ApplicationID
	FROM dbo.utb_application
	WHERE Code = 'CCAV'

	SELECT @SourceApplicationIDWS = ApplicationID
	FROM dbo.utb_application
	WHERE Code = 'WS'


     IF @@ERROR <> 0
     BEGIN
        -- SQL Server Error

         RAISERROR('99|%s', 16, 1, @ProcName)
         RETURN
     END
    /********************************************************************************
    --Project:210474 APD Added the code below when we did the code merge M.A.20061116
    ********************************************************************************/
    IF @InsuranceCompanyIDClaim IS NOT NULL
    BEGIN
        -- Get the client claim number and adjuster information
        SELECT @ClientClaimNumber = isNull(ClientClaimNumber, ''),
               @ClientAdjusterName = u.NameFirst + ' ' + u.NameLast,
               @ClientAdjusterFax = LTRIM(CASE 
                                            WHEN o.ReturnDocFaxAreaCode IS NOT NULL AND LTRIM(o.ReturnDocFaxAreaCode) <> '' THEN o.ReturnDocFaxAreaCode + o.ReturnDocFaxExchangeNumber + o.ReturnDocFaxUnitNumber
                                            WHEN u.FaxAreaCode IS NOT NULL AND LTRIM(u.FaxAreaCode) <> '' THEN u.FaxAreaCode + u.FaxExchangeNumber + u.FaxUnitNumber
                                            ELSE ''
                                          END),
               @ClientAdjusterEmail = LTRIM(CASE 
                                                WHEN o.ReturnDocEmailAddress IS NOT NULL AND LTRIM(o.ReturnDocEmailAddress) <> '' THEN o.ReturnDocEmailAddress
                                                WHEN u.EmailAddress IS NOT NULL AND LTRIM(u.EmailAddress) <> '' THEN u.EmailAddress
                                                ELSE ''
                                            END),
               @ClientAdjusterActiveFlag = dbo.ufnUtilityIsUserActive(u.UserID, NULL, NULL),
               @ClientPolicyNumber = c.PolicyNumber

        FROM dbo.utb_claim c
        LEFT JOIN dbo.utb_user u ON c.CarrierRepUserID = u.UserID
        LEFT JOIN dbo.utb_office o ON u.OfficeID = o.OfficeID
        WHERE LynxID = @LynxID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        -- Get the return document settings for the insurance
        SELECT @ReturnDocDestinationCD = ISNULL(ReturnDocDestinationCD, ''),
               @ReturnDocPackageTypeCD = ISNULL(ReturnDocPackageTypeCD, 'PDF'),
               @ReturnDocRoutingCD = ISNULL(ReturnDocRoutingCD, '')
        FROM dbo.utb_insurance
        WHERE InsuranceCompanyID = @InsuranceCompanyIDClaim

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        IF @ReturnDocRoutingCD = 'CCAV'
        BEGIN
        		IF @SourceApplicationIDCCAV IS NOT NULL
        		BEGIN
	        		-- Check if the source application must be CCAV to use it. Else use override
	        		IF NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE LynxID = @LynxID AND SourceApplicationID = @SourceApplicationIDCCAV)
	        		BEGIN
	        			-- This LynxID did not come to us via Autoverse. So don't use if for Return docs. Default to override.
	        			SET @ReturnDocRoutingCD = ''
	        		END

        		END
        		ELSE
        		BEGIN
        			-- We don't have the code for CCAV. Default to override.
        			SET @ReturnDocRoutingCD = ''
        		END

	         IF @@ERROR <> 0
	         BEGIN
	            -- SQL Server Error

	             RAISERROR('99|%s', 16, 1, @ProcName)
	             RETURN
	         END

        END

        IF @ReturnDocDestinationCD = 'REP'
        BEGIN
            -- Destination code is Carrier Rep.
            IF @ReturnDocRoutingCD = 'FAX'
            BEGIN
                --Get the Carrier Rep's fax number
                SELECT @ReturnDocDestinationValue = u.FaxAreaCode + u.FaxExchangeNumber + u.FaxUnitNumber
                FROM dbo.utb_user u
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
            ELSE IF @ReturnDocRoutingCD = 'FTP'
            BEGIN
                --Get the Office FTP Destination value
                SELECT @ReturnDocDestinationValue = o.ReturnDocDestinationValue
                FROM dbo.utb_office o
                LEFT JOIN dbo.utb_user u ON (o.OfficeID = u.OfficeID)
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
            ELSE
            BEGIN
                --SET @ReturnDocRoutingCD = 'EML'  ---Project:210474 APD remarked off when we did the code merge M.A.20061215

                --Get the Carrier Rep's email address
                SELECT @ReturnDocDestinationValue = u.EmailAddress
                FROM dbo.utb_user u
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
        END
        ELSE IF @ReturnDocDestinationCD = 'OFC'
        BEGIN
            -- Destination code is Carrier Rep.
            IF @ReturnDocRoutingCD = 'FAX'
            BEGIN
                --Get the Carrier Rep's office fax number
                SELECT @ReturnDocDestinationValue = o.ReturnDocFaxAreaCode + o.ReturnDocFaxExchangeNumber + o.ReturnDocFaxUnitNumber
                FROM dbo.utb_office o
                LEFT JOIN dbo.utb_user u ON (o.OfficeID = u.OfficeID)
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
            ELSE IF @ReturnDocRoutingCD = 'FTP'
            BEGIN
                --Get the Office FTP Destination value
                SELECT @ReturnDocDestinationValue = o.ReturnDocDestinationValue
                FROM dbo.utb_office o
                LEFT JOIN dbo.utb_user u ON (o.OfficeID = u.OfficeID)
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
            ELSE
            BEGIN
                --SET @ReturnDocRoutingCD = 'EML' ---Project:210474 APD remarked off when we did the code merge M.A.20061215

                --Get the Carrier Rep's office email address
                SELECT @ReturnDocDestinationValue = o.ReturnDocEmailAddress
                FROM dbo.utb_office o
                LEFT JOIN dbo.utb_user u ON (o.OfficeID = u.OfficeID)
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
        END
        /************************************************************************************/
        ---Project:210474 APD Added the following code when we did the code merge M.A.20061215
        /************************************************************************************/
        ELSE IF @ReturnDocDestinationCD = 'BOTH'
        BEGIN
            -- Destination code is Carrier Rep.
            IF @ReturnDocRoutingCD = 'FAX'
            BEGIN
                --Get the Carrier Rep's office fax number
                SELECT @ReturnDocDestinationValue = o.ReturnDocFaxAreaCode + o.ReturnDocFaxExchangeNumber + o.ReturnDocFaxUnitNumber
                FROM dbo.utb_office o
                LEFT JOIN dbo.utb_user u ON (o.OfficeID = u.OfficeID)
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END

                --Get the Carrier Rep's fax number
                SELECT @ReturnDocDestinationValue = @ReturnDocDestinationValue + '; ' + u.FaxAreaCode + u.FaxExchangeNumber + u.FaxUnitNumber
                FROM dbo.utb_user u
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
            ELSE IF @ReturnDocRoutingCD = 'FTP'
            BEGIN
                --Get the Office FTP Destination value
                SELECT @ReturnDocDestinationValue = o.ReturnDocDestinationValue
                FROM dbo.utb_office o
                LEFT JOIN dbo.utb_user u ON (o.OfficeID = u.OfficeID)
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
            ELSE
            BEGIN
                --SET @ReturnDocRoutingCD = 'EML'

                --Get the Carrier Rep's office email address
                SELECT @ReturnDocDestinationValue = o.ReturnDocEmailAddress
                FROM dbo.utb_office o
                LEFT JOIN dbo.utb_user u ON (o.OfficeID = u.OfficeID)
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END

                --Get the Carrier Rep's email address
                SELECT @ReturnDocDestinationValue = @ReturnDocDestinationValue + '; ' + u.EmailAddress
                FROM dbo.utb_user u
                LEFT JOIN dbo.utb_claim c ON (u.UserID = c.CarrierRepUserID)
                WHERE c.LynxID = @LynxID

                IF @@ERROR <> 0
                BEGIN
                   -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END
            END
        END
    END

   IF @ReturnDocDestinationValue = '' or @ReturnDocDestinationValue is null
   BEGIN
      IF @ReturnDocRoutingCD = 'FAX' set @ReturnDocDestinationValue = @ClientAdjusterFax
      IF @ReturnDocRoutingCD = 'EML' set @ReturnDocDestinationValue = @ClientAdjusterEmail
   END
   
    

    IF @UserID IS NOT NULL
    BEGIN
        SELECT @UserEmailAddress = isNull(u.EmailAddress, ''),
               @UserName = u.NameFirst + ' ' + u.NameLast,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
               @SupervisorFlag = u.SupervisorFlag
        FROM dbo.utb_user u
        WHERE u.UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
        SET @UserEmailAddress = ''
    END

    SELECT @ClaimAspectIDClaim = ClaimAspectID
    FROM dbo.utb_claim_aspect
    WHERE LynxID = @LynxID
      AND ClaimAspectTypeID = (SELECT ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name = 'Claim')

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT TOP 1 @InsuredName = IsNull(invs.BusinessName, invs.NameFirst + ' ' + invs.NameLast)
      FROM dbo.utb_claim_aspect_involved cais
      LEFT JOIN dbo.utb_involved invs ON (cais.InvolvedID = invs.InvolvedID)
      LEFT JOIN dbo.utb_involved_role irs ON (invs.InvolvedID = irs.InvolvedID)
      LEFT JOIN dbo.utb_involved_role_type irts ON (irs.InvolvedRoleTypeID = irts.InvolvedRoleTypeID)
      WHERE cais.ClaimAspectID = @ClaimAspectIDClaim    -- Insured associated with claim
        AND cais.EnabledFlag = 1
        AND irts.Name = 'Insured'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    /********************************************************************************
    --Project:210474 APD Added the code above when we did the code merge M.A.20061116
    ********************************************************************************/

    -- Create temporary table to hold metadata information

    DECLARE @tmpMetadata TABLE
    (
        GroupName           varchar(50) NOT NULL,
        TableName           varchar(50) NOT NULL,
        ColumnName          varchar(50) NOT NULL,
        DataType            varchar(20) NOT NULL,
        MaxLength           int         NULL,
        NumericPrecision    int         NULL,
        Scale               int         NULL,
        Nullable            varchar(3)  NOT NULL
    )


    -- Select Metadata information for all entities and store in the temporary table

    INSERT INTO @tmpMetadata (GroupName, TableName, ColumnName, DataType, MaxLength, NumericPrecision, Scale, Nullable)
    SELECT  'Document',
            Table_Name,
            Column_Name,
            Data_Type,
            Character_Maximum_Length,
            Numeric_Precision,
            Numeric_Scale,
            Is_Nullable
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE   (Table_Name = 'utb_document' AND Column_Name IN
            ('DocumentSourceID',
             'DocumentTypeID',
             'DirectionalCD'))    -- This needs to be changed to real field name

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpMetadata', 16, 1, @ProcName)
        RETURN
    END

    -- Create temporary Table to hold Approved estimates

    DECLARE @tmpApprovedEst TABLE
    (
        DocumentID          bigint NOT NULL,
        ClaimAspectID       bigint NULL
    )
    
    INSERT INTO @tmpApprovedEst
    SELECT d.DocumentID, ca.ClaimAspectID
    FROM utb_claim c
    LEFT JOIN utb_claim_aspect ca ON ca.LynxID = c.LynxID
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d ON cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    WHERE c.LynxID = @LynxID
      AND d.EnabledFlag = 1
      AND dt.EstimateTypeFlag = 1
      AND d.ApprovedFlag = 1 

    -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName            varchar(50) NOT NULL,
        DisplayOrder        int         NULL,
        ReferenceID         varchar(10) NOT NULL,
        Name                varchar(50) NOT NULL,
        EnabledFlag         bit         NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061116
        VANFlag             bit         NULL,
        EstimateTypeFlag    bit         NULL,
        PertainsTo          varchar(8)  NULL,
        AssignmentID        bigint      NULL,
        AssignmentSuffix    varchar(1)  NULL,
        ShopLocationName    varchar(50) NULL,
        OutputTypeCD        varchar(5)  NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061116
        StatusID            int         NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
        ClaimAspectID       varchar(10) NULL,
        MessageDescription  varchar(1000) NULL,
        ApprovedDocumentCount int       NULL
    )


    -- Select All reference information for all pertinent referencetables and store in the
    -- temporary table

    -- Message template description actually contains the description of the message and the 
    -- location of the file. So it is safe to get just the 1000 chars of it.
    --Project:210474 APD Added the column in the following INSERT when we did the code merge M.A.20061215
    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceID, Name, EnabledFlag, VANFlag,
        EstimateTypeFlag, PertainsTo, AssignmentID, AssignmentSuffix, ShopLocationName,
        OutputTypeCD, StatusID, ClaimAspectID, MessageDescription,
        ApprovedDocumentCount )

    SELECT  'DocumentSource' AS ListName,
            DisplayOrder AS DisplayOrder,
            convert(varchar, DocumentSourceID),
            Name,
            NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061116
            VANFlag,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061116
            NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_document_source
    WHERE   EnabledFlag = 1
    AND     DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'DocumentType' AS ListName,
            DisplayOrder AS DisplayOrder,
            convert(varchar, DocumentTypeID),
            Name,
            EnabledFlag,  --Project:210474 APD Added the column when we did the code merge M.A.20061116
            NULL,
            EstimateTypeFlag,
            NULL,
            NULL,
            NULL,
            NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061116
            NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL,
            NULL,
            NULL,
            NULL
    FROM    dbo.utb_document_type
    --Project:210474 APD Modified the WHERE clause below when we did the code merge M.A.20061116
    WHERE   /*EnabledFlag = 1
    AND     */DisplayOrder IS NOT NULL

    UNION ALL

    SELECT  'PertainsTo',
            NULL,
            convert(varchar, ucel.ClaimAspectID),
            ucel.Name,
            NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061116
            NULL,
            NULL,
            ucel.EntityCode,
            NULL, -- AssignmentID
            NULL, -- Assignment Suffix
            NULL, -- Shop Location Name
            NULL, -- OutputTypeCD
            cas.StatusID, -- StatusID
            casc.ClaimAspectID,
            NULL,
            (SELECT COUNT(DocumentID)
             FROM @tmpApprovedEst t
             WHERE t.ClaimAspectID = casc.ClaimAspectID)
    FROM    ufnUtilityGetClaimEntityList (@LynxID, 0, 0) ucel  -- (0, 0) means all aspects, open or closed
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	"ClaimAspectID" column is no longer part of utb_Assignment table.  Instead
		use utb_Claim_Aspect_Service_Channel to join ufnUtilityGetClaimEntityList
		and utb_assignment.
		M.A. 20061108
	*********************************************************************************/
    LEFT OUTER JOIN utb_Claim_Aspect casc ON ucel.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN dbo.utb_claim_aspect_status cas ON (ucel.ClaimAspectID = cas.ClaimAspectID AND cas.StatusTypeCD IS NULL)
    WHERE   ucel.EntityCode <> 'cov'

    UNION ALL

    SELECT  'ServiceChannel',
            NULL,
            convert(varchar, casc.ClaimAspectServiceChannelID),
            urc.Name,
            NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061116
            NULL,
            NULL,
            ucel.EntityCode,
            IsNull(a.AssignmentID, ''),
            IsNull(a.AssignmentSuffix, ''),
            IsNUll(sl.Name, ''),
            NULL,
            cas.StatusID,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            casc.ClaimAspectID,
            NULL,
            NULL
    FROM    ufnUtilityGetClaimEntityList (@LynxID, 0, 0) ucel  -- (0, 0) means all aspects, open or closed
    RIGHT JOIN utb_Claim_Aspect_Service_Channel casc
        ON ucel.ClaimAspectID = casc.ClaimAspectID

    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') urc
        ON casc.ServiceChannelCD = urc.Code
    LEFT OUTER JOIN utb_Claim_Aspect_Status cas
        ON casc.ClaimAspectID = cas.ClaimAspectID and casc.ServiceChannelCD = cas.ServiceChannelCD
            and cas.StatusTypeCD = 'SC'
    LEFT JOIN dbo.utb_assignment a
        ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID)
    LEFT JOIN dbo.utb_shop_location sl
        ON (a.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN dbo.utb_claim_aspect ca
        ON (ucel.ClaimAspectID = ca.ClaimAspectID)
    WHERE   ucel.EntityCode <> 'cov'

    UNION ALL

    SELECT  'Directional',
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,     --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL,
            NULL
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_document', 'DirectionalCD' )

    UNION ALL

    SELECT  'EstimateType',
            NULL,
            Code,
            Name,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL, --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL,
            NULL
     FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'EstimateTypeCD')

    --Project:210474 APD Added the following SELECT stmnt when we did the code merge M.A.20061116
    UNION ALL

    SELECT  'ReturnDocExceptions',
            NULL,
            convert(varchar, DocumentTypeID),
            '',
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            isNull(OutputTypeCD, ''),
            NULL,
            NULL, --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL,
            NULL
     FROM dbo.utb_client_document_exception
     WHERE InsuranceCompanyID = @InsuranceCompanyID
    --Project:210474 APD Added the SELECT stmnt above when we did the code merge M.A.20061116

     UNION ALL
    
     SELECT  'Messages',
             NULL,
             convert(varchar, MessageTemplateID),
             isNull(ServiceChannelCD, ''),
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             substring(Description, 1, 1000),
             NULL
     FROM dbo.utb_message_template
     WHERE AppliesToCD = 'C'
       AND EnabledFlag = 1

     ORDER BY ListName, DisplayOrder


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END


    -- Create temporary table to hold references to the claim's notes

    DECLARE @tmpDocuments TABLE
    (
        LynxID                      bigint          NOT NULL,
        DocumentID                  int             NOT NULL,
        ClaimAspectID               bigint          NOT NULL,
        ClaimAspectServiceChannelID bigint          NULL,
        EstimateTypeFlag            int             NOT NULL
    )


    -- Get document list

    INSERT INTO @tmpDocuments
      SELECT  @LynxID,
              cascd.DocumentID,
              casc.ClaimAspectID,
              casc.ClaimAspectServiceChannelID,
              dt.EstimateTypeFlag
        --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226
        FROM  dbo.utb_claim_aspect_Service_Channel_document cascd
        INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
        LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
        INNER JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
        INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
        WHERE ca.LynxID = @LynxID
          AND dt.Name <> 'Note'
          AND d.EnabledFlag = 1
          AND casc.EnabledFlag = 1
        --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061226
        ORDER BY d.CreatedDate desc

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpDocuments', 16, 1, @ProcName)
        RETURN
    END


    -- Now remove documents we are not interested in

    -- If ClaimAspectID is populated, any documents not belonging to that claim aspect

    IF @ClaimAspectID IS NOT NULL
    BEGIN
        DELETE FROM @tmpDocuments
        WHERE  ClaimAspectID <> @ClaimAspectID

        /*******************************************************************************/
        --Project:210474 APD Added the following when we did the code merge M.A.20061215
        /*******************************************************************************/
    	SELECT @VehicleYear = VehicleYear,
                   @VehicleMake = Make,
                   @VehicleModel = Model
            FROM utb_claim_vehicle
            WHERE ClaimAspectID = @ClaimAspectID

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END

    	SELECT @VehicleNum = ClaimAspectNumber
            FROM utb_claim_aspect
            WHERE ClaimAspectID = @ClaimAspectID

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error

                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END

        /*******************************************************************************/
        --Project:210474 APD Added the above when we did the code merge M.A.20061215
        /*******************************************************************************/
    END

    -- If ClaimAspectServiceChannelID is populated, remove any documents not belonging to that claim aspect service channel

    IF @ClaimAspectServiceChannelID IS NOT NULL
    BEGIN
         DELETE FROM @tmpDocuments
         WHERE  ClaimAspectServiceChannelID <> @ClaimAspectServiceChannelID

         SELECT @VehicleYear = cv.VehicleYear,
                @VehicleMake = cv.Make,
                @VehicleModel = cv.Model
         FROM utb_claim_vehicle cv
         LEFT JOIN utb_claim_aspect_service_channel casc on cv.ClaimAspectID = casc.ClaimAspectID
         WHERE casc.ClaimAspectID = @ClaimAspectID

         IF @@ERROR <> 0
         BEGIN
            -- SQL Server Error

             RAISERROR('99|%s', 16, 1, @ProcName)
             RETURN
         END

         SELECT @VehicleNum = ca.ClaimAspectNumber
         FROM utb_claim_aspect ca
         LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
         WHERE casc.ClaimAspectID = @ClaimAspectID

         IF @@ERROR <> 0
         BEGIN
            -- SQL Server Error

             RAISERROR('99|%s', 16, 1, @ProcName)
             RETURN
         END

    END


    -- Now check Document Class

    If @DocumentClassCD = 'E'
    BEGIN
        DELETE FROM @tmpDocuments
        WHERE  EstimateTypeFlag = 0
    END

    If @DocumentClassCD = 'O'
    BEGIN
        DELETE FROM @tmpDocuments
        WHERE  EstimateTypeFlag = 1
    END

    -- Get the claim aspect type id for the vehicle
    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
    FROM utb_claim_aspect_type
    WHERE Name = 'Vehicle'

    SET @WarrantyExistsFlag = 0
    IF EXISTS(SELECT wa.AssignmentID
               FROM utb_warranty_assignment wa
               LEFT JOIN utb_claim_aspect_service_channel casc  ON wa.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
               WHERE ca.ClaimAspectID = @ClaimAspectID
                 AND wa.CancellationDate IS NULL)
    BEGIN
      SET @WarrantyExistsFlag = 1
    END

    -- Begin XML Select

    -- Select Root Level

    SELECT 	1 AS Tag,
            NULL AS Parent,
            @LynxID AS [Root!1!LynxID],
            @InsuranceCompanyID as [Root!1!InsuranceCompanyID],
            @InsuranceCompanyNameClaim as [Root!1!InsuranceCompanyName],
            --Project:210474 APD Added 13 columns below when we did the code merge M.A.20061116
            @ClientClaimNumber      as [Root!1!ClientClaimNumber],
            @LossDate               as [Root!1!LossDate],
            @InsuredName            as [Root!1!InsuredName],
            @UserName               as [Root!1!UserName],
            @UserEmailAddress       as [Root!1!UserEmailAddress],
            @SupervisorFlag         as [Root!1!UserSupervisorFlag],
            @VehicleNum             as [Root!1!VehicleNumber],
            @VehicleYear            as [Root!1!VehicleYear],
            @VehicleMake            as [Root!1!VehicleMake],
            @VehicleModel           as [Root!1!VehicleModel],
            @ReturnDocDestinationCD as [Root!1!ReturnDocDestinationCD],
            @ReturnDocPackageTypeCD as [Root!1!ReturnDocPackageTypeCD],
            @ReturnDocRoutingCD     as [Root!1!ReturnDocRoutingCD],
            @ReturnDocDestinationValue as [Root!1!ReturnDocDestinationValue],
            @ClientAdjusterName     as [Root!1!AdjusterName],
            @ClientAdjusterFax      as [Root!1!AdjusterFax],
            @ClientAdjusterEmail    as [Root!1!AdjusterEmail],
            @ClientAdjusterActiveFlag as [Root!1!AdjusterActiveFlag],
            convert(varchar, CURRENT_TIMESTAMP, 107) as [Root!1!Today],
            @LossState              as [Root!1!LossState],
            @ClientPolicyNumber     as [Root!1!PolicyNumber],
            @EarlyBillFlag          as [Root!1!EarlyBillFlag],
            -- Document
            NULL AS [Document!2!DocumentID],
            NULL AS [Document!2!AssignmentID],
            NULL AS [Document!2!CreatedUserNameFirst],
            NULL AS [Document!2!CreatedUserNameLast],
            NULL AS [Document!2!CreatedUserRoleName],
            NULL AS [Document!2!CreatedDate],
            NULL AS [Document!2!DirectionalCD],
            NULL AS [Document!2!DocumentSourceID],
            NULL AS [Document!2!DocumentTypeID],
            NULL AS [Document!2!ReceivedDate],
            NULL AS [Document!2!ImageLocation],
            NULL AS [Document!2!ClaimAspectID],
            NULL AS [Document!2!SupplementSeqNumber],
            NULL AS [Document!2!EstimateAvailableFlag],
            NULL AS [Document!2!EstimateTypeCD],
            NULL AS [Document!2!DuplicateFlag],
            NULL AS [Document!2!DirectionToPayFlag],
            NULL AS [Document!2!FinalEstimateFlag],
            NULL AS [Document!2!FullSummaryExistsFlag],
            NULL AS [Document!2!SendToCarrierFlag],
            NULL AS [Document!2!ExternalReferenceDocumentID],
            NULL AS [Document!2!EstimateNetAmt],
            NULL AS [Document!2!SendToCarrierStatusCD], --Project:210474 APD Added column when we did the code merge M.A.20061215
            NULL AS [Document!2!SysLastUpdatedDate],
            NULL AS [Document!2!ServiceChannelCD], --Project:210474 APD Added column to support ClaimPoint M.A.20070108
            NULL AS [Document!2!ServiceChannelCDName], --Project:210474 APD Added column to support ClaimPoint M.A.20070108
            NULL AS [Document!2!ClaimAspectServiceChannelID],
            NULL AS [Document!2!ApprovedFlag],
            NULL AS [Document!2!ApprovedDate],
            NULL AS [Document!2!BilledFlag],
            NULL AS [Document!2!WarrantyFlag],
            NULL AS [Document!2!FinalBillEstimateFlag],
            NULL AS [Document!2!WarrantyExistsFlag],
            NULL AS [Document!2!AgreedPriceMetCD],
            -- Metadata Header
            NULL AS [Metadata!3!Entity],
            -- Columns
            NULL AS [Column!4!Name],
            NULL AS [Column!4!DataType],
            NULL AS [Column!4!MaxLength],
            NULL AS [Column!4!Precision],
            NULL AS [Column!4!Scale],
            NULL AS [Column!4!Nullable],
            -- Reference Data
            NULL AS [Reference!5!List],
            NULL AS [Reference!5!ReferenceID],
            NULL AS [Reference!5!Name],
            NULL AS [Reference!5!EnabledFlag],  --Project:210474 APD Added the column when we did the code merge M.A.20061116
            NULL AS [Reference!5!VANFlag],
            NULL AS [Reference!5!EstimateTypeFlag],
            NULL AS [Reference!5!PertainsTo],
            NULL AS [Reference!5!AssignmentID],
            NULL AS [Reference!5!AssignmentSuffix],
            NULL AS [Reference!5!ShopLocationName],
            NULL AS [Reference!5!OutputTypeCD],  --Project:210474 APD Added the column when we did the code merge M.A.20061116
            NULL AS [Reference!5!StatusID],  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL AS [Reference!5!ClaimAspectID],
            NULL AS [Reference!5!MessageDescription],
            NULL AS [Reference!5!ApprovedDocumentCount],
            -- Client Bundling
            NULL AS [Bundling!6!BundlingID],
            NULL AS [Bundling!6!AutoNotifyAdjusterFlag],
            NULL AS [Bundling!6!ReturnDocPackageTypeCD],
            NULL AS [Bundling!6!ReturnDocRoutingCD],
            NULL AS [Bundling!6!ReturnDocRoutingValue],
            NULL AS [Bundling!6!ServiceChannelCD],
            NULL AS [Bundling!6!OfficeID],
            NULL AS [Bundling!6!Name],
            NULL AS [Bundling!6!MessageTemplateID],
            NULL AS [Bundling!6!DocumentTypeID],
            NULL AS [Bundling!6!DocumentTypeName],
            -- Bundling Documents
            NULL AS [BundlingDocument!7!BundlingID],
            NULL AS [BundlingDocument!7!DocumentTypeID],
            NULL AS [BundlingDocument!7!DirectionalCD],
            NULL AS [BundlingDocument!7!DirectionToPayFlag],
            NULL AS [BundlingDocument!7!DuplicateFlag],
            NULL AS [BundlingDocument!7!EstimateDuplicateFlag],
            NULL AS [BundlingDocument!7!EstimateTypeCD],
            NULL AS [BundlingDocument!7!FinalEstimateFlag],
            NULL AS [BundlingDocument!7!MandatoryFlag],
            NULL AS [BundlingDocument!7!SelectionOrder],
            NULL AS [BundlingDocument!7!VANFlag],
            NULL AS [BundlingDocument!7!LossState],
            NULL AS [BundlingDocument!7!ShopState],
            -- Vehicles
            NULL AS [ClaimAspect!8!ClaimAspectID],
            NULL AS [ClaimAspect!8!ServiceChannelCD],
            NULL AS [ClaimAspect!8!ClaimAspectNumber],
            NULL AS [ClaimAspect!8!OwnerNameFirst],
            NULL AS [ClaimAspect!8!OwnerNameLast],
            NULL AS [ClaimAspect!8!OwnerBusinessName],
            NULL AS [ClaimAspect!8!SourceCCAV],
            NULL AS [ClaimAspect!8!IsNugen],
            NULL AS [ClaimAspect!8!PertainsTo],
            NULL AS [ClaimAspect!8!ClaimAspectServiceChannelID],
            NULL AS [ClaimAspect!8!ShopState],
            NULL AS [ClaimAspect!8!VehicleYear],
            NULL AS [ClaimAspect!8!VehicleMake],
            NULL AS [ClaimAspect!8!VehicleModel],
            NULL AS [ClaimAspect!8!SalvageVendorEmailAddress],
            NULL AS [ClaimAspect!8!LienHolderID],
            NULL AS [ClaimAspect!8!SourceApplicationPassThruData],
            NULL AS [ClaimAspect!8!IsWebservice]

    UNION ALL


    -- Select Note Level

    SELECT 	2,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, NULL, NULL,
            -- Document
            IsNull(doc.DocumentID, ''),
            d.AssignmentID,
            IsNull(u.NameFirst, ''),
            IsNull(u.NameLast, ''),
            IsNull(r.Name, ''),
            IsNull(d.CreatedDate, ''),
            IsNull(d.DirectionalCD, ''),
            d.DocumentSourceID,
            d.DocumentTypeID,
            IsNull(d.ReceivedDate, ''),
            IsNull(d.ImageLocation, ''),
            IsNull(doc.ClaimAspectID, ''),
            IsNull(d.SupplementSeqNumber, 0),
            (select Count(*) FROM dbo.utb_estimate WHERE DocumentID = D.DocumentID),
            IsNull(d.EstimateTypeCD,''),
            IsNull(d.DuplicateFlag,'0'),
            IsNull(d.DirectionToPayFlag,'0'),
            IsNull(d.FinalEstimateFlag,'0'),
            IsNull(d.FullSummaryExistsFlag,'0'),
            ISNull(d.SendToCarrierFlag,'0'),
            IsNull(d.ExternalReferenceDocumentID, ''),
            (SELECT  CASE
                       WHEN es.OriginalExtendedAmt < 0 THEN 0
                       ELSE es.OriginalExtendedAmt
                     END
               FROM  dbo.utb_estimate_summary es
               LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
               WHERE es.DocumentID = d.DocumentID
                 AND est.CategoryCD = 'TT'
                 AND est.Name = 'NetTotal'),
            --Project:210474 APD Added the following when we did the code merge M.A.20061116
            CASE
               WHEN d.SendToCarrierStatusCD IS NULL THEN ''
               ELSE (SELECT Name
                     FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'SendToCarrierStatusCD') ufnRefCd
                     WHERE ufnRefCd.Code = d.SendToCarrierStatusCD)
            END,
            --Project:210474 APD Added the code above when we did the code merge M.A.20061116
            dbo.ufnUtilityGetDateString( d.SysLastUpdatedDate ),
            vt.ServiceChannelCD, --Project:210474 APD Added column to support ClaimPoint M.A.20070108
            vt.ServiceChannelCDName, --Project:210474 APD Added column to support ClaimPoint M.A.20070108
            vt.ClaimAspectServiceChannelID,
            d.ApprovedFlag,
            d.ApprovedDate,
            isNull((SELECT CASE 
                              WHEN i.StatusCD = 'APD' OR 
                                   i.StatusCD = 'AC' OR
                                   i.StatusCD = 'NB' THEN 0
                              ELSE 1
                          END
                     FROM utb_invoice i
                     WHERE i.DocumentID = d.DocumentID
                       AND i.EnabledFlag = 1), 0),
            d.WarrantyFlag,
            d.FinalBillEstimateFlag,
            vt.WarrantyExistsFlag,
            d.AgreedPriceMetCD,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            -- Client Bundling
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Bundling Documents
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Vehicles
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    (SELECT @LynxID AS LynxID) AS parms
    LEFT JOIN @tmpDocuments doc ON (parms.LynxID = doc.LynxID)
    LEFT JOIN dbo.utb_document d ON (doc.DocumentID = d.DocumentID)
    LEFT JOIN dbo.utb_user u ON (d.CreatedUserID = u.UserID)
   LEFT JOIN dbo.utb_role r ON (d.CreatedUserRoleID = r.RoleID)
    --Project:210474 APD Added the code below to support ClaimPoint M.A.20070108
    LEFT OUTER JOIN
                    (
                    Select      cascd.ClaimAspectServiceChannelID,
                                cascd.DocumentID,
                                fn.Code as 'ServiceChannelCD',
                                fn.Name as 'ServiceChannelCDName',
                                CASE 
                                 WHEN EXISTS(SELECT wa.AssignmentID
                                             FROM utb_warranty_assignment wa
                                             WHERE wa.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                                               AND wa.CancellationDate IS NULL) THEN 1
                                 ELSE 0 
                                END as 'WarrantyExistsFlag'
                    from        utb_Claim_Aspect_Service_Channel casc
                    inner join  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') fn
                    on          fn.Code = casc.ServiceChannelCD
                    inner join  utb_Claim_Aspect_Service_Channel_Document cascd
                    on          casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                    inner join utb_claim_aspect ca on casc.ClaimASpectID = ca.ClaimAspectID
                    where ca.LynxID = @LynxID
                    ) vt
    on              vt.DocumentID = d.DocumentID
    --Project:210474 APD Added the above code to support ClaimPoint M.A.20070108

    UNION ALL


    -- Select Metadata Header Level

    SELECT DISTINCT 3,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, Null, Null, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL,  --Project:210474 APD Added columns to support ClaimPoint M.A.20070108
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            -- Client Bundling
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Bundling Documents
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Vehicles
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpMetadata


    UNION ALL


    -- Select Column Metadata Level

    SELECT 	4,
            3,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, Null, Null, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, --Project:210474 APD Added columns to support ClaimPoint M.A.20070108
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata Header
            GroupName,
            -- Columns
            ColumnName,
            DataType,
            MaxLength,
            NumericPrecision,
            Scale,
            Nullable,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            -- Client Bundling
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Bundling Documents
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Vehicles
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM  @tmpMetadata


    UNION ALL


    -- Select Reference Data Level

    SELECT 	5,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, Null, Null, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, --Project:210474 APD Added columns to support ClaimPoint M.A.20070108
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata Header
            'ZZ-Reference',
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
            EnabledFlag,
            VANFlag,
            EstimateTypeFlag,
            PertainsTo,
            AssignmentID,
            AssignmentSuffix,
            ShopLocationName,
            OutputTypeCD,
            StatusID, --Project:210474 APD Added the column when we did the code merge M.A.20061215
            ClaimAspectID,
            MessageDescription,
            ApprovedDocumentCount,
            -- Client Bundling
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            -- Bundling Documents
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Vehicles
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    @tmpReference


    UNION ALL


    -- Select client bundling

    SELECT 	6,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, Null, Null, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, --Project:210474 APD Added columns to support ClaimPoint M.A.20070108
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            -- Client Bundling
            b.BundlingID,
            cb.AutoNotifyAdjusterFlag,
            isNull(cb.ReturnDocPackageTypeCD, ''),
            isNull(cb.ReturnDocRoutingCD, ''),
            isNull(cb.ReturnDocRoutingValue, ''),
            isNull(cb.ServiceChannelCD, ''),
            isNull(cb.OfficeID, ''),
            b.Name,
            isNull(b.MessageTemplateID, ''),
            b.DocumentTypeID,
            dt.Name,
            -- Bundling Documents
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Vehicles
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    utb_bundling b
    LEFT JOIN utb_client_bundling cb ON b.bundlingID = cb.BundlingID
    LEFT JOIN utb_document_type dt ON b.DocumentTypeID = dt.DocumentTypeID
    WHERE cb.InsuranceCompanyID = @InsuranceCompanyIDClaim
      AND b.EnabledFlag = 1


    UNION ALL


    -- Select client bundling

    SELECT 	7,
            6,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, Null, Null, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, --Project:210474 APD Added columns to support ClaimPoint M.A.20070108
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            -- Client Bundling
            b.BundlingID, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL,
            -- Bundling Documents
            b.BundlingID,
            bdt.DocumentTypeID,
            isNull(bdt.DirectionalCD, 'I'),
            isNull(bdt.DirectionToPayFlag, 0),
            isNull(bdt.DuplicateFlag, 0),
            isNull(bdt.EstimateDuplicateFlag, 0),
            isNull(bdt.EstimateTypeCD, ''),
            isNull(bdt.FinalEstimateFlag, 0),
            isNull(bdt.MandatoryFlag, 0),
            bdt.SelectionOrder,
            isNull(bdt.VANFlag, 0),
            LTrim(isNull(bdt.LossState, '')),
            LTrim(isNull(bdt.ShopState, '')),
            -- Vehicles
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    FROM    utb_bundling_document_type bdt
    LEFT JOIN utb_bundling b ON bdt.BundlingID = b.BundlingID
    LEFT JOIN utb_client_bundling cb ON b.bundlingID = cb.BundlingID
    WHERE cb.InsuranceCompanyID = @InsuranceCompanyIDClaim
      AND b.EnabledFlag = 1

    UNION ALL


    -- Select vehicles

    SELECT 	8,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, NULL, NULL,
            -- Document
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, Null, Null, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,  --Project:210474 APD Added the column when we did the code merge M.A.20061215
            NULL, NULL, NULL, --Project:210474 APD Added columns to support ClaimPoint M.A.20070108
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Metadata Header
            NULL,
            -- Columns
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            -- Client Bundling
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL,
            -- Bundling Documents
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL,
            -- Vehicles
            ca.ClaimAspectID, 
            casc.ServiceChannelCD,
            ca.ClaimAspectNumber, 
            IsNull((SELECT  Top 1 i.NameFirst
                    FROM  dbo.utb_claim_aspect_involved cai
                    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cai.ClaimAspectID = cv.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name = 'Owner'), ''),
            IsNull((SELECT  Top 1 i.NameLast
                    FROM  dbo.utb_claim_aspect_involved cai
                    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cai.ClaimAspectID = cv.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name = 'Owner'), ''),
            IsNull((SELECT  Top 1 i.BusinessName
                    FROM  dbo.utb_claim_aspect_involved cai
                    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                    LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cai.ClaimAspectID = cv.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name = 'Owner'), ''),
            CASE 
                WHEN ca.SourceApplicationID = @SourceApplicationIDCCAV THEN 1
                ELSE 0
            END,
            CASE
                WHEN ca.SourceApplicationPassThruData LIKE '%GUID={%' THEN 1
                ELSE 0
            END,
            tmpEntity.EntityCode,
            casc.ClaimAspectServiceChannelID,
            ISNULL((SELECT sl.AddressState
             FROM utb_assignment a 
             LEFT JOIN utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID
             WHERE a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               AND a.AssignmentSequenceNumber = 1
               and a.CancellationDate is NULL), ''),
            cv.VehicleYear, 
            cv.Make, 
            cv.Model,
            isNull((SELECT sv.EmailAddress
                    FROM dbo.utb_salvage_vendor sv
                    WHERE sv.SalvageVendorID = casc.SalvageVendorID), ''),
            isNull(convert(varchar, casc.LienHolderID), ''),
            isNull(ca.SourceApplicationPassThruData,''),
            CASE 
                WHEN ca.SourceApplicationID = @SourceApplicationIDWS THEN 1
                ELSE 0
            END

    FROM    utb_claim_aspect ca
    LEFT JOIN utb_claim_vehicle cv on ca.ClaimAspectID = cv.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN dbo.ufnUtilityGetClaimEntityList(@LynxID, 0, 0) tmpEntity ON ca.ClaimAspectID = tmpEntity.ClaimAspectID
    WHERE ca.LynxID = @LynxID
      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
      AND ca.EnabledFlag = 1
      AND casc.PrimaryFlag = 1
      AND casc.EnabledFlag = 1

    ORDER BY [Metadata!3!Entity], [Bundling!6!BundlingID], Tag, [Document!2!DocumentID] DESC
    FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDocumentGetDetailWSXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspDocumentGetDetailWSXML TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/