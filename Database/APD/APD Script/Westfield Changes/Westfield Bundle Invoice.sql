BEGIN transaction

DECLARE @BundlingID int
DECLARE @DocumentTypeID int

--Closing - Total Loss

select @BundlingID = cb.BundlingID  from utb_client_bundling cb inner join utb_bundling b on cb.BundlingID = b.BundlingID where cb.InsuranceCompanyID = 387 and b.Name = 'Closing - Total Loss' 
select @DocumentTypeID = DocumentTypeID from utb_document_type where Name = 'Invoice'
 
IF NOT EXISTS (select * from utb_bundling_document_type where BundlingID=@BundlingID and DocumentTypeID = @DocumentTypeID)
BEGIN
	insert into utb_bundling_document_type (BundlingID,DocumentTypeID,ConditionValue,DirectionalCD,DirectionToPayFlag,DuplicateFlag,EstimateDuplicateFlag,EstimateTypeCD,FinalEstimateFlag,LossState,MandatoryFlag,SelectionOrder,ShopState,VANFlag,WarrantyFlag,SysLastUserID,SysLastUpdatedDate)
	values (@BundlingID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	print 'Insert Success for Closing - Total Loss'
END
ELSE
BEGIN
	print 'Insert Failed for Closing - Total Loss due to Document already exists'
END

Set @BundlingID = NULL
Set @DocumentTypeID = NULL

--Cash Out Alert & Close

select @BundlingID = cb.BundlingID  from utb_client_bundling cb inner join utb_bundling b on cb.BundlingID = b.BundlingID where cb.InsuranceCompanyID = 387 and b.Name = 'Cash Out Alert & Close'
select @DocumentTypeID = DocumentTypeID from utb_document_type where Name = 'Invoice'
 
IF NOT EXISTS (select * from utb_bundling_document_type where BundlingID=@BundlingID and DocumentTypeID = @DocumentTypeID)
BEGIN
	insert into utb_bundling_document_type (BundlingID,DocumentTypeID,ConditionValue,DirectionalCD,DirectionToPayFlag,DuplicateFlag,EstimateDuplicateFlag,EstimateTypeCD,FinalEstimateFlag,LossState,MandatoryFlag,SelectionOrder,ShopState,VANFlag,WarrantyFlag,SysLastUserID,SysLastUpdatedDate)
	values (@BundlingID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	print 'Insert Success for Cash Out Alert & Close'
END
ELSE
BEGIN
	print 'Insert Failed for Cash Out Alert & Close due to Document already exists'
END


Set @BundlingID = NULL
Set @DocumentTypeID = NULL

--Closing Repair Complete

select @BundlingID = cb.BundlingID  from utb_client_bundling cb inner join utb_bundling b on cb.BundlingID = b.BundlingID where cb.InsuranceCompanyID = 387 and b.Name = 'Closing Repair Complete'
select @DocumentTypeID = DocumentTypeID from utb_document_type where Name = 'Invoice'
 
IF NOT EXISTS (select * from utb_bundling_document_type where BundlingID=@BundlingID and DocumentTypeID = @DocumentTypeID)
BEGIN
	insert into utb_bundling_document_type (BundlingID,DocumentTypeID,ConditionValue,DirectionalCD,DirectionToPayFlag,DuplicateFlag,EstimateDuplicateFlag,EstimateTypeCD,FinalEstimateFlag,LossState,MandatoryFlag,SelectionOrder,ShopState,VANFlag,WarrantyFlag,SysLastUserID,SysLastUpdatedDate)
	values (@BundlingID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	print 'Insert Success for Closing Repair Complete'
END
ELSE
BEGIN
	print 'Insert Failed for Closing Repair Complete due to Document already exists'
END


Set @BundlingID = NULL
Set @DocumentTypeID = NULL

--No Inspection (Stale) Alert & Close

select @BundlingID = cb.BundlingID  from utb_client_bundling cb inner join utb_bundling b on cb.BundlingID = b.BundlingID where cb.InsuranceCompanyID = 387 and b.Name = 'No Inspection (Stale) Alert & Close'
select @DocumentTypeID = DocumentTypeID from utb_document_type where Name = 'Invoice'
 
IF NOT EXISTS (select * from utb_bundling_document_type where BundlingID=@BundlingID and DocumentTypeID = @DocumentTypeID)
BEGIN
	insert into utb_bundling_document_type (BundlingID,DocumentTypeID,ConditionValue,DirectionalCD,DirectionToPayFlag,DuplicateFlag,EstimateDuplicateFlag,EstimateTypeCD,FinalEstimateFlag,LossState,MandatoryFlag,SelectionOrder,ShopState,VANFlag,WarrantyFlag,SysLastUserID,SysLastUpdatedDate)
	values (@BundlingID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	print 'Insert Success for No Inspection (Stale) Alert & Close'
END
ELSE
BEGIN
	print 'Insert Failed for No Inspection (Stale) Alert & Close due to Document already exists'
END

Set @BundlingID = NULL
Set @DocumentTypeID = NULL

--Closing Supplement

select @BundlingID = cb.BundlingID  from utb_client_bundling cb inner join utb_bundling b on cb.BundlingID = b.BundlingID where cb.InsuranceCompanyID = 387 and b.Name = 'Closing Supplement'
select @DocumentTypeID = DocumentTypeID from utb_document_type where Name = 'Invoice'

IF NOT EXISTS (select * from utb_bundling_document_type where BundlingID=@BundlingID and DocumentTypeID = @DocumentTypeID)
BEGIN
	insert into utb_bundling_document_type (BundlingID,DocumentTypeID,ConditionValue,DirectionalCD,DirectionToPayFlag,DuplicateFlag,EstimateDuplicateFlag,EstimateTypeCD,FinalEstimateFlag,LossState,MandatoryFlag,SelectionOrder,ShopState,VANFlag,WarrantyFlag,SysLastUserID,SysLastUpdatedDate)
	values (@BundlingID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	print 'Insert Success for Closing Supplement'
END
ELSE
BEGIN
	print 'Insert Failed for Closing Supplement due to Document already exists'
END

COMMIT transaction