/**** uspCFDASummaryReportGetDetailXML ******/
GO
BEGIN TRANSACTION
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFDASummaryReportGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFDASummaryReportGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END
GO
/**** uspCFSaveFormData ******/

BEGIN TRANSACTION
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFSaveFormData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFSaveFormData TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

/**** uspCFSaveFormDataDetail ******/

BEGIN TRANSACTION
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFSaveFormDataDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFSaveFormDataDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

/**** uspCustomFormsGetListXML ******/

BEGIN TRANSACTION
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCustomFormsGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCustomFormsGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

/**** uspEstimateQuickUpdDetail ******/

BEGIN TRANSACTION
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateQuickUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspEstimateQuickUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

/**** uspEstimateQuickGetDetailXML ******/

BEGIN TRANSACTION
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateQuickGetDetailXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspEstimateQuickGetDetailXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END
GO
/**** uspCFUpdateFormDataDetail ******/

BEGIN TRANSACTION
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCFUpdateFormDataDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCFUpdateFormDataDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO