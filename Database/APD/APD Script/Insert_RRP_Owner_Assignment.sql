Declare @now DateTime
Declare @AssignmentPoolID Varchar(10)
Set @now = GETDATE()

/****Get AssignmentPoolID for Owner****/
Select @AssignmentPoolID = AssignmentPoolID from utb_assignment_pool Where FunctionCD = 'OWN' AND Name = 'File Owner'

IF NOT EXISTS (SELECT * FROM utb_task_assignment_pool WHERE TaskID = 62 AND ServiceChannelCD = 'RRP')
BEGIN
 Insert into utb_task_assignment_pool  values(62,'RRP',0,@AssignmentPoolID,0,@now)
END 

