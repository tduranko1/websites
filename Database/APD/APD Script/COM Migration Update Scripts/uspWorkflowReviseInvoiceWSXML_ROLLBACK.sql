USE [udb_apd]
-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'uspWorkflowReviseInvoiceWSXML' AND type = 'P') 
BEGIN
    DROP PROCEDURE dbo.uspWorkflowReviseInvoiceWSXML
END

GO