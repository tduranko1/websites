-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmUserApplicationInsDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmUserApplicationInsDetailWSXML 
END

GO
/****** Object:  StoredProcedure [dbo].[uspAdmUserApplicationInsDetailWSXML]    Script Date: 02/14/2014 06:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
* uspAdmUserApplicationInsDetail 
* PROCEDURE:    uspAdmUserApplicationInsDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Inserts user information for a particular application
*
* PARAMETERS:  
* (I) @UserID               User ID to update
* (I) @ApplicationID        The application to update
* (I) @LogonId              Logon Id
* (I) @PasswordHint         Password Hint
* (I) @SysLastUserID        User performing the update
* (I) @SysLastUpdatedDate   The "previous" updated date
*
* RESULT SET:
*   The new last updated date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspAdmUserApplicationInsDetailWSXML]
    @UserID                 udt_std_id,
    @ApplicationID          udt_std_id,
    @LogonId                udt_sys_login,
    @PasswordHint           udt_std_desc_mid,
    @SysLastUserID          udt_std_id
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@LogonId))) = 0 SET @LogonId = NULL
    IF LEN(RTRIM(LTRIM(@PasswordHint))) = 0 SET @PasswordHint = NULL


    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @KeyValue               AS varchar(40)
    DECLARE @now                    AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspAdmUserApplicationInsDetail'

    
    -- Check to make sure a valid Sys Last User id was passed in

    IF  (@SysLastUserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @SysLastUserID))
    BEGIN
        -- Invalid Sys last User ID
    
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END
    
    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Begin Update

    BEGIN TRANSACTION UserApplicationInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Insert the user application information
    
    INSERT INTO dbo.utb_user_application
    (
        UserId,
        ApplicationId,
        AccessBeginDate,
        AccessEndDate,
        LogonId,
        PasswordHint,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES (@UserID,
            @ApplicationID,
            @now,
            @now,
            @LogonId,
            @PasswordHint,
            @SysLastUserID,
            @now )

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value

    IF @error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_user_application', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

        
    COMMIT TRANSACTION UserApplicationInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Create XML Document to return updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- User Level
            NULL AS [User!2!UserID],
            NULL AS [User!2!ApplicationID],
            NULL AS [User!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- User Level
            @UserID,
            @ApplicationID,
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
    FOR XML EXPLICIT      -- Comment for Client-side processing

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmUserApplicationInsDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmUserApplicationInsDetailWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


