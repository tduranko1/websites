-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspSessionGetClaimDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSessionGetClaimDetailWSXML
END
GO
/****** Object:  StoredProcedure [dbo].[uspSessionGetClaimDetailXML]    Script Date: 09/25/2014 04:20:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspSessionGetClaimDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Retrieves basic claim information for session purposes
*
* PARAMETERS:
* (I) @LynxID               The LynxID
*
* RESULT SET:
*   An XML universal recordset detailing claim attributes
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSessionGetClaimDetailWSXML]
    @LynxID     udt_std_int_big
AS
BEGIN
    -- Declare local variables

    DECLARE @ClaimAspectTypeIDClaim     AS udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle   AS udt_std_id
    DECLARE @ClaimAspectTypeIDProperty  AS udt_std_id
    DECLARE @FirstVehicleClaimAspectID  AS udt_std_id
    DECLARE @FirstPropertyClaimAspectID AS udt_std_id
    DECLARE @ClaimAspectOwnerUserID     AS udt_std_id
    DECLARE @ClaimAspectAnalystUserID   AS udt_std_id
    DECLARE @ServiceChannelCD           as udt_std_cd
    DECLARE @CoverageLimitWarningInd    AS udt_std_flag
    DECLARE @Claim_ClaimAspectTypeID    AS udt_std_int_tiny
    DECLARE @OwnerUserID                AS udt_std_id_big
    DECLARE @OwnerUserNameFirst         AS udt_per_name
    DECLARE @OwnerUserNameLast          AS udt_per_name
    DECLARE @OwnerUserPhoneAreaCode     AS udt_ph_area_code
    DECLARE @OwnerUserPhoneExchangeNumber  AS udt_ph_exchange_number
    DECLARE @OwnerUserPhoneUnitNumber   AS udt_ph_unit_number
    DECLARE @OwnerUserPhoneExtensionNumber AS udt_ph_extension_number
    DECLARE @OwnerUserEmail             AS udt_web_email


    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspSessionGetClaimDetailWSXML'


    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID

        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    -- Verify APD Data State

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Insured')
    BEGIN
       -- Involved Type Not Found

        RAISERROR('102|%s|"Insured"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END


    -- Create temporary Table to hold Reference information

    DECLARE @tmpReference TABLE
    (
        ListName                            varchar(50) NOT NULL,
        ReferenceID                         varchar(10) NOT NULL,
        Name                                varchar(50) NOT NULL,
        PertainsTo                          varchar(8)  NULL,
        StatusID                            varchar(8)  NOT NULL,
        PrimaryClaimAspectServiceChannelID  varchar(10) NOT NULL
    )

    -- Select All reference information for all pertinent referencetables and store in the
    -- temporary table

    INSERT INTO @tmpReference (ListName, ReferenceID, Name, StatusID, PrimaryClaimAspectServiceChannelID)

    SELECT  'PertainsTo',
            ucel.ClaimAspectID,
            ucel.Name,
            cas.StatusID,
            casc.ClaimAspectServiceChannelID
    FROM    ufnUtilityGetClaimEntityList (@LynxID, 0, 0) ucel  -- (0, 0) means all aspects, open or closed
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added reference to utb_Claim_Aspect_Status table to get the value of "StatusID"
		    M.A. 20061120
	*********************************************************************************/
    LEFT JOIN utb_claim_aspect ca ON (ucel.ClaimAspectID = ca.ClaimAspectID)
    LEFT OUTER JOIN utb_Claim_Aspect_Status cas on ca.ClaimAspectID = cas.ClaimAspectID
	LEFT OUTER JOIN utb_Status s on cas.StatusID = s.StatusID
    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectID = cas.ClaimAspectID
    WHERE   ucel.EntityCode <> 'cov'
	AND		s.StatusTypeCD IS NULL
    AND     casc.PrimaryFlag = 1

    ORDER BY ucel.ClaimAspectID


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END


    -- Get Aspect Type ID for claim

   SELECT @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
       -- Claim Aspect Not Found

        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


    -- Get Aspect Type ID for property

   SELECT @ClaimAspectTypeIDProperty = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Property'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDProperty IS NULL
    BEGIN
       -- Claim Aspect Not Found

        RAISERROR('102|%s|"Property"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END


    -- Get Aspect Type ID for vehicle

   SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
       -- Claim Aspect Not Found

        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

    -- Get the Claim's ClaimAspectTypeID
    SELECT @Claim_ClaimAspectTypeID = ClaimAspectTypeID
    FROM dbo.utb_claim_aspect_type
    WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Check data state of the claim itself

    IF (SELECT Count(*)
          FROM dbo.utb_claim_aspect cas
          LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
          LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
          LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
          WHERE cas.LynxID = @LynxID
            AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
            AND cai.EnabledFlag = 1
            AND irt.Name = 'Insured') > 1
  BEGIN
       -- Multiple Insured Found

        RAISERROR  ('%s: Invalid APD Data State.  Claim has multiple insured associated with it.', 16, 1, @ProcName)
        RETURN
    END

    -- Check data state of the claim Status

    IF NOT EXISTS(SELECT  s.Name
                    FROM  dbo.utb_claim_aspect ca
                	/*********************************************************************************
                	Project: 210474 APD - Enhancements to support multiple concurrent service channels
                	Note:	utb_Claim_Aspect does not have "StatusID" column.  So added reference to
                            utb_Claim_Aspect_Status table to join utb_Claim_Aspect and utb_Status
                            tables and get the value of "Name" column
                		    M.A. 20061120
                	*********************************************************************************/
                    LEFT OUTER JOIN utb_Claim_Aspect_Status cas on ca.ClaimAspectID = cas.ClaimAspectID
                    LEFT JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID)
                    WHERE ca.LynxID = @LynxID
                      AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDClaim
                      AND ca.ClaimAspectNumber = 0)
    BEGIN
       -- Current state not found

        RAISERROR  ('109|%s', 16, 1, @ProcName)
        RETURN
    END


    -- We need to first get the Claim Aspect ID of the first Vehicle and Property.  We wouild normally do this using a subquery, but
    -- SQL has a problem with the ORDER BY clause (because the XML Select is a union), even though it is only a subquery.

    SELECT TOP 1 @FirstVehicleClaimAspectID = ca.ClaimAspectID,
                 @ClaimAspectOwnerUserID = ca.OwnerUserID,
                 @ClaimAspectAnalystUserID = ca.AnalystUserID,
                 --@ServiceChannelCD = ast.ServiceChannelDefaultCD --Project:210474 APD Remarked-off to support the schema change M.A.20061219
                 @ServiceChannelCD = casc.ServiceChannelCD --Project:210474 APD Added to support the schema change M.A.20061219
      FROM  dbo.utb_claim_aspect ca
      --Project:210474 APD Remarked-off the following relation due to schema change M.A.20061219
      --LEFT JOIN dbo.utb_assignment_type ast ON (ca.CurrentAssignmentTypeID = ast.AssignmentTypeID)   --Project:210474 APD The column was added in utb_Claim_Aspect so changed it back to the original state. M.A.20061201
      --Project:210474 APD Added the following relation to support schema change M.A.20061219
      LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc
      ON ca.ClaimAspectID = casc.ClaimAspectID
      and casc.Primaryflag = 1

      WHERE ca.LynxID = @LynxID
        AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
        AND ca.EnabledFlag = 1
      ORDER BY ClaimAspectNumber


    IF @ServiceChannelCD IN ('DR','DA')
    BEGIN
        SET @OwnerUserID = @ClaimAspectAnalystUserID
    END
    ELSE
    BEGIN
        SET @OwnerUserID = @ClaimAspectOwnerUserID
    END




    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT TOP 1 @FirstPropertyClaimAspectID = ClaimAspectID
      FROM  dbo.utb_claim_aspect
      WHERE LynxID = @LynxID
        AND ClaimAspectTypeID = @ClaimAspectTypeIDProperty
        AND EnabledFlag = 1
      ORDER BY ClaimAspectNumber

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET @CoverageLimitWarningInd = 0

--Project:210474 APD Remarked-off the following SELECT when we did the code merge M.A.20061120
--Reason: Because the columns referenced are not valid anymore
    -- Check if the claim has coverage limit < 10000
/*    SELECT @CoverageLimitWarningInd = 1
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND ((CollisionLimitAmt > 0 and CollisionLimitAmt <= 10000)
        OR (ComprehensiveLimitAmt > 0 and ComprehensiveLimitAmt <= 10000)
        OR (LiabilityLimitAmt > 0 and LiabilityLimitAmt <= 10000)
        OR (UnderInsuredLimitAmt > 0 and UnderInsuredLimitAmt <= 10000)
        OR (UnInsuredLimitAmt > 0 and UnInsuredLimitAmt <= 10000)
        )
*/
/*********************************************************************************
Project: 210474 APD - Enhancements to support multiple concurrent service channels
Note:	Added the following variable to hold the values of "LmitAmt" for the
        following "CoverageTypeCD" codes for a particular "LynxID".
	    M.A. 20061120
*********************************************************************************/
DECLARE    @CollisionLimitAmt as udt_std_money,
           @ComprehensiveLimitAmt as udt_std_money,
           @LiabilityLimitAmt as udt_std_money,
           @UnderInsuredLimitAmt as udt_std_money,
           @UnInsuredLimitAmt as udt_std_money

--Now get the LimitAmt value for each type of coverage code for a particular LynxID
--First, "Collision"
SELECT     @CollisionLimitAmt = isnull(LimitAmt,0)
FROM       utb_claim_coverage
WHERE      LynxID = @LynxID
and        CoverageTypeCD = 'COLL'

--Second, "Comprehensive"
SELECT     @ComprehensiveLimitAmt = isnull(LimitAmt,0)
FROM       utb_claim_coverage
WHERE      LynxID = @LynxID
and        CoverageTypeCD = 'COMP'

--Third, "Liability"
SELECT     @LiabilityLimitAmt = isnull(LimitAmt,0)
FROM       utb_claim_coverage
WHERE      LynxID = @LynxID
and        CoverageTypeCD = 'LIAB'

--Fourth, "Under Insured"
SELECT     @UnderInsuredLimitAmt = isnull(LimitAmt,0)
FROM       utb_claim_coverage
WHERE      LynxID = @LynxID
and        CoverageTypeCD = 'UIM'

--Finally, "UnInsured"
SELECT     @UnInsuredLimitAmt = isnull(LimitAmt,0)
FROM       utb_claim_coverage
WHERE      LynxID = @LynxID
and        CoverageTypeCD = 'UM'

    -- Check if the claim has coverage limit < 10000
    if (
        (@CollisionLimitAmt between 1 and 10000)
        OR
        (@ComprehensiveLimitAmt between 1 and 10000)
        OR
        (@LiabilityLimitAmt between 1 and 10000)
        OR
        (@UnderInsuredLimitAmt between 1 and 10000)
        OR
        (@UnInsuredLimitAmt between 1 and 10000)
        )
    BEGIN
        SELECT @CoverageLimitWarningInd = 1
    END
/*********************************************************************************
Project: 210474 APD - Enhancements to support multiple concurrent service channels
Note:	Added the above code to hold the values of "LmitAmt" for the different
        "CoverageTypeCD" codes for a particular "LynxID" and verify if the values
        are within the threshold values
	    M.A. 20061120
*********************************************************************************/

    -- Begin XML Select

    -- Select Root Level

    SELECT  1 AS tag,
            NULL AS parent,
            @LynxID AS [Root!1!LynxID],
            -- Claim
            NULL AS [Claim!2!LynxID],
            NULL AS [Claim!2!OwnerUserID],
            NULL AS [Claim!2!OwnerUserNameFirst],
            NULL AS [Claim!2!OwnerUserNameLast],
            NULL AS [Claim!2!OwnerUserPhoneAreaCode],
            NULL AS [Claim!2!OwnerUserPhoneExchangeNumber],
            NULL AS [Claim!2!OwnerUserPhoneUnitNumber],
            NULL AS [Claim!2!OwnerUserExtensionNumber],
            NULL AS [Claim!2!OwnerUserEmail],
            NULL AS [Claim!2!ClaimOpen],
            NULL AS [Claim!2!ClaimAspectID],
            NULL AS [Claim!2!ClaimStatus],
            NULL AS [Claim!2!RestrictedFlag],
            NULL AS [Claim!2!InsuranceCompanyID],
            NULL AS [Claim!2!InsuranceCompanyName],
            NULL AS [Claim!2!ClientClaimNumber],
            NULL AS [Claim!2!InsuredNameFirst],
            NULL AS [Claim!2!InsuredNameLast],
            NULL AS [Claim!2!BusinessName],
            NULL AS [Claim!2!LossDate],
            NULL AS [Claim!2!VehiclesPresentFlag],
            NULL AS [Claim!2!FirstVehicleClaimAspectID],
            NULL AS [Claim!2!PropertiesPresentFlag],
            NULL AS [Claim!2!FirstPropertyClaimAspectID],
            NULL AS [Claim!2!DemoFlag],
            NULL AS [Claim!2!SourceApplicationCode],
            NULL AS [Claim!2!CoverageLimitWarningInd],
            -- Reference Data
            NULL AS [Reference!3!List],
            NULL AS [Reference!3!ReferenceID],
            NULL AS [Reference!3!Name],
            NULL AS [Reference!3!StatusID],
            NULL AS [Reference!3!PrimaryClaimAspectServiceChannelID]


    UNION ALL


    -- Select Claim level

    SELECT  2,
            1,
            NULL,
            -- Claim
            IsNull(c.LynxID, ''),
            IsNull((case @ServiceChannelCD
                    when 'DA' then @ClaimAspectAnalystUserID
                    when 'DR' then @ClaimAspectAnalystUserID
                    else @ClaimAspectOwnerUserID end), ''),
            IsNull(u.NameFirst, ''),
            IsNull(u.NameLast, ''),
            IsNull(u.PhoneAreaCode, ''),
            IsNull(u.PhoneExchangeNumber, ''),
            IsNull(u.PhoneUnitNumber, ''),
            IsNull(u.PhoneExtensionNumber, ''),
            IsNull(u.EmailAddress, ''),
            CASE
                WHEN (SELECT  s.Name
                        FROM  dbo.utb_claim_aspect ca
                        /*********************************************************************************
                        Project: 210474 APD - Enhancements to support multiple concurrent service channels
                        Note:	Added reference to utb_Claim_Aspect_Status table to join utb_Claim_Aspect
                                and utb_Status table and get the vlaue of "Name" column from utb_Status
                                table
                        	    M.A. 20061120
                        *********************************************************************************/
                        LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
                        LEFT JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID)
                        LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
                        WHERE ca.LynxID = c.LynxID
                          AND cat.Name = 'Claim'
                          AND ca.ClaimAspectNumber = 0) = 'Claim Closed' THEN 0
                ELSE 1
            END,
            IsNull((SELECT ca.ClaimAspectID
                      FROM dbo.utb_claim_aspect ca
                      LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
                      WHERE ca.LynxID = c.LynxID
                        AND cat.Name = 'Claim' AND ca.ClaimAspectNumber = 0), 0),
            IsNull((SELECT  s.Name
                        FROM  dbo.utb_claim_aspect ca
                        /*********************************************************************************
                        Project: 210474 APD - Enhancements to support multiple concurrent service channels
                        Note:	Added reference to utb_Claim_Aspect_Status table to join utb_Claim_Aspect
                                and utb_Status table and get the vlaue of "Name" column from utb_Status
                                table
                        	    M.A. 20061120
                        *********************************************************************************/
                        LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
                        LEFT JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID)
                        LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
                        WHERE ca.LynxID = c.LynxID
                          AND cat.Name = 'Claim'
                          AND ca.ClaimAspectNumber = 0), ''),
            IsNull(c.RestrictedFlag, ''),
            IsNull(c.InsuranceCompanyID, 0),
            IsNull(ic.Name, ''),
            IsNull(c.ClientClaimNumber, ''),
            IsNull((SELECT i.NameFirst
                      FROM dbo.utb_claim_aspect cas
                      LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                      LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                      LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                      LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                      WHERE cas.LynxID = c.LynxID
                        AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                        AND cai.EnabledFlag = 1
                        AND irt.Name = 'Insured'), ''),
            IsNull((SELECT i.NameLast
                      FROM dbo.utb_claim_aspect cas
                      LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                      LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                      LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                      LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                      WHERE cas.LynxID = c.LynxID
                        AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                        AND cai.EnabledFlag = 1
                        AND irt.Name = 'Insured'), ''),
            IsNull((SELECT i.BusinessName
                      FROM dbo.utb_claim_aspect cas
                      LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                      LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                      LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                      LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                      WHERE cas.LynxID = c.LynxID
                        AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                        AND cai.EnabledFlag = 1
                        AND irt.Name = 'Insured'), ''),
            IsNull(c.LossDate, ''),
            CASE
              WHEN (SELECT Count(ClaimAspectID) FROM dbo.utb_claim_aspect WHERE LynxID = @LynxID AND ClaimAspectTypeID = @ClaimAspectTypeIDVehicle AND EnabledFlag = 1) > 0 THEN '1'
              ELSE '0'
            END,
            IsNull(@FirstVehicleClaimAspectID, ''),
            CASE
              WHEN (SELECT Count(ClaimAspectID) FROM dbo.utb_claim_aspect WHERE LynxID = @LynxID AND ClaimAspectTypeID = @ClaimAspectTypeIDProperty AND EnabledFlag = 1) > 0 THEN '1'
              ELSE '0'
            END,
            IsNull(@FirstPropertyClaimAspectID, ''),
            ic.DemoFlag,
            a.Code,
            @CoverageLimitWarningInd,
            -- Reference Data
            NULL, NULL, NULL, NULL, NULL

    FROM    dbo.utb_claim c
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID)
--    LEFT JOIN dbo.utb_user u ON (ca.OwnerUserID = u.UserID)
    LEFT JOIN dbo.utb_user u ON (@OwnerUserID = u.UserID)
    LEFT JOIN dbo.utb_insurance ic ON (c.InsuranceCompanyID = ic.InsuranceCompanyID)
  --  LEFT JOIN dbo.utb_client_contract_state ccs ON (c.InsuranceCompanyID = ccs.InsuranceCompanyID)
    LEFT JOIN dbo.utb_claim_coverage cc ON (c.LynxID = cc.LynxID)
    LEFT JOIN dbo.utb_application a ON (ca.SourceApplicationID = a.ApplicationID)
    WHERE   c.LynxID = @LynxID
      AND   ca.ClaimAspectTypeID = @Claim_ClaimAspectTypeID


    UNION ALL


    -- Select Reference Data Level

    SELECT 	3,
            1,
            NULL,
            -- Claim
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, Null, Null, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            -- Reference Data
            ListName,
            ReferenceID,
            Name,
            StatusID,
            PrimaryClaimAspectServiceChannelID


    FROM    @tmpReference


    ORDER BY tag
    FOR XML EXPLICIT  -- (Comment for client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspSessionGetClaimDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionGetClaimDetailWSXML TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimNumberSourceAppGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimNumberSourceAppGetDetailWSXML 
END

GO
/****** Object:  StoredProcedure [dbo].[uspClaimNumberSourceAppGetDetailWSXML]    Script Date: 09/25/2014 04:25:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimNumberSourceAppGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc will return the vehicle list for the client claim number
*
* PARAMETERS:  
* (I) @ClientClaimNumber    Client Claim Number
* (I) @InsuranceCompanyID   Insurance Company ID
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspClaimNumberSourceAppGetDetailWSXML]
    @ClientClaimNumber      udt_cov_claim_number,
    @InsuranceCompanyID     udt_std_id,
    @SourceApplicationID    udt_std_id
AS
BEGIN
    -- Set database options

    SET CONCAT_NULL_YIELDS_NULL  ON 


    -- Declare local variables

    DECLARE @LynxID                     udt_std_id_big
    DECLARE @InsuranceCompanyIDClaim    udt_std_id
    DECLARE @ClaimAspectTypeID          udt_std_id

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimNumberSourceAppGetDetailWSXML'

    SELECT @LynxID = c.LynxID
    FROM dbo.utb_claim_aspect ca
    INNER JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
    --LEFT JOIN dbo.utb_claim_coverage cc ON (c.LynxID = cc.LynxID) --Project:210474 APD The column ClientClaimNumber is part of utb_Claim, removed the reference to the table utb_Claim_Coverage becuase it is not needed, when we did the code merge M.A.20061211
    WHERE c.ClientClaimNumber = @ClientClaimNumber
      AND c.InsuranceCompanyID = @InsuranceCompanyID
      AND ca.SourceApplicationID = 6

    
    
    -- Get the aspect type id for property for use later
    
    SELECT  @ClaimAspectTypeID = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'
      
    -- Begin XML Select

    SELECT
        1 as Tag,
        0 as Parent,
--        Root
        @LynxID as [Root!1!LynxID],
--        VehicleList
        Null as [Vehicle!2!VehicleNumber],
        Null as [Vehicle!2!ClaimAspectID],
        Null as [Vehicle!2!VehicleYear],
        Null as [Vehicle!2!Make],
        Null as [Vehicle!2!Model],
        Null as [Vehicle!2!NameFirst],
        Null as [Vehicle!2!NameLast],
        Null as [Vehicle!2!BusinessName],
        Null as [Vehicle!2!ClosedStatus],
        Null as [Vehicle!2!Status],
        Null as [Vehicle!2!StatusID],
        Null as [Vehicle!2!ExposureCD],
        Null as [Vehicle!2!CoverageProfileCD],
        Null as [Vehicle!2!CurrentAssignmentType]
        

    UNION ALL

    SELECT
        2 as tag,
        1 as parent,
--        Root
        Null,
--        Vehicle List
        IsNull(ca.ClaimAspectNumber, ''),
        IsNull(ca.ClaimAspectID, ''),
        IsNull(cv.VehicleYear, ''),
        IsNull(cv.Make, ''),
        IsNull(cv.Model, ''),
        IsNull((SELECT  Top 1 i.NameFirst
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.NameLast
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        IsNull((SELECT  Top 1 i.BusinessName
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = cv.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), ''),
        CASE
           WHEN dbo.ufnUtilityGetPertainsTo(@ClaimAspectTypeID, ca.ClaimAspectNumber, 0) IN (SELECT EntityCode FROM dbo.ufnUtilityGetClaimEntityList( @LynxID, 1, 2 )) -- 2 = closed
              THEN '1'
           ELSE '0'
        END,
        vs.Name,
        vs.StatusID,
        IsNull(ca.ExposureCD, ''),
        IsNull(ca.CoverageProfileCD, ''),
        --Project:210474 APD Remarked-off the following to support the schema change M.A.20061219
        --(SELECT Name FROM dbo.utb_assignment_type cat WHERE cat.AssignmentTypeID = ca.CurrentAssignmentTypeID)
        --Project:210474 APD Added the following to support the schema change M.A.20061219
        (SELECT Name from dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD') WHERE Code = casc.ServiceChannelCD)
    FROM
        (SELECT @LynxID AS LynxID, @ClaimAspectTypeID AS ClaimAspectTypeID) AS parms
        LEFT JOIN dbo.utb_claim_aspect ca ON (parms.LynxID = ca.LynxID AND parms.ClaimAspectTypeID = ca.ClaimAspectTypeID AND 1 = ca.EnabledFlag)
        LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
		LEFT OUTER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID  --Project:210474 APD Added reference to the table to provide the join between utb_Claim_Aspect & utb_Status, when we did the code merge M.A.20061211
        LEFT JOIN dbo.utb_status vs ON (cas.StatusID = vs.StatusID )
        left outer join utb_Claim_Aspect_Service_Channel casc
        on    ca.ClaimAspectID = casc.ClaimAspectID
        and    casc.PrimaryFlag = 1

    Order by [Vehicle!2!VehicleNumber], tag
    FOR XML EXPLICIT      -- (Commented for Client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimNumberSourceAppGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimNumberSourceAppGetDetailWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientVerifyAssignmentTypeWSXml' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClientVerifyAssignmentTypeWSXml 
END

GO
/****** Object:  StoredProcedure [dbo].[uspClientVerifyAssignmentTypeXml]    Script Date: 09/25/2014 04:29:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClientVerifyAssignmentTypeXml
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspClientVerifyAssignmentTypeWSXml]
    -- (@parameter_name datatype [OUTPUT],...)
    @InsuranceCompanyID         udt_std_id_small,
    @AssignmentTypeID           udt_std_id
AS
BEGIN
    -- TODO: Add SQL statements here
    DECLARE @Msg udt_std_desc_mid
    DECLARE @AssTypeName udt_std_desc_short
    DECLARE @ClientName  udt_std_desc_short
    
	-- 18Feb2013 - TVD - Added join to utb_insurance table
	-- and checks for EnabledFlag.
	IF NOT EXISTS (
		SELECT 
			* 
		FROM 
			utb_client_assignment_type cat
			INNER JOIN utb_insurance i
				ON i.InsuranceCompanyID = cat.InsuranceCompanyID
		WHERE 
			cat.InsuranceCompanyID = @InsuranceCompanyID
            AND cat.AssignmentTypeID = @AssignmentTypeID
            AND EnabledFlag = 1
    )
    BEGIN
        SELECT @ClientName =  Name
        FROM utb_insurance
        WHERE InsuranceCompanyID = @InsuranceCompanyID
        
		-- 25Mar2013 - TVD - Added RRP to the code
        -- For Now we'll hard code Assignment Type Names here.  Eventually we'll tie
        -- it into database name.
        IF @AssignmentTypeID = 4 
        BEGIN 
            SET @AssTypeName = 'Shop Program'
        END
        IF @AssignmentTypeID = 6
        BEGIN
            SET @AssTypeName = 'Desk Audit'
        END
        IF @AssignmentTypeID = 16
        BEGIN
            SET @AssTypeName = 'Repair Referral'
        END
        
        Set @Msg = 'LYNX Services''s ' + @AssTypeName + ' not activated for ' + @ClientName
    END
    ELSE
    BEGIN
        SET @Msg = 'OK'
    END
    
    SELECT 1 as TAG, 0 as Parent, @Msg as [Verify!1!result]
    FOR XML EXPLICIT 
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClientVerifyAssignmentTypeWSXml' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClientVerifyAssignmentTypeWSXml TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimPassthruGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimPassthruGetDetailWSXML 
END

GO
/****** Object:  StoredProcedure [dbo].[uspClaimPassthruGetDetailXML]    Script Date: 09/25/2014 04:31:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimPassthruGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Returns Source Application Pass-thru data
*
* PARAMETERS:  
* (I) @LynxID               The LynxID to retrieve passthru data for
*
* RESULT SET:
*   LynxID                          The LynxID requested
*   ApplicattionCode                The code of the application the data was submitted from
*   ApplicationPassthruData         The passthru data
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure
CREATE PROCEDURE [dbo].[uspClaimPassthruGetDetailWSXML]
    @LynxID     udt_std_id_big
AS
BEGIN
    -- Declare local variables
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspClaimPassthruGetDetailWSXML'


    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    
    -- Begin Select
    SELECT 1                    AS Tag,
           NULL                 AS Parent,
            
           -- Root
           @LynxID              AS [Root!1!LynxID],
            
           -- ClaimAspect
           NULL                 AS [ClaimAspect!2!ClaimAspectID],
           NULL                 AS [ClaimAspect!2!Code],
           NULL                 AS [ClaimAspect!2!SourceApplicationPassThruData]
            
    UNION ALL
    
    SELECT 2,
           1,
           
           -- Root
           NULL,
           
           -- ClaimAspect
           ca.ClaimAspectID,
           a.Code,
           ca.SourceApplicationPassThruData
      FROM dbo.utb_claim c INNER JOIN dbo.utb_claim_aspect ca ON c.LynxID = ca.LynxID
                            LEFT JOIN dbo.utb_application a ON (ca.SourceApplicationID = a.ApplicationID)
      WHERE c.LynxID = @LynxID

      FOR XML EXPLICIT 
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

END

GO

-- Permissions
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimPassthruGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimPassthruGetDetailWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopRetrieveInfoFromAutoverseIdWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShopRetrieveInfoFromAutoverseIdWSXML 
END

GO
/****** Object:  StoredProcedure [dbo].[uspShopRetrieveInfoFromAutoverseIdXML]    Script Date: 09/25/2014 04:37:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspShopRetrieveInfoFromAutoverseIdXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE procedure [dbo].[uspShopRetrieveInfoFromAutoverseIdWSXML]
    @AutoverseID        udt_std_desc_mid,
    @InsuranceCompanyID udt_std_id
as
begin

    declare @ShopLocID      udt_std_id
    declare @AllowSelection udt_std_flag

    declare @WarrantyRefinishMinYrs     udt_std_int_tiny
    declare @WarrantyWorkmanshipMinYrs  udt_std_int_tiny    
    
    
    select @ShopLocID = ShopLocationID
    from utb_shop_location
    where AutoverseID = @AutoverseID
    
    if @ShopLocID is not null
    begin
      
      select @WarrantyRefinishMinYrs = convert(tinyint, WarrantyPeriodRefinishMinCD),
             @WarrantyWorkmanshipMinYrs = convert(tinyint, WarrantyPeriodWorkmanshipMinCD)
      from dbo.utb_insurance
      where InsuranceCompanyID = @InsuranceCompanyID    

      select @AllowSelection = count(sl.ShopLocationID)
      from utb_shop_location sl 
      inner join utb_client_contract_state ccs on (@InsuranceCompanyID = ccs.InsuranceCompanyID and sl.AddressState = ccs.StateCode)
      left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.ExcludeFlag = 1 and InsuranceCompanyID = @InsuranceCompanyID) csle on (sl.ShopLocationID = csle.ShopLocationID)
      left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.IncludeFlag = 1 and InsuranceCompanyID = @InsuranceCompanyID) csli on (sl.ShopLocationID = csli.ShopLocationID)
      where ((ccs.UseCEIShopsFlag = 1 and sl.CEIProgramFlag = 1) or (ccs.UseCEIShopsFlag = 0 and sl.ProgramFlag = 1)) 
        and sl.EnabledFlag = 1
--        and sl.AvailableForSelectionFlag = 1
        and ((@InsuranceCompanyID not in (145, 209) and sl.AvailableForSelectionFlag = 1) 
          OR (@InsuranceCompanyID in (145, 209)))
        and ((@InsuranceCompanyID not in (145, 209) and sl.AddressState <> 'MA') 
          OR (@InsuranceCompanyID in (145, 209)))
        and isnull(convert(tinyint,sl.WarrantyPeriodRefinishCD),0) >= @WarrantyRefinishMinYrs
        and isnull(convert(tinyint,sl.WarrantyPeriodWorkmanshipCD),0) >= @WarrantyWorkmanshipMinYrs
        and csle.InsuranceCompanyID is null
        and (csli.InsuranceCompanyID is null or csli.InsuranceCompanyID = @InsuranceCompanyID)
        and sl.ShopLocationID = @ShopLocID
    end
    
      
    select 1 as Tag,
           0 as Parent,
           @AutoverseID as [Root!1!AutoverseId],
           --
           null as [Shop!2!ShopLocationID],
           null as [Shop!2!Name],
           null as [Shop!2!AddressState],
           null as [Shop!2!AllowSelection]           
    union all
    select 2 as Tag,
           1 as Parent,
           null,
           --           
           ShopLocationID as [Shop!1!ShopLocationID],
           Name,
           AddressState as [Shop!1!AddressState],
           @AllowSelection
    from utb_shop_location
    where ShopLocationID = @ShopLocID

     FOR XML EXPLICIT   
   
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopRetrieveInfoFromAutoverseIdWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopRetrieveInfoFromAutoverseIdWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO



-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopSearchByDistanceWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShopSearchByDistanceWSXML 
END

GO
/****** Object:  StoredProcedure [dbo].[uspShopSearchByDistanceXML]    Script Date: 09/25/2014 04:39:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspShopSearchByDistanceWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Returns program shops within a maximum distance from the specified area code and exchange
*
* PARAMETERS:  
* (I) @Zip                  The Zip code that forms the basis of the search  
* (I) @InsuranceCompanyID   The Insurance Company ID (To process carrier exclusions)
* (I) @ShopTypeCode         (P)rogram, (N)on-program, (A)ll
* (I) @MaxShops             Limits the number of shops returned in the search.  If omitted, no limit is set.
* (I) @UserID               The ID of the user performing the search
* (I) @LogSearchFlag        Flag allowing caller to control whether the search is logged or not.  Used for "internal"
*                               searches fromthe application (such as Shop Maintenance) 
*
* RESULT SET:
* An XML Stream containing shop summary data that matches the search criteria
*
*
* VSS
* $Workfile: uspShopSearchByDistanceXML.SQL $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspShopSearchByDistanceXML.SQL $
* $Revision: 1 $
* $Author: Jonathan $
* $Date: 10/25/01 2:31p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspShopSearchByDistanceWSXML]
    @Zip                    udt_addr_zip_code = Null,
    @InsuranceCompanyID     udt_std_id,
    @ShopTypeCode           varchar(1),
    @MaxShops               udt_std_int = 0,
    @UserID                 udt_std_id = 0,
    @LogSearchFlag          udt_std_flag = 1
AS
BEGIN
    -- Set default values for area code and exchange if blank

    IF LEN(RTRIM(LTRIM(@Zip))) = 0 SET @Zip = NULL


    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @Now               AS datetime

    SET @Now = Current_Timestamp

    SET @ProcName = 'uspShopSearchByDistanceWSXML'

    DECLARE @debug              AS bit

    SET @debug = 0

    IF @debug = 1
    BEGIN
        PRINT 'Parameters:'
        PRINT '    @Zip = ' + IsNull(@Zip, '<Null>')
        PRINT '    @InsuranceCompanyID = ' + Convert(varchar(10), @InsuranceCompanyID)
        PRINT '    @ShopTypeCode = ' + @ShopTypeCode
        PRINT '    @MaxShops = ' + IsNull(Convert(varchar(10), @MaxShops), '<Null>')
        PRINT '    @UserID = ' + Convert(varchar(10), @UserID)
        PRINT '    @LogSearchFlag = ' + Convert(varchar(1), @LogSearchFlag)
    END
    
    
    -- Check to make sure a valid Insurance Company, Shop Type, and User ID was passed in

    IF  (@InsuranceCompanyID IS NULL) OR
        (NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
    
        RAISERROR('101|%s|@InsuranceCompanyID|%u', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END


    IF  (@ShopTypeCode IS NULL) OR
        (Upper(@ShopTypeCode) <> 'P' AND Upper(@ShopTypeCode) <> 'R' AND Upper(@ShopTypeCode) <> 'N' AND Upper(@ShopTypeCode) <> 'A')
    BEGIN
        -- Invalid Shop Type Code
    
        RAISERROR  ('%s: Invalid Shop Code Must be P/R/N/A (Program/NonProgram/All)', 16, 1, @ProcName)
        RETURN
    END


    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Verify that zip code has been passed in
    
    IF (@Zip is Null) OR
       (IsNumeric(@Zip) = 0)
    BEGIN
        -- Invalid paramter
    
        RAISERROR  ('1|Invalid search criteria.  You must enter a numeric Zip Code.', 16, 1)
        RETURN
    END

    
    -- Verify that the zip code is a valid zip code, if not, throw a warning that will get logged
    
    IF NOT EXISTS(SELECT Zip FROM dbo.utb_zip_code WHERE Zip = @Zip)
    BEGIN
        -- Zip Code not found in utb_zip_code.  We want to throw a warning message so it can be logged, but
        -- do not stop processing.
    
        RAISERROR  ('%s: Zip Code %s not found in utb_zip_code.  Search will not return results.', 10, 1, @ProcName, @Zip)
    END
    
    
    -- Make sure @MaxShops is a positive number
    
    IF (@MaxShops IS NOT NULL) AND (@MaxShops < 0)
    BEGIN
        SET @MaxShops = 0
    END
    
     
    -- Create temporary table to hold output records as they are collected

    DECLARE @tmpShopSearch TABLE
    (
	   ShopLocationID              bigint      NOT NULL,
	   Distance                    int         NOT NULL,
       SelectionScore              int         NULL,
	   OperatingFridayEndTime      varchar(4)  NULL,
	   OperatingFridayStartTime    varchar(4)  NULL,
	   OperatingMondayEndTime      varchar(4)  NULL,
	   OperatingMondayStartTime    varchar(4)  NULL,
	   OperatingSaturdayEndTime    varchar(4)  NULL,
	   OperatingSaturdayStartTime  varchar(4)  NULL,
	   OperatingSundayEndTime      varchar(4)  NULL,
	   OperatingSundayStartTime    varchar(4)  NULL,
	   OperatingThursdayEndTime    varchar(4)  NULL,
	   OperatingThursdayStartTime  varchar(4)  NULL,
	   OperatingTuesdayEndTime     varchar(4)  NULL,
	   OperatingTuesdayStartTime   varchar(4)  NULL,
	   OperatingWednesdayEndTime   varchar(4)  NULL,
	   OperatingWednesdayStartTime varchar(4)  NULL,
	   TimeAtShop                  datetime    NULL,
       TodayStartTime              varchar(20) NULL,
       TodayEndTime                varchar(20) NULL,
	   ShopOpen                    varchar(8)  NOT NULL
    )

    DECLARE @tmpShopSearch2 TABLE
    (
       Rank                        int         IDENTITY(1,1), 
	   ShopLocationID              bigint      NOT NULL,
	   Distance                    int         NOT NULL,
       SelectionScore              int         NULL,
	   OperatingFridayEndTime      varchar(4)  NULL,
	   OperatingFridayStartTime    varchar(4)  NULL,
	   OperatingMondayEndTime      varchar(4)  NULL,
	   OperatingMondayStartTime    varchar(4)  NULL,
	   OperatingSaturdayEndTime    varchar(4)  NULL,
	   OperatingSaturdayStartTime  varchar(4)  NULL,
	   OperatingSundayEndTime      varchar(4)  NULL,
	   OperatingSundayStartTime    varchar(4)  NULL,
	   OperatingThursdayEndTime    varchar(4)  NULL,
	   OperatingThursdayStartTime  varchar(4)  NULL,
	   OperatingTuesdayEndTime     varchar(4)  NULL,
	   OperatingTuesdayStartTime   varchar(4)  NULL,
	   OperatingWednesdayEndTime   varchar(4)  NULL,
	   OperatingWednesdayStartTime varchar(4)  NULL,
	   TimeAtShop                  datetime    NULL,
       ShopOpen                    varchar(8)  NOT NULL
    )

    DECLARE @tmpPPGScores TABLE
    (
        PPGCTSLevelCD               varchar(4)  NOT NULL,
        Score                       int         NOT NULL
    )
    
    DECLARE @tmpDistanceScores TABLE
    (
        MinDistance                 int         NOT NULL,
        MaxDistance                 int         NOT NULL,
        Score                       int         NOT NULL
    )
    
    -- Declare local varaibles

    DECLARE @Business1PersonnelTypeID   udt_std_id
    DECLARE @CEIScore                   int
    DECLARE @CertifiedFirstScore        int
    DECLARE @CFLogoDisplayFlag          udt_std_flag
    DECLARE @DefaultShopScore           int
    DECLARE @Distance                   int
    DECLARE @Latitude                   float
    DECLARE @Longitude                  float
    DECLARE @MaxDistance                int
    DECLARE @MinDistance                int
    DECLARE @MinShops                   int
    DECLARE @SearchID                   udt_std_id_big
    DECLARE @SearchIncrement            int
    DECLARE @ShopSelector               varchar(1)
    DECLARE @UseCEIShops                bit
    DECLARE @WarrantyRefinishMinYrs     tinyint
    DECLARE @WarrantyWorkmanshipMinYrs  tinyint    


    -- Validate required APD data

    SELECT  @Business1PersonnelTypeID = PersonnelTypeID
      FROM  dbo.utb_personnel_type
      WHERE Name = 'Shop Manager'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @Business1PersonnelTypeID IS NULL
    BEGIN
       -- Personnel Type Not Found
    
        RAISERROR('102|%s|"Shop Manager"|utb_personnel_type', 16, 1, @ProcName)
        RETURN
    END


    -- Using the MSA for the zip code, get max distance to search within.  If no MaxDistance is returned, set to 50

    SELECT  @MaxDistance = value
    FROM    dbo.utb_app_variable
    WHERE   Name = 'MAX_SHOP_SEARCH_RADIUS'
      AND   SubName = (SELECT CountyType FROM utb_zip_code WHERE Zip = @Zip)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET  @MaxDistance = IsNull(@MaxDistance, 50) 


    -- Get minimum distance of search (starting point of search), if no Minimum distance is returned, set to 5

    SELECT  @MinDistance = value 
    FROM    dbo.utb_app_variable
    WHERE   name = 'MIN_SHOP_SEARCH_RADIUS'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET  @MinDistance = IsNull(@MinDistance, 5)

    
    -- Get minimum number of shops to return, if no MinShops is returned, set to 3

    SELECT  @MinShops = value 
    FROM    dbo.utb_app_variable
    WHERE   name = 'MIN_SHOPS_FOR_SEARCH_COMPLETE'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET  @MinShops = IsNull(@MinShops, 3)

    
    -- Get search distance increment, if no SearchIncrement is returned, set to 5

    SELECT  @SearchIncrement = value
    FROM    dbo.utb_app_variable
    WHERE   name = 'SHOP_SEARCH_INCREMENT'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET  @SearchIncrement = IsNull(@SearchIncrement, 5) 


    -- Set distance start to Min Distance.  If MinDistance is larger than MaxDistance, set distance equal to
    -- Max Distance.  This makes sure the loop is executed at least once.

    IF @MinDistance >= @MaxDistance
    BEGIN
	    SET @Distance = @MaxDistance
    END
    ELSE
    BEGIN
        SET @Distance = @MinDistance
    END

    
    -- Get default shop score, if no default score is returned, set to 75

    SELECT  @DefaultShopScore = value
    FROM    dbo.utb_app_variable
    WHERE   name = 'DEFAULT_POINTS_SCORELESS_SHOP'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET  @DefaultShopScore = IsNull(@DefaultShopScore, 75) 

    
    -- Get Certified First point score, if no default score is returned, set to 30

    SELECT  @CertifiedFirstScore = value
    FROM    dbo.utb_app_variable
    WHERE   name = 'POINTS_CF_SHOP'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET  @CertifiedFirstScore = IsNull(@CertifiedFirstScore, 30) 


    -- Get CEI point score, if no default score is returned, set to 50

    SELECT  @CEIScore = value
    FROM    dbo.utb_app_variable
    WHERE   name = 'POINTS_CEI_SHOP'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET  @CEIScore = IsNull(@CEIScore, 50) 


    -- Get Point values for PPG affiliation.  Default any levels not populated to 10
    
    INSERT INTO @tmpPPGScores
      SELECT  SubName, IsNull(Value, 10)
        FROM  dbo.utb_app_variable
        WHERE Name = 'POINTS_PPG_SHOP'

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpPPGScores', 16, 1, @ProcName)
        RETURN
    END

    
    -- Get Point values for distance.  Default any levels not populated to 0
    
    INSERT INTO @tmpDistanceScores
      SELECT  IsNull(MinRange, 0), 
              IsNull(MaxRange, 99999), 
              IsNull(Value, 0)
        FROM  dbo.utb_app_variable
        WHERE Name = 'POINTS_SHOP_DISTANCE'

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpDistanceScores', 16, 1, @ProcName)
        RETURN
    END

    
    IF @debug = 1
    BEGIN
        PRINT ''
        PRINT 'Values from utb_app_variables:'
        PRINT '    Max Distance = ' + Convert(varchar(9), @MaxDistance)
        PRINT '    Min Distance = ' + Convert(varchar(9), @MinDistance)
        PRINT '    Min Shops = ' + Convert(varchar(9), @MinShops)
        PRINT '    Search Increment = ' + Convert(varchar(9), @SearchIncrement)
        PRINT '    Start Distance = ' + Convert(varchar(9), @Distance)
        PRINT '    Default Shop Score = ' + Convert(varchar(9), @DefaultShopScore)
        PRINT '    Certified First Score = ' + Convert(varchar(9), @CertifiedFirstScore)
        PRINT '    CEI Shop Score = ' + Convert(varchar(9), @CEIScore)
        PRINT '    PPG Refinish Scores = '
        SELECT * FROM @tmpPPGScores
        PRINT '    Distance Scores = '
        SELECT * FROM @tmpDistanceScores
    END


    -- Get Latitude and Longitude for the entered Zip, as well as whether CEI shops will be considered for the state

    SELECT	@Latitude = zc.latitude,
            @Longitude = zc.longitude,
            @UseCEIShops = IsNull((SELECT  ccs.UseCEIShopsFlag 
                                     FROM  dbo.utb_client_contract_state ccs 
                                     WHERE ccs.InsuranceCompanyID = @InsuranceCompanyID 
                                       AND ccs.StateCode = zc.State) , 0)
      FROM	dbo.utb_zip_Code zc
      WHERE	zc.Zip = @Zip

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- If Zip Code does not exist in zip code table, this flag will not be set, set it now to a default 
    -- value to prevent problems later on.
    
    IF @UseCEIShops IS NULL
    BEGIN
        SET @UseCEIShops=0
    END
    
    IF @debug = 1
    BEGIN
        PRINT ''
        PRINT 'From Location:'
        PRINT '    Latitude = ' + Convert(varchar(9), @Latitude)
        PRINT '    Longitude = ' + Convert(varchar(9), @Longitude)
    END

    IF Upper(@ShopTypeCode) = 'P'
    BEGIN
        SET @ShopSelector = '1'
    END
    ELSE IF upper(@ShopTypeCode) = 'N'
    BEGIN
        SET @ShopSelector = '0'
    END
    ELSE IF Upper(@ShopTypeCode) = 'A'
    BEGIN
        SET @ShopSelector = '%'
    END

    IF @Zip is Null SET @Zip = '%'


    -- Get the minimum warranty for the insurance company.  The shop's warranties must meet or exceed the
    -- insurance company's minimums.  If @UseCEIShops is set, the shops of the CEI network will be considered in the shop search

    SELECT  @WarrantyRefinishMinYrs = Convert(tinyint, WarrantyPeriodRefinishMinCD),
            @WarrantyWorkmanshipMinYrs = Convert(tinyint, WarrantyPeriodWorkmanshipMinCD),
            @CFLogoDisplayFlag = CFLogoDisplayFlag
      FROM  dbo.utb_insurance
      WHERE InsuranceCompanyID = @InsuranceCompanyID
      
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF @WarrantyRefinishMinYrs IS NULL
    BEGIN
        -- The carrier had no configuration for a minimum refinish warranty, set minimum to 0
        
        SET @WarrantyRefinishMinYrs = 0
    END
    
    IF @WarrantyWorkmanshipMinYrs IS NULL
    BEGIN
        -- The carrier had no configuration for a minimum workmanship warranty, set minimum to 0
        
        SET @WarrantyWorkmanshipMinYrs = 0
    END
    
        
    IF @debug = 1
    BEGIN
        PRINT ''
        PRINT 'From Carrier:'
        PRINT '    UseCEIShops = ' + Convert(char(1), @UseCEIShops)
        PRINT '    WarrantyRefinishMinYrs = ' + Convert(varchar(2), @WarrantyRefinishMinYrs)
        PRINT '    WarrantyWorkmanshipMinYrs = ' + Convert(varchar(9), @WarrantyWorkmanshipMinYrs)
    END


    -- Time to get the shops.
    -- This algorithm counts the number of qualifying shops and compares it to @MinShops.  If at least @MinShops are found, these
    -- shops are selected in the temporary table for further processing.

    -- In order to qualify, a shop must meet the following criteria:
    --      1)  Distance to the shop from the caller must be within @Distance.  This is determined by a trigonometric formula
    --          which compares the shops and callers longitutude and latitude.
    --      2)  The program/non-program status of the shop must match that requested
    --      3)  If this carrier can use the CEI shop network, a shop may be a CEI Program Shop
    --      4)  If the shop is specifically on a carrier's inclusion list, they will be considered
    --      4)  The shop must be enabled
    --      5)  The shop must be available for selection (for various reasons, an enabled program shop could be temporarily 
    --          turned off from receiving assignments eg. Local disaster, not accepting any more work, etc.)
    --      5)  The shop's workmanship and refinish warranties must meet or exceed the requirements of the insurance company
    --      6)  The shop must not be on the exclusion list given to us by the Insurance Company
    --      7)  The shop must NOT be located in Massachusetts (LYNX is unable to assign work to shops in this state)

    -- If less then @MinShops qualify, @Distance is increased by @SearchIncrement and the test repeated until at least @MinShops qualifying
    -- shops are found or @MaxDistance is reached.

    IF @debug = 1
    BEGIN
        PRINT ''
        PRINT 'Shops Considered ='
        SELECT  sl.ShopLocationID,
                CASE
                  WHEN sl.CEIProgramFlag = 1 AND @UseCEIShops = 1 THEN 'CEI'
                  WHEN sl.ProgramFlag = 1 THEN 'LS'
                  ELSE 'NON'
                END AS ProgramCD,
                IsNull(sl.Latitude, zc.Latitude) AS LatitudeUsed,
                IsNull(sl.Longitude, zc.Longitude) AS LongitudeUsed,
                sqrt(  square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) +
                                   square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @longitude) * cos(@latitude / 57.3))  ) AS Distance
          FROM  dbo.utb_shop_location sl
          INNER JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.zip)
          LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.ExcludeFlag = 1 AND InsuranceCompanyID = @InsuranceCompanyID) csle ON (sl.ShopLocationID = csle.ShopLocationID)
          LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.IncludeFlag = 1) csli ON (sl.ShopLocationID = csli.ShopLocationID)
          WHERE sqrt(  square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) +
                           square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @longitude) * cos(@latitude / 57.3))  ) <= @MaxDistance 
            --AND ((@UseCEIShops = 1 AND sl.CEIProgramFlag = 1) OR convert(varchar(3), sl.ProgramFlag) LIKE @ShopSelector)            
            AND ((Upper(@ShopTypeCode) = 'N' AND sl.ProgramFlag = 0 ) OR
                 (Upper(@ShopTypeCode) = 'A' AND sl.ProgramFlag in (0,1)) OR
                 (Upper(@ShopTypeCode) = 'R' AND sl.ReferralFlag = 1 ) OR
                 (Upper(@ShopTypeCode) = 'P' AND (sl.ProgramFlag = 1 OR (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1))))            
            AND sl.EnabledFlag = 1
            AND sl.AvailableForSelectionFlag = 1
            AND sl.AddressState <> 'MA'
            AND IsNull(Convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
            AND IsNull(Convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
            AND csle.InsuranceCompanyID IS NULL
            AND (csli.InsuranceCompanyID = @InsuranceCompanyID OR csli.InsuranceCompanyID IS NULL)
    END


    WHILE @Distance <= @MaxDistance
    BEGIN
		IF 	((SELECT  COUNT(*)
          FROM  dbo.utb_shop_location sl
          INNER JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.zip)
          LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.ExcludeFlag = 1 AND InsuranceCompanyID = @InsuranceCompanyID) csle ON (sl.ShopLocationID = csle.ShopLocationID)
          LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.IncludeFlag = 1) csli ON (sl.ShopLocationID = csli.ShopLocationID)
          WHERE sqrt(  square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) +
                           square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @longitude) * cos(@latitude / 57.3))  ) <= @distance 
            --AND ((@UseCEIShops = 1 AND sl.CEIProgramFlag = 1) OR convert(varchar(3), sl.ProgramFlag) LIKE @ShopSelector)            
            AND ((Upper(@ShopTypeCode) = 'N' AND sl.ProgramFlag = 0 ) OR
                 (Upper(@ShopTypeCode) = 'A' AND sl.ProgramFlag in (0,1)) OR
                 (Upper(@ShopTypeCode) = 'R' AND sl.ReferralFlag = 1 ) OR
                 (Upper(@ShopTypeCode) = 'P' AND (sl.ProgramFlag = 1 OR (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1))))            
            AND sl.EnabledFlag = 1
            AND sl.AvailableForSelectionFlag = 1
            AND sl.AddressState <> 'MA'
            AND IsNull(Convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
            AND IsNull(Convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
            AND csle.InsuranceCompanyID IS NULL
            AND (csli.InsuranceCompanyID = @InsuranceCompanyID OR csli.InsuranceCompanyID IS NULL))
                                                                    >= @MinShops)  -- If @MinShops or more shops are within distance
		  OR
		    (@Distance + @SearchIncrement > @MaxDistance)     --If we are on our last loop
   		BEGIN
            -- Perform shop selection, with an initial selection score that takes into account everything but
            -- distance.  Insert the results into the temporary table.
            
            INSERT INTO	@tmpShopSearch
              SELECT	sl.ShopLocationId,
			     		Convert(int,
                                   sqrt(square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) + 
            			     	     square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @Longitude) * cos(IsNull(sl.Latitude, zc.Latitude) / 57.3)))  ) AS Distance,
                        -- Begin selection score logic
                          CASE  -- Shop Performance score
                            WHEN IsNull(sl.ProgramScore, 0) > 0 THEN sl.ProgramScore
                            ELSE @DefaultShopScore
                          END + 
                          CASE  -- PPG Affiliation
                            WHEN sl.CertifiedFirstFlag = 1 THEN @CertifiedFirstScore
                            WHEN sl.PPGCTSFlag = 1 THEN IsNull((SELECT Score FROM @tmpPPGScores WHERE PPGCTSLevelCD = sl.PPGCTSLevelCD), 10)    -- Default unknown levels to 10
                            ELSE 0
                          END + 
                          CASE  -- CEI Shop
                            WHEN sl.CEIProgramFlag = 1 AND @UseCEIShops = 1 THEN @CEIScore
                            ELSE 0
                          END AS SelectionScore,
                        -- End selection score logic
					    slh.OperatingFridayEndTime,
					    slh.OperatingFridayStartTime,
					    slh.OperatingMondayEndTime,
					    slh.OperatingMondayStartTime,
					    slh.OperatingSaturdayEndTime,
					    slh.OperatingSaturdayStartTime,
					    slh.OperatingSundayEndTime,
					    slh.OperatingSundayStartTime,
					    slh.OperatingThursdayEndTime,
					    slh.OperatingThursdayStartTime,
					    slh.OperatingTuesdayEndTime,
					    slh.OperatingTuesdayStartTime,
					    slh.OperatingWednesdayEndTime,
					    slh.OperatingWednesdayStartTime,
                        dbo.ufnUtilityGetLocalTime( GetUTCDate(), sl.AddressZip ) as TimeAtShop,
                        NULL,
                        NULL,
                        'Unknown' as ShopOpen
                FROM   dbo.utb_shop_location sl
                INNER JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.zip)
                LEFT JOIN dbo.utb_shop_location_hours slh ON (sl.ShopLocationID = slh.ShopLocationID)
                LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.ExcludeFlag = 1 AND InsuranceCompanyID = @InsuranceCompanyID) csle ON (sl.ShopLocationID = csle.ShopLocationID)
                LEFT JOIN (SELECT csl.ShopLocationID, csl.InsuranceCompanyID FROM dbo.utb_client_shop_location csl WHERE csl.IncludeFlag = 1) csli ON (sl.ShopLocationID = csli.ShopLocationID)
                WHERE sqrt(  square(69.1 * (IsNull(sl.Latitude, zc.Latitude) - @Latitude)) +
                             square(69.1 * (IsNull(sl.Longitude, zc.Longitude) - @longitude) * cos(@latitude / 57.3))  ) <= @distance 
                  --AND ((@UseCEIShops = 1 AND sl.CEIProgramFlag = 1) OR convert(varchar(3), sl.ProgramFlag) LIKE @ShopSelector)            
                AND ((Upper(@ShopTypeCode) = 'N' AND sl.ProgramFlag = 0 ) OR
                     (Upper(@ShopTypeCode) = 'A' AND sl.ProgramFlag in (0,1)) OR
                     (Upper(@ShopTypeCode) = 'R' AND sl.ReferralFlag = 1 ) OR
                     (Upper(@ShopTypeCode) = 'P' AND (sl.ProgramFlag = 1 OR (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1))))                  
                  AND sl.EnabledFlag = 1
                  AND sl.AvailableForSelectionFlag = 1
                  AND sl.AddressState <> 'MA'
                  AND IsNull(Convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
                  AND IsNull(Convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
                  AND csle.InsuranceCompanyID IS NULL
                  AND (csli.InsuranceCompanyID = @InsuranceCompanyID OR csli.InsuranceCompanyID IS NULL)

            IF @@ERROR <> 0
            BEGIN
                -- Insertion failure
            
                RAISERROR('105|%s|@tmpShopSearch', 16, 1, @ProcName)
                RETURN
            END

			BREAK
		END
		ELSE
        BEGIN
            -- There were less than @MinShops shops within the distance, increase distance by @SearchIncrement until it
            -- reaches @MaxDistance miles

			SET @Distance = @Distance + @SearchIncrement
        END
	END


    -- Update the selection score for distance, and additional updates for shop open/close determination
     
    UPDATE  @tmpShopSearch
      SET   SelectionScore =    SelectionScore + IsNull((SELECT TOP 1 Score FROM @tmpDistanceScores WHERE Distance >= MinDistance AND Distance < MaxDistance ORDER BY Score DESC), 0),  -- Default to 0 if record isn't found
            TodayStartTime =    CASE datepart(dw, TimeAtShop)
                                  WHEN 1 THEN CASE  
                                                WHEN IsDate(Left(OperatingSundayStartTime, 2) + ':' + Right(OperatingSundayStartTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingSundayStartTime, 2) + ':' + Right(OperatingSundayStartTime, 2)
                                              END
                                  WHEN 2 THEN CASE
                                                WHEN IsDate(Left(OperatingMondayStartTime, 2) + ':' + Right(OperatingMondayStartTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingMondayStartTime, 2) + ':' + Right(OperatingMondayStartTime, 2)
                                              END
                                  WHEN 3 THEN CASE
                                                WHEN IsDate(Left(OperatingTuesdayStartTime, 2) + ':' + Right(OperatingTuesdayStartTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingTuesdayStartTime, 2) + ':' + Right(OperatingTuesdayStartTime, 2)
                                              END
                                  WHEN 4 THEN CASE
                                                WHEN IsDate(Left(OperatingWednesdayStartTime, 2) + ':' + Right(OperatingWednesdayStartTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingWednesdayStartTime, 2) + ':' + Right(OperatingWednesdayStartTime, 2)
                                              END
                                  WHEN 5 THEN CASE
                                                WHEN IsDate(Left(OperatingThursdayStartTime, 2) + ':' + Right(OperatingThursdayStartTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingThursdayStartTime, 2) + ':' + Right(OperatingThursdayStartTime, 2)
                                              END
                                  WHEN 6 THEN CASE
                                                WHEN IsDate(Left(OperatingFridayStartTime, 2) + ':' + Right(OperatingFridayStartTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingFridayStartTime, 2) + ':' + Right(OperatingFridayStartTime, 2)
                                              END
                                  WHEN 7 THEN CASE
                                                WHEN IsDate(Left(OperatingSaturdayStartTime, 2) + ':' + Right(OperatingSaturdayStartTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingSaturdayStartTime, 2) + ':' + Right(OperatingSaturdayStartTime, 2)
                                              END
                                END,
            TodayEndTime =      CASE datepart(dw, TimeAtShop)
                                  WHEN 1 THEN CASE
                                                WHEN IsDate(Left(OperatingSundayEndTime, 2) + ':' + Right(OperatingSundayEndTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingSundayEndTime, 2) + ':' + Right(OperatingSundayEndTime, 2)
                                              END
                                  WHEN 2 THEN CASE
                                                WHEN IsDate(Left(OperatingMondayEndTime, 2) + ':' + Right(OperatingMondayEndTime, 2)) = 0 THEN NULL 
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingMondayEndTime, 2) + ':' + Right(OperatingMondayEndTime, 2)
                                              END
                                  WHEN 3 THEN CASE
                                                WHEN IsDate(Left(OperatingTuesdayEndTime, 2) + ':' + Right(OperatingTuesdayEndTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingTuesdayEndTime, 2) + ':' + Right(OperatingTuesdayEndTime, 2)
                                              END
                                  WHEN 4 THEN CASE
                                                WHEN IsDate(Left(OperatingWednesdayEndTime, 2) + ':' + Right(OperatingWednesdayEndTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingWednesdayEndTime, 2) + ':' + Right(OperatingWednesdayEndTime, 2)
                                              END
                                  WHEN 5 THEN CASE
                                                WHEN IsDate(Left(OperatingThursdayEndTime, 2) + ':' + Right(OperatingThursdayEndTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingThursdayEndTime, 2) + ':' + Right(OperatingThursdayEndTime, 2)
                                              END
                                  WHEN 6 THEN CASE
                                                WHEN IsDate(Left(OperatingFridayEndTime, 2) + ':' + Right(OperatingFridayEndTime, 2)) = 0 THEN NULL
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingFridayEndTime, 2) + ':' + Right(OperatingFridayEndTime, 2)
                                              END
                                  WHEN 7 THEN CASE
                                                WHEN IsDate(Left(OperatingSaturdayEndTime, 2) + ':' + Right(OperatingSaturdayEndTime, 2)) = 0 THEN NULL 
                                                ELSE Convert(varchar(10), TimeAtShop, 101) + ' ' + Left(OperatingSaturdayEndTime, 2) + ':' + Right(OperatingSaturdayEndTime, 2)
                                              END
                                END

    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|@tmpShopSearch', 16, 1, @ProcName)
        RETURN
    END


    -- Final update for shop open/close determination
    
    UPDATE  @tmpShopSearch
      SET   ShopOpen = CASE
                         WHEN TodayStartTime IS NULL OR TodayEndTime IS NULL THEN 'Unknown'
                         WHEN TimeAtShop BETWEEN Convert(datetime, TodayStartTime) AND Convert(datetime, TodayEndTime) THEN 'Open'
                         ELSE 'Closed'
                       END

    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|@tmpShopSearch', 16, 1, @ProcName)
        RETURN
    END


    -- Compile with identity for final select.  If @MaxShops is specified, tell SQL to only return @MaxShops rows
            
    IF @MaxShops IS NOT NULL
    BEGIN
        SET ROWCOUNT @MaxShops
    END
    ELSE
    BEGIN
        SET ROWCOUNT 0
    END
            
    
    INSERT INTO @tmpShopSearch2
    (
	   ShopLocationID,
	   Distance,
       SelectionScore,
	   OperatingFridayEndTime,
	   OperatingFridayStartTime,
	   OperatingMondayEndTime,
	   OperatingMondayStartTime,
	   OperatingSaturdayEndTime,
	   OperatingSaturdayStartTime,
	   OperatingSundayEndTime,
	   OperatingSundayStartTime,
	   OperatingThursdayEndTime,
	   OperatingThursdayStartTime,
	   OperatingTuesdayEndTime,
	   OperatingTuesdayStartTime,
	   OperatingWednesdayEndTime,
	   OperatingWednesdayStartTime,
	   TimeAtShop,
	   ShopOpen
    )
    SELECT ShopLocationID,
           Distance,
           SelectionScore,
	       OperatingFridayEndTime,
	       OperatingFridayStartTime,
	       OperatingMondayEndTime,
	       OperatingMondayStartTime,
	       OperatingSaturdayEndTime,
	       OperatingSaturdayStartTime,
	       OperatingSundayEndTime,
	       OperatingSundayStartTime,
	       OperatingThursdayEndTime,
	       OperatingThursdayStartTime,
	       OperatingTuesdayEndTime,
	       OperatingTuesdayStartTime,
	       OperatingWednesdayEndTime,
	       OperatingWednesdayStartTime,
	       TimeAtShop,
	       ShopOpen
      FROM @tmpShopSearch
      ORDER BY SelectionScore DESC, Distance, ShopLocationID


    -- Cancel any previous SET ROWCOUNT
    
    IF @MaxShops IS NOT NULL
    BEGIN
        SET ROWCOUNT 0
    END


    IF @debug = 1
    BEGIN
        PRINT ''
        PRINT 'Final Distance = ' + Convert(varchar(9), @Distance)
        PRINT 'Shops Selected ='
        SELECT * FROM @tmpShopSearch2
    END
    

    -- Log the search attempt if the search was conducted for a non-demo company and @LogSearchFlag = 1
    
    IF ((SELECT DemoFlag FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID) = 0) AND 
        (@LogSearchFlag = 1)
    BEGIN    
        INSERT INTO dbo.utb_shop_search_log 
        (
            CEIShopsIncludedFlag, 
            InsuranceCompanyID, 
            ResultCount,
            SearchUserID, 
            SearchCriteria,
            SearchDate, 
            SearchTypeCD,
            SysLastUserID, 
            SysLastUpdatedDate
        )
          SELECT @UseCEIShops,
                 @InsuranceCompanyID,
                 Count(*),
                 @UserID,
                 @Zip + '; ' + @ShopTypeCode,
                 @Now,
                 'D',
                 @UserID,
                 @Now
            FROM @tmpShopSearch2
        
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure
    
            RAISERROR('105|%s|@utb_shop_search_log', 16, 1, @ProcName)
            RETURN
        END
        ELSE
        BEGIN
            -- Get identity of search
        
            SET @SearchID = SCOPE_IDENTITY()
        END
    
    
        -- Log the search results (if turned on)
    
        IF (SELECT value FROM dbo.utb_app_variable WHERE Name = 'LOG_SHOP_SEARCH_RESULTS') = 'ON'
        BEGIN 
            INSERT INTO dbo.utb_shop_search_results_log
              SELECT  @SearchID,
                      tmp.ShopLocationID,
                      tmp.Distance,
                      CASE  -- CEI Shop
                        WHEN sl.CEIProgramFlag = 1 AND @UseCEIShops = 1 THEN @CEIScore
                        ELSE 0
                      END,
                      IsNull((SELECT TOP 1 Score FROM @tmpDistanceScores WHERE Distance >= MinDistance AND tmp.Distance < MaxDistance ORDER BY Score DESC), 0), -- Defaulted to 0 for not found records
                      CASE  -- Shop Performance score
                        WHEN IsNull(sl.ProgramScore, 0) > 0 THEN sl.ProgramScore
                        ELSE @DefaultShopScore
                      END, 
                      CASE  -- PPG Affiliation
                        WHEN sl.CertifiedFirstFlag = 1 THEN @CertifiedFirstScore
                        WHEN sl.PPGCTSFlag = 1 THEN IsNull((SELECT Score FROM @tmpPPGScores WHERE PPGCTSLevelCD = sl.PPGCTSLevelCD), 10)    -- Default unknown levels to 10
                        ELSE 0
                      END,
                      SelectionScore,
                      CASE  -- PPG Affiliation
                        WHEN sl.CertifiedFirstFlag = 1 THEN 'CF'
                        WHEN sl.PPGCTSFlag = 1 THEN IsNull(sl.PPGCTSLevelCD, '')
                        ELSE ''
                      END,
                      tmp.Rank,
                      @UserID,
                      @Now
                FROM  @tmpShopSearch2 tmp
                LEFT JOIN dbo.utb_shop_location sl ON (tmp.ShopLocationID = sl.ShopLocationID)
                       
            IF @@ERROR <> 0
            BEGIN
                -- Insertion failure
    
                RAISERROR('105|%s|@utb_shop_search_results_log', 16, 1, @ProcName)
                RETURN
            END                 
        END
    END
    ELSE
    BEGIN    
        -- This is a demo company, return a dummy Search ID
        
        SET @SearchID = 0
    END
    

    -- Select for XML
    -- Select Root Level
    SELECT  1 AS Tag,
            NULL AS Parent,
            @SearchID AS [Root!1!ShopSearchLogID],
            @InsuranceCompanyID AS [Root!1!InsuranceCompanyID],
            @Zip as [Root!1!ZipCode],
            @ShopTypeCode as [Root!1!ShopTypeCode],
            (SELECT AssignmentAtSelectionFlag FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID) AS [Root!1!AssignmentAtSelection],
            -- Shop Information
	        0 AS [Shop!2!SelectedShopRank], -- Value Necessary  to bubble the 1 tag to the top of the list so XML can be generated
            NULL AS [Shop!2!Distance],
	        NULL AS [Shop!2!ShopLocationID],
	        NULL AS [Shop!2!SelectedShopScore],      
	        NULL AS [Shop!2!Address1],
	        NULL AS [Shop!2!Address2],
	        NULL AS [Shop!2!AddressCity],
	        NULL AS [Shop!2!AddressState],
	        NULL AS [Shop!2!AddressZip],
	        NULL AS [Shop!2!DrivingDirections],
	        NULL AS [Shop!2!ShopType],
	        NULL AS [Shop!2!Name],
	        NULL AS [Shop!2!OperatingFridayEndTime],
	        NULL AS [Shop!2!OperatingFridayStartTime],
	        NULL AS [Shop!2!OperatingMondayEndTime],
	        NULL AS [Shop!2!OperatingMondayStartTime],
	        NULL AS [Shop!2!OperatingSaturdayEndTime],
	        NULL AS [Shop!2!OperatingSaturdayStartTime],
	        NULL AS [Shop!2!OperatingSundayEndTime],
	        NULL AS [Shop!2!OperatingSundayStartTime],
	        NULL AS [Shop!2!OperatingThursdayEndTime],
	        NULL AS [Shop!2!OperatingThursdayStartTime],
	        NULL AS [Shop!2!OperatingTuesdayEndTime],
	        NULL AS [Shop!2!OperatingTuesdayStartTime],
	        NULL AS [Shop!2!OperatingWednesdayEndTime],
	        NULL AS [Shop!2!OperatingWednesdayStartTime],
	        NULL AS [Shop!2!PhoneAreaCode],
	        NULL AS [Shop!2!PhoneExchangeNumber],
	        NULL AS [Shop!2!PhoneUnitNumber],
	        NULL AS [Shop!2!PhoneExtensionNumber],
	        NULL AS [Shop!2!FaxAreaCode],
	        NULL AS [Shop!2!FaxExchangeNumber],
	        NULL AS [Shop!2!FaxUnitNumber],
	        NULL AS [Shop!2!FaxExtensionNumber],
	        NULL AS [Shop!2!ShopOpen],
	        NULL AS [Shop!2!TimeAtShop],
	        NULL AS [Shop!2!TimeZone],
	        NULL AS [Shop!2!WarrantyPeriodRefinishCD],
	        NULL AS [Shop!2!WarrantyPeriodWorkmanshipCD],
            NULL AS [Shop!2!CertifiedFirstFlag],
            -- Shop Contact
            NULL AS [ShopContact!3!Name],
            NULL AS [ShopContact!3!PhoneAreaCode],
            NULL AS [ShopContact!3!PhoneExchangeNumber],
            NULL AS [ShopContact!3!PhoneExtensionNumber],
            NULL AS [ShopContact!3!PhoneUnitNumber]


    UNION ALL


    -- Select Record level
    SELECT  2,
            1,
            NULL, NULL, NULL, NULL, NULL,
            -- Shop Information
            tmp.Rank,
	        tmp.Distance,
	        tmp.ShopLocationID,
            tmp.SelectionScore,
	        IsNull(LTrim(RTrim(sl.Address1)), ''),
	        IsNull(LTrim(RTrim(sl.Address2)), ''),
	        IsNull(LTrim(RTrim(sl.AddressCity)), ''),
	        IsNull(LTrim(RTrim(sl.AddressState)), ''),
	        IsNull(LTrim(RTrim(sl.AddressZip)), ''),
	        IsNull(LTrim(RTrim(sl.DrivingDirections)), ''),
            --CASE 
               --WHEN (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1)  THEN 'CEI'
               --WHEN sl.ProgramFlag = 1  THEN 'Program'
               --ELSE 'Non Program'
            --END,
			CASE WHEN 
				(Replace(Ltrim(Rtrim((IsNull((CASE WHEN (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1)  THEN 'C ' END), '') + 
					IsNull((CASE WHEN (sl.ProgramFlag = 1)  THEN 'D ' END), '') + 
					IsNull((CASE WHEN (sl.ReferralFlag = 1)  THEN 'R ' END), '')))), ' ', ', ')) = '' THEN 'N/A' 
			ELSE
				(Replace(Ltrim(Rtrim((IsNull((CASE WHEN (@UseCEIShops = 1 AND sl.CEIProgramFlag = 1)  THEN 'C ' END), '') + 
					IsNull((CASE WHEN (sl.ProgramFlag = 1)  THEN 'D ' END), '') + 
					IsNull((CASE WHEN (sl.ReferralFlag = 1)  THEN 'R ' END), '')))), ' ', ', '))
			END,
	
	        IsNull(LTrim(RTrim(sl.Name)), ''),
	        IsNull(tmp.OperatingFridayEndTime, ''),
	        IsNull(tmp.OperatingFridayStartTime, ''),
	        IsNull(tmp.OperatingMondayEndTime, ''),
	        IsNull(tmp.OperatingMondayStartTime, ''),
	        IsNull(tmp.OperatingSaturdayEndTime, ''),
	        IsNull(tmp.OperatingSaturdayStartTime, ''),
	        IsNull(tmp.OperatingSundayEndTime, ''),
	        IsNull(tmp.OperatingSundayStartTime, ''),
	        IsNull(tmp.OperatingThursdayEndTime, ''),
	        IsNull(tmp.OperatingThursdayStartTime, ''),
	        IsNull(tmp.OperatingTuesdayEndTime, ''),
	        IsNull(tmp.OperatingTuesdayStartTime, ''),
	        IsNull(tmp.OperatingWednesdayEndTime, ''),
	        IsNull(tmp.OperatingWednesdayStartTime, ''),
	        IsNull(LTrim(RTrim(sl.PhoneAreaCode)), ''),
	        IsNull(LTrim(RTrim(sl.PhoneExchangeNumber)), ''),
	        IsNull(LTrim(RTrim(sl.PhoneUnitNumber)), ''),
	        IsNull(LTrim(RTrim(sl.PhoneExtensionNumber)), ''),
	        IsNull(LTrim(RTrim(sl.FaxAreaCode)), ''),
	        IsNull(LTrim(RTrim(sl.FaxExchangeNumber)), ''),
	        IsNull(LTrim(RTrim(sl.FaxUnitNumber)), ''),
	        IsNull(LTrim(RTrim(sl.FaxExtensionNumber)), ''),
	        IsNull(tmp.ShopOpen, ''),
	        IsNull(tmp.TimeAtShop, ''),
	        IsNull(tz.Name, ''),
            IsNull(sl.WarrantyPeriodRefinishCD, ''),
            IsNull(sl.WarrantyPeriodWorkmanshipCD, ''),
            CASE @CFLogoDisplayFlag
              WHEN 1 THEN IsNull(sl.CertifiedFirstFlag, 0)
              ELSE 0
            END,
            -- Shop Contact
            NULL, NULL, NULL, NULL, NULL

    FROM    @tmpShopSearch2 tmp
    LEFT JOIN dbo.utb_shop_location sl ON (tmp.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN dbo.utb_zip_code zc ON (sl.AddressZip = zc.Zip)
    LEFT JOIN dbo.utb_time_zone tz ON (zc.TimeZone = tz.TimeZone)


    UNION ALL


    -- Select the Shop Contact Level

    SELECT  3,
            2,
            NULL, NULL, NULL, NULL, NULL,
            -- Shop Information
            tmp.Rank,
	        tmp.Distance,
	        tmp.ShopLocationID,
            tmp.SelectionScore,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            -- Shop Contact
            IsNull(LTrim(RTrim(p.Name)), ''),
            IsNull(LTrim(RTrim(p.PhoneAreaCode)), ''),
            IsNull(LTrim(RTrim(p.PhoneExchangeNumber)), ''),
            IsNull(LTrim(RTrim(p.PhoneExtensionNumber)), ''),
            IsNull(LTrim(RTrim(p.PhoneUnitNumber)), '')

    FROM    @tmpShopSearch2 tmp
    LEFT JOIN dbo.utb_shop_location_personnel slp ON (tmp.ShopLocationID = slp.ShopLocationID)
    LEFT JOIN dbo.utb_personnel p ON (slp.PersonnelID = p.PersonnelID)
    LEFT JOIN dbo.utb_personnel_type pt ON (p.PersonnelTypeID = pt.PersonnelTypeID)
    WHERE pt.PersonnelTypeID = @Business1PersonnelTypeID


    Order by [Shop!2!SelectedShopRank], Tag
    FOR XML EXPLICIT      -- (Comment for Client-side XML generation)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopSearchByDistanceWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopSearchByDistanceWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateChangesGetDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspEstimateChangesGetDetailWSXML 
END
GO
/****** Object:  StoredProcedure [dbo].[uspEstimateChangesGetDetailWSXML]    Script Date: 09/25/2014 04:42:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateChangesGetDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
* UPDATE:		Update for SQL 2008 By Mahes
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspEstimateChangesGetDetailWSXML]
(
    @DocumentID         udt_std_id_big,
--    @ShopLocationID     udt_std_id_big,
    @UserID             udt_std_id_big,
    @EstimateChanges    varchar(8000)
)
AS
BEGIN
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                AS datetime
    DECLARE @LineItem           AS varchar(8000)
    DECLARE @Lynxid             AS udt_std_id_big
    DECLARE @ShopName           AS varchar(200)
    DECLARE @ShopFaxNumber      AS varchar(10)
    DECLARE @ClaimAnalyst       AS varchar(100)
    DECLARE @ClaimAnalystPhone  AS varchar(15)
    DECLARE @VehicleOwner       AS varchar(100)
    DECLARE @VehicleDescription AS varchar(150)
    DECLARE @PertainsTo         AS varchar(10)
    DECLARE @LogonID            AS udt_sys_login
    DECLARE @DocumentDesc       AS varchar(100)
    DECLARE @DocumentCreatedOn  AS datetime

    SET @ProcName = 'uspEstimateChangesGetDetailWSXML'


    -- Set Database options
    
    SET NOCOUNT ON
    
    -- Validate the inputs
    IF @DocumentID is NULL OR NOT EXISTS(SELECT DocumentID
                                            FROM dbo.utb_document
                                            WHERE DocumentID = @DocumentID)
    BEGIN
        -- Invalid Document ID
    
        RAISERROR('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
        RETURN        
    END

    /*IF @ShopLocationID is NULL OR NOT EXISTS(SELECT ShopLocationID
                                                FROM dbo.utb_shop_location
                                                WHERE ShopLocationID = @ShopLocationID)
    BEGIN
        -- Invalid Shop Location ID
    
        RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
        RETURN        
    END*/
    
    IF LEN(LTRIM(RTRIM(@EstimateChanges))) = 0
    BEGIN
        -- Invalid Estimate Changes
    
        RAISERROR('101|%s|@EstimateChanges|%u', 16, 1, @ProcName, @EstimateChanges)
        RETURN        
    END
    
    IF @UserID is NULL OR NOT EXISTS(SELECT UserID
                                        FROM dbo.utb_user
                                        WHERE UserID = @UserID)
    BEGIN
        -- Invalid UserID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN        
    END

    SET @now = CURRENT_TIMESTAMP
    
    DECLARE @tblEstimateChanges TABLE (
        LineItem  varchar(8000)  NOT NULL
    )
    
    DECLARE @tblEstimateChangesLineItems TABLE (
        LineNumber      varchar(50)     NULL,  --Project:210474 APD Modified the datatype definition when we did the code merge M.A.20061117
        ChangesText     varchar(7950)   NULL  --Project:210474 APD Modified the datatype definition when we did the code merge M.A.20061117
    )
    
    -- The Estimate changes will be of the following format
    -- Line1~Description1||Line2~Description2
    -- Each line will be delimited by double pipe and within each line
    -- the line number and the text will be delimited by tilde ~.
    
    -- Extract each line of changes
    INSERT INTO @tblEstimateChanges
    SELECT value FROM dbo.ufnUtilityParseString(@EstimateChanges, '|', 1)
    
    -- Extract the individual columns for each line
    DECLARE curLineItems CURSOR FOR
    SELECT * FROM @tblEstimateChanges
    
    OPEN curLineItems

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM curLineItems
    INTO @LineItem

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
    
        INSERT INTO @tblEstimateChangesLineItems
        SELECT (SELECT value FROM dbo.ufnUtilityParseString(@LineItem, '~', 1) WHERE strIndex = 1),
               (SELECT value FROM dbo.ufnUtilityParseString(@LineItem, '~', 1) WHERE strIndex = 2)
        
        FETCH next
        FROM curLineItems
        INTO @LineItem

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    

    END

    CLOSE curLineItems
    DEALLOCATE curLineItems
    
    -- Get the claim information
    SELECT @LynxID = ca.LynxID,
           @PertainsTo = cat.Code + convert(varchar, ca.ClaimAspectNumber),
           @ClaimAnalyst = isNull(u.NameFirst, '') + ' ' + isNull(u.NameLast, ''),
           @ClaimAnalystPhone = '(' + isNull(u.PhoneAreaCode, '') + ') ' + isNull(u.PhoneExchangeNumber, '') + '-' + isNull(u.PhoneUnitNumber, '') + isNull('x' + u.PhoneExtensionNumber, ''),
           @VehicleDescription = isNull(convert(varchar, cv.VehicleYear), '') + ' ' + isNull(cv.Make, '') + ' ' + isNull(cv.Model, ''),
           @VehicleOwner = IsNull((SELECT  Top 1 CASE 
                                                    WHEN i.BusinessName IS NOT NULL THEN i.BusinessName
                                                    ELSE isNull(i.NameFirst, '')  + ' ' + isNull(i.NameLast, '')
                                                 END 
                                       FROM  dbo.utb_claim_aspect_involved cai
                                       LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                                       LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                                       LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                                       WHERE cai.ClaimAspectID = cv.ClaimAspectID
                                         AND cai.EnabledFlag = 1
                                         AND irt.Name = 'Owner'), ''),
            @DocumentDesc = (CASE
                                WHEN dt.Name = 'Supplement' THEN dt.Name + ' ' + convert(varchar, d.SupplementSeqNumber)
                                ELSE dt.Name
                               END),
            @DocumentCreatedOn = d.CreatedDate,
            @ShopName = sl.Name,
            @ShopFaxNumber = sl.FaxAreaCode + sl.FaxExchangeNumber + sl.FaxUnitNumber
    FROM dbo.utb_document d 
    LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON (d.DocumentID = cascd.DocumentID)--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    Left OUTER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)--Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
    LEFT JOIN dbo.utb_claim_vehicle cv ON (casc.ClaimAspectID = cv.ClaimAspectID)
    LEFT JOIN dbo.utb_assignment a ON (d.AssignmentID = a.AssignmentID)
    LEFT JOIN dbo.utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN dbo.utb_user u ON (ca.AnalystUserID = u.UserID)  --Project:210474 APD Modified the joins when we did the code merge M.A.20061117
    LEFT JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
    WHERE d.DocumentID = @DocumentID
    
/*    SELECT @ShopName = Name,
           @ShopFaxNumber = FaxAreaCode + FaxExchangeNumber + FaxUnitNumber
    FROM dbo.utb_shop_location
    WHERE ShopLocationID = @ShopLocationID*/
    
    SELECT @LogonID = LogonID
    FROM dbo.utb_user_application ua
    LEFT JOIN dbo.utb_application a ON (ua.ApplicationID = a.ApplicationID)
    WHERE ua.UserID = @UserID
      AND a.Name = 'APD'
    
    
    -- Now do the final select
    SELECT
        1                                       AS Tag,
        NULL                                    AS Parent,
        --Root
        @DocumentID                             AS [Root!1!DocumentID],
        --@ShopLocationID                         AS [Root!1!ShopLocationID],
        @UserID                                 AS [Root!1!UserID],
        @LogonID                                AS [Root!1!NTUserID],
        --Estimate Details
        NULL                                    AS [Estimate!2!ReportDateTime],
        NULL                                    AS [Estimate!2!LynxID],
        NULL                                    AS [Estimate!2!PertainsTo],
        NULL                                    AS [Estimate!2!EstimatorName],
        NULL                                    AS [Estimate!2!ShopFaxNumber],
        NULL                                    AS [Estimate!2!ClaimAnalystName],
        NULL                                    AS [Estimate!2!ClaimAnalystPhone],
        NULL                                    AS [Estimate!2!VehicleOwner],
        NULL                                    AS [Estimate!2!VehicleDescription],
        NULL                                    AS [Estimate!2!DocumentDescription],
        NULL                                    AS [Estimate!2!DocumentReceivedOn],
        -- Estimate Change Line Items
        NULL                                    AS [EstimateChanges!3!LineNumber],
        NULL                                    AS [EstimateChanges!3!Description]
        
    UNION ALL
    
    SELECT 
        2,
        1,
        -- Root
        NULL, NULL, NULL, --NULL,
        -- Estimate Details
        convert(varchar, @now, 101),
        @LynxID,
        @PertainsTo,
        @ShopName,
        @ShopFaxNumber,
        @ClaimAnalyst,
        @ClaimAnalystPhone,
        @VehicleOwner,
        @VehicleDescription,
        @DocumentDesc,
        convert(varchar, @DocumentCreatedOn, 101),
        -- Estimate Change Line Items
        NULL, NULL
   
   UNION ALL
   
   SELECT
        3,
        2,
        -- Root
        NULL, NULL, NULL, --NULL,
        -- Estimate Details
        NULL, NULL, NULL, NULL, NULL, 
        NULL, NULL, NULL, NULL, NULL,
        NULL,
        -- Estimate Change Line Items
        LineNumber,
        ChangesText
    FROM @tblEstimateChangesLineItems
     FOR XML EXPLICIT      -- (Comment for Client-side XML generation)
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateChangesGetDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspEstimateChangesGetDetailWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO