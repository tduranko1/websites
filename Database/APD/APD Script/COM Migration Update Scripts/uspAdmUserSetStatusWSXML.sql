-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmUserSetStatusWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmUserSetStatusWSXML 
END

GO
/****** Object:  StoredProcedure [dbo].[uspAdmUserSetStatusWSXML]    Script Date: 02/14/2014 06:55:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdmUserSetStatusWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Activates or deactivates the UserID for the application passed in
*
* PARAMETERS:  
* (I) @UserID               The UserID to update
* (I) @ApplicationID        The ApplicationID to update
* (I) @SysLastUserID        The UserID performing the update
*
* RESULT SET:
*       A flag detailing the user's new status
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspAdmUserSetStatusWSXML]
    @UserID             udt_std_id,
    @ApplicationID      udt_std_id,
    @SysLastUserID      udt_std_id
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @ActiveFlag     AS udt_std_flag
    DECLARE @LogonId        AS udt_sys_login

    DECLARE @error          AS int
    DECLARE @rowcount       AS int
    DECLARE @now            AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspAdmUserSetStatus'

    -- Set Database options
    
    SET NOCOUNT ON


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
       
 
    -- Check to make sure a valid Application id was passed in

    IF  (@ApplicationID IS NULL) OR
        (NOT EXISTS(SELECT ApplicationID FROM dbo.utb_application WHERE ApplicationID = @ApplicationID))
    BEGIN
        -- Invalid Application ID
    
        RAISERROR('101|%s|@ApplicationID|%u', 16, 1, @ProcName, @ApplicationID)
        RETURN
    END
       

    -- Check to make sure a valid Sys Last User id was passed in

    IF  (@SysLastUserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @SysLastUserID))
    BEGIN
        -- Invalid Sys last User ID
    
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END
    
    
    -- First determine the current status of the user for this application

    SELECT @ActiveFlag = dbo.ufnUtilityIsUserActive(@UserID, DEFAULT, @ApplicationID)

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF @ActiveFlag = 0
    BEGIN
        -- Check to make sure a user_application record exists and that the login in it is not already active for this application
        
        SELECT @LogonId = LogonId
          FROM dbo.utb_user_application
          WHERE UserID = @UserID
            AND ApplicationID = @ApplicationID
            
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        IF @LogonId IS NULL
        BEGIN
            -- The user application record does not exist
                   
            RAISERROR  ('%s: An user_application record does not exist for this application.  Unable to set status.', 16, 1, @ProcName)
            RETURN
        END
                        
            
        IF @LogonId IN (SELECT LogonID 
                          FROM dbo.utb_user_application 
                          WHERE LogonID = @LogonID
                          AND dbo.ufnUtilityIsUserActive(UserID, NULL, @ApplicationID) = 1)
        BEGIN
            -- Duplicate active Logon Id
       
            RAISERROR  ('1|This Logon is already active for another active user.', 16, 1)
            RETURN
        END
    END
    

    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP
    

    BEGIN TRANSACTION AdmSetStatusTran1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- First determine the current status of the user

    IF @ActiveFlag = 1
    BEGIN
        -- User must be active, set inactive
        
        UPDATE  dbo.utb_user_application
          SET   AccessEndDate       = @now,
                SysLastUserID       = @SysLastUserID,
                SysLastUpdatedDate  = @now
          WHERE UserID = @UserID
            AND ApplicationID = @ApplicationID

        SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

        IF @error <> 0
        BEGIN
            -- Update failure
    
            RAISERROR('104|%s|utb_user_application', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END

        IF @ApplicationID = 2
        BEGIN
           -- User record is being deactivated from ClaimPoint. We need to deactivate the user from 
           -- all applications
           
            UPDATE  dbo.utb_user_application
             SET   AccessEndDate       = @now,
                   SysLastUserID       = @SysLastUserID,
                   SysLastUpdatedDate  = @now
             WHERE UserID = @UserID
               AND AccessEndDate IS NULL
           
           SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
   
           IF @error <> 0
           BEGIN
               -- Update failure
       
               RAISERROR('104|%s|utb_user_application', 16, 1, @ProcName)
               ROLLBACK TRANSACTION 
               RETURN
           END
        END


        -- Now look to see if the user is active for any applications.  If not, we'll disable their user record as well

        IF (SELECT dbo.ufnUtilityIsUserActive(@UserID, NULL, NULL)) = 0     -- User is not active for ANY applications now
        BEGIN
            -- Check to see if the UserID has any reports.   If they do, we must throw an error, and rollback

            IF (SELECT COUNT(*) FROM dbo.utb_user WHERE SupervisorUserID = @UserID) > 0
            BEGIN
                -- User ID has reports
    
                RAISERROR  ('1|This user has reports.  Unable to change status.', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
            ELSE
            BEGIN
                -- Disable the user record
            
                UPDATE  dbo.utb_user
                  SET   EnabledFlag         = 0,
                        SysLastUserID       = @SysLastUserID,
                        SysLastUpdatedDate  = @now
                  WHERE UserID = @UserID

                IF @@ERROR <> 0
                BEGIN
                    -- Update failure
    
                    RAISERROR('104|%s|utb_user', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION 
                    RETURN
                END
            END        
        END
        ELSE
        BEGIN
            --update the user table for the last update info.
            UPDATE  dbo.utb_user
              SET   SysLastUserID       = @SysLastUserID,
                    SysLastUpdatedDate  = @now
              WHERE UserID = @UserID

            IF @error <> 0
            BEGIN
                -- Update failure
    
                RAISERROR('104|%s|utb_user', 16, 1, @ProcName)
                ROLLBACK TRANSACTION 
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        -- Setting User active, first enable the user record
        
        UPDATE  dbo.utb_user
          SET   EnabledFlag         = 1,
                SysLastUserID       = @SysLastUserID,
                SysLastUpdatedDate  = @now
          WHERE UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('104|%s|utb_user', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END

                    
        UPDATE  dbo.utb_user_application
          SET   AccessBeginDate     = @now,
                AccessEndDate       = NULL,
                SysLastUserID       = @SysLastUserID,
                SysLastUpdatedDate  = @now
          WHERE UserID = @UserID
            AND ApplicationID = @ApplicationID

        SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

        IF @error <> 0
        BEGIN
            -- Update failure

            RAISERROR('104|%s|utb_user', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END
    END


    COMMIT TRANSACTION  AdmSetStatusTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Return Current Status

    -- Create XML Document to return updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- User Level
            NULL AS [User!2!UserID],
            NULL AS [User!2!Active],
            NULL AS [User!2!AccessBeginEndDate],
            NULL AS [User!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- User Level
            @UserID,
            CASE 
                WHEN AccessEndDate IS NULL THEN 1
                ELSE 0
            END,
            dbo.ufnUtilityGetDateString( IsNull(AccessEndDate, AccessBeginDate) ),
            dbo.ufnUtilityGetDateString( SyslastUpdatedDate )

      FROM  dbo.utb_user_application
      WHERE UserID = @UserID
        AND ApplicationID = @ApplicationID

    ORDER BY tag
    FOR XML EXPLICIT      -- Comment for Client-side processing        

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmUserSetStatusWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmUserSetStatusWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
