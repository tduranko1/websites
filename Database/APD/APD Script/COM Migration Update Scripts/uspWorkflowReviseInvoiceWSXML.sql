USE [udb_apd]
/**********************************************************************************************************/
-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION

-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'uspWorkflowReviseInvoiceWSXML' AND type = 'P') 
BEGIN
    DROP PROCEDURE dbo.uspWorkflowReviseInvoiceWSXML
END

GO
/****** Object:  StoredProcedure [dbo].[uspWorkflowReviseInvoiceXML]    Script Date: 09/01/2014 07:06:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowReviseInvoiceWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh
* FUNCTION:     Retrieves billing items for LynxID entered
* UPDATE:		Update for SQL 2008 By Mahes
* PARAMETERS:  
* (I) @InvoiceTypeCD        'F' - Electronic billing file, 'P' - Paper Invoice document
* (I) @ClaimAspectID        LynxID to pick up billing for 
* (I) @ToDate               The date up to which include billing records for
*
* RESULT SET:   XML document detailing all information needed to generate an invoice
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspWorkflowReviseInvoiceWSXML]
    @ClaimAspectID          udt_std_id_big = NULL,
    @DispatchNumber         varchar(50) = NULL,
    @UserID                 bigint
AS
BEGIN
    -- This procedure finds all the records that haven't been picked up yetfor @LynxID and returns them in an XML stream.
    -- Declare internal variables

    --DECLARE @ClaimAspectID                  udt_std_id_big
    DECLARE @InsuranceCompanyID             udt_std_id
    DECLARE @ClaimAspectTypeIDClaim         udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle       udt_std_id
    DECLARE @LynxID                         udt_std_id_big
    DECLARE @VehicleNumber                  udt_std_int
    DECLARE @InvoiceSeqNumber               udt_std_int_tiny
    DECLARE @TotalFeeAmount                 udt_std_money
    DECLARE @TotalInvoiceAmount             udt_std_money
    DECLARE @DefaultLynxShopID              udt_std_id_big
    DECLARE @DefaultCEIShopID               udt_std_id_big    
    DECLARE @UseCEIShopsFlag                udt_std_flag
    DECLARE @InvoiceDate                    datetime
    DECLARE @NTUserID                       varchar(10)

    DECLARE @ProcName                       varchar(30)       -- Used for raise error stmts 
    DECLARE @now                            udt_std_datetime

    SET @ProcName = 'uspWorkflowReviseInvoiceXML'


    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP
    
    
    -- Validate Dispatch Number
    IF @ClaimAspectID IS NULL AND @DispatchNumber IS NULL
    BEGIN
        RAISERROR('101|%s|@ClaimAspectID or @DispatchNumber is required|%u', 16, 1, @ProcName)
        RETURN
    END

    IF  (@ClaimAspectID IS NOT NULL) AND
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_invoice WHERE ClaimAspectID = @ClaimAspectID AND EnabledFlag = 1))
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END

    IF  (@DispatchNumber IS NOT NULL) AND
        (NOT EXISTS(SELECT InvoiceID FROM dbo.utb_invoice WHERE DispatchNumber = @DispatchNumber AND EnabledFlag = 1))
    BEGIN
        -- Invalid Dispatch Number
    
        RAISERROR('101|%s|@DispatchNumber|%u', 16, 1, @ProcName, @DispatchNumber)
        RETURN
    END
    
    IF  (@DispatchNumber IS NULL)
    BEGIN
       -- Get the information from the latest Invoice
       SELECT top 1 @DispatchNumber = DispatchNumber
       FROM utb_invoice i
       WHERE i.ClaimAspectID = @ClaimAspectID
         AND i.StatusCD = 'FS'
         AND i.EnabledFlag = 1
       ORDER BY InvoiceID DESC
    END
    
    IF @DispatchNumber IS NULL
    BEGIN
        RAISERROR('101|%s|@DispatchNumber|%s', 16, 1, @ProcName, @DispatchNumber)
        RETURN
    END
    
    IF  (@ClaimAspectID IS NULL)
    BEGIN
       -- Get the information from the latest Invoice
       SELECT top 1 @ClaimAspectID = i.ClaimAspectID
       FROM utb_invoice i
       WHERE i.DispatchNumber = @DispatchNumber
         AND i.StatusCD = 'FS'
         AND i.EnabledFlag = 1
       ORDER BY InvoiceID DESC
    END
    
    IF @ClaimAspectID IS NULL
    BEGIN
        RAISERROR('101|%s|@ClaimAspectID|%s', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    SELECT top 1 @ClaimAspectID = ca.ClaimAspectID,
           @LynxID = LynxID, 
           @VehicleNumber = ClaimAspectNumber,
           @InvoiceDate = SentToIngresDate
    FROM utb_invoice i
    LEFT JOIN utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
    WHERE i.DispatchNumber = @DispatchNumber
      AND StatusCD = 'FS'
    ORDER BY InvoiceID DESC
    
    -- Get the next available sequence number for the invoice.
	  SET @InvoiceSeqNumber = (SELECT ISNULL(MAX(d.SupplementSeqNumber), 0)
                             FROM dbo.utb_document d INNER JOIN dbo.utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                                                     INNER JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
                                                     inner join utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
                             WHERE casc.ClaimAspectID = @ClaimAspectID
                               AND dt.Name = 'Invoice'
                               AND d.SupplementSeqNumber is not null)--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070103
    
    
    -- Retrieve the default Shop ID to be used if a PayeeID is 0.
    SELECT @DefaultLynxShopID = CONVERT(bigint, Value) 
    FROM dbo.utb_app_variable 
    WHERE Name = 'Default_Payment_Lynx_ShopLocationID'
    
    
    -- Retrieve the defalt Shop ID to be used if a Payee is a CEI shop and the carrier does not use CEI shops.
    SELECT @DefaultCEIShopID = CONVERT(bigint, Value) 
    FROM dbo.utb_app_variable 
    WHERE Name = 'Default_Payment_CEI_ShopLocationID'
    
    
    
    DECLARE @tmpReference TABLE (ListName       varchar(50)     NOT NULL,
                                 ReferenceID    varchar(4)      NOT NULL,
                                 Name           varchar(50)     NOT NULL)
                                 
    INSERT INTO @tmpReference
    SELECT 'ItemType', Code, Name FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD')
    
    UNION ALL
    
    SELECT 'PayeeType', Code, Name FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'PayeeTypeCD')
    
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END
        
    
    -- These are the individual items from the APD database.
    DECLARE @tmpInvoice TABLE (InvoiceID                    bigint          NOT NULL,
                               AuthorizingClientUserID      varchar(50)         NULL,
                               ClaimAspectID                bigint          NOT NULL,
                               LynxID                       bigint          NOT NULL,
                               Amount                       money           NOT NULL,
                               AdminFeeAmount               money               NULL,
                               ClientFeeCode                varchar(50)         NULL,
                               DispatchNumber               varchar(50)         NULL,
                               EntryDate                    datetime        NOT NULL,
                               FeeCategoryCD                varchar(4)          NULL,
                               InvoiceDescription           varchar(250)        NULL,
                               ItemType                     varchar(50)     NOT NULL,
                               PayeeID                      bigint              NULL,
                               PayeeName                    varchar(50)         NULL,
                               PayeeCity                    varchar(30)         NULL,
                               PayeeState                   char(2)             NULL,
                               PayeeTypeCD                  varchar(4)          NULL,
                               InitialPayment               varchar(10)         NULL,
                               DeductibleAmt                money               NULL,
                               TaxTotalAmt                  money               NULL,
                               WorkStartConfirmFlag         bit                 NULL,
                               WorkEndConfirmFlag           bit                 NULL)      
                               
                                  
    
      INSERT INTO @tmpInvoice
      SELECT i.InvoiceID, 
                    u.ClientUserID,
                    i.ClaimAspectID,
                    ca.LynxID,
                    i.Amount, 
                    i.AdminFeeAmount,
                    i.ClientFeeCode,
                    i.DispatchNumber,
                    i.EntryDate,
                    i.FeeCategoryCD,
                    CASE 
                        WHEN i.ItemTypeCD = 'F' THEN i.InvoiceDescription
                        ELSE i.PayeeName + ' - ' + i.Description + ' Claim'
                        --ELSE i.Description
                    END InvoiceDescription,
                    t.Name,              
                    CASE
                        WHEN i.ItemTypeCD = 'F' THEN @DefaultLynxShopID
                        ELSE i.PayeeID
                    END PayeeID,
                    i.PayeeName,
                    i.PayeeAddressCity,
                    i.PayeeAddressState,
                    i.PayeeTypeCD,
                    'false',
                    i.DeductibleAmt,
                    i.TaxTotalAmt,
                    casc.WorkStartConfirmFlag,
                    casc.WorkEndConfirmFlag
      
      FROM dbo.utb_invoice i
      LEFT JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON i.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
      LEFT JOIN dbo.utb_user u ON i.AuthorizingUserID = u.UserID
      LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD') t ON i.ItemTypeCD = t.Code
      WHERE i.DispatchNumber = @DispatchNumber
      
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpInvoice', 16, 1, @ProcName)
        RETURN
    END
    
    --select * from @tmpInvoice
        
    -- Get the ShopLocationID associated with the most recent shop assignment    
    DECLARE @tmpHandlingShopLocationIDs TABLE (ClaimAspectID      bigint    NOT NULL,
                                               AssignmentID       bigint        NULL,
                                               ShopLocationID     bigint        NULL,
                                               APDShopLocationID  bigint        NULL)
                                               
    
    INSERT INTO @tmpHandlingShopLocationIDs
    SELECT casc.ClaimAspectID, MIN(AssignmentID), NULL, NULL
    FROM dbo.utb_assignment a 
    INNER JOIN utb_Claim_Aspect_Service_Channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
	INNER JOIN dbo.utb_invoice i ON casc.ClaimAspectID = i.ClaimAspectID
                              AND a.ShopLocationID = i.PayeeID
    INNER JOIN dbo.utb_shop_location sl ON a.ShopLocationID = sl.ShopLocationID                                                           
    WHERE casc.ClaimAspectID IN (SELECT DISTINCT ClaimAspectID FROM @tmpInvoice)
      AND i.EnabledFlag = 1
    GROUP BY casc.ClaimAspectID
    
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpHandlingShopLocationIDs', 16, 1, @ProcName)
        RETURN
    END
    
    
    /*-- Determine the Final PayeeID to be sent to Ingres.   	
    UPDATE @tmpInvoice
    SET PayeeID = (SELECT CASE
                            WHEN t.PayeeID <> 0 AND ProgramTypeCD = 'CEI' THEN @DefaultCEIShopID
                            WHEN t.PayeeID = 0 THEN @DefaultLynxShopID
                            ELSE t.PayeeID
                          END)
    FROM @tmpInvoice t INNER JOIN dbo.utb_assignment a ON t.PayeeID = a.ShopLocationID
    WHERE a.AssignmentID = (SELECT MAX(AssignmentID) 
                            FROM dbo.utb_assignment a
                            LEFT JOIN dbo.utb_claim_aspect_service_channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                            WHERE casc.ClaimAspectID = t.ClaimAspectID
                              AND ShopLocationID = t.PayeeID)
                              
    IF @@ERROR <> 0
    BEGIN
        -- Update failure
    
        RAISERROR('104|%s|@tmpInvoice', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Set Initial Payment Flag for original payments on each vehicle.
    UPDATE @tmpInvoice
    SET InitialPayment = 'true'
    WHERE ItemType <> 'Fee'
      AND InvoiceID IN (SELECT InvoiceID
                        FROM @tmpInvoice t
                        WHERE ItemType <> 'Fee'
                          AND EntryDate = (SELECT MIN(EntryDate)
                                           FROM @tmpInvoice
                                           WHERE ClaimAspectID = t.ClaimAspectID
                                             AND ItemType <> 'Fee'))
                                                 
    IF @@ERROR <> 0
    BEGIN
        -- Update failure
    
        RAISERROR('104|%s|@tmpInvoice', 16, 1, @ProcName)
        RETURN
    END                                 
                                    
                              
    
    UPDATE @tmpHandlingShopLocationIDs
    SET ShopLocationID    = (SELECT CASE
                                WHEN a.ShopLocationID <> 0 AND a.ProgramTypeCD = 'CEI' THEN @DefaultCEIShopID
                                WHEN a.ShopLocationID = 0 THEN @DefaultLynxShopID
                                ELSE a.ShopLocationID
                             END),
        APDShopLocationID = a.ShopLocationID
    FROM @tmpHandlingShopLocationIDs t LEFT JOIN dbo.utb_assignment a ON t.AssignmentID = a.AssignmentID
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure
    
        RAISERROR('104|%s|@tmpHandlingShopLocationIDs', 16, 1, @ProcName)
        RETURN
    END*/
    
    
     -- Get Claim Aspect Type for use later
    SELECT @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
       -- Claim Aspect Not Found    
        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
    FROM dbo.utb_claim_aspect_type
    WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
       -- Claim Aspect Not Found    
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @TotalInvoiceAmount = Sum(Amount)
    FROM @tmpInvoice
    
    SELECT @NTUserID = LogonID 
    FROM dbo.utb_user_application ua INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
    WHERE a.Name = 'APD'
      AND ua.UserID = @UserID

          
--    select * from @tmpInvoice
--    select * from @tmpReference
--    return

    -- Begin XML Select        
    SELECT  1 AS Tag,
            NULL AS Parent,        
            
            -- Root    
            @InsuranceCompanyID                    AS [Root!1!InsuranceCompanyID],
            @NTUserID                              AS [Root!1!NTUserID],
            dbo.ufnUtilityFormatDate(@InvoiceDate, 'mm/dd/yyy hh:nn') AS [Root!1!ToDate],
            @UserID                                AS [Root!1!UserID],
            convert(varchar, @TotalInvoiceAmount)  AS [Root!1!TotalInvoiceAmount],
            -- Claim Information level
            NULL AS [Claim!2!LynxID],
            NULL AS [Claim!2!CarrierClaimNumber],
            NULL AS [Claim!2!CarrierRepClientUserID],
            NULL AS [Claim!2!CarrierRepNameFirst],
            NULL AS [Claim!2!CarrierRepNameLast],
            NULL AS [Claim!2!InsuredNameFirst],
            NULL AS [Claim!2!InsuredNameLast],
            NULL AS [Claim!2!InsuredBusinessName],
            NULL AS [Claim!2!IntakeFinishDate],
            NULL AS [Claim!2!LossDate],
            NULL AS [Claim!2!LossState],
            NULL AS [Claim!2!PolicyNumber],
            NULL AS [Claim!2!CarrierName],
            NULL AS [Claim!2!CarrierAddress1],
            NULL AS [Claim!2!CarrierAddress2],
            NULL As [Claim!2!CarrierCity],
            NULL AS [Claim!2!CarrierState],
            NULL AS [Claim!2!CarrierZip],
            
            -- Exposure Information Level
            NULL AS [Exposure!3!ClaimAspectID],
            NULL AS [Exposure!3!ClaimantBusinessName],
            NULL AS [Exposure!3!ClaimantNameFirst],
            NULL AS [Exposure!3!ClaimantNameLast],
            NULL AS [Exposure!3!CoverageType],
            NULL AS [Exposure!3!Deductible],
            NULL AS [Exposure!3!SourceApplicationPassThruData],
            NULL AS [Exposure!3!Vehicle],
            NULL AS [Exposure!3!VehicleNumber],
            NULL AS [Exposure!3!HandlingShopLocationID],
            NULL AS [Exposure!3!APDShopLocationID],   -- this field exists in case the id immediately above is overridden by a default (i.e. CEI).
            NULL AS [Exposure!3!PertainsTo],
            NULL AS [Exposure!3!InvoiceNumber],
            NULL AS [Exposure!3!InvoiceSeqNumber],
            NULL AS [Exposure!3!ServiceChannel],
            NULL AS [Exposure!3!Claimant],
            
            -- Invoice Level
            NULL AS [Invoice!4!InvoiceID],
            NULL AS [Invoice!4!AuthorizingClientUserID],
            NULL AS [Invoice!4!Amount],     
            NULL AS [Invoice!4!AdminFeeAmount],
            NULL AS [Invoice!4!ClientFeeCode],           
            NULL AS [Invoice!4!DispatchNumber],
            NULL AS [Invoice!4!EntryDate],
            NULL AS [Invoice!4!FeeCategoryCD],
            NULL AS [Invoice!4!InvoiceDate],
            NULL AS [Invoice!4!InvoiceDescription],
            NULL AS [Invoice!4!ItemType],
            NULL AS [Invoice!4!PayeeID],
            NULL AS [Invoice!4!PayeeName],
            NULL AS [Invoice!4!PayeeCity],
            NULL AS [Invoice!4!PayeeState],
            NULL AS [Invoice!4!PayeeType],
            NULL AS [Invoice!4!InitialPayment],
            NULL AS [Invoice!4!DeductibleAmt],
            NULL AS [Invoice!4!TotalTaxAmt]

    UNION ALL            

    -- Select Claim Level                
    SELECT  DISTINCT 2,
            1,
            
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim level
            ISNULL(CONVERT(varchar(20), c.LynxID), '') + '-' + CONVERT(varchar(10), ca.ClaimAspectNumber),
            ISNULL(c.ClientClaimNumber, ''),  --Project:210474 APD Modified when we did the code merge M.A.20061120
            ISNULL(cu.ClientUserID, ''),
            ISNULL(cu.NameFirst, ''),
            ISNULL(cu.NameLast, ''), 
                     
            ISNULL((SELECT TOP 1 i.NameFirst
                    FROM dbo.utb_claim_aspect cas LEFT JOIN dbo.utb_claim_aspect_involved cai ON cas.ClaimAspectID = cai.ClaimAspectID
                                                  LEFT JOIN dbo.utb_involved i ON cai.InvolvedID = i.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role_type irt ON ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                    WHERE cas.LynxID = t.LynxID
                      AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                      AND cai.EnabledFlag = 1
                      AND irt.Name = 'Insured'), ''),
              
            ISNULL((SELECT TOP 1 i.NameLast
                    FROM dbo.utb_claim_aspect cas LEFT JOIN dbo.utb_claim_aspect_involved cai ON cas.ClaimAspectID = cai.ClaimAspectID
                                                  LEFT JOIN dbo.utb_involved i ON cai.InvolvedID = i.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role_type irt ON ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                    WHERE cas.LynxID = t.LynxID
                      AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                      AND cai.EnabledFlag = 1
                      AND irt.Name = 'Insured'), ''),
              
            ISNULL((SELECT TOP 1 i.BusinessName
                    FROM dbo.utb_claim_aspect cas LEFT JOIN dbo.utb_claim_aspect_involved cai ON cas.ClaimAspectID = cai.ClaimAspectID
                                                  LEFT JOIN dbo.utb_involved i ON cai.InvolvedID = i.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                                  LEFT JOIN dbo.utb_involved_role_type irt ON ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                    WHERE cas.LynxID = t.LynxID
                      AND cas.ClaimAspectTypeID = @ClaimAspectTypeIDClaim    -- Insured associated with claim
                      AND cai.EnabledFlag = 1
                      AND irt.Name = 'Insured'), ''),
                
            ISNULL(c.IntakeFinishDate,''),
            ISNULL(CONVERT(varchar(30), c.LossDate, 101), ''),
            ISNULL(c.LossState, ''),
            ISNULL(c.PolicyNumber, ''),                    
            ISNULL(ins.Name, ''),
            ISNULL(ins.Address1, ''),
            ISNULL(ins.Address2, ''),
            ISNULL(ins.AddressCity, ''),
            ISNULL(ins.AddressState, ''),
            ISNULL(ins.AddressZip, ''),
            
            -- Exposure Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

      FROM @tmpInvoice t INNER JOIN dbo.utb_claim_aspect ca ON t.ClaimAspectID = ca.ClaimAspectID
                         INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                         INNER JOIN dbo.utb_insurance ins ON c.InsuranceCompanyID = ins.InsuranceCompanyID  
                         INNER JOIN dbo.utb_invoice i ON ca.ClaimAspectID = i.ClaimAspectID  
                         LEFT JOIN dbo.utb_claim_coverage cc ON c.LynxID = cc.LynxID
                         LEFT JOIN dbo.utb_user cu ON c.CarrierRepUserID = cu.UserID           
      
    UNION ALL            

    -- Select Exposure Level                
    SELECT DISTINCT
            3,
            2,
            
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim level
            ISNULL(CONVERT(varchar(20), ca.LynxID), '') + '-' + CONVERT(varchar(10), ca.ClaimAspectNumber), 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Exposure Level
            ca.ClaimAspectID,
            ISNULL((SELECT Top 1 i.BusinessName
                    FROM dbo.utb_claim_aspect cas
                        LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                        LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cas.ClaimAspectID = ca.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name IN ('Insured', 'Claimant')), ''),    
                      
            ISNULL((SELECT Top 1 i.NameFirst
                    FROM dbo.utb_claim_aspect cas
                        LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                        LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cas.ClaimAspectID = ca.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name IN ('Insured', 'Claimant')), ''), 
                   
            ISNULL((SELECT Top 1 i.NameLast
                    FROM dbo.utb_claim_aspect cas
                        LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                        LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cas.ClaimAspectID = ca.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name IN ('Insured', 'Claimant')), ''),
            
            ISNULL(cct.Name, ''),
            
            ISNULL(cc.DeductibleAmt,0), --Project: 210474 APD - Enhancements to support multiple concurrent service channels

            ISNULL(ca.SourceApplicationPassThruData, ''),
            LTRIM(ISNULL(CONVERT(varchar(4), cv.VehicleYear), '') + ' ' + ISNULL(cv.Make, '') + ' ' + ISNULL(cv.Model, '') + ' ' + 
                 ISNULL(cv.BodyStyle, '') + ' ' + CASE WHEN cv.VIN IS NOT NULL THEN 'VIN: ' + cv.VIN ELSE '' END),
            ISNULL(ca.ClaimAspectNumber, ''),
            ISNULL(CONVERT(varchar(10), t2.ShopLocationID), ''),
            ISNULL(CONVERT(varchar(10), t2.APDShopLocationID), ''),
            cat.Code + CONVERT(varchar(4), ca.ClaimAspectNumber),
            CONVERT(varchar(20), @LynxID) + '-' + CONVERT(varchar(3), @VehicleNumber) + '-' + CONVERT(varchar(3), @InvoiceSeqNumber), 
            @InvoiceSeqNumber,
            (SELECT Name from dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD') WHERE Code = casc.ServiceChannelCD),
            ISNULL((SELECT Top 1 case
                                    when i.BusinessName is not null then i.BusinessName
                                    else i.NameFirst + ' ' + i.NameLast
                                 end
                    FROM dbo.utb_claim_aspect cas
                        LEFT JOIN dbo.utb_claim_aspect_involved cai ON (cas.ClaimAspectID = cai.ClaimAspectID)
                        LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                        LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                    WHERE cas.ClaimAspectID = ca.ClaimAspectID
                      AND cai.EnabledFlag = 1
                      AND irt.Name IN ('Insured', 'Claimant')), ''),    
            
            -- Invoice Level
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
            
      FROM @tmpInvoice t INNER JOIN dbo.utb_claim_aspect ca ON t.ClaimAspectID = ca.ClaimAspectID
                         INNER JOIN dbo.utb_claim_aspect_type cat ON ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
                         INNER JOIN dbo.utb_claim_vehicle cv ON ca.ClaimAspectID = cv.ClaimAspectID
/*
                         LEFT JOIN dbo.utb_client_coverage_type cct ON ca.ClientCoverageTypeID = cct.ClientCoverageTypeID  

                         LEFT JOIN dbo.utb_claim_coverage cc ON ca.LynxID = cc.LynxID and cc.CoverageTypeCD = ca.CoverageProfileCD --Project: 210474 APD - Enhancements to support multiple concurrent service channels
*/
                         --Project:210474 APD Remarked-off to support the schema change M.A.20061218
                         --LEFT JOIN dbo.utb_assignment_type at ON ca.CurrentAssignmentTypeID = at.AssignmentTypeID
                         --Project:210474 APD Added the following to support the schema change M.A.20061218
                         LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc
                         on ca.ClaimAspectID = casc.ClaimAspectID
                         and casc.PrimaryFlag = 1

                        inner join        utb_Claim c
                        on                c.LynxID = ca.LynxID

                        Left Outer join   utb_Client_Coverage_Type cct
                        on                c.InsuranceCompanyID = cct.InsuranceCompanyID
                        and               cct.ClientCoverageTypeID = ca.ClientCoverageTypeID
                        and               cct.CoverageProfileCD = ca.CoverageProfileCD
                        
                        Left Outer join   utb_Claim_Coverage cc
                        on                cc.ClientCoverageTypeID = cct.ClientCoverageTypeID
                        and               c.LynxID = cc.LynxID
                        and               cc.CoverageTypeCD = cct.CoverageProfileCD
                        and               cc.EnabledFlag = 1
                        -- this additional coverage check was added here because when the claim did not have any
                        --  coverage information, this node is excluded.
                        and               cc.AddtlCoverageFlag = 0 -- exclude the additional coverages
                        
                        left outer join   utb_claim_aspect_service_channel_coverage cascc
                        on                cascc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                        and               cascc.ClaimCoverageID = cc.ClaimCoverageID

                         LEFT JOIN @tmpHandlingShopLocationIDs t2 ON ca.ClaimAspectID = t2.ClaimAspectID
    
      
    UNION ALL            

    -- Select Invoice Level                
    SELECT  4,
            3,
            
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim level
            ISNULL(CONVERT(varchar(20), ca.LynxID), '') + '-' + CONVERT(varchar(10), ca.ClaimAspectNumber), 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Exposure Level
            ISNULL(CONVERT(varchar(20), t.ClaimAspectID), ''), 
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Invoice Level
           InvoiceID,
           ISNULL(AuthorizingClientUserID, ''), 
           ISNULL(CONVERT(varchar(20), Amount), ''),
           ISNULL(CONVERT(varchar(20), AdminFeeAmount), ''),
           ISNULL(ClientFeeCode, ''),
           ISNULL(DispatchNumber, ''),
           ISNULL(EntryDate, ''),
           ISNULL(FeeCategoryCD, ''),
           convert(varchar, @now, 101),
           ISNULL(InvoiceDescription, ''),
           ISNULL(ItemType, ''),
           ISNULL(PayeeID, 0), 
           ISNULL(PayeeName, ''),
           ISNULL(PayeeCity, ''),
           ISNULL(PayeeState, ''),
           ISNULL(tr.Name, ''),
           InitialPayment,
           ISNULL(CONVERT(varchar(20), DeductibleAmt), ''),
           ISNULL(CONVERT(varchar(20), TaxTotalAmt), '')
            
    FROM @tmpInvoice t INNER JOIN dbo.utb_claim_aspect ca ON t.ClaimAspectID = ca.ClaimAspectID
                       LEFT JOIN @tmpReference tr ON ListName = 'PayeeType' AND t.PayeeTypeCD = tr.ReferenceID
       
    

    ORDER BY [Claim!2!LynxID], [Exposure!3!ClaimAspectID], [Invoice!4!InvoiceID], Tag
    
    FOR XML EXPLICIT          -- (Commented for client-side)
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error Selecting XML
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    
   
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowReviseInvoiceWSXML' AND type = 'P') BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowReviseInvoiceWSXML TO ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
/*********************************************************************************************************/

