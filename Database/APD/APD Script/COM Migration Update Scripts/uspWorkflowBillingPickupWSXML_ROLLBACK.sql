USE [udb_apd]
-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'uspWorkflowBillingPickupWSXML' AND type = 'P') 
BEGIN
    DROP PROCEDURE dbo.uspWorkflowBillingPickupWSXML
END

GO