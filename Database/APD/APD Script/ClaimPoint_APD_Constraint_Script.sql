
-- ========================================================================================
-- Purpose    : As per andrew's requirement we added the extendedproperty for PrefMethodUpd.
-- Created by : Constraint uck_involved_prefmethodupd created by glsd452.
-- ========================================================================================

ALTER TABLE [dbo].[utb_involved] WITH NOCHECK ADD CONSTRAINT [uck_involved_prefmethodupd] CHECK (([PrefMethodUpd] = 'NU' or [PrefMethodUpd] = 'EM' or [PrefMethodUpd] = 'CE'))
GO

ALTER TABLE [dbo].[utb_involved] CHECK CONSTRAINT [uck_involved_prefmethodupd]
GO

EXEC dbo.sp_addextendedproperty @name=N'Codes', @value=N'NU|No Updates|EM|E-mail|CE|Cell' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'utb_involved', @level2type=N'CONSTRAINT',@level2name=N'uck_involved_prefmethodupd'
GO

-- ========================================================================================
-- Purpose    : As per andrew's requirement we added the extendedproperty for CellPhoneCarrier.
-- Created by : Constraint uck_involved_cellphonecarrier created by glsd452.
-- ========================================================================================

ALTER TABLE [dbo].[utb_involved] WITH NOCHECK ADD CONSTRAINT [uck_involved_cellphonecarrier] CHECK (([CellPhoneCarrier] = 'AT' or [CellPhoneCarrier] = 'SP' or [CellPhoneCarrier] = 'TM' or [CellPhoneCarrier] = 'VW' or [CellPhoneCarrier] = 'OT'))
GO

ALTER TABLE [dbo].[utb_involved] CHECK CONSTRAINT [uck_involved_cellphonecarrier]
GO

EXEC dbo.sp_addextendedproperty @name=N'Codes', @value=N'AT|AT&T|SP|Sprint|TM|T-Mobile|VW|Verizon Wireless|OT|Other' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'utb_involved', @level2type=N'CONSTRAINT',@level2name=N'uck_involved_cellphonecarrier'
GO

-- ==========================================================================
-- Purpose    : As per andrew's requirement we updated the extendedproperty.
-- Updated by : Constraint uck_involved_bestcontactphonecd updated by glsd452.
-- ==========================================================================

ALTER TABLE [dbo].[utb_involved] DROP CONSTRAINT [uck_involved_bestcontactphonecd]

ALTER TABLE [dbo].[utb_involved] WITH NOCHECK ADD CONSTRAINT [uck_involved_bestcontactphonecd] CHECK (([BestContactPhoneCD] = 'A' or [BestContactPhoneCD] = 'D' or [BestContactPhoneCD] = 'N' or [BestContactPhoneCD] = 'C'))
GO

ALTER TABLE [dbo].[utb_involved] CHECK CONSTRAINT [uck_involved_bestcontactphonecd]
GO

EXEC dbo.sp_addextendedproperty @name=N'Codes', @value=N'A|Alternate|D|Day|N|Night|C|Cell' , @level0type=N'USER',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'utb_involved', @level2type=N'CONSTRAINT',@level2name=N'uck_involved_bestcontactphonecd'
GO

--------------------------------------------------------------------