GO
/****** Object:  StoredProcedure [dbo].[uspCFDASummaryReportGetDetailXML]    Script Date: 02/04/2013 01:13:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspCFDASummaryReportGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Ramesh Vishegu]
* FUNCTION:     [Stored procedure to retrieve Custom Forms as XML]
*
* PARAMETERS:  
* No Parameters
*
* RESULT SET:
* Users List as XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

-- exec uspCFDASummaryReportGetDetailXML 1600116,639816,'DA',5871,1,'Supplement'

ALTER PROCEDURE [dbo].[uspCFDASummaryReportGetDetailXML]
    @LynxID             udt_std_id_big,
    @ClaimAspectID      udt_std_id_big,
    @ServiceChannelCD   udt_std_cd,
    @UserID             udt_std_id_big,
    @SequenceNumber		udt_std_id_big		= NULL,
    @DataSet			udt_std_name,
    @FormID				udt_std_id_big   
   
AS
BEGIN
    -- Declare internal variables
    DECLARE @InsuranceCompanyName as varchar(100)
    DECLARE @ClaimNumber as varchar(50)
    DECLARE @CarrierRepName as varchar(100)
    DECLARE @OriginalEstimateDocumentID as bigint
    DECLARE @AuditedEstimateDocumentID as bigint
    DECLARE @OriginalEstimateAmount as decimal(9, 2)
    DECLARE @AuditedEstimateAmount as decimal(9, 2)
    DECLARE @AuditedDifference as decimal(9, 2)
    DECLARE @Deductible as decimal(9, 2)
    DECLARE @Adjustments as decimal(9, 2)
    DECLARE @Betterment as decimal(9, 2)
    DECLARE @NetAuditedEstimate as decimal(9, 2)
    DECLARE @AuditorName as varchar(100)
    DECLARE @AuditorTitle as varchar(100)
    DECLARE @AuditorPhone as varchar(15)
    DECLARE @AuditorEmail as varchar(75)
    DECLARE @ClaimAspectNumber as tinyint
    DECLARE @LastSupplementSeqNo as int
    DECLARE @Message as varchar(1000)
    DECLARE @VANFlag as bit
    DECLARE @LossState as varchar(2)
    DECLARE @LossStateName as varchar(100)
    DECLARE @ShopContacted as Varchar(1000)
	DECLARE @ShopContactName as Varchar(250)
	DECLARE @PrimaryReasonCode as Varchar(1000)
	DECLARE @PrimaryReasonCodeText as Varchar(1000)
	DECLARE @SecondaryReasonCode as Varchar(1000)
	DECLARE @SecondaryReasonCodeText as Varchar(1000)
	DECLARE @PriceAgreedCD as Varchar(250)
    DECLARE @EstimateSummaryRepairTotalID AS int
    DECLARE @EstimateSummaryAdjustmentsTotalID AS int
    DECLARE @EstimateSummaryBettermentID AS int
    DECLARE @EstimateSummaryDeductibleID AS int
    DECLARE @FormDataID as int
    DECLARE @OriginalDocumentID AS bigint
    DECLARE @AuditedDocumentID AS bigint
    
    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFDASummaryReportGetDetailXML'  
    
   -- SET @LastSupplementSeqNo = @SequenceNumber

    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    -- Check to make sure a valid user id was passed in
    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    -- Get the estimate summary ids
    SELECT @EstimateSummaryRepairTotalID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'TT'
      AND Name = 'RepairTotal'

    SELECT @EstimateSummaryAdjustmentsTotalID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'TT'
      AND Name = 'AdjustmentTotal'

    SELECT @EstimateSummaryBettermentID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'AJ'
      AND Name = 'Betterment'

    SELECT @EstimateSummaryDeductibleID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'AJ'
      AND Name = 'Deductible'

     --Get the latest supplement sequence number
    SELECT top 1 @LastSupplementSeqNo = d.SupplementSeqNumber
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and dt.EstimateTypeFlag = 1
      --and d.EstimateTypeCD = 'O'
      and d.EnabledFlag = 1
    order by d.SupplementSeqNumber desc, ds.VANFlag desc
   
    -- Get the original estimate document
    SELECT top 1 @OriginalEstimateDocumentID = d.DocumentID
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @SequenceNumber
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'O'
      and d.EnabledFlag = 1
    order by d.SupplementSeqNumber desc, ds.VANFlag desc

    -- Get the Audited Estimate
    SELECT top 1 @AuditedEstimateDocumentID = d.DocumentID,
			     @VANFlag = ds.VANFlag
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @SequenceNumber
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'A'
      and d.EnabledFlag = 1	 
    order by d.SupplementSeqNumber desc, ds.VANFlag desc
    
    -- Check Orginal Document Upload By Glsd451
     SELECT top 1 @OriginalDocumentID = d.DocumentID
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @LastSupplementSeqNo
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'O'
      and d.EnabledFlag = 1
    order by d.SupplementSeqNumber desc, ds.VANFlag desc
    
    -- Check Audited Document Upload By Glsd451
    SELECT top 1 @AuditedDocumentID = d.DocumentID,
			     @VANFlag = ds.VANFlag
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @LastSupplementSeqNo
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'A'
      and d.EnabledFlag = 1	 
    order by d.SupplementSeqNumber desc, ds.VANFlag desc

 -- Get the Reason code inforamtiom for Estimate/Supplement Audit
    SELECT top 1 @ShopContacted = d.ShopContacted,
				 @ShopContactName = d.ShopContactName,
				 @PrimaryReasonCode = d.PrimaryReasonCode,
				 @PrimaryReasonCodeText = d.PrimaryReasonCodeText,
				 @SecondaryReasonCode = d.SecondaryReasonCode,
				 @SecondaryReasonCodeText = d.SecondaryReasonCodeText,
				 @PriceAgreedCD = d.AgreedPriceMetCD
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @SequenceNumber
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'A'
      and d.EnabledFlag = 1
	 and  dt.Name = @DataSet
    order by d.SupplementSeqNumber desc, ds.VANFlag desc

    SET @Message = ''

    IF @OriginalEstimateDocumentID IS NOT NULL AND @AuditedEstimateDocumentID IS NOT NULL
    BEGIN
        SELECT @OriginalEstimateAmount = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @OriginalEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryRepairTotalID


        SELECT @AuditedEstimateAmount = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryRepairTotalID

        SELECT @Adjustments = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryAdjustmentsTotalID

        SELECT @Betterment = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryBettermentID

        SELECT @Deductible = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryDeductibleID

        SET @AuditedDifference = @OriginalEstimateAmount - @AuditedEstimateAmount

        SET @NetAuditedEstimate = @AuditedEstimateAmount - @Adjustments

        IF @VANFlag = 0
        BEGIN
            SET @NetAuditedEstimate = @NetAuditedEstimate - @Betterment
        END
    END
   
        IF @OriginalDocumentID IS NULL
        BEGIN
            IF @LastSupplementSeqNo = 0
            BEGIN
                SET @Message = 'Original Estimate not found.'
            END
            ELSE
            BEGIN
                SET @Message = 'Original Supplement ' + convert(varchar, @LastSupplementSeqNo) + ' not found.'
            END
        END

        IF @AuditedDocumentID IS NULL
        BEGIN
            IF @LastSupplementSeqNo = 0
            BEGIN
                SET @Message = 'Audited Estimate not found.'
            END
            ELSE
            BEGIN
                SET @Message = 'Audited Supplement ' + convert(varchar, @LastSupplementSeqNo) + ' not found.'
            END
        END
    

    SELECT @CarrierRepName = isNull(uc.NameFirst + ' ' + uc.NameLast, ''),
           @AuditorName = isNull(ua.NameFirst + ' ' + ua.NameLast, ''),
           @AuditorTitle = 'Quality Control Representative',
           @AuditorPhone = '(' + ua.PhoneAreaCode + ') ' + ua.PhoneExchangeNumber + ' ' + ua.PhoneUnitNumber,
           @AuditorEmail = LTRIM(isNull(ua.EmailAddress, '')),
           @ClaimAspectNumber = ca.ClaimAspectNumber,
           @LossState = c.LossState,
           @LossStateName = sc.StateValue
    FROM utb_claim_aspect ca 
    LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
    LEFT JOIN utb_user ua ON ca.AnalystUserID = ua.UserID
    LEFT JOIN utb_user uc ON c.CarrierRepUserID = uc.UserID
    LEFT JOIN utb_state_code sc ON c.LossState = sc.StateCode
    WHERE ca.ClaimAspectID = @ClaimAspectID

    SELECT @InsuranceCompanyName = i.Name,
           @ClaimNumber = c.ClientClaimNumber
    FROM utb_claim c 
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE c.LynxID = @LynxID   
       
    If @DataSet = 'Estimate'
    Begin
		select Top 1 @FormDataID = formdataid from utb_form_data_detail where formfieldid in
		(select formfieldid from utb_form_field where name='ClaimAspectID' and formid=@FormID ) and fielddata= Convert (varchar(50) ,@ClaimAspectID)
		and formdataid in(
		select formdataid from utb_form_data_detail where formfieldid in
		(select formfieldid from utb_form_field where name='DataSet' and formid=@FormID ) and fielddata= @DataSet and
		formdataid in(
		select formdataid from utb_form_data_detail where formfieldid in
		(select formfieldid from utb_form_field where name='lynxid' and formid=@FormID ) and fielddata= Convert(varchar(50),@LynxID)))
	End
	
	If @DataSet = 'Supplement'
	Begin
		select TOP 1 @FormDataID = formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='ClaimAspectID' and formid=@FormID ) and fielddata= Convert (varchar(50) ,@ClaimAspectID) and formdataid in(
			select formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='SequenceNumber' and formid=@FormID ) and fielddata= Convert (varchar(50) ,@SequenceNumber) and formdataid in(
			select formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='DataSet' and formid=@FormID ) and fielddata= @DataSet and formdataid in(
			select formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='lynxid' and formid=@FormID ) and fielddata= Convert(varchar(50),@LynxID))))
	End
	
    SELECT  1 as Tag,
            NULL as Parent,
            @InsuranceCompanyName AS [Root!1!InsuranceCompanyName],
            @LynxID AS [Root!1!LynxID],
            @ClaimAspectNumber AS [Root!1!ClaimAspectNumber],
            @ClaimNumber AS [Root!1!ClaimNumber],
            @LossState AS [Root!1!LossState],
            @LossStateName AS [Root!1!LossStateName],
            @CarrierRepName AS [Root!1!ClaimRepresentative],
            @OriginalEstimateAmount AS [Root!1!OriginalEstimateAmount],
            @AuditedDifference AS [Root!1!AuditedDifference],
            @AuditedEstimateAmount AS [Root!1!AuditedEstimateAmount],
            @Adjustments AS [Root!1!AdjustmentsTotal],
            @Betterment AS [Root!1!Betterment],
            @Deductible AS [Root!1!Deductible],
            @NetAuditedEstimate AS [Root!1!NetAuditedEstimate],
            @AuditorName AS [Root!1!AuditorName],
            @AuditorTitle AS [Root!1!AuditorTitle],
            @AuditorPhone AS [Root!1!AuditorPhone],
            @AuditorEmail AS [Root!1!AuditorEmail],
            @OriginalEstimateDocumentID AS [Root!1!OriginalDocumentID],
            @AuditedEstimateDocumentID AS [Root!1!AuditedDocumentID],
            @PriceAgreedCD AS [Root!1!PriceAgreedCD],
            @ShopContacted AS [Root!1!ShopContacted],
			@ShopContactName AS [Root!1!ShopContactName],
			@PrimaryReasonCode AS [Root!1!PrimaryReasonCode],
			@PrimaryReasonCodeText AS [Root!1!PrimaryReasonCodeText],
			@SecondaryReasonCode AS [Root!1!SecondaryReasonCode],
			@SecondaryReasonCodeText as [Root!1!SecondaryReasonCodeText],			
            @Message AS [Root!1!Message],		
            @FormDataID AS [Root!1!FormDataID], 
            (SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtSurplusOEMComments')as [Root!1!txtSurplusOEMComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkSurplusOEM') as [Root!1!chkSurplusOEM],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtRecComments') as [Root!1!txtRecComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkReconditioned') as [Root!1!chkReconditioned],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtAfterMarketComments') as [Root!1!txtAfterMarketComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkAftermarket') as [Root!1!chkAftermarket],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtLKQComments') as [Root!1!txtLKQComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkLKQ') as [Root!1!chkLKQ],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkAltParts') as [Root!1!chkAltParts],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='AdjustmentAmount') as [Root!1!AdjustmentAmount],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='PriceNotAgreedItemsChecked') as [Root!1!PriceNotAgreedItemsChecked],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='AltPartsLocatedItemsChecked') as [Root!1!AltPartsLocatedItemsChecked],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtNoShopContactOther') as [Root!1!txtNoShopContactOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkNoShopContactOther') as [Root!1!chkNoShopContactOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtProbableTLComments') as [Root!1!txtProbableTLComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkProbableTL') as [Root!1!chkProbableTL],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtProhibitedComments') as [Root!1!txtProhibitedComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkProhibited') as [Root!1!chkProhibited],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRecommendLowerOrgEstComments') as [Root!1!txtRecommendLowerOrgEstComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRecommendLowerOrgEst') as [Root!1!chkRecommendLowerOrgEst],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRepairCompleteComments') as [Root!1!txtRepairCompleteComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRepairComplete') as [Root!1!chkRepairComplete],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtThreeUnsuccessfulAttemptsComments') as [Root!1!txtThreeUnsuccessfulAttemptsComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkThreeUnsuccessfulAttempts') as [Root!1!chkThreeUnsuccessfulAttempts],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkNoAttemptToContactShop') as [Root!1!chkNoAttemptToContactShop],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAltPartsAvailableComments') as [Root!1!txtAltPartsAvailableComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkAltPartsAvailable') as [Root!1!chkAltPartsAvailable],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkShopContacted') as [Root!1!chkShopContacted],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAuditorCommentsUser') as [Root!1!txtAuditorCommentsUser],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAdverseSubroComments') as [Root!1!txtAdverseSubroComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkAdverseSubro') as [Root!1!chkAdverseSubro],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='DataSet') as [Root!1!DataSet],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='Vehicles') as [Root!1!Vehicles],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='SequenceNumber') as [Root!1!SequenceNumber],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='ClaimAspectID') as [Root!1!ClaimAspectID],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAuditorComments') as [Root!1!txtAuditorComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtPriceNotAgreedOther') as [Root!1!txtPriceNotAgreedOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkPriceNotAgreedOther') as [Root!1!chkPriceNotAgreedOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtSysDiffComments') as [Root!1!txtSysDiffComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkSystemDiff') as [Root!1!chkSystemDiff],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRateDiffComments') as [Root!1!txtRateDiffComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRateDiff') as [Root!1!chkRateDiff],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAgreedPriceObtainedWith') as [Root!1!txtAgreedPriceObtainedWith],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkAgreedPriceObtained') as [Root!1!chkAgreedPriceObtained],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkPriceNotAgreed') as [Root!1!chkPriceNotAgreed],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtOther') as [Root!1!txtOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkOther') as [Root!1!chkOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRateCorrComments') as [Root!1!txtRateCorrComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRateCorrection') as [Root!1!chkRateCorrection],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtEstGuidelinesComments') as [Root!1!txtEstGuidelinesComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkEstimateGuidelines') as [Root!1!chkEstimateGuidelines],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtCorrComments') as [Root!1!txtCorrComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkCorrection') as [Root!1!chkCorrection]
			

    --FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END