-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSessionDel 
END

GO
/****** Object:  StoredProcedure [dbo].[uspSessionDel]    Script Date: 08/08/2014 12:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSessionDel
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Removes session data specified by the session key parameter
*
* PARAMETERS:  
* (I) @SessionID                The session key
*
* RESULT SET:
* None
*
*
* VSS
* $Workfile: uspSessionDel.sql $
* $Archive: /Database/APD/v1.0.0/procedure/uspSessionDel.sql $
* $Revision: 1 $
* $Author: Jim $
* $Date: 10/12/01 12:08p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSessionDel]
	@SessionID AS udt_session_id
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    -- SET TRANSACTION ISOLATION LEVEL to SERIALIZABLE to initiate pessimistic concurrency control
    
    -- SET TRANSACTION ISOLATION LEVEL SERIALIZABLE --glsd451

    -- Declare internal variables

    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @trancount          AS INT

    SET @error = 0
    
    -- Start the new transaction
    
    IF 0 = @error
    BEGIN
        SET @trancount = @@TRANCOUNT

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        BEGIN TRANSACTION

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        DELETE dbo.utb_session_data
         WHERE SessionID = @SessionID 

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        DELETE utb_session
         WHERE SessionID = @SessionID

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    -- Final transaction success determination
        
    IF @@TRANCOUNT > @trancount
    BEGIN
        IF 0 = @error
            COMMIT TRANSACTION
        ELSE
            ROLLBACK TRANSACTION
    END

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionDel TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO