-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionUpd' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSessionUpd 
END

GO
/****** Object:  StoredProcedure [dbo].[uspSessionUpd]    Script Date: 08/08/2014 12:28:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSessionUpd
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Updates the last accessed time for the session key
*
* PARAMETERS:  
* (I) @SessionID                The session key
* (I) @ModifiedDateTime         The new updated date
*
* RESULT SET:
* None
*
*
* VSS
* $Workfile: uspSessionUpd.sql $
* $Archive: /Database/APD/v1.0.0/procedure/uspSessionUpd.sql $
* $Revision: 1 $
* $Author: Jim $
* $Date: 10/12/01 12:09p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSessionUpd]
	@SessionID AS udt_session_id,
	@ModifiedDateTime AS udt_sys_last_updated_date = NULL
AS
BEGIN        
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    -- SET TRANSACTION ISOLATION LEVEL to SERIALIZABLE to initiate pessimistic concurrency control
    
    -- SET TRANSACTION ISOLATION LEVEL SERIALIZABLE --glsd451

    -- Declare internal variables

    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @trancount          AS INT

    SET @error = 0
    
    -- Start the new transaction
    
    IF 0 = @error
    BEGIN
        SET @trancount = @@TRANCOUNT

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        BEGIN TRANSACTION

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        SELECT @rowcount = 1
          FROM utb_session 
         WHERE SessionID = @SessionID

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    IF 0 = @error
    BEGIN
        IF 1 = @rowcount
        BEGIN
            UPDATE dbo.utb_session
               SET SysLastUpdatedDate = ISNULL(@ModifiedDateTime, CURRENT_TIMESTAMP)
             WHERE SessionID = @SessionID
        END

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    -- Final transaction success determination
        
    IF @@TRANCOUNT > @trancount
    BEGIN
        IF 0 = @error
            COMMIT TRANSACTION
        ELSE
            ROLLBACK TRANSACTION
    END

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionUpd' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionUpd TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END