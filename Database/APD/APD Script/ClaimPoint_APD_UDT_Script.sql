-- ==========================================================================
-- Purpose : Created new user defined data types for cellphonecarrier and prefmethodupd.
-- Updated by : glsd452.
-- ==========================================================================

	--CREATE TYPE [dbo].[udt_ph_cellphonecarrier] FROM [varchar](25) NULL
	--GO

	--CREATE TYPE [dbo].[udt_status_prefmethodupd] FROM [varchar](25) NULL
	--GO

	exec sp_addtype udt_ph_cellphonecarrier,'varchar(25)','NULL','dbo'
	exec sp_addtype udt_status_prefmethodupd, 'varchar(25)','NULL','dbo'
