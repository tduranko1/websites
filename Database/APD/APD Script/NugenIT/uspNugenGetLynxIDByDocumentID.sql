/************************************************************************************************************************
* PROCEDURE:    uspNugenGetLynxIDByDocumentID
* SYSTEM:       Lynx Services APD
* AUTHOR:       GLSD451
* FUNCTION:     If the Procedure is Already exists, drop the procedure.
************************************************************************************************************************/

BEGIN TRANSACTION
IF EXISTS (SELECT NAME FROM SYSOBJECTS WHERE NAME = 'uspNugenGetLynxIDByDocumentID' AND type = 'P')
	BEGIN
		DROP PROCEDURE dbo.uspNugenGetLynxIDByDocumentID

		-- Commit the transaction for dropping the stored procedure
		COMMIT
	END
GO
/****** Object:  StoredProcedure [dbo].[uspNugenGetLynxIDByDocumentID]    Script Date: 02/21/2013 08:07:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspNugenGetLynxIDByDocumentID]
(
@DocumentID varchar(10),
@LynxID varchar(10) OUTPUT
)
AS
BEGIN
DECLARE @TempID varchar(10)
SET NOCOUNT ON
	set @LynxID = (
SELECT dbo.utb_claim.lynxid
FROM dbo.utb_document INNER JOIN
dbo.utb_claim_aspect_service_channel_document ON dbo.utb_document.DocumentID = dbo.utb_claim_aspect_service_channel_document.DocumentID INNER JOIN
dbo.utb_claim_aspect_service_channel ON
dbo.utb_claim_aspect_service_channel_document.ClaimAspectServiceChannelID = dbo.utb_claim_aspect_service_channel.ClaimAspectServiceChannelID INNER JOIN
dbo.utb_claim INNER JOIN
dbo.utb_claim_aspect ON dbo.utb_claim.LynxID = dbo.utb_claim_aspect.LynxID ON
dbo.utb_claim_aspect_service_channel.ClaimAspectID = dbo.utb_claim_aspect.ClaimAspectID where utb_document.documentid = @DocumentID)

END
--Set @LynxID = @TempID
return @LynxID
SET NOCOUNT OFF
