/************************************************************************************************************************
* PROCEDURE:    uspNugenGetGUIDByLynxID
* SYSTEM:       Lynx Services APD
* AUTHOR:       GLSD451
* FUNCTION:     If the Procedure is Already exists, drop the procedure.
************************************************************************************************************************/

BEGIN TRANSACTION
IF EXISTS (SELECT NAME FROM SYSOBJECTS WHERE NAME = 'uspNugenGetGUIDByLynxID' AND type = 'P')
	BEGIN
		DROP PROCEDURE dbo.uspNugenGetGUIDByLynxID

		-- Commit the transaction for dropping the stored procedure
		COMMIT
	END
GO

/****** Object:  StoredProcedure [dbo].[uspNugenGetGUIDByLynxID]    Script Date: 02/21/2013 08:05:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspNugenGetGUIDByLynxID]
(
@LynxID varchar(10)
)
AS
BEGIN
SET NOCOUNT ON
IF EXISTS(SELECT SourceApplicationPassThruData FROM utb_claim_aspect WHERE LynxID = @LynxID and SourceApplicationPassThruData Is Not Null)
		BEGIN
			SELECT SourceApplicationPassThruData
			FROM utb_claim_aspect 
			WHERE LynxID = @LynxID and SourceApplicationPassThruData IS NOT NULL		
		END
ELSE
		BEGIN
				SELECT SourceApplicationPassThruData = NULL
		END	 	
END 
SET NOCOUNT OFF
