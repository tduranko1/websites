/**** Change the ReturnDocRoutingCD as Web Service for Electric Insurance ****/
GO
IF EXISTS (SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID= 158 and BundlingID = 31 and ClientBundlingID=21)
BEGIN
	Update utb_client_bundling Set ReturnDocRoutingCD = 'WS' where InsuranceCompanyID= 158 and BundlingID = 31 and ClientBundlingID=21
END 
GO
IF EXISTS (SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID= 158 and BundlingID = 3 and ClientBundlingID=39)
BEGIN
	Update utb_client_bundling Set ReturnDocRoutingCD = 'WS' where InsuranceCompanyID= 158 and BundlingID = 3 and ClientBundlingID=39
END
GO
IF EXISTS (SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID= 158 and BundlingID = 4 and ClientBundlingID=57)
BEGIN
	Update utb_client_bundling Set ReturnDocRoutingCD = 'WS' where InsuranceCompanyID= 158 and BundlingID = 4 and ClientBundlingID=57
END
GO
IF EXISTS (SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID= 158 and BundlingID = 22 and ClientBundlingID=141)
BEGIN
	Update utb_client_bundling Set ReturnDocRoutingCD = 'WS' where InsuranceCompanyID= 158 and BundlingID = 22 and ClientBundlingID=141
END
GO
IF EXISTS (SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID= 158 and BundlingID = 24 and ClientBundlingID=148)
BEGIN
	Update utb_client_bundling Set ReturnDocRoutingCD = 'WS' where InsuranceCompanyID= 158 and BundlingID = 24 and ClientBundlingID=148
END
Go
DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 158

DECLARE @MsgTempID INT
DECLARE @BundlingID INT

--------------------------------
-- Bunding Changes - Closing TL
--------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID

	AND t.Description LIKE '%Cash Out Alert & Close|Messages/msgELECPSCashOutAlert.xsl%'
	UPDATE utb_client_bundling SET ReturnDocRoutingCD = 'WS' WHERE BundlingID = @BundlingID


