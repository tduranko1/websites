-- Begin the transaction for dropping and creating the stored procedure

USE [udb_apd]
BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspHQEstimateSupplementDetails' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHQEstimateSupplementDetails
END
GO
/****** Object:  StoredProcedure [dbo].[uspHQEstimateSupplementDetails]    Script Date: 06/01/2015 08:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHQEstimateSupplementDetails
* SYSTEM:       Lynx Services APD
* AUTHOR:       Mahes Kumar
* FUNCTION:     Returns a Estimate and Supplement document details for the LynxID
* DATE:			27May2015
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* VERSION:		1.0 - Initial Development
*				
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHQEstimateSupplementDetails]
	@iLynxID BIGINT,
	@iVehicleNumber INT
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName					AS varchar(30)       -- Used for raise error stmts  
    DECLARE @EstimateCount				AS varchar(10)
    DECLARE @EstimateDocumentTypeID		AS INT
    DECLARE @SupplementDocumentTypeID	AS INT
    DECLARE @DocumentSourceID			AS INT

    SET @ProcName = 'uspHQEstimateSupplementDetails'
    
    SELECT @EstimateDocumentTypeID = DocumentTypeID
    FROM	utb_Document_Type
			WHERE Name = 'Estimate'
			
	SELECT @SupplementDocumentTypeID = DocumentTypeID
    FROM	utb_Document_Type
			WHERE Name = 'Supplement'
			
	SELECT @DocumentSourceID = DocumentSourceID
    FROM	utb_Document_Source
			WHERE Name = 'HyperQuest'			
        
    SELECT     		
		@EstimateCount = COUNT(dbo.utb_document.DocumentID) 	
	FROM         dbo.utb_claim_aspect_service_channel_document INNER JOIN
                      dbo.utb_claim_aspect_service_channel ON 
                      dbo.utb_claim_aspect_service_channel_document.ClaimAspectServiceChannelID = dbo.utb_claim_aspect_service_channel.ClaimAspectServiceChannelID INNER JOIN
                      dbo.utb_document ON dbo.utb_claim_aspect_service_channel_document.DocumentID = dbo.utb_document.DocumentID INNER JOIN
                      dbo.utb_claim_aspect ON dbo.utb_claim_aspect_service_channel.ClaimAspectID = dbo.utb_claim_aspect.ClaimAspectID
                      WHERE dbo.utb_claim_aspect.LynxID = @iLynxID
                      AND dbo.utb_claim_aspect.ClaimAspectNumber = @iVehicleNumber
                      AND dbo.utb_document.DocumentSourceID = @DocumentSourceID 
                      AND dbo.utb_document.DocumentTypeID = @EstimateDocumentTypeID        
                      AND dbo.utb_document.EstimateTypeCD = 'A' 
    
	IF @EstimateCount = 0
	BEGIN
		SET @EstimateCount = -1
	END
	ELSE IF @EstimateCount >= 1
	BEGIN
	-- Query to retrieve the Supplement document count
	SELECT     		
		@EstimateCount = COUNT(dbo.utb_document.DocumentID) 	
	FROM         dbo.utb_claim_aspect_service_channel_document INNER JOIN
                      dbo.utb_claim_aspect_service_channel ON 
                      dbo.utb_claim_aspect_service_channel_document.ClaimAspectServiceChannelID = dbo.utb_claim_aspect_service_channel.ClaimAspectServiceChannelID INNER JOIN
                      dbo.utb_document ON dbo.utb_claim_aspect_service_channel_document.DocumentID = dbo.utb_document.DocumentID INNER JOIN
                      dbo.utb_claim_aspect ON dbo.utb_claim_aspect_service_channel.ClaimAspectID = dbo.utb_claim_aspect.ClaimAspectID
                      WHERE dbo.utb_claim_aspect.LynxID = @iLynxID
                      AND dbo.utb_claim_aspect.ClaimAspectNumber = @iVehicleNumber
                      AND dbo.utb_document.DocumentSourceID = @DocumentSourceID 
                      AND dbo.utb_document.DocumentTypeID = @SupplementDocumentTypeID     
                      AND dbo.utb_document.EstimateTypeCD = 'A' 
	END	
	
	RETURN @EstimateCount	
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspHQEstimateSupplementDetails' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHQEstimateSupplementDetails TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO