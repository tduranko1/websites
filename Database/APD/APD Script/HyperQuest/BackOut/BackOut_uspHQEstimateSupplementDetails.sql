USE [udb_apd]
BEGIN TRANSACTION

-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspHQEstimateSupplementDetails' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHQEstimateSupplementDetails
END
GO
COMMIT