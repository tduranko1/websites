-- Insert the WebserviceName and WebserviceMethod

IF NOT EXISTS (select * from WebService where WebServiceName = 'PartnerIncomingWrapper.PartnerPosterService' and WebServiceMethod = 'SendWorkflowNotification')
BEGIN
insert into WebService (WebServiceName, WebServiceMethod,Authenticate) values ('PartnerIncomingWrapper.PartnerPosterService','SendWorkflowNotification',1)
Print 'Inserted'
END
ELSE
BEGIN
Print 'Already Exists'
END

-- Insert the WebServiceID and WebServiceAccountID

IF NOT EXISTS (select * from WebServiceAuthorization where WebServiceID = 10 and WebServiceAccountID = 5)
BEGIN
DECLARE @WebserviceID as int
select @WebserviceID = WebServiceID from WebService where WebServiceName = 'PartnerIncomingWrapper.PartnerPosterService' and WebServiceMethod = 'SendWorkflowNotification'
insert into WebServiceAuthorization (WebServiceID, WebServiceAccountID) values (@WebserviceID, 5)
--print @WebServiceID
Print 'Inserted'
END
ELSE
BEGIN
Print 'Already Exists'
END