-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDocumentInsDetail
END

GO
/****** Object:  StoredProcedure [dbo].[uspDocumentInsDetail]    Script Date: 12/16/2014 06:31:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspDocumentInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Binds an image to a given claim, pertains to, and assignment
*
* PARAMETERS:  
* (I) @NTUserID                     NT UserID of user making changes.  'System' is also acceptable.
* (I) @UserID                       UserID making changes.
* (I) @PertainsTo                   Entity to link the document to
* (I) @LynxID                       LynxID of the claim to link the document to.
* (I) @AssignmentID                 Assignment to link the document to
* (I) @CreatedDate                  Datetime that the document was entered into the system.  (This parameter is no longer
*                                         used.  It should be removed as a parameter as soon as all apps that call this proc
*                                         no longer send it)
* (I) @ImageDate                    Datetime that the document was received.
* (I) @ImageLocation                relative path of document (later coupled with Document/RootDirectory from config.xml)
* (I) @DocumentSource               Where did the document come from?
* (I) @DocumentType                 What kind of document is it?
* (I) @DirectionalCD                (I)ncoming or (O)utgoing
* (I) @SupplementSeqNumber          If document type is a suppliment, what is it's sequence number.
* (I) @ExternalReference            External reference for the document
* (I) @ImageType                    The image type (jpg, tif, xml, etc)
* (I) @Notifyevent                  Indicator whether to throw workflow event
* (I) @ClaimAspectServiceChannelID  The Claim Aspect Service ID to use to attach the document to
*
* RESULT SET:
* Returns DocumentID of inserted record.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspDocumentInsDetail]
    @NTUserID                       varchar(10)            = NULL,
    @UserID                         udt_std_id             = NULL,
    @PertainsTo                     varchar(5)             = Null,
    @LynxID                         udt_std_id_big         = Null,
    @AssignmentID                   udt_std_id_big         = NULL,
    @ClaimAspectID                  udt_std_id_big         = Null,
    @CreatedDate                    varchar(30)            = Null,  -- Legacy...Any value passed in this parameter will be ignored 6/17/2004 (JP)
    @ImageDate                      varchar(30),
    @ImageLocation                  udt_std_desc_long,
    @DocumentSource                 udt_std_desc_short,
    @DocumentType                   udt_std_desc_short,
    @DirectionalCD                  udt_std_cd             = 'I',
    @SupplementSeqNumber            udt_std_int            = Null,
    @ExternalReference              udt_std_desc_short     = Null,
    @ImageType                      udt_std_file_extension,
    @NotifyEvent                    udt_std_flag           = 1,
    @ApplicationCD                  udt_std_cd             = 'APD',
    @ClaimAspectServiceChannelID    udt_std_id_big         = Null,
    @SendToCarrierStatusCD          varchar(4)             = 'NS',
    @WarrantyFlag                   udt_std_flag           = 0,
    @HQEstimateTypeCD				udt_std_cd			   = NULL -- EstimateTypeCD added for HyperQuest
AS

BEGIN

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@NTUserID))) = 0 SET @NTUserID = NULL
    IF LEN(RTRIM(LTRIM(@PertainsTo))) = 0 SET @PertainsTo = NULL
    IF LEN(RTRIM(LTRIM(@CreatedDate))) = 0 SET @CreatedDate = NULL
    IF LEN(RTRIM(LTRIM(@ImageDate))) = 0 SET @ImageDate = NULL
    IF LEN(RTRIM(LTRIM(@ImageLocation))) = 0 SET @ImageLocation = NULL
    IF LEN(RTRIM(LTRIM(@DocumentSource))) = 0 SET @DocumentSource = NULL
    IF LEN(RTRIM(LTRIM(@DocumentType))) = 0 SET @DocumentType = NULL
    IF LEN(RTRIM(LTRIM(@DirectionalCD))) = 0 SET @DirectionalCD = NULL
    IF LEN(RTRIM(LTRIM(@ExternalReference))) = 0 SET @ExternalReference = NULL
    IF LEN(RTRIM(LTRIM(@SendToCarrierStatusCD))) = 0 SET @SendToCarrierStatusCD = 'NS'
    IF @WarrantyFlag IS NULL SET @WarrantyFlag = 0
    
    -- Temp fix for duplicate Document Type being repeated from Claimpoint document upload
    IF CHARINDEX(',', @DocumentType) > 0 
    BEGIN
       SELECT @DocumentType = value
       FROM dbo.ufnUtilityParseString(@DocumentType, ',', 1)
       WHERE strIndex = 1
    END
    
    -- Declare local variables
    
    DECLARE @AppraiserID                udt_std_id_big
    DECLARE @ClaimAspectTypeID          udt_std_id
    DECLARE @ClaimAspectNumber          udt_std_int
    DECLARE @CreatedUserID              udt_std_id
    DECLARE @CreatedUserRoleID          udt_std_id
    DECLARE @CreatedUserSupervisorFlag  udt_std_id
    DECLARE @DeskAuditAppraiserType     udt_std_cd
    DECLARE @DeskAuditID                udt_std_id_big
    DECLARE @DocumentID                 udt_std_id
    DECLARE @DocumentTypeID             udt_std_id_big
    DECLARE @DocumentSourceID           udt_std_id_big
    DECLARE @ElectronicFlag             int
    DECLARE @EstimateReviewDate         udt_std_datetime
    DECLARE @EstimateTypeCD             udt_std_cd
    DECLARE @EstimateTypeFlag           udt_std_flag
    DECLARE @EventName                  varchar(50)
    DECLARE @FullSummaryExistsFlag      udt_std_flag
    DECLARE @ImageDateWork              udt_std_datetime
    DECLARE @PertainsToName             udt_std_desc_short
    DECLARE @PertainsToWork             varchar(5)
    DECLARE @ShopLocationID             udt_std_id_big
    DECLARE @PrivateFlag                bit
    DECLARE @ServiceChannelCD           varchar(5)
    DECLARE @EstimateCount              int

    DECLARE @now                        udt_std_datetime
    
    DECLARE @ProcName                   udt_std_desc_short
    SET @ProcName = 'uspDocumentInsDetail'


    -- Validate that either an NTUserID or UserID was passed in 
    
    IF @NTUserID IS NULL AND @UserID IS NULL
    BEGIN
       -- No user passed in

        RAISERROR('%s: @NTUserID or @UserID required', 16, 1, @ProcName)
        RETURN
    END
    

    IF @UserID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
        BEGIN
            -- Invalid UserID

            RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
            RETURN
        END


        SET @CreatedUserID = @UserID
         
        SELECT  @CreatedUserRoleID = RoleID 
          FROM  dbo.utb_user_role
          WHERE UserID = @UserID 
            AND PrimaryRoleFlag = 1
            
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
            
        SELECT  @CreatedUserSupervisorFlag = SupervisorFlag 
          FROM  dbo.utb_user u 
          WHERE UserID = @UserID 

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
        SELECT  @CreatedUserID = u.UserID,
                @CreatedUserSupervisorFlag = u.SupervisorFlag  
          FROM  dbo.utb_user u 
          INNER JOIN dbo.utb_user_application ua ON (u.UserID = ua.UserID)
          INNER JOIN dbo.utb_application a ON (ua.ApplicationID = a.ApplicationID)
          WHERE ua.LogonID = @NTUserID
            AND a.Code = @ApplicationCD

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        IF @CreatedUserID IS NULL
        BEGIN
           -- Invalid UserID

            RAISERROR('101|%s|@NTUserID|%s', 16, 1, @ProcName, @NTUserID)
            RETURN
        END


        SELECT  @CreatedUserRoleID = RoleID 
          FROM  dbo.utb_user_role
          WHERE UserID = @CreatedUserID 
            AND PrimaryRoleFlag = 1
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END


    IF @ClaimAspectID IS NULL
    BEGIN
        -- We need to come up with the Claim Aspect ID...use the LynxID and Pertains to to do this
        
        -- Check to make sure a valid LynxID was passed in
        IF NOT EXISTS (SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID)
        BEGIN
            -- Invalid LynxID

            RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
            RETURN
        END

        -- Validate @PertainsTo
        IF Upper(@PertainsTo) = 'CLM0'
        BEGIN
            SET @PertainsTo = 'Clm'
        END

        IF @PertainsTo IS NOT NULL
        BEGIN
            SELECT @PertainsToWork = CASE
                                       WHEN Len(@PertainsTo) > 3 THEN 
                                                Left(@PertainsTo, 3) + Convert(varchar(1), Replace(Substring(@PertainsTo, 4, 1), 0, '')) + Convert(varchar(4), Substring(@PertainsTo, 5, Len(@PertainsTo) - 4))
                                       ELSE 
                                                Left(@PertainsTo, 3) + Convert(varchar(1), Replace(Substring(@PertainsTo, 4, 1), 0, ''))
                                     END
        END
        ELSE
        BEGIN
            SET @PertainsToWork = NULL
        END
    
        IF ((@PertainsToWork IS NULL) OR
            (@PertainsToWork NOT IN (SELECT EntityCode FROM dbo.ufnUtilityGetClaimEntityList ( @LynxID, 0, 0 )) ))  -- (0, 0) means all aspects, open or closed
        BEGIN
            -- Invalid Pertains To

            --RAISERROR('101|%s|@PertainsTo|%s', 16, 1, @ProcName, @PertainsTo)
            --RETURN
            SELECT TOP 1 @PertainsToWork = EntityCode,
                         @PertainsTo = EntityCode
            FROM dbo.ufnUtilityGetClaimEntityList ( @LynxID, 0, 0 )
            WHERE EntityCode like 'veh%'
            
        END

        IF Upper(@PertainsTo) = 'CLM'
        BEGIN
            SET @PertainsToName = 'Claim'
        END       
        ELSE
        BEGIN
            SELECT @PertainsToName = Name 
              FROM dbo.ufnUtilityGetClaimEntityList (@LynxID, 0, 0)   -- (0, 0) means all aspects, open or closed
              WHERE EntityCode = @PertainsTo

            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END              
        END


        -- Got a valid Pertains To.  Convert this to ClaimAspectType and Number
        
        SELECT @ClaimAspectTypeID = AspectTypeID,
               @ClaimAspectNumber = AspectNumber
          FROM dbo.ufnUtilityGetClaimAspect (@PertainsTo)

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              


        -- Now combine these with the LynxID to get the ClaimAspectID
        
        SELECT  @ClaimAspectID = ClaimAspectID
          FROM  dbo.utb_claim_aspect
          WHERE LynxID = @LynxID
            AND ClaimAspectTypeID = @ClaimAspectTypeID
            AND ClaimAspectNumber = @ClaimAspectNumber

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              
    END


    IF @ClaimAspectID IS NULL OR
        NOT EXISTS (SELECT ClaimAspectID From dbo.utb_claim_aspect where ClaimAspectID = @ClaimAspectID)
    BEGIN
       -- Invalid Claim Aspect ID

        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- If @AssignmentID is passed in, validate that it's valid for the claim aspect
   
    IF @AssignmentID IS NOT NULL
    BEGIN
        IF NOT EXISTS(
			SELECT a.AssignmentID 
			FROM dbo.utb_assignment a
			/*********************************************************************************
			Project: 210474 APD - Enhancements to support multiple concurrent service channels
			Note:	"ClaimAspectID" column is no longer part of utb_Assignment table.  Instead
				use utb_Claim_Aspect_Service_Channel to join to utb_assignment and use
				"ClaimAspectID" in utb_Claim_Aspect_Service_Channel table
				M.A. 20061108
			*********************************************************************************/
			INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
			WHERE casc.ClaimAspectID = @ClaimAspectID 
			AND a.AssignmentID = @AssignmentID
		     )
        BEGIN
            -- Invalid Assignment ID for this claim aspect

            RAISERROR('%s: @AssignmentID %u invalid for claim aspect %u', 16, 1, @ProcName, @AssignmentID, @ClaimAspectID)
            RETURN
        END
    END
            
    
    -- Validate ImageDate
    
    IF @ImageDate IS NOT NULL
    BEGIN
        IF ISDATE(@ImageDate) = 0
        BEGIN
            -- Invalid ImageDate

            RAISERROR('101|%s|@ImageDate|%s', 16, 1, @ProcName, @ImageDate)
            RETURN
        END
        ELSE
        BEGIN
           SET @ImageDateWork = CONVERT(DATETIME, @ImageDate)
        END
    END


    -- Check Document Type
    
    SELECT  @DocumentTypeID = DocumentTypeID,
            @EstimateTypeFlag = EstimateTypeFlag
      FROM  dbo.utb_document_type 
      WHERE Name = @DocumentType
        --AND EnabledFlag = 1  --Project:210474 APD Remarked-off the condition when we did the code merge M.A.20061114

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              

    IF @DocumentTypeID IS NULL
    BEGIN
        -- Invalid Document Type
        
        RAISERROR('101|%s|@DocumentType|%s', 16, 1, @ProcName, @DocumentType)
        RETURN
    END


    IF @DocumentType = 'Estimate'
    BEGIN
        SET @SupplementSeqNumber = 0
    END
    
    
    IF @DocumentType = 'Supplement' AND
       (@SupplementSeqNumber IS NULL OR @SupplementSeqNumber = 0)
    BEGIN
        -- Invalid Supplement Sequence Number
        
        RAISERROR('101|%s|@SupplementSeqNumber|%u', 16, 1, @ProcName, @SupplementSeqNumber)
        RETURN
    END


    -- Check Document Source
    
    SELECT  @DocumentSourceID = DocumentSourceID,
            @ElectronicFlag = VanFlag
      FROM  dbo.utb_document_source 
      WHERE Name = @DocumentSource
        AND EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              

    IF @DocumentSourceID IS NULL
    BEGIN
        -- Invalid Document Source
        
        RAISERROR('101|%s|@DocumentSource|%s', 16, 1, @ProcName, @DocumentSource)
        RETURN
    END
            

    -- Check DirectionalCD
    
    IF @DirectionalCD IS NULL
    BEGIN
        SET @DirectionalCD = 'I'
    END
    
    IF Upper(@DirectionalCD) NOT IN ('I', 'O')
    BEGIN
        RAISERROR('101|%s|@DirectionalCD|%s', 16, 1, @ProcName, @DirectionalCD)
        RETURN
    END


    -- Get information on the LYNX Desk audit unit so we can distinguish assignments to it versus regular assignments

    SELECT  @DeskAuditAppraiserType = Left(LTrim(RTrim(value)), CharIndex(',', LTrim(RTrim(value))) - 1),
            @DeskAuditID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'DESK_AUDIT_AUTOID'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- check to see if the sent to carrier status code is in the approved list.
    IF NOT EXISTS(SELECT Code
                  FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'SendToCarrierStatusCD')
                  WHERE Code = @SendToCarrierStatusCD)
    BEGIN
      -- code does not exist in the reference. Let us override it to "Not Sent"
      SET @SendToCarrierStatusCD = 'NS'
    END
    

    SET @now = CURRENT_TIMESTAMP


    -- When an electronic estimate arrives, we need to check the AssignmentID.  If the assignment is to the LYNX Desk Audit Unit,
    -- the estimate should be set to "Audited", otherwise it should be set to "Original".  It should automatically be set to 
    -- non-duplicate.  Also, the document record should be set to reviewed and have a full summary.  
    
    SET @EstimateReviewDate = NULL
    SET @EstimateTypeCD = NULL
    SET @FullSummaryExistsFLag = 0
        
    IF (@ElectronicFlag = 1) AND (@EstimateTypeFlag = 1)
    BEGIN
		--	Original and Audited set for HyperQuest        
        IF (@DocumentSourceID = 16 AND @HQEstimateTypeCD IS NOT NULL)
        BEGIN
			SET @EstimateTypeCD = @HQEstimateTypeCD
        END       
        ELSE
        BEGIN            
        -- Check if this is an assignment to the desk audit unit 
			SELECT  @AppraiserID = AppraiserID,
					@ShopLocationID = ShopLocationID
			  FROM  dbo.utb_assignment
			  WHERE AssignmentID = @AssignmentID
	          
			IF ((@ShopLocationID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'S')) OR
			   ((@AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A'))
			BEGIN
				-- This is a desk audit assignment, and by extension, a desk audit estimate
	    
				SET @EstimateTypeCD = 'A'   -- Set type to "Audited"
			END
			ELSE
			BEGIN
				SET @EstimateTypeCD = 'O'   -- Set type to "Original"
			END
	     END    
			SET @EstimateReviewDate = @now
			SET @FullSummaryExistsFlag = 1       
    END
    
    IF @WarrantyFlag = 1
    BEGIN
      
      IF EXISTS(SELECT casc.ClaimAspectID
                  FROM utb_warranty_assignment wa
                  LEFT JOIN utb_claim_aspect_service_channel casc ON wa.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                  WHERE casc.ClaimAspectID = @ClaimAspectID
                    AND casc.ServiceChannelCD = 'PS'
                    AND wa.CancellationDate IS NULL)
      BEGIN
                    
         SET @AssignmentID = NULL
         
         -- Warranty documents can be attached to the program shop channel only
         SELECT @ClaimAspectServiceChannelID = ClaimAspectServiceChannelID
         FROM utb_claim_aspect_service_channel casc
         WHERE casc.ServiceChannelCD = 'PS'
           AND casc.ClaimAspectID = @ClaimAspectID
           
         IF @ClaimAspectServiceChannelID IS NULL SET @WarrantyFlag = 0
         
      END
      ELSE
      BEGIN
         SET @WarrantyFlag = 0
      END
         
    END
        
    -- Begin insert
    
    BEGIN TRANSACTION InsertDocument
    
    INSERT INTO utb_Document
        (AssignmentID, 
         CreatedUserID, 
         CreatedUserRoleID, 
         DocumentSourceID, 
         DocumentTypeID,
         CreatedDate, 
         CreatedUserSupervisorFlag, 
         DirectionalCD, 
         EnabledFlag,
         EstimateReviewDate,
         EstimateTypeCD,
         ExternalReferenceDocumentID, 
         FullSummaryExistsFlag,
         ImageLocation, 
         ImageType, 
         --PrivateFlag,
         ReceivedDate, 
         SupplementSeqNumber,
		   SendToCarrierStatusCD, --Project:210474 APD Added the column when we did the code merge M.A.20061211
		   WarrantyFlag,
         SysLastUserID, 
         SysLastUpdatedDate)
      VALUES (@AssignmentID, 
              @CreatedUserID, 
              @CreatedUserRoleID, 
              @DocumentSourceID, 
              @DocumentTypeID,
              @now, 
              @CreatedUserSupervisorFlag, 
              @DirectionalCD, 
              1,    -- Set document as enabled
              @EstimateReviewDate,
              @EstimateTypecD,
              @ExternalReference, 
              @FullSummaryExistsFlag,
              @ImageLocation, 
              @ImageType, 
              --@PrivateFlag,
              @ImageDateWork, 
              @SupplementSeqNumber,
			     @SendToCarrierStatusCD, --Project:210474 APD Added the column value when we did the code merge M.A.20061211
			     @WarrantyFlag,
              @CreatedUserID, 
              @Now)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    ELSE
    BEGIN
        SELECT @DocumentID = SCOPE_IDENTITY()
    END
    
/*
--Project:210474 APD Remarked-off the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    INSERT INTO dbo.utb_claim_aspect_document
        (ClaimAspectID,
         DocumentID,
         SysLastUserID,
         SysLastUpdatedDate)
      VALUES (@ClaimAspectID, 
              @DocumentID, 
              @CreatedUserID, 
              @Now)   
--Project:210474 APD Remarked-off the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
*/
--Project:210474 APD Added the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227

--if only @AssignmentID was provided we can find the specific Service Channel ID
if @AssignmentID is not null and @ClaimAspectServiceChannelID is null
    begin
        select     @ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID,
                   @ServiceChannelCD = casc.ServiceChannelCD
        from       utb_Assignment a
        left join  utb_Claim_Aspect_Service_Channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        where      AssignmentId = @AssignmentID
    end
--If the Service Channel ID is still null then get the primary Service Channel ID linked to the Claim Aspect ID
if @ClaimAspectServiceChannelID is null
    begin
        Select    @ClaimAspectServiceChannelID = ClaimAspectServiceChannelID,
                  @ServiceChannelCD = ServiceChannelCD
        from      utb_Claim_Aspect_Service_Channel
        where     ClaimAspectID = @ClaimAspectID
        and       PrimaryFlag = 1
    end

--Now that we have the Service Channel ID, insert the row in the table utb_Claim_Aspect_Service_Channel_Document
    Insert into utb_Claim_Aspect_Service_Channel_Document
            (
            ClaimAspectServiceChannelID,
            DocumentID,
            SysLastUserID,
            SysLastUpdatedDate
            )
    Values (
           @ClaimAspectServiceChannelID,
           @DocumentID, 
           @CreatedUserID, 
           @Now
           )
--Project:210474 APD Added the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_claim_aspect_service_channel_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
            
            
    -- When an electronic estimate arrives it is automatically set to non-duplicate.  A check must be made for non-electronic 
    -- estimates with the same sequence number and type ("Original" vs. "Audited").  Any that exist should be set to duplicate.
    
    IF (@ElectronicFlag = 1) AND (@EstimateTypeFlag = 1)
    BEGIN      
        -- Flag as duplicate any existing non-electronic estimates/supplements matching this document's sequence number.
      
        UPDATE dbo.utb_document
          SET  DuplicateFlag = 1
          FROM dbo.utb_claim_aspect_Service_Channel_document cascd
          INNER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
          INNER JOIN dbo.utb_document d ON (cascd.DocumentID = d.DocumentID)
          INNER JOIN dbo.utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID)
          INNER JOIN dbo.utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
          WHERE casc.ClaimAspectID = @ClaimAspectID 
            AND dt.EstimateTypeFlag = 1
            AND d.SupplementSeqNumber = @SupplementSeqNumber
            AND EstimateTypeCD = @EstimateTypeCD
            AND d.DocumentID <> @DocumentID
            AND casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061228
            
        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END      
            
    IF @NotifyEvent = 1
    BEGIN
        DECLARE @EventID                udt_std_int
        DECLARE @HistoryDescription     udt_std_desc_long
        DECLARE @WorkFlowPertainsTo     udt_std_desc_long
        DECLARE @ConditionalValue       udt_std_name
        /************************************************************************************
        --Project:210474 APD Added the following code when we did the code merge M.A.20061114
        ************************************************************************************/
        DECLARE @OwnerUserID            udt_std_id
        DECLARE @SupportUserID          udt_std_id
        DECLARE @AnalystUserID          udt_std_id
        
        SELECT @OwnerUserID = ISNULL(OwnerUserID,0),
               @AnalystUserID = ISNULL(AnalystUserID,0), 
               @SupportUserID = ISNULL(SupportUserID,0)
        FROM dbo.utb_claim_aspect ca
        WHERE ca.ClaimAspectID = @ClaimAspectID
        /************************************************************************************
        --Project:210474 APD Added the above code when we did the code merge M.A.20061114
        ************************************************************************************/


        -- If an outside process is attaching a document other than an email, we need to generate an appropriate review task to 
        -- notify the claim owner of this.  We need to for two conditions:
        -- 1)  If the claim owner attaches the document or generates the document from a system process (eg. an invoice)
        -- 2)  The document source is from email
        -- 3)  The estimate is an electronic audited estimate (meaning they uploaded it)
        -- If either of these conditions is met, we don't bother generating the task

        --Project:210474 APD Remarked-off the code below when we did the code merge M.A.20061114
/*        IF (@CreatedUserID = (SELECT  ca.OwnerUserID 
                                FROM  dbo.utb_claim_aspect ca  
                                WHERE ca.ClaimAspectID = @ClaimAspectID 
                                  ))  OR*/

        -- Check for ClaimOwner was removed to support new Assignment Pools / DB Schema -- SWR 211030                                
        IF (@DocumentSource = 'Email') OR
           (@ElectronicFlag = 1 AND @EstimateTypeFlag = 1 AND @EstimateTypeCD = 'A')
        BEGIN            
            SET @ConditionalValue = NULL
        END
        ELSE
        BEGIN
            SET @ConditionalValue = 'NonUserSource'
        END
                        
        
        IF @ElectronicFlag = 0
        BEGIN   
            --Imaged Source
			IF @EstimateTypeFlag = 1
			BEGIN
				SET @EventName = 'Imaged Estimate Received'
				SET @HistoryDescription  = @DocumentType + ' has been added to ' + @PertainsToName + ' (Imaged)'
			END
			ELSE
			BEGIN
				IF @DocumentType = 'Photograph'
				BEGIN
					SET @EventName = 'Photo Received'
					SET @HistoryDescription  = @DocumentType + ' has been added to ' + @PertainsToName + ' (Imaged)'
				END
				ELSE
				BEGIN
					SET @EventName = 'New Imaged Document'
					SET @HistoryDescription  = @DocumentType + ' has been added to ' + @PertainsToName + ' (Imaged)'
				END
				
				-- 28aug2014 - TVD - Adding ReInspection Report Upload Event
				-- 11Nov2014 - TVD - Added DocumentSourceID to IF to protect APD uploaded docs
				IF @DocumentType = 'Reinspection Report' AND @DocumentSourceID = 13
				BEGIN
					SET @ConditionalValue = 'PS'
					SET @EventName = 'Reinspection Report Uploaded'
					SET @HistoryDescription  = @DocumentType + ' has been added to ' + @PertainsToName + ' (Imaged)'
				END
			END
        END
        ELSE
        BEGIN
            --Electronic Source

            SET @EventName = 'New Electronic Document'
            SET @HistoryDescription  = @DocumentType + ' has been added to ' + @PertainsToName + ' (Electronic)'
        END

        SELECT  @EventID = EventID 
          FROM  utb_event 
          WHERE Name = @EventName

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              

        IF @EventID IS NULL
        BEGIN
            -- Event Name not found
        
            RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        

        -- Make the workflow call
       
        EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectID = @ClaimAspectID,
                                    @Description = @HistoryDescription,
                                    @Key = @DocumentType,
                                    @UserID = @CreatedUserID,
                                    @ConditionValue = @ConditionalValue,
                                    @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

        IF @@ERROR <> 0
        BEGIN
            -- APD Workflow Notification failed

            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END
    
    IF @ServiceChannelCD = 'TL'
    BEGIN
      IF @DocumentType IN ('Vehicle Owner Title',
                            'Vehicle Owner POA',
                            'Lien Title Copy',
                            'Letter of Guarantee',
                            'Lien Holder POA',
                            'Lien Holder Clear Title',
                            'Salvage Bundle')
      BEGIN
          DECLARE @MetricDataPoint as varchar(50)
          SELECT @MetricDataPoint = CASE @DocumentType
			                              WHEN 'Vehicle Owner Title' THEN 'VOTitleReceivedDate'
			                              WHEN 'Vehicle Owner POA' THEN 'VOPoAReceivedDate'
			                              WHEN 'Lien Holder POA' THEN 'LHLoGTransmitDate' -- need to create a new data point and them change this
			                              WHEN 'Letter of Guarantee' THEN 'LHLoGReceivedDate'
			                              WHEN 'Lien Holder Clear Title' THEN 'LHTitleReceivedDate'
			                              WHEN 'Salvage Bundle' THEN 'SettlementSalvageBundleDate'
			                              ELSE ''
			                           END
          
          
          IF @MetricDataPoint <> ''
          BEGIN
             -- update the total loss metric.
             EXEC dbo.uspTLUpdateMetrics @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
							                    @MetricDataPoint = @MetricDataPoint,
								                 @DataValue = @now

              IF @@ERROR <> 0
              BEGIN
               raiserror('104|%s|%s',16,1,@ProcName,'uspTLUpdateMetrics')
               ROLLBACK TRANSACTION
               RETURN
              END

          END

      END
    END   
   

    IF @EstimateTypeFlag = 1 
    BEGIN
      SELECT @EstimateCount = COUNT(d.DocumentID)
      FROM dbo.utb_document d
      LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
      LEFT JOIN dbo.utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
      WHERE cascd.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
        AND d.EnabledFlag = 1
        AND dt.EstimateTypeFlag = 1
        AND d.DocumentID <> @DocumentID
      
      IF @EstimateCount = 0
      BEGIN
         --1st estimate is being added to the claim aspect service channel. Trigger workflow.
         SELECT @EventID = EventID
         FROM utb_event
         WHERE Name = 'First Estimate Received'
         
         IF @EventID IS NOT NULL
         BEGIN
         
            IF @ServiceChannelCD IS NULL OR LEN(@ServiceChannelCD) = 0
            BEGIN
               SELECT @ServiceChannelCD = ServiceChannelCD
               FROM utb_claim_aspect_service_channel 
               WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
            END
            
            EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                        @ClaimAspectID = @ClaimAspectID,
                                        @Description = '',
                                        @Key = @DocumentType,
                                        @UserID = @CreatedUserID,
                                        @ConditionValue = @ServiceChannelCD,
                                        @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
         
         END
      END
      
    END

    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
		 ROLLBACK TRANSACTION
        RETURN
    END              

	 COMMIT TRANSACTION InsertDocument

    RETURN @documentID 
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDocumentInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
