--Query to insert the new HQ LINKS in utb_estimate_package
IF NOT Exists (Select * from utb_estimate_package where EstimatePackageID = 5 and Name = 'HQ Link � CCC')
BEGIN
	Insert into utb_estimate_package (EstimatePackageID, DisplayOrder, EnabledFlag, Name, SysMaintainedFlag, SysLastUserID, SysLastUpdatedDate) 
	Values (5, 5, 1, 'HQ Link � CCC', 0, 0, GETDATE())
	Print 'HQ Link � CCC inserted successfully...'
END
ELSE
BEGIN
	Print 'HQ Link � CCC is already Exists...'
END

IF NOT Exists (Select * from utb_estimate_package where EstimatePackageID = 6 and Name = 'HQ Link � Audatex')
BEGIN
	Insert into utb_estimate_package (EstimatePackageID, DisplayOrder, EnabledFlag, Name, SysMaintainedFlag, SysLastUserID, SysLastUpdatedDate) 
	Values (6, 6, 1, 'HQ Link � Audatex', 0, 0, GETDATE())
	Print 'HQ Link � Audatex inserted successfully...'
END
ELSE
BEGIN
	Print 'HQ Link � Audatex is already Exists...'
END

IF NOT Exists (Select * from utb_estimate_package where EstimatePackageID = 7 and Name = 'HQ Link � Mitchell')
BEGIN
	Insert into utb_estimate_package (EstimatePackageID, DisplayOrder, EnabledFlag, Name, SysMaintainedFlag, SysLastUserID, SysLastUpdatedDate) 
	Values (7, 7, 1, 'HQ Link � Mitchell', 0, 0, GETDATE())
	Print 'HQ Link � Mitchell inserted successfully...'
END
ELSE
BEGIN
	Print 'HQ Link � Mitchell is already Exists...'
END


--Query to insert the new HQ Link in utb_communication_method table
IF NOT Exists (Select * from utb_communication_method where CommunicationMethodID = 17 and Name = 'HQ Link')
BEGIN
	Insert into utb_communication_method (CommunicationMethodID, DisplayOrder, EnabledFlag, Name, RoutingCD, SysMaintainedFlag, SysLastUserID, SysLastUpdatedDate) 
	Values (17, 17, 1, 'HQ Link', 'HQ', 1, 0, GETDATE())
	Print 'HQ Link inserted successfully...'
END
ELSE
BEGIN
	Print 'HQ Link is already Exists...'
END
