--Create new task for HQ in DEV
--select * from utb_event order by Name
IF NOT EXISTS (SELECT EventID FROM utb_event WHERE Name = 'HQ Estimate-Supplement Rejected')
BEGIN
	INSERT INTO utb_event VALUES (91,9,10,1,1,'HQ Estimate-Supplement Rejected',0,CURRENT_TIMESTAMP) -- Need to check the DisplayOrder value to insert
END
ELSE
BEGIN
	SELECT 'Event already added...'
END

--select * from utb_workflow
IF NOT EXISTS (SELECT WorkflowID FROM utb_workflow WHERE WorkflowID = 193)
BEGIN
	INSERT INTO utb_workflow VALUES (193,259,0,91,'E',0,CURRENT_TIMESTAMP)  --Need to check the Insuranace Company to configure or Make it as NULL
END
ELSE
BEGIN
	SELECT 'Workflow already added...'
END

--select * from utb_spawn
IF NOT EXISTS (SELECT SpawnID FROM utb_spawn WHERE SpawnID = 306)
BEGIN
	INSERT INTO utb_spawn VALUES (193,'PS','%PertainsToName%',NULL,1,104,NULL,'T',0,CURRENT_TIMESTAMP) -- What is SpawningID and what we need to use here. Seems like SpawningID is a Task ID
	--UPDATE utb_spawn SET ConditionalValue = NULL, SysLastUpdatedDate = CURRENT_TIMESTAMP WHERE SpawnID = 306
END
ELSE
BEGIN
	SELECT 'Spawn already added...'
END

--select * from utb_task order by name
IF NOT EXISTS (SELECT TaskID FROM utb_task WHERE TaskID = 104 AND Name = 'HQ Estimate-Supplement Rejected')
BEGIN
	INSERT INTO utb_task Values (104,18,1,1,NULL,10,1,0,'HQ Estimate-Supplement Rejected',0,0,0,CURRENT_TIMESTAMP)
	--UPDATE utb_task SET NoteTypeDefaultID = 13, SysLastUpdatedDate = CURRENT_TIMESTAMP WHERE TaskID = 104 AND Name = 'HQ Estimate-Supplement Rejected'
END
ELSE
BEGIN
	SELECT 'Task already added...'
END

--select * from utb_status_task order by name
IF NOT EXISTS (SELECT TaskID, StatusID FROM utb_status_task WHERE TaskID = 104 and StatusID = 101 )
BEGIN
	INSERT INTO utb_status_task Values (101,104,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--select * from utb_status_task order by name
IF NOT EXISTS (SELECT TaskID, StatusID FROM utb_status_task WHERE TaskID = 104 and StatusID = 197 )
BEGIN
	INSERT INTO utb_status_task Values (197,104,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--select * from utb_status_task order by name
IF NOT EXISTS (SELECT TaskID, StatusID FROM utb_status_task WHERE TaskID = 104 and StatusID = 198 )
BEGIN
	INSERT INTO utb_status_task Values (198,104,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--select * from utb_status_task order by name
IF NOT EXISTS (SELECT TaskID, StatusID FROM utb_status_task WHERE TaskID = 104 and StatusID = 199 )
BEGIN
	INSERT INTO utb_status_task Values (199,104,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--select * from utb_status_task order by name
IF NOT EXISTS (SELECT TaskID, StatusID FROM utb_status_task WHERE TaskID = 104 and StatusID = 200 )
BEGIN
	INSERT INTO utb_status_task Values (200,104,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--select * from utb_status_task order by name
IF NOT EXISTS (SELECT TaskID, StatusID FROM utb_status_task WHERE TaskID = 104 and StatusID = 297 )
BEGIN
	INSERT INTO utb_status_task Values (297,104,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--select * from utb_status_task order by name
IF NOT EXISTS (SELECT TaskID, StatusID FROM utb_status_task WHERE TaskID = 104 and StatusID = 298 )
BEGIN
	INSERT INTO utb_status_task Values (298,104,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--select * from utb_status_task order by name
IF NOT EXISTS (SELECT TaskID, StatusID FROM utb_status_task WHERE TaskID = 104 and StatusID = 299 )
BEGIN
	INSERT INTO utb_status_task Values (299,104,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 1 )
BEGIN
	INSERT INTO utb_task_role Values (104,1,0,0,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 2 )
BEGIN
	INSERT INTO utb_task_role Values (104,2,0,0,0,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 3 )
BEGIN
	INSERT INTO utb_task_role Values (104,3,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 4 )
BEGIN
	INSERT INTO utb_task_role Values (104,4,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 5 )
BEGIN
	INSERT INTO utb_task_role Values (104,5,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 6 )
BEGIN
	INSERT INTO utb_task_role Values (104,6,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 7 )
BEGIN
	INSERT INTO utb_task_role Values (104,7,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 8 )
BEGIN
	INSERT INTO utb_task_role Values (104,8,0,0,0,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 9 )
BEGIN
	INSERT INTO utb_task_role Values (104,9,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 10 )
BEGIN
	INSERT INTO utb_task_role Values (104,10,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 11 )
BEGIN
	INSERT INTO utb_task_role Values (104,11,0,0,0,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 12 )
BEGIN
	INSERT INTO utb_task_role Values (104,12,0,0,0,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 13 )
BEGIN
	INSERT INTO utb_task_role Values (104,13,0,0,0,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 15 )
BEGIN
	INSERT INTO utb_task_role Values (104,15,0,0,0,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 20 )
BEGIN
	INSERT INTO utb_task_role Values (104,20,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 21 )
BEGIN
	INSERT INTO utb_task_role Values (104,21,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 22 )
BEGIN
	INSERT INTO utb_task_role Values (104,22,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 23 )
BEGIN
	INSERT INTO utb_task_role Values (104,23,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_Task_Role	
IF NOT EXISTS (SELECT TaskID, RoleID FROM utb_task_role WHERE TaskID = 104 and RoleID = 24 )
BEGIN
	INSERT INTO utb_task_role Values (104,24,1,1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Status task already added...'
END

--Select * from utb_document_source	
IF NOT EXISTS (SELECT DocumentSourceID FROM utb_document_source WHERE DocumentSourceID = 15)
BEGIN
	INSERT INTO utb_document_source Values (15,NULL,14,1,'HyperQuest',1,1,0,CURRENT_TIMESTAMP)	
END
ELSE
BEGIN
	SELECT 'Document Source already added...'
END



