-- Begin the transaction for dropping and creating the stored procedure 

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsFeesGetListXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmInsFeesGetListXML 
END

GO
/****** Object:  StoredProcedure [dbo].[uspAdmInsFeesGetListXML]    Script Date: 05/08/2014 11:03:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdmInsFeesGetListXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Function to return the fee structure for the Insurance Company
*
* PARAMETERS:  
* (I) @InsuranceCompanyID       Insurance Company ID
*
* RESULT SET:
* a XML document of Fees applicable to the Insurance Company
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspAdmInsFeesGetListXML]
    @InsuranceCompanyID udt_std_id
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @Now               AS DateTime          -- Timestamp for Procedure
    DECLARE @BillingModelCD    AS varchar(5)

    SET @ProcName = 'uspAdmInsFeesGetListXML'
    SET @Now = Current_TimeStamp

    --Verify that a valid Insurance Company was passed in.
    IF (@InsuranceCompanyID is Null) OR
    NOT EXISTS (SELECT InsuranceCompanyID FROM utb_Insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance CompanyID
    
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END

    DECLARE @tmpReference TABLE
    (
        ServiceID        int,
        Name             varchar(50),
        ServiceChannelCD varchar(5),
        BillingModelCD   varchar(5),
        DisplayOrder    smallint
    )

    INSERT INTO @tmpReference
    SELECT ServiceID, Name, ServiceChannelCD, BillingModelCD, DisplayOrder
       FROM utb_service
       WHERE EnabledFlag = 1
    ORDER BY DisplayOrder
    
--    select * from @tmpReference
    
    SELECT @BillingModelCD = BillingModelCD
    FROM utb_insurance
    WHERE InsuranceCompanyID = @InsuranceCompanyID

    
    --Begin Select Statement
    SELECT
        1                       AS tag,
        Null                    AS Parent,
        @InsuranceCompanyID     AS [Root!1!InsuranceCompanyID],
        @BillingModelCD         AS [Root!1!BillingModelCD],
        -- Client Fee
        Null                    AS [ClientFee!2!ClientFeeID],
        Null                    AS [ClientFee!2!EnabledFlag],
        Null                    AS [ClientFee!2!DisplayOrder],
        Null                    As [ClientFee!2!CategoryCD],
        Null                    As [ClientFee!2!ClaimAspectTypeID],
        Null                    As [ClientFee!2!Description],
        Null                    AS [ClientFee!2!FeeAmount],
        Null                    As [ClientFee!2!FeeInstructions],
        Null                    As [ClientFee!2!ItemizeFlag],
        Null                    As [ClientFee!2!InvoiceDescription],
        Null                    As [ClientFee!2!EffectiveStartDate],
        Null                    As [ClientFee!2!EffectiveEndDate],
        Null                    As [ClientFee!2!AppliesWhenCD],
        Null                    As [ClientFee!2!HideFromInvoiceFlag],
        Null                    AS [ClientFee!2!SysLastUpdatedDate],
        Null                    AS [ClientFee!2!LowerLimit],		--Audit Fee Range Changes
        Null                    AS [ClientFee!2!UpperLimit],		--Audit Fee Range Changes
        Null                    AS [ClientFee!2!FeeRangeFlag],		--Audit Fee Range Changes
        -- Included Services
        Null                    AS [IncludedServices!3!ClientFeeID],
        Null                    AS [IncludedServices!3!ServiceID],
        --Null                    AS [IncludedServices!3!PrimaryFlag],
        Null                    AS [IncludedServices!3!RequiredFlag],
        Null                    AS [IncludedServices!3!ItemizeFlag],
        Null                    AS [IncludedServices!3!LowerLimit],		--Audit Fee Range Changes
        Null                    AS [IncludedServices!3!UpperLimit],		--Audit Fee Range Changes
        Null                    AS [IncludedServices!3!FeeRangeFlag],	--Audit Fee Range Changes
        Null                    AS [IncludedServices!3!EffectiveStartDate],	--Audit Fee Range Changes
        Null                    AS [IncludedServices!3!EffectiveEndDate],	--Audit Fee Range Changes
        Null                    AS [IncludedServices!3!EnabledFlag],	--Audit Fee Range Changes
        -- Claim Aspect Types
        Null                    AS [ClaimAspectTypes!4!ClaimAspectTypeID],
        Null                    AS [ClaimAspectTypes!4!ClaimAspectTypeName],
        -- Service Data
        Null                    AS [Service!5!ServiceID],
        Null                    AS [Service!5!Name],
        Null                    AS [Service!5!ServiceChannelCD],
        Null                    AS [Service!5!BillingModelCD],
        Null                    AS [Service!5!DisplayOrder],
        -- Service Channel Reference Data
        Null                    AS [ServiceChannel!6!Code],
        Null                    AS [ServiceChannel!6!Name],
        -- Applies when Reference Data
        Null                    AS [AppliesWhen!7!Code],
        Null                    AS [AppliesWhen!7!Name],
        -- Admin Fee Applies To Reference Data
        Null                    AS [AdminFeeCD!8!Code],
        Null                    AS [AdminFeeCD!8!Name],
        -- States Reference Data
        Null                    AS [States!9!Code],
        Null                    AS [States!9!Name],
        -- MSA Reference Data
        Null                    AS [MSA!10!Code],
        Null                    AS [MSA!10!Name],
        -- Admin Fees
        Null                    AS [AdminFee!11!ClientFeeID],
        Null                    AS [AdminFee!11!AdminFeeID],
        Null                    AS [AdminFee!11!AdminFeeCD],
        Null                    AS [AdminFee!11!Amount],
        Null                    AS [AdminFee!11!MSA],
        Null                    AS [AdminFee!11!State]
        
    UNION ALL

    SELECT
        2,
        1,
        Null, Null,
        -- Client Fee
        CF.ClientFeeID,
        CF.EnabledFlag,
        isNull(CF.DisplayOrder, 0),
        isNull(CF.CategoryCD, ''),
        CF.ClaimAspectTypeID,
        isNull(CF.Description, ''),
        CF.FeeAmount,
        isNull(CF.FeeInstructions, ''),
        CF.ItemizeFlag,
        isNull(CF.InvoiceDescription, ''),
        convert(varchar, CF.EffectiveStartDate, 101),
        convert(varchar, CF.EffectiveEndDate, 101),
        CF.AppliesToCD,
        isNull(CF.HideFromInvoiceFlag, 0),
        CF.SysLastUpdatedDate,
        isNull(convert(varchar(10),CF.LowerLimit), ''),		--Audit Fee Range Changes
        isNull(convert(varchar(10),CF.UpperLimit), ''),		--Audit Fee Range Changes
        isNull(convert(varchar(10),CF.ApplyFeeRange), ''),  --Audit Fee Range Changes
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        -- Claim Aspect Types
        Null, Null,
        --Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        Null, Null, Null, Null, Null, Null


    FROM
        dbo.utb_Client_Fee CF
    WHERE InsuranceCompanyID = @InsuranceCompanyID
      AND EnabledFlag = 1

    UNION ALL

    --  Included Services

    SELECT DISTINCT
        3 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        CFD.ClientFeeId,
        CFD.ServiceId,
        --CFD.PrimaryFlag,
        CFD.RequiredFlag,
        CFD.ItemizeFlag,
        isNull(convert(varchar(10),CF.LowerLimit), ''),
        isNull(convert(varchar(10),CF.UpperLimit), ''),
        isNull(convert(varchar(10),CF.ApplyFeeRange), ''),
        isNull(convert(varchar, CF.EffectiveStartDate, 101), ''),
        isNull(convert(varchar, CF.EffectiveEndDate, 101), ''),
        CF.EnabledFlag,
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        Null, Null, Null, Null, Null, Null

    FROM
        utb_client_fee cf, utb_client_fee_definition CFD, utb_service s
    WHERE CFD.ServiceID = s.ServiceID
      and CFD.ClientFeeID = CF.ClientFeeID
      and CF.InsuranceCompanyID = @InsuranceCompanyID
      and CF.EnabledFlag = 1

    UNION ALL

    --  Claim Aspect Types supported by the client

    SELECT DISTINCT
        4 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, NULL,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        -- Claim Aspect Types
        CCAT.ClaimAspectTypeID,
        CAT.Name,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        Null, Null, Null, Null, Null, Null

    FROM
        dbo.utb_claim_aspect_type cat, dbo.utb_client_claim_aspect_type ccat
    WHERE cat.ClaimAspectTypeID = ccat.ClaimAspectTypeID
      and ccat.InsuranceCompanyID = @InsuranceCompanyID
      and cat.Name in ('Claim', 'Vehicle', 'Property')


    UNION ALL

    --  MetaData Level

    SELECT DISTINCT
        5 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        R.ServiceID,
        R.Name,
        isNull(R.ServiceChannelCD, ''),
        isNull(R.BillingModelCD, ''),
        R.DisplayOrder,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        Null, Null, Null, Null, Null, Null

    FROM
        @tmpReference R

    UNION ALL

    --  MetaData Level

    SELECT DISTINCT
        6 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Code, 
        Name,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        Null, Null, Null, Null, Null, Null

    FROM
        dbo.ufnUtilityGetReferenceCodes('utb_service', 'ServiceChannelCD')

    UNION ALL

    SELECT DISTINCT
        7 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Code, Name,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        Null, Null, Null, Null, Null, Null

    FROM
        dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'AppliesToCD')

    UNION ALL

    SELECT DISTINCT
        8 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Code, Name,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        Null, Null, Null, Null, Null, Null

    FROM dbo.ufnUtilityGetReferenceCodes('utb_admin_fee', 'AdminFeeCD')

    UNION ALL

    SELECT DISTINCT
        9 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        StateCode, 
        StateValue,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        Null, Null, Null, Null, Null, Null

    FROM utb_state_code
    where enabledFlag = 1

    UNION ALL

    SELECT DISTINCT
        10 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        CountyType, CountyType,
        -- Admin Fees
        Null, Null, Null, Null, Null, Null

    FROM dbo.utb_zip_code

    UNION ALL

    SELECT DISTINCT
        11 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        af.ClientFeeID, 
        af.AdminFeeID, 
        af.AdminFeeCD,
        af.Amount,
        'Flat',
        'Flat'

    FROM dbo.utb_admin_fee af
    LEFT JOIN utb_client_fee cf ON af.ClientFeeID = cf.ClientFeeID
    WHERE cf.InsuranceCompanyID = @InsuranceCompanyID
      AND af.EnabledFlag = 1
      AND cf.EnabledFlag = 1

    UNION ALL

    SELECT DISTINCT
        11 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null,  Null,
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        af.ClientFeeID, 
        afm.AdminFeeID, 
        afm.AdminFeeCD,
        afm.Amount,
        afm.MSA,
        ''

    FROM dbo.utb_admin_fee_msa afm
    LEFT JOIN utb_admin_fee af on afm.AdminFeeID = af.AdminFeeID
    LEFT JOIN utb_client_fee cf ON af.ClientFeeID = cf.ClientFeeID
    WHERE cf.InsuranceCompanyID = @InsuranceCompanyID
      AND afm.EnabledFlag = 1
      AND cf.EnabledFlag = 1

    UNION ALL

    SELECT DISTINCT
        11 as tag,
        1 as parent,
        Null, Null,
        -- Client Fee
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null,
        Null, Null, Null,
        -- Included Services
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        -- Claim Aspect Types
        Null, Null,
        -- Service Data
        Null, Null, Null, Null, Null,
        --Service Channel Reference Data
        Null, Null,
        --Applies when Reference Data
        Null, Null,
        -- Admin Fee Applies To Reference Data
        Null, Null,
        -- States Reference Data
        Null, Null,
        -- MSA Reference Data
        Null, Null,
        -- Admin Fees
        af.ClientFeeID, 
        afs.AdminFeeID, 
        afs.AdminFeeCD,
        afs.Amount,
        '',
        afs.StateCode

    FROM dbo.utb_admin_fee_state afs
    LEFT JOIN utb_admin_fee af on afs.AdminFeeID = af.AdminFeeID
    LEFT JOIN utb_client_fee cf ON af.ClientFeeID = cf.ClientFeeID
    WHERE cf.InsuranceCompanyID = @InsuranceCompanyID
      AND afs.EnabledFlag = 1
      AND cf.EnabledFlag = 1

   -- FOR XML EXPLICIT  --(Commented for client-side processing)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsFeesGetListXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmInsFeesGetListXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO