

/************************************************************************************************************************
* PROCEDURE:    uspEstimateQuickUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       GLSD452
* FUNCTION:     If the Procedure is Already exists, drop the procedure.
************************************************************************************************************************/

BEGIN TRANSACTION
IF EXISTS (SELECT NAME FROM SYSOBJECTS WHERE NAME = 'uspEstimateQuickUpdDetail' AND type = 'P')
	BEGIN
		DROP PROCEDURE dbo.uspEstimateQuickUpdDetail

		-- Commit the transaction for dropping the stored procedure
		COMMIT
	END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateQuickUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates the estimate data in utb_document and utb_estimate_summary
*
* PARAMETERS:
* (I) @DocumentID           The DocumentID to update
* (I) @ClaimAspectID        The claim aspect id
* (I) @AdjustmentTotalAmt   The total adjustment amount
* (I) @DeductibleAmt        The amount of the deductible
* (I) @DocumentTypeID       The document type id
* (I) @DuplicateFlag        Indicates whether the estimate is a duplicate
* (I) @EstimateTypeCD       Indicates whether the estimate is (O)riginal or (A)udited
* (I) @NetTotalAmt          The Net total of the estimate (counting adjustments)
* (I) @OtherAdjustmentAmt   The amount of other adjustments
* (I) @RepairTotalAmt       The repair total of the estimate without applied adjustments
* (l) @BettermentTotalAmt   The betterment amount
* (I) @SupplementSeqNumber  The supplement sequence number
* (I) @AgreedPriceMetCD     Was the price agreed to by the shop
* (I) @UserID               UserID performing the update
*
* New Parameters are Added By Glsd451
* (I) @ShopContacted		Indicates Whether the shopcontacted is Yes or No
* (I) @ShopContactName			Name of the ShopContacted
* (I) @PrimaryReasonCode	Values of PrimaryReasonCode Checkbox
* (I) @PrimaryReasonCodeText Values for PrimaryReasonCode Textbox
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspEstimateQuickUpdDetail]
    @DocumentID             udt_std_id_big,
    @ClaimAspectID          udt_std_id_big,
    @AdjustmentTotalAmt     decimal(9,2),
    @DeductibleAmt          decimal(9,2),
    @DocumentTypeID         udt_std_id,
    @DuplicateFlag          udt_std_flag,
    @EstimateTypeCD         udt_std_cd,
    @NetTotalAmt            decimal(9,2),
    @OtherAdjustmentAmt     decimal(9,2),
    @RepairTotalAmt         decimal(9,2),
    @BettermentTotalAmt     decimal(9,2),
    @SupplementSeqNumber    udt_std_int_tiny,
    @AgreedPriceMetCD       udt_std_cd,
    @TaxTotalAmt            decimal(9, 2),
    @ApprovedFlag           udt_std_flag,
    @WarrantyFlag           udt_std_flag,
    @DocumentEditFlag       udt_std_flag,
    @UserID                 udt_std_id,

-- New Parameters added by Glsd451
	@ShopContacted			udt_std_flag,
	@ShopContactName		udt_std_desc_mid,
	@PrimaryReasonCode		udt_std_note,
	@PrimaryReasonCodeText  udt_std_note,
	@SecondaryReasonCode	udt_std_note,
	@SecondaryReasonCodeText udt_std_note
AS	
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@EstimateTypeCD))) = 0 SET @EstimateTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@AgreedPriceMetCD))) = 0 SET @AgreedPriceMetCD = NULL


    -- Declare internal variables

    DECLARE @DocumentVANSourceFlag                  udt_std_flag
    DECLARE @DateLastUpdatedAdjustmentTotal         varchar(30)
    DECLARE @DateLastUpdatedContractPrice           varchar(30)
    DECLARE @DateLastUpdatedDeductible              varchar(30)
    DECLARE @DateLastUpdatedNetTotal                varchar(30)
    DECLARE @DateLastUpdatedOtherAdjustment         varchar(30)
    DECLARE @DateLastUpdatedRepairTotal             varchar(30)
    DECLARE @DateLastUpdatedBettermentTotal         varchar(30)
    DECLARE @DateLastUpdatedDeductiblesAppliedTotal varchar(30)
    DECLARE @DateLastUpdatedLimitEffectTotal        varchar(30)
    DECLARE @DateLastUpdatedNetTotalEffectTotal     varchar(30)
    DECLARE @DateLastUpdatedTaxTotal                varchar(30)
    
    DECLARE @EstimateSummaryIDAdjustmentTotal       udt_std_id
    DECLARE @EstimateSummaryIDContractPrice         udt_std_id
    DECLARE @EstimateSummaryIDDeductible            udt_std_id
    DECLARE @EstimateSummaryIDNetTotal              udt_std_id
    DECLARE @EstimateSummaryIDOtherAdjustment       udt_std_id
    DECLARE @EstimateSummaryIDRepairTotal           udt_std_id
    DECLARE @EstimateSummaryIDBetterment            udt_std_id
    DECLARE @EstimateSummaryIDDeductiblesApplied    udt_std_id
    DECLARE @EstimateSummaryIDLimitEffect           udt_std_id
    DECLARE @EstimateSummaryIDNetTotalEffect        udt_std_id
    DECLARE @EstimateSummaryIDTaxTotal              udt_std_id
    
    DECLARE @LimitEffectAmt                         udt_std_money
    DECLARE @NetTotalEffectAmt                      udt_std_money
    
    DECLARE @ClaimAspectServiceChannelID            bigint
    DECLARE @ClaimCoverageID                        bigint
    DECLARE @ExposureCD                             varchar(4)
    DECLARE @EventID                                int
    DECLARE @EventName                              varchar(50)
    DECLARE @PHNotificationDocumentTypeID           int

    DECLARE @error AS int
    DECLARE @rowcount AS int

    DECLARE @now               AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspEstimateQuickUpdDetail'


    -- Set Database options

    SET NOCOUNT ON


    -- Check to make sure a valid Document id was passed in

    IF  (@DocumentID IS NULL) OR
        (NOT EXISTS(SELECT DocumentID FROM dbo.utb_document WHERE DocumentID = @DocumentID))
    BEGIN
        -- Invalid Document ID

        RAISERROR('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
     RETURN
    END


    -- Check to make sure a valid Claim Aspect id was passed in

    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid ClaimAspect ID

        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure the document id belongs to the claim aspect id
    --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    IF  (NOT EXISTS(
                    SELECT ClaimAspectID 
                    FROM dbo.utb_claim_aspect_service_channel_document  cascd
                    INNER JOIN    utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                    WHERE DocumentID = @DocumentID 
                    AND ClaimAspectID = @ClaimAspectID
                   )
        )
    --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    BEGIN
        -- Invalid ClaimAspect ID for the Document id

        RAISERROR('%s|Claim Aspect ID invalid for Document', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure EstimateTypeCD is valid

    IF (@EstimateTypeCD IS NOT NULL) AND
       (@EstimateTypeCD NOT IN (SELECT code FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'EstimateTypeCD')))
    BEGIN
        -- Invalid Estimate Type CD

        RAISERROR('101|%s|@EstimateTypeCD|%s', 16, 1, @ProcName, @EstimateTypeCD)
        RETURN
    END


    -- Check to make sure AgreedPriceMetCD is valid

    IF (@AgreedPriceMetCD IS NOT NULL) AND
       (@AgreedPriceMetCD NOT IN (SELECT code FROM dbo.ufnUtilityGetReferenceCodes('utb_document', 'AgreedPriceMetCD')))
    BEGIN
        -- Invalid Agreed Price Met CD

        RAISERROR('101|%s|@AgreedPriceMetCD|%s', 16, 1, @ProcName, @AgreedPriceMetCD)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Validate APD Date State

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'AdjustmentTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"AdjustmentTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'CP' AND Name = 'ContractPrice')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"ContractPrice"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'AJ' AND Name = 'Deductible')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"Deductible"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'NetTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"NetTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'AJ' AND Name = 'Other')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"Other (Other Adjustment)"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'RepairTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"RepairTotal"|utb_estimate_summary_type', 16, 1, @ProcName)
        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'AJ' AND Name = 'Betterment')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"Betterment"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'OT' AND Name = 'DeductiblesApplied')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"DeductiblesApplied"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'OT' AND Name = 'LimitEffect')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"LimitEffect"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'OT' AND Name = 'NetTotalEffect')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"NetTotalEffect"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF NOT EXISTS(SELECT EstimateSummaryTypeID FROM  dbo.utb_estimate_summary_type WHERE CategoryCD = 'TT' AND Name = 'TaxTotal')
    BEGIN
       -- Estimate Summary Type Not Found

        RAISERROR('102|%s|"TaxTotal"|utb_estimate_summary_type', 16, 1, @ProcName)        RETURN
    END
    
    IF @ApprovedFlag = 1 AND @AgreedPriceMetCD = 'N'
    BEGIN
      -- Cannot set Approved Flag to Yes for an estimate that has Agreed Price Met as No
      SET @ApprovedFlag = 0
    END
    
    
   
    

    -- Check to see if we're updating a VAN sourced document.  If so, we'll ignore the amounts
    -- (the user shouldn't have edited those anyway)

    SELECT @DocumentVANSourceFlag = ds.VANFlag
      FROM dbo.utb_document d
      LEFT JOIN dbo.utb_document_source ds ON (d.DocumentSourceID = ds.DocumentSourceID)
      WHERE DocumentID = @DocumentID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    FROM utb_document d
    LEFT JOIN utb_claim_aspect_service_channel_document cascd ON d.DocumentID = cascd.DocumentID
    WHERE d.DocumentID = @DocumentID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Get the claim coverage based on the vehicle coverage.
    SELECT @ClaimCoverageID = cc.ClaimCoverageID,
           @ExposureCD = ExposureCD
    FROM dbo.utb_claim_aspect ca 
    LEFT JOIN dbo.utb_claim_coverage cc ON ca.LynxID = cc.LynxID and ca.CoverageProfileCD = cc.CoverageTypeCD
    WHERE ca.ClaimAspectID = @ClaimAspectID
      AND cc.EnabledFlag = 1
      AND cc.AddtlCoverageFlag = 0

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
   
    SELECT @ExposureCD = ExposureCD
    FROM dbo.utb_claim_aspect ca 
    WHERE ca.ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @DocumentVANSourceFlag = 0
    BEGIN
        -- Initialize all the estimate summary ids to 0

        SET @EstimateSummaryIDAdjustmentTotal = 0
        SET @EstimateSummaryIDContractPrice = 0
        SET @EstimateSummaryIDDeductible = 0
        SET @EstimateSummaryIDNetTotal = 0
        SET @EstimateSummaryIDOtherAdjustment = 0
        SET @EstimateSummaryIDRepairTotal = 0
        SET @EstimateSummaryIDBetterment = 0
        SET @EstimateSummaryIDTaxTotal = 0
        
        SET @EstimateSummaryIDDeductiblesApplied = 0
        SET @EstimateSummaryIDLimitEffect = 0
        SET @EstimateSummaryIDNetTotalEffect = 0
        -- Get the ids for any existing estimate summary records

        SELECT  @EstimateSummaryIDAdjustmentTotal = EstimateSummaryID,
                @DateLastUpdatedAdjustmentTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'AdjustmentTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDContractPrice = EstimateSummaryID,
                @DateLastUpdatedContractPrice = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'CP'
            AND est.Name = 'ContractPrice'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDDeductible = EstimateSummaryID,
                @DateLastUpdatedDeductible = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'AJ'
            AND est.Name = 'Deductible'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDNetTotal = EstimateSummaryID,
                @DateLastUpdatedNetTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'NetTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDOtherAdjustment = EstimateSummaryID,
                @DateLastUpdatedOtherAdjustment = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'AJ'
            AND est.Name = 'Other'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        SELECT  @EstimateSummaryIDRepairTotal = EstimateSummaryID,
                @DateLastUpdatedRepairTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'RepairTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        SELECT  @EstimateSummaryIDTaxTotal = EstimateSummaryID,
                @DateLastUpdatedTaxTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'TT'
            AND est.Name = 'TaxTotal'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        
        SELECT  @EstimateSummaryIDBetterment = EstimateSummaryID,
                @DateLastUpdatedBettermentTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'AJ'
            AND est.Name = 'Betterment'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SELECT  @EstimateSummaryIDDeductiblesApplied = EstimateSummaryID,
                @DateLastUpdatedDeductiblesAppliedTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'OT'
            AND est.Name = 'DeductiblesApplied'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SELECT  @EstimateSummaryIDLimitEffect = EstimateSummaryID,
                @LimitEffectAmt = OriginalExtendedAmt,
                @DateLastUpdatedLimitEffectTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'OT'
            AND est.Name = 'LimitEffect'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SELECT  @EstimateSummaryIDNetTotalEffect = EstimateSummaryID,
                @DateLastUpdatedNetTotalEffectTotal = dbo.ufnUtilityGetDateString(es.SysLastUpdatedDate)
          FROM  dbo.utb_estimate_summary es
          LEFT JOIN dbo.utb_estimate_summary_type est ON (es.EstimateSummaryTypeID = est.EstimateSummaryTypeID)
          WHERE es.DocumentID = @DocumentID
            AND est.CategoryCD = 'OT'
            AND est.Name = 'NetTotalEffect'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        SET @NetTotalEffectAmt = isNull(@RepairTotalAmt, 0) - isNull(@DeductibleAmt, 0) - isNull(@LimitEffectAmt, 0)
        
    END


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Begin Update

    BEGIN TRANSACTION DocumentEstimateUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

        -- Perform an update

        UPDATE  dbo.utb_document
           SET  DocumentTypeID          = @DocumentTypeID,
                DuplicateFlag           = @DuplicateFlag,
                EstimateTypeCD          = @EstimateTypeCD,
                EstimateReviewDate      = @now,
                AgreedPriceMetCD        = @AgreedPriceMetCD,
                SupplementSeqNumber     = @SupplementSeqNumber,
                WarrantyFlag            = @WarrantyFlag,
                SysLastUserID           = @UserID,
                SysLastUpdatedDate      = @now,
				ShopContacted			= @ShopContacted,
				ShopContactName			= @ShopContactName,			
				PrimaryReasonCode		= @PrimaryReasonCode,
				PrimaryReasonCodeText   = @PrimaryReasonCodeText,
				SecondaryReasonCode		= @SecondaryReasonCode,
				SecondaryReasonCodeText = @SecondaryReasonCodeText 
        WHERE DocumentID = @DocumentID

        SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

        -- Check error value

        IF @error <> 0
        BEGIN
            -- Update failed

            RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END



    IF @DocumentVANSourceFlag = 0
    BEGIN
        -- Let's update the estimate summary

        -- First, delete all existing estimate summary records that we're not going to change

        DELETE FROM dbo.utb_estimate_summary
          WHERE DocumentID = @DocumentID
            AND EstimateSummaryID NOT IN (@EstimateSummaryIDAdjustmentTotal,
                                          @EstimateSummaryIDContractPrice,
                                          @EstimateSummaryIDDeductible,
                                          @EstimateSummaryIDNetTotal,
                                          @EstimateSummaryIDOtherAdjustment,
                                          @EstimateSummaryIDRepairTotal,
                                          @EstimateSummaryIDBetterment,
                                          @EstimateSummaryIDDeductiblesApplied,
                                          @EstimateSummaryIDLimitEffect,
                                          @EstimateSummaryIDNetTotalEffect,
                                          @EstimateSummaryIDTaxTotal)

        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('106|%s|utb_estimate_summary', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDContractPrice,
                                              @EstimateSummaryType      = 'ContractPrice',
                                              @EstimateSummaryTypeCD    = 'CP',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @RepairTotalAmt,
                                              @AgreedUnitAmt            = @RepairTotalAmt,
                                              @OriginalExtendedAmt      = @RepairTotalAmt,
                                              @OriginalUnitAmt          = @RepairTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedContractPrice,
                                              @ReturnSysLastUpdate      = 0

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Contract Price)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDRepairTotal,
                                              @EstimateSummaryType      = 'RepairTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @RepairTotalAmt,
                                              @AgreedUnitAmt            = @RepairTotalAmt,
                                              @OriginalExtendedAmt      = @RepairTotalAmt,
                                              @OriginalUnitAmt          = @RepairTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedRepairTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Repair Total)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDDeductible,
                                              @EstimateSummaryType      = 'Deductible',
                                              @EstimateSummaryTypeCD    = 'AJ',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @DeductibleAmt,
                                              @AgreedUnitAmt            = @DeductibleAmt,
                                              @OriginalExtendedAmt      = @DeductibleAmt,
                                              @OriginalUnitAmt          = @DeductibleAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedDeductible,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Deductible)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDOtherAdjustment,
                                              @EstimateSummaryType      = 'Other',
                                              @EstimateSummaryTypeCD    = 'AJ',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @OtherAdjustmentAmt,
                                              @AgreedUnitAmt            = @OtherAdjustmentAmt,
                                              @OriginalExtendedAmt      = @OtherAdjustmentAmt,
                                              @OriginalUnitAmt          = @OtherAdjustmentAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedOtherAdjustment,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDAdjustmentTotal,
                                              @EstimateSummaryType      = 'AdjustmentTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @AdjustmentTotalAmt,
                                              @AgreedUnitAmt            = @AdjustmentTotalAmt,
                                              @OriginalExtendedAmt      = @AdjustmentTotalAmt,
                                              @OriginalUnitAmt          = @AdjustmentTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedAdjustmentTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDTaxTotal,
                                              @EstimateSummaryType      = 'TaxTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @TaxTotalAmt,
                                              @AgreedUnitAmt            = @TaxTotalAmt,
                                              @OriginalExtendedAmt      = @TaxTotalAmt,
                                              @OriginalUnitAmt          = @TaxTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedTaxTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDNetTotal,
                                              @EstimateSummaryType      = 'NetTotal',
                                              @EstimateSummaryTypeCD    = 'TT',
                                              @DocumentID               = @DocumentID,
                                              @AgreedExtendedAmt        = @NetTotalAmt,
                                              @AgreedUnitAmt            = @NetTotalAmt,
                                              @OriginalExtendedAmt      = @NetTotalAmt,
                                              @OriginalUnitAmt          = @NetTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedNetTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDBetterment,
                                              @EstimateSummaryType      = 'Betterment',
                                              @EstimateSummaryTypeCD    = 'AJ',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @BettermentTotalAmt,
                                              @AgreedUnitAmt            = @BettermentTotalAmt,
                                              @OriginalExtendedAmt      = @BettermentTotalAmt,
                                              @OriginalUnitAmt          = @BettermentTotalAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedBettermentTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (Other Adjustment Amount)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDDeductiblesApplied,
                                              @EstimateSummaryType      = 'DeductiblesApplied',
                                              @EstimateSummaryTypeCD    = 'OT',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @DeductibleAmt,
                                              @AgreedUnitAmt            = @DeductibleAmt,
                                              @OriginalExtendedAmt      = @DeductibleAmt,
                                              @OriginalUnitAmt          = @DeductibleAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedDeductiblesAppliedTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (DeductiblesApplied)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDLimitEffect,
                                              @EstimateSummaryType      = 'LimitEffect',
                                              @EstimateSummaryTypeCD    = 'OT',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @LimitEffectAmt,
                                              @AgreedUnitAmt            = @LimitEffectAmt,
                                              @OriginalExtendedAmt      = @LimitEffectAmt,
                                              @OriginalUnitAmt          = @LimitEffectAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedLimitEffectTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (LimitEffect)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        EXEC dbo.uspEstimateSummaryUpdDetail  @EstimateSummaryID        = @EstimateSummaryIDNetTotalEffect,
                                              @EstimateSummaryType      = 'NetTotalEffect',
                                              @EstimateSummaryTypeCD    = 'OT',
                                              @DocumentID               = @DocumentID,      
                                              @AgreedExtendedAmt        = @NetTotalEffectAmt,
                                              @AgreedUnitAmt            = @NetTotalEffectAmt,
                                              @OriginalExtendedAmt      = @NetTotalEffectAmt,
                                              @OriginalUnitAmt          = @NetTotalEffectAmt,
                                              @UserID                   = @UserID,
                                              @SysLastUpdatedDate       = @DateLastUpdatedNetTotalEffectTotal,
                                              @ReturnSysLastUpdate      = 0                                              

        IF @@ERROR <> 0
        BEGIN
            -- Call failed

            RAISERROR('%s: Call to uspEstimateSummaryUpdDetail failed (NetTotalEffect)', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        
        IF @ClaimCoverageID IS NOT NULL AND @ClaimAspectServiceChannelID IS NOT NULL
        BEGIN
           -- Now check if the deductible applied record exist for the claim aspect service channel.
           IF EXISTS (SELECT ClaimAspectServiceChannelID
                      FROM dbo.utb_claim_aspect_service_channel_coverage
                      WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                        AND ClaimCoverageID = @ClaimCoverageID)
           BEGIN
               -- Deductible applied record exist. Update the deductible applied
               UPDATE dbo.utb_claim_aspect_service_channel_coverage
               SET DeductibleAppliedAmt = @DeductibleAmt
               WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                 AND ClaimCoverageID = @ClaimCoverageID
   
               IF @error <> 0
               BEGIN
                   -- Update failed
       
                   RAISERROR('104|%s|utb_claim_aspect_service_channel_coverage', 16, 1, @ProcName)
                   ROLLBACK TRANSACTION
                   RETURN
               END
           END
           ELSE
           BEGIN
               -- Deductible applied record exist. Update the deductible applied
               INSERT INTO dbo.utb_claim_aspect_service_channel_coverage
                  (ClaimAspectServiceChannelID, 
                   ClaimCoverageID, 
                   DeductibleAppliedAmt, 
                   SysLastUserID, 
                   SysLastUpdatedDate)
               VALUES
                  (@ClaimAspectServiceChannelID, 
                   @ClaimCoverageID, 
                   @DeductibleAmt, 
                   @UserID, 
                   @now)
   
               IF @error <> 0
               BEGIN
                   -- Insert failed
       
                   RAISERROR('105|%s|utb_claim_aspect_service_channel_coverage', 16, 1, @ProcName)
                   ROLLBACK TRANSACTION
                   RETURN
               END
           END
        END

        IF @ExposureCD = '1' AND @EstimateTypeCD = 'A' AND @AgreedPriceMetCD = 'Y'
        BEGIN
            -- Adding an price agreed audited estimate for a 1st party vehicle.
            
            -- Check if the PH Notifcation document exists
            SELECT @PHNotificationDocumentTypeID = DocumentTypeID
            FROM utb_document_type
            WHERE Name = 'PH Notification'

            IF NOT EXISTS(SELECT d.DocumentID
                           FROM utb_document d
                           LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
                           LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                           WHERE d.DocumentTypeID = @PHNotificationDocumentTypeID 
                             AND casc.ClaimAspectID = @ClaimAspectID
                             AND d.EnabledFlag = 1)
            BEGIN
               -- Raise the PH Notofication event
               SET @EventName = 'First Party Audited Estimate'

               SELECT  @EventID = EventID 
               FROM  utb_event 
               WHERE Name = @EventName

               IF @@ERROR <> 0
               BEGIN
                  -- SQL Server Error

                  RAISERROR('99|%s', 16, 1, @ProcName)
                  RETURN
               END              

               IF @EventID IS NULL
               BEGIN
                  -- Event Name not found

                  RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
                  ROLLBACK TRANSACTION
                  RETURN
               END


               -- Make the workflow call

               EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                           @ClaimAspectID = @ClaimAspectID,
                                           @Description = 'Audited Estimate with agreed price added to a First Party vehicle',
                                           @Key = NULL,
                                           @UserID = @UserID,
                                           @ConditionValue = 'DA',
                                           @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

            END
        END
    END


    COMMIT TRANSACTION DocumentEstimateUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    SELECT  1                   AS tag,
            NULL                AS parent,
            @DocumentID         AS [Root!1!DocumentID],
            @Now                AS [Root!1!SysLastUpdatedDate]

--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

/************************************************************************************************************************
* PROCEDURE:    uspCFDASummaryReportGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       GLSD452
* FUNCTION:     If the Procedure is Already exists, drop the procedure.
************************************************************************************************************************/

BEGIN TRANSACTION
IF EXISTS (SELECT NAME FROM SYSOBJECTS WHERE NAME = 'uspCFDASummaryReportGetDetailXML' AND type = 'P')
	BEGIN
		DROP PROCEDURE dbo.uspCFDASummaryReportGetDetailXML

		-- Commit the transaction for dropping the stored procedure
		COMMIT
	END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspCFDASummaryReportGetDetailXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Ramesh Vishegu]
* FUNCTION:     [Stored procedure to retrieve Custom Forms as XML]
*
* PARAMETERS:  
* No Parameters
*
* RESULT SET:
* Users List as XML
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

-- exec uspCFDASummaryReportGetDetailXML 1600116,639816,'DA',5871,1,'Supplement'

CREATE PROCEDURE [dbo].[uspCFDASummaryReportGetDetailXML]
    @LynxID             udt_std_id_big,
    @ClaimAspectID      udt_std_id_big,
    @ServiceChannelCD   udt_std_cd,
    @UserID             udt_std_id_big,
    @SequenceNumber		udt_std_id_big		= NULL,
    @DataSet			udt_std_name,
    @FormID				udt_std_id_big   
   
AS
BEGIN
    -- Declare internal variables
    DECLARE @InsuranceCompanyName as varchar(100)
    DECLARE @ClaimNumber as varchar(50)
    DECLARE @CarrierRepName as varchar(100)
    DECLARE @OriginalEstimateDocumentID as bigint
    DECLARE @AuditedEstimateDocumentID as bigint
    DECLARE @OriginalEstimateAmount as decimal(9, 2)
    DECLARE @AuditedEstimateAmount as decimal(9, 2)
    DECLARE @AuditedDifference as decimal(9, 2)
    DECLARE @Deductible as decimal(9, 2)
    DECLARE @Adjustments as decimal(9, 2)
    DECLARE @Betterment as decimal(9, 2)
    DECLARE @NetAuditedEstimate as decimal(9, 2)
    DECLARE @AuditorName as varchar(100)
    DECLARE @AuditorTitle as varchar(100)
    DECLARE @AuditorPhone as varchar(15)
    DECLARE @AuditorEmail as varchar(75)
    DECLARE @ClaimAspectNumber as tinyint
    DECLARE @LastSupplementSeqNo as int
    DECLARE @Message as varchar(1000)
    DECLARE @VANFlag as bit
    DECLARE @LossState as varchar(2)
    DECLARE @LossStateName as varchar(100)
    DECLARE @ShopContacted as Varchar(1000)
	DECLARE @ShopContactName as Varchar(250)
	DECLARE @PrimaryReasonCode as Varchar(1000)
	DECLARE @PrimaryReasonCodeText as Varchar(1000)
	DECLARE @SecondaryReasonCode as Varchar(1000)
	DECLARE @SecondaryReasonCodeText as Varchar(1000)
    DECLARE @EstimateSummaryRepairTotalID AS int
    DECLARE @EstimateSummaryAdjustmentsTotalID AS int
    DECLARE @EstimateSummaryBettermentID AS int
    DECLARE @EstimateSummaryDeductibleID AS int
    DECLARE @FormDataID as int
    
    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCFDASummaryReportGetDetailXML'     

    -- Check to make sure a valid Lynx id was passed in
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    -- Check to make sure a valid user id was passed in
    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    -- Get the estimate summary ids
    SELECT @EstimateSummaryRepairTotalID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'TT'
      AND Name = 'RepairTotal'

    SELECT @EstimateSummaryAdjustmentsTotalID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'TT'
      AND Name = 'AdjustmentTotal'

    SELECT @EstimateSummaryBettermentID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'AJ'
      AND Name = 'Betterment'

    SELECT @EstimateSummaryDeductibleID = EstimateSummaryTypeID
    FROM utb_estimate_summary_type
    WHERE CategoryCD = 'AJ'
      AND Name = 'Deductible'

    -- Get the latest supplement sequence number
    SELECT top 1 @LastSupplementSeqNo = d.SupplementSeqNumber
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and dt.EstimateTypeFlag = 1
      --and d.EstimateTypeCD = 'O'
      and d.EnabledFlag = 1
    order by d.SupplementSeqNumber desc, ds.VANFlag desc
    
    -- Get the original estimate document
    SELECT top 1 @OriginalEstimateDocumentID = d.DocumentID
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @LastSupplementSeqNo
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'O'
      and d.EnabledFlag = 1
    order by d.SupplementSeqNumber desc, ds.VANFlag desc

    -- Get the Audited Estimate
    SELECT top 1 @AuditedEstimateDocumentID = d.DocumentID,
			     @VANFlag = ds.VANFlag
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @LastSupplementSeqNo
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'A'
      and d.EnabledFlag = 1	 
    order by d.SupplementSeqNumber desc, ds.VANFlag desc

 -- Get the Reason code inforamtiom for Estimate/Supplement Audit
    SELECT top 1 @ShopContacted = d.ShopContacted,
				 @ShopContactName = d.ShopContactName,
				 @PrimaryReasonCode = d.PrimaryReasonCode,
				 @PrimaryReasonCodeText = d.PrimaryReasonCodeText,
				 @SecondaryReasonCode = d.SecondaryReasonCode,
				 @SecondaryReasonCodeText = d.SecondaryReasonCodeText
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
    LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    where ca.ClaimAspectID = @ClaimAspectID
      and casc.ServiceChannelCD = @ServiceChannelCD 
      and d.SupplementSeqNumber = @SequenceNumber
      and dt.EstimateTypeFlag = 1
      and d.EstimateTypeCD = 'A'
      and d.EnabledFlag = 1
	 and  dt.Name = @DataSet
    order by d.SupplementSeqNumber desc, ds.VANFlag desc

    SET @Message = ''

    IF @OriginalEstimateDocumentID IS NOT NULL AND @AuditedEstimateDocumentID IS NOT NULL
    BEGIN
        SELECT @OriginalEstimateAmount = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @OriginalEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryRepairTotalID


        SELECT @AuditedEstimateAmount = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryRepairTotalID

        SELECT @Adjustments = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryAdjustmentsTotalID

        SELECT @Betterment = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryBettermentID

        SELECT @Deductible = AgreedExtendedAmt
        FROM utb_estimate_summary
        WHERE DocumentID = @AuditedEstimateDocumentID
          AND EstimateSummaryTypeID = @EstimateSummaryDeductibleID

        SET @AuditedDifference = @OriginalEstimateAmount - @AuditedEstimateAmount

        SET @NetAuditedEstimate = @AuditedEstimateAmount - @Adjustments

        IF @VANFlag = 0
        BEGIN
            SET @NetAuditedEstimate = @NetAuditedEstimate - @Betterment
        END
    END
    ELSE
    BEGIN
        IF @OriginalEstimateDocumentID IS NULL
        BEGIN
            IF @LastSupplementSeqNo = 0
            BEGIN
                SET @Message = 'Original Estimate not found.'
            END
            ELSE
            BEGIN
                SET @Message = 'Original Supplement ' + convert(varchar, @LastSupplementSeqNo) + ' not found.'
            END
        END

        IF @AuditedEstimateDocumentID IS NULL
        BEGIN
            IF @LastSupplementSeqNo = 0
            BEGIN
                SET @Message = 'Audited Estimate not found.'
            END
            ELSE
            BEGIN
                SET @Message = 'Audited Supplement ' + convert(varchar, @LastSupplementSeqNo) + ' not found.'
            END
        END
    END

    SELECT @CarrierRepName = isNull(uc.NameFirst + ' ' + uc.NameLast, ''),
           @AuditorName = isNull(ua.NameFirst + ' ' + ua.NameLast, ''),
           @AuditorTitle = 'Quality Control Representative',
           @AuditorPhone = '(' + ua.PhoneAreaCode + ') ' + ua.PhoneExchangeNumber + ' ' + ua.PhoneUnitNumber,
           @AuditorEmail = LTRIM(isNull(ua.EmailAddress, '')),
           @ClaimAspectNumber = ca.ClaimAspectNumber,
           @LossState = c.LossState,
           @LossStateName = sc.StateValue
    FROM utb_claim_aspect ca 
    LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
    LEFT JOIN utb_user ua ON ca.AnalystUserID = ua.UserID
    LEFT JOIN utb_user uc ON c.CarrierRepUserID = uc.UserID
    LEFT JOIN utb_state_code sc ON c.LossState = sc.StateCode
    WHERE ca.ClaimAspectID = @ClaimAspectID

    SELECT @InsuranceCompanyName = i.Name,
           @ClaimNumber = c.ClientClaimNumber
    FROM utb_claim c 
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE c.LynxID = @LynxID   
       
    If @DataSet = 'Estimate'
    Begin
		select Top 1 @FormDataID = formdataid from utb_form_data_detail where formfieldid in
		(select formfieldid from utb_form_field where name='ClaimAspectID' and formid=@FormID ) and fielddata= Convert (varchar(50) ,@ClaimAspectID)
		and formdataid in(
		select formdataid from utb_form_data_detail where formfieldid in
		(select formfieldid from utb_form_field where name='DataSet' and formid=@FormID ) and fielddata= @DataSet and
		formdataid in(
		select formdataid from utb_form_data_detail where formfieldid in
		(select formfieldid from utb_form_field where name='lynxid' and formid=@FormID ) and fielddata= Convert(varchar(50),@LynxID)))
	End
	
	If @DataSet = 'Supplement'
	Begin
		select TOP 1 @FormDataID = formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='ClaimAspectID' and formid=@FormID ) and fielddata= Convert (varchar(50) ,@ClaimAspectID) and formdataid in(
			select formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='SequenceNumber' and formid=@FormID ) and fielddata= Convert (varchar(50) ,@SequenceNumber) and formdataid in(
			select formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='DataSet' and formid=@FormID ) and fielddata= @DataSet and formdataid in(
			select formdataid from utb_form_data_detail where formfieldid in
			(select formfieldid from utb_form_field where name='lynxid' and formid=@FormID ) and fielddata= Convert(varchar(50),@LynxID))))
	End
	
    SELECT  1 as Tag,
            NULL as Parent,
            @InsuranceCompanyName AS [Root!1!InsuranceCompanyName],
            @LynxID AS [Root!1!LynxID],
            @ClaimAspectNumber AS [Root!1!ClaimAspectNumber],
            @ClaimNumber AS [Root!1!ClaimNumber],
            @LossState AS [Root!1!LossState],
            @LossStateName AS [Root!1!LossStateName],
            @CarrierRepName AS [Root!1!ClaimRepresentative],
            @OriginalEstimateAmount AS [Root!1!OriginalEstimateAmount],
            @AuditedDifference AS [Root!1!AuditedDifference],
            @AuditedEstimateAmount AS [Root!1!AuditedEstimateAmount],
            @Adjustments AS [Root!1!AdjustmentsTotal],
            @Betterment AS [Root!1!Betterment],
            @Deductible AS [Root!1!Deductible],
            @NetAuditedEstimate AS [Root!1!NetAuditedEstimate],
            @AuditorName AS [Root!1!AuditorName],
            @AuditorTitle AS [Root!1!AuditorTitle],
            @AuditorPhone AS [Root!1!AuditorPhone],
            @AuditorEmail AS [Root!1!AuditorEmail],
            @OriginalEstimateDocumentID AS [Root!1!OriginalDocumentID],
            @AuditedEstimateDocumentID AS [Root!1!AuditedDocumentID],
            @ShopContacted AS [Root!1!ShopContacted],
			@ShopContactName AS [Root!1!ShopContactName],
			@PrimaryReasonCode AS [Root!1!PrimaryReasonCode],
			@PrimaryReasonCodeText AS [Root!1!PrimaryReasonCodeText],
			@SecondaryReasonCode AS [Root!1!SecondaryReasonCode],
			@SecondaryReasonCodeText as [Root!1!SecondaryReasonCodeText],			
            @Message AS [Root!1!Message],		
            @FormDataID AS [Root!1!FormDataID], 
            (SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtSurplusOEMComments')as [Root!1!txtSurplusOEMComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkSurplusOEM') as [Root!1!chkSurplusOEM],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtRecComments') as [Root!1!txtRecComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkReconditioned') as [Root!1!chkReconditioned],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtAfterMarketComments') as [Root!1!txtAfterMarketComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkAftermarket') as [Root!1!chkAftermarket],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='txtLKQComments') as [Root!1!txtLKQComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkLKQ') as [Root!1!chkLKQ],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID  where formdataid = @FormDataID and Name ='chkAltParts') as [Root!1!chkAltParts],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='AdjustmentAmount') as [Root!1!AdjustmentAmount],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='PriceNotAgreedItemsChecked') as [Root!1!PriceNotAgreedItemsChecked],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='AltPartsLocatedItemsChecked') as [Root!1!AltPartsLocatedItemsChecked],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtNoShopContactOther') as [Root!1!txtNoShopContactOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkNoShopContactOther') as [Root!1!chkNoShopContactOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtProbableTLComments') as [Root!1!txtProbableTLComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkProbableTL') as [Root!1!chkProbableTL],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtProhibitedComments') as [Root!1!txtProhibitedComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkProhibited') as [Root!1!chkProhibited],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRecommendLowerOrgEstComments') as [Root!1!txtRecommendLowerOrgEstComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRecommendLowerOrgEst') as [Root!1!chkRecommendLowerOrgEst],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRepairCompleteComments') as [Root!1!txtRepairCompleteComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRepairComplete') as [Root!1!chkRepairComplete],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtThreeUnsuccessfulAttemptsComments') as [Root!1!txtThreeUnsuccessfulAttemptsComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkThreeUnsuccessfulAttempts') as [Root!1!chkThreeUnsuccessfulAttempts],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkNoAttemptToContactShop') as [Root!1!chkNoAttemptToContactShop],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAltPartsAvailableComments') as [Root!1!txtAltPartsAvailableComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkAltPartsAvailable') as [Root!1!chkAltPartsAvailable],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkShopContacted') as [Root!1!chkShopContacted],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAuditorCommentsUser') as [Root!1!txtAuditorCommentsUser],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAdverseSubroComments') as [Root!1!txtAdverseSubroComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkAdverseSubro') as [Root!1!chkAdverseSubro],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='DataSet') as [Root!1!DataSet],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='Vehicles') as [Root!1!Vehicles],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='SequenceNumber') as [Root!1!SequenceNumber],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='ClaimAspectID') as [Root!1!ClaimAspectID],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAuditorComments') as [Root!1!txtAuditorComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtPriceNotAgreedOther') as [Root!1!txtPriceNotAgreedOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkPriceNotAgreedOther') as [Root!1!chkPriceNotAgreedOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtSysDiffComments') as [Root!1!txtSysDiffComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkSystemDiff') as [Root!1!chkSystemDiff],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRateDiffComments') as [Root!1!txtRateDiffComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRateDiff') as [Root!1!chkRateDiff],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtAgreedPriceObtainedWith') as [Root!1!txtAgreedPriceObtainedWith],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkAgreedPriceObtained') as [Root!1!chkAgreedPriceObtained],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkPriceNotAgreed') as [Root!1!chkPriceNotAgreed],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtOther') as [Root!1!txtOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkOther') as [Root!1!chkOther],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtRateCorrComments') as [Root!1!txtRateCorrComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkRateCorrection') as [Root!1!chkRateCorrection],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtEstGuidelinesComments') as [Root!1!txtEstGuidelinesComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkEstimateGuidelines') as [Root!1!chkEstimateGuidelines],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='txtCorrComments') as [Root!1!txtCorrComments],
			(SELECT fdd.FieldData FROM dbo.utb_form_field AS ff INNER JOIN dbo.utb_form_data_detail AS fdd ON ff.FormFieldID = fdd.FormFieldID where formdataid = @FormDataID and Name ='chkCorrection') as [Root!1!chkCorrection]
			

    --FOR XML EXPLICIT

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END
GO
