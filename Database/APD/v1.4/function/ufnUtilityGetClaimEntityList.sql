-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetClaimEntityList' AND type = 'TF')
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetClaimEntityList 
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetClaimEntityList
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Returns list of active entities in the particular claim 
*
* PARAMETERS:  
* (I) @LynxID               The Lynx ID of the claim
* (I) @ExposureOnly         Flag indicating whether to return exposure entities only
* (I) @EntityStatusType     Specifies whether to return 0-All, 1-Open, 2-Closed entities  
*
* RESULT SET:
* Table containing one or more rows of the following fields:
*       EntityCode
*       Name
*
* VERSION:					1.0 - Original Version
*							1.1 - TVD - 15Mar2021 - TXFB 3rd party vehicle number greater than 99 FIX
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityGetClaimEntityList( @LynxId udt_std_int_big, @ExposureOnly udt_std_flag, @EntityStatusType udt_std_int_tiny )
RETURNS @EntityTable TABLE
(
    ClaimAspectID   bigint,
    EntityCode      varchar(6),
    Name            varchar(50)
)
AS
BEGIN
    -- Declare temporary table to work with

    DECLARE @tmpEntity TABLE
    (
        ClaimAspectID   bigint,
        EntityCode      varchar(6),  
        Name            varchar(50),
        ExposureFlag    bit,
        ClosedFlag      bit
    )

    
    -- Select all open entities in current state and build the initial Entity Table
    
    INSERT INTO @tmpEntity
      SELECT DISTINCT
            ca.ClaimAspectID,
            CASE cat.EnumeratedFlag
              WHEN 0 THEN LTrim(RTrim(cat.Code))
              WHEN 1 THEN LTrim(RTrim(cat.Code + Convert(char(3), ca.ClaimAspectNumber)))
            END,
            CASE cat.EnumeratedFlag
              WHEN 0 THEN LTrim(RTrim(cat.Name))
              WHEN 1 THEN LTrim(RTrim(cat.Name + ' ' + Convert(char(3), ca.ClaimAspectNumber)))
            END,
            cat.DefaultExposureFlag,
            CASE 
              WHEN cas.StatusID >= 900 THEN 1        -- Status 900 begins all the "closed" statuses.
              ELSE 0
            END
      FROM  dbo.utb_claim_aspect ca
--Project:210474 APD Added reference to the table below to get to the "StatusID" when we did the code merge M.A.20061114
      LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID) 
      LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
      WHERE ca.LynxID = @LynxID
        AND cas.StatusID IS NOT NULL
        AND cat.GeneratesPertainsToFlag = 1
        and cas.ServiceChannelCD is Null
        and cas.StatusTypeCD is Null

    IF @@ERROR <> 0
    BEGIN        
        RETURN
    END


    -- Remove references to non-exposures if @ExposureOnly is 1
    
    IF @ExposureOnly = 1
    BEGIN
        DELETE FROM @tmpEntity
          WHERE ExposureFlag = 0

        IF @@ERROR <> 0
        BEGIN        
            RETURN
        END
    END

    
    -- Remove open or closed entities based on @EntityStatusType
    
    IF @EntityStatusType = 1
    BEGIN
        -- Keep "Open" only

        DELETE FROM @tmpEntity
          WHERE ClosedFlag = 1

        IF @@ERROR <> 0
        BEGIN        
            RETURN
        END
    END


    IF @EntityStatusType = 2
    BEGIN
        -- Keep "Closed" only

        DELETE FROM @tmpEntity
          WHERE ClosedFlag = 0
            
        IF @@ERROR <> 0
        BEGIN        
            RETURN
        END
    END

    
    -- Finally copy remaining records into the return table

    INSERT INTO @EntityTable (ClaimAspectID, EntityCode, Name)
    SELECT  ClaimAspectID,
            EntityCode,
            Name
      FROM  @tmpEntity


    RETURN
END

GO
-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetClaimEntityList' AND type = 'TF')
BEGIN
    GRANT SELECT ON dbo.ufnUtilityGetClaimEntityList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the function

    RAISERROR ('Function creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
*********************************************************************************************************************************/