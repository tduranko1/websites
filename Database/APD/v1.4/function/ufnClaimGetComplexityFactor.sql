-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnClaimGetComplexityFactor' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnClaimGetComplexityFactor 
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnClaimGetComplexityFactor
* SYSTEM:       Lynx Services ADP
* AUTHOR:       James Stein
* FUNCTION:     Converts a Complexity Weight object identifier and row identifier to a factor
*
* PARAMETERS:  
* @ObjectId                      Complexity Weight object identifier
* @ObjectRowID                   Complexity Weight object's row identifier
*
* RETURNS:
* Factor                         Complexity Weight object's row factor
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnClaimGetComplexityFactor
(
    @ObjectId udt_std_int,
    @ObjectRowID udt_std_int_big
)
RETURNS udt_std_name
AS
BEGIN
    -- Declare internal variables

    DECLARE @Factor    AS udt_std_name


    -- Get Factor from appropriate object

    IF OBJECT_NAME(@ObjectId) = 'utb_injury_type'
    BEGIN
            SELECT @Factor = Name 
            FROM utb_injury_type
            WHERE InjuryTypeID = @ObjectRowID
    END
    ELSE
    IF OBJECT_NAME(@ObjectId) = 'utb_loss_type'
    BEGIN
            SELECT @Factor = Name 
            FROM utb_loss_type
            WHERE LossTypeId = @ObjectRowID
    END
    ELSE
    IF OBJECT_NAME(@ObjectId) = 'utb_road_location'
    BEGIN
            SELECT @Factor = Name 
            FROM utb_road_location
            WHERE RoadLocationID = @ObjectRowID
    END
    ELSE
    IF OBJECT_NAME(@ObjectId) = 'utb_claim_complexity'
    BEGIN
            SELECT @Factor = Name 
            FROM utb_claim_complexity
            WHERE ComplexityID = @ObjectRowID
    END
    ELSE
    BEGIN
            SELECT @Factor = 'ERROR!!'
    END

    RETURN @Factor
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnClaimGetComplexityFactor' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnClaimGetComplexityFactor TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnClaimGetComplexityFactor' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnClaimGetComplexityFactor TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/