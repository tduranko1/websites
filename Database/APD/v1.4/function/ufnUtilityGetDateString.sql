-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetDateString' AND type = 'FN')
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetDateString 
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetDateString
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Returns datetime in the application specific style 
*
* PARAMETERS:  
* (I) @Date     The datetime to convert to string
*
* RESULT SET:
* CONVERT(VARCHAR(30), SysLastUpdatedDate, 126)
*
* NOTE: When some data interfaces receive a datetime within a SQL Server result set, they will automatically convert 
* the value into a region specific format based on workstation/server settings. Most often, the default format setting 
* will truncate off the miliseconds. To prevent the loss of miliseconds, the datetime must be converted to a string in 
* the desired "style". The current preferred syle is 126 (ISO8601), yyyy-mm-dd Thh:mm:ss:mmm(no spaces)
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityGetDateString
(
    @Date datetime
)
RETURNS VARCHAR(30)
AS
BEGIN
    DECLARE @DateString AS VARCHAR(30)

    SELECT @DateString = CONVERT(VARCHAR(30), @Date, 126)

    RETURN @DateString
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetDateString' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityGetDateString TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the function

    RAISERROR ('Function creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
*********************************************************************************************************************************/