-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnClaimGetComplexityDescription' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnClaimGetComplexityDescription 
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnClaimGetComplexityDescription
* SYSTEM:       Lynx Services ADP
* AUTHOR:       James Stein
* FUNCTION:     Converts a Complexity Weight object identifier and row identifier to a description
*
* PARAMETERS:  
* @ObjectId                      Complexity Weight object identifier
* @ObjectRowID                   Complexity Weight object's row identifier
*
* RETURNS:
* Factor                        Complexity Weight object's row description
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnClaimGetComplexityDescription
(
    @ObjectId udt_std_int,
    @ObjectRowID udt_std_int_big
)
RETURNS udt_std_name
AS
BEGIN
    -- Declare internal variables

    DECLARE @Description    AS udt_std_name

    -- Get Description Information from appropriate object

    IF OBJECT_NAME(@ObjectId) = 'utb_injury_type'
    BEGIN
            SELECT @Description = 'Injury Type' 
    END
    ELSE
    IF OBJECT_NAME(@ObjectId) = 'utb_loss_type'
    BEGIN
            SELECT @Description = 'Loss Type' 
    END
    ELSE
    IF OBJECT_NAME(@ObjectId) = 'utb_road_location'
    BEGIN
            SELECT @Description = 'Road Location'
    END
    ELSE
    IF OBJECT_NAME(@ObjectId) = 'utb_claim_complexity'
    BEGIN
            SELECT @Description = Description
            FROM utb_claim_complexity
            WHERE ComplexityID = @ObjectRowID
    END
    ELSE
    BEGIN
            SELECT @Description = 'ERROR!!' 
    END

    RETURN @Description
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnClaimGetComplexityDescription' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnClaimGetComplexityDescription TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnClaimGetComplexityDescription' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnClaimGetComplexityDescription TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/