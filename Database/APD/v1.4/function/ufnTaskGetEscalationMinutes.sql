-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnTaskGetEscalationMinutes' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnTaskGetEscalationMinutes 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnTaskGetEscalationMinutes
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the Task Escalation minutes for a given task
*
* PARAMETERS:  
* @TaskID               The Task id
* @InsuranceCompanyID   The insurance company id
* @ServiceChannelCD     Service Channel Code
*
* RETURNS:  int
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnTaskGetEscalationMinutes ( @TaskID int, @InsuranceCompanyID int, @ServiceChannelCD varchar(4))
RETURNS int
AS
BEGIN

    DECLARE @EscalationMinutes as int

    IF EXISTS(SELECT TaskID
                FROM utb_task
                WHERE TaskID = @TaskID)
    BEGIN

        SELECT  @EscalationMinutes = EscalationMinutes
          FROM  dbo.utb_task
          WHERE TaskID = @TaskID

        -- Let us check if there is an escalation minutes defined for 
        -- InsuranceCompany and Service channel
        IF EXISTS (SELECT TaskEscalationID
                    FROM utb_task_escalation
                    WHERE TaskID = @TaskID
                      AND InsuranceCompanyID = @InsuranceCompanyID
                      AND ServiceChannelCD = @ServiceChannelCD)
        BEGIN
            -- Escalation minutes is overridden
            SELECT  @EscalationMinutes = EscalationMinutes
            FROM utb_task_escalation
            WHERE TaskID = @TaskID
              AND InsuranceCompanyID = @InsuranceCompanyID
              AND ServiceChannelCD = @ServiceChannelCD
        END
        ELSE
        BEGIN
            -- Let us check if there is an escalation minutes defined for 
            -- InsuranceCompany only
            IF EXISTS (SELECT TaskEscalationID
                        FROM utb_task_escalation
                        WHERE TaskID = @TaskID
                          AND InsuranceCompanyID = @InsuranceCompanyID
                          AND ServiceChannelCD IS NULL)
            BEGIN
                -- Escalation minutes is overridden
                SELECT  @EscalationMinutes = EscalationMinutes
                FROM utb_task_escalation
                WHERE TaskID = @TaskID
                  AND InsuranceCompanyID = @InsuranceCompanyID
                  AND ServiceChannelCD IS NULL
            END
            ELSE
            BEGIN
                -- Let us check if there is an escalation minutes defined for 
                -- ServiceChannel only
                IF EXISTS (SELECT TaskEscalationID
                            FROM utb_task_escalation
                            WHERE TaskID = @TaskID
                              AND ServiceChannelCD = @ServiceChannelCD
                              AND InsuranceCompanyID IS NULL)
                BEGIN
                    -- Escalation minutes is overridden
                    SELECT  @EscalationMinutes = EscalationMinutes
                    FROM utb_task_escalation
                    WHERE TaskID = @TaskID
                      AND ServiceChannelCD = @ServiceChannelCD
                      AND InsuranceCompanyID IS NULL
                END
            END
        END
    END

    RETURN @EscalationMinutes
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnTaskGetEscalationMinutes' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnTaskGetEscalationMinutes TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnTaskGetEscalationMinutes' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnTaskGetEscalationMinutes TO 
            ugr_lynxapd, ugr_fnolload

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/