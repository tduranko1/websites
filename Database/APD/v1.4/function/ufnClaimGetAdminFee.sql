-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnClaimGetAdminFee' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnClaimGetAdminFee 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnClaimGetAdminFee
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the admin fee settings for the ClaimAspectServiceChannelID and ClientFeeID
*
* PARAMETERS:  
* @ClaimAspectServiceChannelID  The claim aspect service channel id
* @ClientFeeID                  The client fee id being applied
*
* RETURNS:  Recordset { AdminFeeCD, AdminFeeAmt }
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnClaimGetAdminFee ( @ClaimAspectServiceChannelID bigint, @ClientFeeID bigint )
RETURNS @AdminFee TABLE
    (
        AdminFeeCD      varchar(1), 
        AdminFeeAmt     decimal(9,2)
    )
AS
BEGIN
    -- Declare internal variables
    DECLARE @AdminFeeID     as bigint
    DECLARE @AdminFeeCD     as varchar(2)
    DECLARE @AppliesToCD    as varchar(2)
    DECLARE @AdminFeeAmt    as decimal(9,2)
    DECLARE @ShopZipCode    as varchar(5)
    DECLARE @ShopZipMSA     as varchar(5)
    DECLARE @ShopState      as varchar(2)
    DECLARE @ProgramFlag    as bit
    
    -- Admin fees are applicable only to Program shop repair complete
    IF EXISTS(SELECT cfd.ClientFeeID
                FROM utb_client_fee_definition cfd
                LEFT JOIN utb_service s ON cfd.ServiceID = s.ServiceID
                WHERE cfd.ClientFeeID = @ClientFeeID
                  AND s.ServiceChannelCD = 'PS'
                  AND s.DispositionTypeCD = 'RC')
    BEGIN
        -- Get the active assignment zip code of the primary service channel
        SELECT @ShopZipCode = AddressZip,
               @ShopZipMSA = CountyType,
               @ShopState = AddressState,
               @ProgramFlag = ProgramFlag
        FROM utb_assignment a
        LEFT JOIN utb_shop_location sl on a.ShopLocationID = sl.ShopLocationID
        LEFT JOIN utb_zip_code zc on sl.AddressZip = zc.Zip
        WHERE a.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND CancellationDate IS NULL

        IF @ProgramFlag = 1
        BEGIN
            -- Who is paying the admin fee and the flat rate.
            SELECT @AdminFeeID = AdminFeeID,
                   @AdminFeeCD = AdminFeeCD,
                   @AppliesToCD = AppliesToCD,
                   @AdminFeeAmt = Amount
            FROM utb_admin_fee
            WHERE ClientFeeID = @ClientFeeID
              AND EnabledFlag = 1

            IF @AppliesToCD <> 'F'
            BEGIN
                -- admin fee based on state or msa

                IF EXISTS(SELECT AdminFeeID
                            FROM utb_admin_fee_msa
                            WHERE AdminFeeID = @AdminFeeID
                              AND MSA = @ShopZipMSA
                              AND EnabledFlag = 1)
                BEGIN
                    -- If an admin fee is defined for the msa, then use it
                    SELECT @AdminFeeCD = AdminFeeCD,
                           @AdminFeeAmt = Amount
                    FROM utb_admin_fee_msa
                    WHERE AdminFeeID = @AdminFeeID
                      AND MSA = @ShopZipMSA
                      AND EnabledFlag = 1
                END
                ELSE
                BEGIN
                    IF EXISTS(SELECT AdminFeeID
                                FROM utb_admin_fee_state
                                WHERE AdminFeeID = @AdminFeeID
                                  AND StateCode = @ShopState
                                  AND EnabledFlag = 1)
                    BEGIN
                        -- If an admin fee is defined for the state, then use it
                        SELECT @AdminFeeCD = AdminFeeCD,
                               @AdminFeeAmt = Amount
                        FROM utb_admin_fee_state
                        WHERE AdminFeeID = @AdminFeeID
                          AND StateCode = @ShopState
                          AND EnabledFlag = 1
                    END
                END
            END

            INSERT INTO @AdminFee
            (AdminFeeCD, AdminFeeAmt)
            VALUES
            (@AdminFeeCD, @AdminFeeAmt)
        END

    END

    RETURN
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnClaimGetAdminFee' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnClaimGetAdminFee TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnClaimGetAdminFee' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnClaimGetAdminFee TO 
            ugr_lynxapd, ugr_fnolload

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/