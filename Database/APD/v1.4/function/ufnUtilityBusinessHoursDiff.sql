-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityBusinessHoursDiff' AND type = 'FN')
BEGIN
    DROP FUNCTION dbo.ufnUtilityBusinessHoursDiff
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityBusinessHoursDiff
* SYSTEM:       LYNX Services APD
* AUTHOR:       Steve Kearns
* FUNCTION:     Returns the number of business hours between two dates.  Note that either date may be a weekend date.
*
* PARAMETERS:
* (I) @BeginDate       The beginning date
* (I) @EndDate         The ending date
*
* RESULT:  INT   The number of hours between the two dates
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityBusinessHoursDiff( @BeginDate datetime, @EndDate datetime )
RETURNS int

AS
BEGIN
    DECLARE @hours INT

    -- If either @BeginDate or @EndDate are on a weekend, reset them to the next business day hour (Monday 8AM)
    IF DATEPART(weekday, @BeginDate) = 7 SET @BeginDate = Convert(datetime, Convert(varchar(12), DateAdd(d, 2, @BeginDate), 101) + ' 8:00:00 AM')
    IF DATEPART(weekday, @BeginDate) = 1 SET @BeginDate = Convert(datetime, Convert(varchar(12), DateAdd(d, 1, @BeginDate), 101) + ' 8:00:00 AM')
    IF DATEPART(weekday, @EndDate) = 7 SET @EndDate = Convert(datetime, Convert(varchar(12), DateAdd(d, 2, @EndDate), 101) + ' 8:00:00 AM')
    IF DATEPART(weekday, @EndDate) = 1 SET @EndDate = Convert(datetime, Convert(varchar(12), DateAdd(d, 1, @EndDate), 101) + ' 8:00:00 AM')
        
    -- Compute actual number of hours difference
    SET @hours = DATEDIFF(hour, @BeginDate, @EndDate)

    -- Subtract weekends
    SET @hours = @hours - (DATEDIFF(week, @BeginDate, @EndDate) * 56) -- 2 * 24 hrs for Sat/Sun + 8 hours to 8AM Mon morning

    -- Sanity check
    IF @hours < 0 SET @hours = 0

    RETURN @hours
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityBusinessHoursDiff' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityBusinessHoursDiff TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the function

    RAISERROR ('Function creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/ 
