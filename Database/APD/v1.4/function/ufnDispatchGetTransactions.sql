-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnDispatchGetTransactions' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnDispatchGetTransactions 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnDispatchGetTransactions
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the Dispatch transactions
*
* PARAMETERS:  
* @DispatchNumber  The dispatch number you want the transaction history for
*
* RETURNS:  Recordset 
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnDispatchGetTransactions ( @DispatchNumber varchar(50) )
RETURNS @DispatchTransactions TABLE
    (
        TransactionID            bigint,
        Amount                   money,
        Description              varchar(250),
        DisbursementAmount       money,
        DisbursementDate         datetime,
        DisbursementMethodCD     varchar(4),
        DisbursementMethodDesc   varchar(50),
        DispatchNumber           varchar(50),
        DisptachNumberNew        varchar(50),
        TransactionCD            varchar(4),
        TransactionDesc          varchar(50),
        TransactionDate          datetime
    )
AS
BEGIN

    DECLARE @OldDispatchNumber as varchar(50)
    DECLARE @NewDispatchNumber as varchar(50)
    DECLARE @LoopCountMax as int

    DECLARE @tmpDispatchInfo TABLE (
       TransactionID          bigint         NOT NULL,
       Amount                 decimal(9, 2)  NOT NULL,
       Description            varchar(250)   NOT NULL,
       DisbursementAmount     decimal(9, 2)  NULL,
       DisbursementDate       datetime       NULL,
       DisbursementMethodCD   varchar(4)     NULL,
       DisbursementMethodDesc varchar(50)    NULL,
       DispatchNumber         varchar(50)    NOT NULL,
       DisptachNumberNew      varchar(50)    NULL,
       TransactionCD          varchar(4)     NOT NULL,
       TransactionDesc        varchar(50)    NOT NULL,
       TransactionDate        datetime       NOT NULL)
       
    INSERT INTO @tmpDispatchInfo 
    SELECT  TransactionID,
            Amount,
            isNull(Description, ''),
            DisbursementAmount,
            DisbursementDate,
            DisbursementMethodCD,
            dc.Name,
            id.DispatchNumber,
            DisptachNumberNew,
            TransactionCD,
            tc.Name,
            TransactionDate
    FROM dbo.utb_invoice_dispatch id
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'TransactionCD') tc ON id.TransactionCD = tc.Code
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'DisbursementMethodCD') dc ON id.DisbursementMethodCD = dc.Code
    WHERE id.DispatchNumber = @DispatchNumber
       OR id.DisptachNumberNew = @DispatchNumber
    ORDER BY TransactionID DESC

    SELECT TOP 1 @OldDispatchNumber = DispatchNumber,
                 @NewDispatchNumber = DisptachNumberNew
    FROM @tmpDispatchInfo
    WHERE DisptachNumberNew IS NOT NULL
    
    SET @LoopCountMax = 1000 -- Prevent endless loop
    
    WHILE @OldDispatchNumber IS NOT NULL AND @LoopCountMax > 0
    BEGIN
    
       INSERT INTO @tmpDispatchInfo 
       SELECT  TransactionID,
               Amount,
               Description,
               DisbursementAmount,
               DisbursementDate,
               DisbursementMethodCD,
               dc.Name,
               DispatchNumber,
               DisptachNumberNew,
               TransactionCD,
               tc.Name,
               TransactionDate
       FROM dbo.utb_invoice_dispatch id
       LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'TransactionCD') tc ON id.TransactionCD = tc.Code
       LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'DisbursementMethodCD') dc ON id.DisbursementMethodCD = dc.Code
       WHERE DispatchNumber = @OldDispatchNumber
       ORDER BY TransactionID DESC
       
       UPDATE @tmpDispatchInfo
       SET DisptachNumberNew = NULL
       WHERE DispatchNumber = @OldDispatchNumber
         AND DisptachNumberNew = @NewDispatchNumber
       
       SET @OldDispatchNumber = NULL
       SET @NewDispatchNumber = NULL
      
       SELECT TOP 1 @OldDispatchNumber = DispatchNumber,
                    @NewDispatchNumber = DisptachNumberNew
       FROM @tmpDispatchInfo
       WHERE DisptachNumberNew IS NOT NULL
       
       SET @LoopCountMax = @LoopCountMax - 1
      
    END    

    INSERT INTO @DispatchTransactions
    SELECT * FROM @tmpDispatchInfo
    ORDER BY TransactionDate desc
    
    RETURN
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnDispatchGetTransactions' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnDispatchGetTransactions TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnDispatchGetTransactions' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnDispatchGetTransactions TO 
            ugr_lynxapd, ugr_fnolload

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/