-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityParseString' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityParseString 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityParseString
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Parses @String delimited by @Delimiter into a single column recordset 
*
* PARAMETERS:  
* @string               String to parse
* @delimiter            Delimiter character
* @trimspaces           Flag indicating whether to trim spaces, defaults to 0
*
* RETURNS:  Recordset { strindex, value }
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityParseString ( @string varchar(8000), @delimiter char(1), @trimspaces bit = 0 )
RETURNS @ParsedValues TABLE
    (
        strindex    int, 
        value       varchar(8000)
    )
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @StartPos           int
    DECLARE @DelimiterPos       int
    DECLARE @DelimitedString    varchar(8000)
    DECLARE @strindex           int


    -- First check to make sure @string is not NULL.  If it is, simply return an empty table

    IF @string IS NULL
    BEGIN
        RETURN
    END


    -- Check to make sure @delimiter is not NULL.  If it is, return the table with the original string as the only entry

    IF @delimiter IS NULL
    BEGIN
        INSERT INTO @ParsedValues
        VALUES ( 1, @string )

        RETURN
    END


    -- Step through @string looking for @delimiter.  Once found, get the position save that portion into the return
    -- table.

    SET @String = LTrim(RTrim(@string))
    SET @StartPos = 1
    SET @strindex = 1

    WHILE @startpos <= Len(@string)
    BEGIN
        -- Find @delimiter in @string

        SET @DelimiterPos = CharIndex(@delimiter, @string, @startpos)

        IF @DelimiterPos = 0
        BEGIN
            -- Delimiter was not found, set @Delimiter to 1 char beyond the end of the string

            SET @DelimiterPos = Len(@string) + 1
        END

       
        -- Save the delimited portion into the return table, as long as it is not NULL

        /*  Note on the following statement:  THe substring function appears to autotrim spaces during it's operation.  This
         *  can result in some unusual side effects.  For example Len(Substring('  ,abc', 1, 3)) will result in 0 for the 
         *  len.  This obviously has bearing on the Insert statement which follows, causing the extracted '   ' string not
         *  to be inserted.  For now, this behavior is acceptable for our purposes.   */

        SET @DelimitedString = Substring(@string, @StartPos, @DelimiterPos - @StartPos)

        IF @trimspaces = 1
        BEGIN
            SET @DelimitedString = LTrim(RTrim(@DelimitedString))
        END

        INSERT INTO @ParsedValues
        VALUES ( @strindex, @DelimitedString )


        -- Update start position and index
        SET @StartPos = @DelimiterPos + 1
        SET @strindex = @strindex + 1
    END

    RETURN
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityParseString' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityParseString TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityParseString' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityParseString TO 
            ugr_lynxapd, ugr_fnolload

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/