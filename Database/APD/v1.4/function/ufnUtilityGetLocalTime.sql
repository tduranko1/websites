-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetLocalTime' AND type = 'FN')
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetLocalTime
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetLocalTime
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Returns the current local date and time for the zip code.  Compensates for daylight savings time.
*
* PARAMETERS:
* (I) @CurrentGMTText       The current GMT as a varchar.  This must be passed in because you cannot call GetUTCDate
*                           from within a user-defined function.  
* (I) @ZipCode              The zip code to get the local date/time for 
*
* RESULT:       The local date/time
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityGetLocalTime( @CurrentGMTText varchar(20), @ZipCode udt_addr_zip_code )
RETURNS datetime

AS
BEGIN
    -- Declare local variables
    
    DECLARE @CurrentGMT                 AS datetime
    DECLARE @StartDST                   AS datetime
    DECLARE @EndDST                     AS datetime
    DECLARE @GMTOffset                  AS int
    DECLARE @DaylightSavingsTimeFlag    AS int


    -- Validate Parameters.  If invalid, return NULL
    IF (IsDate(@CurrentGMTText) = 0) OR
        (NOT EXISTS(SELECT zip from dbo.utb_zip_code WHERE zip = @ZipCode))
    BEGIN
        RETURN NULL
    END
    ELSE
    BEGIN
        SET @CurrentGMT = Convert(datetime, @CurrentGMTText)
    END


    -- Get this years DST dates

    SELECT  @StartDST = StartDate,
            @EndDST = EndDate
    FROM    dbo.utb_dst
    WHERE   DatePart(year, StartDate) = DatePart(year, @CurrentGMT)


    -- Get time zone and daylight savings information for zip code

    SELECT  @GMTOffset = tz.GMTOffset,
            @DaylightSavingsTimeFlag =
                CASE
                    WHEN @CurrentGMT BETWEEN @StartDST AND @EndDST THEN zc.DaylightSavingsTimeFlag
                    ELSE 0
                END
    FROM    dbo.utb_zip_code zc
    INNER JOIN dbo.utb_time_zone tz
        ON (zc.TimeZone = tz.TimeZone)
    WHERE   zc.Zip = @ZipCode


    -- Calculate local time and return.  Calculation is done by adding the DST Flag (which will be 1 (if true),
    -- or 0 (if false) to the GMT offset for the zip code to get the number of net hours offset from GMT.
    -- This number of hours is then added to the current GMT time to get the local time for the zip code.

    RETURN DateAdd(hour, @GMTOffset + @DaylightSavingsTimeFlag, @CurrentGMT) 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetLocalTime' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityGetLocalTime TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the function

    RAISERROR ('Function creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/ 