-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnIsUserAvail4Assignment' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnIsUserAvail4Assignment 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnIsUserAvail4Assignment
* SYSTEM:       Lynx Services ADP
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* @input                description
* @input                description
*
* RETURNS:
* [Data or result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnIsUserAvail4Assignment ( @UserID int, @ApplicationCD char(4) = 'APD', @ApplicationID int = NULL, @Now datetime)
RETURNS bit
AS
BEGIN
    -- Declare internal variables

    DECLARE @ApplicationIDWork  int
    DECLARE @StatusFlag         bit
    DECLARE @AssignmentBeginDt  datetime
    DECLARE @AssignmentEndDt    datetime


    -- Validate User ID

    IF  (@UserID IS NULL) 
        OR NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
    BEGIN
        -- User ID Invalid, simply return false

        RETURN 0
    END


    -- Validate ApplicationCD or ID has been passed
    
    IF (@ApplicationCD IS NULL) AND (@ApplicationID IS NULL)
    BEGIN
        -- The caller wishes to determine whether the user is active for any application

        SET @ApplicationIDWork = 0
    END
    ELSE
    BEGIN    
        IF @ApplicationID IS NOT NULL
        BEGIN
            -- Validate it exists in the application table
        
            IF NOT EXISTS(SELECT ApplicationID FROM dbo.utb_application WHERE ApplicationID = @ApplicationID)
            BEGIN
                -- Application ID Invalid, simply return false

                RETURN 0
            END
            ELSE
            BEGIN
                SET @ApplicationIDWork = @ApplicationID
            END
        END
        ELSE
        BEGIN
            -- Check @ApplicationCD
        
            SELECT  @ApplicationIDWork = ApplicationID 
              FROM  dbo.utb_application 
              WHERE Code = @ApplicationCD
              
            IF @ApplicationIDWork IS NULL  
            BEGIN
                -- Application CD Invalid, simply return false

                RETURN 0
            END
        END
    END

    -- Check if the user is active
    SET @StatusFlag = dbo.ufnUtilityIsUserActive(@UserID, @ApplicationCD, @ApplicationID)
    
    IF @StatusFlag = 1
    BEGIN
        -- User must have be able to receive assignments from atleast one insurance company
        
        IF EXISTS(SELECT UserID
                    FROM dbo.utb_user_insurance
                    WHERE UserID = @UserID)
        BEGIN
            SET @StatusFlag = 1
        END
        ELSE
        BEGIN
            SET @StatusFlag = 0
        END
    END
    
    IF @StatusFlag = 1
    BEGIN
        -- User must have be able to receive assignments from atleast one state
        
        IF EXISTS(SELECT UserID
                    FROM dbo.utb_user_state
                    WHERE UserID = @UserID
                      AND AssignWorkFlag = 1)
        BEGIN
            SET @StatusFlag = 1
        END
        ELSE
        BEGIN
            SET @StatusFlag = 0
        END
    END

    IF @StatusFlag = 1
    BEGIN
    	-- User must have atleast one of the work assignment pool profiles
	IF EXISTS(SELECT p.ProfileID
			FROM    dbo.utb_profile p
		    	WHERE p.EnabledFlag = 1
			  AND dbo.ufnUtilityGetProfileValue( @UserID, DEFAULT, p.ProfileID, DEFAULT) = 1
			  AND p.Name IN ('Receive Shop Program Claims',
					 'Receive Damage Reinspections',
					 'Receive Mobile Electronics Claims',
					 'Receive Desk Audits',
					 'Analyze Shop Program Claims',
					 'Analyze Desk Audits',
					 'Analyze Damage Reinspections',
					 'Analyze Mobile Electronics Claims',
					 'Administrate Shop Program Claims',
					 'Administrate Desk Audits',
					 'Administrate Damage Reinspections',
					 'Administrate Mobile Electronics Claims')
		 )
	BEGIN
            SET @StatusFlag = 1
	END
	ELSE
	BEGIN
            SET @StatusFlag = 0
	END
    END

    IF @StatusFlag = 1
    BEGIN
	-- User must belong to atleast one of the assignment pools
	IF EXISTS(SELECT AssignmentPoolID
		    FROM dbo.utb_assignment_pool_user
		    WHERE UserID = @UserID)
	BEGIN
            SET @StatusFlag = 1
	END
	ELSE
	BEGIN
            SET @StatusFlag = 0
	END
    END

    IF @StatusFlag = 1
    BEGIN
	-- User must not be on vacation
	SELECT @AssignmentBeginDt = AssignmentBeginDate,
	       @AssignmentEndDt = AssignmentEndDate
	FROM dbo.utb_user
	WHERE UserID = @UserID

	IF @AssignmentBeginDt IS NULL AND @AssignmentEndDt IS NULL
	BEGIN
            SET @StatusFlag = 1
	END
	ELSE IF @Now between @AssignmentEndDt AND @AssignmentBeginDt
	BEGIN
            SET @StatusFlag = 0
	END
	ELSE IF @AssignmentEndDt IS NOT NULL AND @now > @AssignmentEndDt AND @AssignmentBeginDt IS NULL
	BEGIN
            SET @StatusFlag = 0
	END
	ELSE
	BEGIN
            SET @StatusFlag = 1
	END
    END
    
    Return @StatusFlag
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnIsUserAvail4Assignment' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnIsUserAvail4Assignment TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnIsUserAvail4Assignment' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnIsUserAvail4Assignment TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/