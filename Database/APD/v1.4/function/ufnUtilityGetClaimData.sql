-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetClaimData' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetClaimData 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetClaimData
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Ramesh Vishegu
* FUNCTION:      
*
* PARAMETERS:  
*
* RETURNS:  
*
	
	
*
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function

CREATE FUNCTION dbo.ufnUtilityGetClaimData (@ClaimAspectServiceChannelID bigint, @macro varchar(256))
RETURNS varchar(1000)
AS  
BEGIN 

   DECLARE @Result as varchar(256)
   
   SET @Result = '''ERROR'''
   
   IF @ClaimAspectServiceChannelID IS NOT NULL
   BEGIN
      IF @macro = '%EARLYBILLFLAG%' 
      BEGIN
         SELECT @Result = CONVERT(varchar, EarlyBillFlag)
         FROM utb_claim_aspect_service_channel casc
         LEFT JOIN utb_claim_aspect ca ON casc.ClaimASpectID = ca.ClaimAspectID
         LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
         LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
         WHERE casc.ClaimASpectServiceChannelID = @ClaimAspectServiceChannelID
      END
      IF @macro = '%MISSINGREQUIREDDOCUMENTS%' 
      BEGIN

         DECLARE @tmpMissingDocs TABLE (
            DocumentTypeID       int,
            Name                 varchar(100)
         )
         
         DECLARE @InsuranceCompanyID as int
         DECLARE @missingDocuments varchar(500)

         SELECT @InsuranceCompanyID = c.InsuranceCompanyID
         FROM utb_claim_aspect_service_channel casc
         LEFT JOIN utb_claim_aspect ca ON casc.ClaimASpectID = ca.ClaimAspectID
         LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
         WHERE casc.ClaimASpectServiceChannelID = @ClaimAspectServiceChannelID
         
         INSERT INTO @tmpMissingDocs
         SELECT rd.DocumentTypeID, dt.Name
         FROM dbo.utb_client_required_document_type rd
         LEFT JOIN dbo.utb_document_type dt ON rd.DocumentTypeID = dt.DocumentTypeID
         WHERE rd.DocumentTypeID NOT IN (SELECT DISTINCT DocumentTypeID
                                       FROM dbo.utb_claim_aspect_service_channel casc 
                                       LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                                       LEFT JOIN dbo.utb_document d ON cascd.DocumentID = d.DocumentID
                                       WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                                         AND ServiceChannelCD = 'PS'
                                         AND casc.EnabledFlag = 1
                                         AND d.EnabledFlag = 1)
           AND rd.InsuranceCompanyID = @InsuranceCompanyID
           AND rd.ServiceChannelCD = 'PS'
           AND rd.MandatoryFlag = 1
           AND rd.EnabledFlag = 1
         
         
         SET  @missingDocuments = 'NO REQUIRED DOCUMENTS.'
         
         IF EXISTS (SELECT DocumentTypeID 
                     FROM @tmpMissingDocs)
         BEGIN
           SELECT @missingDocuments = @missingDocuments + Name + ', '
           FROM @tmpMissingDocs
           
           IF LEN(@MissingDocuments) > 0 SELECT @missingDocuments = LEFT(@missingDocuments, len(@missingDocuments) - 2)
         END
         
         SET @Result = @missingDocuments

      END
   END

	return(@Result)

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetClaimData' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityGetClaimData TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityGetClaimData' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityGetClaimData TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
