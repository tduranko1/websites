-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityPadChars' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityPadChars 
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityPadChars
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Pads a @string out to @stinglen using @padchar 
*
* PARAMETERS:  
* @string               The string to pad
* @stringlen            The length to pad the string to
* @padchar              The character to pad with
* @padcode              A code to determine where to place the pad
*                           1 - Before @string
*                           2 - After @string
*
* RETURNS:      The padded text string
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityPadChars ( @string varchar(8000), @stringlen int, @padchar char(1), @padcode tinyint)
RETURNS varchar(8000)
AS
BEGIN
    -- First check to see if the length of @string is greater than or equal to @stringlen.  If so, just return @string.

    IF Len(@string) >= @stringlen
    BEGIN
        RETURN @string
    END


    -- Now check to make sure @stringlen is positive, otherwise return @string

    IF @stringlen < 0
    BEGIN
        RETURN @string
    END


    --Otherwise, perform the padding

    -- Set the string that will be concatenated before or after @string

    DECLARE @PadString varchar(8000)
    SET @PadString = Replicate(@padchar, @stringlen - Len(@string))


    -- Now perform the padding based on @padcode

    IF @padcode = 1
    BEGIN
        -- Pad goes before

        RETURN LTrim(RTrim(@PadString + @string))
    END
    ELSE
    IF @padcode = 2
    BEGIN
        -- Pad goes after
        RETURN LTrim(RTrim(@string + @PadString))
    END


    -- Invalid padcode, just return @string
    RETURN @string
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityPadChars' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityPadChars TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityPadChars' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityPadChars TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/