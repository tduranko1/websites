-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityReplaceMacros' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityReplaceMacros 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityReplaceMacros
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Ramesh Vishegu
* FUNCTION:      
*
* PARAMETERS:  
*
* RETURNS:  
*
	
	
*
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function

CREATE FUNCTION dbo.ufnUtilityReplaceMacros (@ClaimAspectServiceChannelID bigint, @ConditionText varchar(5000))
RETURNS varchar(5000)
AS  
BEGIN 

   DECLARE @MacroStartPos as int
   DECLARE @MacroStopPos as int
   DECLARE @MacroName as varchar(50)
   DECLARE @MacroValue as varchar(50)
   DECLARE @LoopCount as int

   SET @MacroStartPos = CHARINDEX('%', @ConditionText, 1)

   SET @LoopCount = 100

   WHILE @MacroStartPos > 0 AND @LoopCount > 0
   BEGIN
      SET @MacroStopPos = CHARINDEX('%', @ConditionText, @MacroStartPos + 1)
      SET @MacroName = SUBSTRING(@ConditionText, @MacroStartPos, (@MacroStopPos - @MacroStartPos) + 1)
      
      SELECT @MacroValue = dbo.ufnUtilityGetClaimData(@ClaimASpectServiceChannelID, @MacroName)
      
      SET @ConditionText = REPLACE(@ConditionText, @MacroName, @MacroValue)

      SET @MacroStartPos = CHARINDEX(@ConditionText, '%', 1)
      SET @MacroStopPos = 0
      
      SET @LoopCount = @LoopCount - 1
   END

	return(@ConditionText)

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityReplaceMacros' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityReplaceMacros TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityReplaceMacros' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityReplaceMacros TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
