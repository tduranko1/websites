/************************************************************************************************************************
*
* PROCEDURE:    ufnRemoveSpecialChar
* SYSTEM:       ADP
* AUTHOR:       Walter J. Chesla
* FUNCTION:     Remove special characters from a input string; special characters may cause problems in APD,
*					any character between Char(32) and Char(123) is considered to be valid (space, punctuation, and alphanumeric)
*
* PARAMETERS:  
* @col - string passed in (e.g. a "collection" of characters to validate)
* @replacementChar - character to replace any invalid chars
*
* RETURNS:  Recordset 
*/

BEGIN TRANSACTION

-- If it already exists, drop the function
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnRemoveSpecialChar' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnRemoveSpecialChar 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION dbo.ufnRemoveSpecialChar(@col VARCHAR(8000), @replacementChar VARCHAR(8000) = ' ')
RETURNS VARCHAR(8000)
AS
BEGIN
	DECLARE
		@position INT,
		@finalCol VARCHAR(8000)

	SELECT @position = 1
	SELECT @finalCol = ''
	
	WHILE @position <= DATALENGTH(@col)
	BEGIN
		IF ASCII(SUBSTRING(@col, @position, 1)) >= 32	--if greater than char(33), it's ok
		BEGIN
			IF ASCII(SUBSTRING(@col, @position, 1)) <= 126		--if less than char(123), it's ok
			BEGIN
				SELECT @finalCol = @finalCol + SUBSTRING(@col, @position, 1)
			END
			ELSE
			BEGIN
				SELECT @finalCol = @finalCol + @replacementChar	--otherwise, it's NOT ok
			END
		END
		ELSE
		BEGIN
			SELECT @finalCol = @finalCol + @replacementChar		--not ok
		END

		SELECT @position = @position + 1
	END

	RETURN @finalCol
END

GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnRemoveSpecialChar' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnRemoveSpecialChar TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function
    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnRemoveSpecialChar' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnRemoveSpecialChar TO 
            ugr_lynxapd, ugr_fnolload

        -- Commit the transaction for dropping and creating the function
        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function
         RAISERROR ('Failure creating function dbo.ufnRemoveSpecialChar', 16, 1)
         ROLLBACK
    END
END

GO