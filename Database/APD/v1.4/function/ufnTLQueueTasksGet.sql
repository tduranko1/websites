-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnTLQueueTasksGet' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnTLQueueTasksGet 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnTLQueueTasksGet
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the TL Queue Tasks
*
* PARAMETERS:  
*
* RETURNS:  Recordset 
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnTLQueueTasksGet ( @now as datetime )
RETURNS @TLQueueTasks TABLE
    (
        ID                 bigint,
        LynxID             bigint,
        ClaimAspectNumber  int,
        TaskID             int,
        TaskName           varchar(100),
        LHName             varchar(100),
        VOName             varchar(100),
        DueDate            varchar(25),
        Locked             varchar(1),
        LockedUserID	      bigint
    )
AS
BEGIN
    DECLARE @TLTaskList as varchar(50)
    DECLARE @ProcName as varchar(50)
    DECLARE @debug as bit


    DECLARE @tmpTLTasks TABLE
    (
        TaskID               int          NOT NULL
    )

    DECLARE @tmpTLQueue TABLE (
       id                   bigint          NOT NULL,
       LynxID               bigint          NOT NULL,
       ClaimAspectNumber    int             NOT NULL,
       TaskID               int             NOT NULL,
       TaskName             varchar(100)    NULL,
       LHName               varchar(100)    NULL,
       VOName               varchar(100)    NULL,
       DueDate              datetime		  NULL,
       Locked               varchar(1)      NOT NULL,
       LockedUserID         bigint			  NULL,
       LockExpirationDate   varchar(25)     NULL)

    SET @ProcName = 'uspTLGetListXML'
    SET @debug = 0

    
    SET @TLTaskList = '93,94,96,97,98'

    INSERT INTO @tmpTLTasks
    SELECT value
    FROM dbo.ufnUtilityParseString(@TLTaskList, ',', 1)
    
    INSERT INTO @tmpTLQueue
    SELECT cl.CheckListID as id,
           ca.LynxID as LynxID,
           ca.ClaimAspectNumber,
           cl.TaskID as TaskID,
           t.Name as TaskName,
           (SELECT Name
            FROM utb_lien_holder lh 
            WHERE lh.LienHolderID = casc.LienHolderID) as LHName,
           IsNull((SELECT  Top 1 case 
                                    when i.BusinessName <> '' then i.BusinessName
                                    else i.NameFirst + ' ' + i.NameLast
                                 end
           FROM  dbo.utb_claim_aspect_involved cai
           LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
           LEFT JOIN  dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
           LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
           WHERE cai.ClaimAspectID = ca.ClaimAspectID
             AND cai.EnabledFlag = 1
             AND irt.Name = 'Owner'), '') as VOName,
           cl.AlarmDate as DueDate,
           /*isNull((SELECT case
							when DateDiff(minute, @now, cll.ExpirationDate) < 0 then 0
							else 1
						  end
            FROM utb_checklist_lock cll
            WHERE cll.ChecklistID = cl.CheckListID), 0) as Locked,
           (SELECT UserID
            FROM utb_checklist_lock cll
            WHERE cll.ChecklistID = cl.CheckListID), 
           (SELECT cll.ExpirationDate
            FROM dbo.utb_checklist_lock cll
            WHERE cll.ChecklistID = cl.CheckListID) as LockExpirationDate*/
			case
				when cll.ExpirationDate is null or DateDiff(minute, @now, cll.ExpirationDate) < 0 then 0
				else 1
			end,
			cll.userID,
			cll.ExpirationDate            
    FROM @tmpTLTasks tmp
    LEFT JOIN utb_checklist cl ON tmp.TaskID = cl.TaskID
    LEFT JOIN utb_claim_aspect_service_channel casc ON cl.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEft JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN utb_task t ON cl.TaskID = t.TaskID
    LEFT OUTER JOIN utb_checklist_lock cll ON cl.CheckListID = cll.CheckListID
    where cl.taskid is not NULL
    ORDER BY cl.DueDate
    
    INSERT INTO @TLQueueTasks
    SELECT id,
           LynxID,
           ClaimAspectNumber,
           TaskID,
           TaskName,
           ISNULL(LHName, ''),
           ISNULL(VOName, ''),
           convert(varchar, DueDate, 101) + ' ' + convert(varchar, DueDate, 108),
           Locked,
           LockedUserID
    FROM @tmpTLQueue
    /*WHERE LockExpirationDate IS NULL
       OR (Locked = 1 AND DateDiff(minute, @now, LockExpirationDate) <= 30)*/
    
    RETURN
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnTLQueueTasksGet' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnTLQueueTasksGet TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnTLQueueTasksGet' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnTLQueueTasksGet TO 
            ugr_lynxapd, ugr_fnolload

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/