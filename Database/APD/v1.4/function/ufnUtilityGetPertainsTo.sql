-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetPertainsTo' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetPertainsTo 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetPertainsTo
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Converts a Claim Aspect Type and Number into a PertainsTo string
*
* PARAMETERS:  
* @ClaimAspectTypeID    The ClaimAspectTypeID
* @ClaimAspectNumber    The ClaimAspectNumber
* @GetStringFlag        Flag indicating whether to return the code or the name 0 = code, 1 = name
*
* RETURNS:      The PertainsTo and PertainsToName
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityGetPertainsTo ( @ClaimAspectTypeID  udt_std_id, @ClaimAspectNumber  udt_std_int, @GetStringFlag  udt_std_flag )
RETURNS varchar(20)
AS
BEGIN
    -- Declare local variables

    DECLARE @EnumeratedFlag     AS udt_std_flag
    DECLARE @PertainsTo         AS varchar(8)
    DECLARE @PertainsToName     AS varchar(20)
    DECLARE @ReturnValue        AS varchar(20)


    -- First look up the Claim Aspect Type ID and get the Code

    SELECT  @EnumeratedFlag = EnumeratedFlag,
            @PertainsTo     = Code,
            @PertainsToName = Name
      FROM  dbo.utb_claim_aspect_type
      WHERE ClaimAspectTypeID = @ClaimAspectTypeID


    -- If nothing is returned, the aspect type is invalid, return an empty pertains to 

    IF @PertainsTo IS NULL
    BEGIN
        RETURN ''
    END


    -- Now look at EnumeratedFlag and claim aspect number.  If EnumeratedFlag is true (1), check to see if claim aspect
    -- number is non-zero. If so, convert it to a varchar and append it to the pertains to string.

    IF @EnumeratedFlag = 1
    BEGIN
        -- This is an enumerated type, check Claim Aspect Number
    
        IF @ClaimAspectNumber > 0
        BEGIN
            -- Append Claim Aspect Number

            SET @PertainsTo = @PertainsTo + Convert(varchar(5), @ClaimAspectNumber)
            SET @PertainsToName = @PertainsToName + ' ' + Convert(varchar(5), @ClaimAspectNumber)
        END
        ELSE
        BEGIN
            -- Aspect is invalid, return an empty pertains to

            RETURN ''
        END
    END


    -- Return the @PertainsTo Value

    IF @GetStringFlag = 1
    BEGIN
        -- Return the string    
    
        SET @ReturnValue = LTrim(RTrim(@PertainsToName))
    END
    ELSE
    BEGIN
        -- Return the string

        SET @ReturnValue = LTrim(RTrim(@PertainsTo))
    END


    RETURN @ReturnValue
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetPertainsTo' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityGetPertainsTo TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityGetPertainsTo' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityGetPertainsTo TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/