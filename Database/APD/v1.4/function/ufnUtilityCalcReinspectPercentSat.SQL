-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityCalcReinspectPercentSat' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityCalcReinspectPercentSat 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityCalcReinspectPercentSat
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Dan Price
* FUNCTION:     Calculate a Percent Satisfaction for a Reinspection
*
* PARAMETERS:  
* @ReinspectID          The ID of the Reinspection on whose detail records this function will operate
*
* RETURNS:
* Percent value representing the Percent Satisfaction score for the indicated Reinspection
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityCalcReinspectPercentSat 
(@ReinspectID int)
RETURNS float
AS
BEGIN

    DECLARE @PercentSat             float
    DECLARE @TotalAdditions         float
    DECLARE @TotalSubtractions      float
    DECLARE @BaseEstimateAmt        float
   

    SET @TotalAdditions = IsNull((Select SUM(ReinspectAmt - EstimateAmt)
                            From dbo.utb_reinspect_detail
                            Where ReinspectID = @ReinspectID
                                And (ReinspectAmt - EstimateAmt) > 0), 0)
                                    
    
    -- @TotalSubtractions will be a negative number
    SET @TotalSubtractions = IsNull((Select SUM(ReinspectAmt - EstimateAmt)
                                    From dbo.utb_reinspect_detail
                                    Where ReinspectID = @ReinspectID
                                        And (ReinspectAmt - EstimateAmt) < 0), 0)


    SET @BaseEstimateAmt = IsNull((Select BaseEstimateAmt From dbo.utb_reinspect Where ReinspectID = @ReinspectID), 0)

    IF @BaseEstimateAmt = 0
    BEGIN
        SET @PercentSat = 0
    END
    ELSE
    BEGIN
        SET @PercentSat = (@BaseEstimateAmt + @TotalSubtractions) / (@BaseEstimateAmt + @TotalAdditions)
    END
    Return @PercentSat

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityCalcReinspectPercentSat' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityCalcReinspectPercentSat TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityCalcReinspectPercentSat' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityCalcReinspectPercentSat TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/