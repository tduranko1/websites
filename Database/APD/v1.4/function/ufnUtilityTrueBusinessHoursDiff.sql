-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityTrueBusinessHoursDiff' AND type = 'FN')
BEGIN
    DROP FUNCTION dbo.ufnUtilityTrueBusinessHoursDiff
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityTrueBusinessHoursDiff
* SYSTEM:       LYNX Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the number of true business hours between two dates.  Note that either date may be a weekend date.
*
* PARAMETERS:
* (I) @BeginDate       The beginning date
* (I) @EndDate         The ending date
*
* RESULT:  INT   The number of hours between the two dates
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityTrueBusinessHoursDiff( @begin_date datetime, @end_date datetime )
RETURNS int

AS
BEGIN
   DECLARE @begin_date_part   varchar(10)
   DECLARE @end_date_part     varchar(10)
   DECLARE @bus_hours         int
   DECLARE @bus_hours_1st     int
   DECLARE @bus_hours_end     int
   DECLARE @days_diff         int
   DECLARE @full_week_bus_hours  int
   DECLARE @day_index         int

   -- set @begin_date = '4/8/2009 15:30'
   -- set @end_date = '4/10/2009 6:30'
   
   SET @bus_hours = 0

   -- -------------------------------------------------------------------------
   -- If either Begin or End Date are on Sunday, then set them to the next
   -- business day (Monday) at 8am. (Note: .33333 = 8am)
   -- -------------------------------------------------------------------------

   IF DATEPART(weekday, @begin_date) = 1 
   begin
      SET @begin_date = CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, @begin_date), 101) + ' 8:00')
   end

   IF DATEPART(weekday, @end_date) = 1 
   begin
      SET @end_date = CONVERT(datetime, CONVERT(varchar, DATEADD(day, 1, @end_date), 101) + ' 8:00')
   end

   -- -------------------------------------------------------------------------
   -- If either Begin or End Date are on Saturday, then set them to the next
   -- business day (Monday).
   -- -------------------------------------------------------------------------
   IF DATEPART(weekday, @begin_date) = 7 
   begin
      SET @begin_date = CONVERT(datetime, CONVERT(varchar, DATEADD(day, 2, @begin_date), 101) + ' 8:00')
   end

   IF DATEPART(weekday, @end_date) = 7 
   begin
      SET @end_date = CONVERT(datetime, CONVERT(varchar, DATEADD(day, 2, @end_date), 101) + ' 8:00')
   end

   SET @begin_date_part = CONVERT(varchar, @begin_date, 101)
   SET @end_date_part = CONVERT(varchar, @end_date, 101)

   -- -------------------------------------------------------------------------
   -- Calculate the Number of Calendar Days between the dates.
   -- -------------------------------------------------------------------------
   IF convert(varchar, @begin_date, 101) = convert(varchar, @end_date, 101)
   BEGIN
      SET @bus_hours = DATEDIFF(hour, @begin_date, @end_date)
   END
   ELSE
   BEGIN
	   -- ---------------------------------------------------------------------
	   -- Calculate the Number of Business Hours on the Begin Day based on the
	   -- Begin Time and 5PM.  If the Begin Time is before 8AM then
	   -- just set the # of Hours = 9.  If Begin Date and Time is after 
	   -- 5PM then set # of Hours = 0.
	   -- ---------------------------------------------------------------------
      IF @begin_date > convert(datetime, @begin_date_part + ' 17:00')
      BEGIN
         SET @bus_hours_1st = 0
      END
      ELSE
      BEGIN
         IF @begin_date < convert(datetime, @begin_date_part + ' 8:00')
         BEGIN
            SET @bus_hours_1st = 9
         END
         ELSE
         BEGIN
            SET @bus_hours_1st = DATEDIFF(hour, @begin_date, convert(datetime, @begin_date_part + ' 17:00'))
         END
      END
	   -- ---------------------------------------------------------------------
	   -- Calculate the Number of Business Hours on the End Day:
	   -- If End Date and Time is before 8AM then set # of Hours = 1.
	   -- ---------------------------------------------------------------------
      IF @end_date < convert(datetime, @end_date_part + ' 8:00')
      BEGIN
         SET @bus_hours_end = 0
      END
      ELSE
      BEGIN
         IF @end_date > convert(datetime, @end_date_part + ' 17:00')
         BEGIN
            SET @bus_hours_end = 9
         END
         ELSE
         BEGIN
            SET @bus_hours_end = DATEDIFF(hour, convert(datetime, @end_date_part + ' 8:00'), @end_date)
         END
      END

	   -- ---------------------------------------------------------------------
	   -- Now Calculate the Number of Days between the Begin and End Date:
	   -- ---------------------------------------------------------------------
      SET @days_diff = DATEDIFF(day, @begin_date, @end_date) + 1

	   -- --------------------------------------------------------------------------
	   -- Now Calculate the Number of Business hours between the Begin and End Date (excluding them):
	   -- --------------------------------------------------------------------------
      SET @day_index = 1
      SET @full_week_bus_hours = 0
      WHILE @day_index < (@days_diff - 1)
      BEGIN
         IF DATEPART(weekday, DATEADD(day, @day_index, @begin_date)) between 1 and 5
         BEGIN
            SET @full_week_bus_hours = @full_week_bus_hours + 9
         END

         SET @day_index = @day_index + 1
      END

      SET @bus_hours = @bus_hours_1st + @full_week_bus_hours + @bus_hours_end
   END

   RETURN @bus_hours
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityTrueBusinessHoursDiff' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityTrueBusinessHoursDiff TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the function

    RAISERROR ('Function creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/ 
