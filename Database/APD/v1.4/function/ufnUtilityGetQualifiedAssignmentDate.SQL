-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetQualifiedAssignmentDate' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetQualifiedAssignmentDate 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetQualifiedAssignmentDate
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Dan Price
* FUNCTION:     Determine if an assignment has not been cancelled and the status indicates the assignment is valid
*
* PARAMETERS:  
* @AssignmentDate           The Pertains To string
*
* RETURNS:
*       AssignmentDate or null
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityGetQualifiedAssignmentDate 
( 
  @AssignmentID as bigint
)

RETURNS datetime

AS

BEGIN
    -- Declare internal variables
    DECLARE @AssignmentDate       as datetime
    DECLARE @ClaimAspectNumber    as int
    DECLARE @LynxID               as bigint
    DECLARE @RoutingCD            as varchar(4)
    DECLARE @Status               as smallint
    DECLARE @ClaimAspectID        as bigint
    DECLARE @CancellationDate     as datetime


    SELECT  @AssignmentDate = a.AssignmentDate, @ClaimAspectID = casc.ClaimAspectID, @CancellationDate = a.CancellationDate 
      FROM  dbo.utb_assignment a
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added utb_Claim_Aspect_Service_Channel to get the ClaimAspectID
		M.A. 20061113
	*********************************************************************************/
      INNER JOIN utb_Claim_Aspect_Service_Channel casc
      ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
      WHERE AssignmentID = @AssignmentID

    IF (@AssignmentDate IS NULL) RETURN @AssignmentDate   
    
    
    IF (@CancellationDate IS NOT NULL)
    BEGIN
      -- If there are Reinspections attached to the ClaimAspect always return the Assignment Date regardless of the assignment's Status
      IF ((SELECT COUNT(*) 
           FROM dbo.utb_reinspect 
           WHERE ClaimAspectID = @ClaimAspectID and EnabledFlag = 1) > 0) 
      BEGIN
        RETURN @AssignmentDate
      END
      ELSE
      BEGIN
        Return NULL
      END
    END
    
    -- Extract LynxID and Vehicle Number from ClaimAspectID
    SELECT  @LynxID = LynxID,
            @ClaimAspectNumber = ca.ClaimAspectNumber
      FROM  dbo.utb_assignment A 
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added utb_Claim_Aspect_Service_Channel to join utb_Assignment to 
		utb_Claim_Aspect table
		M.A. 20061113
	*********************************************************************************/
      INNER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
      left join dbo.utb_claim_aspect CA on casc.ClaimAspectID = CA.ClaimAspectID
      Where AssignmentID = @AssignmentID
            
      
    -- Check electronic assignment status
          
    SELECT  @Status = cas.StatusID 
      FROM  dbo.utb_claim_aspect  ca
	  INNER JOIN utb_Claim_Aspect_Status cas
	  ON ca.ClaimAspectID = cas.ClaimAspectID
     WHERE  LynxID = @LynxID
       AND  ClaimAspectNumber = @ClaimAspectNumber
       AND  ClaimAspectTypeID = (SELECT  ClaimAspectTypeID 
                                   FROM  dbo.utb_claim_aspect_type
                                   WHERE Name = 'Assignment')
    
    
    IF (@Status IN (13,14)) RETURN @AssignmentDate    -- Assignment was Sent or Received By Shop


    -- Check fax assignment status

    SELECT  @Status = cas.StatusID 
      FROM  dbo.utb_claim_aspect ca
	  INNER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
     WHERE  LynxID = @LynxID
       AND  ClaimAspectNumber = @ClaimAspectNumber
       AND  ClaimAspectTypeID = (SELECT  ClaimAspectTypeID 
                                   FROM  dbo.utb_claim_aspect_type
                                   WHERE Name = 'Fax Assignment')
        

    IF (@Status = 24) RETURN @AssignmentDate    -- Assignment was Faxed

    SET @AssignmentDate = NULL
    RETURN @AssignmentDate      -- Electronic Assignment was not Sent or Received By Shop and Fax Assignment was not Sent
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetQualifiedAssignmentDate' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityGetQualifiedAssignmentDate TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityGetQualifiedAssignmentDate' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityGetQualifiedAssignmentDate TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/