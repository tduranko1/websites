-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnTaskGetEscalationDate' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnTaskGetEscalationDate 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnTaskGetEscalationDate
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the admin fee settings for the ClaimAspectServiceChannelID and ClientFeeID
*
* PARAMETERS:  
* @ClaimAspectServiceChannelID  The claim aspect service channel id
* @ClientFeeID                  The client fee id being applied
*
* RETURNS:  Recordset { AdminFeeCD, AdminFeeAmt }
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnTaskGetEscalationDate ( @TaskID int, @InsuranceCompanyID int, @ServiceChannelCD varchar(2), @initialDate datetime)
RETURNS datetime
AS
BEGIN

    DECLARE @EscalationDateTime as datetime
    DECLARE @EscalationMinutes as int

    SELECT @EscalationMinutes = dbo.ufnTaskGetEscalationMinutes(@TaskID, @InsuranceCompanyID, @ServiceChannelCD)

    IF @EscalationMinutes IS NOT NULL AND @initialDate is not NULL
    BEGIN
        SET @EscalationDateTime = DATEADD(minute, @EscalationMinutes, @initialDate)
    END
    
    RETURN @EscalationDateTime
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnTaskGetEscalationDate' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnTaskGetEscalationDate TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnTaskGetEscalationDate' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnTaskGetEscalationDate TO 
            ugr_lynxapd, ugr_fnolload

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/