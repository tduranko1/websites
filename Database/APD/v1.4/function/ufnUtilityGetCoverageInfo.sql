-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetCoverageInfo' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetCoverageInfo 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetCoverageInfo
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Converts a "PertainsTo" string into a Claim Aspect Type ID and Claim Aspect Number
*
* PARAMETERS:  
* @PertainsTo           The Pertains To string
*
* RETURNS:
*       AspectTypeID,
*       AspectNumber
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityGetCoverageInfo ( @LynxID  udt_std_id_big, @CoverageProfileCD udt_std_cd, @Property varchar(10) )
RETURNS varchar
AS
BEGIN
    -- Declare internal variables   
   
   DECLARE @Result                      varchar(15)
   DECLARE @LimitAmt                    udt_std_money
   DECLARE @DeductibleAmt               udt_std_money
   DECLARE @Days                        udt_std_money
   
   SELECT TOP 1 @LimitAmt = LimitAmt,
                @DeductibleAmt = DeductibleAmt,
                @Days          = MaximumDays
   FROM dbo.utb_claim_coverage
   WHERE LynxID = @LynxID
     AND CoverageTypeCD = @CoverageProfileCD
     AND ((@CoverageProfileCD <> 'RENT' AND AddtlCoverageFlag = 0) OR
          (@CoverageProfileCD = 'RENT'))


  SELECT @Result = CASE @Property WHEN 'Limit' THEN CONVERT(varchar,@LimitAmt)
                                  WHEN 'Deductible' THEN CONVERT(varchar,@DeductibleAmt)
                                  WHEN 'Days' THEN CONVERT(varchar,@Days)
                                  ELSE NULL END
                                  
 
  
  RETURN @Result
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetCoverageInfo' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityGetCoverageInfo TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityGetCoverageInfo' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityGetCoverageInfo TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/