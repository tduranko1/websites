-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityWillCloseClaim' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityWillCloseClaim 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityWillCloseClaim
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Converts a "PertainsTo" string into a Claim Aspect Type ID and Claim Aspect Number
*
* PARAMETERS:  
* @PertainsTo           The Pertains To string
*
* RETURNS:
*       AspectTypeID,
*       AspectNumber
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityWillCloseClaim ( @ClaimAspectID  udt_std_id_big )
RETURNS udt_std_flag
AS
BEGIN
    -- Declare internal variables
   DECLARE @LynxID                      udt_std_id_big
   DECLARE @ClaimAspectTypeIDVehicle    udt_std_id
   DECLARE @Result                      udt_std_flag
   
   
   SELECT @LynxID = LynxID
   FROM dbo.utb_claim_aspect
   WHERE ClaimAspectID = @ClaimAspectID
   
   
   SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
   FROM dbo.utb_claim_aspect_type
   where Name = 'Vehicle'
   
   SET @Result = 0
   IF NOT EXISTS (SELECT cas.StatusID
                  FROM utb_claim_aspect ca
                  INNER JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
                  INNER JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID and ca.ClaimAspectTypeID = s.ClaimAspectTypeID)
                  WHERE ca.LynxID = @LynxID
                    AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
                    AND ca.ClaimAspectID <> @ClaimAspectID
                    AND s.StatusTypeCD IS NULL
                    AND s.ServiceChannelCD IS NULL
                    AND s.Name NOT IN ('Vehicle Closed','Vehicle Cancelled', 'Vehicle Voided'))
   BEGIN
    -- Did not find a status ID for Claim Aspect thats not a closed/cancelled/or voided status 
    -- Claim will close
      SET @Result = 1
   END

   RETURN @Result   
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityWillCloseClaim' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityWillCloseClaim TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityWillCloseClaim' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityWillCloseClaim TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/