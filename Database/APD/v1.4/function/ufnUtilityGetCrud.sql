-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetCrud' AND type = 'TF')
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetCrud 
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetCrud
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Returns properly analyzed Crud information for user/entity combination 
*
* PARAMETERS:  
* (I) @UserID               The user ID 
* (I) @Entity               The entity
*
* RESULT SET:
* Table containing one row of fields:
*       CreateFlag
*       DeleteFlag
*       ReadFlag
*       UpdateFlag
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityGetCrud( @UserId udt_std_id, @Entity varchar(50) = NULL, @PermissionID udt_std_id = 0, @IgnoreOverride udt_std_flag = 0 )
RETURNS @CrudTable TABLE
(
    CreateFlag    tinyint,
    ReadFlag      tinyint,
    UpdateFlag    tinyint,
    DeleteFlag    tinyint
)
AS
BEGIN
    
    -- Declare variables

    DECLARE @PermissionIDWork   udt_std_int
    DECLARE @CreateFlag         udt_std_int_tiny
    DECLARE @ReadFlag           udt_std_int_tiny
    DECLARE @UpdateFlag         udt_std_int_tiny
    DECLARE @DeleteFlag         udt_std_int_tiny


    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Entity))) = 0 SET @Entity = NULL


    -- Figure out Permission ID to use
    IF @Entity IS NOT NULL
    BEGIN
        -- Look up Permission Name

        SELECT  @PermissionIdWork = PermissionId
          FROM  dbo.utb_permission WITH (NOLOCK)
          WHERE Name = @Entity
            AND EnabledFlag = 1
    END
    ELSE
    BEGIN
        -- @Entity was not passed, try @PermissionID parameter

        SELECT  @PermissionIdWork = PermissionId
          FROM  dbo.utb_permission WITH (NOLOCK)
          WHERE PermissionID = @PermissionID
            AND EnabledFlag = 1
    END


    -- First check to verify if user id and entity exists
    IF ((SELECT UserId FROM utb_user WITH (NOLOCK) WHERE UserId = @UserId) IS NULL) OR (@PermissionIDWork IS NULL)
    BEGIN

        -- If not, simply return false for all permissions
        INSERT INTO @CrudTable
        VALUES (0, 0, 0, 0)
    END
    ELSE
    BEGIN

        -- Get the Flags
        -- Check for user override first
        IF @IgnoreOverride = 0  
             AND EXISTS(SELECT  up.PermissionID 
                          FROM  dbo.utb_user_permission up WITH (NOLOCK)
                          WHERE up.PermissionID = @PermissionIdWork
                            AND up.UserID = @UserID)
        BEGIN
            SELECT  @CreateFlag = up.CreateFlag,
                    @ReadFlag   = up.ReadFlag,
                    @UpdateFlag = up.UpdateFlag,
                    @DeleteFlag = up.DeleteFlag
              FROM  dbo.utb_user_permission up WITH (NOLOCK)
              WHERE up.PermissionID = @PermissionIdWork
                AND up.UserID = @UserID
        END
        ELSE
        BEGIN
            -- No user override, reconcile permissions using optimistic model.  The code below selects the top
            -- value from all records in utb_role_permission that matches the roles the the user belongs to.
            -- The top value is used, which is defined as the highest numerical value, in this case a 1.  The
            -- effect this has is that if Role A Allow Read but not delete, while Role B allows update, then the
            -- net result will be Read and Update access.
            SELECT TOP 1 @CreateFlag = rp.CreateFlag
              FROM  dbo.utb_role_permission rp WITH (NOLOCK)
              WHERE rp.PermissionID = @PermissionIdWork
                AND rp.RoleID IN (SELECT  RoleID 
                                    FROM  dbo.utb_user_role WITH (NOLOCK)
                                    WHERE UserID = @UserID)
              ORDER BY rp.CreateFlag DESC


            SELECT TOP 1 @ReadFlag = rp.ReadFlag
              FROM  dbo.utb_role_permission rp WITH (NOLOCK)
              WHERE rp.PermissionID = @PermissionIdWork
                AND rp.RoleID IN (SELECT  RoleID 
                                    FROM  dbo.utb_user_role WITH (NOLOCK)
                                    WHERE UserID = @UserID)
              ORDER BY rp.ReadFlag DESC


            SELECT TOP 1 @UpdateFlag = rp.UpdateFlag
              FROM  dbo.utb_role_permission rp WITH (NOLOCK)
              WHERE rp.PermissionID = @PermissionIdWork
                AND rp.RoleID IN (SELECT  RoleID 
                                    FROM  dbo.utb_user_role WITH (NOLOCK)
                                    WHERE UserID = @UserID)
              ORDER BY rp.UpdateFlag DESC


            SELECT TOP 1 @DeleteFlag = rp.DeleteFlag
              FROM  dbo.utb_role_permission rp WITH (NOLOCK)
              WHERE rp.PermissionID = @PermissionIdWork
                AND rp.RoleID IN (SELECT  RoleID 
                                    FROM  dbo.utb_user_role WITH (NOLOCK)
                                    WHERE UserID = @UserID)
              ORDER BY rp.DeleteFlag DESC
        END

        
        -- Doublecheck to make sure we don't have any null values.  NULLS are invalid.  If any are found, 
        -- look up the corresponding default value for the permission

        IF @CreateFlag IS NULL
        BEGIN
            SELECT  @CreateFlag = DefaultCreateFlag
              FROM  dbo.utb_permission WITH (NOLOCK)
              WHERE PermissionID = @PermissionIDWork
        END

        IF @ReadFlag IS NULL
        BEGIN
            SELECT  @ReadFlag = DefaultReadFlag
              FROM  dbo.utb_permission WITH (NOLOCK)
              WHERE PermissionID = @PermissionIDWork
        END

        IF @UpdateFlag IS NULL
        BEGIN
            SELECT  @UpdateFlag = DefaultUpdateFlag
              FROM  dbo.utb_permission WITH (NOLOCK)
              WHERE PermissionID = @PermissionIDWork
        END

        IF @DeleteFlag IS NULL
        BEGIN
            SELECT  @DeleteFlag = DefaultDeleteFlag
              FROM  dbo.utb_permission WITH (NOLOCK)
              WHERE PermissionID = @PermissionIDWork
        END

        
        -- Do some hierarchial stuff.  This code addresses absurd combinations of permissions (such as Delete
        -- without Read), that are unlikely to be used.  These rules allow APD to make some assumptions about 
        -- what permissions to data are allowed.  The rules are as follow:

        -- 1) If Create, Update, or Delete is permitted, then Read is also permitted
        -- 2) If Create is permitted, then update is permitted
        -- 3) If update is not permitted, do not allow deletes

        -- IF CREATE OR UPDATE OR DELETE ALLOWED, READ MUST BE ALLOWED IN FIRST PLACE.
        IF (@CreateFlag = 1) OR (@UpdateFlag = 1) OR (@DeleteFlag = 1)
        BEGIN
            SET @ReadFlag = 1

            IF @CreateFlag = 1 -- IF CREATE, ALLOW UPDATE
            BEGIN
                SET @UpdateFlag = 1
            END
        END

        IF @UpdateFlag = 0 -- IF NO UPDATE, DO NOT ALLOW DELETE
        BEGIN
            SET @DeleteFlag = 0
        END


        -- Save the resultant values

        INSERT INTO @CrudTable
        (CreateFlag, ReadFlag, UpdateFlag, DeleteFlag)
        VALUES
        (@CreateFlag, @ReadFlag, @UpdateFlag, @DeleteFlag)
    END

    RETURN
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetCrud' AND type = 'TF')
BEGIN
    GRANT SELECT ON dbo.ufnUtilityGetCrud TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the function

    RAISERROR ('Function creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/