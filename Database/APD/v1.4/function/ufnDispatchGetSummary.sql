-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnDispatchGetSummary' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnDispatchGetSummary 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnDispatchGetSummary
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the dispatch summary details
*
* PARAMETERS:  
* @DispatchNumber  The dispatch number you want the summary for
*
* RETURNS:  Recordset 
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnDispatchGetSummary ( @DispatchNumber varchar(50) )
RETURNS @DispatchSummary TABLE
    (
        TotalRequestedAmount     money,
        TotalReleasedAmount      money
    )
AS
BEGIN

    DECLARE @OldDispatchNumber as varchar(50)
    DECLARE @NewDispatchNumber as varchar(50)
    DECLARE @TotalRequestAmount as decimal(9, 2)
    DECLARE @TotalReleaseAmount as decimal(9, 2)
    DECLARE @LoopCountMax as int

    DECLARE @tmpDispatchInfo TABLE (
       TransactionID          bigint         NOT NULL,
       Amount                 decimal(9, 2)  NOT NULL,
       DispatchNumber         varchar(50)    NOT NULL,
       DisptachNumberNew      varchar(50)    NULL,
       TransactionCD          varchar(4)     NOT NULL)
       
    INSERT INTO @tmpDispatchInfo 
    SELECT  TransactionID,
            Amount,
            id.DispatchNumber,
            DisptachNumberNew,
            TransactionCD
    FROM dbo.utb_invoice_dispatch id
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'TransactionCD') tc ON id.TransactionCD = tc.Code
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'DisbursementMethodCD') dc ON id.DisbursementMethodCD = dc.Code
    WHERE (id.DispatchNumber = @DispatchNumber
        OR id.DisptachNumberNew = @DispatchNumber)
    ORDER BY TransactionID DESC

    SELECT TOP 1 @OldDispatchNumber = DispatchNumber,
                 @NewDispatchNumber = DisptachNumberNew
    FROM @tmpDispatchInfo
    WHERE DisptachNumberNew IS NOT NULL
    
    SET @LoopCountMax = 1000 -- Prevent endless loop
    
    WHILE @OldDispatchNumber IS NOT NULL AND @LoopCountMax > 0
    BEGIN
    
       INSERT INTO @tmpDispatchInfo 
       SELECT  TransactionID,
               Amount,
               id.DispatchNumber,
               DisptachNumberNew,
               TransactionCD
       FROM dbo.utb_invoice_dispatch id
       LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'TransactionCD') tc ON id.TransactionCD = tc.Code
       LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_invoice_dispatch', 'DisbursementMethodCD') dc ON id.DisbursementMethodCD = dc.Code
       WHERE DispatchNumber = @OldDispatchNumber
       ORDER BY TransactionID DESC
       
       UPDATE @tmpDispatchInfo
       SET DisptachNumberNew = NULL
       WHERE DispatchNumber = @OldDispatchNumber
         AND DisptachNumberNew = @NewDispatchNumber
       
       SET @OldDispatchNumber = NULL
       SET @NewDispatchNumber = NULL
      
       SELECT TOP 1 @OldDispatchNumber = DispatchNumber,
                    @NewDispatchNumber = DisptachNumberNew
       FROM @tmpDispatchInfo
       WHERE DisptachNumberNew IS NOT NULL
       
       SET @LoopCountMax = @LoopCountMax - 1
      
    END    

    SELECT @TotalRequestAmount = SUM(Amount)
    FROM @tmpDispatchInfo
    WHERE TransactionCD = 'Q'
    
    SELECT @TotalReleaseAmount = SUM(Amount)
    FROM @tmpDispatchInfo
    WHERE TransactionCD = 'R'
    
    INSERT INTO @DispatchSummary
    SELECT @TotalRequestAmount, @TotalReleaseAmount
    
    RETURN
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnDispatchGetSummary' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnDispatchGetSummary TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnDispatchGetSummary' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnDispatchGetSummary TO 
            ugr_lynxapd, ugr_fnolload

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/