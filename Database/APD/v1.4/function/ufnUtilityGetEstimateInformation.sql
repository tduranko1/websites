-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetEstimateInformation' AND type = 'TF')
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetEstimateInformation 
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetEstimateInformation
* SYSTEM:       Lynx Services APD
* AUTHOR:       M. Ahmed
* FUNCTION:     Returns the original, the current or the final estimate data for all service channels defined in a claim aspect
*
* PARAMETERS:  
* (I) @ClaimAspectID(BigInt)        The Claim Aspect ID 
* (I) @EstimateType(char(01))       The estimate type is a code.  The valid values are given below:
*                                        C = Current Estimate
*                                        F = Final Estimate
*                                        O = Original
* (I) @EstimateSummaryType
* (I) @EstimateSummaryCategoryCD
*
*
*
* RESULT SET:
* Table containing one or no row comprised of the following::
*       ClaimAspectID
*       ClaimAspectServiceChannelID
*       ServiceChannelCD
*       --utb_Document columns (05)
*       DocumentID
*       Createddate
*       FinalEstimateFlag
*       EstimateTypeCD
*       SupplementSeqNumber
*       --utb_Document_Type (02)
*       DocumentTypeID
*       DocumentTypeIDDesc
*       --utb_Estimate columns (62)
*       AppraiserName
*       AppraiserLicenseNumber
*       ClaimantName
*       ClaimNumber
*       CompanyAddress
*       CompanyAreaCode
*       CompanyCity
*       CompanyName
*       CompanyExchangeNumber
*       CompanyState
*       CompanyUnitNumber
*       CompanyZip
*       DetailEditableFlag
*       InspectionAddress
*       InspectionAreaCode
*       InspectionCity
*       InspectionExchangeNumber
*       InspectionLocation
*       InspectionState
*       InspectionType
*       InspectionUnitNumber
*       InspectionZip
*       InsuranceCompanyName
*       InsuredName
*       LossDate
*       LossType
*       OwnerAddress
*       OwnerCity
*       OwnerDayAreaCode
*       OwnerDayExchangeNumber
*       OwnerDayExtensionNumber
*       OwnerDayUnitNumber 
*       OwnerEveningAreaCode 
*       OwnerEveningExchangeNumber 
*       OwnerEveningExtensionNumber 
*       OwnerEveningUnitNumber 
*       OwnerName                                          
*       OwnerState 
*       OwnerZip 
*       PolicyNumber         
*       Remarks
*       RepairDays 
*       ShopAddress                                        
*       ShopAreaCode 
*       ShopCity                       
*       ShopExchangeNumber 
*       ShopName                                           
*       ShopRegistrationNumber                             
*       ShopState 
*       ShopUnitNumber 
*       ShopZip  
*       VehicleBodyStyle                                   
*       VehicleColor         
*       VehicleEngineType    
*       VehicleImpactPoint
*       VehicleMake                                        
*       VehicleMileage 
*       VehicleModel                                       
*       VehicleOptions
*       VehicleYear 
*       VIN               
*       WrittenDate
        --utb_Estimate_Summary columns (15)
*       EstimateSummaryID
*       AgreedExtendedAmt     
*       AgreedHrs   
*       AgreedPct   
*       AgreedTaxableFlag 
*       AgreedUnitAmt         
*       Comments
*       OriginalExtendedAmt
*       OriginalHrs
*       OriginalHrsRepair 
*       OriginalHrsReplace 
*       OriginalPct 
*       OriginalTaxableFlag 
*       OriginalUnitAmt       
*       TaxTypeCD
*       --Estimaate_Summary_Type Columns (05)
*       EstimateSummaryTypeID 
*       CategoryCD 
*       DisplayOrder 
*       EnabledFlag 
*       Name
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function

Create function dbo.ufnUtilityGetEstimateInformation 
            ( 
            @ClaimAspectID bigint,
            @EstimateType char(01),
            @EstimateSummaryType varchar(50) =null,
            @EstimateSummaryCategoryCD char(04)= null
            )
returns    @EstimateInfo table
    (
    ClaimAspectID bigint,
    ClaimAspectServiceChannelID bigint,
    ServiceChannelCD varchar(4),
    --*************************
    --utb_Document columns (05)
    --*************************
    DocumentID bigint,
    Createddate datetime,
    FinalEstimateFlag bit,
    EstimateTypeCD varchar(04) NULL,
    SupplementSeqNumber tinyint NULL,
    --******************************
    --utb_Document_Type columns (02)
    --******************************
    DocumentTypeID tinyint,
    DocumentTypeIDDesc varchar(50),
    --*************************
    --utb_Estimate columns (62)
    --*************************
    AppraiserName varchar(50) Null,
    AppraiserLicenseNumber varchar(50) null,
    ClaimantName varchar(50) null,
    ClaimNumber varchar(30) NULL,
    CompanyAddress varchar(50) null,
    CompanyAreaCode varchar(03) null,
    CompanyCity varchar(30) null,
    CompanyName varchar(50) null,
    CompanyExchangeNumber varchar(03) null,
    CompanyState varchar(02) null,
    CompanyUnitNumber varchar(04) null,
    CompanyZip varchar(08) null,
    DetailEditableFlag bit,
    InspectionAddress varchar(50) null,
    InspectionAreaCode varchar(03) null,
    InspectionCity varchar(30) null,
    InspectionExchangeNumber varchar(03) null,
    InspectionLocation varchar(50) null,
    InspectionState varchar(02) null,
    InspectionType varchar(50) null,
    InspectionUnitNumber varchar(04) null,
    InspectionZip varchar(08) null,
    InsuranceCompanyName  varchar(50) null,
    InsuredName varchar(50) null,
    LossDate datetime null,
    LossType varchar(50) null,
    OwnerAddress varchar(50) null,
    OwnerCity varchar(30) null,
    OwnerDayAreaCode varchar(03) null,
    OwnerDayExchangeNumber varchar(03) null,
    OwnerDayExtensionNumber varchar(05) null,
    OwnerDayUnitNumber  varchar(04) null,
    OwnerEveningAreaCode varchar(03) null,
    OwnerEveningExchangeNumber varchar(03) null,
    OwnerEveningExtensionNumber varchar(05) null,
    OwnerEveningUnitNumber varchar(04) null,
    OwnerName varchar(50) null,
    OwnerState varchar(02) null,
    OwnerZip varchar(08) null,
    PolicyNumber varchar(20) null,
    Remarks varchar(1000) null,
    RepairDays smallint null,
    ShopAddress varchar(50) null,                          
    ShopAreaCode varchar(03) null,
    ShopCity varchar(30) null,
    ShopExchangeNumber varchar(03) null,
    ShopName varchar(50) null,
    ShopRegistrationNumber varchar(50) null,
    ShopState varchar(02) null,
    ShopUnitNumber varchar(04) null,
    ShopZip varchar(08) null,
    VehicleBodyStyle varchar(50) null,
    VehicleColor varchar(20) null,
    VehicleEngineType varchar(20) null,
    VehicleImpactPoint varchar(500) null,
    VehicleMake varchar(50) null,
    VehicleMileage decimal(9,2) null,
    VehicleModel varchar(50) null,
    VehicleOptions varchar(1000) null,
    VehicleYear smallint null,
    VIN varchar(17) null,
    WrittenDate datetime null,
    --**********************************
    --utb_Estimate_Summary columns (15)
    --**********************************
    EstimateSummaryID bigint,
    AgreedExtendedAmt money null,
    AgreedHrs decimal(9,1) null,
    AgreedPct decimal(9,5) null,
    AgreedTaxableFlag bit,
    AgreedUnitAmt money null,
    Comments varchar(250) null,
    OriginalExtendedAmt money null,
    OriginalHrs decimal(9,1) null,
    OriginalHrsRepair decimal(9,1) null,
    OriginalHrsReplace decimal(9,1) null,
    OriginalPct decimal(9,5) null,
    OriginalTaxableFlag bit,
    OriginalUnitAmt money,
    TaxTypeCD varchar(04) null,
    --***********************************
    --Estimaate_Summary_Type Columns (05)
    --***********************************
    EstimateSummaryTypeID smallint,
    CategoryCD varchar(04),
    DisplayOrder tinyint null,
    EnabledFlag bit,
    EstimateSummaryTypeIDDesc varchar(50)
    )


as
BEGIN

    --Internal variables
    Declare @FinalEstimateFlag bit
    Declare @DocID bigint


    declare  @tmp   table
        (
        RowID  int identity(1,1),
        ClaimAspectServiceChannelID bigint,
        DocumentID bigint
        )

    /*
        if the estimate type requested is not one of the following:
                  i)  O - Original estimate
                 ii)  C - Current estimate
                iii)  F - Final Estimate
                 iv)  A - Approved Estimate
                
        then return an empty table back to the caller
    */
    if    @EstimateType not in ('C', 'F', 'O', 'A')
        begin
            Return
        end
    
    /*
        If Original estimate or the Current Estimate is 
        being requested then set the @FinalEstimateFlag to NULL
    */
    if @EstimateType in ('O','C')
        begin
            set @FinalEstimateFlag = NULL
        end 
    
    /*
        If Final Estimate is being requested then 
        make sure that the @FinalEstimateFlag = 1
    */
    if @EstimateType = 'F'
        begin
            set @FinalEstimateFlag = 1
        end
    
    
    
    If    @EstimateType = 'O'--Return the original estimate information
    
        BEGIN
            --First Get the Service Channels & the Document IDs

             insert into  @tmp(ClaimAspectServiceChannelID,DocumentID)
             Select       casc.ClaimAspectServiceChannelID, 
                          d2.DocumentID

            from         (select @ClaimAspectID as 'ClaimAspectID') t     
            
            inner join    utb_Claim_Aspect ca
            on            t.ClaimAspectID = ca.ClaimAspectID
            
            
            inner join    utb_Claim_Aspect_Service_Channel casc
            on            ca.ClaimAspectID = casc.ClaimAspectID
            
            inner join    utb_Claim_Aspect_Service_Channel_Document d1
            on            casc.ClaimAspectServiceChannelID = d1.ClaimAspectServiceChannelID
            
            
            inner join    utb_Document d2
            on            d1.DocumentID = d2.DocumentID
            
            
            inner join    utb_document_type dt
            on            d2.DocumentTypeID = dt.DocumentTypeID
            
            inner join    utb_document_source s
            on            d2.DocumentSourceID = s.DocumentSourceID
            
            --inner join    utb_estimate e
            --on            d2.documentid = e.documentid

            --inner join    utb_estimate_summary es
            --on            d2.DocumentID = es.DocumentID

            --inner join    dbo.utb_estimate_summary_type est
            --on            es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
           
            where        (dt.Name in ('Estimate','Supplement') and dt.EnabledFlag = 1)
            and            d2.EnabledFlag = 1
            --and           (est.Name = @EstimateSummaryType or @EstimateSummaryType is null)
            --and           (est.CategoryCD = @EstimateSummaryCategoryCD or @EstimateSummaryCategoryCD is null)

            order by     s.VanFlag desc, d2.SupplementSeqNumber , d2.createddate


             Insert into @EstimateInfo
             select       ca.ClaimAspectID,
                          casc.ClaimAspectServiceChannelID,
                          casc.ServiceChannelCD,
                          d2.DocumentID,
                          d2.Createddate,
                          d2.FinalEstimateFlag,
                          d2.EstimateTypeCD,
                          d2.SupplementSeqNumber,
                          dt.DocumentTypeID,
                          dt.Name as 'DocumentTypeIDDesc',
                          --*************************
                          --utb_Estimate columns (62)
                          --*************************
                          e.AppraiserName,
                          e.AppraiserLicenseNumber,
                          e.ClaimantName,
                          e.ClaimNumber,
                          e.CompanyAddress,
                          e.CompanyAreaCode,
                          e.CompanyCity ,
                          e.CompanyName,
                          e.CompanyExchangeNumber,
                          e.CompanyState,
                          e.CompanyUnitNumber,
                          e.CompanyZip,
                          e.DetailEditableFlag ,
                          e.InspectionAddress,
                          e.InspectionAreaCode,
                          e.InspectionCity,
                          e.InspectionExchangeNumber ,
                          e.InspectionLocation,
                          e.InspectionState,
                          e.InspectionType ,
                          e.InspectionUnitNumber,
                          e.InspectionZip,
                          e.InsuranceCompanyName,
                          e.InsuredName,
                          e.LossDate,
                          e.LossType,
                          e.OwnerAddress,
                          e.OwnerCity ,
                          e.OwnerDayAreaCode,
                          e.OwnerDayExchangeNumber,
                          e.OwnerDayExtensionNumber,
                          e.OwnerDayUnitNumber,
                          e.OwnerEveningAreaCode,
                          e.OwnerEveningExchangeNumber,
                          e.OwnerEveningExtensionNumber,
                          e.OwnerEveningUnitNumber,
                          e.OwnerName,
                          e.OwnerState,
                          e.OwnerZip,
                          e.PolicyNumber,
                          e.Remarks,
                          e.RepairDays,
                          e.ShopAddress,
                          e.ShopAreaCode,
                          e.ShopCity,
                          e.ShopExchangeNumber,
                          e.ShopName,
                          e.ShopRegistrationNumber,
                          e.ShopState,
                          e.ShopUnitNumber,
                          e.ShopZip,
                          e.VehicleBodyStyle,
                          e.VehicleColor,
                          e.VehicleEngineType,
                          e.VehicleImpactPoint,
                          e.VehicleMake,
                          e.VehicleMileage,
                          e.VehicleModel,
                          e.VehicleOptions,
                          e.VehicleYear,
                          e.VIN ,
                          e.WrittenDate,
                          --**********************************
                          --utb_Estimate_Summary columns (15)
                          --**********************************
                          es.EstimateSummaryID,
                          es.AgreedExtendedAmt,
                          es.AgreedHrs,
                          es.AgreedPct,
                          es.AgreedTaxableFlag,
                          es.AgreedUnitAmt,
                          es.Comments,
                          es.OriginalExtendedAmt,
                          es.OriginalHrs,
                          es.OriginalHrsRepair,
                          es.OriginalHrsReplace,
                          es.OriginalPct,
                          es.OriginalTaxableFlag ,
                          es.OriginalUnitAmt,
                          es.TaxTypeCD,
                          --***********************************
                          --Estimaate_Summary_Type Columns (05)
                          --***********************************
                          est.EstimateSummaryTypeID,
                          est.CategoryCD,
                          est.DisplayOrder,
                          est.EnabledFlag,
                          est.Name
            
            from         (select @ClaimAspectID as 'ClaimAspectID') t     
            
            inner join    utb_Claim_Aspect ca
            on            t.ClaimAspectID = ca.ClaimAspectID
            
            
            inner join    utb_Claim_Aspect_Service_Channel casc
            on            ca.ClaimAspectID = casc.ClaimAspectID

            inner join    (
                            select      tt.ClaimAspectServiceChannelID, 
                                        tt.DocumentID
                            from        @tmp tt
                            inner join  (
                                        select ClaimAspectServiceChannelID, min(RowID) as rowid
                                        from @tmp
                                        group by ClaimAspectServiceChannelID
                                        )ttt
                            on          tt.RowID = ttt.RowID
                           ) zz
            on            zz.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
            
            inner join    utb_Claim_Aspect_Service_Channel_Document d1
            on            zz.ClaimAspectServiceChannelID = d1.ClaimAspectServiceChannelID
            and           zz.DocumentID = d1.DocumentID 
            
            
            inner join    utb_Document d2
            on            d1.DocumentID = d2.DocumentID
            
            
            inner join    utb_document_type dt
            on            d2.DocumentTypeID = dt.DocumentTypeID
            
            inner join    utb_document_source s
            on            d2.DocumentSourceID = s.DocumentSourceID
            
            Left outer join utb_estimate e
            on            d2.documentid = e.documentid

            inner join    utb_estimate_summary es
            on            d2.DocumentID = es.DocumentID

            inner join    dbo.utb_estimate_summary_type est
            on            es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
           
            where        (dt.Name in ('Estimate','Supplement') and dt.EnabledFlag = 1)
            and            d2.EnabledFlag = 1
            and           (est.Name = @EstimateSummaryType or @EstimateSummaryType is null)
            and           (est.CategoryCD = @EstimateSummaryCategoryCD or @EstimateSummaryCategoryCD is null)
            
            order by     s.VanFlag desc, d2.SupplementSeqNumber , d2.createddate
    
        END
    else    --Return the Current or the Final estimate information
        BEGIN
            IF @EstimateType = 'A'
            BEGIN

               INSERT INTO @tmp(ClaimAspectServiceChannelID,DocumentID)
               SELECT casc.ClaimAspectServiceChannelID, 
                      d.DocumentID
               FROM (select @ClaimAspectID as 'ClaimAspectID') t  
               LEFT JOIN utb_claim_aspect ca ON t.ClaimAspectID = ca.ClaimASpectID
               LEFT JOIN utb_Claim_Aspect_Service_Channel casc on ca.ClaimAspectID = casc.ClaimAspectID
               LEFT JOIN utb_Claim_Aspect_Service_Channel_Document cascd on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
               LEFT JOIN utb_Document d on  cascd.DocumentID = d.DocumentID
               WHERE ca.ClaimAspectID = @ClaimAspectID
                 AND d.ApprovedFlag = 1
                 AND d.EnabledFlag = 1
                 
            END
            ELSE
            BEGIN
               --First Get the Document ID
                insert into  @tmp(ClaimAspectServiceChannelID,DocumentID)
                Select       casc.ClaimAspectServiceChannelID, 
                             d2.DocumentID

               from         (select @ClaimAspectID as 'ClaimAspectID') t  
      
               inner join    utb_Claim_Aspect ca
               on            t.ClaimAspectID = ca.ClaimAspectID

               inner join    utb_Claim_Aspect_Service_Channel casc
               on            ca.ClaimAspectID = casc.ClaimAspectID

               inner join    utb_Claim_Aspect_Service_Channel_Document d1
               on            casc.ClaimAspectServiceChannelID = d1.ClaimAspectServiceChannelID

               inner join    utb_Document d2
               on            d1.DocumentID = d2.DocumentID

               inner join    utb_document_type dt
               on            d2.DocumentTypeID = dt.DocumentTypeID

               inner join    utb_document_source s
               on            d2.DocumentSourceID = s.DocumentSourceID

               --inner join    utb_estimate e
               --on            d2.documentid = e.documentid
               --inner join    utb_estimate_summary es
               --on            d2.DocumentID = es.DocumentID

               --inner join    dbo.utb_estimate_summary_type est
               --on            es.EstimateSummaryTypeID = est.EstimateSummaryTypeID

               where        (dt.Name in ('Estimate','Supplement') and dt.EnabledFlag = 1)
               and            d2.EnabledFlag = 1
               and          (d2.FinalEstimateFlag =@FinalEstimateFlag or @FinalEstimateFlag is null)
               --and           (est.Name = @EstimateSummaryType or @EstimateSummaryType is null)
               --and           (est.CategoryCD = @EstimateSummaryCategoryCD or @EstimateSummaryCategoryCD is null)

               order by     s.VanFlag desc, d2.SupplementSeqNumber desc, d2.createddate desc
            END

            --Now that we have documentid

             Insert into @EstimateInfo
             select       ca.ClaimAspectID,
                          casc.ClaimAspectServiceChannelID,
                          casc.ServiceChannelCD,
                          d2.DocumentID,
                          d2.Createddate,
                          d2.FinalEstimateFlag,
                          d2.EstimateTypeCD,
                          d2.SupplementSeqNumber,
                          dt.DocumentTypeID,
                          dt.Name as 'DocumentTypeIDDesc',
                          --*************************
                          --utb_Estimate columns (62)
                          --*************************
                          e.AppraiserName,
                          e.AppraiserLicenseNumber,
                          e.ClaimantName,
                          e.ClaimNumber,
                          e.CompanyAddress,
                          e.CompanyAreaCode,
                          e.CompanyCity ,
                          e.CompanyName,
                          e.CompanyExchangeNumber,
                          e.CompanyState,
                          e.CompanyUnitNumber,
                          e.CompanyZip,
                          e.DetailEditableFlag ,
                          e.InspectionAddress,
                          e.InspectionAreaCode,
                          e.InspectionCity,
                          e.InspectionExchangeNumber ,
                          e.InspectionLocation,
                          e.InspectionState,
                          e.InspectionType ,
                          e.InspectionUnitNumber,
                          e.InspectionZip,
                          e.InsuranceCompanyName,
                          e.InsuredName,
                          e.LossDate,
                          e.LossType,
                          e.OwnerAddress,
                          e.OwnerCity ,
                          e.OwnerDayAreaCode,
                          e.OwnerDayExchangeNumber,
                          e.OwnerDayExtensionNumber,
                          e.OwnerDayUnitNumber,
                          e.OwnerEveningAreaCode,
                          e.OwnerEveningExchangeNumber,
                          e.OwnerEveningExtensionNumber,
                          e.OwnerEveningUnitNumber,
                          e.OwnerName,
                          e.OwnerState,
                          e.OwnerZip,
                          e.PolicyNumber,
                          e.Remarks,
                          e.RepairDays,
                          e.ShopAddress,
                          e.ShopAreaCode,
                          e.ShopCity,
                          e.ShopExchangeNumber,
                          e.ShopName,
                          e.ShopRegistrationNumber,
                          e.ShopState,
                          e.ShopUnitNumber,
                          e.ShopZip,
                          e.VehicleBodyStyle,
                          e.VehicleColor,
                          e.VehicleEngineType,
                          e.VehicleImpactPoint,
                          e.VehicleMake,
                          e.VehicleMileage,
                          e.VehicleModel,
                          e.VehicleOptions,
                          e.VehicleYear,
                          e.VIN ,
                          e.WrittenDate,
                          --**********************************
                          --utb_Estimate_Summary columns (15)
                          --**********************************
                          es.EstimateSummaryID,
                          es.AgreedExtendedAmt,
                          es.AgreedHrs,
                          es.AgreedPct,
                          es.AgreedTaxableFlag,
                          es.AgreedUnitAmt,
                          es.Comments,
                          es.OriginalExtendedAmt,
                          es.OriginalHrs,
                          es.OriginalHrsRepair,
                          es.OriginalHrsReplace,
                          es.OriginalPct,
                          es.OriginalTaxableFlag ,
                          es.OriginalUnitAmt,
                          es.TaxTypeCD,
                          --***********************************
                          --Estimaate_Summary_Type Columns (05)
                          --***********************************
                          est.EstimateSummaryTypeID,
                          est.CategoryCD,
                          est.DisplayOrder,
                          est.EnabledFlag,
                          est.Name
            
            from         (select @ClaimAspectID as 'ClaimAspectID') t     
            
            inner join    utb_Claim_Aspect ca
            on            t.ClaimAspectID = ca.ClaimAspectID
            
            
            inner join    utb_Claim_Aspect_Service_Channel casc
            on            ca.ClaimAspectID = casc.ClaimAspectID
            
            inner join    (
                            select      tt.ClaimAspectServiceChannelID, 
                                        tt.DocumentID
                            from        @tmp tt
                            inner join  (
                                        select ClaimAspectServiceChannelID, min(RowID) as rowid
                                        from @tmp
                                        group by ClaimAspectServiceChannelID
                                        )ttt
                            on          tt.RowID = ttt.RowID
                           ) zz
            on            zz.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
            
            inner join    utb_Claim_Aspect_Service_Channel_Document d1
            on            zz.ClaimAspectServiceChannelID = d1.ClaimAspectServiceChannelID
            and           zz.DocumentID = d1.DocumentID 
                        
            
            inner join    utb_Document d2
            on            d1.DocumentID = d2.DocumentID
            
            
            inner join    utb_document_type dt
            on            d2.DocumentTypeID = dt.DocumentTypeID
            
            inner join    utb_document_source s
            on            d2.DocumentSourceID = s.DocumentSourceID
            
            left outer join utb_estimate e
            on            d2.documentid = e.documentid

            inner join    utb_estimate_summary es
            on            d2.DocumentID = es.DocumentID

            inner join    dbo.utb_estimate_summary_type est
            on            es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
            
            where        (dt.Name in ('Estimate','Supplement') and dt.EnabledFlag = 1)
            and            d2.EnabledFlag = 1
            and          (d2.FinalEstimateFlag =@FinalEstimateFlag or @FinalEstimateFlag is null)
            and           (est.Name = @EstimateSummaryType or @EstimateSummaryType is null)
            and           (est.CategoryCD = @EstimateSummaryCategoryCD or @EstimateSummaryCategoryCD is null)
            
            order by     s.VanFlag desc, d2.SupplementSeqNumber desc, d2.createddate desc
        END

    Return
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetEstimateInformation' AND type = 'TF')
BEGIN
    GRANT SELECT ON dbo.ufnUtilityGetEstimateInformation TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the function

    RAISERROR ('Function creation failure.', 16, 1)
    ROLLBACK
END

GO
