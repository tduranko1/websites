-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetInsuredOwnerName' AND type = 'TF')
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetInsuredOwnerName 
END

GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetInsuredOwnerName
* SYSTEM:       Lynx Services ADP
* AUTHOR:       M. Ahmed
* FUNCTION:     Returns the insured party's information
*
* PARAMETERS:      @ClaimAspectID (any valid claim aspect id)
*                  @InsuredOwner (valid values are: INSURED or OWNER)
*  
*
* RETURNS:
*       InvolvedID            - the rowid in the table
*       InsuredName           - the name of the insured
*       NameTitle             - the insured's title
*       Address1              - the insured party's address - street name
*       Address2              - the insured party's address - street name
*       AddressCity           - the insured party's address - city
*       AddressState          - the insured party's address - state 
*       AddressZip            - the insured party's address - zip code
*       DayAreaCode           - the phone number
*       DayExchangeNumber     - the phone number
*       DayExtensionNumber    - the phone number 
*       DayUnitNumber         - the phone number 
*       FedTaxID              - the tax id
*       NightAreaCode         - the night phone number
*       NightExchangeNumber   - the night phone number
*       NightExtensionNumber  - the night phone number
*       NightUnitNumber       - the night phone number
*       AlternateAreaCode         - the alternate phone number
*       AlternateExchangeNumber   - the alternate phone number
*       AlternateExtensionNumber  - the alternate phone number
*       AlternateUnitNumber       - the alternate phone number
*       BestContactTime           - the best time to contact
*       BestContactPhoneCD        - the contact phone code
*       SysLastUpdatedDate        - the last time the row was updated
*
*
************************************************************************************************************************/

-- Create the function
CREATE FUNCTION dbo.ufnUtilityGetInsuredOwnerName ( @ClaimAspectID udt_std_id_big, @InsuredOwner varchar(10))
RETURNS @InfoTable TABLE
    (
      ClaimAspectID bigint,
      InvolvedID bigint,
      InsuredName varchar(70), 
      NameTitle varchar(15) NULL, 
      Address1 varchar(35) NULL, 
      Address2 varchar(35) NULL,
      AddressCity varchar(35) NULL, 
      AddressState varchar(15) NULL, 
      AddressZip varchar(09) NULL, 
      DayAreaCode varchar(05) NULL,
      DayExchangeNumber varchar(05) NULL, 
      DayExtensionNumber varchar(05) NULL, 
      DayUnitNumber varchar(05) NULL,
      FedTaxID varchar(10) NULL,
      NightAreaCode varchar(05) NULL,
      NightExchangeNumber varchar(05) NULL, 
      NightExtensionNumber varchar(05) NULL,
      NightUnitNumber varchar(05) NULL, 
      AlternateAreaCode varchar(05) NULL,
      AlternateExchangeNumber varchar(05) NULL,
      AlternateExtensionNumber varchar(05) NULL, 
      AlternateUnitNumber varchar(05) NULL,
      BestContactPhoneCD varchar(05) NULL,
      BestContactTime varchar(50) NULL,
      SysLastUpdatedDate datetime Null
    )

AS
BEGIN


    INSERT INTO @InfoTable    (
                      ClaimAspectID,
                      InvolvedID,
                      InsuredName,
                      NameTitle,
                      Address1,
                      Address2,
                      AddressCity,
                      AddressState, 
                      AddressZip,
                      DayAreaCode,
                      DayExchangeNumber,
                      DayExtensionNumber,
                      DayUnitNumber,
                      FedTaxID,
                      NightAreaCode,
                      NightExchangeNumber,
                      NightExtensionNumber,
                      NightUnitNumber,
                      AlternateAreaCode,
                      AlternateExchangeNumber,
                      AlternateExtensionNumber, 
                      AlternateUnitNumber,
                      BestContactPhoneCD,
                      BestContactTime,
                      SysLastUpdatedDate
                      )

    SELECT            ca.ClaimAspectID,
                      i.InvolvedID,
                      Isnull(i.BusinessName, i.NameFirst + ' ' + i.NameLast) as 'InsuredName', 
                      i.NameTitle, 
                      i.Address1, 
                      i.Address2,
                      i.AddressCity, 
                      i.AddressState, 
                      i.AddressZip, 
                      i.DayAreaCode,
                      i.DayExchangeNumber, 
                      i.DayExtensionNumber, 
                      i.DayUnitNumber,
                      i.FedTaxID,
                      i.NightAreaCode,
                      i.NightExchangeNumber, 
                      i.NightExtensionNumber,
                      i.NightUnitNumber, 
                      i.AlternateAreaCode,
                      i.AlternateExchangeNumber,
                      i.AlternateExtensionNumber, 
                      i.AlternateUnitNumber,
                      i.BestContactPhoneCD,
                      i.BestContactTime,
                      dbo.ufnUtilityGetDateString( i.SysLastUpdatedDate )
    
    FROM               (Select @ClaimAspectID as 'ClaimAspectID') t

    inner join         utb_claim_aspect ca
    on                 t.ClaimAspectID = ca.ClaimAspectID

    Left Outer Join    utb_claim_aspect_involved cai 
    on                 ca.ClaimAspectID = cai.ClaimAspectID
    
    Left Outer Join    utb_involved i 
    on                 cai.InvolvedID = i.InvolvedID
    
    Left Outer Join    utb_involved_role ir 
    on                 i.InvolvedID = ir.InvolvedID
    
    Left Outer Join    utb_involved_role_type irt 
    on                 ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
    
    WHERE             cai.EnabledFlag = 1 
    AND               irt.Name = @InsuredOwner --OR irt.Name IS NULL)
    

    Return

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetInsuredOwnerName' AND type = 'TF')
BEGIN
    GRANT SELECT ON dbo.ufnUtilityGetInsuredOwnerName TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the function

    RAISERROR ('Function creation failure.', 16, 1)
    ROLLBACK
END

GO