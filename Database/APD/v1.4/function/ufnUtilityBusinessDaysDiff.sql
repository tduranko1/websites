-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityBusinessDaysDiff' AND type = 'FN')
BEGIN
    DROP FUNCTION dbo.ufnUtilityBusinessDaysDiff
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityBusinessDaysDiff
* SYSTEM:       LYNX Services APD
* AUTHOR:       Steve Kearns
* FUNCTION:     Returns the number of business days between two dates.  Note that either date may be a weekend date.
*
* PARAMETERS:
* (I) @BeginDate       The beginning date
* (I) @EndDate         The ending date
*
* RESULT:  INT   The number of days between the two dates
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityBusinessDaysDiff( @BeginDate datetime, @EndDate datetime )
RETURNS int

AS
BEGIN
    DECLARE @days INT

    -- If either @BeginDate or @EndDate are on a weekend, reset them to the next business day (Monday)
    IF DATEPART(weekday, @BeginDate) = 7 SET @BeginDate = DateAdd(d, 2, @BeginDate)
    IF DATEPART(weekday, @BeginDate) = 1 SET @BeginDate = DateAdd(d, 1, @BeginDate)
    IF DATEPART(weekday, @EndDate) = 7 SET @EndDate = DateAdd(d, 2, @EndDate)
    IF DATEPART(weekday, @EndDate) = 1 SET @EndDate = DateAdd(d, 1, @EndDate)

    -- Compute actual number of days difference
    SET @days = DATEDIFF(day, @BeginDate, @EndDate)

    -- Subtract weekends
    SET @days = @days - (DATEDIFF(week, @BeginDate, @EndDate) * 2)

    -- Sanity check
    IF @days < 0 SET @days = 0

    RETURN @days

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityBusinessDaysDiff' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityBusinessDaysDiff TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the function

    RAISERROR ('Function creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/ 
