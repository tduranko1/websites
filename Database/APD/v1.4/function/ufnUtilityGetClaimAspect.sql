-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetClaimAspect' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnUtilityGetClaimAspect 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnUtilityGetClaimAspect
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Converts a "PertainsTo" string into a Claim Aspect Type ID and Claim Aspect Number
*
* PARAMETERS:  
* @PertainsTo           The Pertains To string
*
* RETURNS:
*       AspectTypeID,
*       AspectNumber
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnUtilityGetClaimAspect ( @PertainsTo varchar(8) )
RETURNS @ClaimAspect TABLE
(
    AspectTypeID       int  NOT NULL,
    AspectNumber       int  NOT NULL
)
AS
BEGIN
    -- Declare internal variables

    DECLARE @AspectTypeCode  AS varchar(3)
    DECLARE @AspectTypeID    AS udt_std_id
    DECLARE @EnumeratedFlag  AS udt_std_flag


    -- Validate Pertains to 

    SET @PertainsTo = LTRIM(RTRIM(@PertainsTo))

    IF Len(@PertainsTo) < 3
    BEGIN
        -- Invalid pertains to

        INSERT INTO @ClaimAspect VALUES (-1, 0)
        RETURN
    END


    IF (Len(@PertainsTo) > 3) AND
       (IsNumeric(Substring(@PertainsTo, 4, Len(@PertainsTo) - 3)) = 0)
    BEGIN
        -- Invalid pertains to

        INSERT INTO @ClaimAspect VALUES (-1, 0)
        RETURN
    END


    SET @AspectTypeCode = Left(@PertainsTo, 3)


    SELECT @AspectTypeID   = ClaimAspectTypeID,
           @EnumeratedFlag = EnumeratedFlag
    FROM   dbo.utb_claim_aspect_type
    WHERE  code = @AspectTypeCode

    IF @@ERROR <> 0
    BEGIN
        -- Invalid pertains to

        INSERT INTO @ClaimAspect VALUES (-1, 0)
        RETURN
    END

    
    IF @AspectTypeID IS NULL
    BEGIN
        -- Invalid pertains to

        INSERT INTO @ClaimAspect VALUES (-1, 0)
        RETURN
    END
    ELSE
    BEGIN
        -- We have a valid Pertains To

        INSERT INTO @ClaimAspect
        VALUES
        (
            @AspectTypeID,
            CASE 
                WHEN @EnumeratedFlag = 0 THEN 0
                WHEN @EnumeratedFlag = 1 AND Substring(@PertainsTo, 4, Len(@PertainsTo) - 3) = '' THEN 0
                ELSE Convert(int, Substring(@PertainsTo, 4, Len(@PertainsTo) - 3))
            END
        )
    END
    

    RETURN
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnUtilityGetClaimAspect' AND type = 'FN')
BEGIN
    GRANT EXECUTE ON dbo.ufnUtilityGetClaimAspect TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
    IF EXISTS (SELECT name FROM sysobjects 
                WHERE name = 'ufnUtilityGetClaimAspect' AND type = 'TF')
    BEGIN
        GRANT SELECT ON dbo.ufnUtilityGetClaimAspect TO 
            ugr_lynxapd

        -- Commit the transaction for dropping and creating the function

        COMMIT
    END
    ELSE
    BEGIN
         -- There was an error...rollback the transaction for dropping and creating the function

         RAISERROR ('Function creation failure.', 16, 1)
         ROLLBACK
    END
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/