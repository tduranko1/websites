-- Begin the transaction for dropping and creating the function

BEGIN TRANSACTION


-- If it already exists, drop the function

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnClaimAdminFee2ShopApplies' AND (type = 'FN' OR type = 'TF'))
BEGIN
    DROP FUNCTION dbo.ufnClaimAdminFee2ShopApplies 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    ufnClaimAdminFee2ShopApplies
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns a true or false if admin fee applies to the shop. 
*
* PARAMETERS:  
* @ClaimAspectServiceChannelID  The claim aspect service channel id
*
* RETURNS:  1 or 0
* 
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the function


CREATE FUNCTION dbo.ufnClaimAdminFee2ShopApplies ( @ClaimAspectServiceChannelID bigint )
RETURNS decimal(9, 2)
AS
BEGIN
    -- Declare internal variables
    DECLARE @AdminFeeAppliesFlag as bit
    DECLARE @ShopZipCode    as varchar(5)
    DECLARE @ShopZipMSA     as varchar(5)
    DECLARE @ShopState      as varchar(2)
    DECLARE @ProgramFlag    as bit
    DECLARE @AdminFeeAmount as decimal(9, 2)


    DECLARE @tmpAdminFees TABLE (
        AdminFeeID  bigint
    )

    -- Set the default    
    SET @AdminFeeAppliesFlag = 0
    SET @AdminFeeAmount = NULL

    -- Get the active assignment details
    SELECT @ShopZipCode = AddressZip,
           @ShopZipMSA = CountyType,
           @ShopState = AddressState,
           @ProgramFlag = ProgramFlag
    FROM utb_assignment a
    LEFT JOIN utb_shop_location sl on a.ShopLocationID = sl.ShopLocationID
    LEFT JOIN utb_zip_code zc on sl.AddressZip = zc.Zip
    WHERE a.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
      AND CancellationDate IS NULL

    -- Admin fee applies to program shops only
    IF @ProgramFlag = 1
    BEGIN
        -- Let us get all the admin fees associated with this client. Admin fees
        --  are applicable to Program Shop and Repair complete
        INSERT INTO @tmpAdminFees 
        SELECT af.AdminFeeID
        FROM utb_admin_fee af
        LEFT JOIN utb_client_fee cf on af.ClientFeeID = cf.ClientFeeID
        LEFT JOIN utb_client_fee_definition cfd on cf.ClientFeeID = cfd.ClientFeeID
        LEFT JOIN utb_claim c on cf.InsuranceCompanyID = c.InsuranceCompanyID
        LEFT JOIN utb_claim_aspect ca on c.Lynxid = ca.Lynxid
        LEFT JOIN utb_claim_aspect_service_channel casc on ca.ClaimAspectID = casc.ClaimAspectID
        LEFT JOIN utb_service s on cfd.ServiceID = s.ServiceID
        WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
          AND s.ServiceChannelCD = casc.ServiceChannelCD
          AND af.EnabledFlag = 1
          AND s.ServiceChannelCD = 'PS'
          AND s.DispositionTypeCD = 'RC'

        -- See if there exists a flat fee paid by the shop
        IF EXISTS(SELECT tmp.AdminFeeID
                    FROM @tmpAdminFees tmp
                    LEFT JOIN utb_admin_fee af on tmp.AdminFeeID = af.AdminFeeID
                    WHERE af.AdminFeeCD = 'S'
                      AND af.EnabledFlag = 1) 
        BEGIN
            SET @AdminFeeAppliesFlag = 1 

            SELECT @AdminFeeAmount = Amount
            FROM @tmpAdminFees tmp
            LEFT JOIN utb_admin_fee af on tmp.AdminFeeID = af.AdminFeeID
            WHERE af.AdminFeeCD = 'S'
              AND af.EnabledFlag = 1
        END

        -- See if there exists any fees paid by the shop and in the Shop State
        IF EXISTS(SELECT tmp.AdminFeeID
                    FROM @tmpAdminFees tmp
                    LEFT JOIN utb_admin_fee_state afs on tmp.AdminFeeID = afs.AdminFeeID
                    WHERE afs.StateCode = @ShopState
                      AND afs.EnabledFlag = 1
                      AND afs.AdminFeeCD = 'S')
        BEGIN
            SET @AdminFeeAppliesFlag = 1

            SELECT @AdminFeeAmount = Amount
            FROM @tmpAdminFees tmp
            LEFT JOIN utb_admin_fee_state afs on tmp.AdminFeeID = afs.AdminFeeID
            WHERE afs.StateCode = @ShopState
              AND afs.EnabledFlag = 1
              AND afs.AdminFeeCD = 'S'
        END

        -- See if there exists any fees paid by the shop and within the Shop MSA
        IF EXISTS(SELECT tmp.AdminFeeID
                    FROM @tmpAdminFees tmp
                    LEFT JOIN utb_admin_fee_msa afm on tmp.AdminFeeID = afm.AdminFeeID
                    WHERE afm.MSA = @ShopZipMSA
                      AND afm.EnabledFlag = 1
                      AND afm.AdminFeeCD = 'S')
        BEGIN
            SET @AdminFeeAppliesFlag = 1

            SELECT @AdminFeeAmount = Amount
            FROM @tmpAdminFees tmp
            LEFT JOIN utb_admin_fee_msa afm on tmp.AdminFeeID = afm.AdminFeeID
            WHERE afm.MSA = @ShopZipMSA
              AND afm.EnabledFlag = 1
              AND afm.AdminFeeCD = 'S'
        END

    END

    RETURN @AdminFeeAmount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'ufnClaimAdminFee2ShopApplies' AND (type = 'FN' OR type = 'TF'))
BEGIN
    GRANT EXECUTE ON dbo.ufnClaimAdminFee2ShopApplies TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the function

    COMMIT
END
ELSE
BEGIN
     -- There was an error...rollback the transaction for dropping and creating the function

     RAISERROR ('Function creation failure.', 16, 1)
     ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/