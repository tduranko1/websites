/******************************************************************************
* Document Bundling options for QBE (new DRP 10/27/2010)
******************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime
DECLARE @BundlingID as int
DECLARE @MessageTemplateID as int
DECLARE @DocumentTypeID as int
DECLARE @DocumentDisplayOrder as int

SET @now = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'QBE Insurance'

IF @InsuranceCompanyID IS NOT NULL
BEGIN
	BEGIN TRANSACTION
   
	-- DELETE PREVIOUS CLIENT BUNDLING (QBE PS)
	DELETE FROM utb_client_bundling
	WHERE InsuranceCompanyID = @InsuranceCompanyID
		AND ServiceChannelCD = 'PS'	

	/* -------------------------------------------------- */
		-- 1.  Create QBE Acknowledgement of Claim bundle
	/* -------------------------------------------------- */
   
   -- Add the message template for acknowledgement of claim
   INSERT INTO dbo.utb_message_template (
       AppliesToCD,
       EnabledFlag,
       Description,
       ServiceChannelCD,
       SysLastUserID,
       SysLastUpdatedDate
   ) VALUES (
       'C',			-- what's the document class code?
       1,			-- enabled - 0: no, 1: yes
       'Acknowledgement of Claim|Messages/msgPSQBEClaimAck.xsl',		--custom xsl template
       'PS',		-- DA:  desk audit, PS: Program Shop
       0,
       @now
   )
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   -- Create the bundling profile for Acknowledgement of Claim
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      34,							-- DocumentTypeID, (acknowledgement of claim)???
      @MessageTemplateID,			-- MessageTemplateID,
      1,							-- EnabledFlag,
      'Acknowledgement of Claim',	-- Name,
      0,							-- SysLastUserID,
      @NOW							-- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Acknowledgement of Claim doc type
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      34,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )
   
      
   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD (DA: desk audit, PS: program shop),
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
   
   
	/* -------------------------------------------------- */
		-- 2.  Create Assignment Cancellation bundle
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Assignment Cancellation|Messages/msgPSQBEAssignmentCancel.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      36,							-- DocumentTypeID,
      @MessageTemplateID,			-- MessageTemplateID,
      1,							-- EnabledFlag,
      'Assignment Cancellation',	-- Name,
      0,							-- SysLastUserID,
      @NOW							-- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Assignment Cancellation
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      36,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )

	/* -------------------------------------------------- */
		-- 3.  Create Total Loss Alert - Adjuster bundle
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Total Loss Alert - Adjuster|Messages/msgPSQBETLAlert.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      38, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Total Loss Alert - Adjuster', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add total loss alert - adjuster
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      38,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )


	/* -------------------------------------------------- */
		-- 4.  Create Status Update - Notification Only bundle
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Status Update - Notification Only|Messages/msgPSQBEStatusUpdNotify.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      36, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Status Update - Notification Only', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Status Update - Notification Only
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      36,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )


	/* -------------------------------------------------- */
		-- 5.  Create Status Update - Action Required bundle
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Status Update - Action Required|Messages/msgPSQBEStatusUpdAction.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      36, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Status Update - Action Required', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Status Update - Action Required
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      36,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
   
   
   	/* -------------------------------------------------- */
		-- 6.  Create 'Stale Alert and Close' bundle
		-- wjc: needs addl docs added
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Stale Alert & Close|Messages/msgPSQBEStaleAlert.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Stale Alert & Close', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Status Update - Action Required
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      37,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )
   
   
	-- Add invoice
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        13,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        2,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
   
   
   

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
   
   	/* -------------------------------------------------- */
		-- 7.  Create 'Cash Out Alert and Close' bundle
		-- wjc: needs addl docs added
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Cash Out Alert & Close|Messages/msgPSQBECashOutAlert.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      35, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Cash Out Alert & Close', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Cash Out Alert and Close
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      35,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )


	-- Add invoice
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        13,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        2,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
	-- Add initial approved estimate
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        3,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        3,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
	-- Add photographs
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        8,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        4,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )



   	/* -------------------------------------------------- */
		-- 8.  Create 'Closing - Total Loss' bundle
		-- wjc: needs addl docs added
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Closing - Total Loss|Messages/msgPSQBEClosingTL.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Closing - Total Loss', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Cash Out Alert and Close
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      37,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )


	-- Add QBE BCIF
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        40,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        2,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )


	-- Add invoice
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        13,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        3,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

	-- Add initial audited estimate
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        3,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        4,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

	-- Add photographs
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        8,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        5,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )


   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )


   	/* -------------------------------------------------- */
		-- 9.  Create 'Approved Estimate - Early Bill' bundle
		-- wjc: needs addl docs added
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Approved Estimate - Early Bill|Messages/msgPSQBEAppEstEarlyBill.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      53, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Approved Estimate - Early Bill', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Cash Out Alert and Close
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      53,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

	-- Add invoice
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        13,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        2,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
	-- Add direction to pay
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        2,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        3,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
    -- Add initial approved estimate
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        3,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        4,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

	-- Add photographs
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        8,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        5,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )



   	/* -------------------------------------------------- */
		-- 10.  Create 'Repair Complete - Release and Close' bundle
		-- wjc: needs addl docs added
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Repair Complete - Release and Close|Messages/msgPSQBEReleaseClose.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Repair Complete - Release and Close', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
  
   -- Add Cash Out Alert and Close
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      37,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add invoice
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      13,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      2,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )
   
   -- Add final bill estimate
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      3,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      2,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

	-- Add repair acceptance
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        51,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        4,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

	-- Add photographs
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        8,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        5,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )


   	/* -------------------------------------------------- */
		-- 11.  Create 'Repair Complete - Combined Bill, Release, and Close' bundle
		-- wjc: needs addl docs added
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Repair Complete - Combined Bill, Release, and Close|Messages/msgPSQBEBillReleaseClose.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Repair Complete - Combined Bill, Release, & Close', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Cash Out Alert and Close
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      37,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

	-- Add invoice
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        13,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        2,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
	-- Add direction to pay
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        2,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        3,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
    -- Final Bill Estimate
	-- Add direction to pay
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        4,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        3,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
	-- Add repair acceptance
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        51,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        5,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
	-- Add photographs
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        8,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        6,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )

   	/* -------------------------------------------------- */
		-- 12.  Create 'Rental Invoice' bundle
		-- wjc: needs addl docs added
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Rental Invoice|Messages/msgPSQBERentalInv.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      44, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Rental Invoice', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Cash Out Alert and Close
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      44,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )
   
	-- Add rental invoice
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        9,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        2,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )



   	/* -------------------------------------------------- */
		-- 13.  Create 'Closing Supplement' bundle
		-- wjc: needs addl docs added
	/* -------------------------------------------------- */
   
	-- Add the message template
	INSERT INTO dbo.utb_message_template (
	   AppliesToCD,
	   EnabledFlag,
	   Description,
	   ServiceChannelCD,
	   SysLastUserID,
	   SysLastUpdatedDate
	) VALUES (
	   'C',
	   1,
	   'Closing Supplement|Messages/msgPSQBEClosingSupp.xsl',
	   'PS',
	   0,
	   @now
	)
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Closing Supplement', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Cash Out Alert and Close
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      37,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )
   
   	-- Add invoice
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        13,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        2,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
    -- supplement
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        10,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        3,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    
	-- Add photographs
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @BundlingID,	 -- BundlingID,
        8,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        NULL,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        6,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )
    

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )



	/* -------------------------------------------------- */
		-- 14.  Create Missing ProcessClaims Assignment bundle
	/* -------------------------------------------------- */
   
   -- Add the message template for acknowledgement of claim
   INSERT INTO dbo.utb_message_template (
       AppliesToCD,
       EnabledFlag,
       Description,
       ServiceChannelCD,
       SysLastUserID,
       SysLastUpdatedDate
   ) VALUES (
       'C',			-- what's the document class code?
       1,			-- enabled - 0: no, 1: yes
       'Missing ProcessClaims Assignment|Messages/msgPSQBEMissingPCAsgn.xsl',		--custom xsl template
       'PS',		-- DA:  desk audit, PS: Program Shop
       0,
       @now
   )
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   -- Create the bundling profile for Acknowledgement of Claim
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      12,							-- DocumentTypeID, (acknowledgement of claim)???
      @MessageTemplateID,			-- MessageTemplateID,
      1,							-- EnabledFlag,
      'Missing ProcessClaims Assignment',	-- Name,
      0,							-- SysLastUserID,
      @NOW							-- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Acknowledgement of Claim doc type
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      12,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )
   
      
   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD (DA: desk audit, PS: program shop),
      NULL,
      NULL,
      NULL,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )



	/* - - - - - - - - - - - - - - - - - */
	select dt.Name, b.Name, bdt.* 
	from utb_bundling_document_type bdt
	left join utb_document_type dt 
		on bdt.DocumentTypeID = dt.DocumentTypeID
	left join utb_bundling b 
		on bdt.BundlingID = b.BundlingID
	left join utb_client_bundling cb 
		on b.BundlingID = cb.BundlingID
	where cb.InsuranceCompanyID = 304 --@InsuranceCompanyID
		and cb.ServiceChannelCD = 'PS'   

	select * from utb_document_type order by name
END




COMMIT
-- ROLLBACK