
/*
	Filename:
	Author/Date: Walter J. Chesla - 12/30/2010
	Description:  Setup users for Zurich Dallas, Omaha, and Sacramento offices
*/


DECLARE @InsuranceCompanyID		smallint
DECLARE @ModifiedDateTime       DateTime
DECLARE @DallasOfficeID         smallint
DECLARE @OmahaOfficeID          smallint
DECLARE @SacOfficeID            smallint

-- set variables
SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = (SELECT InsuranceCompanyID FROM utb_insurance WHERE name = 'Zurich North America')

SET @DallasOfficeID = (SELECT TOP 1 OfficeID FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID AND ClientOfficeId = 'Dallas')
SET @OmahaOfficeID = (SELECT TOP 1 OfficeID FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID AND ClientOfficeId = 'Omaha')
SET @SacOfficeID = (SELECT TOP 1 OfficeID FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID AND ClientOfficeId = 'Sacramento')


PRINT 'Updating Zurich users...'

UPDATE utb_user
SET OfficeID = @DallasOfficeID,
	SysLastUpdatedDate = @ModifiedDateTime,
	PhoneAreaCode = '214',
	PhoneExchangeNumber = '866',
	PhoneUnitNumber = '1525'
WHERE EmailAddress = 'bryan.blake@zurichna.com'

UPDATE utb_user
SET OfficeID = @OmahaOfficeID,
	SysLastUpdatedDate = @ModifiedDateTime
WHERE EmailAddress = 'bob.smallfoot@zurichna.com'

UPDATE utb_user
SET OfficeID = @SacOfficeID,
	SysLastUpdatedDate = @ModifiedDateTime
WHERE EmailAddress = 'michael.2.dixon@zurichna.com'


PRINT 'Creating new Zurich users...'

-- add new users
-- Dallas:  Ziad Abdella - ziad.abdella@zurichna.com - 214-866-1525
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'ziad.abdella@zurichna.com',
    @NameFirst             = 'Ziad',
    @NameLast              = 'Abdella',
    @EmailAddress          = 'ziad.abdella@zurichna.com',
    @OfficeID              = @DallasOfficeID,
    @PhoneAreaCode         = '214',
    @PhoneExchangeNumber   = '866',
    @PhoneUnitNumber       = '1525',
    @ApplicationID		   = 2
    
-- Sacramento: Kate Petros - kate.petros@zurichna.com - 303-224-4082
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kate.petros@zurichna.com',
    @NameFirst             = 'Kate',
    @NameLast              = 'Petros',
    @EmailAddress          = 'kate.petros@zurichna.com',
    @OfficeID              = @SacOfficeID,
    @PhoneAreaCode         = '303',
    @PhoneExchangeNumber   = '224',
    @PhoneUnitNumber       = '4082',
    @ApplicationID		   = 2

-- Sacramento: Chris Tracy - chris.tracy@zurichna.com - 303-224-4082
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'chris.tracy@zurichna.com',
    @NameFirst             = 'Chris',
    @NameLast              = 'Tracy',
    @EmailAddress          = 'chris.tracy@zurichna.com',
    @OfficeID              = @SacOfficeID,
    @PhoneAreaCode         = '303',
    @PhoneExchangeNumber   = '224',
    @PhoneUnitNumber       = '4082',
    @ApplicationID		   = 2

-- set all user names to proper case
update utb_user
set NameFirst = dbo.ufnProperCase(NameFirst),
	NameLast = dbo.ufnProperCase(NameLast)
where OfficeID in
	(SELECT OfficeID 
	 FROM utb_office 
	 WHERE InsuranceCompanyID = @InsuranceCompanyID)

PRINT 'Complete.'
