DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 158

DECLARE @MsgTempID INT
DECLARE @BundlingID INT

--------------------------------
-- Bunding Changes - Closing TL
--------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%Total Loss Closing|Messages/msgPSTLClosing.xsl%'

	UPDATE utb_client_bundling SET ReturnDocRoutingCD = 'WS' WHERE BundlingID = @BundlingID

----------------------------------------
-- Bunding Changes - Closing Supplement
----------------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%Closing Supplement|Messages/msgPSClosingSupplement.xsl%'

	UPDATE utb_client_bundling SET ReturnDocRoutingCD = 'WS' WHERE BundlingID = @BundlingID

----------------------------------------
-- Bunding Changes - Rental Invoice
----------------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%Rental Invoice|Messages/msgPSRentalInvoice.xsl%'

	UPDATE utb_client_bundling SET ReturnDocRoutingCD = 'WS' WHERE BundlingID = @BundlingID


SELECT 'Web Services Config Updated...'
