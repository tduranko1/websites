DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 464

UPDATE
	utb_client_bundling 
SET 
	ReturnDocPackageTypeCD = 'PDF'
	, ReturnDocRoutingCD = 'EML'
	, ReturnDocRoutingValue = 'claims@countryway.com'
WHERE
	insurancecompanyid = @ToInscCompID
