/************************************************************************
* Add Photos to the exception list for Autoverse transmission
*************************************************************************/

insert into dbo.utb_client_document_exception
(InsuranceCompanyID, DocumentTypeID, OutputTypeCD, SysLastUserID, SysLastUpdatedDate)
values
(261, 8, NULL, 0, current_timestamp)
