declare @InsuranceCompanyID as int

select @InsuranceCompanyID = InsuranceCompanyID
  from utb_insurance
 where Name = 'Electric Insurance'
 
if @InsuranceCompanyID > 0
begin
    -- update default routing profile
    update utb_insurance
    set ReturnDocDestinationCD = 'REP'
        , ReturnDocPackageTypeCD = 'PDF'
        , ReturnDocRoutingCD = 'EML'
    where InsuranceCompanyID = @InsuranceCompanyID
end