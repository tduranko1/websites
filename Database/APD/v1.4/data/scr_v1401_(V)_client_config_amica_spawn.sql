--
-- TABLE INSERT STATEMENTS
--
SET IDENTITY_INSERT utb_spawn ON

INSERT INTO dbo.utb_spawn ( SpawnID, WorkflowID, ConditionalValue, ContextNote, CustomProcName, EnabledFlag, SpawningID, SpawningServiceChannelCD, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 183, 127, 'DR', NULL, NULL, 1, 21, NULL, 'T', 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_spawn ( SpawnID, WorkflowID, ConditionalValue, ContextNote, CustomProcName, EnabledFlag, SpawningID, SpawningServiceChannelCD, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 185, 127, 'DR', NULL, NULL, 1, 17, NULL, 'T', 0, CURRENT_TIMESTAMP ) 
go

SET IDENTITY_INSERT utb_spawn OFF
