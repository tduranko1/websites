-- Test POST
BACKUP DATABASE [udb_apd_test] TO  DISK = N'\\SDRSLYNXSTGSQLA\DatabaseBackups\udb_apd_test_db_1402a_POST.bak' WITH  INIT ,  NOUNLOAD ,  NAME = N'Post udb_apd_test conversion’ ',  NOSKIP ,  STATS = 10,  NOFORMAT 
RESTORE VERIFYONLY FROM  DISK = N'\\SDRSLYNXSTGSQLA\DatabaseBackups\udb_apd_test_db_1402a_POST.bak' WITH  FILE = 1 ,  NOUNLOAD 


-- Production POST
BACKUP DATABASE [udb_apd] TO  DISK = N'\\SGOFLYNXSQLA\DatabaseBackups\udb_apd_db_1402a_POST.bak' WITH  INIT ,  NOUNLOAD ,  NAME = N'Post udb_apd conversion’ ',  NOSKIP ,  STATS = 10,  NOFORMAT 
RESTORE VERIFYONLY FROM  DISK = N'\\SGOFLYNXSQLA\DatabaseBackups\udb_apd_db_1402a_POST.bak' WITH  FILE = 1 ,  NOUNLOAD 

