DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 194

------------------------------
-- Add Form Supplements
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_form_supplement 
	WHERE 
		FormID IN 
		(
			SELECT 
				FormID 
			FROM 
				utb_form
			WHERE 
				InsuranceCompanyID = @ToInscCompID
		) 
)
BEGIN

	INSERT INTO utb_form
	SELECT 
		BundlingTaskID
		, DocumentTypeID
		, EventID
		, @ToInscCompID
		, LossStateCode
		, ShopStateCode
		, AutoBundlingFlag
		, ConditionValue
		, DataMiningFlag
		, EnabledFlag
		, FormSupplementID
		, REPLACE(HTMLPath,'WF','CI')
		, REPLACE(Name,'Westfield','Cincinnati')
		, REPLACE(PDFPath,'Westfield','Cincinnati')
		, PertainsToCD
		, ServiceChannelCD
		, SQLProcedure
		, SystemFlag
		, SysLastUserID
		, CURRENT_TIMESTAMP
	FROM 
		utb_form 
	WHERE 
		InsuranceCompanyID = 387

	DECLARE @iFormID INT
	SELECT @iFormID=FormID FROM utb_form	WHERE InsuranceCompanyID = @ToInscCompID AND Name = 'Shop Assignment'

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Expanded Comments', 'FaxXComments.pdf', NULL, '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Shop Instructions', 'ShopInstructions194.pdf', NULL, '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Direction to Pay', 'DTPType1.pdf', NULL, 'uspWorkflowSendShopAssignXML', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, 'NY', 1, NULL, 'DNY COAR Form', 'NY_COAR.pdf', 'PS', 'uspWorkflowSendShopAssignXML', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, 'CT', 1, NULL, 'CT Customer Choice', 'CTAoClaimantConsumerChoice.pdf', 'PS', '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, 'PA', 1, NULL, 'Direction To Pay', 'DTPType1_PA.pdf', 'PS', '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Repair Acceptance', 'RepairAcceptance.pdf', NULL, 'uspWorkflowSendShopAssignXML', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, 'OH', 1, NULL, 'OHIO Expanded Comments', 'Ohio.pdf', 'PS', '', 0, CURRENT_TIMESTAMP

	SELECT 'Client Form Supplements added...'
END
ELSE
BEGIN
	SELECT 'Client Form Supplements already added...'
END

SELECT * FROM utb_form_supplement WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
