DECLARE @iBundlingID INT
SET @iBundlingID = 0

-- RRP - Status Update - Fix default email
SELECT 
	@iBundlingID = b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = 304
	AND b.[Name] = 'RRP - Status Update - Claim Rep'

--SELECT * FROM utb_client_bundling
IF EXISTS (SELECT * FROM utb_bundling WHERE BundlingID > 0)
BEGIN
	UPDATE utb_client_bundling SET ReturnDocPackageTypeCD = NULL, ReturnDocRoutingCD = NULL, ReturnDocRoutingValue = NULL WHERE BundlingID = @iBundlingID
	
	SELECT 'Updated Successfully...'
END
ELSE
BEGIN
	SELECT 'ERROR: Not Updated Successfully...'
END

-- RRP - Status Update - Action Required

SET @iBundlingID = 0

SELECT 
	@iBundlingID = b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = 304
	AND b.[Name] = 'RRP - Status Update - MDS'
	
IF EXISTS (SELECT * FROM utb_bundling WHERE BundlingID > 0)
BEGIN
	UPDATE utb_client_bundling SET ReturnDocPackageTypeCD = NULL, ReturnDocRoutingCD = NULL, ReturnDocRoutingValue = NULL WHERE BundlingID = @iBundlingID
	
	SELECT 'Updated Successfully...'
END
ELSE
BEGIN
	SELECT 'ERROR: Not Updated Successfully...'
END

SELECT
	@iBundlingID = b.BundlingID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = 304
	AND b.[Name] = 'RRP - Status Update - Claim Rep'

-- Rename Total Loss Alert
UPDATE utb_bundling SET [Name] = 'RRP - Total Loss Alert (Adjuster)' WHERE [Name] = 'RRP - Total Loss Alert'
UPDATE utb_message_template SET [Description] = 'RRP - Total Loss Alert (Adjuster)|Messages/msgRRPTotalLossAlert.xsl' WHERE [Description] = 'RRP - Total Loss Alert|Messages/msgRRPTotalLossAlert.xsl'

-- Add Required BCIF
DECLARE @BundlingID INT
SET @BundlingID = 0

SELECT
	@BundlingID = b.BundlingID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = 304
	AND b.[Name] = 'RRP - Total Loss Valuation (Adjuster & Appraisers)'
	
INSERT INTO
	utb_bundling_document_type
VALUES
	(
		@BundlingID
		, 41
		, NULL
		, NULL
		, 0
		, 0
		, 0
		, NULL
		, 0
		, NULL
		, 1
		, 1
		, NULL
		, 0
		, 0
		, 0
		, CURRENT_TIMESTAMP
	)

-- Adjust Save As name
DECLARE @DocumentTypeID INT
SET @DocumentTypeID = 0

SELECT @DocumentTypeID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Total Loss Valuation'

UPDATE 
	utb_bundling
SET 
	DocumentTypeID = @DocumentTypeID
WHERE
	BundlingID = @BundlingID

-- FinalFlag

SELECT
	@iBundlingID = b.BundlingID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = 304
	AND b.[Name] = 'RRP - Closing Total Loss'		

UPDATE utb_bundling_document_type SET FinalEstimateFlag = 1 WHERE BundlingID = @iBundlingID AND DocumentTypeID = 3
