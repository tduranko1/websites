DECLARE @CICBundlingID INT
SET @CICBundlingID = 0
SELECT @CICBundlingID = BundlingID FROM utb_bundling WHERE [Name] = 'RRP - Closing Inspection Complete'
--SELECT @CICBundlingID

DECLARE @CSBundlingID INT
SET @CSBundlingID = 0
SELECT @CSBundlingID = BundlingID FROM utb_bundling WHERE [Name] = 'RRP - Closing Supplement'
--SELECT @CSBundlingID

DECLARE @CTLBundlingID INT
SET @CTLBundlingID = 0
SELECT @CTLBundlingID = BundlingID FROM utb_bundling WHERE [Name] = 'RRP - Closing Total Loss'
--SELECT @CTLBundlingID
-----------------------------------------
DECLARE @ERSRDocumentTypeID INT
SET @ERSRDocumentTypeID = 0
SELECT @ERSRDocumentTypeID = DocumentTypeID FROM utb_bundling WHERE [Name] = 'SHOP: RRP - Estimate Revision Shop Request'
--SELECT @ERSRDocumentTypeID

DECLARE @TLDDocumentTypeID INT
SET @TLDDocumentTypeID = 0
SELECT @TLDDocumentTypeID = DocumentTypeID FROM utb_bundling WHERE [Name] = 'SHOP: RRP - Total Loss Determination (Shop)'
--SELECT @TLDDocumentTypeID

DECLARE @AESNDocumentTypeID INT
SET @AESNDocumentTypeID = 0
SELECT @AESNDocumentTypeID = DocumentTypeID FROM utb_bundling WHERE [Name] = 'SHOP: RRP - Approved Estimate Shop Notification'
--SELECT @AESNDocumentTypeID

UPDATE utb_bundling_document_type SET DuplicateFlag = 0 WHERE BundlingID = @CICBundlingID AND DocumentTypeID = @ERSRDocumentTypeID

UPDATE utb_bundling_document_type SET DuplicateFlag = 0 WHERE BundlingID = @CICBundlingID AND DocumentTypeID = @AESNDocumentTypeID

UPDATE utb_bundling_document_type SET DuplicateFlag = 0 WHERE BundlingID = @CSBundlingID AND DocumentTypeID = @ERSRDocumentTypeID

UPDATE utb_bundling_document_type SET DuplicateFlag = 0 WHERE BundlingID = @CSBundlingID AND DocumentTypeID = @AESNDocumentTypeID

UPDATE utb_bundling_document_type SET DuplicateFlag = 0 WHERE BundlingID = @CTLBundlingID AND DocumentTypeID = @TLDDocumentTypeID
