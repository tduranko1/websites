BEGIN TRANSACTION

USE udb_apd_dev
DECLARE @InscCompID SMALLINT
DECLARE @WorkFlowID SMALLINT
SET @InscCompID = 575  -- Norther Neck


-- Add Workflows
IF NOT EXISTS (SELECT * FROM utb_workflow WHERE InsuranceCompanyID = @InscCompID AND OriginatorID = 70  )
BEGIN
	INSERT INTO utb_workflow VALUES (349,@InscCompID,0,70,'E',0,Current_Timestamp)
	SELECT 'Workflow row added...'
END
ELSE
BEGIN
	SELECT 'Workflow already added...'
END

-- Add Workflows
IF NOT EXISTS (SELECT * FROM utb_workflow WHERE InsuranceCompanyID = @InscCompID AND OriginatorID = 29  )
BEGIN
	INSERT INTO utb_workflow VALUES (350,@InscCompID,0,29,'E',0,Current_Timestamp)
	SELECT 'Workflow row added...'
END
ELSE
BEGIN
	SELECT 'Workflow already added...'
END

commit

BEGIN TRANSACTION
SELECT @WorkFlowID=WorkFlowID FROM utb_workflow WHERE InsuranceCompanyID = @InscCompID AND OriginatorID = 70

-- Add Spawn
IF NOT EXISTS (SELECT * FROM utb_spawn WHERE WorkflowID = @WorkFlowID AND SpawningID = 6  )
BEGIN
	INSERT INTO utb_spawn VALUES (@WorkFlowID,'PS','%Key%',NULL,1,6,NULL,'T',0,Current_Timestamp)
	SELECT 'Spawn row added...'
END
ELSE
BEGIN
	SELECT 'Spawn already added...'
END

-- Add Spawn
IF NOT EXISTS (SELECT * FROM utb_spawn WHERE WorkflowID = @WorkFlowID AND SpawningID = 35  )
BEGIN
	INSERT INTO utb_spawn VALUES (@WorkFlowID,'PS','%PertainsToName%',NULL,1,35,NULL,'T',0,Current_Timestamp)
	SELECT 'Spawn row added...'
END
ELSE
BEGIN
	SELECT 'Spawn already added...'
END

-- Add Spawn
IF NOT EXISTS (SELECT * FROM utb_spawn WHERE WorkflowID = @WorkFlowID AND SpawningID = 2  )
BEGIN
	INSERT INTO utb_spawn VALUES (@WorkFlowID,'PS','%PertainsToName%',NULL,1,2,NULL,'T',0,Current_Timestamp)
	SELECT 'Spawn row added...'
END
ELSE
BEGIN
	SELECT 'Spawn already added...'
END

-- Add Spawn
IF NOT EXISTS (SELECT * FROM utb_spawn WHERE WorkflowID = @WorkFlowID AND SpawningID = 24  )
BEGIN
	INSERT INTO utb_spawn VALUES (@WorkFlowID,'RENTAL',NULL,NULL,1,24,NULL,'T',0,Current_Timestamp)
	SELECT 'Spawn row added...'
END
ELSE
BEGIN
	SELECT 'Spawn already added...'
END

commit 
--rollback
