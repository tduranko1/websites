--DROP TRIGGER trg_utb_apd_event_log_insert
CREATE TRIGGER trg_utb_apd_event_log_insert
ON
	utb_apd_event_log	
AFTER INSERT
AS

---------------------------------------
-- Global Vars
---------------------------------------
DECLARE @vErrorDetails VARCHAR(150)
DECLARE @vEventDetailedDescription VARCHAR(250)
DECLARE @vEventMessage VARCHAR(200)
DECLARE @dtLastUpdatedDateTime DATETIME
DECLARE @vRecipients VARCHAR (200)
DECLARE @vRecipients_WES VARCHAR (200)

SET @vRecipients = 'lynxapdprdmon@lynxservices.com'
SET @vRecipients_WES = 'lynxapdprdmon@lynxservices.com'

---------------------------------------
-- Monitor the APD Event Log for 
-- Out of Memory issues.
---------------------------------------
IF EXISTS (
	SELECT
		EventDetailedDescription
	FROM 
		inserted 
	WHERE
		EventType = 'COM Conversion'
		AND EventStatus = 'ERROR' 
		AND EventDetailedDescription LIKE '%out%' 
)
BEGIN
	SELECT @vRecipients = Value FROM utb_app_variable WHERE SubName = 'TrgEvents'
	SELECT @vErrorDetails = EventDescription, @dtLastUpdatedDateTime = SysLastUpdatedDate FROM inserted WHERE EventType = 'COM Conversion' AND EventStatus = 'ERROR' AND EventDetailedDescription LIKE '%out%' 
	SET @vErrorDetails = @vErrorDetails + ' - App Pool: Out of Memory'
	SET @vEventDetailedDescription = 'App Pool: Out of Memory'
	SET @vEventMessage = 'SELECT ' + '''' + @vErrorDetails + ''', ' + '''' + @vEventDetailedDescription + ''', ' + '''' + CONVERT(VARCHAR(50), @dtLastUpdatedDateTime,121) + ''''

	EXEC master.dbo.usp_app_db_notification @query=@vEventMessage,@subject=@vErrorDetails, @recipients=@vRecipients
	RAISERROR('SQL-ERROR-101|%s|%s', 16, 1, @vErrorDetails, @vEventDetailedDescription)
END

---------------------------------------
-- Monitor the APD Event Log for 
-- Westfield Invoice send issues.
---------------------------------------
IF EXISTS (
	SELECT
		EventDetailedDescription
	FROM 
		inserted 
	WHERE
		EventType = 'EMS Helper' 
		AND EventStatus = 'ERROR'
		AND EventDetailedDescription LIKE '%faultcode%'
		AND EventDetailedDescription NOT LIKE '%was already received successfully for the claim%'
)
BEGIN
	SELECT @vRecipients_WES = Value FROM utb_app_variable WHERE SubName = 'TrgEvents-WES'
	SELECT @vErrorDetails = EventDescription, @vEventDetailedDescription = SUBSTRING(EventDetailedDescription, 0, 250), @dtLastUpdatedDateTime = SysLastUpdatedDate  FROM inserted WHERE EventType = 'EMS Helper' AND EventStatus = 'ERROR' AND EventDetailedDescription LIKE '%faultcode%' AND EventDetailedDescription NOT LIKE '%was already received successfully for the claim%' 
	SET @vEventMessage = 'SELECT ' + '''' + @vErrorDetails + ''', ' + '''' + @vEventDetailedDescription + ''', ' + '''' + CONVERT(VARCHAR(50), @dtLastUpdatedDateTime,121) + ''''

	EXEC master.dbo.usp_app_db_notification @query=@vEventMessage,@subject=@vErrorDetails, @recipients=@vRecipients_WES
	RAISERROR('SQL-ERROR-101|%s|%s', 16, 1, @vErrorDetails, @vEventDetailedDescription)
END






         
         