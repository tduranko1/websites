

DECLARE @InsuranceCompanyID as int
SET @InsuranceCompanyID = 304


IF EXISTS(SELECT FormID
                FROM dbo.utb_form
                WHERE InsuranceCompanyID = @InsuranceCompanyID
                  AND ServiceChannelCD = 'RRP'
                  AND HTMLPath = 'Forms/QBE_BCIF.htm')
BEGIN


	IF NOT EXISTS(SELECT FormID
					FROM dbo.utb_form
					WHERE InsuranceCompanyID = @InsuranceCompanyID
					  AND ServiceChannelCD = 'RRP'
					  AND HTMLPath = 'Forms/QBE_BCIF.asp')
	BEGIN

		UPDATE utb_form
		SET  HTMLPath = 'Forms/QBE_BCIF.asp'
		WHERE InsuranceCompanyID = @InsuranceCompanyID
			AND SERVICECHANNELCD = 'RRP'
			AND HTMLPath = 'Forms/QBE_BCIF.htm'
		
	END

END

SELECT * FROM dbo.utb_form WHERE InsuranceCompanyID = @InsuranceCompanyID
	


