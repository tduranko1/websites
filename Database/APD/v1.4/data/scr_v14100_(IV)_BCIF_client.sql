/******************************************************************************
* BCIF custom form available only for EIC, TXFB, Farm Family, VAFB & WAG
******************************************************************************/

BEGIN TRANSACTION


UPDATE utb_form
SET EnabledFlag = 0
WHERE Name = 'BCIF Request'

-- Add BCIF to EIC
INSERT INTO utb_form (
   DocumentTypeID,
   AutoBundlingFlag,
   InsuranceCompanyID,
   EnabledFlag,
   HTMLPath,
   Name,
   PDFPath,
   PertainsToCD,
   ServiceChannelCD,
   SQLProcedure,
   SystemFlag,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   40,
   1,
   158,
   1,
   'Forms/BCIF.htm',
   'BCIF Request',
   'BCIF.pdf',
   'S',
   'PS',
   'uspCFBCIFGetXML',
   0,
   0,
   CURRENT_TIMESTAMP
)

-- Add BCIF to Farm Family
INSERT INTO utb_form (
   DocumentTypeID,
   AutoBundlingFlag,
   InsuranceCompanyID,
   EnabledFlag,
   HTMLPath,
   Name,
   PDFPath,
   PertainsToCD,
   ServiceChannelCD,
   SQLProcedure,
   SystemFlag,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   40,
   1,
   198,
   1,
   'Forms/BCIF.htm',
   'BCIF Request',
   'BCIF.pdf',
   'S',
   'PS',
   'uspCFBCIFGetXML',
   0,
   0,
   CURRENT_TIMESTAMP
)

-- Add BCIF to Texas Farm Bureau
INSERT INTO utb_form (
   DocumentTypeID,
   AutoBundlingFlag,
   InsuranceCompanyID,
   EnabledFlag,
   HTMLPath,
   Name,
   PDFPath,
   PertainsToCD,
   ServiceChannelCD,
   SQLProcedure,
   SystemFlag,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   40,
   1,
   184,
   1,
   'Forms/BCIF.htm',
   'BCIF Request',
   'BCIF.pdf',
   'S',
   'PS',
   'uspCFBCIFGetXML',
   0,
   0,
   CURRENT_TIMESTAMP
)


-- Add BCIF to Virginia Farm Bureau
INSERT INTO utb_form (
   DocumentTypeID,
   AutoBundlingFlag,
   InsuranceCompanyID,
   EnabledFlag,
   HTMLPath,
   Name,
   PDFPath,
   PertainsToCD,
   ServiceChannelCD,
   SQLProcedure,
   SystemFlag,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   40,
   1,
   192,
   1,
   'Forms/BCIF.htm',
   'BCIF Request',
   'BCIF.pdf',
   'S',
   'PS',
   'uspCFBCIFGetXML',
   0,
   0,
   CURRENT_TIMESTAMP
)

-- Add BCIF to Western Agricultural
INSERT INTO utb_form (
   DocumentTypeID,
   AutoBundlingFlag,
   InsuranceCompanyID,
   EnabledFlag,
   HTMLPath,
   Name,
   PDFPath,
   PertainsToCD,
   ServiceChannelCD,
   SQLProcedure,
   SystemFlag,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   40,
   1,
   178,
   1,
   'Forms/BCIF.htm',
   'BCIF Request',
   'BCIF.pdf',
   'S',
   'PS',
   'uspCFBCIFGetXML',
   0,
   0,
   CURRENT_TIMESTAMP
)

select * from utb_form


-- COMMIT
-- ROLLBACK