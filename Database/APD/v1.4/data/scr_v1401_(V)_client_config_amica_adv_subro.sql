-----------------------------------------------------------------------------------------------------------
-- This script will add Adverse Subro Desk Audit to Rochester, Philadelphia and Amica Central office of Amica Insurance
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 196

PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'



PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'


--Insert Client level configuration
-- Add Adverse Subro Desk Audit at the client level. Only 3 offices will be sending us this and we will handle it at the office level
------------------------------------------------------

INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE AssignmentTypeID = 6
  



------------------------------------------------------

-- Office level configurations

-- Rochester office configuration
SET @OfficeID = NULL

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeID = 'Rochester'

IF @OfficeID IS NOT NULL
BEGIN
   -- Add Adverse Subro Desk Audit to Rochester office
    INSERT INTO dbo.utb_office_assignment_type
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE AssignmentTypeID = 6
END    


-- Philadelphia office configuration
SET @OfficeID = NULL

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeID = 'Philadelphia'

IF @OfficeID IS NOT NULL
BEGIN
   -- Add Adverse Subro Desk Audit to Philadelphia office
   INSERT INTO dbo.utb_office_assignment_type
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE AssignmentTypeID = 6
END    

-- Amica Central office configuration
SET @OfficeID = NULL

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeID = 'Amica Central'

IF @OfficeID IS NOT NULL
BEGIN
   -- Add Adverse Subro Desk Audit to Amica Central office
   INSERT INTO dbo.utb_office_assignment_type
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE AssignmentTypeID = 6
END    

-- Review affected tables
SELECT * FROM dbo.utb_client_assignment_type
SELECT * FROM dbo.utb_office_assignment_type


--commit 
--rollback
