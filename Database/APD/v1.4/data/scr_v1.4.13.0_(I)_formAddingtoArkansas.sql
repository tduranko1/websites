﻿USE [udb_apd]
GO

IF NOT EXISTS(SELECT *
                FROM dbo.utb_form_supplement
                WHERE Name = 'AR Aftermarket Parts Form'  AND ServiceChannelCD = 'PS'
)
BEGIN
      INSERT INTO 
         utb_form_supplement 
      SELECT
            FormID
            , NULL
            , 'AR'
            , 1
            , NULL
            , 'AR Aftermarket Parts Form'
            , 'AR_Aftermarket_Parts_Form.pdf'
            , 'PS'
            , ''
            , 0
            , CURRENT_TIMESTAMP 
      FROM 
            utb_form_supplement 
      WHERE 
            FormID IN 
            (
                  SELECT 
                        FormID 
                  FROM 
                        utb_form
                  WHERE
                        [name] = 'Shop Assignment'
            )
            AND [Name] = 'Shop Instructions'

      SELECT 'AR_Aftermarket_Parts Forms added...'
END
ELSE
BEGIN
      SELECT 'AR_Aftermarket_Parts Form already added...'
END
