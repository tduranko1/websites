DECLARE @InsuranceCompanyID as int
SET @InsuranceCompanyID = 374

--SELECT
--    *
--FROM
--    utb_form_supplement

BEGIN
UPDATE 
      utb_form_supplement
SET PDFPath = 'DTPType374.pdf'

WHERE
      FormID IN 
      (
            SELECT 
                        FormID 
            FROM 
                  utb_form
            WHERE 
                  InsuranceCompanyID = @InsuranceCompanyID
            )
            AND PDFPath IN ('DTPType1.pdf')
END 

BEGIN
UPDATE 
      utb_form_supplement
SET PDFPath = 'DTPType374_PA.pdf'

WHERE
      FormID IN 
      (
            SELECT 
                        FormID 
            FROM 
                  utb_form
            WHERE 
                  InsuranceCompanyID = @InsuranceCompanyID
            )
            AND PDFPath IN ('DTPType1_PA.pdf')

END 

BEGIN
UPDATE 
      utb_form_supplement
SET PDFPath = 'RepairAcceptance374.pdf'
WHERE
      FormID IN 
      (
            SELECT 
                        FormID 
            FROM 
                  utb_form
            WHERE 
                  InsuranceCompanyID = @InsuranceCompanyID
            )
            AND PDFPath IN ('RepairAcceptance.pdf')

END

 SELECT * FROM dbo.utb_form_supplement  
      WHERE 
   FormID IN 
      (
            SELECT 
                  FormID 
            FROM 
                  utb_form
            WHERE 
                  InsuranceCompanyID = @InsuranceCompanyID
)

