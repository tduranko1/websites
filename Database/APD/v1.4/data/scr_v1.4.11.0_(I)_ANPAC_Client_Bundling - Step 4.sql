DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 473
SET @FromInscCompID = 304

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
SELECT 
	* 
FROM 
	utb_bundling_document_type 
WHERE 	
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @ToInscCompID
	)

)
BEGIN
	INSERT INTO 
		utb_bundling_document_type
	SELECT 
		(SELECT sb.BundlingID FROM utb_bundling sb 	INNER JOIN utb_client_bundling cb 
		ON sb.BundlingId = cb.BundlingID WHERE [Name] = b.Name AND InsuranceCompanyID = @ToInscCompID)
		, bdt.DocumentTypeID
		, bdt.ConditionValue
		, DirectionalCD
		, DirectionToPayFlag
		, DuplicateFlag
		, EstimateDuplicateFlag
		, EstimateTypeCD
		, FinalEstimateFlag
		, LossState
		, MandatoryFlag
		, SelectionOrder
		, ShopState
		, VANFlag
		, WarrantyFlag
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
		INNER JOIN utb_bundling b
		ON b.BundlingID = bdt.BundlingID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @FromInscCompID
				AND mt.[Description] IN (
				'RRP - Total Loss Alert (Adjuster)|Messages/msgRRPTotalLossAlert.xsl'
				, 'RRP - Approved Estimate Shop Notification|Messages/msgRRPApprovedEstimateShopNotification.xsl'
				, 'RRP - Closing Inspection Complete|Messages/msgRRPClosingInspectionComplete.xsl'
				, 'RRP - Closing Supplement|Messages/msgRRPClosingSupplement.xsl'
				, 'RRP - Closing Total Loss|Messages/msgRRPClosingTotalLoss.xsl'
				, 'RRP - Acknowledgement of Claim|Messages/msgRRPClmAck.xsl'
				, 'RRP - Assignment Cancellation|Messages/msgRRPAssignmentCancel.xsl'
				, 'RRP - Status Update - Notification|Messages/msgRRPStatusUpdateNotificationOnly.xsl'
				, 'RRP - Status Update - Action Required|Messages/msgRRPStatusUpdateActionRequired.xsl'
				, 'RRP - Stale Alert & Close|Messages/msgRRPStaleAlertAndClose.xsl'
				, 'RRP - Total Loss Determination (Shop)|Messages/msgRRPTotalLossDetermination.xsl'
				)
		)

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END
