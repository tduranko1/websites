-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminInsLogData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminInsLogData 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspAdminInsLogData
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     [Stored procedure to just log misc data]
*
* PARAMETERS:  
*    @AppName			VARCHAR(50)
*    , @UserID			udt_std_id_big
*    , @FuncName			VARCHAR(50)
*    , @SPName			VARCHAR(50)
*    , @Data1			VARCHAR(500)
*    , @Data2			VARCHAR(500)
*    , @Data3			VARCHAR(500)
*	 , @SysLastUserID	INT
*
* RESULT SET:
* 
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminInsLogData
    @AppName			VARCHAR(50)
    , @UserID			udt_std_id_big
    , @FuncName			VARCHAR(50)
    , @SPName			VARCHAR(50)
    , @Data1			VARCHAR(500)
    , @Data2			VARCHAR(500)
    , @Data3			VARCHAR(500)
	, @SysLastUserID	INT
AS
BEGIN
    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 
    SET @ProcName = 'uspAdminInsLogData'
    
    INSERT INTO 
		utb_Log 
	VALUES (
		@AppName
		,@UserID
		,@FuncName
		,@SPName
		,@Data1
		,@Data2
		,@Data3
		,@SysLastUserID
		,CURRENT_TIMESTAMP
	)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN(1)
    END    
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminInsLogData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminInsLogData TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
