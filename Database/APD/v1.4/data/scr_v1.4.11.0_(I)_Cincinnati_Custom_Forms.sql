DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 194

------------------------------
-- Add Form Supplements
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_form
	WHERE 
		InsuranceCompanyID = @ToInscCompID
		AND HTMLPath = 'Forms/CIN_TLVCReq.asp'
)
BEGIN
	UPDATE utb_form SET HTMLPath = 'Forms/CIN_TLVCReq.pdf', Name = 'Vehicle Condition Report', PDFPath = 'CIN_TLVCReq.asp', DocumentTypeID = 82  WHERE InsuranceCompanyID = 194 AND DocumentTypeID = 40
    insert into utb_form values (NULL,12,NULL,@ToInscCompID,NULL,NULL,0,NULL,0,1,NULL,NULL,'Autosource Condition Book','CIN_AutoCondBook.pdf','S','PS','uspCFBCIFGetXML',0,0,CURRENT_TIMESTAMP)

	SELECT 'Client Custom Forms added...'
END
ELSE
BEGIN
	SELECT 'Client Custom Forms already added...'
END

SELECT * FROM utb_form WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
