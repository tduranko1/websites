-----------------------------------------------------------------------------------------------------------
-- Rollback Configuration for NCFB - 02Dec2011 - Thomas Duranko
-- I'm redesigning this to be easier to process.
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint

-----------------------------------------------------------------------------------------------------------
-- Client Configuration Section.  All changes can be make from here.
-----------------------------------------------------------------------------------------------------------
-- Info
SET @InsuranceCompanyID 		= 327	-- Company ID 
SELECT @OfficeID=OfficeID from utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID

-----------------------------------------------------------------------------------------------------------
-- Process steps
-----------------------------------------------------------------------------------------------------------
PRINT '.'
PRINT '.'
PRINT 'Rolling back new data...'
PRINT '.'

--Delete Insurance Company information.

IF EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
	DELETE FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already deleted in utb_insurance', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END

------------------------------------------------------
--Delete Client level configuration
------------------------------------------------------
IF EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_client_assignment_type WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
	DELETE FROM dbo.utb_client_assignment_type WHERE InsuranceCompanyID = @InsuranceCompanyID
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already deleted in utb_client_assignment_type', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END

IF EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_client_service_channel WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
	DELETE FROM dbo.utb_client_service_channel WHERE InsuranceCompanyID = @InsuranceCompanyID
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already deleted in utb_client_service_channel', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END

------------------------------------------------------

IF EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_client_contract_state WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
	DELETE FROM dbo.utb_client_contract_state WHERE InsuranceCompanyID = @InsuranceCompanyID
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already deleted in utb_client_contract_state', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END

------------------------------------------------------

IF EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
	DELETE FROM dbo.utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already deleted in utb_client_coverage_type', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END

------------------------------------------------------    

IF EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_client_claim_aspect_type WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
	DELETE FROM dbo.utb_client_claim_aspect_type WHERE InsuranceCompanyID = @InsuranceCompanyID
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already deleted in utb_client_claim_aspect_type', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END
    
------------------------------------------------------

IF EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_client_report WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
	DELETE FROM dbo.utb_client_report WHERE InsuranceCompanyID = @InsuranceCompanyID
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already deleted in utb_client_report', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END

------------------------------------------------------
-- Delete Office(s)
-- Auto Owners Insurance
------------------------------------------------------
-- Office 1
------------------------------------------------------
IF EXISTS(SELECT OfficeID FROM dbo.utb_office_assignment_type WHERE OfficeID = @OfficeID)
BEGIN
	DELETE FROM dbo.utb_office_assignment_type WHERE OfficeID = @OfficeID
END
ELSE
BEGIN
    RAISERROR('Office ID already deleted in utb_office_assignment_type', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END

IF EXISTS(SELECT OfficeID FROM dbo.utb_office_contract_state WHERE OfficeID = @OfficeID)
BEGIN
	DELETE FROM dbo.utb_office_contract_state WHERE OfficeID = @OfficeID
END
ELSE
BEGIN
    RAISERROR('Office ID already deleted in utb_office_contract_state', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END

IF EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
	DELETE FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already deleted in utb_office', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END

-- Review affected tables
SELECT * FROM dbo.utb_insurance
SELECT * FROM dbo.utb_client_assignment_type
SELECT * FROM dbo.utb_client_claim_aspect_type
SELECT * FROM dbo.utb_client_contract_state
SELECT * FROM dbo.utb_client_payment_type
SELECT * FROM dbo.utb_client_report
SELECT * FROM dbo.utb_office
SELECT * FROM dbo.utb_office_assignment_type
SELECT * FROM dbo.utb_office_contract_state

-- This script requires a fresh on the workflow and spawn reference tables from the access file

--commit 
--rollback
