DECLARE @iDocumentTypeID INT

IF EXISTS (SELECT * FROM utb_bundling WHERE [Name] = 'RRP - Total Loss Determination (Shop)') 
BEGIN
	UPDATE utb_bundling SET [Name] = 'SHOP: RRP - Total Loss Determination (Shop)' WHERE [Name] = 'RRP - Total Loss Determination (Shop)'
	UPDATE utb_bundling SET [Name] = 'SHOP: RRP - Estimate Revision Shop Request' WHERE [Name] = 'RRP - Estimate Revision Shop Request'

	SELECT 'Documents Renamed...'
END
ELSE
BEGIN
	SELECT 'Document Already Renamed...'
END



