DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 374
SET @FromInscCompID = 277

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_bundling b INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE [Description] LIKE '%ELPS%') 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		34
		, MessageTemplateID 
		,1
		,'Acknowledgement of Claim'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSClmAck.xsl%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Closing Supplement'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSClosingSupplement.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Closing - Total Loss'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSTLClosing.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		44
		, MessageTemplateID 
		,1
		,'Rental Invoice'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSRentalInvoice.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Initial Bill Estimate - Early Billing'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSApprovedEstimate_EB.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Repair Complete - Release and Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPS_RC_Release_Close_EB.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Repair Complete - Combined Bill, Release and Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPS_RC_Combined_Bill_Release_Close_EB.xsl%'
				
	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

