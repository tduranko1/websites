﻿USE [udb_apd]
GO

IF NOT EXISTS(SELECT *
                FROM dbo.utb_form_supplement
                WHERE Name = 'RI Aftermarket Parts Form'  AND ServiceChannelCD = 'PS'
)
BEGIN
      INSERT INTO 
         utb_form_supplement 
      SELECT
            FormID
            , NULL
            , 'RI'
            , 1
            , NULL
            , 'RI Aftermarket Parts Form'
            , 'RI_Aftermarket_Parts_Form.pdf'
            , 'PS'
            , ''
            , 0
            , CURRENT_TIMESTAMP 
      FROM 
            utb_form_supplement 
      WHERE 
            FormID IN 
            (
                  SELECT 
                        FormID 
                  FROM 
                        utb_form
                  WHERE
                        [name] = 'Shop Assignment'
            )
            AND [Name] = 'Shop Instructions'

      SELECT 'RI Aftermarket Parts Forms added...'
END
ELSE
BEGIN
      SELECT 'RI Aftermarket Parts Forms already added...'
END
