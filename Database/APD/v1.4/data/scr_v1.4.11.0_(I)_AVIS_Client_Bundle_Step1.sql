DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 515
SET @FromInscCompID = 387

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%AV%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, REPLACE(mt.[Description],'msgWF','msgAV')
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND NOT Description LIKE 'Rental%'
		
	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

-------------------------------------
UPDATE
	utb_message_template
SET [Description] = 'Initial Estimate|Messages/msgAVPSInitialEstimate.xsl'
WHERE
	[Description] IN (
		SELECT [Description] FROM utb_message_template WHERE [Description] = 'Initial Estimate|Messages/msgAVPInitialEstimate.xsl'
	)

-------------------------------------
SELECT * FROM utb_message_template WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)

-- Add New Document Types -----------------------------------
IF NOT EXISTS (SELECT * FROM utb_document_type WHERE [Name] = 'Vehicle Condition Report' )
BEGIN
	DECLARE @iNewDisplayOrder AS INT
	SELECT @iNewDisplayOrder=MAX(DisplayOrder)+1 FROM utb_document_type
	--SELECT @iNewDisplayOrder
	INSERT INTO	utb_document_type VALUES (@iNewDisplayOrder,@iNewDisplayOrder,'C',1,0,'Vehicle Condition Report',0,0,CURRENT_TIMESTAMP)
	INSERT INTO	utb_document_type VALUES (@iNewDisplayOrder+1,@iNewDisplayOrder+1,'C',1,0,'Salvage Bid Report',0,0,CURRENT_TIMESTAMP)

	SELECT 'Message Templates added...'
END
ELSE
BEGIN
	SELECT 'Message Templates already added...'
END