-- Confirmation Script - Conversion script to convert 1.3.4.x to 1.4.0

PRINT 'Check utb_claim for non-primary service channel'

select count(*) AS 'Successful if recs selected = 0'
from utb_claim c 
inner join utb_Claim_Aspect ca
on            c.LynxID = ca.LynxID
where not exists (
select        distinct ca.lynxid
from          utb_Claim_Aspect ca
inner join    utb_Claim_Aspect_Service_Channel casc
on            casc.ClaimAspectID = ca.ClaimAspectID
where casc.primaryflag = 1
and ca.lynxid = c.lynxid)
and ca.ClaimAspectTypeid <> 0


PRINT 'Check utb_claim'

select count(*) AS 'Successful if recs selected = 0'
  FROM dbo.utb_claim c
  LEFT JOIN dbo.Tmp_utb_claim_coverage cc ON (c.LynxID = cc.LynxID)
  WHERE c.ClientClaimNumber <> cc.ClientClaimNumber
 
  
PRINT 'Check utb_claim_coverage'

SELECT ncc.LynxID, Count(*)
  FROM dbo.utb_claim_coverage ncc
  GROUP BY LynxID

SELECT * FROM dbo.utb_claim_coverage ORDER BY LynxID

PRINT 'Check utb_claim_aspect_service_channel (Successful if counts match)'

SELECT Count(*) FROM dbo.Tmp_utb_claim_aspect ca WHERE ca.ClaimAspectTypeID = 9
SELECT Count(*) FROM dbo.utb_claim_aspect_service_channel

SELECT * FROM utb_claim_aspect_status            

--commit
--rollback


