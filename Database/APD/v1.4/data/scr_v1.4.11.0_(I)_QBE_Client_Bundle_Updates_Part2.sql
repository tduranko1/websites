--select * from utb_insurance
--select * from utb_claim WHERE insurancecompanyid = 304 ORDER BY syslastupdateddate desc

DECLARE @iBundlingID INT
SET @iBundlingID = 0

-- RRP - Status Update - Notification

SELECT
	@iBundlingID = b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = 304
	AND b.[Name] = 'RRP - Status Update - Notification'
	
IF EXISTS (SELECT * FROM utb_bundling WHERE BundlingID > 0)
BEGIN
	UPDATE utb_bundling SET [Name] = 'RRP - Status Update - Claim Rep' WHERE BundlingID = @iBundlingID
	
	SELECT 'Updated Successfully...'
END
ELSE
BEGIN
	SELECT 'ERROR: Not Updated Successfully...'
END

-- RRP - Status Update - Action Required

SET @iBundlingID = 0

SELECT  
	@iBundlingID = b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = 304
	AND b.[Name] = 'RRP - Status Update - Action Required'
	
IF EXISTS (SELECT * FROM utb_bundling WHERE BundlingID > 0)
BEGIN
	UPDATE utb_bundling SET [Name] = 'RRP - Status Update - MDS' WHERE BundlingID = @iBundlingID
	
	SELECT 'Updated Successfully...'
END
ELSE
BEGIN
	SELECT 'ERROR: Not Updated Successfully...'
END

--SELECT * FROM utb_bundling b INNER JOIN utb_client_bundling cb ON b.BundlingId = cb.BundlingID WHERE cb.InsuranceCompanyID = 304 AND b.[Name] IN ('RRP - Status Update - Claim Rep','RRP - Status Update - MDS')
