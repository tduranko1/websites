-- Texas Farm ID 184
-- North Carolina Farm ID 327
-- QBE ID 304
-- Virginia Farm ID 192

-- Elephant ID 367OLD new is 374
-- Republic Group ID 277
-- Electric 158
DECLARE @InscCompID SMALLINT

SET @InscCompID = 374

-- Show Bundles by Insc Company
SELECT 'Show Bundles'
SELECT
	b.* 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = @InscCompID
	--AND serviceChannelCD = 'DA'

-- Show Bundle Messages by Insc Company
SELECT 'Show Bundle Messages'
SELECT
	t.* 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @InscCompID
--	AND cb.serviceChannelCD = 'DA'

-- Show Client Bundling by Insc Company
SELECT 'Show Client Bundle Messages'
SELECT
	* 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @InscCompID

-- Show Bundling Document Types by Insc Company
SELECT 'Show Bundling Document Types'

SELECT 
	* 
FROM 
	utb_bundling_document_type bdt
	INNER JOIN utb_document_type dt
	ON dt.DocumentTypeID = bdt.DocumentTypeID 
WHERE 	
	bdt.BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @InscCompID
	)

-- Show Closing Document Bundling items by Insc Company
CREATE TABLE #DocTypeTmp
(
	DocumentTypeID INT
	, DocName VARCHAR(100)
)

INSERT INTO
	#DocTypeTmp
	SELECT DISTINCT
		b.DocumentTypeID 
		, (SELECT [Name] FROM utb_document_type WHERE DocumentTypeID = b.DocumentTypeID ) AS DocName
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
	WHERE
		cb.InsuranceCompanyID = 304	
	ORDER BY
		b.DocumentTypeID

DECLARE	@DocumentTypeID INT
DECLARE @DocName VARCHAR(100)

DECLARE db_cursor CURSOR FOR
	SELECT
		*
	FROM
		#DocTypeTmp

OPEN db_cursor	
FETCH NEXT FROM db_cursor INTO 
		@DocumentTypeID
		, @DocName

WHILE @@FETCH_STATUS = 0  
BEGIN 
	SELECT 'Show Bundling - ' + @DocName + ' Package'	

	SELECT
		b.* 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN #DocTypeTmp dt 
			ON dt.DocumentTypeID = b.DocumentTypeID
	WHERE
		cb.InsuranceCompanyID = @InscCompID
		AND EnabledFlag = 1
		AND b.DocumentTypeID = @DocumentTypeID

FETCH NEXT FROM db_cursor INTO 
		@DocumentTypeID
		, @DocName
END  

CLOSE db_cursor  
DEALLOCATE db_cursor 

--SELECT * FROM #DocTypeTmp

DROP TABLE #DocTypeTmp

