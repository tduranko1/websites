DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 515

----------------------------------------
-- Add Salvage Bid Report Workflow item
----------------------------------------
IF NOT EXISTS (SELECT * FROM utb_task WHERE [Name] = 'Salvage Bid Required' )
BEGIN
	INSERT INTO utb_event VALUES (88,9,113,1,1,'Salvage Bid Required',0,CURRENT_TIMESTAMP)
	INSERT INTO utb_workflow VALUES (188,515,0,88,'E',0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task VALUES (102,13,1,1,NULL,131,1,240,'Salvage Bid Required',0,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_assignment_pool VALUES (102,'PS',515,2,0,CURRENT_TIMESTAMP)
	--utb_task_completion_criteria
	--utb_task_delay_reason
	--utb_task_escalation
	INSERT INTO utb_task_role VALUES (102,6,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (102,7,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (102,9,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (102,10,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (102,21,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (102,22,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (102,23,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (102,24,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_spawn VALUES (188,'PS','%Key%',NULL,1,102,NULL,'T',0,CURRENT_TIMESTAMP)

	-- Status --		
	INSERT INTO utb_status_task SELECT 100, 102,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 101, 102,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 200, 102,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 300, 102,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 400, 102,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 500, 102,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 700, 102,1,0,CURRENT_TIMESTAMP
	
	SELECT 'Workflow Task added...'
END
ELSE
BEGIN
	SELECT 'Workflow Task already added...'
END

------------------------------------------------
-- Add Vehicle Condition Report Workflow item
------------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_task WHERE [Name] = 'Vehicle Condition Report - Request' )
BEGIN
	INSERT INTO utb_event VALUES (89,9,114,1,1,'Vehicle Condition Report - Request',0,CURRENT_TIMESTAMP)
	INSERT INTO utb_workflow VALUES (189,515,0,89,'E',0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task VALUES (103,13,1,1,NULL,132,1,240,'Vehicle Condition Report Required',0,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_assignment_pool VALUES (103,'PS',515,2,0,CURRENT_TIMESTAMP)
	--utb_task_completion_criteria
	--utb_task_delay_reason
	--utb_task_escalation
	INSERT INTO utb_task_role VALUES (103,6,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (103,7,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (103,9,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (103,10,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (103,21,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (103,22,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (103,23,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_task_role VALUES (103,24,1,1,1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_spawn VALUES (189,'PS','%Key%',NULL,1,103,NULL,'T',0,CURRENT_TIMESTAMP)

	-- Status --		
	INSERT INTO utb_status_task SELECT 100, 103,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 101, 103,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 200, 103,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 300, 103,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 400, 103,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 500, 103,1,0,CURRENT_TIMESTAMP
	INSERT INTO utb_status_task SELECT 700, 103,1,0,CURRENT_TIMESTAMP
	
	SELECT 'Workflow Task added...'
END
ELSE
BEGIN
	SELECT 'Workflow Task already added...'
END
                                                                                                                                              