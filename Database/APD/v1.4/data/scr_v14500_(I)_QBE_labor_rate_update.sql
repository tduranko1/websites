--
-- scr_v14500_(III)_QBE_labor_rate_update.sql
--	- update QBE labor rate variance to $4
--

DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime

-- set to QBE (ID: 304)
SET @InsuranceCompanyID = 304
SET @now = CURRENT_TIMESTAMP


-- update max labor rate to min value + 4
UPDATE utb_labor_rate
SET BodyRateMax = BodyRateMin + 4,
	FrameRateMax = FrameRateMin + 4,
	MaterialRateMax = MaterialRateMin + 4,
	MechRateMax = MechRateMin + 4,
	RefinishRateMax = RefinishRateMin + 4,
	SysLastUserID = 0,
	SysLastUpdatedDate = @now
WHERE InsuranceCompanyID = @InsuranceCompanyID