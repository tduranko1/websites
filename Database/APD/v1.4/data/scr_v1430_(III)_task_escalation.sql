/****************************************************************************
* STARS 217793 - Set Close and Bill task for Program shops to expire 
*  immediately
*****************************************************************************/

declare @now as datetime
declare @TaskID as int

set @now = current_timestamp

select @TaskID = TaskID
from utb_task
where Name = 'Close and Bill File'

begin transaction

-- set the "Close and Bill File" for Program shop to expire in 5 minutes
insert into utb_task_escalation
(TaskID, ServiceChannelCD, EscalationMinutes, SysLastUserID, SysLastUpdatedDate)
values
(@TaskID, 'PS', 5, 0, @now)

select * from utb_task_escalation
where TaskID = @TaskID

-- commit
-- rollback

