DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 493
SET @FromInscCompID = 473

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
SELECT 
	* 
FROM 
	utb_bundling_document_type 
WHERE 	
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @ToInscCompID
	)

)
BEGIN
	INSERT INTO 
		utb_bundling_document_type
	SELECT 
		(SELECT sb.BundlingID FROM utb_bundling sb 	INNER JOIN utb_client_bundling cb 
		ON sb.BundlingId = cb.BundlingID WHERE [Name] = b.Name AND InsuranceCompanyID = @ToInscCompID)
		, bdt.DocumentTypeID
		, bdt.ConditionValue
		, DirectionalCD
		, DirectionToPayFlag
		, DuplicateFlag
		, EstimateDuplicateFlag
		, EstimateTypeCD
		, FinalEstimateFlag
		, LossState
		, MandatoryFlag
		, SelectionOrder
		, ShopState
		, VANFlag
		, WarrantyFlag
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
		INNER JOIN utb_bundling b
		ON b.BundlingID = bdt.BundlingID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @FromInscCompID
				AND mt.[Description] IN (
				'RRP - Total Loss Alert (Adjuster)|Messages/msgANRRPTotalLossAlert.xsl'
				, 'RRP - Approved Estimate Shop Notification|Messages/msgANRRPApprovedEstimateShopNotification.xsl'
				, 'RRP - Closing Inspection Complete|Messages/msgANRRPClosingInspectionComplete.xsl'
				, 'RRP - Closing Supplement|Messages/msgANRRPClosingSupplement.xsl'
				, 'RRP - Closing Total Loss|Messages/msgANRRPClosingTotalLoss.xsl'
				, 'RRP - Acknowledgement of Claim|Messages/msgANRRPClmAck.xsl'
				, 'RRP - Assignment Cancellation|Messages/msgANRRPAssignmentCancel.xsl'
				, 'RRP - Status Update - Notification|Messages/msgANRRPStatusUpdateNotificationOnly.xsl'
				, 'RRP - Status Update - Action Required|Messages/msgANRRPStatusUpdateActionRequired.xsl'
				, 'RRP - Stale Alert & Close|Messages/msgANRRPStaleAlertAndClose.xsl'
				, 'RRP - Total Loss Determination (Shop)|Messages/msgANRRPTotalLossDetermination.xsl'
				)
		)
		AND bdt.DocumentTypeID NOT IN (82)

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END
