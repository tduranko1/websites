DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
DECLARE @BundleID SMALLINT
DECLARE @DocumentTypeID SMALLINT

SET @ToInscCompID = 575
SET @FromInscCompID = 194
SET @BundleID = 0
SET @DocumentTypeID = 0

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Rental Invoice' AND cb.InsuranceCompanyID = @ToInscCompID AND DocumentTypeID = 9
)
BEGIN
	-- DRP - Initial Estimate - Estimate
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Initial Estimate and Photos (Adjuster)' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,3,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,1,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,8,NULL,'I',0,1,0,NULL,0,NULL,1,2,NULL,0,0,0,CURRENT_TIMESTAMP)

	-- DRP - Rental Invoice
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Rental Invoice' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,9,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0,CURRENT_TIMESTAMP)

	-- DRP - Status Update - Claim Rep
	--SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Status Update - Claim Rep' AND cb.InsuranceCompanyID = @ToInscCompID
	--INSERT INTO utb_bundling_document_type VALUES (@BundleID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0,CURRENT_TIMESTAMP)

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END
