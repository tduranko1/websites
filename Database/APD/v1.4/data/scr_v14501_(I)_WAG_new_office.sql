/*
	Author/Date: Walter J. Chesla - 4/14/2010
	Description:  This script will create new office config Western AG - Gilbert AZ
	
*/

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = (SELECT InsuranceCompanyID FROM utb_insurance WHERE Name = 'Western Agricultural')


PRINT 'Creating new Western Ag office...'

INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Gilbert',
		'AZ',
        'Gilbert',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '866',                 -- FaxAreaCode
        '421',                 -- FaxExchangeNumber
        '4752',                -- FaxUnitNumber
        'Gilbert',				-- Name
        '800',					-- PhoneAreaCode
        '727',                 -- PhoneExchangeNumber
        '7868',                -- PhoneUnitNumber		
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)


SELECT @OfficeID = SCOPE_IDENTITY()


INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          '4', 
          0,
          @ModifiedDateTime

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          '6', 
          0,
          @ModifiedDateTime

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          '7', 
          0,
          @ModifiedDateTime
          
INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          '8', 
          0,
          @ModifiedDateTime




    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
