-- This script adds the Glass Appraiser to utb_appraisers

DECLARE @AppraiserID  udt_std_id


if not exists (select AppraiserID from utb_appraiser where Name = 'Lynx Services AGC')
begin
  insert into utb_appraiser (Name,
                             SysLastUserID,
                             SysLastUpdatedDate)
  values ('Lynx Services AGC',0,getdate())

  if @@error <> 0
  begin
    print 'Insertion of Lynx Services AGC Appraiser into utb_appraiser failed.'
  end
end
else
begin
  print 'Lynx Services AGC already exists.'
end                                 