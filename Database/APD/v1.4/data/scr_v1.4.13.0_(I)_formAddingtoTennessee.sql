USE [udb_apd]
GO

IF NOT EXISTS(SELECT *
                FROM dbo.utb_form_supplement
                WHERE Name = 'TN Aftermarket Parts Form'  AND ServiceChannelCD = 'PS'
)
BEGIN
      INSERT INTO 
         utb_form_supplement 
      SELECT
            FormID
            , NULL
            , 'TN'
            , 1
            , NULL
            , 'TN Aftermarket Parts Form'
            , 'TN_Aftermarket_Parts_Form.pdf'
            , 'PS'
            , ''
            , 0
            , CURRENT_TIMESTAMP 
      FROM 
            utb_form_supplement 
      WHERE 
            FormID IN 
            (
                  SELECT 
                        FormID 
                  FROM 
                        utb_form
                  WHERE
                        [name] = 'Shop Assignment'
            )
            AND [Name] = 'Shop Instructions'

      SELECT 'TN Aftermarket Parts Form added...'
END
ELSE
BEGIN
      SELECT 'TN Aftermarket Parts Form already added...'
END

