/*******************************************************************
* Add CT - Acknowledgement of Claimant - CONSUMER CHOICE Vehicle 
* Repair Shop form to all shop assignments
*******************************************************************/

insert into utb_form_supplement
(
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    ServiceChannelCD,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
)
select FormID, 
        'CT', 
        1, 
        'CTConsumerChoice',
        'CTAoClaimantConsumerChoice.pdf',
        'PS',
        '',
        0,
        current_timestamp
from utb_form
where InsuranceCompanyID is not null
  and Name = 'Shop Assignment'
