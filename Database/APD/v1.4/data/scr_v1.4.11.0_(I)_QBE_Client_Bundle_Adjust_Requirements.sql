DECLARE @iBundlingID INT
SET @iBundlingID = 0

IF EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_bundling_document_type 
	WHERE 	
		BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = 304
				AND b.[Name] = 'SHOP: RRP - Approved Estimate Shop Notification'
		)
)

BEGIN
	DELETE 
	FROM 
		utb_bundling_document_type 
	WHERE 	
		BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = 304
				AND b.[Name] = 'SHOP: RRP - Approved Estimate Shop Notification'
		)
		
	SELECT 'Required Document Deleted...'
END
ELSE
BEGIN
	SELECT 'Required Document Already Deleted...'
END

DECLARE @iDocumentTypeID INT
SET @iDocumentTypeID = 0

IF EXISTS (SELECT * FROM utb_document_type WHERE name = 'Approved Estimate Shop Notification')
BEGIN
	SELECT @iDocumentTypeID = DocumentTypeID FROM utb_document_type WHERE name = 'Approved Estimate Shop Notification'
	UPDATE utb_bundling SET DocumentTypeID = @iDocumentTypeID WHERE name = 'SHOP: RRP - Approved Estimate Shop Notification'
	SELECT 'Document Type Changed...'
END
ELSE
BEGIN
	SELECT 'Document Type Already Changed...'
END


