--
-- TABLE INSERT STATEMENTS
--
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 1, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 2, 46, 0, 0, 1, 0, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 3, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 4, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 5, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 6, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 7, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 8, 46, 0, 0, 0, 0, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 9, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 10, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 11, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 12, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 13, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 14, 46, 0, 0, 0, 0, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 15, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 16, 46, 0, 0, 1, 0, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 17, 46, 0, 0, 0, 0, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 18, 46, 0, 0, 0, 0, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 19, 46, 0, 0, 1, 0, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 20, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 21, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 22, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 23, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_role_permission ( RoleID, PermissionID, CreateFlag, DeleteFlag, ReadFlag, UpdateFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 24, 46, 1, 1, 1, 1, 0, CURRENT_TIMESTAMP ) 
go

UPDATE utb_role_permission
   SET CreateFlag = 1,
       DeleteFlag = 1
 WHERE UpdateFlag = 1
   AND PermissionID = 4