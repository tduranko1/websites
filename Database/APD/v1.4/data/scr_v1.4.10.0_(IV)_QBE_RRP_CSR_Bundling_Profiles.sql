/******************************************************************************
* QBE RRP Bundles for CSR
******************************************************************************/

DECLARE @NOW as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int

SET @NOW = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
  FROM utb_insurance
 WHERE Name = 'QBE Insurance'
 
IF @InsuranceCompanyID IS NULL
BEGIN
    RAISERROR('Unable to determine QBE InsuranceCompanyID', 16, 1)
    RETURN
END


-- Start a transaction
BEGIN TRANSACTION




------------------------------------------------------------------
-- Acknowledgement of Claim
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Acknowledgement of Claim|Messages/msgRRPClmAck.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 36, @MessageTemplateID, 1, 'RRP - Acknowledgement of Claim', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Use the Insurance company level Return Document settings.
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   NULL, 
   NULL,
   NULL,
   0,
   @now





------------------------------------------------------------------
-- Missing ProcessClaims Assignment
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Missing ProcessClaims Assignment|Messages/msgRRPMissingProcessClaimAssgn.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 36, @MessageTemplateID, 1, 'RRP - Missing ProcessClaims Assignment', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Use the Insurance company level Return Document settings.
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   NULL, 
   NULL,
   NULL,
   0,
   @now





------------------------------------------------------------------
-- Assignment Cancellation
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Assignment Cancellation|Messages/msgRRPAssignmentCancel.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 36, @MessageTemplateID, 1, 'RRP - Assignment Cancellation', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Use the Insurance company level Return Document settings.
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   NULL, 
   NULL,
   NULL,
   0,
   @now





------------------------------------------------------------------
-- Status Update - Notification
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Status Update - Notification|Messages/msgRRPStatusUpdateNotificationOnly.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 36, @MessageTemplateID, 1, 'RRP - Status Update - Notification', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Use the Insurance company level Return Document settings.
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   NULL, 
   NULL,
   NULL,
   0,
   @now





------------------------------------------------------------------
-- Status Update - Action Required
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Status Update - Action Required|Messages/msgRRPStatusUpdateActionRequired.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 36, @MessageTemplateID, 1, 'RRP - Status Update - Action Required', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Use the Insurance company level Return Document settings.
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   NULL, 
   NULL,
   NULL,
   0,
   @now





------------------------------------------------------------------
-- Stale Alert & Close
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Stale Alert & Close|Messages/msgRRPStaleAlertAndClose.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'RRP - Stale Alert & Close', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add the documents to the bundling profile
-- Add Memo Bill
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    75,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    NULL,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   'PDF', 
   'EML',
   'qbeapd@lynxservices.com',
   0,
   @now


SELECT * from utb_message_template
SELECT * FROM utb_bundling WHERE BundlingID = @BundlingID
SELECT * from utb_client_bundling WherE InsuranceCompanyID = @InsuranceCompanyID

-- commit
-- rollback
