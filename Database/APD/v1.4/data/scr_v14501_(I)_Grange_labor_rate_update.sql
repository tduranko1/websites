--
-- scr_v14500_(III)_QBE_labor_rate_update.sql
--	- update QBE labor rate variance to $4
--

DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime

-- set to Grange (ID: 289)
SET @InsuranceCompanyID = 289
SET @now = CURRENT_TIMESTAMP


-- update max labor rate to min value + 4
UPDATE utb_labor_rate
SET BodyRateMax = 999,
	FrameRateMax = 999,
	MaterialRateMax = 999,
	MechRateMax = 999,
	RefinishRateMax = 999,
	SysLastUserID = 0,
	SysLastUpdatedDate = @now
WHERE InsuranceCompanyID = @InsuranceCompanyID
	AND StateCode = 'CA'