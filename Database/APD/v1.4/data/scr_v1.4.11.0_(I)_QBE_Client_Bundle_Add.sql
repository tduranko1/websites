DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 304

------------------------------
-- Add New Doc Types
------------------------------
IF NOT EXISTS (SELECT * FROM utb_document_type WHERE [Name] LIKE '%Total Loss Determination%') 
BEGIN
INSERT INTO
	utb_document_type
	SELECT
		(SELECT MAX(DocumentTypeID)+1 FROM utb_document_type)
		, (SELECT MAX(DisplayOrder)+1 FROM utb_document_type)
		, DocumentClassCD
		, EnabledFlag	
		, EstimateTypeFlag
		, 'Estimate Revision Shop Request'
		, 1
		, 0
		, CURRENT_TIMESTAMP
	FROM
		utb_document_type
	WHERE 
		[Name] = 'Status Update'
	
INSERT INTO
	utb_document_type
	SELECT 
		(SELECT MAX(DocumentTypeID)+1 FROM utb_document_type)
		, (SELECT MAX(DisplayOrder)+1 FROM utb_document_type)
		, DocumentClassCD
		, EnabledFlag	
		, EstimateTypeFlag
		, 'Total Loss Determination'
		, 1
		, 0
		, CURRENT_TIMESTAMP
	FROM
		utb_document_type
	WHERE 
		[Name] = 'Status Update'		
	
	SELECT 'New document types - Added...'
END
ELSE
BEGIN
	SELECT 'New document types - Already exist...'
END


------------------------------
-- Add Message Template
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%RRP - Total Loss Determination%') 
BEGIN
	INSERT INTO utb_message_template
	SELECT
		AppliesToCD
		, 1
		, 'RRP - Total Loss Determination (Shop)|Messages/msgRRPTotalLossDetermination.xsl'
		, ServiceChannelCD
		, 0 
		, CURRENT_TIMESTAMP 
	FROM
		utb_message_template 
	WHERE 
		Description = 'RRP - Stale Alert & Close|Messages/msgRRPStaleAlertAndClose.xsl'

	INSERT INTO utb_message_template
	SELECT
		AppliesToCD
		, 1
		, 'RRP - Estimate Revision Shop Request|Messages/msgRRPEstimateRevisionShopRequest.xsl'
		, ServiceChannelCD
		, 0 
		, CURRENT_TIMESTAMP 
	FROM
		utb_message_template 
	WHERE 
		Description = 'RRP - Stale Alert & Close|Messages/msgRRPStaleAlertAndClose.xsl'

	SELECT 'New message templates - Added...'
END
ELSE
BEGIN
	SELECT 'New message templates - Already exist...'
END

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_bundling b INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE [Description] LIKE '%RRP - Total Loss Determination (Shop)%') 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID from utb_document_type WHERE [Name] = 'Total Loss Determination')
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] = 'RRP - Total Loss Determination (Shop)|Messages/msgRRPTotalLossDetermination.xsl') 
		,1
		,'RRP - Total Loss Determination (Shop)'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgRRPTotalLossDetermination.xsl%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID from utb_document_type WHERE [Name] = 'Estimate Revision Shop Request')
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] = 'RRP - Estimate Revision Shop Request|Messages/msgRRPEstimateRevisionShopRequest.xsl') 
		,1
		,'RRP - Estimate Revision Shop Request'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgRRPStaleAlertAndClose.xsl%'
				
	SELECT 'Bundles added...'
END
ELSE
BEGIN
	SELECT 'Bundles already added...'
END

------------------------------
-- Add Client Bundling
------------------------------
IF NOT EXISTS (
	SELECT 
		* 
	FROM 
		utb_bundling b 
		INNER JOIN utb_client_bundling cb
		ON cb.BundlingID = b.BundlingID
		INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		[Description] LIKE '%msgRRPTotalLossDetermination.xsl%' 
		AND cb.InsuranceCompanyID = 304
	)
BEGIN
	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgRRPTotalLossDetermination.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgRRPEstimateRevisionShopRequest.xsl%'

END

