declare @TaskID as int
declare @AssignmentPoolID as int

select @TaskID = TaskID
from utb_task
where name = 'Close and Bill File'

select @AssignmentPoolID = AssignmentPoolID
from utb_assignment_pool
where Name = 'File Analyst'

begin transaction

update utb_task_assignment_pool
set AssignmentPoolID = @AssignmentPoolID
where TaskID = @TaskID
  and ServiceChannelCD = 'DR'

-- select the affected tables

select * from utb_task_assignment_pool
where TaskID = @TaskID
  and ServiceChannelCD = 'DR'


-- commit
-- rollback