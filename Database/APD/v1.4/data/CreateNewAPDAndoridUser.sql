DECLARE @iUserID INT
SET @iUserID = 0

IF NOT EXISTS (select UserID from utb_user where EmailAddress = 'APDAndroidUser@lynxservices.com')
BEGIN
	INSERT INTO utb_user VALUES (NULL, NULL, NULL, NULL, NULL, 'APDAndroidUser@lynxservices.com', 0, NULL, NULL, NULL, NULL, NULL, 'APDAndroid', 'User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 412, 111, NULL, 1234, 0, 0, CURRENT_TIMESTAMP, NULL, NULL, CURRENT_TIMESTAMP)
	SELECT @iUserID=UserID FROM utb_user WHERE EmailAddress = 'APDAndroidUser@lynxservices.com'

	IF @iUserID > 0 
	BEGIN
		INSERT INTO utb_user_application VALUES (@iUserID, 1, CURRENT_TIMESTAMP, NULL,	'AUTOCSR' + CONVERT(VARCHAR(20),@iUserID), NULL, NULL, 0, CURRENT_TIMESTAMP)
		INSERT INTO utb_user_role VALUES (@iUserID,	6, 1, 0, CURRENT_TIMESTAMP)
		SELECT 'User Created Successfully...'
	END
	ELSE
	BEGIN
		SELECT 'User Creation FAILED...'
	END
END
ELSE
BEGIN
	SELECT 'User Already Created...'
END

IF NOT EXISTS (select UserID from utb_user where EmailAddress = 'customerservice@hyperquest.com')
BEGIN
	INSERT INTO utb_user VALUES (NULL, NULL, NULL, NULL, NULL, 'customerservice@hyperquest.com', 0, NULL, NULL, NULL, NULL, NULL, 'Hyperquest Customer', 'Service', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 847, 499, NULL, 7400, 0, 0, CURRENT_TIMESTAMP, NULL, NULL, CURRENT_TIMESTAMP)
	SELECT @iUserID=UserID FROM utb_user WHERE EmailAddress = 'customerservice@hyperquest.com'

	IF @iUserID > 0 
	BEGIN
		INSERT INTO utb_user_application VALUES (@iUserID, 1, CURRENT_TIMESTAMP, NULL,	'HQCustService' + CONVERT(VARCHAR(20),@iUserID), NULL, NULL, 0, CURRENT_TIMESTAMP)
		INSERT INTO utb_user_role VALUES (@iUserID,	6, 1, 0, CURRENT_TIMESTAMP)
		SELECT 'User Created Successfully...'
	END
	ELSE
	BEGIN
		SELECT 'User Creation FAILED...'
	END
END
ELSE
BEGIN
	SELECT 'User Already Created...'
END