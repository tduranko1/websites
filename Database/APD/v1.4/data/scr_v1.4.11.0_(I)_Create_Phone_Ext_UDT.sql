EXEC sp_addtype [udt_ph_extension_number_long], 'varchar(7)', 'NOT NULL'
GO
ALTER TABLE [dbo].[utb_user] ALTER COLUMN [PhoneExtensionNumber] [udt_ph_extension_number_long] NULL
GO