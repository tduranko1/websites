-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configurations for AOI
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @PaymentCompanyID       smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 288
SET @PaymentCompanyID = 832


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'


PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

--Insert Insurance Company information.

IF NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
    INSERT INTO dbo.utb_insurance
        (
         InsuranceCompanyID, 
         DeskAuditPreferredCommunicationMethodID,
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip, 
         AssignmentAtSelectionFlag,
         BillingModelCD, 
         BusinessTypeCD,
         CarrierLynxContactPhone,
         CFLogoDisplayFlag,
         ClaimPointCarrierRepSelFlag,
         ClaimPointDocumentUploadFlag, 
         ClientAccessFlag,
         DemoFlag,
         DeskAuditCompanyCD,
         DeskReviewLicenseReqFlag, 
         EnabledFlag,
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         FedTaxId,
         IngresAccountingId,
         InvoicingModelPaymentCD, 
         InvoiceMethodCD,
         LicenseDeterminationCD, 
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocDestinationCD,
         ReturnDocPackageTypeCD,
         ReturnDocRoutingCD, 
         TotalLossValuationWarningPercentage, 
         TotalLossWarningPercentage, 
         WarrantyPeriodRefinishMinCD,
         WarrantyPeriodWorkmanshipMinCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,      -- InsuranceCompanyID
                14,                       -- DeskAuditPreferredCommunicationMethodID
                '6101 Anacapri Blvd',       -- Address1
                '',                       -- Address2
                'Lansing',            -- AddressCity
                'MI',                     -- AddressState
                '48909',                  -- AddressZip
                0,                        -- AssignmentAtSelectionFlag
                'E',                      -- BillingModelCD
                'C',                      -- BusinessTypeCD
                NULL,                     -- CarrierLynxContactPhone
                0,                        -- CFLogoDisplayFlag
                0,                        -- ClaimPointCarrierRepSelFlag
                1,                        -- ClaimPointDocumentUploadFlag
                1,                        -- ClientAccessFlag
                0,                        -- DemoFlag
                'AOI',                    -- DeskAuditCompanyCD
                1,                        -- DeskReviewLicenseReqFlag
                1,                        -- EnabledFlag
                '517',                    -- FaxAreaCode
                '323',                    -- FaxExchangeNumber
                '8796',                   -- FaxUnitNumber
                NULL,                     -- FedTaxId
                @PaymentCompanyID,        -- IngresAccountingId
                'C',                      -- InvoicingModelPaymentCD (Bulk / per claim)
                'P',                      -- InvoiceMethodCD
                'LOSS',                   -- LicenseDeterminationCD
                'Auto-Owners Insurance',  -- Name
                '517',                    -- PhoneAreaCode
                '323',                    -- PhoneExchangeNumber
                '1565',                   -- PhoneUnitNumber
                'REP',                    -- ReturnDocDestinationCD
                'PDF',                    -- ReturnDocPackageTypeCD
                'EML',                    -- ReturnDocRoutingCD
                0.75,                     -- TotalLossValuationWarningPercentage
                0.75,                     -- TotalLossWarningPercentage
                '99',                     -- WarrantyPeriodRefinishMinCD
                '99',                     -- WarrantyPeriodWorkmanshipMinCD
                0,                        -- SysLastUserID
                @ModifiedDateTime         -- SysLastUpdatedDate
        )
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already exists in utb_insurance', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END


--Insert Client level configuration

------------------------------------------------------

INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Demand Estimate Audit')

------------------------------------------------------

INSERT INTO dbo.utb_client_service_channel
SELECT @InsuranceCompanyID,
       ServiceChannelDefaultCD,
       0,       -- ClientAuthorizesPaymentFlag
       'C',     -- InvoicingModelBillingCD
       0,
       @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Demand Estimate Audit')


------------------------------------------------------

INSERT INTO dbo.utb_client_contract_state
SELECT  @InsuranceCompanyID,
        StateCode,
        0,                   -- UseCEIShopFlag
        0,                   -- SysLastUserID
        @ModifiedDateTime
  FROM  dbo.utb_state_code
  WHERE EnabledFlag = 1


------------------------------------------------------

INSERT INTO dbo.utb_client_coverage_type (InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        AdditionalCoverageFlag,
        ClientCode,
        CoverageProfileCD,
        DisplayOrder,
        1,
        Name,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_client_coverage_type
  WHERE InsuranceCompanyID = 145    -- Clone Zurich's configuration


------------------------------------------------------    
    
INSERT INTO dbo.utb_client_claim_aspect_type 
SELECT  @InsuranceCompanyID,
        ClaimAspectTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_claim_aspect_type
  WHERE Name IN ('Claim', 'Assignment', 'Vehicle', 'Fax Assignment')

------------------------------------------------------        
    
INSERT INTO dbo.utb_client_payment_type VALUES (@InsuranceCompanyID, 'I', 0, @ModifiedDateTime)

------------------------------------------------------

INSERT INTO dbo.utb_client_report (InsuranceCompanyID, OfficeID, ReportID, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        NULL,
        ReportID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_report 
  WHERE ReportID IN (1, 2, 3, 4, 5, 6, 7, 12, 13)

------------------------------------------------------

-- Create Office(s)

-- Auto Owners Insurance

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '6101 Anacapri Blvd',   -- Address1
                '',                     -- Address2
                'Lansing',				-- AddressCity
                'MI',                   -- AddressState
                '48909',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Auto-Owners Insurance',-- ClientOfficeId
                1,                      -- EnabledFlag
                NULL,                   -- FaxAreaCode
                NULL,                   -- FaxExchangeNumber
                NULL,                   -- FaxUnitNumber
                'P.O. Box 30660',       -- MailingAddress1
                NULL,                   -- MailingAddress2
                'Lansing',				-- MailingAddressCity
                'MI',                   -- MailingAddressState
                '48909',                -- MailingAddressZip
                'Auto-Owners Insurance',-- Name
                '517',                  -- PhoneAreaCode
                '323',                  -- PhoneExchangeNumber
                '1565',                 -- PhoneUnitNumber
                NULL,                   -- ReturnDocEmailAddress
                NULL,                   -- ReturnDocFaxAreaCode
                NULL,                   -- ReturnDocFaxExchangeNumber
                NULL,                   -- ReturnDocFaxUnitNumber
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    WHERE EnabledFlag = 1



-- Review affected tables
SELECT * FROM dbo.utb_insurance
SELECT * FROM dbo.utb_client_assignment_type
SELECT * FROM dbo.utb_client_claim_aspect_type
SELECT * FROM dbo.utb_client_contract_state
SELECT * FROM dbo.utb_client_payment_type
SELECT * FROM dbo.utb_client_report
SELECT * FROM dbo.utb_office
SELECT * FROM dbo.utb_office_assignment_type
SELECT * FROM dbo.utb_office_contract_state

-- This script requires a fresh on the workflow and spawn reference tables from the access file

--commit 
--rollback
