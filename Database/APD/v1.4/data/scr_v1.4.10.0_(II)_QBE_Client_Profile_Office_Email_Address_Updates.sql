/******************************************************************************
* QBE Office Return Document email address updates
******************************************************************************/

DECLARE @InsuranceCompanyID AS int

SELECT @InsuranceCompanyID = InsuranceCompanyID
  FROM utb_insurance
 WHERE Name = 'QBE Insurance'
 
IF @InsuranceCompanyID IS NULL
BEGIN
    RAISERROR('Unable to determine QBE InsuranceCompanyID', 16, 1)
    RETURN
END

-- Update the General office return document email address
UPDATE utb_office
SET ReturnDocEmailAddress = 'ClaimMailGC@ us.qbe.com'
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND Name = 'General'

-- Update the Unigard office return document email address
UPDATE utb_office
SET ReturnDocEmailAddress = 'claim.appraisal@us.qbe.com'
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND Name = 'Unigard Office'

-- Update the NFU office return document email address
UPDATE utb_office
SET ReturnDocEmailAddress = 'ClaimMailGC@ us.qbe.com'
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND Name = 'National Farmers Union'
  
