DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 158

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%ELECPS%' )
BEGIN
	INSERT INTO utb_message_template SELECT 'C',1,'Assignment Cancellation|Messages/msgELECPSAssignmentCancel.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'No Inspection (Stale) Alert & Close|Messages/msgELECPSStaleAlert.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Cash Out Alert & Close|Messages/msgELECPSCashOutAlert.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Status Update - Claim Rep|Messages/msgELECPSStatusUpdAction.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Status Update - MDS|Messages/msgELECPSStatusUpdActionMDS.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Total Loss Alert - (Adjuster)|Messages/msgELECPSTLAlert.xsl','PS',0,CURRENT_TIMESTAMP
	--INSERT INTO utb_message_template SELECT 'C',1,'Initial Estimate|Messages/msgELECPSInitialEstimate.xsl','PS',0,CURRENT_TIMESTAMP
	
	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

