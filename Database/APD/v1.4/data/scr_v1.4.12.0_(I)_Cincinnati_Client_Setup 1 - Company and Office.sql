-- Cincinnati Insurance

-- Update Company Info
--SELECT * FROM utb_insurance WHERE InsuranceCompanyID = 194
UPDATE 
	utb_insurance 
SET 
	Address1='6200 S. Gilmore Road'
	, AddressCity='Fairfield'
	, AddressZip='45014'
	, PhoneUnitNumber='2000'
	, EarlyBillFlag=0
	, Name='Cincinnati Insurance' 
	, TotalLossValuationWarningPercentage=.65
	, TotalLossWarningPercentage=.65	
	, DeskAuditPreferredCommunicationMethodID=14
	, FaxAreaCode=NULL
	, FaxExchangeNumber=NULL
	, FaxUnitNumber=NULL
	, ReturnDocDestinationCD='REP'
	, ReturnDocPackageTypeCD='PDF'
	, ReturnDocRoutingCD='EML'
	, SysLastUpdatedDate=CURRENT_TIMESTAMP
WHERE 
	InsuranceCompanyID = 194

-- Update Office Info
--SELECT * FROM utb_office_contract_state WHERE OfficeID IN (SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = 194 AND ClientOfficeId='CIN')
DELETE FROM utb_office_contract_state WHERE OfficeID IN (SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = 194 AND ClientOfficeId='CIN')

--SELECT * FROM	utb_office_assignment_type WHERE OfficeID IN (SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = 194 AND ClientOfficeId='CIN')
DELETE FROM	utb_office_assignment_type WHERE OfficeID IN (SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = 194 AND ClientOfficeId='CIN')
	
-- SELECT * FROM utb_office WHERE InsuranceCompanyID = 194
DELETE FROM utb_office WHERE InsuranceCompanyID = 194 AND ClientOfficeId='CIN'

-- Add/Update Office
IF EXISTS (SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = 194 AND ClientOfficeId = 'Cincinnati HQ')
BEGIN
	DECLARE @iOfficeID INT
	SELECT @iOfficeID = OfficeID FROM utb_office WHERE InsuranceCompanyID = 194 AND ClientOfficeId = 'Cincinnati HQ'
	UPDATE 
		utb_office 
	SET   
		Address1='6200 S. Gilmore Road'
		, AddressCity='Fairfield'
		, AddressZip='45014'
		, PhoneUnitNumber='2000'
		, FaxAreaCode=NULL
		, FaxExchangeNumber=NULL
		, FaxUnitNumber=NULL
		, SysLastUpdatedDate=CURRENT_TIMESTAMP
	WHERE 
		OfficeID = @iOfficeID
END

