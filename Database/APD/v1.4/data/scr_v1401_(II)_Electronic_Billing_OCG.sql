/*************************************************
*  turn off electronic billing for OCG per Jonathan Perrigo (TrackIT WO 186372).
*
****************************************************/

begin transaction

select * from utb_client_service_channel
where insuranceCompanyID = 154

select * from utb_insurance where insuranceCompanyID = 154

update utb_client_service_channel
set ClientAuthorizesPaymentFlag = 0
where InsuranceCompanyID = 154

update utb_insurance
set InvoiceMethodCD = 'P'
where InsuranceCompanyID = 154

select * from utb_client_service_channel
where insuranceCompanyID = 154

select * from utb_insurance where insuranceCompanyID = 154

-- commit
-- rollback
