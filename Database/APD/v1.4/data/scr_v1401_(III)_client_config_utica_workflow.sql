
declare @now as datetime

set @now = current_timestamp

begin transaction

insert into dbo.utb_workflow
(WorkflowID, InsuranceCompanyID, IgnoreDefaultFlag, OriginatorID, OriginatorTypeCD, SysLastUserID, SysLastUpdatedDate)
VALUES
(137, 261, 0, 70, 'E', 0, @now)

insert into dbo.utb_spawn
( WorkflowID, ConditionalValue, ContextNote, EnabledFlag, SpawningID, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate)
VALUES
(137, 'PS', '%PertainsToName%', 1, 6, 'T', 0, @now)

insert into dbo.utb_spawn
( WorkflowID, ConditionalValue, ContextNote, EnabledFlag, SpawningID, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate)
VALUES
( 137, 'RENTAL', NULL, 1, 24, 'T', 0, @now)

insert into dbo.utb_spawn
( WorkflowID, ConditionalValue, ContextNote, EnabledFlag, SpawningID, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate)
VALUES
( 137, 'FNOL', NULL, 1, 2, 'T', 0, @now)

insert into dbo.utb_spawn
( WorkflowID, ConditionalValue, ContextNote, EnabledFlag, SpawningID, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate)
VALUES
( 137, 'DA', NULL, 1, 21, 'T', 0, @now)

insert into dbo.utb_spawn
( WorkflowID, ConditionalValue, ContextNote, EnabledFlag, SpawningID, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate)
VALUES
( 137, 'DA', NULL, 1, 17, 'T', 0, @now)

-- commit
-- rollback