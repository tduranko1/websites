/****************************************************************************
* Update the Total Loss Warning Percentage to 50%
_____________________________________________ 
From: 	Harlacher, Tom  
Sent:	Friday, December 19, 2008 9:42 AM
To:	Vishegu, Ramesh
Cc:	Bennett, Jim
Subject:	FW: VAFB guideline change

Ramesh,

What is our current threshold for VAFB total loss� Looks like it should be 50%. If this has not been changed, can we revise ASAP?

Tom

______________________________________________ 
From: 	Bennett, Jim  
Sent:	Wednesday, October 15, 2008 12:03 PM
To:	Harlacher, Tom
Subject:	FW: VAFB guideline change



______________________________________________ 
From: 	Smith, Gary Bob  
Sent:	Wednesday, October 15, 2008 12:02 PM
To:	Lackey, Linda; Bennett, Jim
Subject:	VAFB guideline change

They've requested a change to their total loss guidelines before repairs begin.
There have been a couple of situations recently where the loss was estimated at 
60-70% then after supplements the final repair ended up at 80% of ACV.  Mike 
Cook says the salvage market is strong right now and he wants to take advantage 
of it.  Changing to 50% threshold doesn't mean they will not repair at 55% or 
60%, but it will alert them to manage more closely.  
Please change the threshold to 50%.  Thanks.

****************************************************************************/
begin transaction

update utb_insurance
set TotalLossWarningPercentage = 0.5 -- old value 0.7
where InsuranceCompanyID = 192

select * from utb_insurance where InsuranceCompanyID = 192

-- commit
-- rollback
