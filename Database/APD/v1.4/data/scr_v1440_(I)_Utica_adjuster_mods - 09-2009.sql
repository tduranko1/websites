
BEGIN TRANSACTION

-- Setup users for MARO office - 92


update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Kenneth.Couch@uticanational.com'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Stephanie.Doyle@uticanational.com',
    @NameFirst             = 'Stephanie',
    @NameLast              = 'Doyle',
    @EmailAddress          = 'Stephanie.Doyle@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6657'

-- Setup users for Albany office - 94

-- Setup users for Amherst office - 95

-- Setup users for ERO office - 96

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Dennis.Brenon@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Theresa.Anson@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Debra.Fairbrother@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Annette.Rivera@uticanational.com'

-- Name Change

update dbo.utb_user
   set namelast = 'Bussonnais',
       emailaddress = 'Marie.Bussonnais@uticanational.com'
where emailaddress = 'Marie.Merrick@uticanational.com'

update dbo.utb_user_application
set logonid = 'Marie.Bussonnais@uticanational.com'
where logonid = 'Annette.Rivera@uticanational.com'


-- Setup users for Fast Track office - 97

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Kari.Herman@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Wendy.Hake@uticanational.com'

-- Setup users for NYMRO office - 98

-- Setup users for NERO office - 101

print 'Done.'

-- COMMIT
-- rollback