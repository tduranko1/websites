DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 464
SET @FromInscCompID = 192

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgCWPS%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'PS' THEN 
				REPLACE(mt.[Description],'msgPS','msgCWPS')
			WHEN 'DA' THEN REPLACE(mt.[Description],'msgDA','msgCWDA')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name NOT IN ('Initial Bill Estimate - Early Billing')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'PS' THEN 
			    REPLACE(mt.[Description],'msgPS','msgCWPS')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('Repair Complete - Release and Close')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'PS' THEN 
			    REPLACE(mt.[Description],'msgPS','msgCWPS')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('Repair Complete - Combined Bill, Release and Close')	

	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

SELECT * FROM utb_message_template WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)

