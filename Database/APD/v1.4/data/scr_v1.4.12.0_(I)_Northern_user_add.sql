DECLARE @iNewUserID INT
DECLARE @iOffice INT
DECLARE @vNewUserEmail VARCHAR(50)
DECLARE @vNewUserFirstName VARCHAR(50)
DECLARE @vNewUserLastName VARCHAR(50)
DECLARE @vOfficeName VARCHAR(50)

SET @iNewUserID = 0
SET @iOffice = 0
SET @vNewUserEmail = 'NortherUser@lynxservices.com.com'
SET @vNewUserFirstName = 'Northern'
SET @vNewUserLastName = 'User'
SET @vOfficeName = 'Northern Neck'

--SELECT OfficeID FROM utb_office WHERE ClientOfficeID = @vOfficeName

IF NOT EXISTS 
(
	SELECT * FROM utb_user WHERE emailaddress = @vNewUserEmail
)
BEGIN
	SELECT @iOffice=OfficeID FROM utb_office WHERE ClientOfficeID = @vOfficeName

	INSERT INTO utb_user VALUES
	( 
		@iOffice
		, NULL
		, NULL
		, NULL
		, NULL
		, @vNewUserEmail
		, 1
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, @vNewUserFirstName
		, @vNewUserLastName
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 0
		, 0
		, CURRENT_TIMESTAMP
		, NULL
		, NULL
		, CURRENT_TIMESTAMP
	)

	SELECT 'New User Added...'

	SELECT @iNewUserID = UserID FROM utb_user WHERE emailaddress = @vNewUserEmail

	IF (@iNewUserID > 0)
	BEGIN
		INSERT INTO utb_user_application VALUES (@iNewUserID,1,CURRENT_TIMESTAMP,NULL, @vNewUserEmail, NULL, NULL,0,CURRENT_TIMESTAMP)
		INSERT INTO utb_user_application VALUES (@iNewUserID,2,CURRENT_TIMESTAMP,NULL, @vNewUserEmail, NULL, NULL,0,CURRENT_TIMESTAMP)
		INSERT INTO utb_user_role VALUES (@iNewUserID,17,1,0,CURRENT_TIMESTAMP)
	END
END
ELSE
BEGIN
	SELECT 'User Already Exists...'
END

