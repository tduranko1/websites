
BEGIN TRANSACTION

-- Setup users for MARO office - 92

-- Setup users for Albany office - 94

-- Setup users for Amherst office - 95

-- Setup users for ERO office - 96

-- Setup users for Fast Track office - 97

-- Setup users for NERO office - 101

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'CHARLIE.BENNETT@uticanational.com',
    @NameFirst             = 'Charlie',
    @NameLast              = 'Bennett',
    @EmailAddress          = 'CHARLIE.BENNETT@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '7214'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Peggy.Coyne@uticanational.com',
    @NameFirst             = 'Peggy',
    @NameLast              = 'Coyne',
    @EmailAddress          = 'Peggy.Coyne@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2697'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'JEAN.CUNNINGHAM@uticanational.com',
    @NameFirst             = 'JEAN',
    @NameLast              = 'CUNNINGHAM',
    @EmailAddress          = 'JEAN.CUNNINGHAM@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '7243'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'JAMES.DONOVAN@uticanational.com',
    @NameFirst             = 'JAMES',
    @NameLast              = 'DONOVAN',
    @EmailAddress          = 'JAMES.DONOVAN@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2663'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Pamela.Dugar@uticanational.com',
    @NameFirst             = 'Pamela',
    @NameLast              = 'Dugar',
    @EmailAddress          = 'Pamela.Dugar@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2648'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'WENDY.GARCIA@uticanational.com',
    @NameFirst             = 'WENDY',
    @NameLast              = 'GARCIA',
    @EmailAddress          = 'WENDY.GARCIA@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2629'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'JANE.GIACHINTA@uticanational.com',
    @NameFirst             = 'JANE',
    @NameLast              = 'GIACHINTA',
    @EmailAddress          = 'JANE.GIACHINTA@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2666'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Nancie.Glazer@uticanational.com',
    @NameFirst             = 'Nancie',
    @NameLast              = 'Glazer',
    @EmailAddress          = 'Nancie.Glazer@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '7218'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Michael.Hallion@uticanational.com',
    @NameFirst             = 'Michael',
    @NameLast              = 'Hallion',
    @EmailAddress          = 'Michael.Hallion@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2673'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Linda.Hourihan@uticanational.com',
    @NameFirst             = 'Linda',
    @NameLast              = 'Hourihan',
    @EmailAddress          = 'Linda.Hourihan@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2718'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'MELISSA.HURLEY@uticanational.com',
    @NameFirst             = 'MELISSA',
    @NameLast              = 'HURLEY',
    @EmailAddress          = 'MELISSA.HURLEY@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2662'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'SANDRA.LEWIS@uticanational.com',
    @NameFirst             = 'SANDRA',
    @NameLast              = 'LEWIS',
    @EmailAddress          = 'SANDRA.LEWIS@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '7217'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Brian.Lordan@uticanational.com',
    @NameFirst             = 'Brian',
    @NameLast              = 'Lordan',
    @EmailAddress          = 'Brian.Lordan@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2661'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'LISA.MACCINI-BARR@uticanational.com',
    @NameFirst             = 'LISA',
    @NameLast              = 'MACCINI-BARR',
    @EmailAddress          = 'LISA.MACCINI-BARR@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2670'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'GARY.MORAN@uticanational.com',
    @NameFirst             = 'GARY',
    @NameLast              = 'MORAN',
    @EmailAddress          = 'GARY.MORAN@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2665'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'CHARLES.MURRAY@uticanational.com',
    @NameFirst             = 'CHARLES',
    @NameLast              = 'MURRAY',
    @EmailAddress          = 'CHARLES.MURRAY@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2716'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Mark.Negip@uticanational.com',
    @NameFirst             = 'Mark',
    @NameLast              = 'Negip',
    @EmailAddress          = 'Mark.Negip@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2698'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Sharon.Pittella@uticanational.com',
    @NameFirst             = 'Sharon',
    @NameLast              = 'Pittella',
    @EmailAddress          = 'Sharon.Pittella@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2671'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'DAVID.RAMSDEN@uticanational.com',
    @NameFirst             = 'DAVID',
    @NameLast              = 'RAMSDEN',
    @EmailAddress          = 'DAVID.RAMSDEN@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2679'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'JANICE.RILEY@uticanational.com',
    @NameFirst             = 'JANICE',
    @NameLast              = 'RILEY',
    @EmailAddress          = 'JANICE.RILEY@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2660'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Carmen.Rock@uticanational.com',
    @NameFirst             = 'Carmen',
    @NameLast              = 'Rock',
    @EmailAddress          = 'Carmen.Rock@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2626'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'DANA.ROSCOE@uticanational.com',
    @NameFirst             = 'DANA',
    @NameLast              = 'ROSCOE',
    @EmailAddress          = 'DANA.ROSCOE@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2602'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Edith.Schultz@uticanational.com',
    @NameFirst             = 'Edith',
    @NameLast              = 'Schultz',
    @EmailAddress          = 'Edith.Schultz@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2677'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'JAMES.SURETTE@uticanational.com',
    @NameFirst             = 'JAMES',
    @NameLast              = 'SURETTE',
    @EmailAddress          = 'JAMES.SURETTE@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2652'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'TAMI.TILTON@uticanational.com',
    @NameFirst             = 'TAMI',
    @NameLast              = 'TILTON',
    @EmailAddress          = 'TAMI.TILTON@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2624'

print 'Done.'

-- COMMIT
-- rollback