/******************************************************************************
* Claim Number format for Louisiana Farm Bureau
*  nnnnnn-nn-x nnnnn
* (   1  ) 2(    3  )
* 1 - Claim number
* 2 - Claimant number
* 3 - Policy number
******************************************************************************/

DECLARE @InsuranceCompanyID as int

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Louisiana Farm Bureau'

IF @InsuranceCompanyID IS NOT NULL
BEGIN
   BEGIN TRANSACTION
   
   UPDATE utb_office
   SET ClaimNumberValidJS = '^(\\d{6})[\\s|\\-]{1,}(\\d{2})[\\s|\\-]{1,}([a-z]{1}\\s{1}[a-z0-9]{1}\\d{5,})$',
       ClaimNumberFormatJS = '^@1-@2-@3',
       ClaimNumberMsgText = '[Claim Number]-[Claimant Number]-[Policy Number]\n\nData format required is: 999999-99-L A99999\n\nWhere\n9 = must be a number between 0 and 9\nL = must be a letter of the alphabet\nA = can be either number or letter.'
   WHERE InsuranceCompanyID = @InsuranceCompanyID
   
   SELECT * 
   FROM utb_office
   WHERE InsuranceCompanyID = @InsuranceCompanyID
   
   -- COMMIT
   -- ROLLBACK
END