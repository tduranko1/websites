DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 304

------------------------------
-- Add New Doc Types
------------------------------
IF NOT EXISTS (SELECT * FROM utb_document_type WHERE [Name] LIKE '%Total Loss Valuation%') 
BEGIN
INSERT INTO
	utb_document_type
	SELECT
		(SELECT MAX(DocumentTypeID)+1 FROM utb_document_type)
		, (SELECT MAX(DisplayOrder)+1 FROM utb_document_type)
		, DocumentClassCD
		, EnabledFlag	
		, EstimateTypeFlag
		, 'Total Loss Valuation'
		, 1
		, 0
		, CURRENT_TIMESTAMP
	FROM
		utb_document_type
	WHERE 
		[Name] = 'Status Update'
	
	SELECT 'New document types - Added...'
END
ELSE
BEGIN
	SELECT 'New document types - Already exist...'
END


------------------------------
-- Add Message Template
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%RRP - Total Loss Valuation%') 
BEGIN
	INSERT INTO utb_message_template
	SELECT
		AppliesToCD
		, 1
		, 'RRP - Total Loss Valuation (Adjuster & Appraisers)|Messages/msgRRPTotalLossValuation.xsl'
		, ServiceChannelCD
		, 0 
		, CURRENT_TIMESTAMP 
	FROM
		utb_message_template 
	WHERE 
		Description = 'RRP - Total Loss Determination (Shop)|Messages/msgRRPTotalLossDetermination.xsl'

	SELECT 'New message templates - Added...'
END
ELSE
BEGIN
	SELECT 'New message templates - Already exist...'
END

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_bundling b INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE [Description] LIKE '%RRP - Total Loss Valuation%') 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID from utb_document_type WHERE [Name] = 'Total Loss Valuation')
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] = 'RRP - Total Loss Valuation (Adjuster & Appraisers)|Messages/msgRRPTotalLossValuation.xsl') 
		,1
		,'RRP - Total Loss Valuation (Adjuster & Appraisers)'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgRRPTotalLossDetermination.xsl%'
	
	SELECT 'Bundles added...'
END
ELSE
BEGIN
	SELECT 'Bundles already added...'
END

------------------------------
-- Add Client Bundling
------------------------------
IF NOT EXISTS (
	SELECT 
		* 
	FROM 
		utb_bundling b 
		INNER JOIN utb_client_bundling cb
		ON cb.BundlingID = b.BundlingID
		INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		[Description] LIKE '%msgRRPTotalLossValuation.xsl%' 
		AND cb.InsuranceCompanyID = 304
	)
BEGIN
	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgRRPTotalLossValuation.xsl%'

END

