
DECLARE @intDocID			INT
DECLARE @intDisplayOrder	INT

SET @intDocID = (SELECT MAX(DocumentTypeID) FROM utb_document_type)
SET @intDisplayOrder = (SELECT MAX(DisplayOrder) FROM utb_document_type WHERE DisplayOrder IS NOT NULL)


--W-9
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'W-9' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'W-9'
           ,0
		   ,0
		   ,current_timestamp)
END

-- Repair Facility Info
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'Repair Facility Info' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'Repair Facility Info'
           ,0
		   ,0
		   ,current_timestamp)
END


-- Electronic Payment Authorization
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'Electronic Payment Authorization' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'Electronic Payment Authorization'
           ,0
		   ,0
		   ,current_timestamp)
END


-- Background Check
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'Background Check' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'Background Check'
           ,0
		   ,0
		   ,current_timestamp)
END


-- Terms of Agreement
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'Terms of Agreement' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'Terms of Agreement'
           ,0
		   ,0
		   ,current_timestamp)
END


-- Other
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'Other Document' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'Other Document'
           ,0
		   ,0
		   ,current_timestamp)
END


-- Background Results
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'Background Results' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'Background Results'
           ,0
		   ,0
		   ,current_timestamp)
END


-- TIN Matching
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'TIN Matching' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'TIN Matching'
           ,0
		   ,0
		   ,current_timestamp)
END


-- Labor Rates
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'Labor Rates' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'Labor Rates'
           ,0
		   ,0
		   ,current_timestamp)
END



-- Warranty
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'Warranty' AND DocumentClassCD = 'S') 
BEGIN

	SET @intDocID = @intDocID + 1
	SET @intDisplayOrder = @intDisplayOrder + 1

	INSERT INTO [dbo].[utb_document_type]
           (DocumentTypeID
           ,DisplayOrder
           ,DocumentClassCD
           ,EnabledFlag
           ,EstimateTypeFlag
           ,Name
           ,SysMaintainedFlag
           ,SysLastUserID
           ,SysLastUpdatedDate)
	 VALUES
           (@intDocID
           ,@intDisplayOrder
           ,'S'
           ,1
           ,0
           ,'Warranty'
           ,0
		   ,0
		   ,current_timestamp)
END

