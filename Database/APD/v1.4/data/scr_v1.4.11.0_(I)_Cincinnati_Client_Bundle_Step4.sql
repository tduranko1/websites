DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 194
SET @FromInscCompID = 387

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
SELECT 
	* 
FROM 
	utb_bundling_document_type 
WHERE 	
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @ToInscCompID
	)
)
BEGIN

	DECLARE @MsgTempID INT
	DECLARE @DocumentTypeID INT

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPSClosingSupplement.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,10,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPSTLClosing.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,82,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	
	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPSAssignmentCancel.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPSStaleAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,37,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPSCashOutAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPSStatusUpdAction.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPSTLAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,38,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPS_ClosingRepairComplete.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,2,NULL,'I',1,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,1,NULL,1,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,1,4,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,51,NULL,'I',0,0,0,NULL,0,NULL,1,5,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,9,NULL,'I',0,0,0,NULL,0,NULL,0,0,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPSInitialEstimate.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCIPSRentalInvoice.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,9,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END

----------------------------------------------------------------------

-------------------------------------
-- Correct Desc Initial Est Bundling
-------------------------------------
DECLARE @BundlingID INT

SELECT 
	@BundlingID = BundlingID 
FROM 
	utb_message_template t
	INNER JOIN utb_bundling b 
		ON b.MessageTemplateID = t.MessageTemplateID 
WHERE t.[Description] LIKE '%msgCIPSInitialEstimate%'

IF EXISTS (SELECT * FROM utb_bundling WHERE BundlingID = @BundlingID )
BEGIN
	UPDATE utb_bundling SET [Name] = 'Initial Estimate and Photos (Adjuster)' WHERE BundlingID = @BundlingID
	SELECT 'Bundling Name Corrected...'
END
ELSE
BEGIN
	SELECT 'Bundling Name already corrected...'
END

SELECT 
	@BundlingID = BundlingID 
FROM 
	utb_message_template t
	INNER JOIN utb_bundling b 
		ON b.MessageTemplateID = t.MessageTemplateID 
WHERE t.[Description] LIKE '%msgCIPSStatusUpdAction.xsl%'

IF EXISTS (SELECT * FROM utb_bundling WHERE BundlingID = @BundlingID )
BEGIN
	UPDATE utb_bundling SET [Name] = 'Status Update - Claim Rep' WHERE BundlingID = @BundlingID
	SELECT 'Bundling Name Corrected...'
END
ELSE
BEGIN
	SELECT 'Bundling Name already corrected...'
END