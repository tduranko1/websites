-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configurations for MOFB
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @PaymentCompanyID       smallint
DECLARE @FaxTemplateFormID      int


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 282
SET @PaymentCompanyID = 818


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'


PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

--Insert Insurance Company information.

IF NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
    INSERT INTO dbo.utb_insurance
        (
         InsuranceCompanyID, 
         DeskAuditPreferredCommunicationMethodID,
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip, 
         AssignmentAtSelectionFlag,
         BillingModelCD, 
         BusinessTypeCD,
         CarrierLynxContactPhone,
         CFLogoDisplayFlag,
         ClaimPointCarrierRepSelFlag,
         ClaimPointDocumentUploadFlag, 
         ClientAccessFlag,
         DemoFlag,
         DeskAuditCompanyCD,
         DeskReviewLicenseReqFlag, 
         EnabledFlag,
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         FedTaxId,
         IngresAccountingId,
         InvoicingModelPaymentCD, 
         InvoiceMethodCD,
         LicenseDeterminationCD, 
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocDestinationCD,
         ReturnDocPackageTypeCD,
         ReturnDocRoutingCD, 
         TotalLossValuationWarningPercentage, 
         TotalLossWarningPercentage, 
         WarrantyPeriodRefinishMinCD,
         WarrantyPeriodWorkmanshipMinCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,      -- InsuranceCompanyID
                NULL,                     -- DeskAuditPreferredCommunicationMethodID
                '701 S. Country Club Drive',-- Address1
                '',                       -- Address2
                'Jefferson City, Missouri',-- AddressCity
                'MO',                     -- AddressState
                '65102',                  -- AddressZip
                0,                        -- AssignmentAtSelectionFlag
                'E',                      -- BillingModelCD
                'C',                      -- BusinessTypeCD
                NULL,                     -- CarrierLynxContactPhone
                1,                        -- CFLogoDisplayFlag
                0,                        -- ClaimPointCarrierRepSelFlag
                1,                        -- ClaimPointDocumentUploadFlag
                1,                        -- ClientAccessFlag
                0,                        -- DemoFlag
                'MOFB',                   -- DeskAuditCompanyCD
                1,                        -- DeskReviewLicenseReqFlag
                1,                        -- EnabledFlag
                NULL,                     -- FaxAreaCode
                NULL,                     -- FaxExchangeNumber
                NULL,                     -- FaxUnitNumber
                NULL,                     -- FedTaxId
                @PaymentCompanyID,        -- IngresAccountingId
                'C',                      -- InvoicingModelPaymentCD (Bulk / per claim)
                'P',                      -- InvoiceMethodCD
                'LOSS',                   -- LicenseDeterminationCD
                'Missouri Farm Bureau',   -- Name
                '573',                    -- PhoneAreaCode
                '893',                    -- PhoneExchangeNumber
                '1400',                   -- PhoneUnitNumber
                'REP',                    -- ReturnDocDestinationCD
                'PDF',                    -- ReturnDocPackageTypeCD
                'EML',                    -- ReturnDocRoutingCD
                0.75,                     -- TotalLossValuationWarningPercentage
                0.75,                     -- TotalLossWarningPercentage
                '99',                     -- WarrantyPeriodRefinishMinCD
                '99',                     -- WarrantyPeriodWorkmanshipMinCD
                0,                        -- SysLastUserID
                @ModifiedDateTime         -- SysLastUpdatedDate
        )
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already exists in utb_insurance', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END


--Insert Client level configuration

------------------------------------------------------

INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Program Shop Assignment')

------------------------------------------------------

INSERT INTO dbo.utb_client_service_channel
SELECT @InsuranceCompanyID,
       ServiceChannelDefaultCD,
       0,       -- ClientAuthorizesPaymentFlag
       'B',     -- InvoicingModelBillingCD
       0,
       @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Program Shop Assignment')


------------------------------------------------------

INSERT INTO dbo.utb_client_contract_state
SELECT  @InsuranceCompanyID,
        StateCode,
        0,                   -- UseCEIShopFlag
        0,                   -- SysLastUserID
        @ModifiedDateTime
  FROM  dbo.utb_state_code
  WHERE EnabledFlag = 1


------------------------------------------------------

INSERT INTO dbo.utb_client_coverage_type (InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        AdditionalCoverageFlag,
        ClientCode,
        CoverageProfileCD,
        DisplayOrder,
        1,
        Name,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_client_coverage_type
  WHERE InsuranceCompanyID = 145    -- Clone Zurich's configuration


------------------------------------------------------    
    
INSERT INTO dbo.utb_client_claim_aspect_type 
SELECT  @InsuranceCompanyID,
        ClaimAspectTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_claim_aspect_type
  WHERE Name IN ('Claim', 'Assignment', 'Vehicle', 'Fax Assignment')

------------------------------------------------------        
    
INSERT INTO dbo.utb_client_payment_type VALUES (@InsuranceCompanyID, 'I', 0, @ModifiedDateTime)

------------------------------------------------------

INSERT INTO dbo.utb_client_report (InsuranceCompanyID, OfficeID, ReportID, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        NULL,
        ReportID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_report 
  WHERE ReportID IN (1, 2, 3, 4, 5, 6, 7, 12, 13)

------------------------------------------------------

-- Create Office(s)

-- Missouri Farm Bureau 

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '701 S. Country Club Drive',-- Address1
                '',                     -- Address2
                'Jefferson City',       -- AddressCity
                'MO',                   -- AddressState
                '65102',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Missouri Farm Bureau', -- ClientOfficeId
                1,                      -- EnabledFlag
                NULL,                   -- FaxAreaCode
                NULL,                   -- FaxExchangeNumber
                NULL,                   -- FaxUnitNumber
                'P.O. Box 658',         -- MailingAddress1
                NULL,                   -- MailingAddress2
                'Jefferson City',       -- MailingAddressCity
                'MO',                   -- MailingAddressState
                '65102',                -- MailingAddressZip
                'Missouri Farm Bureau', -- Name
                '573',                  -- PhoneAreaCode
                '893',                  -- PhoneExchangeNumber
                '1400',                 -- PhoneUnitNumber
                NULL,                   -- ReturnDocEmailAddress
                NULL,                   -- ReturnDocFaxAreaCode
                NULL,                   -- ReturnDocFaxExchangeNumber
                NULL,                   -- ReturnDocFaxUnitNumber
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    WHERE EnabledFlag = 1

-- Shop Fax Assignment
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
)
VALUES (
    22,                     -- DocumentTypeID,
    @InsuranceCompanyID,    -- InsuranceCompanyID,
    'Shop Assignment',      -- Name,
    'FaxAssignmentType1.pdf', -- PDFPath,
    'S',                    -- PertainsToCD,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    1,                      -- SystemFlag,
    0,                      -- SysLastUserID,
    @NOW                    -- SysLastUpdatedDate
)

SELECT @FaxTemplateFormID = SCOPE_IDENTITY()

-- Add the expanded comments sheet to the fax template
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FaxTemplateFormID, -- FormID,
    1,                  -- EnabledFlag,
    'Expanded Comments', -- Name,
    'FaxXComments.pdf', -- PDFPath,
    '',                 -- SQLProcedure,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

-- Add the shop instructions sheet to the fax template
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FaxTemplateFormID,     -- FormID,
    1,                      -- EnabledFlag,
    'Shop Instructions',    -- Name,
    'ShopInstructions282.pdf', -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

-- Add the DTP sheet to the fax template
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FaxTemplateFormID,     -- FormID,
    1,                      -- EnabledFlag,
    'Direction to Pay',     -- Name,
    'DTPType1.pdf',         -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

-- Bundling document exceptions
-- NONE.



-- Review affected tables
SELECT * FROM dbo.utb_insurance
SELECT * FROM dbo.utb_client_assignment_type
SELECT * FROM dbo.utb_client_claim_aspect_type
SELECT * FROM dbo.utb_client_contract_state
SELECT * FROM dbo.utb_client_payment_type
SELECT * FROM dbo.utb_client_report
SELECT * FROM dbo.utb_office
SELECT * FROM dbo.utb_office_assignment_type
SELECT * FROM dbo.utb_office_contract_state

-- This script requires a fresh on the workflow and spawn reference tables from the access file

--commit 
--rollback
