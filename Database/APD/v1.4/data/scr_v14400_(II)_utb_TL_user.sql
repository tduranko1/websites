DECLARE @UserID as bigint

InsERT INTO dbo.utb_user (
   SupervisorUserID,
   EnabledFlag,
   NameFirst,
   NameLast,
   SysLastUserID,
   SysLastUpdatedDate
) values (
   2,
   1,
   'Total Loss',
   'Pool',
   0,
   CURRENT_TIMESTAMP
)

SET @UserID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_user_application (
   UserID,
   ApplicationID,
   AccessBeginDate,
   LogonId,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @UserID,
   1,
   '6/1/2010',
   'DEFAULT',
   0,
   current_timestamp
)

INSERT INTO utb_user_role
select @UserID, 25, 1, 0, current_timestamp


INSERT INTO utb_assignment_pool_user 
select 9, @UserID, 0, current_timestamp

INSERT INTO dbo.utb_user_insurance
select @UserID, InsuranceCompanyID, 0, current_timestamp
from utb_insurance
where InsuranceCompanyID = 196

INSERT INTO utb_user_state
SELECT @UserID, statecode, 1, 1, 0, current_timestamp
from utb_state_code
where EnabledFlag = 1

select * from utb_user where namefirst = 'total loss'

-- 4629