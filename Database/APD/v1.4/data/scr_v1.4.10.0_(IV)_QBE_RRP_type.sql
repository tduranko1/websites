DECLARE @InsuranceCompanyID as int
SET @InsuranceCompanyID = 304


IF EXISTS(SELECT FormID
                FROM dbo.utb_form
                WHERE InsuranceCompanyID = @InsuranceCompanyID
                  AND ServiceChannelCD = 'RRP'
                  AND Documenttypeid = 12)
BEGIN

	UPDATE utb_form
	SET  Documenttypeid = 75
	WHERE InsuranceCompanyID = @InsuranceCompanyID
		AND SERVICECHANNELCD = 'RRP'
		AND Documenttypeid = 12
	
END

