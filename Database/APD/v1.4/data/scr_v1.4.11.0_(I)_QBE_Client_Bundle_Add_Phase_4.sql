DECLARE @iBundlingID INT
SELECT @iBundlingID = BundlingID FROM utb_bundling WHERE [Name] = 'RRP - Total Loss Valuation (Adjuster & Appraisers)'

IF NOT EXISTS (SELECT * FROM utb_bundling_document_type WHERE BundlingID = @iBundlingID AND DocumentTypeID = 3) 
BEGIN
	INSERT INTO utb_bundling_document_type VALUES (@iBundlingID, 3, NULL, 'I', 0, 0, 0, 'O', 0, NULL, 1, 2, NULL, 0,	0, 0, CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@iBundlingID, 8, NULL, 'I', 0, 1, 0, NULL, 0, NULL, 1, 3, NULL, 0,	0, 0, CURRENT_TIMESTAMP)
	SELECT 'Required Documents Added...'
END
ELSE
BEGIN
	SELECT 'Required Documents Already Added...'
END

