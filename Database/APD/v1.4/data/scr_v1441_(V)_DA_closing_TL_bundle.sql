declare @now as datetime
declare @messageID as int
declare @DocumentTypeID as int
declare @BundlingID as int

set @now = CURRENT_TIMESTAMP

select @DocumentTypeID = DocumentTypeID
from utb_document_type
where Name = 'Total Loss Bundle'
  and EnabledFlag = 1

begin transaction

-- Add Desk Audit Closing Total Loss message
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Closing - Total Loss|Messages/msgDAClosingTL.xsl',
    'DA',
    0,
    @now
)

set @messageID = SCOPE_IDENTITY()

-- adding the bundling
INSERT INTO utb_bundling (
	DocumentTypeID,
	MessageTemplateID,
	EnabledFlag,
	Name,
	SysLastUserID,
	SysLastUpdatedDate 
) VALUES (
	@DocumentTypeID,	--DocumentTypeID,
	@messageID,			--MessageTemplateID,
	1,					--EnabledFlag,
	'Closing - Total Loss', --Name,
	0,					--SysLastUserID,
	@now				--SysLastUpdatedDate 
)

set @BundlingID = SCOPE_IDENTITY()

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- add this bundling to all DA clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'DA', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'DA'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich


select * from utb_message_template where MessageTemplateID = @messageID 
select * from utb_bundling where BundlingID = @BundlingID 
select * from utb_bundling_document_type where BundlingID = @BundlingID 
select * from utb_client_bundling where BundlingID = @BundlingID 

-- commit
-- rollback
