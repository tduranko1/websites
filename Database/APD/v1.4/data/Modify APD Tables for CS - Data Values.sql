DECLARE @iInscCompanyID INT
SET @iInscCompanyID = 184 -- Texas Farm

----------------------------------------------
-- ADD CS support to the utb_app_variable 
----------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_app_variable WHERE [Name] = 'Default_Assignment_Pool_Support' AND SubName = 'CS')
BEGIN
	DECLARE @iNextID INT
	SELECT @iNextID = MAX(AppVariableID) + 1 FROM utb_app_variable
	INSERT INTO utb_app_variable VALUES (@iNextID, 'The default assignment pool for file support',	NULL, NULL, 'Default_Assignment_Pool_Support', 'CS', 17,	0, 0, CURRENT_TIMESTAMP)
END

----------------------------------------------
-- ADD FNOL Vars
----------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_assignment_pool WHERE [Name] = 'Choice Program Support')
BEGIN
	INSERT INTO utb_assignment_pool VALUES (17, 17, 1, 'SPRT', 'Choice Program Support', 0, CURRENT_TIMESTAMP)
END

----------------------------------------------
-- Add new assignment type
-- utb_assignment_type
----------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_assignment_type WHERE [Name] = 'Choice Shop Assignment' AND ServiceChannelDefaultCD = 'CS')
BEGIN
	-- Add new assignment type
	INSERT INTO utb_assignment_type VALUES (17,	18,	1,	'Choice Shop Assignment', 'CS', 1, 0, CURRENT_TIMESTAMP)
	
	SELECT 'Assignment Type CS added successfully'
END
ELSE
BEGIN
	-- Service Channel already added
	SELECT 'Assignment Type CS already added'
END

----------------------------------------------
-- Add the CS service channel to Insc Company
----------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_client_service_channel WHERE InsuranceCompanyID = @iInscCompanyID AND ServiceChannelCD = 'CS')
BEGIN
	-- Add new Service Channel
	INSERT INTO utb_client_service_channel VALUES (@iInscCompanyID,	'CS', 0, 'B',  0, CURRENT_TIMESTAMP)
	
	SELECT 'Service Channel CS added successfully'
END
ELSE
BEGIN
	-- Service Channel already added
	SELECT 'Service Channel CS already added'
END

----------------------------------------------
-- Add the CS service channel to Status 
----------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_status WHERE StatusID = 800 AND ServiceChannelCD = 'CS')
BEGIN
	-- Add new Service Channel
	INSERT INTO utb_status VALUES (800,	9, 60, 1, 0, 'Active', 'CS', 'SC', 0, CURRENT_TIMESTAMP)
	INSERT INTO utb_status VALUES (897,	9, 61, 1, 0, 'Voided', 'CS', 'SC', 0, CURRENT_TIMESTAMP)
	INSERT INTO utb_status VALUES (898,	9, 62, 1, 0, 'Cancelled', 'CS', 'SC', 0, CURRENT_TIMESTAMP)
	INSERT INTO utb_status VALUES (899,	9, 63, 1, 0, 'Complete', 'CS', 'SC', 0, CURRENT_TIMESTAMP)
	
	SELECT 'Status CS added successfully'
END
ELSE
BEGIN
	-- Service Channel already added
	SELECT 'Status CS already added'
END

----------------------------------------------
-- Add New Event for Comments
----------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_event WHERE EventID = 106)
BEGIN
	-- Add new Event
	INSERT INTO utb_event VALUES (106,9,50,1,1,'Comment',0,CURRENT_TIMESTAMP)
	
	SELECT 'Event Comment added successfully'
END
ELSE
BEGIN
	-- Service Channel already added
	SELECT 'Status Event Comment already added'
END

----------------------------------------------
-- Add New Event for Invoice Close
----------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_event WHERE EventID = 107)
BEGIN
	-- Add new Event
	INSERT INTO utb_event VALUES (107,9,50,1,1,'Transaction Review Complete',0,CURRENT_TIMESTAMP)
	
	SELECT 'Event Transaction Review Complete added successfully'
END
ELSE
BEGIN
	-- Service Channel already added
	SELECT 'Status Event Transaction Review Complete already added'
END
