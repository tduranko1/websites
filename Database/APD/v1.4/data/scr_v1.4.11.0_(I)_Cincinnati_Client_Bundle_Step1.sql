DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 194
SET @FromInscCompID = 387

------------------------------
-- Delete old Bunding Config
------------------------------
IF EXISTS (SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID = @ToInscCompID)
BEGIN
	DELETE FROM utb_client_bundling WHERE InsuranceCompanyID = @ToInscCompID
END

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%CI%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, REPLACE(mt.[Description],'msgWF','msgCI')
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		
	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

------------------------------
-- Correct Desc Initial Est
------------------------------
IF EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgCIPI%' )
BEGIN
	UPDATE utb_message_template SET [Description] = 'Initial Estimate and Photos (Adjuster)|Messages/msgCIPSInitialEstimate.xsl' WHERE [Description] LIKE '%msgCIPI%'
	SELECT 'Message Template Corrected...'
END
ELSE
BEGIN
	SELECT 'Message Template already corrected...'
END

