-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configuration for Utica New York Metro Regional Office
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Amica Mutual Insurance Company'


PRINT '.'
PRINT '.'
PRINT 'Creating Seattle Regional Office...'
PRINT '.'


-- Create Office(s)

-- Central Dallas Office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '7460 Warren Parkway',	 -- Address1
                'Suite 150',            -- Address2
                'Frisco',               -- AddressCity
                'TX',                   -- AddressState
                '75034',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Dallas',               -- ClientOfficeId
                1,                      -- EnabledFlag
                '866',					    -- FaxAreaCode
                '847',					    -- FaxExchangeNumber
                '1569',					    -- FaxUnitNumber
                'PO Box 3000',			 -- MailingAddress1
                NULL,                   -- MailingAddress2
                'Frisco',			       -- MailingAddressCity
                'TX',					    -- MailingAddressState
                '75034',				    -- MailingAddressZip
                'Dallas Office',        -- Name
                '800',					    -- PhoneAreaCode
                '962',					    -- PhoneExchangeNumber
                '6422',					    -- PhoneUnitNumber
                NULL,					    -- ReturnDocEmailAddress
                NULL,					    -- ReturnDocFaxAreaCode
                NULL,					    -- ReturnDocFaxExchangeNumber
                NULL,					    -- ReturnDocFaxUnitNumber
                0,						    -- SysLastUserID
                @ModifiedDateTime		 -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Adverse Subro Desk Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


-- Central Phoenix Office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '10835 N. 25th Ave',	 -- Address1
                'Suite 215',            -- Address2
                'Phoenix',              -- AddressCity
                'AZ',                   -- AddressState
                '85029',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Phoenix',              -- ClientOfficeId
                1,                      -- EnabledFlag
                '866',					    -- FaxAreaCode
                '847',					    -- FaxExchangeNumber
                '1575',					    -- FaxUnitNumber
                '10835 N. 25th Ave',	 -- MailingAddress1
                'Suite 215',            -- MailingAddress2
                'Phoenix',			       -- MailingAddressCity
                'AZ',					    -- MailingAddressState
                '85029',				    -- MailingAddressZip
                'Phoenix Office',       -- Name
                '888',					    -- PhoneAreaCode
                '847',					    -- PhoneExchangeNumber
                '1575',					    -- PhoneUnitNumber
                NULL,					    -- ReturnDocEmailAddress
                NULL,					    -- ReturnDocFaxAreaCode
                NULL,					    -- ReturnDocFaxExchangeNumber
                NULL,					    -- ReturnDocFaxUnitNumber
                0,						    -- SysLastUserID
                @ModifiedDateTime		 -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Adverse Subro Desk Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


    
-- Review affected tables
SELECT * FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT oat.* 
FROM dbo.utb_office_assignment_type oat 
LEFT JOIN utb_office o ON oat.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT ocs.* 
FROM dbo.utb_office_contract_state  ocs
LEFT JOIN utb_office o ON ocs.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID


--commit 
--rollback
