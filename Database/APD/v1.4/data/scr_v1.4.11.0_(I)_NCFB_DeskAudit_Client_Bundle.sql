DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 327
SET @FromInscCompID = 184

------------------------------
-- Add NCFB Client Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID = @ToInscCompID )
BEGIN
	INSERT INTO utb_client_bundling (BundlingID, InsuranceCompanyID, OfficeID, AutoNotifyAdjusterFlag, ReturnDocPackageTypeCD, ReturnDocRoutingValue, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate) 
	SELECT
		BundlingID
		, @ToInscCompID
		, NULL
		, AutoNotifyAdjusterFlag
		, NULL
		, NULL
		, ServiceChannelCD
		, 0 
		, Current_Timestamp
	FROM 
		utb_client_bundling 
	WHERE	
		InsuranceCompanyID = @FromInscCompID
		AND ServiceChannelCD = 'DA' 

	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END
	

