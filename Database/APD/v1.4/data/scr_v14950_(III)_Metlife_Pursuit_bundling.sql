/******************************************************************************
* Metlife DA Pursuit Bundling profiles
******************************************************************************/

DECLARE @NOW as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int

SET @NOW = CURRENT_TIMESTAMP
SET @InsuranceCompanyID = 176

BEGIN TRANSACTION

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Acknowledgement of Claim|Messages/msgPursuitDAClmAck.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 34, @MessageTemplateID, 1, 'Acknowledgement of Claim', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   '$ADJUSTER$',
   0,
   @now


---------------------------------------------------------------
-- Scheduled Inspection Status Update Bundling
---------------------------------------------------------------


INSERT INTO utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Scheduled Inspection Status Update|Messages/msgPursuitDAStatusUpdate.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

-- insert bundling

INSERT INTO dbo.utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 36, @MessageTemplateID, 1, 'Scheduled Inspection Status Update', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- insert client bundling
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
) 
SELECT @BundlingID, @InsuranceCompanyID, 'DA', 'PDF', 'EML', 'Choice1@metlife.com', 0, @now


SELECT * from utb_message_template
SELECT * FROM utb_bundling WHERE BundlingID = @BundlingID
SELECT * from utb_client_bundling WherE InsuranceCompanyID = @InsuranceCompanyID

-- commit
-- rollback