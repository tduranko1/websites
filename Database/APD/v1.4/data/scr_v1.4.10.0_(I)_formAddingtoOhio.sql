IF NOT EXISTS(SELECT *
                FROM dbo.utb_form_supplement
                WHERE Name = 'OHIO Expanded Comments'  AND ServiceChannelCD = 'PS'
)
BEGIN
      INSERT INTO 
         utb_form_supplement 
      SELECT
            FormID
            , NULL
            , 'OH'
            , 1
            , NULL
            , 'OHIO Expanded Comments'
            , 'Ohio.pdf'
            , 'PS'
            , ''
            , 0
            , CURRENT_TIMESTAMP 
      FROM 
            utb_form_supplement 
      WHERE 
            FormID IN 
            (
                  SELECT 
                        FormID 
                  FROM 
                        utb_form
                  WHERE
                        [name] = 'Shop Assignment'
            )
            AND [Name] = 'Shop Instructions'

      SELECT 'Ohio Forms added...'
END
ELSE
BEGIN
      SELECT 'Ohio Form already added...'
END

