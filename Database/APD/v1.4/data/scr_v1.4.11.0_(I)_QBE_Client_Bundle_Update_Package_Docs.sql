DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 304

------------------------------
-- Add 2 New Doc Types as required
------------------------------
IF EXISTS (SELECT * FROM utb_document_type WHERE [Name] LIKE '%Total Loss Determination%') 
BEGIN
	INSERT INTO utb_document_type VALUES (78,78,'C',1,0,'Approved Estimate Shop Notification',1,0, CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (345,76,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,0,0, 0, CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (345,78,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,0,0, 0, CURRENT_TIMESTAMP)
	
	INSERT INTO utb_bundling_document_type VALUES (346,76,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,0,0, 0, CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (346,78,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,0,0, 0, CURRENT_TIMESTAMP)

	INSERT INTO utb_bundling_document_type VALUES (347,77,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,0,0, 0, CURRENT_TIMESTAMP)

	SELECT 'New required documents - Added...'
END
ELSE
BEGIN
	SELECT 'New required documents - Already added...'
END



