declare @InsuranceCompanyID as int

set @InsuranceCompanyID = 145

begin transaction

insert into utb_client_contract_state
select @InsuranceCompanyID, s.StateCode, 1, 0, current_timestamp
from utb_state_code s
where stateCode not in (select StateCode 
			from utb_client_contract_state ccs
			where InsuranceCompanyID = @InsuranceCompanyID)

set @InsuranceCompanyID = 209

insert into utb_client_contract_state
select @InsuranceCompanyID, s.StateCode, 1, 0, current_timestamp
from utb_state_code s
where stateCode not in (select StateCode 
			from utb_client_contract_state ccs
			where InsuranceCompanyID = @InsuranceCompanyID)



-- commit
-- rollback