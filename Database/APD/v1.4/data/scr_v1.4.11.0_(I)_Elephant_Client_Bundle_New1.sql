DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 374
SET @FromInscCompID = 277

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPSInitialEstimate%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT 
		mt.AppliesToCD
		, mt.EnabledFlag
		, REPLACE(SUBSTRING(mt.[Description],0,CHARINDEX('/msg',mt.[Description])+4) + 'EL' + SUBSTRING(mt.[Description],CHARINDEX('/msg',mt.[Description]) + 4 , 99),'Initial Estimate', 'Initial Estimate Bundle')
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID
		AND b.Name = ('Initial Estimate')
	SELECT 'New Message Template added...'
END
ELSE
BEGIN
	SELECT 'New Message Template already added...'
END

SET @FromInscCompID = 387
------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPSStatusUpdActionMDS.xsl%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT 
		mt.AppliesToCD
		, mt.EnabledFlag
		, REPLACE(REPLACE(SUBSTRING(mt.[Description],0,CHARINDEX('/msg',mt.[Description])+4) + 'EL' + SUBSTRING(mt.[Description],CHARINDEX('/msg',mt.[Description]) + 4 , 99),'Status Update', 'Status Update - MDS'),'WF','')
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID
		AND b.Name = ('Status Update - MDS')
	SELECT 'New Message Template added...'
END
ELSE
BEGIN
	SELECT 'New Message Template already added...'
END
