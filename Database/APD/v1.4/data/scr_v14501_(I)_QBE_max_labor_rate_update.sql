--
-- scr_v14501_(III)_QBE_max_labor_rate_update.sql
--	- update QBE max labor rate to $999 (e.g. prevailing market rate)
--

DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime

-- set to QBE (ID: 304)
SET @InsuranceCompanyID = (select InsuranceCompanyID from utb_insurance where name = 'QBE Insurance')
SET @now = CURRENT_TIMESTAMP


-- update max labor rate to min value + 4
UPDATE utb_labor_rate
SET BodyRateMax = 999,
	FrameRateMax = 999,
	MaterialRateMax = 999,
	MechRateMax = 999,
	RefinishRateMax = 999,
	SysLastUserID = 0,
	SysLastUpdatedDate = @now
WHERE InsuranceCompanyID = @InsuranceCompanyID
