DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 387
SET @FromInscCompID = 374

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%WEST%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, REPLACE(mt.[Description],'msgEL','msgWF')
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name NOT IN ('Initial Bill Estimate - Early Billing')	
			AND b.Name NOT IN ('Repair Complete - Release and Close')	
		
	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

-------------------------------------
UPDATE
	utb_message_template
SET [Description] = REPLACE([Description], 'Repair Complete - Combined Bill, Release and Close','Closing Repair Complete')	
WHERE
	[Description] IN (
		SELECT [Description] FROM utb_message_template WHERE [Description] = 'Repair Complete - Combined Bill, Release and Close|Messages/msgWFPS_RC_Combined_Bill_Release_Close_EB.xsl'
	)

-------------------------------------
UPDATE
	utb_message_template
SET [Description] = REPLACE([Description],'msgWFPS_RC_Combined_Bill_Release_Close_EB.xsl', 'msgWFPS_ClosingRepairComplete.xsl')	
WHERE
	[Description] IN (
		SELECT [Description] FROM utb_message_template WHERE [Description] = 'Closing Repair Complete|Messages/msgWFPS_RC_Combined_Bill_Release_Close_EB.xsl'
	)

SELECT * FROM utb_message_template WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
