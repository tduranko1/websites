DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 493

------------------------------
-- Add Form Supplements
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_form_supplement 
	WHERE 
		FormID IN 
		(
			SELECT 
				FormID 
			FROM 
				utb_form
			WHERE 
				InsuranceCompanyID = @ToInscCompID
		) 
)
BEGIN
	DECLARE @iFormID INT
	--SELECT FormID FROM utb_form WHERE InsuranceCompanyID = @ToInscCompID
	SELECT @iFormID=FormID FROM utb_form WHERE InsuranceCompanyID = @ToInscCompID

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Expanded Comments', 'FaxXComments.pdf', NULL, '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Shop Instructions', 'ShopInstructions493.pdf', NULL, '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Direction to Pay', 'ACE_DTP.pdf', NULL, 'uspWorkflowSendShopAssignXML', 0, CURRENT_TIMESTAMP

	SELECT 'Client Form Supplements added...'
END
ELSE
BEGIN
	SELECT 'Client Form Supplements already added...'
END

SELECT * FROM utb_form_supplement WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
