DECLARE @InsuranceCompanyID as int

select @InsuranceCompanyID = InsuranceCompanyID 
from utb_insurance 
where name = 'Utica National Insurance Group'

if @InsuranceCompanyID is null
begin
	print 'Unitrin Business Insurance not found in the database.'
	return
end

begin transaction

Insert into utb_client_report
(InsuranceCompanyID, OfficeID, ReportID, SysLastUserID, SysLastUpdatedDate)
SELECT @InsuranceCompanyID, NULL, r.ReportID, 0, CURRENT_TIMESTAMP
FROM utb_report r
WHERE r.Description in ('Desk Audit Summary', 'Desk Review Summary')

-- commit
-- rollback

print 'Remember to commit/rollback'
