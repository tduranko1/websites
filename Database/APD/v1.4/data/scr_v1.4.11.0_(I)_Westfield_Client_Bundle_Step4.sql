DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 387
SET @FromInscCompID = 374

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
SELECT 
	* 
FROM 
	utb_bundling_document_type 
WHERE 	
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @ToInscCompID
	)
)
BEGIN
	DECLARE @MsgTempID INT

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgWFPSClosingSupplement.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',1,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgWFPSRentalInvoice.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,9,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgWFPSTLClosing.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,0,0,NULL,0,NULL,0,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgWFPSAssignmentCancel.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgWFPSStaleAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,37,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgWFPSCashOutAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,35,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,1,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgWFPSStatusUpdAction.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgWFPSTLAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,38,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgWFPS_ClosingRepairComplete.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,2,NULL,'I',1,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,1,NULL,1,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,1,4,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,9,NULL,'I',0,0,0,NULL,0,NULL,0,6,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,51,NULL,'I',0,0,0,NULL,0,NULL,1,5,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END
