
DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime

SET @now = CURRENT_TIMESTAMP
SET @InsuranceCompanyID = 304

UPDATE utb_form
SET PDFPath = 'FaxAssignmentType3.pdf',
	SysLastUserID = 0,
	SysLastUpdatedDate = @now
WHERE InsuranceCompanyID = @InsuranceCompanyID
	AND DocumentTypeID = 22
	
	
SELECT *
FROM utb_form
WHERE InsuranceCompanyID = @InsuranceCompanyID
