/******************************************************************************
* EIC Initial Estimate bundling changes.
*   Made the photos as optional. Since the old profile was used by other clients
*   and there was no other bundling profile with optional photos I needed to 
*   make a new one.
******************************************************************************/

declare @BundlingID as int
declare @InitialEstBundlingID as int
declare @ClientInitialEstBundlingID as int
declare @InsuranceCompanyID as int

select @InsuranceCompanyID = InsuranceCompanyID
from utb_insurance 
where Name = 'Electric Insurance'

IF @InsuranceCompanyID IS NOT NULL
BEGIN

   select @ClientInitialEstBundlingID = ClientBundlingID,
          @InitialEstBundlingID = cb.BundlingID
   from utb_client_bundling cb
   left join utb_bundling b on cb.BundlingID = b.bundlingID
   WHERE InsuranceCompanyID = @InsuranceCompanyID
     and b.Name = 'Initial Estimate'
     and b.EnabledFlag = 1

   BEGIN TRANSACTION
   
   INSERT INTO utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      35, 
      2,
      1,
      'Initial Estimate',
      0,
      current_timestamp
   )

   /*
   
   6/19/09 - Tom H - does not want any document be part of this profile.
   
   SET @BundlingID = SCOPE_IDENTITY()

   INSERT INTO utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      MandatoryFlag,
      SelectionOrder,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) values (
      @BundlingID,
      2,
      'I',
      0,
      0,
      0,
      'O',
      0,
      1,
      1,
      1,
      0,
      current_timestamp
   )

   INSERT INTO utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      MandatoryFlag,
      SelectionOrder,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) values (
      @BundlingID,
      8,
      'I',
      0,
      1,
      0,
      NULL,
      0,
      0,
      2,
      0,
      0,
      current_timestamp
   )
   
   select * from utb_bundling_document_type where BundlingID = @InitialEstBundlingID*/

   UPDATE utb_client_bundling
   set BundlingID = @BundlingID
   where ClientBundlingID = @ClientInitialEstBundlingID
   
   select * from utb_client_bundling where InsuranceCompanyID = @InsuranceCompanyID
   select * from utb_bundling where bundlingID = @BundlingID
   --select * from utb_bundling_document_type where BundlingID = @BundlingID

END

-- commit
-- rollback

