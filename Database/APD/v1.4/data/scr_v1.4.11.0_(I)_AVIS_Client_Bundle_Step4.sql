DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 515
SET @FromInscCompID = 387

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
SELECT 
	* 
FROM 
	utb_bundling_document_type 
WHERE 	
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @ToInscCompID
	)
)
BEGIN
	DECLARE @MsgTempID INT
	DECLARE @DocumentTypeID INT

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgAVPSClosingSupplement.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,10,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgAVPSTLClosing.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,50,NULL,'I',0,0,0,NULL,0,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	
	SELECT @DocumentTypeID=DocumentTypeID FROM utb_document_type WHERE [Name] = 'Vehicle Condition Report'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP) 
	 
	SELECT @DocumentTypeID=DocumentTypeID FROM utb_document_type WHERE [Name] = 'Salvage Bid Report'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgAVPSAssignmentCancel.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgAVPSStaleAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,37,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgAVPSCashOutAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgAVPSStatusUpdAction.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgAVPSTLAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,38,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgAVPS_ClosingRepairComplete.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,2,NULL,'I',1,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,1,NULL,1,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,1,4,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,51,NULL,'I',0,0,0,NULL,0,NULL,1,5,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgAVPSInitialEstimate.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END
