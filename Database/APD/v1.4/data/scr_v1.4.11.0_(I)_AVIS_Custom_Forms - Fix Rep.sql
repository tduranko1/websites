DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 515

------------------------------
-- Add Form Supplements
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_form
	WHERE 
		InsuranceCompanyID = @ToInscCompID
		AND HTMLPath = 'Forms/AVIS_TLBidRep.asp'
		AND SQLProcedure = 'uspCFVSRGetXML'
)
BEGIN
	DECLARE @SalvDocTypeID INT
	SELECT @SalvDocTypeID=DocumentTypeID FROM utb_document_type WHERE [Name] = 'Salvage Bid Report'
	
	UPDATE utb_form SET SQLProcedure='uspCFVSRGetXML', DocumentTypeID=@SalvDocTypeID WHERE InsuranceCompanyID = @ToInscCompID AND HTMLPath = 'Forms/AVIS_TLBidRep.asp'

	SELECT 'Client Custom Forms Updated...'
END
ELSE
BEGIN
	SELECT 'Client Custom Forms already updated...'
END

SELECT * FROM utb_form WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
