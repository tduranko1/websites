DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
DECLARE @DocTypeID INT
SET @ToInscCompID = 493
SET @FromInscCompID = 473
SET @DocTypeID = 0

------------------------------
-- Add Fax Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_form WHERE insurancecompanyid = @ToInscCompID)
BEGIN
	SELECT @DocTypeID=MAX(DocumentTypeID)+1 FROM utb_document_type
	INSERT INTO utb_document_type VALUES (@DocTypeID,@DocTypeID, 'C',1,0,'Vehicle Condition Request',1,0,CURRENT_TIMESTAMP)
	SELECT @DocTypeID=DocumentTypeID FROM utb_document_type WHERE [Name] = 'Vehicle Condition Request'

	INSERT INTO utb_form VALUES (NULL,22,NULL,@ToInscCompID,NULL,NULL,0,NULL,0,1,NULL,NULL,'Shop Assignment - Cancellation','RRPAssignmentCancel.pdf','S','RRP','uspWorkflowSendShopAssignXML',1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_form VALUES (NULL,@DocTypeID,NULL,@ToInscCompID,NULL,NULL,1,NULL,0,1,NULL,'Forms/ACE_BCIF.asp','Vehicle Condition Request','ACE_BCIF.pdf','S','RRP','uspCFBCIFGetXML',0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_form VALUES (NULL,22,NULL,@ToInscCompID,NULL,NULL,0,NULL,0,1,NULL,NULL,'Shop Assignment','RRP_FaxAssignment.pdf','S','RRP','uspWorkflowSendShopAssignXML',1,0,CURRENT_TIMESTAMP)
  SELECT 'Fax Template added...'
END
ELSE
BEGIN
	SELECT 'Fax Template already added...'
END

