DECLARE @DocumentTypeID INT
SET @DocumentTypeID = 0
SELECT @DocumentTypeID = DocumentTypeID FROM utb_document_type WHERE [Name] LIKE '%BCIF - Vehicle Evaluation Form%'

DECLARE @BundlingID INT
SET @BundlingID = 0
SELECT @BundlingID = BundlingID FROM utb_bundling WHERE [Name] = 'RRP - Closing Total Loss'

DECLARE @SUDocumentTypeID INT
SET @SUDocumentTypeID = 0
SELECT @SUDocumentTypeID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Supplement'

--SELECT * FROM utb_document_type WHERE NAME LIKE '%Approved Es%'
--SELECT * FROM utb_bundling_document_type

IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_bundling_document_type 
	WHERE  BundlingID = @BundlingID
	AND DocumentTypeID = @DocumentTypeID
)

BEGIN
	-- Add BCIF to Closing Total Loss
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID, @DocumentTypeID, NULL, NULL, 0, 0, 0, NULL, 0, NULL, 1, 5, NULL, 0,	0, 0, CURRENT_TIMESTAMP)
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundlingID AND DocumentTypeID = @SUDocumentTypeID

	SELECT 'Required Document Deleted...'
END
ELSE
BEGIN
	SELECT 'Required Document Already Deleted...'
END

-------------------------------------------------------

DECLARE @MBDocumentTypeID INT
SET @MBDocumentTypeID = 0
SELECT @MBDocumentTypeID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Memo Bill'

DECLARE @CSBundlingID INT
SET @CSBundlingID = 0
SELECT @CSBundlingID = BundlingID FROM utb_bundling WHERE [Name] = 'RRP - Closing Supplement'

DECLARE @CTLBundlingID INT
SET @CTLBundlingID = 0
SELECT @CTLBundlingID = BundlingID FROM utb_bundling WHERE [Name] = 'RRP - Closing Total Loss'

DECLARE @CICBundlingID INT
SET @CICBundlingID = 0
SELECT @CICBundlingID = BundlingID FROM utb_bundling WHERE [Name] = 'RRP - Closing Inspection Complete'

DECLARE @EDocumentTypeID INT
SET @EDocumentTypeID = 0
SELECT @EDocumentTypeID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Estimate'

DECLARE @SDocumentTypeID INT
SET @SDocumentTypeID = 0
SELECT @SDocumentTypeID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Supplement'

DECLARE @AEDocumentTypeID INT
SET @AEDocumentTypeID = 0
SELECT @AEDocumentTypeID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Approved Estimate Shop Notification'

/*
IF EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_bundling_document_type 
	WHERE  BundlingID = @CSBundlingID
	AND DocumentTypeID = @MBDocumentTypeID
)

BEGIN
	-- Delete Memo Bill from Closing Supplement
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @CSBundlingID AND DocumentTypeID = @MBDocumentTypeID

	-- Update Estimate for Closing Supplement
	UPDATE utb_bundling_document_type SET EstimateTypeCD = 'O', FinalEstimateFlag = '1' WHERE BundlingID = @CSBundlingID AND DocumentTypeID = @EDocumentTypeID

	-- Delete Supplement from Closing Supplement
	--SELECT * FROM utb_bundling_document_type WHERE BundlingID = @CTLBundlingID AND DocumentTypeID = @SDocumentTypeID
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @CTLBundlingID AND DocumentTypeID = @SDocumentTypeID

	-- Add Approved Estimate Shop Notification to Closing Supplement
	INSERT INTO utb_bundling_document_type VALUES (@CSBundlingID, @AEDocumentTypeID, NULL, 'I', 0, 0, 0, NULL, 0, NULL, 1, 1, NULL, 0,	0, 0, CURRENT_TIMESTAMP)

	-- Update Supplement for RRP - Closing Inspection Complete
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @CICBundlingID AND DocumentTypeID = @SDocumentTypeID
	UPDATE utb_bundling_document_type SET FinalEstimateFlag = '1' WHERE BundlingID = @CICBundlingID AND DocumentTypeID = @EDocumentTypeID
*/
	-- Update Estimate for Closing Total Loss
	SELECT * FROM utb_bundling_document_type WHERE BundlingID = @CTLBundlingID AND DocumentTypeID = @EDocumentTypeID
	UPDATE utb_bundling_document_type SET EstimateTypeCD = 'O', FinalEstimateFlag = '1' WHERE BundlingID = @CSBundlingID AND DocumentTypeID = @EDocumentTypeID

	SELECT 'Required Document Changed...'
END
ELSE
BEGIN
	SELECT 'Required Document Already Changed...'
END

-------------------------------------------------------

SET @MBDocumentTypeID = 0
SELECT @MBDocumentTypeID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Memo Bill'

SET @CSBundlingID = 0
SELECT @CSBundlingID = BundlingID FROM utb_bundling WHERE [Name] = 'RRP - Closing Supplement'

IF EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_bundling_document_type 
	WHERE  BundlingID = @CSBundlingID
	AND DocumentTypeID = @MBDocumentTypeID
)

BEGIN
	-- Delete Memo Bill from Closing Supplement
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundlingID AND DocumentTypeID = @DocumentTypeID
	SELECT 'Required Document Deleted...'
END
ELSE
BEGIN
	SELECT 'Required Document Already Deleted...'
END
