IF NOT EXISTS (
	SELECT
		* 
	FROM
		utb_app_variable
	WHERE
		[Name] = 'Trigger_Event_Recipients'
)
BEGIN
	INSERT INTO
		utb_app_variable
	VALUES (
		72
		, 'The default recipients of APD Event Log trigger emails and texts.'
		, NULL
		, NULL
		, 'Trigger_Event_Recipients'
		, 'TrgEvents'
		, 'jhornbeak@lynxservices.com'
		, 0
		, 0
		, CURRENT_TIMESTAMP
	)
	
	INSERT INTO
		utb_app_variable
	VALUES (
		73
		, 'Recipients of APD Event Log trigger emails and texts for Westfield Invoice Errors.'
		, NULL
		, NULL
		, 'Trigger_Event_Recipients-WestfieldInvoice'
		, 'TrgEvents-WES'
		, 'jhornbeak@lynxservices.com'
		, 0
		, 0
		, CURRENT_TIMESTAMP
	)
END