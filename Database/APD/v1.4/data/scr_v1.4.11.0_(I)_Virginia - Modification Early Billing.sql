DECLARE @InscCompID SMALLINT
DECLARE @BundlingID SMALLINT
DECLARE @DocumentTypeID SMALLINT
DECLARE @NewDocumentTypeID SMALLINT
DECLARE @MessageTemplateID SMALLINT
DECLARE @ClientBundlingID SMALLINT

SET @InscCompID = 192
SET @BundlingID = 0
SET @DocumentTypeID = 0
SET @MessageTemplateID = 0
SET @ClientBundlingID = 0

SET @NewDocumentTypeID = 0

	SELECT
		@BundlingID = b.BundlingID 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template t 
			ON t.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @InscCompID
		AND	b.[Name] = 'Initial Bill Estimate - Early Billing'
		
--------------------------------------
-- Initial Bill Estimate - Early Bill
-- Set Mandatory Doc Types
--------------------------------------
IF EXISTS (
	SELECT * FROM utb_bundling_document_type WHERE BundlingID = @BundlingID AND DocumentTypeID = 2
)
BEGIN
	UPDATE utb_bundling_document_type SET MandatoryFlag = 0 WHERE BundlingID = @BundlingID AND DocumentTypeID IN (2)

	SELECT 'Mandatory Doc -- Updated...'
END
ELSE
BEGIN
	SELECT 'Mandatory Doc -- Already Updated...'
END



