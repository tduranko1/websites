/**********************************************************************
* Add Client Desk Audit Reports for Unitrin
**********************************************************************/

begin transaction

select * from utb_client_report
where insuranceCompanyID = 261

insert into utb_client_report
(InsuranceCompanyID, reportID, SysLastUserID, SysLastUpdatedDate)
select 261, reportID, 0, current_timestamp
from utb_report
where description like 'desk audit%'
  and ReportTypeCD = 'C'

select * from utb_client_report
where insuranceCompanyID = 261


-- commit
-- rollback