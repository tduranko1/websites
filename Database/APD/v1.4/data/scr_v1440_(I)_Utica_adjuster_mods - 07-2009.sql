
BEGIN TRANSACTION

-- Setup users for MARO office - 92


update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Julia.Evans@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'John.Robertson@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Jessica.Calandra@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Kheila.Jones@uticanational.com'

-- Setup users for Albany office - 94

-- Setup users for Amherst office - 95

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Michael.Centrone@uticanational.com',
    @NameFirst             = 'Michael',
    @NameLast              = 'Centrone',
    @EmailAddress          = 'Michael.Centrone@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2347'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Alisa.Hofmann@uticanationalcom',
    @NameFirst             = 'Alisa',
    @NameLast              = 'Hofmann',
    @EmailAddress          = 'Alisa.Hofmann@uticanationalcom',
    @OfficeID              = 95,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2363'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Cynthia.Klein@uticanational.com',
    @NameFirst             = 'Cynthia',
    @NameLast              = 'Klein',
    @EmailAddress          = 'Cynthia.Klein@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2337'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Julie.Merkt@uticanational.com',
    @NameFirst             = 'Julie',
    @NameLast              = 'Merkt',
    @EmailAddress          = 'Julie.Merkt@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2358'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Nicole.Penna@uticanational.com',
    @NameFirst             = 'Nicole',
    @NameLast              = 'Penna',
    @EmailAddress          = 'Nicole.Penna@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2332'


-- Setup users for ERO office - 96

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Sue.Dailey@uticanational.com'

-- Setup users for Fast Track office - 97

-- Setup users for NYMRO office - 98


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Jeanette.Schmidt@uticanational.com',
    @NameFirst             = 'Jeanette',
    @NameLast              = 'Schmidt',
    @EmailAddress          = 'Jeanette.Schmidt@uticanational.com',
    @OfficeID              = 98,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '5075'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Susan.Harkoff@uticanational.com',
    @NameFirst             = 'Susan',
    @NameLast              = 'Harkoff',
    @EmailAddress          = 'Susan.Harkoff@uticanational.com',
    @OfficeID              = 98,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '5125'

-- Setup users for NERO office - 101

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Michael.Hallion@uticanational.com'

print 'Done.'

-- COMMIT
-- rollback