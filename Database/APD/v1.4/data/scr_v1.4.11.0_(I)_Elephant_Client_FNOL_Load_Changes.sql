IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'utb_fnol_claim_vehicle_load' 
           AND  COLUMN_NAME = 'SourceApplicationPassThruDataVeh')
BEGIN
	ALTER TABLE utb_fnol_claim_vehicle_load
		ADD SourceApplicationPassThruDataVeh VARCHAR(8000) 

	SELECT 'Table Column Added...'
END
ELSE
BEGIN
	SELECT 'Table Column Already Added...'
END           
