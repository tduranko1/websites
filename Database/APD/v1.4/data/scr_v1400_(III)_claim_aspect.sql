-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Cleans up claim aspect by deleting assignment aspects that are now carried in 
--        claim_aspect_status under the vehicle
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION

    
PRINT 'Removing assignment records from utb_claim_aspect...'

DELETE FROM dbo.utb_claim_aspect
    WHERE ClaimAspectTypeID NOT IN (0, 4, 9)

--commit
--rollback


