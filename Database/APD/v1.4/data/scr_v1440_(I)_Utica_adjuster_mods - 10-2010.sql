
BEGIN TRANSACTION


/* tmp selects....

select *
from utb_insurance
where InsuranceCompanyID = 259


select *
from utb_office
where InsuranceCompanyID = 259

select *
from utb_office_assignment_type
where officeID = 26

select *
from utb_assignment_type

.... */


-- Setup users for SERO office

DECLARE @InsuranceCompanyID     SMALLINT
DECLARE @OfficeID               SMALLINT

SET @InsuranceCompanyID = 259

SET @OfficeID = (SELECT OfficeID
		FROM utb_office
		WHERE InsuranceCompanyID = @InsuranceCompanyID
		AND ClientOfficeId = 'SERO')


-- ************************************	
-- Add new users for Utica SERO office
-- ************************************

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kristin.bonner@uticanational.com',
    @NameFirst             = 'Kristin',
    @NameLast              = 'Bonner',
    @EmailAddress          = 'kristin.bonner@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4204'
    
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'rachael.campese@uticanational.com',
    @NameFirst             = 'Rachael',
    @NameLast              = 'Campese',
    @EmailAddress          = 'rachael.campese@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4214'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'terri.chester@uticanational.com',
    @NameFirst             = 'Terri',
    @NameLast              = 'Chester',
    @EmailAddress          = 'terri.chester@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4208'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'matthew.combest@uticanational.com',
    @NameFirst             = 'Matthew',
    @NameLast              = 'Combest',
    @EmailAddress          = 'matthew.combest@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4132'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'michael.felten@uticanational.com',
    @NameFirst             = 'Mike',
    @NameLast              = 'Felten',
    @EmailAddress          = 'michael.felten@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4206'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'cassandra.forbes@uticanational.com',
    @NameFirst             = 'Cassandra',
    @NameLast              = 'Forbes',
    @EmailAddress          = 'cassandra.forbes@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4176'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'gizelle.grable@uticanational.com',
    @NameFirst             = 'Gizelle',
    @NameLast              = 'Grable',
    @EmailAddress          = 'gizelle.grable@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4210'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'gail.graves@uticanational.com',
    @NameFirst             = 'Gail',
    @NameLast              = 'Graves',
    @EmailAddress          = 'gail.graves@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4224'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'lauren.gross@uticanational.com',
    @NameFirst             = 'Lauren',
    @NameLast              = 'Gross',
    @EmailAddress          = 'lauren.gross@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4221'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'deana.henry@uticanational.com',
    @NameFirst             = 'Deana',
    @NameLast              = 'Henry',
    @EmailAddress          = 'deana.henry@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4100'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'dionne.howard@uticanational.com',
    @NameFirst             = 'Dionne',
    @NameLast              = 'Howard',
    @EmailAddress          = 'dionne.howard@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4212'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'albert.ingram@uticanational.com',
    @NameFirst             = 'Albert',
    @NameLast              = 'Ingram',
    @EmailAddress          = 'albert.ingram@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4215'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'george.mahon@uticanational.com',
    @NameFirst             = 'George',
    @NameLast              = 'Mahon',
    @EmailAddress          = 'george.mahon@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4216'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'silvia.martinez@uticanational.com',
    @NameFirst             = 'Silvia',
    @NameLast              = 'Martinez',
    @EmailAddress          = 'silvia.martinez@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4203'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'mark.mcgiboney@uticanational.com',
    @NameFirst             = 'Mark',
    @NameLast              = 'McGiboney',
    @EmailAddress          = 'mark.mcgiboney@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4217'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kristin.owens@uticanational.com',
    @NameFirst             = 'Kristin',
    @NameLast              = 'Owens',
    @EmailAddress          = 'kristin.owens@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4160'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'steve.sobolik@uticanational.com',
    @NameFirst             = 'Steve',
    @NameLast              = 'Sobolik',
    @EmailAddress          = 'steve.sobolik@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4110'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'martin.townes@uticanational.com',
    @NameFirst             = 'Martin',
    @NameLast              = 'Townes',
    @EmailAddress          = 'martin.townes@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4225'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'tory.tucker@uticanational.com',
    @NameFirst             = 'Tory',
    @NameLast              = 'Tucker',
    @EmailAddress          = 'tory.tucker@uticanational.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4211'

-- set users for ClaimPoint APD access (override hardcoded value of 4 in uspAdmAddAPDFNOLNewUser)
UPDATE utb_user_application
SET applicationID = 2
WHERE userID in 
	(SELECT UserID
	 FROM utb_user_application
	 WHERE userID in
		(SELECT userID
		 FROM utb_user
		 WHERE OfficeID = @OfficeID
		 AND EnabledFlag = 1))
AND userID NOT IN
	(SELECT UserID
	 FROM utb_user_application
	 WHERE applicationID = 2
	 AND userID in
		(SELECT userID
		 FROM utb_user
		 WHERE OfficeID = @OfficeID
		 AND EnabledFlag = 1))

-- update return fax number for all offices
UPDATE utb_office
SET ReturnDocFaxAreaCode = 972,
	ReturnDocFaxExchangeNumber = 301,
	ReturnDocFaxUnitNumber = 4211,
	SysLastUpdatedDate = Current_Timestamp
WHERE InsuranceCompanyID = @InsuranceCompanyID

print 'Done.'

 COMMIT
-- rollback