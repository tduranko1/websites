DECLARE @iBundlingID INT

IF EXISTS (SELECT * FROM utb_bundling WHERE [Name] LIKE '%RRP - Estimate Revision Shop Request%') 
BEGIN
	SELECT @iBundlingID = BundlingID FROM utb_bundling WHERE [Name] LIKE '%RRP - Estimate Revision Shop Request%'
	UPDATE utb_client_bundling SET ReturnDocPackageTypeCD = 'PDF', ReturnDocRoutingCD = 'EML', ReturnDocRoutingValue = 'qbeapd@lynxservices.com' WHERE BundlingID = @iBundlingID

	SELECT 'Documents Updated...'
END
ELSE
BEGIN
	SELECT 'Document Already Updated...'
END



