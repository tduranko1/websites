-------------------------------------------------------------------------------
-- QBE RRP Fax assignment fix
-------------------------------------------------------------------------------


DECLARE @InsuranceCompanyID as int
DECLARE @FormSupplementID as int

SELECT @InsuranceCompanyID = 304 -- QBE

SELECT @FormSupplementID = fs.FormSupplementID
FROM utb_form f
INNER JOIN utb_form_supplement fs ON f.FormID = fs.FormID
WHERE f.Name = 'Shop Assignment'
  AND fs.Name = 'RRP Shop Instructions'
  
-- Shop Instructions must always be named as "Shop Instructions". 
--  This instructs the custom form component to not stuff the pdf
--   since there aren't any user editable fields.

UPDATE utb_form_supplement
SET Name = 'Shop Instructions'
WHERE FormSupplementID = @FormSupplementID