/******************************************************************************
* QBE RRP Bundles for MDS
******************************************************************************/

DECLARE @NOW as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int

SET @NOW = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
  FROM utb_insurance
 WHERE Name = 'QBE Insurance'
 
IF @InsuranceCompanyID IS NULL
BEGIN
    RAISERROR('Unable to determine QBE InsuranceCompanyID', 16, 1)
    RETURN
END


-- Start a transaction
BEGIN TRANSACTION




------------------------------------------------------------------
-- Total Loss Alert (Adjuster)
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Total Loss Alert|Messages/msgRRPTotalLossAlert.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 36, @MessageTemplateID, 1, 'RRP - Total Loss Alert', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Use the Insurance company level Return Document settings.
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   NULL, 
   NULL,
   NULL,
   0,
   @now





------------------------------------------------------------------
-- Approved Estimate Shop Notification
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Approved Estimate Shop Notification|Messages/msgRRPApprovedEstimateShopNotification.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 36, @MessageTemplateID, 1, 'SHOP: RRP - Approved Estimate Shop Notification', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add the documents to the bundling profile
-- Add Approved Estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   NULL, 
   NULL,
   NULL,
   0,
   @now





------------------------------------------------------------------
-- Closing Inspection Complete
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Closing Inspection Complete|Messages/msgRRPClosingInspectionComplete.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'RRP - Closing Inspection Complete', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add the documents to the bundling profile
-- Add Memo Bill
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    75,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    NULL,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   'PDF', 
   'EML',
   'qbeapd@lynxservices.com',
   0,
   @now





------------------------------------------------------------------
-- Closing Supplement
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Closing Supplement|Messages/msgRRPClosingSupplement.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'RRP - Closing Supplement', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add the documents to the bundling profile
-- Add Memo Bill
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    75,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    NULL,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   'PDF', 
   'EML',
   'qbeapd@lynxservices.com',
   0,
   @now





------------------------------------------------------------------
-- Closing Total Loss
------------------------------------------------------------------
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'RRP - Closing Total Loss|Messages/msgRRPClosingTotalLoss.xsl', 'RRP', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'RRP - Closing Total Loss', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add the documents to the bundling profile
-- Add Memo Bill
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    75,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    NULL,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'RRP',
   'PDF', 
   'EML',
   'qbeapd@lynxservices.com',
   0,
   @now






SELECT * from utb_message_template
SELECT * FROM utb_bundling WHERE BundlingID = @BundlingID
SELECT * from utb_client_bundling WherE InsuranceCompanyID = @InsuranceCompanyID

-- commit
-- rollback
