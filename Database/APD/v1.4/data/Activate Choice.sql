--------------------------------------------
-- Activate Choice Process by Insc Company
--------------------------------------------
DECLARE @InsuranceCompanyID INT

SET @InsuranceCompanyID = 575

-----------------------------------
-- Add Choice process to 
-- utb_hyperquest_insurance
-----------------------------------
SELECT 'Currently Configured for Choice:'
SELECT 
	i.name
	, hi.EnabledFlag 
FROM 
	utb_hyperquest_insurance hi
	INNER JOIN utb_insurance i 
		ON i.InsuranceCompanyID = hi.InsuranceCompanyID

----------------------------------------
-- Add Choice process to @InscCompanyID
----------------------------------------
IF NOT EXISTS (SELECT * FROM utb_hyperquest_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
	INSERT INTO utb_hyperquest_insurance VALUES (@InsuranceCompanyID,1, CURRENT_TIMESTAMP)
END
ELSE
BEGIN
	SELECT 'Choice already added to utb_hyperquest_insurance'
END

----------------------------------------
-- Add Choice Assignment to @InscCompanyID
----------------------------------------
SELECT 'Currently Configured for Choice Assignments:'
SELECT *  FROM utb_client_assignment_type where assignmenttypeid = 17 order by SysLastUpdatedDate desc

IF NOT EXISTS (SELECT * FROM utb_client_assignment_type WHERE InsuranceCompanyID = @InsuranceCompanyID AND AssignmentTypeID = 17)
BEGIN
	INSERT INTO utb_client_assignment_type VALUES (@InsuranceCompanyID,17,0,CURRENT_TIMESTAMP)
END
ELSE
BEGIN
	SELECT 'Choice already added to utb_client_assignment_type'
END

----------------------------------------
-- Add Choice Service Channel
-- to @InscCompanyID
----------------------------------------
SELECT 'Currently Configured for Choice Service Channel:'
SELECT * FROM utb_client_service_channel where ServiceChannelCD = 'CS'

IF NOT EXISTS (SELECT * FROM utb_client_service_channel WHERE InsuranceCompanyID = @InsuranceCompanyID AND ServiceChannelCD = 'CS')
BEGIN
	INSERT INTO utb_client_service_channel VALUES (@InsuranceCompanyID,'CS',0,'C',0,CURRENT_TIMESTAMP)
END
ELSE
BEGIN
	SELECT 'Choice already added to utb_client_service_channel'
END

----------------------------------------
-- Add Choice Fee
-- to @InscCompanyID
----------------------------------------
DECLARE @ClientFeeID INT
SELECT 'Currently Configured for Choice FEE:'
SELECT * FROM utb_client_fee where Description = 'Choice Shop'
SELECT @ClientFeeID = ClientFeeID FROM utb_client_fee where Description = 'Choice Shop'
--SELECT @ClientFeeID

--SELECT * FROM utb_client_fee WHERE Description = 'Choice Shop'
IF NOT EXISTS (SELECT * FROM utb_client_fee WHERE InsuranceCompanyID = @InsuranceCompanyID AND Description = 'Choice Shop')
BEGIN
	INSERT INTO utb_client_fee
	SELECT 
		  [ClaimAspectTypeID]
		  ,@InsuranceCompanyID
		  ,[AdminFeeFlag]
		  ,[AppliesToCD]
		  ,[CategoryCD]
		  ,[ClientCode]
		  ,[Description]
		  ,[DisplayOrder]
		  ,[EffectiveEndDate]
		  ,[EffectiveStartDate]
		  ,[EnabledFlag]
		  ,[FeeAmount]
		  ,[FeeInstructions]
		  ,[HideFromInvoiceFlag]
		  ,[ItemizeFlag]
		  ,[InvoiceDescription]
		  ,[PaymentRequiredFlag]
		  ,[SysLastUserID]
		  ,[SysLastUpdatedDate]
		  ,[LowerLimit]
		  ,[UpperLimit]
		  ,[ApplyFeeRange]
	FROM utb_client_fee WHERE InsuranceCompanyID = 184 AND Description = 'Choice Shop'
END
ELSE
BEGIN
	SELECT 'Choice already added to utb_client_fee'
END

----------------------------------------
-- Add Choice Fee Trigger
-- to @InscCompanyID
----------------------------------------
SELECT @ClientFeeID = ClientFeeID FROM utb_client_fee where Description = 'Choice Shop' AND InsuranceCompanyID = @InsuranceCompanyID

IF NOT EXISTS (SELECT * FROM utb_client_fee_definition WHERE ClientFeeID = @ClientFeeID)
BEGIN
	INSERT INTO utb_client_fee_definition VALUES (@ClientFeeID,41,0,1,0, CURRENT_TIMESTAMP)
END
ELSE
BEGIN
	SELECT 'Choice already added to utb_client_fee_definition'
END

