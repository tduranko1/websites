
/*
	Filename:
	Author/Date: Walter J. Chesla - 12/30/2010
	Description:  This script will create new office configurations for Zurich Dallas, Omaha, and Sacramento
	
*/

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = (SELECT InsuranceCompanyID FROM utb_insurance WHERE name = 'Zurich North America')


PRINT 'Creating new Zurich offices...'

/* 
	Create Dallas Office
		Office Name:    Zurich NA (Dallas)                      
		Address:        PO Box 968072
						Schaumberg, IL 60196-8072
		Phone:	214-866-1525
		Fax:	866-689-8972
*/
INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
        'P.O. Box 968072',		-- Address1
        NULL,					-- Address2
        'Schaumberg',			-- AddressCity
        'IL',                  -- AddressState
        '60196',               -- AddressZip
        NULL,                  -- CCEmailAddress
        '^@1-@2 @3',                  -- ClaimNumberFormatJS
        '^(\\d{3})-{0,1}(\\d{7})\\s{0,}(\\d{0,3})$',                  -- ClaimNumberValidJS
        'nnn-nnnnnnn nnn\n where n is a number',                  -- ClaimNumberMsgText
        'Dallas',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '866',                 -- FaxAreaCode
        '689',                 -- FaxExchangeNumber
        '8972',                -- FaxUnitNumber
        'P.O. Box 968072',       -- MailingAddress1
        NULL,                  -- MailingAddress2
        'Schaumberg',               -- MailingAddressCity
        'IL',                  -- MailingAddressState
        '60196',               -- MailingAddressZip
        'Dallas',	-- Name
        '214',					-- PhoneAreaCode
        '866',                 -- PhoneExchangeNumber
        '1525',                -- PhoneUnitNumber
        NULL,                    -- ReturnDocEmailAddress			
        '866',                 -- ReturnDocFaxAreaCode				
        '689',                 -- ReturnDocFaxExchangeNumber (mailing fax number)
        '8972',                -- ReturnDocFaxUnitNumber
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)


SELECT @OfficeID = SCOPE_IDENTITY()


INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment')

    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


/* 
	Create Omaha Office
		Office Name:	Zurich NA (Omaha)                      
		Address:		PO Box 968070
						Schaumberg, IL 60196-8070
		Phone:	402-963-5000
		Fax:	888-515-1452
*/
INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
        'P.O. Box 968070',		-- Address1
        NULL,					-- Address2
        'Schaumberg',			-- AddressCity
        'IL',                  -- AddressState
        '60196',               -- AddressZip
        NULL,                  -- CCEmailAddress
        '^@1-@2 @3',                  -- ClaimNumberFormatJS
        '^(\\d{3})-{0,1}(\\d{7})\\s{0,}(\\d{0,3})$',                  -- ClaimNumberValidJS
        'nnn-nnnnnnn nnn\n where n is a number',                  -- ClaimNumberMsgText
        'Omaha',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '888',                 -- FaxAreaCode
        '515',                 -- FaxExchangeNumber
        '1452',                -- FaxUnitNumber
        'P.O. Box 968070',       -- MailingAddress1
        NULL,                  -- MailingAddress2
        'Schaumberg',               -- MailingAddressCity
        'IL',                  -- MailingAddressState
        '60196',               -- MailingAddressZip
        'Omaha',	-- Name
        '402',					-- PhoneAreaCode
        '963',                 -- PhoneExchangeNumber
        '5000',                -- PhoneUnitNumber
        NULL,                    -- ReturnDocEmailAddress			
        '888',                 -- ReturnDocFaxAreaCode				
        '515',                 -- ReturnDocFaxExchangeNumber (mailing fax number)
        '1452',                -- ReturnDocFaxUnitNumber
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)


SELECT @OfficeID = SCOPE_IDENTITY()


INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment')

    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code




/*
	Create Sacramento Office
		Office Name:    Zurich NA (Sacramento)
		Address:        3249 Quality Dr. Suite 300                           
						Rancho Cordova, CA 95670
		Phone:          303-224-4082
		Fax:            303-224-4063
*/   
INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
        '3249 Quality Dr.',		-- Address1
        'Suite 300',			-- Address2
        'Rancho Cordova',			-- AddressCity
        'CA',                  -- AddressState
        '95670',               -- AddressZip
        NULL,                  -- CCEmailAddress
        '^@1-@2 @3',                  -- ClaimNumberFormatJS
        '^(\\d{3})-{0,1}(\\d{7})\\s{0,}(\\d{0,3})$',                  -- ClaimNumberValidJS
        'nnn-nnnnnnn nnn\n where n is a number',                  -- ClaimNumberMsgText
        'Sacramento',			-- ClientOfficeId		
        1,                     -- EnabledFlag
        '303',                 -- FaxAreaCode
        '224',                 -- FaxExchangeNumber
        '4063',                -- FaxUnitNumber
        '3249 Quality Dr.',       -- MailingAddress1
        'Suite 300',                  -- MailingAddress2
        'Rancho Cordova',               -- MailingAddressCity
        'CA',                  -- MailingAddressState
        '95670',               -- MailingAddressZip
        'Sacramento',	-- Name
        '303',					-- PhoneAreaCode
        '224',                 -- PhoneExchangeNumber
        '4082',                -- PhoneUnitNumber
        NULL,                    -- ReturnDocEmailAddress			
        '303',                 -- ReturnDocFaxAreaCode				
        '224',                 -- ReturnDocFaxExchangeNumber (mailing fax number)
        '4063',                -- ReturnDocFaxUnitNumber
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)


SELECT @OfficeID = SCOPE_IDENTITY()


INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment')

    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code



PRINT 'Complete.'


-- Review affected tables
--SELECT * FROM dbo.utb_office WHERE OfficeID = @OfficeID
--SELECT * FROM dbo.utb_office_assignment_type WHERE OfficeID = @OfficeID
--SELECT * FROM dbo.utb_office_contract_state WHERE OfficeID = @OfficeID




