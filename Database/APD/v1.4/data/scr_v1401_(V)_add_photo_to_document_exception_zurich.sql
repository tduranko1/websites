/************************************************************************
* Add Photos to the exception list for Autoverse transmission
*************************************************************************/

-- Add Zurich photos to the document exception. The claims are sent back via Autoverse

insert into dbo.utb_client_document_exception
(InsuranceCompanyID, DocumentTypeID, OutputTypeCD, SysLastUserID, SysLastUpdatedDate)
values
(145, 8, NULL, 0, current_timestamp)

insert into dbo.utb_client_document_exception
(InsuranceCompanyID, DocumentTypeID, OutputTypeCD, SysLastUserID, SysLastUpdatedDate)
values
(209, 8, NULL, 0, current_timestamp)