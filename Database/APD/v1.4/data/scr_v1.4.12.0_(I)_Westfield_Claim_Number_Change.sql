UPDATE
	utb_office
SET
	ClaimNumberValidJS = '^([A-Z]{3})-(\\d{7})-(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(12|13|14|15)-([A-Z]{1})(\\d{2})$|^(\\d{10})$'
	, ClaimNumberMsgText = 'ABC-nnnnnnn-mmddyy-Ann\n or \n nnnnnnnnnn \n where n is a number\n mm is the month\n dd is the day\n yy is the year\n Ann is an occurrance number Exp: A01'
WHERE
	InsuranceCompanyID = 387
	AND ClaimNumberValidJS = '^([A-Z]{3})-(\\d{7})-(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])(12|13|14|15)-([A-Z]{1})(\\d{2})$'
