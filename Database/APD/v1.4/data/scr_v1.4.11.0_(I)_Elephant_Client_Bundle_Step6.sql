------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_bundling b INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE [Description] LIKE '%msgELPSAssignmentCancel%') 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Assignment Cancellation'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSAssignmentCancel%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'No Inspection (Stale) Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSStaleAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		35
		, MessageTemplateID 
		,1
		,'Cash Out Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSCashOutAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Status Update'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSStatusUpdAction.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		38
		, MessageTemplateID 
		,1
		,'Total Loss Alert - (Adjuster)'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSTLAlert.xsl%'

	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

SELECT * FROM utb_bundling WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)

