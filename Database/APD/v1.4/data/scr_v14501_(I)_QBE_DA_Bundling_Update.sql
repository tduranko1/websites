
/*********************************************/
--
-- QBE Desk Audit Bundling Changes
--		- WJC - 11/23/2010
--
/*********************************************/
DECLARE @Now as datetime
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int

SET @Now = CURRENT_TIMESTAMP
SET @InsuranceCompanyID = 304


-- Add Desk Audit Summary Report for Closing Audit Complete
IF NOT EXISTS
	(SELECT 1 FROM utb_bundling_document_type 
	 WHERE DocumentTypeID = 33
	 AND BundlingID = 
		(SELECT utb_client_bundling.BundlingID
		FROM utb_client_bundling
		LEFT OUTER JOIN utb_bundling
			ON utb_client_bundling.BundlingID = utb_bundling.BundlingID
		WHERE utb_client_bundling.InsuranceCompanyID = @InsuranceCompanyID
			AND utb_client_bundling.ServiceChannelCD = 'DA'
			AND utb_bundling.Name = 'Closing Audit Complete') )
BEGIN
	INSERT INTO utb_bundling_document_type (
		BundlingID,
		DocumentTypeID,
		DirectionalCD,
		DirectionToPayFlag,
		DuplicateFlag,
		EstimateDuplicateFlag,
		EstimateTypeCD,
		FinalEstimateFlag,
		MandatoryFlag,
		SelectionOrder,
		VANFlag,
		SysLastUserID,
		SysLastUpdatedDate
	) 
	SELECT 
			utb_client_bundling.BundlingID,	-- BundlingID,
			33,              -- DocumentTypeID,
			'I',            -- DirectionalCD,
			0,              -- DirectionToPayFlag,
			0,              -- DuplicateFlag,
			0,              -- EstimateDuplicateFlag,
			'A',            -- EstimateTypeCD,
			0,              -- FinalEstimateFlag,
			1,              -- MandatoryFlag,
			1,              -- SelectionOrder,
			1,              -- VANFlag,
			0,              -- SysLastUserID,
			@Now            -- SysLastUpdatedDate

		FROM utb_client_bundling
		LEFT OUTER JOIN utb_bundling
			ON utb_client_bundling.BundlingID = utb_bundling.BundlingID
		WHERE utb_client_bundling.InsuranceCompanyID = @InsuranceCompanyID
			AND utb_client_bundling.ServiceChannelCD = 'DA'
			AND utb_bundling.Name = 'Closing Audit Complete'
END

-- Add Desk Audit Summary Report for Closing Supplement
IF NOT EXISTS
	(SELECT 1 FROM utb_bundling_document_type 
	 WHERE DocumentTypeID = 33
	 AND BundlingID = 
		(SELECT utb_client_bundling.BundlingID
		FROM utb_client_bundling
		LEFT OUTER JOIN utb_bundling
			ON utb_client_bundling.BundlingID = utb_bundling.BundlingID
		WHERE utb_client_bundling.InsuranceCompanyID = @InsuranceCompanyID
			AND utb_client_bundling.ServiceChannelCD = 'DA'
			AND utb_bundling.Name = 'Closing Supplement') )
BEGIN
	INSERT INTO utb_bundling_document_type (
		BundlingID,
		DocumentTypeID,
		DirectionalCD,
		DirectionToPayFlag,
		DuplicateFlag,
		EstimateDuplicateFlag,
		EstimateTypeCD,
		FinalEstimateFlag,
		MandatoryFlag,
		SelectionOrder,
		VANFlag,
		SysLastUserID,
		SysLastUpdatedDate
	) 
	SELECT 
			utb_client_bundling.BundlingID,	-- BundlingID,
			33,              -- DocumentTypeID,
			'I',            -- DirectionalCD,
			0,              -- DirectionToPayFlag,
			0,              -- DuplicateFlag,
			0,              -- EstimateDuplicateFlag,
			'A',            -- EstimateTypeCD,
			0,              -- FinalEstimateFlag,
			1,              -- MandatoryFlag,
			1,              -- SelectionOrder,
			1,              -- VANFlag,
			0,              -- SysLastUserID,
			@Now            -- SysLastUpdatedDate

		FROM utb_client_bundling
		LEFT OUTER JOIN utb_bundling
			ON utb_client_bundling.BundlingID = utb_bundling.BundlingID
		WHERE utb_client_bundling.InsuranceCompanyID = @InsuranceCompanyID
			AND utb_client_bundling.ServiceChannelCD = 'DA'
			AND utb_bundling.Name = 'Closing Supplement'
END
		

-- Add Desk Audit Summary Report for Closing Total Loss
IF NOT EXISTS
	(SELECT 1 FROM utb_bundling_document_type 
	 WHERE DocumentTypeID = 33
	 AND BundlingID = 
		(SELECT utb_client_bundling.BundlingID
		FROM utb_client_bundling
		LEFT OUTER JOIN utb_bundling
			ON utb_client_bundling.BundlingID = utb_bundling.BundlingID
		WHERE utb_client_bundling.InsuranceCompanyID = @InsuranceCompanyID
			AND utb_client_bundling.ServiceChannelCD = 'DA'
			AND utb_bundling.Name = 'Closing Total Loss') )
BEGIN
	INSERT INTO utb_bundling_document_type (
		BundlingID,
		DocumentTypeID,
		DirectionalCD,
		DirectionToPayFlag,
		DuplicateFlag,
		EstimateDuplicateFlag,
		EstimateTypeCD,
		FinalEstimateFlag,
		MandatoryFlag,
		SelectionOrder,
		VANFlag,
		SysLastUserID,
		SysLastUpdatedDate
	) 
	SELECT 
			utb_client_bundling.BundlingID,	-- BundlingID,
			33,              -- DocumentTypeID,
			'I',            -- DirectionalCD,
			0,              -- DirectionToPayFlag,
			0,              -- DuplicateFlag,
			0,              -- EstimateDuplicateFlag,
			'A',            -- EstimateTypeCD,
			0,              -- FinalEstimateFlag,
			1,              -- MandatoryFlag,
			1,              -- SelectionOrder,
			1,              -- VANFlag,
			0,              -- SysLastUserID,
			@Now            -- SysLastUpdatedDate

		FROM utb_client_bundling
		LEFT OUTER JOIN utb_bundling
			ON utb_client_bundling.BundlingID = utb_bundling.BundlingID
		WHERE utb_client_bundling.InsuranceCompanyID = @InsuranceCompanyID
			AND utb_client_bundling.ServiceChannelCD = 'DA'
			AND utb_bundling.Name = 'Closing Total Loss'
END

-- Closing Total Loss:  invoice doc should not be mandatory
UPDATE utb_bundling_document_type 
SET MandatoryFlag = 0
WHERE DocumentTypeID = 13
AND BundlingID = 
	(SELECT utb_client_bundling.BundlingID
	 FROM utb_client_bundling
	 LEFT OUTER JOIN utb_bundling
	 	ON utb_client_bundling.BundlingID = utb_bundling.BundlingID
	 WHERE utb_client_bundling.InsuranceCompanyID = @InsuranceCompanyID
		AND utb_client_bundling.ServiceChannelCD = 'DA'
		AND utb_bundling.Name = 'Closing Total Loss') 
				
		
-- Estimate Type Code should be O (Original), not A (Audited)
update utb_bundling_document_type
set EstimateTypeCD = 'O',			-- was A
	VANFlag = 0
where BundlingID in
	(select bundlingID
	 from utb_client_bundling
	 where InsuranceCompanyID = 304
	 and ServiceChannelCD = 'DA')
and DocumentTypeID = 3	

-- Estimate Type Code should be A (Audited), not O(Original)
update utb_bundling_document_type
set EstimateTypeCD = 'A',			-- was O
	VANFlag = 0
where BundlingID in
	(select bundlingID
	 from utb_client_bundling
	 where InsuranceCompanyID = 304
	 and ServiceChannelCD = 'DA')
and DocumentTypeID = 10