
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!     SCRIPT FOR UPDATING IMAGING DATABASE TO NEXT VERSION          !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @table              AS SYSNAME          --            !!!!
            SET @table = 'utb_version'                      --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
            DECLARE @version AS VARCHAR(50)                 --            !!!!
            SET @version = 'v1.4.9.5'                       --            !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!                                                                   !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON



DECLARE @cmd                AS NVARCHAR(4000)
DECLARE @error              AS INT
DECLARE @return             AS INT
DECLARE @trancount          AS INT
DECLARE @ModifiedDateTime   AS DATETIME

SET @error = 0
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- Start the new transaction

IF 0 = @error
BEGIN
    SET @trancount = @@TRANCOUNT

    -- Capture any error

    SELECT @error = @@ERROR
END

IF 0 = @error
BEGIN
    BEGIN TRANSACTION

    -- Capture any error

    SELECT @error = @@ERROR
END
    

PRINT '.'
PRINT '.'
PRINT 'Update table... ' + @table
PRINT '.'

IF 0 = @error
BEGIN
    DELETE FROM dbo.utb_version

    -- Capture any error

    SELECT @error = @@ERROR
END


IF 0 = @error
BEGIN
    INSERT INTO utb_version
    (
        Version,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES
    (
        @Version,
        0,
        @ModifiedDateTime
    )

    -- Capture any error

    SELECT @error = @@ERROR
END


-- Final transaction success determination
    
IF @@TRANCOUNT > @trancount
BEGIN
    IF 0 = @error
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'COMMITTING Modified table... ' + @table + '!!!'
        PRINT '!'

        COMMIT TRANSACTION
    END
    ELSE
    BEGIN
        PRINT '!'
        PRINT '!'
        PRINT 'ROLLBACK Modified table... ' + @table + '!!!'
        PRINT '!'

        ROLLBACK TRANSACTION
    
        RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

    END
END
ELSE
BEGIN
    PRINT '!'
    PRINT '!'
    PRINT 'ABNORMAL TRANSACTION TERMINATION - Modified table... ' + @table + '!!!'
    PRINT '!'
    
    RAISERROR('ABNORMAL TRANSACTION TERMINATION', 16,1)

END


PRINT '.'
PRINT '.'
PRINT 'Modified table... ' + @table
PRINT '.'

SELECT @cmd = N'
    SELECT ''' + @table + ''', * FROM ' + @table

EXEC @return = sp_executesql @cmd
