-----------------------------------------------------------------------------------------------------------
-- This script will add three new offices for Utica. Albany, Amherst and Utica District Claims Office
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @PaymentCompanyID       smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 259

-- Create Office(s)

-- Albany office
-- The return documents are faxed to a central imaging server.

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,   -- InsuranceCompanyID
                '119 Great Oaks Blvd', -- Address1
                '',                    -- Address2
                'Albany',              -- AddressCity
                'NY',                  -- AddressState
                '12203',               -- AddressZip
                NULL,                  -- CCEmailAddress
                NULL,                  -- ClaimNumberFormatJS
                NULL,                  -- ClaimNumberValidJS
                NULL,                  -- ClaimNumberMsgText
                'ALBANY',              -- ClientOfficeId
                1,                     -- EnabledFlag
                '518',                 -- FaxAreaCode
                '862',                 -- FaxExchangeNumber
                '6299',                -- FaxUnitNumber
                'P.O. Box 15083',      -- MailingAddress1
                NULL,                  -- MailingAddress2
                'Albany',              -- MailingAddressCity
                'NY',                  -- MailingAddressState
                '12203',               -- MailingAddressZip
                'Albany Claims Office',-- Name
                '518',                 -- PhoneAreaCode
                '862',                 -- PhoneExchangeNumber
                '6200',                -- PhoneUnitNumber
                'AlbanyClaimsMailbox@UticaNational.com', -- ReturnDocEmailAddress
                '315',                 -- ReturnDocFaxAreaCode
                '266',                 -- ReturnDocFaxExchangeNumber
                '4121'                 -- ReturnDocFaxUnitNumber
                0,                     -- SysLastUserID
                @ModifiedDateTime      -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    WHERE StateCode in ('NY')


-- Add the Amherst Office
INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,   -- InsuranceCompanyID
                '130 John Muir Drive', -- Address1
                '',                    -- Address2
                'Getzville',           -- AddressCity
                'NY',                  -- AddressState
                '14068',               -- AddressZip
                NULL,                  -- CCEmailAddress
                NULL,                  -- ClaimNumberFormatJS
                NULL,                  -- ClaimNumberValidJS
                NULL,                  -- ClaimNumberMsgText
                'AMHERST',             -- ClientOfficeId
                1,                     -- EnabledFlag
                '716',                 -- FaxAreaCode
                '639',                 -- FaxExchangeNumber
                '2301',                -- FaxUnitNumber
                'P.O. Box 540',        -- MailingAddress1
                NULL,                  -- MailingAddress2
                'Getzville',           -- MailingAddressCity
                'NY',                  -- MailingAddressState
                '14068',               -- MailingAddressZip
                'Amherst Claims Office',-- Name
                '716',                 -- PhoneAreaCode
                '639',                 -- PhoneExchangeNumber
                '2300',                -- PhoneUnitNumber
                'AmherstClaimsMailbox@UticaNational.com', -- ReturnDocEmailAddress
                '315',                 -- ReturnDocFaxAreaCode
                '266',                 -- ReturnDocFaxExchangeNumber
                '4121'                 -- ReturnDocFaxUnitNumber
                0,                     -- SysLastUserID
                @ModifiedDateTime      -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    WHERE StateCode in ('NY')


-- Add Utica District Claims Office
INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,   -- InsuranceCompanyID
                '180 Genesee Street',  -- Address1
                '',                    -- Address2
                'New Hartford',        -- AddressCity
                'NY',                  -- AddressState
                '13413',               -- AddressZip
                NULL,                  -- CCEmailAddress
                NULL,                  -- ClaimNumberFormatJS
                NULL,                  -- ClaimNumberValidJS
                NULL,                  -- ClaimNumberMsgText
                'ERO',                 -- ClientOfficeId
                1,                     -- EnabledFlag
                '315',                 -- FaxAreaCode
                '734',                 -- FaxExchangeNumber
                '2977',                -- FaxUnitNumber
                'P.O. Box 530',        -- MailingAddress1
                NULL,                  -- MailingAddress2
                'Utica',               -- MailingAddressCity
                'NY',                  -- MailingAddressState
                '13503',               -- MailingAddressZip
                'Utica District Claims Office',-- Name
                '315',                 -- PhoneAreaCode
                '235',                 -- PhoneExchangeNumber
                '6600',                -- PhoneUnitNumber
                'EroClaimsMailbox@UticaNational.com', -- ReturnDocEmailAddress
                '315',                 -- ReturnDocFaxAreaCode
                '266',                 -- ReturnDocFaxExchangeNumber
                '4121'                 -- ReturnDocFaxUnitNumber
                0,                     -- SysLastUserID
                @ModifiedDateTime      -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    WHERE StateCode in ('NY')


-- Update MARO Office Return Document Email Address

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeID = 'MARO'
  
UPDATE dbo.utb_office
SET ReturnDocEmailAddress = 'MAROClaimsMailbox@UticaNational.com'
WHERE OfficeID = @OfficeID

-- Add NY state to the client contract state.
insert into dbo.utb_client_contract_state
(InsuranceCompanyID, StateCode, UseCEIShopsFlag, SysLastUserID, SysLastUpdatedDate)
values 
(259, 'NY', 0, 0, current_timestamp)

-- Review affected tables
SELECT * FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_office_assignment_type
SELECT * FROM dbo.utb_office_contract_state
SELECT * FROM dbo.utb_client_contract_state

--commit 
--rollback
