DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 473
SET @FromInscCompID = 198

------------------------------
-- Add Fax Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_form WHERE insurancecompanyid = @ToInscCompID)
BEGIN
	INSERT INTO utb_form VALUES (NULL,22,NULL,@ToInscCompID,NULL,NULL,0,NULL,0,1,NULL,NULL,'Shop Assignment - Cancellation','FaxAssignmentType473.pdf','S','RRP','uspWorkflowSendShopAssignXML',1,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_form VALUES (NULL,40,NULL,@ToInscCompID,NULL,NULL,1,NULL,0,1,NULL,'Forms/EL_BCIF.asp','BCIF Request','FaxAssignmentType473.pdf','S','RRP','uspCFBCIFGetXML',0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_form VALUES (NULL,22,NULL,@ToInscCompID,NULL,NULL,0,NULL,0,1,NULL,NULL,'Shop Assignment','FaxAssignmentType473.pdf','S','RRP','uspWorkflowSendShopAssignXML',1,0,CURRENT_TIMESTAMP)
  SELECT 'Fax Template added...'
END
ELSE
BEGIN
	SELECT 'Fax Template already added...'
END

