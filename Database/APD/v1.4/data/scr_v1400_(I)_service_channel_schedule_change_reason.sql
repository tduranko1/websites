-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Populates new table utb_service_channel_schedule_change_reason from 
--        utb_repair_schedule_change_reason
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION

PRINT 'Populating utb_service_channel_schedule_change_reason...'

--
-- TABLE INSERT STATEMENTS
--
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 1, 1, 1, 'Parts Delay', 'PS', 1, 1, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 2, 2, 1, 'Hidden Damage', 'PS', 1, 0, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 3, 3, 1, 'Service Provider Delay', 'PS', 1, 1, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 4, 4, 1, 'Vehicle Owner Delay', 'PS', 1, 1, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 5, 5, 1, 'Vehicle Owner Dissatisfaction', 'PS', 1, 0, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 6, 6, 1, 'Other', 'PS', 1, 1, 0, '02/20/2005 10:27:32.780 PM' ) 
go

INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 7, 1, 1, 'Parts Delay', 'ME', 1, 1, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 8, 2, 1, 'Hidden Damage', 'ME', 1, 0, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 9, 3, 1, 'Service Provider Delay', 'ME', 1, 1, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 10, 4, 1, 'Vehicle Owner Delay', 'ME', 1, 1, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 11, 5, 1, 'Vehicle Owner Dissatisfaction', 'ME', 1, 0, 0, '02/20/2005 10:27:32.780 PM' ) 
go
INSERT INTO dbo.utb_service_channel_schedule_change_reason ( ReasonID, DisplayOrder, EnabledFlag, Name, ServiceChannelCD, WorkStartConfirmFlag, WorkStartNotConfirmFlag, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 12, 6, 1, 'Other', 'ME', 1, 1, 0, '02/20/2005 10:27:32.780 PM' ) 
go

PRINT 'Complete!'

--commit
--rollback


