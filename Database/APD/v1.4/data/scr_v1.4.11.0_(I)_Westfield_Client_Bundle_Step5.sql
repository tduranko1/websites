DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 387

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE 'Status Update|Messages/msgWFPSStatusUpdActionMDS.xsl' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT 
		mt.AppliesToCD
		, mt.EnabledFlag
		, 'Status Update|Messages/msgWFPSStatusUpdActionMDS.xsl'
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM 
		utb_message_template mt 
	WHERE 
		[Description] LIKE 'Status Update|Messages/msgWFPSStatusUpdAction.xsl'

	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE 'Initial Estimate and Photos|Messages/msgWFPInitialEstimate.xsl' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT 
		mt.AppliesToCD
		, mt.EnabledFlag
		, 'Initial Estimate and Photos|Messages/msgWFPInitialEstimate.xsl'
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM 
		utb_message_template mt 
	WHERE 
		[Description] LIKE '%msgPSInitialEstimate.xsl%'

	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

------------------------------
-- Add Bundling
------------------------------
	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Status Update - MDS'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSStatusUpdActionMDS.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Initial Estimate and Photos (Adjuster)'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPInitialEstimate%'
		
------------------------------
-- Add Client Bundling
------------------------------
	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSStatusUpdActionMDS.xsl%'
		
	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPInitialEstimate.xsl%'
		

		