INSERT INTO utb_role (
   RoleID,
   BusinessFunctionCD,
   DisplayOrder,
   EnabledFlag,
   Name,
   SysMaintainedFlag,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 25, 'C', 104, 1, 'Total Loss Queue', 1, 0, current_timestamp

insert into utb_role_permission
select 25, permissionid, 0, 0, 0, 0, 0, current_timestamp
from utb_permission

INSERT INTO utb_permission (
   PermissionID,
   DisplayOrder,
   EnabledFlag,
   GroupCD,
   Name,
   SysMaintainedFlag,
   SysLastUserID,
   SysLastUpdatedDate
) 
SELECT 47,
       7,
       1,
       'A',
       'Action:View Total Loss Queue',
       1,
       0,
       current_timestamp

insert into utb_role_permission
select 25, 47, 0, 0, 1, 1, 0, current_timestamp

insert into utb_role_profile
select 25, 17, 9, 0, current_timestamp

insert into utb_role_profile
select 25, 32, 1, 0, current_timestamp

