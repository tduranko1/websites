/******************************************************************************
* Script to add message templates for Shop Remarks Templating
******************************************************************************/


DECLARE @now as datetime

SET @now = CURRENT_TIMESTAMP

BEGIN TRANSACTION

-- Add Drivable Vehicle with Rental Coverage
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   'S', --AppliesToCD,
   1, --EnabledFlag,
   'Drivable Vehicle with Rental Coverage|Messages/msgDrivableRental.xsl', --Description,
   'PS', --ServiceChannelCD,
   0, --SysLastUserID,
   @now  --SysLastUpdatedDate
)

-- Add Drivable Vehicle without Rental Coverage
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   'S', --AppliesToCD,
   1, --EnabledFlag,
   'Drivable Vehicle without Rental Coverage|Messages/msgDrivableNoRental.xsl', --Description,
   'PS', --ServiceChannelCD,
   0, --SysLastUserID,
   @now  --SysLastUpdatedDate
)

-- Add Non-Drivable Vehicle with Rental Coverage
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   'S', --AppliesToCD,
   1, --EnabledFlag,
   'Non-Drivable Vehicle with Rental Coverage|Messages/msgNonDrivableRental.xsl', --Description,
   'PS', --ServiceChannelCD,
   0, --SysLastUserID,
   @now  --SysLastUpdatedDate
)

-- Add Non-Drivable Vehicle without Rental Coverage
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   'S', --AppliesToCD,
   1, --EnabledFlag,
   'Non-Drivable Vehicle without Rental Coverage|Messages/msgNonDrivableNoRental.xsl', --Description,
   'PS', --ServiceChannelCD,
   0, --SysLastUserID,
   @now  --SysLastUpdatedDate
)

-- commit
-- rollback

SELECT * FROM dbo.utb_message_template
