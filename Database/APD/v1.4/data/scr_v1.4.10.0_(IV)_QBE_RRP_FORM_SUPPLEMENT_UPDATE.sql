DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime

SET @InsuranceCompanyID = 304
SET @now = CURRENT_TIMESTAMP

	

BEGIN
		UPDATE dbo.utb_form_supplement  
		SET  PDFPath = 'QBE_DTP.pdf'	
		from utb_form_supplement
		WHERE PDFPath = '' 
	            
	SELECT 'Updated Successfully...'
END


SELECT * FROM dbo.utb_form_supplement WHERE
			FormSupplementID IN 
			(
				SELECT 
					  fs.FormSupplementID 
				FROM 
					  utb_form f
					  INNER JOIN utb_form_supplement fs
					  ON fs.FormID = f.FormID
				WHERE 
					  f.InsuranceCompanyID =  @InsuranceCompanyID
					  AND fs.SERVICECHANNELCD = 'RRP'

)

