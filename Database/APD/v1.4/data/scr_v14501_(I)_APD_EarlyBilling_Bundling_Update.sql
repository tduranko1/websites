
/*
	scr_v14501_(I)_APD_EarlyBilling_Bundling_Update.sql
	 - Update setup for early billing bundling; change names from "approved" to "initial bill"
*/

update utb_bundling
set Name = 'Initial Bill Estimate - Early Billing'
where Name = 'Approved Estimate - Early Billing'


update utb_message_template
set [Description] = 'Initial Bill Estimate - Early Billing|Messages/msgPSApprovedEstimate_EB.xsl'
where [Description] = 'Approved Estimate - Early Billing|Messages/msgPSApprovedEstimate_EB.xsl'

