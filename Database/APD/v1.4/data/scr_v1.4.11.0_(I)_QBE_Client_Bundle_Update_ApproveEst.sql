DECLARE @iDocumentTypeID INT

IF EXISTS (SELECT * FROM utb_bundling WHERE [Name] LIKE '%RRP - Approved Estimate Shop Notification%') 
BEGIN
	SELECT @iDocumentTypeID = DocumentTypeID FROM utb_bundling WHERE [Name] = 'RRP - Estimate Revision Shop Request'
	UPDATE utb_bundling SET DocumentTypeID = @iDocumentTypeID WHERE [Name] LIKE '%RRP - Approved Estimate Shop Notification%'

	SELECT 'Documents Updated...'
END
ELSE
BEGIN
	SELECT 'Document Already Updated...'
END



