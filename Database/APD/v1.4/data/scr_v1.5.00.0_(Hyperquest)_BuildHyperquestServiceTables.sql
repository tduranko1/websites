-- Begin the transaction for dropping and creating the tables
--DROP TABLE [dbo].[utb_hyperquestsvc_jobs]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[utb_hyperquestsvc_jobs](
	[JobID] VARCHAR(255) NOT NULL,
	[InscCompID] INT,
	[LynxID] BIGINT,
	[ClaimNumber] VARCHAR(30) NOT NULL,
	[DocumentID] BIGINT NOT NULL,
	[AssignmentID] BIGINT NULL,
	[ReceivedDate] DATETIME NOT NULL,
	[DocumentTypeID] INT NOT NULL,
	[DocumentTypeName] VARCHAR(50) NOT NULL,
	[DocumentSource] VARCHAR(100) NOT NULL,
	[DocumentImageType] VARCHAR(10) NOT NULL,
	[ClaimAspectServiceChannelID] BIGINT NOT NULL,
	[ClaimAspectID] BIGINT NOT NULL,
	[JobXSLFile] VARCHAR(100) NULL,
	[JobStatus] VARCHAR(25) NOT NULL,
	[JobStatusDetails] VARCHAR(255),
	[JobPriority] INT,
	[JobCreatedDate] DATETIME NULL,
	[JobProcessedDate] DATETIME NULL,
	[JobTransformedDate] DATETIME NULL,
	[JobTransmittedDate] DATETIME NULL,
	[JobArchivedDate] DATETIME NULL,
	[JobFinishedDate] DATETIME NULL,
	[EnabledFlag] bit NOT NULL,
	[JobXML] xml,
	[SysLastUserID] INT NULL,
	[SysLastUpdatedDate] DATETIME NULL
 CONSTRAINT [PK_utb_hyperquestsvc_jobs] PRIMARY KEY CLUSTERED 
(
	[DocumentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [ufg_primary]
) ON [ufg_primary]
GO

SET ANSI_PADDING ON
GO

