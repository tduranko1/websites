/**************************************************************************************
* Utica Bundling changes - Closing repair complete
**************************************************************************************/
DECLARE @InsuranceCompanyID as int
DECLARE @MessageTemplateID as int
DECLARE @DocumentTypeID as int
DECLARE @BundlingID as int 
DECLARE @now as datetime

SET @now = current_timestamp

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Utica National Insurance Group'


BEGIN TRANSACTION

/************************************************/
/***** Closing Repair Complete - Non Photos *****/
/************************************************/

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE Description like 'Closing Repair Complete (Non Photos)%'

IF @MessageTemplateID IS NULL
BEGIN
    -- Create the message template
    INSERT INTO utb_message_template
    (
        EnabledFlag,
        Description,
        ServiceChannelCD,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        1,      --EnabledFlag,
        'Closing Repair Complete (Non Photos)|Messages/msgPSClosingRepairComplete_NonPhotos.xsl', --Description,
        'PS',
        0,
        @now
    )

    SET @MessageTemplateID = SCOPE_IDENTITY()
END

-- Create a new bundling for non-photos
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Repair Complete (Non Photos)%" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Repair Complete (Non-Photos)', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Repair Complete (Non-Photos)"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add DTP
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    2,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    1,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Detach any existing Closing Repair complete for Utica
DELETE FROM utb_client_bundling
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND BundlingID in (SELECT BundlingID
                        FROM dbo.utb_bundling
                        WHERE Name = 'Closing Repair Complete')

-- Add this new bundling tasks to Utica
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
VALUES
(@BundlingID, @InsuranceCompanyID, 'PS', 0, @now)




/********************************************/
/***** Closing Repair Complete - Photos *****/
/********************************************/

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE Description like 'Closing Repair Complete (Photos)%'

IF @MessageTemplateID IS NULL
BEGIN
    -- Create the message template
    INSERT INTO utb_message_template
    (
        EnabledFlag,
        Description,
        ServiceChannelCD,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        1,      --EnabledFlag,
        'Closing Repair Complete (Photos)|Messages/msgPSClosingRepairComplete_Photos.xsl', --Description,
        'PS',
        0,
        @now
    )

    SET @MessageTemplateID = SCOPE_IDENTITY()
END

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Repair Complete (Photos)%" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Repair Complete (Photos)', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add Photos
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    8,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    1,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this new bundling tasks to Utica
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
VALUES
(@BundlingID, @InsuranceCompanyID, 'PS', 0, @now)

select * from dbo.utb_message_template
select * from dbo.utb_bundling
select * from dbo.utb_client_bundling WHERE InsuranceCompanyID = @InsuranceCompanyID

-- commit
-- rollback