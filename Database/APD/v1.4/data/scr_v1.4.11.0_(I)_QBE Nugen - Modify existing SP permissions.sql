-- Modify existing SP permissions
GRANT EXECUTE ON dbo.uspFNOLInsClaim TO wsAPDUser
GRANT EXECUTE ON dbo.uspFNOLLoadInvolved TO wsAPDUser
GRANT EXECUTE ON dbo.uspFNOLLoadClaimVehicle TO wsAPDUser
GRANT EXECUTE ON dbo.uspFNOLLoadClaim TO wsAPDUser
GRANT EXECUTE ON dbo.uspFNOLLoadClaimCoverage TO wsAPDUser
GRANT EXECUTE ON dbo.uspWorkflowUpdateAssignmentStatus TO wsAPDUser


