DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 464
SET @FromInscCompID = 387

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (
	SELECT 
		*
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @ToInscCompID	
		AND mt.[Description] LIKE '%msgCWPSStatusUpdAction.xsl%'
) 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPSStatusUpdAction.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgWFPSStatusUpdAction.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPSStatusUpdActionMDS.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgWFPSStatusUpdActionMDS.xsl%'

	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

SELECT * FROM utb_bundling WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
