DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 493
SET @FromInscCompID = 473

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (
	SELECT 
		*
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgACERRPClmAck.xsl%'
) 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPClmAck.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPClmAck.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPAssignmentCancel.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPAssignmentCancel.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPStaleAlertAndClose.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPStaleAlertAndClose.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPStatusUpdateNotificationOnly.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPStatusUpdateNotificationOnly.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPClosingInspectionComplete.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPClosingInspectionComplete.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPClosingSupplement.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPClosingSupplement.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPClosingTotalLoss.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPClosingTotalLoss.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPStatusUpdateActionRequired.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPStatusUpdateActionRequired.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPApprovedEstimateShopNotification.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPApprovedEstimateShopNotification.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPTotalLossAlert.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPTotalLossAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgACERRPTotalLossDetermination.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPTotalLossDetermination.xsl%'

	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

SELECT * FROM utb_bundling WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
