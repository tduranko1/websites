/******************************************************************************
* AOI Client Setup updates
******************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @BundlingID as int
DECLARE @DocumentTypeID as int
DECLARE @now as datetime

SET @now = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Auto-Owners Insurance'

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

BEGIN TRANSACTION

-- Document bunlding destination setup
UPDATE utb_insurance
SET ReturnDocDestinationCD = 'OFC',
    ReturnDocPackageTypeCD = 'PDF',
    ReturnDocRoutingCD = 'EML',
    TotalLossValuationWarningPercentage = 0.7,
    TotalLossWarningPercentage = 0.7,
    DemoFlag = 0
WHERE InsuranceCompanyID = @InsuranceCompanyID

UPDATE utb_office
SET ReturnDocEmailAddress = 'QCDeskReview@aoins.com'
WHERE InsuranceCompanyID = @InsuranceCompanyID

-- Bulk billing
UPDATE utb_client_service_channel
SET InvoicingModelBillingCD = 'B'
WHERE InsuranceCompanyID = @InsuranceCompanyID

-- COMMIT
-- ROLLBACK
