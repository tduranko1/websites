-- Texas Farm ID 184
-- North Carolina Farm ID 327
-- QBE ID 304
-- Virginia Farm ID 192

-- Elephant ID 367OLD new is 374
-- Republic Group ID 277
-- Electric 158
DECLARE @InscCompID SMALLINT

SET @InscCompID = 304

-- Show Company Config Info by Insc Company
SELECT 'Show Company Config Info'
SELECT 
	* 
FROM
	utb_insurance 
WHERE 
	InsuranceCompanyID = @InscCompID

-- Client Assignment by Insc Company
SELECT 'Client Assignment'
SELECT 
	* 
FROM
	utb_client_assignment_type 
WHERE 
	InsuranceCompanyID = @InscCompID

-- Client Service Channel by Insc Company
SELECT 'Client Service Channel'
SELECT 
	* 
FROM
	utb_client_service_channel 
WHERE 
	InsuranceCompanyID = @InscCompID

-- Client Contract State by Insc Company
SELECT 'Client Contract State'
SELECT 
	* 
FROM
	utb_client_contract_state 
WHERE 
	InsuranceCompanyID = @InscCompID

-- Client Coverage Type by Insc Company
SELECT 'Client Coverage Type'
SELECT 
	* 
FROM
	utb_client_coverage_type
WHERE 
	InsuranceCompanyID = @InscCompID

-- Client Claim Aspect by Insc Company
SELECT 'Client Claim Aspect'
SELECT 
	* 
FROM
	utb_client_claim_aspect_type
WHERE 
	InsuranceCompanyID = @InscCompID

-- Client Report by Insc Company
SELECT 'Client Report'
SELECT 
	* 
FROM
	utb_client_report
WHERE 
	InsuranceCompanyID = @InscCompID

-- Client Office by Insc Company
SELECT 'Client Office'
SELECT 
	* 
FROM
	utb_office
WHERE 
	InsuranceCompanyID = @InscCompID

-- Client Office Assignment Type by Office
SELECT 'Client Office Assignment'
DECLARE @OfficeID AS INT
SELECT @OfficeID = OfficeID FROM utb_office WHERE InsuranceCompanyID = @InscCompID

SELECT 
	* 
FROM
	utb_office_assignment_type
WHERE 
	OfficeID = @OfficeID

-- Client Office Contact State by Office
SELECT 'Client Office Contact State'
SELECT 
	* 
FROM
	utb_office_contract_state
WHERE 
	OfficeID = @OfficeID

-- Show Workflows by Insc Company
SELECT 'Show Workflows'
SELECT 
	* 
FROM
	utb_workflow 
WHERE 
	InsuranceCompanyID = @InscCompID
	
-- Show Spawns by Insc Company
SELECT 'Show Spawns'
SELECT 
	* 
FROM 
	utb_spawn 
WHERE 
	WorkflowID IN
	(
		SELECT WorkflowID FROM utb_workflow WHERE InsuranceCompanyID = @InscCompID
	)

-- Show Bundles by Insc Company
SELECT 'Show Bundles'
SELECT
	b.* 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = @InscCompID
	--AND serviceChannelCD = 'DA'

-- Show Bundle Messages by Insc Company
SELECT 'Show Bundle Messages'
SELECT
	t.* 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @InscCompID
--	AND cb.serviceChannelCD = 'DA'

-- Show Client Bundling by Insc Company
SELECT 'Show Client Bundle Messages'
SELECT
	* 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @InscCompID

-- Show Bundling Document Types by Insc Company
SELECT 'Show Bundling Document Types'

SELECT 
	* 
FROM 
	utb_bundling_document_type bdt
	INNER JOIN utb_document_type dt
	ON dt.DocumentTypeID = bdt.DocumentTypeID 
WHERE 	
	bdt.BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @InscCompID
	)

-- Show Closing Document Bundling items by Insc Company
CREATE TABLE #DocTypeTmp
(
	DocumentTypeID INT
	, DocName VARCHAR(100)
)

INSERT INTO
	#DocTypeTmp
	SELECT DISTINCT
		b.DocumentTypeID 
		, (SELECT [Name] FROM utb_document_type WHERE DocumentTypeID = b.DocumentTypeID ) AS DocName
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
	WHERE
		cb.InsuranceCompanyID = 304	
	ORDER BY
		b.DocumentTypeID

DECLARE	@DocumentTypeID INT
DECLARE @DocName VARCHAR(100)

DECLARE db_cursor CURSOR FOR
	SELECT
		*
	FROM
		#DocTypeTmp

OPEN db_cursor	
FETCH NEXT FROM db_cursor INTO 
		@DocumentTypeID
		, @DocName

WHILE @@FETCH_STATUS = 0  
BEGIN 
	SELECT 'Show Bundling - ' + @DocName + ' Package'	

	SELECT
		b.* 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN #DocTypeTmp dt 
			ON dt.DocumentTypeID = b.DocumentTypeID
	WHERE
		cb.InsuranceCompanyID = @InscCompID
		AND EnabledFlag = 1
		AND b.DocumentTypeID = @DocumentTypeID

FETCH NEXT FROM db_cursor INTO 
		@DocumentTypeID
		, @DocName
END  

CLOSE db_cursor  
DEALLOCATE db_cursor 

--SELECT * FROM #DocTypeTmp

DROP TABLE #DocTypeTmp

-- Show Labor Rates by Insc Company
SELECT 'Show Labor Rates'
SELECT
	* 
FROM
	utb_labor_rate
WHERE
	InsuranceCompanyID = @InscCompID

-- Show Fax Template by Insc Company
SELECT 'Show Fax Template'
SELECT
	* 
FROM
	utb_form
WHERE
	InsuranceCompanyID = @InscCompID

-- Show Form Supplements by Insc Company
SELECT 'Show Form Supplements'
SELECT 
	* 
FROM 
	utb_form_supplement 
WHERE 
	FormID IN 
	(
		SELECT 
			FormID 
		FROM 
			utb_form
		WHERE 
			InsuranceCompanyID = @InscCompID
	)

-- Show Payment by Insc Company
SELECT 'Show Payment'
SELECT 
	* 
FROM 
	utb_client_Payment_Type 
WHERE 
	InsuranceCompanyID = @InscCompID
