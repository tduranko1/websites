/******************************************************************************
* LAFB Closing Stale bundling profile
*   Send via FTP
*   Select invoice.
*   Custom message template (Stale Alert & Close)
******************************************************************************/

declare @BundlingID as int
declare @MessageTemplateID as int
declare @InsuranceCompanyID as int
declare @now as datetime

set @now = current_timestamp

select @InsuranceCompanyID = InsuranceCompanyID
from utb_insurance 
where Name = 'Louisiana Farm Bureau'

IF @InsuranceCompanyID IS NOT NULL
BEGIN
   BEGIN TRANSACTION
   
   SELECT @MessageTemplateID = MessageTemplateID
   FROM utb_message_template
   WHERE Description = 'Stale Alert & Close|Messages/msgPSStaleAlertClose.xsl'
     AND ServiceChannelCD = 'PS'
   
   INSERT INTO utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- Saved as Closing Document
      @MessageTemplateID,
      1,
      'Closing - Stale',
      0,
      current_timestamp
   )
   
   SET @BundlingID = SCOPE_IDENTITY()

   -- Add invoice
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      13,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   
   INSERT INTO utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,
      @InsuranceCompanyID,
      'PDF',
      'FTP',
      '$CONFIG$:ClientDocument/Insurance[@ID="279"]/FTP',
      'PS',
      0,
      CURRENT_TIMESTAMP
   )
   
   select * from utb_client_bundling where InsuranceCompanyID = @InsuranceCompanyID
   select * from utb_bundling where bundlingID = @BundlingID
   select * from utb_bundling_document_type where BundlingID = @BundlingID

END

-- commit
-- rollback
