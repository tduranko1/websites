-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configuration for Utica New York Metro Regional Office
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Utica National Insurance Group'


PRINT '.'
PRINT '.'
PRINT 'Creating Residual Markets Office...'
PRINT '.'


-- Create Office(s)

-- Central Residual Markets Office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '210 LaFayette Street', -- Address1
                '',                     -- Address2
                'Utica',                -- AddressCity
                'NY',                   -- AddressState
                '13502',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Residual Markets',     -- ClientOfficeId
                1,                      -- EnabledFlag
                '',					       -- FaxAreaCode
                '',					       -- FaxExchangeNumber
                '',					       -- FaxUnitNumber
                'PO Box 530',			    -- MailingAddress1
                NULL,                   -- MailingAddress2
                'Utica',			       -- MailingAddressCity
                'NY',					    -- MailingAddressState
                '13503',				    -- MailingAddressZip
                'Residual Markets',     -- Name
                '315',					    -- PhoneAreaCode
                '235',					    -- PhoneExchangeNumber
                '6804',					    -- PhoneUnitNumber
                NULL,					    -- ReturnDocEmailAddress
                NULL,					    -- ReturnDocFaxAreaCode
                NULL,					    -- ReturnDocFaxExchangeNumber
                NULL,					    -- ReturnDocFaxUnitNumber
                0,						    -- SysLastUserID
                @ModifiedDateTime		 -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()

-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Program Shop Assignment', 'IA Estimate Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

    
-- Review affected tables
SELECT * FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT oat.* 
FROM dbo.utb_office_assignment_type oat 
LEFT JOIN utb_office o ON oat.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT ocs.* 
FROM dbo.utb_office_contract_state  ocs
LEFT JOIN utb_office o ON ocs.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID


--commit 
--rollback
