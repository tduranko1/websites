/*******************************************************************************
* APD Messaging
*******************************************************************************/
DECLARE @now as datetime

SET @now = CURRENT_TIMESTAMP

BEGIN TRANSACTION

-- Add Program Shop Claim Acknowledgement
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Claim Acknowledgment|Messages/msgPSClmAck.xsl',
    'PS',
    0,
    @now
)

-- Add Program Shop - Initial Estimate
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Initial Estimate|Messages/msgPSInitialEstimate.xsl',
    'PS',
    0,
    @now
)

-- Add Program Shop - Status Updates
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Status Update|Messages/msgPSStatusUpdate.xsl',
    'PS',
    0,
    @now
)

-- Add Program Shop - Closing Repair Complete
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Closing Repair Complete|Messages/msgPSClosingRepairComplete.xsl',
    'PS',
    0,
    @now
)

-- Add Program Shop - Closing Supplement
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Closing Supplement|Messages/msgPSClosingSupplement.xsl',
    'PS',
    0,
    @now
)

-- Add Program Shop - Status Alert & Close
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Stale Alert & Close|Messages/msgPSStaleAlertClose.xsl',
    'PS',
    0,
    @now
)

-- Add Program Shop - Cash Out Alert & Close
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Cash Out Alert & Close|Messages/msgPSCashOutAlertClose.xsl',
    'PS',
    0,
    @now
)

-- Add Program Shop - Total Loss Alert (Adjuster)
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Total Loss Alert (Adjuster)|Messages/msgPSTLAlertAdjuster.xsl',
    'PS',
    0,
    @now
)

-- Add Program Shop - Total Loss Closing
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Total Loss Closing|Messages/msgPSTLClosing.xsl',
    'PS',
    0,
    @now
)

-- Add Desk Audit - Additional Info Request
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Additional Info Request|Messages/msgDAAdditionalInfo.xsl',
    'DA',
    0,
    @now
)

-- Add Desk Audit - Closing Audit Complete
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Closing Audit Complete|Messages/msgDAClosingAuditComplete.xsl',
    'DA',
    0,
    @now
)

-- Add Desk Audit - Closing Supplement
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Closing Supplement|Messages/msgDAClosingSupplement.xsl',
    'DA',
    0,
    @now
)

-- Add Adverse Subro - Additional Info Request
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Additional Info Request|Messages/msgDRAdditionalInfo.xsl',
    'DR',
    0,
    @now
)

-- Add Adverse Subro - Closing Repair Complete
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Closing Audit Complete|Messages/msgDRClosingAuditComplete.xsl',
    'DR',
    0,
    @now
)

-- Add Adverse Subro - Closing Supplement
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Closing Supplement|Messages/msgDRClosingSupplement.xsl',
    'DR',
    0,
    @now
)

-- Add Program Shop Assignment Cancellation
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Assignment Cancellation|Messages/msgPSCancel.xsl',
    'PS',
    0,
    @now
)


-- select tables affected
select * from dbo.utb_message_template

-- commit
-- rollback

