/******************************************************************************
* Document Bundling options for LAFB PS
******************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime
DECLARE @BundlingID as int
DECLARE @MessageTemplateID as int

SET @now = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Louisiana Farm Bureau'

IF @InsuranceCompanyID IS NOT NULL
BEGIN
   BEGIN TRANSACTION
   
   --------------------------------------------
   -- Create Message Template
   --------------------------------------------
   INSERT INTO dbo.utb_message_template (
      AppliesToCD,
      EnabledFlag,
      Description,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      'C',
      1,
      'LAFB Transfer Alert and Cash Out Close|Messages/msgPSTransferAlertCashOut.xsl',
      'PS',
      0,
      @now
   )
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   --------------------------------------------
   -- Create Transfer Alert and Cash Out bundle
   --------------------------------------------
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'LAFB Transfer Alert and Cash Out Close', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add estimate
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      3,             -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      'A',           -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      1,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add invoice
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      13,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      2,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add photos
   INSERT INTO utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      MandatoryFlag,
      SelectionOrder,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) values (
      @BundlingID,
      8,
      'I',
      0,
      1,
      0,
      NULL,
      0,
      0,
      3,
      0,
      0,
      current_timestamp
   )

   -- add it to the client bundling
   INSERT INTO utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,
      @InsuranceCompanyID,
      'PDF',
      'FTP',
      '$CONFIG$:ClientDocument/Insurance[@ID="279"]/FTP',
      'PS',
      0,
      @now
   )
   

   select dt.Name, b.*, bdt.* 
   from utb_bundling_document_type bdt
   left join utb_document_type dt on bdt.DocumentTypeID = dt.DocumentTypeID
   left join utb_bundling b on bdt.BundlingID = b.BundlingID
   left join utb_client_bundling cb on b.BundlingID = cb.BundlingID
   where cb.InsuranceCompanyID = @InsuranceCompanyID
     and cb.ServiceChannelCD = 'PS'   
END


-- COMMIT
-- ROLLBACK
