/************************************************************************************************
*Project:	210474 APD Multiple Concurrent Service Channels										*
*Script:	This script extracts the information using tmp_utb_Claim_Aspect table, and 			*
*			Tmp_utb_claim_coverage table to provide the following data for insertion into		*
*			utb_Claim_Aspect_Service_Channel_Coverage											*
*																								*
*			A- Program Shop - PS																*
*				  i) Coverage Profile Code/Coverage Type Code "COLL" (Collision)				*
*				 ii) Coverage Profile Code/Coverage Type Code "COMP" (Comprehensive)			*
*				iii) Coverage Profile Code/Coverage Type Code "LIAB" (Liability)				*
*				 iv) Coverage Profile Code/Coverage Type Code "RENT" (Rental)					*
*				  v) Coverage Profile Code/Coverage Type Code "UIM"  (Under Insured Motorist)	*
*				 vi) Coverage Profile Code/Coverage Type Code "UM"   (Uninsured Motorist)		*
*																								*
*			B- Mobile Electronics - ME															*
*				  i) Coverage Profile Code/Coverage Type Code "COLL" (Collision)				*
*				 ii) Coverage Profile Code/Coverage Type Code "COMP" (Comprehensive)			*
*				iii) Coverage Profile Code/Coverage Type Code "LIAB" (Liability)				*
*				 iv) Coverage Profile Code/Coverage Type Code "RENT" (Rental)					*
*				  v) Coverage Profile Code/Coverage Type Code "UIM"  (Under Insured Motorist)	*
*				 vi) Coverage Profile Code/Coverage Type Code "UM"   (Uninsured Motorist)		*
*																								*
*			Once the above is done, additional data must be extracted using tmp_utb_Claim_Aspect*
*			and tmp_utb_mobile_electronics_results tables.											*
*																								*
*																								*
*************************************************************************************************
*    IMPORTANT!  !IMPORTANT!  !IMPORTANT!  !IMPORTANT!  !IMPORTANT!  !IMPORTANT!  !IMPORTANT!   *
*************************************************************************************************
*		Please either COMMIT or ROLLBACK the transaction after you've run the script.  			*
*								This must be done manually.										*
************************************************************************************************/

/**********************************************************************
Check if the Temp Tables exist in the database, if so remove them.
**********************************************************************/

if (select count(*) from tempdb..sysobjects where name like '#t1%') =1
	begin
		drop table #t1
	end

if (select count(*) from tempdb..sysobjects where name like '#t2%') =1
	begin
		drop table #t2
	end
	

/*********************************
Create a table to hold the rows
*********************************/
--drop table #t1  --select * from #t1 where CoverageTypeCD is null  --ClaimAspectID = 71 with LynxID = 116729 has a CoverageTypeCD of NULL
Create table #t1	(
					ClaimAspectID udt_std_id_big,
					LynxID udt_std_id_big,
					ServiceChannelCD udt_std_cd,
					DispositionTypeCD udt_std_cd null,
					CoverageTypeCD udt_std_cd null,
					DeductibleAmt udt_cov_deductible,
					SysLastUserID udt_std_id,
					SysLastUpdatedDate udt_sys_last_updated_date
					)


/***********************************
First case (ServiceChannelCD + COLL)
***********************************/
insert into #t1
select		ca.ClaimAspectID,
			ca.LynxID,
			ca.ServiceChannelCD,
			ca.DispositionTypeCD,
			'COLL', --ca.CoverageProfileCD,
			cc.CollisionDeductibleAmt,
			cc.SysLastUserID,
			cc.SysLastUpdatedDate

from		Tmp_utb_claim_aspect ca
inner join	Tmp_utb_claim_coverage cc
on			ca.LynxID = cc.LynxID

where		ca.ServiceChannelCD in('PS','ME')
and			NOT (cc.CollisionDeductibleAmt is null or cc.CollisionDeductibleAmt= 0)
--and			ca.CoverageProfileCD is not null



/************************************
Second case (ServiceChannelCD + COMP)
************************************/
insert into #t1
select		ca.ClaimAspectID,
			ca.LynxID,
			ca.ServiceChannelCD,
			ca.DispositionTypeCD,
			'COMP', --ca.CoverageProfileCD,
			cc.ComprehensiveDeductibleAmt,
			cc.SysLastUserID,
			cc.SysLastUpdatedDate

from		Tmp_utb_claim_aspect ca
inner join	Tmp_utb_claim_coverage cc
on			ca.LynxID = cc.LynxID

where		ca.ServiceChannelCD in('PS','ME')
and			NOT (cc.ComprehensiveDeductibleAmt is null or cc.ComprehensiveDeductibleAmt = 0)
--and			ca.CoverageProfileCD is not null


/***********************************
Third case (ServiceChannelCD + LIAB)
***********************************/
insert into #t1
select		ca.ClaimAspectID,
			ca.LynxID,
			ca.ServiceChannelCD,
			ca.DispositionTypeCD,
			'LIAB', --ca.CoverageProfileCD,
			cc.LiabilityDeductibleAmt,
			cc.SysLastUserID,
			cc.SysLastUpdatedDate

from		Tmp_utb_claim_aspect ca
inner join	Tmp_utb_claim_coverage cc
on			ca.LynxID = cc.LynxID

where		ca.ServiceChannelCD in('PS','ME')
and			NOT (cc.LiabilityDeductibleAmt is null or cc.LiabilityDeductibleAmt = 0)
--and			ca.CoverageProfileCD is not null


/************************************
Fourth case (ServiceChannelCD + RENT)
************************************/
insert into #t1
select		ca.ClaimAspectID,
			ca.LynxID,
			ca.ServiceChannelCD,
			ca.DispositionTypeCD,
			'RENT', --ca.CoverageProfileCD,
			cc.RentalDeductibleAmt,
			cc.SysLastUserID,
			cc.SysLastUpdatedDate

from		Tmp_utb_claim_aspect ca
inner join	Tmp_utb_claim_coverage cc
on			ca.LynxID = cc.LynxID

where		ca.ServiceChannelCD in('PS','ME')
and			NOT (cc.RentalDeductibleAmt is null or cc.RentalDeductibleAmt = 0)
--and			ca.CoverageProfileCD is not null



/**********************************
Fifth case (ServiceChannelCD + UIM)
**********************************/
insert into #t1
select		ca.ClaimAspectID,
			ca.LynxID,
			ca.ServiceChannelCD,
			ca.DispositionTypeCD,
			'UIM', --ca.CoverageProfileCD,
			cc.UnderInsuredDeductibleAmt,
			cc.SysLastUserID,
			cc.SysLastUpdatedDate

from		Tmp_utb_claim_aspect ca
inner join	Tmp_utb_claim_coverage cc
on			ca.LynxID = cc.LynxID

where		ca.ServiceChannelCD in('PS','ME')
and			NOT (cc.UnderInsuredDeductibleAmt is null or cc.UnderInsuredDeductibleAmt = 0) 
--and			ca.CoverageProfileCD is not null


/*********************************
Sixth case (ServiceChannelCD + UM)
*********************************/
insert into #t1
select		ca.ClaimAspectID,
			ca.LynxID,
			ca.ServiceChannelCD,
			ca.DispositionTypeCD,
			'UM', --ca.CoverageProfileCD,
			cc.UnInsuredDeductibleAmt,
			cc.SysLastUserID,
			cc.SysLastUpdatedDate

from		Tmp_utb_claim_aspect ca
inner join	Tmp_utb_claim_coverage cc
on			ca.LynxID = cc.LynxID

where		ca.ServiceChannelCD in('PS','ME')
and			NOT (cc.UnInsuredDeductibleAmt is null or cc.UnInsuredDeductibleAmt = 0)
--and			ca.CoverageProfileCD is not null


/*********************************
Create a table to hold the rows
*********************************/
--drop table #t2  --select * from #t2 order by 2,1 where CoverageTypeCD is null
Create table #t2	(
					ClaimAspectID udt_std_id_big,
					LynxID udt_std_id_big,
					ServiceChannelCD udt_std_cd,
					DispositionTypeCD udt_std_cd null,
					CoverageTypeCD udt_std_cd null,
					DeductibleAmt udt_cov_deductible,
					SysLastUserID udt_std_id,
					SysLastUpdatedDate udt_sys_last_updated_date
					)

/****************************/
--PROGRAM SHOP(PS) ROWS
/****************************/
insert into #t2
select		ca.ClaimAspectID,
			ca.LynxID,
			'PS', --ca.ServiceChannelCD,
			ca.DispositionTypeCD, 
			'COMP', --ca.CoverageProfileCD,
			me.DeductibleBodyAmt,
			me.SysLastUserID,
			me.SysLastUpdatedDate
from		tmp_utb_Claim_Aspect ca
inner join	tmp_utb_mobile_electronics_results me
on			ca.ClaimAspectID = me.ClaimAspectID
where 		ca.ServiceChannelCD in ('PS', 'ME')
and			not(me.DeductibleBodyAmt is null or me.DeductibleBodyAmt = 0)


/****************************/
--GLASS(GL) ROWS
/****************************/
insert into #t2
select		ca.ClaimAspectID,
			ca.LynxID,
			'GL', --ca.ServiceChannelCD,
			ca.DispositionTypeCD, 
			'COMP', --ca.CoverageProfileCD,
			me.DeductibleGlassAmt,
			me.SysLastUserID,
			me.SysLastUpdatedDate
from		tmp_utb_Claim_Aspect ca
inner join	tmp_utb_mobile_electronics_results me
on			ca.ClaimAspectID = me.ClaimAspectID
where 		ca.ServiceChannelCD in ('PS', 'ME')
and			not(me.DeductibleGlassAmt is null or me.DeductibleGlassAmt = 0)



/****************************/
-- MOBILE ELECTRONICS(ME) ROWS
/****************************/
insert into #t2
select		ca.ClaimAspectID,
			ca.LynxID,
			'ME', --ca.ServiceChannelCD,
			ca.DispositionTypeCD, 
			'COMP', --ca.CoverageProfileCD,
			me.DeductibleMEAmt,
			me.SysLastUserID,
			me.SysLastUpdatedDate

from		tmp_utb_Claim_Aspect ca
inner join	tmp_utb_mobile_electronics_results me
on			ca.ClaimAspectID = me.ClaimAspectID
where 		ca.ServiceChannelCD in ('PS', 'ME')
and			not(me.DeductibleMEAmt is null or me.DeductibleMEAmt = 0)


/***********************************************************************
Now first take the rows from #t2 temp table and insert them into 
utb_Claim_Aspect_Service_Channel_Coverage table
***********************************************************************/
BEGIN TRAN


insert into utb_Claim_Aspect_Service_Channel_Coverage	
				(
				ClaimAspectServiceChannelID,
				ClaimCoverageID,
				DeductibleAppliedAmt,
                LimitAppliedAmt,
				SysLastUserID,
				SysLastUpdatedDate
				)

select				casc.ClaimAspectServiceChannelID,
					cc.ClaimCoverageID,
					t2.DeductibleAmt,
                    0,
					t2.SysLastUserID,
					t2.SysLastUpdatedDate
from				#t2 t2

inner join			utb_Claim_Coverage cc
on					t2.LynxID = cc.LynxID
and					t2.CoverageTypeCD = cc.CoverageTypeCD

inner join			utb_Claim_Aspect_Service_Channel casc
on					t2.claimaspectID = casc.ClaimAspectID
and					t2.ServiceChannelCD = casc.ServiceChannelCD



/***********************************************************************
Insert the rows from the temp table, #t1, into the table.
Note:  We are going to insert the LynxIDs+ClaimAspectIDs that only exist
in #t1 tables and are not part of or were not gathered in #t2
***********************************************************************/

insert into utb_Claim_Aspect_Service_Channel_Coverage	
				(
				ClaimAspectServiceChannelID,
				ClaimCoverageID,
				DeductibleAppliedAmt,
                LimitAppliedAmt,
				SysLastUserID,
				SysLastUpdatedDate
				)

select		casc.ClaimAspectServiceChannelID,
			cc.ClaimCoverageID,
			t1.DeductibleAmt,
            0,
			t1.SysLastUserID,
			t1.SysLastUpdatedDate

from				#t1 t1 
left Outer join	#t2 t2
on					t1.LynxID = t2.LynxID
and					t1.ClaimAspectID = t2.ClaimASpectID
and					t1.CoverageTypeCD = t2.CoverageTypeCD
--and					t1.ServiceChannelCd = t2.ServiceChannelCD

inner join			utb_Claim_Coverage cc
on					t1.LynxID = cc.LynxID
and					t1.CoverageTypeCD = cc.CoverageTypeCD

inner join			utb_Claim_Aspect_Service_Channel casc
on					t1.claimaspectID = casc.ClaimAspectID
and					t1.ServiceChannelCD = casc.ServiceChannelCD

where				t2.LynxID is null


UPDATE dbo.utb_Claim_Aspect_Service_Channel_Coverage
   SET PartialCoverageFlag = me.CoverageExceptionMEFlag
  FROM dbo.utb_Claim_Aspect_Service_Channel_Coverage cascc
 INNER JOIN dbo.utb_Claim_Aspect_Service_Channel casc
    ON cascc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
   AND casc.ServiceChannelCD = 'ME'
 INNER JOIN dbo.tmp_utb_mobile_electronics_results me
    ON me.ClaimAspectID = casc.ClaimAspectID


if (select count(*) from tempdb..sysobjects where name like '#t1%') =1
	begin
		drop table #t1
	end

if (select count(*) from tempdb..sysobjects where name like '#t2%') =1
	begin
		drop table #t2
	end
	
/****************************************************************************
!IMPORTANT!  !IMPORTANT!  !IMPORTANT!  !IMPORTANT!  !IMPORTANT!  !IMPORTANT!
*****************************************************************************

Please either COMMIT or ROLLBACK the transaction.  This must be done manually.
*****************************************************************************/


--COMMIT

--ROLLBACK
