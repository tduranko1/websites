-- Prime the new table
DECLARE @dtNow DATETIME
SET @dtNow = CURRENT_TIMESTAMP

IF NOT EXISTS (SELECT InsuranceCompanyID FROM utb_insurance_HQ_Ext)
BEGIN
	INSERT INTO utb_insurance_HQ_Ext VALUES (158, 'ID 158-DRP', 'DRPElectric@hyperquest.com', @dtNow)
	INSERT INTO utb_insurance_HQ_Ext VALUES (184, 'ID 184-DRP', 'DRPTexasFB@hyperquest.com', @dtNow)
	INSERT INTO utb_insurance_HQ_Ext VALUES (192, 'ID 192-DRP', 'DRPVirginiaFB@hyperquest.com', @dtNow)
	INSERT INTO utb_insurance_HQ_Ext VALUES (277, 'ID 277-DRP', 'DRPRepublic@hyperquest.com', @dtNow)
	INSERT INTO utb_insurance_HQ_Ext VALUES (259, 'ID 259-DRP', 'DRPUtica@hyperquest.com', @dtNow)
	INSERT INTO utb_insurance_HQ_Ext VALUES (387, 'ID 387-DRP', 'DRPWestfield@hyperquest.com', @dtNow)
	INSERT INTO utb_insurance_HQ_Ext VALUES (464, 'ID 464-DRP', 'DRPCountryway@hyperquest.com', @dtNow)
	INSERT INTO utb_insurance_HQ_Ext VALUES (493, 'ID 493-DRP', 'DRPAceAmerican@hyperquest.com', @dtNow)
	INSERT INTO utb_insurance_HQ_Ext VALUES (515, 'ID 515-DRP', 'DRPAvisBudget@hyperquest.com', @dtNow)

	SELECT 'HQ Ext table initialized - Successfully'
END
ELSE
BEGIN
	SELECT 'HQ Ext table already initialized'
END
