/******************************************************************************
* QBE Bundling profiles
******************************************************************************/

DECLARE @NOW as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int

SET @NOW = CURRENT_TIMESTAMP
SET @InsuranceCompanyID = 304

BEGIN TRANSACTION

DELETE FROM utb_client_bundling WHERE InsuranceCompanyID = @InsuranceCompanyID

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing Audit Complete|Messages/msgDAQBEClosingAuditComplete.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing Audit Complete', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add the documents to the bundling profile
-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)


INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'qbeapd@lynxservices.com',
   0,
   @now


INSERT INTO utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Claim Acknowledgment|Messages/msgQBEClmAck.xsl', 'DA', 0, current_timestamp

SET @MessageTemplateID = SCOPE_IDENTITY()

-- insert bundling

INSERT INTO dbo.utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 34, @MessageTemplateID, 1, 'Acknowledgement of Claim', 0, CURRENT_TIMESTAMP

SET @BundlingID = SCOPE_IDENTITY()

-- insert client bundling
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
) 
SELECT @BundlingID, @InsuranceCompanyID, 'DA', 0, CURRENT_TIMESTAMP

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing Supplement|Messages/msgDAQBEClosingSupplement.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing Supplement', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add the documents to the bundling profile
-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'qbeapd@lynxservices.com',
   0,
   @now




----- Closing Total Loss
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing Total Loss|Messages/msgDAQBEClosingTL.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing Total Loss', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add the documents to the bundling profile
-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'qbeapd@lynxservices.com',
   0,
   @now


-- Shop Fax template

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Shop Audit Notice|Messages/msgDAShopCoverPage_QBE.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 46, @MessageTemplateID, 1, 'SHOP: Desk Audit Fax Cover Page', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add the documents to the bundling profile
-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   0,
   @now
   


-- Add the document exceptions so these will not be merged
-- Estimate
insert into dbo.utb_client_document_exception 
select @InsuranceCompanyID, 3, 'PDF', 0, @now

-- Supplement
insert into dbo.utb_client_document_exception 
select @InsuranceCompanyID, 10, 'PDF', 0, @now

SELECT * from utb_message_template where MessageTemplateID = @MessageTemplateID
SELECT * FROM utb_bundling WHERE BundlingID = @BundlingID
SELECT * from utb_client_bundling WherE InsuranceCompanyID = @InsuranceCompanyID

-- commit
-- rollback