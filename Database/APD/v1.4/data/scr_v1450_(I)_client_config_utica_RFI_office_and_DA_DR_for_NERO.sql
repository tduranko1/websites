-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configuration for Utica New York Metro Regional Office
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Utica National Insurance Group'


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'

-- Add DA and DR to NERO Office
SELECT @OfficeID = OfficeID
FROM utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeId = 'NERO'
  AND EnabledFlag = 1
  
IF @OfficeID IS NULL
BEGIN 
	PRINT 'NERO office not found.'
	ROLLBACK TRANSACTION
	RETURN
END
ELSE
BEGIN
	-- What types of work shall be assigned to LYNX (Shop Program, Desk Audits, etc) from this office?
	--		LYNXSelect DRP + Desk Audits and Damage Reinspection  
	-- DRP is already setup. Just add the DA and DR
	
	INSERT INTO dbo.utb_office_assignment_type 
	SELECT  @OfficeID, 
		  AssignmentTypeID, 
		  0,
		  @ModifiedDateTime
	FROM  dbo.utb_assignment_type
	WHERE Name in ('Demand Estimate Audit', 'Adverse Subro Desk Review')
END


-- SETUP Columbus Regional Office
INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '2600 Corporate Exchange Drive',    -- Address1
                '',                     -- Address2
                'Columbus',             -- AddressCity
                'OH',                   -- AddressState
                '43231',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'RFI',                  -- ClientOfficeId
                1,                      -- EnabledFlag
                '614',                  -- FaxAreaCode
                '823',                  -- FaxExchangeNumber
                '5399',                 -- FaxUnitNumber
                'P.O. Box 29906',       -- MailingAddress1
                NULL,                   -- MailingAddress2
                'Columbus',             -- MailingAddressCity
                'OH',                   -- MailingAddressState
                '43231',                -- MailingAddressZip
                'Columbus Regional Office', -- Name
                '800',                 -- PhoneAreaCode
                '955',                 -- PhoneExchangeNumber
                '1914',                -- PhoneUnitNumber
                'RFIClaimsMailbox@UticaNational.com', -- ReturnDocEmailAddress
                NULL,                  -- ReturnDocFaxAreaCode
                NULL,                  -- ReturnDocFaxExchangeNumber
                NULL,                  -- ReturnDocFaxUnitNumber
                0,                     -- SysLastUserID
                @ModifiedDateTime      -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

-- What types of work shall be assigned to LYNX (Shop Program, Desk Audits, etc) from this office?
-- Desk Audits and Damage Reinspection  

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Demand Estimate Audit', 'IA Estimate Review')
    
-- Which states will LYNX be assigned work in from this office? All
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code



-- Review affected tables
SELECT * FROM dbo.utb_office WHERE OfficeID = @OfficeID
SELECT * FROM dbo.utb_office_assignment_type WHERE OfficeID = @OfficeID
SELECT * FROM dbo.utb_office_contract_state WHERE OfficeID = @OfficeID


-- commit
-- rollback
