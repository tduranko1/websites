-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configurations for Utica
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @PaymentCompanyID       smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 279
SET @PaymentCompanyID = 814


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'


PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

--Insert Insurance Company information.

IF NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
    INSERT INTO dbo.utb_insurance
        (
         InsuranceCompanyID, 
         DeskAuditPreferredCommunicationMethodID,
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip, 
         AssignmentAtSelectionFlag,
         BillingModelCD, 
         BusinessTypeCD,
         CarrierLynxContactPhone,
         CFLogoDisplayFlag,
         ClaimPointCarrierRepSelFlag,
         ClaimPointDocumentUploadFlag, 
         ClientAccessFlag,
         DemoFlag,
         DeskAuditCompanyCD,
         DeskReviewLicenseReqFlag, 
         EnabledFlag,
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         FedTaxId,
         IngresAccountingId,
         InvoicingModelPaymentCD, 
         InvoiceMethodCD,
         LicenseDeterminationCD, 
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocDestinationCD,
         ReturnDocPackageTypeCD,
         ReturnDocRoutingCD, 
         TotalLossValuationWarningPercentage, 
         TotalLossWarningPercentage, 
         WarrantyPeriodRefinishMinCD,
         WarrantyPeriodWorkmanshipMinCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,      -- InsuranceCompanyID
                14,                       -- DeskAuditPreferredCommunicationMethodID
                '9516 Airline Hwy',       -- Address1
                '',                       -- Address2
                'Baton Rouge',            -- AddressCity
                'LA',                     -- AddressState
                '70815',                  -- AddressZip
                0,                        -- AssignmentAtSelectionFlag
                'E',                      -- BillingModelCD
                'C',                      -- BusinessTypeCD
                NULL,                     -- CarrierLynxContactPhone
                1,                        -- CFLogoDisplayFlag
                0,                        -- ClaimPointCarrierRepSelFlag
                1,                        -- ClaimPointDocumentUploadFlag
                1,                        -- ClientAccessFlag
                0,                        -- DemoFlag
                'LAFB',                   -- DeskAuditCompanyCD
                1,                        -- DeskReviewLicenseReqFlag
                1,                        -- EnabledFlag
                NULL,                     -- FaxAreaCode
                NULL,                     -- FaxExchangeNumber
                NULL,                     -- FaxUnitNumber
                NULL,                     -- FedTaxId
                @PaymentCompanyID,        -- IngresAccountingId
                'C',                      -- InvoicingModelPaymentCD (Bulk / per claim)
                'P',                      -- InvoiceMethodCD
                'LOSS',                   -- LicenseDeterminationCD
                'Louisiana Farm Bureau',  -- Name
                '225',                    -- PhoneAreaCode
                '922',                    -- PhoneExchangeNumber
                '6200',                   -- PhoneUnitNumber
                'REP',                    -- ReturnDocDestinationCD
                'PDF',                    -- ReturnDocPackageTypeCD
                'EML',                    -- ReturnDocRoutingCD
                0.75,                     -- TotalLossValuationWarningPercentage
                0.75,                     -- TotalLossWarningPercentage
                '99',                     -- WarrantyPeriodRefinishMinCD
                '99',                     -- WarrantyPeriodWorkmanshipMinCD
                0,                        -- SysLastUserID
                @ModifiedDateTime         -- SysLastUpdatedDate
        )
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already exists in utb_insurance', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END


--Insert Client level configuration

------------------------------------------------------

INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Demand Estimate Audit')

------------------------------------------------------

INSERT INTO dbo.utb_client_service_channel
SELECT @InsuranceCompanyID,
       ServiceChannelDefaultCD,
       0,       -- ClientAuthorizesPaymentFlag
       'C',     -- InvoicingModelBillingCD
       0,
       @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Demand Estimate Audit')


------------------------------------------------------

INSERT INTO dbo.utb_client_contract_state
SELECT  @InsuranceCompanyID,
        StateCode,
        0,                   -- UseCEIShopFlag
        0,                   -- SysLastUserID
        @ModifiedDateTime
  FROM  dbo.utb_state_code
  WHERE EnabledFlag = 1


------------------------------------------------------

INSERT INTO dbo.utb_client_coverage_type (InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        AdditionalCoverageFlag,
        ClientCode,
        CoverageProfileCD,
        DisplayOrder,
        1,
        Name,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_client_coverage_type
  WHERE InsuranceCompanyID = 145    -- Clone Zurich's configuration


------------------------------------------------------    
    
INSERT INTO dbo.utb_client_claim_aspect_type 
SELECT  @InsuranceCompanyID,
        ClaimAspectTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_claim_aspect_type
  WHERE Name IN ('Claim', 'Assignment', 'Vehicle', 'Fax Assignment')

------------------------------------------------------        
    
INSERT INTO dbo.utb_client_payment_type VALUES (@InsuranceCompanyID, 'I', 0, @ModifiedDateTime)

------------------------------------------------------

INSERT INTO dbo.utb_client_report (InsuranceCompanyID, OfficeID, ReportID, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        NULL,
        ReportID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_report 
  WHERE ReportID IN (1, 2, 3, 4, 5, 6, 7, 12, 13)

------------------------------------------------------

-- Create Office(s)

-- Louisiana Farm Bureau 

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '9516 Airline Hwy',     -- Address1
                '',                     -- Address2
                'Baton Rouge',          -- AddressCity
                'LA',                   -- AddressState
                '70895',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Louisiana Farm Bureau',-- ClientOfficeId
                1,                      -- EnabledFlag
                NULL,                   -- FaxAreaCode
                NULL,                   -- FaxExchangeNumber
                NULL,                   -- FaxUnitNumber
                'P.O. Box 95005',       -- MailingAddress1
                NULL,                   -- MailingAddress2
                'Baton Rouge',          -- MailingAddressCity
                'LA',                   -- MailingAddressState
                '70895',                -- MailingAddressZip
                'Louisiana Farm Bureau',-- Name
                '225',                  -- PhoneAreaCode
                '922',                  -- PhoneExchangeNumber
                '6200',                 -- PhoneUnitNumber
                NULL,                   -- ReturnDocEmailAddress
                NULL,                   -- ReturnDocFaxAreaCode
                NULL,                   -- ReturnDocFaxExchangeNumber
                NULL,                   -- ReturnDocFaxUnitNumber
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    WHERE EnabledFlag = 1

-- add the business states
insert into utb_client_business_state 
select @InsuranceCompanyID, 'LA', 0, @ModifiedDateTime

-- Review affected tables
SELECT * FROM dbo.utb_insurance
SELECT * FROM dbo.utb_client_assignment_type
SELECT * FROM dbo.utb_client_claim_aspect_type
SELECT * FROM dbo.utb_client_contract_state
SELECT * FROM dbo.utb_client_payment_type
SELECT * FROM dbo.utb_client_report
SELECT * FROM dbo.utb_office
SELECT * FROM dbo.utb_office_assignment_type
SELECT * FROM dbo.utb_office_contract_state
select * from dbo.utb_client_business_state

-- This script requires a fresh on the workflow and spawn reference tables from the access file

--commit 
--rollback
