--SELECT * FROM utb_office WHERE insurancecompanyid = 387

DECLARE @InsuranceCompanyID        smallint
DECLARE @OfficeID                  smallint
DECLARE @OfficeName                VARCHAR(100)
DECLARE @OfficeDesignation         VARCHAR(50)
DECLARE @OfficeStreetAddress1      VARCHAR(100)
DECLARE @OfficeStreetAddress2      VARCHAR(100)
DECLARE @OfficeCity                VARCHAR(100)
DECLARE @OfficeState               VARCHAR(2)
DECLARE @OfficeZip                 VARCHAR(10)
DECLARE @OfficePhone               VARCHAR(12)
DECLARE @OfficeFax                 VARCHAR(12)
DECLARE @ModifiedDateTime          DATETIME

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID             =  387      -- Company ID 


      SET @OfficeName                     = 'Livonia,MI'                           -- Office Name
      SET @OfficeDesignation              = 'Livonia-MI'                           -- Office Designation
      SET @OfficeStreetAddress1           = '2851 CHARLEVOIX DRIVE'                -- Street Address1
      SET @OfficeStreetAddress2           = 'SUITE 110'                            -- Street Address2
      SET @OfficeCity                     = 'Livonia'                              -- City
      SET @OfficeState                    = 'MI'                                   -- State
      SET @OfficeZip                      = '48152'                                -- Zip Code
      SET @OfficePhone                    = '800-243-0237'                         -- Phone (nnn-nnn-nnnn)
      SET @OfficeFax                      = ''                                     -- Fax (nnn-nnn-nnnn)

      

BEGIN
      INSERT INTO dbo.utb_office
                  (
                  InsuranceCompanyID, 
                  Address1,
                  Address2, 
                  AddressCity, 
                  AddressState, 
                  AddressZip,
                  CCEmailAddress,
                  ClaimNumberFormatJS,
                  ClaimNumberValidJS,
                  ClaimNumberMsgText,
                  ClientOfficeId,
                  EnabledFlag, 
                  FaxAreaCode,
                  FaxExchangeNumber,
                  FaxUnitNumber,
                  MailingAddress1,
                  MailingAddress2, 
                  MailingAddressCity, 
                  MailingAddressState, 
                  MailingAddressZip,
                  Name, 
                  PhoneAreaCode, 
                  PhoneExchangeNumber, 
                  PhoneUnitNumber,
                  ReturnDocEmailAddress,
                  ReturnDocFaxAreaCode,
                  ReturnDocFaxExchangeNumber,
                  ReturnDocFaxUnitNumber, 
                   SysLastUserID,
                  SysLastUpdatedDate 
                  )
                  VALUES( @InsuranceCompanyID,           -- InsuranceCompanyID
                              @OfficeStreetAddress1 ,    -- Address1 *
                              @OfficeStreetAddress2,     -- Address2 *
                              @OfficeCity,               -- AddressCity *
                              @OfficeState,              -- AddressState *
                              @OfficeZip,                -- AddressZip *
                              NULL,                      -- CCEmailAddress
                              NULL,                      -- ClaimNumberFormatJS
                              NULL,                      -- ClaimNumberValidJS
                              NULL,                      -- ClaimNumberMsgText
                              @OfficeDesignation,            -- ClientOfficeId * 
                              1,                         -- EnabledFlag
                              SUBSTRING(@OfficeFax,1,3), -- FaxAreaCode *
                              SUBSTRING(@OfficeFax,5,3), -- FaxExchangeNumber *
                              SUBSTRING(@OfficeFax,9,4), -- FaxUnitNumber *
                              @OfficeStreetAddress1 ,    -- MailingAddress1 *
                              @OfficeStreetAddress2,     -- MailingAddress2 *
                              @OfficeCity,                     -- MailingAddressCity *
                              @OfficeState,              -- MailingAddressState *
                              @OfficeZip,                -- MailingAddressZip *
                              @OfficeName ,                    -- Name *
                              SUBSTRING(@OfficePhone,1,3),    -- PhoneAreaCode *
                              SUBSTRING(@OfficePhone,5,3),  -- PhoneExchangeNumber *
                              SUBSTRING(@OfficePhone,9,4),  -- PhoneUnitNumber *
                              NULL,                   -- ReturnDocEmailAddress
                              NULL,                   -- ReturnDocFaxAreaCode
                              NULL,                   -- ReturnDocFaxExchangeNumber
                              NULL,                   -- ReturnDocFaxUnitNumber
                              0,                      -- SysLastUserID
                              @ModifiedDateTime       -- SysLastUpdatedDate
                  )


      SELECT @OfficeID = SCOPE_IDENTITY()


      -- Office level configurations - this must match the Client assignment types

      INSERT INTO dbo.utb_office_assignment_type 
        SELECT  @OfficeID, 
                    AssignmentTypeID, 
                    0,
                    @ModifiedDateTime
            FROM  dbo.utb_assignment_type
            WHERE Name in ( 'Program Shop Assignment')


      INSERT INTO dbo.utb_office_contract_state
        SELECT  @OfficeID,
                    StateCode,
                    0, -- UseCEIShopsFlag
                    0, -- SysLastUserID
                    @ModifiedDateTime
            FROM  dbo.utb_state_code
            WHERE EnabledFlag = 1
END


