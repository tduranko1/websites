/******************************************************************************
* QBE RRP - Photograph exception from merging into Claim Document
******************************************************************************/

DECLARE @NOW as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int

SET @NOW = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
  FROM utb_insurance
 WHERE Name = 'QBE Insurance'
 
IF @InsuranceCompanyID IS NULL
BEGIN
    RAISERROR('Unable to determine QBE InsuranceCompanyID', 16, 1)
    RETURN
END


-- Start a transaction
BEGIN TRANSACTION

INSERT INTO dbo.utb_client_document_exception (
    InsuranceCompanyID
    , DocumentTypeID
    , OutputTypeCD
    , SysLastUserID
    , SysLastUpdatedDate
) VALUES (
    @InsuranceCompanyID
    , 8
    , 'JPG'
    , 0
    , @now
)
-- commit
-- rollback
