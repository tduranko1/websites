
-- QBE DRP Setup - 10/22/2010

DECLARE @InsuranceCompanyID AS int
DECLARE @AssignmentTypeID as int
DECLARE @Now as datetime

SELECT @InsuranceCompanyID = InsuranceCompanyID
    FROM dbo.utb_insurance
    WHERE Name = 'QBE Insurance'
    
SELECT @AssignmentTypeID = AssignmentTypeID
    FROM dbo.utb_assignment_type
    WHERE Name = 'Program Shop Assignment'

SET @NOW = CURRENT_TIMESTAMP


BEGIN TRANSACTION

--Add program shop service (DRP) for QBE
INSERT INTO utb_client_service_channel (
   InsuranceCompanyID,
   ServiceChannelCD,
   ClientAuthorizesPaymentFlag,
   InvoicingModelBillingCD,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @InsuranceCompanyID,
   'PS',
   0,
   'B',		-- need to change? WJC
   0,
   @Now
)


-- add DRP assignment type for QBE
INSERT INTO dbo.utb_client_assignment_type(
   InsuranceCompanyID,
   AssignmentTypeID,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @InsuranceCompanyID,
   @AssignmentTypeID,
   0,
   @Now
)

-- add DRP assignment type for QBE offices
INSERT INTO dbo.utb_office_assignment_type
SELECT OfficeID, @AssignmentTypeID, 0, @Now
FROM utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID


-- update early bill flag to yes (1)
UPDATE utb_insurance
SET EarlyBillFlag = 1
WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT * 
FROM utb_client_service_channel
WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT * 
FROM utb_client_assignment_type
WHERE InsuranceCompanyID = @InsuranceCompanyID


SELECT * 
FROM utb_office_assignment_type
WHERE InsuranceCompanyID = @InsuranceCompanyID


COMMIT
--ROLLBACK