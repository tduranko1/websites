/******************************************************************************
* Document Bundling options for LAFB PS
******************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime
DECLARE @BundlingID as int

SET @now = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Louisiana Farm Bureau'

IF @InsuranceCompanyID IS NOT NULL
BEGIN
   BEGIN TRANSACTION
   
   ----------------------------------------
   -- Create Acknowledgment of Claim bundle
   ----------------------------------------
   -- Do this manually
   /*
      -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      1,                -- BundlingID,
      279,              -- InsuranceCompanyID,
      'PS',             -- ServiceChannelCD,
      0,                -- SysLastUserID,
      current_timestamp -- SysLastUpdatedDate,
   )*/
   
   ---------------------------------
   -- Create Initial Estimate bundle
   ---------------------------------
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      35, -- DocumentTypeID,
      2, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Initial Estimate', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add estimate
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      3,             -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      'O',           -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      1,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
   
   -----------------------------------
   -- Create Closing Supplement bundle
   -----------------------------------
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      5, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Closing Supplement', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add estimate
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      3,             -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      'A',           -- EstimateTypeCD,
      1,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      2,             -- SelectionOrder,
      NULL,          -- ShopState,
      1,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add invoice
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      13,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
      
   ---------------------------------
   -- Create Closing Repair Complete
   ---------------------------------
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      4, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Closing Repair Complete', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add direction to pay
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      2,             -- DocumentTypeID,
      'I',           -- DirectionalCD,
      1,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      2,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add estimate
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      3,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      'A',           -- EstimateTypeCD,
      1,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      3,             -- SelectionOrder,
      NULL,          -- ShopState,
      1,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add rental invoice
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      9,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      5,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add invoice
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      13,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,           -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add NY COAR
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      42,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      1,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,           -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      6,             -- SelectionOrder,
      'NY',          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add CT Consumer Choice Form
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      43,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      1,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,           -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      6,             -- SelectionOrder,
      'CT',          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
         
   ----------------------------
   -- Create Closing Total Loss
   ----------------------------
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      9, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Closing - Total Loss', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )

   
   SET @BundlingID = SCOPE_IDENTITY()

   -- Add estimate
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      3,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      'A',           -- EstimateTypeCD,
      1,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      3,             -- SelectionOrder,
      NULL,          -- ShopState,
      1,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add photo
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      8,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      1,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,           -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      3,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add invoice
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      13,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,           -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )


   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
   
   ------------------------
   -- Create Rental Invoice
   ------------------------
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      44, -- DocumentTypeID,
      20, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Rental Invoice', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )

   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add rental invoice
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      9,            -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )


   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'PS',                -- ServiceChannelCD,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )

   select dt.Name, b.Name, bdt.* 
   from utb_bundling_document_type bdt
   left join utb_document_type dt on bdt.DocumentTypeID = dt.DocumentTypeID
   left join utb_bundling b on bdt.BundlingID = b.BundlingID
   left join utb_client_bundling cb on b.BundlingID = cb.BundlingID
   where cb.InsuranceCompanyID = @InsuranceCompanyID
     and cb.ServiceChannelCD = 'PS'   
END


-- COMMIT
-- ROLLBACK

