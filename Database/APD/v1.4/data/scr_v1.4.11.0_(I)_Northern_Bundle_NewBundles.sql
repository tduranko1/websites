DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 575
SET @FromInscCompID = 194

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_bundling b INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE [Description] LIKE '%NOPS%') 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Assignment Cancellation')
		, MessageTemplateID 
		,1
		,'Assignment Cancellation'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSAssignmentCancel.xsl%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Closing Documents')
		, MessageTemplateID 
		,1
		,'No Inspection (Stale) Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSStaleAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Closing Documents')
		, MessageTemplateID 
		,1
		,'Cash Out Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSCashOutAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Status Update')
		, MessageTemplateID 
		,1
		,'Status Update - Claim Rep'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSStatusUpdAction.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Status Update')
		, MessageTemplateID 
		,1
		,'Status Update - MDS'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSStatusUpdActionMDS.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Total Loss Bundle')
		, MessageTemplateID 
		,1
		,'Total Loss Alert - (Adjuster)'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSTLAlert.xsl%'

----

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Closing Documents')
		, MessageTemplateID 
		,1
		,'Closing Repair Complete'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPS_ClosingRepairComplete.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Acknowledgement of Claim')
		, MessageTemplateID 
		,1
		,'Acknowledgement of Claim'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSClmAck.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Closing Documents')
		, MessageTemplateID 
		,1
		,'Closing Supplement'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSClosingSupplement.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Initial Estimate Bundle')
		, MessageTemplateID 
		,1
		,'Initial Estimate and Photos (Adjuster)'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSInitialEstimate.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Rental Invoice Bundle')
		, MessageTemplateID 
		,1
		,'Rental Invoice'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSRentalInvoice.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE Name = 'Closing Documents')
		, MessageTemplateID 
		,1
		,'Closing - Total Loss'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgNOPSTLClosing.xsl%'


	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

--SELECT * FROM utb_message_template  where description like '%CI%'
--SELECT * FROM utb_bundling where MessageTemplateID IN (433,434,435,436,437,438,439,440,441,442,443,444)
