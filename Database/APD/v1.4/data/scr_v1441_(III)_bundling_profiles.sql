/******************************************************************************
* Bundling Profile initial setup
******************************************************************************/

DECLARE @BundlingID as int
DECLARE @now as datetime
DECLARE @DocumentTypeID as int
DECLARE @MessageTemplateID as int

SET @now = CURRENT_TIMESTAMP

BEGIN TRANSACTION

/*****************************************************************/
/******************* Create bundling profiles ********************/
/*****************************************************************/

/*****************************************************************/
/******************* Acknowledgement of Claim (PS) ***************/
/*****************************************************************/

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Acknowledgement of Claim'


IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Acknowledgement of Claim" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'PS'
  AND Description like 'Claim Acknowledgment|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Claim Acknowledgement" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END


INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Acknowledgement of Claim', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Acknowledgement of Claim"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- NONE. This will be driven by the custom message


-- Add this bundling tasks to PS clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich




/*********************************************************/
/******************* Initial Estimate (PS) ***************/
/*********************************************************/

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Initial Estimate Bundle'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Initial Estimate Bundle" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'PS'
  AND Description like 'Initial Estimate|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Initial Estimate" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END


INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Initial Estimate', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Initial Estimate"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Photos
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    8,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    1,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to PS clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209, 282) -- Zurich & MOFB


/*********************************************************/
/******************* Status Update ***********************/
/*********************************************************/

/*
-- This is just a message. so no need to have a bundle. System will
-- default to Status anyway.

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Status Update'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Status Update" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END


SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'PS'
  AND Description like 'Status Update%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Status Update" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,        -- DocumentTypeID,
    @MessageTemplateID,     -- MessageTemplateID
    1,                      -- EnabledFlag,
    'Status Update',        -- Name,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Status Update"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add documents
-- This will be message driven

-- Add this bundling tasks to PS clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich*/



/**********************************************************/
/******************* Closing Documents (PS) ***************/
/**********************************************************/

--------------------------
-- Closing Repair Complete
--------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'PS'
  AND Description like 'Closing Repair Complete|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Status Update" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Repair Complete', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add DTP
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    2,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    1,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Photos
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    8,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    1,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    0,              -- MandatoryFlag,
    4,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to PS clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209, 184) -- Zurich; TXFB we will use another profile

--------------------------
-- Closing Repair Complete
-- FOR TXFB
--------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'PS'
  AND Description like 'Closing Repair Complete|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Status Update" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Repair Complete', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add DTP
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    2,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    1,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to TXFB
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID in (184) -- TXFB only


---------------------
-- Closing Supplement
---------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'PS'
  AND Description like 'Closing Supplement|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Supplement" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Supplement', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to PS clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich

-------------
-- Total Loss
-------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'PS'
  AND Description like 'Total Loss Closing|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Status Update" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing - Total Loss', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Photos
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    8,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    1,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    0,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add BCIF
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    11,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    4,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to PS clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich


--------------------------------------
-- Desk Audit - Closing Audit Complete
-- Clients with no bulk billing
--------------------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'DA'
  AND Description like 'Closing Audit Complete|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Audit Complete" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing - Audit Complete', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    33,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to DA clients with no bulk billing
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'DA', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'DA'
  AND csc.InvoicingModelBillingCD <> 'B'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich

--------------------------------------
-- Desk Audit - Closing Audit Complete
-- Clients with bulk billing
--------------------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'DA'
  AND Description like 'Closing Audit Complete|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Audit Complete" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing - Audit Complete', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    0,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    33,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to DA clients with bulk billing
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'DA', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'DA'
  AND csc.InvoicingModelBillingCD = 'B'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich

----------------------------------
-- Desk Audit - Closing Supplement
-- No bulk billing
----------------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'DA'
  AND Description like 'Closing Supplement|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Audit Complete" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Supplement', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    33,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to PS clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'DA', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'DA'
  AND csc.InvoicingModelBillingCD <> 'B'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich


----------------------------------
-- Desk Audit - Closing Supplement
-- bulk billing
----------------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'DA'
  AND Description like 'Closing Supplement|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Audit Complete" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Supplement', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    0,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    33,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to DA clients with bulk billing
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'DA', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'DA'
  AND csc.InvoicingModelBillingCD = 'B'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich

-----------------------------------------
-- Adverse Subro - Closing Audit Complete
-- No Bulk Billing
-----------------------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'DR'
  AND Description like 'Closing Audit Complete|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Audit Complete" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Audit Complete', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Adverse Subro Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    32,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to DR clients with NO bulk billing
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'DR', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'DR'
  AND csc.InvoicingModelBillingCD <> 'B'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich

-----------------------------------------
-- Adverse Subro - Closing Audit Complete
-- Bulk Billing
-----------------------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'DR'
  AND Description like 'Closing Audit Complete|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Audit Complete" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Audit Complete', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    0,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Adverse Subro Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    32,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to DR clients with bulk billing
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'DR', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'DR'
  AND csc.InvoicingModelBillingCD = 'B'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich

----------------------------------
-- Adverse Subro - Closing Supplement
-- No Bulk Billing
----------------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'DR'
  AND Description like 'Closing Supplement|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Audit Complete" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Supplement', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    32,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to PS clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'DR', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'DR'
  AND csc.InvoicingModelBillingCD <> 'B'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich

----------------------------------
-- Adverse Subro - Closing Supplement
-- Bulk Billing
----------------------------------
SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'DR'
  AND Description like 'Closing Supplement|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Audit Complete" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing Supplement', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    0,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    32,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to DR clients with bulk billing
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'DR', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'DR'
  AND csc.InvoicingModelBillingCD = 'B'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich


-- now select the affected tables
select * from dbo.utb_bundling
select * from dbo.utb_bundling_document_type
select * from dbo.utb_client_bundling


-- commit
-- rollback
