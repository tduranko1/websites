/******************************************************************************
* QBE RRP Bundling updates
******************************************************************************/

DECLARE @NOW as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int

SET @NOW = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
  FROM utb_insurance
 WHERE Name = 'QBE Insurance'
 
IF @InsuranceCompanyID IS NULL
BEGIN
    RAISERROR('Unable to determine QBE InsuranceCompanyID', 16, 1)
    RETURN
END


-- Start a transaction
BEGIN TRANSACTION


-- RRP-Acknowledgement of Claim
UPDATE utb_bundling
SET DocumentTypeID = 34 -- Acknowledgement of Claim
WHERE Name = 'RRP - Acknowledgement of Claim'


-- commit
-- rollback