DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 387

------------------------------
-- Add Client Bundling
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_client_bundling
	WHERE 
		InsuranceCompanyID = @ToInscCompID
		AND ClientBundlingID IN 
		(
			SELECT 
				b.BundlingID 	
			FROM 
				utb_bundling b 
				INNER JOIN utb_message_template m 
				ON m.MessageTemplateID = b.MessageTemplateID 
			WHERE 
				m.[Description] LIKE '%msgWFPSAssignmentCancel.xsl%'
		)
)
BEGIN
	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSClmAck.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSClosingSupplement.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSTLClosing.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSRentalInvoice.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPS_ClosingRepairComplete.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSAssignmentCancel.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSStaleAlert.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSCashOutAlert.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSStatusUpdAction.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgWFPSTLAlert.xsl%'		

	SELECT 'Client Bundling added...'
END
ELSE
BEGIN
	SELECT 'Client Bundling already added...'
END

SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID = @ToInscCompID AND SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
