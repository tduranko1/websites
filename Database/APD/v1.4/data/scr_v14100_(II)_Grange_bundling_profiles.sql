/******************************************************************************
* Document Bundling options for LAFB PS
******************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime
DECLARE @BundlingID as int
DECLARE @MessageTemplateID as int
DECLARE @DocumentTypeID as int
DECLARE @DocumentDisplayOrder as int

SET @now = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Grange Insurance Group'

IF @InsuranceCompanyID IS NOT NULL
BEGIN
   BEGIN TRANSACTION
   
   -- DELETE PREVIOUS CLIENT BUNDLING
   DELETE FROM utb_client_bundling
   WHERE InsuranceCompanyID = @InsuranceCompanyID
     AND ServiceChannelCD = 'DA'
     
   SELECT @DocumentTypeID = Max(DocumentTypeID) + 1,
          @DocumentDisplayOrder = Max(DisplayOrder) + 1
   FROM utb_document_type
   
   -- Add a new Document Type
   INSERT INTO dbo.utb_document_type (
      DocumentTypeID,
      DisplayOrder,
      DocumentClassCD,
      EnabledFlag,
      EstimateTypeFlag,
      Name,
      SysMaintainedFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @DocumentTypeID,        --DocumentTypeID,
      @DocumentDisplayOrder,  --DisplayOrder,
      'C',                    --DocumentClassCD,
      1,                      --EnabledFlag,
      1,                      --EstimateTypeFlag,
      'Estimate - Audit Detail Report', --Name,
      1,                      --SysMaintainedFlag,
      0,                      --SysLastUserID,
      @now                    --SysLastUpdatedDate
   )
   
   --SELECT @DocumentTypeID = SCOPE_IDENTITY()
   
   ---------------------------------
   -- Create Closing Audit Complete bundle
   ---------------------------------
   -- Add the message template
   INSERT INTO dbo.utb_message_template (
       AppliesToCD,
       EnabledFlag,
       Description,
       ServiceChannelCD,
       SysLastUserID,
       SysLastUpdatedDate
   ) VALUES (
       'C',
       1,
       'Closing - Audit Complete|Messages/msgGrangeDAClosingAuditComplete.xsl',
       'DA',
       0,
       @now
   )
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Closing - Audit Complete', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Desk Audit Summary Report
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      33,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add Final Estimate
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      @DocumentTypeID, -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      2,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'DA',                -- ServiceChannelCD,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
   
   

   ---------------------------------
   -- Create Closing Total Loss
   ---------------------------------
   -- Add the message template
   INSERT INTO dbo.utb_message_template (
       AppliesToCD,
       EnabledFlag,
       Description,
       ServiceChannelCD,
       SysLastUserID,
       SysLastUpdatedDate
   ) VALUES (
       'C',
       1,
       'Closing - Total Loss|Messages/msgGrangeDAClosingTL.xsl',
       'DA',
       0,
       @now
   )
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Closing - Total Loss', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   -- Add Final Estimate
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      @DocumentTypeID, -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'DA',                -- ServiceChannelCD,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
      


   ---------------------------------
   -- Create Closing Supplement
   ---------------------------------
   -- Add the message template
   INSERT INTO dbo.utb_message_template (
       AppliesToCD,
       EnabledFlag,
       Description,
       ServiceChannelCD,
       SysLastUserID,
       SysLastUpdatedDate
   ) VALUES (
       'C',
       1,
       'Closing - Supplement|Messages/msgGrangeDAClosingSupp.xsl',
       'DA',
       0,
       @now
   )
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   
   -- Create the bundling profile
   INSERT INTO dbo.utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      37, -- DocumentTypeID,
      @MessageTemplateID, -- MessageTemplateID,
      1, -- EnabledFlag,
      'Closing - Supplement', -- Name,
      0, -- SysLastUserID,
      @NOW -- SysLastUpdatedDate
   )
   
   SET @BundlingID = SCOPE_IDENTITY()
   
   
   -- Add Desk Audit Summary Report
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      33,            -- DocumentTypeID,
      NULL,          -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      1,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- Add Final Estimate
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      @DocumentTypeID, -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      0,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      1,             -- MandatoryFlag,
      2,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

   -- add it to the client bundling
   INSERT INTO dbo.utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,         -- BundlingID,
      @InsuranceCompanyID, -- InsuranceCompanyID,
      'DA',                -- ServiceChannelCD,
      0,                   -- SysLastUserID,
      @now                 -- SysLastUpdatedDate,
   )
      

   select dt.Name, b.Name, bdt.* 
   from utb_bundling_document_type bdt
   left join utb_document_type dt on bdt.DocumentTypeID = dt.DocumentTypeID
   left join utb_bundling b on bdt.BundlingID = b.BundlingID
   left join utb_client_bundling cb on b.BundlingID = cb.BundlingID
   where cb.InsuranceCompanyID = 289 --@InsuranceCompanyID
     and cb.ServiceChannelCD = 'DA'   
     
     select * from utb_document_type order by name
END


-- COMMIT
-- ROLLBACK