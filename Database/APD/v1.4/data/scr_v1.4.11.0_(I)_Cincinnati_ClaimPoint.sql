DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 194

------------------------------
-- Add Form Supplements
------------------------------
IF EXISTS 
(
	SELECT * FROM utb_client_scripting WHERE InsuranceCompanyID = @ToInscCompID
)
BEGIN
	UPDATE utb_client_scripting SET EnabledFlag = 0 WHERE InsuranceCompanyID = @ToInscCompID

	SELECT 'ClaimPoint settings updated...'
END
ELSE
BEGIN
	SELECT 'ClaimPoint settings already updated...'
END

