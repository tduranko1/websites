-- Conversion script to convert 1.3.4.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Copies repair_schedule_history, changing reference key to ClaimAspectServiceChannel
--
----------------------------------------------------------------------------------------
--  Dependancies:  utb_status must be populated from reference list and 
--                 utb_service_channel_schedule_change_reason must be populated from 
--                 the reference list before this is executed 
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION


--  Note:  utb_service_channel_schedule_change_reason must be populated from reference list before this is executed
          
PRINT 'Converting utb_repair_schedule_history to utb_service_channel_work_history'

SET IDENTITY_INSERT dbo.utb_service_channel_work_history ON

INSERT INTO dbo.utb_service_channel_work_history (
            HistoryID,
            ClaimAspectServiceChannelID,
            CreatedUserID,
            ReasonID,
            ChangeComment,
            ChangeDate,
            WorkEndNewDate,
            WorkEndOldDate,
            WorkStartNewDate,
            WorkStartOldDate,
            SysLastUserID,
            SysLastUpdatedDate
)
  SELECT  rsh.HistoryID,
          casc.ClaimAspectServiceChannelID,
          rsh.CreatedUserID,
          rsh.ReasonID,
          rsh.ChangeComment,
          rsh.ChangeDate,
          rsh.RepairEndNewDate,
          rsh.RepairEndOldDate,
          rsh.RepairStartNewDate,
          rsh.RepairStartOldDate,
          rsh.SysLastUserID,
          rsh.SysLastUpdatedDate
    FROM  dbo.tmp_utb_repair_schedule_history rsh
   INNER JOIN dbo.tmp_utb_claim_aspect ca ON (rsh.ClaimAspectID = ca.ClaimAspectID)
   INNER JOIN dbo.utb_claim_aspect_service_channel casc ON (ca.ClaimAspectID = casc.ClaimAspectID AND ca.ServiceChannelCD = casc.ServiceChannelCD)
    WHERE casc.PrimaryFlag = 1
    
SET IDENTITY_INSERT dbo.utb_service_channel_work_history OFF
          


--commit
--rollback


