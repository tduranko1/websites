/**********************************************************************************
* QBE DA Memo Bill
**********************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @FormID as int
DECLARE @DocumentTypeIDShopAssignment as int
DECLARE @now as datetime

SET @InsuranceCompanyID = 304
Set @now = CURRENT_TIMESTAMP

SELECT @DocumentTypeIDShopAssignment = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Memo Bill'

IF NOT EXISTS(SELECT FormID
                FROM dbo.utb_form
                WHERE InsuranceCompanyID = @InsuranceCompanyID
                  AND ServiceChannelCD = 'DA'
                  AND Name = 'Memo Bill')
BEGIN
    PRINT 'Adding DA Memo Bill'
    BEGIN TRANSACTION
    
    -- Add the DA Memo Bill
    INSERT INTO dbo.utb_form (
        DocumentTypeID
        , InsuranceCompanyID
        , AutoBundlingFlag
        , DataMiningFlag
        , EnabledFlag
        , HTMLPath
        , Name
        , PDFPath
        , PertainsToCD
        , ServiceChannelCD
        , SQLProcedure
        , SystemFlag
        , SysLastUserID
        , SysLastUpdatedDate
    )
    SELECT 
        @DocumentTypeIDShopAssignment
        , @InsuranceCompanyID
        , 1
        , 0
        , 1
        , 'Forms/QBEMemoBill.htm'
        , 'QBE Memo Bill'
        , 'QBEMemoBill.pdf'
        , 'C'
        , 'DA'
        , 'uspCFQBEMemoBillGetXML'
        , 0
        , 0
        , @now
        
    SELECT @FormID = SCOPE_IDENTITY()

    COMMIT TRANSACTION
    
    PRINT 'DA Memo Bill was added.'
END
