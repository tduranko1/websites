-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Copies ClientClaimNumber from claim_coverage to claim
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION

PRINT 'Updating utb_claim...'

SELECT COUNT(*) as 'Null ClientClaimNumbers' FROM dbo.utb_claim WHERE ClientClaimNumber IS NULL

UPDATE dbo.utb_claim
  SET  ClientClaimNumber = cc.ClientClaimNumber,
       ClientClaimNumberSquished = cc.ClientClaimNumberSquished
  FROM dbo.utb_claim c
 INNER JOIN dbo.Tmp_utb_claim_coverage cc ON (c.LynxID = cc.LynxID)

SELECT COUNT(*) as 'Null ClientClaimNumbers' FROM dbo.utb_claim WHERE ClientClaimNumber IS NULL

PRINT 'Complete!'

--commit
--rollback


