-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configuration for Amica Raliegh and Greenville offices
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Amica Mutual Insurance Company'


PRINT '.'
PRINT '.'
PRINT 'Creating Seattle Regional Office...'
PRINT '.'


-- Create Office(s)

-- Central Raleigh Office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '4800 Falls of Neuse Road',	 -- Address1
                'Suite 320',            -- Address2
                'Raleigh',               -- AddressCity
                'NC',                   -- AddressState
                '27609',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Raleigh',              -- ClientOfficeId
                1,                      -- EnabledFlag
                '866',					    -- FaxAreaCode
                '381',					    -- FaxExchangeNumber
                '3243',					    -- FaxUnitNumber
                'PO Box 700002',			 -- MailingAddress1
                NULL,                   -- MailingAddress2
                'Duluth',			       -- MailingAddressCity
                'GA',					    -- MailingAddressState
                '30097',				    -- MailingAddressZip
                'Raleigh Office',       -- Name
                '800',					    -- PhoneAreaCode
                '342',					    -- PhoneExchangeNumber
                '6422',					    -- PhoneUnitNumber
                NULL,					    -- ReturnDocEmailAddress
                NULL,					    -- ReturnDocFaxAreaCode
                NULL,					    -- ReturnDocFaxExchangeNumber
                NULL,					    -- ReturnDocFaxUnitNumber
                0,						    -- SysLastUserID
                @ModifiedDateTime		 -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Adverse Subro Desk Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


-- Central Greenville Office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '2 Independence Pointe',-- Address1
                'Suite 200',            -- Address2
                'Greenville',           -- AddressCity
                'SC',                   -- AddressState
                '29615',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Greenville',           -- ClientOfficeId
                1,                      -- EnabledFlag
                '866',					    -- FaxAreaCode
                '847',					    -- FaxExchangeNumber
                '1609',					    -- FaxUnitNumber
                'PO Box 26838',	       -- MailingAddress1
                '',                     -- MailingAddress2
                'Duluth',			       -- MailingAddressCity
                'GA',					    -- MailingAddressState
                '30097',				    -- MailingAddressZip
                'Greenville Office',    -- Name
                '800',					    -- PhoneAreaCode
                '342',					    -- PhoneExchangeNumber
                '6422',					    -- PhoneUnitNumber
                NULL,					    -- ReturnDocEmailAddress
                NULL,					    -- ReturnDocFaxAreaCode
                NULL,					    -- ReturnDocFaxExchangeNumber
                NULL,					    -- ReturnDocFaxUnitNumber
                0,						    -- SysLastUserID
                @ModifiedDateTime		 -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Adverse Subro Desk Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


    
-- Review affected tables
SELECT * FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT oat.* 
FROM dbo.utb_office_assignment_type oat 
LEFT JOIN utb_office o ON oat.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT ocs.* 
FROM dbo.utb_office_contract_state  ocs
LEFT JOIN utb_office o ON ocs.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID


--commit 
--rollback
