-----------------------------------------------------------------------------------------------------------
-- This script will add Desk Audit Service Channel to Unitrin
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 261

PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'



PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

-- add Desk Audit to the client service channel list
INSERT INTO utb_client_service_channel
(InsuranceCompanyID, ServiceChannelCD, ClientAuthorizesPaymentFlag, InvoicingModelBillingCD, SysLastUserID, SysLastUpdatedDate)
VALUES
(@InsuranceCompanyID, 'DA', 0, 'C', 0, @ModifiedDateTime)


--Insert Client level configuration
-- Add Desk Audit at the client level.
------------------------------------------------------
-- Add Desk Audit
INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE AssignmentTypeID = 7
  

------------------------------------------------------

-- Office level configurations
-- We will add the Desk Audit to each office

-- Unitrin Business Insurance - Southern Region office configuration
SET @OfficeID = NULL

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeID = 'UBI'

IF @OfficeID IS NOT NULL
BEGIN
   -- Add Desk Audit to Unitrin Business Insurance - Southern Region office
    INSERT INTO dbo.utb_office_assignment_type
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE AssignmentTypeID = 7
END    

-- Review affected tables
SELECT * FROM utb_client_service_channel where InsuranceCompanyID = @InsuranceCompanyID

SELECT * FROM dbo.utb_client_assignment_type where InsuranceCompanyID = @InsuranceCompanyID

SELECT oat.*
FROM dbo.utb_office_assignment_type oat
left join utb_office o on oat.OfficeID = o.OfficeID
where o.InsuranceCompanyID = @InsuranceCompanyID

--commit 
--rollback
