/********************************************************************
* Desk Audit Summary Report - Custom Form setup
********************************************************************/
-- Create a new document type for this custom form
DECLARE @DocumentTypeID as int
DECLARE @now as datetime
DECLARE @EventID as int

SET @now = CURRENT_TIMESTAMP

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Desk Audit Summary Report'

SELECT @EventID = EventID
FROM utb_event
WHERE Name = 'New Imaged Document'
  AND ClaimAspectTypeID = 9

BEGIN TRANSACTION

IF @DocumentTypeID IS NULL
BEGIN
    SELECT @DocumentTypeID = MAX(DocumentTypeID) + 1
    FROM utb_document_type

    INSERT INTO utb_document_type (
        DocumentTypeID,
        DisplayOrder,
        DocumentClassCD,
        EnabledFlag,
        EstimateTypeFlag,
        Name,
        SysMaintainedFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @DocumentTypeID,
        31,
        'C',
        1,
        0,
        'Desk Audit Summary Report',
        1,
        0,
        @now
    )

    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('Error inserting record into utb_document_type', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END
END

INSERT INTO utb_form(
    BundlingTaskID,
    DocumentTypeID,
    EventID,
    InsuranceCompanyID,
    LossStateCode,
    ShopStateCode,
    AutoBundlingFlag,
    DataMiningFlag,
    EnabledFlag,
    HTMLPath,
    Name,
    PDFPath,
    PertainsToCD,
    ServiceChannelCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) values(
    NULL,
    @DocumentTypeID,
    @EventID,
    NULL,
    'RI',
    NULL,
    0,
    1,
    1,
    'Forms/DASummaryReport_State.htm',
    'Desk Audit Summary Report',
    'DAAuditSummary_state.pdf',
    NULL,
    'DA',
    'uspCFDASummaryReportGetDetailXML',
    0,
    0,
    current_timestamp
)

IF @@error <> 0
BEGIN
    -- SQL Server Error

    RAISERROR  ('Error inserting record into utb_form', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_form(
    BundlingTaskID,
    DocumentTypeID,
    EventID,
    InsuranceCompanyID,
    LossStateCode,
    ShopStateCode,
    AutoBundlingFlag,
    DataMiningFlag,
    EnabledFlag,
    HTMLPath,
    Name,
    PDFPath,
    PertainsToCD,
    ServiceChannelCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) values(
    NULL,
    @DocumentTypeID,
    @EventID,
    NULL,
    'MA',
    NULL,
    0,
    1,
    1,
    'Forms/DASummaryReport_State.htm',
    'Desk Audit Summary Report',
    'DAAuditSummary_state.pdf',
    NULL,
    'DA',
    'uspCFDASummaryReportGetDetailXML',
    0,
    0,
    current_timestamp
)

IF @@error <> 0
BEGIN
    -- SQL Server Error

    RAISERROR  ('Error inserting record into utb_form', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- now select the table affected
SELECT * FROM utb_document_type
SELECT * FROM utb_form


-- COMMIT
-- ROLLBACK