USE [udb_apd_stg]
GO
/****** Object:  StoredProcedure [dbo].[uspWorkflowSendShopAssignXML]    Script Date: 06/18/2012 10:09:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowSendShopAssignXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Generates an XML Stream contiaining data necessary to send a shop assignment
*
* PARAMETERS:  
* (I) @AssignmentID         The AssignmentID being sent
*
* RESULT SET:
* An XML data stream
*
* Additional Notes:
*   PLEASE DO NOT MAKE SCHEMA CHANGES TO THE OUTPUT XML. THIS PROC IS USED FOR EXTERNAL COMMUNICATION
*     AND WILL BLOW BIZTALK SERVER. IF YOU DO WISH TO MAKE SCHEMA CHANGES, COORDINATE WITH 
*     BIZTALK TEAM TO GET THE CHANGES ACCEPTABLE
*
* VSS
* $Workfile: uspWorkflowSendShopAssignXML.sql $
* $Archive: /Database/APD/v1.0.0/procedure/xml/uspWorkflowSendShopAssignXML.sql $
* $Revision: 3 $
* $Author: Jonathan $
* $Date: 10/23/01 5:10p $
*
* $Revision: 4 $
* $Author: TDuranko $
* $Date: 01/26/2012 11:39AM $
*
************************************************************************************************************************/

-- Create the stored procedure


ALTER PROCEDURE [dbo].[uspWorkflowSendShopAssignXML]
    @AssignmentID   udt_std_id_big
AS
BEGIN
    -- Set database options

    SET ANSI_NULLS ON
    SET CONCAT_NULL_YIELDS_NULL OFF


    -- Declare variables

    DECLARE @ActionCodeVAN                AS varchar(8)
    DECLARE @AppraiserID                  AS udt_std_id_big
    DECLARE @AssignmentStatusIDFax        AS udt_std_id
    DECLARE @AssignmentStatusIDVAN        AS udt_std_id
    DECLARE @AssignmentSupportedFax       AS udt_std_flag
    DECLARE @AssignmentSupportedVAN       AS udt_std_flag
    DECLARE @BookValueAmt                 AS udt_std_money
    DECLARE @ClaimAspectID                AS udt_std_id_big
    DECLARE @ClaimAspectServiceChannelID  AS udt_std_id_big
    DECLARE @ClaimAspectNumber            AS udt_std_int
    DECLARE @ClaimAspectTypeIDClaim       AS udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle     AS udt_std_id
    DECLARE @CurrentImpactFlag            AS udt_std_flag
    DECLARE @DACCCClaimOfficeCode         AS udt_std_cd
    DECLARE @DeskAuditAppraiserType       AS udt_std_cd
    DECLARE @DeskAuditID                  AS udt_std_id_big
    DECLARE @DestinationType              AS varchar(4)
    DECLARE @ImpactString                 AS varchar(100)
    DECLARE @ImpactStringPrimary          AS varchar(50)
    DECLARE @ImpactStringSecondary        AS varchar(100)
    DECLARE @ImpactStringUnrelated        AS varchar(100)
    DECLARE @InsuranceCompanyID           AS udt_std_id
    DECLARE @LynxID                       AS udt_std_id_big
    DECLARE @MEAppraiserID                AS udt_std_id_big
    DECLARE @MessageProgram               AS udt_std_desc_long
    DECLARE @MessageNonProgram            AS udt_std_desc_long
    DECLARE @PrimaryImpactFlag            AS udt_std_flag
    DECLARE @PriorImpactFlag              AS udt_std_flag
    DECLARE @RootLossTypeID               AS udt_std_id
    DECLARE @SendElectronicFlag           AS char(1)
    DECLARE @SendFaxFlag                  AS char(1)
    DECLARE @ServiceChannelCD             AS udt_std_cd
    DECLARE @ShopLocationID               AS udt_std_id_big
    DECLARE @GLAppraiserID                AS udt_std_id_big
    
    DECLARE @CoverageConfirmedFlag        AS bit
    DECLARE @ClientClaimNumber            AS varchar(50)
    DECLARE @EffDeductibleSent            AS money
    DECLARE @RentalCoverageFlag           AS bit
    DECLARE @TotalLossWarningAmt          AS money
    DECLARE @CollLimitAmt                 AS money
    DECLARE @CompLimitAmt                 AS money
    DECLARE @GlassLimitAmt                AS money
    DECLARE @LiabLimitAmt                 AS money
    DECLARE @RentalDailyLimitAmt          AS money
    DECLARE @RentalLimitAmt               AS money
    DECLARE @TowingLimitAmt               AS money
    DECLARE @UIMLimitAmt                  AS money
    DECLARE @UMLimitAmt                   AS money
    DECLARE @AdminFeeComments             AS varchar(500)
    DECLARE @AdminFeeAmount               AS decimal(9, 2)
    DECLARE @CommunicationMethodID_CCC    as int
    DECLARE @CommunicationMethodID_ADP    as int
    DECLARE @CommunicationMethodID        as int
    
    DECLARE @SourceApplicationPassthruData AS VARCHAR(8000)    -- 26Jan2012 - TVD - Added for Elephants use of Passthru
    
    DECLARE @RepairLocationState          as varchar(2)

    DECLARE @now                          AS DateTime
    DECLARE @ProcName                     AS varchar(30)       -- Used for raise error stmts 
	DECLARE @AnalystUserID                AS varchar(50)       -- Used to pass Analyst info for CCC one  
	DECLARE @AnalystUserInfo              AS varchar(150)       -- Used to pass Analyst info for CCC one  

    SET @ProcName = 'uspWorkflowSendShopAssignXML'
	SET @AnalystUserInfo = ''

    -- Check to make sure a valid AssignmentID ID was passed in

    IF  (@AssignmentID IS NULL) OR
        (NOT EXISTS(SELECT AssignmentID FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID))
    BEGIN
        -- Invalid Assignment ID
    
        RAISERROR('101|%s|@AssignmentID|%u', 16, 1, @ProcName, @AssignmentID)
        RETURN
    END


    -- Get info from the assignment that we'll need
    
    SELECT  @LynxID = ca.LynxID,
            @ClaimAspectID = casc.ClaimAspectID,
            @ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID,
            @ClaimAspectNumber = ca.ClaimAspectNumber,
            @AppraiserID = a.AppraiserID,
            @ShopLocationID = a.ShopLocationID,
            @InsuranceCompanyID = c.InsuranceCompanyID,
            @DACCCClaimOfficeCOde = IsNull(i.DeskAuditCompanyCD, 'FTM'),
            @ServiceChannelCD = casc.ServiceChannelCD,
            @EffDeductibleSent = a.EffectiveDeductibleSentAmt,
            @CommunicationMethodID = cm.CommunicationMethodID,
            @RepairLocationState = casc.RepairLocationState,
			-- 26Jan2012 - TVD - Added for Elephants use of Passthru
            @SourceApplicationPassthruData = ca.SourceApplicationPassthruData
      FROM  dbo.utb_assignment a
		/*********************************************************************************
		Project: 210474 APD - Enhancements to support multiple concurrent service channels
		Note:	Added reference to utb_Claim_Aspect_Service_Channel to provide join
				between utb_Assignment and utb_Claim_Aspect tables
			M.A. 20061124
		*********************************************************************************/
      LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
      LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
      LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
      LEFT JOIN dbo.utb_communication_method cm ON a.CommunicationMethodID = cm.CommunicationMethodID
      WHERE a.AssignmentID = @AssignmentID
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
	--Get the Analyst Info for CCC one 
	SELECT @AnalystUserID = AnalystUserID FROM dbo.utb_claim_aspect WHERE claimaspectid = @ClaimAspectID
	
	IF (@AnalystUserID IS NOT NULL)
	BEGIN
		Select @AnalystUserInfo = nameFirst + ' ' + namelast + '(' + (select LogonID from utb_user_application where userid = @AnalystUserID)  + ')' from utb_user where UserID = @AnalystUserID and ReceiveCCCOneAssignmentFlag =  1
	END

    -- Get information on the Mobile Electronics vendor so we can distinguish assignments to it versus regular assignments
    
    SELECT  @MEAppraiserID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'Mobile_Electronics_AutoID'
    
    -- Get information on the Glass vendor so we can distinguish assignments to it versus regular assignments
    
    SELECT  @GLAppraiserID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'AGC_Appraiser_ID'
    
    -- Get information on the LYNX Desk audit unit so we can distinguish assignments to it versus regular assignments

    SELECT  @DeskAuditAppraiserType = Left(LTrim(RTrim(value)), CharIndex(',', LTrim(RTrim(value))) - 1),
            @DeskAuditID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'DESK_AUDIT_AUTOID'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @CommunicationMethodID_CCC = CommunicationMethodID
    FROM dbo.utb_communication_method
    WHERE NAME = 'CCC EZ-Net'
    
    SELECT @CommunicationMethodID_ADP = CommunicationMethodID
    FROM dbo.utb_communication_method
    WHERE NAME = 'ADP Shoplink'

    -- For non-DRP Assignments we need to override the Office code to 'FTM'.  Individual office codes are only sent on 
    -- LDAU assignments
        
    IF ((@ShopLocationID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'S')) OR
       ((@AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A'))
    BEGIN
        -- LDAU Assignment - Just set claim office code back to itself, required because sql requires a statement
        
        SET @DACCCClaimOfficeCode = @DACCCClaimOfficeCode
    END
    ELSE
    BEGIN
        -- Non-LDAU assignment, this code should be overridden to "FTM"
        
        SET @DACCCClaimOfficeCode = 'FTM'
    END

      
    DECLARE @tmpShop TABLE (ID                    bigint        NOT NULL,
                            ProgramFlag           bit               NULL,
                            Name                  varchar(50)   NOT NULL,
                            Address1              varchar(50)       NULL,
                            Address2              varchar(50)       NULL,
                            AddressCity           varchar(30)       NULL,
                            AddressState          char(2)           NULL,
                            AddressZip            varchar(8)        NULL,
                            Phone                 varchar(10)       NULL,
                            Fax                   varchar(10)       NULL,
                            Contact               varchar(50)       NULL)
                            
                            
    -- This is a Mobile Electronics or Desk Audit assignment using an appraiser address, pull Appraiser info from utb_appraiser.
    
    IF (@AppraiserID = @MEAppraiserID) OR
       ((@AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A')) OR
       (@AppraiserID = @GLAppraiserID)
    BEGIN
        INSERT INTO @tmpShop
        SELECT a.AppraiserID,
               CASE a.ProgramTypeCD
                  WHEN 'LS' THEN '1'
                  ELSE ''
               END,
               ap.Name,
               ap.Address1,
               ap.Address2,
               ap.AddressCity,
               ap.AddressState,
               ap.AddressZip,
               ap.PhoneAreaCode + ap.PhoneExchangeNumber + ap.PhoneUnitNumber, 
               Convert(Char(3),ap.FaxAreaCode) + Convert(Char(3),ap.FaxExchangeNumber) + Convert(Char(4),ap.FaxUnitNumber),            
               NULL
        FROM dbo.utb_assignment a
            LEFT JOIN dbo.utb_appraiser ap ON (a.AppraiserID = ap.AppraiserID)
        WHERE a.AssignmentID = @AssignmentID            
    END
    ELSE
    BEGIN
        INSERT INTO @tmpShop
        SELECT a.ShopLocationID,
               CASE a.ProgramTypeCD
                  WHEN 'LS' THEN '1'
                  ELSE ''
               END,
               sl.Name,
               sl.Address1,
               sl.Address2,
               sl.AddressCity,
               sl.AddressState,
               sl.AddressZip,
               sl.PhoneAreaCode + sl.PhoneExchangeNumber + sl.PhoneUnitNumber, 
               Convert(Char(3),sl.FaxAreaCode) + Convert(Char(3),sl.FaxExchangeNumber) + Convert(Char(4),sl.FaxUnitNumber),            
               IsNull((SELECT LTrim(RTrim(p.Name)) 
                       FROM dbo.utb_shop_location_personnel slp 
                           INNER JOIN dbo.utb_personnel p ON slp.PersonnelID = p.PersonnelID
                           INNER JOIN dbo.utb_personnel_type pt ON p.PersonnelTypeID = pt.PersonnelTypeID
                       WHERE slp.ShopLocationID = a.ShopLocationID
                         AND pt.Name = 'Shop Manager'), '')  
        FROM dbo.utb_assignment a
            LEFT JOIN dbo.utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
        WHERE a.AssignmentID = @AssignmentID    
    END
    
    
    ----------------------------------------------------------------------   

      
    SET @AssignmentSupportedFax = 1
    SET @AssignmentSupportedVAN = 1


    -- Get the assignment statuses
    
    SELECT @AssignmentStatusIDFax = cas.StatusID
    FROM dbo.utb_claim_aspect_status cas    
    WHERE cas.ClaimAspectID = @ClaimAspectID
      AND cas.StatusTypeCD = 'FAX'
      AND cas.ServiceChannelCD = @ServiceChannelCD
      

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @AssignmentStatusIDVAN = cas.StatusID
    FROM dbo.utb_claim_aspect_status cas
    WHERE cas.ClaimAspectID = @ClaimAspectID
      AND cas.StatusTypeCD = 'ELC'
      AND cas.ServiceChannelCD = @ServiceChannelCD

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Generate the Action code and Send values for Fax and VAN:  
    -- 1) The assignment record is first checked to see if a Cancellation date exists.  If it does, we must be in
    -- Cancel mode.  Set the Action Code for each to "Cancel", the Send Fax flag to "N" and the Send Electronic
    -- Flag to "Y" if a previous assignment had been successfully sent (determined by VAN Assignment Status).
    -- 2) If CancellationDate is NULL, we are in "New" mode.  Set the action code for each to new.  Then check (1) to
    -- make sure this is not a desk audit unit asignment and (2) the assignment statuses to determine the settings of
    -- the Send Fax and Send Electronic Flag.
    -- 3) If either the assignment states are not supported, the appropriate Send flag is defaulted to "N"
    -- 2/15 - RV - spoke with JP: With the implementation of Service channels, user cannot cancel an assignment. This prevents them
    --        to resend LDAU assignments that are in sent mode. For example: user A sends as LDAU assignment and
    --        goes on vacation. User B is now assigned to the claim and want to get the assignment into Pathways.
    --        Resend assignment does not send the file to his queue because "SendElectronicFlag" is "N". Before the
    --        service channel implementation, user can cancel the assignment and then pick LDAU to send to his queue.
    --        The fix for this is to allow LDAU assignments to be sent no matter what the status the assignment is in.


    -- Default the values to 'N'

    SET @SendFaxFlag = 'N'
    SET @SendElectronicFlag = 'N'


    IF (SELECT CancellationDate FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID) IS NOT NULL
    BEGIN
        -- We are sending a cancellation
            
        SET @ActionCodeVAN = 'Cancel'


        IF @AssignmentSupportedVAN = 1 
           AND @AssignmentStatusIDVAN NOT IN (11, 18)   -- (Shop Selected - Assignment Not Sent, Assignment Failure - OK To Resend)
           AND (SELECT CommunicationMethodID FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID) IS NOT NULL
           AND (SELECT CommunicationAddress FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID) IS NOT NULL
        BEGIN
            SET @SendElectronicFlag = 'Y'
        END
        
        -- RV: 2/9/2009 - Added for sending cancellation fax.
        IF @AssignmentSupportedFax = 1
        BEGIN
            SET @SendFaxFlag = 'Y'
        END
        ELSE
        BEGIN
            SET @SendFaxFlag = 'N'
        END
    END
    ELSE
    BEGIN
        SET @ActionCodeVAN = 'New'

        IF ((@ShopLocationID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'S')) OR
           ((@AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A')) -- Desk Audit
        BEGIN 
            SET @SendFaxFlag = 'N'
        END
        ELSE
        BEGIN
            IF @AssignmentSupportedFax = 1
            BEGIN
                SET @SendFaxFlag = 'Y'
            END
            ELSE
            BEGIN
                SET @SendFaxFlag = 'N'
            END
        END

        IF @AssignmentSupportedVAN = 1 
           AND (@AssignmentStatusIDVAN IN (11, 18)  -- Electronic send only when the status is not sent
            OR ((@AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A'))) -- override for LDAU. LDAU assignments can be sent even if they are in sent status
           AND (SELECT CommunicationMethodID FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID) IS NOT NULL
           AND (SELECT CommunicationAddress FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID) IS NOT NULL
        BEGIN
            SET @SendElectronicFlag = 'Y'
        END
    END

    -- Get current date

    SELECT @now = CURRENT_TIMESTAMP


    -- Determine Destination Type

    IF @ShopLocationID IS NOT NULL
    BEGIN
        SET @DestinationType = 'Shop'
    END
    ELSE
    BEGIN
        SET @DestinationType = 'IA'
    END


    -- Get Message for Program Shops
    SELECT  @MessageProgram = value 
      FROM  dbo.utb_app_variable
      WHERE Name = 'ASSIGNMENT_MSG_PROGRAM'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET  @MessageProgram = IsNull(@MessageProgram, 'Please retain all photos in file and comply with all agreed details.') 


    -- Get Message for Non-Program Shops
    SELECT  @MessageNonProgram = value 
      FROM  dbo.utb_app_variable
      WHERE Name = 'ASSIGNMENT_MSG_NONPROGRAM'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET  @MessageNonProgram = IsNull(@MessageNonProgram, 'This is a Notice of Loss and not an authorization to commence repair. Please forward all photos with your estimates to LYNX Services.') 

    
    -- Find the Root Loss Type ID.  We do this by working our way up the loss type tree until we find the 
    -- highest parent (where ParentLossTypeID = NULL)

    SELECT  @RootLossTypeID = LossTypeID
      FROM  dbo.utb_claim
      WHERE LynxID = @LynxID

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    WHILE (SELECT ParentLossTypeID FROM dbo.utb_loss_type WHERE LossTypeID = @RootLossTypeID) IS NOT NULL
    BEGIN
        SELECT  @RootLossTypeID = ParentLossTypeID
          FROM  dbo.utb_loss_type
          WHERE LossTypeID = @RootLossTypeID

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END  
    END


    -- Find the primary point of impact

    SELECT  @ImpactStringPrimary = i.Name,
            @ImpactString = i.Name
      FROM  dbo.utb_vehicle_impact vi
      LEFT JOIN dbo.utb_impact i ON (vi.ImpactID = i.ImpactID)
      WHERE vi.ClaimAspectID = @ClaimAspectID
        AND vi.PrimaryImpactFlag = 1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Construct All Remaining Points of Impact Data.  Declare a cursor containing impact records related to this claim vehicle

    DECLARE csrImpact CURSOR FOR
      SELECT  vi.CurrentImpactFlag,
              vi.PrimaryImpactflag,
              vi.PriorImpactFlag,
              i.Name
        FROM  dbo.utb_vehicle_impact vi
        INNER JOIN dbo.utb_impact i ON (vi.ImpactId = i.ImpactId)
        WHERE vi.ClaimAspectID = @ClaimAspectID


    -- Now step through the cursor and concatenate the impact names into a comma delimited list for the
    -- claim vehicle

    DECLARE @csrImpactName          udt_std_name
    DECLARE @FirstSecondaryImpact   udt_std_flag
    DECLARE @FirstPriorImpact       udt_std_flag

    SET @ImpactStringSecondary = ''
    SET @FirstSecondaryImpact = 1
    SET @FirstPriorImpact = 1

    OPEN csrImpact

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Fetch first impact record

    FETCH NEXT FROM csrImpact
      INTO @CurrentImpactFlag,
           @PrimaryImpactFlag,
           @PriorImpactFlag,
           @csrImpactName

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Append to string if current and not primary (we already got the primary before entering the loop)

        IF @CurrentImpactFlag = 1 AND @PrimaryImpactFlag = 0
        BEGIN
            SET @ImpactString = @ImpactString + ', ' + @csrImpactName
            IF @FirstSecondaryImpact = 1
            BEGIN
                SET @FirstSecondaryImpact = 0
            END
            ELSE
            BEGIN
                SET @ImpactStringSecondary = @ImpactStringSecondary + ', '
            END
            SET @ImpactStringSecondary = @ImpactStringSecondary + @csrImpactName
        END

        
        -- Append any prior impacts

        IF @PriorImpactFlag = 1
        BEGIN
            IF @FirstPriorImpact = 1
            BEGIN
                SET @FirstPriorImpact = 0
            END
            ELSE
            BEGIN
                SET @ImpactStringUnrelated = @ImpactStringUnrelated + ', '
            END
            SET @ImpactStringUnrelated = @ImpactStringUnrelated + @csrImpactName
        END

        
        -- Fetch next impact record

        FETCH NEXT FROM csrImpact
          INTO @CurrentImpactFlag,
               @PrimaryImpactFlag,
               @PriorImpactFlag,
               @csrImpactName

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END

    -- Close the cursor

    CLOSE csrImpact
    DEALLOCATE csrImpact


    -- Get Claim Aspect Type ID for Claim
    
    SELECT  @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
        -- Claim Aspect Type Not Found
    
        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END
    

    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Get Claim Aspect Type ID for Vehicle
    
    SELECT  @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
        -- Claim Aspect Type Not Found
    
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END
    

    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Get Book Value Amount

    SELECT  @BookValueAmt = BookValueAmt
      FROM  dbo.utb_claim_vehicle
      WHERE ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
     
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Continue to Verify APD Data State

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Insured')
    BEGIN
       -- Involved Type Not Found
    
        RAISERROR('102|%s|"Insured"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Owner')
    BEGIN
       -- Involved Type Not Found
    
        RAISERROR('102|%s|"Owner"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END

    -- Get the coverage information
    SELECT @CoverageConfirmedFlag = COUNT(ClaimCoverageID)
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND EnabledFlag = 1
    
    -- Get the client claim number
    SELECT @ClientClaimNumber = ClientClaimNumber
    FROM dbo.utb_claim
    WHERE LynxID = @LynxID
    
    -- work-around to solve long client claim number sent to CCC electronics shops.
    -- CCC allows a max of 25 chars and Glaxis prefixes our data with "N/A Claim#".
    -- This actually reduces our max to 15 chars long
    IF @SendElectronicFlag = 'Y'
    BEGIN
        -- Is the shop a CCC shop?
        -- Electronic data limits the data to 14 chars
        IF @CommunicationMethodID = @CommunicationMethodID_CCC
        BEGIN
            IF LEN(@ClientClaimNumber) > 14
            BEGIN
                SET @ClientClaimNumber = substring(@ClientClaimNumber, 1, 10) + '...'
            END
        END

        -- Is the shop a ADP shop?
        -- Electronic data limits the data to 17 chars
        IF @CommunicationMethodID = @CommunicationMethodID_CCC OR
		   @CommunicationMethodID = @CommunicationMethodID_ADP
        BEGIN
            IF LEN(@ClientClaimNumber) > 17
            BEGIN
                SET @ClientClaimNumber = substring(@ClientClaimNumber, 1, 14) + '...'
            END
        END
    END


    -- Get the rental is covered or not
    SELECT @RentalCoverageFlag = COUNT(ClaimCoverageID)
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'RENT'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1
      
    -- Get the total loss warning amount
    -- defect #4475 - Display threshold as percentage rather than amount
    /*SELECT @TotalLossWarningAmt  = CASE
                                       WHEN @BookValueAmt IS NULL THEN isNull(i.TotalLossWarningPercentage, 0)
                                       ELSE @BookValueAmt * isNull(i.TotalLossWarningPercentage, 0)
                                   END
    FROM dbo.utb_claim c
    LEFT JOIN dbo.utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE c.LynxID = @LynxID*/
    -- defect #4475 - end of change
    
    SELECT @TotalLossWarningAmt  = isNull(i.TotalLossWarningPercentage, 0)
    FROM dbo.utb_claim c
    LEFT JOIN dbo.utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    WHERE c.LynxID = @LynxID

    -- Get the Collision limit amount
    SELECT TOP 1 @CollLimitAmt = LimitAmt
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'COLL'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1

    -- Get the Comprehensive limit amount
    SELECT TOP 1 @CompLimitAmt = LimitAmt
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'COMP'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1

    -- Get the Glass limit amount
    SELECT TOP 1 @GlassLimitAmt = LimitAmt
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'GLAS'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1

    -- Get the Liability limit amount
    SELECT TOP 1 @LiabLimitAmt = LimitAmt
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'LIAB'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1

    -- Get the Rental Daily limit amount
    SELECT TOP 1 @RentalDailyLimitAmt = LimitDailyAmt
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'RENT'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1

    -- Get the Rental limit amount
    SELECT TOP 1 @RentalLimitAmt = LimitAmt
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'RENT'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1

    -- Get the Towing limit amount
    SELECT TOP 1 @TowingLimitAmt = LimitAmt
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'TOW'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1

    -- Get the Uninsured Insured limit amount
    SELECT TOP 1 @UIMLimitAmt = LimitAmt
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'UIM'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1

    -- Get the Under Insured limit amount
    SELECT TOP 1 @UMLimitAmt = LimitAmt
    FROM dbo.utb_claim_coverage
    WHERE LynxID = @LynxID
      AND CoverageTypeCD = 'UM'
      AND AddtlCoverageFlag = 0
      AND EnabledFlag = 1

    SELECT @AdminFeeAmount = dbo.ufnClaimAdminFee2ShopApplies(@ClaimAspectServiceChannelID)

    IF @AdminFeeAmount IS NOT NULL
    BEGIN
        SET @AdminFeeComments = 'Subject to terms of your LYNXSelect agreement, a $' + convert(varchar, @AdminFeeAmount) + ' administration fee will apply to this assignment when repairs have been completed, and will be deducted from your invoice payment.'
    END
    ELSE
    BEGIN
        SET @AdminFeeComments = ''
    END

    
    -- Begin XML Query
    -- Select Root Level

    SELECT  1 AS Tag,
            NULL AS Parent,
            Convert(varchar(20), @AssignmentID)                 AS [Assignment!1!AssignmentID], 
            @ServiceChannelCD                                   AS [Assignment!1!ServiceChannelCD],
            IsNull(@SendElectronicFlag, 'N')                    AS [Assignment!1!SendElectronic],
            IsNull(@SendFaxFlag, 'N')                           AS [Assignment!1!SendFax],
            @ActionCodeVAN                                      AS [Assignment!1!AssignmentCode],
            @DestinationType                                    AS [Assignment!1!DestinationType],
            LTrim(RTrim(IsNull(cm.RoutingCD, '')))              AS [Assignment!1!DeliveryMethod],
            LTrim(RTrim(IsNull(cm.Name, '')))                   AS [Assignment!1!DeliveryMethodDescription],
            'Assignment!1!DeliveryAddress' =
                CASE cm.RoutingCD
                    WHEN 'ADP' THEN UPPER(LTrim(RTrim(IsNull(a.CommunicationAddress, ''))))
                    ELSE LTrim(RTrim(IsNull(a.CommunicationAddress, '')))
                END,
            @DACCCClaimOfficeCode                               AS [Assignment!1!CCCClaimOfficeCode],
            LTrim(RTrim(IsNull(ep.Name, '')))                   AS [Assignment!1!EstimatingPackage],
            @LynxId                                             AS [Assignment!1!LynxId],
            @ClaimAspectNumber                                  AS [Assignment!1!VehicleNumber],
            IsNull(a.AssignmentSequenceNumber, 1)               AS [Assignment!1!AssignmentSequenceNumber],
            IsNull(a.AssignmentSuffix, '')                      AS [Assignment!1!Suffix],
            @now                                                AS [Assignment!1!CurrentDateTime],
            Right(Convert(char(4), DatePart(yy, @now)), 2) + dbo.ufnUtilityPadChars(Convert(varchar(3), DatePart(dy, @now)), 3, '0', 1) AS [Assignment!1!JulianDate],
            Right(Convert(char(3), DatePart(hh, @now) + 100), 2) + Right(Convert(char(3), DatePart(mi, @now) + 100), 2) + Right(Convert(char(3), DatePart(ss, @now) + 100), 2) AS [Assignment!1!CompressedTime],
            'Assignment!1!FaxHeader' =
                CASE a.ProgramTypeCD
                    WHEN 'LS' THEN 'ASSIGNMENT'
                    ELSE 'NOTICE OF LOSS'
                END,
            'Assignment!1!FaxFooter' =
                CASE a.ProgramTypeCD
                    WHEN 'LS' THEN @MessageProgram
                    ELSE @MessageNonProgram
                END,
            -- Claim Info
            NULL AS [Claim!2!AccidentDescription],
            NULL AS [Claim!2!AgentName],
            NULL AS [Claim!2!CarrierRepNameFirst],
            NULL AS [Claim!2!CarrierRepNameLast],
            NULL AS [Claim!2!CarrierRepPhone],
            NULL AS [Claim!2!CarrierRepPhoneExtension],
            NULL AS [Claim!2!CarrierRepFax],
            NULL AS [Claim!2!CarrierRepFaxExtension],
            NULL AS [Claim!2!CarrierRepEmailAddress],
            NULL AS [Claim!2!InsuranceCompanyID],
            NULL AS [Claim!2!InsuranceCompany],
            NULL AS [Claim!2!InsuranceCompanyAddress1],
            NULL AS [Claim!2!InsuranceCompanyAddress2],
            NULL AS [Claim!2!InsuranceCompanyAddressCity],
            NULL AS [Claim!2!InsuranceCompanyAddressState],
            NULL AS [Claim!2!InsuranceCompanyAddressZip],
            NULL AS [Claim!2!InsuredFirstName],
            NULL AS [Claim!2!InsuredLastName],
            NULL AS [Claim!2!InsuredBusinessName],
            NULL AS [Claim!2!InsuredAddress1],
            NULL AS [Claim!2!InsuredAddress2],
            NULL AS [Claim!2!InsuredAddressCity],
            NULL AS [Claim!2!InsuredAddressState],
            NULL AS [Claim!2!InsuredAddressZip],
            NULL AS [Claim!2!InsuredPrimaryPhone],
            NULL AS [Claim!2!InsuredDayPhone],
            NULL AS [Claim!2!InsuredDayPhoneExt],
            NULL AS [Claim!2!InsuredNightPhone],
            NULL AS [Claim!2!InsuredNightPhoneExt],
            NULL AS [Claim!2!InsuredAltPhone],
            NULL AS [Claim!2!InsuredAltPhoneExt],
            NULL AS [Claim!2!LossDate],
            NULL AS [Claim!2!LossState],
            NULL AS [Claim!2!LossTypeID],
            NULL AS [Claim!2!LossTypeDescription],
            NULL AS [Claim!2!LossDescription],
            NULL AS [Claim!2!PolicyNumber],
            NULL AS [Claim!2!RootLossTypeID],
            NULL AS [Claim!2!NoticeDate],
            
            -- Coverage Info
            NULL AS [Coverage!3!CoverageConfirmed],
            NULL AS [Coverage!3!ClaimNumber],
            NULL AS [Coverage!3!DeductibleType], 
            NULL AS [Coverage!3!Deductible],
            NULL AS [Coverage!3!RentalCoverage],
            NULL AS [Coverage!3!TotalLossWarningAmount],
            NULL AS [Coverage!3!CollisionLimitAmt],
            NULL As [Coverage!3!ComprehensiveLimitAmt],
            NULL AS [Coverage!3!GlassLimitAmt],
            NULL AS [Coverage!3!LiabilityLimitAmt],
            NULL AS [Coverage!3!RentalDailyLimitAmt],
            NULL AS [Coverage!3!RentalLimitAmt],
            NULL AS [Coverage!3!TowingLimitAmt],
            NULL AS [Coverage!3!UnderInsuredLimitAmt],
            NULL AS [Coverage!3!UnInsuredLimitAmt],            
            
            -- Claim Vehicle
            NULL AS [Vehicle!4!ClaimAspectID],
            NULL AS [Vehicle!4!BodyStyle],
            NULL AS [Vehicle!4!BookValueAmt],
            NULL AS [Vehicle!4!Color],
            NULL AS [Vehicle!4!CoverageType],
            NULL AS [Vehicle!4!Driveable],
            NULL AS [Vehicle!4!LicensePlateNo],
            NULL AS [Vehicle!4!LicensePlateState],
            NULL AS [Vehicle!4!LocationName],
            NULL AS [Vehicle!4!LocationAddress1],
            NULL AS [Vehicle!4!LocationAddress2],
            NULL AS [Vehicle!4!LocationCity],
            NULL AS [Vehicle!4!LocationState],
            NULL AS [Vehicle!4!LocationZip],
            NULL AS [Vehicle!4!LocationPhone],
            NULL AS [Vehicle!4!LocationPhoneExtension],
            NULL AS [Vehicle!4!LocationContactName],
            NULL AS [Vehicle!4!Make],
            NULL AS [Vehicle!4!Mileage],
            NULL AS [Vehicle!4!Model],
            NULL AS [Vehicle!4!Party],
            NULL AS [Vehicle!4!PointOfImpactDescription],
            NULL AS [Vehicle!4!PointOfImpactPrimary],
            NULL AS [Vehicle!4!PointOfImpactSecondary],
            NULL AS [Vehicle!4!PointOfImpactUnrelatedPrior],
            NULL AS [Vehicle!4!ServiceChannelCD],
            NULL AS [Vehicle!4!ShopRemarks],
            NULL AS [Vehicle!4!VehicleYear],
            NULL AS [Vehicle!4!Vin],
            NULL AS [Vehicle!4!AdminFeeComments],
            NULL AS [Vehicle!4!SourceApplicationPassthruData],       -- 26Jan2012 - TVD - Added for Elephants use of Passthru
            
            -- Vehicle Contact Info
            NULL AS [VehicleContact!5!VehicleContactFirstName],
            NULL AS [VehicleContact!5!VehicleContactLastName],
            NULL AS [VehicleContact!5!VehicleContactAddress1],
            NULL AS [VehicleContact!5!VehicleContactAddress2],
            NULL AS [VehicleContact!5!VehicleContactCity],
            NULL AS [VehicleContact!5!VehicleContactState],
            NULL AS [VehicleContact!5!VehicleContactZip],
            NULL AS [VehicleContact!5!VehicleContactDayPhone],
            NULL AS [VehicleContact!5!VehicleContactDayPhoneExtension],
            NULL AS [VehicleContact!5!VehicleContactNightPhone],
            NULL AS [VehicleContact!5!VehicleContactNightPhoneExtension],
            NULL AS [VehicleContact!5!VehicleContactAlternatePhone],
            NULL AS [VehicleContact!5!VehicleContactAlternatePhoneExtension],
            NULL AS [VehicleContact!5!VehicleContactEmailAddress],
            
            -- Shop Info
            NULL AS [Shop!6!ShopLocationID],
            NULL AS [Shop!6!ShopProgramFlag],
            NULL AS [Shop!6!ShopName],
            NULL AS [Shop!6!ShopAddress1],
            NULL AS [Shop!6!ShopAddress2],
            NULL AS [Shop!6!ShopCity],
            NULL AS [Shop!6!ShopState],
            NULL AS [Shop!6!ShopZip],
            NULL AS [Shop!6!ShopPhone],
            NULL AS [Shop!6!ShopFax],
            NULL AS [Shop!6!ShopContact],
            
            -- Owner Info
            NULL AS [Owner!7!OwnerFirstName],
            NULL AS [Owner!7!OwnerLastName],
            NULL AS [Owner!7!OwnerBusinessName],
            NULL AS [Owner!7!OwnerAddress1],
            NULL AS [Owner!7!OwnerAddress2],
            NULL AS [Owner!7!OwnerCity],
            NULL AS [Owner!7!OwnerState],
            NULL AS [Owner!7!OwnerZip],
            NULL AS [Owner!7!OwnerDayPhone],
            NULL AS [Owner!7!OwnerDayPhoneExtension],
            NULL AS [Owner!7!OwnerNightPhone],
            NULL AS [Owner!7!OwnerNightPhoneExtension],
            NULL AS [Owner!7!OwnerAlternatePhone],
            NULL AS [Owner!7!OwnerAlternatePhoneExtension],
            NULL AS [Owner!7!OwnerEmailAddress],
            
            -- Lynx Representative
            NULL AS [LynxRep!8!RepNameFirst],
            NULL AS [LynxRep!8!RepNameLast],
            NULL AS [LynxRep!8!RepPhone],
            NULL AS [LynxRep!8!RepPhoneExtension],
            NULL AS [LynxRep!8!RepFax],
            NULL AS [LynxRep!8!RepFaxExtension],
            NULL AS [LynxRep!8!RepEmailAddress]
            
    FROM dbo.utb_assignment a
    LEFT JOIN dbo.utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
    LEFT JOIN dbo.utb_estimate_package ep ON (sl.PreferredEstimatePackageID = ep.EstimatePackageID)
    LEFT JOIN dbo.utb_communication_method cm ON (a.CommunicationMethodID = cm.CommunicationMethodID)
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
    WHERE a.AssignmentID = @AssignmentID    


    UNION ALL


    -- Select Claim Info

    SELECT  2,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            
            -- Claim Info
            LTrim(RTrim(IsNull(c.LossDescription, ''))),
            LTrim(RTrim(IsNull(c.AgentName, ''))),
            LTrim(RTrim(IsNull(u.NameFirst, ''))),
            LTrim(RTrim(IsNull(u.NameLast, ''))),
            IsNull(LTRIM(RTRIM(u.PhoneAreaCode + u.PhoneExchangeNumber + u.PhoneUnitNumber)), ''),
            LTrim(RTrim(IsNull(u.PhoneExtensionNumber, ''))),
            IsNull(LTRIM(RTRIM(u.FaxAreaCode + u.FaxExchangeNumber + u.FaxUnitNumber)), ''),
            LTrim(RTrim(IsNull(u.FaxExtensionNumber, ''))),
            LTrim(RTrim(IsNull(u.EmailAddress, ''))),
            IsNull(ic.InsuranceCompanyID, ''),
            LTrim(RTrim(IsNull(ic.Name, ''))),
            LTrim(RTrim(IsNull(ic.Address1, ''))),
            LTrim(RTrim(IsNull(ic.Address2, ''))),
            LTrim(RTrim(IsNull(ic.AddressCity, ''))),
            LTrim(RTrim(IsNull(ic.AddressState, ''))),
            LTrim(RTrim(IsNull(ic.AddressZip, ''))),
            LTrim(RTrim(IsNull(i.NameFirst, ''))),
            LTrim(RTrim(IsNull(i.NameLast, ''))),
            LTrim(RTrim(IsNull(i.BusinessName, ''))),
            LTrim(RTrim(IsNull(i.Address1, ''))),
            LTrim(RTrim(IsNull(i.Address2, ''))),
            LTrim(RTrim(IsNull(i.AddressCity, ''))),
            LTrim(RTrim(IsNull(i.AddressState, ''))),
            LTrim(RTrim(IsNull(i.AddressZip, ''))),
            CASE
                WHEN BestContactPhoneCD = 'N' THEN 'Night'
                WHEN BestContactPhoneCD = 'A' THEN 'Alternate'
                ELSE 'Day'
            END,
            IsNull(LTRIM(RTRIM(i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber)), ''),
            LTrim(RTrim(IsNull(i.DayExtensionNumber, ''))),
            IsNull(LTRIM(RTRIM(i.NightAreaCode + i.NightExchangeNumber + i.NightUnitNumber)), ''),
            LTrim(RTrim(IsNull(i.NightExtensionNumber, ''))),
            IsNull(LTRIM(RTRIM(i.AlternateAreaCode + i.AlternateExchangeNumber + i.AlternateUnitNumber)), ''),
            LTrim(RTrim(IsNull(i.AlternateExtensionNumber, ''))),
            IsNull(c.LossDate, ''), 
            ISNULL(case 
                     when @ServiceChannelCD = 'DA' AND @RepairLocationState is not null then @RepairLocationState
                     else c.LossState
                   end, ''),
            LTrim(RTrim(IsNull(c.LossTypeID, ''))),
            LTrim(RTrim(IsNull(lt.description, ''))),
            LTrim(RTrim(IsNull(c.LossDescription, ''))),
            LTrim(RTrim(IsNull(c.PolicyNumber, ''))),
            LTrim(RTrim(IsNull(@RootLossTypeID, ''))),
            IsNull(ca.CreatedDate, ''),
            
            -- Coverage Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Vehicle Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Vehicle Contact Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,
            
            -- Shop Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Owner Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Lynx Representative Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL
            
    FROM    dbo.utb_claim c
    LEFT JOIN dbo.utb_insurance ic ON (c.InsuranceCompanyId = ic.InsuranceCompanyId)
    LEFT JOIN dbo.utb_loss_type lt ON (c.losstypeID = lt.LossTypeID)
    LEFT JOIN dbo.utb_user u ON (c.CarrierRepUserID = u.UserID)
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID)
    LEFT JOIN dbo.utb_claim_aspect_involved cai ON (ca.ClaimAspectID = cai.ClaimAspectId)
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE   c.LynxId = @LynxId
      AND   ca.ClaimAspectTypeID = @CLaimAspectTypeIDClaim
      AND   cai.EnabledFlag = 1
      AND   irt.Name = 'Insured'

    
    UNION ALL


    -- Select Coverage Info

    SELECT  3,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Coverage Info
            @CoverageConfirmedFlag, 
            LTrim(RTrim(IsNull(@ClientClaimNumber, ''))),
            'A',
            ISNULL(convert(varchar(20), @EffDeductibleSent),''),
            @RentalCoverageFlag,
            ISNULL(convert(varchar(20), @TotalLossWarningAmt),''),
            ISNULL(convert(varchar(20), @CollLimitAmt),''),
            ISNULL(convert(varchar(20), @CompLimitAmt),''),
            ISNULL(convert(varchar(20), @GlassLimitAmt),''),
            ISNULL(convert(varchar(20), @LiabLimitAmt),''),
            ISNULL(convert(varchar(20), @RentalDailyLimitAmt),''),
            ISNULL(convert(varchar(20), @RentalLimitAmt),''),
            ISNULL(convert(varchar(20), @TowingLimitAmt),''),
            ISNULL(convert(varchar(20), @UIMLimitAmt),''),
            ISNULL(convert(varchar(20), @UMLimitAmt),''),

            -- Claim Vehicle Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Vehicle Contact Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,
            
            -- Shop Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Owner Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Lynx Representative Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL
            
 
    UNION ALL


    -- Select Claim Vehicle Info
    SELECT  4,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            
            -- Claim Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Coverage Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Vehicle Info
            ca.ClaimAspectID,
            IsNull(cv.BodyStyle, ''),
            IsNull(convert(varchar(20), cv.BookValueAmt), ''),
            IsNull(cv.Color, ''),
            CASE ca.CoverageProfileCD
                WHEN 'COLL' THEN 'Collision'
                WHEN 'COMP' THEN 'Comprehensive'
                WHEN 'LIAB' THEN 'Liability'
                WHEN 'UIM'  THEN 'Other'
                WHEN 'UM'   THEN 'Other'
                ELSE ''
            END,
            LTrim(RTrim(IsNull(cv.DriveableFlag, 0))),
            LTrim(RTrim(IsNull(cv.LicensePlateNumber, ''))),
            LTrim(RTrim(IsNull(cv.LicensePlateState, ''))),
            LTrim(RTrim(IsNull(cv.LocationName, ''))),
            LTrim(RTrim(IsNull(cv.LocationAddress1, ''))),
            LTrim(RTrim(IsNull(cv.LocationAddress2, ''))),
            LTrim(RTrim(IsNull(cv.LocationCity, ''))),
            LTrim(RTrim(IsNull(cv.LocationState, ''))),
            LTrim(RTrim(IsNull(cv.LocationZip, ''))),
            IsNull(LTRIM(RTRIM(cv.LocationAreaCode + cv.LocationExchangeNumber + cv.LocationUnitNumber)), ''),
            LTrim(RTrim(IsNull(cv.LocationExtensionNumber, ''))),
            '',         -- Location contact Name just left empty until implemented in db
            LTrim(RTrim(IsNull(cv.Make, ''))),
            LTrim(RTrim(IsNull(cv.Mileage, 0))),
            LTrim(RTrim(IsNull(cv.Model, ''))),
            LTrim(RTrim(IsNull(ca.ExposureCD, ''))),
            IsNull(@ImpactString, ''), 
            IsNull(@ImpactStringPrimary, ''), 
            IsNull(@ImpactStringSecondary, ''), 
            IsNull(@ImpactStringUnrelated, ''), 
            IsNull(casc1.ServiceChannelCD, ''),
             --LTrim(RTrim(IsNull(a.AssignmentRemarks, '')))
             CASE casc1.ServiceChannelCD
                    WHEN 'RRP' THEN 'LYNX Advantage Repair Referral Assignment - Contact QBE Claim Rep regarding coverage or liability questions, deductibles, car rental, or general repair information.'+ ' ' + LTrim(RTrim(IsNull(a.AssignmentRemarks, '')))
					WHEN  'DA' THEN LTrim(RTrim(IsNull(a.AssignmentRemarks, ''))) + ' ' + @AnalystUserInfo
                    ELSE  LTrim(RTrim(IsNull(a.AssignmentRemarks, '')))
            END
            ,
            LTrim(RTrim(IsNull(cv.VehicleYear, 0))),
            LTrim(RTrim(IsNull(cv.Vin, ''))),
            @AdminFeeComments,
            LTrim(RTrim(IsNull(@SourceApplicationPassthruData, ''))),               -- 26Jan2012 - TVD - Added for Elephants use of Passthru
            
            -- Vehicle Contact Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,
            
            -- Shop Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Owner Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, 
            
            -- Lynx Representative Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL
            
    FROM    dbo.utb_assignment a
	LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
    --Project:210474 APD Remarked-off the following to support the schema change M.A.20061219
    --LEFT JOIN dbo.utb_assignment_type at ON (ca.CurrentAssignmentTypeID = at.AssignmentTypeID)   
    --Project:210474 APD Added the following to support the schema change M.A.20061219
    LEFT OUTER JOIN utb_Claim_Aspect_service_Channel casc1
    on ca.ClaimAspectID = casc1.ClaimAspectID
    and casc1.PrimaryFlag = 1
    LEFT JOIN dbo.utb_claim_vehicle cv ON (casc.ClaimAspectID = cv.ClaimAspectID)
    WHERE   a.AssignmentID = @AssignmentID
    

    UNION ALL


    -- Select Vehicle Contact Info
    SELECT  5,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Coverage Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Vehicle Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Vehicle Contact info 
            LTrim(RTrim(IsNull(i.NameFirst, ''))),
            LTrim(RTrim(IsNull(i.NameLast, ''))),
            LTrim(RTrim(IsNull(i.Address1, ''))),
            LTrim(RTrim(IsNull(i.Address2, ''))),
            LTrim(RTrim(IsNull(i.AddressCity, ''))),
            LTrim(RTrim(IsNull(i.AddressState, ''))),
            LTrim(RTrim(IsNull(i.AddressZip, ''))),
            IsNull(LTRIM(RTRIM(i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber)), ''),
            LTrim(RTrim(IsNull(i.DayExtensionNumber, ''))),
            IsNull(LTRIM(RTRIM(i.NightAreaCode + i.NightExchangeNumber + i.NightUnitNumber)), ''), 
            LTrim(RTrim(IsNull(i.NightExtensionNumber, ''))),
            IsNull(LTRIM(RTRIM(i.AlternateAreaCode + i.AlternateExchangeNumber + i.AlternateUnitNumber)), ''),
            LTrim(RTrim(IsNull(i.AlternateExtensionNumber, ''))),
            LTrim(RTrim(IsNull(i.EmailAddress, ''))),
            
            -- Shop Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Owner Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, 
            
            -- Lynx Representative Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL
            
    FROM    dbo.utb_claim_vehicle cv
    LEFT JOIN dbo.utb_involved i ON (cv.ContactInvolvedID = i.InvolvedID)
    WHERE   cv.ClaimAspectID = @ClaimAspectID


    UNION ALL


    -- Select Shop Info
    SELECT  6,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Coverage Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Vehicle Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Vehicle Contact Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,
            
            -- Shop Info
            LTrim(RTrim(IsNull(ID, ''))),
            LTrim(RTrim(IsNull(ProgramFlag, ''))),            
            LTrim(RTrim(IsNull(Name, ''))),
            LTrim(RTrim(IsNull(Address1, ''))),
            LTrim(RTrim(IsNull(Address2, ''))),
            LTrim(RTrim(IsNull(AddressCity, ''))),
            LTrim(RTrim(IsNull(AddressState, ''))),
            LTrim(RTrim(IsNull(AddressZip, ''))),
            IsNull(LTRIM(RTRIM(Phone)), ''), 
            IsNull(LTRIM(RTRIM(Fax)), ''),            
            IsNull(LTRIM(RTRIM(Contact)), ''),  
                      
            -- Owner Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Lynx Representative Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL
            
    FROM @tmpShop     
    

    UNION ALL


    -- Select Owner Info (Select Top 1 to ensure we only receive 1 owner in the output of a multiowner vehicle)
    SELECT TOP 1
            7,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Coverage Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Vehicle Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Vehicle Contact Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,
            
            -- Shop Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Owner Info
            LTrim(RTrim(IsNull(i.NameFirst, ''))),
            LTrim(RTrim(IsNull(i.NameLast, ''))),
            LTrim(RTrim(IsNull(i.BusinessName, ''))),
            LTrim(RTrim(IsNull(i.Address1, ''))),
            LTrim(RTrim(IsNull(i.Address2, ''))),
            LTrim(RTrim(IsNull(i.AddressCity, ''))),
            LTrim(RTrim(IsNull(i.AddressState, ''))),
            LTrim(RTrim(IsNull(i.AddressZip, ''))),
            IsNull(LTRIM(RTRIM(i.DayAreaCode + i.DayExchangeNumber + i.DayUnitNumber)), ''),
            LTrim(RTrim(IsNull(i.DayExtensionNumber, ''))),
            IsNull(LTRIM(RTRIM(i.NightAreaCode + i.NightExchangeNumber + i.NightUnitNumber)), ''), 
            LTrim(RTrim(IsNull(i.NightExtensionNumber, ''))),
            IsNull(LTRIM(RTRIM(i.AlternateAreaCode + i.AlternateExchangeNumber + i.AlternateUnitNumber)), ''),
            LTrim(RTrim(IsNull(i.AlternateExtensionNumber, ''))),
            LTrim(RTrim(IsNull(i.EmailAddress, ''))),
            
            -- Lynx Representative Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL
            
    FROM    dbo.utb_claim_aspect_involved cai
    LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
    LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedId = ir.InvolvedId)
    LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE   cai.ClaimAspectID = @ClaimAspectID
      AND   cai.EnabledFlag = 1
      AND   irt.Name = 'Owner'


    UNION ALL


    -- Select Lynx Representative Info
    SELECT TOP 1
            8,
            1,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            
            -- Coverage Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL,
            
            -- Claim Vehicle Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Vehicle Contact Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL,
            
            -- Shop Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL,
            
            -- Owner Info
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
            NULL, NULL, NULL, NULL, NULL, 
            
            -- Lynx Representative Info
            LTrim(RTrim(IsNull(u.NameFirst, ''))),
            LTrim(RTrim(IsNull(u.NameLast, ''))),
            IsNull(LTRIM(RTRIM(u.PhoneAreaCode + u.PhoneExchangeNumber + u.PhoneUnitNumber)), ''),
            LTrim(RTrim(IsNull(u.PhoneExtensionNumber, ''))),
            IsNull(LTRIM(RTRIM(u.FaxAreaCode + u.FaxExchangeNumber + u.FaxUnitNumber)), ''),
            LTrim(RTrim(IsNull(u.FaxExtensionNumber, ''))),
            LTrim(RTrim(IsNull(u.EmailAddress, '')))
            
    FROM    dbo.utb_user u
    WHERE   u.UserId = (SELECT OwnerUserID 
                        FROM dbo.utb_claim_aspect ca
                        WHERE ClaimAspectID = @ClaimAspectID)
      

    ORDER BY Tag
--    FOR XML EXPLICIT      -- (Comment out for Client-side XML)

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


