/**********************************************************************************
* QBE RRP Assignment form
**********************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @FormID as int
DECLARE @DocumentTypeIDShopAssignment as int
DECLARE @now as datetime

SET @InsuranceCompanyID = 304
Set @now = CURRENT_TIMESTAMP

SELECT @DocumentTypeIDShopAssignment = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Shop Assignment'

IF NOT EXISTS(SELECT FormID
                FROM dbo.utb_form
                WHERE InsuranceCompanyID = @InsuranceCompanyID
                  AND ServiceChannelCD = 'RRP'
                  AND Name = 'Shop Assignment - Cancellation')
BEGIN
    PRINT 'Adding RRP Shop Assignment Cancellation form'
    
    BEGIN TRANSACTION
    
    -- Add the RRP Shop Assignment Main Page
    INSERT INTO dbo.utb_form (
        DocumentTypeID
        , InsuranceCompanyID
        , EnabledFlag
        , Name
        , PDFPath
        , PertainsToCD
        , ServiceChannelCD
        , SQLProcedure
        , SystemFlag
        , SysLastUserID
        , SysLastUpdatedDate
    )
    SELECT 
        @DocumentTypeIDShopAssignment
        , @InsuranceCompanyID
        , 1
        , 'Shop Assignment - Cancellation'
        , 'RRPAssignmentCancel.pdf'
        , 'S'
        , 'RRP'
        , 'uspWorkflowSendShopAssignXML'
        , 1
        , 0
        , @now
        
    COMMIT TRANSACTION
    
    PRINT 'RRP Shop Assignment cancellation form was added.'
END
