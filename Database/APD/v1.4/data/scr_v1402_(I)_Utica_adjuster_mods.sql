BEGIN TRANSACTION

-- Setup users for MARO office - 92

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Terri.Bishop@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Michele.Merrick@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Carolyn.Rawlings@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Leanne.Schmidt@uticanational.com'

update dbo.utb_user_application
set logonid = 'Jessica.Calandra@uticanational.com'
where logonid = 'Jessica.Stuart@uticanational.com'

UPDATE dbo.utb_user 
set EmailAddress = 'Jessica.Calandra@uticanational.com',
    NameLast = 'Calandra'
where EmailAddress = 'Jessica.Stuart@uticanational.com'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Lee.Caplan@uticanational.com',
    @NameFirst             = 'Lee',
    @NameLast              = 'Caplan',
    @EmailAddress          = 'Lee.Caplan@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6619'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Kheila.Jones@uticanational.com',
    @NameFirst             = 'Kheila',
    @NameLast              = 'Jones',
    @EmailAddress          = 'Kheila.Jones@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6669'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Elisa.Lindsey@uticanational.com',
    @NameFirst             = 'Elisa',
    @NameLast              = 'Lindsey',
    @EmailAddress          = 'Elisa.Lindsey@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6668'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'John.Robertson@uticanational.com',
    @NameFirst             = 'John',
    @NameLast              = 'Robertson',
    @EmailAddress          = 'John.Robertson@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6663'


-- Setup users for ERO office - 96

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'David.Roth@uticanational.com'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Sue.Dailey@uticanational.com',
    @NameFirst             = 'Sue',
    @NameLast              = 'Dailey',
    @EmailAddress          = 'Sue.Dailey@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6745'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Kari.Herman@uticanational.com',
    @NameFirst             = 'Kari',
    @NameLast              = 'Herman',
    @EmailAddress          = 'Kari.Herman@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6725'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Linda.Keator@uticanational.com',
    @NameFirst             = 'Linda',
    @NameLast              = 'Keator',
    @EmailAddress          = 'Linda.Keator@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6715'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Michele.Moon@uticanational.com',
    @NameFirst             = 'Michele',
    @NameLast              = 'Moon',
    @EmailAddress          = 'Michele.Moon@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6711'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Tricia.Murray@uticanational.com',
    @NameFirst             = 'Tricia',
    @NameLast              = 'Murray',
    @EmailAddress          = 'Tricia.Murray@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6714'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Robert.Nelson@uticanational.com',
    @NameFirst             = 'Robert',
    @NameLast              = 'Nelson',
    @EmailAddress          = 'Robert.Nelson@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6618'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Kathryn.Papaleo@uticanational.com',
    @NameFirst             = 'Kathryn',
    @NameLast              = 'Papaleo',
    @EmailAddress          = 'Kathryn.Papaleo@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6713'


-- Setup users for Albany office - 94

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Elaine.Tucceri@uticanational.com',
    @NameFirst             = 'Elaine',
    @NameLast              = 'Tucceri',
    @EmailAddress          = 'Elaine.Tucceri@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6227'


-- Setup users for Amherst office - 95

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Perry.Kaupa@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Kathleen.McCann@uticanational.com'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Eric.Dibble@uticanational.com',
    @NameFirst             = 'Eric',
    @NameLast              = 'Dibble',
    @EmailAddress          = 'Eric.Dibble@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2345'



print 'Done.'

-- COMMIT