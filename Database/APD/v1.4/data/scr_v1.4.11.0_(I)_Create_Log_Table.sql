/****** Object:  Table [dbo].[utb_log]    Script Date: 12/03/2012 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[utb_log](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[AppName] [varchar](50) NULL,
	[UserID] [int] NULL,
	[FuncName] [varchar](50) NULL,
	[SPName] [varchar](50) NULL,
	[Data1] [varchar](500) NULL,
	[Data2] [varchar](500) NULL,
	[Data3] [varchar](500) NULL,
	[SysLastUserID] [int] NULL,
	[SysLastUpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_utb_log] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
) ON [ufg_claim]
) ON [ufg_claim]

GO

SET ANSI_PADDING ON
GO
