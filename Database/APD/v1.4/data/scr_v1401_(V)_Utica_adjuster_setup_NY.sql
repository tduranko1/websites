-- Setup users for ERO office
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'linda.angrisano@uticanational.com',
    @NameFirst             = 'Linda',
    @NameLast              = 'Angrisano',
    @EmailAddress          = 'linda.angrisano@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6739'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'theresa.anson@uticanational.com',
    @NameFirst             = 'Theresa',
    @NameLast              = 'Anson',
    @EmailAddress          = 'theresa.anson@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6741'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'dennis.brenon@uticanational.com',
    @NameFirst             = 'Dennis',
    @NameLast              = 'Brenon',
    @EmailAddress          = 'dennis.brenon@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6738'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'genevieve.cahill@uticanational.com',
    @NameFirst             = 'Genevieve',
    @NameLast              = 'Cahill',
    @EmailAddress          = 'genevieve.cahill@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6618'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'paul.campbell@uticanational.com',
    @NameFirst             = 'Paul',
    @NameLast              = 'Campbell',
    @EmailAddress          = 'paul.campbell@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6760'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'dan.d''angelo@uticanational.com',
    @NameFirst             = 'Dan',
    @NameLast              = 'D''Angelo',
    @EmailAddress          = 'dan.d''angelo@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6616'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'michael.defabio@uticanational.com',
    @NameFirst             = 'Michael',
    @NameLast              = 'Defabio',
    @EmailAddress          = 'michael.defabio@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6717'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'debra.fairbrother@uticanational.com',
    @NameFirst             = 'Debra',
    @NameLast              = 'Fairbrother',
    @EmailAddress          = 'debra.fairbrother@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6724'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'suzanne.gable@uticanational.com',
    @NameFirst             = 'Suzanne',
    @NameLast              = 'Gable',
    @EmailAddress          = 'suzanne.gable@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6727'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'joseph.gallagher@uticanational.com',
    @NameFirst             = 'Joseph',
    @NameLast              = 'Gallagher',
    @EmailAddress          = 'joseph.gallagher@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6728'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'wendy.hake@uticanational.com',
    @NameFirst             = 'Wendy',
    @NameLast              = 'Hake',
    @EmailAddress          = 'wendy.hake@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6719'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'michelle.inman@uticanational.com',
    @NameFirst             = 'Michelle',
    @NameLast              = 'Inman',
    @EmailAddress          = 'michelle.inman@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6726'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'patricia.karuzas@uticanational.com',
    @NameFirst             = 'Patricia',
    @NameLast              = 'Karuzas',
    @EmailAddress          = 'patricia.karuzas@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6742'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'rebecca.laymon@uticanational.com',
    @NameFirst             = 'Rebecca',
    @NameLast              = 'Laymon',
    @EmailAddress          = 'rebecca.laymon@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6737'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kathy.marek@uticanational.com',
    @NameFirst             = 'Kathy',
    @NameLast              = 'Marek',
    @EmailAddress          = 'kathy.marek@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6735'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'edna.martinez-herrin@uticanational.com',
    @NameFirst             = 'Edna',
    @NameLast              = 'Martinez-Herrin',
    @EmailAddress          = 'edna.martinez-herrin@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6755'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'marie.merrick@uticanational.com',
    @NameFirst             = 'Marie',
    @NameLast              = 'Merrick',
    @EmailAddress          = 'marie.merrick@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6721'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'liz.mikoda@uticanational.com',
    @NameFirst             = 'Liz',
    @NameLast              = 'Mikoda',
    @EmailAddress          = 'liz.mikoda@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6740'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'lynne.morinitti@uticanational.com',
    @NameFirst             = 'Lynne',
    @NameLast              = 'Morinitti',
    @EmailAddress          = 'lynne.morinitti@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6743'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'carol.murphy@uticanational.com',
    @NameFirst             = 'Carol',
    @NameLast              = 'Murphy',
    @EmailAddress          = 'carol.murphy@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6744'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'vernadine.reid@uticanational.com',
    @NameFirst             = 'Vernadine',
    @NameLast              = 'Reid',
    @EmailAddress          = 'vernadine.reid@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6734'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'annette.rivera@uticanational.com',
    @NameFirst             = 'Annette',
    @NameLast              = 'Rivera',
    @EmailAddress          = 'annette.rivera@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6733'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'david.roth@uticanational.com',
    @NameFirst             = 'David',
    @NameLast              = 'Roth',
    @EmailAddress          = 'david.roth@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6745'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'robin.schiebler@uticanational.com',
    @NameFirst             = 'Robin',
    @NameLast              = 'Schiebler',
    @EmailAddress          = 'robin.schiebler@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6754'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'patricia.schug@uticanational.com',
    @NameFirst             = 'Patricia',
    @NameLast              = 'Schug',
    @EmailAddress          = 'patricia.schug@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6620'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'diane.servello@uticanational.com',
    @NameFirst             = 'Diane',
    @NameLast              = 'Servello',
    @EmailAddress          = 'diane.servello@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6619'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'adam.spooner@uticanational.com',
    @NameFirst             = 'Adam',
    @NameLast              = 'Spooner',
    @EmailAddress          = 'adam.spooner@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6720'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'teresa.steele@uticanational.com',
    @NameFirst             = 'Teresa',
    @NameLast              = 'Steele',
    @EmailAddress          = 'teresa.steele@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6736'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'gregory.von-matt@uticanational.com',
    @NameFirst             = 'Gregory',
    @NameLast              = 'Von-Matt',
    @EmailAddress          = 'gregory.von-matt@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6730'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kathleen.waterman@uticanational.com',
    @NameFirst             = 'Kathleen',
    @NameLast              = 'Waterman',
    @EmailAddress          = 'kathleen.waterman@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6617'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'lisa.watkins@uticanational.com',
    @NameFirst             = 'Lisa',
    @NameLast              = 'Watkins',
    @EmailAddress          = 'lisa.watkins@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6722'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'pam.wilson@uticanational.com',
    @NameFirst             = 'Pam',
    @NameLast              = 'Wilson',
    @EmailAddress          = 'pam.wilson@uticanational.com',
    @OfficeID              = 96,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6691'
GO

-- Setup Albany office adjusters
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kate.boschock@uticanational.com',
    @NameFirst             = 'Kathryn',
    @NameLast              = 'Boschock',
    @EmailAddress          = 'kate.boschock@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6227'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'richard.cancelino@uticanational.com',
    @NameFirst             = 'Richard',
    @NameLast              = 'Cancelino',
    @EmailAddress          = 'richard.cancelino@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6229'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'phil.coniglione@uticanational.com',
    @NameFirst             = 'Phil',
    @NameLast              = 'Coniglione',
    @EmailAddress          = 'phil.coniglione@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6224'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'k.doremus-green@uticanational.com',
    @NameFirst             = 'Kim',
    @NameLast              = 'Doremus-Green',
    @EmailAddress          = 'k.doremus-green@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6237'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'suzanne.flansburg@uticanational.com',
    @NameFirst             = 'Suzanne',
    @NameLast              = 'Flansburg',
    @EmailAddress          = 'suzanne.flansburg@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6223'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'michele.jeffs@uticanational.com',
    @NameFirst             = 'Michele',
    @NameLast              = 'Jeffs',
    @EmailAddress          = 'michele.jeffs@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6222'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'nancy.jeram@uticanational.com',
    @NameFirst             = 'Nancy',
    @NameLast              = 'Jeram',
    @EmailAddress          = 'nancy.jeram@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6244'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'lisa.layman@uticanational.com',
    @NameFirst             = 'Lisa',
    @NameLast              = 'Layman',
    @EmailAddress          = 'lisa.layman@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6236'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'karen.lind@uticanational.com',
    @NameFirst             = 'Karen',
    @NameLast              = 'Lind',
    @EmailAddress          = 'karen.lind@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6225'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'rose.macci@uticanational.com',
    @NameFirst             = 'Rose',
    @NameLast              = 'Macci',
    @EmailAddress          = 'rose.macci@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6201'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'charleen.malek@uticanational.com',
    @NameFirst             = 'Charleen',
    @NameLast              = 'Malek',
    @EmailAddress          = 'charleen.malek@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6226'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'russell.phillips@uticanational.com',
    @NameFirst             = 'Russell',
    @NameLast              = 'Phillips',
    @EmailAddress          = 'russell.phillips@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6250'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'laura.phoenix@uticanational.com',
    @NameFirst             = 'Laura',
    @NameLast              = 'Phoenix',
    @EmailAddress          = 'laura.phoenix@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6251'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'peter.ranalli@uticanational.com',
    @NameFirst             = 'Peter',
    @NameLast              = 'Ranalli',
    @EmailAddress          = 'peter.ranalli@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6253'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'joseph.sbardella@uticanational.com',
    @NameFirst             = 'Joseph',
    @NameLast              = 'Sbardella',
    @EmailAddress          = 'joseph.sbardella@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6233'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'tiffany.sierra@uticanational.com',
    @NameFirst             = 'Tiffany',
    @NameLast              = 'Sierra',
    @EmailAddress          = 'tiffany.sierra@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6228'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'denise.taber@uticanational.com',
    @NameFirst             = 'Denise',
    @NameLast              = 'Taber',
    @EmailAddress          = 'denise.taber@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6235'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'william.wagenbrenner@uticanational.com',
    @NameFirst             = 'William',
    @NameLast              = 'Wagenbrenner',
    @EmailAddress          = 'william.wagenbrenner@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6231'
GO

-- Setup Amherst office adjusters
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'david.clifford@uticanational.com',
    @NameFirst             = 'Dave',
    @NameLast              = 'Clifford',
    @EmailAddress          = 'david.clifford@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2341'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kathleen.earsing@uticanational.com',
    @NameFirst             = 'Kathleen',
    @NameLast              = 'Earsing',
    @EmailAddress          = 'kathleen.earsing@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2346'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kevin.evans@uticanational.com',
    @NameFirst             = 'Kevin',
    @NameLast              = 'Evans',
    @EmailAddress          = 'kevin.evans@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2357'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'diana.hazzan@uticanational.com',
    @NameFirst             = 'Diana',
    @NameLast              = 'Hazzan',
    @EmailAddress          = 'diana.hazzan@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2331'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'scott.holler@uticanational.com',
    @NameFirst             = 'Scott',
    @NameLast              = 'Holler',
    @EmailAddress          = 'scott.holler@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2353'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'perry.kaupa@uticanational.com',
    @NameFirst             = 'Perry',
    @NameLast              = 'Kaupa',
    @EmailAddress          = 'perry.kaupa@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2332'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kathleen.mccann@uticanational.com',
    @NameFirst             = 'Kathleen',
    @NameLast              = 'McCann',
    @EmailAddress          = 'kathleen.mccann@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2347'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'julie.mccreedy@uticanational.com',
    @NameFirst             = 'Julie',
    @NameLast              = 'McCreedy',
    @EmailAddress          = 'julie.mccreedy@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2351'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'sean.mcgovern@@uticanational.com',
    @NameFirst             = 'Sean',
    @NameLast              = 'McGovern',
    @EmailAddress          = 'sean.mcgovern@@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2333'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'richard.mcknight@uticanational.com',
    @NameFirst             = 'Dick',
    @NameLast              = 'McKnight',
    @EmailAddress          = 'richard.mcknight@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2342'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'brenda.moran@uticanational.com',
    @NameFirst             = 'Brenda',
    @NameLast              = 'Moran',
    @EmailAddress          = 'brenda.moran@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2334'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'mark.nowak@uticanational.com',
    @NameFirst             = 'Mark',
    @NameLast              = 'Nowak',
    @EmailAddress          = 'mark.nowak@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2335'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'beth.oliveri@uticanational.com',
    @NameFirst             = 'Beth',
    @NameLast              = 'Oliveri',
    @EmailAddress          = 'beth.oliveri@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2350'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'julie.ortman@uticanational.com',
    @NameFirst             = 'Julie',
    @NameLast              = 'Ortman',
    @EmailAddress          = 'julie.ortman@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2361'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'james.pierino@uticanational.com',
    @NameFirst             = 'Jim',
    @NameLast              = 'Pierino',
    @EmailAddress          = 'james.pierino@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2354'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'tom.schmitt@uticanational.com',
    @NameFirst             = 'Tom',
    @NameLast              = 'Schmitt',
    @EmailAddress          = 'tom.schmitt@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2344'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'paul.strycharz@uticanational.com',
    @NameFirst             = 'Paul',
    @NameLast              = 'Strycharz',
    @EmailAddress          = 'paul.strycharz@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2307'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'samuel.tiranno@uticanational.com',
    @NameFirst             = 'Sam',
    @NameLast              = 'Tiranno',
    @EmailAddress          = 'samuel.tiranno@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2336'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'donna.valone@uticanational.com',
    @NameFirst             = 'Donna',
    @NameLast              = 'Valone',
    @EmailAddress          = 'donna.valone@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '716',
    @PhoneExchangeNumber   = '639',
    @PhoneUnitNumber       = '2300'
GO


print 'Done.'