
BEGIN TRANSACTION

-- Setup users for MARO office - 92

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Chris.Middlebrook@uticanational.com',
    @NameFirst             = 'Chris',
    @NameLast              = 'Middlebrook',
    @EmailAddress          = 'Chris.Middlebrook@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6619'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Nancy.Moore@uticanational.com',
    @NameFirst             = 'Nancy',
    @NameLast              = 'Moore',
    @EmailAddress          = 'Nancy.Moore@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6669'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Jessica.Calandra@uticanational.com'

-- Setup users for Albany office - 94

-- Setup users for Amherst office - 95

-- Setup users for ERO office - 96

-- Setup users for Fast Track office - 97

-- Setup users for NYMRO office - 98

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Susan.Harkoff@uticanational.com',
    @NameFirst             = 'Susan',
    @NameLast              = 'Harkoff',
    @EmailAddress          = 'Susan.Harkoff@uticanational.com',
    @OfficeID              = 98,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '5125'

-- Setup users for NERO office - 101

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Joseph.Pedi@uticanational.com',
    @NameFirst             = 'Joseph',
    @NameLast              = 'Pedi',
    @EmailAddress          = 'Joseph.Pedi@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2674'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Edith.Schultz@uticanational.com'


print 'Done.'

-- COMMIT
-- rollback