
INSERT INTO utb_document_type (
    DocumentTypeID,
    DisplayOrder,
    DocumentClassCD,
    EnabledFlag,
    EstimateTypeFlag,
    Name,
    SysMaintainedFlag,
    SysLastUserID,
    SysLastUpdatedDate
) 
SELECT 53, 53, 'C', 1, 0, 'Vehicle Owner Title', 0, 0, current_timestamp

INSERT INTO utb_document_type (
    DocumentTypeID,
    DisplayOrder,
    DocumentClassCD,
    EnabledFlag,
    EstimateTypeFlag,
    Name,
    SysMaintainedFlag,
    SysLastUserID,
    SysLastUpdatedDate
) 
SELECT 54, 54, 'C', 1, 0, 'Vehicle Owner POA', 0, 0, current_timestamp

INSERT INTO utb_document_type (
    DocumentTypeID,
    DisplayOrder,
    DocumentClassCD,
    EnabledFlag,
    EstimateTypeFlag,
    Name,
    SysMaintainedFlag,
    SysLastUserID,
    SysLastUpdatedDate
) 
SELECT 55, 55, 'C', 1, 0, 'Lien Title Copy', 0, 0, current_timestamp

INSERT INTO utb_document_type (
    DocumentTypeID,
    DisplayOrder,
    DocumentClassCD,
    EnabledFlag,
    EstimateTypeFlag,
    Name,
    SysMaintainedFlag,
    SysLastUserID,
    SysLastUpdatedDate
) 
SELECT 56, 56, 'C', 1, 0, 'Letter of Guarantee', 0, 0, current_timestamp

INSERT INTO utb_document_type (
    DocumentTypeID,
    DisplayOrder,
    DocumentClassCD,
    EnabledFlag,
    EstimateTypeFlag,
    Name,
    SysMaintainedFlag,
    SysLastUserID,
    SysLastUpdatedDate
) 
SELECT 57, 57, 'C', 1, 0, 'Lien Holder POA', 0, 0, current_timestamp

INSERT INTO utb_document_type (
    DocumentTypeID,
    DisplayOrder,
    DocumentClassCD,
    EnabledFlag,
    EstimateTypeFlag,
    Name,
    SysMaintainedFlag,
    SysLastUserID,
    SysLastUpdatedDate
) 
SELECT 58, 58, 'C', 1, 0, 'Lien Holder Clear Title', 0, 0, current_timestamp


INSERT INTO utb_note_type (
   NoteTypeID,
   DisplayOrder,
   EnabledFlag,
   Name,
   SysMaintainedFlag,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 19, 19, 1, 'Claim Edit', 1, 0, current_timestamp