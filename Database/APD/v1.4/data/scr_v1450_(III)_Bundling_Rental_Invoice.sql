/******************************************************************************
* SWR 218805 - Add Rental Invoice as optional to Closing Repair Complete
*			   Bundling profile
* Author:	   Ramesh Vishegu
* Date:		   12/12/2008
******************************************************************************/


DECLARE @DocumentTypeID_RentalInvoice as int
DECLARE @now as datetime
DECLARE @BundlingID as int
DECLARE @MessageTemplateID as int

SET @now = CURRENT_TIMESTAMP 

SELECT @DocumentTypeID_RentalInvoice = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Rental Invoice'

IF @DocumentTypeID_RentalInvoice IS NULL
BEGIN
	PRINT 'Rental Invoice Document Type not found.' 
	RETURN
END

BEGIN TRANSACTION

INSERT INTO utb_bundling_document_type (
	BundlingID,
	DocumentTypeID,
	DirectionalCD,
	DirectionToPayFlag,
	EstimateDuplicateFlag,
	FinalEstimateFlag,
	MandatoryFlag,
	SelectionOrder,
	VANFlag,
	SysLastUserID,
	SysLastUpdatedDate
)
SELECT BundlingID,
	   @DocumentTypeID_RentalInvoice,
	   'I',		-- DirectionalCD
	   0,		-- DirectionToPayFlag
	   0,		-- EstimateDuplicateFlag
	   0,		-- FinalEstimateFlag,
	   0,		-- MandatoryFlag,
	   5,		-- SelectionOrder,
	   0,		-- VANFlag,
	   0,		-- SysLastUserID,
	   @now		-- SysLastUpdatedDate
FROM utb_bundling
WHERE Name in ('Closing Repair Complete', 'Closing Repair Complete (Non-Photos)')

-- select the changes for review
select *
from utb_bundling_document_type bdt
where BundlingID in (select BundlingID 
                        from utb_bundling
                        where name in ('Closing Repair Complete', 'Closing Repair Complete (Non-Photos)'))
  and bdt.DocumentTypeID = @DocumentTypeID_RentalInvoice


-- Create a new "Rental Invoice" message template
INSERT INTO dbo.utb_message_template (
    EnabledFlag,
    Description,
    ServiceChannelCD,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    1,
    'Rental Invoice|Messages/msgPSRentalInvoice.xsl',
    'PS',
    0,
    @now
)

SET @MessageTemplateID = SCOPE_IDENTITY()

-- Create a new bundling profile for all PS clients to include this rental invoice
select @MessageTemplateID = MessageTemplateID
from utb_message_template
where Description like 'Rental Invoice|%'


INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    44,                 -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Rental Invoice',   -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Rental Invoice"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    9,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to PS clients
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID not in (145, 209) -- Zurich

-- Remember to commit/rollback the changes

-- COMMIT
-- ROLLBACK
