-- Conversion script to convert add completion dates back.

----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION

UPDATE dbo.utb_claim_aspect
   SET OriginalCompleteDate = a.OriginalCompleteDate
FROM dbo.utb_claim_aspect ca
INNER JOIN (
SELECT claimaspectid, max(OriginalCompleteDate) as OriginalCompleteDate FROM dbo.utb_claim_aspect_service_channel
GROUP BY claimaspectid) a ON a.ClaimAspectID = ca.ClaimAspectID

UPDATE dbo.utb_claim_aspect
   SET OriginalCompleteDate = NULL
FROM dbo.utb_claim_aspect ca
INNER JOIN dbo.utb_claim_aspect_service_channel casc
ON ca.ClaimAspectID = casc.ClaimAspectID
WHERE casc.OriginalCompleteDate is null

    
--commit
--rollback
