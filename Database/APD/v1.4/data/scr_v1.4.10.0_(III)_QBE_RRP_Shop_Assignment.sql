/**********************************************************************************
* QBE RRP Assignment form
**********************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @FormID as int
DECLARE @DocumentTypeIDShopAssignment as int
DECLARE @now as datetime

SET @InsuranceCompanyID = 304
Set @now = CURRENT_TIMESTAMP

SELECT @DocumentTypeIDShopAssignment = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Shop Assignment'

IF NOT EXISTS(SELECT FormID
                FROM dbo.utb_form
                WHERE InsuranceCompanyID = @InsuranceCompanyID
                  AND ServiceChannelCD = 'RRP'
                  AND Name = 'Shop Assignment')
BEGIN
    PRINT 'Adding RRP Shop Assignment'
    BEGIN TRANSACTION
    
    -- Add the RRP Shop Assignment Main Page
    INSERT INTO dbo.utb_form (
        DocumentTypeID
        , InsuranceCompanyID
        , EnabledFlag
        , Name
        , PDFPath
        , PertainsToCD
        , ServiceChannelCD
        , SQLProcedure
        , SystemFlag
        , SysLastUserID
        , SysLastUpdatedDate
    )
    SELECT 
        @DocumentTypeIDShopAssignment
        , @InsuranceCompanyID
        , 1
        , 'Shop Assignment'
        , 'RRP_FaxAssignment.pdf'
        , 'S'
        , 'RRP'
        , 'uspWorkflowSendShopAssignXML'
        , 1
        , 0
        , @now
        
    SELECT @FormID = SCOPE_IDENTITY()
    
    -- Add the RRP Shop Instructions
    INSERT INTO dbo.utb_form_supplement (
        FormID
        , EnabledFlag
        , Name
        , PDFPath
        , ServiceChannelCD
        , SQLProcedure
        , SysLastUserID
        , SysLastUpdatedDate
    )
    SELECT 
        @FormID
        , 1
        , 'RRP Shop Instructions'
        , 'RRP_ShopInstructions304.pdf'
        , 'RRP'
        , ''
        , 0
        , @now

    -- Add the RRP Direction to Pay
    INSERT INTO dbo.utb_form_supplement (
        FormID
        , EnabledFlag
        , Name
        , PDFPath
        , ServiceChannelCD
        , SQLProcedure
        , SysLastUserID
        , SysLastUpdatedDate
    )
    SELECT 
        @FormID
        , 1
        , 'RRP Direction to Pay'
        , 'RRP_DTP.pdf'
        , 'RRP'
        , 'uspWorkflowSendShopAssignXML'
        , 0
        , @now

    COMMIT TRANSACTION
    
    PRINT 'RRP Shop Assignment was added.'
END
