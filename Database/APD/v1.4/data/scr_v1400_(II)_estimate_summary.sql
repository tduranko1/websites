BEGIN TRANSACTION

-- Create summary records for Deductibles Applied - base on Deductibles

INSERT INTO dbo.utb_estimate_summary 
SELECT  DocumentID,          
        46, 
        AgreedExtendedAmt,     
        NULL, -- AgreedHrs
        NULL, -- AgreedPct
        0, -- AgreedTaxableFlag
        AgreedUnitAmt,        
        NULL, -- Comments
        OriginalExtendedAmt,  
        NULL, -- OriginalHrs
        NULL, -- OriginalHrsRepair
        NULL, -- OriginalHrsReplace
        NULL, -- OriginalPct
        0, -- OriginalTaxableFlag
        OriginalUnitAmt,      
        NULL,            
        0,        
        CURRENT_TIMESTAMP
FROM    dbo.utb_estimate_summary
WHERE   EstimateSummaryTypeID = 28


-- Create summary records for Limit Effect

INSERT INTO dbo.utb_estimate_summary 
SELECT  DocumentID,          
        47, 
        0, -- AgreedExtendedAmt
        NULL, -- AgreedHrs
        NULL, -- AgreedPct
        0, -- AgreedTaxableFlag
        0, -- AgreedUnitAmt
        NULL, -- Comments  
        0, -- OriginalExtendedAmt
        NULL, -- OriginalHrs
        NULL, -- OriginalHrsRepair
        NULL, -- OriginalHrsReplace
        NULL, -- OriginalPct
        0, -- OriginalTaxableFlag
        0, -- OriginalUnitAmt
        NULL,  -- TaxTypeCD
        0,        
        CURRENT_TIMESTAMP
FROM    dbo.utb_estimate



-- Create summary records for Net Total Effect - base on Net Total

INSERT INTO dbo.utb_estimate_summary 
SELECT  DocumentID,          
        48, 
        AgreedExtendedAmt,     
        NULL, -- AgreedHrs
        NULL, -- AgreedPct
        0, -- AgreedTaxableFlag
        AgreedUnitAmt,        
        NULL, -- Comments
        OriginalExtendedAmt,  
        NULL, -- OriginalHrs
        NULL, -- OriginalHrsRepair
        NULL, -- OriginalHrsReplace
        NULL, -- OriginalPct
        0, -- OriginalTaxableFlag
        OriginalUnitAmt,      
        NULL,            
        0,        
        CURRENT_TIMESTAMP
FROM    dbo.utb_estimate_summary
WHERE   EstimateSummaryTypeID = 32


-- COMMIT
-- ROLLBACK