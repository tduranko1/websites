-- Conversion script to convert 1.3.4.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Populates the new claim_aspect_service_channel table with vehicle aspect records
--        from claim_aspect
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION


PRINT 'Populating new table utb_claim_aspect_service_channel'

INSERT INTO dbo.utb_claim_aspect_service_channel
(
    ClaimAspectID,
    CreatedUserID,
    CreatedDate,
    DispositionTypeCD,
    EnabledFlag,
    InspectionDate,
    ClientInvoiceDate,
    OriginalCompleteDate,
    OriginalEstimateDate,
    PrimaryFlag,
    ServiceChannelCD,
    WorkEndConfirmFlag,
    WorkEndDate,
    WorkStartConfirmFlag,
    WorkStartDate,
    SysLastUserID,
    SysLastUpdatedDate
)
  SELECT  ca.ClaimAspectID,
          ca.CreatedUserID,
          ca.CreatedDate,
          ca.DispositionTypeCD,
          ca.EnabledFlag,
          NULL,
          NULL,
          ca.OriginalCompleteDate,
          NULL,
          1,
          ca.ServiceChannelCD,
          cv.RepairEndConfirmFlag,
          cv.RepairEndDate,
          cv.RepairStartConfirmFlag,
          cv.RepairStartDate,
          ca.SysLastUserID,
          ca.SysLastUpdatedDate
    FROM  dbo.Tmp_utb_claim_aspect ca
    LEFT JOIN dbo.Tmp_utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
    WHERE ca.ClaimAspectTypeID = 9

CREATE TABLE #me_results (
	ClaimAspectID udt_std_id_big NOT NULL,
    CashOutDate udt_std_datetime NULL,
	DispositionCD udt_std_cd NOT NULL,
    ServiceChannelCD udt_std_cd NOT NULL,
	SysLastUserID udt_std_id NOT NULL,
	SysLastUpdatedDate udt_sys_last_updated_date NOT NULL )


 INSERT INTO #me_results
 SELECT ClaimAspectID, 
        CashOutBodyDate,
        DispositionBodyCD,
        'PS',
        SysLastUserID, 
        SysLastUpdatedDate
   FROM dbo.tmp_utb_mobile_electronics_results
  WHERE DispositionBodyCD <> 'NAP'

 INSERT INTO #me_results
 SELECT ClaimAspectID, 
        CashOutGlassDate,
        DispositionGlassCD, 
        'GL',
        SysLastUserID, 
        SysLastUpdatedDate
   FROM dbo.tmp_utb_mobile_electronics_results
  WHERE DispositionGlassCD <> 'NAP'

 INSERT INTO #me_results
 SELECT ClaimAspectID, 
        CashOutMEDate,
        DispositionMECD, 
        'ME',
        SysLastUserID, 
        SysLastUpdatedDate
   FROM dbo.tmp_utb_mobile_electronics_results
  WHERE DispositionMECD <> 'NAP'


INSERT INTO dbo.utb_claim_aspect_service_channel
(
    ClaimAspectID,
    CreatedUserID,
    CashOutDate,
    CreatedDate,
    DispositionTypeCD,
    EnabledFlag,
    InspectionDate,
    ClientInvoiceDate,
    OriginalCompleteDate,
    OriginalEstimateDate,
    PrimaryFlag,
    ServiceChannelCD,
    WorkEndConfirmFlag,
    WorkEndDate,
    WorkStartConfirmFlag,
    WorkStartDate,
    SysLastUserID,
    SysLastUpdatedDate
)
  SELECT  
          ca.ClaimAspectID,
          ca.CreatedUserID,
          me.CashOutDate,
          ca.CreatedDate,
          me.DispositionCD,
          ca.EnabledFlag,
          NULL,
          NULL,
          ca.OriginalCompleteDate,
          NULL,
          0,
          me.ServiceChannelCD,
          0,
          NULL,
          0,
          NULL,
          me.SysLastUserID,
          me.SysLastUpdatedDate
    FROM  dbo.Tmp_utb_claim_aspect ca
    INNER JOIN #me_results me ON (ca.ClaimAspectID = me.ClaimAspectID)
    WHERE ca.ClaimAspectTypeID = 9
      AND ca.ServiceChannelCD <> me.ServiceChannelCD


UPDATE dbo.utb_claim_aspect_service_channel
   SET ServiceChannelCD = at.ServiceChannelDefaultCD
  FROM dbo.utb_claim_aspect_service_channel casc
 INNER JOIN dbo.Tmp_utb_claim_aspect ca
    ON ca.ClaimAspectID = casc.ClaimAspectID
 INNER JOIN dbo.utb_assignment_type at
    ON ca.CurrentAssignmentTypeID = at.AssignmentTypeID
 WHERE casc.ServiceChannelCD IS NULL

UPDATE dbo.utb_claim_aspect_service_channel
   SET ServiceChannelCD = at.ServiceChannelDefaultCD
  FROM dbo.utb_claim_aspect_service_channel casc
 INNER JOIN dbo.Tmp_utb_claim_aspect ca
    ON ca.ClaimAspectID = casc.ClaimAspectID
 INNER JOIN dbo.utb_assignment_type at
    ON ca.CompletedAssignmentTypeID = at.AssignmentTypeID
 WHERE casc.ServiceChannelCD IS NULL

UPDATE dbo.utb_claim_aspect_service_channel
   SET ServiceChannelCD = at.ServiceChannelDefaultCD
  FROM dbo.utb_claim_aspect_service_channel casc
 INNER JOIN dbo.Tmp_utb_claim_aspect ca
    ON ca.ClaimAspectID = casc.ClaimAspectID
 INNER JOIN dbo.utb_assignment_type at
    ON ca.InitialAssignmentTypeID = at.AssignmentTypeID
 WHERE casc.ServiceChannelCD IS NULL

UPDATE dbo.utb_claim_aspect_service_channel
   SET ServiceChannelCD = 'PS'
 WHERE ServiceChannelCD IS NULL

--commit
--rollback


