

DECLARE @InsuranceCompanyID     SMALLINT
DECLARE @MaroID		            SMALLINT
DECLARE @EastWindsorID		    SMALLINT
DECLARE @EroID		            SMALLINT
DECLARE @SeroID		            SMALLINT
DECLARE @timestamp				DATETIME

SET @InsuranceCompanyID = (SELECT InsuranceCompanyID
							FROM utb_insurance
							WHERE name = 'Utica National Insurance Group')

SET @timestamp = current_timestamp

-- eliminated offices
SET @MaroID = (SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID AND ClientOfficeID = 'MARO')
SET @EastWindsorID = (SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID AND ClientOfficeID = 'E WINDSOR')

-- transfer offices
SET @EroID = (SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID AND ClientOfficeID = 'ERO')
SET @SeroID = (SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID AND ClientOfficeID = 'SERO')



-- add new users
EXEC dbo.uspAdmAddAPDFNOLNewUser 
@CSRNo                 = 'laura.harrigan@uticanational.com',
@NameFirst             = 'Laura',
@NameLast              = 'Harrigan',
@EmailAddress          = 'laura.harrigan@uticanational.com',
@OfficeID              = @EroID,
@PhoneAreaCode         = '315',
@PhoneExchangeNumber   = '235',
@PhoneUnitNumber       = '6837',
@ApplicationID     = 2


EXEC dbo.uspAdmAddAPDFNOLNewUser 
@CSRNo                 = 'mark.ribnikar@uticanational.com',
@NameFirst             = 'Mark',
@NameLast              = 'Ribnikar',
@EmailAddress          = 'mark.ribnikar@uticanational.com',
@OfficeID              = @EroID,
@PhoneAreaCode         = '315',
@PhoneExchangeNumber   = '235',
@PhoneUnitNumber       = '2493',
@ApplicationID     = 2


EXEC dbo.uspAdmAddAPDFNOLNewUser 
@CSRNo                 = 'mary.tramacera@uticanational.com',
@NameFirst             = 'Mary',
@NameLast              = 'Tramacera',
@EmailAddress          = 'mary.tramacera@uticanational.com',
@OfficeID              = @EroID,
@PhoneAreaCode         = '315',
@PhoneExchangeNumber   = '235',
@PhoneUnitNumber       = '6791',
@ApplicationID     = 2


EXEC dbo.uspAdmAddAPDFNOLNewUser 
@CSRNo                 = 'christopher.montaperto@uticanational.com',
@NameFirst             = 'Christopher',
@NameLast              = 'Montaperto',
@EmailAddress          = 'christopher.montaperto@uticanational.com',
@OfficeID              = @EroID,
@PhoneAreaCode         = '609',
@PhoneExchangeNumber   = '308',
@PhoneUnitNumber       = '4503',
@ApplicationID     = 2


EXEC dbo.uspAdmAddAPDFNOLNewUser 
@CSRNo                 = 'william.smith@uticanational.com',
@NameFirst             = 'William',
@NameLast              = 'Smith',
@EmailAddress          = 'william.smith@uticanational.com',
@OfficeID              = @EroID,
@PhoneAreaCode         = '609',
@PhoneExchangeNumber   = '308',
@PhoneUnitNumber       = '4519',
@ApplicationID     = 2


-- transfer users
update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'linda.angrisano@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'marie.bussonnais@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'genevieve.cahill@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'paul.campbell@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'dan.d' + Char(39) + 'angelo@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'michael.dubiel@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'nikkole.ferrone@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'joseph.gallagher@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'james.hulser@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'michelle.inman@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'melissa.jweid@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'patricia.karuzas@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'rebecca.laymon@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'kathy.marek@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'vicki.marston@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'kathi.merrell@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'liz.mikoda@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'lynne.morinitti@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'carol.murphy@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'tricia.murray@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'robert.nelson@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'deborah.penc@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'vernadine.reid@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'marissa.rivera@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'robin.schiebler@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'patricia.schug@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'diane.servello@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'adam.spooner@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'teresa.steele@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'gregory.von-matt@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'lisa.watkins@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'sean.mclaughlin@uticanational.com'

update dbo.utb_user 
 set OfficeID = @EroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'joseph.caltiere@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'kristin.bonner@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'rachael.campese@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'beatrice.cherry@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'terri.chester@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'matthew.combest@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'michael.felten@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'cassandra.forbes@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'gizelle.grable@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'gail.graves@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'lauren.gross@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'deana.henry@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'dionne.howard@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'steven.hulbert@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'albert.ingram@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'george.mahon@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'silvia.martinez@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'mark.mcgiboney@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'kristin.owens@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'salena.staub@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'steve.sobolik@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'joseph.suarez@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'martin.townes@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'tory.tucker@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'bill.swertfeger@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'jennifer.tomlin@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'nina.lavenia@uticanational.com'

update dbo.utb_user 
 set OfficeID = @SeroID,
 SysLastUserID = 0,
 SysLastUpdatedDate = @timestamp
 where EmailAddress = 'scott.rose@uticanational.com'





-- Deactivate user Gary.DiCicco@UticaNational.com 
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Gary.DiCicco@UticaNational.com '

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Gary.DiCicco@UticaNational.com '

-- Deactivate user tina.amiss@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'tina.amiss@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'tina.amiss@uticanational.com'

-- Deactivate user sheila.hicks@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'sheila.hicks@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'sheila.hicks@uticanational.com'

-- Deactivate user brenda.jackson@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'brenda.jackson@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'brenda.jackson@uticanational.com'

-- Deactivate user linda.walls@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'linda.walls@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'linda.walls@uticanational.com'



-- put all names into proper case and email addresses into lower case for Utica
UPDATE utb_user
SET EmailAddress = Lower(EmailAddress),
	NameFirst = dbo.ufnProperCase(NameFirst),
	NameLast = dbo.ufnProperCase(NameLast)
WHERE OfficeID in
	(SELECT OfficeID 
	 FROM utb_office 
	 WHERE InsuranceCompanyID = @InsuranceCompanyID)


--disable the MARO and East Windsor Offices
UPDATE utb_office
SET EnabledFlag = 0
WHERE OfficeID in (@MaroID, @EastWindsorID)

-- update user access to ClaimPoint APD and/or APD
UPDATE utb_user_application
SET applicationID = 2
WHERE userID in 
	(SELECT UserID
	 FROM utb_user_application
	 WHERE userID in
		(SELECT userID
		 FROM utb_user
		 WHERE OfficeID in (@MaroID, @EastWindsorID, @EroID, @SeroID)
		 AND EnabledFlag = 1))
AND userID NOT IN
	(SELECT UserID
	 FROM utb_user_application
	 WHERE applicationID = 2
	 AND userID in
		(SELECT userID
		 FROM utb_user
		 WHERE OfficeID in (@MaroID, @EastWindsorID, @EroID, @SeroID)
		 AND EnabledFlag = 1))

print 'Done.'

