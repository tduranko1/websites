/******************************************************************************
* SWR 218804 - Add BCIF Custom Form
* Author:	   Ramesh Vishegu
* Date:		   12/12/2008
******************************************************************************/

DECLARE @now as datetime
DECLARE @DocumentTypeID as int
DECLARE @DisplayOrder as int

SET @now = CURRENT_TIMESTAMP 

BEGIN TRANSACTION

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'BCIF Request'

IF @DocumentTypeID IS NULL
BEGIN
    PRINT 'BCIF Request document type not found.'
    RETURN
END

-- Add BCIF as a custom form
IF NOT EXISTS(SELECT FormID FROM utb_form WHERE Name = 'BCIF Request')
BEGIN
	INSERT INTO utb_form (
		DocumentTypeID,
		AutoBundlingFlag,
		DataMiningFlag,
		EnabledFlag,
		HTMLPath,
		Name,
		PDFPath,
		PertainsToCD,
		ServiceChannelCD,
		SQLProcedure,
		SystemFlag,
		SysLastUserID,
		SysLastUpdatedDate
	) VALUES (
		@DocumentTypeID,    -- DocumentTypeID,
		1,                  -- AutoBundlingFlag,
		0,                  -- DataMiningFlag,
		1,                  -- EnabledFlag,
		'Forms/BCIF.htm',	-- HTMLPath,
		'BCIF Request',     -- Name,
		'BCIF.pdf',         -- PDFPath,
		'S',                -- PertainsToCD,
		'PS',               -- ServiceChannelCD,
		'uspCFBCIFGetXML',  -- SQLProcedure,
		0,                  -- SystemFlag,
		0,                  -- SysLastUserID,
		@now                -- SysLastUpdatedDate
	)
END

select * from utb_form where  name like 'BCIF Request'

-- commit
-- rollback