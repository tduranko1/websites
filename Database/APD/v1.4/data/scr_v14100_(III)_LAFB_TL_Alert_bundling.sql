/******************************************************************************
* LAFB Total Loss alert bundling profile
*   Send via Email to JSmith@sfbcic.com; SEmfinger@sfbcic.com; 
*      [claim file adjuster]@sfbcic.com
*   Select photos and the latest estimate.
*   Custom message template (to Physical Damage Manager)
******************************************************************************/

declare @BundlingID as int
declare @MessageTemplateID as int
declare @InsuranceCompanyID as int

select @InsuranceCompanyID = InsuranceCompanyID
from utb_insurance 
where Name = 'Louisiana Farm Bureau'

IF @InsuranceCompanyID IS NOT NULL
BEGIN
   /*select @MessageTemplateID = max(MessageTemplateID) + 1
   from utb_message_template*/

   BEGIN TRANSACTION
   
   -- insert a message
   INSERT INTO utb_message_template (
      --MessageTemplateID,
      AppliesToCD,
      EnabledFlag,
      Description,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      --@MessageTemplateID,
      'C',
      1,
      'Total Loss Alert to Physical Damage Manager|Messages/msgPSTLAlertToPDM.xsl',
      'PS',
      0,
      current_timestamp
   )
   
   SET @MessageTemplateID = SCOPE_IDENTITY()
   
   INSERT INTO utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      38, 
      @MessageTemplateID,
      1,
      'Total Loss Alert',
      0,
      current_timestamp
   )
   
   SET @BundlingID = SCOPE_IDENTITY()



   -- Add Estimate
   INSERT INTO utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      MandatoryFlag,
      SelectionOrder,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) values (
      @BundlingID,
      3,
      'I',
      0,
      0,
      0,
      'O',
      0,
      1,
      1,
      1,
      0,
      current_timestamp
   )

   -- Add photos
   INSERT INTO utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      MandatoryFlag,
      SelectionOrder,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) values (
      @BundlingID,
      8,
      'I',
      0,
      1,
      0,
      NULL,
      0,
      0,
      2,
      0,
      0,
      current_timestamp
   )
   
   INSERT INTO utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,
      @InsuranceCompanyID,
      'PDF',
      'EML',
      'jsmith@sfbcic.com; semfinger@sfbcic.com; $ADJUSTER$',
      'PS',
      0,
      CURRENT_TIMESTAMP
   )
   
   select * from utb_client_bundling where InsuranceCompanyID = @InsuranceCompanyID
   select * from utb_bundling where bundlingID = @BundlingID
   select * from utb_bundling_document_type where BundlingID = @BundlingID

END

-- commit
-- rollback

