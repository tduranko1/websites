/**********************************************************************************
* Westfield BCIF Request 
**********************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @FormID as int
DECLARE @DocumentTypeIDShopAssignment as int
DECLARE @now as datetime

SET @InsuranceCompanyID = 387
Set @now = CURRENT_TIMESTAMP

SELECT @DocumentTypeIDShopAssignment = DocumentTypeID
FROM utb_document_type
WHERE Name = 'BCIF Request'

IF NOT EXISTS(SELECT FormID
                FROM dbo.utb_form
                WHERE InsuranceCompanyID = @InsuranceCompanyID
                  AND ServiceChannelCD = 'PS'
                  AND Name = 'Westfield BCIF Request ')
BEGIN
    PRINT 'Adding Westfield BCIF Request'
    BEGIN TRANSACTION
    
    -- Add the DA Memo Bill
    INSERT INTO dbo.utb_form (
        DocumentTypeID
        , InsuranceCompanyID
        , AutoBundlingFlag
        , DataMiningFlag
        , EnabledFlag
        , HTMLPath
        , Name
        , PDFPath
        , PertainsToCD
        , ServiceChannelCD
        , SQLProcedure
        , SystemFlag
        , SysLastUserID
        , SysLastUpdatedDate
    )
    SELECT 
        @DocumentTypeIDShopAssignment
        , @InsuranceCompanyID
        , 1
        , 0
        , 1
        , 'Forms/WF_BCIF.asp'
        , 'Westfield BCIF Request'
        , 'Westfield_BCIF.pdf'
        , 'S'
        , 'PS'
        , 'uspCFBCIFGetXML'
        , 0
        , 0
        , @now
        
    SELECT @FormID = SCOPE_IDENTITY()

    COMMIT TRANSACTION
    
    PRINT 'Westfield BCIF Request was added.'
END
