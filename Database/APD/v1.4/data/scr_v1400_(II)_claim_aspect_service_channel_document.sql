-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Copies claim_aspect_document, changing reference key to ClaimAspectServiceChannel
--

SET NOCOUNT ON

PRINT 'Converting utb_claim_aspect_document to utb_claim_aspect_service_channel_document'

if (select count(*) from tempdb..sysobjects where name like '#t1%') =1
	begin
		drop table #t1
	end

if (select count(*) from tempdb..sysobjects where name like '#t2%') =1
	begin
		drop table #t2
	end
	

-- Insert the documents that can be associated to the primary Service Channel

select          casc.ClaimAspectID,
                casc.ClaimAspectServiceChannelID,
                d.DocumentID,
                d.SysLastUserID,
                d.SysLastUpdatedDate into     #t1

from            utb_Claim_Aspect_Type  cat

Inner join      utb_Claim_Aspect ca
on              ca.ClaimAspectTypeID = cat.ClaimAspectTypeID

inner join      utb_Claim_Aspect_Service_Channel casc
on              ca.ClaimAspectID = casc.ClaimAspectID

Inner join      tmp_utb_Claim_Aspect_Document d
on              d.ClaimAspectID = ca.ClaimAspectID


Where          cat.Name = 'Vehicle'
and            casc.PrimaryFlag = 1

  
--The remainder of the documents cannot be associated to the service channels.
select          distinct casc.ClaimAspectID,
                casc.ClaimAspectServiceChannelID,
                t1.DocumentID,
                t1.SysLastUserID,
                t1.SysLastUpdatedDate into #t2
 
from            (
                select            d.ClaimAspectID,
                                  d.DocumentID,
                                  d.SysLastUserID,
                                  d.SysLastUpdatedDate
                from              #t1 t
                right outer join  tmp_utb_Claim_Aspect_document d
                on                d.documentid = t.Documentid
                where             t.documentid is null
                ) t1
inner join    utb_Claim_Aspect ca
on            ca.ClaimAspectID = t1.ClaimAspectID

Inner join    utb_Claim_Aspect ca1
on            ca1.LynxID = ca.LynxID


inner join    (
               Select            LynxID,
                                 Min(ClaimAspectNumber) as 'ClaimAspectNumber'
               From              utb_Claim_Aspect
               where             ClaimAspectTypeID = 9
               Group by          LynxID
              ) vt
on            vt.lynxid = ca1.lynxid
and           vt.ClaimAspectNumber = ca1.ClaimAspectNumber

inner join    utb_Claim_Aspect_Service_Channel casc
on            ca1.ClaimAspectID = casc.ClaimAspectID


where         casc.PrimaryFlag = 1

-- Now load both sets of documents into the real table

IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_claim_aspect_service_channel_document') AND name='uix_ie_claim_aspect_service_channel_document_documentid')
BEGIN
    DROP INDEX dbo.utb_claim_aspect_service_channel_document.uix_ie_claim_aspect_service_channel_document_documentid
END

GO

IF EXISTS (SELECT * FROM sysindexes WHERE id=OBJECT_ID('dbo.utb_claim_aspect_service_channel_document') AND name='uix_ie_claim_aspect_service_channel_document_claimaspectservicechannelid')
BEGIN
    DROP INDEX dbo.utb_claim_aspect_service_channel_document.uix_ie_claim_aspect_service_channel_document_claimaspectservicechannelid
END

GO

INSERT INTO dbo.utb_claim_aspect_service_channel_document
  SELECT  ClaimAspectServiceChannelID,
          DocumentID,
          SysLastUserID,
          SysLastUpdatedDate
    FROM  #t1

  UNION ALL

  SELECT  ClaimAspectServiceChannelID,
          DocumentID,
          SysLastUserID,
          SysLastUpdatedDate
    FROM  #t2

GO

CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_document_documentid
    ON dbo.utb_claim_aspect_service_channel_document(DocumentID)
  WITH FILLFACTOR = 90
    ON ufg_claim_index

GO

CREATE NONCLUSTERED INDEX uix_ie_claim_aspect_service_channel_document_claimaspectservicechannelid
    ON dbo.utb_claim_aspect_service_channel_document(ClaimAspectServiceChannelID)
  WITH FILLFACTOR = 90
    ON ufg_claim_index

GO

if (select count(*) from tempdb..sysobjects where name like '#t1%') =1
	begin
		drop table #t1
	end

if (select count(*) from tempdb..sysobjects where name like '#t2%') =1
	begin
		drop table #t2
	end
	


/****************************************************************************/


