/****** Object:  Table [dbo].[utb_log]    Script Date: 12/03/2012 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[utb_partner_event_log](
	[EventTransactionID] [varchar](100) NOT NULL,
	[EventType] [varchar](50) NOT NULL,
	[EventStatus] [varchar](50) NOT NULL,
	[EventDescription] [varchar](100) NOT NULL,
	[EventDetailedDescription] [varchar](4000) NULL,
	[EventXML] [text] NULL,
	[SysLastUserID] [int] NULL,
	[SysLastUpdatedDate] [datetime] NULL
) 

GO

SET ANSI_PADDING ON
GO
