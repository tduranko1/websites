/******************************************************************************
* Metlife Bundling profiles
******************************************************************************/

DECLARE @Now as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int
DECLARE @ClientBundlingID as int

SET @NOW = CURRENT_TIMESTAMP
SET @InsuranceCompanyID = 176

BEGIN TRANSACTION

--------------------------------------
-- Acknowledgement of Claim
--------------------------------------
SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Acknowledgement of Claim'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'importrequest@metlife.com, $ADJUSTER$'
WHERE ClientBundlingID = @ClientBundlingID

--------------------------------------
-- Scheduled Inspection Status Update
--------------------------------------
SET @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Scheduled Inspection Status Update'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'Choice1@metlife.com, $ADJUSTER$'
WHERE ClientBundlingID = @ClientBundlingID


--------------------------------------
-- Escalation Request
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Escalation Request|Messages/msgPursuitAuditEscalationRequest.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Escalation Request', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'Choice1@metlife.com; $ADJUSTER$',
   0,
   @now


--------------------------------------
-- Shop Agreed Price Contact Advice
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Shop Agreed Price Contact Advice|Messages/msgPursuitAuditShopAgreedPriceContactAdvice.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'SHOP: Shop Agreed Price Contact Advice', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   '$SHOPFAXOREMAIL$',
   0,
   @now


--------------------------------------
-- Closing - AGREED Audit Complete
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing - Agreed Audit Complete|Messages/msgPursuitAuditClosingAgreedAuditComplete.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing - Agreed Audit Complete', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    33,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'metlifeautoestimatespayment@metlife.com; importrequest@metlife.com; $ADJUSTER$',
   0,
   @now




--------------------------------------
-- Closing - NON-AGREED Audit Complete
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing - Non Agreed Audit Complete|Messages/msgPursuitAuditClosingNotAgreedAuditComplete.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing - Non Agreed Audit Complete', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    33,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'mds@metlife.com; $ADJUSTER$',
   0,
   @now


COMMIT TRANSACTION
