DECLARE @DocumentTypeID_EstimateEMS as int
DECLARE @BundlingId as int
DECLARE @now as datetime

SELECT @DocumentTypeID_EstimateEMS = DocumentTypeID
  from utb_document_type
 where Name = 'Estimate EMS'
 
set @now = current_timestamp
 
IF @DocumentTypeID_EstimateEMS > 0
BEGIN
    IF NOT EXISTS (select * from utb_client_bundling cb
               inner join utb_bundling b on cb.BundlingID = b.BundlingID
               inner join dbo.utb_bundling_document_type bdt on b.BundlingID = bdt.BundlingID
                    where InsuranceCompanyId = 304
                      and servicechannelcd = 'RRP'
                      and b.Name = 'RRP - Closing Inspection Complete'
                      and bdt.DocumentTypeID = @DocumentTypeID_EstimateEMS)
    BEGIN
        SELECT @BundlingID = b.BundlingID
        from utb_client_bundling cb
        inner join utb_bundling b on cb.BundlingID = b.BundlingID
        where InsuranceCompanyId = 304
          and servicechannelcd = 'RRP'
          and b.Name = 'RRP - Closing Inspection Complete'
               
        INSERT INTO utb_bundling_document_type (
            BundlingID,
            DocumentTypeID,
            DirectionalCD,
            DirectionToPayFlag,
            DuplicateFlag,
            EstimateDuplicateFlag,
            EstimateTypeCD,
            FinalEstimateFlag,
            MandatoryFlag,
            SelectionOrder,
            VANFlag,
            SysLastUserID,
            SysLastUpdatedDate
        ) VALUES (
            @BundlingID,    -- BundlingID,
            @DocumentTypeID_EstimateEMS,              -- DocumentTypeID,
            'I',            -- DirectionalCD,
            0,              -- DirectionToPayFlag,
            0,              -- DuplicateFlag,
            0,              -- EstimateDuplicateFlag,
            NULL,            -- EstimateTypeCD,
            0,              -- FinalEstimateFlag,
            1,              -- MandatoryFlag,
            5,              -- SelectionOrder,
            0,              -- VANFlag,
            0,              -- SysLastUserID,
            @now            -- SysLastUpdatedDate
        )

    END


    IF NOT EXISTS (select * from utb_client_bundling cb
               inner join utb_bundling b on cb.BundlingID = b.BundlingID
               inner join dbo.utb_bundling_document_type bdt on b.BundlingID = bdt.BundlingID
                    where InsuranceCompanyId = 304
                      and servicechannelcd = 'RRP'
                      and b.Name = 'RRP - Closing Supplement'
                      and bdt.DocumentTypeID = @DocumentTypeID_EstimateEMS)
    BEGIN
        SELECT @BundlingID = b.BundlingID
        from utb_client_bundling cb
        inner join utb_bundling b on cb.BundlingID = b.BundlingID
        where InsuranceCompanyId = 304
          and servicechannelcd = 'RRP'
          and b.Name = 'RRP - Closing Supplement'
               
        INSERT INTO utb_bundling_document_type (
            BundlingID,
            DocumentTypeID,
            DirectionalCD,
            DirectionToPayFlag,
            DuplicateFlag,
            EstimateDuplicateFlag,
            EstimateTypeCD,
            FinalEstimateFlag,
            MandatoryFlag,
            SelectionOrder,
            VANFlag,
            SysLastUserID,
            SysLastUpdatedDate
        ) VALUES (
            @BundlingID,    -- BundlingID,
            @DocumentTypeID_EstimateEMS,              -- DocumentTypeID,
            'I',            -- DirectionalCD,
            0,              -- DirectionToPayFlag,
            0,              -- DuplicateFlag,
            0,              -- EstimateDuplicateFlag,
            NULL,            -- EstimateTypeCD,
            0,              -- FinalEstimateFlag,
            1,              -- MandatoryFlag,
            5,              -- SelectionOrder,
            0,              -- VANFlag,
            0,              -- SysLastUserID,
            @now            -- SysLastUpdatedDate
        )

    END
    
    IF NOT EXISTS (select * from utb_client_bundling cb
               inner join utb_bundling b on cb.BundlingID = b.BundlingID
               inner join dbo.utb_bundling_document_type bdt on b.BundlingID = bdt.BundlingID
                    where InsuranceCompanyId = 304
          and servicechannelcd = 'RRP'
          and b.Name = 'RRP - Closing Total Loss'
                      and bdt.DocumentTypeID = @DocumentTypeID_EstimateEMS)
    BEGIN
        SELECT @BundlingID = b.BundlingID
        from utb_client_bundling cb
        inner join utb_bundling b on cb.BundlingID = b.BundlingID
        where InsuranceCompanyId = 304
          and servicechannelcd = 'RRP'
          and b.Name = 'RRP - Closing Total Loss'
               
        INSERT INTO utb_bundling_document_type (
            BundlingID,
            DocumentTypeID,
            DirectionalCD,
            DirectionToPayFlag,
            DuplicateFlag,
            EstimateDuplicateFlag,
            EstimateTypeCD,
            FinalEstimateFlag,
            MandatoryFlag,
            SelectionOrder,
            VANFlag,
            SysLastUserID,
            SysLastUpdatedDate
        ) VALUES (
            @BundlingID,    -- BundlingID,
            @DocumentTypeID_EstimateEMS,              -- DocumentTypeID,
            'I',            -- DirectionalCD,
            0,              -- DirectionToPayFlag,
            0,              -- DuplicateFlag,
            0,              -- EstimateDuplicateFlag,
            NULL,            -- EstimateTypeCD,
            0,              -- FinalEstimateFlag,
            1,              -- MandatoryFlag,
            5,              -- SelectionOrder,
            0,              -- VANFlag,
            0,              -- SysLastUserID,
            @now            -- SysLastUpdatedDate
        )

    END
    
    
    IF NOT EXISTS (select * from utb_client_bundling cb
               inner join utb_bundling b on cb.BundlingID = b.BundlingID
               inner join dbo.utb_bundling_document_type bdt on b.BundlingID = bdt.BundlingID
                    where InsuranceCompanyId = 304
          and servicechannelcd = 'RRP'
          and b.Name = 'RRP - No Inspection Alert & Close'
                      and bdt.DocumentTypeID = @DocumentTypeID_EstimateEMS)
    BEGIN
        SELECT @BundlingID = b.BundlingID
        from utb_client_bundling cb
        inner join utb_bundling b on cb.BundlingID = b.BundlingID
        where InsuranceCompanyId = 304
          and servicechannelcd = 'RRP'
          and b.Name = 'RRP - No Inspection Alert & Close'
               
        INSERT INTO utb_bundling_document_type (
            BundlingID,
            DocumentTypeID,
            DirectionalCD,
            DirectionToPayFlag,
            DuplicateFlag,
            EstimateDuplicateFlag,
            EstimateTypeCD,
            FinalEstimateFlag,
            MandatoryFlag,
            SelectionOrder,
            VANFlag,
            SysLastUserID,
            SysLastUpdatedDate
        ) VALUES (
            @BundlingID,    -- BundlingID,
            @DocumentTypeID_EstimateEMS,              -- DocumentTypeID,
            'I',            -- DirectionalCD,
            0,              -- DirectionToPayFlag,
            0,              -- DuplicateFlag,
            0,              -- EstimateDuplicateFlag,
            NULL,            -- EstimateTypeCD,
            0,              -- FinalEstimateFlag,
            1,              -- MandatoryFlag,
            5,              -- SelectionOrder,
            0,              -- VANFlag,
            0,              -- SysLastUserID,
            @now            -- SysLastUpdatedDate
        )

    END
END
 