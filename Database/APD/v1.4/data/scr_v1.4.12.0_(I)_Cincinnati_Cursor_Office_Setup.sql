--SELECT * FROM utb_office WHERE insurancecompanyid = 194 and syslastupdateddate > '2014-12-10'
--DELETE FROM utb_office WHERE insurancecompanyid = 194 and syslastupdateddate > '2014-12-10'
--SELECT * from  dbo.utb_office_assignment_type WHERE syslastupdateddate > '2014-12-10'
--DELETE from  dbo.utb_office_assignment_type WHERE syslastupdateddate > '2014-12-10'
--SELECT * from  dbo.utb_office_contract_state WHERE syslastupdateddate > '2014-12-10'
--DELETE from  dbo.utb_office_contract_state WHERE syslastupdateddate > '2014-12-10'

IF NOT EXISTS (SELECT * FROM utb_office WHERE insurancecompanyid = 194 AND Address1 = 'PO BOX 350' )
BEGIN

DECLARE db_cursor CURSOR FOR 
	SELECT 
		OfficeName
		, OfficeNameDes
		, MailingAddress
		, MailingCity
		, MailingState
		, SUBSTRING(MailZip,1,5)
		, BusinessPhone
	FROM 
		CincinnatiOfficeList
	WHERE
		LEN(OfficeName) > 0

DECLARE @OfficeName VARCHAR(150)
DECLARE @OfficeDesignation VARCHAR(150)
DECLARE @OfficeStreetAddress1 VARCHAR(150)
DECLARE @OfficeCity VARCHAR(150)
DECLARE @OfficeState VARCHAR(150)
DECLARE @OfficeZip VARCHAR(150)
DECLARE @OfficePhone VARCHAR(150)

DECLARE @InsuranceCompanyID        smallint
DECLARE @OfficeID                  smallint
DECLARE @ModifiedDateTime          DATETIME

SET @ModifiedDateTime = Current_Timestamp
SET @InsuranceCompanyID             =  194

OPEN db_cursor  
FETCH NEXT FROM db_cursor INTO 
	@OfficeName
	, @OfficeDesignation
	, @OfficeStreetAddress1
	, @OfficeCity
	, @OfficeState
	, @OfficeZip
	, @OfficePhone
	
WHILE @@FETCH_STATUS = 0  
BEGIN  
      INSERT INTO dbo.utb_office
                  (
                  InsuranceCompanyID, 
                   Address1,
                  Address2, 
                   AddressCity, 
                   AddressState, 
                   AddressZip,
                  CCEmailAddress,
                  ClaimNumberFormatJS,
                  ClaimNumberValidJS,
                  ClaimNumberMsgText,
                  ClientOfficeId,
                  EnabledFlag, 
                   FaxAreaCode,
                  FaxExchangeNumber,
                  FaxUnitNumber,
                  MailingAddress1,
                  MailingAddress2, 
                   MailingAddressCity, 
                   MailingAddressState, 
                   MailingAddressZip,
                  Name, 
                   PhoneAreaCode, 
                   PhoneExchangeNumber, 
                   PhoneUnitNumber,
                  ReturnDocEmailAddress,
                  ReturnDocFaxAreaCode,
                  ReturnDocFaxExchangeNumber,
                  ReturnDocFaxUnitNumber, 
                   SysLastUserID,
                  SysLastUpdatedDate 
                  )
                  VALUES( @InsuranceCompanyID,           -- InsuranceCompanyID
                              @OfficeStreetAddress1 ,    -- Address1 *
                              NULL,						 -- Address2 *
                              @OfficeCity,               -- AddressCity *
                              @OfficeState,              -- AddressState *
                              @OfficeZip,                -- AddressZip *
                              NULL,                      -- CCEmailAddress
                              NULL,                      -- ClaimNumberFormatJS
                              NULL,                      -- ClaimNumberValidJS
                              NULL,                      -- ClaimNumberMsgText
                              @OfficeDesignation,        -- ClientOfficeId * 
                              1,                         -- EnabledFlag
                              NULL,						 -- FaxAreaCode *
                              NULL,						 -- FaxExchangeNumber *
                              NULL,						 -- FaxUnitNumber *
                              @OfficeStreetAddress1 ,    -- MailingAddress1 *
                              NULL,						 -- MailingAddress2 *
                              @OfficeCity,               -- MailingAddressCity *
                              @OfficeState,              -- MailingAddressState *
                              @OfficeZip,                -- MailingAddressZip *
                              @OfficeName ,              -- Name *
                              SUBSTRING(@OfficePhone,1,3),    -- PhoneAreaCode *
                              SUBSTRING(@OfficePhone,5,3),    -- PhoneExchangeNumber *
                              SUBSTRING(@OfficePhone,9,4),    -- PhoneUnitNumber *
                              NULL,                   -- ReturnDocEmailAddress
                              NULL,                   -- ReturnDocFaxAreaCode
                              NULL,                   -- ReturnDocFaxExchangeNumber
                              NULL,                   -- ReturnDocFaxUnitNumber
                              0,                      -- SysLastUserID
                              @ModifiedDateTime       -- SysLastUpdatedDate
                  )


      SELECT @OfficeID = SCOPE_IDENTITY()


      -- Office level configurations - this must match the Client assignment types

      INSERT INTO dbo.utb_office_assignment_type 
        SELECT  @OfficeID, 
                    AssignmentTypeID, 
                    0,
                    @ModifiedDateTime
            FROM  dbo.utb_assignment_type
            WHERE Name in ( 'Program Shop Assignment')


      INSERT INTO dbo.utb_office_contract_state
        SELECT  @OfficeID,
                    StateCode,
                    0, -- UseCEIShopsFlag
                    0, -- SysLastUserID
                    @ModifiedDateTime
            FROM  dbo.utb_state_code
            WHERE EnabledFlag = 1

	FETCH NEXT FROM db_cursor INTO 	
	@OfficeName
	, @OfficeDesignation
	, @OfficeStreetAddress1
	, @OfficeCity
	, @OfficeState
	, @OfficeZip
	, @OfficePhone
END
CLOSE db_cursor  
DEALLOCATE db_cursor 

END	

	  

