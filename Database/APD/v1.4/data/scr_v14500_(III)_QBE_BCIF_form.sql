/********************************************************************
* QBE Specific BCIF Form and Supplement
********************************************************************/
DECLARE @DocumentTypeID as int
DECLARE @DisplayOrder as int
DECLARE @now as datetime
DECLARE @FormID as int
DECLARE @StateCode as varchar(2)

SET @now = CURRENT_TIMESTAMP

SELECT @DocumentTypeID = MAX(DocumentTypeID) + 1
FROM utb_document_type

SELECT @DisplayOrder = MAX(DisplayOrder) + 1
FROM utb_document_type

/*
-- Insert QBE BCIF document type
IF NOT EXISTS
	(SELECT 1 FROM utb_document_type 
	 WHERE name = 'QBE BCIF Request') 
BEGIN
	INSERT INTO utb_document_type (
		DocumentTypeID,
		DisplayOrder,
		DocumentClassCD,
		EnabledFlag,
		EstimateTypeFlag,
		Name,
		SysMaintainedFlag,
		SysLastUserID,
		SysLastUpdatedDate
	) VALUES (
		@DocumentTypeID,
		@DisplayOrder,
		'C',
		1,
		0,
		'QBE BCIF Request',
		1,
		0,
		@now
	)

	IF @@error <> 0
	BEGIN
		-- SQL Server Error
		RAISERROR  ('Error inserting record into utb_document_type', 16, 1)
		ROLLBACK TRANSACTION 
		RETURN
	END
END 
*/

IF NOT EXISTS
	(SELECT 1 FROM utb_form 
	 WHERE name = 'QBE BCIF Request') 
BEGIN
	INSERT INTO utb_form(
		BundlingTaskID,
		DocumentTypeID,
		EventID,
		InsuranceCompanyID,
		LossStateCode,
		ShopStateCode,
		AutoBundlingFlag,
		DataMiningFlag,
		EnabledFlag,
		HTMLPath,
		Name,
		PDFPath,
		PertainsToCD,
		ServiceChannelCD,
		SQLProcedure,
		SystemFlag,
		SysLastUserID,
		SysLastUpdatedDate
	) values(
		NULL,
		40,
		NULL,
		'304',
		NULL,
		NULL,
		1,		-- auto bundling flag
		0,		-- data mining flag
		1,
		'Forms/QBE_BCIF.htm',		-- html path
		'QBE BCIF Request',			-- name
		'QBE_BCIF.pdf',				-- pdf path
		'S',						-- pertains to cd
		'PS',
		'uspCFBCIFGetXML',
		0,
		0,
		@now
	)
END

SELECT @FormID = SCOPE_IDENTITY()

IF @@error <> 0
BEGIN
    -- SQL Server Error

    RAISERROR  ('Error inserting record into utb_form', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END


-- Insert supplement for QBE northern states (AK, AZ, CO, IA, ID, KS, MN, MT, ND, NE, NV, OR, SD, UT, WA, WY)
--	 - fast_forward b/c this is a forward, read only cursor
DECLARE StateCursor CURSOR FAST_FORWARD FOR
	SELECT StateCode
	FROM utb_state_code
	WHERE StateCode in ('AK', 'AZ', 'CO', 'IA', 'ID', 'KS', 'MN', 'MT', 'ND', 'NE', 'NV', 'OR', 'SD', 'UT', 'WA', 'WY')

OPEN StateCursor

FETCH NEXT FROM StateCursor INTO @StateCode

WHILE @@FETCH_STATUS = 0
BEGIN

	IF NOT EXISTS
		(SELECT 1 FROM utb_form_supplement 
		 WHERE name = 'QBE Northern States Supplement'
		 AND ShopStateCode = @StateCode) 
	BEGIN
		INSERT INTO utb_form_supplement (
			FormID,
			LossStateCode,
			ShopStateCode,
			EnabledFlag,
			HTMLPath,
			Name,
			PDFPath,
			ServiceChannelCD,
			SQLProcedure,
			SysLastUserID,
			SysLastUpdatedDate
		) VALUES(
			@FormID,
			NULL,
			@StateCode,
			1,
			'Forms/QBENorthernSupplement.htm',
			'QBE Northern States Supplement',
			'QBENorthernSupplement.pdf',
			'PS',
			'',
			0,
			@now
		)
	END

	IF @@error <> 0
	BEGIN
		-- SQL Server Error
		RAISERROR  ('Error inserting record into utb_form', 16, 1)
		ROLLBACK TRANSACTION 
		RETURN
	END

	FETCH NEXT FROM StateCursor INTO @StateCode
END

CLOSE StateCursor
DEALLOCATE StateCursor


UPDATE utb_form_supplement
SET HTMLPath = 'Forms/QBENorthernSupplement.htm',
	SysLastUserID = 0,
	SysLastUpdatedDate = @now
WHERE name = 'QBE Northern States Supplement'

GO


-- now select the table affected
--SELECT * FROM utb_document_type
--WHERE name like '%QBE%'

SELECT * FROM utb_form
WHERE name like '%QBE%'

SELECT * FROM utb_form_supplement
WHERE name like '%QBE%'
