DECLARE @iInscCompanyID INT
SET @iInscCompanyID = 184 -- Texas Farm

----------------------------------------------
-- Delete New Event for Invoice Close
----------------------------------------------
IF EXISTS (SELECT * FROM utb_event WHERE EventID = 107)
BEGIN
	DELETE FROM utb_event WHERE EventID = 107	
	SELECT 'Event Transaction Review Complete deleted successfully'
END
ELSE
BEGIN
	-- Service Channel already deleted
	SELECT 'Status Event Transaction Review Complete already deleted'
END

----------------------------------------------
-- Delete New Event for Comments
----------------------------------------------
IF EXISTS (SELECT * FROM utb_event WHERE EventID = 106)
BEGIN
	DELETE FROM utb_event WHERE EventID = 106	
	SELECT 'Event Comment Deleted successfully'
END
ELSE
BEGIN
	-- Service Channel already deleted
	SELECT 'Status Event Comment already deleted'
END

----------------------------------------------
-- Delete the CS service channel to Status 
----------------------------------------------
IF EXISTS (SELECT * FROM utb_status WHERE StatusID = 800 AND ServiceChannelCD = 'CS')
BEGIN
	DELETE FROM utb_status WHERE StatusID IN (800,897,898,899) AND ServiceChannelCD = 'CS'	
	SELECT 'Status CS deleted successfully'
END
ELSE
BEGIN
	-- Service Channel already deleted
	SELECT 'Status CS already deleted'
END

----------------------------------------------
-- Delete the CS service channel to Insc Company
----------------------------------------------
IF EXISTS (SELECT * FROM utb_client_service_channel WHERE InsuranceCompanyID = @iInscCompanyID AND ServiceChannelCD = 'CS')
BEGIN
	DELETE FROM utb_client_service_channel WHERE InsuranceCompanyID = @iInscCompanyID AND ServiceChannelCD = 'CS'	
	SELECT 'Service Channel CS deleted successfully'
END
ELSE
BEGIN
	-- Service Channel already deleted
	SELECT 'Service Channel CS already deleted'
END

----------------------------------------------
-- Delete new assignment type
-- utb_assignment_type
----------------------------------------------
IF EXISTS (SELECT * FROM utb_assignment_type WHERE [Name] = 'Choice Shop Assignment' AND ServiceChannelDefaultCD = 'CS')
BEGIN
	DELETE FROM utb_assignment_type WHERE [Name] = 'Choice Shop Assignment' AND ServiceChannelDefaultCD = 'CS'
	
	SELECT 'Assignment Type CS deleted successfully'
END
ELSE
BEGIN
	-- Service Channel already added
	SELECT 'Assignment Type CS already deleted'
END

----------------------------------------------
-- Delete FNOL Vars
----------------------------------------------
IF EXISTS (SELECT * FROM utb_assignment_pool WHERE [Name] = 'Choice Program Support')
BEGIN
	DELETE FROM utb_assignment_pool WHERE [Name] = 'Choice Program Support'
END

----------------------------------------------
-- Delete CS support to the utb_app_variable 
----------------------------------------------
IF EXISTS (SELECT * FROM utb_app_variable WHERE [Name] = 'Default_Assignment_Pool_Support' AND SubName = 'CS')
BEGIN
	DELETE FROM utb_app_variable WHERE [Name] = 'Default_Assignment_Pool_Support' AND SubName = 'CS'
END






