/*******************************************************************
* Pursuit Audit Shop Fax for Metlife
*******************************************************************/


INSERT INTO utb_form (
   DocumentTypeID,
   AutoBundlingFlag,
   InsuranceCompanyID,
   EnabledFlag,
   HTMLPath,
   Name,
   PDFPath,
   PertainsToCD,
   ServiceChannelCD,
   SQLProcedure,
   SystemFlag,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   22, -- Shop Assignment
   1,
   176, -- Metlife
   1,
   'Forms/PursuitAuditShopFax.asp',
   'Shop Fax - Pursuit Audit',
   'PursuitAuditShopFax.pdf',
   'S',
   'DA',
   'uspCFPursuitAuditShopFax',
   0,
   0,
   CURRENT_TIMESTAMP
)