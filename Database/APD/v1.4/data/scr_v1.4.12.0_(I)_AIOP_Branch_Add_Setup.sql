-----------------------------------------------------------------------------------------------------------
-- Configuration for AIOP Branch - 05Jan2012 - Thomas Duranko
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint

DECLARE @ModifiedDateTime       DateTime
DECLARE @CompanyName 			VARCHAR(100)
DECLARE @DeskAuditCompany		VARCHAR(50)

DECLARE @OfficeName1 			VARCHAR(100)
DECLARE @OfficeDesignation1 	VARCHAR(50)
DECLARE @OfficeStreetAddress1 	VARCHAR(100)
DECLARE @OfficeCity1 			VARCHAR(100)
DECLARE @OfficeState1 			VARCHAR(2)
DECLARE @OfficeZip1				VARCHAR(10)
DECLARE @OfficePhone1 			VARCHAR(12)
DECLARE @OfficeFax1 			VARCHAR(12)
DECLARE @OfficeEmail1 			VARCHAR(50)


SET @ModifiedDateTime = Current_Timestamp

-----------------------------------------------------------------------------------------------------------
-- Client Configuration Section.  All changes can be make from here.
-----------------------------------------------------------------------------------------------------------
-- Lynx Accounting 
SET @InsuranceCompanyID 		= 288	-- Company ID 

-- Basic Company Information
SET @CompanyName 				= 'Auto-Owners Insurance'			-- Company Name
SET @DeskAuditCompany			= 'AIO'								-- Company Name 4 Char

-- Office Information
	-- Company 1
	SET @OfficeName1 				= 'Sioux Falls, SD - Branch #31'	-- Office Name
	SET @OfficeDesignation1 		= 'SD - Sioux Falls'			-- Office Designation
	SET @OfficeStreetAddress1 		= 'P.O. Box 5201'				-- Street Address
	SET @OfficeCity1 				= 'Sioux Falls'					-- City
	SET @OfficeState1 				= 'SD'							-- State
	SET @OfficeZip1					= '57108'						-- Zip Code
	SET @OfficePhone1 				= '605-332-6500'				-- Phone (nnn-nnn-nnnn)
	SET @OfficeFax1 				= '517-327-2331'				-- Fax (nnn-nnn-nnnn)
	SET @OfficeEmail1 				= 'siouxfalls.clm@aoins.com'	-- Email 

-----------------------------------------------------------------------------------------------------------
-- Process steps
-----------------------------------------------------------------------------------------------------------
------------------------------------------------------
-- Create Office(s)
-- Auto Owners Insurance
------------------------------------------------------
-- Office 1
------------------------------------------------------
	INSERT INTO dbo.utb_office
			(
			 InsuranceCompanyID, 
			 Address1,
			 Address2, 
			 AddressCity, 
			 AddressState, 
			 AddressZip,
			 CCEmailAddress,
			 ClaimNumberFormatJS,
			 ClaimNumberValidJS,
			 ClaimNumberMsgText,
			 ClientOfficeId,
			 EnabledFlag, 
			 FaxAreaCode,
			 FaxExchangeNumber,
			 FaxUnitNumber,
			 MailingAddress1,
			 MailingAddress2, 
			 MailingAddressCity, 
			 MailingAddressState, 
			 MailingAddressZip,
			 Name, 
			 PhoneAreaCode, 
			 PhoneExchangeNumber, 
			 PhoneUnitNumber,
			 ReturnDocEmailAddress,
			 ReturnDocFaxAreaCode,
			 ReturnDocFaxExchangeNumber,
			 ReturnDocFaxUnitNumber, 
			 SysLastUserID,
			 SysLastUpdatedDate 
			)
			VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
					@OfficeStreetAddress1 , -- Address1 *
					'',			            -- Address2 *
					@OfficeCity1,			-- AddressCity *
					@OfficeState1,          -- AddressState *
					@OfficeZip1,            -- AddressZip *
					NULL,                   -- CCEmailAddress
					NULL,                   -- ClaimNumberFormatJS
					NULL,                   -- ClaimNumberValidJS
					NULL,                   -- ClaimNumberMsgText
					@OfficeDesignation1,	-- ClientOfficeId * 
					1,                      -- EnabledFlag
					SUBSTRING(@OfficeFax1,1,3),	   -- FaxAreaCode *
					SUBSTRING(@OfficeFax1,5,3),     -- FaxExchangeNumber *
					SUBSTRING(@OfficeFax1,9,4),     -- FaxUnitNumber *
					@OfficeStreetAddress1 ,	-- MailingAddress1 *
					'',			            -- MailingAddress2 *
					@OfficeCity1,			-- MailingAddressCity *
					@OfficeState1,          -- MailingAddressState *
					@OfficeZip1,            -- MailingAddressZip *
					@OfficeName1 ,			-- Name *
					SUBSTRING(@OfficePhone1,1,3),	-- PhoneAreaCode *
					SUBSTRING(@OfficePhone1,5,3),	-- PhoneExchangeNumber *
					SUBSTRING(@OfficePhone1,9,4),	-- PhoneUnitNumber *
					@OfficeEmail1,           -- ReturnDocEmailAddress
					NULL,                   -- ReturnDocFaxAreaCode
					NULL,                   -- ReturnDocFaxExchangeNumber
					NULL,                   -- ReturnDocFaxUnitNumber
					0,                      -- SysLastUserID
					@ModifiedDateTime       -- SysLastUpdatedDate
			)


	SELECT @OfficeID = SCOPE_IDENTITY()


	-- Office level configurations - this must match the Client assignment types

	INSERT INTO dbo.utb_office_assignment_type 
	  SELECT  @OfficeID, 
			  AssignmentTypeID, 
			  0,
			  @ModifiedDateTime
		FROM  dbo.utb_assignment_type
		WHERE Name in ( 'Demand Estimate Audit')


	INSERT INTO dbo.utb_office_contract_state
	  SELECT  @OfficeID,
			  StateCode,
			  0, -- UseCEIShopsFlag
			  0, -- SysLastUserID
			  @ModifiedDateTime
		FROM  dbo.utb_state_code
		WHERE EnabledFlag = 1

	SELECT @OfficeID = SCOPE_IDENTITY()

DECLARE @iAreaCode INT
DECLARE @iExchange INT
DECLARE @iUnit INT

SET @iAreaCode = SUBSTRING(@OfficePhone1,1,3)
SET @iExchange = SUBSTRING(@OfficePhone1,5,3)
SET @iUnit = SUBSTRING(@OfficePhone1,9,4)

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = @OfficeEmail1 ,
    @NameFirst             = @DeskAuditCompany,
    @NameLast              = @OfficeDesignation1,
    @EmailAddress          = @OfficeEmail1,
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = @iAreaCode,
    @PhoneExchangeNumber   = @iExchange,
    @PhoneUnitNumber       = @iUnit,
    @ApplicationID		   = '2'	


-- Review affected tables
SELECT * FROM dbo.utb_office
SELECT * FROM dbo.utb_office_assignment_type
SELECT * FROM dbo.utb_office_contract_state

-- This script requires a fresh on the workflow and spawn reference tables from the access file

--commit 
--rollback
