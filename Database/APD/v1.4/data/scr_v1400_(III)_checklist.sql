-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Copies checklist, changing reference key to ClaimAspectServiceChannel as well as 
--        updating the status to match the aspect service channel status
--
----------------------------------------------------------------------------------------
--  Dependancies:  utb_status must be populated from reference list and 
--                 utb_service_channel_schedule_change_reason must be populated from 
--                 the reference list before this is executed 
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION


PRINT 'Populate new table utb_claim_aspect_status'

--  Note:  utb_status must be populated from reference list before this is executed 

DECLARE @StatusIDDAOpen         udt_std_id
DECLARE @StatusIDDAComplete     udt_std_id
DECLARE @StatusIDDACancelled    udt_std_id
DECLARE @StatusIDDROpen         udt_std_id
DECLARE @StatusIDDRComplete     udt_std_id
DECLARE @StatusIDDRCancelled    udt_std_id
DECLARE @StatusIDGLOpen         udt_std_id
DECLARE @StatusIDGLComplete     udt_std_id
DECLARE @StatusIDGLCancelled    udt_std_id
DECLARE @StatusIDMEOpen         udt_std_id
DECLARE @StatusIDMEComplete     udt_std_id
DECLARE @StatusIDMECancelled    udt_std_id
DECLARE @StatusIDPSOpen         udt_std_id
DECLARE @StatusIDPSComplete     udt_std_id
DECLARE @StatusIDPSCancelled    udt_std_id
    
SELECT  @StatusIDDAOpen = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'DA' AND Name = 'Active'
  
IF @StatusIDDAOpen IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for DA Active not found', 16, 1)
    RETURN
END

SELECT  @StatusIDDAComplete = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'DA' AND Name = 'Complete'
  
IF @StatusIDDAComplete IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for DA Complete not found', 16, 1)
    RETURN
END

SELECT  @StatusIDDACancelled = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'DA' AND Name = 'Cancelled'
  
IF @StatusIDDACancelled IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for DA Cancelled not found', 16, 1)
    RETURN
END

SELECT  @StatusIDDROpen = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'DR' AND Name = 'Active'
  
IF @StatusIDDROpen IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for DR Active not found', 16, 1)
    RETURN
END

SELECT  @StatusIDDRComplete = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'DR' AND Name = 'Complete'
  
IF @StatusIDDRComplete IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for DR Complete not found', 16, 1)
    RETURN
END

SELECT  @StatusIDDRCancelled = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'DR' AND Name = 'Cancelled'
  
IF @StatusIDDRCancelled IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for DR Cancelled not found', 16, 1)
    RETURN
END

SELECT  @StatusIDGLOpen = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'GL' AND Name = 'Active'
  
IF @StatusIDGLOpen IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for GL Active not found', 16, 1)
    RETURN
END

SELECT  @StatusIDGLComplete = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'GL' AND Name = 'Complete'
  
IF @StatusIDGLComplete IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for GL Complete not found', 16, 1)
    RETURN
END

SELECT  @StatusIDGLCancelled = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'GL' AND Name = 'Cancelled'
  
IF @StatusIDGLCancelled IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for GL Cancelled not found', 16, 1)
    RETURN
END

SELECT  @StatusIDMEOpen = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'ME' AND Name = 'Active'
  
IF @StatusIDMEOpen IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for ME Active not found', 16, 1)
    RETURN
END

SELECT  @StatusIDMEComplete = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'ME' AND Name = 'Complete'
  
IF @StatusIDMEComplete IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for ME Complete not found', 16, 1)
    RETURN
END

SELECT  @StatusIDMECancelled = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'ME' AND Name = 'Cancelled'
  
IF @StatusIDMECancelled IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for ME Cancelled not found', 16, 1)
    RETURN
END

SELECT  @StatusIDPSOpen = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'PS' AND Name = 'Active'
  
IF @StatusIDPSOpen IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for PS Active not found', 16, 1)
    RETURN
END

SELECT  @StatusIDPSComplete = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'PS' AND Name = 'Complete'
  
IF @StatusIDPSComplete IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for PS Complete not found', 16, 1)
    RETURN
END

SELECT  @StatusIDPSCancelled = StatusID
  FROM  dbo.utb_status 
  WHERE ServiceChannelCD = 'PS' AND Name = 'Cancelled'
  
IF @StatusIDPSCancelled IS NULL
BEGIN
    RAISERROR('Invalid Data State: StatusID for PS Cancelled not found', 16, 1)
    RETURN
END


PRINT 'Converting utb_checklist'

SET IDENTITY_INSERT dbo.utb_checklist ON

INSERT INTO dbo.utb_checklist (
            CheckListID, 
            AssignedUserID, 
            ClaimAspectServiceChannelID, 
            CreatedRoleID, 
            CreatedUserID, 
            StatusID, 
            TaskID, 
            AlarmDate, 
            CreatedDate, 
            UserTaskDescription, 
            SysLastUserID, 
            SysLastUpdatedDate
)
  SELECT  c.ChecklistID,
          c.AssignedUserID,
          casc.ClaimAspectServiceChannelID,
          c.CreatedRoleID,
          c.CreatedUserID,
          CASE 
            WHEN ca.ServiceChannelCD = 'DA' AND c.StatusID = 100 THEN @StatusIDDAOpen
            WHEN ca.ServiceChannelCD = 'DA' AND c.StatusID IN (994, 995) THEN @StatusIDDACancelled
            WHEN ca.ServiceChannelCD = 'DA' AND c.StatusID = 998 THEN @StatusIDDAComplete
            WHEN ca.ServiceChannelCD = 'DR' AND c.StatusID = 100 THEN @StatusIDDROpen
            WHEN ca.ServiceChannelCD = 'DR' AND c.StatusID IN (994, 995) THEN @StatusIDDRCancelled
            WHEN ca.ServiceChannelCD = 'DR' AND c.StatusID = 998 THEN @StatusIDDRComplete
            WHEN ca.ServiceChannelCD = 'ME' AND c.StatusID = 100 THEN @StatusIDMEOpen
            WHEN ca.ServiceChannelCD = 'ME' AND c.StatusID IN (994, 995) THEN @StatusIDMECancelled
            WHEN ca.ServiceChannelCD = 'ME' AND c.StatusID = 998 THEN @StatusIDMEComplete
            WHEN ca.ServiceChannelCD = 'PS' AND c.StatusID = 100 THEN @StatusIDPSOpen
            WHEN ca.ServiceChannelCD = 'PS' AND c.StatusID IN (994, 995) THEN @StatusIDPSCancelled
            WHEN ca.ServiceChannelCD = 'PS' AND c.StatusID = 998 THEN @StatusIDPSComplete
            ELSE -1
          END,
          c.TaskID,
          c.AlarmDate,
          c.CreatedDate,
          c.UserTaskDescription,
          c.SysLastUserID,
          c.SysLastUpdatedDate
    FROM  dbo.Tmp_utb_checklist c
   INNER JOIN dbo.tmp_utb_claim_aspect ca ON (c.ClaimAspectID = ca.ClaimAspectID)
   INNER JOIN dbo.utb_claim_aspect_service_channel casc ON (ca.ClaimAspectID = casc.ClaimAspectID AND ca.ServiceChannelCD = casc.ServiceChannelCD)
   WHERE casc.PrimaryFlag = 1
    
SET IDENTITY_INSERT dbo.utb_checklist OFF

--commit
--rollback


