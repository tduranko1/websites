--
--
-- Script for reinitializing the datawarehouse
--

SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON


-- Drop indexes so that initial warehouse generation runs faster

    DROP INDEX dbo.utb_dtwh_dim_assignment_type.uix_dtwh_dim_assignment_type_servicechannelid
    DROP INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_insurancecompanyid
    DROP INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_officeid
    DROP INDEX dbo.utb_dtwh_dim_customer.uix_ie_dtwh_dim_customer_userid
    DROP INDEX dbo.utb_dtwh_dim_disposition_type.uix_dtwh_dim_disposition_type_dispositiontypecd
    DROP INDEX dbo.utb_dtwh_dim_disposition_type.uix_ie_dtwh_dim_disposition_type_description
    DROP INDEX dbo.utb_dtwh_dim_document_source.uix_dtwh_dim_document_source_documentsourcename
    DROP INDEX dbo.utb_dtwh_dim_document_source.uix_dtwh_dim_document_source_electronicsourceflag
    DROP INDEX dbo.utb_dtwh_dim_lynx_handler.uix_dtwh_dim_lynx_handler_userid
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ak_dtwh_dim_repair_location_shoplocationid
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_city
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_county
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_stateid
    DROP INDEX dbo.utb_dtwh_dim_repair_location.uix_ie_dtwh_dim_repair_location_zipcode
    DROP INDEX dbo.utb_dtwh_dim_service_channel.uix_dtwh_dim_service_channel_servicechannelcd
    DROP INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_countrycode
    DROP INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_regioncd
    DROP INDEX dbo.utb_dtwh_dim_state.uix_dtwh_dim_state_statecode
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_datevalue
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_dayofmonth
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_hourvalue
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_julianday
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_monthofyear
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_quarter
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_weekofyear
    DROP INDEX dbo.utb_dtwh_dim_time.uix_dtwh_dim_time_yearvalue
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ak_dtwh_fact_claim_claimaspectid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_assignmenttypeclosingid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_assignmenttypeid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_claimlocationid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_coveragetypeid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_coveragetypeid_timeidclosed_claimlocationid_customerid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_customerid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_dispositiontypeid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_dispositiontypeid_assignmenttypeclosingid_coveragetypeid_enabledflag_servicetotallossflag
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_lynxhandleranalystid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_lynxhandlerownerid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_lynxhandlersupportid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_repairlocationid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_servicechannelid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_timeidclosed
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_timeidclosed_claimlocationid_customerid_servicechannelid_coveragetypeid
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_timeidclosed_customerid_claimlocationid_dispositiontypeid_assignmenttypeclosingid_coveragetypeid_flags
    DROP INDEX dbo.utb_dtwh_fact_claim.uix_ie_dtwh_fact_claim_timeidnew
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ak_dtwh_fact_estimate_documentid
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_claimaspectid
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_customerid
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_documentsourceid
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_repairlocationid
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_timeidassignment
    DROP INDEX dbo.utb_dtwh_fact_estimate.uix_ie_dtwh_fact_estimate_timeidreceived



-- Initialize the warehouse

exec uspDtwhGenerateData 'Init'


-- Apply all dropped indexes

CREATE NONCLUSTERED INDEX uix_dtwh_dim_assignment_type_servicechannelid
    ON dbo.utb_dtwh_dim_assignment_type(ServiceChannelID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_customer_insurancecompanyid
    ON dbo.utb_dtwh_dim_customer(InsuranceCompanyID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_customer_officeid
    ON dbo.utb_dtwh_dim_customer(OfficeID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE UNIQUE NONCLUSTERED INDEX uix_ie_dtwh_dim_customer_userid
    ON dbo.utb_dtwh_dim_customer(UserID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE UNIQUE NONCLUSTERED INDEX uix_dtwh_dim_disposition_type_dispositiontypecd
    ON dbo.utb_dtwh_dim_disposition_type(DispositionTypeCD)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_disposition_type_description
    ON dbo.utb_dtwh_dim_disposition_type(DispositionTypeDescription)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE UNIQUE NONCLUSTERED INDEX uix_dtwh_dim_document_source_documentsourcename
    ON dbo.utb_dtwh_dim_document_source(DocumentSourceName)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_document_source_electronicsourceflag
    ON dbo.utb_dtwh_dim_document_source(ElectronicSourceFlag)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE UNIQUE NONCLUSTERED INDEX uix_dtwh_dim_lynx_handler_userid
    ON dbo.utb_dtwh_dim_lynx_handler(UserID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE UNIQUE NONCLUSTERED INDEX uix_ak_dtwh_dim_repair_location_shoplocationid
    ON dbo.utb_dtwh_dim_repair_location(ShopLocationID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_repair_location_city
    ON dbo.utb_dtwh_dim_repair_location(City)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_repair_location_county
    ON dbo.utb_dtwh_dim_repair_location(County)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_repair_location_stateid
    ON dbo.utb_dtwh_dim_repair_location(StateID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_dim_repair_location_zipcode
    ON dbo.utb_dtwh_dim_repair_location(ZipCode)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_service_channel_servicechannelcd
    ON dbo.utb_dtwh_dim_service_channel(ServiceChannelCD)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_state_countrycode
    ON dbo.utb_dtwh_dim_state(CountryCode)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_state_regioncd
    ON dbo.utb_dtwh_dim_state(RegionCD)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE UNIQUE NONCLUSTERED INDEX uix_dtwh_dim_state_statecode
    ON dbo.utb_dtwh_dim_state(StateCode)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE UNIQUE NONCLUSTERED INDEX uix_dtwh_dim_time_datevalue
    ON dbo.utb_dtwh_dim_time(DateValue)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_time_dayofmonth
    ON dbo.utb_dtwh_dim_time(DayOfMonth)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_time_hourvalue
    ON dbo.utb_dtwh_dim_time(HourValue)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_time_julianday
    ON dbo.utb_dtwh_dim_time(JulianDay)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_time_monthofyear
    ON dbo.utb_dtwh_dim_time(MonthOfYear)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_time_quarter
    ON dbo.utb_dtwh_dim_time(Quarter)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_time_weekofyear
    ON dbo.utb_dtwh_dim_time(WeekOfYear)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_dtwh_dim_time_yearvalue
    ON dbo.utb_dtwh_dim_time(YearValue)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE UNIQUE NONCLUSTERED INDEX uix_ak_dtwh_fact_claim_claimaspectid
    ON dbo.utb_dtwh_fact_claim(ClaimAspectID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_assignmenttypeclosingid
    ON dbo.utb_dtwh_fact_claim(AssignmentTypeClosingID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_assignmenttypeid
    ON dbo.utb_dtwh_fact_claim(AssignmentTypeID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_claimlocationid
    ON dbo.utb_dtwh_fact_claim(ClaimLocationID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index



CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_coveragetypeid
    ON dbo.utb_dtwh_fact_claim(CoverageTypeID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_coveragetypeid_timeidclosed_claimlocationid_customerid
    ON dbo.utb_dtwh_fact_claim(CoverageTypeID,TimeIDClosed,ClaimLocationID,CustomerID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_customerid
    ON dbo.utb_dtwh_fact_claim(CustomerID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_dispositiontypeid
    ON dbo.utb_dtwh_fact_claim(DispositionTypeID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_dispositiontypeid_assignmenttypeclosingid_coveragetypeid_enabledflag_servicetotallossflag
    ON dbo.utb_dtwh_fact_claim(DispositionTypeID,AssignmentTypeClosingID,CoverageTypeID,EnabledFlag,ServiceTotalLossFlag)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_lynxhandleranalystid
    ON dbo.utb_dtwh_fact_claim(LynxHandlerAnalystID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_lynxhandlerownerid
    ON dbo.utb_dtwh_fact_claim(LynxHandlerOwnerID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_lynxhandlersupportid
    ON dbo.utb_dtwh_fact_claim(LynxHandlerSupportID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_repairlocationid
    ON dbo.utb_dtwh_fact_claim(RepairLocationID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index

CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_servicechannelid
    ON dbo.utb_dtwh_fact_claim(ServiceChannelID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_timeidclosed
    ON dbo.utb_dtwh_fact_claim(TimeIDClosed)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_timeidclosed_claimlocationid_customerid_servicechannelid_coveragetypeid
    ON dbo.utb_dtwh_fact_claim(TimeIDClosed,ClaimLocationID,CustomerID,ServiceChannelID,CoverageTypeID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_timeidclosed_customerid_claimlocationid_dispositiontypeid_assignmenttypeclosingid_coveragetypeid_flags
    ON dbo.utb_dtwh_fact_claim(TimeIDClosed,CustomerID,ClaimLocationID,DispositionTypeID,AssignmentTypeClosingID,CoverageTypeID,EnabledFlag,ServiceTotalLossFlag,DemoFlag,CycleTimeNewToCloseCalDay)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_claim_timeidnew
    ON dbo.utb_dtwh_fact_claim(TimeIDNew)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE UNIQUE NONCLUSTERED INDEX uix_ak_dtwh_fact_estimate_documentid
    ON dbo.utb_dtwh_fact_estimate(DocumentID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_estimate_claimaspectid
    ON dbo.utb_dtwh_fact_estimate(ClaimAspectID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_estimate_customerid
    ON dbo.utb_dtwh_fact_estimate(CustomerID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_estimate_documentsourceid
    ON dbo.utb_dtwh_fact_estimate(DocumentSourceID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_estimate_repairlocationid
    ON dbo.utb_dtwh_fact_estimate(RepairLocationID)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_estimate_timeidassignment
    ON dbo.utb_dtwh_fact_estimate(TimeIDAssignment)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


CREATE NONCLUSTERED INDEX uix_ie_dtwh_fact_estimate_timeidreceived
    ON dbo.utb_dtwh_fact_estimate(TimeIDReceived)
  WITH FILLFACTOR = 100
    ON ufg_dtwh_index


GO


PRINT '.'
PRINT '.'
PRINT 'Complete'
PRINT '!'

GO
