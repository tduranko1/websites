-- Begin the transaction for dropping and creating the tables
--DROP TABLE [dbo].[utb_hyperquest_processed]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[utb_hyperquest_processed](
	[JobID] VARCHAR(255) NOT NULL,
	[AsyncGUID] VARCHAR(255) NOT NULL,
	[PartnerGUID] VARCHAR(255) NOT NULL,
	[DocumentGUID] VARCHAR(255) NOT NULL,
	[LynxID] BIGINT NOT NULL,
	[DocumentID] BIGINT NOT NULL,
	[DocumentTypeID] INT NOT NULL,
	[JobProcessedDate] DATETIME NOT NULL,
	[SysLastUpdatedDate] DATETIME  NOT NULL
 CONSTRAINT [PK_utb_hyperquest_processed] PRIMARY KEY CLUSTERED 
(
	[JobID] ASC
	, [DocumentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [ufg_primary]
) ON [ufg_primary]
GO

SET ANSI_PADDING ON
GO

