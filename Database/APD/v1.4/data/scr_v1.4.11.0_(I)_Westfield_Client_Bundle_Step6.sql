DECLARE @BundlingID INT
DECLARE @DocumentID INT
DECLARE @InscCompID INT
SET @InscCompID = 387
SET @BundlingID = 0
SET @DocumentID = 0

-- Create New Document Type - Assignment Cancellation
DECLARE @MaxID INT
SELECT @MaxID=MAX(DocumentTypeID)+1 FROM utb_document_type

INSERT INTO 
	utb_document_type 
VALUES 
	(
		@MaxID
		, @MaxID
		, 'C'
		, 1
		, 0
		, 'Assignment Cancellation'
		, 0 
		, 0
		, CURRENT_TIMESTAMP
	)

-- Get the new document type id
SELECT @DocumentID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Assignment Cancellation'

-- Get the bundling ID
SELECT
	@BundlingID = b.BundlingID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = @InscCompID
	AND b.[Name] = 'Assignment Cancellation'

UPDATE utb_bundling SET DocumentTypeID = @DocumentID WHERE BundlingID = @BundlingID


-- SELECT * FROM utb_bundling_document_type WHERE BundlingID = 395
	
-- Add Required Docs
UPDATE 
	utb_bundling_document_type 
SET 
	MandatoryFlag = 1 
--SELECT * FROM utb_bundling_document_type 
WHERE
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_bundling b
		WHERE 	
			b.[Name] = 'Cash Out Alert & Close'
			AND	BundlingID IN
			(
				SELECT 
					b.BundlingID 
				FROM 
					utb_message_template mt 
					INNER JOIN utb_bundling b 
					ON b.MessageTemplateID = mt.MessageTemplateID 
					INNER JOIN utb_client_bundling cb
					ON cb.BundlingID = b.BundlingID 
				WHERE 
					cb.InsuranceCompanyID = @InscCompID
			)
		)
		AND DocumentTypeID IN (8)

UPDATE 
	utb_bundling_document_type 
SET 
	MandatoryFlag = 1 
	, DocumentTypeID = 3
	, FinalEstimateFlag = 1
--SELECT * FROM utb_bundling_document_type 
WHERE
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_bundling b
		WHERE 	
			b.[Name] = 'Cash Out Alert & Close'
			AND	BundlingID IN
			(
				SELECT 
					b.BundlingID 
				FROM 
					utb_message_template mt 
					INNER JOIN utb_bundling b 
					ON b.MessageTemplateID = mt.MessageTemplateID 
					INNER JOIN utb_client_bundling cb
					ON cb.BundlingID = b.BundlingID 
				WHERE 
					cb.InsuranceCompanyID = 387 --@InscCompID
			)
		)
		AND DocumentTypeID IN (35)

SELECT
	@BundlingID = b.BundlingID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = @InscCompID
	AND b.[Name] = 'Cash Out Alert & Close'
	
INSERT INTO
	utb_bundling_document_type
VALUES
	(
		@BundlingID
		, 8
		, NULL
		, 'I'
		, 0
		, 1
		, 0
		, NULL
		, 0
		, NULL
		, 1
		, 4
		, NULL
		, 0
		, 0
		, 0
		, CURRENT_TIMESTAMP
	)

UPDATE utb_bundling SET DocumentTypeID = 37 WHERE BundlingID = @BundlingID

-- Closing

DELETE FROM
	utb_bundling_document_type 
WHERE
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM
			utb_bundling b
			INNER JOIN utb_client_bundling cb 
				ON b.BundlingId = cb.BundlingID
		WHERE
			cb.InsuranceCompanyID = @InscCompID
			AND b.[Name] = 'Closing - Total Loss'
	)
	AND DocumentTypeID = 13

-- Rental not required
UPDATE 
	utb_bundling_document_type 
SET 
	MandatoryFlag = 0
WHERE
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_bundling b
		WHERE 	
			b.[Name] = 'Closing Repair Complete'
			AND	BundlingID IN
			(
				SELECT 
					b.BundlingID 
				FROM 
					utb_message_template mt 
					INNER JOIN utb_bundling b 
					ON b.MessageTemplateID = mt.MessageTemplateID 
					INNER JOIN utb_client_bundling cb
					ON cb.BundlingID = b.BundlingID 
				WHERE 
					cb.InsuranceCompanyID = @InscCompID
			)
		)	
		AND DocumentTypeID = 9

-- Supplement requirements
UPDATE 
	utb_bundling_document_type 
SET 
	MandatoryFlag = 1
WHERE
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_bundling b
		WHERE 	
			b.[Name] = 'Closing Supplement'
			AND	BundlingID IN
			(
				SELECT 
					b.BundlingID 
				FROM 
					utb_message_template mt 
					INNER JOIN utb_bundling b 
					ON b.MessageTemplateID = mt.MessageTemplateID 
					INNER JOIN utb_client_bundling cb
					ON cb.BundlingID = b.BundlingID 
				WHERE 
					cb.InsuranceCompanyID = @InscCompID
			)
		)	
		AND DocumentTypeID = 13

DELETE FROM
	utb_bundling_document_type 
WHERE
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM
			utb_bundling b
			INNER JOIN utb_client_bundling cb 
				ON b.BundlingId = cb.BundlingID
		WHERE
			cb.InsuranceCompanyID = @InscCompID
			AND b.[Name] = 'Closing Supplement'
	)
	AND DocumentTypeID = 3

SELECT
	@BundlingID = b.BundlingID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = @InscCompID
	AND b.[Name] = 'Closing Supplement'

INSERT INTO
	utb_bundling_document_type
VALUES
	(
		@BundlingID
		, 8
		, NULL
		, 'I'
		, 0
		, 1
		, 0
		, NULL
		, 0
		, NULL
		, 1
		, 4
		, NULL
		, 0
		, 0
		, 0
		, CURRENT_TIMESTAMP
	)

INSERT INTO
	utb_bundling_document_type
VALUES
	(
		@BundlingID
		, 10
		, NULL
		, 'I'
		, 0
		, 0
		, 0
		, 'A'
		, 0
		, NULL
		, 1
		, 2
		, NULL
		, 0
		, 0
		, 0
		, CURRENT_TIMESTAMP
	)

SELECT
	@BundlingID = b.BundlingID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = @InscCompID
	AND b.[Name] = 'Initial Estimate and Photos (Adjuster)'

INSERT INTO
	utb_bundling_document_type
VALUES
	(
		@BundlingID
		, 8
		, NULL
		, 'I'
		, 0
		, 0
		, 0
		, 'A'
		, 0
		, NULL
		, 1
		, 2
		, NULL
		, 0
		, 0
		, 0
		, CURRENT_TIMESTAMP
	)

INSERT INTO
	utb_bundling_document_type
VALUES
	(
		@BundlingID
		, 35
		, NULL
		, NULL
		, 0
		, 0
		, 0
		, NULL
		, 0
		, NULL
		, 1
		, 1
		, NULL
		, 1
		, 0
		, 0
		, CURRENT_TIMESTAMP
	)

-- Correct wrong send as
UPDATE utb_bundling SET DocumentTypeID = 35 WHERE BundlingID = @BundlingID

-- Correct wrong bundle for total loss alert
SELECT
	@BundlingID = b.BundlingID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = @InscCompID
	AND b.[Name] = 'Total Loss Alert - (Adjuster)'

UPDATE utb_bundling SET [Name] = 'Total Loss Alert' WHERE BundlingID = @BundlingID

-- Add required for Closing total loss
SELECT
	@BundlingID = b.BundlingID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = @InscCompID
	AND b.[Name] = 'Closing - Total Loss'		

UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @BundlingID
UPDATE utb_bundling_document_type SET DuplicateFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 8
UPDATE utb_bundling_document_type SET FinalEstimateFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 3
UPDATE utb_bundling_document_type SET DocumentTypeID = 41 WHERE BundlingID = @BundlingID AND DocumentTypeID = 40

INSERT INTO
	utb_bundling_document_type
VALUES
	(
		@BundlingID
		, 40
		, NULL
		, NULL
		, 0
		, 0
		, 0
		, NULL
		, 0
		, NULL
		, 1
		, 1
		, NULL
		, 1
		, 0
		, 0
		, CURRENT_TIMESTAMP
	)
