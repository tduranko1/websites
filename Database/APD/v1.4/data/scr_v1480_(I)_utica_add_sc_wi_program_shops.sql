/******************************************************************************
* Add SC and WI Program shops to the Utica shop list
* Requested by: Linda Lackey via email 4/13/2009
******************************************************************************/
DECLARE @InsuranceCompanyID as int
DECLARE @NOW as datetime

set @now = current_timestamp

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM  utb_insurance
WHERE Name = 'Utica National Insurance Group'

BEGIN TRANSACTION

IF @InsuranceCompanyID IS NOT NULL
BEGIN
   INSERT INTO dbo.utb_client_business_state (
      InsuranceCompanyID,
      StateCode,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @InsuranceCompanyID,
      'SC',
      0,
      @now
   )
   
   INSERT INTO dbo.utb_client_business_state (
      InsuranceCompanyID,
      StateCode,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @InsuranceCompanyID,
      'WI',
      0,
      @now
   )

END

-- COMMIT
-- ROLLBACK