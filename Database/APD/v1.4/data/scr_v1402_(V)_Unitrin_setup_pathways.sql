/****************************************************************************************
* Setup Unitrin to use Pathways for Desk Audits.
****************************************************************************************/

begin transaction

update utb_insurance
set DeskAuditPreferredCommunicationMethodID = 14,
    DeskAuditCompanyCD = 'UNBI'
where InsuranceCompanyID = 261

select * from utb_insurance where name like 'unitrin%'

-- commit
-- rollback