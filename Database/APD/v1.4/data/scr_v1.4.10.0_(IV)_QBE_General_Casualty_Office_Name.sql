/**********************************************************************************
* QBE - Update General Office to General General Casualty to match production
**********************************************************************************/

UPDATE utb_office
SET Name = 'General Casualty'
WHERE InsuranceCompanyID = 304
  AND Name = 'General'
  
