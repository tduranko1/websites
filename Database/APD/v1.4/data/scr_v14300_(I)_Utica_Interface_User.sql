DECLARE @LogonID as varchar(50)
DECLARE @NameLast as varchar(50)
DECLARE @NameFirst as varchar(50)
DECLARE @EmailAddress as varchar(100)
DECLARE @UserID as bigint
DECLARE @ApplicationID as int
DECLARE @now as datetime
DECLARE @OfficeID as int

SET @LogonID = 'utica.interface@lynxservices.com'
SET @NameLast = 'Interface'
SET @NameFirst = 'Utica'
SET @EmailAddress = @LogonID
SET @now = CURRENT_TIMESTAMP

SELECT @ApplicationID = ApplicationID
FROM utb_application
WHERE Code = 'CP'

SELECT @UserID = UserID
FROM utb_user_application
WHERE LogonID = @LogonID
  AND (AccessEndDate IS NULL or AccessEndDate > @now) 
  AND ApplicationID = @ApplicationID

SELECT TOP 1 @OfficeID = OfficeID
FROM utb_office o
LEFT JOIN utb_insurance i ON o.InsuranceCompanyID = i.InsuranceCompanyID
WHERE i.Name = 'Utica National Insurance Group'

IF @UserID is NULL
BEGIN
   BEGIN TRANSACTION
   
   -- Insert the User record
   INSERT INTO utb_user (
      OfficeID,
      EmailAddress,
      NameFirst,
      NameLast,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @OfficeID,
      @EmailAddress,
      @NameFirst,
      @NameLast,
      0,
      @now
   )
   
   IF @@ERROR <> 0
   BEGIN
      RAISERROR('Error adding record to utb_user',16,1)
      ROLLBACK TRANSACTION
      RETURN
   END

   SET @UserID = SCOPE_IDENTITY()
   
   -- Insert the user application record
   INSERT INTO dbo.utb_user_application (
      UserID,
      ApplicationID,
      AccessBeginDate,
      LogonId,
      PasswordExpirationDate,
      PasswordHint,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @UserID,
      @ApplicationID,
      @NOW,
      @LogonID,
      NULL,
      NULL,
      0,
      @NOW
   )
   
   IF @@ERROR <> 0
   BEGIN
      RAISERROR('Error adding record to utb_user_application',16,1)
      ROLLBACK TRANSACTION
      RETURN
   END
   
   COMMIT
   
   print 'User "' + @LogonID + '" added.'
END
ELSE
BEGIN
   print 'User "' + @LogonID + '" already exists. User data was updated.'

   UPDATE utb_user
   SET NameFirst = @NameFirst,
       NameLast = @NameLast,
       EmailAddress = @EmailAddress,
       SysLastUserID = 0,
       SysLastUpdatedDate = @now
   WHERE UserID = @UserID

   SELECT *
   FROM utb_user_application ua
   LEFT JOIN utb_user u on ua.UserID = u.UserID
   WHERE ua.UserID = @UserID

END

