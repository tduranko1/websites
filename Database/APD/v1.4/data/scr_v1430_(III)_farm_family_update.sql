-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configurations for Farm Family
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @PaymentCompanyID       smallint


DECLARE @ModifiedDateTime       DateTime

declare @UserID as int
declare @LogonID as varchar(5)

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 198
SET @PaymentCompanyID = 691
SET @LogonID = 'AV_FF'


UPDATE utb_insurance
SET ReturnDocRoutingCD = 'CCAV'
WHERE InsuranceCompanyID = @InsuranceCompanyID

-- Document bundling. Photos are sent separately as jpg
INSERT INTO utb_client_document_exception
(InsuranceCompanyID, DocumentTypeID, OutputTypeCD, SysLastUserID, SysLastUpdatedDate)
VALUES
(@InsuranceCompanyID, 8, NULL, 0, @ModifiedDateTime)

-- delete the MA client contracted state. We are not licensed to do DRP in that state
DELETE FROM utb_client_contract_state
WHERE StateCode = 'MA'
  AND InsuranceCompanyID = @InsuranceCompanyID


if not exists(select * from utb_user_application where logonid = @LogonID and ApplicationID = 6)
begin
	-- we will add this user to the "Farm Family Home Office" office.
	-- this is the only office defined at the time of this writing

	select @OfficeID = OfficeID
	from utb_office
	where Name = 'Home Office'
 	  and InsuranceCompanyID = @InsuranceCompanyID

	if (@OfficeID is null)
	begin
		print 'Cannot find Home Office for Farm Family Insurance'
		rollback
		return
	end

	insert into utb_user
	(OfficeID, EnabledFlag, NameFirst, NameLast, SysLastUserID)
	VALUES
	(@OfficeID, 1, 'Farm Family', 'Autoverse User', 0)
	
	if (@@error <> 0)
	begin
		print 'Unable to add Farm Family Adjuster user to utb_user'
		rollback
		return
	end

	select @UserID = UserID
	from utb_user
	where NameLast = 'Autoverse User'
	  and NameFirst = 'Farm Family'
	
	if (@UserID is null)
	begin
		print 'Unable to find the newly added Farm Family Adjuster user in utb_user'
		rollback
		return
	end

	insert into utb_user_application
	(UserID, ApplicationID, AccessBeginDate, LogonId, SysLastUserID)
	VALUES
	(@UserID, 6, CURRENT_TIMESTAMP, @LogonID, 0)
	
	if (@@error <> 0)
	begin
		print 'Unable to add Farm Family Adjuster user to utb_user_application'
		rollback
		return
	end
	
	insert into utb_user_role
	(UserID, RoleID, PrimaryRoleFlag, SysLastUserID)
	values
	(@UserID, 1, 1, 0)
	
	if (@@error <> 0)
	begin
		print 'Unable to add Farm Family Adjuster user to utb_user_role'
		rollback
		return
	end

	select * from utb_user where userID = @UserID
	select * from utb_user_application where userID = @UserID
	select * from utb_user_role where userID = @UserID

end

select * from utb_insurance where InsuranceCompanyID = @InsuranceCompanyID
select * from utb_client_document_exception where InsuranceCompanyID = @InsuranceCompanyID

-- commit
-- rollback
