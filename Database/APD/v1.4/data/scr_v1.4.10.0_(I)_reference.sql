--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!! YOU MUST APPLY THE COMMIT OR ROLLBACK AFTER EXECUTING THIS SCRIPT !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SET ANSI_NULLS ON
SET ANSI_WARNINGS ON
GO

BEGIN TRANSACTION


--
--
-- Script for updating APD reference data.
--

PRINT 'Remember to COMMIT or ROLLBACK changes'
PRINT '.'

-- Assignment_pool
ALTER TABLE dbo.utb_task_assignment_pool DROP CONSTRAINT ufk_task_assignment_pool_assignmentpoolid_in_assignment_pool
ALTER TABLE dbo.utb_assignment_pool_user DROP CONSTRAINT ufk_assignment_pool_user_assignmentpoolid_in_assignment_pool

-- Assignment_type
ALTER TABLE dbo.utb_claim_aspect DROP CONSTRAINT ufk_claim_aspect_initialassignmenttypeid_in_assignment_type
ALTER TABLE dbo.utb_client_assignment_type DROP CONSTRAINT ufk_client_assignment_type_assignmenttypeid_in_assignment_type
ALTER TABLE dbo.utb_office_assignment_type DROP CONSTRAINT ufk_office_assignment_type_assignmenttypeid_in_assignment_type

-- Communication Method
ALTER TABLE dbo.utb_appraiser DROP CONSTRAINT ufk_appraiser_preferredcommunicationmethodid_in_communication_method
ALTER TABLE dbo.utb_assignment DROP CONSTRAINT ufk_assignment_communicationmethodid_in_communication_method
ALTER TABLE dbo.utb_insurance DROP CONSTRAINT ufk_insurance_deskauditpreferredcommunicationmethodid_in_communication_method
ALTER TABLE dbo.utb_shop_load DROP CONSTRAINT ufk_shop_load_preferredcommunicationmethodid_in_communication_method
ALTER TABLE dbo.utb_shop_location DROP CONSTRAINT ufk_shop_location_preferredcommunicationmethodid_in_communication_method
ALTER TABLE dbo.utb_vendor DROP CONSTRAINT ufk_vendor_preferredcommunicationmethodid_in_communication_method
ALTER TABLE dbo.utb_warranty_assignment DROP CONSTRAINT ufk_warranty_assignment_communicationmethodid_in_communication_method

-- Document Type
ALTER TABLE dbo.utb_client_required_document_type DROP CONSTRAINT ufk_client_required_document_type_documenttypeid_in_document_type
ALTER TABLE dbo.utb_bundling_document_type DROP CONSTRAINT ufk_bundling_document_type_documenttypeid_in_document_type
ALTER TABLE dbo.utb_document DROP CONSTRAINT ufk_document_documenttypeid_in_document_type
ALTER TABLE dbo.utb_deliverable DROP CONSTRAINT ufk_deliverable_documenttypeid_in_document_type
ALTER TABLE dbo.utb_form DROP CONSTRAINT ufk_form_documenttypeid_in_document_type
ALTER TABLE dbo.utb_client_document_exception DROP CONSTRAINT ufk_client_document_exception_documenttypeid_in_document_type
ALTER TABLE dbo.utb_bundling DROP CONSTRAINT ufk_bundling_documenttypeid_in_document_type

-- Profile
ALTER TABLE dbo.utb_role_profile DROP CONSTRAINT ufk_role_profile_profileid_in_profile
ALTER TABLE dbo.utb_user_profile DROP CONSTRAINT ufk_user_profile_profileid_in_profile
ALTER TABLE dbo.utb_profile_selection DROP CONSTRAINT ufk_profile_selection_profileid_in_profile
ALTER TABLE dbo.utb_application_profile DROP CONSTRAINT ufk_application_profile_profileid_in_profile

-- Service
ALTER TABLE dbo.utb_invoice_service DROP CONSTRAINT ufk_invoice_service_serviceid_in_service
ALTER TABLE dbo.utb_client_fee_definition DROP CONSTRAINT ufk_client_fee_definition_serviceid_in_service

-- Status
ALTER TABLE dbo.utb_document DROP CONSTRAINT ufk_document_statusid_in_status
ALTER TABLE dbo.utb_status_task DROP CONSTRAINT ufk_status_task_statusid_in_status
ALTER TABLE dbo.utb_claim_aspect_status DROP CONSTRAINT ufk_claim_aspect_status_statusid_in_status

-- Task
ALTER TABLE dbo.utb_task_escalation DROP CONSTRAINT ufk_task_escalation_taskid_in_task
ALTER TABLE dbo.utb_task_role DROP CONSTRAINT ufk_task_role_taskid_in_task
ALTER TABLE dbo.utb_task_delay_reason DROP CONSTRAINT ufk_task_delay_reason_taskid_in_task
ALTER TABLE dbo.utb_task_completion_criteria DROP CONSTRAINT ufk_task_completion_criteria_taskid_in_task
ALTER TABLE dbo.utb_status_task DROP CONSTRAINT ufk_status_task_taskid_in_task

PRINT '.'
PRINT '.'
PRINT 'Deleting previous data...'
PRINT '.'

DELETE FROM dbo.utb_app_variable
DELETE FROM dbo.utb_assignment_pool
DELETE FROM dbo.utb_assignment_type
DELETE FROM dbo.utb_communication_method
DELETE FROM dbo.utb_document_type
DELETE FROM dbo.utb_profile
DELETE FROM dbo.utb_service
DELETE FROM dbo.utb_status
DELETE FROM dbo.utb_status_task
DELETE FROM dbo.utb_task
DELETE FROM dbo.utb_task_assignment_pool
DELETE FROM dbo.utb_task_completion_criteria
DELETE FROM dbo.utb_task_role

PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

DECLARE @ModifiedDateTime AS DATETIME
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- This is a query against an Access Database through the OLE DB provider for Jet.

------------------------------------------------------

INSERT INTO dbo.utb_app_variable
SELECT  AppVariableID,
		Convert(varchar(500), Description),
		MaxRange,
		MinRange,
		Name,
		SubName,
		Convert(varchar(500), Value),
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_app_variable);

------------------------------------------------------

INSERT INTO dbo.utb_assignment_pool
SELECT  AssignmentPoolID,
        DisplayOrder, 
        EnabledFlag,
        FunctionCD,
        Name,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_assignment_pool);

------------------------------------------------------

INSERT INTO dbo.utb_assignment_type
SELECT  AssignmentTypeID,
        DisplayOrder, 
        EnabledFlag,
        Name,
        ServiceChannelDefaultCD,
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_assignment_type);

------------------------------------------------------

INSERT INTO dbo.utb_communication_method
SELECT  CommunicationMethodID,
        DisplayOrder, 
        EnabledFlag,
        Name,
        RoutingCD,
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_communication_method);

------------------------------------------------------

INSERT INTO dbo.utb_document_type
SELECT  DocumentTypeID,
        DisplayOrder,
        DocumentClassCD, 
        EnabledFlag,
        EstimateTypeFlag,
        Name,
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_document_type);

------------------------------------------------------

INSERT INTO dbo.utb_profile
SELECT  ProfileID,
        BusinessFunctionCD, 
        DataTypeCD,
        DefaultValue, 
        DisplayOrder,
        EnabledFlag,
        Name,
        ReconciliationCD,
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_profile);

------------------------------------------------------

INSERT INTO dbo.utb_service
SELECT  ServiceID,
        BillingModelCD,
        DisplayOrder,
        DispositionTypeCD, 
        EnabledFlag,
        ExposureRequiredFlag,
        MultipleBillingFlag,
        Name,
        PartyCD,
        ServiceChannelCD,
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_service);

------------------------------------------------------

INSERT INTO dbo.utb_status
SELECT  StatusID,
        ClaimAspectTypeID,
        DisplayOrder, 
        EnabledFlag,
        LogtoHistoryFlag,
        Name,
        ServiceChannelCD,
        StatusTypeCD,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_status);

------------------------------------------------------

INSERT INTO dbo.utb_status_task
SELECT  StatusID,
        TaskID,
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_status_task);

------------------------------------------------------

INSERT INTO dbo.utb_task
SELECT  TaskID,
        NoteTypeDefaultID,
        CommentCompleteRequiredFlag,
        CommentNARequiredFlag,
        CRDSortOrder,
        DisplayOrder,
        EnabledFlag,
        EscalationMinutes,
        Name,
        UserTaskFlag,
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_task);

------------------------------------------------------

INSERT INTO dbo.utb_task_assignment_pool
SELECT  TaskID,
        ServiceChannelCD,
        InsuranceCompanyID,
        AssignmentPoolID,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_task_assignment_pool);

------------------------------------------------------

INSERT INTO dbo.utb_task_completion_criteria
SELECT  TaskCompletionCriteriaID,
        TaskID,
        Convert(varchar(500), CriteriaSQL),
        Convert(varchar(500), FailureMessage), 
        Convert(varchar(500), ValidResultSetSQL),
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_task_completion_criteria);

------------------------------------------------------

INSERT INTO dbo.utb_task_role
SELECT  TaskID,
        RoleID,
        AlarmModificationAllowedFlag,
        CompletionAllowedFlag,
        CreationAllowedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_task_role);

------------------------------------------------------


GO
PRINT '.'
PRINT '.'
PRINT 'Modified table(s)...'
PRINT '.'

SELECT * FROM dbo.utb_app_variable
SELECT * FROM dbo.utb_assignment_pool
SELECT * FROM dbo.utb_assignment_type
SELECT * FROM dbo.utb_communication_method
SELECT * FROM dbo.utb_document_type
SELECT * FROM dbo.utb_profile
SELECT * FROM dbo.utb_service
SELECT * FROM dbo.utb_status
SELECT * FROM dbo.utb_status_task
SELECT * FROM dbo.utb_task
SELECT * FROM dbo.utb_task_assignment_pool
SELECT * FROM dbo.utb_task_completion_criteria
SELECT * FROM dbo.utb_task_role

GO

-- Assignment pool
ALTER TABLE dbo.utb_task_assignment_pool 
    ADD CONSTRAINT ufk_task_assignment_pool_assignmentpoolid_in_assignment_pool
    FOREIGN KEY (AssignmentPoolID)
    REFERENCES dbo.utb_assignment_pool(AssignmentPoolID)

ALTER TABLE dbo.utb_assignment_pool_user 
    ADD CONSTRAINT ufk_assignment_pool_user_assignmentpoolid_in_assignment_pool
    FOREIGN KEY (AssignmentPoolID)
    REFERENCES dbo.utb_assignment_pool(AssignmentPoolID)


-- Assignment type
ALTER TABLE dbo.utb_claim_aspect 
    ADD CONSTRAINT ufk_claim_aspect_initialassignmenttypeid_in_assignment_type
    FOREIGN KEY (InitialAssignmentTypeID)
    REFERENCES dbo.utb_assignment_type(AssignmentTypeID)

ALTER TABLE dbo.utb_client_assignment_type 
    ADD CONSTRAINT ufk_client_assignment_type_assignmenttypeid_in_assignment_type
    FOREIGN KEY (AssignmentTypeID)
    REFERENCES dbo.utb_assignment_type(AssignmentTypeID)

ALTER TABLE dbo.utb_office_assignment_type 
    ADD CONSTRAINT ufk_office_assignment_type_assignmenttypeid_in_assignment_type
    FOREIGN KEY (AssignmentTypeID)
    REFERENCES dbo.utb_assignment_type(AssignmentTypeID)

        
-- Communication Method
ALTER TABLE dbo.utb_appraiser 
    ADD CONSTRAINT ufk_appraiser_preferredcommunicationmethodid_in_communication_method
    FOREIGN KEY (PreferredCommunicationMethodID)
    REFERENCES dbo.utb_communication_method(CommunicationMethodID)

ALTER TABLE dbo.utb_assignment 
    ADD CONSTRAINT ufk_assignment_communicationmethodid_in_communication_method
    FOREIGN KEY (CommunicationMethodID)
    REFERENCES dbo.utb_communication_method(CommunicationMethodID)

ALTER TABLE dbo.utb_insurance 
    ADD CONSTRAINT ufk_insurance_deskauditpreferredcommunicationmethodid_in_communication_method
    FOREIGN KEY (DeskAuditPreferredCommunicationMethodID)
    REFERENCES dbo.utb_communication_method(CommunicationMethodID)

ALTER TABLE dbo.utb_shop_load 
    ADD CONSTRAINT ufk_shop_load_preferredcommunicationmethodid_in_communication_method
    FOREIGN KEY (PreferredCommunicationMethodID)
    REFERENCES dbo.utb_communication_method(CommunicationMethodID)

ALTER TABLE dbo.utb_shop_location 
    ADD CONSTRAINT ufk_shop_location_preferredcommunicationmethodid_in_communication_method
    FOREIGN KEY (PreferredCommunicationMethodID)
    REFERENCES dbo.utb_communication_method(CommunicationMethodID)

ALTER TABLE dbo.utb_vendor 
    ADD CONSTRAINT ufk_vendor_preferredcommunicationmethodid_in_communication_method
    FOREIGN KEY (PreferredCommunicationMethodID)
    REFERENCES dbo.utb_communication_method(CommunicationMethodID)

ALTER TABLE dbo.utb_warranty_assignment 
    ADD CONSTRAINT ufk_warranty_assignment_communicationmethodid_in_communication_method
    FOREIGN KEY (CommunicationMethodID)
    REFERENCES dbo.utb_communication_method(CommunicationMethodID)


-- Document Type
ALTER TABLE dbo.utb_client_required_document_type 
    ADD CONSTRAINT ufk_client_required_document_type_documenttypeid_in_document_type
    FOREIGN KEY (DocumentTypeID)
    REFERENCES dbo.utb_document_type(DocumentTypeID)

ALTER TABLE dbo.utb_bundling_document_type 
    ADD CONSTRAINT ufk_bundling_document_type_documenttypeid_in_document_type
    FOREIGN KEY (DocumentTypeID)
    REFERENCES dbo.utb_document_type(DocumentTypeID)

ALTER TABLE dbo.utb_document 
    ADD CONSTRAINT ufk_document_documenttypeid_in_document_type
    FOREIGN KEY (DocumentTypeID)
    REFERENCES dbo.utb_document_type(DocumentTypeID)

ALTER TABLE dbo.utb_deliverable 
    ADD CONSTRAINT ufk_deliverable_documenttypeid_in_document_type
    FOREIGN KEY (DocumentTypeID)
    REFERENCES dbo.utb_document_type(DocumentTypeID)

ALTER TABLE dbo.utb_form 
    ADD CONSTRAINT ufk_form_documenttypeid_in_document_type
    FOREIGN KEY (DocumentTypeID)
    REFERENCES dbo.utb_document_type(DocumentTypeID)

ALTER TABLE dbo.utb_client_document_exception 
    ADD CONSTRAINT ufk_client_document_exception_documenttypeid_in_document_type
    FOREIGN KEY (DocumentTypeID)
    REFERENCES dbo.utb_document_type(DocumentTypeID)

ALTER TABLE dbo.utb_bundling 
    ADD CONSTRAINT ufk_bundling_documenttypeid_in_document_type
    FOREIGN KEY (DocumentTypeID)
    REFERENCES dbo.utb_document_type(DocumentTypeID)


-- Profile
ALTER TABLE dbo.utb_role_profile 
    ADD CONSTRAINT ufk_role_profile_profileid_in_profile
    FOREIGN KEY (ProfileID)
    REFERENCES dbo.utb_profile(ProfileID)

ALTER TABLE dbo.utb_user_profile 
    ADD CONSTRAINT ufk_user_profile_profileid_in_profile
    FOREIGN KEY (ProfileID)
    REFERENCES dbo.utb_profile(ProfileID)

ALTER TABLE dbo.utb_profile_selection 
    ADD CONSTRAINT ufk_profile_selection_profileid_in_profile
    FOREIGN KEY (ProfileID)
    REFERENCES dbo.utb_profile(ProfileID)

ALTER TABLE dbo.utb_application_profile 
    ADD CONSTRAINT ufk_application_profile_profileid_in_profile
    FOREIGN KEY (ProfileID)
    REFERENCES dbo.utb_profile(ProfileID)


-- Service
ALTER TABLE dbo.utb_invoice_service 
    ADD CONSTRAINT ufk_invoice_service_serviceid_in_service
    FOREIGN KEY (ServiceID)
    REFERENCES dbo.utb_service(ServiceID)

ALTER TABLE dbo.utb_client_fee_definition 
    ADD CONSTRAINT ufk_client_fee_definition_serviceid_in_service
    FOREIGN KEY (ServiceID)
    REFERENCES dbo.utb_service(ServiceID)


-- Status
ALTER TABLE dbo.utb_document 
    ADD CONSTRAINT ufk_document_statusid_in_status
    FOREIGN KEY (StatusID)
    REFERENCES dbo.utb_status(StatusID)

ALTER TABLE dbo.utb_status_task 
    ADD CONSTRAINT ufk_status_task_statusid_in_status
    FOREIGN KEY (StatusID)
    REFERENCES dbo.utb_status(StatusID)

ALTER TABLE dbo.utb_claim_aspect_status 
    ADD CONSTRAINT ufk_claim_aspect_status_statusid_in_status
    FOREIGN KEY (StatusID)
    REFERENCES dbo.utb_status(StatusID)


-- Task
ALTER TABLE dbo.utb_task_escalation 
    ADD CONSTRAINT ufk_task_escalation_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task(TaskID)

ALTER TABLE dbo.utb_task_role 
    ADD CONSTRAINT ufk_task_role_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task(TaskID)

ALTER TABLE dbo.utb_task_delay_reason 
    ADD CONSTRAINT ufk_task_delay_reason_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task(TaskID)

ALTER TABLE dbo.utb_task_completion_criteria 
    ADD CONSTRAINT ufk_task_completion_criteria_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task(TaskID)

ALTER TABLE dbo.utb_status_task 
    ADD CONSTRAINT ufk_status_task_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task(TaskID)

       
PRINT '.'
PRINT '.'
PRINT 'Remember to COMMIT or ROLLBACK changes'
PRINT '!'

-- COMMIT
-- ROLLBACK

GO

