/******************************************************************************
* PA Appraiser License Fax template for all APD Clients
******************************************************************************/

begin transaction
insert into utb_form_supplement (
   FormID,
   ShopStateCode,
   EnabledFlag,
   Name,
   PDFPath,
   ServiceChannelCD,
   SQLProcedure,
   SysLastUserID,
   SysLastUpdatedDate
) 
select f.FormID,
   'PA',
   1,
   'Direction To Pay',
   'DTPType1_PA.pdf',
   'PS',
   'uspWorkflowSendShopAssignXML',
   0,
   current_timestamp
from utb_form f
left join utb_form_supplement fs on f.FormID = fs.FormID
where f.Name = 'Shop Assignment'
  and fs.PDFPath = 'DTPType1.pdf'
  and fs.Name <> 'DTPType1_PA.pdf'
  
  
insert into utb_form_supplement (
   FormID,
   ShopStateCode,
   EnabledFlag,
   Name,
   PDFPath,
   ServiceChannelCD,
   SQLProcedure,
   SysLastUserID,
   SysLastUpdatedDate
) 
select f.FormID,
   'PA',
   1,
   'Direction To Pay',
   'DTPType2_PA.pdf',
   'PS',
   'uspWorkflowSendShopAssignXML',
   0,
   current_timestamp
from utb_form f
left join utb_form_supplement fs on f.FormID = fs.FormID
where f.Name = 'Shop Assignment'
  and fs.PDFPath = 'DTPType2.pdf'
  and fs.Name <> 'DTPType2_PA.pdf'
  
-- commit
-- rollback