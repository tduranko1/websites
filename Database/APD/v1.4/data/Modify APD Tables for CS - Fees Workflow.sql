DECLARE @iInscCompanyID INT
SET @iInscCompanyID = 184 -- Texas Farm

----------------------------------------------
-- Add the Choice Shop Fee to Insc Company
----------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_client_fee WHERE InsuranceCompanyID = @iInscCompanyID AND Description = 'Choice Shop')
BEGIN
	DECLARE @iClientFeeID INT
	SET @iClientFeeID = 0
	
	-- Add Choice new Fee
	INSERT INTO utb_client_fee VALUES (9, @iInscCompanyID, 0, 'C', 'H',	NULL, 'Choice Shop', 25, NULL, CURRENT_TIMESTAMP, 1, 35.00,	NULL, 0, 1,	'Choice Shop', 0, 5871, CURRENT_TIMESTAMP, NULL, NULL, 0)
	
	-- Get the new ID for the client fee
	SELECT @iClientFeeID=ClientFeeID from utb_client_fee WHERE InsuranceCompanyID = @iInscCompanyID AND Description = 'Choice Shop'
	
	-- Add Choice Client Fee Definition
	INSERT INTO utb_client_fee_definition VALUES (@iClientFeeID, 17, 0, 1, 0, CURRENT_TIMESTAMP)
	
	SELECT 'Fee added successfully'
END
ELSE
BEGIN
	-- Fee already added
	SELECT 'Fee already added'
END

----------------------------------------------
-- Add the Choice Shop Task Assignment Pool
----------------------------------------------
IF NOT EXISTS (SELECT * FROM utb_task_assignment_pool WHERE TaskID = 6 AND ServiceChannelCD = 'CS')
BEGIN
	-- Add Task Pools for CS
	INSERT INTO utb_task_assignment_pool
	SELECT 
		TaskID
		, 'CS'
		, InsuranceCompanyID
		, AssignmentPoolID
		, SysLastUserID
		, CURRENT_TIMESTAMP 
	FROM 
		utb_task_assignment_pool 
	WHERE 
		ServiceChannelCD = 'PS'

	SELECT 'Task Pools added successfully'
END
ELSE
BEGIN
	-- Fee already added
	SELECT 'Task Pools already added'
END