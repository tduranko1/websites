-----------------------------------------------------------------------------------------------------------
-- This script will update the QBE configuration to support RRP and multiple assignments per service channel
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------  

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint

DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 304


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'


------------------------------------------------------
--Insert Client level configuration

PRINT '.'
PRINT '.'
PRINT 'Client Assignment Type...'
PRINT '.'

DELETE FROM dbo.utb_client_assignment_type 
  WHERE InsuranceCompanyID = @InsuranceCompanyID
    AND AssignmentTypeID IN (SELECT AssignmentTypeID FROM utb_assignment_type WHERE Name in ('Repair Referral'))

INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ('Repair Referral')


------------------------------------------------------

PRINT '.'
PRINT '.'
PRINT 'Client Service Channel...'
PRINT '.'

DELETE FROM dbo.utb_client_service_channel 
  WHERE InsuranceCompanyID = @InsuranceCompanyID
    AND ServiceChannelCD IN (SELECT ServiceChannelDefaultCD FROM utb_assignment_type WHERE Name in ('Repair Referral'))

INSERT INTO dbo.utb_client_service_channel
SELECT @InsuranceCompanyID,
       ServiceChannelDefaultCD,
       0,       -- ClientAuthorizesPaymentFlag
       'B',     -- Invoicing Model (bulk)
       0,
       @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ('Repair Referral')
  

------------------------------------------------------
-- Office level configurations

PRINT '.'
PRINT '.'
PRINT 'Adding Office Assignment Type...'
PRINT '.'

DELETE FROM dbo.utb_office_assignment_type 
  WHERE OfficeID IN (SELECT officeid FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
    AND AssignmentTypeId IN (SELECT AssignmentTypeID FROM utb_assignment_type WHERE Name in ('Repair Referral'))

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  o.OfficeID, 
          at.AssignmentTypeID,
          0,
          @ModifiedDateTime    
    FROM  dbo.utb_assignment_type at, dbo.utb_office o
    WHERE o.OfficeID IN (SELECT officeid FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
      AND at.Name in ('Repair Referral')

    
------------------------------------------------------
-- Workflow Configuration    

PRINT '.'
PRINT '.'
PRINT 'Deleting previous data...'
PRINT '.'


DELETE FROM dbo.utb_task_escalation
DELETE FROM dbo.utb_spawn
DELETE FROM dbo.utb_workflow

-- This is a query against an Access Database through the OLE DB provider for Jet.

------------------------------------------------------

INSERT INTO dbo.utb_workflow
SELECT  WorkflowID,
        InsuranceCompanyID,
        IgnoreDefaultFlag,
        OriginatorID,
        OriginatorTypeCD,
        0,
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_workflow)

------------------------------------------------------

SET IDENTITY_INSERT utb_spawn ON

INSERT INTO dbo.utb_spawn (SpawnID, WorkflowID, ConditionalValue, ContextNote, CustomProcName, EnabledFlag, SpawningID, SpawningServiceChannelCD, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate)
SELECT SpawnID,
       WorkflowID,
       ConditionalValue,
       ContextNote, 
       CustomProcName, 
       EnabledFlag,
       SpawningID,
       SpawningServiceChannelCD,
       SpawningTypeCD,
       0,
       @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_spawn)

SET IDENTITY_INSERT utb_spawn OFF

------------------------------------------------------

SET IDENTITY_INSERT utb_task_escalation ON

INSERT INTO dbo.utb_task_escalation (TaskEscalationID, TaskID, InsuranceCompanyID, ServiceChannelCD, EscalationMinutes, SysLastUserID, SysLastUpdatedDate)
SELECT TaskEscalationID,
       TaskID,
       InsuranceCompanyID,
       ServiceChannelCD,
       EscalationMinutes, 
       0,
       @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_task_escalation)

SET IDENTITY_INSERT utb_task_escalation OFF


-- Review affected tables

PRINT '.'
PRINT '.'
PRINT 'Review affected tables...'
PRINT '.'

SELECT * FROM dbo.utb_client_assignment_type WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_client_service_channel WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_office_assignment_type WHERE OfficeID IN (SELECT OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
SELECT * FROM dbo.utb_workflow WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_spawn WHERE WorkflowID IN (SELECT WorkflowID FROM dbo.utb_workflow WHERE InsuranceCompanyID = @InsuranceCompanyID)
SELECT * FROM dbo.utb_task_escalation 


--commit 
--rollback
                                                                                                                                              