-- Conversion script to convert 1.3.4.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Copies assignment data, changing reference key to ClaimAspectServiceChannel
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION


PRINT 'Converting utb_assignment'

SET IDENTITY_INSERT dbo.utb_assignment ON

INSERT INTO dbo.utb_assignment (
            AssignmentID, 
            AppraiserID, 
            ClaimAspectServiceChannelID, 
            CommunicationMethodID, 
            ShopLocationID, 
            AssignmentDate, 
            AssignmentReceivedDate, 
            AssignmentRemarks, 
            AssignmentSuffix, 
            CancellationDate, 
            CertifiedFirstFlag, 
            CommunicationAddress,
            ProgramTypeCD, 
            ReferenceID, 
            SearchTypeCD, 
            SelectionDate, 
            SysLastUserID, 
            SysLastUpdatedDate 
)
  SELECT  a.AssignmentID,
          a.AppraiserID,
          casc.ClaimAspectServiceChannelID,
          a.CommunicationMethodID,
          a.ShopLocationID,
          a.AssignmentDate,
          a.AssignmentDate,     -- For now, we'll make the assumption, the assignment was received on the same day as sent
          a.AssignmentRemarks,
          a.AssignmentSuffix,
          a.CancellationDate,
          a.CertifiedFirstFlag,
          a.CommunicationAddress,
          a.ProgramTypeCD,
          NULL,
          a.SearchTypeCD,
          a.SelectionDate,
          a.SysLastUserID,
          a.SysLastUpdatedDate
    FROM  dbo.Tmp_utb_assignment a
    INNER JOIN dbo.tmp_utb_claim_aspect ca ON (a.ClaimAspectID = ca.ClaimAspectID)
    INNER JOIN dbo.utb_claim_aspect_service_channel casc ON (ca.ClaimAspectID = casc.ClaimAspectID AND ca.ServiceChannelCD = casc.ServiceChannelCD)
    WHERE casc.PrimaryFlag = 1
    
SET IDENTITY_INSERT dbo.utb_assignment OFF

          
UPDATE dbo.utb_assignment 
   SET EffectiveDeductibleSentAmt = cc.CollisionDeductibleAmt
  FROM dbo.utb_assignment a
 INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
 INNER JOIN dbo.utb_claim_aspect ca
    ON casc.ClaimAspectID = ca.ClaimAspectID
 INNER JOIN dbo.Tmp_utb_claim_coverage cc
    ON ca.LynxID = cc.LynxID
WHERE ca.CoverageProfileCD = 'COLL'

UPDATE dbo.utb_assignment 
   SET EffectiveDeductibleSentAmt = cc.ComprehensiveDeductibleAmt
  FROM dbo.utb_assignment a
 INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
 INNER JOIN dbo.utb_claim_aspect ca
    ON casc.ClaimAspectID = ca.ClaimAspectID
 INNER JOIN dbo.Tmp_utb_claim_coverage cc
    ON ca.LynxID = cc.LynxID
WHERE ca.CoverageProfileCD = 'COMP'

UPDATE dbo.utb_assignment 
   SET EffectiveDeductibleSentAmt = cc.LiabilityDeductibleAmt
  FROM dbo.utb_assignment a
 INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
 INNER JOIN dbo.utb_claim_aspect ca
    ON casc.ClaimAspectID = ca.ClaimAspectID
 INNER JOIN dbo.Tmp_utb_claim_coverage cc
    ON ca.LynxID = cc.LynxID
WHERE ca.CoverageProfileCD = 'LIAB'

UPDATE dbo.utb_assignment 
   SET EffectiveDeductibleSentAmt = cc.UnderInsuredDeductibleAmt
  FROM dbo.utb_assignment a
 INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
 INNER JOIN dbo.utb_claim_aspect ca
    ON casc.ClaimAspectID = ca.ClaimAspectID
 INNER JOIN dbo.Tmp_utb_claim_coverage cc
    ON ca.LynxID = cc.LynxID
WHERE ca.CoverageProfileCD = 'UIM'

UPDATE dbo.utb_assignment 
   SET EffectiveDeductibleSentAmt = cc.UnInsuredDeductibleAmt
  FROM dbo.utb_assignment a
 INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
 INNER JOIN dbo.utb_claim_aspect ca
    ON casc.ClaimAspectID = ca.ClaimAspectID
 INNER JOIN dbo.Tmp_utb_claim_coverage cc
    ON ca.LynxID = cc.LynxID
WHERE ca.CoverageProfileCD = 'UM'

--commit
--rollback


