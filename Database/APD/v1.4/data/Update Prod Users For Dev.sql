-- This script resets role settings in dev for individuals that do not have production access.  This script is designed to be run whenever Prod is restored to dev
-- Script updates profiles for the following people:
--    Karen Swedeen (User ID 87)
--    Susanne Ivanoff  (User ID 2216)
--    Mala Bhagwandin  (User ID 3029)
--    Colleen Conway   (User ID 3297)
--    Tom Harlacher  (User ID 3313)
--    Lance Boyd  (User ID 3173)
--    Gary Keep  (User ID 931)

BEGIN TRANSACTION

-- Set all primary flags to false
UPDATE utb_user_role SET PrimaryRoleFlag = 0 WHERE UserID IN (87, 2216, 3029, 3297, 3313, 3173)

-- Karen Swedeen
INSERT INTO utb_user_role VALUES (87, 1, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (87, 6, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (87, 7, 1, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (87, 8, 0, 0, CURRENT_TIMESTAMP)

-- Susanne Ivanoff
INSERT INTO utb_user_role VALUES (2216, 1, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (2216, 6, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (2216, 7, 1, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (2216, 8, 0, 0, CURRENT_TIMESTAMP)

-- Mala Bhagwandin
INSERT INTO utb_user_role VALUES (3029, 7, 1, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (3029, 8, 0, 0, CURRENT_TIMESTAMP)

-- Colleen Conway
DECLARE @UserID INT
INSERT INTO utb_user ( OfficeID, SupervisorUserID, AssignmentBeginDate, AssignmentEndDate, ClientUserId, EmailAddress, EnabledFlag, FaxAreaCode, FaxExchangeNumber, FaxExtensionNumber, FaxUnitNumber, LastAssignmentDate, NameFirst, NameLast, NameTitle, OperatingFridayEndTime, OperatingFridayStartTime, OperatingMondayEndTime, OperatingMondayStartTime, OperatingSaturdayEndTime, OperatingSaturdayStartTime, OperatingSundayEndTime, OperatingSundayStartTime, OperatingThursdayEndTime, OperatingThursdayStartTime, OperatingTuesdayEndTime, OperatingTuesdayStartTime, OperatingWednesdayEndTime, OperatingWednesdayStartTime, PhoneAreaCode, PhoneExchangeNumber, PhoneExtensionNumber, PhoneUnitNumber, SupervisorFlag, SysLastUserID ) 
		 VALUES ( 13, NULL, NULL, NULL, NULL, 'cconway@lynxservices.com', 1, NULL, NULL, NULL, NULL, NULL, 'Colleen', 'Conway', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '270', '744', NULL, '7154', 0, 975 ) 
SELECT @UserID = SCOPE_IDENTITY()


INSERT INTO utb_user_role VALUES (@UserID, 1, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (@UserID, 6, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (@UserID, 7, 1, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (@UserID, 8, 0, 0, CURRENT_TIMESTAMP)

-- Tom Harlacher
INSERT INTO utb_user_role VALUES (3313, 1, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (3313, 6, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (3313, 7, 1, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (3313, 8, 0, 0, CURRENT_TIMESTAMP)

-- Lance Boyd
INSERT INTO utb_user_role VALUES (3173, 1, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (3173, 6, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (3173, 7, 1, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (3173, 8, 0, 0, CURRENT_TIMESTAMP)

-- Gary Keep
INSERT INTO utb_user_role VALUES (931, 1, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (931, 6, 0, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (931, 7, 1, 0, CURRENT_TIMESTAMP)
INSERT INTO utb_user_role VALUES (931, 8, 0, 0, CURRENT_TIMESTAMP)

--COMMIT
--ROLLBACK
