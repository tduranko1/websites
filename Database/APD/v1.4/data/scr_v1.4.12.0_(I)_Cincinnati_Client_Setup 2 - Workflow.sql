-- Cincinnati Insurance
--SELECT * FROM utb_workflow WHERE InsuranceCompanyID = 194

-- Workflow
-- Add/Update Workflow
IF NOT EXISTS (SELECT WorkflowID FROM utb_workflow WHERE InsuranceCompanyID = 194 AND OriginatorID = 29)
BEGIN
	INSERT INTO utb_workflow VALUES (191,194,0,29,'E',0, CURRENT_TIMESTAMP)
END

IF NOT EXISTS (SELECT WorkflowID FROM utb_workflow WHERE InsuranceCompanyID = 194 AND OriginatorID = 84)
BEGIN
	INSERT INTO utb_workflow VALUES (192,194,0,84,'E',0, CURRENT_TIMESTAMP)
END

-- Spawn - Update
-- SELECT * FROM utb_spawn
IF EXISTS (SELECT SpawnID FROM utb_spawn WHERE SpawnID = 164)
BEGIN
	UPDATE utb_spawn SET ConditionalValue = 'PS', ContextNote = '%Key%', SpawningID = 6, EnabledFlag = 1  WHERE SpawnID = 164
END

IF EXISTS (SELECT SpawnID FROM utb_spawn WHERE SpawnID = 165)
BEGIN
	UPDATE utb_spawn SET SpawningID = 35, EnabledFlag = 1 WHERE SpawnID = 165
END

IF EXISTS (SELECT SpawnID FROM utb_spawn WHERE SpawnID = 166)
BEGIN
	UPDATE utb_spawn SET ConditionalValue = 'PS', ContextNote = '%PertainsToName%', SpawningID = 2, EnabledFlag = 1  WHERE SpawnID = 166
END

-- Spawn - Add
IF NOT EXISTS (SELECT SpawnID FROM utb_spawn WHERE SpawnID = 300)
BEGIN
	INSERT INTO utb_spawn VALUES (126,'RENTAL',NULL,NULL,1,24,NULL,'T',0, CURRENT_TIMESTAMP)
END

-- Add Rental Coverage
IF NOT EXISTS (SELECT ClientCoverageTypeID FROM utb_client_coverage_type WHERE InsuranceCompanyID = 194 AND ClientCode = 'Rental')
BEGIN
	INSERT INTO utb_client_coverage_type VALUES (194,1,'Rental','RENT',10,1,'Rental',0, CURRENT_TIMESTAMP)
END



