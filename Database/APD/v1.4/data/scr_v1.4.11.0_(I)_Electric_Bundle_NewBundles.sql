DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 158
SET @FromInscCompID = 374

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_bundling b INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE [Description] LIKE '%ELECPS%') 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Assignment Cancellation'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELECPSAssignmentCancel.xsl%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'No Inspection (Stale) Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELECPSStaleAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Cash Out Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELECPSCashOutAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Status Update - Claim Rep'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELECPSStatusUpdAction.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Status Update - MDS'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELECPSStatusUpdActionMDS.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		38
		, MessageTemplateID 
		,1
		,'Total Loss Alert - (Adjuster)'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELECPSTLAlert.xsl%'

	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

