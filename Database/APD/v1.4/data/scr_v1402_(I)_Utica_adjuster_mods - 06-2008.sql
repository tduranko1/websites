
BEGIN TRANSACTION

-- Setup users for MARO office - 92

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Elisa.Lindsey@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Candice.Luther@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Daniel.Pettyjohn@uticanational.com'


-- Setup users for ERO office - 96

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Edna.Martinez@uticanational.com'


-- Setup users for Albany office - 94



-- Setup users for Amherst office - 95

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Eric.Dibble@uticanational.com'



print 'Done.'

-- COMMIT
