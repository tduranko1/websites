/*****************************************************************************
 Note Message Templates
 *****************************************************************************/
 
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
) values (
   'N',
   1,
   'Message Left|Messages/msgMessageLeft.xsl',
   NULL,
   0,
   current_timestamp
)

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
) values (
   'N',
   1,
   'Contact Made|Messages/msgContactMade.xsl',
   NULL,
   0,
   current_timestamp
)

select * from utb_message_template where AppliesToCD = 'N'