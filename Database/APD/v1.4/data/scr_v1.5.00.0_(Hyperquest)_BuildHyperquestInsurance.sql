-- Begin the transaction for dropping and creating the tables
--DROP TABLE [dbo].[utb_hyperquest_insurance]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[utb_hyperquest_insurance](
	[InsuranceCompanyID] BIGINT NOT NULL,
	[EnabledFlag] bit NOT NULL,
	[SysLastUpdatedDate] DATETIME  NOT NULL
 CONSTRAINT [PK_utb_hyperquest_insurance] PRIMARY KEY CLUSTERED 
(
	[InsuranceCompanyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [ufg_primary]
) ON [ufg_primary]
GO

SET ANSI_PADDING ON
GO

INSERT INTO utb_hyperquest_insurance VALUES (192,1,CURRENT_TIMESTAMP)
INSERT INTO utb_hyperquest_insurance VALUES (259,1,CURRENT_TIMESTAMP)
INSERT INTO utb_hyperquest_insurance VALUES (464,1,CURRENT_TIMESTAMP)
