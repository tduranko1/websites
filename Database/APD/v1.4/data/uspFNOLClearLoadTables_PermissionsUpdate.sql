-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION
-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLClearLoadTables' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspFNOLClearLoadTables TO 
        wsAPDUser
    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END
GO

