/******************************************************************************
* Add all states to the unitrin office so that they all appear on the reports
******************************************************************************/

BEGIN TRANSACTION

INSERT INTO dbo.utb_office_contract_state
(OfficeID, StateCode, UseCEIShopsFlag, SysLastUserID, SysLastUpdatedDate)
SELECT 93, StateCode, 0, 0, current_timestamp
FROM utb_state_code
WHERE StateCode not in (SELECT StateCode 
                    from utb_office_contract_state
                    where OfficeID = 93)
  and EnabledFlag = 1

-- commit
-- rollback