select ShopLocationID, AutoverseID as 'OldAutoverseID', 'P' + AutoverseID as 'NewAutoverseID'
from utb_shop_location
where AutoverseID is not null
  and substring(AutoverseID, 1, 1) <> 'P'

begin transaction


update utb_shop_location
set AutoverseID = 'P' + AutoverseID
from utb_shop_location
where AutoverseID is not null
  and substring(AutoverseID, 1, 1) <> 'P'
--  and AddressState = 'TX'


-- commit
-- rollback

print 'remember to commit/rollback'