DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 464
SET @FromInscCompID = 192

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (
	SELECT 
		*
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgCWPSClmAck.xsl%'
) 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPSClmAck.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgPSClmAck.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPSInitialEstimate.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgPSInitialEstimate.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPSClosingRepairComplete.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgPSClosingRepairComplete.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPSClosingSupplement.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgPSClosingSupplement.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWDAClosingAuditComplete.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgDAClosingAuditComplete.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWDAClosingSupplement.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgDAClosingSupplement.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWDAClosingTL.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgDAClosingTL.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPSTLClosing.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgPSTLClosing.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPSRentalInvoice.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgPSRentalInvoice.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWDAShopCoverPage.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgDAShopCoverPage.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPSApprovedEstimate_EB.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgPSApprovedEstimate_EB.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPS_RC_Release_Close_EB.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgPS_RC_Release_Close_EB.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgCWPS_RC_Combined_Bill_Release_Close_EB.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgPS_RC_Combined_Bill_Release_Close_EB.xsl%'

	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

SELECT * FROM utb_bundling WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
