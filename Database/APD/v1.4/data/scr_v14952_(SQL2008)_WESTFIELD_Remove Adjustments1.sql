DECLARE @InscCompID INT
SET @InscCompID = 387

DECLARE @BundlingID INT
DECLARE @MessageTemplateID INT
SELECT  
	@BundlingID = b.BundlingID 
	, @MessageTemplateID = mt.MessageTemplateID 
FROM 
	utb_message_template mt 
	INNER JOIN utb_bundling b 
	ON b.MessageTemplateID = mt.MessageTemplateID 
	INNER JOIN utb_client_bundling cb
	ON cb.BundlingID = b.BundlingID 
WHERE 
	cb.InsuranceCompanyID = @InscCompID
	AND [Name] = 'Initial Estimate and Photos (Adjuster)'

UPDATE dbo.utb_bundling set [Name]='Initial Estimate' where BundlingID = @BundlingID
UPDATE dbo.utb_message_template set [Description]='Initial Estimate|Messages/msgWFPInitialEstimate.xsl' where MessageTemplateID = @MessageTemplateID
UPDATE utb_bundling_document_type set DirectionalCD='I', EstimateTypeCD='O', DocumentTypeID = 3 where BundlingID = @BundlingID AND DocumentTypeID = 35
