DECLARE @InsuranceCompanyID as int
SET @InsuranceCompanyID = 374


BEGIN

UPDATE 
      utb_form
SET PDFPath = 'FaxAssignmentType374.pdf'

WHERE  
         InsuranceCompanyID = @InsuranceCompanyID
           
            AND PDFPath IN ('FaxAssignmentType3.pdf')
   END

 SELECT * FROM dbo.utb_form 
      WHERE 
             InsuranceCompanyID = @InsuranceCompanyID
       