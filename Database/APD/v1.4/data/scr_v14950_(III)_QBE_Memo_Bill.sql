/**************************************************
* QBE - Make Memo bill mandatory
**************************************************/

declare @InsuranceCompanyID as int
DECLARE @DocumentTypeIDMemoBill as int
declare @now as datetime

select @InsuranceCompanyID = InsuranceCompanyID
from utb_insurance
where Name = 'QBE Insurance'

SELECT @DocumentTypeIDMemoBill = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Memo Bill'

set @now = current_timestamp

UPDATE utb_bundling_document_type
SET MandatoryFlag = 1
WHERE DocumentTypeID = @DocumentTypeIDMemoBill

if not exists(select * 
                from dbo.utb_client_document_exception
                where InsuranceCompanyID =@InsuranceCompanyID
                  and DOcumentTypeID = @DocumentTypeIDMemoBill)
begin
    insert into utb_client_document_exception (
        InsuranceCompanyID, 
        DocumentTypeID, 
        OutputTypeCD, 
        SysLastUserID, 
        SysLastUpdatedDate)
    select
        @InsuranceCompanyID,
        @DocumentTypeIDMemoBill,
        'PDF',
        0,
        @now
end