﻿USE [udb_apd]
GO

IF NOT EXISTS(SELECT *
                FROM dbo.utb_form_supplement
                WHERE Name = 'WV Aftermarket Parts Form'  AND ServiceChannelCD = 'PS'
)
BEGIN
      INSERT INTO 
         utb_form_supplement 
      SELECT
            FormID
            , NULL
            , 'WV'
            , 1
            , NULL
            , 'WV Aftermarket Parts Form'
            , 'WV_Aftermarket_Parts_Form.pdf'
            , 'PS'
            , ''
            , 0
            , CURRENT_TIMESTAMP 
      FROM 
            utb_form_supplement 
      WHERE 
            FormID IN 
            (
                  SELECT 
                        FormID 
                  FROM 
                        utb_form
                  WHERE
                        [name] = 'Shop Assignment'
            )
            AND [Name] = 'Shop Instructions'

      SELECT 'WV Aftermarket Parts Forms added...'
END
ELSE
BEGIN
      SELECT 'WV Aftermarket Parts Forms already added...'
END
