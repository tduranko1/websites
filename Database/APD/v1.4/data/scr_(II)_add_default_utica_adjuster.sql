SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

declare @now udt_std_datetime
declare @msg varchar(100)
declare @OfficeID bigint

set @now = getdate()
declare @userid udt_std_int



-- Create Zurich's "Default" user for Autoverse

if not exists(select * from utb_user_application where logonid = 'UTICA_ADJ' and ApplicationID = 6)
begin
    --we will assign the default user to the corporate office.
    select @OfficeID = OfficeID
    from utb_office o
    where InsuranceCompanyID = 259
      and ClientOfficeID = 'MARO'
    
    if (@OfficeID is null)
    begin
        print 'No Utica office has been defined yet.'
        return
    end
    
    print 'Creating default utica adjuster...'
    begin transaction

    insert into utb_user (emailaddress,
                          enabledflag,
                          officeid, 
                          namefirst,
                          namelast,
                          supervisorflag,
                          syslastuserid,
                          syslastupdateddate)
                  values (null,
                          1,
                          @OfficeID,
                          'Utica',
                          'Adjuster',
                          0,
                          0,
                          @now)
    if (@@error <> 0)
    begin
        print 'Unable to add Utica Adjuster user to utb_user'
        rollback
        return
    end

    select @userid = max(userid) from utb_user
    
    set @msg = 'User created with userid=' + convert(varchar(10),@userid)
    print @msg
    
    insert into utb_user_application (userid,
                                      applicationid,
                                      accessbegindate,
                                      logonid,
                                      syslastuserid,
                                      syslastupdateddate)
                              values (@userid,
                                      4,
                                      @now,
                                      'UTICA_ADJ',
                                      0,
                                      @now)
                                      
    if (@@error <> 0)
    begin
        print 'Unable to add application to utica user to utb_user_application with application id = 1'
        rollback
        return
    end
    
    
    insert into utb_user_role (userid,
                               roleid,
                               primaryroleflag,
                               syslastuserid,
                               syslastupdateddate)
                       values (@userid,
                               17,
                               1,
                               0,
                               @now)
    
    if (@@error <> 0)
    begin
        print 'Unable to add role to utica user'
        rollback
        return
    end

    insert into utb_user_insurance (userid,
                               InsuranceCompanyID,                               
                               syslastuserid,
                               syslastupdateddate)
                       values (@userid,
                               259,                               
                               0,
                               @now)
    
    if (@@error <> 0)
    begin
        print 'Unable to add insurance company to utica user'
        rollback
        return
    end
    
    commit
end
    
                          
                          

