INSERT INTO dbo.utb_task_assignment_pool (
    TaskID
    , ServiceChannelCD
    , InsuranceCompanyID
    , AssignmentPoolID
    , SysLastUserID
    , SysLastUpdatedDate
)
SELECT 
    29
    , 'RRP'
    , 0
    , 1
    , 0
    , CURRENT_TIMESTAMP
