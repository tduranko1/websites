DECLARE @iInscCompanyID INT
SET @iInscCompanyID = 184 -- Texas Farm

----------------------------------------------
-- DELETE the Choice Shop Fee to Insc Company
----------------------------------------------
IF EXISTS (SELECT * FROM utb_client_fee WHERE InsuranceCompanyID = @iInscCompanyID AND Description = 'Choice Shop')
BEGIN
	DECLARE @iClientFeeID INT
	SET @iClientFeeID = 0
	
	SELECT @iClientFeeID=ClientFeeID FROM utb_client_fee WHERE InsuranceCompanyID = @iInscCompanyID AND Description = 'Choice Shop'
	
	-- Delete Choice Fee
	-- Add Choice Client Fee Definition
	--SELECT *  FROM utb_client_fee_definition WHERE ClientFeeID = @iClientFeeID AND ServiceID = 17
	DELETE FROM utb_client_fee_definition WHERE ClientFeeID = @iClientFeeID AND ServiceID = 17

	--SELECT * FROM utb_client_fee WHERE InsuranceCompanyID = @iInscCompanyID AND Description = 'Choice Shop' 
	DELETE FROM utb_client_fee WHERE InsuranceCompanyID = @iInscCompanyID AND Description = 'Choice Shop' 
	
	SELECT 'Fee deleted successfully'
END
ELSE
BEGIN
	-- Fee already added
	SELECT 'Fee already deleted'
END

----------------------------------------------
-- DELETE the Choice Shop Task Assignment Pool
----------------------------------------------
IF EXISTS (SELECT * FROM utb_task_assignment_pool WHERE TaskID = 6 AND ServiceChannelCD = 'CS')
BEGIN
	-- DELETE Task Pools for CS
	DELETE FROM utb_task_assignment_pool WHERE TaskID = 6 AND ServiceChannelCD = 'CS'

	SELECT 'Task Pools deleted successfully'
END
ELSE
BEGIN
	-- Fee already added
	SELECT 'Task Pools already deleted'
END