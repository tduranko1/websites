/****** Object:  Table [dbo].[utb_dtwh_fact_transaction_summary]    Script Date: 09/21/2015 13:20:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[utb_insurance_HQ_Ext](
	[InsuranceCompanyID] udt_std_int_small NOT NULL,
	[HQID]  VARCHAR(15) NOT NULL,
	[HQEmailAddress]  udt_web_email NOT NULL,
	[SysLastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_utb_insurance_HQ_Ext] PRIMARY KEY CLUSTERED 
(
	[InsuranceCompanyID] ASC,
	[HQID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [ufg_primary]
) ON [ufg_primary]

GO

SET ANSI_PADDING ON
GO


