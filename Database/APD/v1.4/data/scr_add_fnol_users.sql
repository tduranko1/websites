select * from utb_user_application
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'tina.amiss@uticanational.com',
    @NameFirst             = 'Tina',
    @NameLast              = 'Amiss',
    @EmailAddress          = 'tina.amiss@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '267',
    @PhoneUnitNumber       = '3027'
GO


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'cheryl.arnett@uticanational.com',
    @NameFirst             = 'Cheryl',
    @NameLast              = 'Arnett',
    @EmailAddress          = 'cheryl.arnett@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6662'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'terri.bishop@uticanational.com',
    @NameFirst             = 'Terri',
    @NameLast              = 'Bishop',
    @EmailAddress          = 'terri.bishop@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6619'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'janet.bridgman@uticanational.com',
    @NameFirst             = 'Janet',
    @NameLast              = 'Bridgman',
    @EmailAddress          = 'janet.bridgman@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6687'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'robert.capps@uticanational.com',
    @NameFirst             = 'Robert',
    @NameLast              = 'Capps',
    @EmailAddress          = 'robert.capps@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '3029'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'sylvia.carter@uticanational.com',
    @NameFirst             = 'Sylvia',
    @NameLast              = 'Carter',
    @EmailAddress          = 'sylvia.carter@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '3037'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'dienetha.cook@uticanational.com',
    @NameFirst             = 'Dienetha',
    @NameLast              = 'Cook',
    @EmailAddress          = 'dienetha.cook@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6670'
GO

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'kenneth.couch@uticanational.com',
    @NameFirst             = 'Kenneth',
    @NameLast              = 'Couch',
    @EmailAddress          = 'kenneth.couch@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '267',
    @PhoneUnitNumber       = '3028'
GO

		
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'julia.evans@uticanational.com',
    @NameFirst             = 'Julia',
    @NameLast              = 'Evans',
    @EmailAddress          = 'julia.evans@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6646'
GO

	
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'paul.graf@uticanational.com',
    @NameFirst             = 'Paul',
    @NameLast              = 'Graf',
    @EmailAddress          = 'paul.graf@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6660'
GO

	 	
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'eric.hearn@uticanational.com',
    @NameFirst             = 'Eric',
    @NameLast              = 'Hearn',
    @EmailAddress          = 'eric.hearn@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '3036'
GO

		
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'sheila.hicks@uticanational.com',
    @NameFirst             = 'Sheila',
    @NameLast              = 'Hicks',
    @EmailAddress          = 'sheila.hicks@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6653'
GO

	 	
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'brenda.jackson@uticanational.com',
    @NameFirst             = 'Brenda',
    @NameLast              = 'Jackson',
    @EmailAddress          = 'brenda.jackson@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6604'
GO

	 	
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'candice.luther@uticanational.com',
    @NameFirst             = 'Candice',
    @NameLast              = 'Luther',
    @EmailAddress          = 'candice.luther@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6664'
GO

	 	
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'michele.merrick@uticanational.com',
    @NameFirst             = 'Michele',
    @NameLast              = 'Merrick',
    @EmailAddress          = 'michele.merrick@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6637'
GO

		
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'daniel.pettyjohn@uticanational.com',
    @NameFirst             = 'Daniel',
    @NameLast              = 'Pettyjohn',
    @EmailAddress          = 'daniel.pettyjohn@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6661'
GO

		
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'carolyn.rawlings@uticanational.com',
    @NameFirst             = 'Carolyn',
    @NameLast              = 'Rawlings',
    @EmailAddress          = 'carolyn.rawlings@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6669'
GO

	
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'scott.rose@uticanational.com',
    @NameFirst             = 'Scott',
    @NameLast              = 'Rose',
    @EmailAddress          = 'scott.rose@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6694'
GO

	 
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'leanne.schmidt@uticanational.com',
    @NameFirst             = 'Leanne',
    @NameLast              = 'Schmidt',
    @EmailAddress          = 'leanne.schmidt@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6657'
GO

	 
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'jessica.stuart@uticanational.com',
    @NameFirst             = 'Jessica',
    @NameLast              = 'Stuart',
    @EmailAddress          = 'jessica.stuart@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6651'
GO

	
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'bill.swertfeger@uticanational.com',
    @NameFirst             = 'Bill',
    @NameLast              = 'Swertfeger',
    @EmailAddress          = 'bill.swertfeger@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6671'
GO

	 
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'jennifer.tomlin@uticanational.com',
    @NameFirst             = 'Jennifer',
    @NameLast              = 'Tomlin',
    @EmailAddress          = 'jennifer.tomlin@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6667'
GO

	
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'linda.walls@uticanational.com',
    @NameFirst             = 'Linda',
    @NameLast              = 'Walls',
    @EmailAddress          = 'linda.walls@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6672'
GO


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'beverly.williamson@uticanational.com',
    @NameFirst             = 'Beverly',
    @NameLast              = 'Williamson',
    @EmailAddress          = 'beverly.williamson@uticanational.com',
    @OfficeID              = '92',
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6665'
GO

select * from utb_user_application
