DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
DECLARE @BundleID SMALLINT
DECLARE @DocumentTypeID SMALLINT

SET @ToInscCompID = 575
SET @FromInscCompID = 194
SET @BundleID = 0
SET @DocumentTypeID = 0

------------------------------
-- Add Bundling Document Type
------------------------------
-- --DRP - Assignment Cancellation
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID 
	INNER JOIN utb_bundling_document_type bdt ON bdt.BundlingID = b.BundlingID 
    WHERE b.Name = 'Assignment Cancellation' AND cb.InsuranceCompanyID = @ToInscCompID  and bdt.DocumentTypeID in (36)

)
BEGIN
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Assignment Cancellation' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	PRINT 'Bundling Document Type Assignment Cancellation added...'
END
ELSE
BEGIN
	PRINT 'Bundling Document Type Assignment Cancellation Already exist...'
END

-- Closing - Total Loss
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID 
	INNER JOIN utb_bundling_document_type bdt ON bdt.BundlingID = b.BundlingID 
    WHERE b.Name = 'Closing - Total Loss' AND cb.InsuranceCompanyID = @ToInscCompID  and bdt.DocumentTypeID in (8,3,82)
)
BEGIN
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Closing - Total Loss' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,3,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,1,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,8,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,82,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0,CURRENT_TIMESTAMP)
	PRINT 'Bundling Document Type Closing - Total Loss added...'
END
ELSE
BEGIN
	PRINT 'Bundling Document Type Closing - Total Loss Already exist'
END

-- Closing Repair Complete
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID 
	INNER JOIN utb_bundling_document_type bdt ON bdt.BundlingID = b.BundlingID 
    WHERE b.Name = 'Closing Repair Complete' AND cb.InsuranceCompanyID = @ToInscCompID  and bdt.DocumentTypeID in (2,3,8,9,13,51)
)
BEGIN
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Closing Repair Complete' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,2,NULL,'I',1,0,0,NULL,0,NULL,1,2,NULL,0,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,3,NULL,'I',0,0,1,NULL,1,NULL,1,3,NULL,0,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,8,NULL,'I',0,1,0,NULL,0,NULL,1,4,NULL,0,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,9,NULL,'I',0,0,0,NULL,0,NULL,0,0,NULL,0,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,51,NULL,'I',0,0,0,NULL,0,NULL,1,5,NULL,0,0,0,CURRENT_TIMESTAMP)
	SELECT 'Bundling Document Type Closing Repair Complete added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type Closing Repair Complete Already exist...'
END

-- DRP - Closing Supplement
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID 
	INNER JOIN utb_bundling_document_type bdt ON bdt.BundlingID = b.BundlingID 
    WHERE b.Name = 'Closing Supplement' AND cb.InsuranceCompanyID = @ToInscCompID  and bdt.DocumentTypeID in (8,10,13)
)
BEGIN
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Closing Supplement' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,8,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,10,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,1,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	SELECT 'Bundling Document Type Closing Supplement added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type Closing Supplement Already exist'
END

-- DRP - Initial Estimate and Photos (Adjuster)
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID 
	INNER JOIN utb_bundling_document_type bdt ON bdt.BundlingID = b.BundlingID 
    WHERE b.Name = 'Initial Estimate and Photos (Adjuster)' AND cb.InsuranceCompanyID = @ToInscCompID  and bdt.DocumentTypeID in (3,8)
)
BEGIN
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Initial Estimate and Photos (Adjuster)' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,3,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,1,0,0,CURRENT_TIMESTAMP)
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,8,NULL,'I',0,1,0,NULL,0,NULL,1,2,NULL,0,0,0,CURRENT_TIMESTAMP)
	PRINT 'Bundling Document Type Initial Estimate and Photos (Adjuster) added...'
END
ELSE
BEGIN
	PRINT 'Bundling Document Type Initial Estimate and Photos (Adjuster) Already exist'
END

-- DRP - No Inspection (Stale) Alert & Close
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID 
	INNER JOIN utb_bundling_document_type bdt ON bdt.BundlingID = b.BundlingID 
    WHERE b.Name = 'No Inspection (Stale) Alert & Close' AND cb.InsuranceCompanyID = @ToInscCompID  and bdt.DocumentTypeID in (37)
)
BEGIN
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'No Inspection (Stale) Alert & Close' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,37,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	PRINT 'Bundling Document Type No Inspection (Stale) Alert & Close added...'
END
ELSE
BEGIN
	PRINT 'Bundling Document Type No Inspection (Stale) Alert & Close Already exist'
END
-- DRP - Rental Invoice
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID 
	INNER JOIN utb_bundling_document_type bdt ON bdt.BundlingID = b.BundlingID 
    WHERE b.Name = 'Rental Invoice' AND cb.InsuranceCompanyID = @ToInscCompID  and bdt.DocumentTypeID in (9)
)
BEGIN
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Rental Invoice' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,9,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	PRINT 'Bundling Document Type Rental Invoice added...'
END
ELSE
BEGIN
	PRINT 'Bundling Document Type Rental Invoice Already exist'
END
	
-- Status Update - Claim Rep	
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID 
	INNER JOIN utb_bundling_document_type bdt ON bdt.BundlingID = b.BundlingID 
    WHERE b.Name = 'Status Update - Claim Rep' AND cb.InsuranceCompanyID = @ToInscCompID  and bdt.DocumentTypeID in (36)
)
BEGIN
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Status Update - Claim Rep' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	PRINT 'Bundling Document Type Status Update - Claim Rep added...'
END
ELSE
BEGIN
	PRINT 'Bundling Document Type Status Update - Claim Rep Already exist'
END

-- Total Loss Alert - (Adjuster)
IF NOT EXISTS (
	SELECT b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID 
	INNER JOIN utb_bundling_document_type bdt ON bdt.BundlingID = b.BundlingID 
    WHERE b.Name = 'Total Loss Alert - (Adjuster)' AND cb.InsuranceCompanyID = @ToInscCompID  and bdt.DocumentTypeID in (38)
)
BEGIN
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'Total Loss Alert - (Adjuster)' AND cb.InsuranceCompanyID = @ToInscCompID
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,38,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0,CURRENT_TIMESTAMP)
	PRINT 'Bundling Document Type Total Loss Alert - (Adjuster) added...'
END
ELSE
BEGIN
	PRINT 'Bundling Document Type Total Loss Alert - (Adjuster) Already exist'
END
    


