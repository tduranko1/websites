DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 464

------------------------------
-- Add Claimpoint Assignments
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM
		utb_client_service_channel 
	WHERE 
		InsuranceCompanyID = @ToInscCompID
		AND ServiceChannelCD = 'DA'
)
BEGIN
	INSERT INTO 
		utb_client_assignment_type 
	VALUES
	(
		@ToInscCompID
		, 7
		, 0
		, CURRENT_TIMESTAMP
	)

	INSERT INTO 
		utb_client_assignment_type 
	VALUES
	(
		@ToInscCompID
		, 8
		, 0
		, CURRENT_TIMESTAMP
	)

	INSERT INTO 
		utb_client_service_channel 
	VALUES
	(
		@ToInscCompID
		, 'DA'
		, 0	
		, 'C'	
		, 0
		, CURRENT_TIMESTAMP
	)

	SELECT 'Claimpoint Assignments added...'
END
ELSE
BEGIN
	SELECT 'Claimpoint Assignments already added...'
END
SELECT * FROM utb_client_assignment_type WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
SELECT * FROM utb_client_service_channel WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
