-----------------------------------------------------------------------------------------------------------
-- This script will add Program Shop Service Channel to LAFB
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @FaxTemplateID          int


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 279

PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'

-- Update the Total Loss Threshold
UPDATE utb_insurance
SET TotalLossValuationWarningPercentage = 0.6,
    TotalLossWarningPercentage = 0.6,
    ReturnDocRoutingCD = 'FTP'
WHERE InsuranceCompanyID = @InsuranceCompanyID


PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

-- add Desk Audit to the client service channel list
INSERT INTO utb_client_service_channel
(InsuranceCompanyID, ServiceChannelCD, ClientAuthorizesPaymentFlag, InvoicingModelBillingCD, SysLastUserID, SysLastUpdatedDate)
VALUES
(@InsuranceCompanyID, 'PS', 0, 'C', 0, @ModifiedDateTime)


--Insert Client level configuration
-- Add Program Shop at the client level.
------------------------------------------------------
-- Add Program Shop
INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE AssignmentTypeID = 4

-- Insert Client Contract State (All States)
INSERT INTO dbo.utb_client_contract_state
SELECT @InsuranceCompanyID,
       StateCode,
       0,
       0,
       @ModifiedDateTime
FROM dbo.utb_state_code
WHERE StateCode not in (SELECT StateCode
                        FROM dbo.utb_client_contract_state
                        WHERE InsuranceCompanyID = @InsuranceCompanyID)

-- Add the Business state (list of program shops for reporting purposes)
IF NOT EXISTS (SELECT StateCode
               FROM dbo.utb_client_business_state
               WHERE InsuranceCompanyID = @InsuranceCompanyID
                 AND StateCode = 'LA')
BEGIN
   INSERT INTO dbo.utb_client_business_state
   SELECT @InsuranceCompanyID,
          'LA',
          0,
          @ModifiedDateTime
END

-- Fax template
INSERT INTO dbo.utb_form (
   DocumentTypeID,
   InsuranceCompanyID,
   EnabledFlag,
   Name,
   PDFPath,
   PertainsToCD,
   ServiceChannelCD,
   SQLProcedure,
   SystemFlag,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   22,                              -- DocumentTypeID,
   @InsuranceCompanyID,             -- InsuranceCompanyID,
   1,                               -- EnabledFlag,
   'Shop Assignment',               -- Name,
   'FaxAssignmentType1.pdf',        -- PDFPath,
   'S',                             -- PertainsToCD,
   'PS',                            -- ServiceChannelCD,
   'uspWorkflowSendShopAssignXML',  -- SQLProcedure,
   1,                               -- SystemFlag
   0,                               -- SysLastUserID,
   @ModifiedDateTime                -- SysLastUpdatedDate
)

SET @FaxTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_form_supplement (
   FormID,
   ShopStateCode,
   EnabledFlag,
   Name,
   PDFPath,
   ServiceChannelCD,
   SQLProcedure,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @FaxTemplateID,      -- FormID,
   NULL,                -- ShopStateCode,
   1,                   -- EnabledFlag,
   'Expanded Comments', -- Name,
   'FaxXComments.pdf',  -- PDFPath,
   NULL,                -- ServiceChannelCD,
   '',                  -- SQLProcedure,
   0,                   -- SysLastUserID,
   @ModifiedDateTime    -- SysLastUpdatedDate
)

INSERT INTO utb_form_supplement (
   FormID,
   ShopStateCode,
   EnabledFlag,
   Name,
   PDFPath,
   ServiceChannelCD,
   SQLProcedure,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @FaxTemplateID,      -- FormID,
   NULL,                -- ShopStateCode,
   1,                   -- EnabledFlag,
   'Shop Instructions', -- Name,
   'ShopInstructions279.pdf',  -- PDFPath,
   NULL,                -- ServiceChannelCD,
   '',                  -- SQLProcedure,
   0,                   -- SysLastUserID,
   @ModifiedDateTime    -- SysLastUpdatedDate
)

INSERT INTO utb_form_supplement (
   FormID,
   ShopStateCode,
   EnabledFlag,
   Name,
   PDFPath,
   ServiceChannelCD,
   SQLProcedure,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @FaxTemplateID,      -- FormID,
   NULL,                -- ShopStateCode,
   1,                   -- EnabledFlag,
   'Direction to Pay', -- Name,
   'DTPType1.pdf',  -- PDFPath,
   NULL,                -- ServiceChannelCD,
   'uspWorkflowSendShopAssignXML', -- SQLProcedure,
   0,                   -- SysLastUserID,
   @ModifiedDateTime    -- SysLastUpdatedDate
)

-- NY COAR Form
INSERT INTO utb_form_supplement (
   FormID,
   ShopStateCode,
   EnabledFlag,
   Name,
   PDFPath,
   ServiceChannelCD,
   SQLProcedure,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @FaxTemplateID,      -- FormID,
   'NY',                -- ShopStateCode,
   1,                   -- EnabledFlag,
   'NY COAR Form',      -- Name,
   'NY_COAR.pdf',       -- PDFPath,
   'PS',                -- ServiceChannelCD,
   'uspWorkflowSendShopAssignXML', -- SQLProcedure,
   0,                   -- SysLastUserID,
   @ModifiedDateTime    -- SysLastUpdatedDate
)

-- CT Customer Choice 

INSERT INTO utb_form_supplement (
   FormID,
   ShopStateCode,
   EnabledFlag,
   Name,
   PDFPath,
   ServiceChannelCD,
   SQLProcedure,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @FaxTemplateID,      -- FormID,
   'NY',                -- ShopStateCode,
   1,                   -- EnabledFlag,
   'CT Customer Choice',      -- Name,
   'CTAoClaimantConsumerChoice.pdf',       -- PDFPath,
   'PS',                -- ServiceChannelCD,
   '',                  -- SQLProcedure,
   0,                   -- SysLastUserID,
   @ModifiedDateTime    -- SysLastUpdatedDate
)

-- TODO: Document Bundling

------------------------------------------------------
UPDATE utb_office
SET ReturnDocDestinationValue = 'sftp://lynx:password@65.183.99.138:2121'
WHERE InsuranceCompanyID = @InsuranceCompanyID

-- Office level configurations
-- We will add the Program Shop to each office

-- LAFB - Primary Office
SET @OfficeID = NULL

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID

IF @OfficeID IS NOT NULL
BEGIN
   -- Add Program shop to the office
    INSERT INTO dbo.utb_office_assignment_type
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE AssignmentTypeID = 4
END    

-- Review affected tables
SELECT * FROM utb_client_service_channel where InsuranceCompanyID = @InsuranceCompanyID

SELECT * FROM dbo.utb_client_assignment_type where InsuranceCompanyID = @InsuranceCompanyID

SELECT oat.*
FROM dbo.utb_office_assignment_type oat
left join utb_office o on oat.OfficeID = o.OfficeID
where o.InsuranceCompanyID = @InsuranceCompanyID

--commit 
--rollback
