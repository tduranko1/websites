/*****************************************************************************************
Add CA, OR, WA states to contracted states for Unitrin.
Requested by: Tom Harlacher
Requested via: Client Profile setup worksheet
Requested date: 02/26/2008
******************************************************************************************/

DECLARE @InsuranceCompanyID as int

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Unitrin Business Insurance'

BEGIN TRANSACTION

INSERT INTO utb_client_contract_state
(InsuranceCompanyID, StateCode, UseCEIShopsFlag, SysLastUserID, SysLastUpdatedDate)
SELECT @InsuranceCompanyID, 'CA', 0, 0, CURRENT_TIMESTAMP

INSERT INTO utb_client_contract_state
(InsuranceCompanyID, StateCode, UseCEIShopsFlag, SysLastUserID, SysLastUpdatedDate)
SELECT @InsuranceCompanyID, 'OR', 0, 0, CURRENT_TIMESTAMP

INSERT INTO utb_client_contract_state
(InsuranceCompanyID, StateCode, UseCEIShopsFlag, SysLastUserID, SysLastUpdatedDate)
SELECT @InsuranceCompanyID, 'WA', 0, 0, CURRENT_TIMESTAMP

-- View the changes
SELECT * FROM utb_client_contract_state
WHERE InsuranceCompanyID = @InsuranceCompanyID

-- Remember to commit/rollback
-- COMMIT
-- ROLLBACK


