-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * utb_client_coverage_type
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION


UPDATE dbo.utb_client_coverage_type
   SET ClientCode = CoverageProfileCD

-- Add for Allstate

UPDATE dbo.utb_client_coverage_type
   SET ClientCode = 'HH'
 WHERE ClientCoverageTypeID = 59

INSERT INTO dbo.utb_client_coverage_type ( InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 185, 1, 'CH', 'COMP', 2, 1, 'Custom Comprehensive', 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_client_coverage_type ( InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 185, 1, 'HS', 'COMP', 3, 1, 'Glass Endorsement', 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_client_coverage_type ( InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 185, 1, 'ZA', 'COMP', 4, 1, 'Mobile Electronic Endorsement', 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_client_coverage_type ( InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 185, 1, 'ZB', 'COMP', 5, 1, 'Specific Mobile Electronic Endorsement', 0, CURRENT_TIMESTAMP ) 
go
INSERT INTO dbo.utb_client_coverage_type ( InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate ) 
		 VALUES ( 185, 1, 'ZZ', 'COMP', 6, 1, 'Media', 0, CURRENT_TIMESTAMP ) 
go

-- Add Rental for active program shop clients

INSERT INTO dbo.utb_client_coverage_type ( InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate ) 
		 SELECT  i.InsuranceCompanyID, 1, 'Rental', 'RENT', 10, 1, 'Rental', 0, CURRENT_TIMESTAMP 
           FROM  dbo.utb_insurance i WHERE i.InsuranceCompanyID IN (99, 145, 154, 158, 178, 184, 192, 193, 198, 209, 240, 245)
go


PRINT 'utb_client_coverage_type conversion COMPLETE!'

--commit
--rollback


