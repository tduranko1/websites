/*******************************************************************
* Add NY COAR form to all shop assignments
*******************************************************************/

insert into utb_form_supplement
(
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    ServiceChannelCD,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
)
select FormID, 
        'NY', 
        1, 
        'NY COAR Form',
        'NY_COAR.pdf',
        'PS',
        'uspWorkflowSendShopAssignXML',
        0,
        current_timestamp
from utb_form
where InsuranceCompanyID is not null
  and Name = 'Shop Assignment'
