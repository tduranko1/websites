-- Begin the transaction for dropping and creating the stored procedure

/****** Object:  Table [dbo].[utb_apd_event_log]    Script Date: 02/24/2012 15:12:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[utb_apd_event_log]    Script Date: 10/01/2012 11:17:41 ******/
CREATE TABLE [dbo].[utb_apd_event_log](
	[EventID] [udt_std_id_big] IDENTITY(1,1) NOT NULL,
	[EventType] [varchar](50) NOT NULL,
	[EventStatus] [varchar](50) NOT NULL,
	[EventDescription] [varchar](100) NOT NULL,
	[EventDetailedDescription] [varchar](1000) NULL,
	[EventXML] [text] NULL,
	[SysLastUserID] [udt_std_id] NULL,
	[SysLastUpdatedDate] [udt_sys_last_updated_date] NULL
) ON [ufg_primary] TEXTIMAGE_ON [ufg_primary]

GO

SET ANSI_PADDING ON
GO

-- Create stored procedures
