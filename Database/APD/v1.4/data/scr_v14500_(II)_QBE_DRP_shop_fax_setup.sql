
DECLARE @FormID as int
DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime

SET @now = CURRENT_TIMESTAMP

SET @InsuranceCompanyID = 304


BEGIN TRANSACTION
-- -----------------------------------------------------
-- Setup fax template for QBE
-- -----------------------------------------------------


-- Insert QBE (304) Shop Fax Assignment Type
insert into utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    ServiceChannelCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
)
select  22,
        @InsuranceCompanyID,
        1,
        'Shop Assignment',
        'FaxAssignmentType4.pdf',
        'S',
        'PS',
        'uspWorkflowSendShopAssignXML',
        1,
        0,
        current_timestamp


-- set scope identity
SET @FormID = SCOPE_IDENTITY()


-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    NULL,					-- ShopStateCode
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    NULL,					-- ShopStateCode
    1,                      -- EnabledFlag,
    'Shop Instructions',    -- Name,
    'ShopInstructions304.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

-- Add the direction to pay page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    NULL,					-- ShopStateCode
    1,                      -- EnabledFlag,
    'Direction to Pay',    -- Name,
    'DTPType3.pdf',     -- PDFPath,
    'uspWorkflowSendShopAssignXML',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)


-- Add the NY page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    ServiceChannelCD,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    'NY',					-- ShopStateCode
    1,                      -- EnabledFlag,
    'NY COAR Form',    -- Name,
    'NY_COAR.pdf',     -- PDFPath,
    'PS',					-- ServiceChannelCD
    'uspWorkflowSendShopAssignXML',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)


-- Add the CT page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    ServiceChannelCD,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    'CT',					-- ShopStateCode
    1,                      -- EnabledFlag,
    'CT Customer Choice',    -- Name,
    'CTAoClaimantConsumerChoice.pdf',     -- PDFPath,
    'PS',								-- ServiceChannelCD
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)


-- Add the PA page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    ServiceChannelCD,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    'PA',					-- ShopStateCode
    1,                      -- EnabledFlag,
    'Direction To Pay',    -- Name,
    'DTPType3_PA.pdf',     -- PDFPath,
    'PS',						-- ServiceChannelCD
    'uspWorkflowSendShopAssignXML',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)


-- Add the repair acceptance page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    ServiceChannelCD,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    NULL,					-- ShopStateCode
    1,                      -- EnabledFlag,
    'Repair Acceptance',    -- Name,
    'RepairAcceptance.pdf',     -- PDFPath,
    NULL,						-- ServiceChannelCD
    'uspWorkflowSendShopAssignXML',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)


/*
select *
from utb_form
where InsuranceCompanyID = 304

select * 
from utb_form_supplement 
where formID in
	(select formID
	 from utb_form
	 where InsuranceCompanyID = 304)
	 */


-- remember to commit/rollback

commit
-- rollback