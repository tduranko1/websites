/******************************************************************************
* Add NY COAR form for all PS Closing Repair Complete bundling profiles.
* Add CT Consumer Choice form for all PS Closing Repair Complete bundling 
	profiles.
******************************************************************************/

DECLARE @now as datetime
DECLARE @DocumentTypeID_NY_COAR as int
DECLARE @DocumentTypeID_CT_Consumer_Choice as int

SET @now = CURRENT_TIMESTAMP

SELECT @DocumentTypeID_NY_COAR = DocumentTypeID
FROM utb_document_type
WHERE Name = 'NY COAR Form'

IF @DocumentTypeID_NY_COAR IS NULL
BEGIN
	PRINT 'Missing Document Type for "NY COAR Form"'
	RETURN
END

SELECT @DocumentTypeID_CT_Consumer_Choice = DocumentTypeID
FROM utb_document_type
WHERE Name = 'CT Consumer Choice Form'

IF @DocumentTypeID_NY_COAR IS NULL
BEGIN
	PRINT 'Missing Document Type for "CT Consumer Choice Form"'
	RETURN
END

BEGIN TRANSACTION

-- Add NY COAR Form to Closing Repair Complete and Closing Repair Complete (Non-Photos) 
-- for all PS bundling profiles

INSERT INTO utb_bundling_document_type (
	BundlingID,
	DocumentTypeID,
	DirectionalCD,
	DirectionToPayFlag,
	DuplicateFlag,
	EstimateDuplicateFlag,
	EstimateTypeCD,
	FinalEstimateFlag,
	LossState,
	MandatoryFlag,
	SelectionOrder,
	ShopState,
	VANFlag,
	SysLastUserID,
	SysLastUpdatedDate
)
SELECT	tmp.BundlingID, 
		@DocumentTypeID_NY_COAR, 
		'I',
		0,
		1,
		0,
		NULL,
		0,
		NULL,
		1,
		6,
		'NY',
		0,
		0,
		@now
from (select distinct cb.BundlingID
		from utb_client_bundling cb
		left join utb_bundling b on cb.BundlingID = b.BundlingID
		where cb.ServiceChannelCD = 'PS'
		  and b.name in ('Closing Repair Complete', 'Closing Repair Complete (Non-Photos)')) as tmp

-- Add CT Consumer Choice Form to Closing Repair Complete and Closing Repair Complete (Non-Photos) 
-- for all PS bundling profiles

INSERT INTO utb_bundling_document_type (
	BundlingID,
	DocumentTypeID,
	DirectionalCD,
	DirectionToPayFlag,
	DuplicateFlag,
	EstimateDuplicateFlag,
	EstimateTypeCD,
	FinalEstimateFlag,
	LossState,
	MandatoryFlag,
	SelectionOrder,
	ShopState,
	VANFlag,
	SysLastUserID,
	SysLastUpdatedDate
)
SELECT	tmp.BundlingID, 
		@DocumentTypeID_CT_Consumer_Choice, 
		'I',
		0,
		1,
		0,
		NULL,
		0,
		NULL,
		1,
		6,
		'CT',
		0,
		0,
		@now
from (select distinct cb.BundlingID
		from utb_client_bundling cb
		left join utb_bundling b on cb.BundlingID = b.BundlingID
		where cb.ServiceChannelCD = 'PS'
		  and b.name in ('Closing Repair Complete', 'Closing Repair Complete (Non-Photos)')) as tmp


select b.BundlingID, b.Name, bdt.*
from utb_bundling b
left join utb_bundling_document_type bdt on b.BundlingID = bdt.BundlingID 
where b.name in ('Closing Repair Complete', 'Closing Repair Complete (Non-Photos)')
order by b.bundlingID, bdt.SelectionOrder 

-- COMMIT
-- ROLLBACK
