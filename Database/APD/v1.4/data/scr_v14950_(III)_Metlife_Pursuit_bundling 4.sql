/******************************************************************************
* Metlife Bundling profiles
******************************************************************************/

DECLARE @Now as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int
DECLARE @ClientBundlingID as int

SET @NOW = CURRENT_TIMESTAMP
SET @InsuranceCompanyID = 176

BEGIN TRANSACTION

--------------------------------------
-- Acknowledgement of Claim
--------------------------------------
SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Audit Authorization Request - No Photos'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'mds@metlife.com; Choice1@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID


--------------------------------------
-- Closing - Audit Not Authorized
--------------------------------------
SET @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Closing - Audit Not Authorized'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'mds@metlife.com; Choice1@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID


--------------------------------------
-- Closing - Not Agreed Audit Complete
--------------------------------------
SET @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Closing - Not Agreed Audit Complete'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'mds@metlife.com; Choice1@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID


--------------------------------------
-- Closing - Total Loss Audit Complete
--------------------------------------
SET @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Closing - Total Loss Audit Complete'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'mds@metlife.com; Choice1@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID


--------------------------------------
-- Closing - Agreed Supplement Complete
--------------------------------------
SET @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Closing - Agreed Supplement Complete'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'metlifeautoestimatespayment@metlife.com; Choice1@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID


--------------------------------------
-- Closing - Not Agreed Supplement Complete
--------------------------------------
SET @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Closing - Not Agreed Supplement Complete'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'mds@metlife.com; Choice1@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID


-----------------------------------------------------
-- Capitalize some of the key terms as in Tom's spec
-----------------------------------------------------

UPDATE utb_bundling
SET Name = REPLACE(Name, 'Agreed', 'AGREED')
FROM utb_bundling b
INNER JOIN utb_client_bundling cb ON b.BundlingID = cb.BundlingID
WHERE cb.InsuranceCompanyID = @InsuranceCompanyID
  AND b.Name like 'Closing - Agreed%'
  
UPDATE utb_bundling
SET Name = REPLACE(Name, 'Not Agreed', 'NON-AGREED')
FROM utb_bundling b
INNER JOIN utb_client_bundling cb ON b.BundlingID = cb.BundlingID
WHERE cb.InsuranceCompanyID = @InsuranceCompanyID
  AND b.Name like 'Closing - Not Agreed%'

UPDATE utb_bundling
SET Name = REPLACE(Name, 'TOTAL LOSS', 'TOTAL LOSS')
FROM utb_bundling b
INNER JOIN utb_client_bundling cb ON b.BundlingID = cb.BundlingID
WHERE cb.InsuranceCompanyID = @InsuranceCompanyID
  AND b.Name like 'Closing - Total Loss Audit%'
  

COMMIT TRANSACTION