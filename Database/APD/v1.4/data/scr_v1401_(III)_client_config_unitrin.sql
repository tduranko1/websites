-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configurations for Unitrin
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @PaymentCompanyID       smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 261
SET @PaymentCompanyID = 788


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'


PRINT '.'
PRINT '.'
PRINT 'Deleting previous data...'
PRINT '.'


DELETE FROM dbo.utb_spawn
DELETE FROM dbo.utb_workflow


PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

--Insert Insurance Company information.

IF NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
    INSERT INTO dbo.utb_insurance
        (
         InsuranceCompanyID, 
         DeskAuditPreferredCommunicationMethodID,
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip, 
         AssignmentAtSelectionFlag,
         BillingModelCD, 
         BusinessTypeCD,
         CarrierLynxContactPhone,
         CFLogoDisplayFlag,
         ClaimPointCarrierRepSelFlag,
         ClaimPointDocumentUploadFlag, 
         ClientAccessFlag,
         DemoFlag,
         DeskReviewLicenseReqFlag, 
         EnabledFlag,
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         FedTaxId,
         IngresAccountingId,
         InvoicingModelPaymentCD, 
         InvoiceMethodCD,
         LicenseDeterminationCD, 
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocDestinationCD,
         ReturnDocPackageTypeCD,
         ReturnDocRoutingCD, 
         TotalLossValuationWarningPercentage, 
         TotalLossWarningPercentage, 
         WarrantyPeriodRefinishMinCD,
         WarrantyPeriodWorkmanshipMinCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,      -- InsuranceCompanyID
                NULL,                     -- DeskAuditPreferredCommunicationMethodID
                '12790 Merit Drive',      -- Address1
                '',                       -- Address2
                'Dallas',                 -- AddressCity
                'TX',                     -- AddressState
                '75251',                  -- AddressZip
                0,                        -- AssignmentAtSelectionFlag
                'E',                      -- BillingModelCD
                'C',                      -- BusinessTypeCD
                NULL,                     -- CarrierLynxContactPhone
                1,                        -- CFLogoDisplayFlag
                0,                        -- ClaimPointCarrierRepSelFlag
                1,                        -- ClaimPointDocumentUploadFlag
                1,                        -- ClientAccessFlag
                0,                        -- DemoFlag
                0,                        -- DeskReviewLicenseReqFlag
                1,                        -- EnabledFlag
                NULL,                     -- FaxAreaCode
                NULL,                     -- FaxExchangeNumber
                NULL,                     -- FaxUnitNumber
                NULL,                     -- FedTaxId
                @PaymentCompanyID,        -- IngresAccountingId
                'C',                      -- InvoicingModelPaymentCD (Bulk / per claim)
                'P',                      -- InvoiceMethodCD
                'LOSS',                   -- LicenseDeterminationCD
                'Unitrin Business Insurance', -- Name
                '214',                    -- PhoneAreaCode
                '360',                    -- PhoneExchangeNumber
                '8000',                   -- PhoneUnitNumber
                NULL,                     -- ReturnDocDestinationCD
                NULL,                    -- ReturnDocPackageTypeCD
                'CCAV',                   -- ReturnDocRoutingCD
                0.75,                     -- TotalLossValuationWarningPercentage
                0.75,                     -- TotalLossWarningPercentage
                '99',                     -- WarrantyPeriodRefinishMinCD
                '99',                     -- WarrantyPeriodWorkmanshipMinCD
                0,                        -- SysLastUserID
                @ModifiedDateTime         -- SysLastUpdatedDate
        )
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already exists in utb_insurance', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END


--Insert Client level configuration

------------------------------------------------------

INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Program Shop Assignment')

------------------------------------------------------

INSERT INTO dbo.utb_client_service_channel
SELECT @InsuranceCompanyID,
       ServiceChannelDefaultCD,
       0, -- ClientAuthorizesPaymentFlag
       'C', -- Program Shop will be billed per claim
       0,
       @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Program Shop Assignment')


------------------------------------------------------

INSERT INTO dbo.utb_client_contract_state
SELECT  @InsuranceCompanyID,
        StateCode,
        0,                   -- UseCEIShopFlag
        0,                   -- SysLastUserID
        @ModifiedDateTime
  FROM  dbo.utb_state_code
  WHERE StateCode in ('TX')


------------------------------------------------------

INSERT INTO dbo.utb_client_coverage_type (InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        AdditionalCoverageFlag,
        ClientCode,
        CoverageProfileCD,
        DisplayOrder,
        1,
        Name,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_client_coverage_type
  WHERE InsuranceCompanyID = 145    -- Clone Zurich's configuration


------------------------------------------------------    
    
INSERT INTO dbo.utb_client_claim_aspect_type 
SELECT  @InsuranceCompanyID,
        ClaimAspectTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_claim_aspect_type
  WHERE Name IN ('Claim', 'Assignment', 'Vehicle', 'Fax Assignment')

------------------------------------------------------        
    
INSERT INTO dbo.utb_client_payment_type VALUES (@InsuranceCompanyID, 'I', 0, @ModifiedDateTime)

------------------------------------------------------

INSERT INTO dbo.utb_client_report (InsuranceCompanyID, OfficeID, ReportID, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        NULL,
        ReportID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_report 
  WHERE ReportID IN (1, 2, 3, 4, 12, 13)

------------------------------------------------------

INSERT INTO dbo.utb_workflow
SELECT W.WorkflowID,
       W.InsuranceCompanyID,
       CASE 
           WHEN W.IgnoreDefaultFlag = 'Y' THEN 1
           ELSE 0
       END,
       W.OriginatorID, 
       W.OriginatorTypeCD,
       0,
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="f:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...workflow$ AS W
WHERE W.InsuranceCompanyID IS NULL OR W.InsuranceCompanyID IN (SELECT InsuranceCompanyID FROM dbo.utb_insurance)

------------------------------------------------------

SET IDENTITY_INSERT utb_spawn ON

INSERT INTO dbo.utb_spawn (SpawnID, WorkflowID, ConditionalValue, ContextNote, CustomProcName, EnabledFlag, SpawningID, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate)
SELECT S.SpawnID,
       S.WorkflowID,
       S.ConditionalValue,
       S.ContextNote, 
       S.CustomProcName, 
       CASE 
           WHEN EnabledFlag = 'Y' THEN 1
           ELSE 0
       END,
       S.SpawningID,
       S.SpawningTypeCD,
       0,
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="f:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...spawn$ AS S
INNER JOIN dbo.utb_workflow
   ON S.WorkflowID = utb_workflow.WorkFlowID

SET IDENTITY_INSERT utb_spawn OFF

------------------------------------------------------

-- Create Office(s)

-- Southern region office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,   -- InsuranceCompanyID
                '12790 Merit Drive',   -- Address1
                '',                    -- Address2
                'Dallas',              -- AddressCity
                'TX',                  -- AddressState
                '75251',               -- AddressZip
                NULL,                  -- CCEmailAddress
                NULL,                  -- ClaimNumberFormatJS
                NULL,                  -- ClaimNumberValidJS
                NULL,                  -- ClaimNumberMsgText
                'UBI',                 -- ClientOfficeId
                1,                     -- EnabledFlag
                NULL,                  -- FaxAreaCode
                NULL,                  -- FaxExchangeNumber
                NULL,                  -- FaxUnitNumber
                '12790 Merit Drive',   -- MailingAddress1
                NULL,                  -- MailingAddress2
                'Dallas',              -- MailingAddressCity
                'TX',                  -- MailingAddressState
                '75251',               -- MailingAddressZip
                'Unitrin Business Insurance - Southern Region', -- Name
                '214',                 -- PhoneAreaCode
                '360',                 -- PhoneExchangeNumber
                '8000',                -- PhoneUnitNumber
                '',                    -- ReturnDocEmailAddress
                '',                    -- ReturnDocFaxAreaCode
                '',                    -- ReturnDocFaxExchangeNumber
                '',                    -- ReturnDocFaxUnitNumber
                0,                     -- SysLastUserID
                @ModifiedDateTime      -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    WHERE StateCode in ('TX')



-- Review affected tables
SELECT * FROM dbo.utb_insurance
SELECT * FROM dbo.utb_client_assignment_type
SELECT * FROM dbo.utb_client_claim_aspect_type
SELECT * FROM dbo.utb_client_contract_state
SELECT * FROM dbo.utb_client_payment_type
SELECT * FROM dbo.utb_client_report
SELECT * FROM dbo.utb_workflow
SELECT * FROM dbo.utb_spawn
SELECT * FROM dbo.utb_office
SELECT * FROM dbo.utb_office_assignment_type
SELECT * FROM dbo.utb_office_contract_state


--commit 
--rollback
