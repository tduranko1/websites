-- Begin the transaction for dropping and creating the tables
--DROP TABLE [dbo].[utb_partnersvc_jobs]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[utb_partnersvc_jobs](
	[JobID] VARCHAR(255) NOT NULL,
	[InscCompID] INT,
	[LynxID] INT,
	[Method] VARCHAR(10) NOT NULL,
	[ClaimNumber] VARCHAR(30) NOT NULL,
	[FromPartnerID] VARCHAR(25) NULL,
	[ToPartnerID] VARCHAR(25) NULL,
	[JobXSLFile] VARCHAR(100) NULL,
	[ResponseXSLFile] VARCHAR(100) NULL,
	[IODirection] VARCHAR(1) NOT NULL,
	[JobStatus] VARCHAR(25) NOT NULL,
	[JobStatusDetails] VARCHAR(255),
	[JobPriority] INT,
	[JobCreatedDate] DATETIME NULL,
	[JobProcessedDate] DATETIME NULL,
	[JobTransformedDate] DATETIME NULL,
	[JobTransmittedDate] DATETIME NULL,
	[JobArchivedDate] DATETIME NULL,
	[JobFinishedDate] DATETIME NULL,
	[ResponseRequired] BIT NOT NULL,
	[EnabledFlag] bit NOT NULL,
	[JobXML] xml,
	[SysLastUserID] INT NULL,
	[SysLastUpdatedDate] DATETIME NULL
)  

GO

SET ANSI_PADDING ON
GO

