declare @InsuranceCompanyID as int
declare @DocumentTypeIDPhoto as int
declare @now as datetime

set @now = current_timestamp

select @InsuranceCompanyID = InsuranceCompanyID
from utb_insurance
where name = 'Louisiana Farm Bureau'

select @DocumentTypeIDPhoto = DocumentTypeID
from utb_document_type
where Name = 'Photograph'

IF @InsuranceCompanyID IS NOT NULL
BEGIN
   BEGIN TRANSACTION
   
   -- update LAFB document bundling settings to use FTP
   UPDATE utb_insurance
   SET ReturnDocRoutingCD = 'FTP',
       ReturnDocPackageTypeCD = 'PDF',
       ReturnDocDestinationCD = 'OFC'
   WHERE InsuranceCompanyID = @InsuranceCompanyID
   
   -- update all LAFB office to use FTP for document bundling
   UPDATE dbo.utb_office
   SET ReturnDocDestinationValue = 'anonymous:anonymous@10.55.8.6:21/upload'
   WHERE InsuranceCompanyID = @InsuranceCompanyID
   
   -- LAFB requires us to send photos as separate documents
   IF NOT EXISTS(SELECT InsuranceCompanyID
                  FROM dbo.utb_client_document_exception
                  WHERE InsuranceCompanyID = @InsuranceCompanyID
                    AND DocumentTypeID = @DocumentTypeIDPhoto)
   BEGIN
      INSERT INTO dbo.utb_client_document_exception (
         InsuranceCompanyID,
         DocumentTypeID,
         OutputTypeCD,
         SysLastUserID,
         SysLastUpdatedDate
      ) VALUES (
         @InsuranceCompanyID,
         @DocumentTypeIDPhoto,
         'JPG',
         0,
         @now
      )
   END
   
   SELECT * FROM utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID
   SELECT * FROM utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID
   SELECT * FROM utb_client_document_exception WHERE InsuranceCompanyID = @InsuranceCompanyID
END
ELSE
BEGIN
   PRINT 'LAFB not found.'
END

-- Remember to Commit/Rollback changes
-- commit
-- rollback

