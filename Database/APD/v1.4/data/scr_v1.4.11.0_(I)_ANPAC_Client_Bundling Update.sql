DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 473

------------------------------
-- Update Client Bundling
------------------------------
IF EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_client_bundling
	WHERE 
		InsuranceCompanyID = @ToInscCompID
		AND BundlingID IN 
		(
			SELECT
				b.BundlingID 	
			FROM 
				utb_bundling b 
				INNER JOIN utb_message_template m 
				ON m.MessageTemplateID = b.MessageTemplateID 
			WHERE 
				m.[Description] LIKE '%msgANRRPClmAck.xsl%'
		)
	AND ReturnDocRoutingCD IS NULL
)
BEGIN
	UPDATE
		utb_client_bundling 
	SET
		ReturnDocPackageTypeCD = 'PDF'
		, ReturnDocRoutingCD = 'EML'
		, ReturnDocRoutingValue = 'CMD@ANPAC.com'
	WHERE 
		BundlingID IN 
	(		
		SELECT 
			BundlingID 
		FROM                       
			utb_bundling b 
			INNER JOIN utb_message_template m 
			ON m.MessageTemplateID = b.MessageTemplateID 
		WHERE 
			m.[Description] LIKE '%msgANRRPClmAck.xsl%'
	)
	
	UPDATE
		utb_client_bundling 
	SET
		ReturnDocPackageTypeCD = 'PDF'
		, ReturnDocRoutingCD = 'EML'
		, ReturnDocRoutingValue = 'CMD@ANPAC.com'
	WHERE 
		BundlingID IN 
	(		
		SELECT 
			BundlingID 
		FROM                       
			utb_bundling b 
			INNER JOIN utb_message_template m 
			ON m.MessageTemplateID = b.MessageTemplateID 
		WHERE 
			m.[Description] LIKE '%msgANRRPAssignmentCancel.xsl%'
	)

	UPDATE
		utb_client_bundling 
	SET
		ReturnDocPackageTypeCD = 'PDF'
		, ReturnDocRoutingCD = 'EML'
		, ReturnDocRoutingValue = 'CMD@ANPAC.com'
	WHERE 
		BundlingID IN 
	(		
		SELECT 
			BundlingID 
		FROM                       
			utb_bundling b 
			INNER JOIN utb_message_template m 
			ON m.MessageTemplateID = b.MessageTemplateID 
		WHERE 
			m.[Description] LIKE '%msgANRRPStaleAlertAndClose.xsl%'
	)
	
	UPDATE
		utb_client_bundling 
	SET
		ReturnDocPackageTypeCD = 'PDF'
		, ReturnDocRoutingCD = 'EML'
		, ReturnDocRoutingValue = 'CMD@ANPAC.com'
	WHERE 
		BundlingID IN 
	(		
		SELECT 
			BundlingID 
		FROM                       
			utb_bundling b 
			INNER JOIN utb_message_template m 
			ON m.MessageTemplateID = b.MessageTemplateID 
		WHERE 
			m.[Description] LIKE '%msgANRRPStatusUpdateNotificationOnly.xsl%'
	)

	UPDATE
		utb_client_bundling 
	SET
		ReturnDocPackageTypeCD = 'PDF'
		, ReturnDocRoutingCD = 'EML'
		, ReturnDocRoutingValue = 'CMD@ANPAC.com'
	WHERE 
		BundlingID IN 
	(		
		SELECT 
			BundlingID 
		FROM                       
			utb_bundling b 
			INNER JOIN utb_message_template m 
			ON m.MessageTemplateID = b.MessageTemplateID 
		WHERE 
			m.[Description] LIKE '%msgANRRPStatusUpdateActionRequired.xsl%'
	)

	UPDATE
		utb_client_bundling 
	SET
		ReturnDocPackageTypeCD = 'PDF'
		, ReturnDocRoutingCD = 'EML'
		, ReturnDocRoutingValue = 'CMD@ANPAC.com'
	WHERE 
		BundlingID IN 
	(		
		SELECT 
			BundlingID 
		FROM                       
			utb_bundling b 
			INNER JOIN utb_message_template m 
			ON m.MessageTemplateID = b.MessageTemplateID 
		WHERE 
			m.[Description] LIKE '%msgANRRPApprovedEstimateShopNotification.xsl%'
	)

	UPDATE
		utb_client_bundling 
	SET
		ReturnDocPackageTypeCD = 'PDF'
		, ReturnDocRoutingCD = 'EML'
		, ReturnDocRoutingValue = 'CMD@ANPAC.com'
	WHERE 
		BundlingID IN 
	(		
		SELECT 
			BundlingID 
		FROM                       
			utb_bundling b 
			INNER JOIN utb_message_template m 
			ON m.MessageTemplateID = b.MessageTemplateID 
		WHERE 
			m.[Description] LIKE '%msgANRRPTotalLossAlert.xsl%'
	)

	UPDATE
		utb_client_bundling 
	SET
		ReturnDocPackageTypeCD = 'PDF'
		, ReturnDocRoutingCD = 'EML'
		, ReturnDocRoutingValue = 'CMD@ANPAC.com'
	WHERE 
		BundlingID IN 
	(		
		SELECT 
			BundlingID 
		FROM                       
			utb_bundling b 
			INNER JOIN utb_message_template m 
			ON m.MessageTemplateID = b.MessageTemplateID 
		WHERE 
			m.[Description] LIKE '%msgANRRPTotalLossDetermination.xsl%'
	)

	SELECT 'Client Bundling updated...'
END
ELSE
BEGIN
	SELECT 'Client Bundling already updated...'
END

SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID = @ToInscCompID AND SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
