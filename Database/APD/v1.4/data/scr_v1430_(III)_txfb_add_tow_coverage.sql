-----------------------------------------------------------------------------------------------------------
-- This script will add Towing Coverage to TXFB
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------


DECLARE @InsuranceCompanyID     smallint

DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance 
WHERE Name = 'Texas Farm Bureau'

IF NOT EXISTS(SELECT *
                FROM utb_client_coverage_type
                WHERE InsuranceCompanyID = @InsuranceCompanyID
                  AND CoverageProfileCD = 'TOW')
BEGIN 
    BEGIN TRANSACTION

    INSERT INTO utb_client_coverage_type
    (InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate)
    SELECT 184, 1, 'Towing', 'TOW', 11, 1, 'Towing', 0, @ModifiedDateTime

    SELECT * FROM utb_client_coverage_type

    PRINT 'remember to commit/rollback'
END


-- commit
-- rollback
