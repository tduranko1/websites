-----------------------------------------------------------------------------------------------------------
-- Configuration for NCFB - 08Dec2011 - Thomas Duranko
-- I'm redesigning this to be easier to process.
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @RegionID			    smallint
DECLARE @PaymentCompanyID       smallint
DECLARE @ModifiedDateTime       DateTime
DECLARE @CompanyName 			VARCHAR(100)
DECLARE @DeskAuditCompany		VARCHAR(50)
DECLARE @CompanySteetAddress	VARCHAR(100)
DECLARE @CompanyCity 			VARCHAR(100)
DECLARE @CompanyState 			VARCHAR(2)
DECLARE @CompanyZip      		VARCHAR(10)
DECLARE @CompanyPhone      		VARCHAR(12)
DECLARE @CompanyFax      		VARCHAR(12)

DECLARE @TotalLossValPercent    FLOAT
DECLARE @TotalLossPercent	    FLOAT
DECLARE @WarrantyWorkmanship	SMALLINT
DECLARE @WarrantyRefinish	    SMALLINT
DECLARE @LYNXFees 				VARCHAR(1)

DECLARE @ClientProduct      	VARCHAR(10)
DECLARE @LynxContactPhone   	VARCHAR(12)
DECLARE @FNOLSearch 			VARCHAR(3)

DECLARE @NumberOfOffices	    smallint

DECLARE @OfficeName1 			VARCHAR(100)
DECLARE @OfficeDesignation1 	VARCHAR(50)
DECLARE @OfficeStreetAddress1 	VARCHAR(100)
DECLARE @OfficeCity1 			VARCHAR(100)
DECLARE @OfficeState1 			VARCHAR(2)
DECLARE @OfficeZip1				VARCHAR(10)
DECLARE @OfficePhone1 			VARCHAR(12)
DECLARE @OfficeFax1 			VARCHAR(12)

DECLARE @OfficeName2 			VARCHAR(100)
DECLARE @OfficeDesignation2 	VARCHAR(50)
DECLARE @OfficeStreetAddress2 	VARCHAR(100)
DECLARE @OfficeCity2 			VARCHAR(100)
DECLARE @OfficeState2 			VARCHAR(2)
DECLARE @OfficeZip2				VARCHAR(10)
DECLARE @OfficePhone2 			VARCHAR(12)
DECLARE @OfficeFax2 			VARCHAR(12)

DECLARE @OfficeName3 			VARCHAR(100)
DECLARE @OfficeDesignation3 	VARCHAR(50)
DECLARE @OfficeStreetAddress3 	VARCHAR(100)
DECLARE @OfficeCity3 			VARCHAR(100)
DECLARE @OfficeState3 			VARCHAR(2)
DECLARE @OfficeZip3				VARCHAR(10)
DECLARE @OfficePhone3 			VARCHAR(12)
DECLARE @OfficeFax3 			VARCHAR(12)

SET @ModifiedDateTime = Current_Timestamp

-----------------------------------------------------------------------------------------------------------
-- Client Configuration Section.  All changes can be make from here.
-----------------------------------------------------------------------------------------------------------
-- Lynx Accounting 
SET @InsuranceCompanyID 		= 327	-- Company ID 
SET @RegionID 					= 904	-- Region ID 
SET @PaymentCompanyID 			= 904	-- Pay Ctr ID 

-- Basic Company Information
SET @CompanyName 				= 'North Carolina Farm Bureau'		-- Company Name
SET @DeskAuditCompany			= 'NCFB'							-- Company Name
SET @CompanySteetAddress 		= '5301 Glenwood Ave'				-- Street Address
SET @CompanyCity 				= 'Raleigh'							-- City
SET @CompanyState 				= 'NC'								-- State
SET @CompanyZip					= '27612'							-- Zip Code
SET @CompanyPhone 				= '919-782-1705'					-- Phone (nnn-nnn-nnnn)
SET @CompanyFax 				= '919-783-3593'					-- Fax (nnn-nnn-nnnn)

SET @TotalLossValPercent		= 0.75								-- Total Loss Valuation Warning Percentage
SET @TotalLossPercent			= 0.75								-- Total Loss Warning Percentage
SET @WarrantyWorkmanship		= 99								-- Warranty Requirements Workmanship
SET @WarrantyRefinish			= 99								-- Warranty Requirements Refinish
SET @LYNXFees					= 'B'								-- Will LYNX fees be billed on a bulk invoice or individually per claim?  B-Bulk, I-Individual

-- Work Assignment
SET @ClientProduct 				= 'C'								-- C-ClaimPoint, F-FNOL, S-Scene Genesis
SET @LynxContactPhone			= 'N/A'								-- Phone Number through which the client will contact LYNX (This is displayed on ClaimPoint and on Client Reports.  If this is not provided, the LYNX "main" phone number is 
SET @FNOLSearch 				= 'N'								-- Y-Yes, N-No

-- Office Information
SET @NumberOfOffices	    	= 1									-- Number of Offices below  (If more than 3 then the script needs updated

	-- Company 1
	SET @OfficeName1 				= 'Out of State District Claims Office'		-- Office Name
	SET @OfficeDesignation1 		= 'OOS-DCO'									-- Office Designation
	SET @OfficeStreetAddress1 		= '5171 Glenwood Ave. Suite 443'			-- Street Address
	SET @OfficeCity1 				= 'Raleigh'									-- City
	SET @OfficeState1 				= 'NC'										-- State
	SET @OfficeZip1					= '27612'									-- Zip Code
	SET @OfficePhone1 				= '919-420-7701'							-- Phone (nnn-nnn-nnnn)
	SET @OfficeFax1 				= '919-783-3590'							-- Fax (nnn-nnn-nnnn)

	-- Company 2
	SET @OfficeName2 				= ''		-- Office Name
	SET @OfficeDesignation2 		= ''		-- Office Designation
	SET @OfficeStreetAddress2 		= ''		-- Street Address
	SET @OfficeCity2 				= ''		-- City
	SET @OfficeState2 				= ''		-- State
	SET @OfficeZip2					= ''		-- Zip Code
	SET @OfficePhone2 				= ''		-- Phone (nnn-nnn-nnnn)
	SET @OfficeFax2 				= ''		-- Fax (nnn-nnn-nnnn)

	-- Company 3
	SET @OfficeName3 				= ''		-- Office Name
	SET @OfficeDesignation3 		= ''		-- Office Designation
	SET @OfficeStreetAddress3 		= ''		-- Street Address
	SET @OfficeCity3 				= ''		-- City
	SET @OfficeState3 				= ''		-- State
	SET @OfficeZip3					= ''		-- Zip Code
	SET @OfficePhone3 				= ''		-- Phone (nnn-nnn-nnnn)
	SET @OfficeFax3 				= ''		-- Fax (nnn-nnn-nnnn)

-----------------------------------------------------------------------------------------------------------
-- Process steps
-----------------------------------------------------------------------------------------------------------
PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'


PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

--Insert Insurance Company information.

IF NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
    INSERT INTO dbo.utb_insurance
        (
         InsuranceCompanyID, 
         DeskAuditPreferredCommunicationMethodID,
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip, 
         AssignmentAtSelectionFlag,
         BillingModelCD, 
         BusinessTypeCD,
         CarrierLynxContactPhone,
         CFLogoDisplayFlag,
         ClaimPointCarrierRepSelFlag,
         ClaimPointDocumentUploadFlag, 
         ClientAccessFlag,
         DemoFlag,
         DeskAuditCompanyCD,
         DeskReviewLicenseReqFlag, 
         EnabledFlag,
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         FedTaxId,
         IngresAccountingId,
         InvoicingModelPaymentCD, 
         InvoiceMethodCD,
         LicenseDeterminationCD, 
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocDestinationCD,
         ReturnDocPackageTypeCD,
         ReturnDocRoutingCD, 
         TotalLossValuationWarningPercentage, 
         TotalLossWarningPercentage, 
         WarrantyPeriodRefinishMinCD,
         WarrantyPeriodWorkmanshipMinCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,      -- InsuranceCompanyID
                14,                       -- DeskAuditPreferredCommunicationMethodID
                @CompanySteetAddress,    -- Address1 *
                '',                       -- Address2
                @CompanyCity,         	  -- AddressCity *
                @CompanyState,            -- AddressState *
                @CompanyZip,              -- AddressZip *
                0,                        -- AssignmentAtSelectionFlag
                'E',                      -- BillingModelCD
                @ClientProduct,           -- BusinessTypeCD
                NULL,                     -- CarrierLynxContactPhone
                0,                        -- CFLogoDisplayFlag
                1,                        -- ClaimPointCarrierRepSelFlag *
                1,                        -- ClaimPointDocumentUploadFlag
                1,                        -- ClientAccessFlag
                0,                        -- DemoFlag
                @DeskAuditCompany,        -- DeskAuditCompanyCD *
                1,                        -- DeskReviewLicenseReqFlag
                1,                        -- EnabledFlag
                SUBSTRING(@CompanyFax,1,3),	-- FaxAreaCode *
                SUBSTRING(@CompanyFax,5,3),	-- FaxExchangeNumber *
                SUBSTRING(@CompanyFax,9,4),	-- FaxUnitNumber *
                NULL,                     -- FedTaxId
                @PaymentCompanyID,        -- IngresAccountingId
                'C',	                  -- InvoicingModelPaymentCD (Bulk / per claim)
                'P',                      -- InvoiceMethodCD
                'LOSS',                   -- LicenseDeterminationCD
                @CompanyName,  			  -- Name *
                SUBSTRING(@CompanyPhone,1,3),	-- PhoneAreaCode *
                SUBSTRING(@CompanyPhone,5,3),   -- PhoneExchangeNumber *
                SUBSTRING(@CompanyPhone,9,4),   -- PhoneUnitNumber *
                'REP',                    -- ReturnDocDestinationCD
                'PDF',                    -- ReturnDocPackageTypeCD
                'EML',                    -- ReturnDocRoutingCD
                @TotalLossValPercent,     -- TotalLossValuationWarningPercentage
                @TotalLossPercent,        -- TotalLossWarningPercentage
                @WarrantyWorkmanship,     -- WarrantyPeriodRefinishMinCD
                @WarrantyRefinish,        -- WarrantyPeriodWorkmanshipMinCD
                0,                        -- SysLastUserID
                @ModifiedDateTime         -- SysLastUpdatedDate
        )
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID already exists in utb_insurance', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END


--Insert Client level configuration - Must match Office Assignement type

------------------------------------------------------

INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Demand Estimate Audit')

------------------------------------------------------

INSERT INTO dbo.utb_client_service_channel
SELECT @InsuranceCompanyID,
       ServiceChannelDefaultCD,
       0,       	-- ClientAuthorizesPaymentFlag
       @LYNXFees,   -- InvoicingModelBillingCD  (C - Client / B - Bulk *
       0,
       @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Demand Estimate Audit')


------------------------------------------------------

INSERT INTO dbo.utb_client_contract_state
SELECT  @InsuranceCompanyID,
        StateCode,
        0,                   -- UseCEIShopFlag
        0,                   -- SysLastUserID
        @ModifiedDateTime
  FROM  dbo.utb_state_code
  WHERE EnabledFlag = 1


------------------------------------------------------

INSERT INTO dbo.utb_client_coverage_type (InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        AdditionalCoverageFlag,
        ClientCode,
        CoverageProfileCD,
        DisplayOrder,
        1,
        Name,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_client_coverage_type
  WHERE InsuranceCompanyID = 145    -- Clone Zurich's configuration


------------------------------------------------------    
    
INSERT INTO dbo.utb_client_claim_aspect_type 
SELECT  @InsuranceCompanyID,
        ClaimAspectTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_claim_aspect_type
  WHERE Name IN ('Claim', 'Assignment', 'Vehicle', 'Fax Assignment')

------------------------------------------------------        
 
-- Comment this out if DeskAudit.  Uncomment for DRP   
--INSERT INTO dbo.utb_client_payment_type VALUES (@InsuranceCompanyID, 'I', 0, @ModifiedDateTime)

------------------------------------------------------

INSERT INTO dbo.utb_client_report (InsuranceCompanyID, OfficeID, ReportID, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        NULL,
        ReportID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_report 
  WHERE ReportID IN (1, 2, 3, 4, 5, 6, 7, 12, 13)

------------------------------------------------------
-- Create Office(s)
-- Auto Owners Insurance
------------------------------------------------------
-- Office 1
------------------------------------------------------
IF(@NumberOfOffices >= 1)
BEGIN
	INSERT INTO dbo.utb_office
			(
			 InsuranceCompanyID, 
			 Address1,
			 Address2, 
			 AddressCity, 
			 AddressState, 
			 AddressZip,
			 CCEmailAddress,
			 ClaimNumberFormatJS,
			 ClaimNumberValidJS,
			 ClaimNumberMsgText,
			 ClientOfficeId,
			 EnabledFlag, 
			 FaxAreaCode,
			 FaxExchangeNumber,
			 FaxUnitNumber,
			 MailingAddress1,
			 MailingAddress2, 
			 MailingAddressCity, 
			 MailingAddressState, 
			 MailingAddressZip,
			 Name, 
			 PhoneAreaCode, 
			 PhoneExchangeNumber, 
			 PhoneUnitNumber,
			 ReturnDocEmailAddress,
			 ReturnDocFaxAreaCode,
			 ReturnDocFaxExchangeNumber,
			 ReturnDocFaxUnitNumber, 
			 SysLastUserID,
			 SysLastUpdatedDate 
			)
			VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
					@OfficeStreetAddress1 , -- Address1 *
					'',			            -- Address2 *
					@OfficeCity1,			-- AddressCity *
					@OfficeState1,          -- AddressState *
					@OfficeZip1,            -- AddressZip *
					NULL,                   -- CCEmailAddress
					NULL,                   -- ClaimNumberFormatJS
					NULL,                   -- ClaimNumberValidJS
					NULL,                   -- ClaimNumberMsgText
					@OfficeDesignation1,	-- ClientOfficeId * 
					1,                      -- EnabledFlag
					SUBSTRING(@OfficeFax1,1,3),	   -- FaxAreaCode *
					SUBSTRING(@OfficeFax1,5,3),     -- FaxExchangeNumber *
					SUBSTRING(@OfficeFax1,9,4),     -- FaxUnitNumber *
					@OfficeStreetAddress1 ,	-- MailingAddress1 *
					'',			            -- MailingAddress2 *
					@OfficeCity1,			-- MailingAddressCity *
					@OfficeState1,          -- MailingAddressState *
					@OfficeZip1,            -- MailingAddressZip *
					@OfficeName1 ,			-- Name *
					SUBSTRING(@OfficePhone1,1,3),	-- PhoneAreaCode *
					SUBSTRING(@OfficePhone1,5,3),	-- PhoneExchangeNumber *
					SUBSTRING(@OfficePhone1,9,4),	-- PhoneUnitNumber *
					NULL,                   -- ReturnDocEmailAddress
					NULL,                   -- ReturnDocFaxAreaCode
					NULL,                   -- ReturnDocFaxExchangeNumber
					NULL,                   -- ReturnDocFaxUnitNumber
					0,                      -- SysLastUserID
					@ModifiedDateTime       -- SysLastUpdatedDate
			)


	SELECT @OfficeID = SCOPE_IDENTITY()


	-- Office level configurations - this must match the Client assignment types

	INSERT INTO dbo.utb_office_assignment_type 
	  SELECT  @OfficeID, 
			  AssignmentTypeID, 
			  0,
			  @ModifiedDateTime
		FROM  dbo.utb_assignment_type
		WHERE Name in ( 'Demand Estimate Audit')


	INSERT INTO dbo.utb_office_contract_state
	  SELECT  @OfficeID,
			  StateCode,
			  0, -- UseCEIShopsFlag
			  0, -- SysLastUserID
			  @ModifiedDateTime
		FROM  dbo.utb_state_code
		WHERE EnabledFlag = 1
END

------------------------------------------------------
-- Create Office(s)
-- Auto Owners Insurance
------------------------------------------------------
-- Office 2
------------------------------------------------------
IF(@NumberOfOffices >= 2)
BEGIN
	INSERT INTO dbo.utb_office
			(
			 InsuranceCompanyID, 
			 Address1,
			 Address2, 
			 AddressCity, 
			 AddressState, 
			 AddressZip,
			 CCEmailAddress,
			 ClaimNumberFormatJS,
			 ClaimNumberValidJS,
			 ClaimNumberMsgText,
			 ClientOfficeId,
			 EnabledFlag, 
			 FaxAreaCode,
			 FaxExchangeNumber,
			 FaxUnitNumber,
			 MailingAddress1,
			 MailingAddress2, 
			 MailingAddressCity, 
			 MailingAddressState, 
			 MailingAddressZip,
			 Name, 
			 PhoneAreaCode, 
			 PhoneExchangeNumber, 
			 PhoneUnitNumber,
			 ReturnDocEmailAddress,
			 ReturnDocFaxAreaCode,
			 ReturnDocFaxExchangeNumber,
			 ReturnDocFaxUnitNumber, 
			 SysLastUserID,
			 SysLastUpdatedDate 
			)
			VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
					@OfficeStreetAddress2 , -- Address1 *
					'',			            -- Address2 *
					@OfficeCity2,			-- AddressCity *
					@OfficeState2,          -- AddressState *
					@OfficeZip2,            -- AddressZip *
					NULL,                   -- CCEmailAddress
					NULL,                   -- ClaimNumberFormatJS
					NULL,                   -- ClaimNumberValidJS
					NULL,                   -- ClaimNumberMsgText
					@OfficeDesignation2,	-- ClientOfficeId * 
					1,                      -- EnabledFlag
					SUBSTRING(@OfficeFax2,1,3),	   -- FaxAreaCode *
					SUBSTRING(@OfficeFax2,5,3),     -- FaxExchangeNumber *
					SUBSTRING(@OfficeFax2,9,4),     -- FaxUnitNumber *
					@OfficeStreetAddress2 ,	-- MailingAddress1 *
					'',			            -- MailingAddress2 *
					@OfficeCity2,			-- MailingAddressCity *
					@OfficeState2,          -- MailingAddressState *
					@OfficeZip2,            -- MailingAddressZip *
					@OfficeName2 ,			-- Name *
					SUBSTRING(@OfficePhone2,1,3),	-- PhoneAreaCode *
					SUBSTRING(@OfficePhone2,5,3),	-- PhoneExchangeNumber *
					SUBSTRING(@OfficePhone2,9,4),	-- PhoneUnitNumber *
					NULL,                   -- ReturnDocEmailAddress
					NULL,                   -- ReturnDocFaxAreaCode
					NULL,                   -- ReturnDocFaxExchangeNumber
					NULL,                   -- ReturnDocFaxUnitNumber
					0,                      -- SysLastUserID
					@ModifiedDateTime       -- SysLastUpdatedDate
			)


	SELECT @OfficeID = SCOPE_IDENTITY()


	-- Office level configurations - this must match the Client assignment types

	INSERT INTO dbo.utb_office_assignment_type 
	  SELECT  @OfficeID, 
			  AssignmentTypeID, 
			  0,
			  @ModifiedDateTime
		FROM  dbo.utb_assignment_type
		WHERE Name in ( 'Demand Estimate Audit')


	INSERT INTO dbo.utb_office_contract_state
	  SELECT  @OfficeID,
			  StateCode,
			  0, -- UseCEIShopsFlag
			  0, -- SysLastUserID
			  @ModifiedDateTime
		FROM  dbo.utb_state_code
		WHERE EnabledFlag = 1
END
	
------------------------------------------------------
-- Create Office(s)
-- Auto Owners Insurance
------------------------------------------------------
-- Office 3
------------------------------------------------------
IF(@NumberOfOffices >= 3)
BEGIN
	INSERT INTO dbo.utb_office
			(
			 InsuranceCompanyID, 
			 Address1,
			 Address2, 
			 AddressCity, 
			 AddressState, 
			 AddressZip,
			 CCEmailAddress,
			 ClaimNumberFormatJS,
			 ClaimNumberValidJS,
			 ClaimNumberMsgText,
			 ClientOfficeId,
			 EnabledFlag, 
			 FaxAreaCode,
			 FaxExchangeNumber,
			 FaxUnitNumber,
			 MailingAddress1,
			 MailingAddress2, 
			 MailingAddressCity, 
			 MailingAddressState, 
			 MailingAddressZip,
			 Name, 
			 PhoneAreaCode, 
			 PhoneExchangeNumber, 
			 PhoneUnitNumber,
			 ReturnDocEmailAddress,
			 ReturnDocFaxAreaCode,
			 ReturnDocFaxExchangeNumber,
			 ReturnDocFaxUnitNumber, 
			 SysLastUserID,
			 SysLastUpdatedDate 
			)
			VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
					@OfficeStreetAddress3 , -- Address1 *
					'',			            -- Address2 *
					@OfficeCity3,			-- AddressCity *
					@OfficeState3,          -- AddressState *
					@OfficeZip3,            -- AddressZip *
					NULL,                   -- CCEmailAddress
					NULL,                   -- ClaimNumberFormatJS
					NULL,                   -- ClaimNumberValidJS
					NULL,                   -- ClaimNumberMsgText
					@OfficeDesignation3,	-- ClientOfficeId * 
					1,                      -- EnabledFlag
					SUBSTRING(@OfficeFax3,1,3),	   -- FaxAreaCode *
					SUBSTRING(@OfficeFax3,5,3),     -- FaxExchangeNumber *
					SUBSTRING(@OfficeFax3,9,4),     -- FaxUnitNumber *
					@OfficeStreetAddress3 ,	-- MailingAddress1 *
					'',			            -- MailingAddress2 *
					@OfficeCity3,			-- MailingAddressCity *
					@OfficeState3,          -- MailingAddressState *
					@OfficeZip3,            -- MailingAddressZip *
					@OfficeName3,			-- Name *
					SUBSTRING(@OfficePhone3,1,3),	-- PhoneAreaCode *
					SUBSTRING(@OfficePhone3,5,3),	-- PhoneExchangeNumber *
					SUBSTRING(@OfficePhone3,9,4),	-- PhoneUnitNumber *
					NULL,                   -- ReturnDocEmailAddress
					NULL,                   -- ReturnDocFaxAreaCode
					NULL,                   -- ReturnDocFaxExchangeNumber
					NULL,                   -- ReturnDocFaxUnitNumber
					0,                      -- SysLastUserID
					@ModifiedDateTime       -- SysLastUpdatedDate
			)


	SELECT @OfficeID = SCOPE_IDENTITY()


	-- Office level configurations - this must match the Client assignment types

	INSERT INTO dbo.utb_office_assignment_type 
	  SELECT  @OfficeID, 
			  AssignmentTypeID, 
			  0,
			  @ModifiedDateTime
		FROM  dbo.utb_assignment_type
		WHERE Name in ( 'Demand Estimate Audit')


	INSERT INTO dbo.utb_office_contract_state
	  SELECT  @OfficeID,
			  StateCode,
			  0, -- UseCEIShopsFlag
			  0, -- SysLastUserID
			  @ModifiedDateTime
		FROM  dbo.utb_state_code
		WHERE EnabledFlag = 1
END	
	

-- Review affected tables
SELECT * FROM dbo.utb_insurance
SELECT * FROM dbo.utb_client_assignment_type
SELECT * FROM dbo.utb_client_claim_aspect_type
SELECT * FROM dbo.utb_client_contract_state
SELECT * FROM dbo.utb_client_payment_type
SELECT * FROM dbo.utb_client_report
SELECT * FROM dbo.utb_office
SELECT * FROM dbo.utb_office_assignment_type
SELECT * FROM dbo.utb_office_contract_state

-- This script requires a fresh on the workflow and spawn reference tables from the access file

commit 
--rollback
