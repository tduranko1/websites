DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 464
SET @FromInscCompID = 192

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
SELECT 
	* 
FROM 
	utb_bundling_document_type 
WHERE 	
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @ToInscCompID
	)

)
BEGIN
	DECLARE @MsgTempID INT

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWPSInitialEstimate.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'O',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWPSApprovedEstimate_EB.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',0,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,1,4,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWPSClosingRepairComplete.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,2,NULL,'I',1,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',1,NULL,1,3,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULl,0,4,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,9,NULL,'I',0,0,0,NULL,0,NULL,1,6,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,42,NULL,'I',0,1,0,NULL,0,NULL,1,6,'NY',0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,43,NULL,'I',0,1,0,NULL,0,NULL,1,7,'CT',0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,51,NULL,'I',0,1,0,NULL,0,NULL,0,8,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWPSClosingSupplement.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',1,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWDAClosingAuditComplete.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',0,NULL,1,3,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,1,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,33,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWDAClosingSupplement.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',1,NULL,1,3,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,33,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWDAClosingTL.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWPSTLClosing.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,NULL,1,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,0,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,39,NULL,'I',0,0,0,NULL,0,NULL,1,4,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWPSRentalInvoice.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,9,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWDAShopCoverPage.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,0,'A',0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWPSApprovedEstimate_EB.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,2,NULL,'I',1,0,0,NULL,0,NULL,0,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWPS_RC_Combined_Bill_Release_Close_EB.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,2,NULL,'I',1,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,1,NULL,1,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,0,0,NULL,0,NULL,1,4,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,9,NULL,'I',0,1,0,NULL,0,NULL,1,6,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,51,NULL,'I',0,0,0,NULL,0,NULL,1,5,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgCWPS_RC_Release_Close_EB.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,1,NULL,1,NULL,0,4,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,0,6,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,9,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,0,5,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,51,NULL,'I',0,0,0,NULL,0,NULL,0,5,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END
