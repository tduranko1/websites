DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 575

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%NOPS%' )
BEGIN
	INSERT INTO utb_message_template SELECT 'C',1,'Assignment Cancellation|Messages/msgNOPSAssignmentCancel.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'No Inspection (Stale) Alert & Close|Messages/msgNOPSStaleAlert.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Cash Out Alert & Close|Messages/msgNOPSCashOutAlert.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Status Update - Claim Rep|Messages/msgNOPSStatusUpdAction.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Status Update - MDS|Messages/msgNOPSStatusUpdActionMDS.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Total Loss Alert - (Adjuster)|Messages/msgNOPSTLAlert.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Initial Estimate and Photos (Adjuster)|Messages/msgNOPSInitialEstimate.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Closing Repair Complete|Messages/msgNOPS_ClosingRepairComplete.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Claim Acknowledgment|Messages/msgNOPSClmAck.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Closing Supplement|Messages/msgNOPSClosingSupplement.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Rental Invoice|Messages/msgNOPSRentalInvoice.xsl','PS',0,CURRENT_TIMESTAMP
	INSERT INTO utb_message_template SELECT 'C',1,'Total Loss Closing|Messages/msgNOPSTLClosing.xsl','PS',0,CURRENT_TIMESTAMP
	
	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

--SELECT * FROM utb_message_template  where description like '%CI%'
--SELECT * FROM utb_bundling where MessageTemplateID IN (433,434,435,436,437,438,439,440,441,442,443,444)
