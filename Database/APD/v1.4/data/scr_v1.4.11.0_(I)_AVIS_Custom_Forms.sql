DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 515

------------------------------
-- Add Form Supplements
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_form
	WHERE 
		InsuranceCompanyID = @ToInscCompID
		AND HTMLPath = 'Forms/AVIS_TLVCReq.asp'
)
BEGIN
    insert into utb_form values (NULL,70,NULL,@ToInscCompID,NULL,NULL,0,NULL,0,1,NULL,'Forms/AVIS_TLBidRep.asp','Salvage Bid Report','AVIS_TLSBidRep.pdf','S','PS','uspCFBCIFGetXML',0,0,CURRENT_TIMESTAMP)
    insert into utb_form values (NULL,40,NULL,@ToInscCompID,NULL,NULL,0,NULL,0,1,NULL,'Forms/AVIS_TLVCReq.asp','Vehicle Condition Report','AVIS_TLVCReq.pdf','S','PS','uspCFBCIFGetXML',0,0,CURRENT_TIMESTAMP)

	SELECT 'Client Custom Forms added...'
END
ELSE
BEGIN
	SELECT 'Client Custom Forms already added...'
END

SELECT * FROM utb_form WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
