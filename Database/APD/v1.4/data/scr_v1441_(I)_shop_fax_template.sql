DECLARE @now as datetime
DECLARE @FormID as int
DECLARE @DocumentTypeID as int
DECLARE @InsuranceCompanyID as int
DECLARE @ProcName as varchar(50)

SET @now = CURRENT_TIMESTAMP
SET @ProcName = 'Shop Fax Template Setup'

SELECT @DocumentTypeID = DocumentTypeID 
FROM utb_document_type 
WHERE Name = 'Shop Assignment'

BEGIN TRANSACTION
-- -----------------------------------------------------
-- Setup fax template for Electric Insurance
-- -----------------------------------------------------

SET @InsuranceCompanyID = 158

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType3.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions158.pdf',  -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,            -- FormID,
    1,                  -- EnabledFlag,
    'Direction to Pay', -- Name,
    'DTPType1.pdf',     -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END


-- -----------------------------------------------------
-- Setup fax template for Farm Family
-- -----------------------------------------------------

SET @InsuranceCompanyID = 198

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType5.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions198.pdf',  -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Direction to Pay',             -- Name,
    'DTPType1.pdf',                 -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END


-- -----------------------------------------------------
-- Setup fax template for LYNX Services Insurance
-- -----------------------------------------------------

SET @InsuranceCompanyID = 99

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType1.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions158.pdf',  -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Direction to Pay',             -- Name,
    'DTPType1.pdf',                 -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END


-- -----------------------------------------------------
-- Setup fax template for Republic Group
-- -----------------------------------------------------

SET @InsuranceCompanyID = 277

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType1.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions277.pdf',  -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Direction to Pay',             -- Name,
    'DTPType1.pdf',                 -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END


-- -----------------------------------------------------
-- Setup fax template for Texas Farm Bureau
-- -----------------------------------------------------

SET @InsuranceCompanyID = 184

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType1.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions184.pdf',  -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Direction to Pay',             -- Name,
    'DTPType1.pdf',                 -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END


-- -----------------------------------------------------
-- Setup fax template for Unitrin Business Insurance
-- -----------------------------------------------------

SET @InsuranceCompanyID = 261

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType2.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions261.pdf',  -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Direction to Pay',             -- Name,
    'DTPType1.pdf',                 -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END


-- -----------------------------------------------------
-- Setup fax template for Utica National Insurance Group
-- -----------------------------------------------------

SET @InsuranceCompanyID = 259

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType1.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions259.pdf',  -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Direction to Pay',             -- Name,
    'DTPType1.pdf',                 -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END


-- -----------------------------------------------------
-- Setup fax template for Virginia Farm Bureau
-- -----------------------------------------------------

SET @InsuranceCompanyID = 192

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType1.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions192.pdf',  -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Direction to Pay',             -- Name,
    'DTPType1.pdf',                 -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END


-- -----------------------------------------------------
-- Setup fax template for Western Agricultural
-- -----------------------------------------------------

SET @InsuranceCompanyID = 178

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType1.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions178.pdf',  -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Direction to Pay',             -- Name,
    'DTPType1.pdf',                 -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END


-- -----------------------------------------------------
-- Setup fax template for Zurich North America
-- -----------------------------------------------------

SET @InsuranceCompanyID = 145

-- Add the assignment page
INSERT INTO dbo.utb_form (
    DocumentTypeID,
    InsuranceCompanyID,
    EnabledFlag,
    Name,
    PDFPath,
    PertainsToCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,                    --DocumentTypeID,
    @InsuranceCompanyID,                --InsuranceCompanyID,
    1,                                  --EnabledFlag,
    'Shop Assignment',                  --Name,
    'FaxAssignmentType4.pdf',           --PDFPath,
    'S',                                --PertainsToCD,
    'uspWorkflowSendShopAssignXML',     --SQLProcedure,
    1,                                  --SystemFlag
    0,                                  --SysLastUserID,
    @now                                --SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

SET @FormID = SCOPE_IDENTITY()

-- Add the expanded comments page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                -- FormID,
    1,                      -- EnabledFlag,
    'Expanded Comments',    -- Name,
    'FaxXComments.pdf',     -- PDFPath,
    '',                     -- SQLProcedure,
    0,                      -- SysLastUserID,
    @now                    -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the shop instructions page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                    -- FormID,
    1,                          -- EnabledFlag,
    'Shop Instructions',        -- Name,
    'ShopInstructions145.pdf', -- PDFPath,
    '',                         -- SQLProcedure,
    0,                          -- SysLastUserID,
    @now                        -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the DTP page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Direction to Pay',             -- Name,
    'DTPType2.pdf',                 -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Add the Photo requirements page
INSERT INTO dbo.utb_form_supplement (
    FormID,
    EnabledFlag,
    Name,
    PDFPath,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @FormID,                        -- FormID,
    1,                              -- EnabledFlag,
    'Photo Requirements',           -- Name,
    'PhotoRequirment.pdf',          -- PDFPath,
    'uspWorkflowSendShopAssignXML', -- SQLProcedure,
    0,                              -- SysLastUserID,
    @now                            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|utb_form_supplement', 16, 1, @ProcName)
    ROLLBACK TRANSACTION 
END

-- Now select the updates tables
SELECT * FROM utb_form
SELECT * FROM utb_form_supplement

-- remember to commit/rollback

-- commit
-- rollback