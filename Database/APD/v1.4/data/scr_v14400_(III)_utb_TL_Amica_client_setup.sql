INSERT INTO utb_client_service_channel (
   InsuranceCompanyID,
   ServiceChannelCD,
   ClientAuthorizesPaymentFlag,
   InvoicingModelBillingCD,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   196,
   'TL',
   0,
   'B',
   0,
   CURRENT_TIMESTAMP
)



INSERT INTO dbo.utb_client_assignment_type(
   InsuranceCompanyID,
   AssignmentTypeID,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   196,
   14,
   0,
   CURRENT_TIMESTAMP
)

INSERT INTO dbo.utb_office_assignment_type
SELECT OfficeID, 14, 0, CURRENT_TIMESTAMP
FROM utb_office
WHERE InsuranceCompanyID = 196

