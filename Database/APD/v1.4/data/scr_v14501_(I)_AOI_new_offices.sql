
/*
	Filename:
	Author/Date: Walter J. Chesla - 12/30/2010
	Description:  This script will create new office configurations for Zurich Dallas, Omaha, and Sacramento
	
*/

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = (SELECT InsuranceCompanyID FROM utb_insurance WHERE DeskAuditCompanyCD = 'AOI')


PRINT 'Creating new AOI offices...'

/* 
--	Branch	Branch City			Phone			Fax
--	21		Lakeland, FL		863-687-3587	863-687-0371
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
		SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Lakeland',
		'FL',
        'FL - Lakeland',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '863',                 -- FaxAreaCode
        '687',                 -- FaxExchangeNumber
        '0371',                -- FaxUnitNumber
        'Lakeland, FL - Branch #21',				-- Name
        '863',					-- PhoneAreaCode
        '687',                 -- PhoneExchangeNumber
        '3587',                -- PhoneUnitNumber
		'lakeland.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

 
--	Branch	Branch City			Phone			Fax
--	24		Fort Myers, FL		239-939-5222	239-939-1399
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Fort Myers',
		'FL',
        'FL - Fort Myers',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '239',                 -- FaxAreaCode
        '939',                 -- FaxExchangeNumber
        '1399',                -- FaxUnitNumber
        'Fort Myers, FL - Branch #24',				-- Name
        '239',					-- PhoneAreaCode
        '939',                 -- PhoneExchangeNumber
        '5222',                -- PhoneUnitNumber
		'ftmyers.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)
SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

 
--	Branch	Branch City			Phone			Fax
--	30		Clearwater, FL		727-530-5461	727-532-9825
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Clearwater',
		'FL',
        'FL - Clearwater',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '727',                 -- FaxAreaCode
        '532',                 -- FaxExchangeNumber
        '9825',                -- FaxUnitNumber
        'Clearwater, FL - Branch #30',				-- Name
        '727',					-- PhoneAreaCode
        '530',                 -- PhoneExchangeNumber
        '5461',                -- PhoneUnitNumber
		'clearwater.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


--	Branch	Branch City			Phone			Fax
--	38		Pensacola, FL		850-478-0205	850-476-3106
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Pensacola',
		'FL',
        'FL - Pensacola',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '850',                 -- FaxAreaCode
        '476',                 -- FaxExchangeNumber
        '3106',                -- FaxUnitNumber
        'Pensacola, FL - Branch #38',				-- Name
        '850',					-- PhoneAreaCode
        '478',                 -- PhoneExchangeNumber
        '0205',                -- PhoneUnitNumber
		'pensacola.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


--	Branch	Branch City			Phone			Fax
--	42		Ocala, FL			352-873-9388	352-873-9370
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Ocala',
		'FL',
        'FL - Ocala',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '352',                 -- FaxAreaCode
        '873',                 -- FaxExchangeNumber
        '9370',                -- FaxUnitNumber
        'Ocala, FL - Branch #42',				-- Name
        '352',					-- PhoneAreaCode
        '873',                 -- PhoneExchangeNumber
        '9388',                -- PhoneUnitNumber
		'ocala.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


--	Branch	Branch City			Phone			Fax
--	78		Tallahassee, FL		850-216-3180	850-216-3195
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Tallahassee',
		'FL',
        'FL - Tallahassee',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '850',                 -- FaxAreaCode
        '216',                 -- FaxExchangeNumber
        '3195',                -- FaxUnitNumber
        'Tallahassee, FL - Branch #78',				-- Name
        '850',					-- PhoneAreaCode
        '216',                 -- PhoneExchangeNumber
        '3180',                -- PhoneUnitNumber
		'tallahassee.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


--	Branch	Branch City			Phone			Fax
--	93		Stuart, FL			772-219-3465	772-219-9538
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Stuart',
		'FL',
        'FL - Stuart',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '772',                 -- FaxAreaCode
        '219',                 -- FaxExchangeNumber
        '9538',                -- FaxUnitNumber
        'Stuart, FL - Branch #93',				-- Name
        '772',					-- PhoneAreaCode
        '219',                 -- PhoneExchangeNumber
        '3465',                -- PhoneUnitNumber
		'stuart.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code



--	Branch	Branch City			Phone			Fax
--	2		Indianapolis, IN	317-487-1970	317-487-2040	indianapolis.clm@aoins.com

INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Indianapolis',
		'IN',
        'IN - Indianapolis',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '317',                 -- FaxAreaCode
        '487',                 -- FaxExchangeNumber
        '2040',                -- FaxUnitNumber
        'Indianapolis, IN - Branch #2',				-- Name
        '317',					-- PhoneAreaCode
        '487',                 -- PhoneExchangeNumber
        '1970',                -- PhoneUnitNumber
		'indianapolis.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')

INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'indianapolis.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'IN - Indianapolis',
    @EmailAddress          = 'indianapolis.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '317',
    @PhoneExchangeNumber   = '487',
    @PhoneUnitNumber       = '1970',
    @ApplicationID		   = '2'


--	Branch	Branch City			Phone			Fax
--	29		Marion, IN			765-384-5461	765-384-4873	marion.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Marion',
		'IN',
        'IN - Marion',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '765',                 -- FaxAreaCode
        '384',                 -- FaxExchangeNumber
        '4873',                -- FaxUnitNumber
        'Marion, IN - Branch #29',				-- Name
        '765',					-- PhoneAreaCode
        '384',                 -- PhoneExchangeNumber
        '5461',                -- PhoneUnitNumber
		'marion.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
   
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'marion.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'IN - Marion',
    @EmailAddress          = 'marion.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '765',
    @PhoneExchangeNumber   = '384',
    @PhoneUnitNumber       = '5461',
    @ApplicationID		   = '2'


--	Branch	Branch City			Phone			Fax
--	64		Southern Indiana	812-945-1665	812-945-1912	s.indiana.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'IN',
        'IN - Southern Indiana',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '812',                 -- FaxAreaCode
        '945',                 -- FaxExchangeNumber
        '1912',                -- FaxUnitNumber
        'Southern Indiana - Branch #64',				-- Name
        '812',					-- PhoneAreaCode
        '945',                 -- PhoneExchangeNumber
        '1665',                -- PhoneUnitNumber
		's.indiana.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')

INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 's.indiana.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'IN - Southern Indiana',
    @EmailAddress          = 's.indiana.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '812',
    @PhoneExchangeNumber   = '945',
    @PhoneUnitNumber       = '1665',
    @ApplicationID		   = '2'


--	Branch	Branch City			Phone			Fax
--	10		Mishawaka, IN		574-256-6733	574-256-6732	mishawaka.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Mishawaka',
		'IN',
        'IN - Mishawaka',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '574',                 -- FaxAreaCode
        '256',                 -- FaxExchangeNumber
        '6732',                -- FaxUnitNumber
        'Mishawaka, IN - Branch #10',				-- Name
        '574',					-- PhoneAreaCode
        '256',                 -- PhoneExchangeNumber
        '6733',                -- PhoneUnitNumber
		'mishawaka.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')

INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'mishawaka.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'IN - Mishawaka',
    @EmailAddress          = 'mishawaka.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '574',
    @PhoneExchangeNumber   = '256',
    @PhoneUnitNumber       = '6733',
    @ApplicationID		   = '2'

 
--	Branch	Branch City			Phone			Fax
--	52		Lexington, KY		859-278-6181	859-971-4210	lexington.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Lexington',
		'KY',
        'KY - Lexington',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '859',                 -- FaxAreaCode
        '971',                 -- FaxExchangeNumber
        '4210',                -- FaxUnitNumber
        'Lexington, KY - Branch #52',				-- Name
        '859',					-- PhoneAreaCode
        '278',                 -- PhoneExchangeNumber
        '6181',                -- PhoneUnitNumber
		'lexington.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'lexington.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'KY - Lexington',
    @EmailAddress          = 'lexington.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '859',
    @PhoneExchangeNumber   = '278',
    @PhoneUnitNumber       = '6181',
    @ApplicationID		   = '2'
 
 
--	Branch	Branch City			Phone			Fax
--	97		Louisville, KY		502-339-8335	502-339-8415	louisville.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Louisville',
		'KY',
        'KY - Louisville',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '502',                 -- FaxAreaCode
        '339',                 -- FaxExchangeNumber
        '8415',                -- FaxUnitNumber
        'Louisville, KY - Branch #97',				-- Name
        '502',					-- PhoneAreaCode
        '339',                 -- PhoneExchangeNumber
        '8335',                -- PhoneUnitNumber
		'louisville.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'louisville.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'KY - Louisville',
    @EmailAddress          = 'louisville.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '502',
    @PhoneExchangeNumber   = '339',
    @PhoneUnitNumber       = '8335',
    @ApplicationID		   = '2'
 
 
--	Branch	Branch City			Phone			Fax
--	33		Brentwood, TN		615-373-5200	615-661-4654	brentwood.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Brentwood',
		'TN',
        'TN - Brentwood',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '615',                 -- FaxAreaCode
        '661',                 -- FaxExchangeNumber
        '4654',                -- FaxUnitNumber
        'Brentwood, TN - Branch #33',				-- Name
        '615',					-- PhoneAreaCode
        '373',                 -- PhoneExchangeNumber
        '5200',                -- PhoneUnitNumber
		'brentwood.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'brentwood.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'TN - Brentwood',
    @EmailAddress          = 'brentwood.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '615',
    @PhoneExchangeNumber   = '373',
    @PhoneUnitNumber       = '5200',
    @ApplicationID		   = '2'
    
 
--	Branch	Branch City			Phone			Fax
--	76		Knoxville, TN		865-539-2991	865-539-9528	knoxville.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Knoxville',
		'TN',
        'TN - Knoxville',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '865',                 -- FaxAreaCode
        '539',                 -- FaxExchangeNumber
        '9528',                -- FaxUnitNumber
        'Knoxville, TN - Branch #76',				-- Name
        '865',					-- PhoneAreaCode
        '539',                 -- PhoneExchangeNumber
        '2991',                -- PhoneUnitNumber
		'knoxville.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'knoxville.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'TN - Knoxville',
    @EmailAddress          = 'knoxville.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '865',
    @PhoneExchangeNumber   = '539',
    @PhoneUnitNumber       = '2991',
    @ApplicationID		   = '2'
 
 
--	Branch	Branch City			Phone			Fax
--	61		Appleton, WI		920-993-8550	920-993-0720	appleton.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Appleton',
		'WI',
        'WI - Appleton',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '920',                 -- FaxAreaCode
        '993',                 -- FaxExchangeNumber
        '0720',                -- FaxUnitNumber
        'Appleton, WI - Branch #61',				-- Name
        '920',					-- PhoneAreaCode
        '993',                 -- PhoneExchangeNumber
        '8550',                -- PhoneUnitNumber
		'appleton.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'appleton.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'WI - Appleton',
    @EmailAddress          = 'appleton.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '920',
    @PhoneExchangeNumber   = '993',
    @PhoneUnitNumber       = '8550',
    @ApplicationID		   = '2'
 
 
--	Branch	Branch City			Phone			Fax
--	65		Eau Claire, WI		715-552-1462	715-552-1466	eauclaire.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Eau Claire',
		'WI',
        'WI - Eau Claire',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '715',                 -- FaxAreaCode
        '552',                 -- FaxExchangeNumber
        '1466',                -- FaxUnitNumber
        'Eau Claire, WI - Branch #65',				-- Name
        '715',					-- PhoneAreaCode
        '552',                 -- PhoneExchangeNumber
        '1462',                -- PhoneUnitNumber
		'eauclaire.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'eauclaire.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'WI - Eau Claire',
    @EmailAddress          = 'eauclaire.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '715',
    @PhoneExchangeNumber   = '552',
    @PhoneUnitNumber       = '1462',
    @ApplicationID		   = '2'
 
 
--	Branch	Branch City			Phone			Fax
--	75		Central Missouri	573-875-1290	573-875-1281	central.missouri.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'MO',
        'MO - Central Missouri',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '573',                 -- FaxAreaCode
        '875',                 -- FaxExchangeNumber
        '1281',                -- FaxUnitNumber
        'Central Missouri, MI - Branch #75',				-- Name
        '573',					-- PhoneAreaCode
        '875',                 -- PhoneExchangeNumber
        '1290',                -- PhoneUnitNumber
		'central.missouri.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'central.missouri.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'MO - Central Missouri',
    @EmailAddress          = 'central.missouri.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '573',
    @PhoneExchangeNumber   = '875',
    @PhoneUnitNumber       = '1290',
    @ApplicationID		   = '2'

 
--	Branch	Branch City			Phone			Fax
--	26		Cedar Rapids, IA	319-393-0043	319-393-2283	cedarrapids.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Cedar Rapids',
		'IA',
        'IA - Cedar Rapids',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '319',                 -- FaxAreaCode
        '393',                 -- FaxExchangeNumber
        '2283',                -- FaxUnitNumber
        'Cedar Rapids, IN - Branch #26',				-- Name
        '319',					-- PhoneAreaCode
        '393',                 -- PhoneExchangeNumber
        '0043',                -- PhoneUnitNumber
		'cedarrapids.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'cedarrapids.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'IA - Cedar Rapids',
    @EmailAddress          = 'cedarrapids.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '319',
    @PhoneExchangeNumber   = '393',
    @PhoneUnitNumber       = '0043',
    @ApplicationID		   = '2'
 
--	Branch	Branch City			Phone			Fax
--	39		West Des Moines, IA	515-225-7060	515-225-4372	westdesmoines.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'West Des Moines',
		'IA',
        'IA - West Des Moines',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '515',                 -- FaxAreaCode
        '225',                 -- FaxExchangeNumber
        '4372',                -- FaxUnitNumber
        'West Des Moines, IA - Branch #39',				-- Name
        '515',					-- PhoneAreaCode
        '225',                 -- PhoneExchangeNumber
        '7060',                -- PhoneUnitNumber
		'westdesmoines.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'westdesmoines.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'IA - West Des Moines',
    @EmailAddress          = 'westdesmoines.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '515',
    @PhoneExchangeNumber   = '225',
    @PhoneUnitNumber       = '7060',
    @ApplicationID		   = '2'
 
--	Branch	Branch City			Phone			Fax
--	77		Fargo, ND & SD		701-280-2423	701-237-5590	fargo.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Fargo',
        'ND/SD - Fargo',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '701',                 -- FaxAreaCode
        '237',                 -- FaxExchangeNumber
        '5590',                -- FaxUnitNumber
        'Fargo, ND/SD - Branch #77',				-- Name
        '701',					-- PhoneAreaCode
        '280',                 -- PhoneExchangeNumber
        '2423',                -- PhoneUnitNumber
		'fargo.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'fargo.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'ND/SD - Fargo',
    @EmailAddress          = 'fargo.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '701',
    @PhoneExchangeNumber   = '280',
    @PhoneUnitNumber       = '2423',
    @ApplicationID		   = '2'    
    
 
--	Branch	Branch City			Phone			Fax
--	25		Omaha, NE			402-330-0432	402-330-1179	omaha.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Omaha',
		'NE',
        'NE - Omaha',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '402',                 -- FaxAreaCode
        '330',                 -- FaxExchangeNumber
        '1179',                -- FaxUnitNumber
        'Omaha, NE - Branch #25',				-- Name
        '402',					-- PhoneAreaCode
        '330',                 -- PhoneExchangeNumber
        '0432',                -- PhoneUnitNumber
		'omaha.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')

INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'omaha.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'NE - Omaha',
    @EmailAddress          = 'omaha.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '402',
    @PhoneExchangeNumber   = '330',
    @PhoneUnitNumber       = '0432',
    @ApplicationID		   = '2'


--	Branch	Branch City			Phone			Fax
--	74		Westminster, CO		303-262-1015	303-262-1017	westminster.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Westminster',
		'CO',
        'CO - Westminster',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '303',                 -- FaxAreaCode
        '262',                 -- FaxExchangeNumber
        '1017',                -- FaxUnitNumber
        'Westminster, CO - Branch #74',				-- Name
        '303',					-- PhoneAreaCode
        '262',                 -- PhoneExchangeNumber
        '1015',                -- PhoneUnitNumber
		'westminster.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')

INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'westminster.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'CO - Westminster',
    @EmailAddress          = 'westminster.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '303',
    @PhoneExchangeNumber   = '262',
    @PhoneUnitNumber       = '1015',
    @ApplicationID		   = '2'


--	Branch	Branch City			Phone			Fax
--	57		Draper, UT & ID		801-572-0111	801-572-0210	draper.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Draper',
        'UT/ID - Draper',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '801',                 -- FaxAreaCode
        '572',                 -- FaxExchangeNumber
        '0210',                -- FaxUnitNumber
        'Draper, UT/ID - Branch #57',				-- Name
        '801',					-- PhoneAreaCode
        '572',                 -- PhoneExchangeNumber
        '0111',                -- PhoneUnitNumber
		'draper.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'draper.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'UT/ID - Draper',
    @EmailAddress          = 'draper.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '801',
    @PhoneExchangeNumber   = '572',
    @PhoneUnitNumber       = '0111',
    @ApplicationID		   = '2'
        
 
--	Branch	Branch City			Phone			Fax
--	45		Mesa Northeast, AZ	480-830-7119	480-830-8794	mesa.northandeast.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Mesa Northeast',
		'AZ',
        'AZ - Mesa Northeast',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '480',                 -- FaxAreaCode
        '830',                 -- FaxExchangeNumber
        '8794',                -- FaxUnitNumber
        'Mesa Northeast, AZ - Branch #45',				-- Name
        '480',					-- PhoneAreaCode
        '830',                 -- PhoneExchangeNumber
        '7119',                -- PhoneUnitNumber
		'mesa.northandeast.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')

INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'mesa.northandeast.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'AZ - Mesa Northeast',
    @EmailAddress          = 'mesa.northandeast.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '480',
    @PhoneExchangeNumber   = '830',
    @PhoneUnitNumber       = '7119',
    @ApplicationID		   = '2'
    
 
--	Branch	Branch City			Phone			Fax
--	55		Mesa Southwest, AZ	480-830-7119	480-830-2240	mesa.southandwest.clm@aoins.com
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Mesa Southwest',
		'AZ',
        'AZ - Mesa Southwest',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '480',                 -- FaxAreaCode
        '830',                 -- FaxExchangeNumber
        '2240',                -- FaxUnitNumber
        'Mesa Southwest, AZ - Branch #55',				-- Name
        '480',					-- PhoneAreaCode
        '830',                 -- PhoneExchangeNumber
        '7119',                -- PhoneUnitNumber
		'mesa.southandwest.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'mesa.southandwest.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'AZ - Mesa Southwest',
    @EmailAddress          = 'mesa.southandwest.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '480',
    @PhoneExchangeNumber   = '830',
    @PhoneUnitNumber       = '7119',
    @ApplicationID		   = '2'

*/


--	Branch	Branch City			Phone			Fax
--	27		Independence, MO	816-795-6330	816-795-6537	independence.clm@aoins.com		(and Kansas)
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Independence',
		'MO',
        'MO/KS - Independence',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '816',                 -- FaxAreaCode
        '795',                 -- FaxExchangeNumber
        '6537',                -- FaxUnitNumber
        'Independence, MO/KS - Branch #27',				-- Name
        '816',					-- PhoneAreaCode
        '795',                 -- PhoneExchangeNumber
        '6330',                -- PhoneUnitNumber
		'independence.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'independence.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'MO/KS - Independence',
    @EmailAddress          = 'independence.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '816',
    @PhoneExchangeNumber   = '795',
    @PhoneUnitNumber       = '6330',
    @ApplicationID		   = '2'


--	Branch	Branch City			Phone			Fax
--	58		Jackson, TN			731-668-4494	731-668-4558	jackson.clm@aoins.com		(and Arkansas)
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Jackson',
		'TN',
        'TN/AR - Jackson',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '731',                 -- FaxAreaCode
        '668',                 -- FaxExchangeNumber
        '4558',                -- FaxUnitNumber
        'Jackson, MO/KS - Branch #58',				-- Name
        '731',					-- PhoneAreaCode
        '668',                 -- PhoneExchangeNumber
        '4494',                -- PhoneUnitNumber
		'jackson.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'jackson.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'TN/AR - Jackson',
    @EmailAddress          = 'jackson.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '816',
    @PhoneExchangeNumber   = '668',
    @PhoneUnitNumber       = '4494',
    @ApplicationID		   = '2'
    

PRINT 'Complete.'


-- Review affected tables
--SELECT * FROM dbo.utb_office WHERE OfficeID = @OfficeID
--SELECT * FROM dbo.utb_office_assignment_type WHERE OfficeID = @OfficeID
--SELECT * FROM dbo.utb_office_contract_state WHERE OfficeID = @OfficeID




