DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 493

------------------------------
-- Add Reports
------------------------------
IF NOT EXISTS 
(
	SELECT *
	FROM
		utb_client_report
	WHERE 
		InsuranceCompanyID = @ToInscCompID
		AND ReportID = 32
)
BEGIN
	INSERT INTO 
		utb_client_report 
	VALUES
	(
		@ToInscCompID
		, NULL
		, 32
		, 0
		, CURRENT_TIMESTAMP
	)

	SELECT 'Report added...'
END
ELSE
BEGIN
	SELECT 'Report already added...'
END

SELECT * FROM utb_client_report WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
