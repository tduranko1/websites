




DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = (SELECT InsuranceCompanyID FROM utb_insurance WHERE Name = 'Amica Mutual Insurance Company')


PRINT 'Creating new Amica office...'


--The office is �Service Center Operations East.�
--Physical Address:
--10 Amica Center Blvd
--Lincoln, RI  02865
--(Mailing address however is: PO Box 6002, Providence, RI  02940
--Phone: 800-652-6422
--Fax: 888-999-6732
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		Address1,
		AddressCity,
		AddressState, 
		AddressZip,
		MailingAddress1,
		MailingAddressCity,
		MailingAddressState,
		MailingAddressZip,
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,		-- InsuranceCompanyID
		'10 Amica Center Blvd',			-- Address1
		'Lincoln',						-- AddressCity
		'RI',						-- AddressState
		'02865',				-- AddressZip
		'PO Box 6002',
		'Providence',
		'RI',
		'02940',
        'Operations East',			-- ClientOfficeId		
        1,                     -- EnabledFlag
        '888',                 -- FaxAreaCode
        '999',                 -- FaxExchangeNumber
        '6732',                -- FaxUnitNumber
        'Amica Service Center Operations East',				-- Name
        '800',					-- PhoneAreaCode
        '652',                 -- PhoneExchangeNumber
        '6422',                -- PhoneUnitNumber
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE AssignmentTypeID in (1,7,14)
    
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


    

PRINT 'Complete.'