--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!! YOU MUST APPLY THE COMMIT OR ROLLBACK AFTER EXECUTING THIS SCRIPT !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SET ANSI_NULLS ON
SET ANSI_WARNINGS ON
GO

BEGIN TRANSACTION


--
--
-- Script for updating APD reference data.
--

PRINT 'Remember to COMMIT or ROLLBACK changes'
PRINT '.'

ALTER TABLE dbo.utb_claim_aspect_status DROP CONSTRAINT ufk_claim_aspect_status_statusid_in_status
ALTER TABLE dbo.utb_document DROP CONSTRAINT ufk_document_statusid_in_status
ALTER TABLE dbo.utb_estimate_summary DROP CONSTRAINT ufk_estimate_summary_estimatesummarytypeid_in_estimate_summary_type
ALTER TABLE dbo.utb_application_permission DROP CONSTRAINT ufk_application_permission_permissionid_in_permission
ALTER TABLE dbo.utb_role_permission DROP CONSTRAINT ufk_role_permission_permissionid_in_permission
ALTER TABLE dbo.utb_user_permission DROP CONSTRAINT ufk_user_permission_permissionid_in_permission
ALTER TABLE dbo.utb_status_task DROP CONSTRAINT ufk_status_task_statusid_in_status
ALTER TABLE dbo.utb_status_task DROP CONSTRAINT ufk_status_task_taskid_in_task
ALTER TABLE dbo.utb_task_assignment_pool DROP CONSTRAINT ufk_task_assignment_pool_taskid_in_task
ALTER TABLE dbo.utb_task_completion_criteria DROP CONSTRAINT ufk_task_completion_criteria_taskid_in_task
ALTER TABLE dbo.utb_task_delay_reason DROP CONSTRAINT ufk_task_delay_reason_taskid_in_task
ALTER TABLE dbo.utb_task_role DROP CONSTRAINT ufk_task_role_taskid_in_task
ALTER TABLE dbo.utb_spawn DROP CONSTRAINT ufk_spawn_workflowid_in_workflow


PRINT '.'
PRINT '.'
PRINT 'Deleting previous data...'
PRINT '.'

DELETE FROM dbo.utb_app_variable
DELETE FROM dbo.utb_estimate_summary_type
DELETE FROM dbo.utb_event
DELETE FROM dbo.utb_permission
DELETE FROM dbo.utb_status
DELETE FROM dbo.utb_status_task
DELETE FROM dbo.utb_spawn
DELETE FROM dbo.utb_task
DELETE FROM dbo.utb_task_assignment_pool
DELETE FROM dbo.utb_task_completion_criteria
DELETE FROM dbo.utb_workflow


PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

DECLARE @ModifiedDateTime AS DATETIME
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- This is a query against an Excel spreadsheet through the OLE DB provider for Jet.


------------------------------------------------------

INSERT INTO dbo.utb_app_variable
SELECT  AppVariableID,
        Description,
        MaxRange,
        MinRange,
        Name,
        SubName,
        Value,
        CASE
          WHEN SysMaintainedFlag = 'Y' THEN 1
          ELSE 0
        END,
        0,
        @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...app_variable$

------------------------------------------------------


INSERT INTO utb_estimate_summary_type
SELECT EstimateSummaryTypeID,
       CategoryCD,
       DisplayOrder, 
       CASE 
           WHEN EnabledFlag = 'Y' THEN 1
           ELSE 0
       END,
       Name,
       CASE 
           WHEN SysMaintainedFlag = 'Y' THEN 1
           ELSE 0
       END,
       0, 
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...estimate_summary_type$


------------------------------------------------------

INSERT INTO utb_event
SELECT EventID,
       ClaimAspectTypeID,
       DisplayOrder, 
       CASE 
           WHEN EnabledFlag = 'Y' THEN 1
           ELSE 0
       END,
       CASE 
           WHEN LogToHistoryFlag = 'Y' THEN 1
           ELSE 0
       END,
       Name,
       0,
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...event$

------------------------------------------------------

INSERT INTO dbo.utb_permission
SELECT PermissionID,
       BusinessFunctionCD,
       CASE 
           WHEN DefaultCreateFlag = 'Y' THEN 1
           ELSE 0
       END,
       CASE 
           WHEN DefaultDeleteFlag = 'Y' THEN 1
           ELSE 0
       END,
       CASE 
           WHEN DefaultReadFlag = 'Y' THEN 1
           ELSE 0
       END,
       CASE 
           WHEN DefaultUpdateFlag = 'Y' THEN 1
           ELSE 0
       END,
       DisplayOrder,
       CASE 
           WHEN EnabledFlag = 'Y' THEN 1
           ELSE 0
       END,
       GroupCD,
       Name,
       1,
       0,
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...Permission$
  
------------------------------------------------------

SET IDENTITY_INSERT utb_spawn ON

INSERT INTO utb_spawn ( SpawnID, WorkflowID, ConditionalValue, ContextNote, CustomProcName, EnabledFlag, SpawningID, SpawningTypeCD, SpawningServiceChannelCD, SysLastUserID, SysLastUpdatedDate ) 
SELECT SpawnID,
       WorkflowID,
       ConditionalValue,
       ContextNote,
       CustomProcName,
       CASE
          WHEN EnabledFlag='Y' THEN 1
          ELSE 0
       END,
       SpawningID,
       SpawningTypeCD,
       SpawningServiceChannelCD,
       0,
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...spawn$

SET IDENTITY_INSERT utb_spawn OFF
 
  
------------------------------------------------------

INSERT INTO utb_status
SELECT StatusID,
       ClaimAspectTypeID,
       DisplayOrder, 
       CASE 
           WHEN EnabledFlag = 'Y' THEN 1
           ELSE 0
       END,
       CASE 
           WHEN LogToHistoryFlag = 'Y' THEN 1
           ELSE 0
       END,
       Name,
       ServiceChannelCD,
       StatusTypeCD,
       0,
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...status$


------------------------------------------------------

INSERT INTO utb_status_task
SELECT StatusID,
       TaskID,
       1,
       0,
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...status_task$



------------------------------------------------------

INSERT INTO dbo.utb_task ( 
	TaskID, 
	NoteTypeDefaultID, 
	CommentCompleteRequiredFlag, 
	CommentNARequiredFlag, 
	DisplayOrder, 
	EnabledFlag, 
	EscalationMinutes, 
	Name, 
	UserTaskFlag, 
	SysMaintainedFlag, 
	SysLastUserID, 
	SysLastUpdatedDate ) 
SELECT TaskID,
       NoteTypeDefaultID,
       CASE 
          WHEN CommentReqOnCompleteFlag = 'Y' then 1
          ELSE 0
       END,
       CASE
          WHEN CommentReqOnNAFlag = 'Y' then 1
          ELSE 0
       END,
       DisplayOrder,
       CASE
          WHEN EnabledFlag = 'Y' THEN 1
          ELSE 0
       END,
       EscalationMinutes,
       Name,
       CASE
          WHEN UserTaskFlag = 'Y' THEN 1
          ELSE 0
       END,
       0,
       0,
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...task$


------------------------------------------------------

INSERT INTO utb_task_assignment_pool
SELECT TaskID,
       ServiceChannelCD,
       AssignmentPoolID,
       0,
       @ModifiedDateTime
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...task_assignment_pool$


------------------------------------------------------
INSERT INTO utb_task_completion_criteria
SELECT TaskCompletionCriteriaID,
       TaskID,
       CAST(CriteriaSQL AS VARCHAR(500)),
       FailureMessage,
       ValidResultSetSQL,
       0,
       @ModifiedDateTime       
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...task_completion_criteria$
       

------------------------------------------------------

INSERT INTO utb_workflow
SELECT WorkflowID,
       InsuranceCompanyID,
       CASE
          WHEN IgnoreDefaultFlag = 'Y' THEN 1
          ELSE 0
       END,
       OriginatorID,
       OriginatorTypeCD,
       0,
       @ModifiedDateTime       
FROM OpenDataSource( 'Microsoft.Jet.OLEDB.4.0',
  'Data Source="F:\data\reference\ReferenceData140.xls";User ID=;Password=;Extended properties=Excel 5.0')...workflow$
       
       

------------------------------------------------------


     
GO
PRINT '.'
PRINT '.'
PRINT 'Modified table(s)...'
PRINT '.'

SELECT * FROM dbo.utb_app_variable
SELECT * FROM dbo.utb_estimate_summary_type
SELECT * FROM dbo.utb_event
SELECT * FROM dbo.utb_permission
SELECT * FROM dbo.utb_spawn
SELECT * FROM dbo.utb_status
SELECT * FROM dbo.utb_task
SELECT * FROM dbo.utb_task_assignment_pool
SELECT * FROM dbo.utb_workflow



GO

ALTER TABLE dbo.utb_estimate_summary 
    ADD CONSTRAINT ufk_estimate_summary_estimatesummarytypeid_in_estimate_summary_type
    FOREIGN KEY (EstimateSummaryTypeID)
    REFERENCES dbo.utb_estimate_summary_type(EstimateSummaryTypeID)


ALTER TABLE dbo.utb_spawn 
    ADD CONSTRAINT ufk_spawn_workflowid_in_workflow
    FOREIGN KEY (WorkflowID)
    REFERENCES dbo.utb_workflow (WorkflowID)

ALTER TABLE dbo.utb_task_role 
    ADD CONSTRAINT ufk_task_role_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task (TaskID)

ALTER TABLE dbo.utb_task_delay_reason 
    ADD CONSTRAINT ufk_task_delay_reason_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task (TaskID)

ALTER TABLE dbo.utb_task_completion_criteria 
    ADD CONSTRAINT ufk_task_completion_criteria_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task (TaskID)

ALTER TABLE dbo.utb_task_assignment_pool 
    ADD CONSTRAINT ufk_task_assignment_pool_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task (TaskID)

ALTER TABLE dbo.utb_status_task 
    ADD CONSTRAINT ufk_status_task_taskid_in_task
    FOREIGN KEY (TaskID)
    REFERENCES dbo.utb_task (TaskID)

ALTER TABLE dbo.utb_claim_aspect_status 
    ADD CONSTRAINT ufk_claim_aspect_status_statusid_in_status
    FOREIGN KEY (StatusID)
    REFERENCES dbo.utb_status (StatusID)

ALTER TABLE dbo.utb_document 
    ADD CONSTRAINT ufk_document_statusid_in_status
    FOREIGN KEY (StatusID)
    REFERENCES dbo.utb_status (StatusID)

ALTER TABLE dbo.utb_status_task 
    ADD CONSTRAINT ufk_status_task_statusid_in_status
    FOREIGN KEY (StatusID)
    REFERENCES dbo.utb_status (StatusID)

ALTER TABLE dbo.utb_application_permission
    ADD CONSTRAINT ufk_application_permission_permissionid_in_permission
    FOREIGN KEY (PermissionID)
    REFERENCES dbo.utb_permission(PermissionID)
    
ALTER TABLE dbo.utb_role_permission
    ADD CONSTRAINT ufk_role_permission_permissionid_in_permission
    FOREIGN KEY (PermissionID)
    REFERENCES dbo.utb_permission(PermissionID)
    
ALTER TABLE dbo.utb_user_permission
    ADD CONSTRAINT ufk_user_permission_permissionid_in_permission
    FOREIGN KEY (PermissionID)
    REFERENCES dbo.utb_permission(PermissionID)
        


PRINT '.'
PRINT '.'
PRINT 'Remember to COMMIT or ROLLBACK changes'
PRINT '!'

-- COMMIT
-- ROLLBACK

GO

