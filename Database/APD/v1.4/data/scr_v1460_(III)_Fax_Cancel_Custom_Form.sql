/******************************************************************************
* SWR 218804 - Add Shop Assignment Fax Cancellation Custom Form
* Author:	   Ramesh Vishegu
* Date:		   2/9/2009
******************************************************************************/

DECLARE @now as datetime
DECLARE @DocumentTypeID as int
DECLARE @DisplayOrder as int

SET @now = CURRENT_TIMESTAMP 

BEGIN TRANSACTION

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Shop Assignment'

IF @DocumentTypeID IS NULL
BEGIN
    PRINT 'Shop Assignment document type not found.'
    RETURN
END

-- Add Shop Assignment as a custom form
IF NOT EXISTS(SELECT FormID FROM utb_form WHERE Name = 'Shop Assignment Cancel')
BEGIN
	INSERT INTO utb_form (
		DocumentTypeID,
		AutoBundlingFlag,
		DataMiningFlag,
		EnabledFlag,
		HTMLPath,
		Name,
		PDFPath,
		PertainsToCD,
		ServiceChannelCD,
		SQLProcedure,
		SystemFlag,
		SysLastUserID,
		SysLastUpdatedDate
	) VALUES (
		@DocumentTypeID,    -- DocumentTypeID,
		1,                  -- AutoBundlingFlag,
		0,                  -- DataMiningFlag,
		1,                  -- EnabledFlag,
		NULL,				-- HTMLPath,
		'Shop Assignment - Cancellation',	-- Name,
		'FaxAssignmentCancel.pdf',			-- PDFPath,
		'S',                -- PertainsToCD,
		'PS',               -- ServiceChannelCD,
		'uspWorkflowSendShopAssignXML',		-- SQLProcedure,
		1,                  -- SystemFlag,
		0,                  -- SysLastUserID,
		@now                -- SysLastUpdatedDate
	)
END

select * from utb_form where  name like 'Shop Assignment - Cancellation'

-- commit
-- rollback