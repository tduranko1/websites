/*****************************************************************************************
Add LA and AR states to contracted states for Unitrin.
Change requested by Linda Lackey via email to Jonathan on 10/22/2007.
******************************************************************************************/

DECLARE @InsuranceCompanyID as int

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Unitrin Business Insurance'

BEGIN TRANSACTION

INSERT INTO utb_client_contract_state
(InsuranceCompanyID, StateCode, UseCEIShopsFlag, SysLastUserID, SysLastUpdatedDate)
SELECT @InsuranceCompanyID, 'LA', 0, 0, CURRENT_TIMESTAMP

INSERT INTO utb_client_contract_state
(InsuranceCompanyID, StateCode, UseCEIShopsFlag, SysLastUserID, SysLastUpdatedDate)
SELECT @InsuranceCompanyID, 'AR', 0, 0, CURRENT_TIMESTAMP

-- View the changes
SELECT * FROM utb_client_contract_state
WHERE InsuranceCompanyID = @InsuranceCompanyID

-- Remember to commit/rollback
-- COMMIT
-- ROLLBACK


