DECLARE @iInscCompanyID INT
SET @iInscCompanyID = 184 -- Texas Farm

----------------------------------------------
-- Modify the constraints on the 
-- utb_claim_aspect_service_channel table
----------------------------------------------
-- Drop Existing Constraint
ALTER TABLE utb_claim_aspect_service_channel DROP CONSTRAINT uck_claim_aspect_service_channel_servicechannelcd
-- Add New Constraint
ALTER TABLE utb_claim_aspect_service_channel WITH CHECK ADD CONSTRAINT uck_claim_aspect_service_channel_servicechannelcd CHECK ([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP')

----------------------------------------------
-- Modify the constraints on the 
-- utb_claim_aspect_status table
----------------------------------------------
-- Drop Existing Constraint
ALTER TABLE utb_claim_aspect_status DROP CONSTRAINT uck_claim_aspect_status_servicechannelcd
-- Add New Constraint
ALTER TABLE utb_claim_aspect_status WITH CHECK ADD CONSTRAINT uck_claim_aspect_status_servicechannelcd CHECK ([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP')

----------------------------------------------
-- Modify the constraints on the 
-- utb_client_service_channel table
----------------------------------------------
-- Drop Existing Constraint
ALTER TABLE utb_client_service_channel DROP CONSTRAINT uck_client_service_channel_servicechannelcd
-- Add New Constraint
ALTER TABLE utb_client_service_channel  WITH CHECK ADD  CONSTRAINT uck_client_service_channel_servicechannelcd CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

----------------------------------------------
-- Modify the constraints on the 
-- utb_client_service_channel table
----------------------------------------------
-- Drop Existing Constraint
ALTER TABLE utb_assignment_type DROP CONSTRAINT uck_assignment_type_servicechanneldefaultcd
-- Add New Constraint
ALTER TABLE utb_assignment_type  WITH CHECK ADD  CONSTRAINT uck_assignment_type_servicechanneldefaultcd CHECK  (([ServiceChannelDefaultCD] = 'DA' or [ServiceChannelDefaultCD] = 'DR' or [ServiceChannelDefaultCD] = 'GL' or [ServiceChannelDefaultCD] = 'IA' or [ServiceChannelDefaultCD] = 'ME' or [ServiceChannelDefaultCD] = 'PS' or [ServiceChannelDefaultCD] = 'SA' or [ServiceChannelDefaultCD] = 'TL' or [ServiceChannelDefaultCD] = 'RRP'))

----------------------------------------------
-- Modify the constraints on the 
-- utb_task_assignment_pool table
----------------------------------------------
-- Drop Existing Constraint
ALTER TABLE utb_task_assignment_pool DROP CONSTRAINT uck_task_assignment_pool_servicechannelcd
-- Add New Constraint
ALTER TABLE utb_task_assignment_pool  WITH CHECK ADD  CONSTRAINT uck_task_assignment_pool_servicechannelcd CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

----------------------------------------------
-- Modify the constraints on the 
-- utb_status table
----------------------------------------------
-- Drop Existing Constraint
ALTER TABLE utb_status DROP CONSTRAINT uck_status_servicechannelcd
-- Add New Constraint
ALTER TABLE utb_status  WITH CHECK ADD  CONSTRAINT uck_status_servicechannelcd CHECK  (([ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP'))

----------------------------------------------
-- Modify the constraints on the 
-- utb_service table
----------------------------------------------
-- Drop Existing Constraint
ALTER TABLE utb_service DROP CONSTRAINT uck_service_servicechannelcd
-- Add New Constraint
ALTER TABLE utb_service  WITH CHECK ADD  CONSTRAINT uck_service_servicechannelcd CHECK  ([ServiceChannelCD] = '?' or [ServiceChannelCD] = '1P' or [ServiceChannelCD] = '3P' or [ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP')

----------------------------------------------
-- Modify the constraints on the 
-- utb_invoice_service table
----------------------------------------------
-- Drop Existing Constraint
ALTER TABLE utb_invoice_service DROP CONSTRAINT uck_invoice_service_servicechannelcd
-- Add New Constraint
ALTER TABLE utb_invoice_service  WITH CHECK ADD  CONSTRAINT uck_invoice_service_servicechannelcd CHECK  ([ServiceChannelCD] = '?' or [ServiceChannelCD] = '1P' or [ServiceChannelCD] = '3P' or [ServiceChannelCD] = 'DA' or [ServiceChannelCD] = 'DR' or [ServiceChannelCD] = 'GL' or [ServiceChannelCD] = 'IA' or [ServiceChannelCD] = 'ME' or [ServiceChannelCD] = 'PS' or [ServiceChannelCD] = 'SA' or [ServiceChannelCD] = 'TL' or [ServiceChannelCD] = 'RRP')