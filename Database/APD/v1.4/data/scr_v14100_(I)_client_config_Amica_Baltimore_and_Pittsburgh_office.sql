-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configuration for Utica New York Metro Regional Office
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Amica Mutual Insurance Company'


PRINT '.'
PRINT '.'
PRINT 'Creating Baltimore Regional Office...'
PRINT '.'


-- Create Office(s)

-- Baltimore Office

SELECT @OfficeID = OfficeID
FROM utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeId = 'Baltimore'
  
IF @OfficeID IS NOT NULL
BEGIN
   -- Office level configurations

   INSERT INTO dbo.utb_office_assignment_type 
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE Name in ('Adverse Subro Desk Review')
END




-- Pittsburgh Office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '1000 Town Center Way',	 -- Address1
                'Suite 100',            -- Address2
                'Canonsburg',           -- AddressCity
                'PA',                   -- AddressState
                '15317',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Pittsburgh',           -- ClientOfficeId
                1,                      -- EnabledFlag
                '866',					    -- FaxAreaCode
                '847',					    -- FaxExchangeNumber
                '1601',					    -- FaxUnitNumber
                'P.O. Box 127',	       -- MailingAddress1
                '',                     -- MailingAddress2
                'Houston',			       -- MailingAddressCity
                'PA',					    -- MailingAddressState
                '15342',				    -- MailingAddressZip
                'Pittsburgh Office',    -- Name
                '800',					    -- PhoneAreaCode
                '242',					    -- PhoneExchangeNumber
                '6422',					    -- PhoneUnitNumber
                NULL,					    -- ReturnDocEmailAddress
                NULL,					    -- ReturnDocFaxAreaCode
                NULL,					    -- ReturnDocFaxExchangeNumber
                NULL,					    -- ReturnDocFaxUnitNumber
                0,						    -- SysLastUserID
                @ModifiedDateTime		 -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Adverse Subro Desk Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


    
-- Review affected tables
SELECT * FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT oat.* 
FROM dbo.utb_office_assignment_type oat 
LEFT JOIN utb_office o ON oat.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT ocs.* 
FROM dbo.utb_office_contract_state  ocs
LEFT JOIN utb_office o ON ocs.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID


--commit 
--rollback

