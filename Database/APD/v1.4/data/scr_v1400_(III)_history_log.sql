-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Populates new column ServiceChannelCD in utb_history_log
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION

PRINT 'Updating utb_history_log...'

SELECT COUNT(*) as 'Null ServiceChannelCD' FROM dbo.utb_history_log WHERE ServiceChannelCD IS NULL
SELECT COUNT(*) as 'Null ClaimAspectServiceChannelID' FROM dbo.utb_history_log WHERE ClaimAspectServiceChannelID IS NULL

    BEGIN TRANSACTION
    ALTER TABLE dbo.utb_history_log
	    DROP CONSTRAINT ufk_history_log_syslastuserid_in_user
    ALTER TABLE dbo.utb_history_log 
        DROP CONSTRAINT uck_history_log_logtype
    COMMIT
    BEGIN TRANSACTION
    CREATE TABLE dbo.Tmp2_utb_history_log
	    (
	    LogID udt_std_id_big NOT NULL IDENTITY (1, 1),
	    ClaimAspectNumber udt_std_int NOT NULL,
	    ClaimAspectServiceChannelID udt_std_id_big NULL,
	    ClaimAspectTypeID udt_std_int_tiny NOT NULL,
	    CompletedDate udt_std_datetime NOT NULL,
	    CompletedRoleID udt_std_int_small NOT NULL,
	    CompletedUserID udt_std_int_big NOT NULL,
	    CreatedDate udt_std_datetime NOT NULL,
	    CreatedRoleID udt_std_int_small NOT NULL,
	    CreatedUserID udt_std_int NOT NULL,
	    Description udt_std_desc_long NULL,
	    EventID udt_std_int_small NULL,
	    LogType udt_std_cd NOT NULL,
	    LynxID udt_std_int_big NOT NULL,
	    NotApplicableFlag udt_std_flag NULL,
	    ReferenceID udt_std_desc_mid NULL,
	    ServiceChannelCD udt_std_cd NULL,
	    StatusID udt_std_int_small NOT NULL,
	    TaskID udt_std_int_small NULL,
	    SysLastUserID udt_std_id NOT NULL,
	    SysLastUpdatedDate udt_sys_last_updated_date NOT NULL
	    )  ON ufg_workflow
    
    SET IDENTITY_INSERT dbo.Tmp2_utb_history_log ON
    
    IF EXISTS(SELECT * FROM dbo.utb_history_log)
	     EXEC('INSERT INTO dbo.Tmp2_utb_history_log (LogID, ClaimAspectNumber, ClaimAspectServiceChannelID, ClaimAspectTypeID, CompletedDate, CompletedRoleID, CompletedUserID, CreatedDate, CreatedRoleID, CreatedUserID, Description, EventID, LogType, LynxID, NotApplicableFlag, ReferenceID, ServiceChannelCD, StatusID, TaskID, SysLastUserID, SysLastUpdatedDate)
		    SELECT h.LogID, 
            h.ClaimAspectNumber, 
            casc.ClaimAspectServiceChannelID, 
            h.ClaimAspectTypeID, 
            h.CompletedDate, 
            h.CompletedRoleID, 
            CONVERT(bigint, h.CompletedUserID), 
            h.CreatedDate, 
            h.CreatedRoleID, 
            h.CreatedUserID, 
            h.Description, 
            h.EventID, 
            h.LogType, 
            h.LynxID, 
            h.NotApplicableFlag, 
            h.ReferenceID, 
            casc.ServiceChannelCD, 
            h.StatusID, 
            h.TaskID, 
            h.SysLastUserID, 
            h.SysLastUpdatedDate 
from          dbo.utb_Claim_Aspect ca
inner join    dbo.utb_Claim_Aspect_Service_Channel casc
on            casc.ClaimAspectID = ca.ClaimAspectID

inner join    dbo.utb_history_log h (HOLDLOCK TABLOCKX)
on            h.LynxID = ca.LynxID
and           h.ClaimAspectNumber = ca.ClaimAspectNumber
and           h.ClaimAspectTypeID = ca.ClaimAspectTypeID
where casc.primaryflag = 1            
            ')
    
    SET IDENTITY_INSERT dbo.Tmp2_utb_history_log OFF
    
    EXECUTE sp_rename N'dbo.utb_history_log', N'Tmp_utb_history_log', 'OBJECT'
    EXECUTE sp_rename N'dbo.Tmp2_utb_history_log', N'utb_history_log', 'OBJECT'
    
    ALTER TABLE dbo.Tmp_utb_history_log DROP CONSTRAINT
	    upk_history_log

    ALTER TABLE dbo.utb_history_log ADD CONSTRAINT
	    upk_history_log PRIMARY KEY CLUSTERED 
	    (
	    LogID
	    ) WITH FILLFACTOR = 100 ON ufg_workflow

    
    CREATE NONCLUSTERED INDEX uix_ie_history_log_lynxid ON dbo.utb_history_log
	    (
	    LynxID
	    ) WITH FILLFACTOR = 90 ON ufg_workflow_index
    
    ALTER TABLE dbo.utb_history_log ADD CONSTRAINT
	    uck_history_log_logtype CHECK (([LogType] = 'T' or [LogType] = 'S' or [LogType] = 'E'))
    
    ALTER TABLE dbo.utb_history_log ADD CONSTRAINT
	    ufk_history_log_syslastuserid_in_user FOREIGN KEY
	    (
	    SysLastUserID
	    ) REFERENCES dbo.utb_user
	    (
	    UserID
	    )
    
    COMMIT

SELECT COUNT(*) as 'Null ServiceChannelCD' FROM dbo.utb_history_log WHERE ServiceChannelCD IS NULL
SELECT COUNT(*) as 'Null ClaimAspectServiceChannelID' FROM dbo.utb_history_log WHERE ClaimAspectServiceChannelID IS NULL

PRINT 'Complete!'


--commit
--rollback

