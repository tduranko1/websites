/************************************************************************************
* Defect # 4487 - Expand the minimum search radius. 
* A = 20, B = 30, C = 40 and D = 50
************************************************************************************/
begin transaction

update utb_app_variable
set Value = 20
where Name = 'Max_Shop_Search_Radius'
  and SubName = 'A'

update utb_app_variable
set Value = 30
where Name = 'Max_Shop_Search_Radius'
  and SubName = 'B'

update utb_app_variable
set Value = 40
where Name = 'Max_Shop_Search_Radius'
  and SubName = 'C'

update utb_app_variable
set Value = 50
where Name = 'Max_Shop_Search_Radius'
  and SubName = 'D'

select * from utb_app_variable
where Name = 'Max_Shop_Search_Radius'

-- commit
-- rollback