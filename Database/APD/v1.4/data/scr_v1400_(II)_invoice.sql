-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Copies PaymentChannelCD from mobile_electronics_results to invoice
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION

PRINT 'Updating utb_invoice...'

SELECT COUNT(*) as 'Null PaymentChannelCD' FROM dbo.utb_invoice WHERE PaymentChannelCD IS NULL

UPDATE dbo.utb_invoice
  SET  PaymentChannelCD = 'STD'

UPDATE dbo.utb_invoice
   SET PaymentChannelCD = me.PaymentChannelMECD
  FROM dbo.utb_invoice i
 INNER JOIN dbo.tmp_utb_mobile_electronics_results me
    ON me.ClaimAspectID = i.ClaimAspectID
 INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON casc.ClaimAspectID = i.ClaimAspectID
   AND casc.ServiceChannelCD = 'ME'

UPDATE dbo.utb_invoice
   SET PaymentChannelCD = me.PaymentChannelBodyCD
  FROM dbo.utb_invoice i
 INNER JOIN dbo.tmp_utb_mobile_electronics_results me
    ON me.ClaimAspectID = i.ClaimAspectID
 INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON casc.ClaimAspectID = i.ClaimAspectID
   AND casc.ServiceChannelCD = 'PS'

UPDATE dbo.utb_invoice
   SET ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
  FROM dbo.utb_invoice i
 INNER JOIN dbo.utb_claim_aspect_service_channel casc
    ON casc.ClaimAspectID = i.ClaimAspectID
   AND casc.PrimaryFlag = 1
 WHERE i.ItemTypeCD = 'I'

SELECT PaymentChannelCD, COUNT(*) FROM dbo.utb_invoice GROUP BY PaymentChannelCD

PRINT 'Complete!'

--commit
--rollback


