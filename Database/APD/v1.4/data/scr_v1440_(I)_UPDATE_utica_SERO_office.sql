 DECLARE @InsuranceCompanyID as int
 DECLARE @ModifiedDateTime       DateTime
SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 259


IF EXISTS
	       ( SELECT OfficeID FROM dbo.utb_office 
	
                WHERE InsuranceCompanyID = @InsuranceCompanyID
                  AND ClientOfficeId = 'SERO'
                  AND ReturnDocEmailAddress= '')
BEGIN


	IF NOT EXISTS(SELECT OfficeID FROM dbo.utb_office 
	
                WHERE InsuranceCompanyID = @InsuranceCompanyID
                  AND ClientOfficeId = 'SERO'
                  AND ReturnDocEmailAddress= 'SEROClaimsMailBox@uticanational.com'
                  AND SysLastUpdatedDate = @ModifiedDateTime )
	BEGIN

		 
 UPDATE dbo.utb_office 
                  SET ReturnDocEmailAddress = 'SEROClaimsMailBox@uticanational.com',
                SysLastUpdatedDate = @ModifiedDateTime
		WHERE InsuranceCompanyID = @InsuranceCompanyID
			 AND ClientOfficeId = 'SERO'
                  AND ReturnDocEmailAddress = ''
		
	END

END


SELECT * FROM dbo.utb_office  WHERE InsuranceCompanyID = @InsuranceCompanyID
