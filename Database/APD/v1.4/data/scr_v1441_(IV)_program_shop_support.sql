/*****************************************************************************
* Program Shop User changes. The following PS tasks will be assigned to the
* Support Program Shop Work Assignment Pool:
* 1. BCIF Required
* 2. Photos Required
* 3. Direction To Pay Confirmation Required
* 4. Repair Completion Verification Required
* 5. Repair Start Verification Required
* Requested by: Tom Harlacher and Jim Bennett
* Requested Date: 9/19/2008
*****************************************************************************/
declare @now as datetime

set @now = CURRENT_TIMESTAMP

begin transaction

-- Create a new entry for BCIF Required and PS
INSERT INTO utb_task_assignment_pool
(TaskID, ServiceChannelCD, AssignmentPoolID, SysLastUserID, SysLastUpdatedDate)
VALUES
(43, 'PS', 8, 0, @now)

-- Create a new entry for Photos Required and PS
INSERT INTO utb_task_assignment_pool
(TaskID, ServiceChannelCD, AssignmentPoolID, SysLastUserID, SysLastUpdatedDate)
VALUES
(37, 'PS', 8, 0, @now)

-- Update Direction To Pay Confirmation Required
UPDATE utb_task_assignment_pool
SET AssignmentPoolID = 8,
    SysLastUpdatedDate = @now
WHERE TaskID = 19
  AND ServiceChannelCD = 'PS'

-- Update Repair Completion Verification Required
UPDATE utb_task_assignment_pool
SET AssignmentPoolID = 8,
    SysLastUpdatedDate = @now
WHERE TaskID = 33
  AND ServiceChannelCD = 'PS'

-- Update Repair Start Verification Required
UPDATE utb_task_assignment_pool
SET AssignmentPoolID = 8,
    SysLastUpdatedDate = @now
WHERE TaskID = 32
  AND ServiceChannelCD = 'PS'

select * from utb_task_assignment_pool
where ServiceChannelCD = 'PS'
  and TaskID in (6, 19, 32, 33, 37, 43)

-- commit
-- rollback

select * from utb_assignment_pool