

/*
	Description:  Update LAFB Bundling to use email instead of FTP
	Author/Date: Walter J. Chesla - 6/28/2011
*/


DECLARE @InsuranceCompanyID     smallint
DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = (select InsuranceCompanyID from utb_insurance where Name = 'Louisiana Farm Bureau')


UPDATE utb_client_bundling
SET ReturnDocRoutingCD = 'EML',
	ReturnDocRoutingValue = '$ADJUSTER$'
WHERE InsuranceCompanyID = @InsuranceCompanyID
AND ReturnDocRoutingCD = 'FTP'



SELECT *
FROM utb_client_bundling
WHERE InsuranceCompanyID = @InsuranceCompanyID