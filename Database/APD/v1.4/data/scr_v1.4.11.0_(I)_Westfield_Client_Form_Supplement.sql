DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 387

------------------------------
-- Add Form Supplements
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_form_supplement 
	WHERE 
		FormID IN 
		(
			SELECT 
				FormID 
			FROM 
				utb_form
			WHERE 
				InsuranceCompanyID = @ToInscCompID
		) 
)
BEGIN
	DECLARE @iFormID INT
	SELECT @iFormID=FormID FROM utb_form	WHERE InsuranceCompanyID = @ToInscCompID

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Expanded Comments', 'FaxXComments.pdf', NULL, '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Shop Instructions', 'ShopInstructions387.pdf', NULL, '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Direction to Pay', 'DTPType387.pdf', NULL, 'uspWorkflowSendShopAssignXML', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, 'NY', 1, NULL, 'DNY COAR Form', 'NY_COAR.pdf', 'PS', 'uspWorkflowSendShopAssignXML', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, 'CT', 1, NULL, 'CT Customer Choice', 'CTAoClaimantConsumerChoice.pdf', 'PS', '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, 'PA', 1, NULL, 'Direction To Pay', 'DTPType387_PA.pdf', 'PS', '', 0, CURRENT_TIMESTAMP

	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, NULL, 1, NULL, 'Repair Acceptance', 'RepairAcceptance387.pdf', NULL, 'uspWorkflowSendShopAssignXML', 0, CURRENT_TIMESTAMP
	
		
	INSERT INTO 
		utb_form_supplement 
	SELECT @iFormID, NULL, 'OH', 1, NULL, 'OHIO Expanded Comments', 'Ohio.pdf', 'PS', '', 0, CURRENT_TIMESTAMP
	
	SELECT 'Client Form Supplements added...'
END
ELSE
BEGIN
	SELECT 'Client Form Supplements already added...'
END

SELECT * FROM utb_form_supplement WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
