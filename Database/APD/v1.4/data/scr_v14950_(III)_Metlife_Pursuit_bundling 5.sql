DELETE FROM dbo.utb_client_bundling
from utb_client_bundling
WHERE BundlingID in (SELECT BundlingID 
                     FROM utb_bundling 
                     WHERE Name in ('Closing - Audit Complete',
                                    'Closing Supplement', 
                                    'Closing - Total Loss', 
                                    'SHOP: Desk Audit Fax Cover Page'))
and InsuranceCompanyID = 176