-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configuration for Utica New York Metro Regional Office
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Amica Mutual Insurance Company'


PRINT '.'
PRINT '.'
PRINT 'Creating Central Massachusetts Office...'
PRINT '.'


-- Create Office(s)

-- Central Massachusetts Office office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '1 Research Drive',		-- Address1
                'Suite 401B',           -- Address2
                'Westborough',          -- AddressCity
                'MA',                   -- AddressState
                '01581',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Central Mass',         -- ClientOfficeId
                1,                      -- EnabledFlag
                '866',					-- FaxAreaCode
                '381',					-- FaxExchangeNumber
                '3239',					-- FaxUnitNumber
                'PO Box 5201',			-- MailingAddress1
                NULL,                   -- MailingAddress2
                'Westborough',			-- MailingAddressCity
                'MA',					-- MailingAddressState
                '01581',				-- MailingAddressZip
                'Central Massachusetts Office', -- Name
                '800',					-- PhoneAreaCode
                '242',					-- PhoneExchangeNumber
                '6422',					-- PhoneUnitNumber
                NULL,					-- ReturnDocEmailAddress
                NULL,					-- ReturnDocFaxAreaCode
                NULL,					-- ReturnDocFaxExchangeNumber
                NULL,					-- ReturnDocFaxUnitNumber
                0,						-- SysLastUserID
                @ModifiedDateTime		-- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Adverse Subro Desk Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


PRINT '.'
PRINT '.'
PRINT 'Creating Southeastern Massachusetts Office...'
PRINT '.'


-- Create Southeastern Massachusetts Office

-- Southeastern Massachusetts Office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '596 Paramount Drive',  -- Address1
                '',                     -- Address2
                'Raynham',				-- AddressCity
                'MA',                   -- AddressState
                '02767',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Southeastern Mass',    -- ClientOfficeId
                1,                      -- EnabledFlag
                '866',					-- FaxAreaCode
                '759',					-- FaxExchangeNumber
                '3140',					-- FaxUnitNumber
                'PO Box 529',			-- MailingAddress1
                NULL,                   -- MailingAddress2
                'East Taunton',			-- MailingAddressCity
                'MA',					-- MailingAddressState
                '02718',				-- MailingAddressZip
                'Southeastern Massachusetts Office', -- Name
                '800',					-- PhoneAreaCode
                '242',					-- PhoneExchangeNumber
                '6422',					-- PhoneUnitNumber
                NULL,					-- ReturnDocEmailAddress
                NULL,					-- ReturnDocFaxAreaCode
                NULL,					-- ReturnDocFaxExchangeNumber
                NULL,					-- ReturnDocFaxUnitNumber
                0,						-- SysLastUserID
                @ModifiedDateTime		-- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Adverse Subro Desk Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


-- Create Western Massachusetts Office

-- Western Massachusetts Office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '330 Whitney Avenue',   -- Address1
                'Suite 710',            -- Address2
                'Holyoke',				-- AddressCity
                'MA',                   -- AddressState
                '01040',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Western Mass',         -- ClientOfficeId
                1,                      -- EnabledFlag
                '866',					-- FaxAreaCode
                '381',					-- FaxExchangeNumber
                '3244',					-- FaxUnitNumber
                'Northeast Regional Scan Center',-- MailingAddress1
                'PO Box 9690',          -- MailingAddress2
                'Providence',			-- MailingAddressCity
                'RI',					-- MailingAddressState
                '02940',				-- MailingAddressZip
                'Western Massachusetts Office', -- Name
                '800',					-- PhoneAreaCode
                '242',					-- PhoneExchangeNumber
                '6422',					-- PhoneUnitNumber
                NULL,					-- ReturnDocEmailAddress
                NULL,					-- ReturnDocFaxAreaCode
                NULL,					-- ReturnDocFaxExchangeNumber
                NULL,					-- ReturnDocFaxUnitNumber
                0,						-- SysLastUserID
                @ModifiedDateTime		-- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Adverse Subro Desk Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


-- Create Rhode Island Sales & Client Services

-- Western Rhode Island Sales & Client Services Office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                'Rhode Island Claims',	-- Address1
                '10 Amica Center Boulevard',	-- Address2
                'Lincoln',				-- AddressCity
                'RI',                   -- AddressState
                '02865',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Rhode Island',         -- ClientOfficeId
                1,                      -- EnabledFlag
                '866',					-- FaxAreaCode
                '774',					-- FaxExchangeNumber
                '3318',					-- FaxUnitNumber
                'PO Box 6008',			-- MailingAddress1
                NULL,					-- MailingAddress2
                'Providence',			-- MailingAddressCity
                'RI',					-- MailingAddressState
                '02940',				-- MailingAddressZip
                'Rhode Island Sales & Client Services', -- Name
                '800',					-- PhoneAreaCode
                '242',					-- PhoneExchangeNumber
                '6422',					-- PhoneUnitNumber
                NULL,					-- ReturnDocEmailAddress
                NULL,					-- ReturnDocFaxAreaCode
                NULL,					-- ReturnDocFaxExchangeNumber
                NULL,					-- ReturnDocFaxUnitNumber
                0,						-- SysLastUserID
                @ModifiedDateTime		-- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ('Adverse Subro Desk Review', 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    
    
-- Review affected tables
SELECT * FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT oat.* 
FROM dbo.utb_office_assignment_type oat 
LEFT JOIN utb_office o ON oat.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID

SELECT ocs.* 
FROM dbo.utb_office_contract_state  ocs
LEFT JOIN utb_office o ON ocs.OfficeID = o.OfficeID
WHERE InsuranceCompanyID = @InsuranceCompanyID


--commit 
--rollback
