DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 374
SET @FromInscCompID = 277

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
	SELECT 
		* 
	FROM 
		utb_bundling_document_type bt 
		INNER JOIN utb_client_bundling cb 
			ON cb.ClientBundlingId = bt.BundlingID 
	WHERE 
		InsuranceCompanyID = @ToInscCompID 
)
BEGIN
	DECLARE @MsgTempID INT
	DECLARE @BundlingID INT

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSClosingRepairComplete.xsl%'
	DELETE utb_bundling_document_type WHERE BundlingID = @MsgTempID
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,2,NULL,'I',1,0,0,NULL,0,NULL,1,4,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,1,NULL,1,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,9,NULL,'I',0,0,0,NULL,0,NULL,1,6,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,13,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,51,NULL,'I',0,0,0,NULL,0,NULL,1,5,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSCashOutAlert.xsl%'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @MsgTempID AND DocumentTypeID = 35
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,3,NULL,'I',0,0,1,NULL,1,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	
	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSTLClosing.xsl%'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @MsgTempID AND DocumentTypeID = 13
	UPDATE utb_bundling_document_type SET DocumentTypeID = 41, MandatoryFlag = 1 WHERE BundlingID = @MsgTempID AND DocumentTypeID = 40
	UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @MsgTempID AND DocumentTypeID = 3
	UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @MsgTempID AND DocumentTypeID = 8

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSClosingSupplement.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,10,NULL,'I',0,0,0,'A',0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,8,NULL,'I',0,1,0,NULL,0,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @MsgTempID AND DocumentTypeID = 3
	
	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END
