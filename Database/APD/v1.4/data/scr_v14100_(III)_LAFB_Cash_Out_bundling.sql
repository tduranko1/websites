/******************************************************************************
* LAFB Cash out bundling profile
*   Send via FTP. select photos and the latest estimate.
******************************************************************************/

declare @BundlingID as int
declare @InsuranceCompanyID as int

select @InsuranceCompanyID = InsuranceCompanyID
from utb_insurance 
where Name = 'Louisiana Farm Bureau'

IF @InsuranceCompanyID IS NOT NULL
BEGIN

   BEGIN TRANSACTION
   
   INSERT INTO utb_bundling (
      DocumentTypeID,
      MessageTemplateID,
      EnabledFlag,
      Name,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      35, 
      7,
      1,
      'Cash Out Alert & Close',
      0,
      current_timestamp
   )

   
   SET @BundlingID = SCOPE_IDENTITY()

   -- Add invoice
   INSERT INTO utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      MandatoryFlag,
      SelectionOrder,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) values (
      @BundlingID,
      13,
      'I',
      0,
      0,
      0,
      null,
      0,
      1,
      1,
      0,
      0,
      current_timestamp
   )

   -- Add Estimate
   INSERT INTO utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      MandatoryFlag,
      SelectionOrder,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) values (
      @BundlingID,
      3,
      'I',
      0,
      0,
      0,
      'O',
      0,
      1,
      2,
      1,
      0,
      current_timestamp
   )

   -- Add photos
   INSERT INTO utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      MandatoryFlag,
      SelectionOrder,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) values (
      @BundlingID,
      8,
      'I',
      0,
      1,
      0,
      NULL,
      0,
      0,
      3,
      0,
      0,
      current_timestamp
   )
   
   INSERT INTO utb_client_bundling (
      BundlingID,
      InsuranceCompanyID,
      ReturnDocPackageTypeCD,
      ReturnDocRoutingCD,
      ReturnDocRoutingValue,
      ServiceChannelCD,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,
      @InsuranceCompanyID,
      'PDF',
      'FTP',
      '$CONFIG$:ClientDocument/Insurance[@ID="279"]/FTP',
      'PS',
      0,
      CURRENT_TIMESTAMP
   )
   
   select * from utb_client_bundling where InsuranceCompanyID = @InsuranceCompanyID
   select * from utb_bundling where bundlingID = @BundlingID
   select * from utb_bundling_document_type where BundlingID = @BundlingID

END

-- commit
-- rollback

