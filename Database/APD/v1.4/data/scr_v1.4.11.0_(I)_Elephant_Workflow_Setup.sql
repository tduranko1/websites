-----------------------------------------------------------------------------------------------------------
-- This script will update the configuration to support multiple assignments per service channel
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------  

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @ModifiedDateTime       DateTime
DECLARE @DBMethod       		VARCHAR(60)
DECLARE @MDBPath       			VARCHAR(100)
DECLARE @DBUserid       		VARCHAR(20)
DECLARE @DBPassword       		VARCHAR(20)

------------------------------------------------------
-- Workflow Client Configuration    
------------------------------------------------------
SET @ModifiedDateTime = Current_Timestamp
SET @InsuranceCompanyID = 374

-- MDB Vars    
SET @DBMethod = 'Microsoft.Jet.OLEDB.4.0'
SET @MDBPath = 'C:\Temp\referencedata\reference_data_1-4.mdb'
SET @DBUserid = 'admin'
SET @DBPassword = ''
------------------------------------------------------
-- Workflow     

PRINT '.'
PRINT '.'
PRINT 'Deleting previous data...'
PRINT '.'

------------------------------------------------------
-- Workflow Test - This will show you if you have any
-- existing spawn before doing the delete below
------------------------------------------------------
/*SELECT 
	* 
FROM 
	utb_spawn 
WHERE 
	WorkflowID IN
	(
		SELECT WorkflowID FROM utb_workflow WHERE InsuranceCompanyID = @InsuranceCompanyID
	)
*/
	
------------------------------------------------------
-- Workflow DELETE - This will DELETE any existing spawns 
-- and then DELETE the workflow for this insc company 
-- before the next deploy 
------------------------------------------------------
--DELETE FROM dbo.utb_task_escalation --(Non for NCFB)
DELETE FROM dbo.utb_spawn
DELETE FROM dbo.utb_workflow

-----------------------------------------------------------------------------------
-- This is a query against an Access Database through the OLE DB provider for Jet.
-----------------------------------------------------------------------------------

INSERT INTO dbo.utb_workflow
SELECT  WorkflowID,
        InsuranceCompanyID,
        IgnoreDefaultFlag,
        OriginatorID,
        OriginatorTypeCD,
        0,
        @ModifiedDateTime
--		OPENROWSET(@DBMethod,@MDBPath;@DBUserid;@DBPassword,utb_workflow)
FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
    'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_workflow)

------------------------------------------------------

SET IDENTITY_INSERT utb_spawn ON

INSERT INTO dbo.utb_spawn (SpawnID, WorkflowID, ConditionalValue, ContextNote, CustomProcName, EnabledFlag, SpawningID, SpawningServiceChannelCD, SpawningTypeCD, SysLastUserID, SysLastUpdatedDate)
SELECT SpawnID,
       WorkflowID,
       ConditionalValue,
       ContextNote, 
       CustomProcName, 
       EnabledFlag,
       SpawningID,
       SpawningServiceChannelCD,
       SpawningTypeCD,
       0,
       @ModifiedDateTime
--	OPENROWSET(@DBMethod,@MDBPath;@DBUserid;@DBPassword,utb_workflow)
FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
     'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_spawn)

SET IDENTITY_INSERT utb_spawn OFF

------------------------------------------------------

-- Review affected tables

PRINT '.'
PRINT '.'
PRINT 'Review affected tables...'
PRINT '.'

SELECT * FROM dbo.utb_client_assignment_type WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_client_service_channel WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_office_assignment_type WHERE OfficeID IN (SELECT OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
SELECT * FROM dbo.utb_workflow WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_spawn WHERE WorkflowID IN (SELECT WorkflowID FROM dbo.utb_workflow WHERE InsuranceCompanyID = @InsuranceCompanyID)
SELECT * FROM dbo.utb_task_escalation 


commit 
--rollback
                                                                                                                                              