--SELECT * FROM utb_form WHERE [Name] LIKE '%BCIF - Vehicle Valuation%'
--SELECT * FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%Total Loss Alert - Adjuster|Messages/msgPSQBETLAlert.xsl%'

DECLARE @ToInscCompID SMALLINT
DECLARE @MsgTempID INT
SET @ToInscCompID = 374

------------------------------
-- Add Fax Templates - BCIF
------------------------------
IF NOT EXISTS (SELECT * FROM utb_form WHERE insurancecompanyid = @ToInscCompID AND [Name] = 'BCIF - Vehicle Valuation' )
BEGIN
	INSERT INTO utb_form 
	SELECT 
		  NULL
		  ,40
		  ,NULL
		  ,@ToInscCompID
		  ,NULL
		  ,NULL
		  ,1
		  ,NULL
		  ,0
		  ,1
		  ,NULL
		  ,'Forms/EL_BCIF.asp'
		  ,'BCIF - Vehicle Valuation'
		  ,'EL_BCIF.pdf'
		  ,'S'
		  ,'PS'
		  ,'uspCFBCIFGetXML'
		  ,0
		  ,0
		  ,CURRENT_TIMESTAMP

------------------------------
-- Add Document Types 
------------------------------
SELECT @MsgTempID=mt.MessageTemplateID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSTLClosing.xsl%'
INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,40,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

  SELECT 'Fax Template BCIF added...'
END
ELSE
BEGIN
	SELECT 'Fax Template BCIF already added...'
END

SELECT * FROM utb_form WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
SELECT * FROM utb_bundling_document_type WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
