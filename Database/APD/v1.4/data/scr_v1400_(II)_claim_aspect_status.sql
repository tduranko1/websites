-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Populates the new claim_aspect_status table with status records derived from claim,
--        vehicle, and property aspect records in claim_aspect
--      * Populates claim_aspect_status with new records representing vehicle/service channel
--        combination statuses based on current vehicle status in claim_aspect
--      * Populates claim_aspect_status with electronic and fax statuses for each vehicle
--        copied from claim_aspect
--
----------------------------------------------------------------------------------------
--  Dependancies:  utb_status must be populated from reference list and 
--                 utb_service_channel_schedule_change_reason must be populated from 
--                 the reference list before this is executed 
--
----------------------------------------------------------------------------------------
begin transaction
truncate table dbo.utb_claim_aspect_status

-- commit
-- rollback

/**********************************************************************
Check if the Temp Tables exist in the database, if so remove them.
**********************************************************************/

if EXISTS (select name from tempdb..sysobjects where name LIKE '#claim_aspect%')
    drop table #claim_aspect
if EXISTS (select name from tempdb..sysobjects where name LIKE '#veh_aspect%')
    drop table #veh_aspect
if EXISTS (select name from tempdb..sysobjects where name LIKE '#veh_aspect_disp_me%')
    drop table #veh_aspect_disp_me
if EXISTS (select name from tempdb..sysobjects where name LIKE '#veh_aspect_disp_other%')
    drop table #veh_aspect_disp_other

-- Claims / Property
SELECT ca.ClaimAspectID INTO #claim_aspect
  FROM dbo.tmp_utb_claim_aspect ca
    WHERE ca.ClaimAspectTypeID IN (0, 4)
CREATE INDEX temp1 ON #claim_aspect(ClaimAspectID)

-- Vehicles
SELECT ca.ClaimAspectID INTO #veh_aspect
  FROM dbo.tmp_utb_claim_aspect ca
    WHERE ca.ClaimAspectTypeID IN (9)
CREATE INDEX temp2 ON #veh_aspect(ClaimAspectID)

SELECT va.ClaimAspectID, 
       CASE ca.StatusID WHEN '995' THEN 'VD' WHEN '994' THEN 'CA' WHEN '998' THEN 'CL' WHEN '100' THEN 'OP' END StatusCD,
       CASE WHEN me.ClaimAspectID IS NOT NULL THEN 1 ELSE 0 END AS MERecord,
       me.DispositionMECd,
       me.DispositionGlassCd,
       me.DispositionBodyCd
INTO #veh_aspect_disp_me
  FROM #veh_aspect va
 INNER JOIN dbo.tmp_utb_claim_aspect ca
    ON ca.ClaimAspectID = va.ClaimAspectID
 LEFT JOIN dbo.tmp_utb_mobile_electronics_results me 
    ON ca.ClaimAspectID = me.ClaimAspectID
 WHERE ca.ServiceChannelCD = 'ME'
CREATE INDEX temp3 ON #veh_aspect_disp_me(ClaimAspectID)


SELECT va.ClaimAspectID, 
       CASE ca.StatusID WHEN '995' THEN 'VD' WHEN '994' THEN 'CA' WHEN '998' THEN 'CL' WHEN '100' THEN 'OP' END StatusCD
INTO #veh_aspect_disp_other
  FROM #veh_aspect va
 INNER JOIN dbo.tmp_utb_claim_aspect ca
    ON ca.ClaimAspectID = va.ClaimAspectID
 WHERE ca.ServiceChannelCD <> 'ME'
CREATE INDEX temp4 ON #veh_aspect_disp_other(ClaimAspectID)



--**********************************************************************/
-- Claim/Property Statuses for Claim/Property Aspects
--**********************************************************************/

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  t.ClaimAspectID,
              StatusID,
              NULL,
              StatusStartDate,
              NULL,
              SysLastUserID,
              SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #claim_aspect t
           ON ca.ClaimAspectID = t.ClaimAspectID

-- !! DONE


--**********************************************************************/
-- Vehicles Statuses for Vehicle Aspects that aren't ME
--**********************************************************************/

-- All

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              ca.StatusID,
              NULL AS ServiceChannelCD,
              ca.StatusStartDate,
              NULL AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_other t
           ON ca.ClaimAspectID = t.ClaimAspectID


-- Open...
-- Closed...
-- Cancelled...
-- Voided
    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              CASE WHEN ServiceChannelCD = 'PS' AND StatusID = '100'
                   THEN 500
                   WHEN ServiceChannelCD = 'PS' AND StatusID = '994'
                   THEN 598
                   WHEN ServiceChannelCD = 'PS' AND StatusID = '995'
                   THEN 597
                   WHEN ServiceChannelCD = 'PS' AND StatusID = '998'
                   THEN 599
                   WHEN ServiceChannelCD = 'DA' AND StatusID = '100'
                   THEN 101
                   WHEN ServiceChannelCD = 'DA' AND StatusID = '994'
                   THEN 198
                   WHEN ServiceChannelCD = 'DA' AND StatusID = '995'
                   THEN 197
                   WHEN ServiceChannelCD = 'DA' AND StatusID = '998'
                   THEN 199
                   WHEN ServiceChannelCD = 'DR' AND StatusID = '100'
                   THEN 200
                   WHEN ServiceChannelCD = 'DR' AND StatusID = '994'
                   THEN 298
                   WHEN ServiceChannelCD = 'DR' AND StatusID = '995'
                   THEN 297
                   WHEN ServiceChannelCD = 'DR' AND StatusID = '998'
                   THEN 299
              END,
              ca.ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_other t
           ON ca.ClaimAspectID = t.ClaimAspectID
       

    -- Insert claim aspect rows - FAX/ELC

INSERT INTO dbo.utb_claim_aspect_status
(
    ClaimAspectID,
    StatusID,
    ServiceChannelCD,
    StatusStartDate,
    StatusTypeCD,
    SysLastUserID,
    SysLastUpdatedDate
)
SELECT  ca.ClaimAspectID,
        cas.StatusID,
        ca.ServiceChannelCD,
        cas.StatusStartDate,
        CASE cas.ClaimAspectTypeID
          WHEN 2 THEN 'ELC'
          WHEN 10 THEN 'FAX'
        END,
        cas.SysLastUserID,
        cas.SysLastUpdatedDate
    FROM #veh_aspect_disp_other v
  INNER JOIN dbo.Tmp_utb_claim_aspect ca
     ON ca.ClaimAspectId = v.ClaimAspectID
  INNER JOIN dbo.Tmp_utb_claim_aspect cas
     ON cas.LynxID = ca.LynxID 
    AND cas.ClaimAspectNumber = ca.ClaimAspectNumber
    AND cas.ClaimAspectID <> ca.ClaimAspectID
    WHERE cas.ClaimAspectTypeID IN (2, 10)



-- !! DONE

--**********************************************************************/
-- Vehicles Statuses for Vehicle Aspects that ARE ME
--**********************************************************************/

-- Voided

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              995, -- Voided
              NULL AS ServiceChannelCD,
              ca.StatusStartDate,
              NULL AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              497, -- Voided
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Voided
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Voided
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'

-- Glass Void

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              397, -- Voided
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'
         AND t.MERecord = 1
         AND t.DispositionGlassCD <> 'NAP'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Voided
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'
         AND t.MERecord = 1
         AND t.DispositionGlassCD <> 'NAP'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Voided
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'
         AND t.MERecord = 1
         AND t.DispositionGlassCD <> 'NAP'

-- Body Void

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              597, -- Voided
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'
         AND t.MERecord = 1
         AND t.DispositionBodyCD <> 'NAP'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Voided
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'
         AND t.MERecord = 1
         AND t.DispositionBodyCD <> 'NAP'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Voided
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'VD'
         AND t.MERecord = 1
         AND t.DispositionBodyCD <> 'NAP'




-- Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              994, -- Cancelled
              NULL AS ServiceChannelCD,
              ca.StatusStartDate,
              NULL AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'

-- Vehicle Cancelled ME Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              498, -- Cancelled
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionMECD = 'CA'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Cancelled
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionMECD = 'CA'


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Cancelled
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionMECD = 'CA'

-- Vehicle Cancelled ME Complete

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              499, -- Complete
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionMECD NOT IN ('NAP', 'CA')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Cancelled
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionMECD NOT IN ('NAP', 'CA')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Cancelled
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionMECD NOT IN ('NAP', 'CA')

-- Vehicle Cancelled Glass Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              398, -- Cancelled
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionGlassCD = 'CA'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Cancelled
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionGlassCD = 'CA'


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Cancelled
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionGlassCD = 'CA'

-- Vehicle Cancelled Glass Complete

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              399, -- Complete
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionGlassCD NOT IN ('NAP', 'CA')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Cancelled
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionGlassCD NOT IN ('NAP', 'CA')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Cancelled
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionGlassCD NOT IN ('NAP', 'CA')


-- Vehicle Cancelled Body Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              598, -- Cancelled
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionBodyCD = 'CA'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Cancelled
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionBodyCD = 'CA'


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Cancelled
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionBodyCD = 'CA'

-- Vehicle Cancelled Body Complete

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              599, -- Complete
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionBodyCD NOT IN ('NAP', 'CA')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Cancelled
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionBodyCD NOT IN ('NAP', 'CA')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Cancelled
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CA'
         AND t.MERecord = 1
         AND t.DispositionBodyCD NOT IN ('NAP', 'CA')

-- !! DONE


-- Open

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              100, -- Open
              NULL AS ServiceChannelCD,
              ca.StatusStartDate,
              NULL AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'

-- Vehicle Open No ME Rec

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              400, -- Active
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 0

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 0


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 0

-- Vehicle Open ME NAP or NULL

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              400, -- Active
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionMECD IS NULL OR t.DispositionMECD = 'NAP')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionMECD IS NULL OR t.DispositionMECD = 'NAP')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionMECD IS NULL OR t.DispositionMECD = 'NAP')

-- Vehicle Open ME Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              498, -- Cancelled
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionMECD = 'CA')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionMECD = 'CA')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionMECD = 'CA')

-- Vehicle Open ME Complete

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              499, -- Complete
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND NOT (t.DispositionMECD IS NULL OR t.DispositionMECD IN ('CA', 'NAP'))

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND NOT (t.DispositionMECD IS NULL OR t.DispositionMECD IN ('CA', 'NAP'))


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND NOT (t.DispositionMECD IS NULL OR t.DispositionMECD IN ('CA', 'NAP'))

-- Vehicle Open Glass NULL

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              300, -- Active
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionGlassCD IS NULL)

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionGlassCD IS NULL)


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionGlassCD IS NULL)

-- Vehicle Open Glass Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              398, -- Cancelled
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionGlassCD = 'CA')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionGlassCD = 'CA')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionGlassCD = 'CA')

-- Vehicle Open Glass Complete

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              399, -- Complete
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND NOT (t.DispositionGlassCD IS NULL OR t.DispositionGlassCD IN ('CA', 'NAP'))

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND NOT (t.DispositionGlassCD IS NULL OR t.DispositionGlassCD IN ('CA', 'NAP'))


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND NOT (t.DispositionGlassCD IS NULL OR t.DispositionGlassCD IN ('CA', 'NAP'))

-- Vehicle Open Body NULL

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              500, -- Active
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionBodyCD IS NULL)

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionBodyCD IS NULL)


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionBodyCD IS NULL)

-- Vehicle Open Body Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              598, -- Cancelled
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionBodyCD = 'CA')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionBodyCD = 'CA')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND (t.DispositionBodyCD = 'CA')

-- Vehicle Open Body Complete

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              599, -- Complete
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND NOT (t.DispositionBodyCD IS NULL OR t.DispositionBodyCD IN ('CA', 'NAP'))

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND NOT (t.DispositionBodyCD IS NULL OR t.DispositionBodyCD IN ('CA', 'NAP'))


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'OP'
         AND t.MERecord = 1
         AND NOT (t.DispositionBodyCD IS NULL OR t.DispositionBodyCD IN ('CA', 'NAP'))



-- !! DONE


-- Closed

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              998, -- Closed
              NULL AS ServiceChannelCD,
              ca.StatusStartDate,
              NULL AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'

-- Vehicle Closed ME Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              498, -- Cancelled
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionMECD = 'CA'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionMECD = 'CA'


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionMECD = 'CA'

-- Vehicle Closed ME Complete

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              499, -- Complete
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionMECD NOT IN ('NAP', 'CA')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionMECD NOT IN ('NAP', 'CA')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'ME' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionMECD NOT IN ('NAP', 'CA')

-- Vehicle Closed Glass Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              398, -- Cancelled
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionGlassCD = 'CA'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionGlassCD = 'CA'


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionGlassCD = 'CA'

-- Vehicle Closed Glass Complete

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              399, -- Complete
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionGlassCD NOT IN ('NAP', 'CA')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionGlassCD NOT IN ('NAP', 'CA')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'GL' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionGlassCD NOT IN ('NAP', 'CA')


-- Vehicle Closed Body Cancelled

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              598, -- Cancelled
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionBodyCD = 'CA'

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionBodyCD = 'CA'


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionBodyCD = 'CA'

-- Vehicle Closed Body Complete

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              599, -- Complete
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'SC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionBodyCD NOT IN ('NAP', 'CA')

    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              24, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'FAX' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionBodyCD NOT IN ('NAP', 'CA')


    INSERT INTO dbo.utb_claim_aspect_status
    (
        ClaimAspectID,
        StatusID,
        ServiceChannelCD,
        StatusStartDate,
        StatusTypeCD,
        SysLastUserID,
        SysLastUpdatedDate
    )
      SELECT  ca.ClaimAspectID,
              13, -- Sent
              'PS' AS ServiceChannelCD,
              ca.StatusStartDate,
              'ELC' AS StatusTypeCD,
              ca.SysLastUserID,
              ca.SysLastUpdatedDate
        FROM  dbo.Tmp_utb_claim_aspect ca
        INNER JOIN #veh_aspect_disp_me t
           ON ca.ClaimAspectID = t.ClaimAspectID
       WHERE t.StatusCD = 'CL'
         AND t.MERecord = 1
         AND t.DispositionBodyCD NOT IN ('NAP', 'CA')

-- !! DONE

