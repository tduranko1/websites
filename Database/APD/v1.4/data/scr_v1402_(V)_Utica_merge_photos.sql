/*****************************************************************************
* per request from Linda Lackey via email Utica now wants to merge all 
* photos into one pdf.
* --------------------------------------------------------------------------------
*  From: Lackey, Linda 
*  Sent: Friday, April 25, 2008 1:49 PM
*  To: Vishegu, Ramesh
*  Cc: Harlacher, Tom
*  Subject: FW: How Documents are received at Utica


*  Ramesh, 
*  Attached are closing documents for the same claim.  The first pdf  
*  (0425120926) is what Utica receives on their end.   The 2nd pdf 
*  (claim 1114216) is that same email to Utica but the version that is 
*  copied into ClaimPoint in our documents section. The file that utica 
*  receives is 1 email but with individual photos attached.  They must copy 
*  and paste each photo individually into their system.     

*  The question from Utica........ is there anyway to send them the documents 
*  to them in a version that is similar to how we save it in ClaimPoint?  That 
*  way they could just  copy and paste the 1 pdf file into their system for the 
*  documentation.   

*  Thanks, 
*  Linda  

*****************************************************************************/
begin transaction

delete from utb_client_document_exception
where insurancecompanyid = 259
  and DocumentTypeID = 8

select * from utb_client_document_exception
where insurancecompanyid = 259

-- commit
-- rollback