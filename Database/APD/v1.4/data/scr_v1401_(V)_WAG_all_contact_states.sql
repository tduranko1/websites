/******************************************************************************
This script will all non-contracted states for Unitrin to the contracted states.
Change Requestor: Linda Lackey
Request date: 2/11/2008
******************************************************************************/
DECLARE @InsuranceCompanyID as int

begin transaction

select @InsuranceCompanyID = InsuranceCompanyID 
from utb_insurance
where name = 'Western Agricultural'

insert into dbo.utb_client_contract_state
(InsuranceCompanyID, StateCode, UseCEIShopsFlag, SysLastUserID, SysLastUpdatedDate)
select @InsuranceCompanyID, StateCode, 0, 0, current_timestamp
from utb_state_code
where statecode not in (select StateCode 
                        from dbo.utb_client_contract_state 
                        where InsuranceCompanyID = @InsuranceCompanyID)


select *
from dbo.utb_client_contract_state 
where InsuranceCompanyID = @InsuranceCompanyID

print 'Remember to commit/rollback'

-- commit
-- rollback