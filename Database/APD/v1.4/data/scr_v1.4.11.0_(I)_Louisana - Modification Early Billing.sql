DECLARE @InscCompID SMALLINT
DECLARE @BundlingID SMALLINT
DECLARE @DocumentTypeID SMALLINT
DECLARE @NewDocumentTypeID SMALLINT
DECLARE @MessageTemplateID SMALLINT

SET @InscCompID = 279
SET @BundlingID = 0
SET @DocumentTypeID = 0
SET @MessageTemplateID = 0

SET @NewDocumentTypeID = 0

--------------------------------------
-- Get BundleID for 
-- Initial Bill Estimate - Early Bill
--------------------------------------
SELECT
	@BundlingID = b.BundlingID 
	, @DocumentTypeID = b.DocumentTypeID 
	, @MessageTemplateID = b.MEssageTemplateID 
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
WHERE
	cb.InsuranceCompanyID = @InscCompID
	AND b.[Name] = 'Initial Bill Estimate - Early Billing'

--SELECT 	@BundlingID, @DocumentTypeID, @MessageTemplateID

--------------------------------------
-- Initial Bill Estimate - Early Bill
--------------------------------------
IF EXISTS (
	SELECT 
		* 
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE
		bdt.BundlingID = @BundlingID
)
BEGIN
	-- Get Document Type for Initial Bill Estimate
	SELECT @NewDocumentTypeID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Initial Estimate Bundle'
	
	--SELECT * FROM utb_bundling WHERE BundlingID = @BundlingID
	UPDATE utb_bundling SET DocumentTypeID = @NewDocumentTypeID WHERE BundlingID = @BundlingID
	
	SELECT 'Louisana - Initial Bill Estimate - Early Bill -- Updated...'
END
ELSE
BEGIN
	SELECT 'Louisana - Initial Bill Estimate - Early Bill -- Already Updated...'
END

--------------------------------------
-- Initial Bill Estimate - Early Bill
-- Set Mandatory Doc Types
--------------------------------------
IF EXISTS (
	SELECT * FROM utb_bundling_document_type WHERE BundlingID = @BundlingID AND DocumentTypeID IN (3,8,13)
)
BEGIN
	UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID IN (3,8,13)
	
	SELECT 'Louisana - Mandatory Doc -- Updated...'
END
ELSE
BEGIN
	SELECT 'Louisana - Mandatory Doc -- Already Updated...'
END



