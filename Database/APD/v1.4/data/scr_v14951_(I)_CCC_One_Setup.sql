/****************************************************************************************
* CCC One Implementation
* Creator: Ramesh Vishegu
* Date: 03/11/2012
* Notes: We need to update this script when we are ready for production promote.
* Check with Business to see if all DA clients are migrating to CCC One.
*****************************************************************************************/
 
DECLARE @AppVariableId as int
--DECLARE @InsuranceCompanyID_TXFB as int
DECLARE @CommunicationMethodID_CCC as int
 
SELECT @AppVariableId = max(AppVariableID)
  FROM dbo.utb_app_variable
  
--SELECT @InsuranceCompanyID_TXFB = InsuranceCompanyID
--  FROM utb_insurance
-- WHERE Name = 'Texas Farm Bureau'
SELECT @CommunicationMethodID_CCC = CommunicationMethodId
  FROM utb_communication_method
WHERE Name = 'CCC EZ-Net'
  
 
-------------------------------------------------------------------------
-- Let's update All Insurance Companies that have DA channel to use 
--   CCC One testing. 
-------------------------------------------------------------------------
 
--UPDATE utb_insurance
--   SET DeskAuditPreferredCommunicationMethodID = @CommunicationMethodID_CCC
--WHERE InsuranceCompanyID IN (SELECT InsuranceCompanyID 
--                                FROM utb_client_service_channel 
--                               WHERE serviceChannelCD = 'DA')

 
UPDATE utb_insurance
   SET DeskAuditPreferredCommunicationMethodID = @CommunicationMethodID_CCC
WHERE InsuranceCompanyID = 99

UPDATE utb_insurance
SET DemoFlag = 0
WHERE InsuranceCompanyID = 99

 
-------------------------------------------------------------------------
-- Let's add a app variable to store the default CCC One mailbox
-------------------------------------------------------------------------
IF NOT EXISTS(SELECT * 
                FROM utb_app_variable
               WHERE Name = 'Desk_Audit_CCC_Address')
BEGIN
    INSERT INTO dbo.utb_app_variable (
        AppVariableID
        , Description
        , MaxRange
        , MinRange
        , Name
        , SubName
        , Value
        , SysMaintainedFlag
        , SysLastUserID
        , SysLastUpdatedDate
    )
    select 
        @AppVariableId+1
        , 'Desk Audit Unit CCC One Address'
        , NULL
        , NULL
        , 'Desk_Audit_CCC_Address'
        , NULL
        , 42029
        , 1
        , 0
        , current_timestamp
END
 
-- End of script
