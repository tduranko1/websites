-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configuration for Utica New York Metro Regional Office
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 259


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'


-- Create Office(s)

-- New York Metro Regional office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                'One Jericho Plaza',    -- Address1
                '',                     -- Address2
                'Jerrico',              -- AddressCity
                'NY',                   -- AddressState
                '11753',                -- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'NYMRO',                -- ClientOfficeId
                1,                      -- EnabledFlag
                '516',                  -- FaxAreaCode
                '479',                  -- FaxExchangeNumber
                '5001',                 -- FaxUnitNumber
                '2nd floor, Wing A',    -- MailingAddress1
                NULL,                   -- MailingAddress2
                'Jerrico',              -- MailingAddressCity
                'NY',                   -- MailingAddressState
                '11753',                -- MailingAddressZip
                'New York Metro Regional Office', -- Name
                '516',                 -- PhoneAreaCode
                '479',                 -- PhoneExchangeNumber
                '5000',                -- PhoneUnitNumber
                'NYMROClaimsMailbox@UticaNational.com', -- ReturnDocEmailAddress
                '315',                 -- ReturnDocFaxAreaCode
                '734',                 -- ReturnDocFaxExchangeNumber
                '2977',                -- ReturnDocFaxUnitNumber
                0,                     -- SysLastUserID
                @ModifiedDateTime      -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment', 
	            'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code



-- Review affected tables
SELECT * FROM dbo.utb_office WHERE OfficeID = @OfficeID
SELECT * FROM dbo.utb_office_assignment_type WHERE OfficeID = @OfficeID
SELECT * FROM dbo.utb_office_contract_state WHERE OfficeID = @OfficeID


--commit 
--rollback
