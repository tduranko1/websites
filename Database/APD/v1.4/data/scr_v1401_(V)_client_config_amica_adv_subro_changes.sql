-----------------------------------------------------------------------------------------------------------
-- This script will remove the changes made for Adverse Subro Desk Audit and replace them with 
-- Adverse Subro Desk Review. Adverse Subro Desk Audit was setup for Rochester, Philadelphia and Amica Central 
-- office of Amica Insurance. In addition to the above three offices, we need to add Northern New Jersey to the
-- new list
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 196

PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'



PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

-- add Adverse Subro Desk Review to the client service channel list
INSERT INTO utb_client_service_channel
(InsuranceCompanyID, ServiceChannelCD, ClientAuthorizesPaymentFlag, InvoicingModelBillingCD, SysLastUserID, SysLastUpdatedDate)
VALUES
(@InsuranceCompanyID, 'DR', 0, 'B', 0, @ModifiedDateTime)


--Insert Client level configuration
-- Remove Adverse Subro Desk Audit at the client level. This will be changed to Adverse Subro Desk Review
------------------------------------------------------
DELETE FROM dbo.utb_client_assignment_type
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND AssignmentTypeID = 6

-- Add Adverse Subro Desk Review
INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE AssignmentTypeID = 1
  

------------------------------------------------------

-- Office level configurations
-- We will delete the Adverse Subro Desk Audit from each office and then replace with Adverse Subro Desk Review

-- Rochester office configuration
SET @OfficeID = NULL

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeID = 'Rochester'

IF @OfficeID IS NOT NULL
BEGIN
   -- Delete Adverse Subro Desk Audit to Rochester office
   DELETE FROM dbo.utb_office_assignment_type
   WHERE OfficeID = @OfficeID
     AND AssignmentTypeID = 6
     
   -- Add Adverse Subro Desk Review to Rochester office
    INSERT INTO dbo.utb_office_assignment_type
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE AssignmentTypeID = 1
END    


-- Philadelphia office configuration
SET @OfficeID = NULL

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeID = 'Philadelphia'

IF @OfficeID IS NOT NULL
BEGIN
   -- Delete Adverse Subro Desk Audit to Philadelphia office
   DELETE FROM dbo.utb_office_assignment_type
   WHERE OfficeID = @OfficeID
     AND AssignmentTypeID = 6
     
   -- Add Adverse Subro Desk Review to Philadelphia office
   INSERT INTO dbo.utb_office_assignment_type
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE AssignmentTypeID = 1
END    

-- Amica Central office configuration
SET @OfficeID = NULL

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeID = 'Amica Central'

IF @OfficeID IS NOT NULL
BEGIN
   -- Delete Adverse Subro Desk Audit to Rochester office
   DELETE FROM dbo.utb_office_assignment_type
   WHERE OfficeID = @OfficeID
     AND AssignmentTypeID = 6
     
   -- Add Adverse Subro Desk Review to Amica Central office
   INSERT INTO dbo.utb_office_assignment_type
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE AssignmentTypeID = 1
END    

-- Northern New Jersey office configuration
SET @OfficeID = NULL

SELECT @OfficeID = OfficeID
FROM dbo.utb_office
WHERE InsuranceCompanyID = @InsuranceCompanyID
  AND ClientOfficeID = 'Northern New Jersey'

IF @OfficeID IS NOT NULL
BEGIN
   -- Delete Adverse Subro Desk Audit to Rochester office
   DELETE FROM dbo.utb_office_assignment_type
   WHERE OfficeID = @OfficeID
     AND AssignmentTypeID = 6
     
   -- Add Adverse Subro Desk Review to Amica Central office
   INSERT INTO dbo.utb_office_assignment_type
     SELECT  @OfficeID, 
             AssignmentTypeID, 
             0,
             @ModifiedDateTime
       FROM  dbo.utb_assignment_type
       WHERE AssignmentTypeID = 1
END    

-- Review affected tables
SELECT * FROM dbo.utb_client_assignment_type
SELECT * FROM dbo.utb_office_assignment_type


--commit 
--rollback
