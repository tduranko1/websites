
BEGIN TRANSACTION

-- Setup users for MARO office - 92

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Joshua.Canulli@uticanational.com',
    @NameFirst             = 'Joshua',
    @NameLast              = 'Canulli',
    @EmailAddress          = 'Joshua.Canulli@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6664'


-- Setup users for Albany office - 94



-- Setup users for Amherst office - 95

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Carol.Pyne@uticanational.com',
    @NameFirst             = 'Carol',
    @NameLast              = 'Pyne',
    @EmailAddress          = 'Carol.Pyne@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2345'

-- Setup users for ERO office - 96

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'sue.daily@uticanational.com'



-- Setup users for Fast Track office - 97

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Dennis.Hartman@uticanational.com',
    @NameFirst             = 'Dennis',
    @NameLast              = 'Hartman',
    @EmailAddress          = 'Dennis.Hartman@uticanational.com',
    @OfficeID              = 97,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6775'

print 'Done.'

-- COMMIT
-- rollback