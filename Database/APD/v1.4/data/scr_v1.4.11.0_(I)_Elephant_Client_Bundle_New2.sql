DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 374
SET @FromInscCompID = 277

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_bundling b INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE [Description] LIKE '%msgELPSInitialEstimate%') 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		35
		, MessageTemplateID 
		,1
		,'Initial Estimate Bundle'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSInitialEstimate%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Status Update - MDS'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgELPSStatusUpdActionMDS.xsl%'
				
	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

