DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 515

------------------------------
-- Add Message Templates
------------------------------
IF EXISTS (SELECT * FROM utb_office WHERE InsuranceCompanyID = @ToInscCompID )
BEGIN
	UPDATE
		utb_office
	SET
		ClaimNumberFormatJS = '^@1@2@3'
		, ClaimNumberValidJS = '^(\\d{2})([a-zA-Z]{1})(\\d{6})$'
		, ClaimNumberMsgText = 'nnAnnnnnn \n where n is a number and A is a alpha'
	WHERE
		InsuranceCompanyID = @ToInscCompID	
		
	SELECT 'Claim Number Format Updated...'
END
ELSE
BEGIN
	SELECT 'Claim Number Format - Already Updated...'
END

-------------------------------------
SELECT * FROM utb_office WHERE InsuranceCompanyID = @ToInscCompID	
