/*****************************************************************************
 Shop - Desk Audit Fax Cover page bundling
 This bundling will be used by a QCR to send a fax to the shop with the
 message and Audited Estimate on the file.
 *****************************************************************************/
 

DECLARE @DocumentTypeID as int
DECLARE @now as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Shop Audit Notice'

SET @now = CURRENT_TIMESTAMP

BEGIN TRANSACTION
 
IF @DocumentTypeID IS NULL
BEGIN
    SELECT @DocumentTypeID = MAX(DocumentTypeID) + 1
    FROM utb_document_type

    -- Add the document type
    INSERT INTO utb_document_type (
        DocumentTypeID,
        DisplayOrder,
        DocumentClassCD,
        EnabledFlag,
        EstimateTypeFlag,
        Name,
        SysMaintainedFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @DocumentTypeID,
        32,
        'C',
        1,
        0,
        'Shop Audit Notice',
        1,
        0,
        @now
    )

    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('Error inserting record into utb_document_type', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END
END 

-- Add the new message template
INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   'C',  --AppliesToCD,
   1,    --EnabledFlag,
   'Shop Audit Notice|Messages/msgDAShopCoverPage.xsl', --Description,
   'DA', --ServiceChannelCD,
   0,    --SysLastUserID,
   @now  --SysLastUpdatedDate
)

SELECT @MessageTemplateID = SCOPE_IDENTITY()

-- Add the new bundle
INSERT INTO dbo.utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @DocumentTypeID,     -- DocumentTypeID,
   @MessageTemplateID,  -- MessageTemplateID,
   1,                   -- EnabledFlag,
   'SHOP: Desk Audit Fax Cover Page',  -- Name,
   0, -- SysLastUserID,
   @NOW -- SysLastUpdatedDate
)

SELECT @BundlingID = SCOPE_IDENTITY()

-- Add the bundling documents
-- Add Audited Estimate
INSERT INTO dbo.utb_bundling_document_type (
   BundlingID,
   DocumentTypeID,
   DirectionalCD,
   DirectionToPayFlag,
   DuplicateFlag,
   EstimateDuplicateFlag,
   EstimateTypeCD,
   FinalEstimateFlag,
   LossState,
   MandatoryFlag,
   SelectionOrder,
   ShopState,
   VANFlag,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   @BundlingID,   -- BundlingID,
   3,             -- DocumentTypeID,
   'I',           -- DirectionalCD,
   0,             -- DirectionToPayFlag,
   0,             -- DuplicateFlag,
   0,             -- EstimateDuplicateFlag,
   'A',           -- EstimateTypeCD,
   0,             -- FinalEstimateFlag,
   NULL,          -- LossState,
   1,             -- MandatoryFlag,
   1,             -- SelectionOrder,
   NULL,          -- ShopState,
   1,             -- VANFlag,
   0,             -- SysLastUserID,
   @now           -- SysLastUpdatedDate
)

-- Add this bundling to all DA clients
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
) 
SELECT distinct
       @BundlingID,
       InsuranceCompanyID,
       'DA',
       0,
       @now
FROM utb_client_service_channel csc
WHERE ServiceChannelCD = 'DA'

select * from utb_message_template
select * from utb_bundling where bundlingid = @BundlingID
select * from utb_bundling_document_type where bundlingID = @BundlingID
select * from utb_client_bundling where bundlingid = @bundlingid

-- COMMIT
-- ROLLBACK
