DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 194
SET @FromInscCompID = 387

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_bundling b INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE [Description] LIKE '%CIPS%') 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		34
		, MessageTemplateID 
		,1
		,'Acknowledgement of Claim'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSClmAck.xsl%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Closing Supplement'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSClosingSupplement.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Closing - Total Loss'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSTLClosing.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Closing Repair Complete'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPS_ClosingRepairComplete.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		(SELECT DocumentTypeID FROM utb_document_type WHERE [Name] = 'Assignment Cancellation')
		, MessageTemplateID 
		,1
		,'Assignment Cancellation'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSAssignmentCancel%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'No Inspection (Stale) Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSStaleAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Cash Out Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSCashOutAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Status Update'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSStatusUpdAction.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		38
		, MessageTemplateID 
		,1
		,'Total Loss Alert - (Adjuster)'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSTLAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Status Update - MDS'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSStatusUpdActionMDS.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		35
		, MessageTemplateID 
		,1
		,'Initial Estimate'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSInitialEstimate.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		44
		, MessageTemplateID 
		,1
		,'Rental Invoice'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgCIPSRentalInvoice.xsl%'
				
	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

SELECT * FROM utb_bundling WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
