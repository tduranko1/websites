DECLARE @iNewUserID INT
SET @iNewUserID = 0

IF NOT EXISTS 
(
	SELECT * FROM utb_user WHERE emailaddress = 'QBEWSUser@pgwglass.com'
)
BEGIN
	INSERT INTO utb_user VALUES
	( 
		NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 'QBEWSUser@pgwglass.com'
		, 1
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 'QBE-WS'
		, 'User'
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 0
		, 0
		, CURRENT_TIMESTAMP
		, NULL
		, NULL
	)

	SELECT 'New User Added...'

	SELECT @iNewUserID = UserID FROM utb_user WHERE emailaddress = 'QBEWSUser@pgwglass.com'

	IF (@iNewUserID > 0)
	BEGIN
		INSERT INTO utb_user_application VALUES (@iNewUserID,7,CURRENT_TIMESTAMP,NULL, 'QBEWSUser@pgwglass.com', NULL, NULL,0,CURRENT_TIMESTAMP)
		INSERT INTO utb_user_role VALUES (@iNewUserID,17,1,0,CURRENT_TIMESTAMP)
	END
END
ELSE
BEGIN
	SELECT 'User Already Exists...'
END

IF NOT EXISTS 
(
	SELECT * FROM utb_user WHERE emailaddress = 'ElectricWSUser@pgwglass.com'
)
BEGIN
	INSERT INTO utb_user VALUES
	( 
		NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 'ElectricWSUser@pgwglass.com'
		, 1
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 'ELEC-WS'
		, 'User'
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 0
		, 0
		, CURRENT_TIMESTAMP
		, NULL
		, NULL
	)

	SELECT 'New User Added...'

	SELECT @iNewUserID = UserID FROM utb_user WHERE emailaddress = 'ElectricWSUser@pgwglass.com'

	IF (@iNewUserID > 0)
	BEGIN
		INSERT INTO utb_user_application VALUES (@iNewUserID,7,CURRENT_TIMESTAMP,NULL, 'ElectricWSUser@pgwglass.com', NULL, NULL,0,CURRENT_TIMESTAMP)
		INSERT INTO utb_user_role VALUES (@iNewUserID,17,1,0,CURRENT_TIMESTAMP)
	END
END
ELSE
BEGIN
	SELECT 'User Already Exists...'
END

