/********************************************************************
* Desk Audit Summary Report - Custom Form setup
********************************************************************/
-- Create a new document type for this custom form
DECLARE @DocumentTypeID as int
DECLARE @now as datetime
DECLARE @FormID as int
DECLARE @EventID as int

SET @now = CURRENT_TIMESTAMP

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Adverse Subrogation Audit Summary Report'

SELECT @EventID = EventID
FROM utb_event
WHERE Name = 'New Imaged Document'
  AND ClaimAspectTypeID = 9

BEGIN TRANSACTION

IF @DocumentTypeID IS NULL
BEGIN
    SELECT @DocumentTypeID = MAX(DocumentTypeID) + 1
    FROM utb_document_type

    INSERT INTO utb_document_type (
        DocumentTypeID,
        DisplayOrder,
        DocumentClassCD,
        EnabledFlag,
        EstimateTypeFlag,
        Name,
        SysMaintainedFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @DocumentTypeID,
        32,
        'C',
        1,
        0,
        'Adverse Subrogation Audit Summary Report',
        1,
        0,
        @now
    )

    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('Error inserting record into utb_document_type', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END

    SELECT @DocumentTypeID = DocumentTypeID
    FROM utb_document_type
    WHERE Name = 'Adverse Subrogation Audit Summary Report'
END

INSERT INTO utb_form(
    BundlingTaskID,
    DocumentTypeID,
    EventID,
    InsuranceCompanyID,
    LossStateCode,
    ShopStateCode,
    AutoBundlingFlag,
    DataMiningFlag,
    EnabledFlag,
    HTMLPath,
    Name,
    PDFPath,
    PertainsToCD,
    ServiceChannelCD,
    SQLProcedure,
    SystemFlag,
    SysLastUserID,
    SysLastUpdatedDate
) values(
    NULL,
    @DocumentTypeID,
    @EventID,
    NULL,
    NULL,
    NULL,
    0,
    1,
    1,
    'Forms/AdvSubroAuditSummary.htm',
    'Adverse Subrogation Audit Summary Report',
    'AdvSubroAuditSummary.pdf',
    NULL,
    'DR',
    'uspCFAdvSubroAuditGetXML',
    0,
    0,
    current_timestamp
)

SELECT @FormID = SCOPE_IDENTITY()

IF @@error <> 0
BEGIN
    -- SQL Server Error

    RAISERROR  ('Error inserting record into utb_form', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

INSERT INTO utb_form_supplement (
    FormID,
    LossStateCode,
    ShopStateCode,
    EnabledFlag,
    HTMLPath,
    Name,
    PDFPath,
    ServiceChannelCD,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
) values(
    @FormID,
    NULL,
    NULL,
    1,
    'Forms/AdvSubroAuditSummaryXComments.htm',
    'Expanded Comments',
    'AdvSubroAuditSummaryXComments.pdf',
    NULL,
    'uspCFAdvSubroAuditGetXML',
    0,
    current_timestamp
)


IF @@error <> 0
BEGIN
    -- SQL Server Error

    RAISERROR  ('Error inserting record into utb_form', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END
-- now select the table affected
SELECT * FROM utb_document_type
SELECT * FROM utb_form
SELECT * FROM utb_form_supplement


-- COMMIT
-- ROLLBACK
