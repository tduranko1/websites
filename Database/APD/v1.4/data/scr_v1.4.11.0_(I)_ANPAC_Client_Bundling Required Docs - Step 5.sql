DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
DECLARE @BundleID SMALLINT
DECLARE @DocumentTypeID SMALLINT

SET @ToInscCompID = 473
SET @FromInscCompID = 304
SET @BundleID = 0
SET @DocumentTypeID = 0

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
	SELECT 
		* 
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @ToInscCompID
		)
	AND dt.[Name] = 'Estimate EMS'
)
BEGIN
	-- RRP - Closing Inspection Complete
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'RRP - Closing Inspection Complete' AND cb.InsuranceCompanyID = @ToInscCompID

	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Estimate'
	UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	--SELECT * FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Photograph'
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Photograph'
	UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Memo Bill'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Estimate Revision Shop Request'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Approved Estimate Shop Notification'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	--SELECT dt.DocumentTypeID FROM utb_document_type dt WHERE dt.[Name] = 'Estimate EMS'
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_document_type dt WHERE dt.[Name] = 'Estimate EMS'
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,5,NULL,0,0,0,CURRENT_TIMESTAMP)
	
	-- RRP - Closing Supplement
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'RRP - Closing Supplement' AND cb.InsuranceCompanyID = @ToInscCompID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Estimate'
	UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Photograph'
	UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Memo Bill'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Estimate Revision Shop Request'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Approved Estimate Shop Notification'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	--SELECT dt.DocumentTypeID FROM utb_document_type dt WHERE dt.[Name] = 'Estimate EMS'
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_document_type dt WHERE dt.[Name] = 'Estimate EMS'
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,5,NULL,0,0,0,CURRENT_TIMESTAMP)

	-- RRP - Closing Supplement
	SELECT @BundleID = b.BundlingID FROM utb_bundling b INNER JOIN utb_client_bundling cb ON cb.BundlingID = b.BundlingID WHERE b.Name = 'RRP - Closing Total Loss' AND cb.InsuranceCompanyID = @ToInscCompID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Estimate'
	UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Photograph'
	UPDATE utb_bundling_document_type SET MandatoryFlag = 1 WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Memo Bill'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Estimate Revision Shop Request'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Approved Estimate Shop Notification'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'BCIF - Vehicle Evaluation Form'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_bundling_document_type bdt INNER JOIN utb_document_type dt ON dt.DocumentTypeID = bdt.DocumentTypeID WHERE BundlingID = @BundleID AND dt.[Name] = 'Total Loss Determination'
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundleID AND DocumentTypeID = @DocumentTypeID

	SET @DocumentTypeID = 0
	--SELECT dt.DocumentTypeID FROM utb_document_type dt WHERE dt.[Name] = 'Estimate EMS'
	SELECT @DocumentTypeID = dt.DocumentTypeID FROM utb_document_type dt WHERE dt.[Name] = 'Estimate EMS'
	INSERT INTO utb_bundling_document_type VALUES (@BundleID,@DocumentTypeID,NULL,'I',0,0,0,NULL,0,NULL,1,5,NULL,0,0,0,CURRENT_TIMESTAMP)

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END
