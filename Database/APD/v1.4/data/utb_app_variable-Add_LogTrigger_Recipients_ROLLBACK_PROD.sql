IF EXISTS (
	SELECT
		* 
	FROM
		utb_app_variable
	WHERE
		[Name] = 'Trigger_Event_Recipients'
)
BEGIN
	DELETE FROM
		utb_app_variable
	WHERE
		AppVariableID IN (72,73)
	
	SELECT 'App Variables rollback sucessfully...'
END
ELSE
BEGIN
	SELECT 'App Variables already rolledback...'
END