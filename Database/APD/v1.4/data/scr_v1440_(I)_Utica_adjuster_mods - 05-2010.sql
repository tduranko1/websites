
BEGIN TRANSACTION

-- Setup users for MARO office - 92

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Joshua.Canulli@uticanational.com',
    @NameFirst             = 'Joshua',
    @NameLast              = 'Canulli',
    @EmailAddress          = 'Joshua.Canulli@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6664'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Nina.Lavenia@uticanational.com',
    @NameFirst             = 'Nina',
    @NameLast              = 'Lavenia',
    @EmailAddress          = 'Nina.Lavenia@uticanational.com',
    @OfficeID              = 92,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6657'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Robert.Capps@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Stephanie.Doyle@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Beverly.Williamson@uticanational.com'

-- Setup users for Albany office - 94

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Phil.Coniglione@uticanational.com'


update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Kate.Boschock@uticanational.com'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Kate.Crandall@uticanational.com',
    @NameFirst             = 'Kate',
    @NameLast              = 'Crandall',
    @EmailAddress          = 'Kate.Crandall@uticanational.com',
    @OfficeID              = 94,
    @PhoneAreaCode         = '518',
    @PhoneExchangeNumber   = '862',
    @PhoneUnitNumber       = '6238'


-- Setup users for Amherst office - 95

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Julie.McCreedy@uticanational.com'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Steve.Krol@uticanational.com',
    @NameFirst             = 'Steve',
    @NameLast              = 'Krol',
    @EmailAddress          = 'Steve.Krol@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6775'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Julie.Merkt@uticanational.com'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Julie.Schulz@uticanational.com',
    @NameFirst             = 'Julie',
    @NameLast              = 'Schulz',
    @EmailAddress          = 'Julie.Schulz@uticanational.com',
    @OfficeID              = 95,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2358'


-- Setup users for ERO office - 96

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Suzanne.Gable@uticanational.com'


-- Setup users for Fast Track office - 97

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Sarah.Buczek@uticanational.com',
    @NameFirst             = 'Sarah',
    @NameLast              = 'Buczek',
    @EmailAddress          = 'Sarah.Buczek@uticanational.com',
    @OfficeID              = 97,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6711'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Jessica.Majecki@uticanational.com',
    @NameFirst             = 'Jessica',
    @NameLast              = 'Majecki',
    @EmailAddress          = 'Jessica.Majecki@uticanational.com',
    @OfficeID              = 97,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6719'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Marissa.Rivera@uticanational.com',
    @NameFirst             = 'Marissa',
    @NameLast              = 'Rivera',
    @EmailAddress          = 'Marissa.Rivera@uticanational.com',
    @OfficeID              = 97,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6725'


-- Setup users for NYMRO office - 98

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Jennifer.Hodrick@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Christine.Murphy@uticanational.com'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Kathleen.Hirsch@uticanational.com',
    @NameFirst             = 'Kathleen',
    @NameLast              = 'Hirsch',
    @EmailAddress          = 'Kathleen.Hirsch@uticanational.com',
    @OfficeID              = 98,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '5124'



-- Setup users for NERO office - 101

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'James.Donovan@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate()
where logonid = 'Linda.Hourihan@uticanational.com'

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Katherine.Thibault@uticanational.com',
    @NameFirst             = 'Katherine',
    @NameLast              = 'Thibault',
    @EmailAddress          = 'Katherine.Thibault@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '2663'


EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Elaine.Yu@uticanational.com',
    @NameFirst             = 'Elaine',
    @NameLast              = 'Yu',
    @EmailAddress          = 'Elaine.Yu@uticanational.com',
    @OfficeID              = 101,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '7250'



print 'Done.'

-- COMMIT
-- rollback