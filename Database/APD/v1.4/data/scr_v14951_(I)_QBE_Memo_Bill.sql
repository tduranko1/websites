


-- Memo Bill Document Type
IF NOT EXISTS 
	(SELECT 1 FROM utb_document_type WHERE DocumentTypeID = 75)
BEGIN
	INSERT INTO utb_document_type (DocumentTypeID, DisplayOrder, DocumentClassCD, EnabledFlag, EstimateTypeFlag, Name, SysMaintainedFlag, SysLastUserID, SysLastUpdatedDate)
	VALUES (75, 75, 'C', 1, 0, 'Memo Bill', 0, 0, current_timestamp)
END



-- Update QBE to bulk billing
UPDATE utb_client_service_channel
SET InvoicingModelBillingCD = 'B',
	SysLastUserID = 0,
	SysLastUpdatedDate = current_timestamp
WHERE InsuranceCompanyID = 304
AND ServiceChannelCD = 'DA'




-- Add custom form entry for QBE Memo Bill
IF NOT EXISTS
	(SELECT 1 FROM utb_form WHERE HTMLPath = 'Forms/QBEMemoBill.htm')
BEGIN
	INSERT INTO utb_form (DocumentTypeID, InsuranceCompanyID, AutoBundlingFlag, DataMiningFlag, EnabledFlag, HTMLPath, Name, PDFPath, PertainsToCD, ServiceChannelCD, SQLProcedure, SystemFlag, SysLastUserID, SysLastUpdatedDate)
	VALUES (75, 304, 1, 0, 1, 'Forms/QBEMemoBill.htm', 'QBE Memo Bill', 'QBEMemoBill.pdf', 'C', 'DA', 'uspCFQBEMemoBillGetXML', 0, 0, current_timestamp)
END



UPDATE utb_bundling_document_type
SET DocumentTypeID = 75,
	SysLastUserID = 0,
	SysLastUpdatedDate = current_timestamp
WHERE DocumentTypeID = 13
	AND BundlingID in
		(SELECT BundlingID
		FROM utb_bundling_document_type
		WHERE DocumentTypeID = 13 
		and BundlingID in
			(SELECT BundlingID
			FROM utb_client_bundling
			WHERE InsuranceCompanyID = 304
			AND ServiceChannelCD = 'DA'))

