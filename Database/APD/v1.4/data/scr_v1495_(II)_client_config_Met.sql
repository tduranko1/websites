-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configurations for Met in APD.  An insurance company configuration
-- already exists for this Insurance Company but is now defunct.  We will reuse for this new Desk Audit 
-- initiative.
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @PaymentCompanyID       smallint
DECLARE @WorkflowIDSCActivated	smallint
DECLARE @WorkflowIDFirstEstimate smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 176
SET @PaymentCompanyID = 667
SET @WorkflowIDSCActivated = 119
SET @WorkflowIDFirstEstimate = 168


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'

--Insert Insurance Company information.

PRINT '.'
PRINT '.'
PRINT 'Insurance Company...'
PRINT '.'

IF EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
BEGIN
    UPDATE dbo.utb_insurance
      SET  DeskAuditPreferredCommunicationMethodID = 14,	-- Pathways Directory
           Address1 = '700 Quaker Lane',
           Address2 = NULL, 
           AddressCity = 'Warwick', 
           AddressState = 'RI', 
           AddressZip = '02886', 
           AssignmentAtSelectionFlag = 0,	-- Send DRP Assignment Automatically at shop Selection
           AuthorizeLaborRatesFlag = 0, 
           BillingModelCD = 'E',			-- Bill per exposure 
           BusinessTypeCD = 'C',			-- Corporation
           CarrierLynxContactPhone = '239-479-5858',	-- Phone number carrier uses to contact LYNX,
           CFLogoDisplayFlag = 0,			-- Display CF Logo in shop results list on Claimpoint
           ClaimPointCarrierRepSelFlag = 1,		-- Allow selection of client rep on Claimpoint
           ClaimPointDocumentUploadFlag = 1,	-- Allow documents to be uploaded on ClaimPoint
           ClientAccessFlag = 1,			-- Claimpoint Access
           DemoFlag = 0,					-- Demo Insurance Company
           DeskAuditCompanyCD = 'MLAH',		-- Internal Desk Audit Company Code
           DeskReviewLicenseReqFlag = 1,	-- Only Assign audits to state licensed individuals 
           EarlyBillFlag = 0,
           EarlyBillStartDate = NULL,
           EnabledFlag = 1,					-- Active client
           FaxAreaCode = NULL,
           FaxExchangeNumber = NULL,
           FaxUnitNumber = NULL,
           FedTaxId = NULL,
           IngresAccountingId = @PaymentCompanyID,	-- Used by Ingres for billing
           InvoicingModelPaymentCD = 'C',	-- Default Invoicing Model (Bulk / per claim) 
           InvoiceMethodCD = 'P',			-- Paper Invoicing
           LicenseDeterminationCD = 'LOSS', -- Licensing Requirements determined by Loss State, 
           Name = 'MetLife Auto & Home', 
           PhoneAreaCode = '314', 
           PhoneExchangeNumber = '576', 
           PhoneUnitNumber = '6000',
           ReturnDocDestinationCD = 'OFC',	-- Return Docs to Office
           ReturnDocPackageTypeCD = 'PDF',	-- (Return docs as PDFs?)
           ReturnDocRoutingCD = 'EML',		-- Return docs by email 
           TotalLossValuationWarningPercentage = 0.8,	-- TotalLossValuationWarningPercentage, 
           TotalLossWarningPercentage = 0.8,	-- TotalLossWarningPercentage 
           WarrantyPeriodRefinishMinCD = '99',	-- WarrantyPeriodRefinishMinCD,
           WarrantyPeriodWorkmanshipMinCD = '99',	-- WarrantyPeriodWorkmanshipMinCD,
           SysLastUserID = 0,
           SysLastUpdatedDate = @ModifiedDateTime  
        WHERE InsuranceCompanyID =  @InsuranceCompanyID      -- InsuranceCompanyID
END
ELSE
BEGIN
    RAISERROR('Insurance Company ID does not exist in utb_insurance', 16, 1)
    ROLLBACK TRANSACTION
    RETURN
END


--Insert Client level configuration

------------------------------------------------------
PRINT '.'
PRINT '.'
PRINT 'Client Assignment Type...'
PRINT '.'

DELETE FROM dbo.utb_client_assignment_type WHERE InsuranceCompanyID = @InsuranceCompanyID

INSERT INTO dbo.utb_client_assignment_type
SELECT  @InsuranceCompanyID,
        AssignmentTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Pursuit Audit')

------------------------------------------------------

PRINT '.'
PRINT '.'
PRINT 'Client Service Channel...'
PRINT '.'

DELETE FROM dbo.utb_client_service_channel WHERE InsuranceCompanyID = @InsuranceCompanyID

INSERT INTO dbo.utb_client_service_channel
SELECT @InsuranceCompanyID,
       ServiceChannelDefaultCD,
       0,       -- ClientAuthorizesPaymentFlag
       'B',     -- Invoicing Model (bulk)
       0,
       @ModifiedDateTime
  FROM  dbo.utb_assignment_type
  WHERE Name in ( 'Pursuit Audit')


------------------------------------------------------

PRINT '.'
PRINT '.'
PRINT 'Client Contract State...'
PRINT '.'

DELETE FROM dbo.utb_client_contract_state WHERE InsuranceCompanyID = @InsuranceCompanyID

INSERT INTO dbo.utb_client_contract_state
SELECT  @InsuranceCompanyID,
        StateCode,
        0,                   -- UseCEIShopFlag
        0,                   -- SysLastUserID
        @ModifiedDateTime
  FROM  dbo.utb_state_code
  WHERE EnabledFlag = 1


------------------------------------------------------
/*   Bypassing client coveage type as existing records cause issues with the foreign keys
PRINT '.'
PRINT '.'
PRINT 'Client Coverage Type...'
PRINT '.'

ALTER TABLE dbo.utb_claim_aspect DROP CONSTRAINT ufk_claim_aspect_clientcoveragetypeid_in_client_coverage_type
ALTER TABLE dbo.utb_claim_coverage DROP CONSTRAINT ufk_claim_coverage_clientcoveragetypeid_in_client_coverage_type

DELETE FROM dbo.utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID

INSERT INTO dbo.utb_client_coverage_type (InsuranceCompanyID, AdditionalCoverageFlag, ClientCode, CoverageProfileCD, DisplayOrder, EnabledFlag, Name, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        AdditionalCoverageFlag,
        ClientCode,
        CoverageProfileCD,
        DisplayOrder,
        1,
        Name,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_client_coverage_type
  WHERE InsuranceCompanyID = 288    -- Clone AOI's configuration
    AND CoverageProfileCD <> 'RENT'

ALTER TABLE dbo.utb_claim_aspect 
    ADD CONSTRAINT ufk_claim_aspect_clientcoveragetypeid_in_client_coverage_type
    FOREIGN KEY (ClientCoverageTypeID)
    REFERENCES dbo.utb_client_coverage_type(ClientCoverageTypeID)

ALTER TABLE dbo.utb_claim_coverage 
    ADD CONSTRAINT ufk_claim_coverage_clientcoveragetypeid_in_client_coverage_type
    FOREIGN KEY (ClientCoverageTypeID)
    REFERENCES dbo.utb_client_coverage_type(ClientCoverageTypeID)
*/
------------------------------------------------------    

PRINT '.'
PRINT '.'
PRINT 'Client Claim Aspect Type...'
PRINT '.'

DELETE FROM dbo.utb_client_claim_aspect_type WHERE InsuranceCompanyID = @InsuranceCompanyID
    
INSERT INTO dbo.utb_client_claim_aspect_type 
SELECT  @InsuranceCompanyID,
        ClaimAspectTypeID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_claim_aspect_type
  WHERE Name IN ('Claim', 'Assignment', 'Vehicle', 'Fax Assignment')

------------------------------------------------------        

PRINT '.'
PRINT '.'
PRINT 'Client Payment Type...'
PRINT '.'

DELETE FROM dbo.utb_client_payment_type WHERE InsuranceCompanyID = @InsuranceCompanyID
    
INSERT INTO dbo.utb_client_payment_type VALUES (@InsuranceCompanyID, 'I', 0, @ModifiedDateTime)

------------------------------------------------------

PRINT '.'
PRINT '.'
PRINT 'Client Report...'
PRINT '.'

DELETE FROM dbo.utb_client_report WHERE InsuranceCompanyID = @InsuranceCompanyID

INSERT INTO dbo.utb_client_report (InsuranceCompanyID, OfficeID, ReportID, SysLastUserID, SysLastUpdatedDate)
SELECT  @InsuranceCompanyID,
        NULL,
        ReportID,
        0,
        @ModifiedDateTime
  FROM  dbo.utb_report 
  WHERE ReportID IN (1, 2, 3, 4, 5, 6, 7, 14, 23)

------------------------------------------------------

PRINT '.'
PRINT '.'
PRINT 'Client Scripting...'
PRINT '.'

DELETE FROM utb_client_scripting WHERE insurancecompanyid = @InsuranceCompanyID

INSERT INTO utb_client_scripting
		   (
			InsuranceCompanyID,
			AssignmentTypeIDList,
			EnabledFlag,
			Event,
			EventControl,
			InputFormat,
			InputRequestedFlag, 
			InputRequiredFlag,
			InputResponseName,
			InputTag,
			InputTypeCD,
			ScriptGroupName,
			ScriptText,
			SysLastUserID,
			SysLastUpdatedDate
		   )
  VALUES (176, 15, 1, 'LostFocus', 'InsuredBusinessName', NULL, 1, 1, 'InspectionDate', 'InspectionDate', 'T', 'RepairInformation', 'Enter Inspection Date (Yesterday if inspection has already occurred):', 0, CURRENT_TIMESTAMP) 
  
------------------------------------------------------

-- Create Office(s).

PRINT '.'
PRINT '.'
PRINT 'Removing previous office configurations...'
PRINT '.'  

IF EXISTS(SELECT OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID AND ClientOfficeId = 'Pilot')
BEGIN
	-- Remove old configurations
	SELECT @OfficeID = OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID AND ClientOfficeId = 'Pilot'
	
	DELETE FROM dbo.utb_office_assignment_type WHERE OfficeID = @OfficeID
	DELETE FROM dbo.utb_office_contract_state WHERE OfficeID = @OfficeID
	DELETE FROM dbo.utb_office WHERE OfficeID = @OfficeID
END

SET @OfficeID = NULL

PRINT '.'
PRINT '.'
PRINT 'Adding Office...'
PRINT '.'

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,    -- InsuranceCompanyID
                '',						-- Address1
                NULL,                   -- Address2
                'Warwick',				-- AddressCity
                'RI',                   -- AddressState
                '',						-- AddressZip
                NULL,                   -- CCEmailAddress
                NULL,                   -- ClaimNumberFormatJS
                NULL,                   -- ClaimNumberValidJS
                NULL,                   -- ClaimNumberMsgText
                'Pilot',                -- ClientOfficeId
                1,                      -- EnabledFlag
                '',						-- FaxAreaCode
                '',						-- FaxExchangeNumber
                '',						-- FaxUnitNumber
                '',						-- MailingAddress1
                NULL,                   -- MailingAddress2
                '',						-- MailingAddressCity
                '',                     -- MailingAddressState
                '',						-- MailingAddressZip
                'Met Life Auto Home Pilot',	-- Name
                '',						-- PhoneAreaCode
                '',						-- PhoneExchangeNumber
                '',						-- PhoneUnitNumber
                NULL,                   -- ReturnDocEmailAddress
                NULL,                   -- ReturnDocFaxAreaCode
                NULL,                   -- ReturnDocFaxExchangeNumber
                NULL,                   -- ReturnDocFaxUnitNumber
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

PRINT '.'
PRINT '.'
PRINT 'Adding Office Assignment Type...'
PRINT '.'

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Pursuit Audit')
    

PRINT '.'
PRINT '.'
PRINT 'Office Contract State...'
PRINT '.'
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code
    WHERE EnabledFlag = 1

------------------------------------------------------

-- Workflow

PRINT '.'
PRINT '.'
PRINT 'Removing previous workflow configurations...'
PRINT '.'  

IF EXISTS(SELECT WorkflowID FROM dbo.utb_workflow WHERE WorkflowID IN (@WorkflowIDSCActivated, @WorkflowIDFirstEstimate))
BEGIN
	-- Remove old configurations
	DELETE FROM dbo.utb_task_assignment_pool WHERE InsuranceCompanyID = @InsuranceCompanyID	
	DELETE FROM dbo.utb_task_escalation WHERE InsuranceCompanyID = @InsuranceCompanyID
	DELETE FROM dbo.utb_spawn WHERE WorkflowID IN (@WorkflowIDSCActivated, @WorkflowIDFirstEstimate)
	DELETE FROM dbo.utb_workflow WHERE WorkflowID IN (@WorkflowIDSCActivated, @WorkflowIDFirstEstimate)
END

PRINT '.'
PRINT '.'
PRINT 'Adding Workflow...'
PRINT '.'

INSERT INTO dbo.utb_workflow
        (
         WorkflowID, 
         InsuranceCompanyID,
         IgnoreDefaultFlag, 
         OriginatorID, 
         OriginatorTypeCD, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @WorkflowIDSCActivated, -- Workflow ID 
				@InsuranceCompanyID,    -- InsuranceCompanyID
                0,                      -- Ignore Default Flag
                70,						-- Originator ID
                'E',					-- Originator Type
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )

INSERT INTO dbo.utb_workflow
        (
         WorkflowID, 
         InsuranceCompanyID,
         IgnoreDefaultFlag, 
         OriginatorID, 
         OriginatorTypeCD, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @WorkflowIDFirstEstimate, -- Workflow ID 
				@InsuranceCompanyID,    -- InsuranceCompanyID
                0,                      -- Ignore Default Flag
                84,						-- Originator ID
                'E',					-- Originator Type
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )

INSERT INTO dbo.utb_spawn
        (
         WorkflowID,
         ConditionalValue,
         ContextNote,
         CustomProcName,
         EnabledFlag,
         SpawningID,
         SpawningTypeCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @WorkflowIDSCActivated, -- Workflow ID 
				'DA',					-- Conditional Value
                NULL,                   -- Context Note
                NULL,					-- Custom Proc
                1,						-- Enabled Flag
                5,						-- Spawning ID
                'T',					-- Spawning Type
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )

INSERT INTO dbo.utb_spawn
        (
         WorkflowID,
         ConditionalValue,
         ContextNote,
         CustomProcName,
         EnabledFlag,
         SpawningID,
         SpawningTypeCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @WorkflowIDSCActivated,	-- Workflow ID 
				'DA',					-- Conditional Value
                NULL,                   -- Context Note
                NULL,					-- Custom Proc
                1,						-- Enabled Flag
                34,						-- Spawning ID
                'T',					-- Spawning Type
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )

INSERT INTO dbo.utb_spawn
        (
         WorkflowID,
         ConditionalValue,
         ContextNote,
         CustomProcName,
         EnabledFlag,
         SpawningID,
         SpawningTypeCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @WorkflowIDSCActivated,	-- Workflow ID 
				'DA',					-- Conditional Value
                NULL,                   -- Context Note
                NULL,					-- Custom Proc
                1,						-- Enabled Flag
                91,						-- Spawning ID
                'T',					-- Spawning Type
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )

INSERT INTO dbo.utb_spawn
        (
         WorkflowID,
         ConditionalValue,
         ContextNote,
         CustomProcName,
         EnabledFlag,
         SpawningID,
         SpawningTypeCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @WorkflowIDSCActivated,	-- Workflow ID 
				'DA',					-- Conditional Value
                NULL,                   -- Context Note
                NULL,					-- Custom Proc
                1,						-- Enabled Flag
                15,						-- Spawning ID
                'T',					-- Spawning Type
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )

INSERT INTO dbo.utb_spawn
        (
         WorkflowID,
         ConditionalValue,
         ContextNote,
         CustomProcName,
         EnabledFlag,
         SpawningID,
         SpawningTypeCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @WorkflowIDSCActivated,	-- Workflow ID 
				'DA',					-- Conditional Value
                NULL,                   -- Context Note
                NULL,					-- Custom Proc
                1,						-- Enabled Flag
                37,						-- Spawning ID
                'T',					-- Spawning Type
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )

INSERT INTO dbo.utb_spawn
        (
         WorkflowID,
         ConditionalValue,
         ContextNote,
         CustomProcName,
         EnabledFlag,
         SpawningID,
         SpawningTypeCD,
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @WorkflowIDFirstEstimate,	-- Workflow ID 
				NULL,					-- Conditional Value
                NULL,                   -- Context Note
                NULL,					-- Custom Proc
                1,						-- Enabled Flag
                21,						-- Spawning ID
                'T',					-- Spawning Type
                0,                      -- SysLastUserID
                @ModifiedDateTime       -- SysLastUpdatedDate
        )

INSERT INTO dbo.utb_task_escalation
		(
		 TaskID, 
		 InsuranceCompanyID, 
		 ServiceChannelCD, 
		 EscalationMinutes, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 5,						-- Task ID
				@InsuranceCompanyID,	-- Insurance Company ID
				'DA',					-- Service Channel
				60,					    -- Escalation Minutes
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

INSERT INTO dbo.utb_task_escalation
		(
		 TaskID, 
		 InsuranceCompanyID, 
		 ServiceChannelCD, 
		 EscalationMinutes, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 15,						-- Task ID
				@InsuranceCompanyID,	-- Insurance Company ID
				'DA',					-- Service Channel
				10080,					-- Escalation Minutes
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

INSERT INTO dbo.utb_task_escalation
		(
		 TaskID, 
		 InsuranceCompanyID, 
		 ServiceChannelCD, 
		 EscalationMinutes, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 34,						-- Task ID
				@InsuranceCompanyID,	-- Insurance Company ID
				'DA',					-- Service Channel
				60,					-- Escalation Minutes
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

INSERT INTO dbo.utb_task_escalation
		(
		 TaskID, 
		 InsuranceCompanyID, 
		 ServiceChannelCD, 
		 EscalationMinutes, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 37,						-- Task ID
				@InsuranceCompanyID,	-- Insurance Company ID
				'DA',					-- Service Channel
				10080,					-- Escalation Minutes
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

INSERT INTO dbo.utb_task_escalation
		(
		 TaskID, 
		 InsuranceCompanyID, 
		 ServiceChannelCD, 
		 EscalationMinutes, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 91,						-- Task ID
				@InsuranceCompanyID,	-- Insurance Company ID
				'DA',					-- Service Channel
				60,					    -- Escalation Minutes
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

INSERT INTO dbo.utb_task_assignment_pool
		(
		 TaskID, 
		 ServiceChannelCD, 
		 InsuranceCompanyID, 
		 AssignmentPoolID, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 5,						-- Task ID
				'DA',					-- Service Channel
				@InsuranceCompanyID,	-- Insurance Company ID
				1,					    -- Assignmnent Pool
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

INSERT INTO dbo.utb_task_assignment_pool
		(
		 TaskID, 
		 ServiceChannelCD, 
		 InsuranceCompanyID, 
		 AssignmentPoolID, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 15,						-- Task ID
				'DA',					-- Service Channel
				@InsuranceCompanyID,	-- Insurance Company ID
				1,					    -- Assignmnent Pool
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

INSERT INTO dbo.utb_task_assignment_pool
		(
		 TaskID, 
		 ServiceChannelCD, 
		 InsuranceCompanyID, 
		 AssignmentPoolID, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 34,						-- Task ID
				'DA',					-- Service Channel
				@InsuranceCompanyID,	-- Insurance Company ID
				1,					    -- Assignmnent Pool
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

INSERT INTO dbo.utb_task_assignment_pool
		(
		 TaskID, 
		 ServiceChannelCD, 
		 InsuranceCompanyID, 
		 AssignmentPoolID, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 37,						-- Task ID
				'DA',					-- Service Channel
				@InsuranceCompanyID,	-- Insurance Company ID
				1,					    -- Assignmnent Pool
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

INSERT INTO dbo.utb_task_assignment_pool
		(
		 TaskID, 
		 ServiceChannelCD, 
		 InsuranceCompanyID, 
		 AssignmentPoolID, 
		 SysLastUserID, 
		 SysLastUpdatedDate
		)
		VALUES( 91,						-- Task ID
				'DA',					-- Service Channel
				@InsuranceCompanyID,	-- Insurance Company ID
				1,					    -- Assignmnent Pool
				0,						-- SysLastUser ID
				@ModifiedDateTime		-- SysLastUpdatedDate
		)

		
------------------------------------------------------

-- Review affected tables

PRINT '.'
PRINT '.'
PRINT 'Review affected tables...'
PRINT '.'

SELECT * FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_client_assignment_type WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_client_claim_aspect_type WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_client_contract_state WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_client_coverage_type WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_client_payment_type WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_client_report WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_office_assignment_type WHERE OfficeID IN (SELECT OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
SELECT * FROM dbo.utb_office_contract_state WHERE OfficeID IN (SELECT OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
SELECT * FROM dbo.utb_workflow WHERE InsuranceCompanyID = @InsuranceCompanyID
SELECT * FROM dbo.utb_spawn WHERE WorkflowID IN (SELECT WorkflowID FROM dbo.utb_workflow WHERE InsuranceCompanyID = @InsuranceCompanyID)
SELECT * FROM dbo.utb_task_escalation WHERE InsuranceCompanyID = @InsuranceCompanyID


--commit 
--rollback
