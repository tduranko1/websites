DECLARE @iInscCompanyID INT
SET @iInscCompanyID = 184 -- Texas Farm

----------------------------------------------
-- Modify the column constraints on the tables
----------------------------------------------
-- utb_claim_aspect_service_channel
EXEC sys.sp_addextendedproperty @name=N'Codes', @value=N'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|CS|Choice Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'utb_claim_aspect_service_channel', @level2type=N'CONSTRAINT',@level2name=N'uck_claim_aspect_service_channel_servicechannelcd'

-- Check it
IF NOT EXISTS (
    SELECT  Distinct 'ClientServiceChannels',
            NULL,
            csc.ServiceChannelCD,
            urc.Name
    FROM    utb_client_service_channel csc
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') urc on csc.ServiceChannelCD = urc.Code
	WHERE csc.InsuranceCompanyID = @iInscCompanyID
    AND csc.ServiceChannelCD = 'CS'
)
BEGIN
	SELECT 'ERROR - utb_claim_aspect_service_channel column extended properties failed to set...'
END
ELSE
BEGIN
	SELECT 'SUCCESSFUL (utb_claim_aspect_service_channel)'
END

-------------------------------------------------------------------------------------------------

-- utb_claim_aspect_status
EXEC sys.sp_addextendedproperty @name=N'Codes', @value=N'DA|Desk Audit|DR|Damage Reinspection|GL|Glass|IA|Ind Adjuster|ME|Mobile Electronics|PS|Program Shop|CS|Choice Shop|SA|Staff Appraiser|TL|Total Loss|RRP|Repair Referral' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'utb_claim_aspect_status', @level2type=N'CONSTRAINT',@level2name=N'uck_claim_aspect_status_servicechannelcd'

-- Check it
IF NOT EXISTS (
    SELECT  Distinct 'ClientServiceChannels',
            NULL,
            csc.ServiceChannelCD,
            urc.Name
    FROM    utb_client_service_channel csc
    LEFT JOIN dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_status', 'ServiceChannelCD') urc on csc.ServiceChannelCD = urc.Code
    WHERE csc.InsuranceCompanyID = @iInscCompanyID
    AND csc.ServiceChannelCD = 'CS'
)
BEGIN
	SELECT 'ERROR - utb_claim_aspect_status column extended properties failed to set...'
END
ELSE
BEGIN
	SELECT 'SUCCESSFUL (utb_claim_aspect_status)'
END

