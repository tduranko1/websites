

DECLARE @InsuranceCompanyID     SMALLINT
DECLARE @MaroID		            SMALLINT
DECLARE @AmherstID		        SMALLINT
DECLARE @EroID		            SMALLINT
DECLARE @SeroID		            SMALLINT
DECLARE @SwroID		            SMALLINT
DECLARE @timestamp				DATETIME

SET @InsuranceCompanyID = 259
SET @timestamp = current_timestamp

SET @MaroID = (SELECT OfficeID FROM utb_office WHERE ClientOfficeID = 'MARO')
SET @AmherstID = (SELECT OfficeID FROM utb_office WHERE ClientOfficeID = 'Amherst')
SET @EroID = (SELECT OfficeID FROM utb_office WHERE ClientOfficeID = 'ERO')
SET @SeroID = (SELECT OfficeID FROM utb_office WHERE ClientOfficeID = 'SERO')
SET @SwroID = (SELECT OfficeID FROM utb_office WHERE ClientOfficeID = 'SWRO')


-- Fix email address
UPDATE utb_user
SET 
	EmailAddress = 'Gary.DiCicco@UticaNational.com',
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
WHERE 
	UserID = 4420
	AND OfficeID = @MaroID		--MARO office
	AND EmailAddress = 'GaryDiCicco@UticaNational.com'

-- Fix email address
UPDATE utb_user
SET 
	EmailAddress = 'sean.mcgovern@uticanational.com',
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
WHERE 
	UserID = 3603
	AND OfficeID = @AmherstID		--Amherst office
	AND EmailAddress = 'sean.mcgovern@@uticanational.com'



-- Add new users for Utica ERO office (ID: 96)
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Michael.Dubiel@uticanational.com',
    @NameFirst             = 'Michael',
    @NameLast              = 'Dubiel',
    @EmailAddress          = 'Michael.Dubiel@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6727',
    @ApplicationID		   = 2
    
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Nikkole.Ferrone@uticanational.com',
    @NameFirst             = 'Nikkole',
    @NameLast              = 'Ferrone',
    @EmailAddress          = 'Nikkole.Ferrone@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6762',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'James.Hulser@uticanational.com',
    @NameFirst             = 'James',
    @NameLast              = 'Hulser',
    @EmailAddress          = 'James.Hulser@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6745',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Melissa.Jweid@uticanational.com',
    @NameFirst             = 'Melissa',
    @NameLast              = 'Jweid',
    @EmailAddress          = 'Melissa.Jweid@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6718',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Vicki.Marston@uticanational.com',
    @NameFirst             = 'Vicki',
    @NameLast              = 'Marston',
    @EmailAddress          = 'Vicki.Marston@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6000',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Sean.Mclaughlin@uticanational.com',
    @NameFirst             = 'Sean',
    @NameLast              = 'McLaughlin',
    @EmailAddress          = 'Sean.Mclaughlin@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '734',
    @PhoneUnitNumber       = '6647',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Kathi.Merrell@uticanational.com',
    @NameFirst             = 'Kathi',
    @NameLast              = 'Merrell',
    @EmailAddress          = 'Kathi.Merrell@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6748',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Tricia.Murray@uticanational.com',
    @NameFirst             = 'Tricia',
    @NameLast              = 'Murray',
    @EmailAddress          = 'Tricia.Murray@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6714',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Deborah.Penc@uticanational.com',
    @NameFirst             = 'Deborah',
    @NameLast              = 'Penc',
    @EmailAddress          = 'Deborah.Penc@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6716',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Marissa.Rivera@uticanational.com',
    @NameFirst             = 'Marissa',
    @NameLast              = 'Rivera',
    @EmailAddress          = 'Marissa.Rivera@uticanational.com',
    @OfficeID              = @EroID,
    @PhoneAreaCode         = '315',
    @PhoneExchangeNumber   = '235',
    @PhoneUnitNumber       = '6725',
    @ApplicationID		   = 2

-- Add new users for Utica MARO office (ID: 92)
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Bill.swertfeger@uticanational.com',
    @NameFirst             = 'Bill',
    @NameLast              = 'Swertfeger',
    @EmailAddress          = 'Bill.swertfeger@uticanational.com',
    @OfficeID              = @MaroID,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6671',
    @ApplicationID		   = 2
    
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Jennifer.tomlin@uticanational.com',
    @NameFirst             = 'Jennifer',
    @NameLast              = 'Tomlin',
    @EmailAddress          = 'Jennifer.tomlin@uticanational.com',
    @OfficeID              = @MaroID,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6667',
    @ApplicationID		   = 2
    
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Gregg.Barkley@uticanational.com',
    @NameFirst             = 'Gregg',
    @NameLast              = 'Barkley',
    @EmailAddress          = 'Gregg.Barkley@uticanational.com',
    @OfficeID              = @MaroID,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6651',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Linda.Carter@uticanational.com',
    @NameFirst             = 'Linda',
    @NameLast              = 'Carter',
    @EmailAddress          = 'Linda.Carter@uticanational.com',
    @OfficeID              = @MaroID,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6619',
    @ApplicationID		   = 2
    


-- Add new users for Utica SERO office (ID: 126)
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Beatrice.cherry@uticanational.com',
    @NameFirst             = 'Beatrice',
    @NameLast              = 'Cherry',
    @EmailAddress          = 'Beatrice.cherry@uticanational.com',
    @OfficeID              = @SeroID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4229',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Steven.hulbert@uticanational.com',
    @NameFirst             = 'Steven',
    @NameLast              = 'Hulbert',
    @EmailAddress          = 'Steven.hulbert@uticanational.com',
    @OfficeID              = @SeroID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4220',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Nina.lavenia@uticanational.com',
    @NameFirst             = 'Nina',
    @NameLast              = 'Lavenia',
    @EmailAddress          = 'Nina.lavenia@uticanational.com',
    @OfficeID              = @SeroID,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6657',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Scott.rose@uticanational.com',
    @NameFirst             = 'Scott',
    @NameLast              = 'Rose',
    @EmailAddress          = 'Scott.rose@uticanational.com',
    @OfficeID              = @SeroID,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '560',
    @PhoneUnitNumber       = '6694',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Salena.staub@uticanational.com',
    @NameFirst             = 'Salena',
    @NameLast              = 'Sanford Staub',
    @EmailAddress          = 'Salena.staub@uticanational.com',
    @OfficeID              = @SeroID,
    @PhoneAreaCode         = '770',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4227',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'joseph.suarez@uticanational.com',
    @NameFirst             = 'Joseph',
    @NameLast              = 'Suarez',
    @EmailAddress          = 'joseph.suarez@uticanational.com',
    @OfficeID              = @SeroID,
    @PhoneAreaCode         = '804',
    @PhoneExchangeNumber   = '510',
    @PhoneUnitNumber       = '4230',
    @ApplicationID		   = 2



-- Add new users for Utica SWRO office (ID: @SwroID)
EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Jeffrey.Deland@uticanational.com',
    @NameFirst             = 'Jeffrey',
    @NameLast              = 'Deland',
    @EmailAddress          = 'Jeffrey.Deland@uticanational.com',
    @OfficeID              = @SwroID,
    @PhoneAreaCode         = '972',
    @PhoneExchangeNumber   = '677',
    @PhoneUnitNumber       = '6725',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Margaret.flores@uticanational.com',
    @NameFirst             = 'Margaret',
    @NameLast              = 'Flores',
    @EmailAddress          = 'Margaret.flores@uticanational.com',
    @OfficeID              = @SwroID,
    @PhoneAreaCode         = '972',
    @PhoneExchangeNumber   = '677',
    @PhoneUnitNumber       = '6724',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Stephen.fox@uticanational.com',
    @NameFirst             = 'Stephen',
    @NameLast              = 'Fox',
    @EmailAddress          = 'Stephen.fox@uticanational.com',
    @OfficeID              = @SwroID,
    @PhoneAreaCode         = '972',
    @PhoneExchangeNumber   = '677',
    @PhoneUnitNumber       = '6717',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Beth.Furber@uticanational.com',
    @NameFirst             = 'Beth',
    @NameLast              = 'Furber',
    @EmailAddress          = 'Beth.Furber@uticanational.com',
    @OfficeID              = @SwroID,
    @PhoneAreaCode         = '972',
    @PhoneExchangeNumber   = '677',
    @PhoneUnitNumber       = '6714',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Larry.Hocutt@uticanational.com',
    @NameFirst             = 'Larry',
    @NameLast              = 'Hocutt',
    @EmailAddress          = 'Larry.Hocutt@uticanational.com',
    @OfficeID              = @SwroID,
    @PhoneAreaCode         = '972',
    @PhoneExchangeNumber   = '677',
    @PhoneUnitNumber       = '6719',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Georgia.larew@uticanational.com',
    @NameFirst             = 'Georgia',
    @NameLast              = 'Larew',
    @EmailAddress          = 'Georgia.larew@uticanational.com',
    @OfficeID              = @SwroID,
    @PhoneAreaCode         = '972',
    @PhoneExchangeNumber   = '677',
    @PhoneUnitNumber       = '6716',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Larry.Lewis@uticanational.com',
    @NameFirst             = 'Larry',
    @NameLast              = 'Lewis',
    @EmailAddress          = 'Larry.Lewis@uticanational.com',
    @OfficeID              = @SwroID,
    @PhoneAreaCode         = '972',
    @PhoneExchangeNumber   = '677',
    @PhoneUnitNumber       = '6720',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'Don.Lilly@uticanational.com',
    @NameFirst             = 'Don',
    @NameLast              = 'Lilly',
    @EmailAddress          = 'Don.Lilly@uticanational.com',
    @OfficeID              = @SwroID,
    @PhoneAreaCode         = '972',
    @PhoneExchangeNumber   = '677',
    @PhoneUnitNumber       = '6718',
    @ApplicationID		   = 2

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'John.Preston@uticanational.com',
    @NameFirst             = 'John',
    @NameLast              = 'Preston',
    @EmailAddress          = 'John.Preston@uticanational.com',
    @OfficeID              = @SwroID,
    @PhoneAreaCode         = '972',
    @PhoneExchangeNumber   = '677',
    @PhoneUnitNumber       = '6704',
    @ApplicationID		   = 2




-- Deactivate user Dienetha.Cook@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Dienetha.Cook@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Dienetha.Cook@uticanational.com'

-- Deactivate user James.Koczon@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'James.Koczon@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'James.Koczon@uticanational.com'

-- Deactivate user Pamela.Dugar@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Pamela.Dugar@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Pamela.Dugar@uticanational.com'

-- Deactivate user Joshua.Canulli@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Joshua.Canulli@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Joshua.Canulli@uticanational.com'

-- Deactivate user Chris.Middlebrook@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Chris.Middlebrook@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Chris.Middlebrook@uticanational.com'


-- Deactivate user Nancy.Moore@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Nancy.Moore@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Nancy.Moore@uticanational.com'

-- Deactivate user phil.coniglione@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'phil.coniglione@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'phil.coniglione@uticanational.com'

-- Deactivate user denise.taber@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'denise.taber@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'denise.taber@uticanational.com'

-- Deactivate user Eric.Dibble@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Eric.Dibble@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Eric.Dibble@uticanational.com'

-- Deactivate user perry.kaupa@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'perry.kaupa@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'perry.kaupa@uticanational.com'

-- Deactivate user perry.kaupa@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'kathleen.mccann@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'kathleen.mccann@uticanational.com'

-- Deactivate user julie.mccreedy@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'julie.mccreedy@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'julie.mccreedy@uticanational.com'

-- Deactivate user Sue.Dailey@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Sue.Dailey@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Sue.Dailey@uticanational.com'

-- Deactivate user edna.martinez-herrin@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'edna.martinez-herrin@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'edna.martinez-herrin@uticanational.com'

-- Deactivate user david.roth@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'david.roth@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'david.roth@uticanational.com'

-- Deactivate user terri.bishop@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'terri.bishop@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'terri.bishop@uticanational.com'

-- Deactivate user Jessica.Calandra@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Jessica.Calandra@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Jessica.Calandra@uticanational.com'

-- Deactivate user julia.evans@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'julia.evans@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'julia.evans@uticanational.com'

-- Deactivate user Kheila.Jones@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Kheila.Jones@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Kheila.Jones@uticanational.com'

-- Deactivate user Elisa.Lindsey@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Elisa.Lindsey@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Elisa.Lindsey@uticanational.com'

-- Deactivate user candice.luther@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'candice.luther@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'candice.luther@uticanational.com'

-- Deactivate user michele.merrick@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'michele.merrick@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'michele.merrick@uticanational.com'

-- Deactivate user daniel.pettyjohn@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'daniel.pettyjohn@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'daniel.pettyjohn@uticanational.com'

-- Deactivate user carolyn.rawlings@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'carolyn.rawlings@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'carolyn.rawlings@uticanational.com'

-- Deactivate user John.Robertson@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'John.Robertson@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'John.Robertson@uticanational.com'

-- Deactivate user leanne.schmidt@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'leanne.schmidt@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'leanne.schmidt@uticanational.com'

-- Deactivate user JAMES.DONOVAN@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'JAMES.DONOVAN@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'JAMES.DONOVAN@uticanational.com'

-- Deactivate user Michael.Hallion@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Michael.Hallion@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Michael.Hallion@uticanational.com'

-- Deactivate user Edith.Schultz@uticanational.com
update dbo.utb_user
set EnabledFlag = 0,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Edith.Schultz@uticanational.com'

update dbo.utb_user_application
set AccessEndDate = GetDate(),
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where LogonID = 'Edith.Schultz@uticanational.com'



-- Last Name Changes
update utb_user
set EmailAddress = 'Julie.Schulz@uticanational.com',
	NameLast = 'Schulz',
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Julie.Merkt@uticanational.com'

update utb_user
set EmailAddress = 'Melissa.Kowalczyk@uticanational.com',
	NameLast = 'Kowalczyk',
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Melissa.Hurley@uticanational.com'

update utb_user
set EmailAddress = 'tracy.buchanan@uticanational.com',
	NameLast = 'Buchanan',
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'tracy.rozelle@uticanational.com'




-- Transfers to SERO office
update dbo.utb_user
set OfficeID = @SeroID,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Nina.Lavenia@uticanational.com'

update dbo.utb_user
set OfficeID = @SeroID,
	SysLastUserID = 0,
	SysLastUpdatedDate = current_timestamp
where EmailAddress = 'SCOTT.ROSE@uticanational.com'

update dbo.utb_user
set OfficeID = @SeroID,
	SysLastUserID = 0,
	SysLastUpdatedDate = current_timestamp
where EmailAddress = 'BILL.SWERTFEGER@uticanational.com'

update dbo.utb_user
set OfficeID = @SeroID,
	SysLastUserID = 0,
	SysLastUpdatedDate = current_timestamp
where EmailAddress = 'JENNIFER.TOMLIN@uticanational.com'

-- Transfer to ERO office
update dbo.utb_user
set OfficeID = @EroID,
	SysLastUserID = 0,
	SysLastUpdatedDate = @timestamp
where EmailAddress = 'Joseph.Caltiere@uticanational.com'





print 'Done.'

