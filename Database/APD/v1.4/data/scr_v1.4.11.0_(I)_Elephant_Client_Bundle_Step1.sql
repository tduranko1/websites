DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 374
SET @FromInscCompID = 277

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%ELPS%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, SUBSTRING(mt.[Description],0,CHARINDEX('/msg',mt.[Description])+4) + 'EL' + SUBSTRING(mt.[Description],CHARINDEX('/msg',mt.[Description]) + 4 , 99)
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID
		AND b.Name NOT IN ('Initial Estimate')
	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

