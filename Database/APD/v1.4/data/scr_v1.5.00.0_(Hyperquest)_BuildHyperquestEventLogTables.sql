/****** Object:  Table [dbo].[utb_log]    Script Date: 12/03/2012 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[utb_hyperquest_event_log](
	[ProcessingServer] [varchar](100) NOT NULL,
	[EventTransactionID] [varchar](100) NOT NULL,
	[EventType] [varchar](50) NOT NULL,
	[EventSeq] [int] NOT NULL,
	[EventStatus] [varchar](150) NOT NULL,
	[EventDescription] [varchar](150) NOT NULL,
	[EventDetailedDescription] [varchar](4000) NULL,
	[EventXML] [text] NULL,
	[SysLastUserID] [int] NULL,
	[SysLastUpdatedDate] [datetime] NULL
) 

GO

SET ANSI_PADDING ON
GO
