--SELECT * FROM utb_message_template WHERE [Description] LIKE '%PSQBE%'
--SELECT * FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%Total Loss Alert - Adjuster|Messages/msgPSQBETLAlert.xsl%'

DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 374

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_bundling_document_type 
	WHERE 	
		BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @ToInscCompID
				AND mt.[Description] LIKE '%msgELPSAssignmentCancel.xsl%'
		)
)
BEGIN
	DECLARE @MsgTempID INT

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSAssignmentCancel.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSStaleAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,37,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSCashOutAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,35,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,1,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSStatusUpdAction.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,36,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT @MsgTempID=b.BundlingID FROM utb_message_template mt INNER JOIN utb_bundling b ON b.MessageTemplateID = mt.MessageTemplateID WHERE mt.[Description] LIKE '%msgELPSTLAlert.xsl%'
	INSERT INTO utb_bundling_document_type VALUES (@MsgTempID,38,NULL,NULL,0,0,0,NULL,0,NULL,0,1,NULL,0,0,0, CURRENT_TIMESTAMP)  

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END

SELECT * FROM utb_bundling_document_type WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
