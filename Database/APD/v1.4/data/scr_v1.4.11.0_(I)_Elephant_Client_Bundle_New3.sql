DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 374

------------------------------
-- Add Client Bundling
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_client_bundling
	WHERE 
		InsuranceCompanyID = @ToInscCompID 
		AND ClientBundlingID IN 
		(
			SELECT 
				b.BundlingID 	
			FROM 
				utb_bundling b 
				INNER JOIN utb_message_template m 
				ON m.MessageTemplateID = b.MessageTemplateID 
			WHERE 
				m.[Description] LIKE '%msgELPSInitialEstimate.xsl%'
		)
)
BEGIN
	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgELPSInitialEstimate.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgELPSStatusUpdActionMDS.xsl%'

	SELECT 'Client Bundling added...'
END
ELSE
BEGIN
	SELECT 'Client Bundling already added...'
END

SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID = @ToInscCompID AND SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
