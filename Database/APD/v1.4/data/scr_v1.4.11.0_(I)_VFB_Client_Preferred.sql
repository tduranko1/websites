------------------------------
-- Correct Client Preferred vs
-- Override
------------------------------
UPDATE 
	utb_insurance 
SET 
	ReturnDocDestinationCD='REP'
	, ReturnDocPackageTypeCD='PDF'
	, ReturnDocRoutingCD='EML' 
WHERE 
	insurancecompanyid = 192 

SELECT * FROM utb_insurance WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)

