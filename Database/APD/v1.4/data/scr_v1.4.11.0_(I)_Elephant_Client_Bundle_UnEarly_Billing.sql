DECLARE @BundlingID SMALLINT
DECLARE @MessageTemplateID SMALLINT
SET @BundlingID = 0
SET @MessageTemplateID = 0

DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT


SET @ToInscCompID = 374
SET @FromInscCompID = 277

------------------------------
-- Delete old bundles
------------------------------
--SELECT * FROM utb_client_bundling WHERE BundlingID = 371
--SELECT * FROM utb_bundling WHERE BundlingID = 371
--SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPS%'
--SELECT * FROM utb_message_template WHERE MessageTemplateID = 359 --@MessageTemplateID
--SELECT * FROM utb_bundling_document_type WHERE BundlingID = 371
IF EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPSApprovedEstimate_EB%')
BEGIN
	SELECT
		@BundlingID=b.BundlingID
		, @MessageTemplateID=t.MessageTemplateID
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template t 
			ON t.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @ToInscCompID
		AND t.[Description] LIKE '%msgELPSApprovedEstimate_EB%'

	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundlingID
	DELETE FROM utb_client_bundling WHERE BundlingID = @BundlingID
	DELETE FROM utb_bundling WHERE BundlingID = @BundlingID
	DELETE FROM utb_message_template WHERE MessageTemplateID = @MessageTemplateID

	SELECT 'Message Template deleted...'
END
ELSE
BEGIN
	SELECT 'Message Template already deleted...'
END

SET @BundlingID = 0
SET @MessageTemplateID = 0
------------------------------
-- Delete old bundles
------------------------------
--SELECT * FROM utb_client_bundling WHERE BundlingID = 371
--SELECT * FROM utb_bundling WHERE BundlingID = 371
--SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPS%'
--SELECT * FROM utb_message_template WHERE MessageTemplateID = 359 --@MessageTemplateID
--SELECT * FROM utb_bundling_document_type WHERE BundlingID = 371
IF EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPS_RC_Release_Close_EB%')
BEGIN
	SELECT
		@BundlingID=b.BundlingID
		, @MessageTemplateID=t.MessageTemplateID
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template t 
			ON t.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @ToInscCompID
		AND t.[Description] LIKE '%msgELPS_RC_Release_Close_EB%'

	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundlingID
	DELETE FROM utb_client_bundling WHERE BundlingID = @BundlingID
	DELETE FROM utb_bundling WHERE BundlingID = @BundlingID
	DELETE FROM utb_message_template WHERE MessageTemplateID = @MessageTemplateID

	SELECT 'Message Template deleted...'
END
ELSE
BEGIN
	SELECT 'Message Template already deleted...'
END

SET @MessageTemplateID = 0
IF EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPS_RC_Release_Close_EB%')
BEGIN
	SELECT @MessageTemplateID=MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgELPS_RC_Release_Close_EB%'
	--DELETE FROM utb_message_template WHERE MessageTemplateID = @MessageTemplateID

	SELECT 'Message Template deleted...'
END
ELSE
BEGIN
	SELECT 'Message Template already deleted...'
END

--SELECT * FROM utb_insurance WHERE InsuranceCompanyID=374
UPDATE utb_insurance SET EarlyBillFlag=0, EarlyBillStartDate=null WHERE InsuranceCompanyID=374