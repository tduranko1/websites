/******************************************************************************
* AOI DA Agreed 1st Party Notification custom form available only for AOI
******************************************************************************/

BEGIN TRANSACTION

-- Add AOI DA Agreed 1st Party Notification Custom Form

INSERT INTO utb_form (
   DocumentTypeID,
   AutoBundlingFlag,
   InsuranceCompanyID,
   EnabledFlag,
   HTMLPath,
   Name,
   PDFPath,
   PertainsToCD,
   ServiceChannelCD,
   SQLProcedure,
   SystemFlag,
   SysLastUserID,
   SysLastUpdatedDate
) VALUES (
   47,
   1,
   288,
   1,
   'Forms/AOIDAAgreed1stPartyNotification.htm',
   'AOI DA Agreed 1st Party Notification',
   'AOI DA Agreed 1st Party Notification.pdf',
   'C',
   NULL,
   'uspCFAOIPHNotificationGetXML',
   0,
   0,
   CURRENT_TIMESTAMP
)

select * from utb_form


-- COMMIT
-- ROLLBACK
