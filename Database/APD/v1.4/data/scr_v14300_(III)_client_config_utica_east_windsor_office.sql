-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configuration for Utica East Windsor District Office
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 259


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'


-- Create Office(s)

-- East Windsor District office

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,   -- InsuranceCompanyID
                '50 Millstone Rd',-- Address1
                'Building 200, Suite 240', -- Address2
                'East Windsor',        -- AddressCity
                'NJ',                  -- AddressState
                '08520',               -- AddressZip
                NULL,                  -- CCEmailAddress
                NULL,                  -- ClaimNumberFormatJS
                NULL,                  -- ClaimNumberValidJS
                NULL,                  -- ClaimNumberMsgText
                'E WINDSOR',           -- ClientOfficeId
                1,                     -- EnabledFlag
                '609',                 -- FaxAreaCode
                '308',                 -- FaxExchangeNumber
                '4599',                -- FaxUnitNumber
                'P.O. Box 6544',       -- MailingAddress1
                NULL,                  -- MailingAddress2
                'Utica',               -- MailingAddressCity
                'NY',                  -- MailingAddressState
                '13504',               -- MailingAddressZip
                'East Windsor District Office', -- Name
                '800',                 -- PhoneAreaCode
                '759',                 -- PhoneExchangeNumber
                '1914',                -- PhoneUnitNumber
                '',                    -- ReturnDocEmailAddress
                '609',                 -- ReturnDocFaxAreaCode
                '308',                 -- ReturnDocFaxExchangeNumber
                '4599',                -- ReturnDocFaxUnitNumber
                0,                     -- SysLastUserID
                @ModifiedDateTime      -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment', 
	                 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code



-- Review affected tables
SELECT * FROM dbo.utb_office WHERE OfficeID = @OfficeID
SELECT * FROM dbo.utb_office_assignment_type WHERE OfficeID = @OfficeID
SELECT * FROM dbo.utb_office_contract_state WHERE OfficeID = @OfficeID


--commit 
--rollback
