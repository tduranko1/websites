DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 387
SET @FromInscCompID = 184

------------------------------
-- Add Fax Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_form WHERE insurancecompanyid = @ToInscCompID)
BEGIN
	INSERT INTO utb_form 
SELECT 
      [BundlingTaskID]
      ,[DocumentTypeID]
      ,[EventID]
      ,@ToInscCompID
      ,[LossStateCode]
      ,[ShopStateCode]
      ,[AutoBundlingFlag]
      ,[ConditionValue]
      ,[DataMiningFlag]
      ,[EnabledFlag]
      ,[FormSupplementID]
      ,[HTMLPath]
      ,[Name]
      ,'FaxAssignmentType1.pdf'
      ,[PertainsToCD]
      ,[ServiceChannelCD]
      ,[SQLProcedure]
      ,[SystemFlag]
      ,[SysLastUserID]
      ,[SysLastUpdatedDate]
  FROM utb_form
  WHERE insurancecompanyid = @FromInscCompID
  
  SELECT 'Fax Template added...'
END
ELSE
BEGIN
	SELECT 'Fax Template already added...'
END

