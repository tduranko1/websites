DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 387
SET @FromInscCompID = 374

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (SELECT * FROM utb_bundling b INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE [Description] LIKE '%WEST%') 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		34
		, MessageTemplateID 
		,1
		,'Acknowledgement of Claim'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSClmAck.xsl%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Closing Supplement'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSClosingSupplement.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Closing - Total Loss'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSTLClosing.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		44
		, MessageTemplateID 
		,1
		,'Rental Invoice'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSRentalInvoice.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'Closing Repair Complete'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPS_ClosingRepairComplete.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Assignment Cancellation'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSAssignmentCancel%'
	
	INSERT INTO 
		utb_bundling 
	SELECT 
		37
		, MessageTemplateID 
		,1
		,'No Inspection (Stale) Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSStaleAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		35
		, MessageTemplateID 
		,1
		,'Cash Out Alert & Close'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSCashOutAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		36
		, MessageTemplateID 
		,1
		,'Status Update - Claim Rep'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSStatusUpdAction.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		38
		, MessageTemplateID 
		,1
		,'Total Loss Alert - (Adjuster)'
		,0
		, CURRENT_TIMESTAMP
	FROM 
		utb_message_template 
	WHERE 
		[Description] LIKE '%msgWFPSTLAlert.xsl%'
				
	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

SELECT * FROM utb_bundling WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
