DECLARE @ShopID as bigint
DECLARE @ShopLocationID as bigint
DECLARE @BillingID as bigint
DECLARE @now as datetime
DECLARE @ShopName as varchar(50)
DECLARE @AllStateInsuranceCompanyID AS int

SET @now = CURRENT_TIMESTAMP
SET @ShopName = '* LYNX Unmatched shop'

IF EXISTS(SELECT ShopID
		FROM dbo.utb_shop
		WHERE Name = @ShopName)
BEGIN
	RAISERROR('"%s" already exists in utb_shop', 16, 1, @ShopName)
	RETURN
END

IF EXISTS(SELECT ShopLocationID
		FROM dbo.utb_shop_location
		WHERE Name = @ShopName)
BEGIN
	RAISERROR('"%s" already exists in utb_shop_location', 16, 1, @ShopName)
	RETURN
END

SELECT @AllStateInsuranceCompanyID = InsuranceCompanyID
FROM dbo.utb_insurance
WHERE Name = 'Allstate Mobile Electronics'

BEGIN TRANSACTION

-- Add the shop to the shop table
INSERT INTO dbo.utb_shop (
	Address1,
	AddressCity,
	AddressCounty,
	AddressState,
	AddressZip,
	EnabledFlag,
	Name,
	SysLastUserID,
	SysLastUpdatedDate
)
VALUES (
	'6351 Bayshore Road Suite 18', 
	'Ft. Myers', 
	'Lee', 
	'FL', 
	'33917', 
	1,
	@ShopName,
	0,
	@now
)

IF @@ERROR <> 0
BEGIN
	-- Insertion failure
	
	RAISERROR('Insert failed into utb_shop', 16, 1)
	ROLLBACK TRANSACTION 
	RETURN
END    

SELECT @ShopID = SCOPE_IDENTITY()

-- Add a dummy billing record
INSERT INTO dbo.utb_billing (
	Address1,
	AddressCity,
	AddressCounty,
	AddressState,
	AddressZip,
	Name,
	SysLastUserID,
	SysLastUpdatedDate
)
VALUES (
	'6351 Bayshore Road Suite 18', 
	'Ft. Myers', 
	'Lee', 
	'FL', 
	'33917', 
	@ShopName,
	0,
	@now
)

IF @@ERROR <> 0
BEGIN
	-- Insertion failure
	
	RAISERROR('Insert failed into utb_shop', 16, 1)
	ROLLBACK TRANSACTION 
	RETURN
END    

SELECT @BillingID = SCOPE_IDENTITY()

-- Add the shop to the shop location table
INSERT INTO dbo.utb_shop_location(
	BillingID, 
	ShopID, 
	Address1, 
	AddressCity, 
	AddressCounty, 
	AddressState, 
	AddressZip, 
	AvailableForSelectionFlag, 
	CEIProgramFlag, 
	DataReviewStatusCD, 
	EnabledFlag, 
	FaxAreaCode,
	FaxExchangeNumber,
	FaxUnitNumber,
	Name,
	PhoneAreaCode,
	PhoneExchangeNumber,
	PhoneUnitNumber,
	ProgramFlag,
	ProgramManagerUserID,
	WarrantyPeriodRefinishCD,
	WarrantyPeriodWorkmanshipCD,
	SysLastUserID,
	SysLastUpdatedDate)
VALUES (
	@BillingID, 
	@ShopID, 
	'6351 Bayshore Road Suite 18', 
	'Ft. Myers', 
	'Lee', 
	'FL', 
	'33917', 
	0, 
	1, 
	'N', 
	1, 
	239,
	479,
	5943,
	@ShopName,
	888,
	374,
	6110,
	1,
	0,
	99,
	99,
	0,
	@now
)

IF @@ERROR <> 0
BEGIN
	-- Insertion failure
	
	RAISERROR('Insert failed into utb_shop_location', 16, 1)
	ROLLBACK TRANSACTION 
	RETURN
END    

SELECT @ShopLocationID = SCOPE_IDENTITY()

-- Now add the shop Exclusion/Inclusion
-- Shop must be in the inclusion list of Allstate
INSERT INTO dbo.utb_client_shop_location(
	InsuranceCompanyID,
	ShopLocationID,
	ExcludeFlag,
	IncludeFlag,
	SysLastUserID,
	SysLastUpdatedDate
)
SELECT 	InsuranceCompanyID,
	@ShopLocationID,
	0,
	1,
	0,
	@now
FROM dbo.utb_insurance
WHERE name = 'Allstate Mobile Electronics'

IF @@ERROR <> 0
BEGIN
	-- Insertion failure
	
	RAISERROR('Insert failed into utb_client_shop_location', 16, 1)
	ROLLBACK TRANSACTION 
	RETURN
END    

-- Shop must be in the exclusion list of other insurances
INSERT INTO dbo.utb_client_shop_location(
	InsuranceCompanyID,
	ShopLocationID,
	ExcludeFlag,
	IncludeFlag,
	SysLastUserID,
	SysLastUpdatedDate
)
SELECT 	InsuranceCompanyID,
	@ShopLocationID,
	1,
	0,
	0,
	@now
FROM dbo.utb_insurance
WHERE name <> 'Allstate Mobile Electronics'

IF @@ERROR <> 0
BEGIN
	-- Insertion failure
	
	RAISERROR('Insert failed into utb_client_shop_location', 16, 1)
	ROLLBACK TRANSACTION 
	RETURN
END

-- update the  Allstate FL to use CEI flag
IF EXISTS(SELECT InsuranceCompanyID
		FROM dbo.utb_client_contract_state
		WHERE InsuranceCompanyID = @AllStateInsuranceCompanyID
		  AND StateCode = 'FL')
BEGIN
	UPDATE dbo.utb_client_contract_state
	SET UseCEIShopsFlag = 1
	WHERE InsuranceCompanyID = @AllStateInsuranceCompanyID
	  AND StateCode = 'FL'

	IF @@ERROR <> 0
	BEGIN
		-- Insertion failure
		
		RAISERROR('Update failed into utb_client_contract_state', 16, 1)
		ROLLBACK TRANSACTION 
		RETURN
	END
END
ELSE
BEGIN
	INSERT INTO dbo.utb_client_contract_state (
		InsuranceCompanyID,
		StateCode,
		UseCEIShopsFlag,
		SysLastUserID,
		SysLastUpdatedDate
	) VALUES (
		@AllStateInsuranceCompanyID,
		'FL',
		1,
		0,
		@now
	)

	IF @@ERROR <> 0
	BEGIN
		-- Insertion failure
		
		RAISERROR('Insert failed into utb_client_contract_state', 16, 1)
		ROLLBACK TRANSACTION 
		RETURN
	END
END

-- Final Check
SELECT * FROM dbo.utb_shop
WHERE ShopID = @ShopID


SELECT * FROM dbo.utb_shop_location
WHERE ShopLocationID = @ShopLocationID

SELECT * FROM dbo.utb_client_shop_location
WHERE ShopLocationID = @ShopLocationID

SELECT InsuranceCompanyID
FROM dbo.utb_client_contract_state
WHERE InsuranceCompanyID = @AllStateInsuranceCompanyID
  AND StateCode = 'FL'

PRINT 'Remember to commit/rollback the transaction'

-- COMMIT
-- ROLLBACK