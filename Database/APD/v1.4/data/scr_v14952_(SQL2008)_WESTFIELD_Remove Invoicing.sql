DECLARE @InscCompID INT
SET @InscCompID = 387

DELETE
FROM 
	utb_bundling_document_type
WHERE 	
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @InscCompID
			AND [Name] = 'Closing Repair Complete'
	)
AND DocumentTypeID = 13
	
	

----------------------------------------------------------------

DELETE
FROM 
	utb_bundling_document_type
WHERE 	
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @InscCompID
			AND [Name] = 'Closing Supplement'
	)
AND DocumentTypeID = 13
