DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT

SET @ToInscCompID = 575
SET @FromInscCompID = 194

IF NOT EXISTS (SELECT * FROM utb_client_business_state WHERE InsuranceCompanyID = @ToInscCompID)
BEGIN
	INSERT INTO utb_client_business_state
	SELECT 
		@ToInscCompID
		, StateCode
		, 0
		, CURRENT_TIMESTAMP 
	FROM 
		utb_client_business_state 
	WHERE 
		InsuranceCompanyID = @FromInscCompID

	SELECT 'Business States Updated...'
END
ELSE
BEGIN
	SELECT 'Business States ALREADY Updated...'
END