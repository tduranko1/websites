------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPSAssignmentCancel%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		'C'
		, 1
		, 'Assignment Cancellation|Messages/msgELPSAssignmentCancel.xsl'
		, 'PS'
		, 0
		, CURRENT_TIMESTAMP 

	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPSStaleAlert%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		'C'
		, 1
		, 'No Inspection (Stale) Alert & Close|Messages/msgELPSStaleAlert.xsl'
		, 'PS'
		, 0
		, CURRENT_TIMESTAMP 

	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE 'msgELPSCashOutAlert%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		'C'
		, 1
		, 'Cash Out Alert & Close|Messages/msgELPSCashOutAlert.xsl'
		, 'PS'
		, 0
		, CURRENT_TIMESTAMP 

	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE 'msgELPSStatusUpdAction%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		'C'
		, 1
		, 'Status Update|Messages/msgELPSStatusUpdAction.xsl'
		, 'PS'
		, 0
		, CURRENT_TIMESTAMP 

	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE 'msgELPSTLAlert%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		'C'
		, 1
		, 'Total Loss Alert - (Adjuster)|Messages/msgELPSTLAlert.xsl'
		, 'PS'
		, 0
		, CURRENT_TIMESTAMP 

	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

SELECT * FROM utb_message_template WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
