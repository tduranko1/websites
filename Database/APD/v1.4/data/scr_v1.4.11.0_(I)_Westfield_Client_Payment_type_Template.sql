DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 387
SET @FromInscCompID = 184

------------------------------
-- Add Insurance Company  Templates
------------------------------

IF NOT EXISTS (SELECT * FROM utb_client_Payment_Type WHERE insurancecompanyid = @ToInscCompID)
BEGIN
	INSERT INTO utb_client_Payment_Type
SELECT 
       @ToInscCompID
      ,[PaymentTypeCD]     
      ,[SysLastUserID]
      ,[SysLastUpdatedDate]
  FROM utb_client_Payment_Type
  WHERE insurancecompanyid = @FromInscCompID
  
  

  
  
  SELECT 'insurancecompanyid added...'
END
ELSE
BEGIN
	SELECT 'insurancecompanyid already added...'
END

