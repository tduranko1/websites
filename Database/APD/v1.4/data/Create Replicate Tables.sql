/****** Object:  Table [dbo].[utb_HQRepl_History]    Script Date: 7/21/2016 9:34:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
-- DROP TABLE utb_HQRepl_History
CREATE TABLE [dbo].[utb_HQRepl_History](
	[HQReplID]										INT IDENTITY(1,1) NOT NULL,
	[LogID]											INT NOT NULL,
	[InsuranceCompanyID]							INT NOT NULL,
	[LynxID]										INT NOT NULL,
	[ServiceChannelCD]								VARCHAR(10) NULL,
	[InscCompAndServiceChannel]						VARCHAR(10) NOT NULL,
	[ClaimAspectServiceChannelID]					INT NULL,
	[VehicleNumber]									INT NOT NULL,
	[CreatedByRole]									VARCHAR(50) NULL,
	[CreatedByUserID]								INT NULL,
	[CreatedByNameFirst]							VARCHAR(50) NULL,
	[CreatedByNameLast]								VARCHAR(50) NULL,
	[CreatedByEmail]								VARCHAR(50) NULL,
	[CreatedDate]									DATETIME NULL,
	[CompletedByRole]								VARCHAR(50) NULL,
	[CompletedByUserID]								INT NULL,
	[CompletedByNameFirst]							VARCHAR(50) NULL,
	[CompletedByNameLast]							VARCHAR(50) NULL,
	[CompletedByEmail]								VARCHAR(50) NULL,
	[CompletedDate]									DATETIME NULL,
	[EventID]										INT NULL,
	[EventName]										VARCHAR(50) NULL,
	[EventDescription]								VARCHAR(500) NULL,
	[EventPertainsTo]								VARCHAR(50) NULL,
	[TaskName]										VARCHAR(150) NULL,
	[SysLastUserID]									INT NOT NULL,
	[SysLastUpdatedDate]							DATETIME NOT NULL,
	[OwnerUserID]									INT NULL,
	[OwnerNameFirst]								VARCHAR(50) NULL,
	[OwnerNameLast]									VARCHAR(50) NULL,
	[OwnerEmail]									VARCHAR(50) NULL,
	[AnalystUserID]									INT NULL,
	[AnalystNameFirst]								VARCHAR(50) NULL,
	[AnalystNameLast]								VARCHAR(50) NULL,
	[AnalystEmail]									VARCHAR(50) NULL,
	[SupportUserID]									INT NULL,
	[SupportNameFirst]								VARCHAR(50) NULL,
	[SupportNameLast]								VARCHAR(50) NULL,
	[SupportEmail]									VARCHAR(50) NULL
 CONSTRAINT [PK_utb_HQRepl_History] PRIMARY KEY CLUSTERED 
(
	[HQReplID] ASC,
	[InsuranceCompanyID],
	[LynxID]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [ufg_primary]
) ON [ufg_primary]

GO

SET ANSI_PADDING ON
GO


