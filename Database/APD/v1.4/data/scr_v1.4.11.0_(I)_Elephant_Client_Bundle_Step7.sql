DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 374
SET @FromInscCompID = 277

------------------------------
-- Add Client Bundling
------------------------------
IF NOT EXISTS 
(
	SELECT 
		*
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
		INNER JOIN utb_client_bundling cb 
		ON cb.BundlingID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgELPSAssignmentCancel.xsl%'
		AND cb.InsuranceCompanyID = @ToInscCompID
)
BEGIN
	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgELPSAssignmentCancel.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgELPSStaleAlert.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgELPSCashOutAlert.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgELPSStatusUpdAction.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgELPSTLAlert.xsl%'		
	
	SELECT 'Client Bundling added...'
END
ELSE
BEGIN
	SELECT 'Client Bundling already added...'
END

SELECT * FROM utb_client_bundling WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
