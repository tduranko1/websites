DECLARE @iNewUserID INT
DECLARE @iOffice INT
DECLARE @vNewUserEmail VARCHAR(50)
DECLARE @vNewUserFirstName VARCHAR(50)
DECLARE @vNewUserLastName VARCHAR(50)
DECLARE @vOfficeName VARCHAR(50)

SET @iNewUserID = 0
SET @iOffice = 0
SET @vNewUserEmail = 'ANPACUser@pgwglass.com'
SET @vNewUserFirstName = 'ANPAC'
SET @vNewUserLastName = 'User'
SET @vOfficeName = 'ANPAC'

IF NOT EXISTS 
(
	SELECT * FROM utb_user WHERE emailaddress = @vNewUserEmail
)
BEGIN
	SELECT @iOffice=OfficeID FROM utb_office WHERE ClientOfficeID = @vOfficeName

	INSERT INTO utb_user VALUES
	( 
		@iOffice
		, NULL
		, NULL
		, NULL
		, NULL
		, @vNewUserEmail
		, 1
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, @vNewUserFirstName
		, @vNewUserLastName
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 0
		, 0
		, CURRENT_TIMESTAMP
		, NULL
		, NULL
	)

	SELECT 'New User Added...'

	SELECT @iNewUserID = UserID FROM utb_user WHERE emailaddress = @vNewUserEmail

	IF (@iNewUserID > 0)
	BEGIN
		INSERT INTO utb_user_application VALUES (@iNewUserID,1,CURRENT_TIMESTAMP,NULL, @vNewUserEmail, NULL, NULL,0,CURRENT_TIMESTAMP)
		INSERT INTO utb_user_application VALUES (@iNewUserID,2,CURRENT_TIMESTAMP,NULL, @vNewUserEmail, NULL, NULL,0,CURRENT_TIMESTAMP)
		INSERT INTO utb_user_role VALUES (@iNewUserID,17,1,0,CURRENT_TIMESTAMP)
	END
END
ELSE
BEGIN
	SELECT 'User Already Exists...'
END

-- ADD CCAV user

IF NOT EXISTS 
(
	SELECT * FROM utb_user_application WHERE LogonID = 'AV_B0'
)
BEGIN
	SELECT @iOffice=OfficeID FROM utb_office WHERE ClientOfficeID = @vOfficeName

	INSERT INTO utb_user VALUES
	( 
		@iOffice
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 1
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 'ANPAC'
		, 'Autoverse User'
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, NULL
		, 0
		, 0
		, CURRENT_TIMESTAMP
		, NULL
		, NULL
	)

	SELECT 'New User Added...'

	SELECT @iNewUserID = UserID FROM utb_user WHERE NameLast = 'Autoverse User'

	IF (@iNewUserID > 0)
	BEGIN
		INSERT INTO utb_user_application VALUES (@iNewUserID,6,CURRENT_TIMESTAMP,NULL, 'AV_B0', NULL, NULL,0,CURRENT_TIMESTAMP)
		INSERT INTO utb_user_role VALUES (@iNewUserID,1,1,0,CURRENT_TIMESTAMP)
	END
END
ELSE
BEGIN
	SELECT 'User Already Exists...'
END
