DECLARE @InsuranceCompanyID as int
DECLARE @OldClientBundlingID as int
DECLARE @NewBundlingID as int
DECLARE @DocumentTypeID as int
DECLARE @MessageTemplateID as int
DECLARE @now as datetime


SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Farm Family'

SET @now = CURRENT_TIMESTAMP

BEGIN TRANSACTION

IF @InsuranceCompanyID IS NOT NULL
BEGIN

    SELECT @DocumentTypeID = DocumentTypeID
    FROM utb_document_type
    WHERE Name = 'Closing Documents'

    IF @DocumentTypeID IS NULL
    BEGIN
        -- SQL Server Error
        RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END

    SET @MessageTemplateID = NULL

    SELECT @MessageTemplateID = MessageTemplateID
    FROM utb_message_template
    WHERE ServiceChannelCD = 'PS'
      AND Description like 'Closing Repair Complete|%'
      AND EnabledFlag = 1

    IF @MessageTemplateID IS NULL
    BEGIN
        -- SQL Server Error
        RAISERROR  ('Cannot find "Closing Repair Complete|" in utb_message_template ', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END


    INSERT INTO utb_bundling (
        DocumentTypeID,
        MessageTemplateID,
        EnabledFlag,
        Name,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @DocumentTypeID,    -- DocumentTypeID,
        @MessageTemplateID, -- MessageTemplateID
        1,                  -- EnabledFlag,
        'Closing Repair Complete', -- Name,
        0,                  -- SysLastUserID,
        @now                -- SysLastUpdatedDate
    )

    IF @@error <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR  ('Error inserting into utb_bundling "Closing Repair Complete"', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END

    SET @NewBundlingID = SCOPE_IDENTITY()

    -- Add document types to this profile
    -- Add Invoice
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @NewBundlingID, -- BundlingID,
        13,             -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        null,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        1,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

    IF @@error <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END

    -- Add DTP
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @NewBundlingID, -- BundlingID,
        2,              -- DocumentTypeID,
        'I',            -- DirectionalCD,
        1,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        null,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        2,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

    IF @@error <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END

    -- Add estimate
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @NewBundlingID, -- BundlingID,
        3,              -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        'A',            -- EstimateTypeCD,
        1,              -- FinalEstimateFlag,
        1,              -- MandatoryFlag,
        3,              -- SelectionOrder,
        1,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

    IF @@error <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END

    -- Add Photos
    INSERT INTO utb_bundling_document_type (
        BundlingID,
        DocumentTypeID,
        DirectionalCD,
        DirectionToPayFlag,
        DuplicateFlag,
        EstimateDuplicateFlag,
        EstimateTypeCD,
        FinalEstimateFlag,
        MandatoryFlag,
        SelectionOrder,
        VANFlag,
        SysLastUserID,
        SysLastUpdatedDate
    ) VALUES (
        @NewBundlingID, -- BundlingID,
        8,              -- DocumentTypeID,
        'I',            -- DirectionalCD,
        0,              -- DirectionToPayFlag,
        0,              -- DuplicateFlag,
        0,              -- EstimateDuplicateFlag,
        null,           -- EstimateTypeCD,
        0,              -- FinalEstimateFlag,
        0,              -- MandatoryFlag,
        4,              -- SelectionOrder,
        0,              -- VANFlag,
        0,              -- SysLastUserID,
        @now            -- SysLastUpdatedDate
    )

    IF @@error <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
        ROLLBACK TRANSACTION 
        RETURN
    END

    -- get the old bundling id that was assigned to Republic
    SELECT @OldClientBundlingID = cb.ClientBundlingID
    FROM utb_client_bundling cb
    LEFT JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
    WHERE InsuranceCompanyID = @InsuranceCompanyID
      AND b.Name = 'Closing Repair Complete'
      AND cb.ServiceChannelCD = 'PS'

    IF @OldClientBundlingID IS NOT NULL
    BEGIN
        UPDATE utb_client_bundling
        SET BundlingID = @NewBundlingID
        WHERE ClientBundlingID = @OldClientBundlingID
    END
    ELSE
    BEGIN
        -- Add this bundling tasks to PS clients
        INSERT INTO dbo.utb_client_bundling
        (BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
        SELECT @NewBundlingID, i.InsuranceCompanyID, 'PS', 0, @now
        FROM dbo.utb_client_service_channel csc 
        left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
        WHERE csc.ServiceChannelCD = 'PS'
          AND i.EnabledFlag = 1
          AND i.InsuranceCompanyID = @InsuranceCompanyID
    END

END

select * from utb_bundling where BundlingID = @NewBundlingID
select * from utb_bundling_document_type where BundlingID = @NewBundlingID
SELECT * FROM utb_client_bundling where InsuranceCompanyID = @InsuranceCompanyID

-- commit
-- rollback
