/******************************************************************************
* SWR 206682 - script to add Towing coverage for Utica
******************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @now as datetime

SET @now = CURRENT_TIMESTAMP

SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance 
WHERE Name = 'Utica National Insurance Group'

begin transaction

IF @InsuranceCompanyID IS NOT NULL
BEGIN
    insert into utb_client_coverage_type (
        InsuranceCompanyID,
        AdditionalCoverageFlag,
        ClientCode,
        CoverageProfileCD,
        DisplayOrder,
        EnabledFlag,
        Name,
        SysLastUserID,
        SysLastUpdatedDate
    ) values (
        @InsuranceCompanyID,    -- InsuranceCompanyID,
        1,                      -- AdditionalCoverageFlag,
        'Towing',               -- ClientCode,
        'TOW',                  -- CoverageProfileCD,
        11,                     -- DisplayOrder,
        1,                      -- EnabledFlag,
        'Towing',               -- Name,
        0,                      -- SysLastUserID,
        CURRENT_TIMESTAMP       -- SysLastUpdatedDate
    )
END

-- commit
-- rollback
