DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 473
SET @FromInscCompID = 304

------------------------------
-- Add Bundling
------------------------------
IF NOT EXISTS (
	SELECT 
		*
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgANRRPClmAck.xsl%'
) 
BEGIN
	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPClmAck.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPClmAck.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPAssignmentCancel.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPAssignmentCancel.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPStaleAlertAndClose.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPStaleAlertAndClose.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPStatusUpdateNotificationOnly.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPStatusUpdateNotificationOnly.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPClosingInspectionComplete.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPClosingInspectionComplete.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPClosingSupplement.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPClosingSupplement.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPClosingTotalLoss.xsl%') 
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPClosingTotalLoss.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPStatusUpdateActionRequired.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPStatusUpdateActionRequired.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPApprovedEstimateShopNotification.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPApprovedEstimateShopNotification.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPTotalLossAlert.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPTotalLossAlert.xsl%'

	INSERT INTO 
		utb_bundling 
	SELECT 
		b.DocumentTypeID
		, (SELECT MessageTemplateID FROM utb_message_template WHERE [Description] LIKE '%msgANRRPTotalLossDetermination.xsl%')
		,1
		, b.[Name]
		,0
		, CURRENT_TIMESTAMP
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
		AND mt.[Description] LIKE '%msgRRPTotalLossDetermination.xsl%'

	SELECT 'Bundling added...'
END
ELSE
BEGIN
	SELECT 'Bundling already added...'
END

SELECT * FROM utb_bundling WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
