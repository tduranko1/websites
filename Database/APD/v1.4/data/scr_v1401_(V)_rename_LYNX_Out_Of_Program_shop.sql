/**********************************************************
Rename "* LYNX Out of Program Shop" to "* LYNX Unmatched shop"
Per discussion with JP, JL, and Tom Harlacher we need a default
shop for Autoverse assignments. It was decided to use an existing 
"* LYNX Out of Program Shop" shop which was setup for ME and 
we are not doing business with ME during this time.
***********************************************************/
declare @ShopLocationID as bigint

select @ShopLocationID = ShopLocationID
from utb_shop_location
where Name = '* LYNX Out of Program Shop'

if @ShopLocationID is null
begin 
   print '"* LYNX Out of Program Shop" shop not found in utb_shop_location'
   return
end

select 'Before Update'

Select * from utb_shop_location
where ShopLocationID = @ShopLocationID

begin transaction

update utb_shop_location
set Name = '* LYNX Unmatched shop',
    ProgramFlag = 1,
    AvailableForSelectionFlag = 0,
    ProgramManagerUserID = 0
where ShopLocationID = @ShopLocationID

select 'After Update'

Select * from utb_shop_location
where ShopLocationID = @ShopLocationID


-- commit
-- rollback