/******************************************************************************
* Add Photos QBE RRP Closing Bundling profiles
******************************************************************************/

DECLARE @InsuranceCompanyID as int
DECLARE @BundlingID as int
DECLARE @now as datetime

SET @InsuranceCompanyID = 304
SET @now = CURRENT_TIMESTAMP

-----------------------------------------------------
-- Add Photos to the following bundling profiles:
-- RRP - Closing Inspection Complete
-- RRP - Closing Supplement
-- RRP - Closing Total Loss
-----------------------------------------------------
  
-- Add photo
INSERT INTO dbo.utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    LossState,
    MandatoryFlag,
    SelectionOrder,
    ShopState,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) 
SELECT  b.BundlingID,
        8,             -- DocumentTypeID,
        'I',           -- DirectionalCD,
        0,             -- DirectionToPayFlag,
        1,             -- DuplicateFlag,
        0,             -- EstimateDuplicateFlag,
        NULL,          -- EstimateTypeCD,
        0,             -- FinalEstimateFlag,
        NULL,          -- LossState,
        0,             -- MandatoryFlag,
        5,             -- SelectionOrder,
        NULL,          -- ShopState,
        0,             -- VANFlag,
        0,             -- SysLastUserID,
        @now           -- SysLastUpdatedDate
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE cb.InsuranceCompanyID = @InsuranceCompanyID
  AND b.Name IN ('RRP - Closing Inspection Complete',
                 'RRP - Closing Supplement',
                 'RRP - Closing Total Loss')
