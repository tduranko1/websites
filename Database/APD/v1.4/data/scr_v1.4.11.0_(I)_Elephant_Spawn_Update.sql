--SELECT * FROM dbo.utb_spawn WHERE SpawnID = 260 AND WorkflowID = 172

------------------------------
-- Task Update
------------------------------
IF EXISTS (SELECT * FROM dbo.utb_spawn WHERE SpawnID = 260 AND WorkflowID = 172)
BEGIN
	UPDATE 
		dbo.utb_spawn 
	SET 
		ContextNote = '%Key%' 
	WHERE 
		SpawnID = 260 
		AND WorkflowID = 172

	SELECT 'Task Update *** COMPLETED ***'
END
ELSE
BEGIN
	SELECT 'Task Update *** ALREADY COMPLETE ***'
END

SELECT * FROM dbo.utb_spawn WHERE SpawnID = 260 AND WorkflowID = 172