/*******************************************************************
* Add CT Consumer Choice form to all CT shop assignments
*******************************************************************/

begin transaction

insert into utb_form_supplement
(
    FormID,
    ShopStateCode,
    EnabledFlag,
    Name,
    PDFPath,
    ServiceChannelCD,
    SQLProcedure,
    SysLastUserID,
    SysLastUpdatedDate
)
select FormID, 
        'CT', 
        1, 
        'CT Customer Choice',
        'CTAoClaimantConsumerChoice.pdf',
        'PS',
        '',
        0,
        current_timestamp
from utb_form
where InsuranceCompanyID is not null
  and Name = 'Shop Assignment'

select * from utb_form_supplement

-- commit
-- rollback