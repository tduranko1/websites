DECLARE @iUserID INT
SET @iUserID = 0

IF EXISTS (select UserID from utb_user where EmailAddress = 'APDAndroidUser@lynxservices.com')
BEGIN
	UPDATE utb_user SET EnabledFlag = 0 WHERE EmailAddress = 'APDAndroidUser@lynxservices.com'
	SELECT 'User ROLLBACK Completed...'
END
ELSE
BEGIN
	SELECT 'User Already ROLLEDBACK...'
END

IF EXISTS (select UserID from utb_user where EmailAddress = 'customerservice@hyperquest.com')
BEGIN
	UPDATE utb_user SET EnabledFlag = 0 WHERE EmailAddress = 'customerservice@hyperquest.com'
	SELECT 'User ROLLBACK Completed...'
END
ELSE
BEGIN
	SELECT 'User Already ROLLEDBACK...'
END