DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 575

------------------------------
-- Add Client Bundling
------------------------------
IF NOT EXISTS 
(
	SELECT 
		* 
	FROM 
		utb_client_bundling
	WHERE 
		InsuranceCompanyID = @ToInscCompID 
		AND ClientBundlingID IN 
		(
			SELECT 
				b.BundlingID 	
			FROM 
				utb_bundling b 
				INNER JOIN utb_message_template m 
				ON m.MessageTemplateID = b.MessageTemplateID 
			WHERE 
				m.[Description] LIKE '%msgNOPSAssignmentCancel.xsl%'
		)
)
BEGIN
	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPS_ClosingRepairComplete.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT  TOP 1
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSClmAck.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSClosingSupplement.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSInitialEstimate.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSAssignmentCancel.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSRentalInvoice.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSTLClosing.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSStaleAlert.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, 575 --@ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSCashOutAlert.xsl%'
						  

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSStatusUpdAction.xsl%'

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, NULL
		, NULL 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSStatusUpdActionMDS.xsl%'		

	INSERT INTO 
		utb_client_bundling 
	SELECT TOP 1 
		b.BundlingID
		, @ToInscCompID
		, NULL
		, 0
		, NULL
		, 'EML'
		, ';elec.advance.charges@electricinsurance.com' 
		, m.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling b 
		INNER JOIN utb_message_template m 
		ON m.MessageTemplateID = b.MessageTemplateID 
	WHERE 
		m.[Description] LIKE '%msgNOPSTLAlert.xsl%'		

	SELECT 'Client Bundling added...'
END
ELSE
BEGIN
	SELECT 'Client Bundling already added...'
END

SELECT * FROM utb_client_bundling WHERE InsuranceCompanyID = @ToInscCompID AND SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)
