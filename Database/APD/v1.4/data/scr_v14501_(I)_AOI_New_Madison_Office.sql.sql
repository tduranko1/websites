
/*
	Filename:
	Author/Date: Walter J. Chesla - 6/9/2011
	Description:  Create new AOI Madison, WI branch
*/



DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint
DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = (SELECT InsuranceCompanyID FROM utb_insurance WHERE DeskAuditCompanyCD = 'AOI')


PRINT 'Creating new AOI offices...'



-- Madison, WI #95;  madison.clm@aoins.com
-- Phone:  608-249-1509
-- Fax:  517-327-2395
INSERT INTO dbo.utb_office
        (
		InsuranceCompanyID, 
		AddressCity,
		AddressState, 
		ClientOfficeId,
		EnabledFlag, 
		FaxAreaCode,
		FaxExchangeNumber,
		FaxUnitNumber,
		Name, 
		PhoneAreaCode, 
		PhoneExchangeNumber, 
		PhoneUnitNumber,
		ReturnDocEmailAddress,
        SysLastUserID,
        SysLastUpdatedDate 
        )
VALUES( @InsuranceCompanyID,	-- InsuranceCompanyID
		'Madison',
		'WI',
        'WI - Madison',				-- ClientOfficeId		
        1,                     -- EnabledFlag
        '517',                 -- FaxAreaCode
        '327',                 -- FaxExchangeNumber
        '2395',                -- FaxUnitNumber
        'Madison, WI - Branch #95',				-- Name
        '608',					-- PhoneAreaCode
        '249',                 -- PhoneExchangeNumber
        '1509',                -- PhoneUnitNumber
		'madison.clm@aoins.com',                    -- ReturnDocEmailAddress			
        0,                     -- SysLastUserID
        @ModifiedDateTime      -- SysLastUpdatedDate
)

SELECT @OfficeID = SCOPE_IDENTITY()

INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Demand Estimate Audit')
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0,					-- UseCEIShopsFlag
          0,					-- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code

EXEC dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                 = 'madison.clm@aoins.com',
    @NameFirst             = 'AOI',
    @NameLast              = 'WI - Madison',
    @EmailAddress          = 'madison.clm@aoins.com',
    @OfficeID              = @OfficeID,
    @PhoneAreaCode         = '608',
    @PhoneExchangeNumber   = '249',
    @PhoneUnitNumber       = '1509',
    @ApplicationID		   = '2'
    

PRINT 'Complete.'