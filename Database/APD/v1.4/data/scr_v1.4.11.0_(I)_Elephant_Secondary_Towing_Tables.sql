/****** Object:  Table [dbo].[utb_client_towing]    Script Date: 02/24/2012 15:12:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[utb_client_towing](
      [TowingID] [udt_std_id_big] IDENTITY(1,1) NOT NULL,
      [InsuranceCompanyID] [udt_std_int_small] NOT NULL,
      [AnalystUserID] [udt_std_int] NOT NULL,
      [ClaimNumber] [varchar](50) NULL,
      [CusAddress] [udt_addr_line_1] NULL,
      [CusAltPhone] [varchar](15) NULL,
      [CusCity] [udt_addr_city] NULL,
      [CusEmail] [udt_web_email] NULL,
      [CusHomePhone] [varchar](15) NULL,
      [CusName] [varchar](100) NULL,
      [CusState] [udt_addr_state] NULL,
      [CusWorkPhone] [varchar](15) NULL,
      [CusZip] [udt_addr_zip_code] NULL,
      [DesAddress] [udt_addr_line_1] NULL,
      [DesCity] [udt_addr_city] NULL,
      [DesFacilityContactName] [varchar](100) NULL,
      [DesHoursOfOperation] [varchar](150) NULL,
      [DesPhone] [varchar](15) NULL,
      [DesState] [udt_addr_state] NULL,
      [DesZip] [udt_addr_zip_code] NULL,
      [EnabledFlag] [udt_enabled_flag] NULL,
      [LynxID] [udt_std_int_big] NULL,
      [LocAddress] [udt_addr_line_1] NULL,
      [LocCity] [udt_addr_city] NULL,
      [LocFacilityContactName] [varchar](100) NULL,
      [LocHoursOfOperation] [varchar](150) NULL,
      [LocPhone] [varchar](15) NULL,
      [LocState] [udt_addr_state] NULL,
      [LocZip] [udt_addr_zip_code] NULL,
      [VehConditionDescription] [varchar](255) NULL,
      [VehLicensePlateNumber] [udt_auto_plate_number] NULL,
      [VehMake] [varchar](50) NULL,
      [VehModel] [varchar](50) NULL,
      [VehVINState] [udt_auto_vin] NULL,
      [VehYear] [udt_std_int_small] NOT NULL,
      [SysLastUserID] [udt_std_id] NULL,
      [SysLastUpdatedDate] [udt_sys_last_updated_date] NULL,
CONSTRAINT [PK_utb_client_towing] PRIMARY KEY CLUSTERED 
(
      [TowingID] ASC
) ON [ufg_claim]
) ON [ufg_claim]

GO

SET ANSI_PADDING ON
GO
