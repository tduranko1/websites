-- Begin the transaction for dropping and creating the tables

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

----------------------------------------
-- Populate the new table from the old
----------------------------------------
-- DROP TABLE utb_hyperquestsvc_jobs1
-- SELECT * FROM utb_hyperquestsvc_jobs1
-- SELECT * FROM utb_hyperquestsvc_jobs
/*
SELECT 
	JobID
	, InscCompID
	, ClaimNumber
	, DocumentID
	, LynxID 
	, 1 VehicleID
	, 0 ShopLocationID
	, 0 SeqID
	, AssignmentID
	, ReceivedDate
	, DocumentTypeID
	, DocumentTypeName
	, DocumentSource
	, DocumentImageType
	, DocumentImageLocation
	, ClaimAspectServiceChannelID
	, ClaimAspectID
	, JobXSLFile
	, JobStatus
	, JobStatusDetails
	, JobPriority
	, JobCreatedDate
	, JobProcessedDate
	, JobTransformedDate
	, JobTransmittedDate
	, JobArchivedDate
	, JobFinishedDate
	, EnabledFlag
	, JobXML
	, SysLastUserID
	, SysLastUpdatedDate
INTO
	utb_hyperquestsvc_jobs1
FROM 
	utb_hyperquestsvc_jobs
*/

---------------------------------
-- Update the Key/Index
---------------------------------
ALTER TABLE [dbo].[utb_hyperquestsvc_jobs]
ADD 
	[VehicleID] BIGINT NOT NULL DEFAULT(0),
	[ShopLocationID] BIGINT NOT NULL DEFAULT(0),
	[SeqID] INT NOT NULL DEFAULT(0)
GO

---------------------------------
-- Make LynxID NOT NULL
---------------------------------
ALTER TABLE [dbo].[utb_hyperquestsvc_jobs]
ALTER COLUMN 
	[LynxID] BIGINT NOT NULL
GO

---------------------------------
-- Update the Key/Index
---------------------------------
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[utb_hyperquestsvc_jobs]') AND name = N'PK_utb_hyperquestsvc_jobs')
ALTER TABLE [dbo].[utb_hyperquestsvc_jobs] DROP CONSTRAINT [PK_utb_hyperquestsvc_jobs]
ALTER TABLE [dbo].[utb_hyperquestsvc_jobs] ADD CONSTRAINT [PK_utb_hyperquestsvc_jobs] PRIMARY KEY (JobID,LynxID,DocumentID, DocumentTypeID, ShopLocationID, SeqID)
GO

SET ANSI_PADDING ON
GO

