DECLARE @BundlingID SMALLINT
DECLARE @MessageTemplateID SMALLINT
SET @BundlingID = 0
SET @MessageTemplateID = 0

DECLARE @ToInscCompID SMALLINT
SET @ToInscCompID = 374

------------------------------
-- Delete old bundles
------------------------------
--SELECT * FROM utb_client_bundling WHERE BundlingID = 371
--SELECT * FROM utb_bundling WHERE BundlingID = 371
--SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPS%'
--SELECT * FROM utb_message_template WHERE MessageTemplateID = 359 --@MessageTemplateID
--SELECT * FROM utb_bundling_document_type WHERE BundlingID = 371

IF EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPS_RC_Combined_Bill_Release_Close_EB%')
BEGIN
	SELECT
		@BundlingID=b.BundlingID
		, @MessageTemplateID=t.MessageTemplateID
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template t 
			ON t.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @ToInscCompID
		AND t.[Description] LIKE '%msgELPS_RC_Combined_Bill_Release_Close_EB%'

	UPDATE utb_bundling SET [Name] = 'Closing - Repair Complete' WHERE BundlingID = @BundlingID
	UPDATE utb_message_template SET Description = 'Closing - Repair Complete|Messages/msgELPSClosingRepairComplete.xsl' WHERE MessageTemplateID = @MessageTemplateID

	SELECT 'Bundling Name Changed...'
END
ELSE
BEGIN
	SELECT 'Bundling Name Already Changed...'
END

IF EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgELPSStatusUpdAction.xsl')
BEGIN
	SELECT
		@BundlingID=b.BundlingID
		, @MessageTemplateID=t.MessageTemplateID
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template t 
			ON t.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @ToInscCompID
		AND t.[Description] LIKE '%msgELPSStatusUpdAction.xsl'

	UPDATE utb_bundling SET [Name] = 'Status Update - Claim Rep' WHERE BundlingID = @BundlingID
	UPDATE utb_message_template SET Description = 'Status Update - Claim Rep|Messages/msgELPSStatusUpdAction.xsl' WHERE MessageTemplateID = @MessageTemplateID

	SELECT 'Bundling Name Changed...'
END
ELSE
BEGIN
	SELECT 'Bundling Name Already Changed...'
END

