-----------------------------------------------------------------------------------------------------------
-- This script will create the basic configuration for Utica East Windsor District Office
-----------------------------------------------------------------------------------------------------------
-- BE SURE TO COMMIT OR ROLLBACK THE TRANSACTION
-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @InsuranceCompanyID     smallint
DECLARE @OfficeID               smallint


DECLARE @ModifiedDateTime       DateTime

SET @ModifiedDateTime = Current_Timestamp

SET @InsuranceCompanyID = 259


PRINT '.'
PRINT '.'
PRINT 'Updating tables...'
PRINT '.'


-- Create Office(s)

-- Southeastern Regional office (10/26/2010)

INSERT INTO dbo.utb_office
        (
         InsuranceCompanyID, 
         Address1,
         Address2, 
         AddressCity, 
         AddressState, 
         AddressZip,
         CCEmailAddress,
         ClaimNumberFormatJS,
         ClaimNumberValidJS,
         ClaimNumberMsgText,
         ClientOfficeId,
         EnabledFlag, 
         FaxAreaCode,
         FaxExchangeNumber,
         FaxUnitNumber,
         MailingAddress1,
         MailingAddress2, 
         MailingAddressCity, 
         MailingAddressState, 
         MailingAddressZip,
         Name, 
         PhoneAreaCode, 
         PhoneExchangeNumber, 
         PhoneUnitNumber,
         ReturnDocEmailAddress,
         ReturnDocFaxAreaCode,
         ReturnDocFaxExchangeNumber,
         ReturnDocFaxUnitNumber, 
         SysLastUserID,
         SysLastUpdatedDate 
        )
        VALUES( @InsuranceCompanyID,   -- InsuranceCompanyID
                '1040 Crown Pointe Parkway',	-- Address1
                'Suite 450',					-- Address2
                'Atlanta',						-- AddressCity
                'GA',                  -- AddressState
                '30338',               -- AddressZip
                NULL,                  -- CCEmailAddress
                NULL,                  -- ClaimNumberFormatJS
                NULL,                  -- ClaimNumberValidJS
                NULL,                  -- ClaimNumberMsgText
                'SERO',           -- ClientOfficeId			--SERO
                1,                     -- EnabledFlag
                '770',                 -- FaxAreaCode
                '510',                 -- FaxExchangeNumber
                '4272',                -- FaxUnitNumber
                'P.O. Box 5310',       -- MailingAddress1
                NULL,                  -- MailingAddress2
                'Binghamton',               -- MailingAddressCity
                'NY',                  -- MailingAddressState
                '13902',               -- MailingAddressZip
                'Southeastern Regional Office', -- Name
                '800',                 -- PhoneAreaCode
                '477',                 -- PhoneExchangeNumber
                '1914',                -- PhoneUnitNumber
                '',                    -- ReturnDocEmailAddress			
                '972',                 -- ReturnDocFaxAreaCode				
                '301',                 -- ReturnDocFaxExchangeNumber		--mailing fax number
                '4211',                -- ReturnDocFaxUnitNumber
                0,                     -- SysLastUserID
                @ModifiedDateTime      -- SysLastUpdatedDate
        )


SELECT @OfficeID = SCOPE_IDENTITY()


-- Office level configurations
INSERT INTO dbo.utb_office_assignment_type 
  SELECT  @OfficeID, 
          AssignmentTypeID, 
          0,
          @ModifiedDateTime
    FROM  dbo.utb_assignment_type
    WHERE Name in ( 'Program Shop Assignment', 
	                 'Demand Estimate Audit')
    
    
INSERT INTO dbo.utb_office_contract_state
  SELECT  @OfficeID,
          StateCode,
          0, -- UseCEIShopsFlag
          0, -- SysLastUserID
          @ModifiedDateTime
    FROM  dbo.utb_state_code


-- Update Fax Return Number to 972-301-4211 for all offices per Utica
UPDATE utb_office
SET ReturnDocFaxAreaCode = 972,
	ReturnDocFaxExchangeNumber = 301,
	ReturnDocFaxUnitNumber = 4211,
	SysLastUpdatedDate = @ModifiedDateTime
WHERE InsuranceCompanyID = @InsuranceCompanyID


-- Review affected tables
SELECT * FROM dbo.utb_office WHERE OfficeID = @OfficeID
SELECT * FROM dbo.utb_office_assignment_type WHERE OfficeID = @OfficeID
SELECT * FROM dbo.utb_office_contract_state WHERE OfficeID = @OfficeID


commit 
--rollback

