declare @DocumentSourceId as int
declare @DisplayOrder as int

SELECT @DocumentSourceId = MAX(DocumentSourceId) + 1
       , @DisplayOrder = MAX(DisplayOrder) + 1
  FROM dbo.utb_document_source
  
IF NOT EXISTS(SELECT DocumentSourceId
                FROM dbo.utb_document_source
               WHERE Name = 'Web Services')
BEGIN
    INSERT INTO dbo.utb_document_source (
        DocumentSourceID
        , DisplayOrder
        , EnabledFlag
        , Name
        , VANFlag
        , SysMaintainedFlag
        , SysLastUserID
        , SysLastUpdatedDate
    )
    SELECT 
        @DocumentSourceId
        , @DisplayOrder
        , 1
        , 'Web Services'
        , 0
        , 1
        , 0
        , current_timestamp
END