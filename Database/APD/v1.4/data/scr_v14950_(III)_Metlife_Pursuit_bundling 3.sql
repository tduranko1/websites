/******************************************************************************
* Metlife Bundling profiles
******************************************************************************/

DECLARE @Now as datetime
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @InsuranceCompanyID AS int
DECLARE @ClientBundlingID as int

SET @NOW = CURRENT_TIMESTAMP
SET @InsuranceCompanyID = 176

BEGIN TRANSACTION

--------------------------------------
-- Acknowledgement of Claim
--------------------------------------
SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Acknowledgement of Claim'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'importrequest@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID



--------------------------------------
-- Scheduled Inspection Status Update
--------------------------------------
SELECT @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Scheduled Inspection Status Update'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'Choice1@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID


--------------------------------------
-- Escalation Request
--------------------------------------
SELECT @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Escalation Request'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'Choice1@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID


--------------------------------------
-- Closing - Agreed Audit Complete
--------------------------------------
SELECT @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID,
        @BundlingID = b.BundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Closing - Agreed Audit Complete'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'metlifeautoestimatespayment@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add photos
INSERT INTO dbo.utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    LossState,
    MandatoryFlag,
    SelectionOrder,
    ShopState,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,   -- BundlingID,
    8,             -- DocumentTypeID,
    'I',           -- DirectionalCD,
    0,             -- DirectionToPayFlag,
    1,             -- DuplicateFlag,
    0,             -- EstimateDuplicateFlag,
    NULL,          -- EstimateTypeCD,
    0,             -- FinalEstimateFlag,
    NULL,          -- LossState,
    0,             -- MandatoryFlag,
    4,             -- SelectionOrder,
    NULL,          -- ShopState,
    0,             -- VANFlag,
    0,             -- SysLastUserID,
    @now           -- SysLastUpdatedDate
)
   
--------------------------------------
-- Closing - Non-Agreed Audit Complete
--------------------------------------
SELECT @ClientBundlingID = -1

SELECT @ClientBundlingID = ClientBundlingID,
        @BundlingID = b.BundlingID
FROM utb_client_bundling cb
INNER JOIN utb_bundling b ON cb.BundlingID = b.BundlingID
WHERE b.Name = 'Closing - Non Agreed Audit Complete'

UPDATE utb_client_bundling
SET ReturnDocRoutingValue = 'mds@metlife.com'
WHERE ClientBundlingID = @ClientBundlingID

UPDATE utb_bundling
SET Name = 'Closing - Not Agreed Audit Complete'
WHERE BundlingID = @BundlingID

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add photos
INSERT INTO dbo.utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    LossState,
    MandatoryFlag,
    SelectionOrder,
    ShopState,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,   -- BundlingID,
    8,             -- DocumentTypeID,
    'I',           -- DirectionalCD,
    0,             -- DirectionToPayFlag,
    1,             -- DuplicateFlag,
    0,             -- EstimateDuplicateFlag,
    NULL,          -- EstimateTypeCD,
    0,             -- FinalEstimateFlag,
    NULL,          -- LossState,
    0,             -- MandatoryFlag,
    4,             -- SelectionOrder,
    NULL,          -- ShopState,
    0,             -- VANFlag,
    0,             -- SysLastUserID,
    @now           -- SysLastUpdatedDate
)
   
--------------------------------------
-- Audit Authorization Request - No Photos
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Audit Authorization Request - No Photos|Messages/msgPursuitAuditAuthReqNoPhotos.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Audit Authorization Request - No Photos', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'mds@metlife.com',
   0,
   @now





--------------------------------------
-- Closing - Audit Not Authorized
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing - Audit Not Authorized|Messages/msgPursuitAuditClosingAuditNotAuthorized.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing - Audit Not Authorized', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add photos
   INSERT INTO dbo.utb_bundling_document_type (
      BundlingID,
      DocumentTypeID,
      DirectionalCD,
      DirectionToPayFlag,
      DuplicateFlag,
      EstimateDuplicateFlag,
      EstimateTypeCD,
      FinalEstimateFlag,
      LossState,
      MandatoryFlag,
      SelectionOrder,
      ShopState,
      VANFlag,
      SysLastUserID,
      SysLastUpdatedDate
   ) VALUES (
      @BundlingID,   -- BundlingID,
      8,             -- DocumentTypeID,
      'I',           -- DirectionalCD,
      0,             -- DirectionToPayFlag,
      1,             -- DuplicateFlag,
      0,             -- EstimateDuplicateFlag,
      NULL,          -- EstimateTypeCD,
      0,             -- FinalEstimateFlag,
      NULL,          -- LossState,
      0,             -- MandatoryFlag,
      2,             -- SelectionOrder,
      NULL,          -- ShopState,
      0,             -- VANFlag,
      0,             -- SysLastUserID,
      @now           -- SysLastUpdatedDate
   )

-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'mds@metlife.com',
   0,
   @now




--------------------------------------
-- Closing - TOTAL LOSS Audit Complete
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing - Total Loss Audit Complete|Messages/msgPursuitAuditClosingTLAuditComplete.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing - Total Loss Audit Complete', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    33,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add photos
INSERT INTO dbo.utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    LossState,
    MandatoryFlag,
    SelectionOrder,
    ShopState,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,   -- BundlingID,
    8,             -- DocumentTypeID,
    'I',           -- DirectionalCD,
    0,             -- DirectionToPayFlag,
    1,             -- DuplicateFlag,
    0,             -- EstimateDuplicateFlag,
    NULL,          -- EstimateTypeCD,
    0,             -- FinalEstimateFlag,
    NULL,          -- LossState,
    0,             -- MandatoryFlag,
    4,             -- SelectionOrder,
    NULL,          -- ShopState,
    0,             -- VANFlag,
    0,             -- SysLastUserID,
    @now           -- SysLastUpdatedDate
)
   
-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'mds@metlife.com',
   0,
   @now




--------------------------------------
-- Closing - Estimate Not Secured
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing - Estimate Not Secured|Messages/msgPursuitAuditClosingEstNotSecured.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing - Estimate Not Secured', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'Choice1@metlife.com',
   0,
   @now



--------------------------------------
-- Pursuit Extension Request
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Pursuit Extension Request|Messages/msgPursuitAuditExtensionRequest.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Pursuit Extension Request', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'Choice1@metlife.com',
   0,
   @now


--------------------------------------
-- Closing - AGREED Supplement Complete
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing - Agreed Supplement Complete|Messages/msgPursuitAuditClosingAgreedSuppComplete.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing - Agreed Supplement Complete', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    33,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add photos
INSERT INTO dbo.utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    LossState,
    MandatoryFlag,
    SelectionOrder,
    ShopState,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,   -- BundlingID,
    8,             -- DocumentTypeID,
    'I',           -- DirectionalCD,
    0,             -- DirectionToPayFlag,
    1,             -- DuplicateFlag,
    0,             -- EstimateDuplicateFlag,
    NULL,          -- EstimateTypeCD,
    0,             -- FinalEstimateFlag,
    NULL,          -- LossState,
    0,             -- MandatoryFlag,
    4,             -- SelectionOrder,
    NULL,          -- ShopState,
    0,             -- VANFlag,
    0,             -- SysLastUserID,
    @now           -- SysLastUpdatedDate
)
   
-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'metlifeautoestimatespayment@metlife.com',
   0,
   @now



--------------------------------------
-- Closing - NOT AGREED Supplement Complete
--------------------------------------

INSERT INTO dbo.utb_message_template (
   AppliesToCD,
   EnabledFlag,
   Description,
   ServiceChannelCD,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 'C', 1, 'Closing - Not Agreed Supplement Complete|Messages/msgPursuitAuditClosingNotAgreedSuppComplete.xsl', 'DA', 0, @now

SET @MessageTemplateID = SCOPE_IDENTITY()

INSERT INTO utb_bundling (
   DocumentTypeID,
   MessageTemplateID,
   EnabledFlag,
   Name,
   SysLastUserID,
   SysLastUpdatedDate
)
SELECT 37, @MessageTemplateID, 1, 'Closing - Not Agreed Supplement Complete', 0, @now

SET @BundlingID = SCOPE_IDENTITY()

-- Add Desk Audit Summary Report
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    33,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add audited estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'A',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add original estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    10,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    'O',            -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

-- Add photos
INSERT INTO dbo.utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    LossState,
    MandatoryFlag,
    SelectionOrder,
    ShopState,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,   -- BundlingID,
    8,             -- DocumentTypeID,
    'I',           -- DirectionalCD,
    0,             -- DirectionToPayFlag,
    1,             -- DuplicateFlag,
    0,             -- EstimateDuplicateFlag,
    NULL,          -- EstimateTypeCD,
    0,             -- FinalEstimateFlag,
    NULL,          -- LossState,
    0,             -- MandatoryFlag,
    4,             -- SelectionOrder,
    NULL,          -- ShopState,
    0,             -- VANFlag,
    0,             -- SysLastUserID,
    @now           -- SysLastUpdatedDate
)
   
-- Add this bundle to the client
INSERT INTO dbo.utb_client_bundling (
   BundlingID,
   InsuranceCompanyID,
   ServiceChannelCD,
   ReturnDocPackageTypeCD,
   ReturnDocRoutingCD,
   ReturnDocRoutingValue,
   SysLastUserID,
   SysLastUpdatedDate
)
select
   @BundlingID,
   @InsuranceCompanyID,
   'DA',
   'PDF',
   'EML',
   'mds@metlife.com',
   0,
   @now

COMMIT TRANSACTION
