--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!! YOU MUST APPLY THE COMMIT OR ROLLBACK AFTER EXECUTING THIS SCRIPT !!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SET ANSI_NULLS ON
SET ANSI_WARNINGS ON
GO

BEGIN TRANSACTION


--
--
-- Script for updating APD reference data.
--

PRINT 'Remember to COMMIT or ROLLBACK changes'
PRINT '.'

-- Assignment_pool
ALTER TABLE dbo.utb_task_assignment_pool DROP CONSTRAINT ufk_task_assignment_pool_assignmentpoolid_in_assignment_pool
ALTER TABLE dbo.utb_assignment_pool_user DROP CONSTRAINT ufk_assignment_pool_user_assignmentpoolid_in_assignment_pool

-- Assignment_type
ALTER TABLE dbo.utb_claim_aspect DROP CONSTRAINT ufk_claim_aspect_initialassignmenttypeid_in_assignment_type
ALTER TABLE dbo.utb_client_assignment_type DROP CONSTRAINT ufk_client_assignment_type_assignmenttypeid_in_assignment_type
ALTER TABLE dbo.utb_office_assignment_type DROP CONSTRAINT ufk_office_assignment_type_assignmenttypeid_in_assignment_type

-- Service
ALTER TABLE dbo.utb_invoice_service DROP CONSTRAINT ufk_invoice_service_serviceid_in_service
ALTER TABLE dbo.utb_client_fee_definition DROP CONSTRAINT ufk_client_fee_definition_serviceid_in_service


PRINT '.'
PRINT '.'
PRINT 'Deleting previous data...'
PRINT '.'

DELETE FROM dbo.utb_app_variable
DELETE FROM dbo.utb_assignment_pool
DELETE FROM dbo.utb_assignment_type
DELETE FROM dbo.utb_service
DELETE FROM dbo.utb_task_completion_criteria

PRINT '.'
PRINT '.'
PRINT 'Inserting new data...'
PRINT '.'

DECLARE @ModifiedDateTime AS DATETIME
SET @ModifiedDateTime = CURRENT_TIMESTAMP


-- This is a query against an Excel spreadsheet through the OLE DB provider for Jet.

------------------------------------------------------

INSERT INTO dbo.utb_app_variable
SELECT  AppVariableID,
		Convert(varchar(500), Description),
		MaxRange,
		MinRange,
		Name,
		SubName,
		Convert(varchar(500), Value),
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_app_variable);

------------------------------------------------------

INSERT INTO dbo.utb_assignment_pool
SELECT  AssignmentPoolID,
        DisplayOrder, 
        EnabledFlag,
        FunctionCD,
        Name,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_assignment_pool);

------------------------------------------------------

INSERT INTO dbo.utb_assignment_type
SELECT  AssignmentTypeID,
        DisplayOrder, 
        EnabledFlag,
        Name,
        ServiceChannelDefaultCD,
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_assignment_type);

------------------------------------------------------

INSERT INTO dbo.utb_service
SELECT  ServiceID,
        BillingModelCD,
        DisplayOrder,
        DispositionTypeCD, 
        EnabledFlag,
        ExposureRequiredFlag,
        MultipleBillingFlag,
        Name,
        PartyCD,
        ServiceChannelCD,
        SysMaintainedFlag,
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_service);

------------------------------------------------------

INSERT INTO dbo.utb_task_completion_criteria
SELECT  TaskCompletionCriteriaID,
        TaskID,
        Convert(varchar(500), CriteriaSQL),
        Convert(varchar(500), FailureMessage), 
        Convert(varchar(500), ValidResultSetSQL),
        0, 
        @ModifiedDateTime
  FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0',
      'C:\Temp\referencedata\reference_data_1-4.mdb';'admin';'',utb_task_completion_criteria);

------------------------------------------------------

GO
PRINT '.'
PRINT '.'
PRINT 'Modified table(s)...'
PRINT '.'

SELECT * FROM dbo.utb_app_variable
SELECT * FROM dbo.utb_assignment_pool
SELECT * FROM dbo.utb_assignment_type
SELECT * FROM dbo.utb_service
SELECT * FROM dbo.utb_task_completion_criteria


GO

-- Assignment pool
ALTER TABLE dbo.utb_task_assignment_pool 
    ADD CONSTRAINT ufk_task_assignment_pool_assignmentpoolid_in_assignment_pool
    FOREIGN KEY (AssignmentPoolID)
    REFERENCES dbo.utb_assignment_pool(AssignmentPoolID)

ALTER TABLE dbo.utb_assignment_pool_user 
    ADD CONSTRAINT ufk_assignment_pool_user_assignmentpoolid_in_assignment_pool
    FOREIGN KEY (AssignmentPoolID)
    REFERENCES dbo.utb_assignment_pool(AssignmentPoolID)


-- Assignment type
ALTER TABLE dbo.utb_claim_aspect 
    ADD CONSTRAINT ufk_claim_aspect_initialassignmenttypeid_in_assignment_type
    FOREIGN KEY (InitialAssignmentTypeID)
    REFERENCES dbo.utb_assignment_type(AssignmentTypeID)

ALTER TABLE dbo.utb_client_assignment_type 
    ADD CONSTRAINT ufk_client_assignment_type_assignmenttypeid_in_assignment_type
    FOREIGN KEY (AssignmentTypeID)
    REFERENCES dbo.utb_assignment_type(AssignmentTypeID)

ALTER TABLE dbo.utb_office_assignment_type 
    ADD CONSTRAINT ufk_office_assignment_type_assignmenttypeid_in_assignment_type
    FOREIGN KEY (AssignmentTypeID)
    REFERENCES dbo.utb_assignment_type(AssignmentTypeID)

        
-- Service
ALTER TABLE dbo.utb_invoice_service 
    ADD CONSTRAINT ufk_invoice_service_serviceid_in_service
    FOREIGN KEY (ServiceID)
    REFERENCES dbo.utb_service(ServiceID)

ALTER TABLE dbo.utb_client_fee_definition 
    ADD CONSTRAINT ufk_client_fee_definition_serviceid_in_service
    FOREIGN KEY (ServiceID)
    REFERENCES dbo.utb_service(ServiceID)



PRINT '.'
PRINT '.'
PRINT 'Remember to COMMIT or ROLLBACK changes'
PRINT '!'

-- COMMIT
-- ROLLBACK

GO

