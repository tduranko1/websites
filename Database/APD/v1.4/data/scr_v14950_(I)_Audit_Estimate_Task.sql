
/*
	scr_v14950_(I)_Audit_Estimate_Task.sql
	W. Chesla
	9/12/2011
*/

-- utb_task
IF NOT EXISTS (SELECT 1 FROM utb_task WHERE TaskID = 100)
BEGIN
	INSERT INTO [utb_task]
			   ([TaskID]
			   ,[NoteTypeDefaultID]
			   ,[CommentCompleteRequiredFlag]
			   ,[CommentNARequiredFlag]
			   ,[CRDSortOrder]
			   ,[DisplayOrder]
			   ,[EnabledFlag]
			   ,[EscalationMinutes]
			   ,[Name]
			   ,[UserTaskFlag]
			   ,[SysMaintainedFlag]
			   ,[SysLastUserID]
			   ,[SysLastUpdatedDate])
		 VALUES
			   (100,		-- TaskID
			   NULL,		-- NoteTypeDefaultID
			   0,		-- CommentCompleteRequiredFlag
			   0,		-- CommentNARequiredFlag
			   NULL,		-- CRDSortOrder
			   128,		-- DisplayOrder
			   1,		-- EnabledFlag
			   2160,	-- EscalationMinutes
			   'Audit Estimate Values Required',		-- Name
			   0,		-- UserTaskFlag
			   1,		-- SysMaintainedFlag
			   0,		-- SysLastUserID
			   current_timestamp)		--SysLastUpdatedDate
END



-- utb_workflow
IF NOT EXISTS (SELECT 1 FROM utb_workflow WHERE WorkflowID = 169)
BEGIN
	INSERT INTO [utb_workflow]
           ([WorkflowID]
           ,[InsuranceCompanyID]
           ,[IgnoreDefaultFlag]
           ,[OriginatorID]
           ,[OriginatorTypeCD]
           ,[SysLastUserID]
           ,[SysLastUpdatedDate])
     VALUES
           (169,
           NULL,
           0,
           84,
           'E',
           0,
           current_timestamp)
END



-- utb_spawn
IF NOT EXISTS (SELECT 1 FROM utb_spawn WHERE WorkflowID = 169)
BEGIN
	INSERT INTO [utb_spawn]
           ([WorkflowID]
           ,[ConditionalValue]
           ,[ContextNote]
           ,[CustomProcName]
           ,[EnabledFlag]
           ,[SpawningID]
           ,[SpawningServiceChannelCD]
           ,[SpawningTypeCD]
           ,[SysLastUserID]
           ,[SysLastUpdatedDate])
     VALUES
           (169
           ,NULL
           ,NULL
           ,NULL
           ,1
           ,100
           ,NULL
           ,'T'
           ,0
           ,current_timestamp)
END



-- task assignment pool
IF NOT EXISTS (SELECT 1 FROM utb_task_assignment_pool WHERE TaskID = 100)
BEGIN
	INSERT INTO utb_task_assignment_pool
	VALUES (100, 'PS', 0, 2, 0, current_timestamp)		-- file owner (1), file analyst (2)
END



--utb_task_role inserts (24 total)
IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 1)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100, 1, 0, 0, 1, 0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 2)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,2,0,0,0,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 3)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,3,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 4)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,4,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 5)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,5,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 6)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,6,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 7)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,7,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 8)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,8,0,0,0,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 9)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,9,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 10)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,10,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 11)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,11,0,0,0,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 12)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,12,0,0,0,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 13)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,13,0,0,0,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 15)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,15,0,0,0,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 20)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,20,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 21)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,21,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 22)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,22,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 23)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,23,1,1,1,0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_task_role WHERE TaskID = 100 and RoleID = 24)
BEGIN
	INSERT INTO utb_task_role
	VALUES (100,24,1,1,1,0, current_timestamp)
END



--utb_status_task records
IF NOT EXISTS (SELECT 1 FROM utb_status_task WHERE StatusID = 2 AND TaskID = 100)
BEGIN
	INSERT INTO utb_status_task
	VALUES (2, 100, 1, 0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_status_task WHERE StatusID = 81 AND TaskID = 100)
BEGIN
	INSERT INTO utb_status_task
	VALUES (81, 100, 1, 0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_status_task WHERE StatusID = 100 AND TaskID = 100)
BEGIN
	INSERT INTO utb_status_task
	VALUES (100, 100, 1, 0, current_timestamp)
END

IF NOT EXISTS (SELECT 1 FROM utb_status_task WHERE StatusID = 500 AND TaskID = 100)
BEGIN
	INSERT INTO utb_status_task
	VALUES (500, 100, 1, 0, current_timestamp)
END


IF NOT EXISTS 
	(SELECT 1 FROM utb_task_completion_criteria WHERE TaskCompletionCriteriaID = 14 AND TaskID = 100)
BEGIN
	INSERT INTO utb_task_completion_criteria
	VALUES (14, 100, 'SELECT ClaimAspectID FROM utb_claim_aspect_audit_values WHERE ClaimAspectID = %ClaimAspectID%', 'Audit estimate values must be entered and saved', 'IS NOT NULL', 0, current_timestamp)
END
