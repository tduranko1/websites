------------------------------
-- AVIS Build Spawn/Workflow
------------------------------
IF NOT EXISTS (SELECT * FROM utb_workflow WHERE WorkflowID = 185 AND InsuranceCompanyID = 515)
BEGIN
	INSERT INTO utb_workflow SELECT 185,515,0,70,'E',0,CURRENT_TIMESTAMP
	INSERT INTO utb_workflow SELECT 186,515,0,29,'E',0,CURRENT_TIMESTAMP
	INSERT INTO utb_workflow SELECT 187,515,0,84,'E',0,CURRENT_TIMESTAMP

  SELECT 'AVIS Workflow Added...'
END
ELSE
BEGIN
	SELECT 'AVIS Workflow already added...'
END

IF NOT EXISTS (SELECT * FROM utb_spawn WHERE SpawnID = 291 AND WorkflowID = 185)
BEGIN
	INSERT INTO utb_spawn SELECT 185, 'PS', '%Key%', NULL, 1, 6, NULL, 'T', 0, CURRENT_TIMESTAMP
	INSERT INTO utb_spawn SELECT 185, 'PS', '%PertainsToName%', NULL, 0, 35, NULL, 'T', 0, CURRENT_TIMESTAMP
	INSERT INTO utb_spawn SELECT 185, 'PS', '%PertainsToName%', NULL, 1, 2, NULL, 'T', 0, CURRENT_TIMESTAMP
	INSERT INTO utb_spawn SELECT 185, 'RENTAL', NULL, NULL, 1, 24, NULL, 'T', 0, CURRENT_TIMESTAMP
	INSERT INTO utb_spawn SELECT 185, 'FNOL', NULL, NULL, 1, 2, NULL, 'T', 0, CURRENT_TIMESTAMP

	INSERT INTO utb_spawn SELECT 187, 'PS', NULL, NULL, 1, 68, NULL, 'T', 0, CURRENT_TIMESTAMP

  SELECT 'AVIS Spawns Added...'
END
ELSE
BEGIN
	SELECT 'AVIS Spawns Added already added...'
END

