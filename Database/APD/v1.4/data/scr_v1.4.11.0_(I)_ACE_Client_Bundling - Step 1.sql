DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 493
SET @FromInscCompID = 473

------------------------------
-- Add Message Templates
------------------------------
IF NOT EXISTS (SELECT * FROM utb_message_template WHERE [Description] LIKE '%msgACERRP%' )
BEGIN
	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('RRP - Acknowledgement of Claim')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('RRP - Assignment Cancellation')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('RRP - No Inspection Alert & Close')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('RRP - Status Update - Claim Rep')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('RRP - Closing Inspection Complete')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('RRP - Closing Supplement')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('RRP - Closing Total Loss')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('RRP - Status Update - MDS')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('SHOP: RRP - Approved Estimate Shop Notification')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('RRP - Total Loss Alert (Adjuster)')	

	INSERT INTO utb_message_template 
	SELECT
		mt.AppliesToCD
		, mt.EnabledFlag
		, CASE mt.ServiceChannelCD
			WHEN 'RRP' THEN 
			    REPLACE(mt.[Description],'msgANRRP','msgACERRP')
		  END
		, mt.ServiceChannelCD
		, 0
		, CURRENT_TIMESTAMP 
	FROM
		utb_bundling b
		INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID
		INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
	WHERE
		cb.InsuranceCompanyID = @FromInscCompID	
			AND b.Name IN ('SHOP: RRP - Total Loss Determination (Shop)')	

	SELECT 'Message Template added...'
END
ELSE
BEGIN
	SELECT 'Message Template already added...'
END

SELECT * FROM utb_message_template WHERE SysLastUpdatedDate > CONVERT(VARCHAR(50),CURRENT_TIMESTAMP,101)

