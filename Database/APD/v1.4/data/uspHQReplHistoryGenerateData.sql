-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHQReplHistoryGenerateData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHQReplHistoryGenerateData 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHQReplHistoryGenerateData
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Populates the replication table for HQ History.  HQ will replicate this table with their database.
* Date:			21Jul2016
*
* PARAMETERS:  @LastRunOverride = VARCHAR of override date for specific date processing
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	21Jul2016 - TVD - Initial development
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHQReplHistoryGenerateData]
	@LastRunOverride	VARCHAR(50) = NULL
AS
BEGIN
	-----------------------------------
	-- Declarations 
	-----------------------------------
	DECLARE @Debug              udt_std_flag
	SET @Debug = 1

	DECLARE @LastRunDateTime	udt_std_datetime
	DECLARE @LastAPDRunDateTime	udt_std_datetime
	DECLARE @LynxID				INT
	DECLARE @ApplicationCD      udt_std_cd='APD'
	DECLARE @NumRowsProcessed   INT
	SET @NumRowsProcessed = 0

    IF @Debug = 1
    BEGIN
        PRINT 'Processing Started: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
		PRINT '---> Declaring tables '
    END

	DECLARE @tmpHistory TABLE
	(
		LogID               bigint      NOT NULL,
		ClaimAspectTypeID   int         NOT NULL,
		ClaimAspectNumber   int         NOT NULL,
		PertainsTo          varchar(50) NULL,
		InsuranceCompanyID  int         NOT NULL
	)

	-- Create temporary table to hold references to the claim's notes
	DECLARE @tmpNotes TABLE 
	(
		LynxID                      bigint          NOT NULL,
		DocumentID                  int             NOT NULL,
		PertainsTo                  varchar(50)     NOT NULL,
		ClaimAspectTypeName         varchar(50)     NOT NULL,
		ClaimAspectNumber           tinyint         NOT NULL,
		ClaimAspectID               bigint          NOT NULL,    
		ClaimAspectServiceChannelID bigint          NOT NULL,
		InsuranceCompanyID			int				NOT NULL
	)

	-- Create temporary table to hold references to the claim's history and notes
	DECLARE @tmpHistoryNotes TABLE 
	(
		LogID			            bigint			NULL,
		CompletedDate	            varchar(30)     NULL,
		Description                 varchar(6000)   NULL,
		EventName                   varchar(250)    NULL,
		PertainsTo                  varchar(50)     NULL,
		TaskName                    varchar(250)    NULL,
		ClaimAspectServiceChannelID bigint          NULL,
		VehicleNumber				bigint          NULL,
		InsuranceCompanyID			bigint          NULL,
		LynxID						bigint          NULL,
		OwnerUserID					bigint          NULL,
		OwnerNameFirst				varchar(250)    NULL,
		OwnerNameLast				varchar(250)    NULL,
		OwnerEmail		            varchar(250)    NULL,
		AnalystUserID				bigint          NULL,
		AnalystNameFirst			varchar(250)    NULL,
		AnalystNameLast				varchar(250)    NULL,
		AnalystEmail		        varchar(250)    NULL,
		SupportUserID				bigint          NULL,
		SupportNameFirst			varchar(250)    NULL,
		SupportNameLast				varchar(250)    NULL,
		SupportEmail		        varchar(250)    NULL,
		CreatedRoleName	            varchar(50)     NULL,
		CreatedUserID				bigint          NULL,
		CreatedByNameFirst			varchar(250)    NULL,
		CreatedByNameLast			varchar(250)    NULL,
		CreatedEmail                varchar(250)    NULL,
		CreatedDate                 varchar(250)    NULL,
		CompletedRoleName	        varchar(50)     NULL,
		CompletedUserID				bigint		    NULL,
		CompletedByNameFirst		varchar(50)     NULL,
		CompletedByNameLast			varchar(50)     NULL,
		CompletedEmail				varchar(50)     NULL,
		EventID			            bigint			NULL
	)

	-- Create temporary table to hold currently active LynxID's to process
	DECLARE @tmpActiveLynxID TABLE 
	(
		LynxID						INT
		, Syslastupdateddate 		DATETIME
	)


	DECLARE @DocumentID                 udt_std_int_big
	DECLARE @InsuranceCompanyIDClaim    udt_std_id
	DECLARE @ProcName                   AS varchar(30)   -- Used for raise error stmts 

    IF @Debug = 1
    BEGIN
		PRINT '---> Checking last run times: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
        PRINT '---> Processing Started: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
    END

	SET @ProcName = 'EventsToHQ'
	SET @LastRunDateTime = CURRENT_TIMESTAMP

	-----------------------------------
	-- Start by getting the last 
	-- SysLastUpdatedDate from the 
	-- utb_HQRepl_History table 
	-----------------------------------
	IF @LastRunOverride IS NULL
	BEGIN
		SELECT 'NO Override'

		SELECT TOP 1 
			@LastRunDateTime = SysLastUpdatedDate 
		FROM 
			utb_HQRepl_History 
		ORDER BY 
			LynxID DESC
			, SysLastUpdatedDate DESC

		IF @Debug = 1
		BEGIN
			PRINT '---> NO Override: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
			PRINT '-----> @LastRunDateTime : ' + CONVERT(VARCHAR(50), @LastRunDateTime)
		END

		--SELECT @LastRunDateTime
	END
	ELSE
	BEGIN
		SELECT 'Override'
		SET @LastRunDateTime = @LastRunOverride

		IF @Debug = 1
		BEGIN
			PRINT '---> Override: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
			PRINT '-----> @LastRunOverride : ' + CONVERT(VARCHAR(50), @LastRunDateTime)
		END
	END

	-----------------------------------
	-- Insert new history lynxID's 
	-- into Temp Table
	-----------------------------------
	INSERT INTO @tmpActiveLynxID
	SELECT DISTINCT
		LynxID
		, SysLastUpdatedDate 
	FROM 
		utb_history_log 
	WHERE 
		SysLastUpdatedDate > @LastRunDateTime 
	ORDER BY 
		SysLastUpdatedDate DESC
		, LynxID DESC

	IF @Debug = 1
	BEGIN
		PRINT '---> Collecting NEW LynxIDs from History: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
	END

	--TEMP
	--SET @LynxID = 1815989

	IF @LastAPDRunDateTime IS NULL
	BEGIN
		SET @LastAPDRunDateTime = CURRENT_TIMESTAMP
	END

	-----------------------------------
	-- Check if anything needs processed
	-----------------------------------
	IF EXISTS(
		SELECT DISTINCT 
			LynxID 
		FROM 
			@tmpActiveLynxID
	)
	BEGIN
		IF @Debug = 1
		BEGIN
			PRINT '---> NEW LynxIDs are waiting to process: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
		END

		-----------------------------------
		-- Cursor loop through each lynxID
		-- and insert into main table
		-----------------------------------
		DECLARE db_cursor CURSOR FOR 
		SELECT DISTINCT 
			LynxID 
		FROM 
			@tmpActiveLynxID

		DECLARE @curLynxID INT

		IF @Debug = 1
		BEGIN
			PRINT '-----> Initializing the Cursor'
		END

		OPEN db_cursor  
		FETCH NEXT FROM db_cursor INTO 	
				@curLynxID

		WHILE @@FETCH_STATUS = 0  
		BEGIN 
			----------------------------
			-- Initalize the temp tables
			----------------------------
			DELETE FROM @tmpHistory
			DELETE FROM @tmpNotes
			DELETE FROM @tmpHistoryNotes

			IF @Debug = 1
			BEGIN
				PRINT '-----> Clearing Temp tables and starting Processing : ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
				PRINT '--------> Updating @tempHistory table'
			END

			SET @LynxID = @curLynxID

			-----------------------------------
			-- NEW Records exist, process them
			-- Lets get Claim Aspects from 
			-- History
			-----------------------------------
			INSERT INTO @tmpHistory (LogID, ClaimAspectTypeID, ClaimAspectNumber, InsuranceCompanyID)
				SELECT  
						hl.LogID,
						hl.ClaimAspectTypeID,
						hl.ClaimAspectNumber,
						c.InsuranceCompanyID
				FROM  
					dbo.utb_history_log hl WITH(NOLOCK)
					INNER JOIN utb_claim c WITH(NOLOCK)
						ON c.LynxID = hl.LynxID
				WHERE hl.LynxID = @LynxID

			IF @@ERROR <> 0
			BEGIN
				-- SQL Server Error
				IF @Debug = 1
				BEGIN
					PRINT '--------> ERROR: Updating @tempHistory table - FAILED...'
				END
    
				RAISERROR('10|%s', 16, 1, @ProcName)
				RETURN
			END

			-- Update Pertains To
			UPDATE @tmpHistory
				SET  PertainsTo = dbo.ufnUtilityGetPertainsTo(tmpH.ClaimAspectTypeID, tmpH.ClaimAspectNumber, 1)
				FROM @tmpHistory tmpH

			IF @@ERROR <> 0
			BEGIN
				-- SQL Server Error
				IF @Debug = 1
				BEGIN
					PRINT '--------> ERROR: Updating PertainsTo in @tempHistory table - FAILED...'
				END
    
				RAISERROR('20|%s', 16, 1, @ProcName)
				RETURN
			END

			--------------------------------------
			-- Let's get the Notes for this claim
			--------------------------------------
			IF NOT EXISTS(SELECT Name FROM dbo.utb_document_type WITH(NOLOCK) WHERE Name = 'Note')
			BEGIN
				-- Note Document Type Not Found
    			IF @Debug = 1
				BEGIN
					PRINT '-----> Clearing Temp tables and starting Processing : ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
					PRINT '--------> ERROR: Note DocumentTypeID not found - FAILED...'
				END

				RAISERROR('102|%s|"Note"|utb_document_type', 16, 1, @ProcName)
				RETURN
			END

    		IF @Debug = 1
			BEGIN
				PRINT '--------> Updating @tmpNotes table'
			END

			--------------------------------------
			-- Compile note list together from all 
			-- the various entities
			--------------------------------------
			INSERT INTO @tmpNotes
			SELECT  
				@LynxID,
				cascd.DocumentID,
				dbo.ufnUtilityGetPertainsTo(ca.ClaimAspectTypeID, ca.ClaimAspectNumber, 1) AS PertainsTo,
				cat.Code,
				ca.ClaimAspectNumber,
				ca.ClaimAspectID,
				casc.ClaimAspectServiceChannelID,
				c.InsuranceCompanyID
			FROM  
				dbo.utb_claim_aspect_Service_Channel_document cascd WITH(NOLOCK)
				INNER join utb_Claim_Aspect_Service_Channel casc WITH(NOLOCK) 
					ON casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
				LEFT JOIN dbo.utb_claim_aspect ca WITH(NOLOCK) 
					ON (casc.ClaimAspectID = ca.ClaimAspectID)
				INNER JOIN dbo.utb_Claim c WITH(NOLOCK) 
					ON (c.LynxID = ca.LynxID)
				INNER JOIN dbo.utb_document d WITH(NOLOCK) 
					ON (cascd.DocumentID = d.DocumentID)
				INNER JOIN dbo.utb_document_type dt WITH(NOLOCK) 
					ON (d.DocumentTypeID = dt.DocumentTypeID)
				INNER JOIN dbo.utb_claim_aspect_type cat WITH(NOLOCK) 
					ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
			WHERE 
				ca.LynxID = @LynxID
				AND dt.Name = 'Note'
				AND d.EnabledFlag = 1
				--AND d.PrivateFlag = 0
				AND casc.EnabledFlag = 1

			IF @@ERROR <> 0
			BEGIN
				-- Insertion failure
	   			IF @Debug = 1
				BEGIN
					PRINT '--------> ERROR: Updating the @tmpNotes table - FAILED...'
				END    
				RAISERROR('105|%s|@tmpNotes', 16, 1, @ProcName)
				RETURN
			END

    		IF @Debug = 1
			BEGIN
				PRINT '--------> Updating @tmpHistoryNotes table - Phase 1'
			END

			--------------------------------------
			-- Let's insert the History gathered 
			-- first into the common table
			--------------------------------------
			INSERT INTO @tmpHistoryNotes
			SELECT  
					tmpH.LogID,
					dbo.ufnUtilityGetDateString(hl.CompletedDate),
					CASE 
						WHEN hl.LogType = 'E' THEN IsNull(hl.Description, '')
						WHEN hl.LogType = 'S' THEN IsNull(hl.Description, '')
						WHEN hl.LogType = 'T' THEN 'Task ' +
								CASE 
									WHEN LTRIM(RTRIM(hl.Description)) IS NOT NULL AND LTRIM(RTRIM(hl.Description)) <> '[NULL]' THEN '(Description: ' + IsNull(LTRIM(RTRIM(hl.Description)), '') + ') '
									ELSE ''
								END +
								CASE
									WHEN hl.NotApplicableFlag = 0 THEN 'Completed ' 
									ELSE 'Marked N/A '
								END +
								CASE
									WHEN (LTRIM(RTRIM(hl.ReferenceID)) IS NOT NULL) AND
											((SELECT LTRIM(RTRIM(d.Note)) FROM dbo.utb_document d WHERE DocumentID = hl.ReferenceID) IS NOT NULL) AND
											((SELECT LTRIM(RTRIM(d.Note)) FROM dbo.utb_document d WHERE DocumentID = hl.ReferenceID) <> '[NULL]') 
												THEN 'with Note: ' + IsNull((SELECT LTRIM(RTRIM(d.Note)) FROM dbo.utb_document d WHERE DocumentID = hl.ReferenceID), '')
									ELSE ''
								END
						ELSE ''
					END,
					IsNull(e.Name, ''),
					tmpH.PertainsTo,
					IsNull(t.Name, ''),
					hl.ClaimAspectServiceChannelID,
					hl.ClaimAspectNumber,
					tmpH.InsuranceCompanyID,
					NULL,
					Entity.OwnerUserID,
					Entity.OwnerNameFirst,
					Entity.OwnerNameLast,
					Entity.OwnerEmail,
					Entity.AnalystUserID,
					Entity.AnalystNameFirst,
					Entity.AnalystNameLast,
					Entity.AnalystEmail,
					Entity.SupportUserID,
					Entity.SupportNameFirst,
					Entity.SupportNameLast,
					Entity.SupportEmail,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					e.EventID
			FROM (SELECT @LynxID AS LynxID, @ApplicationCD AS ApplicationCD) AS parms
			LEFT JOIN dbo.utb_history_log hl WITH(NOLOCK) ON (parms.LynxID = hl.LynxID)
			LEFT JOIN @tmpHistory tmpH ON (hl.LogID = tmpH.LogID)
			LEFT JOIN dbo.utb_event e WITH(NOLOCK) ON (hl.EventID = e.EventID)
			LEFT JOIN dbo.utb_task t WITH(NOLOCK) ON (hl.TaskID = t.TaskID)
			LEFT JOIN (
				SELECT TOP 1
				ca.LynxID,
				uo.UserID 'OwnerUserID',
				uo.NameFirst 'OwnerNameFirst',
				uo.NameLast 'OwnerNameLast',
				uo.EmailAddress 'OwnerEmail',
				ua.UserID 'AnalystUserID',
				ua.NameFirst 'AnalystNameFirst',
				ua.NameLast 'AnalystNameLast',
				ua.EmailAddress 'AnalystEmail',
				us.UserID 'SupportUserID',
				us.NameFirst 'SupportNameFirst',
				us.NameLast 'SupportNameLast',
				us.EmailAddress 'SupportEmail'
			FROM dbo.utb_claim_aspect ca 
				LEFT JOIN dbo.utb_claim_aspect_type cat ON ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
				LEFT JOIN dbo.utb_user uo ON ca.OwnerUserID = uo.UserID
				LEFT JOIN dbo.utb_user_application uoa ON (uo.UserID = uoa.UserID AND uoa.ApplicationID = 1)
				LEFT JOIN dbo.utb_user ua ON ca.AnalystUserID = ua.UserID
				LEFT JOIN dbo.utb_user_application uaa ON (ua.UserID = uaa.UserID AND uaa.ApplicationID = 1)
				LEFT JOIN dbo.utb_user us ON ca.SupportUserID = us.UserID
				LEFT JOIN dbo.utb_user_application usa ON (us.UserID = usa.UserID AND usa.ApplicationID = 1)
			WHERE ca.LynxID = @LynxID
				AND ca.EnabledFlag = 1
				AND cat.Name in ('Vehicle')
			) AS Entity ON Entity.LynxID = @LynxID

			--LEFT JOIN @tmpOwners o ON (o.LynxID = parms.LynxID)

    		IF @Debug = 1
			BEGIN
				PRINT '--------> Updating @tmpHistoryNotes table - Phase 2'
			END

			--------------------------------------
			-- Let's insert the Notes gathered next 
			-- into the common table
			--------------------------------------
			INSERT INTO @tmpHistoryNotes
			SELECT 
					d.DocumentID,
					dbo.ufnUtilityGetDateString( d.SysLastUpdatedDate ),
					IsNull(LEFT(u.NameFirst, 1), '') + '. ' + IsNull(u.NameLast, '') + ' - ' + IsNull(d.Note, ''),
					nt.Name,
					IsNull(n.PertainsTo, ''),
					NULL,
					n.ClaimAspectServiceChannelID,
					n.ClaimAspectNumber,
					n.InsuranceCompanyID,
					n.LynxID,
					Entity.OwnerUserID,
					Entity.OwnerNameFirst,
					Entity.OwnerNameLast,
					Entity.OwnerEmail,
					Entity.AnalystUserID,
					Entity.AnalystNameFirst,
					Entity.AnalystNameLast,
					Entity.AnalystEmail,
					Entity.SupportUserID,
					Entity.SupportNameFirst,
					Entity.SupportNameLast,
					Entity.SupportEmail,
					r.name,
					d.CreatedUserID,
					u.NameFirst,
					u.NameLast,
					u.EmailAddress,
					CONVERT(VARCHAR(50), d.CreatedDate,121),
					r1.Name,
					d.syslastuserid,
					u1.NameFirst,
					u1.NameLast,
					u1.EmailAddress,
					999
			FROM    
				(
					SELECT @LynxID AS LynxID) AS parms
					LEFT JOIN @tmpNotes n 
						ON (parms.LynxID = n.LynxID
				)
				LEFT JOIN dbo.utb_document d WITH(NOLOCK) 
					ON (n.DocumentID = d.DocumentID)
				LEFT JOIN dbo.utb_user u WITH(NOLOCK) 
					ON (d.CreatedUserID = u.UserID)
				LEFT JOIN dbo.utb_user u1 WITH(NOLOCK) 
					ON (d.SysLastUserID = u1.UserID)
				LEFT JOIN dbo.utb_role r WITH(NOLOCK) 
					ON (d.CreatedUserRoleID = r.RoleID)
				LEFT JOIN dbo.utb_role r1 WITH(NOLOCK) 
					ON (d.CreatedUserRoleID = r1.RoleID)
				LEFT JOIN dbo.utb_note_type nt WITH(NOLOCK) 
					ON (nt.NoteTypeID = d.NoteTypeID)
			LEFT JOIN (
				SELECT TOP 1
				ca.LynxID,
				uo.UserID 'OwnerUserID',
				uo.NameFirst 'OwnerNameFirst',
				uo.NameLast 'OwnerNameLast',
				uo.EmailAddress 'OwnerEmail',
				ua.UserID 'AnalystUserID',
				ua.NameFirst 'AnalystNameFirst',
				ua.NameLast 'AnalystNameLast',
				ua.EmailAddress 'AnalystEmail',
				us.UserID 'SupportUserID',
				us.NameFirst 'SupportNameFirst',
				us.NameLast 'SupportNameLast',
				us.EmailAddress 'SupportEmail'
			FROM dbo.utb_claim_aspect ca 
				LEFT JOIN dbo.utb_claim_aspect_type cat ON ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
				LEFT JOIN dbo.utb_user uo ON ca.OwnerUserID = uo.UserID
				LEFT JOIN dbo.utb_user_application uoa ON (uo.UserID = uoa.UserID AND uoa.ApplicationID = 1)
				LEFT JOIN dbo.utb_user ua ON ca.AnalystUserID = ua.UserID
				LEFT JOIN dbo.utb_user_application uaa ON (ua.UserID = uaa.UserID AND uaa.ApplicationID = 1)
				LEFT JOIN dbo.utb_user us ON ca.SupportUserID = us.UserID
				LEFT JOIN dbo.utb_user_application usa ON (us.UserID = usa.UserID AND usa.ApplicationID = 1)
			WHERE ca.LynxID = @LynxID
				AND ca.EnabledFlag = 1
				AND cat.Name in ('Vehicle')
			) AS Entity ON Entity.LynxID = @LynxID

				--LEFT JOIN @tmpOwners o ON (o.LynxID = parms.LynxID)
			WHERE 
				d.Note <> ''

			IF @@ERROR <> 0
			BEGIN
				-- Insertion failure
    	   		IF @Debug = 1
				BEGIN
					PRINT '--------> ERROR: Updating the @tmpHistoryNotes table - FAILED...'
				END    

				RAISERROR('105|%s|@tmpHistoryNotes', 16, 1, @ProcName)
				RETURN
			END

    		IF @Debug = 1
			BEGIN
				PRINT '--------> Updating the Main utb_HQRepl_History table'
			END

			------------------------------
			-- Main - Select
			------------------------------
			--SELECT * FROM utb_HQRepl_History
			INSERT INTO utb_HQRepl_History
			SELECT  
					LogID
					, n.InsuranceCompanyID
					, @LynxID AS LynxID
					, CASE 
						WHEN vt.ServiceChannelCD = 'PS' THEN 'DRP'
						ELSE
							vt.ServiceChannelCD
						END
					, CASE 
						WHEN vt.ServiceChannelCD IS NULL THEN CONVERT(VARCHAR(10), n.InsuranceCompanyID)
						WHEN n.VehicleNumber > 0 THEN 
							CONVERT(VARCHAR(10), n.InsuranceCompanyID) + '-' + 
							CASE 
								WHEN vt.ServiceChannelCD = 'PS' THEN 'DRP'
								ELSE
									vt.ServiceChannelCD
							END
						ELSE CONVERT(VARCHAR(10), n.InsuranceCompanyID)
						END AS InscCompAndServiceChannel
					, vt.ClaimAspectServiceChannelID
					, n.VehicleNumber
					, n.CreatedRoleName
					, n.CreatedUserID
					, n.CreatedByNameFirst
					, n.CreatedByNameLast
					, n.CreatedEmail
					, n.CreatedDate
					, n.CompletedRoleName
					, n.CompletedUserID
					, n.CompletedByNameFirst
					, n.CompletedByNameLast
					, n.CompletedEmail
					, n.CompletedDate
					, n.EventID
					, n.EventName
					, SUBSTRING(REPLACE(Description,'''',''),0,500)
					, PertainsTo
					, isnull(TaskName,'') AS TaskName
					, 0
					, @LastAPDRunDateTime
					, n.OwnerUserID
					, n.OwnerNameFirst
					, n.OwnerNameLast
					, n.OwnerEmail
					, n.AnalystUserID
					, n.AnalystNameFirst
					, n.AnalystNameLast
					, n.AnalystEmail
					, n.SupportUserID
					, n.SupportNameFirst
					, n.SupportNameLast
					, n.SupportEmail
			FROM 
				@tmpHistoryNotes n
				LEFT OUTER JOIN (
					Select      casc.ClaimAspectServiceChannelID,
								CASE
									WHEN fn.Code = 'PS' THEN 'DRP'
									ELSE 
										fn.Code
									END as 'ServiceChannelCD',
								fn.Name as 'ServiceChannelCDName'
					from        utb_Claim_Aspect_Service_Channel casc WITH(NOLOCK)
					inner join  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') fn
					on          fn.Code = casc.ServiceChannelCD
				) vt
				ON vt.ClaimAspectServiceChannelID = n.ClaimAspectServiceChannelID
			ORDER BY 
				CompletedDate DESC

    		IF @Debug = 1
			BEGIN
				PRINT '-----> Process ' + CONVERT(VARCHAR(10), @curLynxID) + ' - Completed'
				SELECT @NumRowsProcessed = COUNT(LogID) FROM @tmpHistoryNotes
				PRINT '---> Number of rows processed ' + CONVERT(VARCHAR(10), @NumRowsProcessed)
			END

		FETCH NEXT FROM db_cursor INTO 	
				@curLynxID
		END

		CLOSE db_cursor  
		DEALLOCATE db_cursor 

		IF @Debug = 1
		BEGIN
			PRINT '---> All processing Complete: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
		END
	END
	ELSE
	BEGIN
    	IF @Debug = 1
		BEGIN
			PRINT '---> Nothing to process ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121)
		END

		SELECT CONVERT(VARCHAR(20), CURRENT_TIMESTAMP, 121) + ' - Nothing to Process' 
	END
END
GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHQReplHistoryGenerateData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHQReplHistoryGenerateData TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/