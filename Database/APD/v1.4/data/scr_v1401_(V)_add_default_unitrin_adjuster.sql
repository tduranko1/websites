
declare @UserID as int
declare @OfficeID as int
declare @InsuranceCompanyID as int

-- Create Unitrin's "Default" user for Autoverse

begin transaction

if not exists(select * from utb_user_application where logonid = 'AV_UN' and ApplicationID = 6)
begin
	-- we will add this user to the "Unitrin Business Insurance - Southern Region" office.
	-- this is the only office defined at the time of this writing

	select @InsuranceCompanyID = InsuranceCompanyID
	from utb_insurance
	where Name = 'Unitrin Business Insurance'

	select @OfficeID = OfficeID
	from utb_office
	where Name = 'Unitrin Business Insurance - Southern Region'
 	  and InsuranceCompanyID = @InsuranceCompanyID

	if (@InsuranceCompanyID is null)
	begin
		print 'Unitrin Business Insurance'
		rollback
		return
	end

	if (@OfficeID is null)
	begin
		print 'Cannot find Unitrin Business Insurance - Southern Region office for Unitrin Business Insurance'
		rollback
		return
	end

	insert into utb_user
	(OfficeID, EnabledFlag, NameFirst, NameLast, SysLastUserID)
	VALUES
	(@OfficeID, 1, 'Unitrin', 'Autoverse User', 0)
	
	if (@@error <> 0)
	begin
		print 'Unable to add Unitrin Adjuster user to utb_user'
		rollback
		return
	end

	select @UserID = UserID
	from utb_user
	where NameLast = 'Autoverse User'
	  and NameFirst = 'Unitrin'
	
	if (@UserID is null)
	begin
		print 'Unable to find the newly added Unitrin Adjuster user in utb_user'
		rollback
		return
	end

	insert into utb_user_application
	(UserID, ApplicationID, AccessBeginDate, LogonId, SysLastUserID)
	VALUES
	(@UserID, 6, CURRENT_TIMESTAMP, 'AV_UN', 0)
	
	if (@@error <> 0)
	begin
		print 'Unable to add Unitrin Adjuster user to utb_user_application'
		rollback
		return
	end
	
	insert into utb_user_role
	(UserID, RoleID, PrimaryRoleFlag, SysLastUserID)
	values
	(@UserID, 1, 1, 0)
	
	if (@@error <> 0)
	begin
		print 'Unable to add Unitrin Adjuster user to utb_user_role'
		rollback
		return
	end

	select * from utb_user where userID = @UserID
	select * from utb_user_application where userID = @UserID
	select * from utb_user_role where userID = @UserID

end

print 'Remember to commit/rollback the changes'

-- commit
-- rollback