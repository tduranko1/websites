/******************************************************************************
* Initial set of business states for all non-Autoverse clients.
* State list provided by Jim Bennett and based on the claims we received for
* each client.
******************************************************************************/
declare @InsuranceCompanyID as int
declare @StateList as varchar(1000)
declare @now as datetime

set @now = current_timestamp

begin transaction

-- Add Business states for Electric
SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Electric Insurance'

set @StateList = 'AL, AR, AZ, CA, CO, CT, DE, FL, GA, IA, ID, IL, IN, KS, KY, LA, MD, ME, MI, MN, MO, MT, NC, NE, NH, NJ, NM, NV, NY, OH, OK, OR, PA, RI, SC, SD, TN, TX, UT, VA, VT, WA, WI, WV'

insert into dbo.utb_client_business_state
SELECT @InsuranceCompanyID, value, 0, @now
from dbo.ufnUtilityParseString(@StateList, ',', 1)


-- Add Business states for Missouri Farm Bureau
SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Missouri Farm Bureau'

set @StateList = 'MO'

insert into dbo.utb_client_business_state
SELECT @InsuranceCompanyID, value, 0, @now
from dbo.ufnUtilityParseString(@StateList, ',', 1)

/****************************************************
-- Add Business states for Republic Group
****************************************************/
SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Republic Group'

set @StateList = 'LA, TX'

insert into dbo.utb_client_business_state
SELECT @InsuranceCompanyID, value, 0, @now
from dbo.ufnUtilityParseString(@StateList, ',', 1)


/****************************************************
-- Add Business states for Texas Farm Bureau
****************************************************/
SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Texas Farm Bureau'

set @StateList = 'AR, AZ, CA, CO, CT, FL, GA, IA, IL, KS, KY, LA, MO, NC, NM, NV, NY, OK, OR, TN, TX, VA, WA'

insert into dbo.utb_client_business_state
SELECT @InsuranceCompanyID, value, 0, @now
from dbo.ufnUtilityParseString(@StateList, ',', 1)

/****************************************************
-- Add Business states for Unitrin Business Insurance
****************************************************/
SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Unitrin Business Insurance'

set @StateList = 'AR, CA, LA, TX, WA'

insert into dbo.utb_client_business_state
SELECT @InsuranceCompanyID, value, 0, @now
from dbo.ufnUtilityParseString(@StateList, ',', 1)

/****************************************************
-- Add Business states for Utica National Insurance Group
****************************************************/
SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Utica National Insurance Group'

set @StateList = 'CA, CT, DE, FL, IL, MD, MS, NC, NJ, NY, PA, TN, VA'

insert into dbo.utb_client_business_state
SELECT @InsuranceCompanyID, value, 0, @now
from dbo.ufnUtilityParseString(@StateList, ',', 1)

/****************************************************
-- Add Business states for Virginia Farm Bureau
****************************************************/
SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Virginia Farm Bureau'

set @StateList = 'CO, FL, GA, KS, KY, LA, MD, NC, NJ, NY, OH, PA, SC, TN, TX, VA, WV'

insert into dbo.utb_client_business_state
SELECT @InsuranceCompanyID, value, 0, @now
from dbo.ufnUtilityParseString(@StateList, ',', 1)

/****************************************************
-- Add Business states for Western Agricultural
****************************************************/
SELECT @InsuranceCompanyID = InsuranceCompanyID
FROM utb_insurance
WHERE Name = 'Western Agricultural'

set @StateList = 'AZ, CO, IA, IN, KS, MN, NE, UT, VA, WY'

insert into dbo.utb_client_business_state
SELECT @InsuranceCompanyID, value, 0, @now
from dbo.ufnUtilityParseString(@StateList, ',', 1)

-- commit
-- rollback