-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Searches through claim_coverage and creates seperate records for each of the 
--        coverage types (Collision, Comprehensive, Liability, Underinsured, Uninsured,
--        Rental
--
----------------------------------------------------------------------------------------
-- Remember to COMMIT or ROLLBACK after the script is run.  MUST BE DONE MANUALLY!!!! --
----------------------------------------------------------------------------------------

SET NOCOUNT ON

BEGIN TRANSACTION

DECLARE @ccRecCount                     udt_std_int_big
DECLARE @ccRecNumber                    udt_std_int_big

SELECT  @ccRecCount = Count(*)
  FROM  dbo.Tmp_utb_claim_coverage

SET @ccRecNumber = 0
         
PRINT 'Converting utb_claim_coverage...Rec Count ' + Convert(varchar(10), @ccRecCount)

DECLARE @cc_CollisionCoverageFlag       udt_std_flag
DECLARE @cc_CollisionDeductibleAmt      udt_cov_deductible
DECLARE @cc_CollisionLimitAmt           udt_std_money
DECLARE @cc_ComprehensiveCoverageFlag   udt_std_flag
DECLARE @cc_ComprehensiveDeductibleAmt  udt_cov_deductible
DECLARE @cc_ComprehensiveLimitAmt       udt_std_money
DECLARE @cc_LiabilityCoverageFlag       udt_std_flag
DECLARE @cc_LiabilityDeductibleAmt      udt_cov_deductible
DECLARE @cc_LiabilityLimitAmt           udt_std_money
DECLARE @cc_LynxID                      udt_std_id
DECLARE @cc_RentalCoverageFlag          udt_std_flag
DECLARE @cc_RentalDailyLimitAmt         udt_std_money
DECLARE @cc_RentalDays                  udt_dt_day
DECLARE @cc_RentalDeductibleAmt         udt_cov_deductible
DECLARE @cc_RentalLimitAmt              udt_std_money
DECLARE @cc_SysLastUpdatedDate          udt_sys_last_updated_date
DECLARE @cc_SysLastUserID               udt_std_id
DECLARE @cc_UnderinsuredCoverageFlag    udt_std_flag
DECLARE @cc_UnderinsuredDeductibleAmt   udt_cov_deductible
DECLARE @cc_UnderinsuredLimitAmt        udt_std_money
DECLARE @cc_UninsuredCoverageFlag       udt_std_flag
DECLARE @cc_UninsuredDeductibleAmt      udt_cov_deductible
DECLARE @cc_UninsuredLimitAmt           udt_std_money


DECLARE csrCC CURSOR FOR
  SELECT cc.LynxID,
         cc.CollisionCoverageFlag,
         cc.CollisionDeductibleAmt,
         cc.CollisionLimitAmt,
         cc.ComprehensiveCoverageFlag,
         cc.ComprehensiveDeductibleAmt,
         cc.ComprehensiveLimitAmt,
         cc.LiabilityCoverageFlag,
         cc.LiabilityDeductibleAmt,
         cc.LiabilityLimitAmt,
         cc.UnderinsuredCoverageFlag,
         cc.UnderinsuredDeductibleAmt,
         cc.UnderinsuredLimitAmt,
         cc.UninsuredCoverageFlag,
         cc.UninsuredDeductibleAmt,
         cc.UninsuredLimitAmt,
         cc.RentalCoverageFlag,
         cc.RentalDeductibleAmt,
         cc.RentalLimitAmt,
         cc.RentalDailyLimitAmt,
         cc.RentalDays,
         cc.SysLastUserID,
         cc.SysLastUpdatedDate
    FROM dbo.Tmp_utb_claim_coverage cc
    ORDER BY LynxID
  
OPEN csrCC 
  
FETCH NEXT FROM csrCC
  INTO  @cc_LynxID,
        @cc_CollisionCoverageFlag,
        @cc_CollisionDeductibleAmt,
        @cc_CollisionLimitAmt,
        @cc_ComprehensiveCoverageFlag,
        @cc_ComprehensiveDeductibleAmt,
        @cc_ComprehensiveLimitAmt,
        @cc_LiabilityCoverageFlag,
        @cc_LiabilityDeductibleAmt,
        @cc_LiabilityLimitAmt,
        @cc_UnderinsuredCoverageFlag,
        @cc_UnderinsuredDeductibleAmt,
        @cc_UnderinsuredLimitAmt,
        @cc_UninsuredCoverageFlag,
        @cc_UninsuredDeductibleAmt,
        @cc_UninsuredLimitAmt,
        @cc_RentalCoverageFlag,
        @cc_RentalDeductibleAmt,
        @cc_RentalLimitAmt,
        @cc_RentalDailyLimitAmt,
        @cc_RentalDays,
        @cc_SysLastUserID,
        @cc_SysLastUpdatedDate
  
WHILE @@FETCH_STATUS = 0
BEGIN
    SET @ccRecNumber = @ccRecNumber + 1
    
--     PRINT 'LynxID = ' + Convert(varchar(10), @cc_LynxID)
    
    -- Collision
    
    IF @cc_CollisionCoverageFlag = 1 OR @cc_CollisionDeductibleAmt > 0 OR @cc_CollisionLimitAmt > 0
    BEGIN
        INSERT INTO dbo.utb_claim_coverage
        (
            LynxID,
            ClientCoverageTypeID,
            AddtlCoverageFlag,
            CoverageTypeCD,
            DeductibleAmt,
            LimitAmt,
            LimitDailyAmt,
            MaximumDays,
            SysLastUserID,
            SysLastUpdatedDate
        )
        SELECT  @cc_LynxID,
                (SELECT  TOP 1 cct.ClientCoverageTypeID 
                   FROM dbo.Tmp_utb_claim_coverage cc
				   LEFT JOIN dbo.utb_claim c ON (cc.LynxID = c.LynxID)
				   LEFT JOIN dbo.utb_client_coverage_type cct ON (c.InsuranceCompanyID = cct.InsuranceCompanyID)
                   WHERE cc.LynxID = @cc_LynxID
					 AND CoverageProfileCD = 'COLL'),
                0,
                'COLL',
                IsNull(@cc_CollisionDeductibleAmt, 0),
                IsNull(@cc_CollisionLimitAmt, 0),
                NULL,
                NULL,
                @cc_SysLastUserID,
                @cc_SysLastUpdatedDate
    END
    
    
    -- Comprehensive
    
    IF @cc_ComprehensiveCoverageFlag = 1 OR @cc_ComprehensiveDeductibleAmt > 0 OR @cc_ComprehensiveLimitAmt > 0
    BEGIN
        INSERT INTO dbo.utb_claim_coverage
        (
            LynxID,
            ClientCoverageTypeID,
            AddtlCoverageFlag,
            CoverageTypeCD,
            DeductibleAmt,
            LimitAmt,
            LimitDailyAmt,
            MaximumDays,
            SysLastUserID,
            SysLastUpdatedDate
        )
        SELECT  @cc_LynxID,
                (SELECT  TOP 1 cct.ClientCoverageTypeID 
                   FROM dbo.Tmp_utb_claim_coverage cc
				   LEFT JOIN dbo.utb_claim c ON (cc.LynxID = c.LynxID)
				   LEFT JOIN dbo.utb_client_coverage_type cct ON (c.InsuranceCompanyID = cct.InsuranceCompanyID)
                   WHERE cc.LynxID = @cc_LynxID
					 AND CoverageProfileCD = 'COMP'),
                0,
                'COMP',
                IsNull(@cc_ComprehensiveDeductibleAmt, 0),
                IsNull(@cc_ComprehensiveLimitAmt, 0),
                NULL,
                NULL,
                @cc_SysLastUserID,
                @cc_SysLastUpdatedDate
    END


    -- Liability
    
    IF @cc_LiabilityCoverageFlag = 1 OR @cc_LiabilityDeductibleAmt > 0 OR @cc_LiabilityLimitAmt > 0
    BEGIN
        INSERT INTO dbo.utb_claim_coverage
        (
            LynxID,
            ClientCoverageTypeID,
            AddtlCoverageFlag,
            CoverageTypeCD,
            DeductibleAmt,
            LimitAmt,
            LimitDailyAmt,
            MaximumDays,
            SysLastUserID,
            SysLastUpdatedDate
        )
        SELECT  @cc_LynxID,
                (SELECT  TOP 1 cct.ClientCoverageTypeID 
                   FROM dbo.Tmp_utb_claim_coverage cc
				   LEFT JOIN dbo.utb_claim c ON (cc.LynxID = c.LynxID)
				   LEFT JOIN dbo.utb_client_coverage_type cct ON (c.InsuranceCompanyID = cct.InsuranceCompanyID)
                   WHERE cc.LynxID = @cc_LynxID
					 AND CoverageProfileCD = 'LIAB'),
                0,
                'LIAB',
                IsNull(@cc_LiabilityDeductibleAmt, 0),
                IsNull(@cc_LiabilityLimitAmt, 0),
                NULL,
                NULL,
                @cc_SysLastUserID,
                @cc_SysLastUpdatedDate
    END


    -- Underinsured
    
    IF @cc_UnderinsuredCoverageFlag = 1 OR @cc_UnderinsuredDeductibleAmt > 0 OR @cc_UnderinsuredLimitAmt > 0
    BEGIN
        INSERT INTO dbo.utb_claim_coverage
        (
            LynxID,
            ClientCoverageTypeID,
            AddtlCoverageFlag,
            CoverageTypeCD,
            DeductibleAmt,
            LimitAmt,
            LimitDailyAmt,
            MaximumDays,
            SysLastUserID,
            SysLastUpdatedDate
        )
        SELECT  @cc_LynxID,
                (SELECT  TOP 1 cct.ClientCoverageTypeID 
                   FROM dbo.Tmp_utb_claim_coverage cc
				   LEFT JOIN dbo.utb_claim c ON (cc.LynxID = c.LynxID)
				   LEFT JOIN dbo.utb_client_coverage_type cct ON (c.InsuranceCompanyID = cct.InsuranceCompanyID)
                   WHERE cc.LynxID = @cc_LynxID
					 AND CoverageProfileCD = 'UIM'),
                0,
                'UIM',
                IsNull(@cc_UnderinsuredDeductibleAmt, 0),
                IsNull(@cc_UnderinsuredLimitAmt, 0),
                NULL,
                NULL,
                @cc_SysLastUserID,
                @cc_SysLastUpdatedDate
    END


    -- Uninsured
    
    IF @cc_UninsuredCoverageFlag = 1 OR @cc_UninsuredDeductibleAmt > 0 OR @cc_UninsuredLimitAmt > 0
    BEGIN
        INSERT INTO dbo.utb_claim_coverage
        (
            LynxID,
            ClientCoverageTypeID,
            AddtlCoverageFlag,
            CoverageTypeCD,
            DeductibleAmt,
            LimitAmt,
            LimitDailyAmt,
            MaximumDays,
            SysLastUserID,
            SysLastUpdatedDate
        )
        SELECT  @cc_LynxID,
                (SELECT  TOP 1 cct.ClientCoverageTypeID 
                   FROM dbo.Tmp_utb_claim_coverage cc
				   LEFT JOIN dbo.utb_claim c ON (cc.LynxID = c.LynxID)
				   LEFT JOIN dbo.utb_client_coverage_type cct ON (c.InsuranceCompanyID = cct.InsuranceCompanyID)
                   WHERE cc.LynxID = @cc_LynxID
					 AND CoverageProfileCD = 'UM'),
                0,
                'UM',
                IsNull(@cc_UninsuredDeductibleAmt, 0),
                IsNull(@cc_UninsuredLimitAmt, 0),
                NULL,
                NULL,
                @cc_SysLastUserID,
                @cc_SysLastUpdatedDate
    END


    -- Rental
    
    IF @cc_RentalCoverageFlag = 1 OR @cc_RentalDeductibleAmt > 0 OR @cc_RentalLimitAmt > 0 OR @cc_RentalDailyLimitAmt > 0 OR @cc_RentalDays > 0
    BEGIN
        INSERT INTO dbo.utb_claim_coverage
        (
            LynxID,
            ClientCoverageTypeID,
            AddtlCoverageFlag,
            CoverageTypeCD,
            DeductibleAmt,
            LimitAmt,
            LimitDailyAmt,
            MaximumDays,
            SysLastUserID,
            SysLastUpdatedDate
        )
        SELECT  @cc_LynxID,
                (SELECT  TOP 1 cct.ClientCoverageTypeID 
                   FROM dbo.Tmp_utb_claim_coverage cc
				   LEFT JOIN dbo.utb_claim c ON (cc.LynxID = c.LynxID)
				   LEFT JOIN dbo.utb_client_coverage_type cct ON (c.InsuranceCompanyID = cct.InsuranceCompanyID)
                   WHERE cc.LynxID = @cc_LynxID
					 AND CoverageProfileCD = 'RENT'),
                0,
                'RENT',
                IsNull(@cc_RentalDeductibleAmt, 0),
                IsNull(@cc_RentalLimitAmt, 0),
                IsNull(@cc_RentalDailyLimitAmt, 0),
                IsNull(@cc_RentalDays, 0),
                @cc_SysLastUserID,
                @cc_SysLastUpdatedDate
    END

    IF @ccRecNumber % 10000 = 0 PRINT '    Recs Processed:' +   + Convert(varchar(10), @ccRecNumber)

    SET @cc_LynxID = NULL
    SET @cc_CollisionCoverageFlag = NULL
    SET @cc_CollisionDeductibleAmt = NULL
    SET @cc_CollisionLimitAmt = NULL
    SET @cc_ComprehensiveCoverageFlag = NULL
    SET @cc_ComprehensiveDeductibleAmt = NULL
    SET @cc_ComprehensiveLimitAmt = NULL
    SET @cc_LiabilityCoverageFlag = NULL
    SET @cc_LiabilityDeductibleAmt = NULL
    SET @cc_LiabilityLimitAmt = NULL
    SET @cc_UnderinsuredCoverageFlag = NULL
    SET @cc_UnderinsuredDeductibleAmt = NULL
    SET @cc_UnderinsuredLimitAmt = NULL
    SET @cc_UninsuredCoverageFlag = NULL
    SET @cc_UninsuredDeductibleAmt = NULL
    SET @cc_UninsuredLimitAmt = NULL
    SET @cc_RentalCoverageFlag = NULL
    SET @cc_RentalDeductibleAmt = NULL
    SET @cc_RentalLimitAmt = NULL
    SET @cc_RentalDailyLimitAmt = NULL
    SET @cc_RentalDays = NULL
    SET @cc_SysLastUserID = NULL
    SET @cc_SysLastUpdatedDate = NULL


    FETCH NEXT FROM csrCC
      INTO  @cc_LynxID,
            @cc_CollisionCoverageFlag,
            @cc_CollisionDeductibleAmt,
            @cc_CollisionLimitAmt,
            @cc_ComprehensiveCoverageFlag,
            @cc_ComprehensiveDeductibleAmt,
            @cc_ComprehensiveLimitAmt,
            @cc_LiabilityCoverageFlag,
            @cc_LiabilityDeductibleAmt,
            @cc_LiabilityLimitAmt,
            @cc_UnderinsuredCoverageFlag,
            @cc_UnderinsuredDeductibleAmt,
            @cc_UnderinsuredLimitAmt,
            @cc_UninsuredCoverageFlag,
            @cc_UninsuredDeductibleAmt,
            @cc_UninsuredLimitAmt,
            @cc_RentalCoverageFlag,
            @cc_RentalDeductibleAmt,
            @cc_RentalLimitAmt,
            @cc_RentalDailyLimitAmt,
            @cc_RentalDays,
            @cc_SysLastUserID,
            @cc_SysLastUpdatedDate
END  

CLOSE csrCC
DEALLOCATE csrCC




--commit
--rollback


