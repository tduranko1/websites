

DECLARE @AppVariableID	INT
DECLARE @DatabaseName	VARCHAR(50)


SET @AppVariableID = (SELECT Max([AppVariableID])FROM [utb_app_variable])
SET @DatabaseName = (SELECT DB_NAME() AS DataBaseName)


-- **** DEV ****
IF (@DatabaseName = 'udb_apd_dev')
BEGIN
	--ROCINTRAD01
	IF NOT EXISTS
		(SELECT 1 FROM [utb_app_variable] 
		 WHERE name = 'SYS_STATUS'
		 AND SubName = 'ROCINTRAD01') 
	BEGIN

		SET @AppVariableID = @AppVariableID + 1
	
		INSERT INTO [dbo].[utb_app_variable]
			   ([AppVariableID]
			   ,[Description]
			   ,[Name]
			   ,[SubName]
			   ,[Value]
			   ,[SysMaintainedFlag]
			   ,[SysLastUserID]
			   ,[SysLastUpdatedDate])
		 VALUES
			   (@AppVariableID
			   ,'Status of ROCINTRAD01'
			   ,'SYS_STATUS'
			   ,'ROCINTRAD01'
			   ,'UP'
			   ,0
			   ,0
			   ,current_timestamp)
	END
END


-- **** STAGE ****
IF (@DatabaseName = 'udb_apd_stg')
BEGIN
	--PENINTRAS01
	IF NOT EXISTS
		(SELECT 1 FROM [utb_app_variable] 
		 WHERE name = 'SYS_STATUS'
		 AND SubName = 'PENINTRAS01') 
	BEGIN
		SET @AppVariableID = @AppVariableID + 1

		INSERT INTO [dbo].[utb_app_variable]
			   ([AppVariableID]
			   ,[Description]
			   ,[Name]
			   ,[SubName]
			   ,[Value]
			   ,[SysMaintainedFlag]
			   ,[SysLastUserID]
			   ,[SysLastUpdatedDate])
		 VALUES
			   (@AppVariableID
			   ,'Status of PENINTRAS01'
			   ,'SYS_STATUS'
			   ,'PENINTRAS01'
			   ,'UP'
			   ,0
			   ,0
			   ,current_timestamp)
	END

	--PENINTRAS02
	IF NOT EXISTS
		(SELECT 1 FROM [utb_app_variable] 
		 WHERE name = 'SYS_STATUS'
		 AND SubName = 'PENINTRAS02') 
	BEGIN
		SET @AppVariableID = @AppVariableID + 1

		INSERT INTO [dbo].[utb_app_variable]
			   ([AppVariableID]
			   ,[Description]
			   ,[Name]
			   ,[SubName]
			   ,[Value]
			   ,[SysMaintainedFlag]
			   ,[SysLastUserID]
			   ,[SysLastUpdatedDate])
		 VALUES
			   (@AppVariableID
			   ,'Status of PENINTRAS02'
			   ,'SYS_STATUS'
			   ,'PENINTRAS02'
			   ,'UP'
			   ,0
			   ,0
			   ,current_timestamp)
	END
END


-- **** PRODUCTION ****
IF (@DatabaseName = 'udb_apd')
BEGIN
	--ROCINTRAP01
	IF NOT EXISTS
		(SELECT 1 FROM [utb_app_variable] 
		 WHERE name = 'SYS_STATUS'
		 AND SubName = 'ROCINTRAP01') 
	BEGIN
		SET @AppVariableID = @AppVariableID + 1

		INSERT INTO [dbo].[utb_app_variable]
			   ([AppVariableID]
			   ,[Description]
			   ,[Name]
			   ,[SubName]
			   ,[Value]
			   ,[SysMaintainedFlag]
			   ,[SysLastUserID]
			   ,[SysLastUpdatedDate])
		 VALUES
			   (@AppVariableID
			   ,'Status of ROCINTRAP01'
			   ,'SYS_STATUS'
			   ,'ROCINTRAP01'
			   ,'UP'
			   ,0
			   ,0
			   ,current_timestamp)
	END

	--ROCINTRAP02
	IF NOT EXISTS
		(SELECT 1 FROM [utb_app_variable] 
		 WHERE name = 'SYS_STATUS'
		 AND SubName = 'ROCINTRAP02') 
	BEGIN
		SET @AppVariableID = @AppVariableID + 1

		INSERT INTO [dbo].[utb_app_variable]
           ([AppVariableID]
           ,[Description]
           ,[Name]
           ,[SubName]
           ,[Value]
           ,[SysMaintainedFlag]
           ,[SysLastUserID]
           ,[SysLastUpdatedDate])
		VALUES
           (@AppVariableID
           ,'Status of ROCINTRAP02'
           ,'SYS_STATUS'
           ,'ROCINTRAP02'
           ,'UP'
           ,0
           ,0
           ,current_timestamp)
	END
END

