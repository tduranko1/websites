DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 158
SET @FromInscCompID = 374

DECLARE @MsgTempID INT
DECLARE @BundlingID INT

DECLARE @EstEMSID INT
SELECT @EstEMSID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Estimate EMS'

IF NOT EXISTS (
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @FromInscCompID
			AND mt.Description LIKE '%msgELECPSAssignmentCancel.xsl%'
)
BEGIN

--------------------------------
-- Bunding Changes - AssignmentCancel
--------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%msgELECPSAssignmentCancel.xsl%'
	
	
INSERT INTO utb_bundling_document_type
	SELECT
		@BundlingID
		, bdt.DocumentTypeID
		, bdt.ConditionValue
		, bdt.DirectionalCD
		, bdt.DirectionToPayFlag
		, bdt.DuplicateFlag
		, bdt.EstimateDuplicateFlag
		, bdt.EstimateTypeCD
		, bdt.FinalEstimateFlag
		, bdt.LossState
		, bdt.MandatoryFlag
		, bdt.SelectionOrder
		, bdt.ShopState
		, bdt.VANFlag
		, bdt.WarrantyFlag
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @FromInscCompID
				AND mt.Description LIKE '%msgELPSAssignmentCancel.xsl%'
		)

--------------------------------
-- Bunding Changes - No Inspec
--------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%msgELECPSStaleAlert.xsl%'
	
	
INSERT INTO utb_bundling_document_type
	SELECT
		@BundlingID
		, bdt.DocumentTypeID
		, bdt.ConditionValue
		, bdt.DirectionalCD
		, bdt.DirectionToPayFlag
		, bdt.DuplicateFlag
		, bdt.EstimateDuplicateFlag
		, bdt.EstimateTypeCD
		, bdt.FinalEstimateFlag
		, bdt.LossState
		, bdt.MandatoryFlag
		, bdt.SelectionOrder
		, bdt.ShopState
		, bdt.VANFlag
		, bdt.WarrantyFlag
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @FromInscCompID
				AND mt.Description LIKE '%msgELPSStaleAlert.xsl%'
		)

--------------------------------
-- Bunding Changes - Cash Out
--------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%msgELECPSCashOutAlert.xsl%'
	
	
INSERT INTO utb_bundling_document_type
	SELECT
		@BundlingID
		, bdt.DocumentTypeID
		, bdt.ConditionValue
		, bdt.DirectionalCD
		, bdt.DirectionToPayFlag
		, bdt.DuplicateFlag
		, bdt.EstimateDuplicateFlag
		, bdt.EstimateTypeCD
		, bdt.FinalEstimateFlag
		, bdt.LossState
		, bdt.MandatoryFlag
		, bdt.SelectionOrder
		, bdt.ShopState
		, bdt.VANFlag
		, bdt.WarrantyFlag
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @FromInscCompID
				AND mt.Description LIKE '%msgELPSCashOutAlert.xsl%'
		)

	UPDATE utb_bundling_document_type SET EstimateTypeCD = NULL, FinalEstimateFlag = 0, MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 3
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,@EstEMSID,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  

------------------------------------------
-- Bunding Changes - Statu Update ClaimRep
------------------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%msgELECPSStatusUpdAction.xsl%'
	
INSERT INTO utb_bundling_document_type
	SELECT
		@BundlingID
		, bdt.DocumentTypeID
		, bdt.ConditionValue
		, bdt.DirectionalCD
		, bdt.DirectionToPayFlag
		, bdt.DuplicateFlag
		, bdt.EstimateDuplicateFlag
		, bdt.EstimateTypeCD
		, bdt.FinalEstimateFlag
		, bdt.LossState
		, bdt.MandatoryFlag
		, bdt.SelectionOrder
		, bdt.ShopState
		, bdt.VANFlag
		, bdt.WarrantyFlag
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @FromInscCompID
				AND mt.Description LIKE '%msgELPSStatusUpdAction.xsl%'
		)

------------------------------------------
-- Bunding Changes - Statu Update MDS
------------------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%msgELECPSStatusUpdActionMDS.xsl%'
	

INSERT INTO utb_bundling_document_type
	SELECT
		@BundlingID
		, bdt.DocumentTypeID
		, bdt.ConditionValue
		, bdt.DirectionalCD
		, bdt.DirectionToPayFlag
		, bdt.DuplicateFlag
		, bdt.EstimateDuplicateFlag
		, bdt.EstimateTypeCD
		, bdt.FinalEstimateFlag
		, bdt.LossState
		, bdt.MandatoryFlag
		, bdt.SelectionOrder
		, bdt.ShopState
		, bdt.VANFlag
		, bdt.WarrantyFlag
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @FromInscCompID
				AND mt.Description LIKE '%msgELPSTLAlert.xsl%'
		)

------------------------------------------
-- Bunding Changes - Total Loss Adj
------------------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%msgELECPSTLAlert.xsl%'
	
INSERT INTO utb_bundling_document_type
	SELECT
		@BundlingID
		, bdt.DocumentTypeID
		, bdt.ConditionValue
		, bdt.DirectionalCD
		, bdt.DirectionToPayFlag
		, bdt.DuplicateFlag
		, bdt.EstimateDuplicateFlag
		, bdt.EstimateTypeCD
		, bdt.FinalEstimateFlag
		, bdt.LossState
		, bdt.MandatoryFlag
		, bdt.SelectionOrder
		, bdt.ShopState
		, bdt.VANFlag
		, bdt.WarrantyFlag
		, 0
		, CURRENT_TIMESTAMP
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @FromInscCompID
				AND mt.Description LIKE '%msgELPSTLAlert.xsl%'
		)

	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,3,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,8,NULL,'I',0,1,0,NULL,0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  

------------------------------------------
-- Bunding Changes - Closing Total Loss
------------------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%msgPSTLClosing.xsl%'
	
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,@EstEMSID,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  
	DELETE FROM utb_bundling_document_type WHERE BundlingID = @BundlingID AND DocumentTypeID = 13

	SELECT 'Required Docs added...'
END
ELSE
BEGIN
	SELECT 'Required Docs already added...'
END
