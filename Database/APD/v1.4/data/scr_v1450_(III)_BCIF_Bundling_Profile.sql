DECLARE @DocumentTypeID_BCIF_VEF as int
DECLARE @DocumentTypeID_VEF as int
DECLARE @DocumentTypeID as int
DECLARE @MessageTemplateID as int
DECLARE @BundlingID as int
DECLARE @now as datetime

SET @now = CURRENT_TIMESTAMP

SELECT @DocumentTypeID_BCIF_VEF = DocumentTypeID
FROM utb_document_type
WHERE Name = 'BCIF - Vehicle Evaluation Form'

SELECT @DocumentTypeID_VEF = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Vehicle Evaluation Form'

IF @DocumentTypeID_BCIF_VEF IS NULL
BEGIN
    PRINT 'Missing document type: BCIF - Vehicle Evaluation Form'
    RETURN
END

IF @DocumentTypeID_VEF IS NULL
BEGIN
    PRINT 'Missing document type: Vehicle Evaluation Form'
    RETURN
END

SELECT @DocumentTypeID = DocumentTypeID
FROM utb_document_type
WHERE Name = 'Closing Documents'

IF @DocumentTypeID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Closing Documents" in utb_document_type ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @MessageTemplateID = NULL

SELECT @MessageTemplateID = MessageTemplateID
FROM utb_message_template
WHERE ServiceChannelCD = 'PS'
  AND Description like 'Total Loss Closing|%'
  AND EnabledFlag = 1

IF @MessageTemplateID IS NULL
BEGIN
    -- SQL Server Error
    RAISERROR  ('Cannot find "Status Update" in utb_message_template ', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

BEGIN TRANSACTION

/*******************************************************
* Delete Closing - Total Loss bundling from EIC, TXFB,
*  FF, VAB and WAG
*******************************************************/

PRINT 'Dereferencing existing Closing - Total Loss for EIC, TXFB, FF, VAFB and WAG'

DELETE from utb_client_bundling
WHERE InsuranceCompanyID in (158, 184, 198, 192, 178)
  AND BundlingID in (select BundlingID
						from utb_bundling
						where Name = 'Closing - Total Loss')
  AND ServiceChannelCD = 'PS'
						

/*******************************************************
* TL bundling for EIC and TXFB
*******************************************************/

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing - Total Loss', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Photos
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    8,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    1,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    0,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add BCIF - Vehicle Evaluation Form
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    @DocumentTypeID_BCIF_VEF, -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    4,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to EIC and TXFB
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID in (158, 184) -- EIC and TXFB
  
/*******************************************************
* TL bundling for FF, VAFB and WAG
*******************************************************/

INSERT INTO utb_bundling (
    DocumentTypeID,
    MessageTemplateID,
    EnabledFlag,
    Name,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @DocumentTypeID,    -- DocumentTypeID,
    @MessageTemplateID, -- MessageTemplateID
    1,                  -- EnabledFlag,
    'Closing - Total Loss', -- Name,
    0,                  -- SysLastUserID,
    @now                -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling "Closing Documents"', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

SET @BundlingID = SCOPE_IDENTITY()

-- Add document types to this profile
-- Add Invoice
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    13,             -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    1,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add estimate
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    3,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    1,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    2,              -- SelectionOrder,
    1,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Photos
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    8,              -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    1,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    0,              -- MandatoryFlag,
    3,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add Vehicle Evaluation Form
INSERT INTO utb_bundling_document_type (
    BundlingID,
    DocumentTypeID,
    DirectionalCD,
    DirectionToPayFlag,
    DuplicateFlag,
    EstimateDuplicateFlag,
    EstimateTypeCD,
    FinalEstimateFlag,
    MandatoryFlag,
    SelectionOrder,
    VANFlag,
    SysLastUserID,
    SysLastUpdatedDate
) VALUES (
    @BundlingID,    -- BundlingID,
    @DocumentTypeID_VEF, -- DocumentTypeID,
    'I',            -- DirectionalCD,
    0,              -- DirectionToPayFlag,
    0,              -- DuplicateFlag,
    0,              -- EstimateDuplicateFlag,
    null,           -- EstimateTypeCD,
    0,              -- FinalEstimateFlag,
    1,              -- MandatoryFlag,
    4,              -- SelectionOrder,
    0,              -- VANFlag,
    0,              -- SysLastUserID,
    @now            -- SysLastUpdatedDate
)

IF @@error <> 0
BEGIN
    -- SQL Server Error
    RAISERROR  ('Error inserting into utb_bundling_document_type', 16, 1)
    ROLLBACK TRANSACTION 
    RETURN
END

-- Add this bundling tasks to EIC and TXFB
INSERT INTO dbo.utb_client_bundling
(BundlingID, InsuranceCompanyID, ServiceChannelCD, SysLastUserID, SysLastUpdatedDate)
SELECT @BundlingID, i.InsuranceCompanyID, 'PS', 0, @now
FROM dbo.utb_client_service_channel csc 
left join dbo.utb_insurance i on csc.InsuranceCompanyID = i.InsuranceCompanyID
WHERE csc.ServiceChannelCD = 'PS'
  AND i.EnabledFlag = 1
  AND i.InsuranceCompanyID in (198, 192, 178) -- FF, VAFB and WAG


SELECT * FROM utb_bundling
select * from utb_client_bundling where BundlingID in (22,23)
select * from utb_bundling_document_type where BundlingID = 22
select * from utb_bundling_document_type where BundlingID = 23
  
-- COMMIT
-- ROLLBACK

