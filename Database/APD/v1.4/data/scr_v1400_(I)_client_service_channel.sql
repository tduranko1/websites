-- Conversion script to convert 1.3.7.x to 1.4.0

----------------------------------------------------------------------------------------
-- This conversion script:
--      * Populate utb_client_service_channel
--
----------------------------------------------------------------------------------------

SET NOCOUNT ON

INSERT INTO dbo.utb_client_service_channel
SELECT DISTINCT cat.InsuranceCompanyID,
       at.ServiceChannelDefaultCD,
       cat.ClientAuthorizesPaymentFlag,
       cat.InvoicingModelBillingCD, 
       0, 
       CURRENT_TIMESTAMP
  FROM dbo.tmp_utb_client_assignment_type cat
 INNER JOIN dbo.utb_assignment_type at ON cat.AssignmentTypeID = at.AssignmentTypeID


INSERT INTO [dbo].[utb_client_service_channel]
           ([InsuranceCompanyID]
           ,[ServiceChannelCD]
           ,[ClientAuthorizesPaymentFlag]
           ,[InvoicingModelBillingCD]
           ,[SysLastUserID]
           ,[SysLastUpdatedDate])
     VALUES
           (185
           ,'PS'
           ,0
           ,'B'
           ,0
           ,CURRENT_TIMESTAMP)

INSERT INTO [dbo].[utb_client_service_channel]
           ([InsuranceCompanyID]
           ,[ServiceChannelCD]
           ,[ClientAuthorizesPaymentFlag]
           ,[InvoicingModelBillingCD]
           ,[SysLastUserID]
           ,[SysLastUpdatedDate])
     VALUES
           (185
           ,'GL'
           ,0
           ,'B'
           ,0
           ,CURRENT_TIMESTAMP)

