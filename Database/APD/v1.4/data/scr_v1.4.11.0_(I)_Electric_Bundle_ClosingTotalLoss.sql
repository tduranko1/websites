DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 158
SET @FromInscCompID = 277

DECLARE @MsgTempID INT
DECLARE @BundlingID INT
DECLARE @EstEMSID INT
SELECT @EstEMSID = DocumentTypeID FROM utb_document_type WHERE [Name] = 'Estimate EMS'

--------------------------------
-- Bunding Changes - Closing TL
--------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%Total Loss Closing|Messages/msgPSTLClosing.xsl%'

IF NOT EXISTS (
	SELECT 
		* 
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @ToInscCompID
		)
		AND bdt.DocumentTypeID = @EstEMSID
)
BEGIN
	UPDATE utb_bundling_document_type SET SelectionOrder = 0, MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 3
	UPDATE utb_bundling_document_type SET SelectionOrder = 3, MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 8
	UPDATE utb_bundling_document_type SET SelectionOrder = 5, MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 41
	DELETE utb_bundling_document_type WHERE BundlingID = @BundlingID AND DocumentTypeID = 13
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,@EstEMSID,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  

	SELECT 'Closing Total Loss Updated...'
END
ELSE
BEGIN
	SELECT 'Closing Total Loss ALREADY Updated...'
END		
		
----------------------------------------------
-- Bunding Changes - Closing Repair Complete
----------------------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%Closing Repair Complete|Messages/msgPSClosingRepairComplete.xsl%'

	UPDATE utb_bundling_document_type SET SelectionOrder = 0,MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 13
	UPDATE utb_bundling_document_type SET SelectionOrder = 1,MandatoryFlag = 0 WHERE BundlingID = @BundlingID AND DocumentTypeID = 9
	UPDATE utb_bundling_document_type SET SelectionOrder = 2,MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 51
	UPDATE utb_bundling_document_type SET SelectionOrder = 5,MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 8
	UPDATE utb_bundling_document_type SET SelectionOrder = 4,MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 3
	--INSERT INTO utb_bundling_document_type VALUES (@BundlingID,2,NULL,'I',1,0,0,NULL,0,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  
	--INSERT INTO utb_bundling_document_type VALUES (@BundlingID,@EstEMSID,NULL,'I',0,0,0,NULL,0,NULL,0,6,NULL,1,0,0, CURRENT_TIMESTAMP)  

	SELECT 'Closing Repair Complete Updated...'
		
----------------------------------------------
-- Bunding Changes - Closing Supplement
----------------------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%Closing Supplement|Messages/msgPSClosingSupplement.xsl%'

IF NOT EXISTS (
	SELECT 
		* 
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @ToInscCompID
				AND b.BundlingID = @BundlingID
		)
		AND bdt.DocumentTypeID = @EstEMSID
)
BEGIN

	UPDATE utb_bundling_document_type SET SelectionOrder = 0,MandatoryFlag = 1 WHERE BundlingID = @BundlingID AND DocumentTypeID = 13
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,@EstEMSID,NULL,'I',0,0,0,NULL,0,NULL,1,3,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,8,NULL,'I',0,1,0,NULL,0,NULL,1,4,NULL,0,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,10,NULL,'I',0,0,0,'A',0,NULL,1,2,NULL,0,0,0, CURRENT_TIMESTAMP)  
	DELETE utb_bundling_document_type WHERE BundlingID = @BundlingID AND DocumentTypeID = 3
	
	SELECT 'Closing Supplement Updated...'
END
ELSE
BEGIN
	SELECT 'Closing Total Loss ALREADY Updated...'
END		

----------------------------------------------
-- Bunding Changes - Initial Estimate
----------------------------------------------
SET @MsgTempID = NULL
SET @BundlingID = NULL

SELECT 
	@MsgTempID=t.MessageTemplateID
	, @BundlingID=b.BundlingID
FROM
	utb_bundling b
	INNER JOIN utb_client_bundling cb 
		ON b.BundlingId = cb.BundlingID
	INNER JOIN utb_message_template t 
		ON t.MessageTemplateID = b.MessageTemplateID
WHERE
	cb.InsuranceCompanyID = @ToInscCompID
	AND t.Description LIKE '%Initial Estimate|Messages/msgPSInitialEstimate.xsl%'

IF NOT EXISTS (
	SELECT 
		* 
	FROM 
		utb_bundling_document_type bdt
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = bdt.DocumentTypeID 
	WHERE 	
		bdt.BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @ToInscCompID
				AND b.BundlingID = @BundlingID
		)
		AND bdt.DocumentTypeID = @EstEMSID
)
BEGIN

	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,@EstEMSID,NULL,'I',0,0,0,NULL,0,NULL,1,2,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,3,NULL,'I',0,0,0,NULL,0,NULL,1,1,NULL,1,0,0, CURRENT_TIMESTAMP)  
	INSERT INTO utb_bundling_document_type VALUES (@BundlingID,8,NULL,'I',0,1,0,NULL,0,NULL,1,3,NULL,0,0,0, CURRENT_TIMESTAMP)  
	
	SELECT 'Initial Estimate Updated...'
END
ELSE
BEGIN
	SELECT 'Initial Estimate ALREADY Updated...'
END		
