--select * from utb_app_variable
DECLARE @iAppVariableID INT
------------------------------
-- Add APDNet Debugging
------------------------------
IF NOT EXISTS (SELECT * FROM utb_app_variable WHERE [Name] = 'APDNet_Debugging' )
BEGIN
	SELECT @iAppVariableID=MAX(AppVariableID)+1 FROM utb_app_variable
	INSERT INTO utb_app_variable VALUES (@iAppVariableID,'This turns on/off debugging code in the APD.net',null,null,'APDNet_Debugging',null,'True',0,0,CURRENT_TIMESTAMP) 

	SELECT 'APDNet Debugging added...'
END
ELSE
BEGIN
	SELECT 'APDNet Debugging already added...'
END

SELECT * FROM utb_app_variable WHERE [Name] = 'APDNet_Debugging' 
