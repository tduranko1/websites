DECLARE @ToInscCompID SMALLINT
DECLARE @FromInscCompID SMALLINT
SET @ToInscCompID = 192
SET @FromInscCompID = 387

------------------------------
-- Add Bundling Document Type
------------------------------
IF NOT EXISTS (
SELECT 
	* 
FROM 
	utb_bundling_document_type 
WHERE 	
	BundlingID IN
	(
		SELECT 
			b.BundlingID 
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @FromInscCompID
			AND	mt.[Description] LIKE '%msgVFBPSStatusUpdAction.xsl%'
	)
)
BEGIN
	INSERT INTO 
		utb_bundling_document_type
	SELECT
		(SELECT 
			b.BundlingID 
		FROM 
			utb_bundling b 
			INNER JOIN utb_message_template mt
			ON mt.MessageTemplateID = b.MessageTemplateID
		WHERE 
			mt.[Description] LIKE '%msgVFBPSStatusUpdAction.xsl%'
		)	 
		, DocumentTypeID
		, ConditionValue
		, DirectionalCD
		, DirectionToPayFlag
		, DuplicateFlag
		, EstimateDuplicateFlag
		, EstimateTypeCD
		, FinalEstimateFlag
		, LossState
		, MandatoryFlag
		, SelectionOrder
		, ShopState
		, VANFlag
		, WarrantyFlag
		, 0
		, CURRENT_TIMESTAMP 
	FROM 
		utb_bundling_document_type 
	WHERE 	
		BundlingID IN
		(
			SELECT 
				b.BundlingID 
			FROM 
				utb_message_template mt 
				INNER JOIN utb_bundling b 
				ON b.MessageTemplateID = mt.MessageTemplateID 
				INNER JOIN utb_client_bundling cb
				ON cb.BundlingID = b.BundlingID 
			WHERE 
				cb.InsuranceCompanyID = @FromInscCompID
				AND mt.[Description] LIKE '%msgWFPSStatusUpdAction.xsl%'
		)

	SELECT 'Bundling Document Type added...'
END
ELSE
BEGIN
	SELECT 'Bundling Document Type already added...'
END
