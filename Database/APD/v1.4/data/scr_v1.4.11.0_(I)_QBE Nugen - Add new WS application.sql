-- Add new WSAPD Application to utb_application
IF NOT EXIST (SELECT * FROM utb_application WHERE Code = 'WS')
BEGIN
	INSERT INTO utb_application 
	VALUES 
	(
		(SELECT MAX(ApplicationID) + 1 FROM utb_application)
		, 'WS'
		, (SELECT MAX(DisplayOrder) + 1 FROM utb_application)
		,1
		,'Web Service'
		,1
		,0
		,CURRENT_TIMESTAMP
	)
	
	SELECT 'New WS code added to utb_application...'
END
ELSE
BEGIN
	SELECT 'WS code already exists...'
END
