--select * from utb_event order by Name
IF NOT EXISTS (SELECT EventID FROM utb_event WHERE Name = 'Reinspection Report Uploaded')
BEGIN
	INSERT INTO utb_event VALUES (90,9,38,1,1,'Reinspection Report Uploaded',0,CURRENT_TIMESTAMP)
END
ELSE
BEGIN
	SELECT 'Event already added...'
END

--select * from utb_workflow
IF NOT EXISTS (SELECT WorkflowID FROM utb_workflow WHERE WorkflowID = 190)
BEGIN
	INSERT INTO utb_workflow VALUES (190,184,0,90,'E',0,CURRENT_TIMESTAMP)
END
ELSE
BEGIN
	SELECT 'Workflow already added...'
END

--select * from utb_spawn
IF NOT EXISTS (SELECT SpawnID FROM utb_spawn WHERE SpawnID = 299)
BEGIN
	INSERT INTO utb_spawn VALUES (190,'PS','%PertainsToName%',NULL,1,41,NULL,'T',0,CURRENT_TIMESTAMP)
END
ELSE
BEGIN
	SELECT 'Spawn already added...'
END

--select * from utb_task order by name
IF EXISTS (SELECT TaskID FROM utb_task WHERE TaskID = 41 AND Name = 'Reinspection Review Required')
BEGIN
	UPDATE utb_task SET DisplayOrder = 119, SysLastUpdatedDate = CURRENT_TIMESTAMP WHERE TaskID = 41 AND Name = 'Reinspection Review Required'
END
ELSE
BEGIN
	SELECT 'Task already added...'
END
