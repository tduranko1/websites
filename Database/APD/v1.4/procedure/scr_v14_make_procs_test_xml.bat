@ECHO OFF
REM !!!!! SET THE -S isql parameter to the SQL Server to be updated !!!!!
REM !!!!! SET THE -d isql parameter to the database to be updated !!!!!

set ssuser=jstein
set sspwd=p1r2t3s
set ssdir=\\sftmprimary\sys\Apps\Developers\VSS2
D:
CD \Database\SQLServer\SQL2K\development\xml
erase *.sql /S /F /Q
"C:\Program Files\Microsoft Visual Studio\Common\VSS\win32\SS.EXE" Get $/Database/apd/v1.4/procedure/xml/*.sql -W -R -I-
CD XML
for %%I IN (*.sql) do "C:\Program Files\Microsoft SQL Server\80\Tools\Binn\isql" -E -S SFTMAPDDEVDB001\SQL -i %%I -n -d udb_apd_test >> @@make_procs_output.txt
pause