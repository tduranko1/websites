-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptShopProgramDelta' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptShopProgramDelta 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptShopProgramDelta
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptShopProgramDelta
    @RoutingCD  udt_std_cd,
	@InsuranceCompanyIDs udt_std_desc_mid
AS
BEGIN
   
    declare @now as datetime
    declare @max_deltadate as datetime

    SET @now = CURRENT_TIMESTAMP
    
    declare @InsCoList table (InsuranceCompanyID	 int)

	insert into @InsCoList
	select value from dbo.ufnUtilityParseString(@InsuranceCompanyIDs,';',1)

    -- Check if we have data ready from the previous day. 
    SELECT @max_deltadate = max(DeltaDate)
    FROM dbo.utb_shop_location_program_delta

    IF @max_deltadate IS NOT NULL
    BEGIN
        IF DATEDIFF(hour, @max_deltadate, @now) > 24 
        BEGIN
            raiserror ('Shop Delta is older than 24 hours. Need to regenerate the data. Run uspGenerateShopProgramDelta proc.',16,1)
            return
        END
    END  

    declare @ReportData table (ShopLocationID         bigint,
                               InsuranceCompanyID     smallint,
                               ActionCD               varchar(4))


    -- Code added. Get the canned data from dbo.utb_shop_location_program_delta.
    insert into @ReportData
    select  slpd.ShopLocationID,
            slpd.InsuranceCompanyID,
            case 
                when ActionCD = 'A' then 'ADD'
                when ActionCD = 'D' then 'REMV'
            end
    from dbo.utb_insurance i
		inner join @InsCoList icl on icl.InsuranceCompanyID = i.InsuranceCompanyID
		left join dbo.utb_shop_location_program_delta slpd on i.InsuranceCompanyID = slpd.InsuranceCompanyID
    where i.ReturnDocRoutingCD = @RoutingCD	  
      and slpd.ActionCD in ('A', 'D')
    select i.Name [Insurance Company Name],
       case r.ActionCD when 'ADD' then 'Add' else 'Remove' end [Action],
       sl.ShopLocationID [Lynx Shop ID],
       sl.AutoverseID [Autoverse Shop ID],
       sl.Name [Shop Name],
       sl.Address1 [Shop Address],
       sl.AddressCity [City],
       sl.AddressState [State],
       sl.AddressZip [Zip],
       '(' + sl.PhoneAreaCode + ')' + sl.PhoneExchangeNumber + '-' + sl.PhoneUnitNumber [Phone Number],
       '(' + sl.FaxAreaCode + ')' + sl.FaxExchangeNumber + '-' + sl.FaxUnitNumber [Fax Number]
    from @ReportData r inner join utb_shop_location sl on sl.ShopLocationID = r.ShopLocationID
                       inner join utb_insurance i on i.InsuranceCompanyID = r.InsuranceCompanyID
    order by i.Name, r.ActionCD                                                    
                        
   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptShopProgramDelta' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptShopProgramDelta TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/