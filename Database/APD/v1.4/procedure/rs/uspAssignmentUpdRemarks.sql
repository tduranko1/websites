-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAssignmentUpdRemarks' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAssignmentUpdRemarks 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAssignmentUpdRemarks
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Updates Assignment information in the assignment and the claim aspect service channel tables
*
* PARAMETERS:  
* (I) @AssignmentID
* (I) @AssignmentRemarks            Remarks to shop
* (I) @EffectiveDeductibleSentAmt   Effective Deductible sent amount
* (I) @AppraiserInvoiceDate         Appraiser Invoiced date
* (I) @CashOutDate                  Cash Out date
* (I) @DispositionTypeCD            Disposition Type code
* (I) @InspectionDate               Inspection date
* (I) @WorkEndDate                  Work End date
* (I) @WorkStartDate                Work Start date
* (I) @RepairScheduleChangeReasonID The Repair Schedule change reason ID
* (I) @RepairScheduleChangeReason   The Repair Schedule change reason
* (I) @UserID                       The user updating the record
* (I) @SysLastUpdatedDate           The "previous" updated date
*
* RESULT SET:   An XML document containing the new SysLastUpdatedDate value        
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAssignmentUpdRemarks
    @AssignmentID                 udt_std_id_big,
    @AssignmentRemarks            udt_std_desc_xlong,
    @EffectiveDeductibleSentAmt   decimal(9,2) = NULL,
    @AppraiserInvoiceDate         varchar(30) = NULL,
    @CashOutDate                  varchar(30) = NULL,
    @DispositionTypeCD            varchar(5) = NULL,
    @InspectionDate               varchar(30) = NULL,
    @WorkEndDate                  varchar(30) = NULL,
    @WorkStartDate                varchar(30) = NULL,
    @RepairLocationCity           varchar(75) = NULL,
    @RepairLocationCounty         varchar(75) = NULL,
    @RepairLocationState          varchar(2) = NULL,
    @RepairScheduleChangeReasonID int = NULL,
    @RepairScheduleChangeReason   udt_std_desc_mid = NULL,
    @SourceApplicationPassThruData  udt_std_desc_huge = NULL,
    @UserID                       udt_std_id,
    @SysLastUpdatedDate           varchar(30)
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@AssignmentRemarks))) = 0 SET @AssignmentRemarks = NULL
    IF LEN(LTRIM(RTRIM(@SourceApplicationPassThruData))) = 0 SET @SourceApplicationPassThruData = NULL


    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @KeyValue          AS varchar(40)
    DECLARE @now               AS datetime 
    DECLARE @ClaimAspectServiceChannelID  AS bigint
    DECLARE @ClaimAspectID      as bigint
    DECLARE @ServiceChannelCD  AS varchar(5)
    DECLARE @WorkStartDateOld  AS datetime
    DECLARE @WorkEndDateOld    AS datetime
    DECLARE @ClaimIntakeFinishDate  AS datetime
    DECLARE @EntityCreatedDate      AS datetime
    DECLARE @EntityCreatedDateStr   AS varchar(10)
    DECLARE @WorkEndDateOriginal    AS datetime
    DECLARE @LossDate               AS datetime    
    DECLARE @OldInspectionDate      AS datetime
    DECLARE @OldWorkStartDate     AS datetime
    DECLARE @OldWorkEndDate       AS datetime
    DECLARE @EventID                AS udt_std_int
    DECLARE @HistoryDescription     AS udt_std_desc_long
    DECLARE @PertainsToName         AS varchar(50)
    DECLARE @EventName              AS varchar(50)
    DECLARE @NoteTypeIDClaim        AS udt_std_id
    DECLARE @WorkStartConfirmed     AS bit
    DECLARE @EntityStatusID         AS udt_std_id
    DECLARE @ReasonID               AS udt_std_id
    DECLARE @CheckListID            AS udt_std_id_big
    DECLARE @AssignmentTypeID       AS udt_std_int


    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspAssignmentUpdRemarks'


    -- Set Database options
    
    SET NOCOUNT ON
    

    -- Check to make sure a valid Lynx ID and Vehicle Number was passed in

    IF  (@AssignmentID IS NULL) OR
        (NOT EXISTS(SELECT AssignmentID FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@AssignmentID|%u', 16, 1, @ProcName, @AssignmentID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    -- Get the claim aspect service channel id for the assignment
    SELECT @ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID,
           @ClaimAspectID = ca.ClaimAspectID,
           @ServiceChannelCD = casc.ServiceChannelCD,
           @WorkStartDateOld = casc.WorkStartDate,
           @WorkEndDateOld = casc.WorkEndDate,
           @ClaimIntakeFinishDate = c.IntakeFinishDate,
           @EntityCreatedDate = ca.CreatedDate,
           @EntityCreatedDateStr = CONVERT(varchar, ca.CreatedDate, 101),
           @WorkEndDateOriginal = casc.WorkEndDateOriginal,
           @LossDate = c.LossDate,
           @AssignmentTypeID = InitialAssignmentTypeID,
           @OldInspectionDate = casc.InspectionDate
    FROM dbo.utb_assignment a
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEFT JOIN dbo.utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
    WHERE AssignmentID = @AssignmentID
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
    IF @ClaimAspectServiceChannelID IS NULL
    BEGIN
        -- Could not determine the ClaimAspectServiceChannelID from the assignment record.
        RAISERROR('101|%s|@ClaimAspectServiceChannelID|Could not determine the ClaimAspectServiceChannelID for AssignmentID: %u', 16, 1, @ProcName, @AssignmentID)
        RETURN
    END
    
    IF @WorkStartDate IS NOT NULL
    BEGIN
        IF DATEDIFF(day, CONVERT(datetime, CONVERT(varchar, @WorkStartDate, 101) + ' 23:59:59'),  @ClaimIntakeFinishDate) > 7
        BEGIN
            -- Repair start date cannot be before claim start date
            
            RAISERROR('1|Repair start date cannot be more than 7 days before the claim start date.', 16, 1)
            RETURN
        END

        IF DATEDIFF(day, CONVERT(datetime, CONVERT(varchar, @WorkStartDate, 101) + ' 23:59:59'),  @EntityCreatedDate) > 7
        BEGIN
            -- Repair start date cannot be before vehicle creation date
            
            RAISERROR('1|Repair start date cannot be more than 7 days before the vehicle created date %s.', 16, 1, @EntityCreatedDateStr)
            RETURN
        END
    END

    IF @WorkEndDate IS NOT NULL
    BEGIN
        IF CONVERT(datetime, CONVERT(varchar, @WorkEndDate, 101) + ' 23:59:59') < @ClaimIntakeFinishDate
        BEGIN
            -- Repair end date cannot be before claim start date
            
            RAISERROR('1|Repair end date cannot be before the claim start date.', 16, 1)
            RETURN
        END

        IF CONVERT(datetime, CONVERT(varchar, @WorkEndDate, 101) + ' 23:59:59') < @EntityCreatedDate
        BEGIN
            -- Repair end date cannot be before vehicle creation date
            
            RAISERROR('1|Repair end date cannot be before the vehicle created date %s.', 16, 1, @EntityCreatedDateStr)
            RETURN
        END

        IF CONVERT(datetime, @WorkEndDate) < CONVERT(datetime, @WorkStartDate)
        BEGIN
            -- Repair end date cannot be before claim start date
            
            RAISERROR('1|Repair end date cannot be before the repair start date.', 16, 1)
            RETURN
        END
    END

    IF @InspectionDate IS NOT NULL
    BEGIN
        IF CONVERT(datetime, CONVERT(varchar, @InspectionDate, 101) + ' 23:59:59') < @LossDate
        BEGIN
            -- Inspection date cannot be before the loss date
            
            RAISERROR('1|Inspection date cannot be before the claim loss date.', 16, 1)
            RETURN
        END
    END
        
    IF @AppraiserInvoiceDate IS NOT NULL
    BEGIN
        IF CONVERT(datetime, CONVERT(varchar, @AppraiserInvoiceDate, 101) + ' 23:59:59') < @ClaimIntakeFinishDate
        BEGIN
            -- Appraiser Invoice date cannot be before claim start date
            
            RAISERROR('1|Appraiser Invoice date cannot be before the claim start date.', 16, 1)
            RETURN
        END

        IF CONVERT(datetime, CONVERT(varchar, @AppraiserInvoiceDate, 101) + ' 23:59:59') < @EntityCreatedDate
        BEGIN
            -- Appraiser Invoice date cannot be before vehicle creation date
            
            RAISERROR('1|Appraiser Invoice date cannot be before the vehicle created date %s.', 16, 1, @EntityCreatedDateStr)
            RETURN
        END
    END

    IF @CashOutDate IS NOT NULL
    BEGIN
        IF CONVERT(datetime, CONVERT(varchar, @CashOutDate, 101) + ' 23:59:59') < @ClaimIntakeFinishDate
        BEGIN
            -- Cash Out date cannot be before claim start date
            
            RAISERROR('1|Cash Out date cannot be before the claim start date.', 16, 1)
            RETURN
        END

        IF CONVERT(datetime, CONVERT(varchar, @CashOutDate, 101) + ' 23:59:59') < @EntityCreatedDate
        BEGIN
            -- Cash Out date cannot be before vehicle creation date
            
            RAISERROR('1|Cash Out date cannot be before the vehicle created date %s.', 16, 1, @EntityCreatedDateStr)
            RETURN
        END
    END
    
    IF @LossDate IS NOT NULL
    BEGIN
        IF CONVERT(datetime, @WorkStartDate + ' 23:59:59') < CONVERT(datetime, @LossDate)
        BEGIN
            -- Repair start date cannot be before loss date
            
            RAISERROR('1|Repair start date cannot be before the loss date.', 16, 1)
            RETURN
        END

        IF CONVERT(datetime, @WorkEndDate + ' 23:59:59') < CONVERT(datetime, @LossDate)
        BEGIN
            -- Repair end date cannot be before loss date
            
            RAISERROR('1|Repair end date cannot be before the loss date.', 16, 1)
            RETURN
        END

        IF CONVERT(datetime, @InspectionDate + ' 23:59:59') < CONVERT(datetime, @LossDate)
        BEGIN
            -- Inspection date cannot be before loss date
            
            RAISERROR('1|Inspection date cannot be before the loss date.', 16, 1)
            RETURN
        END

        IF CONVERT(datetime, @AppraiserInvoiceDate + ' 23:59:59') < CONVERT(datetime, @LossDate)
        BEGIN
            -- Appraiser Invoice date cannot be before loss date
            
            RAISERROR('1|Appraiser Invoice date cannot be before the loss date.', 16, 1)
            RETURN
        END

        IF CONVERT(datetime, @CashOutDate + ' 23:59:59') < CONVERT(datetime, @LossDate)
        BEGIN
            -- Cash Out date cannot be before loss date
            
            RAISERROR('1|Cash Out date cannot be before the loss date.', 16, 1)
            RETURN
        END
      
    END
    
    IF @AssignmentTypeID = 15
    BEGIN
        IF @SourceApplicationPassThruData IS NULL
        BEGIN
            -- Shop information is required
            
            RAISERROR('1|Pursuit shop information is required.', 16, 1)
            RETURN
        END
        
        IF @InspectionDate IS NULL OR
            ISDATE(@InspectionDate) = 0
        BEGIN
            -- Inspection Date is required
            
            RAISERROR('1|Inspection Date is required.', 16, 1)
            RETURN
        END
    END

    SELECT @OldInspectionDate = InspectionDate,
           @OldWorkStartDate = WorkStartDate,
           @OldWorkEndDate   = WorkEndDate,
           @WorkStartConfirmed = WorkStartConfirmFlag
    FROM dbo.utb_Claim_Aspect_Service_Channel
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    -- if repair start date was changed make sure a reason was submitted
    IF (@OldWorkStartDate IS NOT NULL AND @OldWorkStartDate <> @WorkStartDate) OR 
       (@OldWorkEndDate IS NOT NULL AND @OldWorkEndDate <> @WorkEndDate)
    BEGIN
        IF @RepairScheduleChangeReason IS NULL
        BEGIN
            -- Invalid Repair start date change reason
            
            RAISERROR('1|No reason was provided for repair schedule change', 16, 1)
            RETURN
        END
    END

    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

   
    -- Validate the updated date parameter

    SET @KeyValue = Convert(varchar(20), @AssignmentID)

    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_assignment', @KeyValue

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END


    -- Begin Update

    BEGIN TRANSACTION VehicleShopUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update assignment

    UPDATE dbo.utb_assignment
    SET AssignmentRemarks           = @AssignmentRemarks,
        EffectiveDeductibleSentAmt  = @EffectiveDeductibleSentAmt,
        SysLastUserID               = @UserID,
        SysLastUpdatedDate          = @now
    WHERE AssignmentID = @AssignmentID
    

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT


    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_assignment', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    -- Update claim aspect service channel
    
    UPDATE dbo.utb_claim_aspect_service_channel
    SET AppraiserInvoiceDate  = @AppraiserInvoiceDate,
        CashOutDate           = @CashOutDate,
        DispositionTypeCD     = @DispositionTypeCD,
        InspectionDate        = @InspectionDate,
        InspectionDateOriginal = @OldInspectionDate,
        RepairLocationCity    = @RepairLocationCity,
        RepairLocationCounty  = @RepairLocationCounty,
        RepairLocationState   = @RepairLocationState,
        WorkEndDate           = @WorkEndDate,
        WorkStartDate         = @WorkStartDate,
        SysLastUserID         = @UserID,
        SysLastUpdatedDate    = @now
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
    
    IF @AssignmentTypeID = 15
    BEGIN
        UPDATE dbo.utb_claim_aspect
        SET SourceApplicationPassThruData = @SourceApplicationPassThruData
        WHERE ClaimAspectID = @ClaimAspectID
    END

    -- Check error value
    
    IF @@error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim_aspect_service_channel', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    
   SELECT @PertainsToName = cat.Name + ' ' + convert(varchar, ClaimAspectNumber)
   FROM dbo.utb_claim_aspect_service_channel casc
   LEFT JOIN dbo.utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
   LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.claimAspectTypeID = cat.ClaimAspectTypeID)
   WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
   
    -- Check the Inspection Date
    IF @OldInspectionDate IS NULL AND @InspectionDate IS NOT NULL AND @WorkStartConfirmed = 0
    BEGIN
        -- Inspection Date was entered. Throw the Inspection Scheduled event.
        
        SET @EventName ='Inspection Scheduled' 
        
        SELECT @EventID = EventID
        FROM dbo.utb_event
        WHERE Name = @EventName
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              

        IF @EventID IS NULL
        BEGIN
            -- Event Name not found
        
            RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        SET @HistoryDescription = @EventName + ' for ' + @PertainsToName + '. Scheduled for:' + convert(varchar, @InspectionDate, 101)
        
        -- Make the workflow call
        
        EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                                    @Description = @HistoryDescription,
                                    @UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- APD Workflow Notification failed

            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- reset the id so that we can safely reuse the variable
        SET @EventID = NULL
    END
    
    -- if the repair date was entered and the old value was a null then it is a fresh entry
    IF (@OldWorkStartDate IS NULL AND @WorkStartDate IS NOT NULL) OR 
       (@OldWorkEndDate IS NULL AND @WorkEndDate IS NOT NULL)
    BEGIN
        -- Prepare to throw Repair Scheduled event
        
        SET @EventName ='Repair Scheduled' 
        
        SELECT @EventID = EventID
        FROM dbo.utb_event
        WHERE Name = @EventName
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              

        IF @EventID IS NULL
        BEGIN
            -- Event Name not found
        
            RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        SET @HistoryDescription = @EventName + ' for ' + @PertainsToName + '.'
        
        IF @WorkStartDate IS NOT NULL
        BEGIN
            SET @HistoryDescription = @HistoryDescription + ' Repair Start Date:' + convert(varchar, @WorkStartDate, 101)
        END

        IF @WorkEndDate IS NOT NULL
        BEGIN
            SET @HistoryDescription = @HistoryDescription + ' Repair End Date:' + convert(varchar, @WorkEndDate, 101)
        END
        
        -- Make the workflow call
        
        EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                                    @Description = @HistoryDescription,
                                    @UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- APD Workflow Notification failed

            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- reset the id so that we can safely reuse the variable
        SET @EventID = NULL
        
    END
    
    -- Change in repair schedule
    
    IF (@OldWorkStartDate IS NOT NULL AND @OldWorkStartDate <> @WorkStartDate) OR
       (@OldWorkEndDate IS NOT NULL AND @OldWorkEndDate <> @WorkEndDate)
    BEGIN
        -- Prepare to throw Repair Scheduled Changed event
        
        SET @EventName ='Repair Schedule Changed' 
        
        SELECT @EventID = EventID
        FROM dbo.utb_event
        WHERE Name = @EventName
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              

        IF @EventID IS NULL
        BEGIN
            -- Event Name not found
        
            RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- Get Note Type ID for Claim
        SELECT  @NoteTypeIDClaim = NoteTypeID
          FROM  dbo.utb_note_type
          WHERE Name = 'Claim'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        IF @NoteTypeIDClaim IS NULL
        BEGIN
           -- Note Type ID for Claim

            RAISERROR('102|%s|"Note Type: Claim"|utb_note_type', 16, 1, @ProcName)
            RETURN
        END
        
        -- Get the Entity status id
        SELECT @EntityStatusID = StatusID
        FROM dbo.utb_claim_aspect_service_channel casc
        LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
        LEFT JOIN dbo.utb_claim_aspect_Status cas ON cas.ClaimAspectID = ca.ClaimAspectID
        WHERE cas.ServiceChannelCD = casc.ServiceChannelCD
          AND cas.StatusTypeCD = 'SC'
          AND casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
        
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        IF @OldWorkStartDate <> @WorkStartDate AND
           @OldWorkEndDate <> @WorkEndDate
        BEGIN
            SET @HistoryDescription = @PertainsToName + ' Repair start date ' + ' changed to ' +
                                   convert(varchar, @WorkStartDate, 101) + ' and ' +
                                   'Repair end date changed to ' + convert(varchar, @WorkEndDate, 101) + '. Reason: ' + @RepairScheduleChangeReason
        END
        ELSE IF @OldWorkStartDate <> @WorkStartDate
        BEGIN
            SET @HistoryDescription = @PertainsToName + ' Repair start date ' + ' changed to ' +
                                   convert(varchar, @WorkStartDate, 101) + '. Reason: ' +  @RepairScheduleChangeReason
        END
        ELSE IF @OldWorkEndDate <> @WorkEndDate
        BEGIN
            SET @HistoryDescription = @PertainsToName + ' Repair end date changed to ' + convert(varchar, @WorkEndDate, 101) + '. Reason: ' + @RepairScheduleChangeReason
        END
        
        -- Make the workflow call
        
        EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                                    @Description = @HistoryDescription,
                                    @UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- APD Workflow Notification failed

            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- Record the change to the repair schedule history table
        IF @RepairScheduleChangeReason IS NOT NULL
        BEGIN
            -- Get the Reason id for the text
            SELECT @ReasonID = ReasonID
            FROM dbo.utb_service_channel_schedule_change_reason cr
            LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON cr.ServiceChannelCD = casc.ServiceChannelCD
            WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
              AND cr.Name = @RepairScheduleChangeReason

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('99|%s', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        
            IF @ReasonID IS NULL
            BEGIN
                -- The reason text could not be found in the reference. Consider this as an "Other"
            
                -- Get the "Other" reason id
                SELECT @ReasonID = ReasonID
                FROM dbo.utb_service_channel_schedule_change_reason cr
                LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON cr.ServiceChannelCD = casc.ServiceChannelCD
                WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                  AND cr.Name = 'Other'

                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION
                    RETURN
                END              

                IF @ReasonID IS NULL
                BEGIN
                    -- "Other" reason not found
        
                    RAISERROR('102|%s|"%s"|utb_service_channel_schedule_change_reason - "Other" reason not found for service channel', 16, 1, @ProcName, 'Other')
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END

            -- Insert repair history
        
            IF @RepairScheduleChangeReasonID IS NOT NULL
            BEGIN
               INSERT INTO utb_service_channel_work_history (     
                  ClaimAspectServiceChannelID,
                  CreatedUserID,
                  ReasonID,
                  ChangeComment,
                  ChangeDate,
                  WorkEndNewDate,
                  WorkEndOldDate,
                  WorkStartNewDate,
                  WorkStartOldDate,
                  SysLastUserID,
                  SysLastUpdatedDate)
               VALUES (     
                  @ClaimAspectServiceChannelID,
                  @UserID,
                  @RepairScheduleChangeReasonID,
                  @RepairScheduleChangeReason,
                  @now,
                  @WorkEndDate,
                  @WorkEndDateOld,
                  @WorkStartDate,
                  @WorkStartDateOld,
                  @UserID,
                  @now
               )
               
               IF @@ERROR <> 0
               BEGIN
                  -- Insertion failure
                  
                  RAISERROR('105|%s|utb_service_channel_work_history', 16, 1, @ProcName)
                  ROLLBACK TRANSACTION 
                  RETURN
               END    
            END

        END
        
        -- Insert the new note
        exec uspNoteInsDetail @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID, 
                              @NoteTypeID = @NoteTypeIDClaim,
                              @StatusID = @EntityStatusID, 
                              @Note = @HistoryDescription, 
                              @UserID = @UserID
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('104|%s|%s', 16, 1, @ProcName, 'uspNoteInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END    
        
        -- Get the repair started confirmation flag
        -- Update the "Verify Repair Complete" task if repair under progress
        
        IF @WorkStartConfirmed = 1
        BEGIN
            -- Repair started. So update the Verify repairs completed task to the new date.
            
            -- First get the checklist id
            SELECT @CheckListID = CheckListID
            FROM dbo.utb_checklist c
            INNER JOIN utb_Claim_Aspect_Service_Channel casc ON c.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
            LEFT JOIN dbo.utb_task t ON (c.TaskID = t.TaskID)
            WHERE t.Name = 'Repair Completion Verification Required'
              AND casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
        
            IF @CheckListID IS NOT NULL
            BEGIN
                -- Defect 4221 - Task alaram date to be set a day before the repair end date
                SET @WorkEndDate = DATEADD(day, -1, @WorkEndDate)
                -- Defect 4221 - End of changes

                -- Task found. Set the alarm date to the new end date
                UPDATE dbo.utb_checklist
                SET AlarmDate = @WorkEndDate
                WHERE CheckListID = @CheckListID

                -- Check error value
    
                IF @@ERROR <> 0
                BEGIN
                    -- Update failure

                    RAISERROR('104|%s|utb_checklist', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION 
                    RETURN
                END    
            END
        END        
    END
    

    
    COMMIT TRANSACTION VehicleShopUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Assignment Level
            NULL AS [Assignment!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Assignment Level
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAssignmentUpdRemarks' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAssignmentUpdRemarks TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
