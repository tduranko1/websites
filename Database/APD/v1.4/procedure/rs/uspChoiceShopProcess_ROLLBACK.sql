-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceShopProcess' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceShopProcess 
END

GO

