-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerSvcUpdateJobDetails' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPartnerSvcUpdateJobDetails 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspPartnerSvcUpdateJobDetails
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Updates the job status details for a specific file being processed by the 
*				PartnerService.
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspPartnerSvcUpdateJobDetails]
    @vFileName AS VARCHAR(255)
    , @vJobStatus AS VARCHAR(25)
    , @dtJobProcessedDate AS DATETIME = NULL
    , @dtJobTransformedDate AS DATETIME = NULL
    , @dtJobTransmittedDate AS DATETIME = NULL
    , @dtJobArchivedDate AS DATETIME = NULL
    , @dtJobFinishedDate AS DATETIME = NULL
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)

    SET @ProcName = 'uspPartnerSvcUpdateJobDetails'
    
    SET @ApplicationCD = 'APDPartner'

    -- Validation
    -- None
    
    -- Begin UPDATE
    BEGIN TRAN utrUpdatePartnerData
		UPDATE
			utb_partnersvc_jobs
		SET
			JobStatus = @vJobStatus
			, SysLastUpdatedDate = CURRENT_TIMESTAMP
		WHERE
			JobID = @vFileName

		IF @@ERROR <> 0
		BEGIN
			-- Update Failure
			ROLLBACK TRANSACTION
			RAISERROR('%s: (Partner Job Processing) Error updating the Job Status.', 16, 1, @ProcName)
			--RETURN
		END

		IF (@dtJobProcessedDate IS NOT NULL)
		BEGIN 
			UPDATE
				utb_partnersvc_jobs
			SET
				JobStatus = @vJobStatus
				, SysLastUpdatedDate = CURRENT_TIMESTAMP
				, JobProcessedDate = @dtJobProcessedDate
			WHERE
				JobID = @vFileName
	
			IF @@ERROR <> 0
			BEGIN
				-- Update Failure
				ROLLBACK TRANSACTION
				RAISERROR('%s: (Partner Job Processing) Error updating the Job Status.', 16, 1, @ProcName)
				--RETURN
			END
		END

		IF (@dtJobTransformedDate IS NOT NULL)
		BEGIN 
			UPDATE
				utb_partnersvc_jobs
			SET
				JobStatus = @vJobStatus
				, SysLastUpdatedDate = CURRENT_TIMESTAMP
				, JobTransformedDate = @dtJobTransformedDate
			WHERE
				JobID = @vFileName
	
			IF @@ERROR <> 0
			BEGIN
				-- Update Failure
				ROLLBACK TRANSACTION
				RAISERROR('%s: (Partner Job Processing) Error updating the Job Status.', 16, 1, @ProcName)
				--RETURN
				SELECT '%s: (Partner Job Processing) Error updating the Job Status.', 16, 1, @ProcName
			END
		END

		IF (@dtJobTransmittedDate IS NOT NULL)
		BEGIN 
			UPDATE
				utb_partnersvc_jobs
			SET
				JobStatus = @vJobStatus
				, SysLastUpdatedDate = CURRENT_TIMESTAMP
				, JobTransmittedDate = @dtJobTransmittedDate
			WHERE
				JobID = @vFileName
	
			IF @@ERROR <> 0
			BEGIN
				-- Update Failure
				ROLLBACK TRANSACTION
				RAISERROR('%s: (Partner Job Processing) Error updating the Job Status.', 16, 1, @ProcName)
				--RETURN
			END
		END

		IF (@dtJobArchivedDate IS NOT NULL)
		BEGIN 
			UPDATE
				utb_partnersvc_jobs
			SET
				JobStatus = @vJobStatus
				, SysLastUpdatedDate = CURRENT_TIMESTAMP
				, JobArchivedDate = @dtJobArchivedDate
			WHERE
				JobID = @vFileName
	
			IF @@ERROR <> 0
			BEGIN
				-- Update Failure
				ROLLBACK TRANSACTION
				RAISERROR('%s: (Partner Job Processing) Error updating the Job Status.', 16, 1, @ProcName)
				--RETURN
			END
		END

		IF (@dtJobFinishedDate IS NOT NULL)
		BEGIN 
			UPDATE
				utb_partnersvc_jobs
			SET
				JobStatus = @vJobStatus
				, SysLastUpdatedDate = CURRENT_TIMESTAMP
				, JobFinishedDate = @dtJobFinishedDate
			WHERE
				JobID = @vFileName
	
			IF @@ERROR <> 0
			BEGIN
				-- Update Failure
				ROLLBACK TRANSACTION
				RAISERROR('%s: (Partner Job Processing) Error updating the Job Status.', 16, 1, @ProcName)
				--RETURN
			END
		END
		
		COMMIT TRANSACTION utrUpdatePartnerData
		--rollback transaction utrUpdatePartnerData

		IF @@ERROR <> 0
		BEGIN
			-- SQL Server Error
	       
			RAISERROR('%s: SQL Server Error during Commit', 16, 1, @ProcName)
			--RETURN
		END                                     

	SELECT 'Updated'
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerSvcUpdateJobDetails' AND type = 'P')
BEGIN
--    GRANT EXECUTE ON dbo.uspPartnerSvcUpdateJobDetails TO 
--        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspPartnerSvcUpdateJobDetails TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/