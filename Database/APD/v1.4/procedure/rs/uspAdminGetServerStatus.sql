-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminGetServerStatus' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminGetServerStatus 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminGetServerStatus
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets Performace data for each server requested
*
* PARAMETERS:  
*				
* RESULT SET:
*				Record Set
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminGetServerStatus
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	/*******************************/
	/* Hold SQL Who Snapshot       */
	/*******************************/
	CREATE TABLE #APDStatusTmp
	(
	   SPID INT
	   , CurrentStatus VARCHAR(50)
	   , CurrentLogin VARCHAR(50)
	   , HostName VARCHAR(50)
	   , BlkBy VARCHAR(50)
	   , DBName VARCHAR(50)
	   , CurrentCommand VARCHAR(50)
	   , CPUTime INT
	   , DiskID INT
	   , LastBatch VARCHAR(50)
	   , ProgramName VARCHAR(50)
	   , SPID2 INT 	
	)
	INSERT INTO #APDStatusTmp 
	exec sp_who2

	/*******************************/
	/* Get Server Names            */
	/*******************************/
	CREATE TABLE #APDServersTmp
	(
	   HostName VARCHAR(50)
	)
	INSERT INTO #APDServersTmp SELECT DISTINCT HostName FROM #APDStatusTmp WHERE DBName LIKE 'udb_apd%' AND CurrentLogin = 'PGW\LYNXAPDPRDCOM'
	--SELECT * FROM #APDServersTmp

	/********************************/
	/* Hold ServerStatus for return */
	/********************************/
	CREATE TABLE #APDServerStatusTmp
	(
	   [Server] VARCHAR(50)
	   , ConcurrentUsers INT
	   , DiskIO INT
	   , CPUTime INT
	   , Blocks INT
	   , [OnLine] VARCHAR(50)
	)

	/*********************************/
	/* Get Concurrent Users Snapshot */
	/*********************************/
	DECLARE db_cursor CURSOR FOR 
		SELECT 
			HostName
		FROM 
			#APDServersTmp
			
	DECLARE @HostName VARCHAR(50)
			
	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO 
		@HostName		
	WHILE @@FETCH_STATUS = 0  
	BEGIN  		
		INSERT INTO #APDServerStatusTmp
		SELECT 
			@HostName AS 'Server'
			, (SELECT COUNT(SPID) FROM #APDStatusTmp WHERE HostName = @HostName AND DBName LIKE 'udb_apd%') AS 'ConcurrentUsers'
			, (SELECT SUM(DiskID) FROM #APDStatusTmp WHERE HostName = @HostName AND DBName LIKE 'udb_apd%') AS 'DiskIO'
			, (SELECT SUM(CPUTime) FROM #APDStatusTmp WHERE HostName = @HostName AND DBName LIKE 'udb_apd%') AS 'CPUTime'
			, (SELECT COUNT(BlkBy) FROM #APDStatusTmp WHERE HostName = @HostName AND DBName LIKE 'udb_apd%') AS 'Blocks'
			--, (SELECT COUNT(SPID) FROM #APDStatusTmp WHERE DBName LIKE 'udb_apd%' AND HostName NOT IN (SELECT HostName FROM #APDServersTmp)) AS 'OtherActivity'
			, (SELECT Value from utb_app_variable WHERE [Name] = 'Sys_Status' AND SubName = @HostName) AS 'OnLine'
		FETCH NEXT FROM db_cursor INTO 	
			@HostName		
	END  
	CLOSE db_cursor  
	DEALLOCATE db_cursor 		
			
	SELECT * FROM #APDServerStatusTmp

	DROP TABLE #APDServerStatusTmp
	DROP TABLE #APDServersTmp	
	DROP TABLE #APDStatusTmp

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminGetServerStatus' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminGetServerStatus TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAdminGetServerStatus TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/