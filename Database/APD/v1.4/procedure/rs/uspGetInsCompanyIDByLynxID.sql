BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetInsCompanyIDByLynxID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetInsCompanyIDByLynxID
END

GO
/****** Object:  StoredProcedure [dbo].[uspNugenGetGUIDByLynxID]    Script Date: 09/24/2013 03:59:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGetInsCompanyIDByLynxID]
(
@LynxID varchar(10),
@InsuranceCompanyID varchar(10) OUTPUT
)
AS
BEGIN
SET NOCOUNT ON;
IF EXISTS(SELECT InsuranceCompanyID FROM utb_claim WHERE LynxID = @LynxID)
		BEGIN
		SET @InsuranceCompanyID = (SELECT InsuranceCompanyID
			FROM utb_claim
			WHERE LynxID = @LynxID)
		END
return @InsuranceCompanyID
SET NOCOUNT OFF;
END
Go
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'uspGetInsCompanyIDByLynxID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetInsCompanyIDByLynxID TO 
        ugr_lynxapd
	GRANT EXECUTE ON dbo.uspGetInsCompanyIDByLynxID TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO