                                                                                                                         -- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptPendingCounts' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptPendingCounts
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptPendingCounts
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Produces a report of claim and task counts by user
*
* PARAMETERS:   None
*
* RESULT SET:
*   User, Supervisor, Claim Count, DA Count, DR Count, ME Count, PS Count, Task Count, Aging tasks over 30 days Count
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptPendingCounts
AS
BEGIN
    -- Set Database Options

    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    DECLARE @tmpClaimCountsRaw TABLE
    (
        UserID              int,
        CountAtLevel        varchar(5),
        ServiceChannelCD    varchar(4),
        ClaimCount          int
    )

    DECLARE @tmpReportCounts TABLE
    (
        UserID              int,
        ServiceChannelCD    varchar(4),
        CountType           char(2),
        RecCount            int

        PRIMARY KEY (UserID, ServiceChannelCD, CountType)
    )

    DECLARE @tmpReportPivot TABLE
    (
        UserID              int,
        TTLClaimCount       int,
        DAClaimCount        int,
        DRClaimCount        int,
        MEClaimCount        int,
        PSClaimCount        int,
        RRPClaimCount       int,
        TTLTaskCount        int,
        TTLAgingTaskCount   int
    )


    --First the count at Claim Level
    INSERT INTO @tmpClaimCountsRaw
    SELECT  tmp.UserID,
            'CLM',
            '', --casc.ServiceChannelCD,
            Count(distinct ca.ClaimAspectID)
      FROM  (   SELECT  ca.ClaimAspectID,
                        ca.OwnerUserID AS UserID
                  FROM  dbo.utb_claim_aspect ca
                  LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
                  Left Outer Join utb_Insurance ins on c.InsuranceCompanyID = ins.InsuranceCompanyID
                  LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
                  WHERE ca.ClaimAspectTypeID = 9
                    AND cas.ServiceChannelCD IS NULL
                    AND cas.StatusTypeCD IS NULL
                    AND cas.StatusID < 900
                    AND ca.OwnerUserID  IS NOT NULL
                    AND c.DemoFlag = 0
                    and ins.Name not like ('%Zurich%') --Exclude CEI Claim Handlers -- Zurich


                UNION ALL

                SELECT  ca.ClaimAspectID,
                        ca.AnalystUserID AS UserID
                  FROM  dbo.utb_claim_aspect ca
                  LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
                  Left Outer Join utb_Insurance ins on c.InsuranceCompanyID = ins.InsuranceCompanyID
                  LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
                  WHERE ca.ClaimAspectTypeID = 9
                    AND cas.ServiceChannelCD IS NULL
                    AND cas.StatusTypeCD IS NULL
                    AND cas.StatusID < 900
                    AND ca.AnalystUserID  IS NOT NULL
                    AND c.DemoFlag = 0
                    and ins.Name not like ('%Zurich%') --Exclude CEI Claim Handlers -- Zurich

                UNION ALL

                SELECT  ca.ClaimAspectID,
                        ca.SupportUserID AS UserID
                  FROM  dbo.utb_claim_aspect ca
                  LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
                  Left Outer Join utb_Insurance ins on c.InsuranceCompanyID = ins.InsuranceCompanyID
                  LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
                  WHERE ca.ClaimAspectTypeID = 9
                    AND cas.ServiceChannelCD IS NULL
                    AND cas.StatusTypeCD IS NULL
                    AND cas.StatusID < 900
                    AND ca.SupportUserID  IS NOT NULL
                    AND c.DemoFlag = 0
                    and ins.Name not like ('%Zurich%') )  tmp   

      LEFT JOIN dbo.utb_claim_aspect ca
            ON (tmp.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc
            ON (ca.ClaimAspectID = casc.ClaimAspectID)
      LEFT JOIN dbo.utb_assignment asgn
            ON (asgn.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
      WHERE (asgn.CancellationDate IS NULL)
      AND    casc.EnabledFlag = 1
      GROUP BY tmp.UserID



    --Now the Service Channel Counts

    INSERT INTO @tmpClaimCountsRaw
    SELECT  tmp.UserID,
            'SC',
            casc.ServiceChannelCD,
            Count(*)
      FROM  (   Select distinct zzt.ClaimAspectID, zzt.Userid from
                (SELECT  ca.ClaimAspectID,
                        ca.OwnerUserID AS UserID
                  FROM  dbo.utb_claim_aspect ca
                  LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
                  Left Outer Join utb_Insurance ins on c.InsuranceCompanyID = ins.InsuranceCompanyID
                  LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
                  WHERE ca.ClaimAspectTypeID = 9
                    AND cas.ServiceChannelCD IS NULL
                    AND cas.StatusTypeCD IS NULL
                    AND cas.StatusID < 900
                    AND ca.OwnerUserID  IS NOT NULL
                    AND c.DemoFlag = 0
                    and ins.Name not like ('%Zurich%')

                UNION ALL

                SELECT  ca.ClaimAspectID,
                        ca.AnalystUserID AS UserID
                  FROM  dbo.utb_claim_aspect ca
                  LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
                  Left Outer Join utb_Insurance ins on c.InsuranceCompanyID = ins.InsuranceCompanyID
                  LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
                  WHERE ca.ClaimAspectTypeID = 9
                    AND cas.ServiceChannelCD IS NULL
                    AND cas.StatusTypeCD IS NULL
                    AND cas.StatusID < 900
                    AND ca.AnalystUserID IS NOT NULL
                    AND c.DemoFlag = 0
                    and ins.Name not like ('%Zurich%')

                UNION ALL

                SELECT  ca.ClaimAspectID,
                        ca.SupportUserID AS UserID
                  FROM  dbo.utb_claim_aspect ca
                  LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
                  Left Outer Join utb_Insurance ins on c.InsuranceCompanyID = ins.InsuranceCompanyID
                  LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
                  WHERE ca.ClaimAspectTypeID = 9
                    AND cas.ServiceChannelCD IS NULL
                    AND cas.StatusTypeCD IS NULL
                    AND cas.StatusID < 900
                    AND ca.SupportUserID IS NOT NULL
                    AND c.DemoFlag = 0
                    and ins.Name not like ('%Zurich%'))zzt    )  tmp

      LEFT JOIN dbo.utb_claim_aspect ca
            ON (tmp.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc
            ON (ca.ClaimAspectID = casc.ClaimAspectID)
      LEFT JOIN dbo.utb_assignment asgn
            ON (asgn.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
      WHERE (asgn.CancellationDate IS NULL)  AND asgn.assignmentsequencenumber = 1
      AND    casc.EnabledFlag = 1
      GROUP BY tmp.UserID, casc.ServiceChannelCD



    INSERT INTO @tmpReportCounts
      SELECT  UserID,
              'TTL',
              'CT',
              Sum(ClaimCount)
        FROM  @tmpClaimCountsRaw
        Where CountAtLevel = 'CLM'
        GROUP BY UserID

    INSERT INTO @tmpReportCounts
      SELECT  UserID,
              ServiceChannelCD,
              'CT',
              Sum(ClaimCount)
        FROM  @tmpClaimCountsRaw
        Where CountAtLevel = 'SC'
        GROUP BY UserID, ServiceChannelCD

    INSERT INTO @tmpReportCounts
      SELECT  ch.AssignedUserID,
              'TTL',
              'TT',
              Count(distinct ch.ChecklistID)
        FROM  dbo.utb_checklist ch
        LEFT JOIN dbo.utb_claim_aspect_service_channel casc
            ON (ch.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
        LEFT JOIN dbo.utb_claim_aspect ca
            ON (casc.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_claim c
            ON (ca.LynxID = c.LynxID)
        Left Outer Join utb_Insurance ins on c.InsuranceCompanyID = ins.InsuranceCompanyID
        WHERE c.DemoFlag = 0
        and ins.Name not like ('%Zurich%')
        GROUP BY AssignedUserID

    INSERT INTO @tmpReportCounts
      SELECT  ch.AssignedUserID,
              'TTL',
              'TA',
              Count(distinct ch.ChecklistID)
        FROM  dbo.utb_checklist ch
        LEFT JOIN dbo.utb_claim_aspect_service_channel casc
            ON (ch.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
        LEFT JOIN dbo.utb_claim_aspect ca
            ON (casc.ClaimAspectID = ca.ClaimAspectID)
        LEFT JOIN dbo.utb_claim c
            ON (ca.LynxID = c.LynxID)
        Left Outer Join utb_Insurance ins on c.InsuranceCompanyID = ins.InsuranceCompanyID
        WHERE c.DemoFlag = 0
        and ins.Name not like ('%Zurich%')
          AND DateDiff(d, ch.CreatedDate, ch.AlarmDate) > 29
        GROUP BY AssignedUserID

    INSERT INTO @tmpReportPivot
      SELECT UserID,
             SUM(CASE WHEN ServiceChannelCD = 'TTL' AND CountType='CT' THEN RecCount ELSE 0 END),
             SUM(CASE WHEN ServiceChannelCD = 'DA' AND CountType='CT' THEN RecCount ELSE 0 END),
             SUM(CASE WHEN ServiceChannelCD = 'DR' AND CountType='CT' THEN RecCount ELSE 0 END),
             SUM(CASE WHEN ServiceChannelCD = 'ME' AND CountType='CT' THEN RecCount ELSE 0 END),
             SUM(CASE WHEN ServiceChannelCD = 'PS' AND CountType='CT' THEN RecCount ELSE 0 END),
             SUM(CASE WHEN ServiceChannelCD = 'RRP' AND CountType='CT' THEN RecCount ELSE 0 END),             
             SUM(CASE WHEN ServiceChannelCD = 'TTL' AND CountType='TT' THEN RecCount ELSE 0 END),
             SUM(CASE WHEN ServiceChannelCD = 'TTL' AND CountType='TA' THEN RecCount ELSE 0 END)
      FROM   @tmpReportCounts
      GROUP BY UserID


    SELECT  ISNULL(Convert(varchar(50), su.NameLast + ', ' + su.NameFirst), ' None') AS Supervisor,
            Convert(varchar(50), u.NameLast + ', ' + u.NameFirst) AS 'Assigned To',
            tmp.TTLClaimCount AS 'Claim Count',
            tmp.DAClaimCount AS 'DA Channel Count',
            tmp.DRClaimCount AS 'DR Channel Count',
            tmp.MEClaimCount AS 'ME Channel Count',
            tmp.PSClaimCount AS 'PS Channel Count',
            tmp.RRPClaimCount AS 'RRP Channel Count',
            tmp.TTLTaskCount AS 'Task Count',
            tmp.TTLAgingTaskCount AS 'Tasks Older Than 30 Days',
            1 AS 'User Count'
      FROM  @tmpReportPivot tmp
      LEFT JOIN dbo.utb_user u ON (tmp.UserID = u.UserID)
      LEFT JOIN dbo.utb_user su ON (u.SupervisorUserID = su.UserID)
      ORDER BY 1,2  --u.NameLast + ', ' + u.NameFirst, su.NameLast + ', ' + su.NameFirst
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptPendingCounts' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptPendingCounts TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/

