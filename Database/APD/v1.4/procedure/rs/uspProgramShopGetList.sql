-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspProgramShopGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspProgramShopGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspProgramShopGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This will get the shops for a particular insurance company
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspProgramShopGetList
    @InsuranceCompanyID  udt_std_int_small,
    @StateList           varchar(1000) = '%'
AS
BEGIN
    declare @WarrantyRefinishMinYrs      tinyint
    declare @WarrantyWorkmanshipMinYrs   tinyint
    declare @CCAV_Client                 bit

    declare @now                         datetime

    set nocount on
    set @now = CURRENT_TIMESTAMP
   
        -- Get Warranty requirements for carrier
        
        select @WarrantyRefinishMinYrs = convert(tinyint, WarrantyPeriodRefinishMinCD),
               @WarrantyWorkmanshipMinYrs = convert(tinyint, WarrantyPeriodWorkmanshipMinCD),
               @CCAV_Client =  case
                                    when ReturnDocRoutingCD = 'CCAV' then 1
                                    else 0
                               end
        from dbo.utb_insurance
        where InsuranceCompanyID = @InsuranceCompanyID
        
        if @WarrantyRefinishMinYrs is null
        begin
            -- The carrier had no configuration for a minimum refinish warranty, set minimum to 0
        
            set @WarrantyRefinishMinYrs = 0
        end
        
        if @WarrantyWorkmanshipMinYrs is null
        begin
            -- The carrier had no configuration for a minimum workmanship warranty, set minimum to 0
        
            set @WarrantyWorkmanshipMinYrs = 0
        end
        
        DECLARE @tmpStateList TABLE 
        (
           StateCode           varchar(2) NOT NULL
        )
        
        
        IF @StateList = '%' OR @StateList IS NULL OR @StateList = '' 
        BEGIN
            INSERT INTO @tmpStateList
            SELECT StateCode
            FROM utb_state_code
            WHERE EnabledFlag = 1
        END
        ELSE
        BEGIN
            INSERT INTO @tmpStateList
            SELECT DISTINCT value FROM dbo.ufnUtilityParseString(@StateList, ',', 1)
        END
        
        if @CCAV_Client = 1
        begin
            select  sl.ShopLocationID, replace(sl.Name, ',', '') as Name, replace(sl.Address1, ',', '') as Address1, replace(sl.AddressCity, ',', '') as AddressCity, sl.AddressState, sl.AddressZip, zc.County, 
                    sl.PhoneAreaCode + ' ' + sl.PhoneExchangeNumber + ' ' + sl.PhoneUnitNumber as 'Phone',
                    sl.FaxAreaCode + ' ' + sl.FaxExchangeNumber + ' ' + sl.FaxUnitNumber as 'Fax'
                  from  dbo.utb_shop_location sl
                  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.ExcludeFlag = 1 and InsuranceCompanyID = @InsuranceCompanyID) csle on (sl.ShopLocationID = csle.ShopLocationID)
                  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.IncludeFlag = 1) csli on (sl.ShopLocationID = csli.ShopLocationID)
                  LEFT JOIN utb_zip_code zc ON sl.AddressZip = zc.Zip
                  inner join dbo.utb_client_contract_state ccs on (@InsuranceCompanyID = ccs.InsuranceCompanyID and sl.AddressState = ccs.StateCode)
                  where ((ccs.UseCEIShopsFlag = 1 and sl.CEIProgramFlag = 1) or (ccs.UseCEIShopsFlag = 0 and sl.ProgramFlag = 1))
                    and sl.EnabledFlag = 1
                    and sl.AvailableForSelectionFlag = 1
                    and sl.AddressState <> 'MA'
                    and isnull(convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
                    and isnull(convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
                    and csle.InsuranceCompanyID is null
                    and (csli.InsuranceCompanyID = @InsuranceCompanyID or csli.InsuranceCompanyID is null)
                    and sl.ProgramFlag = 1
            order by sl.AddressState, sl.Name
        end
        else
        begin
            select  sl.ShopLocationID, replace(sl.Name, ',', '') as Name, replace(sl.Address1, ',', '') Address1, replace(sl.AddressCity, ',', '') as AddressCity, sl.AddressState, sl.AddressZip, zc.County, 
                    sl.PhoneAreaCode + ' ' + sl.PhoneExchangeNumber + ' ' + sl.PhoneUnitNumber as 'Phone',
                    sl.FaxAreaCode + ' ' + sl.FaxExchangeNumber + ' ' + sl.FaxUnitNumber as 'Fax'
                  from  dbo.utb_shop_location sl
                  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.ExcludeFlag = 1 and InsuranceCompanyID = @InsuranceCompanyID) csle on (sl.ShopLocationID = csle.ShopLocationID)
                  left join (select csl.ShopLocationID, csl.InsuranceCompanyID from dbo.utb_client_shop_location csl where csl.IncludeFlag = 1) csli on (sl.ShopLocationID = csli.ShopLocationID)
                  LEFT JOIN utb_zip_code zc ON sl.AddressZip = zc.Zip
                  inner join dbo.utb_client_business_state cbs on (@InsuranceCompanyID = cbs.InsuranceCompanyID and sl.AddressState = cbs.StateCode)
                  where sl.EnabledFlag = 1
                    and sl.AvailableForSelectionFlag = 1
                    and sl.AddressState <> 'MA'
                    and sl.AddressState in (SELECT StateCode FROM @tmpStateList)
                    and isnull(convert(tinyint, sl.WarrantyPeriodRefinishCD), 0) >= @WarrantyRefinishMinYrs  
                    and isnull(convert(tinyint, sl.WarrantyPeriodWorkmanshipCD), 0) >= @WarrantyWorkmanshipMinYrs  
                    and csle.InsuranceCompanyID is null
                    and (csli.InsuranceCompanyID = @InsuranceCompanyID or csli.InsuranceCompanyID is null)
                    and sl.ProgramFlag = 1
            order by sl.AddressState, sl.Name
        end
   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspProgramShopGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspProgramShopGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/