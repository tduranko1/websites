-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCheckRepairNotConfirmed' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCheckRepairNotConfirmed 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCheckRepairNotConfirmed
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This proc will check all claims that have a repair start date and have not been confirmed and throw the 
*               "Repair start not confirmed" task. Intended to run as a job.
*
* PARAMETERS:  
*
* RESULT SET:
*           (none)
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCheckRepairNotConfirmed
AS
BEGIN
    DECLARE @now                                   AS udt_std_datetime
    DECLARE @EventID                               AS udt_std_int
    DECLARE @EventName                             AS varchar(50)
    DECLARE @HistoryDescription                    AS udt_std_desc_long
    DECLARE @PertainsToName                        AS varchar(50)
    DECLARE @ClaimAspectServiceChannelIDWorking    AS udt_std_id_big
    DECLARE @RepairStartDateWorking                AS udt_std_datetime
    
    DECLARE @TaskVerifyRepairStart                 AS udt_std_int
    DECLARE @OpenStatusID                          AS udt_std_int
    DECLARE @SCActiveStatusID                      AS udt_std_int
    DECLARE @StatusID                              AS udt_std_int

    DECLARE @ProcName                              AS udt_std_desc_short
    SET @ProcName = 'uspCheckRepairNotConfirmed'
    
    -- Set Database options
    
    SET NOCOUNT ON
    
    
    SET @now = CURRENT_TIMESTAMP
    
    -- Get the EventID corresponding to "Repair Start Not Confirmed"
    
    SET @EventName = 'Repair Start Not Confirmed'
    
    SELECT @EventID = EventID
    FROM dbo.utb_event
    WHERE Name = @EventName
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              

    IF @EventID IS NULL
    BEGIN
        -- Event Name not found
    
        RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
        RETURN
    END
    
    SELECT @TaskVerifyRepairStart = TaskID
    FROM dbo.utb_task
    WHERE Name = 'Repair Start Verification Required'
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              

    IF @TaskVerifyRepairStart IS NULL
    BEGIN
        -- Task Name not found
    
        RAISERROR('102|%s|"%s"|utb_task', 16, 1, @ProcName, @EventName)
        RETURN
    END
    
    SELECT @OpenStatusID = StatusID
    FROM dbo.utb_status
    WHERE Name='Open'
      and ClaimAspectTypeID = 9

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
        
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              
    
    IF @OpenStatusID IS NULL
    BEGIN
        RAISERROR('102|%s|"%s"|utb_status', 16, 1, @ProcName, @EventName)
        RETURN
    END

    -- Get the active status id for PS
    SELECT @SCActiveStatusID = StatusID
    FROM dbo.utb_status
    WHERE Name='Active'
      and ClaimAspectTypeID = 9
      and ServiceChannelCD = 'PS'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
        
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              
    
    IF @SCActiveStatusID IS NULL
    BEGIN
        RAISERROR('102|%s|"%s"|utb_status', 16, 1, @ProcName, @EventName)
        RETURN
    END


    DECLARE csrRepairNotConfirmed CURSOR FOR
      SELECT casc.ClaimAspectServiceChannelID
      FROM dbo.utb_claim_aspect_service_channel casc
      LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
      LEFT JOIN utb_claim_aspect_status cas on ca.ClaimAspectID = cas.ClaimAspectID and casc.ServiceChannelCD = cas.ServiceChannelCD
      WHERE WorkStartDate < @now
        AND WorkStartConfirmFlag = 0
        AND cas.StatusTypeCD = 'SC'
        AND cas.StatusID = @SCActiveStatusID
          

     
    OPEN csrRepairNotConfirmed

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrRepairNotConfirmed
    INTO @ClaimAspectServiceChannelIDWorking

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        -- Check if the Verify Repairs Started task was created for the current
        --  ClaimAspectServiceChannelID.
        IF NOT EXISTS(SELECT CheckListID
                        FROM dbo.utb_checklist cl
                        INNER JOIN utb_Claim_Aspect_Service_Channel casc ON cl.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                        WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelIDWorking
                          AND cl.TaskID = @TaskVerifyRepairStart)
        BEGIN
            -- Verify Repairs Started task not found. So create one.
            
            -- reset the loop variables
            SET @HistoryDescription = NULL
            SET @PertainsToName = NULL
            SET @RepairStartDateWorking = NULL
        
            SELECT @RepairStartDateWorking = WorkStartDate
            FROM dbo.utb_claim_aspect_service_channel
            WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelIDWorking
        
            SELECT @PertainsToName = cat.Name + ' ' + convert(varchar, ClaimAspectNumber),
                   @StatusID = cas.StatusID
            FROM dbo.utb_claim_aspect_service_channel casc
            LEFT JOIN dbo.utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID 
            LEFT JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID and casc.ServiceChannelCD = cas.ServiceChannelCD
            LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.claimAspectTypeID = cat.ClaimAspectTypeID)
            WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelIDWorking
              AND cas.StatusTypeCD = 'SC'
        
            SET @HistoryDescription = @EventName + ' for ' + @PertainsToName + '. Repair Start Date: ' + convert(varchar, @RepairStartDateWorking, 101)
            IF @StatusID = @SCActiveStatusID
            BEGIN

               print convert(varchar(20),@EventID) + ' - ' + convert(varchar(20),@ClaimAspectServiceChannelIDWorking) + ' - ' + convert(varchar(5),@StatusID) + '-' + @HistoryDescription            
               --print convert(varchar, @ClaimAspectIDWorking)
               --print isNull(@HistoryDescription, 'NULL')
   
               -- Make the workflow call
           
               EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                           @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelIDWorking,
                                           @Description = @HistoryDescription,
                                           @UserID = 0
   
               IF @@ERROR <> 0
               BEGIN
                   -- APD Workflow Notification failed
   
                   RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
                   -- Do the cursor cleanup
                   CLOSE csrRepairNotConfirmed
                   DEALLOCATE csrRepairNotConfirmed            
                   RETURN
               END
            END
        END

        FETCH next
        FROM csrRepairNotConfirmed
        INTO @ClaimAspectServiceChannelIDWorking

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    
    END
    
    CLOSE csrRepairNotConfirmed
    DEALLOCATE csrRepairNotConfirmed
        
    
      
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCheckRepairNotConfirmed' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCheckRepairNotConfirmed TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
