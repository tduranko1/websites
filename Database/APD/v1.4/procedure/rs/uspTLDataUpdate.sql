-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLDataUpdate' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLDataUpdate 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLDataUpdate
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Update the Total Loss Amounts
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID  Checklist ID
* (I) @UserID       User ID
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLDataUpdate
    @ClaimAspectServiceChannelID    udt_std_id_big,
    @SettlementAmount               decimal(9, 2),
    @PayoffAmount                   decimal(9, 2),
    @PayoffExpirationDate           varchar(15) = NULL,
    @AdvanceAmount                  decimal(9, 2),
    @LetterOfGuaranteeAmount        decimal(9, 2),
    @LienHolderAccountNumber        varchar(100),
    @UserID                         varchar(50)
AS
BEGIN

    --Initialize string parameters
    IF LEN(LTRIM(RTRIM(@PayoffExpirationDate))) = 0 OR ISDATE(@PayoffExpirationDate) = 0 SET @PayoffExpirationDate = NULL
    IF LEN(LTRIM(RTRIM(@LienHolderAccountNumber))) = 0  SET @LienHolderAccountNumber = NULL
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    
    SET @ProcName = 'uspTLDataUpdate'
    SET @now = CURRENT_TIMESTAMP
    
    IF NOT EXISTS(SELECT ClaimAspectServiceChannelID
                  FROM utb_claim_aspect_service_channel
                  WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
    BEGIN
        -- Invalid ClaimAspectServiceChannelID
        RAISERROR('%s: Invalid ClaimAspectServiceChannelID.', 16, 1, @ProcName)
        RETURN
    END

    -- Check the user id
    IF NOT EXISTS (SELECT UserID FROM utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User
        RAISERROR('%s: Invalid User.', 16, 1, @ProcName)
        RETURN
    END

    BEGIN TRANSACTION
    
    UPDATE utb_claim_aspect_service_channel
    SET SettlementAmount = @SettlementAmount,
        PayoffAmount = @PayoffAmount,
        PayoffExpirationDate = @PayoffExpirationDate,
        AdvanceAmount = @AdvanceAmount,
        LetterOfGuaranteeAmount = @LetterOfGuaranteeAmount,
        LeinHolderAccountNumber = @LienHolderAccountNumber,
        SysLastUserID = @UserID,
        SysLastUpdatedDate = @now
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_claim_apsect_service_channel', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
      
    COMMIT TRANSACTION
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLDataUpdate' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLDataUpdate TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/