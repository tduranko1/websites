-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptCFShopUpdates' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptCFShopUpdates 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- Create the stored procedure

/************************************************************************************************************************
*
* PROCEDURE:    uspRptCFShopUpdates
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Report updates to the CF shops.
*
* PARAMETERS:  
* None.
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptCFShopUpdates
    @age       INT = 1       -- Default: 1 day
AS
BEGIN
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @ProcName                       AS VARCHAR(30)

    SET @ProcName = 'uspRptCFShopUpdates'  

   
    SELECT    
        CAST(dbo.ufnUtilityFormatDate(sl.SysLastUpdatedDate, 'YYY/MM/DD') AS CHAR(10)) AS UpdatedDay,
        Address1  ,
        Address2  ,
        AddressCity  ,
        AddressState  ,
        AddressZip  ,
        CertifiedFirstId  ,
        EmailAddress  ,
        FaxAreaCode  ,
        FaxExchangeNumber  ,
        FaxExtensionNumber  ,
        FaxUnitNumber  ,
        sl.Name  ,
        OperatingFridayEndTime  ,
        OperatingFridayStartTime  ,
        OperatingMondayEndTime  ,
        OperatingMondayStartTime  ,
        OperatingSaturdayEndTime  ,
        OperatingSaturdayStartTime  ,
        OperatingSundayEndTime  ,
        OperatingSundayStartTime  ,
        OperatingThursdayEndTime  ,
        OperatingThursdayStartTime  ,
        OperatingTuesdayEndTime  ,
        OperatingTuesdayStartTime  ,
        OperatingWednesdayEndTime  ,
        OperatingWednesdayStartTime  ,
        PhoneAreaCode  ,
        PhoneExchangeNumber  ,
        PhoneExtensionNumber  ,
        PhoneUnitNumber  ,
        ep.Name PreferredEstimatePackageID  ,
        SLPEmailAddress  ,
        SLPFaxAreaCode  ,
        SLPFaxExchangeNumber  ,
        SLPFaxExtensionNumber  ,
        SLPFaxUnitNumber  ,
        SLPName  ,
        SLPPhoneAreaCode  ,
        SLPPhoneExchangeNumber  ,
        SLPPhoneExtensionNumber  ,
        SLPPhoneUnitNumber  ,
        pcm.Name SLPPreferredContactMethodID  ,
        dbo.ufnUtilityGetDateString(sl.SysLastUpdatedDate)  ,
        WebSiteAddress  
    FROM utb_shop_load    sl
    LEFT JOIN utb_estimate_package   ep ON sl.PreferredEstimatePackageID = ep.EstimatePackageID
    LEFT JOIN utb_personnel_contact_method   pcm ON sl.SLPPreferredContactMethodID = pcm.PersonnelContactMethodID
    WHERE sl.SysLastUpdatedDate IS NOT NULL 
      AND sl.SysLastUpdatedDate > CURRENT_TIMESTAMP - @age
      AND CertifiedFirstId IS NOT NULL
    ORDER BY sl.SysLastUpdatedDate DESC
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('APD - %s: SQL Server Error getting daily cf shop updates', 16, 1, @ProcName)
        RETURN
    END

    IF @rowcount = 0
    BEGIN
        SELECT 'No Changes Found' AS Result
    END
    
    RETURN @rowcount
END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptCFShopUpdates' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptCFShopUpdates TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/