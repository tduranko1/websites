-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspIsAssignmentToLDAU' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspIsAssignmentToLDAU 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspIsAssignmentToLDAU
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Accepts an assignment id and returns a value indicating whether the assignment is/was to the Lynx Desk Audit unit
*
* PARAMETERS:  
* (I) @AssignmentID         The assignment id being checked
*
* RESULT SET: None
* 
* RETURNS:  1 if assignment is for LDAU, 0 if it is not
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspIsAssignmentToLDAU
    @AssignmentID       udt_std_id_big
AS
BEGIN
   -- Declare internal variables

    DECLARE @AppraiserID                udt_std_id_big
    DECLARE @AssignmentIDWork           udt_std_id_big
    DECLARE @DeskAuditAppraiserType     udt_std_cd
    DECLARE @DeskAuditID                udt_std_id_big
    DECLARE @ShopLocationID             udt_std_id_big
    
    DECLARE @error                      AS int

    DECLARE @ProcName                   AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspIsAssignmentToLDAU'
    

    -- Set Database options
    
    SET NOCOUNT ON
    SET CONCAT_NULL_YIELDS_NULL OFF
    
    
    -- Try looking up the Assignment ID
    
    SELECT  @AssignmentIDWork = AssignmentID,
            @AppraiserID = AppraiserID,
            @ShopLocationID = ShopLocationID
      FROM  dbo.utb_assignment 
      WHERE AssignmentID = @AssignmentID
    
    IF @AssignmentIDWork IS NULL
    BEGIN
        -- Invalid Assignment ID for this claim aspect

        RAISERROR('101|%s|@AssignmentID|%u', 16, 1, @ProcName, @AssignmentID)
        RETURN
    END
    
    
    -- Look up the Desk Audit unit information
    
    SELECT  @DeskAuditAppraiserType = Left(LTrim(RTrim(value)), CharIndex(',', LTrim(RTrim(value))) - 1),
            @DeskAuditID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable
      WHERE Name = 'DESK_AUDIT_AUTOID'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Check if this is an assignment to the desk audit unit
    
    IF ((@ShopLocationID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'S')) OR
       ((@AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A'))
    BEGIN
        -- This is a desk audit assignment, return 1 (true)
    
        RETURN 1
    END
    ELSE
    BEGIN
        RETURN 0
    END 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspIsAssignmentToLDAU' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspIsAssignmentToLDAU TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/