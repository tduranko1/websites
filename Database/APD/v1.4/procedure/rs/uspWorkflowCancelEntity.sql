-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowCancelEntity' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowCancelEntity 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowCancelEntity
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc to cancel vehicle/property
*
* PARAMETERS:  
* (I) @ClaimAspectID  The ClaimAspectID of the entity for which the operation is being performed
* (I) @InsuranceID    The Insurance company ID to which the LynxID belongs
* (I) @Comment        The claim rep's comment for canceling the claim
* (I) @UserID         The ID of the user that is making this change
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowCancelEntity
    @ClaimAspectID       udt_std_id_big,
    @InsuranceCompanyID  udt_std_id_big,
    @Comment             udt_std_note,
    @UserID              udt_std_id,
    @ApplicationCD       udt_std_cd='APD'
AS
BEGIN
    DECLARE @now                            AS udt_std_datetime
    DECLARE @error                          AS int
    DECLARE @rowcount                       AS int

    DECLARE @ProcName                       AS varchar(30)       -- Used for raise error stmts 
    DECLARE @ClaimAspectTypeIDClaim         AS udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle       AS udt_std_id

    DECLARE @EventIDWork                    AS udt_std_id
    DECLARE @EventNameWork                  AS udt_std_name
    DECLARE @HistoryDescription             AS udt_std_desc_long
    DECLARE @EntityName                     AS varchar(15)
    DECLARE @LynxID                         AS udt_std_id_big
    DECLARE @InsuranceCompanyIDClm          AS udt_std_id
    DECLARE @w_ClaimAspectServiceChannelID  AS udt_std_id_big

    SET @ProcName = 'uspWorkflowCancelEntity'

    DECLARE @debug                          AS udt_std_flag

    SET @debug = 0
    
    IF @debug = 1
    BEGIN
        Print ''
        Print 'Stored Proc Input Params:'
        Print '@ClaimAspectID: ' + convert(varchar, @ClaimAspectID)
        Print '@InsuranceCompanyID: ' + convert(varchar, @InsuranceCompanyID)
        Print '@Comment: ' + isNull(@Comment, '[null]')
        Print '@UserID: ' + convert(varchar, @UserID)
    END

    -- Check to make sure a valid ClaimAspect ID was passed in

    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid ClaimAspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END

    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    -- Get the Insurance Company Id for the claim
    SELECT  @InsuranceCompanyIDClm = C.InsuranceCompanyID 
      FROM  dbo.utb_claim C, dbo.utb_claim_aspect ca
      WHERE c.LynxID = ca.LynxID
        AND ca.ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Validate it against what has been passed in

    IF (@InsuranceCompanyIDClm <> @InsuranceCompanyID)
    BEGIN
        -- Insurance Company ID does not match
    
        RAISERROR('111|%s|%u|%u', 16, 1, @ProcName, @InsuranceCompanyIDClm, @InsuranceCompanyID)
        RETURN
    END

    -- Get ClaimAspectTypeID for the Claim
    SELECT  @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
       -- ClaimAspectTypeID Not Found for Claim

        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

    SELECT  @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
       -- ClaimAspectTypeID Not Found for Claim

        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END
    -- Is ClaimAspectID passed a vehicle aspect type?
    IF (SELECT ClaimAspectTypeID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID) <> @ClaimAspectTypeIDVehicle
    BEGIN
      RAISERROR('1|Claim Aspect ID passed in does not map to a vehicle claim aspect.',16,1)
      RETURN 
    END
    
    -- Is vehicle voided?
    IF EXISTS (SELECT cas.StatusID
               FROM dbo.utb_claim_aspect_status cas
               INNER JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID)
               WHERE cas.ClaimAspectID = @ClaimAspectID                  
                 AND s.Name = 'Vehicle Voided')
    BEGIN                 
      -- Vehicle is voided so we can not cancelled it
      RAISERROR('1|This vehicle has been voided. Can not cancel.',16,1)
      RETURN
    END       
    
    -- Check if the entity has been billed
    
    IF EXISTS(SELECT InvoiceID
                FROM dbo.utb_invoice i
                WHERE i.ClaimAspectID = @ClaimAspectID
                  AND i.EnabledFlag = 1
                  AND i.ItemTypeCD = 'F'    -- Fee
              )
    BEGIN
        -- There are billing records for the claim / exposures.

        RAISERROR  ('1|This vehicle has been billed. Cannot cancel it.', 16, 1)
        RETURN
    END
    
    --Check if assignment has been sent. Cannot cancel if assignment sent.
    IF EXISTS( SELECT  st.StatusID
                 FROM  dbo.utb_claim_aspect_status cas				
                   LEFT JOIN dbo.utb_status st ON (cas.StatusID = st.StatusID)
                 WHERE cas.ClaimAspectID = @ClaimAspectID                   
                   AND cas.StatusID in ( SELECT StatusID
                                        FROM utb_status st
                                        WHERE st.Name in ('Enroute', 
                                                          'Sent',
                                                          'Received By Appraiser')))
    BEGIN
       -- Assignment has been sent for one or more of its exposures. Cannot cancel/void claim.

        RAISERROR  ('1|This vehicle has assignments sent. Cannot cancel', 16, 1)
        RETURN
    END
    
    IF EXISTS (SELECT cas.StatusID
               FROM dbo.utb_claim_aspect_status cas
               INNER JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID)
               WHERE cas.ClaimAspectID = @ClaimAspectID
                 AND s.StatusTypeCD IS NOT NULL
                 AND s.Name = 'Complete')
    BEGIN
      -- Can not cancel a vehicle with a completed service channel
      RAISERROR ('1|This vehicle has completed service channels attached. Cannot cancel.',16,1)
      RETURN
    END
    
    IF @debug = 1
    BEGIN
      SELECT distinct casc.ClaimAspectServiceChannelID
      FROM dbo.utb_claim_aspect_service_channel casc    
      LEFT JOIN dbo.utb_claim_aspect_status cas ON (casc.ClaimAspectID = cas.ClaimAspectID)
      INNER JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID)
      WHERE cas.ClaimAspectID = @ClaimAspectID
        AND cas.ServiceChannelCD = casc.ServiceChannelCD
        AND s.StatusTypeCD = 'SC' 
        AND s.Name NOT IN ('Cancelled','Complete','Voided')
    END      
      
    DECLARE csrServiceChannels CURSOR FOR
    SELECT distinct casc.ClaimAspectServiceChannelID
    FROM dbo.utb_claim_aspect_service_channel casc    
    LEFT JOIN dbo.utb_claim_aspect_status cas ON (casc.ClaimAspectID = cas.ClaimAspectID)
    INNER JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID)
    WHERE cas.ClaimAspectID = @ClaimAspectID
      AND cas.ServiceChannelCD = casc.ServiceChannelCD
      AND s.StatusTypeCD = 'SC' 
      AND s.Name NOT IN ('Cancelled','Complete','Voided')
      
    
    BEGIN TRANSACTION WorkflowCancelEntity1
              
    OPEN csrServiceChannels
    
    IF @@ERROR <> 0
    BEGIN
      RAISERROR('99|%s',16,1,@ProcName)
      ROLLBACK TRANSACTION
      RETURN
    END
    
    FETCH NEXT FROM csrServiceChannels INTO @w_ClaimAspectServiceChannelID

    IF @@ERROR <> 0
    BEGIN
      RAISERROR('99|%s',16,1,@ProcName)
      ROLLBACK TRANSACTION
      RETURN
    END
    
    WHILE @@FETCH_STATUS=0
    BEGIN
    
      EXEC uspWorkflowCancelServiceChannel @ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
                                           @UserID = @UserID,
                                           @Comment = @Comment,
                                           @ApplicationCD = @ApplicationCD

      IF @@ERROR <> 0
      BEGIN
        RAISERROR('104|%s|%s',16,1,@ProcName,'uspWorkflowCancelServiceChannel')
        ROLLBACK TRANSACTION
        RETURN
      END
      
      FETCH NEXT FROM csrServiceChannels INTO @w_ClaimAspectServiceChannelID

      IF @@ERROR <> 0
      BEGIN
        RAISERROR('99|%s',16,1,@ProcName)
        ROLLBACK TRANSACTION
        RETURN
      END
    END      
                                                           
    
    COMMIT TRANSACTION WorkflowCancelEntity1       
    
    
END



GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowCancelEntity' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowCancelEntity TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/