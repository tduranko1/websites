-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspVehicleDescriptionUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspVehicleDescriptionUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspVehicleDescriptionUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates Description data in Vehicle Table (Description Tab on Vehicle Detail Screen)
*
* PARAMETERS:  
* (I) @ClaimAspectID            The claim aspect to update
* (I) @AssignmentTypeID         Assignment Type ID for this vehicle
* (I) @BodyStyle                Vehicle Body Style  
* (I) @BookValueAmt             Book Value
* (I) @Color                    Vehicle Color
* (I) @CoverageProfileCD        Coverage Profile Interepreted for this vehicle
* (I) @ClientCoverageTypeID     Client Coverage Type 
* (I) @DriveableFlag            Is the vehicle driveable?
* (I) @ImpactPoints             Comma delimited list of Impact IDs  
* (I) @ImpactSpeed              Vehicle's speed at impact
* (I) @LicensePlateNumber       License Plate State  
* (I) @LicensePlateState        License Plate Number  
* (I) @Make                     Vehicle Make  
* (I) @Mileage                  Odometer Reading  
* (I) @Model                    Vehicle Model
* (I) @PermissionToDriveCD      Permission To Drive
* (I) @PostedSpeed              The posted speed limit
* (I) @PriorImpactPoints        Comma delimited list of Prior Impact IDs  
* (I) @Remarks                  Remarks to Claim Rep
* (I) @RepairEndDate            The repair ending date
* (I) @RepairStartDate          The repair start date  
* (I) @VehicleYear              Vehicle Year  
* (I) @Vin                      VIN Number  
* (I) @SafetyDevicesDeployed    Comma delimited list of Safety Device IDs  
* (I) @UserID                   The user updating the note
* (I) @SysLastUpdatedDate       The "previous" updated date
*
* RESULT SET:   An XML document containing the new SysLastUpdatedDate value        
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspVehicleDescriptionUpdDetail
    @ClaimAspectID              udt_std_id_big,
    @AssignmentTypeID           udt_std_int,
    @BodyStyle                  udt_auto_body,  
    @BookValueAmt               float,
    @Color                      udt_auto_color, 
    @CoverageProfileCD          udt_std_cd, 
    @ClientCoverageTypeID       udt_std_int,
    @DriveableFlag              udt_std_flag,
    @ImpactPoints               varchar(100),
    @ImpactSpeed                udt_auto_speed,
    @LicensePlateNumber         udt_auto_plate_number,  
    @LicensePlateState          udt_addr_state,
    @InspectionDate             udt_std_datetime,
    @Make                       udt_auto_make,  
    @Mileage                    udt_auto_mileage,  
    @Model                      udt_auto_model,
    @PermissionToDriveCD        udt_std_cd,
    @PostedSpeed                udt_auto_speed,
    @PriorImpactPoints          varchar(100),
    @Remarks                    udt_std_desc_xlong,
    @RepairEndDate              udt_std_datetime,
    @RepairStartDate            udt_std_datetime,
    @RepairScheduleChangeReason udt_std_desc_mid=NULL,
    @VehicleYear                udt_dt_year,
    @Vin                        udt_auto_vin,  
    @SafetyDevicesDeployed      varchar(30),
    @UserID                     udt_std_id,
    @SysLastUpdatedDate         varchar(30)
AS
BEGIN

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@BodyStyle))) = 0 SET @BodyStyle = NULL
    IF LEN(RTRIM(LTRIM(@Color))) = 0 SET @Color = NULL
    IF LEN(RTRIM(LTRIM(@CoverageProfileCD))) = 0 SET @CoverageProfileCD = NULL
    IF LEN(RTRIM(LTRIM(@ClientCoverageTypeID))) = 0 SET @ClientCoverageTypeID = NULL
    IF LEN(RTRIM(LTRIM(@ImpactPoints))) = 0 SET @ImpactPoints = NULL
    IF LEN(RTRIM(LTRIM(@LicensePlateNumber))) = 0 SET @LicensePlateNumber = NULL
    IF LEN(RTRIM(LTRIM(@LicensePlateState))) = 0 SET @LicensePlateState = NULL
    IF LEN(RTRIM(LTRIM(@Make))) = 0 SET @Make = NULL
    IF LEN(RTRIM(LTRIM(@Model))) = 0 SET @Model = NULL
    IF LEN(RTRIM(LTRIM(@PermissionToDriveCD))) = 0 SET @PermissionToDriveCD = NULL
    IF LEN(RTRIM(LTRIM(@PriorImpactPoints))) = 0 SET @PriorImpactPoints = NULL
    IF LEN(RTRIM(LTRIM(@Remarks))) = 0 SET @Remarks = NULL
    IF LEN(RTRIM(LTRIM(@RepairEndDate))) = 0 SET @RepairEndDate = NULL
    IF LEN(RTRIM(LTRIM(@RepairStartDate))) = 0 SET @RepairStartDate = NULL
    IF LEN(RTRIM(LTRIM(@Vin))) = 0 SET @Vin = NULL
    IF LEN(RTRIM(LTRIM(@SafetyDevicesDeployed))) = 0 SET @SafetyDevicesDeployed = NULL


    -- Declare internal variables

    DECLARE @ServiceChannelCD       udt_std_cd
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @OldInspectionDate      AS datetime
    DECLARE @OldRepairStartDate     AS datetime
    DECLARE @OldRepairEndDate       AS datetime
    DECLARE @EventID                AS udt_std_int
    DECLARE @HistoryDescription     AS udt_std_desc_long
    DECLARE @PertainsToName         AS varchar(50)
    DECLARE @EventName              AS varchar(50)
    DECLARE @NoteTypeIDClaim        AS udt_std_id
    DECLARE @EntityStatusID         AS udt_std_id
    DECLARE @ClaimIntakeFinishDate  AS datetime
    DECLARE @EntityCreatedDate      AS datetime
    DECLARE @EntityCreatedDateStr   AS varchar(10)
    DECLARE @ReasonID               AS udt_std_id
    DECLARE @CheckListID            AS udt_std_id_big
    DECLARE @RepairStartConfirmed   AS bit
    DECLARE @NoteDescription        AS udt_std_desc_long
/*  --Project:210474 APD Remarked-off the following because it does not apply anymore M.A.20070109
   DECLARE @OldAssignmentTypeID    AS udt_std_int
*/

    SET @ProcName = 'uspVehicleDescriptionUpdDetail'


    -- Set Database options
    
    SET NOCOUNT ON
    
    
    -- Check to make sure a valid Claim Aspect ID was passed in

    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_vehicle WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    -- Repair start/end date, Inspection dates should not be before the claim start date
    
    SELECT @ClaimIntakeFinishDate = c.IntakeFinishDate,
           @EntityCreatedDate = ca.CreatedDate,
           @EntityCreatedDateStr = CONVERT(varchar, ca.CreatedDate, 101)
    FROM dbo.utb_claim c
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LynxID = ca.LynxID)
    WHERE ca.ClaimAspectID = @ClaimAspectID
    
    IF @RepairStartDate IS NOT NULL
    BEGIN
        --IF CONVERT(datetime, CONVERT(varchar, @RepairStartDate, 101) + ' 23:59:59') < @ClaimIntakeFinishDate
        IF DATEDIFF(day, CONVERT(datetime, CONVERT(varchar, @RepairStartDate, 101) + ' 23:59:59'),  @ClaimIntakeFinishDate) > 7
        BEGIN
            -- Repair start date cannot be before claim start date
            
            RAISERROR('1|Repair start date cannot be more than 7 days before the claim start date.', 16, 1)
            RETURN
        END

        --IF CONVERT(datetime, CONVERT(varchar, @RepairStartDate, 101) + ' 23:59:59') < @EntityCreatedDate
        IF DATEDIFF(day, CONVERT(datetime, CONVERT(varchar, @RepairStartDate, 101) + ' 23:59:59'),  @EntityCreatedDate) > 7
        BEGIN
            -- Repair start date cannot be before vehicle creation date
            
            RAISERROR('1|Repair start date cannot be more than 7 days before the vehicle created date %s.', 16, 1, @EntityCreatedDateStr)
            RETURN
        END
    END

    IF @RepairEndDate IS NOT NULL
    BEGIN
        IF CONVERT(datetime, CONVERT(varchar, @RepairEndDate, 101) + ' 23:59:59') < @ClaimIntakeFinishDate
        BEGIN
            -- Repair end date cannot be before claim start date
            
            RAISERROR('1|Repair end date cannot be before the claim start date.', 16, 1)
            RETURN
        END

        IF CONVERT(datetime, CONVERT(varchar, @RepairEndDate, 101) + ' 23:59:59') < @EntityCreatedDate
        BEGIN
            -- Repair end date cannot be before vehicle creation date
            
            RAISERROR('1|Repair end date cannot be before the vehicle created date %s.', 16, 1, @EntityCreatedDateStr)
            RETURN
        END

        IF @RepairEndDate < @RepairStartDate
        BEGIN
            -- Repair end date cannot be before claim start date
            
            RAISERROR('1|Repair end date cannot be before the repair start date.', 16, 1)
            RETURN
        END
    END

    IF @InspectionDate IS NOT NULL
    BEGIN
        IF CONVERT(datetime, CONVERT(varchar, @InspectionDate, 101) + ' 23:59:59') < @ClaimIntakeFinishDate
        BEGIN
            -- Inspection date cannot be before claim start date
            
            RAISERROR('1|Inspection date cannot be before the claim start date.', 16, 1)
            RETURN
        END

        IF CONVERT(datetime, CONVERT(varchar, @InspectionDate, 101) + ' 23:59:59') < @EntityCreatedDate
        BEGIN
            -- Inspection date cannot be before vehicle creation date
            
            RAISERROR('1|Inspection date cannot be before the vehicle created date %s.', 16, 1, @EntityCreatedDateStr)
            RETURN
        END
    END
    
    
    
    -- Validate the updated date parameter

    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_claim_vehicle', @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END


    -- Get new service channel
    
    SET @ServiceChannelCD = NULL
    
    SELECT  @ServiceChannelCD = ServiceChannelDefaultCD
      FROM  dbo.utb_assignment_type
      WHERE AssignmentTypeID = @AssignmentTypeID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Get the old values of repair start and end date
    SELECT @OldInspectionDate = casc.InspectionDate,
           @OldRepairStartDate = casc.WorkStartDate,
           @OldRepairEndDate   = casc.WorkEndDate,
           @RepairStartConfirmed = casc.WorkStartConfirmFlag
    --FROM dbo.utb_claim_vehicle   --Project:210474 APD Removed the reference to the table when we did the code merge M.A.20061120
    FROM utb_Claim_Aspect_Service_Channel casc  --Project:210474 APD Added the reference to the table when we did the code merge M.A.20061120
    WHERE casc.ClaimAspectID = @ClaimAspectID

    -- if repair start date was changed make sure a reason was submitted
    IF (@OldRepairStartDate IS NOT NULL AND @OldRepairStartDate <> @RepairStartDate) OR 
       (@OldRepairEndDate IS NOT NULL AND @OldRepairEndDate <> @RepairEndDate)
    BEGIN
        IF @RepairScheduleChangeReason IS NULL
        BEGIN
            -- Invalid Repair start date change reason
            
            RAISERROR('1|No reason was provided for repair schedule change', 16, 1)
            RETURN
        END
    END

/*  --Project:210474 APD Remarked-off the following because it does not apply anymore M.A.20070109
    -- Get the old current_assignment_type
    SELECT @OldAssignmentTypeID = currentAssignmentTypeID   --Project:210474 APD The column was added in utb_Claim_Aspect so changed it back to the original state. M.A.20061201
    FROM dbo.utb_claim_aspect
    WHERE claimAspectID = @ClaimAspectID
*/     


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Begin Update

    BEGIN TRANSACTION VehicleDescriptionUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update claim vehicle

    UPDATE  dbo.utb_claim_vehicle
      SET 	BodyStyle               = @BodyStyle,
            BookValueAmt            = @BookValueAmt,
		    Color                   = @Color,
    		DriveableFlag           = @DriveableFlag,
            ImpactSpeed             = @ImpactSpeed,
		    LicensePlateNumber      = @LicensePlateNumber,
		    LicensePlateState       = @LicensePlateState,
		    Make                    = @Make,
		    Mileage                 = @Mileage,
		    Model                   = @Model,
            PermissionToDriveCD     = @PermissionToDriveCD,
    		PostedSpeed             = @PostedSpeed,
    		Remarks                 = @Remarks,
            --InspectionDate          = @InspectionDate,  --Project:210474 APD Remarked-off the column when we did the code merge M.A.20061114.  Do we need to update utb_ClaimAspect_ServiceChannel.InspectionDate?
            --RepairEndDate           = @RepairEndDate, --Project:210474 APD Remarked-off the column when we did the code merge M.A.20061114.  Do we need to update utb_ClaimAspect_ServiceChannel.WorkEndDate?
            --RepairStartDate         = @RepairStartDate,  --Project:210474 APD Remarked-off the column when we did the code merge M.A.20061114.  Do we need to update utb_ClaimAspect_ServiceChannel.WorkStartDate?
		    VehicleYear             = @VehicleYear,
		    Vin                     = @Vin,
		    SysLastUserID           = @UserID,
		    SysLastUpdatedDate      = @now
      WHERE ClaimAspectID = @ClaimAspectID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim_vehicle', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    UPDATE  dbo.utb_claim_aspect
      SET   CoverageProfileCD        = @CoverageProfileCD,
            ClientCoverageTypeID     = @ClientCoverageTypeID,
            --CurrentAssignmentTypeID  = @AssignmentTypeID,--Project:210474 APD Remarked-off the column when we did the code merge M.A.20061114.  Do we need to update utb_ClaimAspect_ServiceChannel.AssignmentTypeID?
            --ServiceChannelCD         = @ServiceChannelCD,--Project:210474 APD Remarked-off the column when we did the code merge M.A.20061114.  Do we need to update utb_ClaimAspect_ServiceChannel.ServiceChannelCD?
            SysLastUserID            = @UserID,
            SysLastUpdatedDate       = @now
      WHERE	ClaimAspectID = @ClaimAspectID
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim_aspect', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Update safety devices deployed.  This is done by deleting the existing safety device records and inserting new ones.

    DELETE FROM dbo.utb_vehicle_safety_device
      WHERE ClaimAspectID = @ClaimAspectID

    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Deletion failure

        RAISERROR('106|%s|utb_vehicle_safety_device', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    INSERT INTO dbo.utb_vehicle_safety_device
    (
        ClaimAspectID,
        SafetyDeviceID,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @ClaimAspectID,
            value,
            @UserID,
            @now
      FROM  dbo.ufnUtilityParseString( @SafetyDevicesDeployed, ',', 1 )     -- 1 means to trim spaces
        
    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_vehicle_safety_device', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    -- Update vehicle impacts.  This is done by deleting the existing impact records and inserting new ones.

    DELETE FROM dbo.utb_vehicle_impact
       WHERE ClaimAspectID = @ClaimAspectID


    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Deletion failure

        RAISERROR('106|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    -- Insert vehicle impacts into utb_vehicle_impact

    INSERT INTO dbo.utb_vehicle_impact
    (
        ClaimAspectID,
        ImpactID,
        CurrentImpactFlag,
        PrimaryImpactFlag,
        PriorImpactFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT @ClaimAspectID,
           value,
           1,
           0,
           0,
           @UserID,
           @now
      FROM dbo.ufnUtilityParseString( @ImpactPoints, ',', 1 )     -- 1 means to trim spaces
        
    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    -- Now assume the first impact is the primary and set the flag in the temporary table

    UPDATE  dbo.utb_vehicle_impact
      SET   PrimaryImpactFlag = 1
      WHERE ClaimAspectID = @ClaimAspectID
        AND ImpactID = (SELECT TOP 1 value FROM dbo.ufnUtilityParseString( @ImpactPoints, ',', 1 ))

    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    -- Now update existing impact records to set the prior impact flag on any duplicates in the Prior Impact Point parameter
        
    UPDATE  dbo.utb_vehicle_impact
      SET   PriorImpactFlag = 1
      WHERE ClaimAspectID = @ClaimAspectID
        AND ImpactID IN (SELECT value FROM dbo.ufnUtilityParseString( @PriorImpactPoints, ',', 1 ))     -- 1 means to trim spaces
        
    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    -- Now insert any prior impact records that do not yet exist 

    INSERT INTO dbo.utb_vehicle_impact
    (
        ClaimAspectID,
        ImpactID,
        CurrentImpactFlag,
        PrimaryImpactFlag,
        PriorImpactFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @ClaimAspectID,
            value,
            0,
            0,
            1,
            @UserID,
            @now
      FROM  dbo.ufnUtilityParseString( @PriorImpactPoints, ',', 1 )     -- 1 means to trim spaces
      WHERE value NOT IN (SELECT  ImpactID 
                            FROM  dbo.utb_vehicle_impact 
                            WHERE ClaimAspectID = @ClaimAspectID)

    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    

    -- Now start the events
    -- If the currentAssignmentTypeID changed, record it in history
/*  --Project:210474 APD Remarked-off the following because it does not apply anymore M.A.20070109
    IF @OldAssignmentTypeID <> @AssignmentTypeID
    BEGIN
        SET @EventName = 'Assignment Type Changed'
        
        SELECT @EventID = EventID
        FROM dbo.utb_event
        WHERE Name = @EventName
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              

        IF @EventID IS NULL
        BEGIN
            -- Event Name not found
        
            RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END

        SET @HistoryDescription = 'Assignment Type changed from ' + 
        (SELECT isNull(Name, '<Unknown>') + ( SELECT isNull(' (' + Name + ')', '')
                                                FROM dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD')
                                                WHERE Code = ServiceChannelDefaultCD
                                            )
            FROM dbo.utb_assignment_type 
            WHERE AssignmentTypeID = @OldAssignmentTypeID) + ' to ' +
        (SELECT isNull(Name, '<Unknown>') + ( SELECT isNull(' (' + Name + ')', '')
                                                FROM dbo.ufnUtilityGetReferenceCodes('utb_assignment_type', 'ServiceChannelDefaultCD')
                                                WHERE Code = ServiceChannelDefaultCD
                                            )
            FROM dbo.utb_assignment_type 
            WHERE AssignmentTypeID = @AssignmentTypeID)
        
        
        -- Make the workflow call
        
        EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectID = @ClaimAspectID,
                                    @Description = @HistoryDescription,
                                    @UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- APD Workflow Notification failed

            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- reset the id so that we can safely reuse the variable
        SET @EventID = NULL
        SET @HistoryDescription = NULL
    END
--Project:210474 APD Remarked-off the above because it does not apply anymore M.A.20070109
*/
    
    -- Check the Inspection Date
    IF @OldInspectionDate IS NULL AND @InspectionDate IS NOT NULL AND @RepairStartConfirmed = 0
    BEGIN
        -- Inspection Date was entered. Throw the Inspection Scheduled event.
        
        SET @EventName ='Inspection Scheduled' 
        
        SELECT @EventID = EventID
        FROM dbo.utb_event
        WHERE Name = @EventName
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              

        IF @EventID IS NULL
        BEGIN
            -- Event Name not found
        
            RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        SELECT @PertainsToName = cat.Name + ' ' + convert(varchar, ClaimAspectNumber)
        FROM dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.claimAspectTypeID = cat.ClaimAspectTypeID)
        WHERE ca.ClaimAspectID = @ClaimAspectID
          
        SET @HistoryDescription = @EventName + ' for ' + @PertainsToName + '. Scheduled for:' + convert(varchar, @InspectionDate, 101)
        
        -- Make the workflow call
        
        EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectID = @ClaimAspectID,
                                    @Description = @HistoryDescription,
                                    @UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- APD Workflow Notification failed

            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- reset the id so that we can safely reuse the variable
        SET @EventID = NULL
    END
    
    -- if the repair date was entered and the old value was a null then it is a fresh entry
    IF (@OldRepairStartDate IS NULL AND @RepairStartDate IS NOT NULL) OR 
       (@OldRepairEndDate IS NULL AND @RepairEndDate IS NOT NULL)
    BEGIN
        -- Prepare to throw Repair Scheduled event
        
        SET @EventName ='Repair Scheduled' 
        
        SELECT @EventID = EventID
        FROM dbo.utb_event
        WHERE Name = @EventName
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              

        IF @EventID IS NULL
        BEGIN
            -- Event Name not found
        
            RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        SELECT @PertainsToName = cat.Name + ' ' + convert(varchar, ClaimAspectNumber)
        FROM dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.claimAspectTypeID = cat.ClaimAspectTypeID)
        WHERE ca.ClaimAspectID = @ClaimAspectID
          
        SET @HistoryDescription = @EventName + ' for ' + @PertainsToName + '.'
        
        IF @RepairStartDate IS NOT NULL
        BEGIN
            SET @HistoryDescription = @HistoryDescription + ' Repair Start Date:' + convert(varchar, @RepairStartDate, 101)
        END

        IF @RepairEndDate IS NOT NULL
        BEGIN
            SET @HistoryDescription = @HistoryDescription + ' Repair End Date:' + convert(varchar, @RepairEndDate, 101)
        END
        
        -- Make the workflow call
        
        EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectID = @ClaimAspectID,
                                    @Description = @HistoryDescription,
                                    @UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- APD Workflow Notification failed

            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- reset the id so that we can safely reuse the variable
        SET @EventID = NULL
        
    END
    
    -- Change in repair schedule
    
    IF (@OldRepairStartDate IS NOT NULL AND @OldRepairStartDate <> @RepairStartDate) OR
       (@OldRepairEndDate IS NOT NULL AND @OldRepairEndDate <> @RepairEndDate)
    BEGIN
        -- Prepare to throw Repair Scheduled Changed event
        
        SET @EventName ='Repair Schedule Changed' 
        
        SELECT @EventID = EventID
        FROM dbo.utb_event
        WHERE Name = @EventName
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END              

        IF @EventID IS NULL
        BEGIN
            -- Event Name not found
        
            RAISERROR('102|%s|"%s"|utb_event', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- Get Note Type ID for Claim
        SELECT  @NoteTypeIDClaim = NoteTypeID
          FROM  dbo.utb_note_type
          WHERE Name = 'Claim'

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        IF @NoteTypeIDClaim IS NULL
        BEGIN
           -- Note Type ID for Claim

            RAISERROR('102|%s|"Note Type: Claim"|utb_note_type', 16, 1, @ProcName)
            RETURN
        END
        
        -- Get the Entity status id
        SELECT @EntityStatusID = cas.StatusID
        FROM dbo.utb_claim_aspect ca
        INNER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID  --Project:210474 APD Added reference to get the values, when we did the code merge M.A.20061120
        WHERE ca.ClaimAspectID = @ClaimAspectID
        and   cas.ServiceChannelCD is null
        and   cas.StatusTypeCD is NULL
        
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        -- Generate the pertains to text
        
        SELECT @PertainsToName = cat.Name + ' ' + convert(varchar, ClaimAspectNumber)
        FROM dbo.utb_claim_aspect ca
        LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.claimAspectTypeID = cat.ClaimAspectTypeID)
        WHERE ca.ClaimAspectID = @ClaimAspectID
          
        IF @OldRepairStartDate <> @RepairStartDate AND
           @OldRepairEndDate <> @RepairEndDate
        BEGIN
            SET @HistoryDescription = @PertainsToName + ' Repair start date ' + ' changed to ' +
                                   convert(varchar, @RepairStartDate, 101) + ' and ' +
                                   'Repair end date changed to ' + convert(varchar, @RepairEndDate, 101) + '. Reason: ' + @RepairScheduleChangeReason
        END
        ELSE IF @OldRepairStartDate <> @RepairStartDate
        BEGIN
            SET @HistoryDescription = @PertainsToName + ' Repair start date ' + ' changed to ' +
                                   convert(varchar, @RepairStartDate, 101) + '. Reason: ' +  @RepairScheduleChangeReason
        END
        ELSE IF @OldRepairEndDate <> @RepairEndDate
        BEGIN
            SET @HistoryDescription = @PertainsToName + ' Repair end date changed to ' + convert(varchar, @RepairEndDate, 101) + '. Reason: ' + @RepairScheduleChangeReason
        END
        
        -- Make the workflow call
        
        EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectID = @ClaimAspectID,
                                    @Description = @HistoryDescription,
                                    @UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- APD Workflow Notification failed

            RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        -- Record the change to the repair schedule history table
        IF @RepairScheduleChangeReason IS NOT NULL
        BEGIN
            -- Get the Reason id for the text
            SELECT @ReasonID = ReasonID
            FROM dbo.utb_service_channel_schedule_change_reason --Project:210474 APD Modified to support the schema change M.A.20070111
            WHERE Name = @RepairScheduleChangeReason

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END
        
            IF @ReasonID IS NULL
            BEGIN
                -- The reason text could not be found in the reference. Consider this as an "Other"
            
                -- Get the "Other" reason id
                SELECT @ReasonID = ReasonID
                FROM dbo.utb_service_channel_schedule_change_reason --Project:210474 APD Modified to support the schema change M.A.20070111
                WHERE Name = 'Other'

                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error

                    RAISERROR('99|%s', 16, 1, @ProcName)
                    RETURN
                END              

                IF @ReasonID IS NULL
                BEGIN
                    -- "Other" reason not found
        
                    RAISERROR('102|%s|"%s"|utb_service_channel_schedule_change_reason', 16, 1, @ProcName, 'Other') --Project:210474 APD Modified to support the schema change M.A.20070111
                    ROLLBACK TRANSACTION
                    RETURN
                END
            END

            -- Insert repair history
        
            INSERT INTO utb_repair_schedule_history
            (ClaimAspectID, 
             CreatedUserID, 
             ReasonID, 
             ChangeComment, 
             ChangeDate, 
             RepairEndNewDate, 
             RepairEndOldDate, 
             RepairStartNewDate, 
             RepairStartOldDate, 
             SysLastUserID, 
             SysLastUpdatedDate)
            VALUES
            (@ClaimAspectID,
             @UserID,
             @ReasonID,
             @RepairScheduleChangeReason,
             @now,
             @RepairEndDate,
             @OldRepairEndDate,
             @RepairStartDate,
             @OldRepairStartDate,
             @UserID,
             @now
            )


            -- Check error value
    
            IF @@ERROR <> 0
            BEGIN
                -- Insertion failure

                RAISERROR('105|%s|utb_service_channel_schedule_change_reason', 16, 1, @ProcName)--Project:210474 APD Modified to support the schema change M.A.20070111
                ROLLBACK TRANSACTION 
                RETURN
            END    
        END
        
        -- Insert the new note
        exec uspNoteInsDetail @ClaimAspectID, @NoteTypeIDClaim, @EntityStatusID, @HistoryDescription, @UserID
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('104|%s|%s', 16, 1, @ProcName, 'uspNoteInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END    
        
        -- Get the repair started confirmation flag
        -- Update the "Verify Repair Complete" task if repair under progress
        
        IF @RepairStartConfirmed = 1
        BEGIN
            -- Repair started. So update the Verify repairs completed task to the new date.
            
            -- First get the checklist id
            SELECT @CheckListID = c.CheckListID
            FROM dbo.utb_checklist c
            LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON c.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID --Project:210474 APD Added reference to get the values, when we did the code merge M.A.20061120
            LEFT JOIN dbo.utb_task t ON (c.TaskID = t.TaskID)
            WHERE t.Name = 'Repair Completion Verification Required'
              AND casc.ClaimAspectID = @ClaimAspectID
        
            IF @CheckListID IS NOT NULL
            BEGIN
                -- Defect 4221 - Task alaram date to be set a day before the repair end date
                SET @RepairEndDate = DATEADD(day, -1, @RepairEndDate)
                -- Defect 4221 - End of changes

                -- Task found. Set the alarm date to the new end date
                UPDATE dbo.utb_checklist
                SET AlarmDate = @RepairEndDate
                WHERE CheckListID = @CheckListID

                -- Check error value
    
                IF @@ERROR <> 0
                BEGIN
                    -- Update failure

                    RAISERROR('104|%s|utb_checklist', 16, 1, @ProcName)
                    ROLLBACK TRANSACTION 
                    RETURN
                END    
            END
        END        
    END

    
    COMMIT TRANSACTION VehicleDescriptionUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Vehicle Level
            NULL AS [Vehicle!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Vehicle Level
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspVehicleDescriptionUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspVehicleDescriptionUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/