-- SELECT * FROM utb_hyperquest_history_processed
ALTER TABLE utb_hyperquest_history_processed ADD EventStatus VARCHAR(50) 
GO
UPDATE utb_hyperquest_history_processed SET EventStatus = 'Processed'
GO

-- Drop Key from the above table
ALTER TABLE utb_hyperquest_history_processed DROP CONSTRAINT upk_history_processed_log
GO
