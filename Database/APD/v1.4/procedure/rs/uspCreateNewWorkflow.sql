-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewWorkflow' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateNewWorkflow 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateNewWorkflow
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client workflow by inserting a new record into the 
*				utb_workflow table modeled after @iModelInsuranceCompanyID
*
* PARAMETERS:  
*
*   @iNewInsuranceCompanyID
*   @iModelInsuranceCompanyID
*	@iCreatedByUserID
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateNewWorkflow
	@iNewInsuranceCompanyID INT
	, @iModelInsuranceCompanyID INT
	, @iCreatedByUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @dtNow AS DATETIME 
    DECLARE @ProcName AS VARCHAR(50)

    SET @ProcName = 'uspCreateNewWorkflow'

    -- Set Database options
    SET NOCOUNT ON
	SET @dtNow = CURRENT_TIMESTAMP

	-- Check to make sure the new insurance company id exists
    IF  (@iNewInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iNewInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iNewInsuranceCompanyID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure the model insurance company id exists
    IF  (@iModelInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iModelInsuranceCompanyID))
    BEGIN
        -- Invalid Model Insurance Company ID
        RAISERROR('101|%s|@iModelInsuranceCompanyID|%u', 16, 1, @ProcName, @iModelInsuranceCompanyID)
        RETURN
    END

	-- Add the new
	DECLARE db_cursor CURSOR FOR 
	SELECT 
		@iNewInsuranceCompanyID
		, IgnoreDefaultFlag
		, OriginatorID
		, OriginatorTypeCD
		, @iCreatedByUserID
		, @dtNow
	FROM
		dbo.utb_workflow
	WHERE
		InsuranceCompanyID = @iModelInsuranceCompanyID	

	DECLARE @iIgnoreDefaultFlag INT
	DECLARE @iOriginatorID INT
	DECLARE @vOriginatorTypeCD VARCHAR(5)

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO 	
		@iNewInsuranceCompanyID
		, @iIgnoreDefaultFlag
		, @iOriginatorID
		, @vOriginatorTypeCD
		, @iCreatedByUserID
		, @dtNow

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		INSERT INTO dbo.utb_workflow
		SELECT
		(SELECT MAX(WorkflowID)+1 FROM dbo.utb_workflow)
		, @iNewInsuranceCompanyID
		, @iIgnoreDefaultFlag
		, @iOriginatorID
		, @vOriginatorTypeCD
		, @iCreatedByUserID
		, @dtNow

		FETCH NEXT FROM db_cursor INTO 	
		@iNewInsuranceCompanyID
		, @iIgnoreDefaultFlag
		, @iOriginatorID
		, @vOriginatorTypeCD
		, @iCreatedByUserID
		, @dtNow

	END
	CLOSE db_cursor  
	DEALLOCATE db_cursor 		
	 
	IF EXISTS(
		SELECT 
			* 
		FROM 
			dbo.utb_workflow
		WHERE 
			InsuranceCompanyID = @iNewInsuranceCompanyID
	)
	BEGIN
		SELECT 1 RetCode
	END
	ELSE
	BEGIN
		SELECT 0 RetCode
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewWorkflow' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateNewWorkflow TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateNewWorkflow TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/