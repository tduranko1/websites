-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceDel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspInvoiceDel 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspInvoiceDel
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     "Deletes" (ie. Disables) an invoice record
*
* PARAMETERS:  
* (I) @InvoiceID            The Invoice ID record to "delete"
* (I) @UserID               The user id performing the "delete"
* (I) @SyslastUpdatedDate   The "previous" updated date
* (I) @NotifyEvent          A flag specifying whether this proc handles workflow notification
*
* RESULT SET:
*       None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspInvoiceDel
    @InvoiceID            udt_std_id_big,
    @DispatchNumber       udt_std_desc_short,
    @UserID               udt_std_id_big,
    @SyslastUpdatedDate   varchar(30),
    @NotifyEvent          udt_std_flag
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now                AS udt_std_datetime

    DECLARE @ClaimAspectID      AS udt_std_id_big
    DECLARE @EventID            AS udt_std_id
    DECLARE @Description        AS udt_std_desc_short
    DECLARE @HistoryDescription AS udt_std_desc_long
    DECLARE @UpdateFlag         AS udt_std_int_tiny
    DECLARE @DeleteFlag         AS udt_std_int_tiny
    DECLARE @RecordingUserID    AS udt_std_int_big
    DECLARE @SentStatus         AS udt_std_name
    DECLARE @ItemTypeCD         AS udt_std_cd
    DECLARE @FeeItemTypeCD      AS udt_std_cd
    DECLARE @AspectDescription  AS varchar(30)
    DECLARE @StatusCD           AS udt_std_cd
    DECLARE @SentToIngresDate   AS udt_std_datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspInvoiceDel'

    -- Set Database options    
    SET NOCOUNT ON
    
    IF LEN(LTRIM(RTRIM(@DispatchNumber))) = 0 SET @DispatchNumber = NULL

    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP

    -- Validate the Last updated date
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_invoice', @InvoiceID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.
        RETURN
    END

        
    IF (@DispatchNumber IS NOT NULL) AND NOT EXISTS (SELECT InvoiceID FROM dbo.utb_invoice 
                                                                      WHERE DispatchNumber = @DispatchNumber
                                                                        AND EnabledFlag = 1)
    BEGIN
        -- Invalid Dispatch Number
        RAISERROR('101|%s|@DispatchNumber|%u', 16, 1, @ProcName, @DispatchNumber)
        RETURN 
    END
        
    -- Check to make sure a valid User id was passed
    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    
    SELECT @DeleteFlag = DeleteFlag
      FROM ufnUtilityGetCRUD(@UserID, 'Client Billing', DEFAULT, DEFAULT)

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END 

    IF @DeleteFlag = 0 
    BEGIN    
        
        SELECT  @RecordingUserID = RecordingUserID 
          FROM  dbo.utb_invoice 
          WHERE @InvoiceID = InvoiceID
    
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END 
     
        IF @UserID <> @RecordingUserID 
        BEGIN
        -- This is not the user who created the Billing Item
            RAISERROR  ('1|Cannot remove Invoice item.  You may only remove Invoice Items which you created.  A Supervisor can remove this Invoice Item.', 16, 1, @ProcName)
            RETURN    
        END
    END

    -- Holds items to be deleted.
    DECLARE @tmpInvoice TABLE (InvoiceID            bigint        NOT NULL,
                               ClaimAspectID        bigint        NOT NULL,
                               ItemTypeCD           varchar(4)    NOT NULL,
                               AspectDescription    varchar(30)   NULL)
    
	--------------------------------------------------------
	-- 22Jan2013 - TVD - ReDesigned this section because we
	-- have bulk bill clients that are also single dispatch
	-- number clients.  This code currently deletes all fees.
	-- I'm commenting out the @DispatchNumber check.
	--------------------------------------------------------
	-- No dispatch was passed; delete a single invoice item only.                           
    --IF (@DispatchNumber IS NULL)
    --BEGIN
      INSERT INTO @tmpInvoice
      SELECT InvoiceID,
             i.ClaimAspectID,
             ItemTypeCD,
             cat.Name + ' ' + convert(varchar(10), ca.ClaimAspectNumber)
      FROM dbo.utb_invoice i INNER JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
                             INNER JOIN dbo.utb_claim_aspect_type cat ON ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
      WHERE InvoiceID = @InvoiceID 
    --END
    --ELSE  -- Dispatch Number was passed; deleted all invoice items associated with this dispatch.
    --BEGIN
    --  INSERT INTO @tmpInvoice
    --  SELECT InvoiceID,
    --         i.ClaimAspectID,
    --         ItemTypeCD,
    --         cat.Name + ' ' + convert(varchar(10), ca.ClaimAspectNumber)
    --  FROM dbo.utb_invoice i INNER JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
    --                         INNER JOIN dbo.utb_claim_aspect_type cat ON ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
    --  WHERE DispatchNumber = @DispatchNumber 
    --END

--    -- Get information we will require later from the invoice record
--    SELECT  @ClaimAspectID = i.ClaimAspectID,
--            @Description = i.Description,
--            @ItemTypeCD = i.ItemTypeCD,
--            @AspectDescription = cat.Name + ' ' + convert(varchar(10), ca.ClaimAspectNumber)
--      FROM  dbo.utb_invoice i INNER JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
--                              INNER JOIN dbo.utb_claim_aspect_type cat ON ca.ClaimAspectTypeID = cat.ClaimAspectTypeID
--      WHERE InvoiceID = @InvoiceID

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END 
   
    
         
    ----------------------------------------------------------------------------------------------------------------------------------
    
    -- Begin Update
    BEGIN TRANSACTION InvoiceDelTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    UPDATE  dbo.utb_invoice
      SET   EnabledFlag         = 0,    
            SysLastUserID       = @UserID,
            SysLastUpdatedDate  = @now
      FROM @tmpInvoice t INNER JOIN dbo.utb_invoice i ON t.InvoiceID = i.InvoiceID
      

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
        
    -- Check error value    
    IF @error <> 0
    BEGIN
        -- Update failed
        RAISERROR('104|%s|utb_invoice', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

        
    -- update the billing service records
    UPDATE  dbo.utb_invoice_service
    SET   EnabledFlag         = 0,        -- Set invoice record to disabled
          SysLastUserID       = @UserID,
          SysLastUpdatedDate  = @now
    FROM @tmpInvoice t INNER JOIN dbo.utb_invoice_service i ON t.InvoiceID = i.InvoiceID
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
          
    -- Check error value      
    IF @error <> 0
    BEGIN
        -- Update failed
        RAISERROR('104|%s|utb_invoice_service', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    
    -- Determine if a handling fee is being removed.
    SET @ClaimAspectID = NULL
    
    SELECT @ClaimAspectID = t.ClaimAspectID 
    FROM dbo.utb_invoice i INNER JOIN @tmpInvoice t ON i.ClaimAspectID = t.ClaimAspectID AND i.ItemTypeCD = t.ItemTypeCD
    WHERE i.ItemTypeCD = 'F' 
      AND i.FeeCategoryCD = 'H'
      AND i.InvoiceID = @InvoiceID
      
    -- If one of the fees being removed is the handling fee, set the ClaimAspect's disposition to NULL.
    IF (@ClaimAspectID IS NOT NULL)
    BEGIN
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	The column being updated "DispositionTypeCD" is now in a different table.
		The DispositionTypeCD in utb_Claim_Aspect_Service_Channel should be upadated.
		Ask JR/JP abou tthe relationship between the tables.  It seems like there
		might/might not be a row in the table for the ClaimAspectID.
		M.A. 20061109
	*********************************************************************************/
	/* Original UPDATE Statement
        UPDATE dbo.utb_claim_aspect
        SET DispositionTypeCD = NULL
        WHERE ClaimAspectID = @ClaimAspectID
	*/
	--If a row for the ClaimAspectID Exists in the table then update M.A. 20061109
	if (SELECT Count(*) from utb_Claim_Aspect_Service_Channel WHERE ClaimAspectID = @ClaimAspectID) <> 0
		BEGIN
			UPDATE utb_Claim_Aspect_Service_Channel
			SET DispositionTypeCD = NULL
			WHERE ClaimAspectID = @ClaimAspectID
		END
    END            
        
    -- Notify APD of the billing event if we have been instructed to do so.  (If not, it is up to the caller to notify
    -- APD of the event.)
    IF @NotifyEvent = 1
    BEGIN        
        DECLARE @EventDesc  udt_std_desc_short
        
        DECLARE csrInvoice CURSOR FOR SELECT InvoiceID, ClaimAspectID, ItemTypeCD, AspectDescription FROM @tmpInvoice
        
        OPEN csrInvoice
        
        IF @@ERROR <> 0
        BEGIN
          -- SQL Server Error    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
                
        FETCH NEXT FROM csrInvoice INTO @InvoiceID, @ClaimAspectID, @ItemTypeCD, @AspectDescription
        
        IF @@ERROR <> 0
        BEGIN
          -- SQL Server Error    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        WHILE (@@FETCH_STATUS = 0)
        BEGIN
        
        
            -- Gather info for the event to be thrown
            IF (@ItemTypeCD = @FeeItemTypeCD)
            BEGIN
              SELECT  @EventID = EventID FROM  dbo.utb_event WHERE Name = 'Fee Removed'
              SET @HistoryDescription = 'Fee for ' + @Description + ' removed'
              SET @EventDesc = 'Fee Removed'
            END
            ELSE
            BEGIN
              SELECT  @EventID = EventID FROM  dbo.utb_event WHERE Name = 'Payment Removed'
              SET @HistoryDescription = 'Payment for ' + @AspectDescription + ' (' + @Description + ') removed'
              SET @EventDesc = 'Payment Removed'
            END
        
            IF @@ERROR <> 0
            BEGIN
              -- SQL Server Error        
                RAISERROR('99|%s', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        
            -- Throw the event here
            IF @EventID IS NULL
            BEGIN
                -- Reference record not found
                RAISERROR('102|%s|%s|utb_event', 16, 1, @ProcName, @EventDesc)
                ROLLBACK TRANSACTION 
                RETURN
            END
            
            exec uspWorkflowNotifyEvent @EventID = @EventID,
                                        @ClaimAspectID = @ClaimAspectID,
                                        @Description = @HistoryDescription,
                                        @UserID = @UserID

            -- Check error value        
            IF @@ERROR <> 0
            BEGIN
              -- SQL Server Error        
                RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventDesc)
                ROLLBACK TRANSACTION
                RETURN
            END
        
            FETCH NEXT FROM csrInvoice INTO @InvoiceID, @ClaimAspectID, @ItemTypeCD, @AspectDescription
        END
        
        CLOSE csrInvoice        
        DEALLOCATE csrInvoice
        
        IF @@ERROR <> 0
        BEGIN
          -- SQL Server Error    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END
        
    COMMIT TRANSACTION InvoiceDelTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    RETURN @rowcount
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceDel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspInvoiceDel TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspInvoiceDel TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/