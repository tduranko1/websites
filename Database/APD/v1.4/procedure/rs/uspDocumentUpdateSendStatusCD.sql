-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentUpdateSendStatusCD' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDocumentUpdateSendStatusCD 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspDocumentUpdateSendStatusCD
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspDocumentUpdateSendStatusCD
    @DocumentID     udt_std_id_big  = null,
    @TransactionID  udt_std_desc_mid = null,
    @SendStatusCD   udt_std_cd
AS
BEGIN

    IF (@DocumentID IS NULL) AND (@TransactionID IS NULL)
    BEGIN
        -- Can not have both items null,
        RAISERROR('1|Either a DocumentID or TransactionID must be provided',16,1)
        RETURN
    END
    
    IF EXISTS (SELECT * FROM utb_document
               WHERE (DocumentID = @DocumentID AND @TransactionID IS NULL) OR
                      (DocumentID IS NULL AND SendToCarrierTransactionID = @TransactionID) OR
                      (DocumentID = @DocumentID AND SendToCarrierTransactionID = @TransactionID))
    BEGIN
        BEGIN TRANSACTION
        UPDATE utb_document
        SET SendToCarrierStatusCD = @SendStatusCD
        WHERE (DocumentID = @DocumentID AND @TransactionID IS NULL) OR
              (DocumentID IS NULL AND SendToCarrierTransactionID = @TransactionID) OR
              (DocumentID = @DocumentID AND SendToCarrierTransactionID = @TransactionID)
        IF @@ERROR <> 0
        BEGIN
            ROLLBACK
            RETURN
        END
        COMMIT                                      
    END
    ELSE
    BEGIN
        RAISERROR('No document can be found for parameters passed.',16,1)
    END
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentUpdateSendStatusCD' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDocumentUpdateSendStatusCD TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/