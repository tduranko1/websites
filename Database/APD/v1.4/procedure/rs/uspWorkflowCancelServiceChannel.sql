-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowCancelServiceChannel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowCancelServiceChannel 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowCancelServiceChannel
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Closes 1 or more claim exposures.  
*
* PARAMETERS:
* (I) @LynxID               LynxID of the claim to operate on  
* (I) @ClaimAspectList      Comma delimited list of exposures (ClaimAspectIDs) to close
* (I) @UserID               The User performing this action
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

create procedure dbo.uspWorkflowCancelServiceChannel
  @ClaimAspectServiceChannelID        udt_std_id_big,
  @UserID                             udt_std_id,
  @Comment                            udt_std_note,
  @ApplicationCD                      udt_std_cd = 'APD'
as
begin
  
  declare @debug                        udt_std_flag
  declare @now                          udt_std_datetime
  declare @ProcName                     varchar(30)
  
  set @ProcName = 'uspWorkflowCancelServiceChannel'
  set @now = getdate()
  
  declare @EventIDCancelServiceChannel  udt_std_id
  declare @EventIDVehicleCancel         udt_std_id
  declare @EventIDVehicleClose          udt_std_id
  declare @EventIDClaimClose            udt_std_id
  declare @EventIDReICancelled          udt_std_id
  declare @NoteTypeIDClaim              udt_std_id
  declare @StatusIDSvcChannelCancelled  udt_std_id
  
    
  
  declare @w_ClaimAspectID              udt_std_id_big
  declare @w_ClaimAspectIDClaim         udt_std_id_big
  declare @w_ClaimAspectTypeIDClaim     udt_std_id
  declare @w_VehicleWillCancel          udt_std_flag
  declare @w_VehicleWillClose           udt_std_flag
  declare @w_ClaimWillClose             udt_std_flag
  declare @w_ServiceChannelCD           udt_std_cd
  
  declare @w_EventDescription           udt_std_desc_long
  declare @w_EventID                    udt_std_id
  declare @w_ChecklistID                udt_std_id_big
  declare @w_ChecklistLastUpdatedDate   varchar(30)
  declare @w_ChecklistIDNote            udt_std_desc_mid
  declare @w_LynxID                     udt_std_id_big
  
  
  set @debug = 0
  set @ProcName = 'uspWorkflowCancelServiceChannel'
  

  if (@ClaimAspectServiceChannelID IS NULL) OR
     (not exists (select ClaimAspectServiceChannelID 
                  from dbo.utb_claim_aspect_service_channel 
                  where ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID))
  begin
    raiserror('101|%s|@ClaimAspectServiceChannelID passed is null or invalid',16,1,@ProcName)
    return
  end 
  
  
  
  select @NoteTypeIDClaim = NoteTypeID from dbo.utb_note_type where Name = 'Claim'
  
  if @NoteTypeIDCLaim is null
  begin
    raiserror('102|%s|Note Type: Claim"|utb_note_type',16,1,@ProcName)
    return
  end    
  
  select @w_ClaimAspectTypeIDClaim = ClaimAspectTypeID
  from utb_claim_aspect_type
  where Name = 'Claim'
  
  if @@error <>0
  begin
    raiserror('99|%s',16,1,@ProcName)
    return
  end
  
  if @w_ClaimAspectTypeIDClaim is null
  begin
    raiserror('101|%s|Claim Aspect Type for Claim not defined.',16,1,@ProcName)
    return
  end
                  
  -- Vehicle Service Channel Cancelled Event
  select @EventIDCancelServiceChannel = EventID
  from utb_event
  where Name = 'Vehicle Service Channel Cancelled'

  if @@error <> 0
  begin
    raiserror('99|%s',16,1,@ProcName)
    return
  end

  if @EventIDCancelServiceChannel is null
  begin
    raiserror('101|%s|Vehicle Service Channel Cancelled event not defined',16,1,@ProcName)
    return
  end
  
  -- Vehicle Cancelled Event
  select @EventIDVehicleCancel = EventID
  from utb_event
  where Name = 'Vehicle Cancelled'

  if @@error <> 0
  begin
    raiserror('99|%s',16,1,@ProcName)
    return
  end

  if @EventIDVehicleCancel is null
  begin
    raiserror('101|%s|Vehicle cancelled event not defined',16,1,@ProcName)
    return
  end
  
  -- Vehicle Close Event
  
  select @EventIDVehicleClose = EventID
  from utb_event
  where Name = 'Vehicle Closed'

  if @@error <> 0
  begin
    raiserror('99|%s',16,1,@ProcName)
    return
  end

  if @EventIDVehicleClose is null
  begin
    raiserror('101|%s|Vehicle closed event not defined',16,1,@ProcName)
    return
  end
  
  
  -- Claim Close Event
  select @EventIDClaimClose = EventID
  from utb_event
  where Name = 'Claim Closed'

  if @@error <> 0
  begin
    raiserror('99|%s',16,1,@ProcName)
    return
  end

  if @EventIDClaimClose is null
  begin
    raiserror('101|%s|Claim closed event not defined',16,1,@ProcName)
    return
  end
  
  
    
  -- validate user id
  
  if (dbo.ufnUtilityIsUserActive(@UserID,@ApplicationCD, NULL) = 0)
  begin
    -- Invalid User ID
    raiserror('101|%s|@UserID|%u',16,1,@ProcName,@UserID)
    return
  end
  
  
  select @w_ClaimAspectID     = casc.ClaimAspectID,
         @w_ServiceChannelCD  = casc.ServiceChannelCD,
         @w_LynxID            = ca.LynxID
  from dbo.utb_claim_aspect_service_channel casc inner join dbo.utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
  where casc.ClaimAspectServiceChannelId = @ClaimAspectServiceChannelID   
  
  if @@error <> 0
  begin
    raiserror('99|%s',16,1,@ProcName)
    return
  end
  
  select @w_ClaimAspectIDClaim = ClaimAspectID
  from utb_claim_aspect
  where LynxID = @w_LynxID 
    and ClaimAspectTypeID = @w_ClaimAspectTypeIDClaim
  
  -- build a list of all service channels for claim aspect w/ its associated status
  set @w_VehicleWillCancel = 0
  set @w_VehicleWillClose = 0
  set @w_ClaimWillClose = 0
  
  if not exists (select cas.StatusID
                 from utb_claim_aspect_status cas
                 inner join utb_status s on (cas.StatusID = s.StatusID)
                 where cas.ClaimAspectID = @w_ClaimAspectID
                   and cas.StatusTypeCD = 'SC'
                   and s.Name <> 'Cancelled'
                   and cas.ServiceChannelCD <> @w_ServiceChannelCD)
  begin
    -- All other service channels for this vehicle are in cancelled state.'
    -- are there any billed items for this Vehicle?
    if exists (select InvoiceID
               from dbo.utb_invoice
               where ClaimAspectID = @w_ClaimAspectID
                 and EnabledFlag = 1
                 and ItemTypeCD = 'F')
    begin
      -- fee exists for this vehicle.  Processing of this request would cancel the vehicle
      -- thus we can not perform this request
      raiserror('1|Cancelling the service channel(s) specified would cause cancellation of vehicle.  There is already a fee applied to this vehicle. Vehicle can not be cancelled.',16,1)                 
      return
    end
    else  
    begin     
      set @w_VehicleWillCancel = 1
    end
  end
  else
  begin
    set @w_VehicleWillCancel = 0
    -- we need to see if cancelling this service channel will cause closure of vehicle
    if not exists (select ClaimAspectServiceChannelID
                   from dbo.utb_claim_aspect_service_channel casc 
                    inner join dbo.utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
                    inner join dbo.utb_claim_aspect_status cas on ((ca.ClaimAspectID = cas.ClaimAspectID) and
                                                                   (casc.ServiceChannelCD = cas.ServiceChannelCD))                                                   
                    where ca.ClaimAspectID = @w_ClaimAspectID 
                      and casc.ClaimAspectServiceChannelID <> @ClaimAspectServiceChannelID
                      and cas.StatusID IN (select StatusID from utb_status where Name='Active' and StatusTypeCD = 'SC'))
    begin
      set @w_VehicleWillClose = 1    
    end                      
  end
  
  select @StatusIDSvcChannelCancelled = StatusID 
  from utb_status 
  where StatusTypeCD = 'SC'
    and ServiceChannelCD = @w_ServiceChannelCD
    and Name = 'Cancelled'

  if @@error <>0
  begin
    raiserror('99|%s',16,1,@ProcName)
    return
  end
  
  if @StatusIDSvcChannelCancelled is null
  begin
    raiserror('101|%s|Cancelled status for %s not defined.',16,1,@ProcName, @w_ServiceChannelCD)
    return
  end      
   
  
  if @w_VehicleWillCancel = 1 or @w_VehicleWillClose = 1
  begin
    -- Now we must check if all other vehicles are either closed, cancelled, or voided
    -- if so we'll need to close the claim
    set @w_ClaimWillClose = dbo.ufnUtilityWillCloseClaim(@w_ClaimAspectID)
  end               
  
  -- Now cancel to service channel
  
  begin transaction WorkflowCancelServiceChannel1
  
  -- First we need to mark any outstanding tasks for service channel.
  declare csrTasks cursor for
  select c.ChecklistID, 
         dbo.ufnUtilityGetDateString(c.SysLastUpdatedDate),
         t.Name
  from dbo.utb_checklist c inner join utb_task t on (c.TaskID = t.TaskID)
  where ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
  
  open csrTasks
  
  if @@error<>0
  begin
    raiserror('99|%s',16,1,@ProcName)
    rollback transaction
    return  
  end
  
  fetch next from csrTasks into @w_ChecklistID, @w_ChecklistLastUpdatedDate, @w_ChecklistIDNote

  if @@error <> 0
  begin  
    raiserror('99|%s',16,1,@ProcName)
    rollback transaction
    return
  end
  
  while @@fetch_status = 0
  begin
  
    set @w_ChecklistIDNote = 'The task "' + @w_ChecklistIDNote + '" for service channel "' +  @w_ServiceChannelCD + '" was marked as Not Applicable because service channel was cancelled.'
    if @debug = 1
    begin
      print ''
      print 'Closing CheckListID: ' + convert(varchar,@w_ChecklistID)
    end
  
    exec uspWorkflowCompleteTask @CheckListID = @w_ChecklistID,
                                 @NotApplicableFlag = 1,
                                 @NoteRequiredFlag = 1,
                                 @Note = @w_ChecklistIDNote,
                                 @NoteTypeID = @NoteTypeIDClaim,
                                 @UserID = @UserID,
                                 @SysLastUpdatedDate = @w_ChecklistLastUpdatedDate,
                                 @ApplicationCD = @ApplicationCD
    
    if @@error <> 0
    begin
      raiserror('104|%s|%s',16,1,@ProcName,'uspWorkflowCompleteTask')
      rollback transaction
      return
    end
    
    fetch next from csrTasks into @w_ChecklistID, @w_ChecklistLastUpdatedDate, @w_ChecklistIDNote

    if @@error <> 0
    begin  
      raiserror('99|%s',16,1,@ProcName)
      rollback transaction
      return
    end
                                     
  end
  
  close csrTasks
  deallocate csrTasks
  
  exec uspWorkflowNotifyEvent @EventID = @EventIDCancelServiceChannel,
                              @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                              @Description = 'Vehicle Service Channel Cancelled',
                              @UserID = @UserID,
                              @ConditionValue = @w_ServiceChannelCD

  if @@error <> 0 
  begin
    raiserror('99|%s',16,1,@ProcName)
    rollback transaction
    return
  end
  
  if exists (select cascd.DocumentID
             from utb_claim_aspect_service_channel_document cascd
             left join dbo.utb_document d on (cascd.DocumentID = d.DocumentID)             
             where cascd.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
               and d.ReinspectionRequestFlag = 1)
  begin
    -- cancel reinspection request
    update dbo.utb_document
    set ReinspectionRequestFlag = 0
    where DocumentID in (select cascd.DocumentID               
                         from utb_claim_aspect_service_channel_document cascd
                         left join dbo.utb_document d on (cascd.DocumentID = d.DocumentID)             
                         where cascd.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
                           and d.ReinspectionRequestFlag = 1)
    
    if @@error <> 0 
    begin
      -- update failure
      raiserror('104|%s|utb_document',16,1,@ProcName)
      rollback transaction
      return
    end             

    select @EventIDReICancelled = EventID from dbo.utb_event where Name = 'Reinspection Request Cancelled'
    
    if @@error<> 0
    begin
      raiserror('99|%s',16,1,@ProcName)
      rollback transaction
      return
    end
   
    exec uspWorkflowNotifyEvent @EventID = @EventIDReICancelled,
                               @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                               @Description = 'Reinspection Request Cancelled',
                               @UserID = @UserID
  
    if @@error <> 0 
    begin
      raiserror('107|%s|%s',16,1,@procName,'Reinspection Request Cancelled')
      rollback transaction
      return
    end  
  end  
  
  if @debug = 1
  begin
    Print 'Insert Note:'
    Print '  ClaimAspectServiceChannelID = ' + convert(varchar,@ClaimAspectServiceChannelID)
    Print '  NoteTypeID = ' + convert(varchar,@NoteTypeIDClaim)
    print '    StatusID = ' + convert(varchar, @StatusIDSvcChannelCancelled)
    print '        Note = ' + @Comment
    print '      UserID = ' + convert(varchar,@UserID)
    
    select * 
    from dbo.utb_claim_aspect_status cas 
    inner join utb_claim_aspect_service_channel casc ON (cas.ClaimAspectID = casc.ClaimAspectID)
    where casc.claimaspectservicechannelid = @claimaspectservicechannelid
  end
  
  exec uspNoteInsDetail @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                        @NoteTypeID = @NoteTypeIDClaim,
                        @StatusID = @StatusIDSvcChannelCancelled,
                        @Note = @Comment,
                        @UserID = @UserID

  if @@error<>0
  begin
    raiserror('104|%s|%s',16,1,@ProcName,'uspNoteInsDetail');
    rollback transaction
    return
  end                        
                                                    
    
  if @w_VehicleWillCancel = 1 or @w_VehicleWillClose = 1
  begin
    select @w_EventID = case when @w_VehicleWillCancel = 1 then @EventIDVehicleCancel
                             else @EventIDVehicleClose end,
           @w_EventDescription = case when @w_VehicleWillCancel = 1 then 'Vehicle ' + convert(varchar(10), ClaimAspectNumber) + ' Cancelled.'
                             else 'Vehicle ' + convert(varchar(10),ClaimAspectNumber) + ' Closed.' end
    from dbo.utb_claim_aspect
    where ClaimAspectID = @w_ClaimAspectID
    
    if @@error <> 0
    begin
      raiserror('99|%s',16,1,@ProcName)
      rollback transaction
      return
    end
    
    -- clear new assignment flags for cancelled vehicle   
    
    exec uspWorkflowNotifyEvent @EventID = @w_EventID,
                                @ClaimAspectID = @w_ClaimAspectID,
                                @Description = @w_EventDescription,
                                @UserID = @UserID


    if @@error <> 0
    begin
      raiserror('107|%s|%s',16,1,@ProcName,@w_EventDescription)
      rollback transaction
      return
    end

    if @w_VehicleWillCancel = 1
    begin
      update utb_claim_aspect
      set NewAssignmentAnalystFlag = 0,
          NewAssignmentOwnerFlag = 0,
          NewAssignmentSupportFlag = 0
      where ClaimAspectID = @w_ClaimAspectID        

         -- Add an audit log entry
         INSERT INTO dbo.utb_audit_log
         (AuditTypeCD, KeyID, KeyDescription, LogComment, SysLastUserID)
         VALUES
         ('C', @UserID, @w_ClaimAspectID, 'UserID : ' + convert(varchar, @UserID) + ' is cancelling the service channel ' + convert(varchar, @ClaimAspectServiceChannelID) + '. NewAssignmentOwnerFlag, NewAssignmentAnalystFlag and NewAssignmentSupportFlag are set to 0.', 0)
    end
    
    if @w_VehicleWillClose = 1
    begin
      if (select max(OriginalCompleteDate)
          from dbo.utb_claim_aspect_service_channel
          where ClaimAspectID = @w_ClaimAspectID) = @now
      begin
        -- Original Closing
        update dbo.utb_claim_aspect
        set CompletedAnalystUserID = AnalystUserID,
            CompletedOwnerUserID = OwnerUserID,
            CompletedSupportUserID = SupportUserID
        from dbo.utb_claim_aspect
        where ClaimAspectID = @w_ClaimAspectID

        if @@error <> 0
        begin
          raiserror('107|%s|%s',16,1,@ProcName,'Setting CompletedUserInfo')
          rollback transaction
          return
        end                              
      end
    end
    
    if @w_ClaimWillClose = 1
    begin
      exec uspWorkflowNotifyEvent @EventID = @EventIDClaimClose,
                                  @ClaimAspectID = @w_ClaimAspectIDClaim,
                                  @Description = 'Claim Closed',
                                  @UserID = @UserID

                                        
          if @@error <> 0
          begin
            raiserror('107|%s|%s',16,1,@ProcName,'Claim Closed')
            rollback transaction
            return
          end
    end
  end                                          
  
  commit transaction WorkflowCancelServiceChannel1

end                              
-- permissions
go 

if exists(select name from sysobjects
          where name = 'uspWorkflowCancelServiceChannel' and type='P')
begin
  grant execute on dbo.uspWorkflowCancelServiceChannel to ugr_lynxapd
  commit
end
else
begin
  raiserror ('Stored proc creation failure.',16,1)
  rollback
end

go            
                                  
                              
                 
  
 
  
  
  
  
  
  
  
  
  
  
  
      
         
  
  
 

    



  
