-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefPersonnelContactMethodGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefPersonnelContactMethodGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefPersonnelContactMethodGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Retrieve all Personnel Contact Methods
*
* PARAMETERS:  
* None.
*
* RESULT SET:
* PersonnelContactMethodID      Personnel Contact Method unique identity
* DisplayOrder                  Personnel Contact Method display order
* EnabledFlag                   Personnel Contact Method active/inactive flag
* Name                          Personnel Contact Method name
* SysMaintainedFlag             Personnel Contact Method system maintained flag
* SysLastUserID                 Personnel Contact Method last user
* SysLastUpdatedDate            Personnel Contact Method last update date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefPersonnelContactMethodGetList
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspRefPersonnelContactMethodGetList'


    SELECT 
        PersonnelContactMethodID,
        DisplayOrder,
        EnabledFlag,
        Name,
        SysMaintainedFlag,
        SysLastUserID,
        dbo.ufnUtilityGetDateString(SysLastUpdatedDate) AS SysLastUpdatedDate
    FROM dbo.utb_personnel_contact_method
    ORDER BY DisplayOrder

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefPersonnelContactMethodGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefPersonnelContactMethodGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/