-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestInsDAJobDocument' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestInsDAJobDocument 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestInsDAJobDocument
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* DATE:			2014Nov24
* FUNCTION:     Inserts a new DA job document the utb_hyperquestsvc_jobs table for processing.
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* Version: 1.0 - Initial Build
*          1.1 - 14Apr2015 - Added VehicleID, ShopLocationID, SeqID, code to increment SeqID
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestInsDAJobDocument]
	@vJobID VARCHAR(255)
	, @iInscCompID INT
	, @iLynxID INT
	, @vClaimNumber VARCHAR(30)
	, @iDocumentID BIGINT
	, @iAssignmentID BIGINT
	, @iVehicleID INT = 0 
	, @iShopLocationID INT = 0 
	, @iSeqID INT = 0
	, @dtReceivedDate DATETIME
	, @iDocumentTypeID INT
	, @vDocumentTypeName VARCHAR(50)
	, @vDocumentSource VARCHAR(100)
	, @vDocumentImageType VARCHAR(10)
	, @vDocumentImageLocation VARCHAR(255)
	, @iClaimAspectServiceChannelID BIGINT
	, @iClaimAspectID BIGINT
	, @vJobXSLFile VARCHAR(100)
	, @vJobStatus VARCHAR(25)
	, @vJobStatusDetails VARCHAR(255)
	, @iJobPriority INT
	, @dtJobCreatedDate DATETIME
	, @bEnabledFlag BIT
	, @xJobXML XML
	, @iSysLastUserID INT
AS
BEGIN
    -- Declare local variables
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)
	DECLARE @dtNow DATETIME
	DECLARE @iError INT
	DECLARE @Debug bit
	---------------------------
	DECLARE @iCurrentShopLocationID INT
	DECLARE @iCurrentSeqID INT
	DECLARE @vAdjustedDocumentID VARCHAR(50)
	
    SET @ProcName = 'uspHyperquestInsDAJobDocument'
    SET @ApplicationCD = 'APDHyperquestPartner'
	SET @dtNow = CURRENT_TIMESTAMP
	SET @iError = 0
	SET @debug = 0
	SET @iCurrentSeqID = 0
	
	IF (@iDocumentTypeID = 22)  -- 22 = ShopAssignment, 81 - CancelAssignment
	BEGIN
		------------------------------
		-- Validate - Shop Assignment
		------------------------------
		IF @debug = 1
		BEGIN
			PRINT 'Parameters:'
			PRINT '@iVehicleID = ' + Convert(varchar(20), @iVehicleID)
			PRINT '@iShopLocationID = ' + Convert(varchar(20), @iShopLocationID)
			PRINT '@iSeqID = ' + Convert(varchar(20), @iSeqID)
		END    

		-- Validate we have a good Assignment
		SELECT TOP 1 
			@iCurrentShopLocationID = a.ShopLocationID
		FROM 
			utb_claim_aspect ca
			INNER JOIN utb_claim_aspect_service_channel casc
			ON casc.ClaimAspectID = ca.ClaimAspectID
			INNER JOIN utb_assignment a
			ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
		WHERE 
			LynxID = @iLynxID
	    
		IF @iCurrentShopLocationID IS NULL
		BEGIN
			-- SQL Server Error
			DECLARE @vLynxID VARCHAR(20)
			DECLARE @vShopLocationID VARCHAR(20)
			SET @vLynxID = CONVERT(VARCHAR(20),@iLynxID)
			SET @vShopLocationID = CONVERT(VARCHAR(20),@iShopLocationID)
			RAISERROR  ('%s: Invalid ShopLocationID (NULL) for LynxID %s Shop is not currently assigned.  ShopLocationID in Job table is %s', 16, 1, @ProcName, @vLynxID, @vShopLocationID)
			RETURN
		END    
	
		------------------------------
		-- Check if assignment already
		-- exists.  If so, increment
		-- the sequence number
		------------------------------
		IF EXISTS (
			SELECT TOP 1
				JobID
				, LynxID
				, VehicleID
				, ShopLocationID
				, SeqID
				--, SeqID + 1 NewAssignmentSeq
			FROM 
				utb_hyperquestsvc_jobs
			WHERE
				LynxID = @iLynxID
				AND VehicleID = @iVehicleID
				AND ShopLocationID = @iShopLocationID
				AND DocumentTypeID = 22
			ORDER BY 
				SysLastUpdatedDate DESC
		)
		BEGIN
			-- Previous assignment already exists, increment SeqID
			SELECT TOP 1
				@iCurrentSeqID = SeqID + 1
				--, SeqID + 1 NewAssignmentSeq
			FROM 
				utb_hyperquestsvc_jobs
			WHERE
				LynxID = @iLynxID
				AND VehicleID = @iVehicleID
				AND ShopLocationID = @iShopLocationID
				AND DocumentTypeID = 22
			ORDER BY 
				SysLastUpdatedDate DESC
		END
		ELSE
		BEGIN
			-- Using existing SeqID
			SET @iCurrentSeqID = 0
		END
	END
	--ELSE
	--BEGIN
			-- Using existing SeqID
	--		SET @iCurrentSeqID = @iSeqID
	--END

	IF (@iDocumentTypeID = 81)  -- 81 - CancelAssignment
	BEGIN
		------------------------------
		-- Validate - Shop Assignment
		------------------------------
		IF @debug = 1
		BEGIN
			PRINT 'Parameters:'
			PRINT '@iVehicleID = ' + Convert(varchar(20), @iVehicleID)
			PRINT '@iShopLocationID = ' + Convert(varchar(20), @iShopLocationID)
			PRINT '@iSeqID = ' + Convert(varchar(20), @iSeqID)
		END   
		
		-- Validate we have a good Assignment
		SELECT TOP 1 
			@iCurrentShopLocationID = a.ShopLocationID
		FROM 
			utb_claim_aspect ca
			INNER JOIN utb_claim_aspect_service_channel casc
			ON casc.ClaimAspectID = ca.ClaimAspectID
			INNER JOIN utb_assignment a
			ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
		WHERE 
			LynxID = @iLynxID

		------------------------------
		-- Check if assignment already
		-- exists.  If so, increment
		-- the sequence number
		------------------------------
		IF EXISTS (
			SELECT TOP 1
				JobID
				, LynxID
				, VehicleID
				, ShopLocationID
				, SeqID
				--, SeqID + 1 NewAssignmentSeq
			FROM 
				utb_hyperquestsvc_jobs
			WHERE
				LynxID = @iLynxID
				AND VehicleID = @iVehicleID
				AND ShopLocationID = @iShopLocationID
				AND DocumentTypeID = 81
			ORDER BY 
				SysLastUpdatedDate DESC
		)
		BEGIN
			-- Previous assignment already exists, increment SeqID
			SELECT TOP 1
				@iCurrentSeqID = SeqID + 1
				--, SeqID + 1 NewAssignmentSeq
			FROM 
				utb_hyperquestsvc_jobs
			WHERE
				LynxID = @iLynxID
				AND VehicleID = @iVehicleID
				AND ShopLocationID = @iShopLocationID
				AND DocumentTypeID = 81
			ORDER BY 
				SysLastUpdatedDate DESC
		END
		ELSE
		BEGIN
			-- Using existing SeqID
			SET @iCurrentSeqID = 0
		END
	END


	
    -- PreSQL 
    -- Test to see if the DocumentID already existing in the job table
	If NOT EXISTS (SELECT JobID FROM utb_hyperquestsvc_jobs WHERE JobID = @vJobID AND EnabledFlag = 1)
	BEGIN
		-- DocumentID doesn't exist.  Insert the row data
		BEGIN TRAN utrInsertDocumentData
		INSERT INTO
			utb_hyperquestsvc_jobs
			(
			  [JobID]
			  , [InscCompID]
			  , [LynxID]
			  , [ClaimNumber]
			  , [DocumentID]
			  , [AssignmentID]
			  , [VehicleID]
			  , [ShopLocationID]
			  , [SeqID]
			  , [ReceivedDate]
			  , [DocumentTypeID]
			  , [DocumentTypeName]
			  , [DocumentSource]
			  , [DocumentImageType]
			  , [DocumentImageLocation]
			  , [ClaimAspectServiceChannelID]
			  , [ClaimAspectID]
			  , [JobXSLFile]
			  , [JobStatus]
			  , [JobStatusDetails]
			  , [JobPriority]
			  , [JobCreatedDate]
			  , [JobProcessedDate]
			  , [JobTransformedDate]
			  , [JobTransmittedDate]
			  , [JobArchivedDate]
			  , [JobFinishedDate]
			  , [EnabledFlag]
			  , [JobXML]
			  , [SysLastUserID]
			  , [SysLastUpdatedDate]
			)
			VALUES
			(
			  @vJobID
			  , @iInscCompID
			  , @iLynxID
			  , @vClaimNumber
			  , @iDocumentID
			  , @iAssignmentID
			  , @iVehicleID
			  , @iShopLocationID
			  , @iCurrentSeqID
			  , @dtReceivedDate
			  , @iDocumentTypeID
			  , @vDocumentTypeName
			  , @vDocumentSource
			  , @vDocumentImageType
			  , @vDocumentImageLocation
			  , @iClaimAspectServiceChannelID
			  , @iClaimAspectID
			  , @vJobXSLFile
			  , @vJobStatus
			  , @vJobStatusDetails
			  , @iJobPriority
			  , @dtJobCreatedDate
			  , NULL
			  , NULL
			  , NULL
			  , NULL
			  , NULL
			  , @bEnabledFlag
			  , @xJobXML
			  , @iSysLastUserID
			  , @dtNow
			)

			IF @@ERROR <> 0
			BEGIN
				-- Insert Failure
				ROLLBACK TRANSACTION
				RAISERROR('%s: (Hyperquest Job Insert) Error inserting the new document into utb_hyperquestsvc_jobs.', 16, 1, @ProcName)
				SET @iError = 101
			END
			ELSE
			BEGIN
				COMMIT TRANSACTION utrInsertDocumentData
				--rollback transaction utrInsertDocumentData

				IF @@ERROR <> 0
				BEGIN
					-- SQL Server Error
			       
					RAISERROR('%s: SQL Server Error during new document insert Commit', 16, 1, @ProcName)
					SET @iError = 102
				END    
			END
	END
	ELSE
	BEGIN
		-- DocumentID already exists.  Return message.
		SET @iError = 99
	END

	SELECT @iError
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestInsDAJobDocument' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestInsDAJobDocument TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/