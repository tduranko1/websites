-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminLaborRatesDelDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminLaborRatesDelDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminLaborRatesDelDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Delete a Labor Rate entry
*
* PARAMETERS:  
*
* RESULT SET:
* An XML data stream
*
* Additional Notes:
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminLaborRatesDelDetail
    @LaborRateID        udt_std_id_big,
    @InsuranceCompanyID udt_std_int_small,
    @UserID             udt_std_id_big
    
AS
BEGIN
    DECLARE @ProcName                 AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                      AS datetime
    DECLARE @tmpMoney                 AS udt_std_money

    SET @ProcName = 'uspAdminLaborRatesDelDetail'
    SET @now = CURRENT_TIMESTAMP


    IF @LaborRateID IS NOT NULL
    BEGIN
      IF NOT EXISTS(SELECT LaborRateID
                     FROM utb_labor_rate
                     WHERE LaborRateID = @LaborRateID)
      BEGIN
           -- Invalid LaborRateID
        
            RAISERROR  ('1|Invalid LaborRateID', 16, 1, @ProcName)
            RETURN
      END
    END
    
    IF @UserID IS NOT NULL
    BEGIN
      IF NOT EXISTS(SELECT UserID
                     FROM utb_user
                     WHERE UserID = @UserID)
      BEGIN
           -- Invalid User ID
        
            RAISERROR  ('1|Invalid UserID', 16, 1, @ProcName)
            RETURN
      END
    END
    
    IF @InsuranceCompanyID IS NOT NULL
    BEGIN
      IF NOT EXISTS(SELECT InsuranceCompanyID
                     FROM utb_insurance
                     WHERE InsuranceCompanyID = @InsuranceCompanyID)
      BEGIN
           -- Invalid InsuranceCompanyID
        
            RAISERROR  ('1|Invalid InsuranceCompanyID', 16, 1, @ProcName)
            RETURN
      END
    END
    
    BEGIN TRANSACTION
    
    DELETE FROM utb_labor_rate
    WHERE LaborRateID = @LaborRateID
      AND InsuranceCompanyID = @InsuranceCompanyID
    
    COMMIT TRANSACTION
   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminLaborRatesDelDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminLaborRatesDelDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
************************************************************************************************************************/
