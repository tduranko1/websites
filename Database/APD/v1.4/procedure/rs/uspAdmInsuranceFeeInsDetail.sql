-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsuranceFeeInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmInsuranceFeeInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdmInsuranceFeeInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Ramesh Vishegu]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @ClaimAspectTypeID    Claim Aspect Type ID
* (I) @InsuranceCompanyID   Insurance Company ID
* (I) @CategoryCD           Category Code
* (I) @Description          Description for the fee item that will appear on the billing screens
* (I) @DisplayOrder         Display Order
* (I) @FeeAmount            Fee Amount
* (I) @FeeInstructions      Fee Instructions
* (I) @ItemizeFlag          Can the fee item appear on the invoice
* (I) @HideFromInvoiceFlag  Hide this fee item from the invoice
* (I) @InvoiceDescription   The description that will appear on the invoice
* (I) @EffectiveStartDate   The fee effective from date
* (I) @EffectiveEndDate     The fee effective end date
* (I) @AppliesWhenCD        The Fee applies when code
* (I) @IncludedServices     A series of string comprising of ServiceID, ItemizeFlag, RequiredFlag and separated by ;
* (I) @AdminFees            A series of string comprising of AdminFeeCD, Amount, MSA and State separated by ;
* (I) @SysLastUserID        User that last updated the record
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdmInsuranceFeeInsDetail
(
    @ClaimAspectTypeID    udt_std_int_tiny,
    @InsuranceCompanyID   udt_std_int_small,
    @CategoryCD           udt_std_cd,
    @Description          udt_std_name,
    @DisplayOrder         udt_std_int,
    @FeeAmount            varchar(50),
    @FeeInstructions      udt_std_desc_mid,
    @ItemizeFlag      udt_std_flag,
    @HideFromInvoiceFlag  udt_std_flag,
    @InvoiceDescription   udt_std_name,
    @EffectiveStartDate   varchar(10) = NULL,
    @EffectiveEndDate     varchar(10) = NULL,
    @AppliesWhenCD        varchar(1),
    @IncludedServices     varchar(1000),
    @AdminFees            varchar(1000),
    @SysLastUserID        udt_std_id, 
    @ApplicationCD        udt_std_cd = 'APD'
)
AS
BEGIN
    SET NOCOUNT ON

    -- Declare internal variables

    DECLARE @ClientFeeID        AS INT
    DECLARE @EnabledFlag        AS Bit
    DECLARE @InsuranceName      AS udt_std_name
    DECLARE @LogComment         AS udt_std_desc_long
    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT

    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                AS datetime

    DECLARE @AdminFeeID               AS bigint
    DECLARE @AdminFee                 AS varchar(50)
    DECLARE @AdminFeeCD               AS varchar(1)    
    DECLARE @AdminFeeAmount           AS decimal(9, 2)
    DECLARE @AdminFeeMSA              AS varchar(5)
    DECLARE @AdminFeeState            AS varchar(5)    

    DECLARE @Debug              AS bit
    
    SET @Debug = 0

    SET @ProcName = 'uspAdmInsuranceFeeInsDetail'
    SET @now = CURRENT_TIMESTAMP
    
    IF @Debug = 1
    BEGIN
        Print 'Input Parameters:'
        Print ''
        Print '@ClaimAspectTypeID= ' + convert(varchar, @ClaimAspectTypeID)
        Print '@InsuranceCompanyID= ' + convert(varchar, @InsuranceCompanyID)
        Print '@CategoryCD= ' + convert(varchar, @CategoryCD)
        Print '@Description= ' + convert(varchar, @Description)
        Print '@DisplayOrder= ' + convert(varchar, @DisplayOrder)
        Print '@FeeAmount= ' + convert(varchar, @FeeAmount)
        Print '@FeeInstructions= ' + convert(varchar, @FeeInstructions)
        Print '@ItemizeFlag= ' + convert(varchar, @ItemizeFlag)
        Print '@HideFromInvoiceFlag= ' + convert(varchar, @HideFromInvoiceFlag)
        Print '@InvoiceDescription= ' + convert(varchar, @InvoiceDescription)
        Print '@EffectiveStartDate= ' + convert(varchar, @EffectiveStartDate)
        Print '@EffectiveEndDate= ' + convert(varchar, @EffectiveEndDate)
        Print '@AppliesWhenCD= ' + convert(varchar, @AppliesWhenCD)
        Print '@IncludedServices= ' + convert(varchar, @IncludedServices)
        Print '@SysLastUserID= ' + convert(varchar, @SysLastUserID)
    END
    
    -- Validate the input parameters
    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT u.UserID 
                       FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                           INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
                       WHERE a.Code = @ApplicationCD
                         AND u.UserID = @SysLastUserID
                         AND @SysLastUserID <> 0
                         AND ua.AccessBeginDate IS NOT NULL
                         AND ua.AccessBeginDate <= @now
                         AND ua.AccessEndDate IS NULL)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeID IS NULL 
        OR 
        NOT EXISTS (SELECT ClaimAspectTypeID
                        FROM utb_claim_aspect_type
                        WHERE ClaimAspectTypeID = @ClaimAspectTypeID)
    BEGIN
        RAISERROR  ('1|Invalid ClaimAspectTypeID', 16, 1, @ProcName)
        RETURN
    END

    IF @InsuranceCompanyID IS NULL 
        OR 
        NOT EXISTS (SELECT InsuranceCompanyID
                        FROM utb_insurance
                        WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        RAISERROR  ('1|Invalid InsuranceCompanyID', 16, 1, @ProcName)
        RETURN
    END

    IF @CategoryCD IS NULL OR @CategoryCD not in ('A', 'H')
        
    BEGIN
        RAISERROR  ('1|Invalid CategoryCD', 16, 1, @ProcName)
        RETURN
    END

    IF @Description IS NULL
        
    BEGIN
        RAISERROR  ('1|Description cannot be empty', 16, 1, @ProcName)
        RETURN
    END

    IF @FeeAmount IS NULL
        
    BEGIN
        RAISERROR  ('1|Fee Amount cannot be empty', 16, 1, @ProcName)
        RETURN
    END

    IF @ItemizeFlag IS NULL
        
    BEGIN
        RAISERROR  ('1|ItemizeFlag cannot be null', 16, 1, @ProcName)
        RETURN
    END
    ELSE
    BEGIN
        /*IF @ItemizeFlag = 1 AND @InvoiceDescription IS NULL
        BEGIN
            RAISERROR  ('1|Invoice Description cannot be empty', 16, 1, @ProcName)
            RETURN
        END
        ELSE
        BEGIN
            SET @InvoiceDescription = @Description
        END*/
        IF @InvoiceDescription IS NULL OR LEN(LTRIM(RTRIM(@InvoiceDescription))) = 0
        BEGIN
            SET @InvoiceDescription = @Description
        END        
    END

    IF @EffectiveStartDate IS NOT NULL
    BEGIN
        IF ISDATE(@EffectiveStartDate) = 0
        BEGIN
            RAISERROR  ('1|Invalid Fee Effective From Date.', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF @EffectiveEndDate IS NOT NULL
    BEGIN
        IF ISDATE(@EffectiveEndDate) = 0
        BEGIN
            RAISERROR  ('1|Invalid Fee Effective To Date.', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF @EffectiveStartDate IS NOT NULL AND @EffectiveEndDate IS NOT NULL
    BEGIN
        IF convert(datetime, @EffectiveEndDate) < convert(datetime, @EffectiveStartDate)
        BEGIN
            RAISERROR  ('1|The Fee Effective To date cannot be before the From date.', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF @AppliesWhenCD IS NULL OR @AppliesWhenCD NOT IN ('B', 'C')
    BEGIN
        RAISERROR  ('1|Invalid Applies When value.', 16, 1, @ProcName)
        RETURN
    END

    IF @IncludedServices IS NULL OR LEN(RTrim(LTrim(@IncludedServices))) = 0
        
    BEGIN
        RAISERROR  ('1|Fee must include atleast one service.', 16, 1, @ProcName)
        RETURN
    END
    
    -- Done with validations, do the save operation.
    
    -- Extract the Included services
    DECLARE @tmpIncludedServices TABLE
    (
        ServiceInfo     varchar(50)
    )
    
    DECLARE @tmpClientFeeDefinition TABLE
    (
        ServiceID        int     not NULL,
        ItemizeFlag      bit     not NULL,
        RequiredFlag     bit     not NULL
    )
    
    INSERT INTO @tmpIncludedServices
    SELECT value from ufnUtilityParseString(@IncludedServices, ';', 1) -- @IncludedServices format: ServiceID,ItemizeFlag,RequiredFlag[;ServiceID,ItemizeFlag,RequiredFlag[...]]
    
    IF @Debug = 1
        SELECT * FROM  @tmpIncludedServices
    
    DECLARE csrIncludedServices CURSOR FOR
        SELECT ServiceInfo FROM @tmpIncludedServices
        
    DECLARE @IncludedService      varchar(50)
    DECLARE @ServiceID            int
    DECLARE @ServiceItemizeFlag   bit
    DECLARE @ServiceRequiredFlag  bit

    OPEN csrIncludedServices

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrIncludedServices
    INTO @IncludedService

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        SET @ServiceID = NULL
        SET @ServiceItemizeFlag = NULL
        SET @ServiceRequiredFlag = NULL

        DECLARE csrIncludedService CURSOR FOR
            SELECT VALUE FROM ufnUtilityParseString(@IncludedService, ',', 1)
        
        OPEN csrIncludedService

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            CLOSE csrIncludedServices
            DEALLOCATE csrIncludedServices
            RETURN
        END    

        FETCH next
        FROM csrIncludedService
        INTO @ServiceID
        
        FETCH next
        FROM csrIncludedService
        INTO @ServiceRequiredFlag

        FETCH next
        FROM csrIncludedService
        INTO @ServiceItemizeFlag
        
        CLOSE csrIncludedService
        DEALLOCATE csrIncludedService
        
        IF NOT EXISTS(SELECT ServiceID from utb_service where ServiceID = @ServiceID)
        BEGIN
            RAISERROR  ('1|Invalid Service ID specified.', 16, 1, @ProcName)
            CLOSE csrIncludedServices
            DEALLOCATE csrIncludedServices
            RETURN
        END
        
        IF @ServiceItemizeFlag IS NULL
            SET @ServiceItemizeFlag = 0

        IF @ServiceRequiredFlag IS NULL
            SET @ServiceRequiredFlag = 0
            
        INSERT INTO @tmpClientFeeDefinition VALUES (@ServiceID, @ServiceItemizeFlag, @ServiceRequiredFlag)

        FETCH next
        FROM csrIncludedServices
        INTO @IncludedService

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    

    END

    CLOSE csrIncludedServices
    DEALLOCATE csrIncludedServices
    
    IF @Debug = 1
        SELECT * FROM @tmpClientFeeDefinition
        
    SET @EnabledFlag = 1
    

    -- Extract the Included services
    DECLARE @tmpAdminFees TABLE
    (
        AdminFeeInfo     varchar(100)
    )
    
    DECLARE @tmpAdminFeeDefinition TABLE
    (
        PaidByCD    varchar(1)      not NULL,
        AdminAmount decimal(9,2)    not NULL,
        AdminMSA    varchar(5)      NULL,
        AdminState  varchar(5)      NULL
    )
    
    INSERT INTO @tmpAdminFees
    SELECT value from ufnUtilityParseString(@AdminFees, ';', 1) -- @IncludedServices format: ServiceID,ItemizeFlag,RequiredFlag[;ServiceID,ItemizeFlag,RequiredFlag[...]]
    
    IF @Debug = 1
        SELECT * FROM  @tmpAdminFees
    
    DECLARE csrAdminFees CURSOR FOR
        SELECT AdminFeeInfo FROM @tmpAdminFees
        
    

    OPEN csrAdminFees

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrAdminFees
    INTO @AdminFee

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        SET @AdminFeeCD = NULL
        SET @AdminFeeAmount = NULL
        SET @AdminFeeMSA = NULL
        SET @AdminFeeState = NULL

        DECLARE csrAdminFeeInfo CURSOR FOR
            SELECT VALUE FROM ufnUtilityParseString(@AdminFee, ',', 1)
        
        OPEN csrAdminFeeInfo

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            CLOSE csrIncludedServices
            DEALLOCATE csrIncludedServices
            RETURN
        END    

        FETCH next
        FROM csrAdminFeeInfo
        INTO @AdminFeeCD
        
        FETCH next
        FROM csrAdminFeeInfo
        INTO @AdminFeeAmount

        FETCH next
        FROM csrAdminFeeInfo
        INTO @AdminFeeMSA

        FETCH next
        FROM csrAdminFeeInfo
        INTO @AdminFeeState
        
        CLOSE csrAdminFeeInfo
        DEALLOCATE csrAdminFeeInfo
        
        INSERT INTO @tmpAdminFeeDefinition
        VALUES 
        (@AdminFeeCD, @AdminFeeAmount, @AdminFeeMSA, @AdminFeeState)

        FETCH next
        FROM csrAdminFees
        INTO @AdminFee

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    

    END

    CLOSE csrAdminFees
    DEALLOCATE csrAdminFees
    
    IF @Debug = 1
        SELECT * FROM @tmpAdminFeeDefinition
    
           
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END  
    
    
    BEGIN TRANSACTION InsuranceFeeInsTran
    
      INSERT INTO utb_client_fee
      (
          ClaimAspectTypeID,
          InsuranceCompanyID,
          CategoryCD,
          Description,
          DisplayOrder,
          EnabledFlag,
          FeeAmount,
          FeeInstructions,
          ItemizeFlag,
          HideFromInvoiceFlag,
          InvoiceDescription,
          EffectiveStartDate,
          EffectiveEndDate,
          AppliesToCD,
          SysLastUserID,
          SysLastUpdatedDate
      )
      VALUES
      (
          @ClaimAspectTypeID,
          @InsuranceCompanyID,
          @CategoryCD,
          @Description,
          @DisplayOrder,
          @EnabledFlag,
          convert(money, @FeeAmount),
          @FeeInstructions,
          @ItemizeFlag,
          @HideFromInvoiceFlag,
          @InvoiceDescription,
          @EffectiveStartDate,
          @EffectiveEndDate,
          @AppliesWhenCD,
          @SysLastUserID,
          @now
      )
       
      SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
      -- Check error value
    
      IF @error <> 0
      BEGIN
          -- SQL Server Error

          RAISERROR  ('105|%s|utb_client_fee', 16, 1, @ProcName)
          ROLLBACK TRANSACTION 
          RETURN
      END
      ELSE
      BEGIN
          -- Get the new ClientFeeID

          SET @ClientFeeID = SCOPE_IDENTITY()
      END

      -- now insert the ones that were passed in
      INSERT INTO utb_client_fee_definition
      SELECT  @ClientFeeID,
              ServiceID,
              ItemizeFlag,
              RequiredFlag,
              @SysLastUserID,
              @now
      FROM @tmpClientFeeDefinition

      IF @@ERROR <> 0
      BEGIN
          -- SQL Server Error

          RAISERROR  ('105|%s|utb_client_fee_definition', 16, 1, @ProcName)
          ROLLBACK TRANSACTION 
          RETURN
      END

    IF EXISTS(SELECT * FROM @tmpAdminFeeDefinition)
    BEGIN
        -- Add the fee to the utb_admin_fee
        IF EXISTS (SELECT * 
                    FROM @tmpAdminFeeDefinition 
                    WHERE AdminMSA = 'Flat' 
                      AND AdminState = 'Flat')
        BEGIN
            INSERT INTO utb_admin_fee (
                ClientFeeID, 
                AdminFeeCD, 
                AppliesToCD, 
                Amount, 
                EnabledFlag, 
                SplitFlag, 
                SysLastUserID, 
                SysLastUpdatedDate)
            SELECT @ClientFeeID,
                PaidByCD,
                'F',
                AdminAmount,
                1,
                0,
                0,
                @now
            FROM @tmpAdminFeeDefinition 
            WHERE AdminMSA = 'Flat' 
              AND AdminState = 'Flat'

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
            
                RAISERROR  ('105|%s|utb_admin_fee', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            SELECT @AdminFeeID = SCOPE_IDENTITY()
        END
        ELSE
        BEGIN
            INSERT INTO utb_admin_fee (
                ClientFeeID, 
                AdminFeeCD, 
                AppliesToCD, 
                Amount, 
                EnabledFlag, 
                SplitFlag, 
                SysLastUserID, 
                SysLastUpdatedDate)
            SELECT @ClientFeeID,
                'I',
                'M',
                NULL,
                1,
                0,
                0,
                @now
            FROM @tmpAdminFeeDefinition 

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
            
                RAISERROR  ('105|%s|utb_admin_fee', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END

            SELECT @AdminFeeID = SCOPE_IDENTITY()
        END

        -- add the MSA
        INSERT INTO utb_admin_fee_msa (
            AdminFeeID,
            MSA,
            AdminFeeCD,
            Amount,
            EnabledFlag,
            SysLastUserID,
            SysLastUpdatedDate)
        SELECT @AdminFeeID,
            AdminMSA,
            PaidByCD,
            AdminAmount,
            1,
            0,
            @now
        FROM @tmpAdminFeeDefinition
        WHERE AdminMSA <> 'Flat'
          AND AdminMSA <> ''
          AND AdminMSA IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('105|%s|utb_admin_fee_msa', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END


        -- add the State
        INSERT INTO utb_admin_fee_state (
            AdminFeeID,
            StateCode,
            AdminFeeCD,
            Amount,
            EnabledFlag,
            SysLastUserID,
            SysLastUpdatedDate)
        SELECT @AdminFeeID,
            AdminState,
            PaidByCD,
            AdminAmount,
            1,
            0,
            @now
        FROM @tmpAdminFeeDefinition
        WHERE AdminState <> 'Flat'
          AND AdminState <> '' 
          AND AdminState IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('105|%s|utb_admin_fee_state', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

        IF EXISTS(SELECT AdminFeeID
                    FROM utb_admin_fee_msa
                    WHERE AdminFeeID = @AdminFeeID
                      AND EnabledFlag = 1) OR
           EXISTS(SELECT AdminFeeID
                    FROM utb_admin_fee_msa
                    WHERE AdminFeeID = @AdminFeeID
                      AND EnabledFlag = 1)
        BEGIN
            UPDATE utb_admin_fee
            SET AppliesToCD = 'X',
                EnabledFlag = 1
            WHERE @AdminFeeID = @AdminFeeID

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
            
                RAISERROR  ('104|%s|utb_admin_fee', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END
    END
    
    
    
      -- Audit Log Entries ---------------------------------------------------------------------------------------------
      ------------------------------------------------------------------------------------------------------------------
      
      SET @LogComment = 'Fee Created'
      
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
      
      
      SET @LogComment = 'Category Established - ' + 
                        (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'CategoryCD')
                                     WHERE Code = @CategoryCD) 
                                     
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END 
                                     
      SET @LogComment = 'Aspect Type Established - ' + 
                        (SELECT Name FROM dbo.utb_claim_aspect_type 
                                    WHERE ClaimAspectTypeID = @ClaimAspectTypeID) 
                                                                        
                                    
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END                                
                 
                                    
      SET @LogComment = 'Amount Established - ' + @FeeAmount
      
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
      
      
      
      IF @FeeInstructions IS NOT NULL AND LEN(@FeeInstructions) > 0
      BEGIN
        SET @LogComment = 'Instructions Established - ' + @FeeInstructions
      
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
             
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END 
      END
      
      
      
      SET @LogComment = 'Itemize Established - ' + (SELECT CASE @ItemizeFlag 
                                                                 WHEN 1 THEN 'ON'
                                                                 ELSE 'OFF'
                                                               END)
                              
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END 
       
      
      SET @LogComment = 'Invoice Description Established - ' + @InvoiceDescription
      
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END 
      
      
      SET @LogComment = 'Effective From Date Established - ' + ISNULL(@EffectiveStartDate, 'NONE')
      
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END 
      
      
      SET @LogComment = 'Effective To Date Established - ' + ISNULL(@EffectiveEndDate, 'NONE')
      
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END 
                       
      
      SET @LogComment = 'Effective Date Compared To Established - ' + 
                        (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'AppliesToCD')
                                     WHERE Code = @AppliesWhenCD) + ' Creation'
                                     
      EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
                          
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END 
      
                                
      DECLARE csrServices CURSOR FOR (SELECT ServiceID, ItemizeFlag, RequiredFlag FROM @tmpClientFeeDefinition)
      
      
      OPEN csrServices
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, @ProcName)
          ROLLBACK TRANSACTION
          RETURN
      END 
            
      
      FETCH NEXT FROM csrServices INTO @ServiceID, @ServiceItemizeFlag, @ServiceRequiredFlag
      WHILE @@FETCH_STATUS = 0
      BEGIN
        
        SET @LogComment = 'Service Added - ' + 
                          (SELECT Name FROM dbo.utb_service WHERE ServiceID = @ServiceID) + 
                          ' - Required: ' + (SELECT CASE @ServiceRequiredFlag WHEN 1 THEN 'ON' ELSE 'OFF' END) + 
                          ' - Itemize: ' + (SELECT CASE @ServiceItemizeFlag WHEN 1 THEN 'ON' ELSE 'OFF' END)
                                     
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
                          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END 
      
      
        FETCH NEXT FROM csrServices INTO @ServiceID, @ServiceItemizeFlag, @ServiceRequiredFlag
      END
      
      
      CLOSE csrServices
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, @ProcName)
          ROLLBACK TRANSACTION
          RETURN
      END 
      
      
      DEALLOCATE csrServices
      
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, @ProcName)
          ROLLBACK TRANSACTION
          RETURN
      END 
      
      
    -- Admin Fees log  -----------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------------
    -- Check for flat admin fee
    IF EXISTS(SELECT *
                FROM @tmpAdminFeeDefinition
                WHERE AdminMSA = 'Flat'
                  AND AdminState = 'Flat')
    BEGIN
        -- Flat admin fee is added
        SELECT @AdminFeeCD = paidByCD,
               @AdminFeeAmount = AdminAmount
        FROM @tmpAdminFeeDefinition
        WHERE AdminMSA = 'Flat'
          AND AdminState = 'Flat'

        SET @LogComment = 'Flat Admin Fee was added. Paid by ' + CASE
                                                                    WHEN @AdminFeeCd = 'S' THEN 'Shop'
                                                                    WHEN @AdminFeeCd = 'I' THEN 'Insurance Company'
                                                                    ELSE 'Unknown'
                                                                 END + '; Amount: $' +
                                                                 CONVERT(varchar, @AdminFeeAmount)
        --SET @LogComment = 'Flat Admin Fee was added.'
        EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
    END

    -- Check for Admin fee msa changes
    -- Admin fee msa additions
    DECLARE @tmpComments as varchar(1000)
    SET @tmpComments = ''

    DECLARE csrAdminFees CURSOR FOR 
        (SELECT PaidByCD, AdminAmount, AdminMSA, AdminState 
            FROM @tmpAdminFeeDefinition
            WHERE AdminMSA IS NOT NULL
              AND AdminMSA <> 'Flat'
              AND AdminMSA <> '')


    OPEN csrAdminFees

    IF @@ERROR <> 0
    BEGIN
     -- SQL Server Error

      RAISERROR  ('99|%s', 16, 1, @ProcName)
      ROLLBACK TRANSACTION
      RETURN
    END 
        

    FETCH NEXT FROM csrAdminFees INTO @AdminFeeCd, @AdminFeeAmount, @AdminFeeMSA, @AdminFeeState

    IF @@ERROR <> 0
    BEGIN
    -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END 

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @tmpComments = 'Admin fee for MSA: ' + @AdminFeeMSA + ' was added. ' + 
                            'Paid by '  + CASE
                                            WHEN @AdminFeeCd = 'S' THEN 'Shop'
                                            WHEN @AdminFeeCd = 'I' THEN 'Insurance Company'
                                            ELSE 'Unknown'
                                          END + ' ' +
                            'Amount: ' + CONVERT(varchar, @AdminFeeAmount)

        IF @Debug = 1
        BEGIN
            print 'Admin fee msa additions...' + @tmpComments
        END
        
        IF LEN(@tmpComments) > 0
        BEGIN
            SET @LogComment = @tmpComments
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END

        FETCH NEXT FROM csrAdminFees INTO @AdminFeeCd, @AdminFeeAmount, @AdminFeeMSA, @AdminFeeState
        IF @@ERROR <> 0
        BEGIN
        -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END 

        SET @tmpComments = ''
    END

    CLOSE csrAdminFees

    IF @@ERROR <> 0
    BEGIN
    -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END 


    DEALLOCATE csrAdminFees

    IF @@ERROR <> 0
    BEGIN
    -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END 

    -- Check for Admin fee state changes
    -- Admin fee state additions
    SET @tmpComments = ''

    DECLARE csrAdminFees CURSOR FOR 
        (SELECT PaidByCD, AdminAmount, AdminMSA, AdminState 
            FROM @tmpAdminFeeDefinition
            WHERE AdminState IS NOT NULL
              AND AdminState <> 'Flat'
              AND AdminState <> '')


    OPEN csrAdminFees

    IF @@ERROR <> 0
    BEGIN
     -- SQL Server Error

      RAISERROR  ('99|%s', 16, 1, @ProcName)
      ROLLBACK TRANSACTION
      RETURN
    END 
        

    FETCH NEXT FROM csrAdminFees INTO @AdminFeeCd, @AdminFeeAmount, @AdminFeeMSA, @AdminFeeState

    IF @@ERROR <> 0
    BEGIN
    -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END 

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @tmpComments = 'Admin fee for State: ' + @AdminFeeState + ' was added. ' + 
                            'Paid by '  + CASE
                                            WHEN @AdminFeeCd = 'S' THEN 'Shop'
                                            WHEN @AdminFeeCd = 'I' THEN 'Insurance Company'
                                            ELSE 'Unknown'
                                          END + ' ' +
                            'Amount: ' + CONVERT(varchar, @AdminFeeAmount)

        IF @Debug = 1
        BEGIN
            print 'Admin fee state additions...' + @tmpComments
        END
        
        IF LEN(@tmpComments) > 0
        BEGIN
            SET @LogComment = @tmpComments
            EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @ClientFeeID, @LogComment, @SysLastUserID
        END

        FETCH NEXT FROM csrAdminFees INTO @AdminFeeCd, @AdminFeeAmount, @AdminFeeMSA, @AdminFeeState
        IF @@ERROR <> 0
        BEGIN
        -- SQL Server Error

            RAISERROR  ('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END 

        SET @tmpComments = ''
    END

    CLOSE csrAdminFees

    IF @@ERROR <> 0
    BEGIN
    -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END 


    DEALLOCATE csrAdminFees

    IF @@ERROR <> 0
    BEGIN
    -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END 

      
      -- END Audit Log Entries ---------------------------------------------------------------------------------------
      ----------------------------------------------------------------------------------------------------------------
      
    
          
    COMMIT TRANSACTION InsuranceFeeInsTran
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Create XML Document to return new updated date time and updated vehicle involved list
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- New Involved Level
            NULL AS [ClientFee!2!ClientFeeID],
            NULL AS [ClientFee!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- New Involved Level
            @ClientFeeID,
            dbo.ufnUtilityGetDateString( @now )

    ORDER BY tag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount

    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsuranceFeeInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmInsuranceFeeInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/