-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceServiceInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspInvoiceServiceInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspInvoiceServiceInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Inserts a record into the utb_invoice_service
*
* PARAMETERS:  
* (I) @InvoiceID            Invoice ID to attach the billed service to
* (I) @ClientFeeID          The Client Fee ID of the billed service
* (I) @ServiceID            Service attached
* (I) @ClaimAspectID        The claim aspect the service is for
* (I) @ServiceChannelCD     The service channel billed for
* (I) @UserID               The User ID performing the invoicing
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspInvoiceServiceInsDetail
    @InvoiceID          udt_std_id,
    @ClientFeeID        udt_std_id,
    @ServiceID          udt_std_id,
    @ClaimAspectID      udt_std_id,
    @ServiceChannelCD   udt_std_cd,
    @DispositionTypeCD  udt_std_cd,
    @UserID             udt_std_id
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@ServiceChannelCD))) = 0 SET @ServiceChannelCD = NULL
    IF LEN(RTRIM(LTRIM(@DispositionTypeCD))) = 0 SET @DispositionTypeCD = NULL


    -- Declare internal variables

    DECLARE @error              AS int
    DECLARE @rowcount           AS int
    DECLARE @now                AS datetime

    DECLARE @ClaimAspectTypeID  AS udt_std_id
    DECLARE @ClaimAspectNumber  AS udt_std_int
    DECLARE @ItemizeFlag        AS udt_std_flag
    DECLARE @LynxID             AS udt_std_id
    DECLARE @UId                AS varchar(20)
    DECLARE @UIdSuffix          AS varchar(5)
    
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspInvoiceServiceInsDetail'

    
    -- Set Database options
    
    SET NOCOUNT ON
    SET CONCAT_NULL_YIELDS_NULL OFF
    
        
    -- Check to make sure a valid Invoice ID was passed in
    
    IF  (@InvoiceID IS NULL) OR
        (NOT EXISTS(SELECT InvoiceID FROM dbo.utb_invoice WHERE InvoiceID = @InvoiceID))
    BEGIN
        -- Invalid Invoice ID
    
        RAISERROR('101|%s|@InvoiceID|%u', 16, 1, @ProcName, @InvoiceID)
        RETURN
    END 
       
    
    -- Check to make sure a valid Claim Aspect ID was passed in
    
    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END 
       
    
    -- IF Service Channel CD was passed in, check to make sure it is valid
    
    IF @ServiceChannelCD IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice_service', 'ServiceChannelCD') WHERE Code = @ServiceChannelCD)
        BEGIN
            -- Invalid Service Channel CD
    
            RAISERROR('101|%s|@ServiceChannelCD|%s', 16, 1, @ProcName, @ServiceChannelCD)
            RETURN
        END
    END 
       
    
    -- IF Disposition Type CD was passed in, check to make sure it is valid
    
    IF @DispositionTypeCD IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice_service', 'DispositionTypeCD') WHERE Code = @DispositionTypeCD)
        BEGIN
            -- Invalid Disposition Type CD
    
            RAISERROR('101|%s|@DispositionTypeCD|%s', 16, 1, @ProcName, @DispositionTypeCD)
            RETURN
        END
    END 
       
    
    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Check to make sure a valid Fee id was passed in

    IF  (@ClientFeeID IS NULL) OR
        (NOT EXISTS(SELECT  ClientFeeID 
                      FROM  dbo.utb_client_fee 
                      WHERE ClientFeeID = @ClientFeeID 
                        AND InsuranceCompanyID = (SELECT  c.InsuranceCompanyID
                                                  FROM  dbo.utb_invoice i
                                                        INNER JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
                                                        INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                                                  WHERE i.InvoiceID = @InvoiceID)))
    BEGIN
        -- Invalid Fee ID
    
        RAISERROR('101|%s|@ClientFeeID|%u', 16, 1, @ProcName, @ClientFeeID)
        RETURN
    END


    -- Check to make sure a valid Service ID was passed in for this fee
    
    IF  (@ServiceID IS NULL) OR
        (NOT EXISTS(SELECT ServiceID FROM dbo.utb_client_fee_definition WHERE ClientFeeID = @ClientFeeID AND ServiceID = @ServiceID))
    BEGIN
        -- Invalid Service ID
    
        RAISERROR('101|%s|@ServiceID|%u', 16, 1, @ProcName, @ServiceID)
        RETURN
    END 
       
    
    -- Get information for the fee service

    SELECT  @ItemizeFlag    = ItemizeFlag
      FROM  dbo.utb_client_fee_definition
      WHERE ClientFeeID = @ClientFeeID
        AND ServiceID = @ServiceID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Verify APD Data State

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Insured')
    BEGIN
       -- Involved Role Type Not Found
    
        RAISERROR('102|%s|"Insured"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Claimant')
    BEGIN
       -- Involved Role Type Not Found
    
        RAISERROR('102|%s|"Claimant"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END


    -- Get the Lynx ID and ClaimAspectType/Number for this claim aspect

    SELECT  @LynxID = LynxID,
            @ClaimAspectTypeID = ClaimAspectTypeID,
            @ClaimAspectNumber = ClaimAspectNumber
      FROM  dbo.utb_claim_aspect 
      WHERE ClaimAspectID = @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Generate UId
    SELECT @UIdSuffix = CASE (SELECT Name FROM dbo.utb_claim_aspect_type WHERE ClaimAspectTypeID = @ClaimAspectTypeID)
                            WHEN 'Vehicle'  THEN '-' + Convert(varchar(3), @ClaimAspectNumber)
                            WHEN 'Property' THEN '-P' + Convert(varchar(3), @ClaimAspectNumber)
                            ELSE ''
                        END

    SET @UId = Convert(varchar(8), @LynxID) + @UIdSuffix


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP
    

    -- Begin Insert

    BEGIN TRANSACTION InvoiceInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_invoice_service
   	(
        InvoiceID,
        ServiceID,
        ClaimAspectID,
        ClaimantName,
        DispositionTypeCD,
        EnabledFlag,
        ItemizeFlag,
        ServiceChannelCD,
        UID,   
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @InvoiceID,
            @ServiceID,
            @ClaimAspectID,
            (SELECT  CASE 
                        WHEN LEN(LTRIM(RTRIM(i.BusinessName))) > 0 THEN i.BusinessName
                        WHEN LEN(LTRIM(RTRIM(i.NameLast)) + ',' + LTRIM(RTRIM(i.NameLast))) > 1 THEN LEFT(i.NameLast + ', ' + i.NameFirst, 50)
                        ELSE NULL
                     END
               FROM  dbo.utb_claim_aspect_involved cai
               LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
               LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
               LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
               WHERE cai.ClaimAspectID = @ClaimAspectID
                 AND cai.EnabledFlag = 1
                 AND (irt.Name = 'Claimant' OR irt.Name = 'Insured')),
            @DispositionTypeCD,
            1,          -- Enabled
            @ItemizeFlag,
            @ServiceChannelCD,
            @UId,
            @UserID,
            @now
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('105|%s|utb_invoice_service', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    
    COMMIT TRANSACTION InvoiceInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceServiceInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspInvoiceServiceInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/ 