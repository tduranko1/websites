-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientMonthly' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2ClientMonthly 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2ClientMonthly
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Compiles data displayed on the Client Monthly Report
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company the report is being run for
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
*
* RESULT SET:
*       InsuranceCompanyName - The insurance company name (for report title)
*       ReportMonth - The name of the month the report is being run for
*       ReportYear - The year the report is being run for
*       Level3Display - Display Name for Detail line items on report
*       Level3Sort - The sort order of the Detai Line Items on the report
*       Level1Group - Top level group code ('Month', 'YTD')
*       Level2Group - 2nd level group code ('DA', 'CS')
*       Level3Group - Detail line code ('PS', 'DR', 'IA', 'SA', 'TTL', 'LU', TL')
*       NewTotal - Total of new exposures
*       NewPercent - Percentage of New TTL Total
*       PendingTotal - Total of pending exposures
*       PendingPercent - Percentage of pending TTL total
*       ClosedTotal - Total of closed exposures
*       ClosedPercent - Percentage of closed TTL total
*       AvgCollCycleTime - Avg Collision Cycle Time
*       AvgCollSeverity - Avg Collision Severity
*       AvgCollCount   -  Collision Count
*       AvgCompCycleTime - Avg Comprehensive Cycle Time
*       AvgCompSeverity - Avg Comprehensive Severity
*       AvgCompCount   -  Comprehensive Count
*       AvgPDCycleTime - Avg Liability Cycle Time
*       AvgPDSeverity - Avg Liability Severity
*       AvgPDCount   -  Liabilityl Count
*       AvgOtherCycleTime - Avg UIM + UM  Cycle Time
*       AvgOtherSeverity - Avg UIM + UM Severity
*       AvgOtherCount   - UIM + UM Count  
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2ClientMonthly
    @InsuranceCompanyID     udt_std_int_small,
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare internal variables

    DECLARE @tmpReportDataMonthDA TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Level3Group                 varchar(20)  NOT NULL,
        NewTotal                    bigint       NULL,
        NewPercent                  int          NULL,
        PendingTotal                bigint       NULL,
        PendingPercent              int          NULL,
        ClosedTotal                 bigint       NULL,
        ClosedPercent               int          NULL,
        AvgCollCycleTime            decimal(6,1) NULL,
        AvgCollSeverity             int          NULL,
		AvgCollCount	     		int 	     NULL,
        AvgCompCycleTime            decimal(6,1) NULL,
        AvgCompSeverity             int          NULL,
		AvgCompCount	     		int 	     NULL,
        AvgPDCycleTime              decimal(6,1) NULL,
        AvgPDSeverity               int          NULL,
        AvgPDCount               	int          NULL,
        AvgOtherCycleTime           decimal(6,1) NULL,
		AvgOtherSeverity			int			 NULL,		
		AvgOtherCount				int			 NULL,		
        AvgCycleTime                decimal(6,1) NULL,
        AvgServerity                int          NULL
    )

    DECLARE @tmpReportDataMonthCS TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Level3Group                 varchar(20)  NOT NULL,
        NewTotal                    bigint       NULL,
        NewPercent                  int          NULL,
        PendingTotal                bigint       NULL,
        PendingPercent              int          NULL,
        ClosedTotal                 bigint       NULL,
        ClosedPercent               int          NULL,
        AvgCollCycleTime            decimal(6,1) NULL,
        AvgCollSeverity             int          NULL,
		AvgCollCount	     		int 	     NULL,
        AvgCompCycleTime            decimal(6,1) NULL,
        AvgCompSeverity             int          NULL,
		AvgCompCount	     		int 	     NULL,
        AvgPDCycleTime              decimal(6,1) NULL,
        AvgPDSeverity               int          NULL,
		AvgPDCount               	int          NULL,
		AvgOtherCycleTime           decimal(6,1) NULL,
		AvgOtherSeverity            int		     NULL,
		AvgOtherCount				int			 NULL,		
        AvgCycleTime                decimal(6,1) NULL,
        AvgServerity                int          NULL
    )

    DECLARE @tmpReportDataYTDDA TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Level3Group                 varchar(20)  NOT NULL,
        NewTotal                    bigint       NULL,
        NewPercent                  int          NULL,
        PendingTotal                bigint       NULL,
        PendingPercent              int          NULL,
        ClosedTotal                 bigint       NULL,
        ClosedPercent               int          NULL,
        AvgCollCycleTime            decimal(6,1) NULL,
        AvgCollSeverity             int          NULL,
		AvgCollCount	     		int 	     NULL,
        AvgCompCycleTime            decimal(6,1) NULL,
        AvgCompSeverity             int          NULL,
		AvgCompCount	     		int 	     NULL,
        AvgPDCycleTime              decimal(6,1) NULL,
        AvgPDSeverity               int          NULL,
		AvgPDCount               	int          NULL,
  	    AvgOtherCycleTime           decimal(6,1) NULL,
		AvgOtherSeverity            int		     NULL,
		AvgOtherCount				int			 NULL,		    
        AvgCycleTime                decimal(6,1) NULL,
        AvgServerity                int          NULL
    )

    DECLARE @tmpReportDataYTDCS TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Level3Group                 varchar(20)  NOT NULL,
        NewTotal                    bigint       NULL,
        NewPercent                  int          NULL,
        PendingTotal                bigint       NULL,
        PendingPercent              int          NULL,
        ClosedTotal                 bigint       NULL,
        ClosedPercent               int          NULL,
        AvgCollCycleTime            decimal(6,1) NULL,
        AvgCollSeverity             int          NULL,
		AvgCollCount	     		int 	     NULL,
        AvgCompCycleTime            decimal(6,1) NULL,
        AvgCompSeverity             int          NULL,
		AvgCompCount	     		int 	     NULL,
        AvgPDCycleTime              decimal(6,1) NULL,
        AvgPDSeverity               int          NULL,
		AvgPDCount               	int          NULL,
	  	AvgOtherCycleTime           decimal(6,1) NULL,
		AvgOtherSeverity            int		     NULL,    
		AvgOtherCount				int			 NULL,		    
        AvgCycleTime                decimal(6,1) NULL,
        AvgServerity                int          NULL
    )

    DECLARE @tmpNewData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        NewCount                    int          NOT NULL,
        NewPercent                  int          NULL
    )
        
    DECLARE @tmpPendingData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        PendingCount                int          NOT NULL,
        PendingPercent              int          NULL
    )

    DECLARE @tmpClosedData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        CoverageTypeCD              varchar(4)   NOT NULL,        ClosedCount                 int          NOT NULL,
        ClosedPercent               int          NULL,
        AvgSeverity                 int          NULL,
        AvgCycleTimeDays            decimal(6,1) NOT NULL
    )
    
    DECLARE @ClosedCountMonth       udt_std_int
    DECLARE @ClosedCountYTD         udt_std_int
    DECLARE @InsuranceCompanyName   udt_std_name
    DECLARE @InsuranceCompanyLCPhone udt_std_name
    DECLARE @MonthName              udt_std_name
    DECLARE @NewCountMonth          udt_std_int
    DECLARE @NewCountYTD            udt_std_int
    DECLARE @PendingCountMonth      udt_std_int
    DECLARE @RptDate                datetime
    DECLARE @PSClosedCountMonth     udt_std_int
    DECLARE @PSClosedCountYTD       udt_std_int

    DECLARE @Debug      udt_std_flag
    DECLARE @now        udt_std_datetime
    DECLARE @DataWareHouseDate    udt_std_datetime

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2ClientMonthly'

    SET @Debug = 0


    -- Validate Insurance Company ID
    
    IF (@InsuranceCompanyID IS NULL) OR
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID
        
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name,
                @InsuranceCompanyLCPhone = IsNull(CarrierLynxContactPhone, '239-337-4300')
          FROM  dbo.utb_insurance
          WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END          
        
   
    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    
    SELECT  @DataWareHouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @RptDate = convert(datetime, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
        SET @MonthName = DateName(mm, @RptDate)
    END
    
    
    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID = ' + Convert(varchar, @InsuranceCompanyID)
        PRINT '@InsuranceCompanyName = ' + @InsuranceCompanyName
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptMonthName = ' + @MonthName
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear, 101)
        
        PRINT '@RptDate = ' + Convert(varchar, @RptDate, 101)
    END
    
    
    -- Compile data needed for the report
    
    -- Monthly Damage Assessment Totals
    
    -- New 
    
    INSERT INTO @tmpNewData
      SELECT  'TTL',
              Count(*) AS OpenCount,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    SELECT @NewCountMonth = NewCount
      FROM @tmpNewData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
          
    INSERT INTO @tmpNewData
      SELECT  dsc.ServiceChannelCD,
              Count(*) AS OpenCount,
              Round(Convert(decimal(9,2), Count(*)) / Convert(decimal(9,2), @NewCountMonth), 2) * 100 AS NewPercent
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.CoverageTypeID IS NOT NULL
  		  AND fc.EnabledFlag = 1
        GROUP BY dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    -- Pending
    
    INSERT INTO @tmpPendingData
      SELECT  'TTL',
              Count(*) AS PendingCount,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtn ON (fc.TimeIDNew = dtn.TimeID)
        --LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)        
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        WHERE dtn.DateValue < @RptDate
          AND fc.TimeIDClosed IS NULL
          AND fc.TimeIDCancelled IS NULL
          AND fc.TimeIDVoided IS NULL
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
  		  AND fc.EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpPendingData', 16, 1, @ProcName)
        RETURN
    END
          
    SELECT @PendingCountMonth = PendingCount
      FROM @tmpPendingData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
          
    INSERT INTO @tmpPendingData
      SELECT  dsc.ServiceChannelCD,
              Count(*) AS PendingCount,
              Round(Convert(decimal(9,2), Count(*)) / Convert(decimal(9,2), @PendingCountMonth), 2) * 100 AS PendingPercent
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtn ON (fc.TimeIDNew = dtn.TimeID)
        --LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)        
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dtn.DateValue < @RptDate
          AND fc.TimeIDClosed IS NULL
          AND fc.TimeIDCancelled IS NULL
          AND fc.TimeIDVoided IS NULL
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.CoverageTypeID IS NOT NULL
 		  AND fc.EnabledFlag = 1
        GROUP BY dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpPendingData', 16, 1, @ProcName)
        RETURN
    END


    -- Closed
    
    INSERT INTO @tmpClosedData
      SELECT  'TTL',
              dct.CoverageTypeCD,
              Count(*) AS ClosedCount,
              NULL,
              Round(Avg(fc.IndemnityAmount), 0) AS AvgSeverity,
              Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1) AS AvgCycleTime
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeClosingID IS NOT NULL
  		  AND fc.EnabledFlag = 1
        GROUP BY dct.CoverageTypeCD
        HAVING dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    SELECT @ClosedCountMonth = SUM(ClosedCount)
      FROM @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
            
    INSERT INTO @tmpClosedData
      SELECT  dsc.ServiceChannelCD,
              dct.CoverageTypeCD,
              Count(*) AS ClosedCount,
              Round(Convert(decimal(9,2), Count(*)) / Convert(decimal(9,2), @ClosedCountMonth), 2) * 100 AS ClosedPercent,
              Round(Avg(fc.IndemnityAmount), 0) AS AvgSeverity,
              Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1) AS AvgCycleTime
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
  		  AND fc.EnabledFlag = 1
        GROUP BY dsc.ServiceChannelCD, dct.CoverageTypeCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
          AND dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

	 SELECT @PSClosedCountMonth = SUM(ClosedCount)
      FROM @tmpClosedData WHERE LineItemCD  ='PS'
	
    -- Pivot and save the data extracted

    INSERT INTO @tmpReportDataMonthDA
      SELECT  'Month',
              'DA',
              LineItemCD,
              0,
              0,
              0,
              0,
              SUM(ClosedCount),
              SUM(ClosedPercent),
              SUM(CASE CoverageTypeCD WHEN 'COLL' THEN AvgCycleTimeDays ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'COLL' THEN AvgSeverity ELSE 0 END),
			  SUM(CASE CoverageTypeCD WHEN 'COLL' THEN ClosedCount ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'COMP' THEN AvgCycleTimeDays ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'COMP' THEN AvgSeverity ELSE 0 END),
			  SUM(CASE CoverageTypeCD WHEN 'COMP' THEN ClosedCount ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'LIAB' THEN AvgCycleTimeDays ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'LIAB' THEN AvgSeverity ELSE 0 END),
			  SUM(CASE CoverageTypeCD WHEN 'LIAB' THEN ClosedCount ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'UIM' THEN AvgCycleTimeDays ELSE 0 END) + SUM(CASE CoverageTypeCD WHEN 'UM' THEN AvgCycleTimeDays ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'UIM' THEN AvgSeverity ELSE 0 END) + SUM(CASE CoverageTypeCD WHEN 'UM' THEN AvgSeverity ELSE 0 END),
			  SUM(CASE CoverageTypeCD WHEN 'UIM' THEN ClosedCount ELSE 0 END)+ SUM(CASE CoverageTypeCD WHEN 'UM' THEN ClosedCount ELSE 0 END),
              SUM(ClosedCount * AvgCycleTimeDays) / SUM(ClosedCount),
              SUM(ClosedCount * AvgSeverity) / SUM(ClosedCount)
        FROM  @tmpClosedData
        GROUP BY LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END
          
          
    UPDATE  @tmpReportDataMonthDA
      SET   NewTotal = nd.NewCount,
            NewPercent = nd.NewPercent
      FROM  @tmpNewData nd
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, NewTotal, NewPercent)
      SELECT  'Month',
              'DA',
              LineItemCD,
              NewCount,
              NewPercent
        FROM  @tmpNewData
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA)
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END
          
    UPDATE  @tmpReportDataMonthDA
      SET   PendingTotal = pd.PendingCount,
            PendingPercent = pd.PendingPercent
      FROM  @tmpPendingData pd
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, PendingTotal, PendingPercent)
      SELECT  'Month',
              'DA',
              LineItemCD,
              PendingCount,
              PendingPercent
        FROM  @tmpPendingData
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA
      SELECT  'Month',
              'DA',
              dsc.ServiceChannelCD,
              0, 0,
              0, 0, 
              0, 0, 
              0, 0, 0,
              0, 0, 0,
              0, 0, 0,
              0, 0, 0,
              0, 0
        FROM  dbo.utb_dtwh_dim_service_channel dsc
        WHERE dsc.ServiceChannelCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA)
          AND dsc.ServiceChannelCD IN (SELECT  at.ServiceChannelDefaultCD 
                                         FROM  dbo.utb_client_assignment_type cat
                                         LEFT JOIN dbo.utb_assignment_type at ON (cat.AssignmentTypeID = at.AssignmentTypeID)
                                         WHERE cat.InsuranceCompanyID = @InsuranceCompanyID)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END


    -- Reset the temporary tables for next cycle
    
    DELETE FROM @tmpNewData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    DELETE FROM @tmpPendingData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpPendingData', 16, 1, @ProcName)
        RETURN
    END

    DELETE FROM @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END


    -- YTD Damage Assessment Totals
    
    -- New 
    
    INSERT INTO @tmpNewData
      SELECT  'TTL',
              Count(*) AS OpenCount,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        WHERE dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
  		  AND fc.EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName) 
        RETURN
    END
          
    SELECT @NewCountYTD = NewCount
      FROM @tmpNewData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
          
    INSERT INTO @tmpNewData
      SELECT  dsc.ServiceChannelCD,
              Count(*) AS OpenCount,
              Round(Convert(decimal(9,2), Count(*)) / Convert(decimal(9,2), @NewCountYTD), 2) * 100 AS NewPercent
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.CoverageTypeID IS NOT NULL
  		  AND fc.EnabledFlag = 1
        GROUP BY dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName) 
        RETURN
    END


    -- Closed
    
    INSERT INTO @tmpClosedData
      SELECT  'TTL',
              dct.CoverageTypeCD,
              Count(*) AS ClosedCount,
              NULL,
              Round(Avg(fc.IndemnityAmount), 0) AS AvgSeverity,
              Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1) AS AvgCycleTime
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeClosingID IS NOT NULL
  		  AND fc.EnabledFlag = 1
        GROUP BY dct.CoverageTypeCD
        HAVING dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    SELECT @ClosedCountYTD = SUM(ClosedCount)
      FROM @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
            
    INSERT INTO @tmpClosedData
      SELECT  dsc.ServiceChannelCD,
              dct.CoverageTypeCD,
              Count(*) AS ClosedCount,
              Round(Convert(decimal(9,2), Count(*)) / Convert(decimal(9,2), @ClosedCountYTD), 2) * 100 AS ClosedPercent,
              Round(Avg(fc.IndemnityAmount), 0) AS AvgSeverity,
              Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1) AS AvgCycleTime
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
  		  AND fc.EnabledFlag = 1
        GROUP BY dsc.ServiceChannelCD, dct.CoverageTypeCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
          AND dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    SELECT @PSClosedCountYTD = SUM(ClosedCount)
    FROM @tmpClosedData WHERE LineItemCD = 'PS'
	
    -- Pivot and save the data extracted

    INSERT INTO @tmpReportDataYTDDA
      SELECT  'YTD',
              'DA',
              LineItemCD,
              0,
              0,
              NULL,
              NULL,
              SUM(ClosedCount),
              SUM(ClosedPercent),
              SUM(CASE CoverageTypeCD WHEN 'COLL' THEN AvgCycleTimeDays ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'COLL' THEN AvgSeverity ELSE 0 END),
			  SUM(CASE CoverageTypeCD WHEN 'COLL' THEN ClosedCount ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'COMP' THEN AvgCycleTimeDays ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'COMP' THEN AvgSeverity ELSE 0 END),
			  SUM(CASE CoverageTypeCD WHEN 'COMP' THEN ClosedCount ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'LIAB' THEN AvgCycleTimeDays ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'LIAB' THEN AvgSeverity ELSE 0 END),
			  SUM(CASE CoverageTypeCD WHEN 'LIAB' THEN ClosedCount ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'UIM' THEN AvgCycleTimeDays ELSE 0 END) + SUM(CASE CoverageTypeCD WHEN 'UM' THEN AvgCycleTimeDays ELSE 0 END),
              SUM(CASE CoverageTypeCD WHEN 'UIM' THEN AvgSeverity ELSE 0 END) + SUM(CASE CoverageTypeCD WHEN 'UM' THEN AvgSeverity ELSE 0 END),
			  SUM(CASE CoverageTypeCD WHEN 'UIM' THEN ClosedCount ELSE 0 END)+ SUM(CASE CoverageTypeCD WHEN 'UM' THEN ClosedCount ELSE 0 END),
              SUM(ClosedCount * AvgCycleTimeDays) / SUM(ClosedCount),
              SUM(ClosedCount * AvgSeverity) / SUM(ClosedCount)
        FROM  @tmpClosedData
        GROUP BY LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTDDA', 16, 1, @ProcName) 
        RETURN
    END
          
    UPDATE  @tmpReportDataYTDDA
      SET   NewTotal = nd.NewCount,
            NewPercent = nd.NewPercent
      FROM  @tmpNewData nd
      WHERE Level1Group = 'YTD'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataYTDDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataYTDDA (Level1Group, Level2Group, Level3Group, NewTotal, NewPercent)
      SELECT  'YTD',
              'DA',
              LineItemCD,
              NewCount,
              NewPercent
        FROM  @tmpNewData
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataYTDDA)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTDDA', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpReportDataYTDDA
      SELECT  'YTD',
              'DA',
              dsc.ServiceChannelCD,
              0, 0, 
              NULL, NULL, 
              0, 0, 
              0, 0, 0,
              0, 0, 0,
              0, 0, 0,
              0, 0, 0,
              0, 0
        FROM  dbo.utb_dtwh_dim_service_channel dsc
        WHERE dsc.ServiceChannelCD NOT IN (SELECT Level3Group FROM @tmpReportDataYTDDA)
          AND dsc.ServiceChannelCD IN (SELECT  at.ServiceChannelDefaultCD 
                                         FROM  dbo.utb_client_assignment_type cat
                                         LEFT JOIN dbo.utb_assignment_type at ON (cat.AssignmentTypeID = at.AssignmentTypeID)
                                         WHERE cat.InsuranceCompanyID = @InsuranceCompanyID)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTDDA', 16, 1, @ProcName)
        RETURN
    END
        

    -- Reset the temporary tables for next cycle
    
    DELETE FROM @tmpNewData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    DELETE FROM @tmpPendingData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpPendingData', 16, 1, @ProcName)
        RETURN
    END

    DELETE FROM @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END


    -- Monthly Claim Services Totals
    
    -- Looking at closed claims only
    
    INSERT INTO @tmpClosedData
      SELECT  'LU',
              dct.CoverageTypeCD,
              Count(*) AS ClosedCount,
              Round(Convert(decimal(9,2), Count(*)) / Convert(decimal(9,2), @PSClosedCountMonth), 2) * 100 AS ClosedPercent,
              NULL,
              Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1) AS AvgCycleTime
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.ServiceLossOfUseFlag = 1
  		  AND fc.EnabledFlag = 1
        GROUP BY dct.CoverageTypeCD
        HAVING dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpClosedData
      SELECT  'TL',
              dct.CoverageTypeCD,
              Count(*) AS ClosedCount,
              Round(Convert(decimal(9,2), Count(*)) / Convert(decimal(9,2), @PSClosedCountMonth), 2) * 100 AS ClosedPercent,
              NULL,
              Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1) AS AvgCycleTime
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.ServiceTotalLossFlag = 1
  		  AND fc.EnabledFlag = 1
        GROUP BY dct.CoverageTypeCD
        HAVING dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END
 
    -- Pivot and save the data extracted

    INSERT INTO @tmpReportDataMonthCS
      SELECT  'Month',
              'CS',
              LineItemCD,
              NULL,
              NULL,
              NULL,
              NULL,
              SUM(ClosedCount),
              SUM(ClosedPercent),
              SUM(CASE CoverageTypeCD WHEN 'COLL' THEN AvgCycleTimeDays ELSE 0 END),
              0,
			  0,
              SUM(CASE CoverageTypeCD WHEN 'COMP' THEN AvgCycleTimeDays ELSE 0 END),
              0,
			  0,
              SUM(CASE CoverageTypeCD WHEN 'LIAB' THEN AvgCycleTimeDays ELSE 0 END),
              0,
			  0,
              SUM(CASE CoverageTypeCD WHEN 'UIM' THEN AvgCycleTimeDays ELSE 0 END) + SUM(CASE CoverageTypeCD WHEN 'UM' THEN AvgCycleTimeDays ELSE 0 END),
              0,
			  0,
              SUM(ClosedCount * AvgCycleTimeDays) / SUM(ClosedCount),
              0
        FROM  @tmpClosedData
        GROUP BY LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthCS', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpReportDataMonthCS
      SELECT  'Month',
              'CS',
              value,
              NULL, NULL, NULL, NULL, 
			  0,0,0,0,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM  dbo.ufnUtilityParseString('LU,TL', ',', 1)
        WHERE value NOT IN (SELECT Level3Group FROM @tmpReportDataMonthCS)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthCS', 16, 1, @ProcName)
        RETURN
    END
          

    -- Reset the temporary tables for next cycle
    
    DELETE FROM @tmpNewData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    DELETE FROM @tmpPendingData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpPendingData', 16, 1, @ProcName)
        RETURN
    END

    DELETE FROM @tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END


    -- YTD Claim Services Totals
    
    -- Looking at closed claims only
    
    INSERT INTO @tmpClosedData
      SELECT  'LU',
              dct.CoverageTypeCD,
              Count(*) AS ClosedCount,
              Round(Convert(decimal(9,2), Count(*)) / Convert(decimal(9,2), @PSClosedCountYTD), 2) * 100 AS ClosedPercent,
              NULL,
              Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1) AS AvgCycleTime
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.ServiceLossOfUseFlag = 1
  		  AND fc.EnabledFlag = 1
        GROUP BY dct.CoverageTypeCD
        HAVING dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpClosedData
      SELECT  'TL',
              dct.CoverageTypeCD,
              Count(*) AS ClosedCount,
              Round(Convert(decimal(9,2), Count(*)) / Convert(decimal(9,2), @PSClosedCountYTD), 2) * 100 AS ClosedPercent,
              NULL,
              Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1) AS AvgCycleTime
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.ServiceTotalLossFlag = 1
  		  AND fc.EnabledFlag = 1
        GROUP BY dct.CoverageTypeCD
        HAVING dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END
  
    -- Pivot and save the data extracted

    INSERT INTO @tmpReportDataYTDCS
      SELECT  'YTD',
              'CS',
              LineItemCD,
              NULL,
              NULL,
              NULL,
              NULL,
              SUM(ClosedCount),
              SUM(ClosedPercent),
              SUM(CASE CoverageTypeCD WHEN 'COLL' THEN AvgCycleTimeDays ELSE 0 END),
              0,
			  0,
              SUM(CASE CoverageTypeCD WHEN 'COMP' THEN AvgCycleTimeDays ELSE 0 END),
              0,
			  0,
              SUM(CASE CoverageTypeCD WHEN 'LIAB' THEN AvgCycleTimeDays ELSE 0 END),
              0,
			  0,
              SUM(CASE CoverageTypeCD WHEN 'UIM' THEN AvgCycleTimeDays ELSE 0 END) + SUM(CASE CoverageTypeCD WHEN 'UM' THEN AvgCycleTimeDays ELSE 0 END),
              0,
			  0,
              SUM(ClosedCount * AvgCycleTimeDays) / SUM(ClosedCount),
              0
        FROM  @tmpClosedData
        GROUP BY LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTDCS', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpReportDataYTDCS
      SELECT  'YTD',
              'CS',
              value,
              NULL, NULL, NULL, NULL, 
			  0,0,0,0,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM  dbo.ufnUtilityParseString('LU,TL', ',', 1)
        WHERE value NOT IN (SELECT Level3Group FROM @tmpReportDataYTDCS)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTDCS', 16, 1, @ProcName)
        RETURN
    END
    

    -- Final Select
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @MonthName AS ReportMonth,
            @RptYear AS ReportYear,
            CASE tmp.Level3Group
              WHEN 'PS' THEN 'Program Shop'
              WHEN 'DR' THEN 'Desk Review'
              WHEN 'DA' THEN 'Desk Audit'
              WHEN 'IA' THEN 'Indepemdent Adj.'
              WHEN 'SA' THEN 'Client Field Staff'
              WHEN 'TTL' THEN 'Total Assignments'
            END AS Level3Display,
            CASE tmp.Level3Group
              WHEN 'PS' THEN 1
              WHEN 'DR' THEN 2
              WHEN 'DA' THEN 3
              WHEN 'IA' THEN 4
              WHEN 'SA' THEN 5
              WHEN 'TTL' THEN 6
            END AS Level3Sort,
            tmp.* ,
         @DataWareHouseDate as DataWareDate
      FROM  @tmpReportDataMonthDA tmp
    
    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @MonthName,
            @RptYear,
            CASE tmp.Level3Group
              WHEN 'PS' THEN 'Program Shop'
              WHEN 'DR' THEN 'Desk Review'
              WHEN 'DA' THEN 'Desk Audit'
              WHEN 'IA' THEN 'Indepemdent Adj.'
              WHEN 'SA' THEN 'Client Field Staff'
              WHEN 'TTL' THEN 'Total Assignments'
            END,
            CASE tmp.Level3Group
              WHEN 'PS' THEN 1
              WHEN 'DR' THEN 2
              WHEN 'DA' THEN 3
              WHEN 'IA' THEN 4
              WHEN 'SA' THEN 5
              WHEN 'TTL' THEN 6
            END,
            tmp.* ,
          @DataWareHouseDate as DataWareDate
      FROM  @tmpReportDataYTDDA tmp
    
    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @MonthName,
            @RptYear,
            CASE tmp.Level3Group
              WHEN 'LU' THEN 'Loss Of Use'
              WHEN 'TL' THEN 'Total Loss'
            END,
            CASE tmp.Level3Group
              WHEN 'LU' THEN 1
              WHEN 'TL' THEN 2
            END,
            tmp.* ,
           @DataWareHouseDate as DataWareDate
      FROM  @tmpReportDataMonthCS tmp
    
    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @MonthName,
            @RptYear,
            CASE tmp.Level3Group
              WHEN 'LU' THEN 'Loss Of Use'
              WHEN 'TL' THEN 'Total Loss'
            END,
            CASE tmp.Level3Group
              WHEN 'LU' THEN 1
              WHEN 'TL' THEN 2
            END,
            tmp.* ,
          @DataWareHouseDate as DataWareDate
      FROM  @tmpReportDataYTDCS tmp




    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientMonthly' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2ClientMonthly TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/