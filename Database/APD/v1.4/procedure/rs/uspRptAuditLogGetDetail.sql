BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptAuditLogGetDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptAuditLogGetDetail
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspRptAuditLogGetDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieve report info from utb_audit_log
*
* PARAMETERS:

*
* RESULT SET:
*   The new last updated date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptAuditLogGetDetail
  @AuditTypeCD          AS udt_std_cd=null,
  @UserID               AS udt_std_id=null,
  @InsuranceCompanyID   AS udt_std_id=null,
  @RptFromDate          AS udt_std_datetime=null,
  @RptToDate            AS udt_std_datetime=null,
  @ReportTypeCD         AS udt_std_cd='A'

AS
BEGIN

  DECLARE @ProcName             udt_std_name
  DECLARE @now                  udt_std_datetime


  DECLARE @frmDate              udt_std_datetime
  DECLARE @toDate               udt_std_datetime
  DECLARE @tmpDate              udt_std_datetime


  DECLARE @tmpAuditCode TABLE (Code     varchar(4)    NOT NULL,
                               Name     varchar(50)   NOT NULL)

  DECLARE @tmpAudit TABLE (AuditType            varchar(100)       NOT NULL,
                           InsuranceCompanyID   int                   NULL,
                           Insurance            varchar(50)           NULL,
                           KeyDescription       varchar(500)           NULL,
                           KeyID                int                   NULL,
                           LogComment           varchar(500)          NULL,
                           UserName             varchar(101)          NULL,
                           SysLastUpdatedDate   datetime              NULL,
                           ToDate               varchar(100)      NOT NULL)



  SET @now = CURRENT_TIMESTAMP
  SET @ProcName = 'uspRptAuditLogGetDetail'


  SET NOCOUNT ON

  INSERT INTO @tmpAuditCode
  SELECT code, name FROM dbo.ufnUtilityGetReferenceCodes('utb_audit_log', 'AuditTypeCD')


  IF @@ERROR <> 0
  BEGIN
      -- Insertion failure

      RAISERROR('105|%s|@tmpAuditCode', 16, 1, @ProcName)
      RETURN
  END


  -- validate parameters
  IF (@ReportTypeCD) <> 'A' AND (@ReportTypeCD <> 'M')    -- A - Automatic    M - Manual
  BEGIN
    -- SQL Server Error

      RAISERROR('101|%s|@ReportTypeCD|%s', 16, 1, @ProcName, @ReportTypeCD)
      RETURN
  END

  DECLARE @ReportDays int

  IF @RptFromDate IS NULL OR LEN(@RptFromDate) = 0
  BEGIN
    IF (@AuditTypeCD = 'F' and @ReportTypeCD = 'A')
      SET @ReportDays = 5
    ELSE
      SET @ReportDays = 1

    SET @frmDate = CONVERT(datetime, convert(varchar, DATEADD(day, @ReportDays * -1, @now), 101))
  END
  ELSE
    SET @frmDate = @RptFromDate

  IF @RptToDate IS NULL OR LEN(@RptToDate) = 0
    SET @toDate = CONVERT(datetime, CONVERT(varchar, @now, 101) + ' 23:59:59')
  ELSE
    SET @toDate = CONVERT(datetime, @RptToDate +  ' 23:59:59')

  IF @frmDate > @toDate
  BEGIN
      SET @tmpDate = @toDate
      SET @toDate = @frmDate
      SET @frmDate = @tmpDate
  END


  IF @@ERROR <> 0
  BEGIN
      -- SQL Server Error

      RAISERROR('99|%s', 16, 1, @ProcName)
      RETURN
  END


  IF (@ReportTypeCD = 'A')
  BEGIN
    SELECT (SELECT Name FROM dbo.utb_insurance WHERE InsuranceCompanyID = al.InsuranceCompanyID) Insurance,
           (SELECT CASE @AuditTypeCD
              WHEN 'F' THEN
                KeyDescription +
                '  Amount:  ' + (SELECT CONVERT(varchar(20), FeeAmount) FROM dbo.utb_client_fee
                                                                    WHERE ClientFeeID = al.KeyID) +
                '  Effective Start: ' + ISNULL((SELECT CONVERT(varchar(20), EffectiveStartDate) FROM dbo.utb_client_fee
                                                                    WHERE ClientFeeID = al.KeyID) ,'NONE') +
                '  Expiration End: ' + ISNULL((SELECT CONVERT(varchar(20), EffectiveEndDate) FROM dbo.utb_client_fee
                                                                    WHERE ClientFeeID = al.KeyID) ,'NONE')
              ELSE
                KeyDescription
            END)  Description,
           LogComment Comment,
           (SELECT ISNULL(NameFirst, '') + ' ' + ISNULL(NameLast, '')
            FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
            WHERE a.Name = 'APD'
              AND u.UserID = al.SysLastUserID) 'Changed By',
            al.SysLastUpdatedDate 'Changed Date'
    FROM dbo.utb_audit_log al INNER JOIN @tmpAuditCode ac ON al.AuditTypeCD = ac.Code
    WHERE (SysLastUpdatedDate >= @frmDate) AND (SysLastUpdatedDate <= @toDate)
      AND ((@AuditTypeCD IS NULL) OR (AuditTypeCD = @AuditTypeCD))
      AND ((@UserID IS NULL) OR (SysLastUserID = @UserID))
      AND ((@InsuranceCompanyID IS NULL) OR (al.InsuranceCompanyID = @InsuranceCompanyID))
      AND LogComment LIKE 'Amount%'
    --GROUP BY AuditTypeCD, al.InsuranceCompanyID, KeyDescription, LogComment, al.SysLastUserID, al.SysLastUpdatedDate
    ORDER BY AuditTypeCD, Insurance, SysLastUpdatedDate, SysLastUserID
  END
  ELSE
  BEGIN
    INSERT INTO @tmpAudit
    SELECT ac.Name AuditType,
           InsuranceCompanyID,
           (SELECT Name FROM dbo.utb_insurance WHERE InsuranceCompanyID = al.InsuranceCompanyID) Insurance,
           (SELECT CASE @AuditTypeCD
              WHEN 'F' THEN
                KeyDescription +
                '  Amount:  ' + (SELECT CONVERT(varchar(20), FeeAmount) FROM dbo.utb_client_fee
                                                                    WHERE ClientFeeID = al.KeyID) +
                '  Effective Start: ' + ISNULL((SELECT CONVERT(varchar(20), EffectiveStartDate) FROM dbo.utb_client_fee
                                                                    WHERE ClientFeeID = al.KeyID) ,'NONE') +
                '  Expiration End: ' + ISNULL((SELECT CONVERT(varchar(20), EffectiveEndDate) FROM dbo.utb_client_fee
                                                                    WHERE ClientFeeID = al.KeyID) ,'NONE')
              ELSE
                KeyDescription
              END) KeyDescription,
           KeyID,
           LogComment,
           (SELECT ISNULL(NameFirst, '') + ' ' + ISNULL(NameLast, '')
            FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
            WHERE a.Name = 'APD'
              AND u.UserID = al.SysLastUserID) 'User',
           al.SysLastUpdatedDate,
           convert(varchar(10), @frmDate, 101) + ' - ' + convert(varchar(10), @toDate, 101) ToDate
    FROM dbo.utb_audit_log al INNER JOIN @tmpAuditCode ac ON al.AuditTypeCD = ac.Code
    WHERE (SysLastUpdatedDate >= @frmDate) AND (SysLastUpdatedDate <= @toDate)
      AND ((@AuditTypeCD IS NULL) OR (AuditTypeCD = @AuditTypeCD))
      AND ((@UserID IS NULL) OR (SysLastUserID = @UserID))
      AND ((@InsuranceCompanyID IS NULL) OR (al.InsuranceCompanyID = @InsuranceCompanyID))
    --ORDER BY AuditTypeCD, Insurance, SysLastUpdatedDate, SysLastUserID

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('105|%s|@tmpAudit', 16, 1, @ProcName)
        RETURN
    END


    IF (SELECT COUNT(*) FROM @tmpAudit) = 0
    BEGIN
      INSERT INTO @tmpAudit
      SELECT (SELECT Name From @tmpAuditCode WHERE Code = @AuditTypeCD), NULL, NULL, NULL, NULL, NULL, NULL, NULL,
             convert(varchar, @frmDate, 101) + ' - ' + convert(varchar, @toDate, 101) ToDate
    END

    SELECT AuditType, InsuranceCompanyID, Insurance, KeyDescription, KeyID, LogComment, UserName, SysLastUpdatedDate, ToDate
    FROM @tmpAudit
    ORDER BY AuditType, Insurance, SysLastUpdatedDate, UserName
  END


  IF @@ERROR <> 0
  BEGIN
      -- SQL Server Error

      RAISERROR('99|%s', 16, 1, @ProcName)
      RETURN
  END

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptAuditLogGetDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptAuditLogGetDetail TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/