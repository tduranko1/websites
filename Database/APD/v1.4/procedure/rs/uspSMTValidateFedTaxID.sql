-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTValidateFedTaxID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTValidateFedTaxID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTValidateFedTaxID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Checks for duplicate Fed Tax IDs
*
* PARAMETERS:  
* (I) @FedTaxId             Federal Tax ID
* (I) @ShopId               Shop ID
*
* RESULT SET:
* ValidFedTaxId             this is 1 if Fed Tax ID is valid, else 0
*
* BusinessInfoID and other pertinent data regarding businesses with duplicate FedTaxIDs
* 
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTValidateFedTaxID
    @FedTaxId       udt_fed_tax_id,
    @ShopId         udt_std_id_big=NULL
AS
BEGIN

  -- Declare internal variables

  DECLARE @ValidFed as int
  DECLARE @ProcName as varchar(30)
    
  SET @ValidFed = 0
  SET @ProcName = 'uspSMTValidateFedTaxID'
  
  
  DECLARE @tmpBusiness TABLE (BusinessInfoID    bigint        NOT NULL,
                              Name              varchar(50)       NULL,
                              Address1          varchar(50)       NULL,
                              Address2          varchar(50)       NULL,
                              AddressCity       varchar(30)       NULL,
                              AddressState      char(2)           NULL)
                                          
  
  IF (@ShopID IS NOT NULL)
  BEGIN
    --IF (NOT EXISTS (SELECT FedTaxId FROM utb_shop WHERE FedTaxId = @FedTaxId AND ShopID <> @ShopId AND EnabledFlag = 1))  
    --BEGIN               
      --SET @ValidFed = 1
      
      INSERT INTO  @tmpBusiness (BusinessInfoID, Name, Address1, Address2, AddressCity, AddressState)
        SELECT ShopID, Name, Address1, Address2, AddressCity, AddressState
        FROM dbo.utb_shop 
        WHERE FedTaxId = @FedTaxId 
          AND ShopID <> @ShopId 
          AND EnabledFlag = 1  
          
      IF ((SELECT COUNT(*) FROM @tmpBusiness) > 0) SET @ValidFed = 1 
      
      
    --END
  END
  ELSE
  BEGIN
    --IF (NOT EXISTS (SELECT FedTaxId FROM utb_shop WHERE FedTaxId = @FedTaxId AND EnabledFlag = 1))  
    --BEGIN
      --SET @ValidFed = 1
      
      INSERT INTO  @tmpBusiness (BusinessInfoID, Name, Address1, Address2, AddressCity, AddressState)
        SELECT ShopID, Name, Address1, Address2, AddressCity, AddressState
        FROM dbo.utb_shop 
        WHERE FedTaxId = @FedTaxId 
          AND EnabledFlag = 1  
          
      IF ((SELECT COUNT(*) FROM @tmpBusiness) > 0) SET @ValidFed = 1 
      
    --END
  END
    
  IF @@ERROR <> 0
  BEGIN
    -- SQL Server Error
    RAISERROR  ('105|%s|@tmpBusiness', 16, 1, @ProcName)
    RETURN
  END  
      
  
    
  --SELECT @ValidFed AS ValidFedTaxID
  
  SELECT  1           AS Tag,
          NULL        AS Parent,
          
          -- Root
          @ValidFed   AS [Root!1!ValidFed],
          
          -- Business
          NULL        AS [Business!2!BusinessInfoID],
          NULL        AS [Business!2!Name],
          NULL        AS [Business!2!Address1],
          NULL        AS [Business!2!Address2],
          NULL        AS [Business!2!AddressCity],
          NULL        AS [Business!2!AddressState]
          
  UNION ALL
  
  -- Business Level
  
  SELECT  2,
          1,
          
          -- Root
          NULL,
          
          -- Business
          BusinessInfoID,
          Name,
          Address1,
          Address2,
          AddressCity,
          AddressState
          
  FROM @tmpBusiness
  ORDER BY Tag, [Business!2!Name]  
  --    FOR XML EXPLICIT      -- (Commented for Client-side processing)

  IF @@ERROR <> 0
  BEGIN
    -- SQL Server Error
    RAISERROR  ('99|%s', 16, 1, @ProcName)
    RETURN
  END        
          
          
          
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTValidateFedTaxID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTValidateFedTaxID TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/