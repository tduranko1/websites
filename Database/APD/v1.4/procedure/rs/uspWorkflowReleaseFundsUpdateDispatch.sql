-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowReleaseFundsUpdateDispatch' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowReleaseFundsUpdateDispatch 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowReleaseFundsUpdateDispatch
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Updates the dispatch information for release funds
*
* PARAMETERS:  
* (I) @UpdateID             Billing or Payment ID to update
* (I) @UpdateType           'B' - Billing, 'P' - Payment
* (I) @BillDate             The bill date
* (I) @DispatchNumber       The dispatch number
* (I) @UserID               User performing the update
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowReleaseFundsUpdateDispatch
    @DispatchNumber     udt_std_desc_short,
    @DispatchNumberNew  udt_std_desc_short,
    @ReleaseFundAmount  decimal(9, 2),
    @Description        udt_std_desc_mid,
    @UserID             udt_std_id
AS
BEGIN
    
    -- Declare internal variables

    DECLARE @now                            datetime
    DECLARE @error                          int
    DECLARE @rowcount                       int

    DECLARE @ProcName                       varchar(30)       -- Used for raise error stmts 
    
    SET NOCOUNT ON

    SET @ProcName = 'uspWorkflowReleaseFundsUpdateDispatch'
    
    IF LEN(LTRIM(RTRIM(@DispatchNumber))) = 0 SET @DispatchNumber = NULL
    IF LEN(LTRIM(RTRIM(@DispatchNumberNew))) = 0 SET @DispatchNumberNew = NULL
    IF LEN(LTRIM(RTRIM(@ReleaseFundAmount))) = 0 SET @ReleaseFundAmount = NULL
    IF LEN(LTRIM(RTRIM(@Description))) = 0 SET @Description = NULL


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Check to make sure a valid UpdateId was passed in    
    IF  (@DispatchNumber IS NULL) OR
        (NOT EXISTS(SELECT DispatchNumber FROM dbo.utb_invoice WHERE DispatchNumber = @DispatchNumber))
    BEGIN
        -- Invalid Dispatch Number
        RAISERROR('101|%s|@DispatchNumber|%u', 16, 1, @ProcName, @DispatchNumber)
        RETURN
    END
    
            
    -- Validate @UserID    
    IF NOT EXISTS (SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User ID
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    

    -- Begin Update
    BEGIN TRANSACTION BillingUpdateDispatchTran1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_invoice_dispatch (
       Amount,
       Description,
       DispatchNumber,
       TransactionCD,
       TransactionDate,
       SysLastUserID,
       SysLastUpdatedDate
    ) VALUES (
       @ReleaseFundAmount,
       @Description,
       @DispatchNumber,
       'R',
       @now,
       @UserID,
       @now
    )
    
    SET @rowcount = @@ROWCOUNT

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_invoice_dispatch', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    IF @DispatchNumber <> @DispatchNumberNew
    BEGIN
       -- Update the Invoice record    
       UPDATE  dbo.utb_invoice
         SET   DispatchNumber = @DispatchNumberNew,
               SysLastUserID = @UserID,
               SysLastUpdatedDate = @now
         WHERE DispatchNumber = @DispatchNumber

       IF @@ERROR <> 0
       BEGIN
           -- SQL Server Error

           RAISERROR  ('105|%s|utb_invoice', 16, 1, @ProcName)
           ROLLBACK TRANSACTION
           RETURN
       END


       INSERT INTO dbo.utb_invoice_dispatch (
          Amount,
          Description,
          DispatchNumber,
          DisptachNumberNew,
          TransactionCD,
          TransactionDate,
          SysLastUserID,
          SysLastUpdatedDate
       ) VALUES (
          0,
          'Dispatch Number was updated from ' + @DispatchNumber + ' to ' + @DispatchNumberNew,
          @DispatchNumber,
          @DispatchNumberNew,
          'A',
          @now,
          @UserID,
          @now
       )


       IF @@ERROR <> 0
       BEGIN
           -- SQL Server Error

           RAISERROR  ('104|%s|utb_invoice_dispatch', 16, 1, @ProcName)
           ROLLBACK TRANSACTION
           RETURN
       END
    END


    COMMIT TRANSACTION BillingUpdateDispatchTran1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    

    RETURN @rowcount 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowReleaseFundsUpdateDispatch' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowReleaseFundsUpdateDispatch TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/