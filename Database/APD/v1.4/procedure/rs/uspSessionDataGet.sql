-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDataGet' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSessionDataGet 
END

GO
/****** Object:  StoredProcedure [dbo].[uspSessionDataGet]    Script Date: 08/08/2014 12:12:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSessionDataGet
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Retrieves session data for the specified session key
*
* PARAMETERS:  
* (I) @SessionID                The session key
* (I) @SessionVariable          The session's variable to retrieve
*
* UPDATES:						1.1 - 20Mar2017 - Sergey - Modified to fix deadlock issues
*
* RESULT SET:
* SessionID                     The session key
* SessionVariable               The session variable
* SessionValue                  The session value
* ModifiedDateTime              The sessions last accessed date
*
*
* VSS
* $Workfile: uspSessionDataGet.sql $
* $Archive: /Database/APD/v1.0.0/procedure/uspSessionDataGet.sql $
* $Revision: 1 $
* $Author: Jim $
* $Date: 10/12/01 12:08p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSessionDataGet]
    @SessionID         AS udt_session_id,
    @SessionVariable   AS udt_session_variable = NULL
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    -- Declare internal variables
    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @trancount          AS INT

    SET @error = 0
	SET @trancount = @@TRANCOUNT
   
    -- Start the new transaction
	BEGIN TRANSACTION
    
	SELECT s.SessionID, sd.SessionVariable, sd.SessionValue, s.SysLastUpdatedDate
	FROM dbo.utb_session s 
	INNER JOIN dbo.utb_session_data sd ON s.SessionID = sd.SessionID
	WHERE sd.SessionID = @SessionID
	AND (SessionVariable = @SessionVariable OR @SessionVariable IS NULL)
	ORDER BY sd.SessionVariable

	-- Capture any error
	SELECT @error = @@ERROR

    IF 0 = @error
    BEGIN
        -- Update the "Last Access Date/Time"
        EXECUTE dbo.uspSessionUpd @SessionID

        -- Capture any error
        SELECT @error = @@ERROR
    END
    
    -- Final transaction success determination
    IF @@TRANCOUNT > @trancount
    BEGIN
        IF 0 = @error
            COMMIT TRANSACTION
        ELSE
            ROLLBACK TRANSACTION
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDataGet' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionDataGet TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO