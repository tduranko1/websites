BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspNugenGetGUIDByLynxID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspNugenGetGUIDByLynxID 
END



GO
/****** Object:  StoredProcedure [dbo].[uspNugenGetGUIDByLynxID]    Script Date: 03/06/2013 06:59:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspNugenGetGUIDByLynxID]
(
@LynxID varchar(10),
@VehicleNumber varchar(10)
)
AS
BEGIN
SET NOCOUNT ON
IF EXISTS(SELECT SourceApplicationPassThruData FROM utb_claim_aspect WHERE LynxID = @LynxID and SourceApplicationPassThruData Is Not Null and ClaimAspectNumber = @VehicleNumber)
		BEGIN
			SELECT SourceApplicationPassThruData
			FROM utb_claim_aspect 
			WHERE LynxID = @LynxID and SourceApplicationPassThruData IS NOT NULL and ClaimAspectNumber = @VehicleNumber		
		END
ELSE
		BEGIN
				SELECT SourceApplicationPassThruData = NULL
		END	 	
END 
SET NOCOUNT OFF
GO

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspNugenGetGUIDByLynxID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspNugenGetGUIDByLynxID TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

