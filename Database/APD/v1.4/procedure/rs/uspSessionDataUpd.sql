-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDataUpd' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSessionDataUpd 
END

GO
/****** Object:  StoredProcedure [dbo].[uspSessionDataUpd]    Script Date: 08/08/2014 12:17:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSessionDataUpd
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Updates session data for the specified session key
*
* PARAMETERS:  
* (I) @SessionID                The session key
* (I) @SessionVariable          The session's variable to update
* (I) @SessionValue             The session variable's value
*
* UPDATES:						1.1 - 20Mar2017 - Sergey - Modified to fix deadlock issues
*
* RESULT SET:
* None
*
*
* VSS
* $Workfile: uspSessionDataUpd.sql $
* $Archive: /Database/APD/v1.0.0/procedure/uspSessionDataUpd.sql $
* $Revision: 2 $
* $Author: Jim $
* $Date: 10/24/01 5:10p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSessionDataUpd]
    @SessionID         AS udt_session_id,
    @SessionVariable   AS udt_session_variable,
    @SessionValue      AS udt_session_value
AS
BEGIN
    SET NOCOUNT ON

    -- Declare internal variables
    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @trancount          AS INT
    DECLARE @update				AS BIT

    SET @error = 0
	SET @update = 0
	SET @trancount = @@TRANCOUNT
  
    
    -- Start the new transaction
	BEGIN TRANSACTION
    
	SELECT @rowcount = 1
	FROM dbo.utb_session_data 
	WHERE SessionID = @SessionID
	AND SessionVariable = @SessionVariable

	-- Capture any error
	SELECT @error = @@ERROR

    IF 0 = @error
    BEGIN
        IF 1 = @rowcount
        BEGIN
            UPDATE dbo.utb_session_data
            SET SessionValue = @SessionValue,SysLastUpdatedDate = CURRENT_TIMESTAMP , @update = 1
            WHERE SessionID = @SessionID
            AND SessionVariable = @SessionVariable
        END

        -- Capture any error
        SELECT @error = @@ERROR
    END

    IF 0 = @error
    BEGIN
        IF 1 <> @update
        BEGIN
            SELECT @rowcount = 1
            FROM dbo.utb_session 
            WHERE SessionID = @SessionID
         END
           
        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    IF 0 = @error
    BEGIN
        IF 1 <> @update AND 1 = @rowcount
        BEGIN
            INSERT INTO dbo.utb_session_data	
				(SessionID,
                SessionVariable,
                SessionValue,
                SysLastUserID)
            VALUES	
				(@SessionID,
                @SessionVariable,
                @SessionValue,
                0)  -- System
        END

        -- Capture any error
        SELECT @error = @@ERROR
    END

    -- Update the "Last Access Date/Time"
    
    IF 0 = @error
    BEGIN
        EXECUTE dbo.uspSessionUpd @SessionID
    
        -- Capture any error
        SELECT @error = @@ERROR
    END

    -- Final transaction success determination
    IF @@TRANCOUNT > @trancount
    BEGIN
        IF 0 = @error
            COMMIT TRANSACTION
        ELSE
            ROLLBACK TRANSACTION
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDataUpd' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionDataUpd TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO