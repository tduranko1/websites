-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsuranceFeeCopy' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmInsuranceFeeCopy 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdmInsuranceFeeCopy
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Creates a copy of an existing Fee
*
* PARAMETERS:  
* (I) @InsuranceCompanyID       Insurance Company ID
*
* RESULT SET:
* a XML document of Fees applicable to the Insurance Company
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdmInsuranceFeeCopy
    @ClientFeeID        udt_std_id,
    @SysLastUserID      udt_std_id,
    @ApplicationCD      udt_std_cd = 'APD'
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

    -- Declare local variables

    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 
    DECLARE @Now                AS DateTime          -- Timestamp for Procedure
    DECLARE @rowcount           AS udt_std_int
    DECLARE @NewClientFeeID     AS udt_std_id
    DECLARE @NewAdminFeeID      AS udt_std_id
    DECLARE @OldAdminFeeID      AS udt_std_id
    DECLARE @Description        AS udt_std_name
    DECLARE @LogComment         AS udt_std_desc_long
    DECLARE @InsuranceCompanyID AS udt_std_int_small
    

    SET @ProcName = 'uspAdmInsuranceFeeCopy'
    SET @Now = Current_TimeStamp


    -- Validate the input parameters
    --Verify that a valid ClientFeeID was passed in.
    IF (@ClientFeeID is Null) OR
    NOT EXISTS (SELECT ClientFeeID FROM utb_client_fee WHERE ClientFeeID = @ClientFeeID)
    BEGIN
        -- Invalid Client Fee ID 
    
        RAISERROR  ('101|%s|@ClientFeeID|%n', 16, 1, @ProcName, @ClientFeeID)
        RETURN
    END

    
     --Verify that a valid UserID was passed in.
    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT u.UserID 
                       FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                           INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
                       WHERE a.Code = @ApplicationCD
                         AND u.UserID = @SysLastUserID
                         AND @SysLastUserID <> 0
                         AND ua.AccessBeginDate IS NOT NULL
                         AND ua.AccessBeginDate <= @Now
                         AND ua.AccessEndDate IS NULL)
        BEGIN
           -- Invalid User
    
            RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
        RETURN
    END
    
    
    
    BEGIN TRANSACTION uspAdmInsuranceFeeCopyTran1
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
        
    
    INSERT INTO dbo.utb_client_fee (ClaimAspectTypeID, InsuranceCompanyID, AppliesToCD, CategoryCD, Description,
                                    DisplayOrder, EffectiveEndDate, EffectiveStartDate, EnabledFlag, FeeAmount,
                                    FeeInstructions, ItemizeFlag, InvoiceDescription, SysLastUserID, SysLastUpdatedDate)
                                    
                             SELECT ClaimAspectTypeID, InsuranceCompanyID, AppliesToCD, CategoryCD, 
                                    'COPY OF ' + ISNULL(Description, ''),
                                    (1 + (SELECT MAX(DisplayOrder) FROM dbo.utb_client_fee)),
                                    EffectiveEndDate, EffectiveStartDate, EnabledFlag, FeeAmount,
                                    FeeInstructions, ItemizeFlag, InvoiceDescription, @SysLastUserID, @Now
                               FROM dbo.utb_client_fee
                              WHERE ClientFeeID = @ClientFeeID
                              
              
    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_client_fee', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    ELSE
    BEGIN
        -- Get the new ClientFeeID

        SET @NewClientFeeID = SCOPE_IDENTITY()
    END
    
    SELECT @rowcount = @@ROWCOUNT
    
    INSERT INTO dbo.utb_client_fee_definition
    SELECT @NewClientFeeID, ServiceID, ItemizeFlag, RequiredFlag, @SysLastUserID, @Now
    FROM dbo.utb_client_fee_definition
    WHERE ClientFeeID = @ClientFeeID
    
    
                              
    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_client_fee_definition', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    SELECT @OldAdminFeeID = AdminFeeID
    FROM dbo.utb_admin_fee
    WHERE ClientFeeID = @ClientFeeID
      AND EnabledFlag = 1
    
    INSERT INTO dbo.utb_admin_fee
    SELECT @NewClientFeeID, AdminFeeCD, AppliesToCD, Amount, EnabledFlag, SplitFlag, SysLastUserID, @Now
    FROM dbo.utb_admin_fee
    WHERE AdminFeeID = @OldAdminFeeID

    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_admin_fee', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    ELSE
    BEGIN
        SET @NewAdminFeeID = SCOPE_IDENTITY()
    END

    INSERT INTO dbo.utb_admin_fee_msa
    SELECT @NewAdminFeeID, MSA, AdminFeeCD, Amount, EnabledFlag, SysLastUserID, @now
    FROM dbo.utb_admin_fee_msa
    WHERE AdminFeeID = @OldAdminFeeID
      AND EnabledFlag = 1
    
    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_admin_fee_msa', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    
    INSERT INTO dbo.utb_admin_fee_state
    SELECT @NewAdminFeeID, StateCode, AdminFeeCD, Amount, EnabledFlag, SysLastUserID, @now
    FROM dbo.utb_admin_fee_state
    WHERE AdminFeeID = @OldAdminFeeID
      AND EnabledFlag = 1
    
    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_admin_fee_msa', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    -- Begin Audit Logging ---------------------------------------------------------------------------------------
    --------------------------------------------------------------------------------------------------------------
    
    SELECT @Description = Description,
           @InsuranceCompanyID = InsuranceCompanyID
    FROM dbo.utb_client_fee 
    WHERE ClientFeeID = @ClientFeeID
    
    SET @LogComment = 'A copy of this Fee created under the name, ''COPY OF ' + @Description + '''' 
                         
                                     
    EXEC dbo.uspAuditLogInsDetail 'F', @InsuranceCompanyID, @Description, @NewClientFeeID, @LogComment, @SysLastUserID
                      
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
        ROLLBACK TRANSACTION
        RETURN
    END 
    
    -- End Audit Logging -----------------------------------------------------------------------------------------
    --------------------------------------------------------------------------------------------------------------
    
    
    COMMIT TRANSACTION uspAdmInsuranceFeeCopyTran1
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            
            -- New ClientFee Level
            NULL AS [ClientFee!2!ClientFeeID],
            NULL AS [ClientFee!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            
            -- New Involved Level
            @NewClientFeeID,
            dbo.ufnUtilityGetDateString( @Now )

    ORDER BY tag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount


END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmInsuranceFeeCopy' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmInsuranceFeeCopy TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/