-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopUpdBusinessInfoID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopUpdBusinessInfoID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopUpdBusinessInfoID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Move a Shop to a different Business Info
*
* PARAMETERS:  
* (I) @ShopID                       The ID of the Shop to be moved
*
* RESULT SET:
* BusinessInfoID                    The ID of the destination Business Info
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopUpdBusinessInfoID
(
    @Updates                            udt_std_desc_long,
    @BusinessInfoID                     udt_std_id_big,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd='APD'     
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables
    DECLARE @BusinessInfoIDOrig         udt_std_int_big
    DECLARE @error                      udt_std_int
    DECLARE @rowcount                   udt_std_int
    DECLARE @Now                        udt_std_datetime

    DECLARE @tupdated_date              udt_std_datetime 
    DECLARE @temp_updated_date          udt_std_datetime 
    DECLARE @BusinessName               udt_std_name
    

    DECLARE @ProcName                   varchar(30)       -- Used for raise error stmts 

    DECLARE @tmpShop             TABLE (ShopID            bigint          NOT NULL,
                                        Name              varchar(50)         NULL,
                                        BusinessInfoID    bigint              NULL,
                                        BusinessName      varchar(50)         NULL)
    
    
    SET @ProcName = 'uspSMTShopUpdBusinessInfoID'
    SET @Now = CURRENT_TIMESTAMP
        
    
    IF @BusinessInfoID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT ShopID FROM utb_shop WHERE ShopID = @BusinessInfoID)
        BEGIN
           -- Invalid Business Info
           RAISERROR('101|%s|@BusinessInfoID|%s', 16, 1, @ProcName, @BusinessInfoID)
           RETURN
        END
    END

    IF ((SELECT dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default)) = 0)
    BEGIN
      -- Invalid User
      RAISERROR  ('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
      RETURN
    END
        
    
    INSERT INTO @tmpShop (ShopID)  SELECT value FROM dbo.ufnUtilityParseString(@Updates, ',', 1) f
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|@tmpShop', 16, 1, @ProcName)
        RETURN
    END
    
       
    -- Ensure all ShopIDs passed in @Updates are valid
    IF EXISTS (SELECT t.ShopID
               FROM @tmpShop t LEFT JOIN dbo.utb_shop_location s ON t.ShopID = s.ShopLocationID
               WHERE s.ShopLocationID IS NULL)
    BEGIN
      -- Invalid ShopLocationID passed in @Updates string
      RAISERROR  ('101|%s|@Updates|%u', 16, 1, @ProcName, @Updates)
      RETURN
    END
    
    
    -- Get some extra info that will be useful later
    UPDATE @tmpShop
    SET Name = s.Name,
        BusinessInfoID = s.ShopID,
        BusinessName = b.Name    
    FROM @tmpShop t INNER JOIN dbo.utb_shop_location s ON t.ShopID = s.ShopLocationID
                    INNER JOIN dbo.utb_shop b ON s.ShopID = b.ShopID
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('104|%s|@tmpShop', 16, 1, @ProcName)
        RETURN
    END
    
    SET @BusinessName = (SELECT Name FROM dbo.utb_shop WHERE ShopID = @BusinessInfoID)
    
    -- Begin Update(s)

    BEGIN TRANSACTION MoveShopTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Move the Shops listed in the @Updates string to @BusinessInfoID
    UPDATE  dbo.utb_shop_location 
    SET     ShopID = @BusinessInfoID,  
            SysLastUserID = @SysLastUserID,
            SysLastUpdatedDate = @Now
    FROM @tmpShop t INNER JOIN dbo.utb_shop_location s on t.ShopID = s.ShopLocationID          
             
             
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('104|%s|utb_shop', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    

    -- If any of the Businesses that Shops were moved FROM are left with NO Shops, 'delete' those Businesses.
    UPDATE dbo.utb_shop 
    SET    EnabledFlag = 0,
           SysLastUserID = @SysLastUserID,
           SysLastUpdatedDate = @Now
    FROM @tmpShop t INNER JOIN dbo.utb_shop s ON t.BusinessInfoID = s.ShopID
    WHERE NOT EXISTS (SELECT ShopLocationID FROM dbo.utb_shop_location WHERE ShopID = t.BusinessInfoID
                                                                         AND EnabledFlag = 1)
                                                                         
                                                         
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
  
    -- Check error value
    IF @error <> 0
    BEGIN
     -- SQL Server Error
      RAISERROR('104|%s|utb_shop', 16, 1, @ProcName)
      ROLLBACK TRANSACTION
      RETURN
    END 
    
    -- Begin Audit Log code  -----------------------------------------------------------------------------------
    
    INSERT INTO dbo.utb_audit_log (AuditTypeCD,
                                   KeyDescription,
                                   KeyID,
                                   LogComment,
                                   SysLastUserID,
                                   SysLastUpdatedDate)
                            SELECT 'P',
                                   Name,
                                   ShopID,
                                   ' Shop moved from Business, ' + BusinessName + ', to Business, ' + @BusinessName,
                                   @SysLastUserID,
                                   @now
                            FROM @tmpShop
    

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    IF @error <> 0
    BEGIN
       -- Insertion failure
    
        ROLLBACK TRANSACTION 
        RETURN
    END
    
    
   
    
    
    -- audit any Businesses deleted by the move
    
    INSERT INTO dbo.utb_audit_log (AuditTypeCD,
                                   KeyDescription,
                                   KeyID,
                                   LogComment,
                                   SysLastUserID,
                                   SysLastUpdatedDate)
                            SELECT 'P',
                                   Name,
                                   ShopID,
                                   ' Business deleted  (All associated shops were moved to other Businesses)',
                                   @SysLastUserID,
                                   @now
                            FROM dbo.utb_shop
                            WHERE EnabledFlag = 0
                              AND SysLastUpdatedDate = @now
    

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    IF @error <> 0
    BEGIN
       -- Insertion failure
    
        ROLLBACK TRANSACTION 
        RETURN
    END
    
        
    
    -- End of Audit Log code  ----------------------------------------------------------------------------------
    

    COMMIT TRANSACTION MoveShopTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @BusinessInfoID AS BusinessInfoID

    RETURN @rowcount
END


GO

-- Permissions
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopUpdBusinessInfoID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopUpdBusinessInfoID TO 
        ugr_lynxapd
    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
