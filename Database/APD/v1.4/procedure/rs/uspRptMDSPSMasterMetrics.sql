-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMDSPSMasterMetrics' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptMDSPSMasterMetrics 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptMDSPSMasterMetrics
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This Report procedure will return the datawarehouse metrics for CSR PS channel.
*
* PARAMETERS:  
* (I) @ReportStart   The report start date. 
* (I) @ReportEnd     The report end date
* (I) @IndustryGoal  Industry goal value
* (I) @LYNXGoal      LYNX goal value
*
* RESULT SET:
* None.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptMDSPSMasterMetrics
(
    @ReportStart        datetime = NULL,
    @ReportEnd          datetime = NULL,
    @IndustryGoal       decimal(9, 2) = 9.5,
    @LYNXGoal           decimal(9, 2) = 9.0
)
AS
BEGIN
   SET NOCOUNT ON
   
   DECLARE @now as datetime

   SET @now = CURRENT_TIMESTAMP

   IF @ReportStart IS NULL OR @ReportEnd IS NULL
   BEGIN
      IF DAY(@now) = 1
      BEGIN
         SET @ReportStart = convert(varchar, DATEADD(month, -1, @now), 101)
         SET @ReportEnd = DATEADD(second, -1, DATEADD(month, 1, @ReportStart))
      END
      ELSE
      BEGIN
         SET @ReportStart = convert(varchar, month(@now)) + '/1/' + convert(varchar, year(@now))
         SET @ReportEnd = convert(varchar, @now, 101) + ' 23:59:59'
      END
   END

   DECLARE @tmpRawData TABLE (
      FactID                           bigint,
      LynxID                           bigint,
      ClaimAspectID_fc                 bigint,
      DriveableFlag                    varchar(1),
      ServiceChannelCD                 varchar(2),
      DispositionCD                    varchar(4),
      ClaimStatus                      varchar(15),
      InsuranceCompany                 varchar(100),
      AnalystName                      varchar(100),
      OwnerName                        varchar(100),
      DateAssignSent                   datetime,
      DateClosed                       datetime,
      DateEstimate                     datetime,
      DateNew                          datetime,
      DateRepairComplete               datetime,
      DateRepairStarted                datetime,
      CycleTimeAssignToEstBus          decimal(9, 2),
      CycleTimeEstToCloseBus           decimal(9, 2),
      CycleTimeNewToCloseBus           decimal(9, 2),
      CycleTimeRepairStartToEndBus     decimal(9, 2),
      OriginalGrossAmount              decimal(9, 2) NULL,
      FBEGrossAmount                   decimal(9, 2) NULL,
      SupplementCount                  int      NULL,
      -- Fact Estimate
      BodyRepairHours                  decimal(9, 2) NULL,
      BodyReplaceHours                 decimal(9, 2) NULL,
      LaborTotalHours                  decimal(9, 2) NULL,
      PartsNewAmount                   money NULL,
      PartsLKQAmount                   money NULL,
      PartsAFMKAmount                  money NULL,
      PartsTotalAmount                 money NULL,
      
      -- Calculated Data
      CycleTimeNewToAssignBus          decimal(9, 2) NULL,
      CycleTimeNewToEstBus             decimal(9, 2) NULL,
      CycleTimeEstToRepairStartBus     decimal(9, 2) NULL,
      CycleTimeRepairEndToCloseBus     decimal(9, 2) NULL,
      Original2FinalGrossAmountDelta   decimal(9, 2) NULL,
      EstimateSavings                  decimal(9, 2) NULL
   )

   DECLARE @tmpUsers TABLE (
      UserName    varchar(100),
      RoleName    varchar(10)
   )

   DECLARE @tmpInsuranceCompany TABLE (
      InsuranceCompanyName varchar(100)
   )

   DECLARE @tmpReportData TABLE (
      LineLevel             int,
      UserName              varchar(75),
      InsuranceCompany      varchar(75),
      ServiceChannelCD      varchar(2),
      DriveableFlag         varchar(1),
      AvgClaimRec2Assg      decimal(9, 5) NULL,
      AvgAssg2Est           decimal(9, 5) NULL,
      PerClmAssg2Est1to5    decimal(9, 5) NULL,
      PerClmAssg2Est6to10   decimal(9, 5) NULL,
      PerClmAssg2Est11plus  decimal(9, 5) NULL,
      AvgNew2Est            decimal(9, 5) NULL,
      AvgEst2RepairSt       decimal(9, 5) NULL,
      AvgRepairSt2End       decimal(9, 5) NULL,
      AvgRepairEnd2Close    decimal(9, 5) NULL,
      AvgEst2Close          decimal(9, 5) NULL,
      AvgNew2Close          decimal(9, 5) NULL,
      VehicleCount          int NULL,
      SupplementCount       int NULL,
      ClaimsWithSupplement  int NULL,
      PerClamsWithSupp      decimal(9, 5) NULL,
      AvgGrossOrigEst       decimal(9, 3) NULL,
      AvgGrossFBEEst        decimal(9, 3) NULL,
      AvgGrossSupp          decimal(9, 3) NULL,
      IndustryGoal          decimal(9, 2) NULL,
      LYNXGoal              decimal(9, 2) NULL,
      PerGrossSavings       decimal(9, 3) NULL,
      DeltaActual2LYNX      decimal(9, 3) NULL,
      PerRpr2RepLaborHr     decimal(9, 5) NULL,
      PerRpr2TotLaborHr     decimal(9, 5) NULL,
      PerRep2RepLaborHr     decimal(9, 5) NULL,
      PerOEM2TotLaborHr     decimal(9, 5) NULL,
      PerLKQ2TotLaborHr     decimal(9, 5) NULL,
      PerAFMK2TotLaborHr    decimal(9, 5) NULL
   )

   INSERT INTO @tmpRawData
   SELECT fc.FactID,
          fc.LynxID,
          fc.ClaimAspectID,
          case 
            when cv.DriveableFlag = 1 then 'Y'
            when cv.DriveableFlag = 0 then 'N'
            else ''
          end,
          sc.ServiceChannelCD,
          dt.DispositionTypeCD,
          s.Name,
          i.Name,
          ua.NameFirst + ' ' + ua.NameLast,
          uo.NameFirst + ' ' + uo.NameLast,
          dta.DateValue,
          dtc.DateValue,
          dte.DateValue,
          dtn.DateValue,
          dtrc.DateValue,
          dtrs.DateValue,
          CASE
            WHEN sc.ServiceChannelCD in ('DA', 'DR') THEN CycleTimeAssignToEstHrs
            ELSE CycleTimeAssignToEstBusDay
          END,
          CASE
            WHEN sc.ServiceChannelCD in ('DA', 'DR') THEN CycleTimeEstToCloseHrs
            ELSE CycleTimeEstToCloseBusDay
          END,
          CASE
            WHEN sc.ServiceChannelCD in ('DA', 'DR') THEN CycleTimeNewToCloseHrs
            ELSE CycleTimeNewToCloseBusDay
          END,
          CASE
            WHEN sc.ServiceChannelCD in ('DA', 'DR') THEN CycleTimeRepairStartToEndHrs
            ELSE CycleTimeRepairStartToEndBusDay
          END,
          OriginalEstimateGrossAmt - isNull(OriginalEstimateBettermentAmt, 0) - isNull(OriginalEstimateOtherAdjustmentAmt, 0),
          CASE
            WHEN sc.ServiceChannelCD = 'PS' THEN FinalSupplementGrossAmt - isNull(FinalSupplementBettermentAmt, 0) - isNull(FinalSupplementOtherAdjustmentAmt, 0)
            ELSE FinalAuditedSuppGrossAmt - isNull(FinalAuditedSuppBettermentAmt, 0) - isNull(FinalSupplementOtherAdjustmentAmt, 0)
          END,
          MaxEstSuppSequenceNumber, 
          NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
          NULL, NULL, NULL
   FROM dbo.utb_dtwh_fact_claim fc
   LEFT JOIN dbo.utb_dtwh_dim_disposition_type dt ON fc.DispositionTypeID =  dt.DispositionTypeID
   LEFT JOIN dbo.utb_dtwh_dim_time dta ON fc.TimeIDAssignSent = dta.TimeID
   LEFT JOIN dbo.utb_dtwh_dim_time dtc ON fc.TimeIDClosed = dtc.TimeID
   LEFT JOIN dbo.utb_dtwh_dim_time dte ON fc.TimeIDEstimate = dte.TimeID
   LEFT JOIN dbo.utb_dtwh_dim_time dtn ON fc.TimeIDNew = dtn.TimeID
   LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON fc.TimeIDRepairComplete = dtrc.TimeID
   LEFT JOIN dbo.utb_dtwh_dim_time dtrs ON fc.TimeIDRepairStarted = dtrs.TimeID
   LEFT JOIN dbo.utb_dtwh_dim_service_channel sc ON fc.ServiceChannelID = sc.ServiceChannelID
   LEFT JOIN dbo.utb_claim_aspect_status cas ON fc.ClaimAspectID = cas.ClaimAspectID
   LEFT JOIN dbo.utb_status s ON cas.StatusID = s.StatusID
   LEFT JOIN dbo.utb_dtwh_dim_customer dc ON fc.CustomerID = dc.CustomerID
   LEFT JOIN dbo.utb_insurance i ON dc.InsuranceCompanyID = i.InsuranceCompanyID
   LEFT JOIN dbo.utb_claim_aspect ca ON fc.ClaimAspectID = ca.ClaimAspectID
   LEFT JOIN dbo.utb_user ua ON ca.AnalystUserID = ua.UserID
   LEFT JOIN dbo.utb_user uo ON ca.OwnerUserID = uo.UserID
   LEFT JOIN dbo.utb_claim_vehicle cv ON fc.ClaimAspectID = cv.ClaimAspectID
   WHERE dtc.DateValue between @ReportStart and @ReportEnd
     AND dt.DispositionTypeCD in ('CO', 'RC')
     AND cas.ServiceChannelCD = sc.ServiceChannelCD
     AND sc.ServiceChannelCD = 'PS'
     AND cas.StatusTypeCD = 'SC'
     AND s.Name not in ('Cancelled', 'Voided')
     AND dc.InsuranceCompanyID NOT IN (145, 209)
     AND i.DemoFlag = 0
     
   --Update the Repair Start to End Business days for those that are missing

   --UPDATE @tmpRawData
   --SET CycleTimeRepairStartToEndBusDay = CASE
   --                                        WHEN dbo.ufnUtilityBusinessDaysDiff(DateRepairStarted, DateRepairComplete) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(DateRepairStarted, DateRepairComplete)
   --                                        ELSE NULL
   --                                      END
   --WHERE CycleTimeRepairStartToEndBusDay IS NULL
   --  AND ServiceChannelCD = 'PS'

   UPDATE @tmpRawData
   SET CycleTimeNewToAssignBus          = CASE
                                            WHEN ServiceChannelCD in ('DA', 'DR') THEN dbo.ufnUtilityBusinessHoursDiff(DateNew, DateAssignSent)
                                            WHEN dbo.ufnUtilityBusinessDaysDiff(DateNew, DateAssignSent) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(DateNew, DateAssignSent)
                                            ELSE NULL
                                          END,
       CycleTimeNewToEstBus             = CASE
                                            WHEN ServiceChannelCD in ('DA', 'DR') THEN dbo.ufnUtilityBusinessHoursDiff(DateNew, DateEstimate)
                                            WHEN dbo.ufnUtilityBusinessDaysDiff(DateNew, DateEstimate) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(DateNew, DateEstimate)
                                            ELSE NULL
                                          END,
       CycleTimeEstToRepairStartBus     = CASE
                                            WHEN ServiceChannelCD in ('DA', 'DR') THEN dbo.ufnUtilityBusinessHoursDiff(DateEstimate, DateRepairStarted)
                                            WHEN dbo.ufnUtilityBusinessDaysDiff(DateEstimate, DateRepairStarted) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(DateEstimate, DateRepairStarted)
                                            ELSE NULL
                                          END,
       CycleTimeRepairEndToCloseBus     = CASE
                                            WHEN ServiceChannelCD in ('DA', 'DR') THEN dbo.ufnUtilityBusinessHoursDiff(DateRepairComplete, DateClosed)
                                            WHEN dbo.ufnUtilityBusinessDaysDiff(DateRepairComplete, DateClosed) >= 0 THEN dbo.ufnUtilityBusinessDaysDiff(DateRepairComplete, DateClosed)
                                            ELSE NULL
                                          END,
      Original2FinalGrossAmountDelta = OriginalGrossAmount - FBEGrossAmount,
      EstimateSavings = CASE
                           WHEN ServiceChannelCD in ('DA', 'DR') AND FBEGrossAmount > OriginalGrossAmount THEN 0
                           WHEN ServiceChannelCD in ('DA', 'DR') AND OriginalGrossAmount > FBEGrossAmount THEN OriginalGrossAmount - FBEGrossAmount
                           ELSE FBEGrossAmount - OriginalGrossAmount
                        END


   -- Get the Fact Estimate

   UPDATE @tmpRawData
   SET BodyRepairHours = fe.BodyRepairHrs,
       BodyReplaceHours = fe.BodyReplaceHrs,
       LaborTotalHours = fe.LaborTotalHours,
       PartsNewAmount = fe.PartsNewAmount,
       PartsLKQAmount = fe.PartsLKQAmount,
       PartsAFMKAmount = fe.PartsAFMKAmount,
       PartsTotalAmount = fe.PartsTotalAmount
   FROM dbo.utb_dtwh_fact_estimate fe
   WHERE fe.ClaimAspectID = ClaimAspectID_fc
     AND EstSuppSequenceNumber = (SELECT Max(EstSuppSequenceNumber)
                                    FROM dbo.utb_dtwh_fact_estimate fe1
                                    WHERE fe1.ClaimAspectID = fe.ClaimAspectID)


   INSERT INTO @tmpUsers
   SELECT DISTINCT AnalystName, 'MDS'
   FROM @tmpRawData
   WHERE AnalystName IS NOT NULL
   ORDER BY 1

   INSERT INTO @tmpUsers
   SELECT DISTINCT OwnerName, 'CR'
   FROM @tmpRawData
   WHERE OwnerName IS NOT NULL
   ORDER BY 1

   INSERT INTO @tmpInsuranceCompany
   SELECT DISTINCT InsuranceCompany
   FROM @tmpRawData
   ORDER BY 1



   INSERT INTO @tmpReportData
   SELECT 1,
          rd.AnalystName,
          InsuranceCompany,
          ServiceChannelCD,
          DriveableFlag,
          (SELECT AVG(CycleTimeNewToAssignBus)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.CycleTimeNewToAssignBus IS NOT NULL),
          (SELECT AVG(CycleTimeAssignToEstBus)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.CycleTimeAssignToEstBus IS NOT NULL),
          CASE
            WHEN count(LynxID) > 0 THEN 
                ((SELECT convert(decimal(9,5), count(rd1.LynxID))
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND CycleTimeAssignToEstBus between 0 and 5) / count(LynxID)) * 100.00
            ELSE 0
          END,
          CASE
            WHEN count(LynxID) > 0 THEN 
                ((SELECT convert(decimal(9,5), count(rd1.LynxID))
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND CycleTimeAssignToEstBus between 6 and 10) / count(LynxID)) * 100.00
            ELSE 0
          END,
          CASE
            WHEN count(LynxID) > 0 THEN 
                ((SELECT convert(decimal(9,5), count(rd1.LynxID))
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND CycleTimeAssignToEstBus > 10) / count(LynxID)) * 100.00
            ELSE 0
          END,
          (SELECT AVG(CycleTimeNewToEstBus)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.CycleTimeNewToEstBus IS NOT NULL),
          (SELECT AVG(CycleTimeEstToRepairStartBus)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.CycleTimeEstToRepairStartBus IS NOT NULL) as 'Avg Est to Repair Start Bus Days/Hours',
          (SELECT AVG(convert(decimal(9, 2), dbo.ufnUtilityBusinessDaysDiff(DateRepairStarted, DateRepairComplete)))
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.DateRepairStarted IS NOT NULL
              AND rd1.DateRepairComplete IS NOT NULL) as 'Avg. Repair Start to End Bus Days',
          (SELECT AVG(CycleTimeRepairEndToCloseBus)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.CycleTimeRepairEndToCloseBus IS NOT NULL) as 'Avg. Repair End to Close Bus Days/Hours',
          (SELECT AVG(CycleTimeEstToCloseBus)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.CycleTimeEstToCloseBus IS NOT NULL) as 'Avg Est to Close Bus Days/Hours',
          (SELECT AVG(CycleTimeNewToCloseBus)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.CycleTimeNewToCloseBus IS NOT NULL) as 'Avg New to Close Bus Days/Hours',
          COUNT(LynxID) as 'Number of Vehicles',
          SUM(SupplementCount) as 'Number of Supplements',
          (SELECT count(DISTINCT rd1.LynxID)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.SupplementCount > 0) as 'Number of Claims with Supplements',
          CASE
            WHEN COUNT(LynxID) > 0 THEN
                ((SELECT convert(decimal(9, 2), count(DISTINCT rd1.LynxID))
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND rd1.SupplementCount > 0) / Count(LynxID)) * 100.00
            ELSE 0
          END  as '% of Claims with Supplements',
          (SELECT AVG(OriginalGrossAmount)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.OriginalGrossAmount IS NOT NULL) as 'Avg Gross Original Est.',
          (SELECT AVG(FBEGrossAmount)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.FBEGrossAmount IS NOT NULL) as 'Avg Gross FBE Est.',
          (SELECT AVG(Original2FinalGrossAmountDelta)
            FROM @tmpRawData rd1 
            WHERE rd1.InsuranceCompany = rd.InsuranceCompany
              AND rd1.AnalystName = rd.AnalystName
              AND rd1.ServiceChannelCD = rd.ServiceChannelCD
              AND rd1.DriveableFlag = rd.DriveableFlag
              AND rd1.DispositionCD <> 'CO'
              AND rd1.Original2FinalGrossAmountDelta IS NOT NULL) as 'Avg Gross Supplement Amount',
          @IndustryGoal as 'Industry Goal',
          @LYNXGoal as 'LYNX Goal',
          (SELECT AVG(CASE
                          WHEN rd1.OriginalGrossAmount > 0 THEN (rd1.EstimateSavings / rd1.OriginalGrossAmount) * 100.00
                          ELSE 0
                      END)
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND rd1.OriginalGrossAmount IS NOT NULL
                    AND rd1.EstimateSavings IS NOT NULL) as '% Saving of Gross Est Amount',
          (SELECT AVG(CASE
                          WHEN rd1.OriginalGrossAmount > 0 THEN @LYNXGoal - ((rd1.EstimateSavings / rd1.OriginalGrossAmount) * 100.00)
                          ELSE 0
                      END)
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND rd1.OriginalGrossAmount IS NOT NULL
                    AND rd1.EstimateSavings IS NOT NULL) as 'Delta for Actual to LYNX Goal',
          (SELECT AVG(CASE
                          WHEN rd1.BodyReplaceHours > 0 THEN (rd1.BodyRepairHours / rd1.BodyReplaceHours) * 100.00
                          ELSE 0
                      END)
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND rd1.BodyRepairHours IS NOT NULL
                    AND rd1.BodyReplaceHours IS NOT NULL) as '% Repair to Replace Labor Hours',
          (SELECT AVG(CASE
                          WHEN rd1.LaborTotalHours > 0 THEN (rd1.BodyRepairHours / rd1.LaborTotalHours) * 100.00
                          ELSE 0
                      END)
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND rd1.BodyRepairHours IS NOT NULL
                    AND rd1.LaborTotalHours IS NOT NULL) as '% Repair to Total Labor Hours',
          (SELECT AVG(CASE
                          WHEN rd1.LaborTotalHours > 0 THEN (rd1.BodyReplaceHours / rd1.LaborTotalHours) * 100.00
                          ELSE 0
                      END)
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND rd1.BodyReplaceHours IS NOT NULL
                    AND rd1.LaborTotalHours IS NOT NULL) as '% Replace to Total Labor Hours',
          (SELECT AVG(CASE
                          WHEN rd1.PartsTotalAmount > 0 THEN (rd1.PartsNewAmount / rd1.PartsTotalAmount) * 100.00
                          ELSE 0
                      END)
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND rd1.PartsTotalAmount IS NOT NULL
                    AND rd1.PartsNewAmount IS NOT NULL) as '% OEM to Total Parts $',
          (SELECT AVG(CASE
                          WHEN rd1.PartsTotalAmount > 0 THEN (rd1.PartsLKQAmount / rd1.PartsTotalAmount) * 100.00
                          ELSE 0
                      END)
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND rd1.PartsTotalAmount IS NOT NULL
                    AND rd1.PartsLKQAmount IS NOT NULL) as '% LKQ to Total Parts $',
          (SELECT AVG(CASE
                          WHEN rd1.PartsTotalAmount > 0 THEN (rd1.PartsAFMKAmount / rd1.PartsTotalAmount) * 100.00
                          ELSE 0
                      END)
                  FROM @tmpRawData rd1 
                  WHERE rd1.InsuranceCompany = rd.InsuranceCompany
                    AND rd1.AnalystName = rd.AnalystName
                    AND rd1.ServiceChannelCD = rd.ServiceChannelCD
                    AND rd1.DriveableFlag = rd.DriveableFlag
                    AND rd1.PartsTotalAmount IS NOT NULL
                    AND rd1.PartsAFMKAmount IS NOT NULL) as '% AFMK to Total Parts $'
   FROM @tmpUsers u
   LEFT JOIN @tmpRawData rd ON u.UserName = rd.AnalystName
   WHERE rd.AnalystName IS NOT NULL
   GROUP BY InsuranceCompany, rd.AnalystName, ServiceChannelCD, DriveableFlag


   INSERT INTO @tmpReportData
   SElECT   2,
            UserName + ' Total',
            InsuranceCompany,
            ServiceCHannelCD,
            NULL,
            AVG(AvgClaimRec2Assg),
            AVG(AvgAssg2Est),
            AVG(PerClmAssg2Est1to5),
            AVG(PerClmAssg2Est6to10),
            AVG(PerClmAssg2Est11plus),
            AVG(AvgNew2Est),
            AVG(AvgEst2RepairSt),
            AVG(AvgRepairSt2End),
            AVG(AvgRepairEnd2Close),
            AVG(AvgEst2Close),
            AVG(AvgNew2Close),
            SUM(VehicleCount),
            SUM(SupplementCount),
            SUM(ClaimsWithSupplement),
            AVG(PerClamsWithSupp),
            AVG(AvgGrossOrigEst),
            AVG(AvgGrossFBEEst),
            AVG(AvgGrossSupp),
            AVG(IndustryGoal),
            AVG(LYNXGoal),
            AVG(PerGrossSavings),
            AVG(DeltaActual2LYNX),
            AVG(PerRpr2RepLaborHr),
            AVG(PerRpr2TotLaborHr),
            AVG(PerRep2RepLaborHr),
            AVG(PerOEM2TotLaborHr),
            AVG(PerLKQ2TotLaborHr),
            AVG(PerAFMK2TotLaborHr)
   from @tmpReportData
   where LineLevel = 1
   group by ServiceChannelCD, InsuranceCompany, UserName

   INSERT INTO @tmpReportData
   SElECT   3,
            'ZZ_' + InsuranceCompany + ' Company Total',
            InsuranceCompany,
            ServiceCHannelCD,
            DriveableFlag,
            AVG(AvgClaimRec2Assg),
            AVG(AvgAssg2Est),
            AVG(PerClmAssg2Est1to5),
            AVG(PerClmAssg2Est6to10),
            AVG(PerClmAssg2Est11plus),
            AVG(AvgNew2Est),
            AVG(AvgEst2RepairSt),
            AVG(AvgRepairSt2End),
            AVG(AvgRepairEnd2Close),
            AVG(AvgEst2Close),
            AVG(AvgNew2Close),
            SUM(VehicleCount),
            SUM(SupplementCount),
            SUM(ClaimsWithSupplement),
            AVG(PerClamsWithSupp),
            AVG(AvgGrossOrigEst),
            AVG(AvgGrossFBEEst),
            AVG(AvgGrossSupp),
            AVG(IndustryGoal),
            AVG(LYNXGoal),
            AVG(PerGrossSavings),
            AVG(DeltaActual2LYNX),
            AVG(PerRpr2RepLaborHr),
            AVG(PerRpr2TotLaborHr),
            AVG(PerRep2RepLaborHr),
            AVG(PerOEM2TotLaborHr),
            AVG(PerLKQ2TotLaborHr),
            AVG(PerAFMK2TotLaborHr)
   from @tmpReportData
   where LineLevel = 1
   group by ServiceChannelCD, InsuranceCompany, DriveableFlag

   INSERT INTO @tmpReportData
   SElECT   4,
            'ZZ_' + InsuranceCompany + ' Company Total',
            InsuranceCompany,
            ServiceCHannelCD,
            NULL,
            AVG(AvgClaimRec2Assg),
            AVG(AvgAssg2Est),
            AVG(PerClmAssg2Est1to5),
            AVG(PerClmAssg2Est6to10),
            AVG(PerClmAssg2Est11plus),
            AVG(AvgNew2Est),
            AVG(AvgEst2RepairSt),
            AVG(AvgRepairSt2End),
            AVG(AvgRepairEnd2Close),
            AVG(AvgEst2Close),
            AVG(AvgNew2Close),
            SUM(VehicleCount),
            SUM(SupplementCount),
            SUM(ClaimsWithSupplement),
            AVG(PerClamsWithSupp),
            AVG(AvgGrossOrigEst),
            AVG(AvgGrossFBEEst),
            AVG(AvgGrossSupp),
            AVG(IndustryGoal),
            AVG(LYNXGoal),
            AVG(PerGrossSavings),
            AVG(DeltaActual2LYNX),
            AVG(PerRpr2RepLaborHr),
            AVG(PerRpr2TotLaborHr),
            AVG(PerRep2RepLaborHr),
            AVG(PerOEM2TotLaborHr),
            AVG(PerLKQ2TotLaborHr),
            AVG(PerAFMK2TotLaborHr)
   from @tmpReportData
   where LineLevel = 1
   group by ServiceChannelCD, InsuranceCompany

   INSERT INTO @tmpReportData
   SElECT   5,
            UserName,
            'ZZ',
            ServiceCHannelCD,
            DriveableFlag,
            AVG(AvgClaimRec2Assg),
            AVG(AvgAssg2Est),
            AVG(PerClmAssg2Est1to5),
            AVG(PerClmAssg2Est6to10),
            AVG(PerClmAssg2Est11plus),
            AVG(AvgNew2Est),
            AVG(AvgEst2RepairSt),
            AVG(AvgRepairSt2End),
            AVG(AvgRepairEnd2Close),
            AVG(AvgEst2Close),
            AVG(AvgNew2Close),
            SUM(VehicleCount),
            SUM(SupplementCount),
            SUM(ClaimsWithSupplement),
            AVG(PerClamsWithSupp),
            AVG(AvgGrossOrigEst),
            AVG(AvgGrossFBEEst),
            AVG(AvgGrossSupp),
            AVG(IndustryGoal),
            AVG(LYNXGoal),
            AVG(PerGrossSavings),
            AVG(DeltaActual2LYNX),
            AVG(PerRpr2RepLaborHr),
            AVG(PerRpr2TotLaborHr),
            AVG(PerRep2RepLaborHr),
            AVG(PerOEM2TotLaborHr),
            AVG(PerLKQ2TotLaborHr),
            AVG(PerAFMK2TotLaborHr)
   from @tmpReportData
   where LineLevel = 1
   group by ServiceChannelCD, UserName, DriveableFlag

   INSERT INTO @tmpReportData
   SElECT   6,
            UserName + ' Total',
            'ZZ',
            ServiceCHannelCD,
            NULL,
            AVG(AvgClaimRec2Assg),
            AVG(AvgAssg2Est),
            AVG(PerClmAssg2Est1to5),
            AVG(PerClmAssg2Est6to10),
            AVG(PerClmAssg2Est11plus),
            AVG(AvgNew2Est),
            AVG(AvgEst2RepairSt),
            AVG(AvgRepairSt2End),
            AVG(AvgRepairEnd2Close),
            AVG(AvgEst2Close),
            AVG(AvgNew2Close),
            SUM(VehicleCount),
            SUM(SupplementCount),
            SUM(ClaimsWithSupplement),
            AVG(PerClamsWithSupp),
            AVG(AvgGrossOrigEst),
            AVG(AvgGrossFBEEst),
            AVG(AvgGrossSupp),
            AVG(IndustryGoal),
            AVG(LYNXGoal),
            AVG(PerGrossSavings),
            AVG(DeltaActual2LYNX),
            AVG(PerRpr2RepLaborHr),
            AVG(PerRpr2TotLaborHr),
            AVG(PerRep2RepLaborHr),
            AVG(PerOEM2TotLaborHr),
            AVG(PerLKQ2TotLaborHr),
            AVG(PerAFMK2TotLaborHr)
   from @tmpReportData
   where LineLevel = 1
   group by ServiceChannelCD, UserName

   INSERT INTO @tmpReportData
   SElECT   7,
            'Grand Total',
            'ZZZ',
            ServiceCHannelCD,
            DriveableFlag,
            AVG(AvgClaimRec2Assg),
            AVG(AvgAssg2Est),
            AVG(PerClmAssg2Est1to5),
            AVG(PerClmAssg2Est6to10),
            AVG(PerClmAssg2Est11plus),
            AVG(AvgNew2Est),
            AVG(AvgEst2RepairSt),
            AVG(AvgRepairSt2End),
            AVG(AvgRepairEnd2Close),
            AVG(AvgEst2Close),
            AVG(AvgNew2Close),
            SUM(VehicleCount),
            SUM(SupplementCount),
            SUM(ClaimsWithSupplement),
            AVG(PerClamsWithSupp),
            AVG(AvgGrossOrigEst),
            AVG(AvgGrossFBEEst),
            AVG(AvgGrossSupp),
            AVG(IndustryGoal),
            AVG(LYNXGoal),
            AVG(PerGrossSavings),
            AVG(DeltaActual2LYNX),
            AVG(PerRpr2RepLaborHr),
            AVG(PerRpr2TotLaborHr),
            AVG(PerRep2RepLaborHr),
            AVG(PerOEM2TotLaborHr),
            AVG(PerLKQ2TotLaborHr),
            AVG(PerAFMK2TotLaborHr)
   from @tmpReportData
   where LineLevel = 1
   group by ServiceChannelCD, DriveableFlag


   INSERT INTO @tmpReportData
   SElECT   8,
            'Grand Total',
            'ZZZZ',
            ServiceCHannelCD,
            NULL,
            AVG(AvgClaimRec2Assg),
            AVG(AvgAssg2Est),
            AVG(PerClmAssg2Est1to5),
            AVG(PerClmAssg2Est6to10),
            AVG(PerClmAssg2Est11plus),
            AVG(AvgNew2Est),
            AVG(AvgEst2RepairSt),
            AVG(AvgRepairSt2End),
            AVG(AvgRepairEnd2Close),
            AVG(AvgEst2Close),
            AVG(AvgNew2Close),
            SUM(VehicleCount),
            SUM(SupplementCount),
            SUM(ClaimsWithSupplement),
            AVG(PerClamsWithSupp),
            AVG(AvgGrossOrigEst),
            AVG(AvgGrossFBEEst),
            AVG(AvgGrossSupp),
            AVG(IndustryGoal),
            AVG(LYNXGoal),
            AVG(PerGrossSavings),
            AVG(DeltaActual2LYNX),
            AVG(PerRpr2RepLaborHr),
            AVG(PerRpr2TotLaborHr),
            AVG(PerRep2RepLaborHr),
            AVG(PerOEM2TotLaborHr),
            AVG(PerLKQ2TotLaborHr),
            AVG(PerAFMK2TotLaborHr)
   from @tmpReportData
   where LineLevel = 1
   group by ServiceChannelCD


   SELECT  convert(varchar, @ReportStart, 101) as 'ReportStartDate', convert(varchar, @ReportEnd, 101) as 'ReportEndDate', *
   /*convert(varchar(50), UserName) as 'Name',
   InsuranceCompany,
   IsNULL(Convert(varchar(1), DriveableFlag), '') as Driveable,
   AvgNew2Est,
   AvgNew2Close*/
   FROM @tmpReportData
   --where ServiceChannelCD = 'PS'
   order by ServiceChannelCD, InsuranceCompany, UserName, LineLevel, DriveableFlag
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMDSPSMasterMetrics' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptMDSPSMasterMetrics TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/