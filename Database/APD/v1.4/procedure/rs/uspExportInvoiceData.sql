-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspExportInvoiceData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspExportInvoiceData 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspExportInvoiceData
* SYSTEM:       APD VAFB Invoice Extract - Data Warehouse and Reporting 
* AUTHOR:       Thomas Duranko
* FUNCTION:     This is the stored procedure for the VAFB Invoice Extract
* DATE:			03Mar2016
*
* PARAMETERS:  
*				@InscCompID - Insurance company ID for the export
*				@vRunType - Type of run requested I = DAILY Indemnity, F = MONTHLY Fee
*				@dtStartDate - (Optional) start date if you want to override the date range
*				@dtEndDate - (Optional) end date if you want to override the date range
* RESULT SET:
*				Structured XML
*
* REVISIONS:	03Mar2016 - TVD - Initial development
*				02May2017 - TVD - Added ISNULL's so that blank tags will show in XML
*				28Jun2017 - TVD - Change WHERE clause to look at DispatchNumber instead of SentToIngresDate.
*								  This fixes CS issues because of Bulk billing
*
* PROCESS:
*				If vRunType is an I, this will set the date range to 1 day and provide only Indemnity data.  Fee tag will
*				be $0.00
*
*				If vRunType is an F, this will set the date range to 1 day and provide only Fee data.  Indemnity tag will
*				be $0.00
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspExportInvoiceData]
	@InscCompID							INT
	, @vRunType							VARCHAR(1) -- I Indemnity, F Fee
	, @dtStartDate						DATETIME = NULL
	, @dtEndDate						DATETIME = NULL
AS
BEGIN
	DECLARE @LBoundDate					DATETIME				
	DECLARE @UBoundDate					DATETIME
	DECLARE @vBatchNumber				VARCHAR(15)
	DECLARE @iInvoiceCount				INT
	DECLARE @mTotalCost					MONEY
	DECLARE @mTotalIndemnityAmt			MONEY
	DECLARE @mTotalFeeAmt				MONEY
	DECLARE @mTotalDeductibleAmt		MONEY
	DECLARE @mTotalTaxTotalAmt			MONEY
	DECLARE @vNow						VARCHAR(10)
    DECLARE @bDebug						udt_std_flag
	DECLARE @ProcName					VARCHAR(50)
	DECLARE @vMsg						VARCHAR(50)

	SET NOCOUNT ON

	-- Init Params
    SET @bDebug = 0 
	SET @ProcName = 'uspExportInvoiceData'
	SET @vMsg = ''
	SET @vRunType = UPPER(@vRunType)
	
	-- Processing Begins
	IF @bDebug = 1
	BEGIN
		PRINT 'Invoice Extract processing began at: ' + @vNow
	END
	
	-- Parameter Validation
	
    -- Check to make sure a valid insurance Company was passed in
    IF  (@InscCompID IS NULL) OR
        (NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InscCompID))
    BEGIN
        -- Invalid InscCompID
		SET @vMsg = CONVERT(VARCHAR(20),@InscCompID) + ' - Not valid or does not exist'
        RAISERROR('ERR-100 "%s" (@InscCompID: %u)', 16, 1, @vMsg, @InscCompID)
        RETURN
    END

    -- Check to make sure a valid RunType was passed in
    IF  (@vRunType IS NULL)
        OR (@vRunType NOT IN ('I','F'))
    BEGIN
        -- Invalid RunType
		SET @vMsg = @vRunType + ' - Not valid or does not exist'
        RAISERROR('ERR-101 "%s" (@vRunType: %s)', 16, 1, @vMsg, @vRunType)
        RETURN
    END

    -- If Start and End dates are supplied, validate them
    IF (ISDATE(@dtStartDate) = 1)
    BEGIN
	    IF  (ISDATE(@dtEndDate) = 1)
		BEGIN
			-- GOOD Valid Date
			SET @LBoundDate = CONVERT(VARCHAR(10), @dtStartDate, 121)
			SET @UBoundDate = CONVERT(VARCHAR(10), @dtEndDate, 121)
		END
		ELSE
		BEGIN
			-- Invalid EndDate
			SET @vMsg = CONVERT(VARCHAR(50), @dtEndDate, 121) + ' - Not a valid Date.  YYYY-MM-DD'
			RAISERROR('ERR-102 "%s" ', 16, 1, @vMsg)
			RETURN
		END
    END
    ELSE
    BEGIN
		-- Use default date range processing
		-- Set bounds based on RunType.  I = Daily, F = Monthly
		IF (@vRunType = 'I')
		BEGIN
			SET @UBoundDate = CAST(CEILING(CAST(CURRENT_TIMESTAMP AS float)) AS DATETIME)	
			SET @LBoundDate = CAST(FLOOR(CAST(CURRENT_TIMESTAMP AS float)) AS DATETIME)				
		END
		ELSE
		BEGIN
			SET @UBoundDate = DATEADD(DAY, -(DAY(DATEADD(MONTH, 1, CURRENT_TIMESTAMP))), DATEADD(MONTH, 1, CAST(CEILING(CAST(CURRENT_TIMESTAMP AS float)) AS DATETIME)))	
			SET @LBoundDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, CURRENT_TIMESTAMP), 0)	
		END
	END

	-- Date Manual Override
	--SET @UBoundDate = '2016-01-08'	
	--SET @LBoundDate = '2016-01-07'				

	IF @bDebug = 1
	BEGIN
		PRINT 'Invoice Extract processing began at: ' + @vNow
		SELECT 'Start Date: ' + CONVERT(VARCHAR(10), @LBoundDate,121)
		SELECT 'End Date: ' + CONVERT(VARCHAR(10), @UBoundDate,121)
	END

	SET @iInvoiceCount = 0
	SET @vNow = CONVERT(VARCHAR(10), CURRENT_TIMESTAMP, 121)

	-- Create Batch Number
	SET @vBatchNumber = @vRunType + REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(20), CURRENT_TIMESTAMP,120),'-',''),':',''),' ','')

	-- Create Temp table to keep this all in sync
	CREATE TABLE #InvoiceData (
		ClientClaimNumber				VARCHAR(30)
		, LynxID						INT
		, ServiceChannel				VARCHAR(4)			
		, InvoiceID						INT
		, ApplicationName				VARCHAR(50)
		, ClaimAspectServiceChannelID	INT
		, CoverageProfileCD				VARCHAR(4)			
		, VIN							VARCHAR(17)				
		, DispatchNumber				VARCHAR(50)	
		, InvoiceDate					VARCHAR(50)	
		, ItemTypeCD					VARCHAR(4)
		, IndemnityAmt					MONEY
		, FeeAmt						MONEY
		, DeductibleAmt					MONEY
		, TaxTotalAmt					MONEY
		, SysLastUpdatedDate			VARCHAR(50)
	)

	-- Populate Temp table
	INSERT INTO #InvoiceData
	SELECT 
		c.ClientClaimNumber
		, c.LynxID
		, casc.ServiceChannelCD
		, i.InvoiceID
		, a.Name
		, casc.ClaimAspectServiceChannelID
		, cct.CoverageProfileCD		
		, cv.VIN					
		, i.DispatchNumber	
		, CONVERT(VARCHAR(50), i.EntryDate,121)
		, i.ItemTypeCD
		, CASE i.ItemTypeCD
			WHEN 'I' THEN i.Amount
			ELSE
				0
		  END
		, CASE i.ItemTypeCD
			WHEN 'F' THEN i.Amount
			ELSE
				0
		  END
		, i.DeductibleAmt	
		, i.TaxTotalAmt	
		, CONVERT(VARCHAR(50),i.SysLastUpdatedDate,121)
	FROM  
		utb_claim c WITH(NOLOCK)
		INNER JOIN utb_claim_aspect ca WITH(NOLOCK)
			ON ca.LynxID = c.LynxID
		LEFT JOIN utb_invoice i WITH(NOLOCK)
			ON i.ClaimAspectID = ca.ClaimAspectID
		INNER JOIN utb_application a WITH(NOLOCK)
			ON a.ApplicationID = ca.SourceApplicationID
		INNER JOIN dbo.utb_claim_aspect_service_channel casc WITH(NOLOCK)
			ON casc.ClaimAspectID = ca.ClaimAspectID
		INNER JOIN dbo.utb_client_coverage_type cct WITH(NOLOCK)
			ON cct.ClientCoverageTypeID = ca.ClientCoverageTypeID
		INNER JOIN dbo.utb_claim_vehicle cv WITH(NOLOCK)
			ON cv.ClaimAspectID = ca.ClaimAspectID
	WHERE 
		c.InsuranceCompanyID = @InscCompID
		--AND SentToIngresDate BETWEEN @LBoundDate AND @UBoundDate
		AND i.Entrydate BETWEEN @LBoundDate AND @UBoundDate
		AND i.DispatchNumber IS NOT NULL
		AND i.EnabledFlag = 1

	IF @bDebug = 1
	BEGIN
		PRINT 'Invoice Extract processing began at: ' + @vNow
		SELECT 'InscCompID: ' + CONVERT(VARCHAR(10), @InscCompID)
		SELECT 'Start Date: ' + CONVERT(VARCHAR(10), @LBoundDate,121)
		SELECT 'End Date: ' + CONVERT(VARCHAR(10), @UBoundDate,121)
		--SELECT * FROM #InvoiceData
		--DROP TABLE #InvoiceData
	END

	-- Calculate Totals for Footer
	SELECT
		@iInvoiceCount = COUNT(InvoiceID)
	FROM  
		#InvoiceData
	WHERE 
		ItemTypeCD = @vRunType

	-- Indemnity Total
	SELECT
		@mTotalIndemnityAmt = SUM(IndemnityAmt)
		, @mTotalTaxTotalAmt = SUM(TaxTotalAmt)
		, @mTotalDeductibleAmt = SUM(DeductibleAmt)
	FROM  
		#InvoiceData
	WHERE 
		ItemTypeCD = 'I'

	-- Fee Total
	IF (@vRunType='I')
	BEGIN
		SET @mTotalFeeAmt = 0
	END
	ELSE
	BEGIN
		SELECT
			@mTotalFeeAmt = SUM(FeeAmt)
		FROM  
			#InvoiceData
		WHERE 
			ItemTypeCD = 'F'
	END

	-- Tests
	--SELECT 'Invoice Count: ' + CONVERT(VARCHAR(50),@iInvoiceCount)
	--SELECT 'TotalIndemnityAmt Count: ' + CONVERT(VARCHAR(50),@mTotalIndemnityAmt)
	--SELECT 'TotalFeeAmt Count: ' + CONVERT(VARCHAR(50),@mTotalFeeAmt)
	--SELECT 'TotalDeductibleAmt Count: ' + CONVERT(VARCHAR(50),@mTotalDeductibleAmt)
	--SELECT 'TotalTaxTotalAmt Count: ' + CONVERT(VARCHAR(50),@mTotalTaxTotalAmt)

	-- Main XML
	SELECT 
		1									AS Tag
		, NULL								AS Parent

		--BillingData
		, NULL								AS [BillingData!1!Root]

		--BillingHeader
		, NULL								AS [BillingHeader!2!InsuranceCo!Element]
		, NULL								AS [BillingHeader!2!BatchNumber!Element]
		, NULL								AS [BillingHeader!2!RunDate!Element]
		, NULL								AS [BillingHeader!2!PeriodStart!Element]
		, NULL								AS [BillingHeader!2!PeriodEnd!Element]

		--BillingDetails
		, NULL								AS [BillingDetails!3!Root!Element]
		
		--BillingDetail
		, NULL								AS [BillingDetail!4!ClaimNumber!Element]
		, NULL								AS [BillingDetail!4!LynxID!Element]
		, NULL								AS [BillingDetail!4!ServiceChannel!Element]
		, NULL								AS [BillingDetail!4!ApplicationName!Element]
		, NULL								AS [BillingDetail!4!ClaimAspectServiceChannelCD!Element]
		, NULL								AS [BillingDetail!4!CoverageProfileCD!Element]
		, NULL								AS [BillingDetail!4!ClaimVehicleVin!Element]
		, NULL								AS [BillingDetail!4!DispatchNumber!Element]
		, NULL								AS [BillingDetail!4!InvoiceDate!Element]
		, NULL								AS [BillingDetail!4!IndemnityAmt!Element]
		, NULL								AS [BillingDetail!4!FeeAmt!Element]
		, NULL								AS [BillingDetail!4!DeductibleAmt!Element]
		, NULL								AS [BillingDetail!4!TaxAmt!Element]

		--BillingFooter
		, NULL								AS [BillingFooter!5!TotalInvoice!Element]
		, NULL								AS [BillingFooter!5!TotalIndemnityAmt!Element]
		, NULL								AS [BillingFooter!5!TotalFeeAmt!Element]
		, NULL								AS [BillingFooter!5!TotalDeductibleAmt!Element]
		, NULL								AS [BillingFooter!5!TotalTaxAmt!Element]

	UNION ALL

	SELECT
		2
		, 1
		-- BillingData
		, NULL

		--BillingHeader
		, @InscCompID
		, @vBatchNumber
		, @vNow
		, CONVERT(VARCHAR(10),@LBoundDate,121)
		, CONVERT(VARCHAR(10),@UBoundDate,121)

		--BillingDetails
		, NULL

		--BillingDetail
		, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

		--BillingFooter
		, NULL, NULL, NULL, NULL, NULL

	UNION ALL

	SELECT
		3
		, 1
		-- BillingData
		, NULL

		--BillingHeader
		, NULL, NULL, NULL, NULL, NULL

		--BillingDetails
		, NULL
		
		--BillingDetail
		, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

		--BillingFooter
		, NULL, NULL, NULL, NULL, NULL

	UNION ALL

	SELECT
		4
		, 3
		-- BillingData
		, NULL

		--BillingHeader
		, NULL, NULL, NULL, NULL, NULL

		--BillingDetails
		, NULL
		
		--BillingDetail
		, ClientClaimNumber
		, LynxID
		, ServiceChannel
		, ApplicationName
		, ClaimAspectServiceChannelID
		, CoverageProfileCD		
		, VIN					
		, DispatchNumber	
		, InvoiceDate
		, CASE ItemTypeCD
			WHEN 'I' THEN IndemnityAmt
			ELSE
				0
		  END
		, CASE ItemTypeCD
			WHEN 'F' THEN FeeAmt
			ELSE
				0
		  END
		, ISNULL(DeductibleAmt,'0.0000')	
		, ISNULL(TaxTotalAmt,'0.0000')		

		--BillingFooter
		, NULL, NULL, NULL, NULL, NULL
	FROM  
		#InvoiceData
	WHERE 
		ItemTypeCD = @vRunType

	UNION ALL

	SELECT
		5
		, 1
		-- BillingData
		, NULL

		--BillingHeader
		, NULL, NULL, NULL, NULL, NULL

		--BillingDetails
		, NULL
		
		--BillingDetail
		, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

		--BillingFooter
		, @iInvoiceCount
		, CASE UPPER(@vRunType)
			WHEN 'F' THEN '0.0000'
			ELSE
				ISNULL(@mTotalIndemnityAmt,'0.0000')
		  END 
		, CASE UPPER(@vRunType)
			WHEN 'I' THEN '0.0000'
			ELSE
				ISNULL(@mTotalFeeAmt,'0.0000')
		  END 
		, ISNULL(@mTotalDeductibleAmt,'0.0000')
		, ISNULL(@mTotalTaxTotalAmt,'0.0000')
		
	FOR XML EXPLICIT

	DROP TABLE #InvoiceData

	-- Processing Ends
	IF @bDebug = 1
	BEGIN
		PRINT 'Invoice Extract processing ended at: ' + @vNow
	END

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspExportInvoiceData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspExportInvoiceData TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspExportInvoiceData TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/