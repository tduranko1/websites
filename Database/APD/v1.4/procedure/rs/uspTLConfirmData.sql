-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLConfirmData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLConfirmData 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLConfirmData
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Confirms and locks the Total Loss Data
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID  Checklist ID
* (I) @UserID       User ID
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLConfirmData
    @ClaimAspectServiceChannelID    udt_std_id_big,
    @UserID                         varchar(50)
AS
BEGIN

    --Initialize string parameters
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    
    SET @ProcName = 'uspTLConfirmData'
    SET @now = CURRENT_TIMESTAMP
    
    IF NOT EXISTS(SELECT ClaimAspectServiceChannelID
                  FROM utb_claim_aspect_service_channel
                  WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
    BEGIN
        -- Invalid ClaimAspectServiceChannelID
        RAISERROR('%s: Invalid ClaimAspectServiceChannelID.', 16, 1, @ProcName)
        RETURN
    END

    -- Check the user id
    IF NOT EXISTS (SELECT UserID FROM utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User
        RAISERROR('%s: Invalid User.', 16, 1, @ProcName)
        RETURN
    END

    BEGIN TRANSACTION
    
    UPDATE utb_claim_aspect_service_channel
    SET PayoffAmountConfirmedDate = @now,
        PayoffAmountConfirmedFlag = 1,
        SysLastUserID = @UserID,
        SysLastUpdatedDate = @now
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_claim_apsect_service_channel', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
      
    COMMIT TRANSACTION
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLConfirmData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLConfirmData TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/