

USE [udb_apd]

/****** Object:  Table [dbo].[utb_Document_EMSEstimate]   Script Date: 12/15/2017 05:51:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[utb_Document_EMSEstimate]') AND type in (N'U'))
DROP TABLE [dbo].[utb_Document_EMSEstimate]
GO

/****** Object:  Table [dbo].[utb_Document_EMSEstimate]   Script Date: 04/19/2018 05:51:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[utb_Document_EMSEstimate](
	[DocumentID] [dbo].[udt_std_int_big] NOT NULL,
	[ClaimNumber] [varchar](30) NOT NULL,
	[DocumentSource] [varchar](255) NULL,
	[EstimateGUID]  [varchar](255) NOT NULL,
	[KEYID] [varchar](255) NULL,
	[SysLastUserID] [dbo].[udt_std_id] NOT NULL,
	[SysLastUpdatedDate] [dbo].[udt_sys_last_updated_date] NOT NULL
) ON [ufg_primary]

GO


