-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptLSShops' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptLSShops 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- Create the stored procedure

/************************************************************************************************************************
*
* PROCEDURE:    uspRptLSShops
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Report CF shops.
*
* PARAMETERS:  
* None.
*
* RESULT SET:
*
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptLSShops
AS
BEGIN
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @ProcName                       AS VARCHAR(30)

    SET @ProcName = 'uspRptLSShops'

    SELECT '' AS 'LYNXSelect Shop Activity'

    SELECT  sl.ShopLocationID,
            LTRIM(sl.Name) AS ShopName,
            sl.Address1 AS ShopAddress,
            sl.AddressCity AS ShopCity,
            sl.AddressState AS ShopState,
            sl.AddressZip AS ShopZip,
            sl.ProgramFlag,
            sl.CertifiedFirstFlag,
            sl.PPGCTSId,
            IsNull(dtwhfc_py.CompletedAssignments,0) AS CompletedPriorYearAssignments,
            IsNull(dtwhfc_py.TotalAmount,0.00) AS TotalPriorYearAmount,
            IsNull(dtwhfc_cy.CompletedAssignments,0) AS CompletedCurrentYearAssignments,
            IsNull(dtwhfc_cy.TotalAmount,0.00) AS TotalCurrentYearAmount,
            IsNull(dtwhfc_pm.CompletedAssignments,0) AS CompletedCurrentMonthAssignments,
            IsNull(dtwhfc_pm.TotalAmount,0.00) AS TotalCurrentMonthAmount
    FROM dbo.utb_shop_location sl
   LEFT JOIN (SELECT rl.ShopLocationID, COUNT(fc.FactID) AS CompletedAssignments, SUM(FinalEstimateGrossAmt) AS TotalAmount
                 FROM dbo.utb_dtwh_fact_claim AS fc
                INNER JOIN dbo.utb_dtwh_dim_repair_location AS rl
                   ON fc.RepairLocationID = rl.RepairLocationID
                INNER JOIN dbo.utb_dtwh_dim_disposition_type AS dt
                   ON fc.DispositionTypeID = dt.DispositionTypeID
                  AND dt.DispositionTypeCD = 'RC'
                INNER JOIN dbo.utb_dtwh_dim_time as dtime
                   on fc.TimeIDClosed = dtime.TimeID
                WHERE dtime.YearValue = Year(CURRENT_TIMESTAMP) - 1
                GROUP BY rl.ShopLocationID) AS dtwhfc_py
       ON dtwhfc_py.ShopLocationID = sl.ShopLocationID
   LEFT JOIN (SELECT rl.ShopLocationID, COUNT(fc.FactID) AS CompletedAssignments, SUM(FinalEstimateGrossAmt) AS TotalAmount
                 FROM dbo.utb_dtwh_fact_claim AS fc
                INNER JOIN dbo.utb_dtwh_dim_repair_location AS rl
                   ON fc.RepairLocationID = rl.RepairLocationID
                INNER JOIN dbo.utb_dtwh_dim_disposition_type AS dt
                   ON fc.DispositionTypeID = dt.DispositionTypeID
                  AND dt.DispositionTypeCD = 'RC'
                INNER JOIN dbo.utb_dtwh_dim_time as dtime
                   on fc.TimeIDClosed = dtime.TimeID
                WHERE dtime.YearValue = Year(CURRENT_TIMESTAMP)
                GROUP BY rl.ShopLocationID) AS dtwhfc_cy
       ON dtwhfc_cy.ShopLocationID = sl.ShopLocationID
   LEFT JOIN (SELECT rl.ShopLocationID, COUNT(fc.FactID) AS CompletedAssignments, SUM(FinalEstimateGrossAmt) AS TotalAmount
                 FROM dbo.utb_dtwh_fact_claim AS fc
                INNER JOIN dbo.utb_dtwh_dim_repair_location AS rl
                   ON fc.RepairLocationID = rl.RepairLocationID
                INNER JOIN dbo.utb_dtwh_dim_disposition_type AS dt
                   ON fc.DispositionTypeID = dt.DispositionTypeID
                  AND dt.DispositionTypeCD = 'RC'
                INNER JOIN dbo.utb_dtwh_dim_time as dtime
                   on fc.TimeIDClosed = dtime.TimeID
                WHERE dtime.YearValue = Year(CURRENT_TIMESTAMP)
                  AND dtime.MonthOfYear = Month(CURRENT_TIMESTAMP)
                GROUP BY rl.ShopLocationID) AS dtwhfc_pm
       ON dtwhfc_pm.ShopLocationID = sl.ShopLocationID
    WHERE sl.ProgramFlag = 1
    ORDER BY 1

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT


    -- Check error value

    IF @error <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('APD - %s: SQL Server Error getting Lynx Select shop list', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptLSShops' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptLSShops TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

