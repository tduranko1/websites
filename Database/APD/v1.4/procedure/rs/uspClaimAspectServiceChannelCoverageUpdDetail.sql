-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimAspectServiceChannelCoverageUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimAspectServiceChannelCoverageUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimAspectServiceChannelCoverageUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       M. Ahmed
* FUNCTION:     Updates/Inserts Coverage data into usp_Claim_Aspect_Service_Channel_Coverage Table.  If no 
*               coverage record exists, this proc will insert one other wise the existing row will be updated
*
* PARAMETERS:
* (I) @ClaimAspectServiceChannelID..The claim aspect service channel to be used for the update of a row in the table.
* (I) @ClaimCoverageID..............The claim coverage id to be used for the update of a particular row in the table.
* (I) @DocumentID...................The current estimate document id.
* (I) @DeductibleAppliedAmt.........The deductible amount applied for the coverage
* (I) @LimitAppliedAmt..............The coverage limit amount applied
* (I) @PartialCoverageFlag..........The partial coverage flag 
* (I) @UserID.......................The user updating the note
* (I) @SysLastUpdatedDate...........The "previous" updated date for coverage information
*
* RESULT SET:
*       An XML document containing last updated date time values for Coverage
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimAspectServiceChannelCoverageUpdDetail
   @ClaimAspectServiceChannelID  udt_std_id_big,
   @ClaimCoverageID			      udt_std_id_big,
   @DocumentID                   udt_std_id_big = null,
   @DeductibleAppliedAmt		   decimal(9,2) = null,
   @LimitAppliedAmt			      decimal(9,2) = null,
   @PartialCoverageFlag          udt_std_flag,         
   @UserID                       udt_std_id,
   @SysLastUpdatedDate           varchar(30) = null   
AS
BEGIN

    -- Declare internal variables
    Declare @Key                  varchar(8000)
    DECLARE @now               AS datetime
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    Declare @NewRow            as bit
    DECLARE @Debug             AS bit

    SET @ProcName = 'uspClaimAspectServiceChannelCoverageUpdDetail'

    -- SET NOCOUNT to ON and no longer display the count message or warnings

    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF
    
    IF @DeductibleAppliedAmt IS NULL SET @DeductibleAppliedAmt = 0
    IF @LimitAppliedAmt IS NULL SET @LimitAppliedAmt = 0
    
    SET @Debug = 0

    --Find out if this row exists or not.  The reason for this is, if the
    --row does not exist then we insert the data otherwise it is an update

	--Is it a valid set of claim aspect service channel id & claim coverage id?
	 IF (
        SELECT   count(*) 
        FROM     utb_Claim_Aspect_Service_Channel_Coverage
        WHERE    ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
        AND      ClaimCoverageID = @ClaimCoverageID
        ) = 0
        BEGIN
            set @NewRow = 1 -- it is a new row and we are eventually going to insert the data
        END
    else
        begin
            set @NewRow = 0 --Just update the row with the data provided
        end

    -- Check to make sure a valid User id was passed in
    IF NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    IF @NewRow = 0
    BEGIN
      Select @key = ltrim(rtrim(convert(varchar(20),@ClaimAspectServiceChannelID))) + ',' + ltrim(rtrim(convert(varchar(20),@ClaimCoverageID)))
      exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_claim_aspect_service_channel_coverage', @key
    END


   IF @Debug = 1
   BEGIN
      PRINT '@NewRow = ' + convert(varchar, @NewRow)
   END

    
   -- Get current timestamp
   SET @now = CURRENT_TIMESTAMP
   
   BEGIN TRANSACTION

    if @NewRow = 0 --If it is not a new row then update the row
        Begin
            IF @Debug = 1
            BEGIN
               PRINT 'Updating record...'
            END
            
            --Now update the table with the values provided
            Update utb_Claim_Aspect_Service_Channel_Coverage
            set    DeductibleAppliedAmt =@DeductibleAppliedAmt,
                   LimitAppliedAmt = @LimitAppliedAmt,
                   PartialCoverageFlag = @PartialCoverageFlag,
                   SysLastUserID = @UserID,
                   SysLastUpdatedDate = @now
            where  ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
            and    ClaimCoverageID = @ClaimCoverageID
        
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error
               
                 RAISERROR('104|%s|utb_Claim_Aspect_Service_Channel_Coverage', 16, 1, @ProcName)
                 ROLLBACK TRANSACTION
                 RETURN
            END

            IF @Debug = 1
            BEGIN
               PRINT 'Updating record...'
            END
        End
    else  -- Otherwise insert the data into a new row
        Begin
            IF @Debug = 1
            BEGIN
               PRINT 'Inserting record...'
            END
            Insert into utb_Claim_Aspect_Service_Channel_Coverage
                    (
                    ClaimAspectServiceChannelID,
                    ClaimCoverageID,
                    DeductibleAppliedAmt,
                    LimitAppliedAmt,
                    PartialCoverageFlag,
                    SysLastUserID,
                    SysLastUpdatedDate
                    )
            values  (
                    @ClaimAspectServiceChannelID,
                    @ClaimCoverageID,
                    @DeductibleAppliedAmt,
                    @LimitAppliedAmt,
                    @PartialCoverageFlag,
                    @UserID,
                    @now
                    )

        IF @@ERROR <> 0
            BEGIN
             -- SQL Server Error
            
              RAISERROR('105|%s|utb_Claim_Aspect_Service_Channel_Coverage', 16, 1, @ProcName)
              ROLLBACK TRANSACTION
              RETURN
            END
        End

    IF (@DocumentID IS NOT NULL) AND
       EXISTS(SELECT DocumentID
               FROM dbo.utb_estimate_summary
               WHERE DocumentID = @DocumentID)
    BEGIN
      IF @Debug = 1
      BEGIN
         PRINT 'Refreshing Ded/Limit effects...'
      END
      
      EXEC uspRefreshEstimateDedLimitEffects @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                                             @ClaimCoverageID = @ClaimCoverageID,
                                             @DocumentID = @DocumentID,
                                             @UserID = @UserID


       IF @@ERROR <> 0
       BEGIN
          -- SQL Server Error
       
           RAISERROR  ('99|%s', 16, 1, @ProcName)
           ROLLBACK TRANSACTION
           RETURN
       END    
   
    END
       
    COMMIT

    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Claim Aspect Service Channel Coverage Level Information
            NULL AS [ClaimAspectServiceChannelCoverage!2!ClaimAspectServiceChannelID],
            NULL AS [ClaimAspectServiceChannelCoverage!2!ClaimCoverageID],
            NULL AS [ClaimAspectServiceChannelCoverage!2!SysLastUpdatedDate]


    UNION ALL

    SELECT  2,
            1,
            NULL,
            -- Claim Aspect Service Channel Coverage Level Information
            @ClaimAspectServiceChannelID,
            @ClaimCoverageID,
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimAspectServiceChannelCoverageUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimAspectServiceChannelCoverageUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

