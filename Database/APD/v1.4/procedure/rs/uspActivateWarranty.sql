-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspActivateWarranty' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspActivateWarranty 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspActivateWarranty
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Activate warranty on a vehicle
*
* PARAMETERS:  
*   None
*
* RESULT SET:
* 
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspActivateWarranty
    @ClaimAspectID      udt_std_id_big,
    @UserID             udt_std_id_big
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspActivateWarranty'

    
    -- Set Database options
    
    SET NOCOUNT ON
    

    -- Check to make sure a valid Claim Aspect id was passed in
    
    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT @ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))

    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END
    
    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT @UserID FROM dbo.utb_user WHERE UserID = @UserID))

    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

   BEGIN TRANSACTION
   
   UPDATE utb_claim_aspect
   SET WarrantyExistsFlag = 1,
       SysLastUserID = @UserID
   WHERE ClaimAspectID = @ClaimAspectID
   
   SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
   
    IF @error <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|utb_claim_aspect', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

   COMMIT
   
   RETURN @rowCount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspActivateWarranty' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspActivateWarranty TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/