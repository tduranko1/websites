-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefreshEstimateDedLimitEffects' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefreshEstimateDedLimitEffects 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefreshEstimateDedLimitEffects
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Updates/Inserts Estimate Deductible applied, Limit Applied data.
*
* PARAMETERS:
* (I) @ClaimAspectServiceChannelID..The claim aspect service channel to be used for the update of a row in the table.
* (I) @ClaimCoverageID..............The claim coverage id to be used for the update of a particular row in the table.
* (I) @DeductibleAppliedAmt.........The deductible amount applied for the coverage
* (I) @UserID.......................The user updating the note
* (I) @SysLastUpdatedDate...........The "previous" updated date for coverage information
*
* RESULT SET:
*       None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefreshEstimateDedLimitEffects
   @ClaimAspectServiceChannelID  udt_std_id_big,
   @ClaimCoverageID              udt_std_id_big,
   @DocumentID                   udt_std_id_big,
   @UserID                       udt_std_id
AS
BEGIN
    DECLARE @now               AS datetime
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    DECLARE @EstimateSummaryTypeIDRepairTotal as int
    DECLARE @EstimateSummaryTypeIDDeductiblesApplied as int
    DECLARE @EstimateSummaryTypeIDLimitEffect as int
    DECLARE @EstimateSummaryTypeIDNetTotalEffect as int

    DECLARE @EstGrossRepairTotal as decimal(9,2)
    DECLARE @EstNetTotalEffect   as decimal(9,2)
    DECLARE @DeductiblesApplied  as decimal(9,2)
    DECLARE @LimitsEffect        as decimal(9,2)

    SET @ProcName = 'uspRefreshEstimateDedLimitEffects'

    -- SET NOCOUNT to ON and no longer display the count message or warnings

    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

      
    SELECT @EstimateSummaryTypeIDRepairTotal = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'RepairTotal'
      AND CategoryCD = 'TT'
      
    IF @EstimateSummaryTypeIDRepairTotal IS NULL
    BEGIN
        -- Missing Repair Total Estimate Summary Type

        RAISERROR('101|%s|Missing Estimate Summary Type: RepairTotal', 16, 1, @ProcName)
        RETURN
    END

    SELECT @EstimateSummaryTypeIDDeductiblesApplied = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'DeductiblesApplied'
      AND CategoryCD = 'OT'
      
    IF @EstimateSummaryTypeIDDeductiblesApplied IS NULL
    BEGIN
        -- Missing Deductibles applied Estimate Summary Type

        RAISERROR('101|%s|Missing Estimate Summary Type: DeductiblesApplied', 16, 1, @ProcName)
        RETURN
    END

    SELECT @EstimateSummaryTypeIDLimitEffect = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'LimitEffect'
      AND CategoryCD = 'OT'
      
    IF @EstimateSummaryTypeIDLimitEffect IS NULL
    BEGIN
        -- Missing Limit Effect Estimate Summary Type

        RAISERROR('101|%s|Missing Estimate Summary Type: LimitEffect', 16, 1, @ProcName)
        RETURN
    END

    SELECT @EstimateSummaryTypeIDNetTotalEffect = EstimateSummaryTypeID
    FROM dbo.utb_estimate_summary_type
    WHERE Name = 'NetTotalEffect'
      AND CategoryCD = 'OT'
      
    IF @EstimateSummaryTypeIDNetTotalEffect IS NULL
    BEGIN
        -- Missing Net Total Effect Estimate Summary Type

        RAISERROR('101|%s|Missing Estimate Summary Type: NetTotalEffect', 16, 1, @ProcName)
        RETURN
    END
      
    SET @now = CURRENT_TIMESTAMP
      
    -- Get the estimate Gross Repair Total Amount
    SELECT @EstGrossRepairTotal = OriginalExtendedAmt
    FROM dbo.utb_estimate_summary
    WHERE DocumentID = @DocumentID
      AND EstimateSummaryTypeID = @EstimateSummaryTypeIDRepairTotal

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    

    -- Get the deductibles and Limits applied for this service channel id
    SELECT @DeductiblesApplied = SUM(isNull(DeductibleAppliedAmt, 0)),
           @LimitsEffect = SUM(isNull(LimitAppliedAmt, 0))
    FROM dbo.utb_claim_aspect_service_channel_coverage
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
              
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    

    SET @EstNetTotalEffect = @EstGrossRepairTotal - @DeductiblesApplied - @LimitsEffect
    
    -- Update the Deductibles applied
    UPDATE dbo.utb_estimate_summary
    SET AgreedUnitAmt = @DeductiblesApplied,
        AgreedExtendedAmt = @DeductiblesApplied,
        OriginalUnitAmt = @DeductiblesApplied,
        OriginalExtendedAmt = @DeductiblesApplied,
        SysLastUserID = @UserID,
        SysLastUpdatedDate = @now
    WHERE DocumentID = @DocumentID
      AND EstimateSummaryTypeID = @EstimateSummaryTypeIDDeductiblesApplied
      
    IF @@ROWCOUNT = 0
    BEGIN
      INSERT INTO dbo.utb_estimate_summary (
         DocumentID,
         EstimateSummaryTypeID,
         AgreedExtendedAmt,
         AgreedUnitAmt,
         AgreedTaxableFlag,
         OriginalExtendedAmt,
         OriginalUnitAmt,
         SysLastUserID,
         SysLastUpdatedDate
      ) VALUES (
         @DocumentID,
         @EstimateSummaryTypeIDDeductiblesApplied,
         @DeductiblesApplied,
         @DeductiblesApplied,
         0,
         @DeductiblesApplied,
         @DeductiblesApplied,
         @UserID,
         @now
      )
    END

    -- Update the Limit effects
    UPDATE dbo.utb_estimate_summary
    SET AgreedUnitAmt = @LimitsEffect,
        AgreedExtendedAmt = @LimitsEffect,
        OriginalUnitAmt = @LimitsEffect,
        OriginalExtendedAmt = @LimitsEffect,
        SysLastUserID = @UserID,
        SysLastUpdatedDate = @now
    WHERE DocumentID = @DocumentID
      AND EstimateSummaryTypeID = @EstimateSummaryTypeIDLimitEffect
      
    IF @@ROWCOUNT = 0
    BEGIN
      INSERT INTO dbo.utb_estimate_summary (
         DocumentID,
         EstimateSummaryTypeID,
         AgreedExtendedAmt,
         AgreedUnitAmt,
         AgreedTaxableFlag,
         OriginalExtendedAmt,
         OriginalUnitAmt,
         SysLastUserID,
         SysLastUpdatedDate
      ) VALUES (
         @DocumentID,
         @EstimateSummaryTypeIDLimitEffect,
         @LimitsEffect,
         @LimitsEffect,
         0,
         @LimitsEffect,
         @LimitsEffect,
         @UserID,
         @now
      )
    END

    -- Update the Estimate Net total effect
    UPDATE dbo.utb_estimate_summary
    SET AgreedUnitAmt = @EstNetTotalEffect,
        AgreedExtendedAmt = @EstNetTotalEffect,
        OriginalUnitAmt = @EstNetTotalEffect,
        OriginalExtendedAmt = @EstNetTotalEffect,
        SysLastUserID = @UserID,
        SysLastUpdatedDate = @now
    WHERE DocumentID = @DocumentID
      AND EstimateSummaryTypeID = @EstimateSummaryTypeIDNetTotalEffect
      
    IF @@ROWCOUNT = 0
    BEGIN
      INSERT INTO dbo.utb_estimate_summary (
         DocumentID,
         EstimateSummaryTypeID,
         AgreedExtendedAmt,
         AgreedUnitAmt,
         AgreedTaxableFlag,
         OriginalExtendedAmt,
         OriginalUnitAmt,
         SysLastUserID,
         SysLastUpdatedDate
      ) VALUES (
         @DocumentID,
         @EstimateSummaryTypeIDNetTotalEffect,
         @EstNetTotalEffect,
         @EstNetTotalEffect,
         0,
         @EstNetTotalEffect,
         @EstNetTotalEffect,
         @UserID,
         @now
      )
    END

END
      

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefreshEstimateDedLimitEffects' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefreshEstimateDedLimitEffects TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

