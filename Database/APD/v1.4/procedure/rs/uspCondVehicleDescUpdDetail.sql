-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCondVehicleDescUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCondVehicleDescUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCondVehicleDescUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc to update the Condensed version (ClaimsXP) of Vehicle description.
*
* PARAMETERS:  
*  (I)  @BodyStyle                  Vehicle Body Style 
*  (I)  @BookValueAmt               Book Value amount
*  (I)  @ClaimAspectID              The claim aspect to update
*  (I)  @Color                      Vehicle color
*  (I)  @DriveableFlag              Driveable condition of the vehicle
*  (I)  @ImpactPoints               The current impact points
*  (I)  @LicensePlateNumber         License plate number
*  (I)  @LicensePlateState          License Plate State
*  (I)  @Make                       Vehicle make
*  (I)  @Mileage                    Vehicle mileage/odometer reading
*  (I)  @Model                      Vehicle model
*  (I)  @PriorImpactPoints          Prior impact points
*  (I)  @Remarks                    Vehicle remarks
*  (I)  @RentalDaysAuthorized       Number of rental days authorized
*  (I)  @RentalInstructions         Rental instructions
*  (I)  @VehicleYear                Vehicle year
*  (I)  @VIN                        VIN number of the vehicle
*  (I)  @UserID                     The user updating the data
*  (I)  @SysLastUpdatedDate         The "previous" updated date

* RESULT SET:
* An XML document containing the new SysLastUpdatedDate value
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCondVehicleDescUpdDetail
    @BodyStyle                  udt_auto_body,
    @BookValueAmt               float,
    @ClaimAspectID              udt_std_id_big,
    @Color                      udt_auto_color,
    @CoverageProfileCD          varchar(10),
    @DriveableFlag              udt_std_flag,
    @ImpactPoints               varchar(100),
    @LicensePlateNumber         udt_auto_plate_number,
    @LicensePlateState          udt_addr_state,
    @Make                       udt_auto_make,
    @Mileage                    udt_auto_mileage,
    @Model                      udt_auto_model,
    @PriorImpactPoints          varchar(100),
    @Remarks                    udt_std_desc_xlong,
    @RentalDaysAuthorized       udt_std_int_tiny,
    @RentalInstructions         udt_std_desc_mid,
    @VehicleYear                udt_dt_year,
    @VIN                        udt_auto_vin,
    @UserID                     udt_std_id,
    @SysLastUpdatedDate         varchar(30)
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@BodyStyle))) = 0 SET @BodyStyle = NULL
    IF LEN(RTRIM(LTRIM(@Color))) = 0 SET @Color = NULL
    IF LEN(RTRIM(LTRIM(@LicensePlateNumber))) = 0 SET @LicensePlateNumber = NULL
    IF LEN(RTRIM(LTRIM(@LicensePlateState))) = 0 SET @LicensePlateState = NULL
    IF LEN(RTRIM(LTRIM(@Make))) = 0 SET @Make = NULL
    IF LEN(RTRIM(LTRIM(@Model))) = 0 SET @Model = NULL
    IF LEN(RTRIM(LTRIM(@Vin))) = 0 SET @Vin = NULL
    IF LEN(RTRIM(LTRIM(@Remarks))) = 0 SET @Remarks = NULL
    IF LEN(RTRIM(LTRIM(@RentalInstructions))) = 0 SET @RentalInstructions = NULL

    -- Declare internal variables
    
    DECLARE @ServiceChannelCD       udt_std_cd
    
    DECLARE @error                  AS int
    DECLARE @rowcount               AS int
    
    DECLARE @now                    AS datetime 

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 
    DECLARE @EventID                AS udt_std_int
    DECLARE @HistoryDescription     AS udt_std_desc_long
    DECLARE @PertainsToName         AS varchar(50)
    DECLARE @EventName              AS varchar(50)
    DECLARE @NoteTypeIDClaim        AS udt_std_id
    DECLARE @EntityStatusID         AS udt_std_id
    DECLARE @ClaimIntakeFinishDate  AS datetime
    DECLARE @EntityCreatedDate      AS datetime
    DECLARE @EntityCreatedDateStr   AS varchar(10)
    DECLARE @ReasonID               AS udt_std_id
    DECLARE @CheckListID            AS udt_std_id_big
    DECLARE @NoteDescription        AS udt_std_desc_long

    SET @ProcName = 'uspCondVehicleDescUpdDetail'


    -- Set Database options
    
    SET NOCOUNT ON
    
    
    -- Check to make sure a valid Claim Aspect ID was passed in

    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_vehicle WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    -- Validate the updated date parameter

    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_claim_vehicle', @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END

    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Begin Update

    BEGIN TRANSACTION VehicleDescriptionUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update claim aspect
    UPDATE utb_claim_aspect
    SET CoverageProfileCD  = @CoverageProfileCD,
        SysLastUserID      = @UserID,
        SysLastUpdatedDate = @now
    WHERE ClaimAspectID = @ClaimAspectID
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim_aspect', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    -- Update claim vehicle
    UPDATE  dbo.utb_claim_vehicle
      SET 	BodyStyle               = @BodyStyle,
	    	   BookValueAmt            = @BookValueAmt,
		      Color                   = @Color,
	    	   DriveableFlag           = @DriveableFlag,
      		LicensePlateNumber      = @LicensePlateNumber,
      		LicensePlateState       = @LicensePlateState,
      		Make                    = @Make,
      		Mileage                 = @Mileage,
      		Model                   = @Model,
      		Remarks                 = @Remarks,
      		RentalDaysAuthorized    = @RentalDaysAuthorized,
      		RentalInstructions      = @RentalInstructions,
      		VehicleYear             = @VehicleYear,
      		Vin                     = @Vin,
      		SysLastUserID           = @UserID,
      		SysLastUpdatedDate      = @now
    WHERE ClaimAspectID = @ClaimAspectID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim_vehicle', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    -- Update vehicle impacts.  This is done by deleting the existing impact records and inserting new ones.

    DELETE FROM dbo.utb_vehicle_impact
       WHERE ClaimAspectID = @ClaimAspectID


    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Deletion failure

        RAISERROR('106|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    -- Insert vehicle impacts into utb_vehicle_impact

    INSERT INTO dbo.utb_vehicle_impact
    (
        ClaimAspectID,
        ImpactID,
        CurrentImpactFlag,
        PrimaryImpactFlag,
        PriorImpactFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT @ClaimAspectID,
           value,
           1,
           0,
           0,
           @UserID,
           @now
      FROM dbo.ufnUtilityParseString( @ImpactPoints, ',', 1 )     -- 1 means to trim spaces
        
    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    -- Now assume the first impact is the primary and set the flag in the temporary table

    UPDATE  dbo.utb_vehicle_impact
      SET   PrimaryImpactFlag = 1
      WHERE ClaimAspectID = @ClaimAspectID
        AND ImpactID = (SELECT TOP 1 value FROM dbo.ufnUtilityParseString( @ImpactPoints, ',', 1 ))

    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    -- Now update existing impact records to set the prior impact flag on any duplicates in the Prior Impact Point parameter
        
    UPDATE  dbo.utb_vehicle_impact
      SET   PriorImpactFlag = 1
      WHERE ClaimAspectID = @ClaimAspectID
        AND ImpactID IN (SELECT value FROM dbo.ufnUtilityParseString( @PriorImpactPoints, ',', 1 ))     -- 1 means to trim spaces
        
    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    


    -- Now insert any prior impact records that do not yet exist 

    INSERT INTO dbo.utb_vehicle_impact
    (
        ClaimAspectID,
        ImpactID,
        CurrentImpactFlag,
        PrimaryImpactFlag,
        PriorImpactFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @ClaimAspectID,
            value,
            0,
            0,
            1,
            @UserID,
            @now
      FROM  dbo.ufnUtilityParseString( @PriorImpactPoints, ',', 1 )     -- 1 means to trim spaces
      WHERE value NOT IN (SELECT  ImpactID 
                            FROM  dbo.utb_vehicle_impact 
                            WHERE ClaimAspectID = @ClaimAspectID)

    
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_vehicle_impact', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END    
    

    COMMIT TRANSACTION VehicleDescriptionUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Vehicle Level
            NULL AS [Vehicle!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Vehicle Level
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCondVehicleDescUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCondVehicleDescUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
