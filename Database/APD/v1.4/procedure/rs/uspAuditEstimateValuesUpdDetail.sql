-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditEstimateValuesUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAuditEstimateValuesUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET NOCOUNT ON
GO



CREATE PROCEDURE dbo.uspAuditEstimateValuesUpdDetail
    @ClaimAspectID					udt_std_id_big,
    @AuditedOriginalGrossAmount		udt_std_money	= Null,
    @AdditionalLineItemValues		udt_std_money	= Null,
    @MissingLineItemValues			udt_std_money	= Null,
    @LineItemValueCorrection		udt_std_money	= Null,  
    @UserID							udt_std_id		= 0,
    @ReturnSysLastUpdate			udt_std_flag	= 1

AS
BEGIN
    DECLARE @ProcName       AS VARCHAR(30)       -- Used for raise error stmts 
    DECLARE @error          AS INT
    DECLARE @rowcount       AS INT
    

    SET @ProcName = 'uspAuditEstimateValuesUpdDetail'


    ----Verify User
    --IF @UserID IS NOT NULL
    --BEGIN
    --    IF NOT EXISTS (SELECT u.UserID 
    --                   FROM utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
    --                                   INNER JOIN dbo.utb_application a on ua.ApplicationID = a.ApplicationID
    --                   WHERE ua.AccessBeginDate IS NOT NULL
    --                     AND ua.AccessBeginDate <= CURRENT_TIMESTAMP
    --                     AND ua.AccessEndDate IS NULL
    --                     AND a.Code = @ApplicationCD)
    --    BEGIN
    --       -- Invalid User
    --        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @UserID)
    --        RETURN
    --    END
    --END
    --ELSE
    --BEGIN
    --    IF @UserName is NOT NULL
    --    BEGIN
    --        SET @UserID = (SELECT u.UserID 
    --                       FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
    --                                           INNER JOIN dbo.utb_application a on ua.ApplicationID = a.ApplicationID
    --                       WHERE ua.LogonID = @UserName
    --                         AND ua.AccessBeginDate IS NOT NULL
    --                         AND ua.AccessBeginDate <= CURRENT_TIMESTAMP
    --                         AND ua.AccessEndDate IS NULL
    --                         AND a.Code = @ApplicationCD)
    --        IF @UserID is Null
    --        BEGIN
    --           -- Invalid User
    --            RAISERROR('101|%s|@UserName|%s', 16, 1, @ProcName, @UserName)
    --            RETURN
    --        END
    --    END
    --    ELSE
    --    BEGIN
    --       -- Invalid User
    --        RAISERROR('101|%s|@UserName|%s', 16, 1, @ProcName, @UserName)
    --        RETURN
    --    END
    --END
 


    --Perform the update/insert
    BEGIN TRANSACTION AuditEstimatesValues

		IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
	    
			RAISERROR('99|%s', 16, 1, @ProcName)
			RETURN
		END


		IF EXISTS (SELECT 'TRUE' FROM utb_claim_aspect_audit_values WHERE ClaimAspectID = @ClaimAspectID)
		BEGIN

			UPDATE [dbo].[utb_claim_aspect_audit_values]
			   SET [AuditedOriginalGrossAmount] = @AuditedOriginalGrossAmount
				  ,[AdditionalLineItemValues] = @AdditionalLineItemValues
				  ,[MissingLineItemValues] = @MissingLineItemValues
				  ,[LineItemValueCorrection] = @LineItemValueCorrection
				  ,[SysLastUserID] = @UserID
				  ,[SysLastUpdatedDate] = current_timestamp
			 WHERE [ClaimAspectID] = @ClaimAspectID
			 
			IF @error <> 0
			BEGIN
				-- Update failed
				RAISERROR('104|%s|utb_claim_aspect_audit_values', 16, 1, @ProcName)
				ROLLBACK TRANSACTION
				RETURN
			END

		END
		ELSE
		BEGIN

			INSERT INTO [dbo].[utb_claim_aspect_audit_values]
				([ClaimAspectID]
				,[AuditedOriginalGrossAmount]
				,[AdditionalLineItemValues]
				,[MissingLineItemValues]
				,[LineItemValueCorrection]
				,[SysLastUserID]
				,[SysLastUpdatedDate])
			VALUES
				(@ClaimAspectID
				,@AuditedOriginalGrossAmount
				,@AdditionalLineItemValues
				,@MissingLineItemValues
				,@LineItemValueCorrection
				,@UserID
				,current_timestamp)

			IF @error <> 0
			BEGIN
				-- Insert failed
				RAISERROR('105|%s|utb_claim_aspect_service_channel_coverage', 16, 1, @ProcName)
				ROLLBACK TRANSACTION
				RETURN
			END
		END


   
    COMMIT TRANSACTION AuditEstimatesValues

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Create XML Document to return new updated date time
    IF @ReturnSysLastUpdate = 1
    BEGIN
        SELECT  1                   AS tag,
                NULL                AS parent,
                @ClaimAspectID		AS [Root!1!ClaimAspectID],
                current_timestamp	AS [Root!1!SysLastUpdatedDate]


        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END


END
GO

-- Permissions
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditEstimateValuesUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAuditEstimateValuesUpdDetail TO 
        ugr_lynxapd
    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


