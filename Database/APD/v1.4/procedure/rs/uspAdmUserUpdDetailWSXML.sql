-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmUserUpdDetailWSXML' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmUserUpdDetailWSXML 
END

GO
/****** Object:  StoredProcedure [dbo].[uspAdmUserUpdDetailWSXML]    Script Date: 08/20/2014 02:56:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspAdmUserUpdDetailWSXML
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates user information 
*
* PARAMETERS:  
* (I) @UserID               		User ID to update
* (I) @OfficeID             		Office ID
* (I) @SupervisorUserID     		SupervisorID
* (I) @AssignmentBeginDate  		Assignment Begin Date
* (I) @AssignmentEndDate    		Assignment End Date
* (I) @ClientUserID                 Client User ID
* (I) @EmailAddress         		Email Address
* (I) @FaxAreaCode          		Fax Area Code
* (I) @FaxExchangeNumber    		Fax Exchange Number
* (I) @FaxExtensionNumber   		Fax Extension Number
* (I) @FaxUnitNumber        		Fax Unit Number
* (I) @NameFirst            		User's first name
* (I) @NameLast             		User's last name
* (I) @NameTitle            		User's Salutation
* (I) @PhoneAreaCode        		Phone Area Code
* (I) @PhoneExchangeNumber  		Phone Exchange Number
* (I) @PhoneExtensionNumber 		Phone Extension Number
* (I) @PhoneUnitNumber      		Phone Unit Number
* (I) @SupervisorFlag       		Is this user a supervisor?
* (I) @OperatingMondayStartTime 	User work start time on Monday
* (I) @OperatingMondayEndTime		User work end time on Monday
* (I) @OperatingTuesdayStartTime	User work start time on Tuesday
* (I) @OperatingTuesdayEndTime		User work end time on Tuesday
* (I) @OperatingWednesdayStartTime	User work start time on Wednesday
* (I) @OperatingWednesdayEndTime	User work end time on Wednesday
* (I) @OperatingThursdayStartTime	User work start time on Thursday
* (I) @OperatingThursdayEndTime		User work end time on Thursday
* (I) @OperatingFridayStartTime		User work start time on Friday
* (I) @OperatingFridayEndTime		User work end time on Friday
* (I) @OperatingSaturdayStartTime	User work start time on Saturday
* (I) @OperatingSaturdayEndTime		User work end time on Saturday
* (I) @OperatingSundayStartTime		User work start time on Sunday
* (I) @OperatingSundayEndTime		User work end time on Sunday
* (I) @LicenseStateList     		Comma delimited list of license states the user is licensed for
* (I) @InsAssignWorkList    		Semicolon delimited list of Insurance companies from which the rep can receive work.
* (I) @RoleList             		Comma delimited list of Role IDs the user is a member of.  The primary role is the first in the list.
* (I) @ProfileList         		 	Comma delimited list of profile/value pairs
* (I) @PermissionList       		Comma delimited list of permission/Create/Read/Update/Delete sets
* (I) @AssignmentPoolList       	Comma delimited list of Work Assignment Pool list IDs the user belongs to.
* (I) @SysLastUserID        		User performing the update
* (I) @SysLastUpdatedDate   		The "previous" updated date
*
* RESULT SET:
*   The new last updated date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspAdmUserUpdDetailWSXML]
    @UserID                 		udt_std_id,
    @OfficeID               		udt_std_id,
    @SupervisorUserID       		udt_std_id,
    @AssignmentBeginDate    		varchar(30),
    @AssignmentEndDate      		varchar(30),
    @ClientUserID                   udt_std_desc_short = null,
    @EmailAddress           		udt_web_email,
    @FaxAreaCode            		udt_ph_area_code,
    @FaxExchangeNumber      		udt_ph_exchange_number,
    @FaxExtensionNumber     		udt_ph_extension_number,
    @FaxUnitNumber          		udt_ph_unit_number,
    @NameFirst              		udt_per_name,
    @NameLast               		udt_per_name,
    @NameTitle              		udt_per_title,
    @PhoneAreaCode          		udt_ph_area_code,
    @PhoneExchangeNumber    		udt_ph_exchange_number,
    @PhoneExtensionNumber   		udt_ph_extension_number_long,
    @PhoneUnitNumber        		udt_ph_unit_number,
    @SupervisorFlag         		udt_std_flag,
    @OperatingMondayStartTime       udt_std_time=null,
    @OperatingMondayEndTime         udt_std_time=null,
    @OperatingTuesdayStartTime      udt_std_time=null,
    @OperatingTuesdayEndTime        udt_std_time=null,
    @OperatingWednesdayStartTime    udt_std_time=null,
    @OperatingWednesdayEndTime      udt_std_time=null,
    @OperatingThursdayStartTime     udt_std_time=null,
    @OperatingThursdayEndTime       udt_std_time=null,
    @OperatingFridayStartTime       udt_std_time=null,
    @OperatingFridayEndTime         udt_std_time=null,
    @OperatingSaturdayStartTime     udt_std_time=null,
    @OperatingSaturdayEndTime       udt_std_time=null,
    @OperatingSundayStartTime       udt_std_time=null,
    @OperatingSundayEndTime         udt_std_time=null, 
    @LicenseAssignStList    		varchar(500),
    @InsAssignWorkList      		udt_std_desc_long=null,
    @RoleList               		varchar(100),
    @ProfileList            		varchar(500),
    @PermissionList         		varchar(500),
    @AssignmentPoolList             varchar(100)=null,
    @SysLastUserID          		udt_std_id,
    @SysLastUpdatedDate     		varchar(30),
    @ApplicationCD          		udt_std_cd='APD'
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@AssignmentBeginDate))) = 0 SET @AssignmentBeginDate = NULL
    IF LEN(RTRIM(LTRIM(@AssignmentEndDate))) = 0 SET @AssignmentEndDate = NULL
    IF LEN(RTRIM(LTRIM(@EmailAddress))) = 0 SET @EmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@NameFirst))) = 0 SET @NameFirst = NULL
    IF LEN(RTRIM(LTRIM(@NameLast))) = 0 SET @NameLast = NULL
    IF LEN(RTRIM(LTRIM(@NameTitle))) = 0 SET @NameTitle = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@OperatingMondayStartTime))) = 0 SET @OperatingMondayStartTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingMondayEndTime))) = 0 SET @OperatingMondayEndTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayStartTime))) = 0 SET @OperatingTuesdayStartTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayEndTime))) = 0 SET @OperatingTuesdayEndTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayStartTime))) = 0 SET @OperatingWednesdayStartTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayEndTime))) = 0 SET @OperatingWednesdayEndTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingThursdayStartTime))) = 0 SET @OperatingThursdayStartTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingThursdayEndTime))) = 0 SET @OperatingThursdayEndTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingFridayStartTime))) = 0 SET @OperatingFridayStartTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingFridayEndTime))) = 0 SET @OperatingFridayEndTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayStartTime))) = 0 SET @OperatingSaturdayStartTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayEndTime))) = 0 SET @OperatingSaturdayEndTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingSundayStartTime))) = 0 SET @OperatingSundayStartTime = NULL 
    IF LEN(RTRIM(LTRIM(@OperatingSundayEndTime))) = 0 SET @OperatingSundayEndTime = NULL  
    IF LEN(RTRIM(LTRIM(@LicenseAssignStList))) = 0 SET @LicenseAssignStList = NULL
    IF LEN(RTRIM(LTRIM(@InsAssignWorkList))) = 0 SET @InsAssignWorkList = NULL
    IF LEN(RTRIM(LTRIM(@RoleList))) = 0 SET @RoleList = NULL
    IF LEN(RTRIM(LTRIM(@ProfileList))) = 0 SET @ProfileList = NULL
    IF LEN(RTRIM(LTRIM(@PermissionList))) = 0 SET @PermissionList = NULL
    IF LEN(RTRIM(LTRIM(@AssignmentPoolList))) = 0 SET @AssignmentPoolList = NULL

    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int    
    DECLARE @tupdated_date          AS datetime 
    DECLARE @now                    AS datetime
    DECLARE @tAssignmentBeginDate   AS datetime 
    DECLARE @tAssignmentEndDate     AS datetime 


    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspAdmUserUpdDetailWSXML'

    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)

    -- Validate the updated date parameter
    
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_user', @UserID

    if @@error <> 0
    BEGIN
        RETURN
    END


    -- Apply edits

    IF @OfficeID IS NOT NULL AND
       @OfficeID NOT IN (Select OfficeID FROM dbo.utb_office WHERE OfficeID = @OfficeID AND EnabledFlag = 1)
    BEGIN
        -- Invalid Office
   
        RAISERROR  ('101|%s|@OfficeID|%u', 16, 1, @ProcName, @OfficeID)
        RETURN
    END

    
    IF @SupervisorUserID IS NOT NULL
    BEGIN
        IF (@SupervisorUserID = 0) OR
           (@SupervisorUserID = @UserID) OR
           NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @SupervisorUserID)
        BEGIN
           -- Invalid Supervisor

            RAISERROR  ('101|%s|@SupervisorUserID|%u', 16, 1, @ProcName, @SupervisorUserID)
            RETURN
        END
        ELSE
        BEGIN
            IF (@SupervisorUserID IN (SELECT NewUserID FROM dbo.ufnUtilityOrgChartGetList( @UserID )))
            BEGIN
                -- Trying to assign a subordinate as a supervisor

                RAISERROR  ('1|Cannot assign a subordinate as the supervisor for this user.', 16, 1)
                RETURN
            END
        END
    END

    IF @SupervisorFlag IS NULL or @SupervisorFlag = 0 
    BEGIN
        IF ((SELECT COUNT(*) FROM dbo.utb_user WHERE SupervisorUserID = @UserID) > 0)
        BEGIN
            -- Trying to remove as supervisor when there are users reporting to him

            RAISERROR  ('1|Cannot uncheck supervisor flag. There are one or more users reporting.', 16, 1)
            RETURN
        END
    END


    IF @AssignmentEndDate IS NOT NULL
    BEGIN
        IF ISDATE(@AssignmentEndDate) = 1
        BEGIN
            -- Convert the value passed in into date format

            SET @tAssignmentEndDate = CAST(@AssignmentEndDate AS DATETIME)
        END
        ELSE
        BEGIN
            -- Invalid date value
    
            RAISERROR  ('1|The Assignment End Date is not valid.', 16, 1)
            RETURN
        END
    END
        
        
    IF @AssignmentBeginDate IS NOT NULL
    BEGIN
        IF (@AssignmentEndDate IS NOT NULL) AND (ISDATE(@AssignmentBeginDate) = 1)
        BEGIN
            -- Convert the value passed in into date format

            SET @tAssignmentBeginDate = CAST(@AssignmentBeginDate AS DATETIME)

            IF @tAssignmentBeginDate <= @tAssignmentEndDate
            BEGIN
                -- Invalid Begin Date is before End Date
    
                RAISERROR  ('1|The Assignment Begin Date cannot be before the Assignment End Date ', 16, 1)
                RETURN
            END
        END
        ELSE
        BEGIN
            -- Invalid date value
    
            RAISERROR  ('1|The Assignment Begin Date is not valid.', 16, 1)
            RETURN
        END
    END
    

    IF @NameFirst IS NULL
    BEGIN
        -- First Name Required
   
        RAISERROR  ('1|The User First Name is required.', 16, 1)
        RETURN
    END

    
    IF @NameLast IS NULL
    BEGIN
        -- Last Name Required
   
        RAISERROR  ('1|The User Last Name is required.', 16, 1)
        RETURN
    END


    IF @EmailAddress IS NULL
    BEGIN
        -- Email Address Required
   
        RAISERROR  ('1|The Email Address is required.', 16, 1)
        RETURN
    END


    IF @RoleList IS NULL
    BEGIN
        -- Role Required
   
        RAISERROR  ('1|At least 1 Role must be selected.', 16, 1)
        RETURN
    END


    -- Check to make sure a valid Sys Last User id was passed in

    IF  (@SysLastUserID IS NULL) OR
        (@SysLastUserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @SysLastUserID))
    BEGIN
        -- Invalid Sys last User ID
    
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END
    
    
   
    -- Declare and navigate cursor to get and populate Profile overrides.  The structure of the profile list is pairs of
    -- values.  Each pair corresponds to a Profile ID and an associated value.  An example Profile List string will be
    -- as follows:    1,ABC,4,50
    -- In the example above, the first profile ID id 1 and it's associated value is ABC.  The second Profile ID is 4 and
    -- it's associated value is 50.


    DECLARE @tmpProfile TABLE
    (
        ProfileID       int,
        Value           varchar(50)
    )

    DECLARE @ProfileID              int
    DECLARE @ProfileValue           varchar(50)   

    DECLARE csrProfile CURSOR FOR
        SELECT value FROM dbo.ufnUtilityParseString( @ProfileList, ',', 1 ) -- 1=trim spaces
     
    OPEN csrProfile

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    -- Get Profile ID (1st value of the pair)

    FETCH next
    FROM csrProfile
    INTO @ProfileID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    -- Get Profile Value (2nd value of the pair)

    FETCH next
    FROM csrProfile
    INTO @ProfileValue

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        -- Insert the value pait into another table more suited to contain ID/Value pairs
        
        INSERT INTO @tmpProfile VALUES (@ProfileID, @ProfileValue)

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('105|%s|@tmpProfile', 16, 1, @ProcName)
            RETURN
        END


        -- Fetch the next values and then repeat until the end of the list
        
        FETCH next
        FROM csrProfile
        INTO @ProfileID

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        FETCH next
        FROM csrProfile
        INTO @ProfileValue

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    
    END


    CLOSE csrProfile
    DEALLOCATE csrProfile

    
    -- Declare and navigate cursor to get and populate Permission overrides.  The structure of the permission list is as sets
    -- of 5 values.  Each set corresponds to a Permission ID and 4 associated values representing Create, Read, Update, and
    -- Delete flags.  An example Permission List string will be as follows:
    -- 1,1,1,0,1,4,0,1,1,1
    -- In the example above, the first Permission ID id 1 and it's associated CRUD values are 1, 1, 0, 1.  The second 
    -- Permission ID is 4 and it's associated CRUD values are 0, 1, 1, 1.


    DECLARE @tmpPermission TABLE
    (
        PermissionID    int,
        CreateFlag      bit,
        ReadFlag        bit,
        UpdateFlag      bit,
        DeleteFlag      bit
    )

    DECLARE @PermissionID   int
    DECLARE @CreateFlag     bit
    DECLARE @ReadFlag       bit
    DECLARE @UpdateFlag     bit
    DECLARE @DeleteFlag     bit

    DECLARE csrPermission CURSOR FOR
        SELECT value FROM dbo.ufnUtilityParseString( @PermissionList, ',', 1 ) -- 1=trim spaces
     
    OPEN csrPermission

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    -- Get Permission ID (1st value of the set)

    FETCH next
    FROM csrPermission
    INTO @PermissionID

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    -- Get CRUD Values (next 4 values)

    FETCH next
    FROM csrPermission
    INTO @CreateFlag

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrPermission
    INTO @ReadFlag

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrPermission
    INTO @UpdateFlag

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrPermission
    INTO @DeleteFlag

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        -- Insert the value sets into another table more suited to contain them
        
        INSERT INTO @tmpPermission VALUES (@PermissionID, @CreateFlag, @ReadFlag, @UpdateFlag, @DeleteFlag)
 
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('105|%s|@tmpPermission', 16, 1, @ProcName)
            RETURN
        END


        -- Fetch the next set of values and then repeat until the end of the list
        
        FETCH next
        FROM csrPermission
        INTO @PermissionID

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    

        
        FETCH next
        FROM csrPermission
        INTO @CreateFlag

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        FETCH next
        FROM csrPermission
        INTO @ReadFlag

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        FETCH next
        FROM csrPermission
        INTO @UpdateFlag
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        FETCH next
        FROM csrPermission
        INTO @DeleteFlag
    END


    CLOSE csrPermission
    DEALLOCATE csrPermission
    
    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Begin Update

    BEGIN TRANSACTION UserUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update the user record
    
    UPDATE  dbo.utb_user
    SET OfficeID                	= @OfficeID,
        SupervisorUserID        	= @SupervisorUserID,
        AssignmentBeginDate     	= @AssignmentBeginDate,
        AssignmentEndDate       	= @AssignmentEndDate,
        ClientUserID                = @ClientUserID,
        EmailAddress            	= @EmailAddress,
        FaxAreaCode             	= @FaxAreaCode,
        FaxExchangeNumber       	= @FaxExchangeNumber,
        FaxExtensionNumber      	= @FaxExtensionNumber,
        FaxUnitNumber           	= @FaxUnitNumber,
        NameFirst               	= @NameFirst,
        NameLast                	= @NameLast,
        NameTitle               	= @NameTitle,
        PhoneAreaCode           	= @PhoneAreaCode,
        PhoneExchangeNumber     	= @PhoneExchangeNumber,
        PhoneExtensionNumber    	= @PhoneExtensionNumber,
        PhoneUnitNumber         	= @PhoneUnitNumber,
        SupervisorFlag          	= @SupervisorFlag,
        OperatingMondayStartTime    = @OperatingMondayStartTime ,
        OperatingMondayEndTime      = @OperatingMondayEndTime,
        OperatingTuesdayStartTime   = @OperatingTuesdayStartTime,
        OperatingTuesdayEndTime     = @OperatingTuesdayEndTime,
        OperatingWednesdayStartTime = @OperatingWednesdayStartTime,
        OperatingWednesdayEndTime   = @OperatingWednesdayEndTime,
        OperatingThursdayStartTime  = @OperatingThursdayStartTime,
        OperatingThursdayEndTime    = @OperatingThursdayEndTime,
        OperatingFridayStartTime    = @OperatingFridayStartTime,
        OperatingFridayEndTime      = @OperatingFridayEndTime,
        OperatingSaturdayStartTime  = @OperatingSaturdayStartTime,
        OperatingSaturdayEndTime    = @OperatingSaturdayEndTime,
        OperatingSundayStartTime    = @OperatingSundayStartTime,
        OperatingSundayEndTime      = @OperatingSundayEndTime,
        SysLastUserID           	= @SysLastUserID,
        SysLastUpdatedDate      	= @now
    WHERE 
        UserID = @UserID AND
	    SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value

    IF @error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_user', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    
    -- Update License States for this user

    -- First remove all states that were saved before

    DELETE FROM dbo.utb_user_state
      WHERE UserID = @UserID


    -- Check error value

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|utb_user_state', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Now populate with values from License State List

    INSERT INTO dbo.utb_user_state
    (
        UserID,
        StateCode,
        LicenseFlag,
        AssignWorkFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @UserID,
            substring(value, 1, 2), -- State Code
            substring(value, 4, 1), -- Licensed Flag
            substring(value, 6, 1), -- Assign Work Flag
            @SysLastUserID,
            @now
      FROM  dbo.ufnUtilityParseString( @LicenseAssignStList, ';', 1 ) -- 1=trim spaces

      
    -- Check error value

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_user_state', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END       

    
    --------------------------------------------------------
    
    -- Update Insurance Companies for this user

    -- First remove all Companies that were saved before

    IF (@ApplicationCD='APD')
    BEGIN
      DELETE FROM dbo.utb_user_insurance
        WHERE UserID = @UserID
      
      -- Check error value

      IF @@ERROR <> 0
      BEGIN
          -- SQL Server Error

          RAISERROR  ('106|%s|utb_user_insurance', 16, 1, @ProcName)
          ROLLBACK TRANSACTION 
          RETURN
      END

    
      -- Now populate with values from Insurance Company List
    
      INSERT INTO dbo.utb_user_insurance
      (
          UserID,
          InsuranceCompanyID,
          SysLastUserID,
          SysLastUpdatedDate
      )
      SELECT  @UserID,
              value, -- Insurance Company ID
              @SysLastUserID,
              @now
        FROM  dbo.ufnUtilityParseString( @InsAssignWorkList, ';', 1 ) -- 1=trim spaces

      
      -- Check error value

      IF @@ERROR <> 0
      BEGIN
          -- SQL Server Error

          RAISERROR  ('105|%s|utb_user_insurance', 16, 1, @ProcName)
          ROLLBACK TRANSACTION 
          RETURN
      END  
    END  
    
    
    -------------------------------------------------------
    
    
    -- Update Roles for this user

    -- First remove all roles

    DELETE FROM dbo.utb_user_role
      WHERE UserID = @UserID


    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|utb_user_role', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Now populate with values from Role List

    INSERT INTO dbo.utb_user_role
    (
        UserID,
        RoleID,
        PrimaryRoleFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @UserID,
            value,
            CASE 
                WHEN value = (SELECT TOP 1 value FROM dbo.ufnUtilityParseString( @RoleList, ',', 1 )) THEN 1 -- 1 in function=trim spaces
                ELSE 0
            END,
            @SysLastUserID,
            @now
      FROM  dbo.ufnUtilityParseString( @RoleList, ',', 1 ) -- 1=trim spaces

      
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_user_role', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Update Profile Overrides for this user

    -- First remove all profile overrides

    DELETE FROM dbo.utb_user_profile
      WHERE UserID = @UserID


    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|utb_user_profile', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Now populate with values from Profile List

    INSERT INTO dbo.utb_user_profile
    (
        UserID,
        ProfileID,
        Value,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @UserID,
            ProfileID,
            Value,
            @SysLastUserID,
            @now
      FROM  @tmpProfile

      
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_user_profile', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Update Permission Overrides for this user

    -- First remove all permission overrides

    DELETE FROM dbo.utb_user_permission
      WHERE UserID = @UserID


    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|utb_user_permission', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Now populate with values from Permission List

    INSERT INTO dbo.utb_user_permission
    (
        UserID,
        PermissionID,
        CreateFlag,
        ReadFlag,
        UpdateFlag,
        DeleteFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @UserID,
            PermissionID,
            CreateFlag,
            ReadFlag,
            UpdateFlag,
            DeleteFlag,
            @SysLastUserID,
            @now
      FROM  @tmpPermission

      
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_user_permission', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Update Assignment Pool for this user

    -- 
    -- Temporary hack to allow ClaimPoint users saves without passing the AssignmentPoolList
    -- Only update the Assignment Pools if the @AssignmentPoolList is not null
    IF @AssignmentPoolList IS NOT NULL
    BEGIN

	    -- First remove all Assignment Pools
	
	    DELETE FROM dbo.utb_assignment_pool_user
	      WHERE UserID = @UserID
	
	
	    -- Check error value
	    
	    IF @@ERROR <> 0
	    BEGIN
	        -- SQL Server Error
	
	        RAISERROR  ('106|%s|utb_assignment_pool_user', 16, 1, @ProcName)
	        ROLLBACK TRANSACTION 
	        RETURN
	    END
	
	
	    -- Now populate with values from Assignment Pool list
	
	    INSERT INTO dbo.utb_assignment_pool_user
	    (
	        AssignmentPoolID,
	        UserID,
	        SysLastUserID,
	        SysLastUpdatedDate
	    )
	    SELECT  value,
	            @UserID,
	            @SysLastUserID,
	            @now
	      FROM  dbo.ufnUtilityParseString( @AssignmentPoolList, ',', 1 ) -- 1=trim spaces
	
	      
	    -- Check error value
	    
	    IF @@ERROR <> 0
	    BEGIN
	        -- SQL Server Error
	
	        RAISERROR  ('105|%s|utb_assignment_pool_user', 16, 1, @ProcName)
	        ROLLBACK TRANSACTION 
	        RETURN
	    END

    END


    COMMIT TRANSACTION UserUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Create XML Document to return updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- User Level
            NULL AS [User!2!UserID],
            NULL AS [User!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- User Level
            @UserID,
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
    FOR XML EXPLICIT      -- Comment for Client-side processing

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmUserUpdDetailWSXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmUserUpdDetailWSXML TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO