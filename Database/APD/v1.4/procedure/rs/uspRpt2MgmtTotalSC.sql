-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2MgmtTotalSC' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2MgmtTotalSC 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2MgmtTotalSC
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Stored proc that drives the data for Management Report totals by Service Channel
*
* PARAMETERS:  
* (I) @RptMonth             The starting month for the report
* (I) @RptYear              The year of the report

* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2MgmtTotalSC
    @RptMonth           int = NULL,
    @RptYear            int = NULL
AS
BEGIN

    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

    DECLARE @RptFromDate            DATETIME
    DECLARE @RptToDate              DATETIME
    DECLARE @tmpDate                DATETIME
    DECLARE @CurrentQuarter         tinyint
    DECLARE @CurrentMonthYrName     varchar(50)
    DECLARE @QuarterName            varchar(50)
    DECLARE @YTDName                varchar(50)
    DECLARE @12MonthName            varchar(50)
    DECLARE @ReportTitle            varchar(100)


    DECLARE @Debug      udt_std_flag
    DECLARE @now        udt_std_datetime
    DECLARE @DataWarehouseDate    udt_std_datetime

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2MgmtTotalSC'

    DECLARE @tmpReportDataMonth TABLE 
    (
        ReportTitle                 varchar(100) NOT NULL,
        Level1Group                 tinyInt      NOT NULL,
        Level1Display               varchar(100) NOT NULL,
        Level2Group                 tinyInt      NOT NULL,
        Level2Display               varchar(100) NOT NULL,
        LineItem                    varchar(50)  NOT NULL,
        InsuranceCompany            varchar(50)  NOT NULL,
        Total                       bigint       NULL,
        TotalEstimate               money        NULL,
        AvgEstimate                 money        NULL,
        AvgCycleTimeNewToClose      decimal(6,1) NULL
    )
    
    DECLARE @tmpLevel1ServiceChannel TABLE
    (
        Level1Group                 tinyInt      NOT NULL,
        LineItem                    varchar(50)  NOT NULL,
        InsuranceCompany            varchar(50)  NOT NULL
    )

    SET @Debug = 0

    -- Get Current timestamp
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    -- validate the input dates
    -- if the month was not specified or invalid, assume the current month
    IF @RptMonth IS NULL OR @RptMonth < 1 OR @RptMonth > 12
        SET @RptMonth = Month(@now)
     
    -- if the year was not specified or greater than current year or less than 2002, assume current year
    IF @RptYear IS NULL OR @RptYear > YEAR(@now) OR @RptYear < 2002
        SET @RptYear = Year(@now)
        
    -- Calculate the from and to date for the report
    -- set the to date for the report to the 1st of the month and year specifed.
    SET @RptToDate = convert(datetime, convert(varchar, @RptMonth) + '/1/' + convert(varchar, @RptYear))
    -- set the from date for the report 12 months prior (includes current month)
    SET @RptFromDate = DATEADD(mm, -11, @RptToDate)
    
    -- calculate the last day of the to date
    SET @tmpDate = DATEADD(mm, 1, @RptToDate)
    SET @RptToDate = DATEADD(dd, -1, @tmpDate)
    
    SELECT @CurrentQuarter = Quarter
    FROM utb_dtwh_dim_time
    WHERE MonthOfYear = Month(@RptToDate)
      AND YearValue = Year(@RptToDate)
    
    SET @ReportTitle = 'Totals by Service Channel'
    
    -- initialize the names for each level
    SET @CurrentMonthYrName = DateName(mm, @RptToDate) + ' ' + CONVERT(varchar(4), Year(@RptToDate))
    
    SET @QuarterName = CASE 
                        WHEN @CurrentQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                        WHEN @CurrentQuarter = 1 THEN '1st Quarter: January - ' + @CurrentMonthYrName
                        WHEN @CurrentQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                        WHEN @CurrentQuarter = 2 THEN '2nd Quarter: April - ' + @CurrentMonthYrName
                        WHEN @CurrentQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                        WHEN @CurrentQuarter = 3 THEN '3rd Quarter: July - ' + @CurrentMonthYrName
                        WHEN @CurrentQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                        WHEN @CurrentQuarter = 4 THEN '4th Quarter: October - ' + @CurrentMonthYrName
                      END

    
    SET @YTDName = 'Year To Date: January - ' + DateName(mm, @RptToDate) + ' ' + CONVERT(varchar(4), Year(@RptToDate))
    
    SET @12MonthName = '12 Months Preceding ' + DateName(mm, @RptToDate) + ' ' + CONVERT(varchar(4), Year(@RptToDate))
    
    IF @Debug = 1
    BEGIN
        PRINT '@RptMonth: ' + CONVERT(varchar, @RptMonth)
        PRINT '@RptYear: ' + CONVERT(varchar, @RptYear)
        PRINT '@RptFromDate: ' + CONVERT(varchar, @RptFromDate, 101)
        PRINT '@RptToDate: ' + CONVERT(varchar, @RptToDate, 101)
        PRINT '@CurrentQuarter: ' + CONVERT(varchar, @CurrentQuarter)
    END

    -- Current Month calculations
    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           1,
           @CurrentMonthYrName,
           1,
           'New Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL 
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtn on (fc.TimeIDNew = dtn.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtn.YearValue = Year(@RptToDate)
        and dtn.MonthOfYear = Month(@RptToDate)
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           1,
           @CurrentMonthYrName,
           2,
           'Cancelled Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL 
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtc on (fc.TimeIDCancelled = dtc.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtc.YearValue = Year(@RptToDate)
        and dtc.MonthOfYear = Month(@RptToDate)
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           1,
           @CurrentMonthYrName,
           3,
           'Voided Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL  
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtv on (fc.TimeIDVoided = dtv.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtv.YearValue = Year(@RptToDate)
        and dtv.MonthOfYear = Month(@RptToDate)
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           1,
           @CurrentMonthYrName,
           4,
           'Closed Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*) AS ClaimsClosed, 
           Round(Sum(fc.FinalEstimateGrossAmt), 2) AS TotalEstimateDollars, 
           Round(Avg(fc.FinalEstimateGrossAmt), 2) AS AvgEstimateDollars,
           Round(Avg(Convert(decimal(5,2), fc.CycleTimeNewToCloseCalDay)), 1) AS AverageCycleTime
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtc on (fc.TimeIDClosed = dtc.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtc.YearValue = Year(@RptToDate)
        and dtc.MonthOfYear = Month(@RptToDate)
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    -- Current Quarter calculations
    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           2,
           @QuarterName,
           1,
           'New Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL 
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtn on (fc.TimeIDNew = dtn.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtn.YearValue = Year(@RptToDate)
        and dtn.Quarter = @CurrentQuarter
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           2,
           @QuarterName,
           2,
           'Cancelled Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL 
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtc on (fc.TimeIDCancelled = dtc.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtc.YearValue = Year(@RptToDate)
        and dtc.Quarter = @CurrentQuarter
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           2,
           @QuarterName,
           3,
           'Voided Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL  
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtv on (fc.TimeIDVoided = dtv.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtv.YearValue = Year(@RptToDate)
        and dtv.Quarter = @CurrentQuarter
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           2,
           @QuarterName,
           4,
           'Closed Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*) AS ClaimsClosed, 
           Round(Sum(fc.FinalEstimateGrossAmt), 2) AS TotalEstimateDollars, 
           Round(Avg(fc.FinalEstimateGrossAmt), 2) AS AvgEstimateDollars,
           Round(Avg(Convert(decimal(5,2), fc.CycleTimeNewToCloseCalDay)), 1) AS AverageCycleTime
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtc on (fc.TimeIDClosed = dtc.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtc.YearValue = Year(@RptToDate)
        and dtc.Quarter = @CurrentQuarter
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    -- YTD Calculation
    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           3,
           @YTDName,
           1,
           'New Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL 
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtn on (fc.TimeIDNew = dtn.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtn.YearValue = Year(@RptToDate)
        and dtn.MonthOfYear <= Month(@RptToDate)
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           3,
           @YTDName,
           2,
           'Cancelled Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL 
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtc on (fc.TimeIDCancelled = dtc.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtc.YearValue = Year(@RptToDate)
        and dtc.MonthOfYear <= Month(@RptToDate)
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           3,
           @YTDName,
           3,
           'Voided Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL  
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtv on (fc.TimeIDVoided = dtv.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtv.YearValue = Year(@RptToDate)
        and dtv.MonthOfYear <= Month(@RptToDate)
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           3,
           @YTDName,
           4,
           'Closed Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*) AS ClaimsClosed, 
           Round(Sum(fc.FinalEstimateGrossAmt), 2) AS TotalEstimateDollars, 
           Round(Avg(fc.FinalEstimateGrossAmt), 2) AS AvgEstimateDollars,
           Round(Avg(Convert(decimal(5,2), fc.CycleTimeNewToCloseCalDay)), 1) AS AverageCycleTime
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtc on (fc.TimeIDClosed = dtc.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtc.YearValue = Year(@RptToDate)
        and dtc.MonthOfYear <= Month(@RptToDate)
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    -- 12 Month calculations
    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           4,
           @12MonthName,
           1,
           'New Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL 
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtn on (fc.TimeIDNew = dtn.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtn.DateValue >= @RptFromDate
        and dtn.DateValue <= @RptToDate
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           4,
           @12MonthName,
           2,
           'Cancelled Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL 
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtc on (fc.TimeIDCancelled = dtc.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtc.DateValue >= @RptFromDate
        and dtc.DateValue <= @RptToDate
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           4,
           @12MonthName,
           3,
           'Voided Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*),
           NULL, NULL, NULL  
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtv on (fc.TimeIDVoided = dtv.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtv.DateValue >= @RptFromDate
        and dtv.DateValue <= @RptToDate
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription

    INSERT INTO @tmpReportDataMonth
    Select @ReportTitle,
           4,
           @12MonthName,
           4,
           'Closed Claims',
           dsc.ServiceChannelDescription,
           dc.InsuranceCompanyName,
           Count(*) AS ClaimsClosed, 
           Round(Sum(fc.FinalEstimateGrossAmt), 2) AS TotalEstimateDollars, 
           Round(Avg(fc.FinalEstimateGrossAmt), 2) AS AvgEstimateDollars,
           Round(Avg(Convert(decimal(5,2), fc.CycleTimeNewToCloseCalDay)), 1) AS AverageCycleTime
      from dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      left join dbo.utb_dtwh_dim_time dtc on (fc.TimeIDClosed = dtc.TimeID)
      left join dbo.utb_dtwh_dim_customer dc on (fc.CustomerID = dc.CustomerID)
      where dtc.DateValue >= @RptFromDate
        and dtc.DateValue <= @RptToDate
        and dc.InsuranceCompanyID in (SELECT InsuranceCompanyID FROM dbo.utb_insurance where DemoFlag = 0)
		AND fc.EnabledFlag = 1
      GRoup by dsc.ServiceChannelDescription, dc.InsuranceCompanyName
      HAVING dsc.ServiceChannelDescription IS NOT NULL
      Order By dsc.ServiceChannelDescription
      
    INSERT INTO @tmpLevel1ServiceChannel
    SELECT distinct Level1Group,
           LineItem,
           InsuranceCompany
    FROM @tmpReportDataMonth
    
    SELECT @ReportTitle as ReportTitle,
           Level1Group,
           LineItem,
           InsuranceCompany as SubLineItem,
           (SELECT distinct Level1Display
            FROM @tmpReportDataMonth t1
            WHERE t1.Level1Group = t.Level1Group
              AND t1.LineItem = t.LineItem
              AND t1.InsuranceCompany = t.InsuranceCompany
           ) as Level1Display,
           (SELECT Total
            FROM @tmpReportDataMonth t1
            WHERE t1.Level1Group = t.Level1Group
              AND t1.LineItem = t.LineItem
              AND t1.InsuranceCompany = t.InsuranceCompany
              AND t1.Level2Display = 'New Claims') as NewClaims,
           (SELECT Total
            FROM @tmpReportDataMonth t1
            WHERE t1.Level1Group = t.Level1Group
              AND t1.LineItem = t.LineItem
              AND t1.InsuranceCompany = t.InsuranceCompany
              AND t1.Level2Display = 'Cancelled Claims') as CancelledClaims,
           (SELECT Total
            FROM @tmpReportDataMonth t1
            WHERE t1.Level1Group = t.Level1Group
              AND t1.LineItem = t.LineItem
              AND t1.InsuranceCompany = t.InsuranceCompany
              AND t1.Level2Display = 'Voided Claims') as VoidedClaims,
           (SELECT Total
            FROM @tmpReportDataMonth t1
            WHERE t1.Level1Group = t.Level1Group
              AND t1.LineItem = t.LineItem
              AND t1.InsuranceCompany = t.InsuranceCompany
              AND t1.Level2Display = 'Closed Claims') as ClosedClaims,
           (SELECT TotalEstimate
            FROM @tmpReportDataMonth t1
            WHERE t1.Level1Group = t.Level1Group
              AND t1.LineItem = t.LineItem
              AND t1.InsuranceCompany = t.InsuranceCompany
              AND t1.Level2Display = 'Closed Claims') as ClosedTotalEstimate,
           (SELECT AvgEstimate
            FROM @tmpReportDataMonth t1
            WHERE t1.Level1Group = t.Level1Group
              AND t1.LineItem = t.LineItem
              AND t1.InsuranceCompany = t.InsuranceCompany
              AND t1.Level2Display = 'Closed Claims') as ClosedAvgEstimate,
           (SELECT AvgCycleTimeNewToClose
            FROM @tmpReportDataMonth t1
            WHERE t1.Level1Group = t.Level1Group
              AND t1.LineItem = t.LineItem
              AND t1.InsuranceCompany = t.InsuranceCompany
              AND t1.Level2Display = 'Closed Claims') as ClosedAvgCT,
              @DataWareHouseDate as DataWareDate 
    FROM @tmpLevel1ServiceChannel t
           

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2MgmtTotalSC' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2MgmtTotalSC TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/