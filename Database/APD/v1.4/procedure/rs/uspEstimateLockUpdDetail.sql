-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateLockUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspEstimateLockUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspEstimateLockUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Updates the lock status of an estimate/supplement
*
* PARAMETERS:
* (I) @DocumentID           The DocumentID to update
* (I) @LockedFlag			The lock indicator on the estimate
* (I) @UserID               UserID performing the update
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspEstimateLockUpdDetail
    @DocumentID             udt_std_id_big,
    @ClaimAspectID          udt_std_id_big,
    @LockedFlag				udt_std_flag,
    @UserID                 udt_std_id
AS
BEGIN
    -- Initialize any empty string parameters
    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int

    DECLARE @now               AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspEstimateLockUpdDetail'


    -- Set Database options

    SET NOCOUNT ON


    -- Check to make sure a valid Document id was passed in

    IF  (@DocumentID IS NULL) OR
        (NOT EXISTS(SELECT DocumentID FROM dbo.utb_document WHERE DocumentID = @DocumentID))
    BEGIN
        -- Invalid Document ID

        RAISERROR('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
     RETURN
    END


    -- Check to make sure the document id belongs to the claim aspect id
    --Project:210474 APD Modified the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    IF  (NOT EXISTS(
                    SELECT ClaimAspectID 
                    FROM dbo.utb_claim_aspect_service_channel_document  cascd
                    INNER JOIN    utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID
                    WHERE DocumentID = @DocumentID 
                    AND ClaimAspectID = @ClaimAspectID
                   )
        )
    --Project:210474 APD Modified the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
    BEGIN
        -- Invalid ClaimAspect ID for the Document id

        RAISERROR('%s|Claim Aspect ID invalid for Document', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END

    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Begin Update

    BEGIN TRANSACTION DocumentEstimateUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Perform an update

    UPDATE  dbo.utb_document
       SET  EstimateLockedFlag = @LockedFlag
    WHERE DocumentID = @DocumentID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value

    IF @error <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    COMMIT TRANSACTION DocumentEstimateUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    SELECT  1                   AS tag,
            NULL                AS parent,
            @DocumentID         AS [Root!1!DocumentID],
            @Now                AS [Root!1!SysLastUpdatedDate]

--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspEstimateLockUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspEstimateLockUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
