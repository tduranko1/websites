-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptClientFeeEffective' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptClientFeeEffective 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspRptClientFeeEffective
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Report of fees beginning and/or ending in the indicated month
*
* PARAMETERS:  
* (I) @Month              
* (I) @Year               

*
* RESULT SET:
*   Report of fees beginning and/or ending in the indicated month
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptClientFeeEffective

  @RptMonth             varchar(2),
  @RptYear              varchar(4),  
  @InsuranceCompanyID   int = null
  
AS
BEGIN


  DECLARE @ProcName     varchar(30)       -- Used for raise error stmts 
  DECLARE @BeginDate    datetime
  DECLARE @EndDate      datetime
  
  DECLARE @tmpFees TABLE (Name                  varchar(50)     NOT NULL,
                          AppliesTo             varchar(50)     NOT NULL,
                          Category              varchar(50)     NOT NULL,
                          Description           varchar(50)     NOT NULL,
                          EffectiveStartDate    datetime            NULL,
                          EffectiveEndDate      datetime            NULL,
                          FeeAmount             money           NOT NULL,
                          FeeInstructions       varchar(250)        NULL,
                          Invoiceable           varchar(3)      NOT NULL,
                          InvoiceDescription    varchar(50)         NULL,
                          ChangedBy             varchar(101)    NOT NULL,
                          ChangedDate           datetime        NOT NULL,
                          sOrder                tinyint         NOT NULL)   
                          
                          
  
  SET @ProcName = 'uspRptClientFeeEffective'
  SET @BeginDate = CONVERT(datetime, (@RptMonth + '/01/' + @RptYear))
  SET @EndDate = DATEADD(mm, 1, @BeginDate)
  

  SET NOCOUNT ON
  

  INSERT INTO @tmpFees
  SELECT i.Name,
         (SELECT name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'AppliesToCD') WHERE code = cf.AppliesToCD) AppliesTo,
         (SELECT name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'CategoryCD') WHERE code = cf.CategoryCD) Category,
         cf.Description,
         EffectiveStartDate,
         EffectiveEndDate,
         FeeAmount,
         FeeInstructions,
         (CASE cf.ItemizeFlag
            WHEN 1 THEN 'YES'
            ELSE 'NO'
          END) Invoiceable,
         InvoiceDescription,
         (SELECT NameFirst + ' ' + NameLast FROM dbo.utb_user WHERE UserID = cf.SysLastUserID) ChangedBy,
         cf.SysLastUpdatedDate,
         1         
  FROM dbo.utb_client_fee cf INNER JOIN dbo.utb_insurance i  ON cf.InsuranceCompanyID = i.InsuranceCompanyID
                             --INNER JOIN dbo.utb_audit_log al ON cf.ClientFeeID = al.KeyID
  WHERE ((@InsuranceCompanyID IS NULL) OR (cf.InsuranceCompanyID = @InsuranceCompanyID))
    AND cf.EnabledFlag = 1
    AND cf.EffectiveStartDate >= @BeginDate AND cf.EffectiveStartDate < @EndDate         
    --AND (upper(al.LogComment) LIKE '%EFFECTIVE START DATE CHANGED%' OR upper(al.LogComment) LIKE '%EFFECTIVE FROM DATE CHANGED%')
    
  

  IF @@ERROR <> 0
  BEGIN
     -- SQL Server Error
  
      RAISERROR  ('105|%s|@tmpFees', 16, 1, @ProcName)
      RETURN
  END
  
  
  
  INSERT INTO @tmpFees
  SELECT i.Name,
         (SELECT name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'AppliesToCD') WHERE code = cf.AppliesToCD) AppliesTo,
         (SELECT name FROM dbo.ufnUtilityGetReferenceCodes('utb_client_fee', 'CategoryCD') WHERE code = cf.CategoryCD) Category,
         cf.Description,
         EffectiveStartDate,
         EffectiveEndDate,
         FeeAmount,
         FeeInstructions,
         (CASE cf.ItemizeFlag
            WHEN 1 THEN 'YES'
            ELSE 'NO'
          END) Invoiceable,
         InvoiceDescription,
         (SELECT NameFirst + ' ' + NameLast FROM dbo.utb_user WHERE UserID = cf.SysLastUserID) ChangedBy,
         cf.SysLastUpdatedDate,
         2        
  FROM dbo.utb_client_fee cf INNER JOIN dbo.utb_insurance i  ON cf.InsuranceCompanyID = i.InsuranceCompanyID
                             --INNER JOIN dbo.utb_audit_log al ON cf.ClientFeeID = al.KeyID
  WHERE ((@InsuranceCompanyID IS NULL) OR (cf.InsuranceCompanyID = @InsuranceCompanyID))
    AND cf.EnabledFlag = 1 
    AND cf.EffectiveEndDate >= @BeginDate AND cf.EffectiveEndDate < @EndDate
    --AND (upper(al.LogComment) LIKE '%EFFECTIVE END DATE CHANGED%' OR upper(al.LogComment) LIKE '%EFFECTIVE TO DATE CHANGED%')
  

  IF @@ERROR <> 0
  BEGIN
     -- SQL Server Error
  
      RAISERROR  ('105|%s|@tmpFees', 16, 1, @ProcName)
      RETURN
  END
  
   
  
  SELECT Name, AppliesTo, Category, Description, EffectiveStartDate, EffectiveEndDate, FeeAmount,         
         FeeInstructions, Invoiceable, InvoiceDescription, ChangedBy, MAX(ChangedDate) ChangedDate, sOrder,
         DateName(mm, @BeginDate) + ' ' + convert(char(4), DATEPART(yyyy, @BeginDate)) RptDate
  FROM  @tmpFees
  GROUP BY  Name, AppliesTo, Category, Description, EffectiveStartDate, EffectiveEndDate, FeeAmount,         
         FeeInstructions, Invoiceable, InvoiceDescription, ChangedBy, sOrder
  ORDER BY sOrder
  
  
  IF @@ERROR <> 0
  BEGIN
     -- SQL Server Error
  
      RAISERROR  ('99|%s', 16, 1, @ProcName)
      RETURN
  END
   
  

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptClientFeeEffective' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptClientFeeEffective TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/