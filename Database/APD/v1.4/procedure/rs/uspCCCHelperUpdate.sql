-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCCCHelperUpdate' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCCCHelperUpdate 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspCCCHelperUpdate
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Updates claim information from CCC helper 
*
* PARAMETERS:  

*
* RESULT SET:
*   
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure



CREATE PROCEDURE dbo.uspCCCHelperUpdate
	-- Add the parameters for the stored procedure here
	@LynxID			udt_std_id_big = Null,
	@VehNo			udt_std_id_big = Null,
	--@Estimate		udt_std_id_big = Null,
	@SeqNo			udt_std_id_big = Null,
	@Note			udt_std_desc_huge,
	@LogonID         varchar(150)
AS
BEGIN
	--SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- Declare variables
	SET NOCOUNT ON;

    DECLARE @UserID               AS udt_std_id_big
	DECLARE @TaskID				  AS udt_std_id_big
	DECLARE	@ClaimAspectServiceChannelID	AS udt_std_id_big
	DECLARE	@ClaimAspectID	AS udt_std_id_big
	DECLARE @UserTaskDescription AS VARCHAR(200)
	DECLARE	@NoteTypeID		As udt_std_id_big
	DECLARE	@PrivateFlag		As udt_std_id_big
	DECLARE @StatusID		As udt_std_id_big
	DECLARE	@AlarmDate	DATETIME

	DECLARE @ProcName               varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspCCCHelperUpdate'
	IF  (@LynxID IS NULL) OR
            (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
        BEGIN
            -- Invalid Lynx ID
    
            RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
            RETURN
        END

	-- Throw an event (usp_workflow_notify_event)
	-- EXEC [dbo].[uspWorkflowNotifyEvent] 71,@UserID 

	-- select * from utb_claim where  lynxid = 115500
	-- select * from utb_claim_aspect where  lynxid = 115500 and ClaimaspecttypeID = 9 

	SELECT @ClaimAspectID = claimaspectid FROM utb_claim_aspect WHERE  LynxId = @LynxID and ClaimaspecttypeID = 9

	IF  (@ClaimAspectID IS NULL)
        BEGIN
            -- Invalid ClaimAspectID
    
            RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
            RETURN
        END

	SELECT  @ClaimAspectServiceChannelID = ClaimAspectServiceChannelid FROM utb_Claim_Aspect_Service_Channel 
	WHERE	ClaimAspectId = @ClaimAspectID
	
	SET @TaskID = 73
	SET	@AlarmDate = getdate()

	SELECT	@UserID = UserID	
	FROM	utb_user_application
	WHERE	LogonId = @LogonID
	  AND ApplicationID = 1 -- APD

	EXEC dbo.uspDiaryInsDetail @ClaimAspectServiceChannelID,@TaskID, @AlarmDate , @Note ,NULL,NULL,0, @UserID

    -- call the uspDiaryInsDetail for creating new  task   ***** 
	/* strData = "ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID;
               strData += "&TaskID=73";
               strData += "&AlarmDate=" + DateTime.Now.AddHours(1);
               strData += "&UserTaskDescription=" + strDescription;
               strData += "&UserID=" + strUserID;  

		select * from utb_claim where  lynxid = 115500
		select * from utb_claim_aspect where lynxid = 115500   */

	-- Add notes to claim. 
	SET @NoteTypeID = 1
	SET @PrivateFlag = 1
	
	SELECT @StatusID = StatusId FROM dbo.utb_claim_aspect_status WHERE ClaimAspectId = @ClaimAspectID
	AND StatusTypeCD = 'SC' and ServiceChannelCD = 'DA'

	EXEC dbo.uspNoteInsDetail @ClaimAspectID, @NoteTypeID, @StatusID, @Note, @UserID, @PrivateFlag, @ClaimAspectServiceChannelID



	-- dbo.uspNoteInsDetail
	/*strData = "ClaimAspectID=" + strClaimAspectID;
               strData += "&NoteTypeID=1";
               strData += "&StatusID=" + strStatusID;
               strData += "&Note=" + strDescription;
               strData += "&UserID=" + strUserID;
               strData += "&PrivateFlag=1";
               strData += "&ClaimAspectServiceChannelID=" + strClaimAspectServiceChannelID;*/

	/*UPDATE	utb_claim
	SET		Remarks = @Note
	WHERE	lynxid = @LynxID*/
			
	-- 3. Get the User id for the logon id from utb_user_application table and assign the utb_claim_aspect.SupportUserId (for ClaimAspectTypeID = 9) to this user.
	
	/*select * from  utb_user_application , select * from  utb_application
		where logonid like '%kven%'*/

	UPDATE	utb_claim_aspect
	SET		SupportUserId = @UserID, 
			SysLastUpdatedDate = getdate()
	WHERE	Lynxid = @LynxID AND ClaimAspectTypeID = 9

	--select supportuserid,ClaimAspectTypeID,* from utb_claim_aspect where ClaimAspectTypeID = 9

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCCCHelperUpdate' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCCCHelperUpdate TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
