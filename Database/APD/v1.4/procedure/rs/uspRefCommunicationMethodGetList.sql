-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefCommunicationMethodGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefCommunicationMethodGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefCommunicationMethodGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Retrieve all Communication Methods
*
* PARAMETERS:  
* None.
*
* RESULT SET:
* CommunicationMethodID         Communication Method unique identity
* DisplayOrder                  Communication Method display order
* EnabledFlag                   Communication Method active/inactive flag
* Name                          Communication Method name
* SysMaintainedFlag             Communication Method system maintained flag
* SysLastUserID                 Communication Method last user
* SysLastUpdatedDate            Communication Method last update date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefCommunicationMethodGetList
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspRefCommunicationMethodGetList'


    SELECT 
        CommunicationMethodID,
        DisplayOrder,
        EnabledFlag,
        Name,
        SysMaintainedFlag,
        SysLastUserID,
        dbo.ufnUtilityGetDateString(SysLastUpdatedDate) AS SysLastUpdatedDate
    FROM dbo.utb_communication_method
    ORDER BY DisplayOrder

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefCommunicationMethodGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefCommunicationMethodGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/