BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPreviewBillingRecordsByBillingSessionID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPreviewBillingRecordsByBillingSessionID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspPreviewBillingRecordsByBillingSessionID
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspPreviewBillingRecordsByBillingSessionID
	@BillingSessionID		varchar(50),
	@StatementEndDate		udt_std_datetime
AS
BEGIN

	select	tip.InvoiceID,
			tip.BillingSessionID BillingHistoryID,
			tip.Amount,
			tip.BillingEntryDate,
			tip.CarrierClaimRep,			
			tip.ClaimantName,
			tip.ClaimNumber,
			tip.ClientID,
			tip.ClientName,
			tip.CreateDate,
			'Preview' DispatchNumber,
			tip.Description,
			tip.InsuredName,
			tip.LossDate,
			tip.LossState,
			tip.LynxID,
			tip.OfficeID,
			tip.OfficeName,
			tip.PolicyNumber,
			@StatementEndDate StatementEndingDate
	from utb_temp_invoice_prep tip
	where tip.BillingSessionID = @BillingSessionID
	order by billingentrydate


end

go


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPreviewBillingRecordsByBillingSessionID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspPreviewBillingRecordsByBillingSessionID TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/		  