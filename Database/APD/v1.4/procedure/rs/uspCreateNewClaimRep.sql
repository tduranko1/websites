-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewClaimRep' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateNewClaimRep 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateNewClaimRep
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client rep by inserting a new record into the 
*				utb_user, utb_user_application and utb_user_role table 
*
* PARAMETERS:  
*
*	@vEmailAddress
*	, @vNameFirst
*	, @vNameLast
*	, @vPhoneAreaCode
*	, @vPhoneExchangeNumber
*	, @vPhoneExtensionNumber
*	, @vPhoneUnitNumber
*
* RESULT SET:
*			Identity of new inserted row
*
* Version:  1.0 - TVD - Initial development
*           1.1 - TVD - 23Apr2013 - Added code to assign user to the 1st office in the list
*           1.2 - TVD - 28Apr2015 - Updated to correct issue with AssignmentBeginDate and AssignmentEndDate
*           1.3 - TVD - 20Sep2016 - Added date created date
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateNewClaimRep
	@vEmailAddress VARCHAR(50)
	, @vNameFirst VARCHAR(50)
	, @vNameLast VARCHAR(50)
	, @vPhoneAreaCode VARCHAR(3)
	, @vPhoneExchangeNumber VARCHAR(3)
	, @vPhoneExtensionNumber VARCHAR(5)
	, @vPhoneUnitNumber VARCHAR(4)
	, @iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @dtNow AS DATETIME 
    DECLARE @ProcName AS VARCHAR(50)
    DECLARE @iUserID AS INT
    DECLARE @iApplicationID AS INT
    DECLARE @iRoleID AS INT
    DECLARE @iOfficeID AS INT
    DECLARE @vMsg AS VARCHAR(150)
    DECLARE @debug AS udt_std_flag

    SET @debug = 0
    SET @iOfficeID = 0

    SET @ProcName = 'uspCreateNewClaimRep'

    -- Set Database options
    SET NOCOUNT ON
	SET @dtNow = CURRENT_TIMESTAMP

	-- Get the first office in the list for this insurance company 
	-- and assign this user to it.
    IF  (@iInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = @iInsuranceCompanyID))
    BEGIN
        -- Invalid InsuranceCompanyID or no Offices exist
        SET @vMsg = 'Invalid InsuranceCompanyID or no Offices exist for insurance company: ' + @iInsuranceCompanyID
        RAISERROR('100|%s|OfficeID|%s', 16, 1, @ProcName, @vMsg)
        RETURN
    END
    ELSE
    BEGIN
		SELECT TOP 1 @iOfficeID=OfficeID FROM utb_office WHERE InsuranceCompanyID = @iInsuranceCompanyID
    END
	

	-- Check to make sure the user email address doesn't already exist
    IF  (@vEmailAddress IS NULL) 
        OR (EXISTS(SELECT UserID FROM utb_user WHERE EmailAddress = @vEmailAddress))
    BEGIN
        -- Invalid EmailAddress already exists
        SET @vMsg = @vEmailAddress + ' already exists'
        RAISERROR('101|%s|@vEmailAddress|%s', 16, 1, @ProcName, @vMsg)
        RETURN
    END

	-- Check to make sure the required data was passed
    IF  (@vNameFirst IS NULL) 
        OR (@vNameLast IS NULL)
        OR (@vPhoneAreaCode IS NULL)
        OR (@vPhoneExchangeNumber IS NULL)
        OR (@vPhoneUnitNumber IS NULL)
    BEGIN
        -- Invalid Parameters
        SET @vMsg = 'CarrierRepNameFirst, CarrierRepNameLast, CarrierRepPhoneAreaCode, CarrierRepPhoneExchangeNumber and CarrierRepPhoneUnitNumber are required fields'
        RAISERROR('102|%s|Parameters|%s', 16, 1, @ProcName, @vMsg)
        RETURN
    END

	-- Debug information
    IF @Debug = 1
    BEGIN
        Print ''
        Print 'Check if EmailAddress already exists:'
        Print '@vEmailAddress=' + @vEmailAddress
	    IF EXISTS(SELECT UserID FROM utb_user WHERE EmailAddress = @vEmailAddress)
		BEGIN
			Print 'Email Address already exists (BAD)...'
		END
		ELSE
		BEGIN
			Print 'Email Address does not exist (GOOD)...'
		END
    END

	BEGIN TRANSACTION CreateUser
	
	-- Insert new user into utb_user table
	INSERT INTO 
		utb_user
	VALUES (	
	  @iOfficeID
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,@vEmailAddress
      ,1
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,@vNameFirst
      ,@vNameLast
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,NULL
      ,@vPhoneAreaCode
      ,@vPhoneExchangeNumber
      ,@vPhoneExtensionNumber
      ,@vPhoneUnitNumber
      ,0
      ,0
      ,@dtNow
      ,NULL
      ,NULL
	  , CURRENT_TIMESTAMP
	)

	-- Debug information
    IF @Debug = 1
    BEGIN
        Print ''
        Print 'Check if new user got created in utb_user:'
        Print '@vEmailAddress=' + @vEmailAddress
	    IF EXISTS(SELECT UserID FROM utb_user WHERE EmailAddress = @vEmailAddress)
		BEGIN
			Print 'User was created successfully (GOOD)...'
		END
		ELSE
		BEGIN
			Print 'User did not get created (BAD)...'
		END
    END

	-- Confirm and get new Userid
    IF EXISTS(SELECT UserID FROM utb_user WHERE EmailAddress = @vEmailAddress)
    BEGIN
		SELECT @iUserID = UserID FROM utb_user WHERE EmailAddress = @vEmailAddress
    END
    ELSE
    BEGIN
        -- Invalid UserID
        SET @vMsg = @vEmailAddress + ' userid failed to create'
        RAISERROR('103|%s|@vEmailAddress|%s', 16, 1, @ProcName, @vMsg)
        RETURN
    END

	-- Get ApplicationID for Claimpoint
    SELECT @iApplicationID = ApplicationID FROM utb_application WHERE [Name] = 'ClaimPoint'	
    
	-- Insert new user into utb_user_application
	INSERT INTO 
		utb_user_application
	VALUES (	
	  @iUserID
      ,@iApplicationID
      ,@dtNow
      ,NULL
      ,@vEmailAddress
      ,NULL
      ,NULL
      ,0
      ,@dtNow
	)

	-- Debug information
    IF @Debug = 1
    BEGIN
        Print ''
        Print 'Check if new user got created in utb_user_application:'
        Print '@vEmailAddress=' + @vEmailAddress
	    IF EXISTS(SELECT UserID FROM utb_user_application WHERE UserID = @iUserID)
		BEGIN
			Print 'User was created successfully (GOOD)...'
		END
		ELSE
		BEGIN
			Print 'User did not get created (BAD)...'
		END
    END

	-- Confirm new user applications exists
    IF NOT EXISTS(SELECT UserID FROM utb_user_application WHERE UserID = @iUserID)
    BEGIN
        -- Invalid UserID
        SET @vMsg = CONVERT(VARCHAR(20),@iUserID) + ' user application create failed'
        RAISERROR('104|%s|@iUserID|%s', 16, 1, @ProcName, @vMsg)
        RETURN
    END
	
	-- Get RoleID for Claims Representative
	SELECT @iRoleID = RoleID FROM utb_role WHERE [Name] = 'Claims Representative'

	-- Insert new user into utb_user_role
	INSERT INTO 
		utb_user_role
	VALUES (	
	  @iUserID
      ,@iRoleID
      ,1
      ,0
      ,@dtNow
	)

	-- Debug information
    IF @Debug = 1
    BEGIN
        Print ''
        Print 'Check if new user got created in utb_user_role:'
        Print '@vEmailAddress=' + @vEmailAddress
	    IF EXISTS(SELECT UserID FROM utb_user_role WHERE UserID = @iUserID)
		BEGIN
			Print 'User was created successfully (GOOD)...'
		END
		ELSE
		BEGIN
			Print 'User did not get created (BAD)...'
		END
    END

	-- Confirm new role added
    IF EXISTS(SELECT RoleID FROM utb_user_role WHERE UserID = @iUserID)
	BEGIN
		COMMIT TRANSACTION CreateUser
		SELECT @iUserID RetCode
	END 
	ELSE
    BEGIN
		ROLLBACK TRANSACTION CreateUser
		SELECT 0 RetCode
    END

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewClaimRep' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateNewClaimRep TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateNewClaimRep TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/