-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefPersonnelTypeGetDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefPersonnelTypeGetDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefPersonnelTypeGetDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Retrieve a specific Personnel Type
*
* PARAMETERS:  
* (I) @PersonnelTypeID          Personnel Type unique identity
*
* RESULT SET:
* PersonnelTypeID               Personnel Type unique identity
* DisplayOrder                  Personnel Type display order
* EnabledFlag                   Personnel Type active/inactive flag
* Name                          Personnel Type name
* SysMaintainedFlag             Personnel Type system maintained flag
* SysLastUserID                 Personnel Type last user
* SysLastUpdatedDate            Personnel Type last update date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefPersonnelTypeGetDetail
(
	@PersonnelTypeID                    	udt_std_int_tiny
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspRefPersonnelTypeGetDetail'


    SELECT 
        PersonnelTypeID,
        DisplayOrder,
        EnabledFlag,
        Name,
        SysMaintainedFlag,
        SysLastUserID,
        dbo.ufnUtilityGetDateString(SysLastUpdatedDate) AS SysLastUpdatedDate
    FROM dbo.utb_personnel_type
    WHERE
        PersonnelTypeID = @PersonnelTypeID
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefPersonnelTypeGetDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefPersonnelTypeGetDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/