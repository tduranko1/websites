-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerSvcInsertJobDetails' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspPartnerSvcInsertJobDetails 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspPartnerSvcInsertJobDetails
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Inserts the job status details for a specific file being processed by the 
*				PartnerService.
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspPartnerSvcInsertJobDetails]
    @vJobID AS VARCHAR(255)
    , @iInscCompID AS INT
    , @iLynxID AS INT
    , @vMethod AS VARCHAR(10)
    , @vClaimNumber AS VARCHAR(30)
    , @vFromPartnerID AS VARCHAR(25)
    , @vToPartnerID AS VARCHAR(25)
	, @vIODirection AS VARCHAR(1)
	, @vJobXSLFile AS VARCHAR(100)
	, @vResponseXSLFile AS VARCHAR(100)
    , @vJobStatus AS VARCHAR(25)
    , @vJobStatusDetails AS VARCHAR(255)
    , @iJobPriority AS INT
    , @bResponseRequired AS BIT
    , @xJobXML AS XML
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)

    SET @ProcName = 'uspPartnerSvcInsertJobDetails'
    
    SET @ApplicationCD = 'APDPartner'

    -- Validation
    -- None
    
    -- Begin UPDATE
    BEGIN TRAN utrInsertPartnerData
		INSERT INTO
			utb_partnersvc_jobs
			(
				JobID
				, InscCompID
				, LynxID
				, Method
				, ClaimNumber
				, FromPartnerID
				, ToPartnerID
				, IODirection
				, JobXSLFile
				, ResponseXSLFile
				, JobStatus
				, JobStatusDetails
				, JobPriority
				, JobCreatedDate
				, ResponseRequired
				, EnabledFlag
				, JobXML
				, SysLastUpdatedDate
			)
		VALUES
			(
				@vJobID
				, @iInscCompID
				, @iLynxID
				, @vMethod
				, @vClaimNumber
				, @vFromPartnerID
				, @vToPartnerID
				, @vIODirection
				, @vJobXSLFile
				, @vResponseXSLFile
				, @vJobStatus
				, @vJobStatusDetails
				, @iJobPriority
				, CURRENT_TIMESTAMP
				, @bResponseRequired
				, 1
				, @xJobXML
				, CURRENT_TIMESTAMP
			)
			
		IF @@ERROR <> 0
		BEGIN
			-- Update Failure
			ROLLBACK TRANSACTION
			RAISERROR('%s: (Partner Job Processing) Error inserting the Job Status.', 16, 1, @ProcName)
			RETURN 'ERROR'
		END
		
		COMMIT TRANSACTION utrInsertPartnerData
		--rollback transaction utrInsertPartnerData

		IF @@ERROR <> 0
		BEGIN
			-- SQL Server Error
	       
			RAISERROR('%s: SQL Server Error during Commit', 16, 1, @ProcName)
			RETURN 'ERROR'
		END                                     

	SELECT 'Inserted'
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspPartnerSvcInsertJobDetails' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspPartnerSvcInsertJobDetails TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/