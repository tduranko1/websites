-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefNoteTypeUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefNoteTypeUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefNoteTypeUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Update a specific Note Type
*
* PARAMETERS:  
* (I) @NoteTypeID               Note Type unique identity
* (I) @DisplayOrder             Note Type display order
* (I) @EnabledFlag              Note Type active/inactive flag
* (I) @Name                     Note Type name
* (I) @SysLastUserID            Note Type user performing action
* (I) @SysLastUpdatedDate       Note Type last update date as string (VARCHAR(30))
*
* RESULT SET:
* NoteTypeID     The updated Note Type unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefNoteTypeUpdDetail
(
	@NoteTypeID               udt_std_int_tiny,
	@DisplayOrder             udt_std_int_tiny,
	@EnabledFlag              udt_std_flag,
	@Name                     udt_std_name,
	@SysLastUserID            udt_std_id,
  @SysLastUpdatedDate       VARCHAR(30),
  @ApplicationCD            udt_std_cd = 'APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @tupdated_date     AS DATETIME 
    DECLARE @temp_updated_date AS DATETIME 

    DECLARE @SysMaintainedFlag AS udt_sys_maintained_flag

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now               AS datetime

    SET @ProcName = 'uspRefNoteTypeUpdDetail'
    SET @now = CURRENT_TIMESTAMP

    
    -- Validate the updated date parameter
    
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_note_type', @NoteTypeID

    if @@error <> 0
    BEGIN
        RETURN
    END
   
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END


    IF @SysMaintainedFlag = 1
    BEGIN
        -- raise a error if the user has modified the data other than the display order
        -- for the System Maintained records.
        IF NOT EXISTS (SELECT NoteTypeID 
                            FROM utb_note_type
                            WHERE NoteTypeID = @NoteTypeID
                                AND EnabledFlag = @EnabledFlag
                                AND Name = @Name)
        BEGIN
            RAISERROR  ('1|Database record may not be modified', 16, 1, @ProcName)
            RETURN
        END
    END 

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT u.UserID 
                       FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                           INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
                       WHERE u.UserID = @SysLastUserID
                         AND @SysLastUserID <> 0
                         AND ua.AccessBeginDate IS NOT NULL
                         AND ua.AccessBeginDate <= CURRENT_TIMESTAMP
                         AND ua.AccessEndDate IS NULL
                         AND a.Code = @ApplicationCD)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    BEGIN TRANSACTION RefNoteTypeUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    UPDATE dbo.utb_note_type
       SET 
		DisplayOrder              = @DisplayOrder,
		EnabledFlag               = @EnabledFlag,
		Name                      = @Name,
		SysLastUserID             = @SysLastUserID,
		SysLastUpdatedDate        = @now
     WHERE 
		NoteTypeID = @NoteTypeID AND
		SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_note_type', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    COMMIT TRANSACTION RefNoteTypeUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Create XML Document to return new updated date time and updated vehicle involved list
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- New Involved Level
            NULL AS [NoteType!2!NoteTypeID],
            NULL AS [NoteType!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- New Involved Level
            @NoteTypeID,
            dbo.ufnUtilityGetDateString( @now )

    ORDER BY tag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefNoteTypeUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefNoteTypeUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/