-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspClaimNumberUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimNumberUpdDetail
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimNumberUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       John Carpenter
* FUNCTION:     Updates the client claim number of a claim.
*
* PARAMETERS:
* (I) @LynxID................Lynx ID for the claim to be updated
* (I) @ClientClaimNumber.....The new client claim number.
*
* RESULT SET:
*       rowcount
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspClaimNumberUpdDetail
    @LynxID                 udt_std_int_big,
    @ClientClaimNumber      udt_cov_claim_number
AS
BEGIN
    -- Initialize any empty string parameters

    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int

    DECLARE @ClientClaimNumberSquished AS udt_cov_claim_number

    DECLARE @now               AS datetime
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspClaimNumberUpdDetail'

    -- SET NOCOUNT to ON and no longer display the count message or warnings

    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

	--*******************************************************************
	--Make sure that all the codes and IDs that are being sent are valid.
	--*******************************************************************

    IF NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID)
    BEGIN
        -- Invalid Lynx ID
        RAISERROR('10|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END

    IF (@ClientClaimNumber IS NULL)
    BEGIN
        -- Invalid Client Claim Number
        RAISERROR('101|%s|@ClientClaimNumber|%u', 16, 1, @ProcName, @ClientClaimNumber)
        RETURN
    END

    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP
	SET @rowcount = 0

    SET @ClientClaimNumberSquished = dbo.ufnUtilitySquishString(@ClientClaimNumber, 1, 1, 0, NULL)

    UPDATE  utb_claim
    SET     ClientClaimNumber = @ClientClaimNumber,
            ClientClaimNumberSquished = @ClientClaimNumberSquished
    WHERE   LynxID = @LynxID

    IF @@ERROR <> 0
    BEGIN
       -- Problem updating a row into the table.
        RAISERROR('103|%s|utb_claim', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspClaimNumberUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimNumberUpdDetail TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
