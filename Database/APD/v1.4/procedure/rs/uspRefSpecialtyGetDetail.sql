-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefSpecialtyGetDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefSpecialtyGetDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefSpecialtyGetDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Retrieve a specific Specialty
*
* PARAMETERS:  
* (I) @SpecialtyID          Specialty unique identity
*
* RESULT SET:
* SpecialtyID                   Specialty unique identity
* DisplayOrder                  Specialty display order
* EnabledFlag                   Specialty active/inactive flag
* Name                          Specialty name
* SysMaintainedFlag             Specialty system maintained flag
* SysLastUserID                 Specialty last user
* SysLastUpdatedDate            Specialty last update date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefSpecialtyGetDetail
(
	@SpecialtyID                    	udt_std_int_tiny
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspRefSpecialtyGetDetail'


    SELECT 
        SpecialtyID,
        DisplayOrder,
        EnabledFlag,
        Name,
        SysMaintainedFlag,
        SysLastUserID,
        dbo.ufnUtilityGetDateString(SysLastUpdatedDate) AS SysLastUpdatedDate
    FROM dbo.utb_specialty
    WHERE
        SpecialtyID = @SpecialtyID
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefSpecialtyGetDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefSpecialtyGetDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/