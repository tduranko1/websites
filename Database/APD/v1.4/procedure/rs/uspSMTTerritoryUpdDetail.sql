-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTTerritoryUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTTerritoryUpdDetail 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTTerritoryUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Retrieve a specific Shop Location
*
* PARAMETERS:  
* 
* RESULT SET:
* 
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTTerritoryUpdDetail
(
    @Territories            udt_std_desc_mid=null,
    @States                 udt_std_desc_mid=null,
    @UserID                 udt_std_id        

)
AS
BEGIN

  -- SET NOCOUNT to ON and no longer display the count message
  
  SET NOCOUNT ON
  
  
  -- Declare internal variables

  DECLARE @error      AS INT
  DECLARE @rowcount   AS INT
  DECLARE @Now        AS datetime
  DECLARE @ProcName   AS varchar(30)       -- Used for raise error stmts 
  
  DECLARE @tmpTemp      TABLE (ID                     int             NOT NULL,
                               DataChunk              varchar(30)     NOT NULL)
                        
  DECLARE @tmpTerritory TABLE (TerritoryID            int             NOT NULL,
                               ProgramManagerUserID   int             NOT NULL)       

  DECLARE @tmpState     TABLE (StateCode              char(2)         NOT NULL,
                               TerritoryID            int                 NULL)
                               
  SET @ProcName = 'uspSMTTerritoryUpdDetail'
  SET @Now = CURRENT_TIMESTAMP
    
  
                               
  BEGIN TRANSACTION TerritoryTran1
    
    IF @Territories IS NOT NULL
    BEGIN
      
      INSERT INTO @tmpTemp SELECT * FROM dbo.ufnUtilityParseString(@Territories, ';', 1)

      IF @@ERROR <> 0
      BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpTemp', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
      END
  
  
      -- The field, 'DataChunk', is a string in the following format, 'TERRITORYID,PROGRAMMANAGERUSERID'. The 
      -- following statement will break the string into its component parts, trim them and convert TERRITORYID 
      -- to an actual interger value before inserting them into @tmpState
      
      INSERT INTO @tmpTerritory 
      SELECT CONVERT(int, LTRIM(RTRIM(LEFT(DataChunk, CHARINDEX(',', DataChunk) - 1)))), 
             CONVERT(int, LTRIM(RTRIM(RIGHT(DataChunk, LEN(DataChunk) - CHARINDEX(',', DataChunk)))))
      FROM @tmpTemp
  
  
      IF @@ERROR <> 0
      BEGIN
        -- Insertion failure
  
        RAISERROR('105|%s|@tmpTerritory', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
      END
            
      
      DELETE @tmpTemp
      
      IF @@ERROR <> 0
      BEGIN
        -- Insertion failure
  
        RAISERROR('106|%s|@tmpTemp', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
      END
         
      
      -- Begin Audit Log code  -------------------------------------------------------------------------------
      
      INSERT INTO dbo.utb_audit_log (AuditTypeCD,
                                     KeyDescription,
                                     KeyID,
                                     LogComment,
                                     SysLastUserID,
                                     SysLastUpdatedDate)
                              SELECT 'P',
                                     (SELECT Name FROM dbo.utb_territory WHERE TerritoryID = t.TerritoryID),
                                     TerritoryID,
                                     'Program Manager changed from '+
                                        (SELECT ISNULL(NameFirst, '') + ' ' + ISNULL(NameLast, '') 
                                         FROM dbo.utb_user
                                         WHERE UserID = (SELECT isNull(ut.ProgramManagerUserID, '')
                                                         FROM dbo.utb_territory ut
                                                         where ut.TerritoryID = t.TerritoryID
                                                        )
                                        ) +
                                        ' to '+ 
                                        (SELECT ISNULL(NameFirst, '') + ' ' + ISNULL(NameLast, '') 
                                           FROM dbo.utb_user
                                           WHERE UserID = t.ProgramManagerUserID
                                        ),
                                     @UserID,
                                     @Now
                              FROM @tmpTerritory t
                                                                 
          
         
        IF (@@ERROR <> 0)
        BEGIN
          -- Error raised in calling proc

          ROLLBACK TRANSACTION
          RETURN
        END
    
    
      -- End of Audit Log code  -----------------------------------------------------------------------------------------
    
      UPDATE dbo.utb_territory 
      SET ProgramManagerUserID = t.ProgramManagerUserID,
          SysLastUserID = @UserID,
          SysLastUpdatedDate = @Now
      FROM dbo.utb_territory dbt INNER JOIN @tmpTerritory t ON dbt.TerritoryID = t.TerritoryID
  
      
      IF @@ERROR <> 0
      BEGIN
        -- Update failure
  
        RAISERROR('104|%s|utb_territory', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
      END
    END
    
       
    IF @States IS NOT NULL
    BEGIN  
      INSERT INTO @tmpTemp SELECT * FROM dbo.ufnUtilityParseString(@States, ';', 1)
    
      IF @@ERROR <> 0
      BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpTemp', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
      END
    
    
      -- The field, 'DataChunk', is a string in the following format, 'STATECODE,TERRITORYID'. The following statement 
      -- will break the string into its component parts, trim them and convert TERRITORYID to an actual interger value 
      -- before inserting them into @tmpState
        
      INSERT INTO @tmpState 
      SELECT LTRIM(RTRIM(LEFT(DataChunk, CHARINDEX(',', DataChunk) - 1))), 
             CONVERT(int, LTRIM(RTRIM(RIGHT(DataChunk, LEN(DataChunk) - CHARINDEX(',', DataChunk)))))
      FROM @tmpTemp
    
    
      IF @@ERROR <> 0
      BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpState', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
      END
       
       
      -- Begin Audit Log code  -------------------------------------------------------------------------------
    
    
      INSERT INTO dbo.utb_audit_log (AuditTypeCD,
                                     KeyDescription,
                                     KeyID,
                                     LogComment,
                                     SysLastUserID,
                                     SysLastUpdatedDate)
                              SELECT 'P',
                                     (SELECT Name FROM dbo.utb_territory WHERE TerritoryID = t.TerritoryID),
                                     TerritoryID,
                                     (SELECT StateValue FROM dbo.utb_state_code WHERE StateCode = t.StateCode) +
                                     ' changed territories from ' +
                                      (SELECT ISNULL(Name, '') FROM dbo.utb_territory WHERE TerritoryID = t.TerritoryID) +
                                     ' to ' + (SELECT TOP 1 Name FROM dbo.utb_territory terr INNER JOIN dbo.utb_state_code sc 
                                                                                               ON terr.TerritoryID = sc.TerritoryID),
                                    
                                     @UserID,
                                     @Now
                              FROM @tmpState t
                                                                 
          
         
        IF (@@ERROR <> 0)
        BEGIN
          -- Error raised in calling proc

          ROLLBACK TRANSACTION
          RETURN
        END
    
    
      -- End of Audit Log code  -----------------------------------------------------------------------------------------
    
    
      UPDATE dbo.utb_state_code
      SET TerritoryID = t.TerritoryID,
          SysLastUserID = @UserID,
          SysLastUpdatedDate = @Now       
      FROM dbo.utb_state_code s INNER JOIN @tmpState t ON s.StateCode = t.StateCode
    
    
      IF @@ERROR <> 0
      BEGIN
        -- Update failure
    
        RAISERROR('104|%s|utb_state_code', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
      END
    END
    
     
    
    COMMIT TRANSACTION TerritoryTran1
       
  END


  SELECT  1       AS tag,
          NULL    AS parent,
          NULL    AS [Root!1!Root],
          -- New Billing Record
          NULL    AS [LastUpdate!2!SysLastUpdatedDate]
            
  UNION ALL
    
  SELECT  2,
          1,
          NULL,
          @Now


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount

  
GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTTerritoryUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTTerritoryUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
