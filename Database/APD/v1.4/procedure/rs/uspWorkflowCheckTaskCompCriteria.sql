-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspWorkflowCheckTaskCompCriteria' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowCheckTaskCompCriteria
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowCheckTaskCompCriteria
* SYSTEM:       Lynx Services APD
* SYSTEM:       Lynx Services ADP
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Given a Task ID, Claim Aspect ID, and an optional replacable key value, this function determines
*               whether all requirements for completion of a task have been met
*
* PARAMETERS:
* @TaskID               The task id which is being checked
* @ClaimAspectID        The claim aspect id used as reference
* @Key                  A replaceable key value used to assist in performing the check
* @ReturnText  OUTPUT   A text description of the success or reason for the failure (based on return value)
*
* UPDATES:						1.1 - 27Mar2017 - Sergey - Modified to fix deadlock issues
*
* RETURNS:
* Result                A bit flag (1 = All criteria have been met, 0 = One or more criteria have failed)
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowCheckTaskCompCriteria
    @TaskID                       udt_std_id,
    @ClaimAspectServiceChannelID  udt_std_id_big,
    @Key                          udt_std_desc_short = NULL,
    @ReturnText                   udt_std_desc_mid OUTPUT
AS
BEGIN
    -- SET database options
    SET CONCAT_NULL_YIELDS_NULL OFF
    SET NOCOUNT ON

    -- Declare local variables
    DECLARE @ClaimAspectID      udt_std_id_big
    DECLARE @CriteriaID         udt_std_id
    DECLARE @CriteriaSQL        udt_std_desc_long
    DECLARE @ReturnValue        udt_std_int
    DECLARE @SPExecParms        nvarchar(4)
    DECLARE @SQLStmt            nvarchar(1000)
    DECLARE @SQLStmtNoExec      nvarchar(1000)
    DECLARE @ValidResultSetSQL  udt_std_desc_mid
    DECLARE @debug              udt_std_flag
    DECLARE @ProcName           udt_std_name
    DECLARE @ErrorCode          int

    -- We must first create a temporary table to serve as a link between this proc and the exec statments
    -- that will be tested.  An actual temporary table is required as table variables will not scope to
    -- embedded execs
    CREATE TABLE #tmpCriteria
		(CriteriaID          int             NOT NULL,
        CriteriaSQL         varchar(500)    NOT NULL,
        FailureMessage      varchar(500)    NOT NULL,
        ValidResultSetSQL   varchar(500)    NOT NULL,
        SuccessFlag         bit             NULL)


    -- Initialize any empty string parameters
    IF LEN(RTRIM(LTRIM(@Key))) = 0 SET @Key = NULL

    SET @debug = 0
    SET @ProcName = 'uspWorkflowCheckTaskCompCriteria'

    IF @debug = 1
    BEGIN
        PRINT 'Parameters:'
        PRINT '@TaskID = ' + Convert(varchar(20), @TaskID)
        PRINT '@ClaimAspectServiceChannelID = ' + Convert(varchar(20), @ClaimAspectServiceChannelID)
        PRINT '@Key = ' + @Key
    END

    -- Validate that task is is valid
    IF NOT EXISTS(SELECT TaskID FROM dbo.utb_task (NOLOCK) WHERE TaskID = @TaskID)
    BEGIN
        RAISERROR('101|%s|@TaskID|%u', 16, 1, @ProcName, @TaskID)
        RETURN
    END

    -- Validate that claim aspect is valid
    IF NOT EXISTS(SELECT ClaimAspectServiceChannelID FROM dbo.utb_claim_aspect_service_channel (NOLOCK) WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
    BEGIN
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
        RETURN
    END

    SELECT @ClaimAspectID = ClaimAspectID FROM dbo.utb_claim_aspect_service_channel WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    -- Populate with the task completion criteria we need checked
    INSERT INTO #tmpCriteria
    SELECT  TaskCompletionCriteriaID,
            CriteriaSQL,
            FailureMessage,
            ValidResultSetSQL,
            NULL
    FROM dbo.utb_task_completion_criteria
    WHERE TaskID = @TaskID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
        RAISERROR('105|%s|#tmpCriteria', 16, 1, @ProcName)
        RETURN
    END

    IF @debug = 1
    BEGIN
        PRINT 'Criteria to be checked:'
        SELECT * FROM #tmpCriteria
    END

    -- Start a cursor and walk through the criteria to be checked for this task
    DECLARE csrCriteria CURSOR FOR
      SELECT CriteriaID, CriteriaSQL, ValidResultSetSQL 
      FROM #tmpCriteria

    OPEN csrCriteria

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SET @CriteriaID = NULL
    SET @CriteriaSQL = NULL
    SET @ValidResultSetSQL = NULL

    FETCH NEXT FROM csrCriteria
      INTO  @CriteriaID, @CriteriaSQL, @ValidResultSetSQL

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    WHILE @@FETCH_STATUS=0
    BEGIN
        -- Build the SQL we'll use to test with
        -- First test to make sure @CriteriaSQL is valid and contains no action statements
        IF (PatIndex('%ALTER %', @CriteriaSQL) > 0) OR
           (PatIndex('%BACKUP %', @CriteriaSQL) > 0) OR
           (PatIndex('%COMMIT %', @CriteriaSQL) > 0) OR
           (PatIndex('%CREATE %', @CriteriaSQL) > 0) OR
           (PatIndex('%DBCC %', @CriteriaSQL) > 0) OR
           (PatIndex('%DELETE %', @CriteriaSQL) > 0) OR
           (PatIndex('%DENY %', @CriteriaSQL) > 0) OR
           (PatIndex('%DROP %', @CriteriaSQL) > 0) OR
           (PatIndex('%EXECUTE %', @CriteriaSQL) > 0) OR
           (PatIndex('%EXEC %', @CriteriaSQL) > 0) OR
           (PatIndex('%GRANT %', @CriteriaSQL) > 0) OR
           (PatIndex('%INSERT %', @CriteriaSQL) > 0) OR
           (PatIndex('%KILL %', @CriteriaSQL) > 0) OR
           (PatIndex('%PRINT %', @CriteriaSQL) > 0) OR
           (PatIndex('%RESTORE %', @CriteriaSQL) > 0) OR
           (PatIndex('%REVOKE %', @CriteriaSQL) > 0) OR
           (PatIndex('%ROLLBACK %', @CriteriaSQL) > 0) OR
           (PatIndex('%INTO %', @CriteriaSQL) > 0) OR
           (PatIndex('%SHUTDOWN %', @CriteriaSQL) > 0) OR
           (PatIndex('%TRUNCATE %', @CriteriaSQL) > 0) OR
           (PatIndex('%UPDATE %', @CriteriaSQL) > 0) OR
           (PatIndex('%UPDATETEXT %', @CriteriaSQL) > 0) OR
           (PatIndex('%WRITETEXT %', @CriteriaSQL) > 0)
        BEGIN
            -- Raise a warning that will be caught internally but ignore the record
            RAISERROR('3|%s: Invalid Criteria statment contains action command.  Check skipped for CriteriaID %u.', 10, 1, @ProcName, @CriteriaID)
        END
        ELSE
        BEGIN
            -- Continue with check, first we'll parse the exec without executing it in case there is a syntax problem
            SET @SQLStmt = N'UPDATE #tmpCriteria ' +
                                'SET SuccessFlag = CASE ' +
                                                    'WHEN ((' + @CriteriaSQL + ') ' + @ValidResultSetSQL + ') THEN 1 ' +
                                                    'ELSE 0 ' +
                                                'END ' +
                                'WHERE CriteriaID = ' + Convert(varchar(10), @CriteriaID)
            SET @SQLStmt = Replace(@SQLStmt, '%ClaimAspectServiceChannelID%', Convert(varchar(10), @ClaimAspectServiceChannelID))
            SET @SQLStmt = Replace(@SQLStmt, '%Key%', IsNull(Convert(varchar(10), @Key), ''))
            SET @SQLStmt = Replace(@SQLStmt, '%ClaimAspectID%',Convert(varchar(10),@ClaimAspectID))
            SET @SPExecparms = NULL

            Exec sp_executeSQL @SQLStmt, @SPExecparms

            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error occurred with the Exec statement.  Theoretically should not occur at this
                -- point as it sahould have been caught in the parse test above.
                RAISERROR('99|%s', 16, 1, @ProcName)
                RETURN
            END
        END

        -- Get next criteria
        SET @CriteriaID = NULL
        SET @CriteriaSQL = NULL
        SET @ValidResultSetSQL = NULL

        FETCH NEXT FROM csrCriteria
			INTO  @CriteriaID, @CriteriaSQL, @ValidResultSetSQL

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END

    CLOSE csrCriteria
    DEALLOCATE csrCriteria


    -- The results of our completion tests are now stored in #tmpCriteria, review the success flag column.
    -- If any of the values are 0, the test has failed and a message needs to be returned to the caller
    -- with a failure return code, otherwise a "Success" message will be returned, with a 1 return code
    IF @debug = 1
    BEGIN
        PRINT 'Criteria check results:'
        SELECT * FROM #tmpCriteria
    END

    IF EXISTS(SELECT SuccessFlag FROM #tmpCriteria WHERE SuccessFlag = 0)
    BEGIN
        -- We have at least one failed criteria
        SELECT TOP 1 @ReturnText = FailureMessage
        FROM  #tmpCriteria
        WHERE SuccessFlag = 0
        ORDER BY CriteriaID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        SET @ReturnValue = 0
    END
    ELSE
    BEGIN
        -- All criteria have passed, return sucess
        SET @ReturnText = 'Success'
        SET @ReturnValue = 1
    END

    ---- Clean up after ourselves
    --DROP TABLE #tmpCriteria

    IF @debug = 1
    BEGIN
        SELECT @ReturnValue AS ReturnCode, @ReturnText AS ReturnText
    END

    RETURN @ReturnValue
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspWorkflowCheckTaskCompCriteria' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowCheckTaskCompCriteria TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/