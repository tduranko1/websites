-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientDeskAuditDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2ClientDeskAuditDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2ClientDeskAuditDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Returns Desk Audit or Desk Review Details
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company the report is being run for
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
* (I) @ServiceChannelCD     ServiceChannelCD (Indicates whether we pull Desk Audit or Desk Review data)
* (I) @OfficeID             Optional Office ID
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2ClientDeskAuditDetail
    @InsuranceCompanyID     udt_std_int_small,
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL,
    @ServiceChannelCD       udt_std_cd = NULL,
    @OfficeID               udt_std_int_small = NULL,
    @AssignmentTypeID       udt_std_int_small = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare Local variables
    
    DECLARE @InsuranceCompanyName   udt_std_name
    DECLARE @InsuranceCompanyLCPhone udt_std_name
    DECLARE @MonthName              udt_std_name
    DECLARE @ServiceChannelCDWork   udt_std_cd
    DECLARE @ServiceChannelName     udt_std_name
    DECLARE @OfficeIDWork           varchar(10)
    DECLARE @OfficeName             udt_std_name
    DECLARE @AssignmentTypeIDWork   varchar(2) 
    DECLARE @AssignmentTypeWork     udt_std_name
    
    DECLARE @Debug      udt_std_flag
    DECLARE @now        udt_std_datetime
    DECLARE @DataWarehouseDate    udt_std_datetime
	DECLARE @CancelledDispositionID int
	DECLARE @VoidedDispositionID    int

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2ClientDeskAuditDetail'

    SET @Debug = 0


    -- Validate Insurance Company ID
    
    IF (@InsuranceCompanyID IS NULL) OR
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID
        
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name,
                @InsuranceCompanyLCPhone = IsNull(CarrierLynxContactPhone, '239-337-4300')
          FROM  dbo.utb_insurance
          WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END          
        
   
    -- Validate Service Channel Code
    
    IF @ServiceChannelCD NOT IN ('DA', 'DR')
    BEGIN
        -- Invalid Service Channel CD
    
        RAISERROR  ('101|%s|@ServiceChannelCD|%s', 16, 1, @ProcName, @ServiceChannelCD)
        RETURN
    END
    ELSE
    BEGIN
        SET @ServiceChannelCDWork = @ServiceChannelCD
        
        SELECT  @ServiceChannelName = Name
          FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
          WHERE Code = @ServiceChannelCD
          
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END
                
   
    -- Validate APD Data State
    
    IF NOT EXISTS(SELECT InvolvedRoleTypeID FROM dbo.utb_involved_role_type WHERE Name = 'Owner')
    BEGIN
       -- Involved Role Type Not Found
    
        RAISERROR('%s: Invalid APD Data State.  "Owner" not found in utb_involved_role_type.', 16, 1, @ProcName)
        RETURN
    END

    -- Validate Office ID
    
    IF @OfficeID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT OfficeID FROM dbo.utb_office WHERE OfficeID = @OfficeID AND InsuranceCompanyID = @InsuranceCompanyID)
        BEGIN
            -- Invalid Office ID
    
            RAISERROR  ('101|%s|@OfficeID|%u', 16, 1, @ProcName, @OfficeID)
            RETURN
        END
        ELSE
        BEGIN
            SET @OfficeIDWork = convert(varchar, @OfficeID)
        
            SELECT  @OfficeName = Name
              FROM  dbo.utb_office
              WHERE OfficeID = @OfficeID
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @OfficeIDWork = '%'
        SET @OfficeName = 'All'
    END          
    
    -- Validate AssignmentTypeID
    IF @AssignmentTypeID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT AssignmentTypeID
                        FROM dbo.utb_assignment_type
                        WHERE AssignmentTypeID = @AssignmentTypeID)
        BEGIN
            -- Invalid AssignmentType ID
    
            RAISERROR  ('101|%s|@AssignmentTypeID|%u', 16, 1, @ProcName, @AssignmentTypeID)
            RETURN
        END
        ELSE
        BEGIN
            SET @AssignmentTypeIDWork = CONVERT(varchar, @AssignmentTypeID)
            
            SELECT @AssignmentTypeWork = Name
            FROM dbo.utb_assignment_type
            WHERE AssignmentTypeID = @AssignmentTypeID
        END
    END
    ELSE
    BEGIN
        SET @AssignmentTypeIDWork = '%'
        SET @AssignmentTypeWork = 'All'
    END
    
      -- Get Cancelled DispositionType ID
   
    SET @CancelledDispositionID = NULL
	
	SELECT @CancelledDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Cancelled'
    
    IF @CancelledDispositionID IS NULL OR LEN(@CancelledDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Cancelled in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
	-- Get Voided DispositionType ID
	
	SET @VoidedDispositionID = NULL

	SELECT @VoidedDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Voided'
    
    IF @VoidedDispositionID IS NULL OR LEN(@VoidedDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Voided in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END


    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID = ' + Convert(varchar, @InsuranceCompanyID)
        PRINT '@InsuranceCompanyName = ' + @InsuranceCompanyName
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@ServiceChannelCD = ' + @ServiceChannelCD
        PRINT '@ServiceChannelCDWork = ' + @ServiceChannelCDWork
        PRINT '@OfficeID = ' + convert(varchar, @OfficeID)
        PRINT '@OfficeIDWork = ' + convert(varchar, @OfficeIDWork)
        PRINT '@AssignmentTypeID = ' + convert(varchar, @AssignmentTypeID)
        PRINT '@AssignmentTypeIDWork = ' + convert(varchar, @AssignmentTypeIDWork)        
    END
    
    
    -- Compile data needed for the report

    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            @ServiceChannelName AS ServiceChannelName,
            @OfficeName AS OfficeName,
            CASE 
              WHEN Len(LTrim(RTrim(dc.OfficeClientId))) > 0 THEN dc.OfficeClientId
              ELSE dc.OfficeName
            END AS Office,
            @AssignmentTypeWork AS AssignmenTypeSelection,
            Convert(varchar(15), ca.LynxID) AS LynxID,
            cc.ClientClaimNumber AS ClaimNumber,
			dat.AssignmentTypeDescription,
            dc.RepNameFirst AS CarrierRepNameFirst,
            dc.RepNameLast AS CarrierRepNameLast,
            dlh.UserNameFirst AS LYNXRepNameFirst,
            dlh.UserNameLast AS LYNXRepNameLast,
            i.BusinessName AS VehicleOwnerBusinessName,
            i.NameFirst AS VehicleOwnerNameFirst,
            i.NameLast AS VehicleOwnerNameLast,
            fc.OriginalEstimateGrossAmt,
            fc.AuditedEstimatewoBettAmt,
            fc.OriginalEstimateGrossAmt - fc.AuditedEstimatewoBettAmt AS EstimateDifference,
            AuditedEstimateAgreedFlag AS EstimateAgreedFlag,
            Convert(varchar(15), dtn.DateValue, 1) AS DateOpened,
            Convert(varchar(15), dtc.DateValue, 1) AS DateClosed,
            fc.CycleTimeEstToCloseHrs,
            @DataWareHouseDate as DataWareDate              
      FROM  dbo.utb_dtwh_fact_claim fc
      LEFT JOIN dbo.utb_dtwh_dim_time dtn ON (fc.TimeIDNew = dtn.TimeID)
      LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
      LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
      LEFT JOIN dbo.utb_dtwh_dim_lynx_handler dlh ON (fc.LynxHandlerAnalystID = dlh.LynxHandlerID)
      LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
      LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim cc ON (ca.LynxID = cc.LynxID)
      LEFT JOIN dbo.utb_claim_aspect_involved cai ON (fc.ClaimAspectID = cai.ClaimAspectID)
      LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
      LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
      LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
      WHERE dtc.MonthOfYear = @RptMonth
        AND dtc.YearValue = @RptYear
        AND dc.InsuranceCompanyID = @InsuranceCompanyID
        AND dsc.ServiceChannelCD = @ServiceChannelCDWork
        AND irt.Name = 'Owner'
        AND fc.TimeIDVoided IS NULL
        AND fc.TimeIDCancelled IS NULL
        AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
		AND fc.EnabledFlag = 1
		AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
        AND convert(varchar, dat.AssignmentTypeID) LIKE @AssignmentTypeIDWork

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientDeskAuditDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2ClientDeskAuditDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
