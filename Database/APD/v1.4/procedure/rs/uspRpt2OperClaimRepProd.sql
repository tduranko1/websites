-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2OperClaimRepProd' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2OperClaimRepProd 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2OperClaimRepProd
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2OperClaimRepProd
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare internal variables

    DECLARE @tmpReportDataMonthDA TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Level3Group                 varchar(20)  NOT NULL,
        LynxHandler                 bigint       NOT NULL,
        NewTotal                    bigint       NULL,
        OpenTotal                   bigint       NULL,
        ClosedTotal                 bigint       NULL,
        CancelledTotal              bigint       NULL,
        VoidedTotal                 bigint       NULL,
        ReopenedTotal               bigint       NULL,
        ReclosedTotal               bigint       NULL,
        AvgCTNew2Close              decimal(6,1) NULL,
        AvgCTReopen2Reclose         decimal(6,1) NULL
    )

    DECLARE @tmpNewData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        LynxHandlerID               bigint       NOT NULL,
        NewCount                    int          NOT NULL
    )
        
    DECLARE @tmpOpenData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        LynxHandlerID               bigint       NOT NULL,
        OpenCount                   int          NOT NULL
    )

    DECLARE @tmpClosedData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        CoverageTypeCD              varchar(4)   NOT NULL,
        LynxHandlerID               bigint       NOT NULL,
        ClosedCount                 int          NOT NULL
    )

    DECLARE @tmpCancelledData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        LynxHandlerID               bigint       NOT NULL,
        CancelledCount              int          NOT NULL
    )

    DECLARE @tmpVoidedData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        LynxHandlerID               bigint       NOT NULL,
        VoidedCount                 int          NOT NULL
    )

    DECLARE @tmpReopenedData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        LynxHandlerID               bigint       NOT NULL,
        ReopenedCount               int          NOT NULL
    )

    DECLARE @tmpReclosedData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        LynxHandlerID               bigint       NOT NULL,
        ReclosedCount               int          NOT NULL
    )

    DECLARE @tmpCTNew2CloseData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        LynxHandlerID               bigint       NOT NULL,
        AvgCTNew2Close              decimal(6,1) NULL
    )

    DECLARE @tmpCTReopen2RecloseData TABLE
    (
        LineItemCD                  varchar(4)   NOT NULL,
        LynxHandlerID               bigint       NOT NULL,
        AvgCTReopen2Reclose         decimal(6,1) NULL
    )
    
    DECLARE @tmpLevel3Group TABLE
    (
   Level3Group                 varchar(5) NOT NULL
    )


    DECLARE @ClosedCountMonth       udt_std_int
    DECLARE @MonthName              udt_std_name
    DECLARE @NewCountMonth          udt_std_int
    DECLARE @PendingCountMonth      udt_std_int
    DECLARE @CancelledCountMonth    udt_std_int
    DECLARE @VoidedCountMonth       udt_std_int
    DECLARE @ReopenedCountMonth     udt_std_int
    DECLARE @ReclosedCountMonth     udt_std_int
    DECLARE @LYNXHandlerWorking     bigint

    DECLARE @Debug      			udt_std_flag
    DECLARE @now        			udt_std_datetime
    DECLARE @DataWarehouseDate    	udt_std_datetime
    DECLARE @RptDate    			datetime
	DECLARE @CancelledDispositionID int
	DECLARE @VoidedDispositionID    int

    DECLARE @ProcName   varchar(30)
    SET @ProcName = ' uspRpt2OperClaimRepProd'

    SET @Debug = 0

	 -- Get Cancelled DispositionType ID
   
    SET @CancelledDispositionID = NULL
	
	SELECT @CancelledDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Cancelled'
    
    IF @CancelledDispositionID IS NULL OR LEN(@CancelledDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Cancelled in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
	-- Get Voided DispositionType ID
	
	SET @VoidedDispositionID = NULL

	SELECT @VoidedDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Voided'
    
    IF @VoidedDispositionID IS NULL OR LEN(@VoidedDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Voided in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @RptDate = CONVERT(datetime, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
        SET @MonthName = DateName(mm, @RptDate)
    END
    

    IF @Debug = 1
    BEGIN
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptMonthName = ' + @MonthName
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear, 101)
        
        PRINT '@RptDate = ' + convert(varchar, @RptDate, 101)
    END
    
    
    -- Compile data needed for the report
    
    -- Monthly Damage Assessment Totals
    
    -- New 
    
    INSERT INTO @tmpNewData
      SELECT  'TTL',
              CASE
                WHEN dsc.ServiceChannelCD IN ('PS', 'ME', 'RRP') THEN ISNULL(fc.LynxHandlerOwnerID,0)
                WHEN dsc.ServiceChannelCD IN ('DA', 'DR') THEN ISNULL(fc.LynxHandlerAnalystID,0)
              END,
              Count(*) AS OpenCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY CASE
                WHEN dsc.ServiceChannelCD IN ('PS', 'ME', 'RRP') THEN ISNULL(fc.LynxHandlerOwnerID,0)
                WHEN dsc.ServiceChannelCD IN ('DA', 'DR') THEN ISNULL(fc.LynxHandlerAnalystID,0)
                 END


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpNewData
      SELECT  dsc.ServiceChannelCD,
              CASE
                WHEN dsc.ServiceChannelCD IN ('PS', 'ME', 'RRP') THEN ISNULL(fc.LynxHandlerOwnerID,0)
                WHEN dsc.ServiceChannelCD IN ('DA', 'DR') THEN ISNULL(fc.LynxHandlerAnalystID,0)
              END,
              Count(*) AS OpenCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY CASE
            WHEN dsc.ServiceChannelCD IN ('PS', 'ME', 'RRP') THEN ISNULL(fc.LynxHandlerOwnerID,0)
            WHEN dsc.ServiceChannelCD IN ('DA', 'DR') THEN ISNULL(fc.LynxHandlerAnalystID,0)
                 END, dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpNewData', 16, 1, @ProcName)
        RETURN
    END
    
    -- Open
    
    INSERT INTO @tmpOpenData
      SELECT  'TTL',
              CASE
                WHEN dsc.ServiceChannelCD IN ('PS', 'ME', 'RRP') THEN ISNULL(fc.LynxHandlerOwnerID,0)
                WHEN dsc.ServiceChannelCD IN ('DA', 'DR') THEN ISNULL(fc.LynxHandlerAnalystID,0)
              END,
              Count(*) AS OpenCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtn ON (fc.TimeIDNew = dtn.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dtn.DateValue < DateAdd(mm, 1, @RptDate)
          AND fc.TimeIDClosed IS NULL
          AND fc.TimeIDCancelled IS NULL
          AND fc.TimeIDVoided IS NULL
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY CASE
                   WHEN dsc.ServiceChannelCD IN ('PS', 'ME', 'RRP') THEN ISNULL(fc.LynxHandlerOwnerID,0)
                   WHEN dsc.ServiceChannelCD IN ('DA', 'DR') THEN ISNULL(fc.LynxHandlerAnalystID,0)
                 END

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpPendingData', 16, 1, @ProcName)
        RETURN
    END
/*          
    INSERT INTO @tmpOpenData
      SELECT  dsc.ServiceChannelCD,
              fc.LynxHandlerID,
              Count(*) AS OpenCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtn ON (fc.TimeIDNew = dtn.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dtn.DateValue < DateAdd(mm, 1, @RptDate)
          AND fc.TimeIDClosed IS NULL
          AND fc.TimeIDCancelled IS NULL
          AND fc.TimeIDVoided IS NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY fc.LynxHandlerID, dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpPendingData', 16, 1, @ProcName)
        RETURN
    END

    -- Closed
    
    INSERT INTO @tmpClosedData
      SELECT  'TTL',
              dct.CoverageTypeCD,
              fc.LynxHandlerID,
              Count(*) AS ClosedCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.AssignmentTypeClosingID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY fc.LynxHandlerID, dct.CoverageTypeCD
        HAVING dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpClosedData
      SELECT  dsc.ServiceChannelCD,
              dct.CoverageTypeCD,
              fc.LynxHandlerID,
              Count(*) AS ClosedCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY fc.LynxHandlerID, dsc.ServiceChannelCD, dct.CoverageTypeCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
          AND dct.CoverageTypeCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    -- Cancelled
    
    INSERT INTO @tmpCancelledData
      SELECT  'TTL',
              fc.LynxHandlerID,
              Count(*) AS CancelledCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDCancelled = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDCancelled IS NOT NULL
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
        GROUP BY fc.LynxHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpCancelledData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpCancelledData
      SELECT  dsc.ServiceChannelCD,
              fc.LynxHandlerID,
              Count(*) AS CancelledCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDCancelled = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDCancelled IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
        GROUP BY fc.LynxHandlerID, dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpCancelledData', 16, 1, @ProcName) 
        RETURN
    END

    -- Voided
    
    INSERT INTO @tmpVoidedData
      SELECT  'TTL',
              fc.LynxHandlerID,
              Count(*) AS VoidedCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDVoided = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDVoided IS NOT NULL
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
        GROUP BY fc.LynxHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpVoidedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpVoidedData
      SELECT  dsc.ServiceChannelCD,
              fc.LynxHandlerID,
              Count(*) AS VoidedCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDVoided = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDVoided IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
        GROUP BY fc.LynxHandlerID, dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpVoidedData', 16, 1, @ProcName) 
        RETURN
    END
    
    -- Reopened
    
    INSERT INTO @tmpReopenedData
      SELECT  'TTL',
              fc.LynxHandlerID,
              Count(*) AS ReopenedCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDReopened = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDReopened IS NOT NULL
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
          AND fc.DemoFlag = 0
        GROUP BY fc.LynxHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReopenedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpReopenedData
      SELECT  dsc.ServiceChannelCD,
              fc.LynxHandlerID,
              Count(*) AS ReopenedCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDReopened = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDReopened IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
          AND fc.DemoFlag = 0
        GROUP BY fc.LynxHandlerID, dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReopenedData', 16, 1, @ProcName) 
        RETURN
    END
    
    -- Reclosed
    
    INSERT INTO @tmpReclosedData
      SELECT  'TTL',
              fc.LynxHandlerID,
              Count(*) AS ReclosedCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDReclosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDReclosed IS NOT NULL
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY fc.LynxHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReclosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpReclosedData
      SELECT  dsc.ServiceChannelCD,
              fc.LynxHandlerID,
              Count(*) AS ReclosedCount
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDReclosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDReclosed IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
          AND fc.DemoFlag = 0
        GROUP BY fc.LynxHandlerID, dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReclosedData', 16, 1, @ProcName) 
        RETURN
    END
    

    -- Avg CT New to Close
    
    INSERT INTO @tmpCTNew2CloseData
      SELECT  'TTL',
              fc.LynxHandlerID,
              Avg(CycleTimeNewToCloseHrs/24.00) AS AvgCTNew2Close
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDClosed IS NOT NULL
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND fc.CycleTimeNewToCloseHrs IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY fc.LynxHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpCTNew2CloseData
      SELECT  dsc.ServiceChannelCD,
              fc.LynxHandlerID,
              Avg(CycleTimeNewToCloseHrs/24.00) AS AvgCTNew2Close
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.TimeIDClosed IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND fc.CycleTimeNewToCloseHrs IS NOT NULL
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY fc.LynxHandlerID, dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    -- Avg CT Reopen to Reclose
    
    INSERT INTO @tmpCTReopen2RecloseData
      SELECT  'TTL',
              fc.LynxHandlerID,
              Avg(datediff(hh, dtro.DateValue, dtrc.DateValue)/24.00)  AS AvgCTReopen2Reclose
              --Avg(CycleTimeNewToRecloseHrs/24.00) AS AvgCTNew2Reclose
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtro ON (fc.TimeIDReopened = dtro.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        WHERE dtrc.MonthOfYear = @RptMonth
          AND dtrc.YearValue = @RptYear
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND fc.CycleTimeNewToRecloseHrs IS NOT NULL
          AND dtro.TimeID < dtrc.TimeID
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY fc.LynxHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpCTReopen2RecloseData
      SELECT  dsc.ServiceChannelCD,
              fc.LynxHandlerID,
              Avg(datediff(hh, dtro.DateValue, dtrc.DateValue)/24.00)  AS AvgCTReopen2Reclose
              --Avg(CycleTimeNewToRecloseHrs/24.00) AS AvgCTNew2Reclose
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtro ON (fc.TimeIDReopened = dtro.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dtrc.MonthOfYear = @RptMonth
          AND dtrc.YearValue = @RptYear
          AND fc.TimeIDReclosed IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND fc.CycleTimeNewToRecloseHrs IS NOT NULL
          AND dtro.TimeID < dtrc.TimeID
		  AND fc.EnabledFlag = 1
          AND fc.DemoFlag = 0
		  AND (fc.DispositionTypeID IS NULL OR fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID ))
        GROUP BY fc.LynxHandlerID, dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    -- Pivot and save the data extracted

    -- Closed
    INSERT INTO @tmpReportDataMonthDA
      SELECT  'Month',
              'DA',
              LineItemCD,
              LynxHandlerID,
              0,
              0,
              SUM(ClosedCount),
              0, 0, 0, 0, 0, 0
        FROM  @tmpClosedData
        GROUP BY LynxHandlerID, LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END

    -- New
    UPDATE  @tmpReportDataMonthDA
      SET   NewTotal = nd.NewCount
      FROM  @tmpNewData nd
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD
        AND LYNXHandler = LYNXHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END


    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, LynxHandler, NewTotal, OpenTotal, ClosedTotal, CancelledTotal, VoidedTotal, ReopenedTotal, ReclosedTotal, AvgCTNew2Close, AvgCTReopen2Reclose)
      SELECT  'Month',
              'DA',
              LineItemCD,
              LynxHandlerID, 
              NewCount,
              0, 0, 0, 0,
              0, 0, 0, 0
        FROM  @tmpNewData t
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA where LynxHandler = t.LynxHandlerID)
      
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END

    -- Open
    UPDATE  @tmpReportDataMonthDA
      SET   OpenTotal = od.OpenCount
      FROM  @tmpOpenData od
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD
        AND LYNXHandler = LYNXHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, LynxHandler, OpenTotal, NewTotal, ClosedTotal, CancelledTotal, VoidedTotal, ReopenedTotal, ReclosedTotal, AvgCTNew2Close, AvgCTReopen2Reclose)
      SELECT  'Month',
              'DA',
              LineItemCD,
              LynxHandlerID, 
              OpenCount,
              0, 0, 0, 0,
              0, 0, 0, 0
        FROM  @tmpOpenData t
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA where LynxHandler = t.LynxHandlerID)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END

    -- Cancelled
    UPDATE  @tmpReportDataMonthDA
      SET   CancelledTotal = cd.CancelledCount
      FROM  @tmpCancelledData cd
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD
        AND LYNXHandler = LYNXHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, LYNXHandler, CancelledTotal, NewTotal, OpenTotal, ClosedTotal, VoidedTotal, ReopenedTotal, ReclosedTotal, AvgCTNew2Close, AvgCTReopen2Reclose)
      SELECT  'Month',
              'DA',
              LineItemCD,
              LYNXHandlerID,
              CancelledCount,
              0, 0, 0, 0,
              0, 0, 0, 0
        FROM  @tmpCancelledData t
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA where LynxHandler = t.LynxHandlerID)
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END

    -- Voided
    UPDATE  @tmpReportDataMonthDA
      SET   VoidedTotal = vd.VoidedCount
      FROM  @tmpVoidedData vd
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD
        AND LYNXHandler = LYNXHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, LYNXHandler, VoidedTotal, NewTotal, OpenTotal, ClosedTotal, CancelledTotal, ReopenedTotal, ReclosedTotal, AvgCTNew2Close, AvgCTReopen2Reclose)
      SELECT  'Month',
              'DA',
              LineItemCD,
              LYNXHandlerID,
              VoidedCount,
              0, 0, 0, 0,
              0, 0, 0, 0
        FROM  @tmpVoidedData t
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA where LynxHandler = t.LynxHandlerID)
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END

    -- Reopened
    UPDATE  @tmpReportDataMonthDA
      SET   ReopenedTotal = rd.ReopenedCount
      FROM  @tmpReopenedData rd
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD
        AND LYNXHandler = LYNXHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, LYNXHandler, ReopenedTotal, NewTotal, OpenTotal, ClosedTotal, CancelledTotal, VoidedTotal, ReclosedTotal, AvgCTNew2Close, AvgCTReopen2Reclose)
      SELECT  'Month',
              'DA',
              LineItemCD,
              LYNXHandlerID,
              ReopenedCount,
              0, 0, 0, 0,
              0, 0, 0, 0
        FROM  @tmpReopenedData t
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA where LynxHandler = t.LynxHandlerID)
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END

    -- Reclosed
    UPDATE  @tmpReportDataMonthDA
      SET   ReclosedTotal = rd.ReclosedCount
      FROM  @tmpReclosedData rd
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD
        AND LYNXHandler = LYNXHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, LYNXHandler, ReclosedTotal, NewTotal, OpenTotal, ClosedTotal, CancelledTotal, VoidedTotal, ReopenedTotal, AvgCTNew2Close, AvgCTReopen2Reclose)
      SELECT  'Month',
              'DA',
              LineItemCD,
              LYNXHandlerID,
              ReclosedCount, 
              0, 0, 0, 0,
              0, 0, 0, 0
        FROM  @tmpReclosedData t
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA where LynxHandler = t.LynxHandlerID)
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END

    -- Avg CT New to Close
    UPDATE  @tmpReportDataMonthDA
      SET   AvgCTNew2Close = rd.AvgCTNew2Close
      FROM  @tmpCTNew2CloseData rd
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD
        AND LYNXHandler = LYNXHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, LYNXHandler, AvgCTNew2Close, NewTotal, OpenTotal, ClosedTotal, CancelledTotal, VoidedTotal, ReopenedTotal, ReclosedTotal,  AvgCTReopen2Reclose)
      SELECT  'Month',
              'DA',
              LineItemCD,
              LynxHandlerID,
              AvgCTNew2Close,
              0, 0, 0, 0,
              0, 0, 0, 0
        FROM  @tmpCTNew2CloseData t
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA where LynxHandler = t.LynxHandlerID)
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END

    -- Avg CT Reopen to Reclose
    UPDATE  @tmpReportDataMonthDA
      SET   AvgCTReopen2Reclose = rd.AvgCTReopen2Reclose
      FROM  @tmpCTReopen2RecloseData rd
      WHERE Level1Group = 'Month'
        AND Level2Group = 'DA'
        AND Level3Group = LineItemCD
        AND LYNXHandler = LYNXHandlerID

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonthDA (Level1Group, Level2Group, Level3Group, LYNXHandler, AvgCTReopen2Reclose, NewTotal, OpenTotal, ClosedTotal, CancelledTotal, VoidedTotal, ReopenedTotal, ReclosedTotal, AvgCTNew2Close)
      SELECT  'Month',
              'DA',
              LineItemCD,
              LYNXHandlerID,
              AvgCTReopen2Reclose,
              0, 0, 0, 0,
              0, 0, 0, 0
        FROM  @tmpCTReopen2RecloseData t
        WHERE LineItemCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA where LynxHandler = t.LynxHandlerID)
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName) 
        RETURN
    END


    INSERT INTO @tmpReportDataMonthDA
      SELECT  'Month',
              'DA',
              dsc.ServiceChannelCD,
              0, 0, 0, 0, 0, 
              0, 0, 0, 0, 0
        FROM  dbo.utb_dtwh_dim_service_channel dsc
        WHERE dsc.ServiceChannelCD NOT IN (SELECT Level3Group FROM @tmpReportDataMonthDA)
          AND dsc.ServiceChannelCD IN (SELECT  at.ServiceChannelDefaultCD 
                                         FROM  dbo.utb_client_assignment_type cat
                                         LEFT JOIN dbo.utb_assignment_type at ON (cat.AssignmentTypeID = at.AssignmentTypeID)
                                         )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonthDA', 16, 1, @ProcName)
        RETURN
    END

    -- Compile for the missing Level3Group

    INSERT INTO @tmpLevel3Group
    SELECT distinct Level3Group
    FROM @tmpReportDataMonthDA

    DECLARE csrLYNXHandlers CURSOR FOR
        SELECT DISTINCT LynxHandler FROM @tmpReportDataMonthDA

    OPEN csrLYNXHandlers

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrLYNXHandlers
    INTO @LYNXHandlerWorking

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        
        INSERT INTO @tmpReportDataMonthDA
        SELECT 'Month',
              'DA',
              tmp.Level3Group,
              @LYNXHandlerWorking,
              0, 0, 0, 0, 0, 
              0, 0, 0, 0
        FROM @tmpLevel3Group tmp
        WHERE Level3Group not in (SELECT distinct Level3Group 
                                    FROM @tmpReportDataMonthDA where LynxHandler = @LYNXHandlerWorking)

        FETCH next
        FROM csrLYNXHandlers
        INTO @LYNXHandlerWorking

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    
    END    

    CLOSE csrLYNXHandlers
    DEALLOCATE csrLYNXHandlers



    -- Final Select
    
    SELECT  @MonthName AS ReportMonth,
            @RptYear AS ReportYear,
            CASE tmp.Level3Group
              WHEN 'PS' THEN 'Program Shop'
              WHEN 'DR' THEN 'Desk Review'
              WHEN 'DA' THEN 'Desk Audit'
              WHEN 'IA' THEN 'Independent Adj.'
              WHEN 'SA' THEN 'Client Field Staff'
              WHEN 'TTL' THEN 'Total Assignments'
            END AS Level3Display,
            CASE tmp.Level3Group
              WHEN 'PS' THEN 1
              WHEN 'DR' THEN 2
              WHEN 'DA' THEN 3
              WHEN 'IA' THEN 4
              WHEN 'SA' THEN 5
              WHEN 'TTL' THEN 6
            END AS Level3Sort,
            tmp.*,
            dlh.UserNameLast + ', ' + dlh.UserNameFirst as LynxHandlerName,
            @DataWareHouseDate as DataWareDate
      FROM  @tmpReportDataMonthDA tmp
      LEFT JOIN utb_dtwh_dim_lynx_handler dlh ON (tmp.LynxHandler = dlh.LynxHandlerID)
    order by LynxHandlerName, Level3Sort
*/      

SELECT * FROM @tmpNewData
SELECT * FROM @tmpOpenData

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2OperClaimRepProd' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2OperClaimRepProd TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/