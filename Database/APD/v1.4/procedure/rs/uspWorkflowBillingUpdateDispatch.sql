-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowBillingUpdateDispatch' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowBillingUpdateDispatch 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowBillingUpdateDispatch
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates a payment or billing record with billing and dispatch information
*
* PARAMETERS:  
* (I) @UpdateID             Billing or Payment ID to update
* (I) @UpdateType           'B' - Billing, 'P' - Payment
* (I) @BillDate             The bill date
* (I) @DispatchNumber       The dispatch number
* (I) @UserID               User performing the update
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowBillingUpdateDispatch
    @InvoiceID          udt_std_id_big,
    @InvoiceDate        varchar(30),
    @DispatchNumber     udt_std_desc_short,
    @UserID             udt_std_id
AS
BEGIN
    -- This procedure updates a billing or payment record with Billing and Ingres Dispatch data.
    DECLARE @EarlyBillInvoice             bit
    DECLARE @DispatchNumberOld            varchar(50)
    DECLARE @InvoiceAmount                decimal(9, 2)
    DECLARE @ClaimAspectID                bigint
    DECLARE @ClaimAspectServiceChannelID  bigint
    DECLARE @NoteDescription              varchar(100)
    
    -- Declare internal variables

    DECLARE @now                            datetime
    DECLARE @error                          int
    DECLARE @rowcount                       int

    DECLARE @ProcName                       varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspWorkflowBillingUpdateDispatch'


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Check to make sure a valid UpdateId was passed in    
    IF  (@InvoiceID IS NULL) OR
        (NOT EXISTS(SELECT InvoiceID FROM dbo.utb_invoice WHERE InvoiceID = @InvoiceID))
    BEGIN
        -- Invalid Invoice ID
        RAISERROR('101|%s|@InvoiceID|%u', 16, 1, @ProcName, @InvoiceID)
        RETURN
    END
    
            
    -- Check to see if the @InvoiceDate parameter is valid
    IF IsDate(@InvoiceDate) <> 1
    BEGIN
        -- Invalid Invoice Date
        RAISERROR('101|%s|@InvoiceDate|%s', 16, 1, @ProcName, @InvoiceDate)
        RETURN
    END
    

    -- Validate @UserID    
    IF NOT EXISTS (SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User ID
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    SET @InvoiceAmount = 0
    SET @EarlyBillInvoice = 0

    IF EXISTS(SELECT i.InvoiceID
               FROM dbo.utb_invoice i
               LEFT JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
               LEFT JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
               LEFT JOIN dbo.utb_insurance ins ON c.InsuranceCompanyID = ins.InsuranceCompanyID
               WHERE i.InvoiceID = @InvoiceID
                 --AND i.ItemTypeCD = 'I'
                 AND ins.EarlyBillFlag = 1)
    BEGIN
       SET @EarlyBillInvoice = 1
    END
    
    
    SELECT @InvoiceAmount = Amount,
           @DispatchNumberOld = DispatchNumber,
           @ClaimAspectID = ClaimAspectID,
           @ClaimAspectServiceChannelID = ClaimAspectServiceChannelID
    FROM dbo.utb_invoice
    WHERE InvoiceID = @InvoiceID
    
    -- Begin Update
    BEGIN TRANSACTION BillingUpdateDispatchTran1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    
    -- Update the Invoice record    
    UPDATE  dbo.utb_invoice
      SET   StatusCD = 'FS',  -- Financial Services
            StatusDate = @InvoiceDate,
            DispatchNumber = @DispatchNumber,
            SentToIngresDate = @InvoiceDate,
            SysLastUserID = @UserID,
            SysLastUpdatedDate = @now
      WHERE InvoiceID = @InvoiceID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT


    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_invoice', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    IF @EarlyBillInvoice = 1
    BEGIN
      -- Is this an update to an old dispatch?
      IF @DispatchNumberOld IS NOT NULL AND @DispatchNumberOld <> @DispatchNumber
      BEGIN
         SET @NoteDescription = 'Dispatch Number updated [Old: ' + @DispatchNumberOld + ', New: ' + @DispatchNumber +']'
         -- update the old dispatch to point to the new one
         INSERT INTO dbo.utb_invoice_dispatch (
            Amount,
            Description,
            DispatchNumber,
            DisptachNumberNew,
            TransactionCD,
            TransactionDate,
            SysLastUserID,
            SysLastUpdatedDate
         ) VALUES (
            0,
            @NoteDescription,
            @DispatchNumberOld,
            @DispatchNumber,
            'A',
            @now,
            @UserID,
            @now
         )
         
         -- Add a private note to the claim
         EXEC uspNoteInsDetail @ClaimAspectID = @ClaimAspectID,
                               @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                               @NoteTypeID = 15,
                               @StatusID = 500,
                               @Note = @NoteDescription,
                               @UserID = @UserID,
                               @PrivateFlag = 1
      END

    END

      -- add the current transaction the dispatch history
      INSERT INTO dbo.utb_invoice_dispatch (
         Amount,
         Description,
         DispatchNumber,
         TransactionCD,
         TransactionDate,
         SysLastUserID,
         SysLastUpdatedDate
      ) VALUES (
         @InvoiceAmount,
         'Billing information sent to AGC',
         @DispatchNumber,
         'Q',
         @now,
         @UserID,
         @now
      )

    COMMIT TRANSACTION BillingUpdateDispatchTran1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    

    RETURN @rowcount 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowBillingUpdateDispatch' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowBillingUpdateDispatch TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/