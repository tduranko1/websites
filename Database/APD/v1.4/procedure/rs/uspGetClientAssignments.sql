-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientAssignments' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClientAssignments 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetClientAssignments
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all client assignments from the database by InsuranceCompanyID
*
* PARAMETERS:  
*				InsuranceCompanyID 
* RESULT SET:
*   All data related to insurance client assignments
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetClientAssignments
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT 
		cat.InsuranceCompanyID
		, cat.AssignmentTypeID
		, ISNULL(at.DisplayOrder,'') DisplayOrder
		, ISNULL(at.EnabledFlag,'') EnabledFlag
		, ISNULL(at.Name,'') AssignmentName
		, ISNULL(at.ServiceChannelDefaultCD,'') ServiceChannelDefaultCD
		, ISNULL(at.SysMaintainedFlag,'') SysMaintainedFlag 
		, cat.SysLastUserID
		, cat.SysLastUpdatedDate
		, csc.ClientAuthorizesPaymentFlag
		, csc.InvoiceModelBillingCD
	FROM
		utb_client_assignment_type cat
		INNER JOIN utb_assignment_type at
		ON at.AssignmentTypeID = cat.AssignmentTypeID
		INNER JOIN utb_client_service_channel csc
		ON csc.InsuranceCompanyID = cat.InsuranceCompanyID
	WHERE 
		at.EnabledFlag = 1	 
		AND cat.InsuranceCompanyID = @iInsuranceCompanyID
	ORDER BY
		at.[Name] 
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientAssignments' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClientAssignments TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClientAssignments TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/