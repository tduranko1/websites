-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceProcessDiaryTask' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceProcessDiaryTask 
END

GO

