-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocProcLog' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDocProcLog 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspDocProcLog
* SYSTEM:       Lynx Services APD
* AUTHOR:       Sarah Liss
* FUNCTION:     Get Document Processor Log
*
* PARAMETERS:  
*	None
*   (@InsuranceCompanyID - ID of the insurance company)
*
* RESULT SET:
*   Document Processor Log Records
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspDocProcLog
    --@InsuranceCompanyID      udt_std_id_big
    --, @AnalystUserID		 udt_std_int
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
        
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspDocProcLog'
    
    -- Set Database options
    
    SET NOCOUNT ON
    
	SELECT TOP 100 *
	FROM
		 dbo.utb_document_processor_log
	ORDER BY SysLastUpdatedDate desc
	
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocProcLog' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDocProcLog TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspDocProcLog TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/