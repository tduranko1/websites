-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspIMPSGetAssignments' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspIMPSGetAssignments 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspIMPSGetAssignments
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jeffery A. Lund
* FUNCTION:     Returns a recordset containing assignments for a given claim aspect
*
* PARAMETERS:  
* (I) @ClaimAspectID          ClaimAspectID for vehicle (use either this or the combination @lynxid/@PertainsTo
* (I) @LynxID                 LynxID for vehicle (use in conjunction with @PertainsTo or use @ClaimAspectID)
* (I) @PertainsTo             Vehicle Number for vehicle (use in conjunction with @LynxID or use @ClaimAspectID)
*
* RESULT SET:
*     AssignmentID            AssignmentID
*     AssignmentSuffix        Code for assignment, found on incoming estimates
*     Name                    Name of shop location of assignment
*     AddressCity             City of shop location of assignment
*     AddressState            State of shop location of assignment
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspIMPSGetAssignments
    @ClaimAspectID              udt_std_id_big      = Null,
    @LynxID                     udt_std_id_big      = Null,
    @PertainsTo                 varchar(8)          = Null
AS
BEGIN
    DECLARE @ProcName           varchar(25)
    DECLARE @AspectTypeID       udt_std_id
    DECLARE @AspectNumber       udt_std_int
    
    SET @ProcName = 'uspIMPSGetAssignments'
    SET CONCAT_NULL_YIELDS_NULL off
    
    
    --Verify @ClaimAspectID, If Null, attpemt to get from @LynxID & @ClaimAspectNumber
    
    IF @ClaimAspectID IS NULL
    BEGIN
        SELECT  @AspectTypeID = AspectTypeID,
                @AspectNumber = AspectNumber
        FROM    dbo.ufnUtilityGetClaimAspect (@PertainsTo)

        SELECT 
            @ClaimAspectID = ClaimAspectID
            
        FROM
            dbo.utb_claim_aspect CA
            
        WHERE
            CA.LynxID = @LynxID
            AND CA.ClaimAspectTypeID = @AspectTypeID
            AND CA.ClaimAspectNumber = @AspectNumber
        
                
        IF @ClaimAspectID IS NULL
        BEGIN
            -- Invalid @LynxID

            --RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
            RETURN
        END
    END
    ELSE
    BEGIN
        IF NOT EXISTS (Select ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID)
        BEGIN
            -- Invalid Assignment ID

            --RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
            RETURN
        END
    END

    SELECT
        A.AssignmentID,
        'Name' =
        Case 
            WHEN A.AssignmentSuffix is Null THEN SL.Name
            ELSE SL.Name + ': ' + A.AssignmentSuffix
        END
        
    FROM
        dbo.utb_claim_aspect CA
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Added utb_Claim_Aspect_Service_Channel to provide a join between the 
		tables utb_claim_aspect and utb_Assignment for a ClaimAspectID
		M.A. 20061109
	*********************************************************************************/
	LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON casc.ClaimAspectID = ca.ClaimAspectID
        LEFT JOIN dbo.utb_Assignment A ON casc.ClaimAspectServiceChannelID = A.ClaimAspectServiceChannelID
        LEFT JOIN dbo.utb_shop_location SL ON A.ShopLocationID = SL.ShopLocationID
        
    WHERE
        CA.ClaimAspectID = @ClaimAspectID

    ORDER BY A.AssignmentID

    IF @@Error <> 0
    BEGIN
        -- SQL Error

        RAISERROR('99|%s|', 16, 1, @ProcName)
        RETURN
    END

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspIMPSGetAssignments' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspIMPSGetAssignments TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/