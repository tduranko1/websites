-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptActivityFootprint' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptActivityFootprint
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptActivityFootprint
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Provides a detailed log of staff activity in the APD system
*
* PARAMETERS:
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptActivityFootprint
    @ReportDate AS udt_std_datetime = NULL,
    @LBoundDate AS udt_std_datetime = NULL,
    @UBoundDate AS udt_std_datetime = NULL
AS
BEGIN
    DECLARE @tmpActivity TABLE
    (
        UserID          int,
        ActivityDate    datetime,
        ClaimAspectID   int,
        LynxID          int,
        VehicleNumber   int,
        Description     varchar(500)
    )

    -- Set Database Options

    SET NOCOUNT ON


    -- If @ReportDate was not passed in, use yesterday (CURRENT_TIMESTAMP - 1 day)

    IF @ReportDate IS NULL
    BEGIN
        SET @ReportDate = DateDiff(d, 1, CURRENT_TIMESTAMP)
    END


    -- Set LBound and UBound

    IF @LBoundDate IS NULL
    BEGIN
        SET @LBoundDate = Convert(datetime, Convert(varchar(12), @ReportDate, 101) + ' 0:00:00')
    END

    IF @UBoundDate IS NULL
    BEGIN
        SET @UBoundDate = Convert(datetime, Convert(varchar(12), @ReportDate, 101) + ' 23:59:59')
    END


    -- Begin collecting activity for users

    DECLARE @tmpUsers TABLE
    (
        UserID      bigint
    )


    INSERT INTO @tmpUsers
      SELECT UserID FROM dbo.utb_user
        WHERE UserID IN (SELECT  UserID
                           FROM dbo.utb_user
                           WHERE OfficeID IS NULL
                             AND EnabledFlag = 1
                             AND UserID <> 0)


    -- Start with the claim history log first

    INSERT INTO @tmpActivity
      SELECT   hl.CompletedUserID,
               hl.CompletedDate,
               ca.ClaimAspectID,
               hl.LynxID,
               hl.ClaimAspectNumber,
               CASE hl.LogType
                 WHEN 'T' THEN 'Task Completed: ' + IsNull(t.Name, '')
                 ELSE IsNull(Description, '')
               END
      FROM     dbo.utb_history_log hl
      LEFT JOIN dbo.utb_task t ON (hl.TaskID = t.TaskID)
      LEFT JOIN dbo.utb_claim c ON (hl.LynxID = c.LynxID)
      LEFT JOIN dbo.utb_claim_aspect ca ON
        (hl.LynxID = ca.LynxID AND 9 = ca.ClaimAspectTypeID
            AND hl.ClaimAspectNumber = ca.ClaimAspectNumber)
      WHERE    hl.CompletedDate BETWEEN @LBoundDate AND @UBoundDate
        AND    hl.CompletedUserID IN (SELECT UserID FROM @tmpUsers)
        AND    c.DemoFlag = 0

    -- Look for tasks that have been created

    INSERT INTO @tmpActivity
      SELECT   c.CreatedUserID,
               c.CreatedDate,
               ca.ClaimAspectID,
               ca.LynxID,
               ca.ClaimAspectNumber,
               'Task Created: ' + IsNull(t.Name, '')
      FROM     dbo.utb_checklist c
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc
        ON (c.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
      LEFT JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim cl ON (ca.LynxID = cl.LynxID)
      LEFT JOIN dbo.utb_task t ON (c.TaskID = t.TaskID)
      WHERE    c.CreatedDate BETWEEN @LBoundDate AND @UBoundDate
        AND    c.CreatedUserID IN (SELECT UserID FROM @tmpUsers)
        AND    cl.DemoFlag = 0


    -- Now look for added notes

    INSERT INTO @tmpActivity
      SELECT   d.CreatedUserID,
               d.CreatedDate,
               ca.ClaimAspectID,
               ca.LynxID,
               ca.ClaimAspectNumber,
               'Note Created: Type ' + IsNull(nt.Name, '')
      FROM     dbo.utb_document d
      LEFT JOIN dbo.utb_claim_aspect_service_channel_document cad
        ON (d.DocumentID = cad.DocumentID)
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc
        ON (casc.ClaimAspectServiceChannelID = cad.ClaimAspectServiceChannelID)
      LEFT JOIN dbo.utb_claim_aspect ca
        ON (casc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim c
        ON (ca.LynxID = c.LynxID)
      LEFT JOIN dbo.utb_note_type nt
        ON (d.NoteTypeID = nt.NoteTypeID)
      LEFT JOIN dbo.utb_document_type dt
        ON (d.DocumentTypeID = dt.DocumentTypeID)
      WHERE    d.CreatedDate BETWEEN @LBoundDate AND @UBoundDate
        AND    d.CreatedUserID IN (SELECT UserID FROM @tmpUsers)
        AND    c.DemoFlag = 0
        AND    dt.Name = 'Note'



    -- Repair History Changes

    INSERT INTO @tmpActivity
      SELECT   wh.CreatedUserID,
               wh.ChangeDate,
               ca.ClaimAspectID,
               ca.LynxID,
               ca.ClaimAspectNumber,
               'Work Information Changed: ' + IsNull(wh.ChangeComment, '')
      FROM     dbo.utb_service_channel_work_history wh
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc
        ON (casc.ClaimAspectServiceChannelID = wh.ClaimAspectServiceChannelID)
      LEFT JOIN dbo.utb_claim_aspect ca
        ON (casc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim c
        ON (ca.LynxID = c.LynxID)
      WHERE    wh.ChangeDate BETWEEN @LBoundDate AND @UBoundDate
        AND    wh.CreatedUserID IN (SELECT UserID FROM @tmpUsers)
        AND    c.DemoFlag = 0


    -- Now added fees

    INSERT INTO @tmpActivity
      SELECT   i.RecordingUserID,
               i.EntryDate,
               ca.ClaimAspectID,
               ca.LynxID,
               ca.ClaimAspectNumber,
               'Fee Created: ' + IsNull(i.Description, '')
      FROM     dbo.utb_invoice i
      LEFT JOIN dbo.utb_claim_aspect ca ON (i.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
      WHERE    i.EntryDate BETWEEN @LBoundDate AND @UBoundDate
        AND    i.RecordingUserID IN (SELECT UserID FROM @tmpUsers)
        AND    c.DemoFlag = 0


    -- Final Selects

    SELECT Convert(varchar, @LBoundDate, 100) + ' - ' + Convert(varchar, @UBoundDate, 100) AS 'Activity Date'


    SELECT  Convert(varchar(50), u.NameLast + ', ' + u.NameFirst) AS 'Activity Performed By',
            Convert(varchar(10), tmp.ActivityDate, 8) AS Occurred,
            Convert(varchar(10), tmp.LynxID) + '-' + Convert(varchar(2), tmp.VehicleNumber) AS LynxID,
            i.Name AS 'Insurance Company',
            IsNull(casc.ServiceChannelCD, '') AS 'Service Channel',
            CASE
              WHEN casc.ServiceChannelCD IN ('DA', 'DR') THEN IsNull(Convert(varchar(50), au.NameLast + ', ' + au.NameFirst), '')
              ELSE IsNull(Convert(varchar(50), ou.NameLast + ', ' + ou.NameFirst), '')
            END AS 'File Assigned To',
            Convert(varchar(150), tmp.Description) AS Activity,
            1 AS 'Task Count'
      FROM  @tmpActivity tmp
      LEFT JOIN dbo.utb_user u ON (tmp.UserID = u.UserID)
      LEFT JOIN dbo.utb_claim c ON (tmp.LynxID = c.LynxID)
      LEFT JOIN dbo.utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID)
      LEFT JOIN dbo.utb_claim_aspect ca ON (tmp.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (casc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_user ou ON (ca.OwnerUserID = ou.UserID)
      LEFT JOIN dbo.utb_user au ON (ca.AnalystUserID = au.UserID)
      ORDER BY u.NameLast, u.NameFirst, tmp.ActivityDate

    IF @@ERROR <> 0
    BEGIN

        RAISERROR('SQL Error', 16, 1)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptActivityFootprint' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptActivityFootprint TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/