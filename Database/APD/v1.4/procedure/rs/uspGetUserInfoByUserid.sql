-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetUserInfoByUserid' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetUserInfoByUserid 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetUserInfoByUserid
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all user information by userid
*
* PARAMETERS:  
*				UseridID
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetUserInfoByUserid
	@iUserID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT TOP 1
		u.UserID
	    ,ISNULL(u.OfficeID,0) as OfficeID
        ,ISNULL(u.SupervisorUserID,0) AS SupervisorUserID
        ,ISNULL(u.AssignmentBeginDate,'1900-01-01') AS AssignmentBeginDate
        ,ISNULL(u.AssignmentEndDate,'1900-01-01') AS AssignmentEndDate
        ,ISNULL(u.ClientUserId,0) AS ClientUserId
        ,ISNULL(u.EmailAddress,'') AS EmailAddress
        ,u.EnabledFlag
        ,ISNULL(u.FaxAreaCode,'') AS FaxAreaCode 
        ,ISNULL(u.FaxExchangeNumber,'') AS FaxExchangeNumber
        ,ISNULL(u.FaxExtensionNumber,'') AS FaxExtensionNumber
        ,ISNULL(u.FaxUnitNumber,'') AS FaxUnitNumber
        ,ISNULL(u.LastAssignmentDate,'') AS LastAssignmentDate
        ,u.NameFirst
        ,u.NameLast
        ,ISNULL(u.NameTitle,'') AS NameTitle
        ,ISNULL(u.OperatingFridayEndTime,'00:00:00') AS OperatingFridayEndTime
        ,ISNULL(u.OperatingFridayStartTime,'00:00:00') AS OperatingFridayStartTime
        ,ISNULL(u.OperatingMondayEndTime,'00:00:00') AS OperatingMondayEndTime
        ,ISNULL(u.OperatingMondayStartTime,'00:00:00') AS OperatingMondayStartTime
        ,ISNULL(u.OperatingSaturdayEndTime,'00:00:00') AS OperatingSaturdayEndTime
        ,ISNULL(u.OperatingSaturdayStartTime,'00:00:00') AS OperatingSaturdayStartTime
        ,ISNULL(u.OperatingSundayEndTime,'00:00:00') AS OperatingSundayEndTime
        ,ISNULL(u.OperatingSundayStartTime,'00:00:00') AS OperatingSundayStartTime
        ,ISNULL(u.OperatingThursdayEndTime,'00:00:00') AS OperatingThursdayEndTime
        ,ISNULL(u.OperatingThursdayStartTime,'00:00:00') AS OperatingThursdayStartTime
        ,ISNULL(u.OperatingTuesdayEndTime,'00:00:00') AS OperatingTuesdayEndTime
        ,ISNULL(u.OperatingTuesdayStartTime,'00:00:00') AS OperatingTuesdayStartTime
        ,ISNULL(u.OperatingWednesdayEndTime,'00:00:00') AS OperatingWednesdayEndTime
        ,ISNULL(u.OperatingWednesdayStartTime,'00:00:00') AS OperatingWednesdayStartTime
        ,ISNULL(u.PhoneAreaCode,'') AS PhoneAreaCode
        ,ISNULL(u.PhoneExchangeNumber,'') AS PhoneExchangeNumber
        ,ISNULL(u.PhoneExtensionNumber,'') AS PhoneExtensionNumber
        ,ISNULL(u.PhoneUnitNumber,'') AS PhoneUnitNumber
        ,u.SupervisorFlag
        ,u.SysLastUserID
        ,u.SysLastUpdatedDate
        ,ISNULL(u.CCCOneCommunicationAddress,'') AS CCCOneCommunicationAddress
        ,ISNULL(u.ReceiveCCCOneAssignmentFlag,'') AS ReceiveCCCOneAssignmentFlag
	FROM 
		utb_user u
		INNER JOIN utb_user_application ua
		ON ua.UserID = u.UserID
	WHERE
		u.UserID = @iUserID	
		AND ua.ApplicationID IN (1,2)
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetUserInfoByUserid' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetUserInfoByUserid TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetUserInfoByUserid TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/