-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTValidateInfo' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTValidateInfo 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTValidateInfo
* SYSTEM:       Lynx Services APD
* AUTHOR:       [Ramesh Vishegu]
* FUNCTION:     [This procedure will validate for duplicate information and valid referential information.
*                When saving data from the shop maintenance, errors are raised if referential information 
*                are incorrect and this does not stop the saving operation. Using this proc, we validate the
*                referential info and make sure that error will not be raised by this. FYI: required info are
*                validated before saving.]
*
* PARAMETERS:  
* (I) @FedTaxId             Federal Tax ID
* (I) @ZipCode              Zip Code
* (I) @ShopId               Shop ID
*
* RESULT SET:
* ValidFedTaxId             this is 1 if Fed Tax ID is valid, else 0
* ValidZipCode              this is 1 if ZipCode is valid, else 0
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTValidateInfo
    @FedTaxId       udt_fed_tax_id,
    @ZipCode        udt_addr_zip_code=NULL,
    @ShopId         udt_std_id_big=NULL
AS
BEGIN

    -- Declare internal variables

    declare @validFed as int
    declare @validZip as int

    set @validFed = 0
    set @validZip = 0
    
    IF (@FedTaxId is not null) and (LEN(@FedTaxId) > 0)
    BEGIN
      SET @FedTaxId = (SELECT dbo.ufnUtilitySquishString(@FedTaxId, 0, 1, 0, null))
      IF (@ShopID IS NOT NULL)
      BEGIN
        IF (NOT EXISTS (SELECT FedTaxId FROM utb_shop WHERE FedTaxId = @FedTaxId 
                                                        AND ShopID <> @ShopId 
                                                        AND EnabledFlag = 1))  
        set @validFed = 1
      END
      ELSE
      BEGIN
        IF (NOT EXISTS (SELECT FedTaxId FROM utb_shop WHERE FedTaxId = @FedTaxId 
                                                        AND EnabledFlag = 1))  
        set @validFed = 1
      END
   END
    
    --    if (@ZipCode is not null) and (EXISTS (SELECT Zip FROM utb_zip_code WHERE Zip = @ZipCode))
        set @validZip = 1
    
    
    
    SELECT @validFed as ValidFedTaxID, @validZip as ValidZipCode

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTValidateInfo' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTValidateInfo TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/