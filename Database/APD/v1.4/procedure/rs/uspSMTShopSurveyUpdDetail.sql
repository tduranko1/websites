-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopSurveyUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopSurveyUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopSurveyUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Updates Location Pricing Info.  
*
* PARAMETERS:  
* (I) @Shop                                 The location's identity
*
* RESULT SET:       
*       An XML document containing last updated date time values for Involved
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopSurveyUpdDetail
    @ShopID                                  udt_std_id_big,
    @Age                                     varchar(10)=null,
    @ASEFlag                                 char(1)=null,
    @BusinessRegsFlag                        udt_std_flag=null,
    @BusinessStabilityFlag                   udt_std_flag=null,
    @CommercialLiabilityFlag                 udt_std_flag=null,
    @CompetentEstimatorsFlag                 udt_std_flag=null,
    @CustomaryHoursFlag                      udt_std_flag=null,
    @DigitalPhotosFlag                       udt_std_flag=null,
    @EFTFlag                                 udt_std_flag=null,
    @ElectronicEstimatesFlag                 udt_std_flag=null,
    @EmployeeEducationFlag                   udt_std_flag=null,
    @EmployerLiabilityFlag                   udt_std_flag=null,
    @FrameAnchoringPullingEquipFlag          udt_std_flag=null,
    @FrameDiagnosticEquipFlag                udt_std_flag=null,
    @HazMatFlag                              udt_std_flag=null,
    @HydraulicLiftFlag                       char(1)=null,
    @ICARFlag                                char(1)=null,
    @MechanicalRepairFlag                    udt_std_flag=null,
    @MIGWeldersFlag                          udt_std_flag=null,
    @MinimumWarrantyFlag                     udt_std_flag=null,
    @NoFeloniesFlag                          udt_std_flag=null,
    @ReceptionistFlag                        udt_std_flag=null,
    @RefinishCapabilityFlag                  udt_std_flag=null,
    @RefinishTrainedFlag                     char(1)=null,
    @SecureStorageFlag                       char(1)=null,
    @ValidEPALicenseFlag                     char(1)=null,
    @VANInetFlag                             udt_std_flag=null,
    @WorkersCompFlag                         udt_std_flag=null,
    @SysLastUserID                           udt_std_id,
    @SysLastUpdatedDate                      varchar(30),
    @ApplicationCD                           udt_std_cd='APD'
AS
BEGIN
    
-- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @tupdated_date     AS datetime 
    DECLARE @temp_updated_date AS datetime
    DECLARE @now               AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspSMTShopSurveyUpdDetail'


    -- Set Database options
    
    SET NOCOUNT ON


    -- Validate the updated date parameter
    
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_shop_location_survey', @ShopID

    if @@error <> 0
    BEGIN
        RETURN
    END
    
    
    -- Check to make sure a valid User id was passed in

    IF  (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
    BEGIN
        -- Invalid User ID
    
        RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END
    
    
    -- initialize any empty string parameters
    IF (LEN(LTRIM(RTRIM(@Age))) = 0) SET @Age = NULL
    IF (LEN(LTRIM(RTRIM(@ASEFlag))) = 0) SET @ASEFlag = NULL
    IF (LEN(LTRIM(RTRIM(@HydraulicLiftFlag))) = 0) SET @HydraulicLiftFlag = NULL
    IF (LEN(LTRIM(RTRIM(@ICARFlag))) = 0) SET @ICARFlag = NULL
    IF (LEN(LTRIM(RTRIM(@RefinishTrainedFlag))) = 0) SET @RefinishTrainedFlag = NULL
    IF (LEN(LTRIM(RTRIM(@SecureStorageFlag))) = 0) SET @SecureStorageFlag = NULL
    IF (LEN(LTRIM(RTRIM(@ValidEPALicenseFlag))) = 0) SET @ValidEPALicenseFlag = NULL
        
    
    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Begin Update

    BEGIN TRANSACTION LocationSurveyUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    UPDATE  dbo.utb_shop_location_survey
    SET Age = @Age,
        ASEFlag = convert(bit, @ASEFlag),
        BusinessRegsFlag = @BusinessRegsFlag,
        BusinessStabilityFlag = @BusinessStabilityFlag,
        CommercialLiabilityFlag = @CommercialLiabilityFlag,
        CompetentEstimatorsFlag = @CompetentEstimatorsFlag,
        CustomaryHoursFlag = @CustomaryHoursFlag,
        DigitalPhotosFlag = @DigitalPhotosFlag,
        EFTFlag = @EFTFlag,
        ElectronicEstimatesFlag = @ElectronicEstimatesFlag,
        EmployeeEducationFlag = @EmployeeEducationFlag,
        EmployerLiabilityFlag = @EmployerLiabilityFlag,
        FrameAnchoringPullingEquipFlag = @FrameAnchoringPullingEquipFlag,
        FrameDiagnosticEquipFlag = @FrameDiagnosticEquipFlag,
        HazMatFlag = @HazMatFlag,
        HydraulicLiftFlag = convert(bit, @HydraulicLiftFlag),
        ICARFlag = convert(bit, @ICARFlag),
        MechanicalRepairFlag = @MechanicalRepairFlag,
        MIGWeldersFlag = @MIGWeldersFlag,
        MinimumWarrantyFlag = @MinimumWarrantyFlag,
        NoFeloniesFlag = @NoFeloniesFlag,
        ReceptionistFlag = @ReceptionistFlag,
        RefinishCapabilityFlag = @RefinishCapabilityFlag,
        RefinishTrainedFlag = convert(bit, @RefinishTrainedFlag),
        SecureStorageFlag = convert(bit, @SecureStorageFlag),
        ValidEPALicenseFlag = convert(bit, @ValidEPALicenseFlag),
        VANInetFlag = @VANInetFlag,
        WorkersCompFlag = @WorkersCompFlag,
        SysLastUserID = @SysLastUserID,
        SysLastUpdatedDate = @now 
    WHERE 
        ShopLocationID = @ShopID AND
	      SysLastUpdatedDate = @SysLastUpdatedDate

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_shop_location_survey', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION LocationSurveyUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
    

    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Insured Level
            NULL AS [Survey!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Insured Level
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing

IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    return @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopSurveyUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopSurveyUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/