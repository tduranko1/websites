-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLLoadClaimCoverage' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspFNOLLoadClaimCoverage 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspFNOLLoadClaimCoverage
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
* Updates:
* 01Oct2012 - Added EXEC user wsAPDUser - so that the Web Service and execute this SP
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspFNOLLoadClaimCoverage
    @LynxID                         udt_std_int_big,
    @ClientCode                     udt_std_desc_short = NULL,
    @ClientCoverageTypeID           udt_std_id = NULL,
    @CoverageTypeCD                 udt_std_cd = NULL,
    @DeductibleAmt                  udt_std_money = NULL,
    @LimitAmt                       udt_std_money = NULL,
    @LimitDailyAmt                  udt_std_money = NULL,
    @MaximumDays                    udt_std_int_small = NULL
AS
BEGIN
    IF LEN(RTRIM(LTRIM(@ClientCode))) = 0 SET @ClientCode = NULL
    IF LEN(RTRIM(LTRIM(@CoverageTypeCD))) = 0 SET @CoverageTypeCD = NULL
    
    DECLARE @ProcName   AS varchar(30)
    
    SET @ProcName = 'uspFNOLLoadClaimCoverage'
    
    
    INSERT INTO utb_fnol_claim_coverage_load
    (
        LynxID,       
        ClientCode,
        ClientCoverageTypeID,
        CoverageTypeCD,
        DeductibleAmt,
        LimitAmt,
        LimitDailyAmt,
        MaximumDays
    )
    VALUES
    (
        @LynxID,
        @ClientCode,
        @ClientCoverageTypeID,
        @CoverageTypeCD,
        @DeductibleAmt,
        @LimitAmt,
        @LimitDailyAmt,
        @MaximumDays
    )

    IF @@ERROR <> 0
    BEGIN
       -- Insertion Error
    
        RAISERROR('%s: Error inserting into utb_fnol_claim_coverage_load', 16, 1, @ProcName)
        RETURN
    END    
        
        
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLLoadClaimCoverage' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspFNOLLoadClaimCoverage TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspFNOLLoadClaimCoverage TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/