-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopBillingInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopBillingInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopBillingInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Insert a new Shop Location Billing record
*
*
* RESULT SET:
* BillingID                               The newly created Shop Location billing unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopBillingInsDetail
(
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
	  @EFTAccountNumber                   udt_std_desc_short=NULL,
	  @EFTAccountTypeCD                   udt_std_cd=NULL,
	  @EFTContractSignedFlag              udt_std_flag=0,
	  @EFTEffectiveDate                   VARCHAR(30)=NULL,
	  @EFTRoutingNumber                   udt_std_desc_short=NULL,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @Name                               udt_std_name,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd='APD',
    @BillingID							udt_std_id = NULL OUTPUT
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error              udt_std_int
    DECLARE @rowcount           udt_std_int

    DECLARE @tEFTEffectiveDate  udt_std_datetime
    
    DECLARE @ShopBillingID      udt_std_id_big

    DECLARE @ProcName           varchar(30)       -- Used for raise error stmts 
    
    DECLARE @LogComment         udt_std_desc_long,
            @newValue           udt_std_desc_long

    SET @ProcName = 'uspSMTShopBillingInsDetail'


    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0 SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@EFTAccountNumber))) = 0 SET @EFTAccountNumber = NULL
    IF LEN(RTRIM(LTRIM(@EFTAccountTypeCD))) = 0 SET @EFTAccountTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@EFTEffectiveDate))) = 0 SET @EFTEffectiveDate = NULL
    IF LEN(RTRIM(LTRIM(@EFTRoutingNumber))) = 0 SET @EFTRoutingNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL


    -- Apply edits

    IF @EFTEffectiveDate IS NOT NULL AND
       ISDATE(@EFTEffectiveDate) = 0
    BEGIN
        -- Convert the value passed in effective date into date format
        
        SET @tEFTEffectiveDate = CAST(@EFTEffectiveDate AS DATETIME)
        
        -- Validate the effective date
        
        IF @tEFTEffectiveDate IS NULL
        BEGIN 
            -- Invalid effective date value
        
            RAISERROR('101|%s|@EFTEffectiveDate|%s', 16, 1, @ProcName, @EFTEffectiveDate)
            RETURN
        END 
    END
    
    IF @EFTAccountTypeCD IS NOT NULL
    BEGIN
        IF @EFTAccountTypeCD NOT IN ('S', 'C')
        BEGIN
           -- Invalid Account Type
            RAISERROR('101|%s|@EFTAccountTypeCD|%s', 16, 1, @ProcName, @EFTAccountTypeCD)
            RETURN
        END
    END
    
    IF LEN(@Name) < 1
    BEGIN
       -- Missing Billing Name
        RAISERROR('101|%s|@Name|%s', 16, 1, @ProcName, @Name)
        RETURN
    END

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Billing Address State
            RAISERROR('101|%s|@AddressState|%s', 16, 1, @ProcName, @AddressState)
            RETURN
        END
    END

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    -- Begin Update(s)

    BEGIN TRANSACTION AdmBillingInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_billing
   	(
        Address1,
        Address2,
        AddressCity,
        AddressCounty,
        AddressState,
        AddressZip,
		    EFTAccountNumber,
		    EFTAccountTypeCD,
		    EFTContractSignedFlag,
		    EFTEffectiveDate,
		    EFTRoutingNumber,
        FaxAreaCode,
        FaxExchangeNumber,
        FaxExtensionNumber,
        FaxUnitNumber,
        Name,
        PhoneAreaCode,
        PhoneExchangeNumber,
        PhoneExtensionNumber,
        PhoneUnitNumber,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES	
    (
        @Address1,
        @Address2,
        @AddressCity,
        @AddressCounty,
        @AddressState,
        @AddressZip,
		    @EFTAccountNumber,
		    @EFTAccountTypeCD,
		    @EFTContractSignedFlag,
		    @EFTEffectiveDate,
		    @EFTRoutingNumber,
        @FaxAreaCode,
        @FaxExchangeNumber,
        @FaxExtensionNumber,
        @FaxUnitNumber,
        @Name,
        @PhoneAreaCode,
        @PhoneExchangeNumber,
        @PhoneExtensionNumber,
        @PhoneUnitNumber,
        @SysLastUserID,
        CURRENT_TIMESTAMP
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    SET @ShopBillingID = SCOPE_IDENTITY()
    SET @BillingID = @ShopBillingID
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_billing', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    -- Audit Log code  ---------------------------------------------------------------------------------------
    
    
    
    SET @LogComment = 'Payment Location created: ' + @Name + ' (ID: ' + convert(varchar(10), @ShopBillingID) + ')'
    
    EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
            
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
        ROLLBACK TRANSACTION
        RETURN
    END  
    
    
    IF @Address1 IS NOT NULL
    BEGIN   
      SET @LogComment = 'Payment Address 1 established: ' + @Address1
  
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF @Address2 IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Address 2 established: ' + @Address2
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @AddressZip IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Zip Code established: ' + @AddressZip
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF @AddressCity IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment City established: ' + @AddressCity
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @AddressState IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment State established: ' + @AddressState
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
    
    
    IF @AddressCounty IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment County established: ' + @AddressCounty
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
    
    
    IF @PhoneAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Phone Area Code established ' + @PhoneAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF @PhoneExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Phone Exchange Number established: ' + @PhoneExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PhoneUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Phone Unit Number establisned: ' + @PhoneUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PhoneExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Phone Extension Number established: ' + @PhoneExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Fax Area Code established: ' + @FaxAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    
    IF @FaxExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Fax Exchange Number established: ' + @FaxExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Fax Unit Number established: ' + @FaxUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Payment Fax Extension Number established: ' + @FaxExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
         
    
    IF @EFTContractSignedFlag IS NOT NULL
    BEGIN
      
      SET @LogComment = 'EFT Contract Signed Flag established: ' + (SELECT CASE @EFTContractSignedFlag WHEN 0 THEN 'ON' ELSE 'OFF' END)
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    
    IF @EFTAccountNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'EFT Account Number established: ' + @EFTAccountNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @EFTAccountTypeCD IS NOT NULL
    BEGIN
      SET @newValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_billing', 'EFTAccountTypeCD') WHERE Code = @EFTAccountTypeCD)
      
      SET @LogComment = 'EFT Account Type established: ' + ISNULL(CONVERT(varchar(50), @newValue), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
    
    
    IF @EFTRoutingNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'EFT Routing Number established ' + @EFTRoutingNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
    
    
    IF @EFTEffectiveDate IS NOT NULL
    BEGIN
            
      SET @LogComment = 'EFT Effective Date established: ' + CONVERT(varchar(12), @EFTEffectiveDate)
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopBillingID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
         
      
   -- End of Audit Log code  --------------------------------------------------------------------------------
       

    COMMIT TRANSACTION AdmBillingInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ShopBillingID AS ShopBillingID

    RETURN @rowcount
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopBillingInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopBillingInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO