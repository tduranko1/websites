﻿USE [udb_apd]
GO
BEGIN TRANSACTION
-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRPT2WeeklyAssignmentStatus' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRPT2WeeklyAssignmentStatus 
END

GO

/****** Object:  StoredProcedure [dbo].[uspRPT2WeeklyAssignmentStatus]    Script Date: 01/27/2020 18:56:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

    
/************************************************************************************************************************  
*  
* PROCEDURE:    uspRPT2WeeklyAssignmentStatus  
* SYSTEM:          
* AUTHOR:       Mahes  
* FUNCTION:     Return LYNX APD: Cincinnati DRP Assignment Report  
*  
* PARAMETERS:    
*  
* RESULT SET: None  
*   
* RETURNS:  Recordset of non drivable claim information  
*  
*  
* VSS  
* $Workfile: $  
* $Archive:  $  
* $Revision: $  
* $Author:   $  
* $Date:     $  
*  
************************************************************************************************************************/  
  
-- Create the stored procedure  
  
  
CREATE PROCEDURE [dbo].[uspRPT2WeeklyAssignmentStatus]  
@InsuranceCompanyID     udt_std_id
AS  
BEGIN  
    set nocount on  
  
   declare @StartDate as datetime  
    declare @EndDate as datetime  
  
    set @EndDate = convert(datetime, convert(varchar, current_timestamp, 110))  
    set @StartDate = dateadd(day, -1, dateadd(week, -1, @EndDate)) 

SELECT    CONVERT(VARCHAR, c.IntakeFinishDate, 22) AS 'Date Assignment Sent',  
   c.ClientClaimNumber AS 'CIC Claim Number',  
   c.LynxID AS 'Lynx ID',  
   IsNull((SELECT   TOP 1 i.NameLast + ', ' + i.NameFirst  
     FROM dbo.utb_claim AS ci INNER JOIN  
                     dbo.utb_claim_aspect AS ca ON ci.LynxID = ca.LynxID INNER JOIN  
                     dbo.utb_claim_aspect_involved AS cai ON ca.ClaimAspectID = cai.ClaimAspectID INNER JOIN  
                     dbo.utb_involved AS i ON cai.InvolvedID = i.InvolvedID INNER JOIN  
                     dbo.utb_involved_role ON i.InvolvedID = dbo.utb_involved_role.InvolvedID INNER JOIN  
                     dbo.utb_involved_role_type AS irt ON dbo.utb_involved_role.InvolvedRoleTypeID = irt.InvolvedRoleTypeID  
                     WHERE irt.Name like 'insured'  
                 AND ci.LynxID = c.LynxID), '') AS InsuredName,  
                 cv.Make + ' - ' + cv.Model AS 'Vehicle Info',  
     uc.NameFirst +' '+ uc.NameLast AS 'Carrier Representative',  
    IsNull((SELECT top 1 i.NameLast + ', ' + i.NameFirst  
               FROM dbo.utb_involved i INNER JOIN dbo.utb_claim_aspect_involved cai ON i.InvolvedID = cai.InvolvedID  
                                       INNER JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID  
                                       INNER JOIN dbo.utb_involved_role_type irt On ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID  
               WHERE irt.Name = 'Owner'  
                 AND cai.ClaimAspectID = ca.ClaimAspectID), '') as OwnerName,  
   app.Name AS 'Claim Submission Method'    
 
FROM         dbo.utb_application AS app INNER JOIN  
                     dbo.utb_claim AS c INNER JOIN  
                     dbo.utb_claim_aspect AS ca ON c.LynxID = ca.LynxID ON app.ApplicationID = ca.SourceApplicationID INNER JOIN  
                     dbo.utb_claim_vehicle AS cv ON cv.ClaimAspectID = ca.ClaimAspectID INNER JOIN  
                     dbo.utb_user AS uc ON c.CarrierRepUserID = uc.UserID  
                     where c.InsuranceCompanyID = @InsuranceCompanyID  
     
      and ca.InitialAssignmentTypeID = 4 
      and c.IntakeFinishDate between @StartDate and @EndDate  
      order by c.IntakeFinishDate
  
END  
  

GO

--Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRPT2WeeklyAssignmentStatus' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRPT2WeeklyAssignmentStatus TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

