-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefSpecialtyInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefSpecialtyInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefSpecialtyInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Insert a new Specialty
*
* PARAMETERS:  
* (I) @DisplayOrder         Specialty display order
* (I) @EnabledFlag          Specialty active/inactive flag
* (I) @Name                 Specialty name
* (I) @SysLastUserID        Specialty user
*
* RESULT SET:
* SpecialtyID     The newly created Specialty unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefSpecialtyInsDetail
(
    @DisplayOrder                       udt_std_int_tiny,
    @EnabledFlag                        udt_std_flag,
    @Name                               udt_std_name,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd = 'APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @SpecialtyID AS udt_std_int_tiny

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspRefSpecialtyInsDetail'


    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT u.UserID 
                       FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                           INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
                       WHERE u.UserID = @SysLastUserID
                         AND @SysLastUserID <> 0
                         AND ua.AccessBeginDate IS NOT NULL
                         AND ua.AccessBeginDate <= CURRENT_TIMESTAMP
                         AND ua.AccessEndDate IS NULL
                         AND a.Code = @ApplicationCD)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('1|Invalid User', 16, 1, @ProcName)
        RETURN
    END


    SELECT @SpecialtyID = MAX(SpecialtyID) + 1
      FROM utb_specialty

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    BEGIN TRANSACTION RefSpecialtyInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_specialty
   	(
        SpecialtyID,
        DisplayOrder,
        EnabledFlag,
        Name,
        SysMaintainedFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES	
    (
        @SpecialtyID,
        @DisplayOrder,
        @EnabledFlag,
        @Name,
        0,
        @SysLastUserID,
        CURRENT_TIMESTAMP
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    COMMIT TRANSACTION RefSpecialtyInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    SELECT @SpecialtyID AS SpecialtyID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefSpecialtyInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefSpecialtyInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/