-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspVehicleRentalUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspVehicleRentalUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspVehicleRentalUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates rental information for the vehicle
*
* PARAMETERS:  
* (I) @ClaimAspectID            The claim aspect to update
* (I) @RentalDaysAuthorized     Rental Days Authorized
* (I) @RentalInstructions       Rental Instructions
* (I) @UserID                   The user updating the note
* (I) @SysLastUpdatedDate       The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspVehicleRentalUpdDetail
    @ClaimAspectID            udt_std_id_big,
    @RentalDaysAuthorized     udt_std_int_tiny,
    @RentalInstructions       udt_std_desc_mid,
    @UserID                   udt_std_id,
    @SysLastUpdatedDate       varchar(30)
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@RentalInstructions))) = 0 SET @RentalInstructions = NULL


    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspVehicleRentalUpdDetail'


    -- Set Database options
    
    SET NOCOUNT ON
    
    
    -- Check to make sure a valid Claim Aspect ID was passed in

    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_vehicle WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Validate the updated date parameter

    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_claim_vehicle', @ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Begin Update

    BEGIN TRANSACTION VehicleRentalUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update claim vehicle

    UPDATE dbo.utb_claim_vehicle
       SET 
		RentalDaysAuthorized  = @RentalDaysAuthorized,
        RentalInstructions    = @RentalInstructions,
		SysLastUserID         = @UserID,
		SysLastUpdatedDate    = @now
       WHERE 
		ClaimAspectID = @ClaimAspectID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT


    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim_vehicle', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    
    COMMIT TRANSACTION VehicleRentalUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Vehicle Level
            NULL AS [Vehicle!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Vehicle Level
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspVehicleRentalUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspVehicleRentalUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/