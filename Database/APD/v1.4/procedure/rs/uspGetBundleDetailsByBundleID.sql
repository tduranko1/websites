-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetBundleDetailsByBundleID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetBundleDetailsByBundleID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetBundleDetailsByBundleID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets existing bundling information by InsuranceCompanyID
*
* PARAMETERS:  
*			@iBundleID
* RESULT SET:
*   All data related to client bundling details by bundle ID
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetBundleDetailsByBundleID
	@iBundlingID INT = 0
	, @iDocumentTypeID INT = 0
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 
    DECLARE @ProcName AS varchar(30)

    SET @ProcName = 'uspGetBundleDetailsByBundleID'

    -- Set Database options
    
    SET NOCOUNT ON

    -- Check to make sure a valid Claim Aspect id was passed in
    IF (@iBundlingID > 0) AND (NOT EXISTS(SELECT BundlingID FROM utb_Bundling WHERE BundlingID = @iBundlingID))
    BEGIN
        -- Invalid Bundling ID
        RAISERROR('101|%s|@iBundlingID|%u', 16, 1, @ProcName, @iBundlingID)
        RETURN
    END

	IF (@iBundlingID = 0)
	BEGIN
		SELECT
			b.BundlingID
			, udt.DocumentTypeID
			, b.MessageTemplateID 
			, udt.[Name]
			, b.EnabledFlag
			, mt.[Description]
			, ISNULL(bdt.DirectionalCD,'') AS DirectionalCD
			, bdt.DirectionToPayFlag
			, bdt.DuplicateFlag
			, bdt.EstimateTypeCD
			, bdt.FinalEstimateFlag
			, bdt.MandatoryFlag
			, bdt.SelectionOrder
			, bdt.VANFlag
			, bdt.WarrantyFlag
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
			LEFT JOIN utb_bundling_document_type bdt
			ON bdt.BundlingID = b.BundlingID
			INNER JOIN dbo.utb_document_type udt
			ON udt.DocumentTypeID = bdt.DocumentTypeID
		ORDER BY
			b.[Name]
	END
	ELSE
	BEGIN
		SELECT
			b.BundlingID
			, udt.DocumentTypeID
			, b.MessageTemplateID 
			, udt.[Name]
			, b.EnabledFlag
			, mt.[Description]
			, ISNULL(bdt.DirectionalCD,'') AS DirectionalCD
			, bdt.DirectionToPayFlag
			, bdt.DuplicateFlag
			, bdt.EstimateTypeCD
			, bdt.FinalEstimateFlag
			, bdt.MandatoryFlag
			, bdt.SelectionOrder
			, bdt.VANFlag
			, bdt.WarrantyFlag
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
			LEFT JOIN utb_bundling_document_type bdt
			ON bdt.BundlingID = b.BundlingID
			INNER JOIN dbo.utb_document_type udt
			ON udt.DocumentTypeID = bdt.DocumentTypeID
		WHERE 
			b.BundlingID = @iBundlingID
			AND bdt.DocumentTypeID = @iDocumentTypeID
		ORDER BY
			b.[Name]
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetBundleDetailsByBundleID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetBundleDetailsByBundleID TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetBundleDetailsByBundleID TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/