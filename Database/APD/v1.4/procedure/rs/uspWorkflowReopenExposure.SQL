-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowReopenExposure' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowReopenExposure 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowReopenExposure
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Reopens 1 or more claim exposures
*
* PARAMETERS:  
* (I) @LynxID               The LynxID
* (I) @ExposureList         Comma delimited list of exposures to reopen
* (I) @Comments             Reason for reopening.
* (I) @AssignToUserID       The User ID to whom this claim is assigned to (Claim Reopen).
* (I) @UserID               The User performing this action
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowReopenExposure
    @LynxID             udt_std_id,
    @ClaimAspectList    varchar(500),
    @Comments           udt_std_note,
    @AssignToUserID     udt_std_id  = NULL,
    @UserID             udt_std_id,
    @ApplicationCD      udt_std_cd='APD'
AS
BEGIN
    -- Set database Options

    SET NOCOUNT ON


    -- Declare internal variables

    DECLARE @tmpExposure TABLE
    (
        ClaimAspectID   bigint,
        Valid           bit,
        Priority        tinyint
    )


    DECLARE @ClaimAspectIDClaim             udt_std_id_big
    DECLARE @ClaimAspectIDWork              udt_std_id_big
    DECLARE @ClaimAspectTypeIDClaim         udt_std_id
    DECLARE @ClaimAspectTypeIDProperty      udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle       udt_std_id
    DECLARE @ClaimAspectTypeIDWork          udt_std_id
    DECLARE @ClaimAspectNumberWork          udt_std_int
    DECLARE @EventIDReopenClaim             udt_std_id
    DECLARE @EventIDReopenProperty          udt_std_id
    DECLARE @EventIDReopenVehicle           udt_std_id
    DECLARE @EventIDWork                    udt_std_id
    DECLARE @EventNameWork                  udt_std_name
    DECLARE @HistoryDescription             udt_std_desc_long
    DECLARE @ClaimStatus                    udt_std_id
    DECLARE @NoteTypeIDClaim                udt_std_id
    DECLARE @StatusClaimOpenID              udt_std_id
    DECLARE @StatusPropertyOpenID           udt_std_id
    DECLARE @StatusVehicleOpenID            udt_std_id
    DECLARE @StatusOpenIDWork               udt_std_id
    DECLARE @EntityOwnerUserIDWork          udt_std_id

    DECLARE @now                            udt_std_datetime
    DECLARE @error                          int
    DECLARE @rowcount                       int
    DECLARE @ClaimReopen                    udt_std_flag
    DECLARE @NoteDescription               udt_std_note

    DECLARE @ProcName                       varchar(30)       -- Used for raise error stmts 


    SET @ProcName = 'uspWorkflowReopenExposure'

    DECLARE @debug                          udt_std_flag

    SET @debug = 0
    SET @ClaimReopen = 0


    -- Debug Code
    IF @debug = 1
    BEGIN
        Print 'BEGIN PROCEDURE uspWorkflowReopenExposure'
        Print ''
        Print 'Parameters:'
        Print '@LynxID = ' + Convert(varchar(10), @LynxID)
        Print '@ClaimAspectList = ' + @ClaimAspectList
        Print '@UserID = ' + Convert(varchar(10), @UserID)
    END
    -- End Debug


    -- Check to make sure a valid Lynx id was passed in

    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
    
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (dbo.ufnUtilityIsUserActive(@UserID, @ApplicationCD, NULL) = 0)
    BEGIN
        -- Invalid User ID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END



    -- Check to make sure a valid Exposure List was passed in

    IF  (@ClaimAspectList IS NULL) OR
        (Len(LTRIM(RTRIM(@ClaimAspectList))) = 0)
    BEGIN
        -- Empty exposure list
    
        RAISERROR('101|%s|@ClaimAspectList|%s', 16, 1, @ProcName, @ClaimAspectList)
        RETURN
    END
    
    
    -- Get Claim Aspect for claim

    SELECT  @ClaimAspectTypeIDClaim = ca.ClaimAspectTypeID,
            @ClaimAspectIDClaim = ca.ClaimAspectID,
            @ClaimStatus = cas.StatusID
      FROM  dbo.utb_claim_aspect ca
      LEFT JOIN dbo.utb_claim_aspect_type cat ON (ca.ClaimAspectTypeID = cat.ClaimAspectTypeID)
      LEFT JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
      WHERE ca.LynxID = @LynxID
        AND cat.Name = 'Claim'
        
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Claim"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END
    
    -- Check if the claim is voided
    IF EXISTS(SELECT StatusID
                FROM utb_status
                WHERE StatusID = @ClaimStatus
                  AND Name = 'Claim Voided'
             )
    BEGIN
        -- Voided Claims cannot be reopened
        
        RAISERROR  ('1|This claim has been voided and cannot be reopened.', 16, 1)
        RETURN
    END


    -- Debug Code
    IF @debug = 1
    BEGIN
        Print ''
        Print 'Closed Exposures in claim:'
        SELECT * FROM dbo.ufnUtilityGetClaimEntityList ( @LynxID, 1, 2 )  -- 1 means Exposure Only, 2 -- Means Closed only
    END
    -- End Debug
    
    IF EXISTS (SELECT StatusID
                FROM utb_status
                WHERE StatusID = @ClaimStatus
                  AND Name in ('Claim Closed', 'Claim Cancelled')
        )
    BEGIN
        -- We are reopening the claim and check the AssignToUserID
        IF  (@AssignToUserID IS NULL) OR
            (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @AssignToUserID))
        BEGIN
            -- Invalid Assign To User ID
    
            RAISERROR('101|%s|@AssignToUserID|%u', 16, 1, @ProcName, @AssignToUserID)
            RETURN
        END
        
        SET @ClaimReopen = 1
    END


    -- Get the list of exposures, also determining whether all exposures listed are valid

    INSERT INTO @tmpExposure (ClaimAspectID, Valid, Priority)
      SELECT  ufnPS.value,
              Count(ufnCEL.ClaimAspectID),
              Priority = CASE 
                           WHEN (SELECT ClaimAspectTypeID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = ufnPS.value) = 0 THEN 0
                           ELSE 1
                         END
        FROM  dbo.ufnUtilityParseString( @ClaimAspectList, ',', 1 ) ufnPS   --1 means to trim spaces
        LEFT JOIN dbo.ufnUtilityGetClaimEntityList ( @LynxID, 1, 2 ) ufnCEL   -- 1 means Exposure Only, 2 -- Means Closed only
            ON (ufnPS.value = ufnCEL.ClaimAspectID)
        GROUP BY ufnPS.value
        ORDER BY Priority

    IF @@ERROR <> 0
    BEGIN
       -- Insertion failure
    
        RAISERROR('105|%s|@tmpExposure', 16, 1, @ProcName)
        RETURN
    END
 
 
    -- Debug Code
    IF @debug = 1
    BEGIN
        Print ''
        Print '@tmpExposure Table:'
        SELECT * FROM @tmpExposure
    END
    -- End Debug

 
    IF EXISTS(SELECT ClaimAspectID FROM @tmpExposure WHERE Valid = 0 AND ClaimAspectID <> @ClaimAspectIDClaim)
    BEGIN
        -- Invalid exposure listed
    
        RAISERROR('101|%s|@ClaimAspectList|%s', 16, 1, @ProcName, @ClaimAspectList)
        RETURN
    END


    -- Make sure the entity list does not just contain the claim entity and no exposures

    IF NOT EXISTS(SELECT ClaimAspectID FROM @tmpExposure WHERE ClaimAspectID <> @ClaimAspectIDClaim)
    BEGIN
        -- claim entity only 


        RAISERROR('%s: @ExposureList not valid.  Cannot reopen the claim without reopening at least 1 exposure.', 16, 1, @ProcName)
        RETURN
    END


    -- We need to make sure that the claim aspect is in the entity list if the claim is not open, remove it if the claim is open

    IF (SELECT cas.StatusID FROM dbo.utb_claim_aspect ca
        INNER JOIN dbo.utb_claim_aspect_status cas ON (ca.ClaimAspectID = cas.ClaimAspectID)
        WHERE LynxID = @LynxID AND ClaimAspectTypeID = @ClaimAspectTypeIDClaim) > 900
    BEGIN
        -- Claim is closed, verify open exists in entity list

        IF NOT EXISTS(SELECT ClaimAspectID FROM @tmpExposure WHERE ClaimAspectID = @ClaimAspectIDClaim)
        BEGIN
            -- Claim is closed and claim is not part of the reopen request

            RAISERROR('%s: @ExposureList not valid.  Cannot reopen exposures without reopening the claim.', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
        -- Claim is open, remove from the entity list if it exists

        DELETE FROM @tmpExposure
          WHERE ClaimAspectID = @ClaimAspectIDClaim

        IF @@ERROR <> 0
        BEGIN
            -- Deletion failure
        
            RAISERROR('106|%s|@tmpExposure', 16, 1, @ProcName)
            RETURN
        END

        
        -- Debug Code
        IF @debug = 1
        BEGIN
            Print ''
            Print 'Removing claim entity from @tmpExposure Table - New image:'
            SELECT * FROM @tmpExposure
        END
        -- End Debug
    END
    
    IF (@Comments is NULL) or (LEN(RTRIM(LTRIM(@Comments))) = 0)
    BEGIN
        RAISERROR  ('1|APD System requires a non-blank comment inorder to reopen this claim/exposure. Please enter the comments and try reopen again.', 16, 1)
        RETURN
    END

    -- Get Claim Aspect for property

    SELECT @ClaimAspectTypeIDProperty = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Property'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDProperty IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Property"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Get Claim Aspect for vehicle

    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM dbo.utb_claim_aspect_type
      WHERE Name = 'Vehicle'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
       -- Claim Aspect Not Found
    
        RAISERROR('102|%s|"Vehicle"|utb_claim_aspect_type', 16, 1, @ProcName)
        RETURN
    END

    -- Get Note Type ID for Claim
    SELECT  @NoteTypeIDClaim = NoteTypeID
      FROM  dbo.utb_note_type
      WHERE Name = 'Claim'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @NoteTypeIDClaim IS NULL
    BEGIN
       -- Note Type ID for Claim

        RAISERROR('102|%s|"Note Type: Claim"|utb_note_type', 16, 1, @ProcName)
        RETURN
    END

    -- Get StatusID for Claim Open
    SELECT  @StatusClaimOpenID = StatusID
      FROM  dbo.utb_status
      WHERE Name = 'Open' and
        ClaimAspectTypeID = @ClaimAspectTypeIDClaim

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @StatusClaimOpenID IS NULL
    BEGIN
       -- Status ID for Claim Cancelled

        RAISERROR('102|%s|"StatusID: Open for Claim"|utb_status', 16, 1, @ProcName)
        RETURN
    END

    -- Get StatusID for Vehicle Open
    SELECT  @StatusVehicleOpenID = StatusID
      FROM  dbo.utb_status
      WHERE Name = 'Open' and
        ClaimAspectTypeID = @ClaimAspectTypeIDVehicle

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @StatusVehicleOpenID IS NULL
    BEGIN
       -- Status ID for Claim Cancelled

        RAISERROR('102|%s|"StatusID: Open for Vehicle"|utb_status', 16, 1, @ProcName)
        RETURN
    END

    -- Get StatusID for Property Open
    SELECT  @StatusPropertyOpenID = StatusID
      FROM  dbo.utb_status
      WHERE Name = 'Open' and
        ClaimAspectTypeID = @ClaimAspectTypeIDProperty

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @StatusPropertyOpenID IS NULL
    BEGIN
       -- Status ID for Claim Cancelled

        RAISERROR('102|%s|"StatusID: Open for Property"|utb_status', 16, 1, @ProcName)
        RETURN
    END
    
    -- Get Event ID for "Claim Reopened"

    SELECT @EventIDReopenClaim = EventID
      FROM dbo.utb_event
      WHERE Name = 'Claim Reopened'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDReopenClaim IS NULL
    BEGIN
       -- Event ID Not Found
    
        RAISERROR('102|%s|"Claim Reopened"|utb_event', 16, 1, @ProcName)
        RETURN
    END

    
    -- Get Event ID for "Property Reopened"

    SELECT @EventIDReopenProperty = EventID
      FROM dbo.utb_event
      WHERE Name = 'Property Reopened'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDReopenProperty IS NULL
    BEGIN
       -- Event ID Not Found
    
        RAISERROR('102|%s|"Property Reopened"|utb_event', 16, 1, @ProcName)
        RETURN
    END

    
    -- Get Event ID for "Vehicle Reopened"

    SELECT @EventIDReopenVehicle = EventID
      FROM dbo.utb_event
      WHERE Name = 'Vehicle Reopened'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDReopenVehicle IS NULL
    BEGIN
       -- Event ID Not Found
    
        RAISERROR('102|%s|"Vehicle Reopened"|utb_event', 16, 1, @ProcName)
        RETURN
    END

    
    -- Debug Code
    IF @debug = 1
    BEGIN
        Print ''
        Print 'Derived Variables:'
        Print '@ClaimAspectTypeIDClaim = ' + Convert(varchar(10), @ClaimAspectTypeIDClaim)
        Print '@ClaimAspectTypeIDProperty = ' + Convert(varchar(10), @ClaimAspectTypeIDProperty)
        Print '@ClaimAspectTypeIDVehicle = ' + Convert(varchar(10), @ClaimAspectTypeIDVehicle)
        Print '@EventIDReopenClaim = ' + Convert(varchar(10), @EventIDReopenClaim)
        Print '@EventIDReopenProperty = ' + Convert(varchar(10), @EventIDReopenProperty)
        Print '@EventIDReopenVehicle = ' + Convert(varchar(10), @EventIDReopenVehicle)

        Print '@NoteTypeIDClaim = ' + Convert(varchar(10), @NoteTypeIDClaim)
        Print '@StatusClaimOpenID = ' + Convert(varchar(10), @StatusClaimOpenID)
        Print '@StatusPropertyOpenID = ' + Convert(varchar(10), @StatusPropertyOpenID)
        Print '@StatusVehicleOpenID = ' + Convert(varchar(10), @StatusVehicleOpenID)
    END
    -- End Debug


    -- Lets now step through the list of exposures to reopen and process each one in turn

    DECLARE csrEntityList CURSOR FOR
      SELECT  ClaimAspectID
        FROM  @tmpExposure


    OPEN csrEntityList

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrEntityList
    INTO @ClaimAspectIDWork

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
  -- Begin Update

    BEGIN TRANSACTION WorkflowReopenExposureTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    SET @NoteDescription =''


    WHILE @@Fetch_Status = 0
    BEGIN
        -- To reopen an exposure we simply have to throw the Reopen workflow event    
    
        -- Get the Aspect Type/Number from the ClaimAspectID
    
        SELECT  @ClaimAspectTypeIDWork = ClaimAspectTypeID,
                @ClaimAspectNumberWork = ClaimAspectNumber,
                @EntityOwnerUserIDWork = OwnerUserID
          FROM  dbo.utb_claim_aspect
          WHERE ClaimAspectID = @ClaimAspectIDWork

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END    


        -- Debug Code
        IF @debug = 1
        BEGIN
            Print ''
            Print 'Start Loop Iteration:'
            Print '    @ClaimAspectIDWork = ' + @ClaimAspectIDWork
            Print '    @ClaimAspectTypeIDWork = ' + Convert(varchar(10), @ClaimAspectTypeIDWork)
            Print '    @ClaimAspectNumberWork = ' + Convert(varchar(10), @ClaimAspectNumberWork)
        END
        -- End Debug


        -- Look at the claim aspect and throw the appropriate event

        SET @EventIDWork = NULL

        IF @ClaimAspectTypeIDWork = @ClaimAspectTypeIDClaim
        BEGIN
            SET @EventIDWork = @EventIDReopenClaim
            SET @EventNameWork = '"Claim Reopened"'
            SET @HistoryDescription = 'Claim Reopened'
            SET @StatusOpenIDWork = @StatusClaimOpenID
            SET @ClaimReopen = 1
            SET @NoteDescription = @NoteDescription +  'Claim' + ', '
         END
        
        IF @ClaimAspectTypeIDWork = @ClaimAspectTypeIDProperty
        BEGIN
            SET @EventIDWork = @EventIDReopenProperty
            SET @EventNameWork = '"Property Reopened"'
            SET @HistoryDescription = 'Property ' + Convert(varchar(5), @ClaimAspectNumberWork) + ' Reopened'
            SET @StatusOpenIDWork = @StatusPropertyOpenID
            SET @NoteDescription = @NoteDescription + 'Property' + Convert(varchar(10),@ClaimAspectNumberWork) + ', '
        END

        IF @ClaimAspectTypeIDWork = @ClaimAspectTypeIDVehicle
        BEGIN
            SET @EventIDWork = @EventIDReopenVehicle
            SET @EventNameWork = '"Vehicle Reopened"'
            SET @HistoryDescription = 'Vehicle ' + Convert(varchar(5), @ClaimAspectNumberWork) + ' Reopened'
            SET @StatusOpenIDWork = @StatusVehicleOpenID
            SET @NoteDescription = @NoteDescription + 'Vehicle' +  Convert(varchar(10),@ClaimAspectNumberWork) + ', '
        END


        -- Debug Code
        IF @debug = 1
        BEGIN
            Print ''
            Print '    @EventIDWork = ' + + Convert(varchar(10), @EventIDWork)
        END
        -- End Debug
        
        
        -- Throw the event

        IF @EventIDWork IS NOT NULL
        BEGIN
            exec uspWorkflowNotifyEvent @EventID = @EventIDWork,
                                        @ClaimAspectID = @ClaimAspectIDWork,
                                        @Description = @HistoryDescription,
                                        @UserID = @UserID

            IF @@ERROR <> 0
            BEGIN
                -- Error notifying APD of event
        
                RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventNamework)
                ROLLBACK TRANSACTION
                RETURN
            END    
        END

        IF @ClaimAspectTypeIDWork <> @ClaimAspectTypeIDClaim AND
           @EntityOwnerUserIDWork <> @AssignToUserID
        BEGIN
            exec uspWorkflowAssignClaim @ClaimAspectIDWork, @UserID, @AssignToUserID, default, default
        END
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END    


        FETCH next
        FROM csrEntityList
        INTO @ClaimAspectIDWork

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END    
    END


    -- Close and deallocate the cursor

    CLOSE csrEntityList
    DEALLOCATE csrEntityList

         SET @NoteDescription = left(ltrim(rtrim(@NoteDescription)), len(ltrim(rtrim(@NoteDescription)))-1)  +  '  Reopened: ' + @Comments       
    
   -- Add a note with the comments supplied
        exec uspNoteInsDetail @ClaimAspectIDClaim, @NoteTypeIDClaim, @StatusClaimOpenID, @NoteDescription, @UserID
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('104|%s|%s', 16, 1, @ProcName, 'uspNoteInsDetail')
            ROLLBACK TRANSACTION
            RETURN
        END    


    
    -- Reassign the claim the AssignToUserID
    /*IF @ClaimReopen = 1
    BEGIN
        exec uspWorkflowAssignClaim @LynxID, @UserID, @AssignToUserID, default, default
        
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END    
    END*/

    COMMIT TRANSACTION WorkflowReopenExposureTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    RETURN
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowReopenExposure' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowReopenExposure TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/