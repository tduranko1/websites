-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTPersonnelUpdEntity' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTPersonnelUpdEntity 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTPersonnelUpdEntity
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Update a Personnel record
*
* PARAMETERS:  
* (I) @PersonnelID                         The Personnel's unique identity
* (I) @PersonnelTypeID                     The Personnel's type
* (I) @PreferredContactMethodID            The Personnel's preferred contact method
* (I) @CellAreaCode                        The Personnel's cell area code
* (I) @CellExchangeNumber                  The Personnel's cell exchange number
* (I) @CellExtensionNumber                 The Personnel's cell extension number
* (I) @CellUnitNumber                      The Personnel's cell unit number
* (I) @EmailAddress                        The Personnel's email address
* (I) @FaxAreaCode                         The Personnel's fax area code
* (I) @FaxExchangeNumber                   The Personnel's fax exchange number
* (I) @FaxExtensionNumber                  The Personnel's fax extension number
* (I) @FaxUnitNumber                       The Personnel's fax unit number
* (I) @GenderCD                            The Personnel's gender cd
* (I) @MinorityFlag                        The Personnel's minority flag
* (I) @Name                                The Personnel's name
* (I) @PagerAreaCode                       The Personnel's pager area code
* (I) @PagerExchangeNumber                 The Personnel's pager exchange number
* (I) @PagerExtensionNumber                The Personnel's pager extension number
* (I) @PagerUnitNumber                     The Personnel's pager unit number
* (I) @PhoneAreaCode                       The Personnel's phone area code
* (I) @PhoneExchangeNumber                 The Personnel's phone exchange number
* (I) @PhoneExtensionNumber                The Personnel's phone extension number
* (I) @PhoneUnitNumber                     The Personnel's phone unit number
* (I) @ShopManagerFlag                     The Personnel's shop manager flag
* (I) @SysLastUserID                       The Personnel's updating user identity
* (I) @SysLastUpdatedDate                  The Personnel's last update date as string (VARCHAR(30))
*
* RESULT SET:
* PersonnelID                              The updated Personnel unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTPersonnelUpdEntity
(
  @EntityID                    	     udt_std_int_big,
	@EntityType                        udt_std_cd,  
  -- Personnel 1
	@PersonnelID1                    	 udt_std_int_big,
  @PersonnelTypeID1                  udt_std_int_tiny,
  @PreferredContactMethodID1         udt_std_int_tiny,
  @CellAreaCode1                     udt_ph_area_code=NULL,
  @CellExchangeNumber1               udt_ph_exchange_number=NULL,
  @CellExtensionNumber1              udt_ph_extension_number=NULL,
  @CellUnitNumber1                   udt_ph_unit_number=NULL,
  @EmailAddress1                     udt_web_email=NULL,
  @FaxAreaCode1                      udt_ph_area_code=NULL,
  @FaxExchangeNumber1                udt_ph_exchange_number=NULL,
  @FaxExtensionNumber1               udt_ph_extension_number=NULL,
  @FaxUnitNumber1                    udt_ph_unit_number=NULL,
  @GenderCD1                         udt_per_gender_cd=NULL,
  @MinorityFlag1                     udt_std_flag=0,
  @Name1                             udt_std_name,
  @PagerAreaCode1                    udt_ph_area_code=NULL,
  @PagerExchangeNumber1              udt_ph_exchange_number=NULL,
  @PagerExtensionNumber1             udt_ph_extension_number=NULL,
  @PagerUnitNumber1                  udt_ph_unit_number=NULL,
  @PhoneAreaCode1                    udt_ph_area_code=NULL,
  @PhoneExchangeNumber1              udt_ph_exchange_number=NULL,
  @PhoneExtensionNumber1             udt_ph_extension_number=NULL,
  @PhoneUnitNumber1                  udt_ph_unit_number=NULL,
  @ShopManagerFlag1                  udt_std_flag=0,
  @SysLastUpdatedDate1               VARCHAR(30),
  @RelationSysLastUpdatedDate1       VARCHAR(30),
  @DeletePersonnel1                  udt_std_flag=0,
  -- Personnel 2
  @PersonnelID2                    	 udt_std_int_big=NULL,
  @PersonnelTypeID2                  udt_std_int_tiny=NULL,
  @PreferredContactMethodID2         udt_std_int_tiny=NULL,
  @CellAreaCode2                     udt_ph_area_code=NULL,
  @CellExchangeNumber2               udt_ph_exchange_number=NULL,
  @CellExtensionNumber2              udt_ph_extension_number=NULL,
  @CellUnitNumber2                   udt_ph_unit_number=NULL,
  @EmailAddress2                     udt_web_email=NULL,
  @FaxAreaCode2                      udt_ph_area_code=NULL,
  @FaxExchangeNumber2                udt_ph_exchange_number=NULL,
  @FaxExtensionNumber2               udt_ph_extension_number=NULL,
  @FaxUnitNumber2                    udt_ph_unit_number=NULL,
  @GenderCD2                         udt_per_gender_cd=NULL,
  @MinorityFlag2                     udt_std_flag=0,
  @Name2                             udt_std_name=NULL,
  @PagerAreaCode2                    udt_ph_area_code=NULL,
  @PagerExchangeNumber2              udt_ph_exchange_number=NULL,
  @PagerExtensionNumber2             udt_ph_extension_number=NULL,
  @PagerUnitNumber2                  udt_ph_unit_number=NULL,
  @PhoneAreaCode2                    udt_ph_area_code=NULL,
  @PhoneExchangeNumber2              udt_ph_exchange_number=NULL,
  @PhoneExtensionNumber2             udt_ph_extension_number=NULL,
  @PhoneUnitNumber2                  udt_ph_unit_number=NULL,
  @ShopManagerFlag2                  udt_std_flag=0,
  @SysLastUpdatedDate2               VARCHAR(30)=NULL,
  @RelationSysLastUpdatedDate2       VARCHAR(30)=NULL,
  @DeletePersonnel2                  udt_std_flag=0,
  -- Personnel 3
  @PersonnelID3                    	 udt_std_int_big=NULL,
  @PersonnelTypeID3                  udt_std_int_tiny=NULL,
  @PreferredContactMethodID3         udt_std_int_tiny=NULL,
  @CellAreaCode3                     udt_ph_area_code=NULL,
  @CellExchangeNumber3               udt_ph_exchange_number=NULL,
  @CellExtensionNumber3              udt_ph_extension_number=NULL,
  @CellUnitNumber3                   udt_ph_unit_number=NULL,
  @EmailAddress3                     udt_web_email=NULL,
  @FaxAreaCode3                      udt_ph_area_code=NULL,
  @FaxExchangeNumber3                udt_ph_exchange_number=NULL,
  @FaxExtensionNumber3               udt_ph_extension_number=NULL,
  @FaxUnitNumber3                    udt_ph_unit_number=NULL,
  @GenderCD3                         udt_per_gender_cd=NULL,
  @MinorityFlag3                     udt_std_flag=0,
  @Name3                             udt_std_name=NULL,
  @PagerAreaCode3                    udt_ph_area_code=NULL,
  @PagerExchangeNumber3              udt_ph_exchange_number=NULL,
  @PagerExtensionNumber3             udt_ph_extension_number=NULL,
  @PagerUnitNumber3                  udt_ph_unit_number=NULL,
  @PhoneAreaCode3                    udt_ph_area_code=NULL,
  @PhoneExchangeNumber3              udt_ph_exchange_number=NULL,
  @PhoneExtensionNumber3             udt_ph_extension_number=NULL,
  @PhoneUnitNumber3                  udt_ph_unit_number=NULL,
  @ShopManagerFlag3                  udt_std_flag=0,
  @SysLastUpdatedDate3               VARCHAR(30)=NULL,
  @RelationSysLastUpdatedDate3       VARCHAR(30)=NULL,
  @DeletePersonnel3                  udt_std_flag=0,
  -- Personnel 4
  @PersonnelID4                    	 udt_std_int_big=NULL,
  @PersonnelTypeID4                  udt_std_int_tiny=NULL,
  @PreferredContactMethodID4         udt_std_int_tiny=NULL,
  @CellAreaCode4                     udt_ph_area_code=NULL,
  @CellExchangeNumber4               udt_ph_exchange_number=NULL,
  @CellExtensionNumber4              udt_ph_extension_number=NULL,
  @CellUnitNumber4                   udt_ph_unit_number=NULL,
  @EmailAddress4                     udt_web_email=NULL,
  @FaxAreaCode4                      udt_ph_area_code=NULL,
  @FaxExchangeNumber4                udt_ph_exchange_number=NULL,
  @FaxExtensionNumber4               udt_ph_extension_number=NULL,
  @FaxUnitNumber4                    udt_ph_unit_number=NULL,
  @GenderCD4                         udt_per_gender_cd=NULL,
  @MinorityFlag4                     udt_std_flag=0,
  @Name4                             udt_std_name=NULL,
  @PagerAreaCode4                    udt_ph_area_code=NULL,
  @PagerExchangeNumber4              udt_ph_exchange_number=NULL,
  @PagerExtensionNumber4             udt_ph_extension_number=NULL,
  @PagerUnitNumber4                  udt_ph_unit_number=NULL,
  @PhoneAreaCode4                    udt_ph_area_code=NULL,
  @PhoneExchangeNumber4              udt_ph_exchange_number=NULL,
  @PhoneExtensionNumber4             udt_ph_extension_number=NULL,
  @PhoneUnitNumber4                  udt_ph_unit_number=NULL,
  @ShopManagerFlag4                  udt_std_flag=0,
  @SysLastUpdatedDate4               VARCHAR(30)=NULL,
  @RelationSysLastUpdatedDate4       VARCHAR(30)=NULL,
  @DeletePersonnel4                  udt_std_flag=0,  
  
  @SysLastUserID                     udt_std_id
  
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    -- Declare internal variables

    DECLARE @error                            AS INT
    DECLARE @rowcount                         AS INT
    DECLARE @tupdated_date                    AS DATETIME 
    DECLARE @ProcName                         AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                              AS DATETIME
            
      
    -- Begin Update(s)

    BEGIN TRANSACTION AdmPersonnelUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF (@PersonnelID1 IS NOT NULL)
    BEGIN
        IF (@DeletePersonnel1 = 0)
        BEGIN
          EXEC uspSMTPersonnelUpdDetail @PersonnelID1, @PersonnelTypeID1, @PreferredContactMethodID1, @CellAreaCode1,
                                        @CellExchangeNumber1, @CellExtensionNumber1, @CellUnitNumber1, @EmailAddress1,               
                                        @FaxAreaCode1, @FaxExchangeNumber1, @FaxExtensionNumber1, @FaxUnitNumber1,
                                        @GenderCD1, @MinorityFlag1, @Name1, @PagerAreaCode1, @PagerExchangeNumber1,
                                        @PagerExtensionNumber1, @PagerUnitNumber1, @PhoneAreaCode1, @PhoneExchangeNumber1,
                                        @PhoneExtensionNumber1, @PhoneUnitNumber1, @ShopManagerFlag1, @SysLastUserID,
                                        @SysLastUpdatedDate1         
        END
        ELSE
        BEGIN
          EXEC uspSMTPersonnelDel @EntityID, @EntityType, @PersonnelID1, @SysLastUserID, @RelationSysLastUpdatedDate1    
        END
    END
    
    
    IF (@PersonnelID2 IS NOT NULL)
    BEGIN
        IF (@DeletePersonnel2 = 0)
        BEGIN
          EXEC uspSMTPersonnelUpdDetail @PersonnelID2, @PersonnelTypeID2, @PreferredContactMethodID2, @CellAreaCode2,
                                        @CellExchangeNumber2, @CellExtensionNumber2, @CellUnitNumber2, @EmailAddress2,               
                                        @FaxAreaCode2, @FaxExchangeNumber2, @FaxExtensionNumber2, @FaxUnitNumber2,
                                        @GenderCD2, @MinorityFlag2, @Name2, @PagerAreaCode2, @PagerExchangeNumber2,
                                        @PagerExtensionNumber2, @PagerUnitNumber2, @PhoneAreaCode2, @PhoneExchangeNumber2,
                                        @PhoneExtensionNumber2, @PhoneUnitNumber2, @ShopManagerFlag2, @SysLastUserID,
                                        @SysLastUpdatedDate2         
        END
        ELSE
        BEGIN
          EXEC uspSMTPersonnelDel @EntityID, @EntityType, @PersonnelID2, @SysLastUserID, @RelationSysLastUpdatedDate2    
        END
    END
    
    
    IF (@PersonnelID3 IS NOT NULL)
    BEGIN
        IF (@DeletePersonnel3 = 0)
        BEGIN
          EXEC uspSMTPersonnelUpdDetail @PersonnelID3, @PersonnelTypeID3, @PreferredContactMethodID3, @CellAreaCode3,
                                        @CellExchangeNumber3, @CellExtensionNumber3, @CellUnitNumber3, @EmailAddress3,               
                                        @FaxAreaCode3, @FaxExchangeNumber3, @FaxExtensionNumber3, @FaxUnitNumber3,
                                        @GenderCD3, @MinorityFlag3, @Name3, @PagerAreaCode3, @PagerExchangeNumber3,
                                        @PagerExtensionNumber3, @PagerUnitNumber3, @PhoneAreaCode3, @PhoneExchangeNumber3,
                                        @PhoneExtensionNumber3, @PhoneUnitNumber3, @ShopManagerFlag3, @SysLastUserID,
                                        @SysLastUpdatedDate3         
        END
        ELSE
        BEGIN
          EXEC uspSMTPersonnelDel @EntityID, @EntityType, @PersonnelID3, @SysLastUserID, @RelationSysLastUpdatedDate3    
        END
    END
    
    IF (@PersonnelID4 IS NOT NULL)
    BEGIN
        IF (@DeletePersonnel4 = 0)
        BEGIN
          EXEC uspSMTPersonnelUpdDetail @PersonnelID4, @PersonnelTypeID4, @PreferredContactMethodID4, @CellAreaCode4,
                                        @CellExchangeNumber4, @CellExtensionNumber4, @CellUnitNumber4, @EmailAddress4,               
                                        @FaxAreaCode4, @FaxExchangeNumber4, @FaxExtensionNumber4, @FaxUnitNumber4,
                                        @GenderCD4, @MinorityFlag4, @Name4, @PagerAreaCode4, @PagerExchangeNumber4,
                                        @PagerExtensionNumber4, @PagerUnitNumber4, @PhoneAreaCode4, @PhoneExchangeNumber4,
                                        @PhoneExtensionNumber4, @PhoneUnitNumber4, @ShopManagerFlag4, @SysLastUserID,
                                        @SysLastUpdatedDate4         
        END
        ELSE
        BEGIN
          EXEC uspSMTPersonnelDel @EntityID, @EntityType, @PersonnelID4, @SysLastUserID, @RelationSysLastUpdatedDate4    
        END
    END   
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('104|%s|utb_personnel', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END


    COMMIT TRANSACTION AdmPersonnelUpdDetailTran1    


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    --SELECT 1
    
    RETURN 1
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTPersonnelUpdEntity' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTPersonnelUpdEntity TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/