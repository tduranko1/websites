-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDelExpired' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSessionDelExpired 
END
GO

/****** Object:  StoredProcedure [dbo].[uspSessionDelExpired]    Script Date: 08/08/2014 12:21:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSessionDelExpired
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Removes expired session data
*
* PARAMETERS:  
* (I) @minutes  The number of minutes for sessions to be deleted
*
* RESULT SET:
* None
*
*
* VSS
* $Workfile: uspSessionDelExpired.sql $
* $Archive: /Database/APD/v1.0.0/procedure/uspSessionDelExpired.sql $
* $Revision: 1 $
* $Author: Jim $
* $Date: 10/12/01 12:08p $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSessionDelExpired]
    @minutes AS INT
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    -- SET TRANSACTION ISOLATION LEVEL to SERIALIZABLE to initiate pessimistic concurrency control
    
    -- SET TRANSACTION ISOLATION LEVEL SERIALIZABLE --glsd451

    -- Declare internal variables

    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @trancount          AS INT

    SET @error = 0
    
    IF 0 = @error
    BEGIN
        DECLARE @cutoff_date AS DATETIME

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    --- Calculate the cutoff date

    IF 0 = @error
    BEGIN
        SELECT @cutoff_date = DATEADD(n, -@minutes, CURRENT_TIMESTAMP)

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    -- Start the new transaction
    
    IF 0 = @error
    BEGIN
        SET @trancount = @@TRANCOUNT

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        BEGIN TRANSACTION

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    --- Remove all Session Data records prior to the cutoff date

    IF 0 = @error
    BEGIN
        DELETE dbo.utb_session_data 
         WHERE SessionID IN
        (
            SELECT SessionID
              FROM dbo.utb_session
             WHERE SysLastUpdatedDate < @cutoff_date 
        )

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    --- Remove all Session records prior to the cutoff date

    IF 0 = @error
    BEGIN
        DELETE dbo.utb_session
         WHERE SysLastUpdatedDate < @cutoff_date

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    -- Final transaction success determination
        
    IF @@TRANCOUNT > @trancount
    BEGIN
        IF 0 = @error
            COMMIT TRANSACTION
        ELSE
            ROLLBACK TRANSACTION
    END

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionDelExpired' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionDelExpired TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO