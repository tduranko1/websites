-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientUtilization' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2ClientUtilization 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2ClientUtilization
* SYSTEM:       Lynx Services APD
* FUNCTION:     Compiles data displayed on the Utilization Report
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company the report is being run for
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
* (I) @ShowUserDetails      Indicates whether details for each office user should be shown
* (I) @OfficeID             Optional office the report may be run for
* (I) @UserID               Optional user the report may be run for
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspRpt2ClientUtilization]
    @InsuranceCompanyID     udt_std_int_small,
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL,
    @ShowUserDetails        udt_std_flag      = 1,
    @OfficeID               udt_std_id_small  = NULL,
    @UserID                 udt_std_id_small  = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare internal variables

    DECLARE @tmpReportData TABLE 
    (
        OfficeID                     int          NOT NULL,
        OfficeName                   varchar(50)  NULL,
        UserID                       int          NOT NULL,
        UserNameSort                 varchar(50)  NULL,
        UserNameFirst                varchar(50)  NULL,
        UserNameLast                 varchar(50)  NULL,
        UserEnabled                  bit          NULL,
        DetailDisplayOrder           tinyint      NULL,
        ServiceChannelCD             varchar(4)   NOT NULL,
        ServiceChannelName           varchar(50)  NULL,
        Pos1Total                    int          NOT NULL,
        Pos2Total                    int          NOT NULL,
        Pos3Total                    int          NOT NULL,
        Pos4Total                    int          NOT NULL,
        Pos5Total                    int          NOT NULL,
        Pos6Total                    int          NOT NULL,
        MonthTotal                   int          NOT NULL,
        YearTotal                    int          NOT NULL,
        Prev12MonthTotal             int          NOT NULL
    )

    CREATE TABLE #tmpNewData
    (
        PeriodType                   varchar(5)   NOT NULL,
        PositionID                   tinyint      NOT NULL,
        WeekID                       tinyint      NOT NULL,
        OfficeID                     int          NOT NULL,
        UserID                       int          NOT NULL,
        ServiceChannelCD             varchar(4)   NOT NULL,
        NewCount                     int          NOT NULL
    )
                
    DECLARE @tmpTimePeriodCurrent TABLE
    (
        PositionID                  tinyint     NOT NULL,
        WeekID                      tinyint     NOT NULL,
        StartDate                   varchar(15) NOT NULL,
        EndDate                     varchar(15) NOT NULL
    )

    DECLARE @tmpTimePeriod12Month TABLE
    (
        TimePeriod                  varchar(8)  NOT NULL,
        MonthValue                  tinyint     NOT NULL,
        YearValue                   smallint    NOT NULL
    )


    DECLARE @InsuranceCompanyName   udt_std_name
    DECLARE @InsuranceCompanyLCPhone udt_std_name
    DECLARE @LoopIndex              udt_std_int_tiny
    DECLARE @MinWeekNumber          udt_std_int_tiny
    DECLARE @MonthName              udt_std_name
    DECLARE @OfficeIDWork           varchar(10)
    DECLARE @OfficeName             udt_std_name
    DECLARE @UserIDWork             varchar(10)
    DECLARE @UserName               udt_std_name
    
    DECLARE @Debug      udt_std_flag
    DECLARE @now        udt_std_datetime
    DECLARE @DataWarehouseDate    udt_std_datetime

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2ClientUtilization'

    SET @Debug = 0


    -- Validate Insurance Company ID
    
    IF (@InsuranceCompanyID IS NULL) OR
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID
        
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name,
                @InsuranceCompanyLCPhone = IsNull(CarrierLynxContactPhone, '239-337-4300')
          FROM  dbo.utb_insurance
          WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END          

    -- TVD Debugging 
    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID = ' + Convert(varchar, @InsuranceCompanyID) + ' - Validated Successfully'
	END        
   
    -- Validate Office ID
    
    IF @OfficeID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT OfficeID FROM dbo.utb_office WHERE OfficeID = @OfficeID AND InsuranceCompanyID = @InsuranceCompanyID)
        BEGIN
            -- Invalid Office ID
    
            RAISERROR  ('101|%s|@OfficeID|%u', 16, 1, @ProcName, @OfficeID)
            RETURN
        END
        ELSE
        BEGIN
            SET @OfficeIDWork = @OfficeID
        
            SELECT  @OfficeName = Name
              FROM  dbo.utb_office
              WHERE OfficeID = @OfficeID
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    

    -- TVD Debugging 
    IF @Debug = 1
    BEGIN
        PRINT '@OfficeID = ' + Convert(varchar, @OfficeID) + ' - Office Successfully'
	END        

    -- Validate User ID
    
    IF @UserID IS NOT NULL AND @ShowUserDetails = 1
    BEGIN
        IF NOT EXISTS(SELECT UserID FROM dbo.utb_user u LEFT JOIN dbo.utb_office o ON (u.OfficeID = o.OfficeID) WHERE u.UserID = @UserID AND o.InsuranceCompanyID = @InsuranceCompanyID)
        BEGIN
            -- Invalid User ID
    
            RAISERROR  ('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
            RETURN
        END
        ELSE
        BEGIN
            SET @UserIDWork = @UserID
        
            SELECT  @UserName = NameLast + ', ' + NameFirst,
                    @OfficeIDWork = OfficeID
              FROM  dbo.utb_user
              WHERE UserID = @UserID
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END

    -- TVD Debugging 
    IF @Debug = 1
    BEGIN
        PRINT '@UserID = ' + Convert(varchar, @UserID) + ' - UserID Successfully'
	END        

    
    IF @OfficeIDWork IS NULL SET @OfficeIDWork = '%'
    IF @OfficeName IS NULL SET @OfficeName = 'All'
    IF @UserIDWork IS NULL SET @UserIDWork = '%'                  
    IF @UserName IS NULL SET @UserName = 'All'


    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END


    -- Compile time periods
    
    SELECT @MinWeekNumber = Min(WeekOfYear)
        FROM  dbo.utb_dtwh_dim_time
        WHERE MonthOfYear = @RptMonth
          AND YearValue = @RptYear

    INSERT INTO @tmpTimePeriodCurrent
      SELECT  WeekOfYear - @MinWeekNumber + 1 AS PositionID,
              WeekOfYear,
              Convert(varchar(15), Min(DateValue), 101),
              Convert(varchar(15), Max(DateValue), 101)
        FROM  dbo.utb_dtwh_dim_time
        WHERE MonthOfYear = @RptMonth
          AND YearValue = @RptYear
        GROUP BY WeekOfYear
        ORDER BY WeekOfYear

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpTimePeriodCurrent', 16, 1, @ProcName)
        RETURN
    END

    SET @LoopIndex = 12

    WHILE @LoopIndex > 0
    BEGIN
        INSERT INTO @tmpTimePeriod12Month
          SELECT Convert(varchar(2), DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))))
                    + '/'
                    + Convert(varchar(4), DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))),
                 DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))), 
                 DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpTimePeriod12Month', 16, 1, @ProcName)
            RETURN
        END
    
        SET @LoopIndex = @LoopIndex - 1
    END


    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID = ' + Convert(varchar, @InsuranceCompanyID)
        PRINT '@InsuranceCompanyName = ' + @InsuranceCompanyName
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptMonthName = ' + @MonthName
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear, 101)
        PRINT '@OfficeID = ' + Convert(varchar, @OfficeID)
        PRINT '@OfficeName = ' + @OfficeName
        PRINT '@UserID = ' + Convert(varchar, @UserID)
        PRINT '@UserName = ' + @UserName
    END
    
      -- Compile data needed for the report
    
    -- Week based data
    
    INSERT INTO #tmpNewData       
      SELECT  'Week',
              tmp.PositionID, 
              dt.WeekOfYear,
              0,
              0,
              'TTL',
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN @tmpTimePeriodCurrent tmp ON (dt.WeekOfYear = tmp.WeekID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY tmp.PositionID, dt.WeekOfYear
                
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END
    INSERT INTO #tmpNewData
      SELECT  'Week',
              tmp.PositionID, 
              dt.WeekOfYear,
              0,
              0,
              dsc.ServiceChannelCD,
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN @tmpTimePeriodCurrent tmp ON (dt.WeekOfYear = tmp.WeekID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY tmp.PositionID, dt.WeekOfYear, dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL
                  
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  'Week',
              tmp.PositionID, 
              dt.WeekOfYear,
              dc.OfficeID,
              0,
              'TTL',
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN @tmpTimePeriodCurrent tmp ON (dt.WeekOfYear = tmp.WeekID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY tmp.PositionID, dt.WeekOfYear, dc.OfficeID
        HAVING dc.OfficeID LIKE @OfficeIDWork
                  
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  'Week',
              tmp.PositionID, 
              dt.WeekOfYear,
              dc.OfficeID,
              0,
              dsc.ServiceChannelCD,
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN @tmpTimePeriodCurrent tmp ON (dt.WeekOfYear = tmp.WeekID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY tmp.PositionID, dt.WeekOfYear, dc.OfficeID, dsc.ServiceChannelCD
        HAVING dc.OfficeID LIKE @OfficeIDWork
          AND  dsc.ServiceChannelCD IS NOT NULL
                  
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    -- Pull user details if flag is set
    
    IF @ShowUserDetails = 1
    BEGIN
        INSERT INTO #tmpNewData
          SELECT  'Week',
                  tmp.PositionID, 
                  dt.WeekOfYear,
                  dc.OfficeID,
                  dc.UserID,
                  'TTL',
                  Count(*)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            LEFT JOIN @tmpTimePeriodCurrent tmp ON (dt.WeekOfYear = tmp.WeekID)
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dt.MonthOfYear = @RptMonth
              AND dt.YearValue = @RptYear
              AND fc.AssignmentTypeID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
			  AND fc.EnabledFlag = 1
            GROUP BY tmp.PositionID, dt.WeekOfYear, dc.OfficeID, dc.UserID
            HAVING dc.OfficeID LIKE @OfficeIDWork
              AND  dc.UserID LIKE @UserIDWork
                      
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
            RETURN
        END
        
        INSERT INTO #tmpNewData
          SELECT  'Week',
                  tmp.PositionID, 
                  dt.WeekOfYear,
                  dc.OfficeID,
                  dc.UserID,
                  dsc.ServiceChannelCD,
                  Count(*)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            LEFT JOIN @tmpTimePeriodCurrent tmp ON (dt.WeekOfYear = tmp.WeekID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dt.MonthOfYear = @RptMonth
              AND dt.YearValue = @RptYear
              AND fc.CoverageTypeID IS NOT NULL
			  AND fc.EnabledFlag = 1
            GROUP BY tmp.PositionID, dt.WeekOfYear, dc.OfficeID, dc.UserID, dsc.ServiceChannelCD
            HAVING dc.OfficeID LIKE @OfficeIDWork
              AND  dc.UserID LIKE @UserIDWork
              AND  dsc.ServiceChannelCD IS NOT NULL
                      
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
            RETURN
        END
    END
       
       
    -- Month based data
                
    INSERT INTO #tmpNewData
      SELECT  'Month',
              7,
              0,
              0,
              0,
              'TTL',
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  'Month',
              7,
              0,
              0,
              0,
              dsc.ServiceChannelCD,
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  'Month',
              7,
              0,
              dc.OfficeID,
              0,
              'TTL',
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY dc.OfficeID
        HAVING dc.OfficeID LIKE @OfficeIDWork

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  'Month',
              7,
              0,
              dc.OfficeID,
              0,
              dsc.ServiceChannelCD,
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY dc.OfficeID, dsc.ServiceChannelCD
        HAVING dc.OfficeID LIKE @OfficeIDWork
          AND  dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    -- Pull user details if flag is set
    
    IF @ShowUserDetails = 1
    BEGIN
        INSERT INTO #tmpNewData
          SELECT  'Month',
                  7,
                  0,
                  dc.OfficeID,
                  dc.UserID,
                  'TTL',
                  Count(*)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dt.MonthOfYear = @RptMonth
              AND dt.YearValue = @RptYear
              AND fc.AssignmentTypeID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
			  AND fc.EnabledFlag = 1
            GROUP BY dc.OfficeID, dc.UserID
            HAVING dc.OfficeID LIKE @OfficeIDWork
              AND  dc.UserID LIKE @UserIDWork

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO #tmpNewData
          SELECT  'Month',
                  7,
                  0,
                  dc.OfficeID,
                  dc.UserID,
                  dsc.ServiceChannelCD,
                  Count(*)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dt.MonthOfYear = @RptMonth
              AND dt.YearValue = @RptYear
              AND fc.CoverageTypeID IS NOT NULL
			  AND fc.EnabledFlag = 1
            GROUP BY dc.OfficeID, dc.UserID, dsc.ServiceChannelCD
            HAVING dc.OfficeID LIKE @OfficeIDWork
              AND  dc.UserID LIKE @UserIDWork
              AND  dsc.ServiceChannelCD IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
            RETURN
        END
    END
    
    
    -- Pull year-based data
    
    INSERT INTO #tmpNewData
      SELECT  'Year',
              8,
              0,
              0,
              0,
              'TTL',
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
     LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  'Year',
              8,
              0,
              0,
              0,
              dsc.ServiceChannelCD,
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  'Year',
              8,
              0,
              dc.OfficeID,
              0,
              'TTL',
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY dc.OfficeID
        HAVING dc.OfficeID LIKE @OfficeIDWork

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  'Year',
              8,
              0,
              dc.OfficeID,
              0,
              dsc.ServiceChannelCD,
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dt.MonthOfYear <= @RptMonth
          AND dt.YearValue = @RptYear
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY dc.OfficeID, dsc.ServiceChannelCD
        HAVING dc.OfficeID LIKE @OfficeIDWork
          AND  dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    -- Pull user details if flag is set
    
    IF @ShowUserDetails = 1
    BEGIN
        INSERT INTO #tmpNewData
          SELECT  'Year',
                  8,
                  0,
                  dc.OfficeID,
                  dc.UserID,
                  'TTL',
                  Count(*)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
			  AND dt.MonthOfYear <= @RptMonth
              AND dt.YearValue = @RptYear
              AND fc.AssignmentTypeID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
			  AND fc.EnabledFlag = 1
            GROUP BY dc.OfficeID, dc.UserID
            HAVING dc.OfficeID LIKE @OfficeIDWork
              AND  dc.UserID LIKE @UserIDWork

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO #tmpNewData
          SELECT  'Year',
                  8,
                  0,
                  dc.OfficeID,
                  dc.UserID,
                  dsc.ServiceChannelCD,
                  Count(*)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
              AND dt.MonthOfYear <= @RptMonth
              AND dt.YearValue = @RptYear
              AND fc.CoverageTypeID IS NOT NULL
			  AND fc.EnabledFlag = 1
            GROUP BY dc.OfficeID, dc.UserID, dsc.ServiceChannelCD
            HAVING dc.OfficeID LIKE @OfficeIDWork
              AND  dc.UserID LIKE @UserIDWork
              AND  dsc.ServiceChannelCD IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
            RETURN
        END
    END
    
    
    -- Pull data for previous 12 months
    
    INSERT INTO #tmpNewData
      SELECT  '12Mth',
              9,
              0,
              0,
              0,
              'TTL',
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        INNER JOIN @tmpTimePeriod12Month tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  '12Mth',
              9,
              0,
              0,
              0,
              dsc.ServiceChannelCD,
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        INNER JOIN @tmpTimePeriod12Month tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  '12Mth',
              9,
              0,
              dc.OfficeID,
              0,
              'TTL',
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        INNER JOIN @tmpTimePeriod12Month tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY dc.OfficeID
        HAVING dc.OfficeID LIKE @OfficeIDWork

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpNewData
      SELECT  '12Mth',
              9,
              0,
              dc.OfficeID,
              0,
              dsc.ServiceChannelCD,
              Count(*)
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
        INNER JOIN @tmpTimePeriod12Month tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.CoverageTypeID IS NOT NULL
		  AND fc.EnabledFlag = 1
        GROUP BY dc.OfficeID, dsc.ServiceChannelCD
        HAVING dc.OfficeID LIKE @OfficeIDWork
          AND  dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
        RETURN
    END

    -- Pull user details if flag is set
    
    IF @ShowUserDetails = 1
    BEGIN
        INSERT INTO #tmpNewData
          SELECT  '12Mth',
                  9,
                  0,
                  dc.OfficeID,
                  dc.UserID,
                  'TTL',
                  Count(*)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            INNER JOIN @tmpTimePeriod12Month tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
              AND fc.AssignmentTypeID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
			  AND fc.EnabledFlag = 1
            GROUP BY dc.OfficeID, dc.UserID
            HAVING dc.OfficeID LIKE @OfficeIDWork
              AND  dc.UserID LIKE @UserIDWork

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO #tmpNewData
          SELECT  '12Mth',
                  9,
                  0,
                  dc.OfficeID,
                  dc.UserID,
                  dsc.ServiceChannelCD,
                  Count(*)
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDNew = dt.TimeID)
            INNER JOIN @tmpTimePeriod12Month tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
              AND fc.CoverageTypeID IS NOT NULL
			  AND fc.EnabledFlag = 1
            GROUP BY dc.OfficeID, dc.UserID, dsc.ServiceChannelCD
            HAVING dc.OfficeID LIKE @OfficeIDWork
              AND  dc.UserID LIKE @UserIDWork
              AND  dsc.ServiceChannelCD IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpNewData', 16, 1, @ProcName)
            RETURN
        END
    END

    IF @OfficeIDWork = '%'
    BEGIN
      DELETE FROM #tmpNewData
         WHERE OfficeID in (SELECT OfficeID 
                                 FROM utb_office 
                                 WHERE InsuranceCompanyID = @InsuranceCompanyID 
                                   AND EnabledFlag = 0)
    END
    

    -- Pivot and save the data extracted

    INSERT INTO @tmpReportData
      SELECT  tmp.OfficeID,
              NULL,
              tmp.UserID,
              NULL,
              NULL, 
              NULL,
              NULL,
              NULL,
              tmp.ServiceChannelCD,
              NULL,
              SUM(CASE PositionID WHEN 1 THEN NewCount ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN NewCount ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN NewCount ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN NewCount ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN NewCount ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN NewCount ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN NewCount ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN NewCount ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN NewCount ELSE 0 END)
        FROM  #tmpNewData tmp
        GROUP BY tmp.OfficeID, tmp.UserID, tmp.ServiceChannelCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpReportData
      SELECT  0,
              NULL,
              0,
              NULL,
              NULL, 
              NULL,
              NULL,
              NULL,
              dsc.ServiceChannelCD,
              NULL,
              0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM  dbo.utb_dtwh_dim_service_channel dsc
        WHERE dsc.ServiceChannelCD NOT IN (SELECT ServiceChannelCD FROM @tmpReportData WHERE OfficeID = 0 and UserID = 0)
          AND dsc.ServiceChannelCD IN (SELECT  at.ServiceChannelDefaultCD 
                                         FROM  dbo.utb_client_assignment_type cat
                                         LEFT JOIN dbo.utb_assignment_type at ON (cat.AssignmentTypeID = at.AssignmentTypeID)
                                         WHERE cat.InsuranceCompanyID = @InsuranceCompanyID)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData', 16, 1, @ProcName) 
        RETURN
    END

    IF NOT EXISTS(SELECT * FROM @tmpReportData WHERE OfficeID = 0 and UserID = 0 AND ServiceChannelCD = 'TTL')
    BEGIN
        INSERT INTO @tmpReportData
          SELECT  0,
                  NULL,
                  0,
                  NULL,
                  NULL, 
                  NULL,
                  NULL,
                  NULL,
                  'TTL',
                  NULL,
                  0, 0, 0, 0, 0, 0, 0, 0, 0
        
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpReportData', 16, 1, @ProcName) 
            RETURN
        END
    END

    INSERT INTO @tmpReportData
      SELECT  o.OfficeID,
              NULL,
              0,
              NULL,
              NULL, 
              NULL,
              NULL,
              NULL,
              dsc.ServiceChannelCD,
              NULL,
              0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM  dbo.utb_office o, dbo.utb_dtwh_dim_service_channel dsc
        WHERE o.OfficeID IN (SELECT OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
          AND o.OfficeID LIKE @OfficeIDWork
          AND Convert(varchar(10), o.OfficeID) + '-0-' + dsc.ServiceChannelCD NOT IN
                (SELECT Convert(varchar(10), OfficeID) + '-0-' + ServiceChannelCD FROM @tmpReportData)
          AND dsc.ServiceChannelCD IN (SELECT  at.ServiceChannelDefaultCD 
                                         FROM  dbo.utb_client_assignment_type cat
                                         LEFT JOIN dbo.utb_assignment_type at ON (cat.AssignmentTypeID = at.AssignmentTypeID)
                                         WHERE cat.InsuranceCompanyID = @InsuranceCompanyID)
                                     
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO @tmpReportData
      SELECT  o.OfficeID,
              NULL,
              0,
              NULL,
              NULL, 
              NULL,
              NULL,
              NULL,
              'TTL',
              NULL,
              0, 0, 0, 0, 0, 0, 0, 0, 0
        FROM  dbo.utb_office o
        WHERE o.OfficeID IN (SELECT OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
          AND o.OfficeID LIKE @OfficeIDWork
          AND Convert(varchar(10), o.OfficeID) + '-0-TTL' NOT IN
                (SELECT Convert(varchar(10), OfficeID) + '-0-' + ServiceChannelCD FROM @tmpReportData)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData', 16, 1, @ProcName) 
        RETURN
    END
    
    IF @OfficeIDWork = '%'
    BEGIN
      DELETE FROM @tmpReportData
         WHERE OfficeID in (SELECT OfficeID 
                                 FROM utb_office 
                                 WHERE InsuranceCompanyID = @InsuranceCompanyID 
                                   AND EnabledFlag = 0)
    END

    -- Pull user details if flag is set
    
    IF @ShowUserDetails = 1
    BEGIN
        INSERT INTO @tmpReportData
          SELECT  u.OfficeID,
                  NULL,
                  u.UserID,
                  NULL,
                  NULL, 
                  NULL,
                  NULL,
                  NULL,
                  dsc.ServiceChannelCD,
                  NULL,
                  0, 0, 0, 0, 0, 0, 0, 0, 0
            FROM  dbo.utb_user u, dbo.utb_dtwh_dim_service_channel dsc
            WHERE u.OfficeID IN (SELECT OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
              AND u.OfficeID LIKE @OfficeIDWork
              AND u.UserID LIKE @UserIDWork
              AND Convert(varchar(10), u.UserID) + '-' + dsc.ServiceChannelCD NOT IN
                    (SELECT Convert(varchar(10), UserID) + '-' + ServiceChannelCD FROM @tmpReportData)
              AND ServiceChannelCD IN (SELECT  at.ServiceChannelDefaultCD 
                                         FROM  dbo.utb_client_assignment_type cat
                                         LEFT JOIN dbo.utb_assignment_type at ON (cat.AssignmentTypeID = at.AssignmentTypeID)
                                         WHERE cat.InsuranceCompanyID = @InsuranceCompanyID)

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpReportData', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO @tmpReportData
          SELECT  u.OfficeID,
                  NULL,
                  u.UserID,
                  NULL,
                  NULL, 
                  NULL,
                  NULL,
                  NULL,
                  'TTL',
                  NULL,
                  0, 0, 0, 0, 0, 0, 0, 0, 0
            FROM  dbo.utb_user u
            WHERE u.OfficeID IN (SELECT OfficeID FROM dbo.utb_office WHERE InsuranceCompanyID = @InsuranceCompanyID)
              AND u.OfficeID LIKE @OfficeIDWork
              AND u.UserID LIKE @UserIDWork
              AND Convert(varchar(10), u.UserID) + '-TTL' NOT IN
                    (SELECT Convert(varchar(10), UserID) + '-'  + ServiceChannelCD FROM @tmpReportData)

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpReportData', 16, 1, @ProcName) 
            RETURN
        END
    END
    
    -- Set fields that will be governing the report
    
    UPDATE @tmpReportData
      SET  OfficeName = CASE
                          WHEN tmp.OfficeID = 0 THEN 'ZZCompany'
                          WHEN Len(LTrim(RTrim(o.ClientOfficeId))) > 0 THEN o.ClientOfficeID
                          ELSE o.Name
                        END,
           UserNameSort = CASE
                            WHEN tmp.UserID = 0 THEN 'ZZOffice'
                            ELSE u.NameLast + ', ' + u.NameFirst + ', ' + Convert(varchar(8), tmp.UserID)
                          END,
           UserNameFirst = CASE
                             WHEN tmp.UserID = 0 OR tmp.ServiceChannelCD = 'TTL' THEN ''
                             ELSE u.NameFirst + CASE 
                                                    WHEN u.ClientUserID is not null THEN ' (' + u.ClientUserID + ')'
                                                    ELSE ''
                                                END
                           END,
           UserNameLast = CASE
                             WHEN tmp.UserID = 0 THEN 'ZZOffice'
                             WHEN tmp.ServiceChannelCD = 'TTL' AND u.EnabledFlag = 0 THEN '(In Active)'
                             WHEN tmp.ServiceChannelCD = 'TTL' AND u.EnabledFlag = 1 THEN ''
                             ELSE u.NameLast
                           END,
           UserEnabled = u.EnabledFlag,
           ServiceChannelName = CASE
                                  WHEN tmp.ServiceChannelCD = 'TTL' THEN 'Total'
                                  ELSE dsc.ServiceChannelDescription
                                END,
           DetailDisplayOrder = CASE
                                  WHEN tmp.ServiceChannelCD = 'PS' THEN 1
                                  WHEN tmp.ServiceChannelCD = 'RRP' THEN 1
                                  WHEN tmp.ServiceChannelCD = 'DR' THEN 2
                                  WHEN tmp.ServiceChannelCD = 'DA' THEN 2
                                  WHEN tmp.ServiceChannelCD = 'IA' THEN 3
                                  WHEN tmp.ServiceChannelCD = 'SA' THEN 4
                                  WHEN tmp.ServiceChannelCD = 'TTL' THEN 5
                                END
      FROM @tmpReportData tmp
      LEFT JOIN dbo.utb_office o ON (tmp.OfficeID = o.OfficeID)
      LEFT JOIN dbo.utb_user u ON (tmp.UserID = u.UserID)
      LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (tmp.ServiceChannelCD = dsc.ServiceChannelCD)      

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpReportData', 16, 1, @ProcName)
        RETURN
    END
    

    
    IF @ShowUserDetails = 1
    BEGIN
        -- Delete out the records for disabled users who have no totals at all

        DELETE FROM @tmpReportData
          WHERE UserID IN (SELECT  UserID 
                             FROM  @tmpReportData
                             WHERE UserEnabled = 0
                               AND MonthTotal = 0
                               AND YearTotal = 0
                               AND Prev12MonthTotal = 0)
                               
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR  ('106|%s|@tmpReportData', 16, 1, @ProcName)
            RETURN
        END
    END

    
    -- Now massage the User Name based on the DetailDisplayOrder. The first line should contain
    -- the user name and the second line should contain the enabled status
    
    UPDATE @tmpReportData
    SET UserNameFirst = '',
        UserNameLast = ''
    FROM @tmpReportData tmp
    WHERE tmp.DetailDisplayOrder > (SELECT min(tmp2.DetailDisplayOrder) FROM @tmpReportData tmp2 WHERE tmp2.UserID = tmp.UserID AND tmp2.OfficeID = tmp.OfficeID)
      AND tmp.DetailDisplayOrder < (SELECT max(tmp2.DetailDisplayOrder) FROM @tmpReportData tmp2 WHERE tmp2.UserID = tmp.UserID AND tmp2.OfficeID = tmp.OfficeID)

    /*select * from @tmpReportData
    order by UserID*/
    -- Final Select
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @OfficeName AS OfficeNameParm,
            @UserName AS UserNameParm,
            @MonthName + ' ' + Convert(varchar(4), @RptYear) AS RptMonth,
            (SELECT  'Week ' + Convert(varchar(2), WeekID) 
               FROM  @tmpTimePeriodCurrent 
               WHERE PositionID = 1) AS Pos1Header1,
            (SELECT  Convert(varchar(2), DatePart(mm, StartDate)) + '/' + Convert(varchar(2), DatePart(dd, StartDate)) + ' - ' + Convert(varchar(2), DatePart(mm, EndDate)) + '/' + Convert(varchar(2), DatePart(dd, EndDate))
               FROM  @tmpTimePeriodCurrent 
               WHERE PositionID = 1) AS Pos1Header2,
            (SELECT  'Week ' + Convert(varchar(2), WeekID) 
               FROM  @tmpTimePeriodCurrent 
               WHERE PositionID = 2) AS Pos2Header1,
            (SELECT  Convert(varchar(2), DatePart(mm, StartDate)) + '/' + Convert(varchar(2), DatePart(dd, StartDate)) + ' - ' + Convert(varchar(2), DatePart(mm, EndDate)) + '/' + Convert(varchar(2), DatePart(dd, EndDate))
               FROM  @tmpTimePeriodCurrent 
               WHERE PositionID = 2) AS Pos2Header2,
            (SELECT  'Week ' + Convert(varchar(2), WeekID) 
               FROM @tmpTimePeriodCurrent 
               WHERE PositionID = 3) AS Pos3Header1,
            (SELECT  Convert(varchar(2), DatePart(mm, StartDate)) + '/' + Convert(varchar(2), DatePart(dd, StartDate)) + ' - ' + Convert(varchar(2), DatePart(mm, EndDate)) + '/' + Convert(varchar(2), DatePart(dd, EndDate))
               FROM  @tmpTimePeriodCurrent 
               WHERE PositionID = 3) AS Pos3Header2,
            (SELECT  'Week ' + Convert(varchar(2), WeekID) 
               FROM @tmpTimePeriodCurrent 
               WHERE PositionID = 4) AS Pos4Header1,
            (SELECT  Convert(varchar(2), DatePart(mm, StartDate)) + '/' + Convert(varchar(2), DatePart(dd, StartDate)) + ' - ' + Convert(varchar(2), DatePart(mm, EndDate)) + '/' + Convert(varchar(2), DatePart(dd, EndDate))
               FROM  @tmpTimePeriodCurrent 
               WHERE PositionID = 4) AS Pos4Header2,
            (SELECT  'Week ' + Convert(varchar(2), WeekID) 
               FROM @tmpTimePeriodCurrent 
               WHERE PositionID = 5) AS Pos5Header1,
            (SELECT  Convert(varchar(2), DatePart(mm, StartDate)) + '/' + Convert(varchar(2), DatePart(dd, StartDate)) + ' - ' + Convert(varchar(2), DatePart(mm, EndDate)) + '/' + Convert(varchar(2), DatePart(dd, EndDate))
               FROM  @tmpTimePeriodCurrent 
               WHERE PositionID = 5) AS Pos5Header2,
            (SELECT  'Week ' + Convert(varchar(2), WeekID) 
               FROM @tmpTimePeriodCurrent 
               WHERE PositionID = 6) AS Pos6Header1,
            (SELECT  Convert(varchar(2), DatePart(mm, StartDate)) + '/' + Convert(varchar(2), DatePart(dd, StartDate)) + ' - ' + Convert(varchar(2), DatePart(mm, EndDate)) + '/' + Convert(varchar(2), DatePart(dd, EndDate))
               FROM  @tmpTimePeriodCurrent 
               WHERE PositionID = 6) AS Pos6Header2,
            'Month' AS Pos7Header1,
            @MonthName AS Pos7Header2,
            'January -' AS Pos8Header1,
            @MonthName AS Pos8Header2,
            'Previous' AS Pos9Header1,
            '12 Months' AS Pos9Header2,
            tmp.*,
            @DataWareHouseDate as DataWareDate   
      FROM  @tmpReportData tmp    
      --ORDER BY tmp.OfficeName, tmp.UserNameSort, tmp.DetailDisplayOrder
               
             
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END 
DROP TABLE #tmpNewData

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientUtilization' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2ClientUtilization TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/