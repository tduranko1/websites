-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminGetMostActiveShops' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminGetMostActiveShops 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminGetMostActiveShops
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets the most active shops base on the last 100 claims
*
* PARAMETERS:  
*				
* RESULT SET:
*				Record Set
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminGetMostActiveShops
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	/*******************************/
	/* Hold SQL Snapshot           */
	/*******************************/
	CREATE TABLE #APDMostActiveShopsSnapShotTmp
	(
	   ShopLocationID INT
	)

	/*******************************/
	/* Hold SQL Snapshot           */
	/*******************************/
	CREATE TABLE #APDMostActiveShopsTmp
	(
	   ShopName VARCHAR(100)
	   , NumberOfAssignments INT
	)

	/*********************************/
	/* Get Shop Assignments Snapshot */
	/*********************************/
	INSERT INTO #APDMostActiveShopsSnapShotTmp
	SELECT TOP 100 
		ShopLocationID
	FROM
		utb_assignment
	WHERE
		ShopLocationID IS NOT NULL
	ORDER BY
		SyslastUpdatedDate DESC

	/*********************************/
	/* Get Shop Assignments Snapshot */
	/*********************************/
	DECLARE db_cursor CURSOR FOR 
	SELECT DISTINCT 
		ShopLocationID
	FROM
		#APDMostActiveShopsSnapShotTmp

	DECLARE @ShopLocationID INT
				
	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO 
		@ShopLocationID
		
		WHILE @@FETCH_STATUS = 0  
		BEGIN  		
			INSERT INTO #APDMostActiveShopsTmp
			SELECT
				--a.ShopLocationID
				sl.[Name] AS ShopName
				, COUNT(a.ShopLocationID) AS NumOfAssignments
				--, a.SyslastUpdatedDate
			FROM
				#APDMostActiveShopsSnapShotTmp a
				INNER JOIN utb_shop_location sl
				ON sl.ShopLocationID = a.ShopLocationID 
			WHERE
				a.ShopLocationID = @ShopLocationID
			GROUP BY
				sl.[Name]

			FETCH NEXT FROM db_cursor INTO 	
			@ShopLocationID
		END  

	CLOSE db_cursor  
	DEALLOCATE db_cursor 					

	SELECT * FROM #APDMostActiveShopsTmp ORDER BY NumberOfAssignments DESC, ShopName 
	
	DROP TABLE #APDMostActiveShopsSnapShotTmp
	DROP TABLE #APDMostActiveShopsTmp	
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminGetMostActiveShops' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminGetMostActiveShops TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAdminGetMostActiveShops TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/