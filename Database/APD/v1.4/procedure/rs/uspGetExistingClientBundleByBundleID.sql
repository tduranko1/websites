-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetExistingClientBundleByBundleID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetExistingClientBundleByBundleID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetExistingClientBundleByBundleID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets existing basic bundling information by BundlingID
*
* PARAMETERS:  
*			@iBundlingID
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspGetExistingClientBundleByBundleID
	@iBundlingID INT = 0
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	IF (@iBundlingID = 0)
	BEGIN
		SELECT
			b.BundlingID
			, mt.MessageTemplateID
			, mt.documentTypeID
			, b.[Name]
			, b.EnabledFlag
			, mt.[Description]
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		ORDER BY
			b.[Name]
	END
	ELSE
	BEGIN
		SELECT
			b.BundlingID
			, mt.MessageTemplateID
			, mt.documentTypeID
			, b.[Name]
			, b.EnabledFlag
			, mt.[Description]
		FROM 
			utb_message_template mt 
			INNER JOIN utb_bundling b 
			ON b.MessageTemplateID = mt.MessageTemplateID 
			INNER JOIN utb_client_bundling cb
			ON cb.BundlingID = b.BundlingID 
		WHERE 
			b.BundlingID = @iBundlingID
		ORDER BY
			b.[Name]
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetExistingClientBundleByBundleID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetExistingClientBundleByBundleID TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetExistingClientBundleByBundleID TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/