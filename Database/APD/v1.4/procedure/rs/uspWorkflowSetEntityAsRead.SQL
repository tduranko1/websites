-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowSetEntityAsRead' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowSetEntityAsRead 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowSetEntityAsRead
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Updates a claimAspect's New Assignment to false if the user passed in is the owner of the Entity
*
* PARAMETERS:  
* (I) @ClaimAspectID        The claim Aspect to update
* (I) @UserID               The user ID
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowSetEntityAsRead
    @ClaimAspectID     AS udt_std_int_big,
    @UserID            AS udt_std_id
AS
BEGIN
    -- Declare internal variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspWorkflowSetEntityAsRead'


    -- Set Database options
    
    SET NOCOUNT ON


    -- Check to make sure a valid ClaimAspectID was passed in

    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid ClaimAspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Get the Owner User ID of the claim.  If this is the same as @UserID, update the claim's NewAssignmentFlag to
    -- false, if not, exit the stored procedure with no action.

    IF @UserID = (SELECT OwnerUserID 
                    FROM dbo.utb_claim_aspect 
                    WHERE ClaimAspectID = @ClaimAspectID
                  )
    BEGIN
        -- The user is the owner.  Update the New Assignment Flag

        BEGIN TRANSACTION WorkflowSetEntityAsReadTran1

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        UPDATE dbo.utb_claim_aspect
          SET  NewAssignmentOwnerFlag = 0,
               PriorityFlag = 0
          WHERE ClaimAspectID = @ClaimAspectID

        -- Check error value
    
        IF @@ERROR <> 0
        BEGIN
            -- Update failure
    
            RAISERROR('104|%s|utb_claim', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

         -- Add an audit log entry
         INSERT INTO dbo.utb_audit_log
         (AuditTypeCD, KeyID, KeyDescription, LogComment, SysLastUserID)
         VALUES
         ('C', @UserID, @ClaimAspectID, 'UserID : ' + convert(varchar, @UserID) + ' has read the claim. NewAssignmentOwnerFlag is set to 0 and priority set to 0', 0)

        COMMIT TRANSACTION WorkflowSetEntityAsReadTran1

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END

	--Project:210474 APD Added the following code when we did the code merge M.A.20061211
    -- Get the Analyst User ID of the claim.  If this is the same as @UserID, update the claim's NewAssignmentFlag to
    -- false, if not, exit the stored procedure with no action.

    IF @UserID = (SELECT AnalystUserID 
                    FROM dbo.utb_claim_aspect 
                    WHERE ClaimAspectID = @ClaimAspectID
                  )
    BEGIN
        -- The user is the owner.  Update the New Assignment Flag

        BEGIN TRANSACTION WorkflowSetEntityAsReadTran1

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        UPDATE dbo.utb_claim_aspect
          SET  NewAssignmentAnalystFlag = 0,
               PriorityFlag = 0
          WHERE ClaimAspectID = @ClaimAspectID

        -- Check error value
    
        IF @@ERROR <> 0
        BEGIN
            -- Update failure
    
            RAISERROR('104|%s|utb_claim', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

         -- Add an audit log entry
         INSERT INTO dbo.utb_audit_log
         (AuditTypeCD, KeyID, KeyDescription, LogComment, SysLastUserID)
         VALUES
         ('C', @UserID, @ClaimAspectID, 'UserID : ' + convert(varchar, @UserID) + ' has read the claim. NewAssignmentAnalystFlag is set to 0 and priority set to 0', 0)

        COMMIT TRANSACTION WorkflowSetEntityAsReadTran1

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END

    -- Get the Support User ID of the claim.  If this is the same as @UserID, update the claim's NewAssignmentFlag to
    -- false, if not, exit the stored procedure with no action.

    IF @UserID = (SELECT SupportUserID 
                    FROM dbo.utb_claim_aspect 
                    WHERE ClaimAspectID = @ClaimAspectID
                  )
    BEGIN
        -- The user is the owner.  Update the New Assignment Flag

        BEGIN TRANSACTION WorkflowSetEntityAsReadTran1

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END


        UPDATE dbo.utb_claim_aspect
          SET  NewAssignmentSupportFlag = 0,
               PriorityFlag = 0
          WHERE ClaimAspectID = @ClaimAspectID

        -- Check error value
    
        IF @@ERROR <> 0
        BEGIN
            -- Update failure
    
            RAISERROR('104|%s|utb_claim', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END

         -- Add an audit log entry
         INSERT INTO dbo.utb_audit_log
         (AuditTypeCD, KeyID, KeyDescription, LogComment, SysLastUserID)
         VALUES
         ('C', @UserID, @ClaimAspectID, 'UserID : ' + convert(varchar, @UserID) + ' has read the claim. NewAssignmentSupportFlag is set to 0 and priority set to 0', 0)

        COMMIT TRANSACTION WorkflowSetEntityAsReadTran1

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END
	--Project:210474 APD Added the code above when we did the code merge M.A.20061211

    RETURN
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowSetEntityAsRead' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowSetEntityAsRead TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
