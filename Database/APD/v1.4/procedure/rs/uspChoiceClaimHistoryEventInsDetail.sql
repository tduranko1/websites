-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceClaimHistoryEventInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceClaimHistoryEventInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspChoiceClaimHistoryEventInsDetail
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     No frills just make a history entry
* Date:			20Nov2015
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	20Nov2015 - TVD - Initial development
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspChoiceClaimHistoryEventInsDetail]
	@LynxID                         udt_std_id_big,
    @EventID                        udt_std_id,
    @ClaimAspectTypeID              udt_std_id = NULL,
    @ClaimAspectNumber              udt_std_int = NULL, 
    @ServiceChannelCD               udt_std_cd = NULL,
    @ClaimAspectServiceChannelID    udt_std_id_big = NULL,
    @Description                    udt_std_desc_long,
    @UserID                         udt_std_id_big,
    @UserRoleID						udt_std_id,		
    @StatusID                       udt_std_id,
    @ClaimVehicleAspectFlag         udt_std_flag = 0
AS
BEGIN
    -- Initialize any empty string parameters
    IF LEN(RTRIM(LTRIM(@Description))) = 0 SET @Description = NULL
    
    -- Declare internal variables

    DECLARE @ClaimAspectIDWork                  udt_std_id_big
    DECLARE @ClaimAspectTypeIDWork              udt_std_id
    DECLARE @ClaimAspectNumberWork              udt_std_int
    DECLARE @ClaimAspectTypeIDClaim             udt_std_id
    DECLARE @ClaimAspectServiceChannelIDWork    udt_std_id_big
    DECLARE @ServiceChannelCDWork               udt_std_cd

    DECLARE @KeyValue                           varchar(40)
    DECLARE @now                                udt_std_datetime
    DECLARE @error                              int
    DECLARE @rowcount                           int
    DECLARE @debug                              bit
    
    SET @debug = 1

    DECLARE @ProcName               varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspChoiceClaimHistoryEventInsDetail'
    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP
  
	IF (@debug = 1)
	BEGIN
		PRINT 'Incoming Parameters...'
		PRINT '   @LynxID: ' + CONVERT(VARCHAR(50),@LynxID)
		PRINT '   @EventID: ' + CONVERT(VARCHAR(50),@EventID)
		PRINT '   @ClaimAspectTypeID: ' + CONVERT(VARCHAR(50),@ClaimAspectTypeID)
		PRINT '   @ClaimAspectNumber: ' + CONVERT(VARCHAR(50),@ClaimAspectNumber)
		PRINT '   @ServiceChannelCD: ' + CONVERT(VARCHAR(50),@ServiceChannelCD)
		PRINT '   @ClaimAspectServiceChannelID: ' + CONVERT(VARCHAR(50),@ClaimAspectServiceChannelID)
		PRINT '   @Description: ' + CONVERT(VARCHAR(50),@Description)
		PRINT '   @UserID: ' + CONVERT(VARCHAR(50),@UserID)
	END    
	----------------------------------------------------
    -- Check to make sure a valid Lynx id was passed in
	----------------------------------------------------
    IF  (@LynxID IS NULL) OR
        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
    BEGIN
        -- Invalid Lynx ID
        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END
    
	----------------------------------------------------
    -- Get ClaimAspectTypeID for Claim
	----------------------------------------------------
    SELECT  @ClaimAspectTypeIDClaim = ClaimAspectTypeID
    FROM  dbo.utb_claim_aspect_type 
    WHERE Name = 'Claim'        

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
	----------------------------------------------------
    -- Check ClaimAspectTypeID for Claim
	----------------------------------------------------
    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
        RAISERROR('%s: Invalid APD Data State.  ClaimAspectTypeID for "Claim" not found.', 16, 1, @ProcName)
        RETURN
    END
    -- Check to make sure a valid Event id was passed in
    IF  (@EventID IS NULL) OR
        (NOT EXISTS(SELECT EventID FROM dbo.utb_event WHERE EventID = @EventID))
    BEGIN
        -- Invalid Event ID
        RAISERROR('101|%s|@EventID|%u', 16, 1, @ProcName, @EventID)
        RETURN
    END

	----------------------------------------------------
    -- Check for good StatusID 
	----------------------------------------------------
    IF (@StatusID IS NULL) OR 
		(NOT EXISTS(SELECT StatusID FROM dbo.utb_status WHERE StatusID = @StatusID))
    BEGIN
       -- Status not returned
    
        RAISERROR  ('111|%s', 16, 1, @ProcName)
        RETURN
    END
    
	----------------------------------------------------
    -- Check to make sure a valid User id was passed in
	----------------------------------------------------
    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

	----------------------------------------------------
    -- Debugging
	----------------------------------------------------
	IF (@debug = 1)
	BEGIN
		PRINT 'Debugging...'
		--PRINT '   @PertainsTo: ' + CONVERT(VARCHAR(50),@PertainsTo)
	END    

	----------------------------------------------------
    -- Validate ClaimAspectServiceChannelID, 
    -- Service channel code and compute 
    -- ClaimAspectServiceChannelID
	----------------------------------------------------
    IF @ClaimAspectServiceChannelID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT ClaimAspectServiceChannelID
                        FROM dbo.utb_claim_aspect_service_channel
                        WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
        BEGIN
            -- Invalid ClaimAspectServiceChannelId
    
            RAISERROR('101|%s|@ClaimAspectServiceChannelID|%u', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
            RETURN
            
        END
        ELSE
        BEGIN
            SELECT @ClaimAspectServiceChannelIDWork = ClaimAspectServiceChannelID,
                   @ServiceChannelCDWork            = ServiceChannelCD
            FROM dbo.utb_claim_aspect_service_channel
            WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

			IF (@debug = 1)
			BEGIN
				PRINT 'ClaimAspectServiceChannelIDWork Parameters...'
				PRINT '    @ClaimAspectServiceChannelIDWork: ' + CONVERT(VARCHAR(50), @ClaimAspectServiceChannelIDWork)
				PRINT '    @ServiceChannelCDWork: ' + CONVERT(VARCHAR(50), @ServiceChannelCDWork)
			END
        END
    END
    ELSE
    BEGIN
        IF @ServiceChannelCD IS NOT NULL
        BEGIN
            IF NOT EXISTS (SELECT ClaimAspectServiceChannelID
                            FROM dbo.utb_claim_aspect ca
                            LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (ca.ClaimAspectID = casc.ClaimAspectID)
                            WHERE ca.LynxID = @LynxID
                              AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDWork
                              AND ca.ClaimAspectNumber = @ClaimAspectNumberWork
                              AND casc.ServiceChannelCD = @ServiceChannelCD
                          )
            BEGIN
                -- Invalid Service channel code for the Lynxid, claim aspect type and claim aspect number
    
                RAISERROR('101|%s|@ServiceChannelCD|%u', 16, 1, @ProcName, @ServiceChannelCD)
                RETURN
                
            END
            ELSE
            BEGIN
                -- Service channel code is valid. Get the ClaimAspectServiceChannelID
                SELECT @ClaimAspectServiceChannelIDWork = ClaimAspectServiceChannelID,
                       @ServiceChannelCDWork            = casc.ServiceChannelCD
                FROM dbo.utb_claim_aspect ca
                LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (ca.ClaimAspectID = casc.ClaimAspectID)
                WHERE ca.LynxID = @LynxID
                  AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDWork
                  AND ca.ClaimAspectNumber = @ClaimAspectNumberWork
                  AND casc.ServiceChannelCD = @ServiceChannelCD

				IF (@debug = 1)
				BEGIN
					PRINT 'ClaimAspectServiceChannelID WAS NULL but good ServiceChannelCD Parameters...'
					PRINT '    @ClaimAspectServiceChannelIDWork: ' + CONVERT(VARCHAR(50), @ClaimAspectServiceChannelIDWork)
					PRINT '    @ServiceChannelCDWork: ' + CONVERT(VARCHAR(50), @ServiceChannelCDWork)
				END
            END
        END
        ELSE
        BEGIN
            -- No service channel and ClaimAspectServiceChannelID was passed. So use the primary service channel for the claim
                SELECT @ClaimAspectServiceChannelIDWork = ClaimAspectServiceChannelID,
                       @ServiceChannelCDWork            = casc.ServiceChannelCD
                FROM dbo.utb_claim_aspect ca
                LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON (ca.ClaimAspectID = casc.ClaimAspectID)
                WHERE ca.LynxID = @LynxID
                  AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDWork
                  AND ca.ClaimAspectNumber = @ClaimAspectNumberWork
                  AND casc.PrimaryFlag = 1

				IF (@debug = 1)
				BEGIN
					PRINT 'ClaimAspectServiceChannelID WAS NULL Parameters...'
					PRINT '    @ClaimAspectServiceChannelIDWork: ' + CONVERT(VARCHAR(50), @ClaimAspectServiceChannelIDWork)
					PRINT '    @ServiceChannelCDWork: ' + CONVERT(VARCHAR(50), @ServiceChannelCDWork)
				END
        END
    END

	----------------------------------------------------
    -- Start the Transaction
	----------------------------------------------------
    BEGIN TRANSACTION HistoryInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
	----------------------------------------------------
    -- Insert a record into claim history
	----------------------------------------------------
    INSERT INTO dbo.utb_history_log
    (
        ClaimAspectNumber,
        ClaimAspectServiceChannelID,
        ClaimAspectTypeID,
        CompletedDate,
        CompletedRoleID,
        CompletedUserID,
        CreatedDate,
        CreatedRoleID,
        CreatedUserID,
        Description,
        EventID,
        LogType,
        LynxID,
        ReferenceID,
        ServiceChannelCD,
        StatusID,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES
    (   
        @ClaimAspectNumber,
        @ClaimAspectServiceChannelIDWork,
        @ClaimAspectTypeID,
        @now,
        @UserRoleID,
        @UserID,
        @now,
        @UserRoleID,
        @UserID,
        @Description,
        @EventID,
        'E',            -- Event entry
        @LynxID,
        NULL,
        @ServiceChannelCD,
        @StatusID,
        @UserID,
        @now
    )

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    IF @error <> 0
    BEGIN
       -- Insertion failure
    
        RAISERROR('105|%s|utb_history_log', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION HistoryInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    RETURN @rowcount

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceClaimHistoryEventInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspChoiceClaimHistoryEventInsDetail TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/