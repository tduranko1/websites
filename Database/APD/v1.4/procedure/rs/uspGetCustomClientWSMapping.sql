-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetCustomClientWSMapping' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetCustomClientWSMapping 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetCustomClientWSMapping
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns all the conditional based XML incoming mappings by LynxID
*
* PARAMETERS:  
*				InsuranceCompanyID
* RESULT SET:
*				RecordSet
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetCustomClientWSMapping
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT 
		WSConfigID
		, InsuranceCompanyID
		, ISNULL(XMLConditionalTag,'') XMLConditionalTag
		, ISNULL(Condition,'') Condition
		, ISNULL(XMLMappingFrom,'') XMLMappingFrom
		, ISNULL(XMLMappingTo,'') XMLMappingTo
		, EnabledFlag
		, SysLastUserID
		, SysLastUpdatedDate
	FROM 
		utb_client_webservice_config
	WHERE
		InsuranceCompanyID = 304		
		AND EnabledFlag = 1

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetCustomClientWSMapping' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetCustomClientWSMapping TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetCustomClientWSMapping TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/