-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewOffice' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateNewOffice 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateNewOffice
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client office by inserting a new recored into the utb_office table.
*
* PARAMETERS:  
*
*  InsuranceCompanyID
*  Address1
*  Address2 
*  AddressCity
*  AddressState 
*  AddressZip
*  CCEmailAddress
*  ClaimNumberFormatJS
*  ClaimNumberValidJS
*  ClaimNumberMsgText
*  ClientOfficeId
*  EnabledFlag 
*  FaxAreaCode
*  FaxExchangeNumber
*  FaxUnitNumber
*  MailingAddress1
*  MailingAddress2 
*  MailingAddressCity 
*  MailingAddressState 
*  MailingAddressZip
*  OfficeName 
*  PhoneAreaCode 
*  PhoneExchangeNumber 
*  PhoneUnitNumber
*  ReturnDocEmailAddress
*  ReturnDocFaxAreaCode
*  ReturnDocFaxExchangeNumber
*  ReturnDocFaxUnitNumber 
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateNewOffice
	@iInsuranceCompanyID INT
    , @vAddress1 VARCHAR(50)
    , @vAddress2 VARCHAR(50)
    , @vAddressCity VARCHAR(30)
    , @vAddressState VARCHAR(2)
    , @vAddressZip VARCHAR(8)
    , @vCCEmailAddress VARCHAR(50)
    , @vClaimNumberFormatJS VARCHAR(250)
    , @vClaimNumberValidJS VARCHAR(250)
    , @vClaimNumberMsgText VARCHAR(500)
    , @vClientOfficeId VARCHAR(50)
	, @bEnabledFlag BIT
    , @cFaxAreaCode CHAR(3)
    , @cFaxExchangeNumber CHAR(3)
    , @cFaxExtensionNumber CHAR(5)
    , @cFaxUnitNumber CHAR(4)
    , @vMailingAddress1 VARCHAR(50)
    , @vMailingAddress2 VARCHAR(50)
    , @vMailingAddressCity VARCHAR(30)
    , @vMailingAddressState VARCHAR(2)
    , @vMailingAddressZip VARCHAR(8)
    , @vOfficeName VARCHAR(50)
    , @cPhoneAreaCode CHAR(3)
    , @cPhoneExchangeNumber CHAR(3)
    , @cPhoneExtensionNumber CHAR(5)
    , @cPhoneUnitNumber CHAR(4)
    , @vReturnDocDestinationValue VARCHAR(250)
    , @vReturnDocEmailAddress VARCHAR(50)
    , @cReturnDocFaxAreaCode CHAR(3)
    , @cReturnDocFaxExchangeNumber CHAR(3)
    , @cReturnDocFaxExtensionNumber CHAR(5)
    , @cReturnDocFaxUnitNumber CHAR(4)
    , @iUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS int
    DECLARE @rowcount AS int
    DECLARE @now AS datetime 
    DECLARE @iOfficeID AS INT
    DECLARE @ProcName AS varchar(30)

    SET @ProcName = 'uspCreateNewOffice'

    -- Set Database options
    SET NOCOUNT ON
	SET @now = CURRENT_TIMESTAMP
	SET @iOfficeID = 0
	
	-- Check to make sure a valid insurance company id was passed in
    IF  (@iInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iInsuranceCompanyID|%u', 16, 1, @ProcName, @iInsuranceCompanyID)
        RETURN
    END
    
    --Verify that a valid Insurance Company already exist
    IF (@iInsuranceCompanyID IS Null)
    OR NOT EXISTS (SELECT InsuranceCompanyID FROM utb_Insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID)
    OR EXISTS (SELECT OfficeID FROM utb_office WHERE [Name] = @vOfficeName)
    BEGIN
        -- Office already exists
        RETURN 0
    END
    ELSE
    BEGIN
	INSERT INTO dbo.utb_office
			(
				 InsuranceCompanyID
				 , Address1
				 , Address2 
				 , AddressCity
				 , AddressState 
				 , AddressZip
				 , CCEmailAddress
				 , ClaimNumberFormatJS
				 , ClaimNumberValidJS
				 , ClaimNumberMsgText
				 , ClientOfficeId
				 , EnabledFlag 
				 , FaxAreaCode
				 , FaxExchangeNumber
				 , FaxUnitNumber
				 , MailingAddress1
				 , MailingAddress2 
				 , MailingAddressCity 
				 , MailingAddressState 
				 , MailingAddressZip
				 , Name 
				 , PhoneAreaCode 
				 , PhoneExchangeNumber 
				 , PhoneUnitNumber
				 , ReturnDocEmailAddress
				 , ReturnDocFaxAreaCode
				 , ReturnDocFaxExchangeNumber
				 , ReturnDocFaxUnitNumber 
				 , SysLastUserID
				 , SysLastUpdatedDate 
			)
			VALUES
			( 
				@iInsuranceCompanyID   
				, @vAddress1
				, @vAddress2
				, @vAddressCity
				, @vAddressState 
				, @vAddressZip
				, @vCCEmailAddress
				, @vClaimNumberFormatJS
				, @vClaimNumberValidJS
				, @vClaimNumberMsgText
				, @vClientOfficeId
				, @bEnabledFlag 
				, @cFaxAreaCode
				, @cFaxExchangeNumber
				, @cFaxUnitNumber
				, @vMailingAddress1
				, @vMailingAddress2 
				, @vMailingAddressCity 
				, @vMailingAddressState 
				, @vMailingAddressZip
				, @vOfficeName 
				, @cPhoneAreaCode 
				, @cPhoneExchangeNumber 
				, @cPhoneUnitNumber
				, @vReturnDocEmailAddress
				, @cReturnDocFaxAreaCode
				, @cReturnDocFaxExchangeNumber
				, @cReturnDocFaxUnitNumber 
				, @iUserID
				, @now
			)

			SET @iOfficeID = @@IDENTITY
			
			IF EXISTS(SELECT * FROM utb_office WHERE InsuranceCompanyID = @iInsuranceCompanyID AND OfficeID = @iOfficeID)
			BEGIN
				SELECT OfficeID FROM utb_office WHERE InsuranceCompanyID = @iInsuranceCompanyID AND OfficeID = @iOfficeID
			END
			ELSE
			BEGIN
				SELECT 0
			END
		END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewOffice' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateNewOffice TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateNewOffice TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/