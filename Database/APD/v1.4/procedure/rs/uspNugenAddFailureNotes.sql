
BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspNugenAddFailureNotes' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspNugenAddFailureNotes
END


GO
/****** Object:  StoredProcedure [dbo].[uspNugenAddFailureNotes]    Script Date: 03/06/2013 06:51:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspNugenAddFailureNotes]
(
@LynxID varchar(15),
@Description varchar(500),
@VehicleNumber varchar(10)
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @ClaimAspectServiceChannelID varchar(20)
DECLARE @ClaimAspectID Varchar(20)
DECLARE @UserID varchar(10)


IF EXISTS(Select ClaimAspectID from utb_claim_aspect where LynxID = @LynxID AND InitialAssignmentTypeID Is Not Null AND ClaimAspectNumber = @VehicleNumber)
		BEGIN
		SET @ClaimAspectID =(SELECT ClaimAspectID
			FROM utb_claim_aspect 
			WHERE LynxID = @LynxID and InitialAssignmentTypeID IS NOT NULL AND ClaimAspectNumber = @VehicleNumber)			

		SET @UserID =(SELECT SysLastUserID
			FROM utb_claim_aspect 
			WHERE ClaimAspectID = @ClaimAspectID and InitialAssignmentTypeID IS NOT NULL)	
		END



IF EXISTS(Select ClaimAspectServiceChannelID from utb_claim_aspect_service_channel where ClaimAspectID = @ClaimAspectID)
		BEGIN
		SET @ClaimAspectServiceChannelID =(SELECT ClaimAspectServiceChannelID
			FROM utb_claim_aspect_service_channel 
			WHERE ClaimAspectID = @ClaimAspectID)		
		END



--print @ClaimAspectServiceChannelID
--print @ClaimAspectID
--print @UserID
--print @ClaimAspectNumber
--print @ClaimAspectTypeID
--print @ServiceChannelCD


  EXEC dbo.uspNoteInsDetail					  @ClaimAspectID				= @ClaimAspectID,
                                              @NoteTypeID					= '14',
                                              @StatusID						= '500',
                                              @Note							= @Description,
                                              @UserID						= @UserID,
                                              @PrivateFlag					= 1,
                                              @ClaimAspectServiceChannelID  = @ClaimAspectServiceChannelID                                            
                                             


SET NOCOUNT OFF
END

GO
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'uspNugenAddFailureNotes' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspNugenAddFailureNotes TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO