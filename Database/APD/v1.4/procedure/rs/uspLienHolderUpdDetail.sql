-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspLienHolderUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspLienHolderUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspLienHolderUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Inserts/Updates Lien Holder information
*
* PARAMETERS:  
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspLienHolderUpdDetail
    @LienHolderID           udt_std_int_big         OUTPUT,    
    @Name                   udt_std_name,
    @Address1               udt_addr_line_1,
    @Address2               udt_addr_line_2         = NULL,
    @AddressCity            udt_addr_city,
    @AddressState           udt_addr_state,
    @AddressZip             udt_addr_zip_code,
    @ContactName            udt_std_name            = NULL,
    @EmailAddress           udt_web_email           = NULL,
    @FaxAreaCode            udt_ph_area_code        = NULL,
    @FaxExchangeNumber      udt_ph_exchange_number  = NULL,
    @FaxUnitNumber          udt_ph_unit_number      = NULL,
    @MailingAddress1        udt_addr_line_1         = NULL,
    @MailingAddress2        udt_addr_line_2         = NULL,
    @MailingAddressCity     udt_addr_city           = NULL,
    @MailingAddressState    udt_addr_state          = NULL,
    @MailingAddressZip      udt_addr_zip_code       = NULL,
    @OfficeName             udt_std_name            = NULL,
    @PhoneAreaCode          udt_ph_area_code        = NULL,
    @PhoneExchangeNumber    udt_ph_exchange_number  = NULL,
    @PhoneUnitNumber        udt_ph_unit_number      = NULL,
    @PhoneExtensionNumber   udt_ph_extension_number = NULL,
    @WebsiteAddress         udt_web_address         = NULL,
    @UpdateIfExists         bit                     = 0,
    @UserID                 udt_std_id
AS
BEGIN

    --Initialize string parameters
    
    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@ContactName))) = 0 SET @ContactName = NULL
    IF LEN(RTRIM(LTRIM(@EmailAddress))) = 0 SET @EmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@MailingAddress1))) = 0 SET @MailingAddress1 = NULL
    IF LEN(RTRIM(LTRIM(@MailingAddress2))) = 0 SET @MailingAddress2 = NULL
    IF LEN(RTRIM(LTRIM(@MailingAddressCity))) = 0 SET @MailingAddressCity = NULL
    IF LEN(RTRIM(LTRIM(@MailingAddressZip))) = 0 SET @MailingAddressZip = NULL
    IF LEN(RTRIM(LTRIM(@OfficeName))) = 0 SET @OfficeName = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@WebsiteAddress))) = 0 SET @WebsiteAddress = NULL

     -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(50)
    DECLARE @now        AS datetime
    DECLARE @BillingID  AS bigint
     
    SET @ProcName = 'uspLienHolderUpdDetail'
    SET @now = CURRENT_TIMESTAMP
    
    IF @OfficeName IS NULL SET @OfficeName = @Name
    
    IF @UserID > 0 
    BEGIN
        IF NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
        BEGIN
            -- Raise error
            RAISERROR('%s: Invalid UserID %s', 16, 1, @ProcName, @UserID)
            RETURN
        END
    END 
    
    SELECT @LienHolderID = LienHolderID
    FROM dbo.utb_lien_holder
    WHERE AddressZip = @AddressZip
      AND PhoneAreaCode = @PhoneAreaCode
      AND PhoneExchangeNumber = @PhoneExchangeNumber
      AND PhoneUnitNumber = @PhoneUnitNumber
    
    BEGIN TRANSACTION lienHolderTrans
    
    IF @LienHolderID IS NULL
    BEGIN
        -- Lien holder does not exist. create one.
        -- first let us create empty billing record
        INSERT INTO dbo.utb_billing(
            Address1,
            Address2,
            AddressCity,
            AddressState,
            AddressZip,
            FaxAreaCode,
            FaxExchangeNumber,
            FaxUnitNumber,
            Name,
            PhoneAreaCode,
            PhoneExchangeNumber,
            PhoneUnitNumber,
            SysLastUserID,
            SysLastUpdatedDate
        ) VALUES (
            @Address1,
            @Address2,
            @AddressCity,
            @AddressState,
            @AddressZip,
            @FaxAreaCode,
            @FaxExchangeNumber,
            @FaxUnitNumber,
            @Name,
            @PhoneAreaCode,
            @PhoneExchangeNumber,
            @PhoneUnitNumber,
            @UserID,
            @now
        )
        

        IF @@ERROR <> 0
        BEGIN
            -- Insertion Failure
    
            ROLLBACK TRANSACTION lienHolderTrans
            RAISERROR('%s: Error inserting into utb_billing.', 16, 1, @ProcName)
            RETURN
        END

        SET @BillingID = SCOPE_IDENTITY()
        
        INSERT INTO dbo.utb_lien_holder (
            Address1,
            Address2,
            AddressCity,
            AddressState,
            AddressZip,
            BillingID,
            ContactName,
            EmailAddress,
            EnabledFlag,
            FaxAreaCode,
            FaxExchangeNumber,
            FaxUnitNumber,
            MailingAddress1,
            MailingAddress2,
            MailingAddressCity,
            MailingAddressState,
            MailingAddressZip,
            Name,
            OfficeName,
            PhoneAreaCode,
            PhoneExchangeNumber,
            PhoneUnitNumber,
            PhoneExtensionNumber,
            WebsiteAddress,
            SysLastUserID,
            SysLastUpdatedDate
        ) VALUES (
            @Address1,
            @Address2,
            @AddressCity,
            @AddressState,
            @AddressZip,
            @BillingID,
            @ContactName,
            @EmailAddress,
            1,
            @FaxAreaCode,
            @FaxExchangeNumber,
            @FaxUnitNumber,
            @MailingAddress1,
            @MailingAddress2,
            @MailingAddressCity,
            @MailingAddressState,
            @MailingAddressZip,
            @Name,
            @OfficeName,
            @PhoneAreaCode,
            @PhoneExchangeNumber,
            @PhoneUnitNumber,
            @PhoneExtensionNumber,
            @WebsiteAddress,
            @UserID,
            @now
        )
        
        SET @LienHolderID = SCOPE_IDENTITY()

        IF @@ERROR <> 0
        BEGIN
            -- Insertion Failure
    
            ROLLBACK TRANSACTION lienHolderTrans
            RAISERROR('%s: Error inserting into utb_lien_holder.', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
        IF @UpdateIfExists = 1
        BEGIN
            -- Lien Holder exists. Update information?
            UPDATE dbo.utb_lien_holder
            SET Address1 = @Address1,
                Address2 = @Address2,
                AddressCity = @AddressCity,
                AddressState = @AddressState,
                AddressZip = @AddressZip,
                ContactName = @ContactName,
                EmailAddress = @EmailAddress,
                EnabledFlag = 1,
                FaxAreaCode = @FaxAreaCode,
                FaxExchangeNumber = @FaxExchangeNumber,
                FaxUnitNumber = @FaxUnitNumber,
                Name = @Name,
                OfficeName = @OfficeName,
                PhoneAreaCode = @PhoneAreaCode,
                PhoneExchangeNumber = @PhoneExchangeNumber,
                PhoneUnitNumber = @PhoneUnitNumber,
                PhoneExtensionNumber = @PhoneExtensionNumber,
                WebsiteAddress = @WebsiteAddress,
                SysLastUserID = @UserID,
                SysLastUpdatedDate = @now
            WHERE LienHolderID = @LienHolderID

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
        
                ROLLBACK TRANSACTION lienHolderTrans
                RAISERROR('%s: Error updating utb_lien_holder.', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    
    COMMIT TRANSACTION lienHolderTrans
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspLienHolderUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspLienHolderUpdDetail TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
