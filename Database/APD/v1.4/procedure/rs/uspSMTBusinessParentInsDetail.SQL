-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTBusinessParentInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTBusinessParentInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTBusinessParentInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Insert a new Shop Parent
*
* PARAMETERS:  
* (I) @Address1                           The Shop Parent's address line 1
* (I) @Address2                           The Shop Parent's address line 2
* (I) @AddressCity                        The Shop Parent's address city
* (I) @AddressCounty                      The Shop Parent's address county
* (I) @AddressState                       The Shop Parent's address state
* (I) @AddressZip                         The Shop Parent's address zip code
* (I) @FaxAreaCode                        The Shop Parent's fax area code
* (I) @FaxExchangeNumber                  The Shop Parent's fax exchange number
* (I) @FaxExtensionNumber                 The Shop Parent's fax extension number
* (I) @FaxUnitNumber                      The Shop Parent's fax unit number
* (I) @Name                               The Shop Parent's name
* (I) @PhoneAreaCode                      The Shop Parent's phone area code
* (I) @PhoneExchangeNumber                The Shop Parent's phone exchange number
* (I) @PhoneExtensionNumber               The Shop Parent's phone extension number
* (I) @PhoneUnitNumber                    The Shop Parent's phone unit number
* (I) @SysLastUserID                      The Shop Parent's updating user identity
*
* RESULT SET:
* ShopParentID                            The newly created Shop Parent unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTBusinessParentInsDetail
(
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @Name                               udt_std_name,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error                      udt_std_int
    DECLARE @rowcount                   udt_std_int

    DECLARE @BusinessParentID           udt_std_id_big

    DECLARE @ProcName                   varchar(30)       -- Used for raise error stmts 
    DECLARE @LogComment                 udt_std_desc_long

    SET @ProcName = 'uspSMTBusinessParentInsDetail'


    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0 SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL


    -- Apply edits
    
    IF LEN(@Name) < 1
    BEGIN
       -- Missing Shop Parent Name
    
        RAISERROR  ('1|Missing Shop Parent Name', 16, 1, @ProcName)
        RETURN
    END

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop Parent Address State
        
            RAISERROR  ('1|Invalid Shop Parent Address State', 16, 1, @ProcName)
            RETURN
        END
    END

    IF @AddressZip IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT Zip FROM utb_zip_code WHERE Zip = @AddressZip)
        BEGIN
           -- Invalid Shop Parent Address Zip Code
        
            RAISERROR  ('1|Invalid Shop Parent Address Zip Code', 16, 1, @ProcName)
            RETURN
        END
    END


    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('101|%s|@SysLastUserID|NULL', 16, 1, @ProcName)
        RETURN
    END


    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopParentInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_shop_parent
   	(
        Address1,
        Address2,
        AddressCity,
        AddressCounty,
        AddressState,
        AddressZip,
        FaxAreaCode,
        FaxExchangeNumber,
        FaxExtensionNumber,
        FaxUnitNumber,
        Name,
        PhoneAreaCode,
        PhoneExchangeNumber,
        PhoneExtensionNumber,
        PhoneUnitNumber,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES	
    (
        @Address1,
        @Address2,
        @AddressCity,
        @AddressCounty,
        @AddressState,
        @AddressZip,
        @FaxAreaCode,
        @FaxExchangeNumber,
        @FaxExtensionNumber,
        @FaxUnitNumber,
        @Name,
        @PhoneAreaCode,
        @PhoneExchangeNumber,
        @PhoneExtensionNumber,
        @PhoneUnitNumber,
        @SysLastUserID,
        CURRENT_TIMESTAMP
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    SET @BusinessParentID = SCOPE_IDENTITY()
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|utb_shop_parent', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END


    -- Begin Audit Log code  -------------------------------------------------------------------------------------
    
       
    IF @Name IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent created: ' + @Name
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF @Address1 IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Address 1 established: ' + @Address1
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @Address2 IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Address 2 established: ' + @Address2
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @AddressZip IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Zip Code established: ' + @AddressZip
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF @AddressCity IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent City established: ' + @AddressCity
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    IF @AddressState IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent State established: ' + @AddressState
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF @AddressCounty IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent County established: ' + @AddressCounty
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
    
    
    IF @PhoneAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Phone Area Code established: ' + @PhoneAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF @PhoneExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Phone Exchange Number established: ' + @PhoneExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
      
    
    IF @PhoneUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Phone Unit Number established: ' + @PhoneUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PhoneExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Phone Extension Number established: ' + @PhoneExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Fax Area Code established: ' + @FaxAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    
    IF @FaxExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Fax Exchange Number established: ' + @FaxExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF @FaxUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Fax Unit Number established: ' + @FaxUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END
    END
    
    
    IF @FaxExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Parent Fax Extension Number established: ' + @FaxExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    -- End of Audit Log code  ------------------------------------------------------------------------------------


    COMMIT TRANSACTION AdmShopParentInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @BusinessParentID AS BusinessParentID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTBusinessParentInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTBusinessParentInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/