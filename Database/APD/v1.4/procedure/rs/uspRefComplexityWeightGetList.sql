-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefComplexityWeightGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefComplexityWeightGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefComplexityWeightGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Retrieve all Complexity Weights
*
* PARAMETERS:  
* None.
*
* RESULT SET:
* ObjectId                      Complexity Weight object identity
* ObjectName                    Complexity Weight object name
* ObjectRowID                   Complexity Weight object's row identity
* Description                   Complexity Weight object's row factor description
* Factor                        Complexity Weight object's row factor
* MultiplierFlag                Complexity Weight object's row multiplier flag
* SysMaintainedFlag             Complexity Weight system maintained flag
* Value                         Complexity Weight object's row weight value
* SysLastUserID                 Complexity Weight last user
* SysLastUpdatedDate            Complexity Weight last update date
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefComplexityWeightGetList
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspRefComplexityWeightGetList'


    SELECT 
         ObjectId,
         OBJECT_NAME(ObjectId) AS ObjectName,
         ObjectRowID,
         dbo.ufnClaimGetComplexityDescription(ObjectId, ObjectRowID) AS Description,
         dbo.ufnClaimGetComplexityFactor(ObjectId, ObjectRowID) AS Factor,
         MultiplierFlag,
         SysMaintainedFlag,
         Value,
         SysLastUserID,
         dbo.ufnUtilityGetDateString(SysLastUpdatedDate) AS SysLastUpdatedDate
    FROM dbo.utb_complexity_weight
    ORDER BY OBJECT_NAME(ObjectId), ObjectRowID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefComplexityWeightGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefComplexityWeightGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/