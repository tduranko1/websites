-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLLienHolderUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLLienHolderUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLLienHolderUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Inserts/Updates Lien Holder information
*
* PARAMETERS:  
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLLienHolderUpdDetail
    @ClaimAspectServiceChannelID    udt_std_int_big,
    @LienHolderID                   udt_std_int_big,    
    @ParentLienHolderID             udt_std_int_big         = NULL,
    @ChildLienHolderID              udt_std_int_big         = NULL,
    @BusinessTypeCD                 udt_std_cd              = NULL,
    @Name                           udt_std_name,
    @Address1                       udt_addr_line_1         = NULL,
    @Address2                       udt_addr_line_2         = NULL,
    @AddressCity                    udt_addr_city           = NULL,
    @AddressState                   udt_addr_state          = NULL,
    @AddressZip                     udt_addr_zip_code       = NULL,
    @ContactName                    udt_std_name            = NULL,
    @EmailAddress                   udt_web_email           = NULL,
    @FaxAreaCode                    udt_ph_area_code        = NULL,
    @FaxExchangeNumber              udt_ph_exchange_number  = NULL,
    @FaxUnitNumber                  udt_ph_unit_number      = NULL,
    @FedTaxId                       udt_fed_tax_id          = NULL,
    @OfficeName                     udt_std_name            = NULL,
    @PhoneAreaCode                  udt_ph_area_code        = NULL,
    @PhoneExchangeNumber            udt_ph_exchange_number  = NULL,
    @PhoneUnitNumber                udt_ph_unit_number      = NULL,
    @PhoneExtensionNumber           udt_ph_extension_number = NULL,
    @BillingName                    udt_std_name,
    @BillingAddress1                udt_addr_line_1         = NULL,
    @BillingAddress2                udt_addr_line_2         = NULL,
    @BillingAddressCity             udt_addr_city           = NULL,
    @BillingAddressState            udt_addr_state          = NULL,
    @BillingAddressZip              udt_addr_zip_code       = NULL,
    @BillingFaxAreaCode             udt_ph_area_code        = NULL,
    @BillingFaxExchangeNumber       udt_ph_exchange_number  = NULL,
    @BillingFaxUnitNumber           udt_ph_unit_number      = NULL,
    @BillingPhoneAreaCode           udt_ph_area_code        = NULL,
    @BillingPhoneExchangeNumber     udt_ph_exchange_number  = NULL,
    @BillingPhoneUnitNumber         udt_ph_unit_number      = NULL,
    @BillingPhoneExtensionNumber    udt_ph_extension_number = NULL,
    @EFTAccountNumber               udt_std_desc_short      = NULL,
    @EFTAccountTypeCD               udt_std_cd              = NULL,
    @EFTContractSignedFlag          udt_std_flag            = 0,
    @EFTEffectiveDate               udt_std_datetime        = NULL,
    @EFTRoutingNumber               udt_std_desc_short      = NULL,
    @UserID                         udt_std_id
AS
BEGIN

    SEt NOCOUNT ON
    --Initialize string parameters
    
    IF LEN(RTRIM(LTRIM(@BusinessTypeCD))) = 0 SET @BusinessTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@ContactName))) = 0 SET @ContactName = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@FedTaxId))) = 0 SET @FedTaxId = NULL
    IF LEN(RTRIM(LTRIM(@OfficeName))) = 0 SET @OfficeName = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL

    IF LEN(RTRIM(LTRIM(@BillingAddress1))) = 0 SET @BillingAddress1 = NULL
    IF LEN(RTRIM(LTRIM(@BillingAddress2))) = 0 SET @BillingAddress2 = NULL
    IF LEN(RTRIM(LTRIM(@BillingAddressCity))) = 0 SET @BillingAddressCity = NULL
    IF LEN(RTRIM(LTRIM(@BillingAddressState))) = 0 SET @BillingAddressState = NULL
    IF LEN(RTRIM(LTRIM(@BillingAddressZip))) = 0 SET @BillingAddressZip = NULL
    IF LEN(RTRIM(LTRIM(@BillingFaxAreaCode))) = 0 SET @BillingFaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@BillingFaxExchangeNumber))) = 0 SET @BillingFaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@BillingFaxUnitNumber))) = 0 SET @BillingFaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@BillingPhoneAreaCode))) = 0 SET @BillingPhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@BillingPhoneExchangeNumber))) = 0 SET @BillingPhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@BillingPhoneUnitNumber))) = 0 SET @BillingPhoneUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@BillingPhoneExtensionNumber))) = 0 SET @BillingPhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@EFTAccountNumber))) = 0 SET @EFTAccountNumber = NULL
    IF LEN(RTRIM(LTRIM(@EFTAccountTypeCD))) = 0 SET @EFTAccountTypeCD = NULL
    IF LEN(RTRIM(LTRIM(@EFTContractSignedFlag))) = 0 SET @EFTContractSignedFlag = NULL
    IF LEN(RTRIM(LTRIM(@EFTEffectiveDate))) = 0 SET @EFTEffectiveDate = NULL
    IF LEN(RTRIM(LTRIM(@EFTRoutingNumber))) = 0 SET @EFTRoutingNumber = NULL
    
    IF @ParentLienHolderID = @LienHolderID SET @ParentLienHolderID = NULL


     -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(50)
    DECLARE @now        AS datetime
    DECLARE @BillingID  AS bigint
    DECLARE @LienHolderID_Old as bigint
    DECLARE @FedTaxID_Old as varchar(50)
    DECLARE @ParentLHFedTaxID_Old as varchar(50)
    DecLARE @c_LienHolderID as bigint
    DECLARE @EventIDTLLienHolderCreated     AS udt_std_id
    DECLARE @EventIDTLLienHolderMissingBilling AS udt_std_id
     
    SET @ProcName = 'uspTLLienHolderUpdDetail'
    SET @now = CURRENT_TIMESTAMP
    
    IF @OfficeName IS NULL SET @OfficeName = @Name
    
    IF NOT EXISTS(SELECT ClaimAspectServiceChannelID 
                  FROM dbo.utb_claim_aspect_service_channel 
                  WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
    BEGIN
        -- Raise error
        RAISERROR('%s: Invalid ClaimAspectServiceChannelID %s', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
        RETURN
    END

    IF @LienHolderID <> -1 AND
	   NOT EXISTS(SELECT LienHolderID 
                  FROM dbo.utb_lien_holder
                  WHERE LienHolderID = @LienHolderID)
    BEGIN
        -- Raise error
        RAISERROR('%s: Invalid LienHolderID %s', 16, 1, @ProcName, @LienHolderID)
        RETURN
    END

    IF NOT EXISTS(SELECT UserID 
                  FROM dbo.utb_user 
                  WHERE UserID = @UserID)
    BEGIN
        -- Raise error
        RAISERROR('%s: Invalid UserID %s', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    IF @Name IS NULL
    BEGIN
        -- Raise error
        RAISERROR('%s: Invalid Lien Holder Name %s', 16, 1, @ProcName, @Name)
        RETURN
    END
    
    IF @BillingName IS NULL
    BEGIN
        SET @BillingName = @Name
    END

    SELECT  @EventIDTLLienHolderCreated = EventID
      FROM  dbo.utb_event
      WHERE Name = 'Total Loss Lien Holder Created'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
    IF @EventIDTLLienHolderCreated IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Total Loss Lien Holder Created Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END    

    SELECT  @EventIDTLLienHolderMissingBilling = EventID
      FROM  dbo.utb_event
      WHERE Name = 'Total Loss Lien Holder Missing Billing'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
    IF @EventIDTLLienHolderMissingBilling IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Total Loss Lien Holder Missing Billing Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END    
    
    SELECT @LienHolderID_Old = LienHolderID
    FROM dbo.utb_claim_aspect_service_channel 
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
    
    SELECT @BillingID = BillingID,
           @FedTaxID_Old = FedTaxID
    FROM dbo.utb_lien_holder
    WHERE LienHolderID = @LienHolderID
    
    SELECT @ParentLHFedTaxID_Old = FedTaxID
    FROM dbo.utb_lien_holder
    WHERE LienHolderID = @ParentLienHolderID
    
    IF @ParentLHFedTaxID_Old <> @FedTaxID
    begin
      set @ParentLienHolderID = null
    end
    
    BEGIN TRANSACTION 
    
    IF @LienHolderID IS NOT NULL AND 
	   @LienHolderID <> -1 AND
	   (@LienHolderID_Old <> @LienHolderID OR 
        @LienHolderID_Old IS NULL)
    BEGIN
       UPDATE dbo.utb_claim_aspect_service_channel 
       SET LienHolderID = @LienHolderID
       WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

       IF @@ERROR <> 0
       BEGIN
           -- Insertion Failure 

           ROLLBACK TRANSACTION 
           RAISERROR('%s: Error updating utb_claim_aspect_service_channel.', 16, 1, @ProcName)
           RETURN
       END
    END
    
    IF @LienHolderID = -1
    BEGIN
        -- Lien Holder is being added to the claim. Create  it.
        EXEC uspLienHolderUpdDetail  @LienHolderID = @c_LienHolderID output,
                                     @Name = @Name,
                                     @Address1 = @Address1,
                                     @Address2 = @Address2,
                                     @AddressCity = @AddressCity,
                                     @AddressState = @AddressState,
                                     @AddressZip = @AddressZip,
                                     @ContactName = @ContactName,
                                     @EmailAddress = @EmailAddress,
                                     @FaxAreaCode = @FaxAreaCode,
                                     @FaxExchangeNumber = @FaxExchangeNumber,
                                     @FaxUnitNumber = @FaxUnitNumber,
                                     @PhoneAreaCode = @PhoneAreaCode,
                                     @PhoneExchangeNumber = @PhoneExchangeNumber,
                                     @PhoneUnitNumber = @PhoneUnitNumber,
                                     @PhoneExtensionNumber = @PhoneExtensionNumber,
                                     @UpdateIfExists = 0,
                                     @UserID = @UserID

        IF @@ERROR <> 0
        BEGIN
            -- Insertion Failure
    
            ROLLBACK TRANSACTION
            RAISERROR('%s: (uspLienHolderUpdDetail %s Processing) Error add/update Lien Holder', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
            RETURN
        END
        
        SET @LienHolderID = @c_LienHolderID

		SELECT @BillingID = BillingID,
			   @FedTaxID_Old = FedTaxID
		FROM dbo.utb_lien_holder
		WHERE LienHolderID = @LienHolderID
		
		
       UPDATE dbo.utb_claim_aspect_service_channel 
       SET LienHolderID = @LienHolderID
       WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

       IF @@ERROR <> 0
       BEGIN
           -- Insertion Failure 

           ROLLBACK TRANSACTION 
           RAISERROR('%s: Error updating utb_claim_aspect_service_channel.', 16, 1, @ProcName)
           RETURN
       END
      		
    END
    
    
	UPDATE dbo.utb_lien_holder
	SET Address1 = @Address1,
		Address2 = @Address2,
		AddressCity = @AddressCity,
		AddressState = @AddressState,
		AddressZip = @AddressZip,
		BusinessTypeCD = @BusinessTypeCD,
		ContactName = @ContactName,
		EmailAddress = @EmailAddress,
		EnabledFlag = 1,
		FaxAreaCode = @FaxAreaCode,
		FaxExchangeNumber = @FaxExchangeNumber,
		FaxUnitNumber = @FaxUnitNumber,
		FedTaxID = @FedTaxID,
		Name = @Name,
		OfficeName = @OfficeName,
		ParentLienHolderID = @ParentLienHolderID,
		PhoneAreaCode = @PhoneAreaCode,
		PhoneExchangeNumber = @PhoneExchangeNumber,
		PhoneUnitNumber = @PhoneUnitNumber,
		PhoneExtensionNumber = @PhoneExtensionNumber,
		SysLastUserID = @UserID,
		SysLastUpdatedDate = @now
	WHERE LienHolderID = @LienHolderID

	IF @@ERROR <> 0
	BEGIN
		-- Insertion Failure 

		ROLLBACK TRANSACTION 
		RAISERROR('%s: Error updating utb_lien_holder.', 16, 1, @ProcName)
		RETURN
	END

	UPDATE dbo.utb_billing
	SET Address1 = @BillingAddress1,
		Address2 = @BillingAddress2,
		AddressCity = @BillingAddressCity,
		AddressState = @BillingAddressState,
		AddressZip = @BillingAddressZip,
		EFTAccountNumber = @EFTAccountNumber,
		EFTAccountTypeCD = @EFTAccountTypeCD,
		EFTContractSignedFlag = @EFTContractSignedFlag,
		EFTEffectiveDate = @EFTEffectiveDate,
		EFTRoutingNumber = @EFTRoutingNumber,
		FaxAreaCode = @BillingFaxAreaCode,
		FaxExchangeNumber = @BillingFaxExchangeNumber,
		FaxUnitNumber = @BillingFaxUnitNumber,
		Name = @BillingName,
		PhoneAreaCode = @BillingPhoneAreaCode,
		PhoneExchangeNumber = @BillingPhoneExchangeNumber,
		PhoneUnitNumber = @BillingPhoneUnitNumber,
		PhoneExtensionNumber = @BillingPhoneExtensionNumber,
		SysLastUserID = @UserID,
		SysLastUpdatedDate = @now
	WHERE BillingID = @BillingID

	IF @@ERROR <> 0
	BEGIN
		-- Insertion Failure 

		ROLLBACK TRANSACTION 
		RAISERROR('%s: Error updating utb_billing.', 16, 1, @ProcName)
		RETURN
	END
    
    IF @ChildLienHolderID IS NOT NULL
    BEGIN
      UPDATE dbo.utb_lien_holder
      SET ParentLienHolderID = @LienHolderID
      WHERE LienHolderID = @ChildLienHolderID

       IF @@ERROR <> 0
       BEGIN
           -- Insertion Failure 

           ROLLBACK TRANSACTION 
           RAISERROR('%s: Error updating utb_lien_holder.', 16, 1, @ProcName)
           RETURN
       END
    END
    
    IF @LienHolderID_Old IS NULL
    BEGIN
		-- Lien Holder was added. Trigger tasks.
		  EXEC uspWorkflowNotifyEvent @EventID = @EventIDTLLienHolderCreated,
									  @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
									  @Description = 'Lien Holder added',
									  @UserID = @UserID,
									  @ConditionValue = 'TL'
		  -- Check error value

		  IF @@ERROR <> 0
		  BEGIN
			  -- Error executing procedure

			  ROLLBACK TRANSACTION
			  RAISERROR('%s: Error notifying APD of "Total Loss Lien Holder Created" event', 16, 1, @ProcName)
			  RETURN
		  END 

          IF EXISTS (SELECT b.BillingID
                      FROM utb_lien_holder lh
                      LEFT JOIN utb_billing b ON lh.BillingID = b.BillingID
                      WHERE lh.LienHolderID = @LienHolderID
                        AND (b.EFTContractSignedFlag = 0
                          OR (lh.FedTaxId is null OR LEN(RTRIM(LTRIM(lh.FedTaxId))) = 0)
                            )
                     )
          BEGIN
             -- Fire off events that are Lien Holder specific
             EXEC uspWorkflowNotifyEvent @EventID = @EventIDTLLienHolderMissingBilling,
                                         @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                                         @Description = 'Lien Holder setup required',
                                         @UserID = @UserID,
                                         @ConditionValue = 'TL'
             -- Check error value

             IF @@ERROR <> 0
             BEGIN
                 -- Error executing procedure

                 ROLLBACK TRANSACTION
                 RAISERROR('%s: Error notifying APD of "Total Loss Lien Holder set up required" event', 16, 1, @ProcName)
                 RETURN
             END 
          END
    END
        
    COMMIT TRANSACTION 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLLienHolderUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLLienHolderUpdDetail TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
