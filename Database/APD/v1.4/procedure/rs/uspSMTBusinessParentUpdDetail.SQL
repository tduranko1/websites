-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTBusinessParentUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTBusinessParentUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTBusinessParentUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Update a Shop Parent
*
* PARAMETERS:  
* (I) @BusinessParentID                   The Shop Parent's unique identity
* (I) @Address1                           The Shop Parent's address line 1
* (I) @Address2                           The Shop Parent's address line 2
* (I) @AddressCity                        The Shop Parent's address city
* (I) @AddressCounty                      The Shop Parent's address county
* (I) @AddressState                       The Shop Parent's address state
* (I) @AddressZip                         The Shop Parent's address zip code
* (I) @FaxAreaCode                        The Shop Parent's fax area code
* (I) @FaxExchangeNumber                  The Shop Parent's fax exchange number
* (I) @FaxExtensionNumber                 The Shop Parent's fax extension number
* (I) @FaxUnitNumber                      The Shop Parent's fax unit number
* (I) @Name                               The Shop Parent's name
* (I) @PhoneAreaCode                      The Shop Parent's phone area code
* (I) @PhoneExchangeNumber                The Shop Parent's phone exchange number
* (I) @PhoneExtensionNumber               The Shop Parent's phone extension number
* (I) @PhoneUnitNumber                    The Shop Parent's phone unit number
* (I) @SysLastUserID                      The Shop Parent's updating user identity
* (I) @SysLastUpdatedDate                 The Shop Parent's last update date as string (VARCHAR(30))
*
* RESULT SET:
* BusinessParentID                        The updated Shop Parent unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTBusinessParentUpdDetail
(
	@BusinessParentID                 	  udt_std_int_big,
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @Name                               udt_std_name,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @SysLastUserID                      udt_std_id,
    @SysLastUpdatedDate                 varchar(30),
    @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error                      udt_std_int
    DECLARE @rowcount                   udt_std_int

    DECLARE @tupdated_date              udt_std_datetime 
    DECLARE @temp_updated_date          udt_std_datetime 

    DECLARE @ProcName                   varchar(30)       -- Used for raise error stmts 

    DECLARE @LogComment                 udt_std_desc_long


    DECLARE @dbName                     udt_std_name,
            @dbAddress1                 udt_addr_line_1,
            @dbAddress2                 udt_addr_line_2,
            @dbAddressZip               udt_addr_zip_code,
            @dbAddressCity              udt_addr_city,
            @dbAddressState             udt_addr_state,
            @dbAddressCounty            udt_addr_county,
            @dbPhoneAreaCode            udt_ph_area_code,
            @dbPhoneExchangeNumber      udt_ph_exchange_number,
            @dbPhoneUnitNumber          udt_ph_unit_number,
            @dbPhoneExtensionNumber     udt_ph_extension_number,
            @dbFaxAreaCode              udt_ph_area_code,
            @dbFaxExchangeNumber        udt_ph_exchange_number,
            @dbFaxUnitNumber            udt_ph_unit_number,
            @dbFaxExtensionNumber       udt_ph_extension_number



    SET @ProcName = 'uspSMTBusinessParentUpdDetail'


    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0 SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL


    -- Apply edits
    
    IF LEN(@Name) < 1
    BEGIN
       -- Missing Shop Parent Name
    
        RAISERROR  ('1|Missing Shop Parent Name', 16, 1, @ProcName)
        RETURN
    END

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop Parent Address State
        
            RAISERROR  ('1|Invalid Shop Parent Address State', 16, 1, @ProcName)
            RETURN
        END
    END

    IF @AddressZip IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT Zip FROM utb_zip_code WHERE Zip = @AddressZip)
        BEGIN
           -- Invalid Shop Parent Address Zip Code
        
            RAISERROR  ('1|Invalid Shop Parent Address Zip Code', 16, 1, @ProcName)
            RETURN
        END
    END


    -- Validate the updated date parameter
    
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_shop_parent', @BusinessParentID

    if @@error <> 0
    BEGIN
        RETURN
    END
    
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('101|%s|@SysLastUserID|NULL', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @dbName = Name,
            @dbAddress1 = Address1,
            @dbAddress2 = Address2,
            @dbAddressZip = AddressZip,
            @dbAddressCity = AddressCity,
            @dbAddressState = AddressState,
            @dbAddressCounty = AddressCounty,
            @dbPhoneAreaCode = PhoneAreaCode,
            @dbPhoneExchangeNumber = PhoneExchangeNumber,
            @dbPhoneUnitNumber = PhoneUnitNumber,
            @dbPhoneExtensionNumber = PhoneExtensionNumber,
            @dbFaxAreaCode = FaxAreaCode,
            @dbFaxExchangeNumber = FaxExchangeNumber,
            @dbFaxUnitNumber = FaxUnitNumber,
            @dbFaxExtensionNumber = FaxExtensionNumber
    FROM dbo.utb_shop_parent
    WHERE ShopParentID = @BusinessParentID
    

    IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspSMTBusinessParentUpdDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
      

    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopParentUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    UPDATE  dbo.utb_shop_parent SET
            Address1 = @Address1,
            Address2 = @Address2,
            AddressCity = @AddressCity,
            AddressCounty = @AddressCounty,
            AddressState = @AddressState,
            AddressZip = @AddressZip,
            FaxAreaCode = @FaxAreaCode,
            FaxExchangeNumber = @FaxExchangeNumber,
            FaxExtensionNumber = @FaxExtensionNumber,
            FaxUnitNumber = @FaxUnitNumber,
            Name = @Name,
            PhoneAreaCode = @PhoneAreaCode,
            PhoneExchangeNumber = @PhoneExchangeNumber,
            PhoneExtensionNumber = @PhoneExtensionNumber,
            PhoneUnitNumber = @PhoneUnitNumber,
            SysLastUserID = @SysLastUserID,
            SysLastUpdatedDate = CURRENT_TIMESTAMP
      WHERE ShopParentID = @BusinessParentID
        AND SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('104|%s|utb_shop_parent', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    -- Begin Audit Log code  -------------------------------------------------------------------------------------
    
       
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbName, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Name, ''))))
    BEGIN
      SET @LogComment = 'Parent Name changed from ' + @dbName + ' to ' + @Name
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddress1, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Address1, ''))))
    BEGIN
      SET @LogComment = 'Parent Address 1 was changed from ' + 
                        ISNULL(@dbAddress1, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@Address1, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddress2, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Address2, ''))))
    BEGIN
      SET @LogComment = 'Parent Address 2 was changed from ' + 
                        ISNULL(@dbAddress2, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@Address2, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressZip, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressZip, ''))))
    BEGIN
      SET @LogComment = 'Parent Zip Code was changed from ' + 
                        ISNULL(convert(varchar(12), @dbAddressZip), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @AddressZip), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressCity, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressCity, ''))))
    BEGIN
      SET @LogComment = 'Parent City was changed from ' + 
                        ISNULL(@dbAddressCity, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@AddressCity, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressState, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressState, ''))))
    BEGIN
      SET @LogComment = 'Parent State was changed from ' + 
                        ISNULL(convert(varchar(12), @dbAddressState), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @AddressState), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressCounty, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressCounty, ''))))
    BEGIN
      SET @LogComment = 'Parent County was changed from ' + 
                        ISNULL(@dbAddressCounty, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@AddressCounty, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Parent Phone Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Parent Phone Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
      
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Parent Phone Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Parent Phone Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Parent Fax Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Parent Fax Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Parent Fax Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Parent Fax Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @BusinessParentID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    -- End of Audit Log code  ------------------------------------------------------------------------------------

    COMMIT TRANSACTION AdmShopParentUpdDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @BusinessParentID AS BusinessParentID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTBusinessParentUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTBusinessParentUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/