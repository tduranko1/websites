-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestProcessedIns' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestProcessedIns 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestProcessedIns
* SYSTEM:       Lynx Services Hyperquest Partner
* AUTHOR:       Thomas Duranko
* FUNCTION:     Inserts processed file details that control if document was sent.
* Date:			2014Nov04
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	27Jul2015 - TVD - Updated to test if entry already exists before inserting new row.
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestProcessedIns]
	@vJobID VARCHAR(255)
	, @vAsyncGUID VARCHAR(255)
	, @vPartnerGUID VARCHAR(255)
	, @vDocumentGUID VARCHAR(255)
	, @iLynxID BIGINT
	, @iDocumentID BIGINT
	, @iDocumentTypeID INT
	, @dtJobProcessedDate DATETIME
	--, @iErrorID INT OUTPUT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @ProcName AS varchar(30)       -- Used for raise error stmts
    SET @ProcName = 'uspHyperquestProcessedIns'

    --DECLARE @error AS int
    --DECLARE @rowcount AS int
	DECLARE @vErrorMessage VARCHAR(100)
    SET @vErrorMessage = ''

    DECLARE @now AS datetime 
	SET @now = CURRENT_TIMESTAMP
	
	DECLARE @iErrorID INT
	SET @iErrorID = 0

    -- Set Database options
    
    SET NOCOUNT ON

	--------------------------------
	-- Check if row already exists
	--------------------------------
	IF NOT EXISTS (
		SELECT 
			JobID 
		FROM 
			utb_hyperquest_processed 
		WHERE  
			JobID = @vJobID
			AND DocumentGUID = @vDocumentGUID
			AND LynxID = @iLynxID
			AND DocumentID = @iDocumentID
			AND DocumentTypeID = @iDocumentTypeID
	)
	BEGIN
		BEGIN TRANSACTION ProcessedInsTran1
			IF @@ERROR <> 0
			BEGIN
			   -- SQL Server Error
				SET @vErrorMessage = @ProcName + ' Transaction ProcessedInsTran1 Failed'
				RAISERROR('SQL-ERROR-100|%s|%u', 16, 1, @vErrorMessage, @@ERROR)
				RETURN
			END
			
			INSERT INTO
				utb_hyperquest_processed 
			(
				JobID
				, AsyncGUID
				, PartnerGUID
				, DocumentGUID
				, LynxID
				, DocumentID
				, DocumentTypeID
				, JobProcessedDate
				, SysLastUpdatedDate
			)		
			VALUES
			(
				@vJobID
				, @vAsyncGUID
				, @vPartnerGUID
				, @vDocumentGUID
				, @iLynxID
				, @iDocumentID
				, @iDocumentTypeID
				, @dtJobProcessedDate
				, @now
			)

			IF @@ERROR <> 0
			BEGIN
				-- Insertion failure
				SET @vErrorMessage = @ProcName + ' Row insert failed and transaction rolled back'
				RAISERROR('SQL-ERROR-101|%s|%u', 16, 1, @vErrorMessage, @@ERROR)
				ROLLBACK TRANSACTION
				SET @iErrorID = 101 
			END

			COMMIT TRANSACTION ProcessedInsTran1
			IF @@ERROR <> 0
			BEGIN
			   -- SQL Server Error
				SET @vErrorMessage = @ProcName + ' Transaction EventLogInsTran1 COMMIT Failed'
				RAISERROR('SQL-ERROR-102|%s|%u', 16, 1, @vErrorMessage, @@ERROR)
				SET @iErrorID = 102 
			END
	END
	ELSE
	BEGIN
		SET @iErrorID = 0
	END

	SELECT CONVERT(VARCHAR(5), @iErrorID)
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestProcessedIns' AND type = 'P')
BEGIN
--    GRANT EXECUTE ON dbo.uspHyperquestProcessedIns TO 
--        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspHyperquestProcessedIns TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/