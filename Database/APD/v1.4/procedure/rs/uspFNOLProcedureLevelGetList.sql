-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLProcedureLevelGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspFNOLProcedureLevelGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspFNOLProcedureLevelGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       John Carpenter
* FUNCTION:     Retrieve all Procedure Levels
*
* PARAMETERS:
* None.
*
* RESULT SET:
* ProcedureLevelID
* Name
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspFNOLProcedureLevelGetList
AS
BEGIN
     -- SET NOCOUNT to ON and no longer display the count message

    SET NOCOUNT ON


    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts

    SET @ProcName = 'uspFNOLProcedureLevelGetList'

    SELECT ProcedureLevelID, Name
    FROM utb_fnol_procedure_level
    ORDER BY ProcedureLevelID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value

    IF @error <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLProcedureLevelGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspFNOLProcedureLevelGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/