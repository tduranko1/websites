-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhRevenueData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDtwhRevenueData 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspDtwhRevenueData
* SYSTEM:       Lynx Services FNOL
* AUTHOR:       Jon Leatherwood
* FUNCTION:     Extract Revenue Data for APD for data warehouse.
*
* PARAMETERS:  
* None.
*
* RESULT SET:
* None.
*
* 19-JUL-2012 MAH - Added case statement logic for RRP.
* 17-Apr-2014 TVD - Added LynxSelect stuff
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspDtwhRevenueData	
	@ExtractDate	udt_std_datetime = null
AS
BEGIN
    
    -- SET NOCOUNT to ON and no longer display the count message
	SET NOCOUNT ON
	declare @debug bit
	declare @now datetime
	declare @startdate udt_std_datetime
	declare @enddate udt_std_datetime
	
	set @now = current_timestamp
	
	if @ExtractDate is null
	begin
		set @enddate = convert(datetime, convert(varchar(13),dateadd(d,-1,@now),101) + ' 23:59:59')
	end 
	else
	begin
		set @enddate = convert(datetime, convert(varchar(13),@ExtractDate,101) + ' 23:59:59')
	end
	
	set @startdate = convert(datetime, convert(varchar(2),month(@enddate)) + '/1/' + convert(varchar(4),year(@enddate)))
	
	select convert(varchar(12),i.entrydate,101) REVENUE_DATE,
		   ins.InsuranceCompanyID INSURANCE_COMPANY_ID,
		   ins.Name INSURANCE_COMPANY_NAME,
		   case when i.Description like 'Program%' then 'Program Shops' 
				when i.Description like 'Desk%' then 'Desk Audits'
				when i.Description like 'Supp%' then 'Desk Audits'
				when i.Description like '%Reinspection%' then 'Reinspections'
				when i.Description like 'Adverse Subro%' then 'Adverse Subro'	        
				when i.Description like '%Mobile%' then 'Mobile Electronics'
				when i.Description like 'RRP%' then 'Repair Referral Program'
				when i.Description like 'LYNXSelect Assignment%' then 'Program Shops'
				else 'Other' end REVENUE_GROUP,
		   REPLACE(i.Description,'�','-')REVENUE_ITEM,	   
		   i.Amount REVENUE_AMOUNT       
	from dbo.utb_invoice i WITH (NOLOCK) inner join dbo.utb_claim_aspect ca WITH (NOLOCK) on (i.ClaimAspectID = ca.ClaimAspectID)
					   inner join dbo.utb_claim c WITH (NOLOCK) on (ca.LynxID = c.LynxID)
					   inner join dbo.utb_insurance ins WITH (NOLOCK) on (ins.InsuranceCompanyID = c.InsuranceCompanyID)
	where i.itemtypecd = 'F' 
	  and i.entrydate between @startdate and @enddate
	  and i.EnabledFlag = 1 -- Added 3/25/2009 to filter out voided items.
	  and c.DemoFlag <> 1
	  and ins.DemoFlag <> 1	  
	order by convert(varchar(12),i.entrydate,101) 	
			
     
    RETURN @@ROWCOUNT
END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhRevenueData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDtwhRevenueData TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
