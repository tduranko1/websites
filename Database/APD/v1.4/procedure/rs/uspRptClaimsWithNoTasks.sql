-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptClaimsWithNoTasks' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptClaimsWithNoTasks
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptClaimsWithNoTasks
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
* Updates:  05Feb2014 - TVD - Added EnabledFlag to the WHERE clause.
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptClaimsWithNoTasks
AS
BEGIN
    -- Set Database Options

    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

    declare @ntClaims table(LynxID  bigint, ClaimAspectID  bigint, TaskCount tinyint)
    

    --Claims with no tasks defined at all
    insert into @ntClaims
    select      caz.LynxID, caz.ClaimAspectID, 0 as 'TaskCount'
    
    from        utb_Claim_Aspect caz
    
    inner join  
    
                (
                select             distinct ca1.ClaimAspectID
                
                
                From               utb_Claim_Aspect ca1
                inner JOIN         utb_claim_aspect_service_channel casc1
                ON                 casc1.ClaimAspectID = ca1.ClaimAspectID
                
                inner join         utb_Claim_Aspect_Status cas1
                on                 cas1.ClaimAspectid = ca1.ClaimAspectID
                
                LEFT OUTER JOIN    utb_checklist ckl
                ON                 casc1.ClaimAspectServiceChannelID = ckl.ClaimAspectServiceChannelID
                
                
                
                where              cas1.ServiceChannelCD is null -- for vehicles
                and                cas1.StatusTypeCD is null -- for vehicles
                and                cas1.StatusID = 100 -- open only 
                and                ckl.ClaimAspectServiceChannelID is NULL  --There is no corresponding row in utb_Checklist table but the claim is still open
				AND				   ca1.EnabledFlag=1 -- 05Feb2014 TVD - Only real vehicles
                ) nt
    on          caz.ClaimAspectID = nt.ClaimAspectID
    
    left outer join
               (
                select             distinct ca2.ClaimAspectID
               
                From               utb_Claim_Aspect ca2
                inner JOIN         utb_claim_aspect_service_channel casc2
                ON                 casc2.ClaimAspectID = ca2.ClaimAspectID
                
                inner join         utb_Claim_Aspect_Status cas2
                on                 cas2.ClaimAspectid = ca2.ClaimAspectID
                
                inner JOIN    utb_checklist ck2
                ON                 casc2.ClaimAspectServiceChannelID = ck2.ClaimAspectServiceChannelID
                
                
                
                where              cas2.ServiceChannelCD is null -- for vehicles
                and                cas2.StatusTypeCD is null -- for vehicles
                and                cas2.StatusID = 100 -- open only 
                and                ck2.ClaimAspectServiceChannelID is not NULL  --There is no corresponding row in utb_Checklist table but the claim is still open
				AND				   ca2.EnabledFlag=1 -- 05Feb2014 TVD - Only real vehicles
                )t
    on        nt.ClaimAspectID = t.ClaimAspectID
    
    
    
    where     t.ClaimAspectID is null


    -- Final selects

    SELECT Convert(varchar, CURRENT_TIMESTAMP, 100) AS 'Run Date'

    SELECT  ca.LynxID AS LynxID,
            ca.ClaimAspectNumber AS Vehicle,
            i.Name AS 'Insurance Company',
            casc.ServiceChannelCD AS 'Primary Service Channel',
            CASE
              WHEN casc.ServiceChannelCD IN ('DA', 'DR') THEN au.NameLast + ', ' + au.NameFirst
              ELSE ou.NameLast + ', ' + ou.NameFirst
            END AS 'Claim Rep',
            --Count(c.ChecklistID) AS TaskCount
            z.TaskCount

      FROM dbo.utb_claim_aspect ca
      LEFT JOIN dbo.utb_user ou
        ON (ca.OwnerUserID = ou.UserID)
      LEFT JOIN dbo.utb_user au
        ON (ca.AnalystUserID = au.UserID)
      LEFT JOIN dbo.utb_claim_aspect_service_channel casc
        ON (casc.ClaimAspectID = ca.ClaimAspectID)
      LEFT JOIN dbo.utb_claim_aspect_status cas
        ON (ca.ClaimAspectID = cas.ClaimAspectID)
      /*
      LEFT JOIN dbo.utb_checklist c
        ON (casc.ClaimAspectServiceChannelID = c.ClaimAspectServiceChannelID)
      */
      inner join @ntClaims z
      on     z.LynxID = ca.LynxID
      and    z.ClaimAspectID = ca.ClaimAspectID

      LEFT JOIN dbo.utb_claim cl
        ON (ca.LynxID = cl.LynxID)
      LEFT JOIN dbo.utb_insurance i
        ON (cl.InsuranceCompanyID = i.InsuranceCompanyID)

      WHERE ca.ClaimAspectTypeID = 9    -- Vehicles only
        AND cas.StatusID = 100           -- Open only
        AND cl.DemoFlag = 0
        and casc.PrimaryFlag=1
		AND ca.EnabledFlag=1 -- 05Feb2014 TVD - Only real vehicles

        /*
      GROUP BY ca.LynxID, ca.ClaimAspectNumber, i.Name, casc.ServiceChannelCD,
        CASE
          WHEN casc.ServiceChannelCD IN ('DA', 'DR') THEN au.NameLast + ', ' + au.NameFirst
          ELSE ou.NameLast + ', ' + ou.NameFirst
        END
      HAVING Count(c.ChecklistID) = 0
        */
      ORDER BY ca.LynxID, ca.ClaimAspectNumber, i.Name, casc.ServiceChannelCD,
        CASE
          WHEN casc.ServiceChannelCD IN ('DA', 'DR') THEN au.NameLast + ', ' + au.NameFirst
          ELSE ou.NameLast + ', ' + ou.NameFirst
        END

    IF @@ERROR <> 0
    BEGIN

        RAISERROR('SQL Error', 16, 1)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspRptClaimsWithNoTasks' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptClaimsWithNoTasks TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/