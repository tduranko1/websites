-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowClaimSearch' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowClaimSearch 
END

GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowClaimSearch
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jeffery A. Lund
* FUNCTION:     Returns claims matching the entered search criteria
*
* PARAMETERS:  
*   (I) @ClaimNumber        The insurance company's claim number
*   (I) @InsuredHomePhone   Involved home phone
*   (I) @InvolvedWorkPhone   Involved work phone
*   (I) @LicensePlateNumber License Plate number of vehicle
*   (I) @LicensePlateState  License Plate state of vehicle
*   (I) @LynxId             Internal Lynx ID
*   (I) @PolicyNumber       Involved's policy number
*   (I) @ShopAreaCode       Shop's Area Code
*   (I) @ShopCity           Shop's City
*   (I) @ShopExchange       Shop Exchange Number
*   (I) @ShopName           The shop's name
*   (I) @ShopUnitNumber     Shop's unit number
*   (I) @VIN                The vehicle's VIN
*
* RESULT SET:
* Results of claim search
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowClaimSearch
    @InvolvedType               udt_std_name = Null,
    @LynxId                     varchar(10) = Null,
    @LossDate                   udt_std_datetime = Null,
    @PolicyNumber               udt_cov_policy_number = Null,
    @ClaimNumber                udt_cov_claim_number = Null,
    @FirstName                  udt_per_name = Null,
    @LastName                   udt_per_name = Null,
    @Address                    udt_addr_line_1 = Null,
    @Address2                   udt_addr_line_2 = Null,
    @City                       udt_addr_city = Null,
    @State                      varchar(5) = Null,
    @Zip                        udt_addr_zip_code = Null,
    @InvolvedNightAreaCode      varchar(6) = Null,
    @InvolvedNightExchange      varchar(6) = Null,
    @InvolvedNIghtUnitNumber    varchar(10) = Null,
    @InvolvedNightExtension     varchar(6) = Null,
    @InvolvedDayAreaCode        varchar(6) = Null,
    @InvolvedDayExchange        varchar(6) = Null,
    @InvolvedDayUnitNumber      varchar(6) = Null,
    @InvolvedDayExtension       varchar(6) = Null,
    @VehicleYear                varchar(6) = Null,
    @VehicleMake                udt_auto_make = Null,
    @VehicleModel               udt_auto_model = Null,
    @VIN                        udt_auto_vin = Null,
    @LicensePlateNumber         udt_auto_plate_number = Null,
    @LicensePlateState          varchar(6) = Null,
    @ShopName                   udt_std_name = Null,
    @ShopAddress                udt_addr_line_1 = Null,
    @ShopAddress2               udt_addr_line_2 = Null,
    @ShopCity                   udt_addr_city = Null,
    @ShopState                  udt_addr_state = Null,
    @ShopZip                    udt_addr_zip_code = Null,
    @ShopNumber                 varchar(10) = Null

AS
BEGIN
    -- Set Database options

    set CONCAT_NULL_YIELDS_NULL off
    set ansi_nulls on
    set ansi_warnings off


    -- Declare internal variables

    DECLARE @ProcName           varchar(30)
    DECLARE @debug              udt_std_flag
    
    SET @debug = 0

    SET @ProcName = 'uspWorkflowClaimSearch'


    IF @DEBUG = 1
    BEGIN
        Print ''
        Print 'Input Parameters:'
        Print '@InvolvedType = ' + convert(varchar, @InvolvedType)
        Print '@LynxId = ' + convert(varchar, @LynxId)
        Print '@LossDate = ' + convert(varchar, @LossDate)
        Print '@PolicyNumber = ' + convert(varchar, @PolicyNumber)
        Print '@ClaimNumber = ' + convert(varchar, @ClaimNumber)
        Print '@FirstName = ' + convert(varchar, @FirstName)
        Print '@LastName = ' + convert(varchar, @LastName)
        Print '@Address = ' + convert(varchar, @Address)
        Print '@Address2 = ' + convert(varchar, @Address2)
        Print '@City = ' + convert(varchar, @City)
        Print '@State = ' + convert(varchar, @State)
        Print '@Zip = ' + convert(varchar, @Zip)
        Print '@InvolvedNightAreaCode = ' + convert(varchar, @InvolvedNightAreaCode)
        Print '@InvolvedNightExchange = ' + convert(varchar, @InvolvedNightExchange)
        Print '@InvolvedNIghtUnitNumber = ' + convert(varchar, @InvolvedNIghtUnitNumber)
        Print '@InvolvedNightExtension = ' + convert(varchar, @InvolvedNightExtension)
        Print '@InvolvedDayAreaCode = ' + convert(varchar, @InvolvedDayAreaCode)
        Print '@InvolvedDayExchange = ' + convert(varchar, @InvolvedDayExchange)
        Print '@InvolvedDayUnitNumber = ' + convert(varchar, @InvolvedDayUnitNumber)
        Print '@InvolvedDayExtension = ' + convert(varchar, @InvolvedDayExtension)
        Print '@VehicleYear = ' + convert(varchar, @VehicleYear)
        Print '@VehicleMake = ' + convert(varchar, @VehicleMake)
        Print '@VehicleModel = ' + convert(varchar, @VehicleModel)
        Print '@VIN = ' + convert(varchar, @VIN)
        Print '@LicensePlateNumber = ' + convert(varchar, @LicensePlateNumber)
        Print '@LicensePlateState = ' + convert(varchar, @LicensePlateState)
        Print '@ShopName = ' + convert(varchar, @ShopName)
        Print '@ShopAddress = ' + convert(varchar, @ShopAddress)
        Print '@ShopAddress2 = ' + convert(varchar, @ShopAddress2)
        Print '@ShopCity = ' + convert(varchar, @ShopCity)
        Print '@ShopState = ' + convert(varchar, @ShopState)
        Print '@ShopZip = ' + convert(varchar, @ShopZip)
        Print '@ShopNumber = ' + convert(varchar, @ShopNumber)
    END
    
    --Use cmd vars to build search statements dynamically.
    DECLARE @Sql                varchar(4000),
            @Where              varchar(1000),
            @Order              varchar(25),
            @FirstArg           bit,
            @Max_Search_Records int

    --Order is fixed
    set @order = ' Order by C.LynxID desc'

    --Limit search to Max_Search_Records
    set @Max_Search_Records = (select Value from utb_app_variable where Name = 'Max_Search_Records')
    SET ROWCOUNT  @Max_Search_Records

    Create table #tmpSearchResults (
        LYNXID              INT             NULL,
        ClaimNumber         varchar(50)     NULL,
        Source              varchar(50)     NULL,
        EntityNumber        Int             NULL,
        Description         varchar(50)    NULL,
        InvolvedNameLast    varchar(50)     NULL,
        InvolvedNameFirst   varchar(50)     NULL,
        ContactNameLast     varchar(50)     NULL,
        ContactNameFirst    varchar(50)     NULL,
        LossDate            datetime        NULL)


    --Prepare parameters for search
    if Len(lTrim(rtrim(@LynxId))) = 0 SET @LynxId = Null
    if Len(lTrim(rtrim(@LossDate))) = 0 SET @LossDate = Null
    if Len(lTrim(rtrim(@PolicyNumber))) = 0 SET @PolicyNumber = Null
    if Len(lTrim(rtrim(@ClaimNumber))) = 0 SET @ClaimNumber = Null
    if Len(lTrim(rtrim(@FirstName))) = 0 SET @FirstName = Null
    if Len(lTrim(rtrim(@LastName))) = 0 SET @LastName = Null
    if Len(lTrim(rtrim(@Address))) = 0 SET @Address = Null
    if Len(lTrim(rtrim(@City))) = 0 SET @City = Null
    if Len(lTrim(rtrim(@State))) = 0 SET @State = Null
    if Len(lTrim(rtrim(@Zip))) = 0 SET @Zip = Null
    if Len(lTrim(rtrim(@InvolvedNightAreaCode))) = 0 SET @InvolvedNightAreaCode = Null
    if Len(lTrim(rtrim(@InvolvedNightExchange))) = 0 SET @InvolvedNightExchange = Null
    if Len(lTrim(rtrim(@InvolvedNIghtUnitNumber))) = 0 SET @InvolvedNIghtUnitNumber = Null
    if Len(lTrim(rtrim(@InvolvedNightExtension))) = 0 SET @InvolvedNightExtension = Null
    if Len(lTrim(rtrim(@InvolvedDayAreaCode))) = 0 SET @InvolvedDayAreaCode = Null
    if Len(lTrim(rtrim(@InvolvedDayExchange))) = 0 SET @InvolvedDayExchange = Null
    if Len(lTrim(rtrim(@InvolvedDayUnitNumber))) = 0 SET @InvolvedDayUnitNumber = Null
    if Len(lTrim(rtrim(@InvolvedDayExtension))) = 0 SET @InvolvedDayExtension = Null
    if Len(lTrim(rtrim(@VehicleYear))) = 0 SET @VehicleYear = Null
    if Len(lTrim(rtrim(@VehicleMake))) = 0 SET @VehicleMake = Null
    if Len(lTrim(rtrim(@VehicleModel))) = 0 SET @VehicleModel = Null
    if Len(lTrim(rtrim(@VIN))) = 0 SET @VIN = Null
    if Len(lTrim(rtrim(@LicensePlateNumber))) = 0 SET @LicensePlateNumber = Null
    if Len(lTrim(rtrim(@LicensePlateState))) = 0 SET @LicensePlateState = Null
    if Len(lTrim(rtrim(@ShopName))) = 0 SET @ShopName = Null
    if Len(lTrim(rtrim(@ShopAddress))) = 0 SET @ShopAddress = Null
    if Len(lTrim(rtrim(@ShopCity))) = 0 SET @ShopCity = Null
    if Len(lTrim(rtrim(@ShopState))) = 0 SET @ShopState = Null
    if Len(lTrim(rtrim(@ShopZip))) = 0 SET @ShopZip = Null
    if Len(lTrim(rtrim(@ShopNumber))) = 0 SET @ShopNumber = Null
    if Len(lTrim(rtrim(@Address2))) = 0 SET @Address2 = Null
    if Len(lTrim(rtrim(@ShopAddress2))) = 0 SET @ShopAddress2 = Null

/************************************************************************************
        -- Search Claim
*************************************************************************************/

        set @sql = 'INSERT INTO #tmpSearchResults SELECT c.LynxId,'
        set @sql = @sql + ' ISNULL(c.ClientClaimNumber,' + char(39) + char(39) + '),'
        set @sql = @sql + ' ' + char(39) + 'Claim' + char(39) + ','
        set @sql = @sql + ' ' + char(39) + char(39) + ','
        set @sql = @sql + ' ' + char(39) + char(39) + ','
        set @sql = @sql + ' ' + char(39) + char(39) + ','
        set @sql = @sql + ' ' + char(39) + char(39) + ','
        set @sql = @sql + ' ISNULL(CI.NameLast,' + char(39) + char(39) + ') AS ' + char(39) +  'ContactLastName' + char(39) + ','
        set @sql = @sql + ' ISNULL(CI.NameFirst,' + char(39) + char(39) + ') AS ' + char(39) +  'ContactFirstName' + char(39) + ','
        set @sql = @sql + ' c.LossDate'

        set @sql = @sql + ' FROM'
        set @sql = @sql + ' dbo.utb_claim c'

        set @sql = @sql + ' LEFT JOIN dbo.utb_Claim_Coverage CC'
            set @sql = @sql + ' ON C.LynxID = CC.LynxID'

        SET @sql = @sql + ' LEFT JOIN dbo.utb_Involved CI'
            set @sql = @sql + ' ON C.ContactInvolvedID = CI.InvolvedID'

        set @FirstArg = 0

        set @Where = ' WHERE'
        
        IF @LynxID is not null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            SET @Where = @Where + ' C.LynxID = ' + @LynxID
        end
        
        IF @LossDate is not null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' C.LossDate between ' + char(39)
                + convert(varchar(20), @LossDate) + char(39) + ' AND '
                + '' + char(39) + convert(varchar(20),dateadd(day, 1, @LossDate)) + char(39) 
        END
                        
        IF @PolicyNumber is not null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' C.PolicyNumber like '  + char(39) + '%' + @PolicyNumber + '%' + char(39)
        END
            
        IF @ClaimNumber is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' C.ClientClaimNumber like '  + char(39) + '%' + @ClaimNumber + '%' + char(39)
        END
            
        IF @FirstName is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.NameFirst  LIKE ' + char(39) + '%' + @FirstName + '%' + char(39)
        END
            
        IF @LastName is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.NameLast  LIKE ' + char(39) + '%' + @LastName + '%' + char(39)
        END
            
        IF @Address is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.Address1 like '  + char(39) + '%' + @Address + '%' + char(39)
        END
            
        IF @Address2 is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.Address2 like '  + char(39) + '%' + @Address2 + '%' + char(39)
        END
            
        IF @City is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.AddressCity like '  + char(39) + '%' + @City + '%' + char(39)
        END
            
        IF @State is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.AddressState like '  + char(39) + '%' + @State + '%' + char(39)
        END

        IF @Zip is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.AddressZip like '  + char(39) + '%' + @Zip + '%' + char(39)
        END

        IF @InvolvedNightAreaCode is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.NightAreaCode = ' + char(39) + @InvolvedNightAreaCode + char(39)
        END

        IF @InvolvedNightExchange is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
           set @Where = @Where + ' CI.NightExchangeNumber = ' + char(39) + @InvolvedNightExchange + char(39)
        END

        IF @InvolvedNightExchange is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.NightExchangeNumber = ' + char(39) + @InvolvedNightExchange + char(39)
        END

        IF @InvolvedNIghtUnitNumber is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.NightUnitNumber = ' + char(39) + @InvolvedNIghtUnitNumber + char(39)
        END

        IF @InvolvedNightExtension is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.NightExtensionNumber = ' + char(39) + @InvolvedNightExtension + char(39)
        END

        IF @InvolvedDayAreaCode is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.DayAreaCode = ' + char(39) + @InvolvedDayAreaCode + char(39)
        END

        IF @InvolvedDayExchange is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.DayExchangeNumber = ' + char(39) + @InvolvedDayExchange + char(39)
        END

        IF @InvolvedDayUnitNumber is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' CI.DayUnitNumber = ' + char(39) + @InvolvedDayUnitNumber + char(39)
        END

        IF @InvolvedDayExtension is not Null
        begin
            if @Where <> ' WHERE'
                set @Where = @Where + ' AND '
                
            set @Where = @Where + ' AND CI.DayExtensionNumber = ' + char(39) + @InvolvedDayExtension + char(39)
        END
        
    if @Where = ' WHERE' set @Where = ''

    --If they've entered any of these fields, they're not interestd in a claim or property.
    IF @VehicleYear is Null
        AND @VehicleMake is Null
        AND @VehicleModel is Null
        AND @VIN is Null
        AND @LicensePlateNumber is Null
        AND @LicensePlateState is Null
        AND @ShopName is Null
        AND @ShopAddress is Null
        AND @ShopAddress2 is Null
        AND @ShopCity is Null
        AND @ShopState is Null
        AND @ShopZip is Null
        AND @ShopNumber is Null
    BEGIN
        IF @Debug = 1
        BEGIN
            Print ''
            Print '1st execution ...'
            Print '@SQL: ' + @SQL
            Print '@Where: ' + @Where
            Print '@order: ' + @order
        END
        
        exec (@sql + @Where + @order)

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        IF @Debug = 1
        BEGIN
            Print ''
            SELECT * FROM #tmpSearchResults
        END
        
    END


/************************************************************************************
        -- Search Properties
*************************************************************************************/
    set @sql = ''
    set @sql = @sql + '  INSERT INTO #tmpSearchResults '
    set @sql = @sql + ' 	SELECT '
    set @sql = @sql + ' 	c.LynxId, '
    set @sql = @sql + '     ISNULL(c.ClientClaimNumber,' + char(39) + char(39) + '), '
    set @sql = @sql + '     ' + char(39) + 'Property' + char(39) + ', '
    set @sql = @sql + ' 	CA.ClaimAspectNumber, '
    set @sql = @sql + '     Isnull(cp.PropertyDescription, ' + char(39) + char(39) + '), '
    set @sql = @sql + '     ISNULL(i.NameLast,' + char(39) + char(39) + '), '
    set @sql = @sql + '     ISNULL(i.NameFirst,' + char(39) + char(39) + '), '
    set @sql = @sql + '     ISNULL(CI.NameLast,' + char(39) + char(39) + '), '
    set @sql = @sql + '     ISNULL(CI.NameFirst,' + char(39) + char(39) + '), '
    set @sql = @sql + ' 	c.LossDate '

    set @sql = @sql + ' FROM '
    set @sql = @sql + ' 	dbo.utb_claim_aspect CA '
    set @sql = @sql + ' 	LEFT JOIN dbo.utb_claim C on C.LynxID = CA.LynxID '
    set @sql = @sql + '     LEFT JOIN dbo.utb_Claim_Coverage CC ON C.LynxID = CC.LynxID	'
    set @sql = @sql + '     LEFT JOIN dbo.utb_claim_property CP ON CA.ClaimAspectID = CP.ClaimAspectID'
    set @sql = @sql + ' 	LEFT JOIN dbo.utb_Involved CI ON CP.ContactInvolvedID = CI.InvolvedID'
    set @sql = @sql + ' 	LEFT JOIN dbo.utb_Claim_aspect_involved CAI on CA.ClaimAspectID = CAI.ClaimAspectID'
    set @sql = @sql + ' 	LEFT JOIN dbo.utb_involved I ON CAI.InvolvedID = I.InvolvedID'
    
    set @Where = ' WHERE'
    set @Where = @Where + '     CA.ClaimAspectTypeID = '
    set @Where = @Where + ' 		(Select ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name = ' + char(39) + 'Property' + char(39) + ')'

        IF @LynxID is not Null
        BEGIN
            set @Where = @Where + ' AND C.LynxID = ' + @LynxID
        END

        IF @LossDate is not Null
        BEGIN
            set @Where = @Where + ' AND C.LossDate between ' + char(39)
                + convert(varchar(20), @LossDate) + char(39) + ' AND '
                + char(39) + convert(varchar(20),dateadd(day, 1, @LossDate)) + char(39)
        END

        IF @PolicyNumber is not Null
        BEGIN
            set @Where = @Where + ' AND C.PolicyNumber like '  + char(39) + '%' + @PolicyNumber + '%' + char(39)
        END

        IF @ClaimNumber is not Null
        BEGIN
            set @Where = @Where + ' AND C.ClientClaimNumber like '  + char(39) + '%' + @ClaimNumber + '%' + char(39)
        END

        IF @LastName is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NameLast  LIKE ' + char(39) + '%' + @LastName + '%' + char(39)
            set @Where = @Where + ' OR I.NameLast like '  + char(39) + '%' + @LastName + '%' + char(39) + ')'
        END

        IF @FirstName is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NameFirst  LIKE ' + char(39) + '%' + @FirstName + '%' + char(39)
            set @Where = @Where + ' OR I.NameFirst like '  + char(39) + '%' + @FirstName + '%' + char(39) + ')'
        END

        IF @Address is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.Address1 like '  + char(39) + '%' + @Address + '%' + char(39)
            set @Where = @Where + ' OR I.Address1 like '  + char(39) + '%' + @Address + '%' + char(39) + ')'
        END

        IF @Address2 is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.Address2 like '  + char(39) + '%' + @Address2 + '%' + char(39)
            set @Where = @Where + ' OR I.Address2 like '  + char(39) + '%' + @Address2 + '%' + char(39) + ')'
        END

        IF @City is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.AddressCity like '  + char(39) + '%' + @City + '%' + char(39)
            set @Where = @Where + ' OR I.AddressCity like '  + char(39) + '%' + @City + '%' + char(39) + ')'
        END

        IF @State is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.AddressState like '  + char(39) + '%' + @State + '%' + char(39)
            set @Where = @Where + ' OR I.AddressState like '  + char(39) + '%' + @State + '%' + char(39) + ')'
        END

        IF @Zip is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.AddressZip like '  + char(39) + '%' + @Zip + '%' + char(39)
            set @Where = @Where + ' OR I.AddressZip like '  + char(39) + '%' + @Zip + '%' + char(39) + ')'
        END

        IF @InvolvedNightAreaCode is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NightAreaCode = ' + char(39) + @InvolvedNightAreaCode + char(39)
            set @Where = @Where + ' OR I.NightAreaCode like '  + char(39) + '%' + @InvolvedNightAreaCode + '%' + char(39) + ')'
        END

        IF @InvolvedNightExchange is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NightExchangeNumber = ' + char(39) + @InvolvedNightExchange + char(39)
            set @Where = @Where + ' OR I.NightExchangeNumber like '  + char(39) + '%' + @InvolvedNightExchange + '%' + char(39) + ')'
        END

        IF @InvolvedNIghtUnitNumber is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NightUnitNumber = ' + char(39) + @InvolvedNIghtUnitNumber + char(39)
            set @Where = @Where + ' OR I.NightUnitNumber like '  + char(39) + '%' + @InvolvedNIghtUnitNumber + '%' + char(39) + ')'
        END

        IF @InvolvedNightExtension is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NightExtensionNumber = ' + char(39) + @InvolvedNightExtension + char(39)
            set @Where = @Where + ' OR I.NightExtensionNumber like '  + char(39) + '%' + @InvolvedNightExtension + '%' + char(39) + ')'
        END

        IF @InvolvedDayAreaCode is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.DayAreaCode = ' + char(39) + @InvolvedDayAreaCode + char(39)
            set @Where = @Where + ' OR I.DayAreaCode like '  + char(39) + '%' + @InvolvedDayAreaCode + '%' + char(39) + ')'
        END

        IF @InvolvedDayExchange is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.DayExchangeNumber = ' + char(39) + @InvolvedDayExchange + char(39)
            set @Where = @Where + ' OR I.DayExchangeNumber like '  + char(39) + '%' + @InvolvedDayExchange + '%' + char(39) + ')'
        END

        IF @InvolvedDayUnitNumber is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.DayUnitNumber = ' + char(39) + @InvolvedDayUnitNumber + char(39)
            set @Where = @Where + ' OR I.DayUnitNumber like '  + char(39) + '%' + @InvolvedDayUnitNumber + '%' + char(39) + ')'
        END

        IF @InvolvedDayExtension is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.DayExtensionNumber = ' + char(39) + @InvolvedDayExtension + char(39)
            set @Where = @Where + ' OR I.DayExtensionNumber like '  + char(39) + '%' + @InvolvedDayExtension + '%' + char(39) + ')'
        END

    --If they've entered any of these fields, they're not interestd in a claim or property.
    IF @VehicleYear is Null
        AND @VehicleMake is Null
        AND @VehicleModel is Null
        AND @VIN is Null
        AND @LicensePlateNumber is Null
        AND @LicensePlateState is Null
        AND @ShopName is Null
        AND @ShopAddress is Null
        AND @ShopAddress2 is Null
        AND @ShopCity is Null
        AND @ShopState is Null
        AND @ShopZip is Null
        AND @ShopNumber is Null
    BEGIN
        IF @Debug = 1
        BEGIN
            Print ''
            Print '2nd execution ...'
            Print '@SQL: ' + @SQL
            Print '@Where: ' + @Where
            Print '@order: ' + @order
        END
        
        exec (@sql + @Where + @order)

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
        
        IF @Debug = 1
        BEGIN
            Print ''
            SELECT * FROM #tmpSearchResults
        END
    END

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

/************************************************************************************
        -- Search Vehicles
*************************************************************************************/
    set @sql = ''
        set @sql = @sql + '  INSERT INTO '
        set @sql = @sql + ' 	#tmpSearchResults '

        set @sql = @sql + ' SELECT distinct ' 
        set @sql = @sql + ' 	CA.LynxId, '
        set @sql = @sql + ' 	ISNULL(c.ClientClaimNumber,' + char(39) + char(39) + '), '
        set @sql = @sql + ' 	' + char(39) + 'Vehicle' + char(39) + ', '
        set @sql = @sql + ' 	ISNULL(CA.ClaimAspectNumber, ' + char(39) + char(39) + '), convert(varchar(50), cv.VehicleYear) + ' + char(39) + ' '  + char(39) + '  + convert(varchar(50),cv.Make) + ' + char(39) + ' '  + char(39) + ' + convert(varchar(50), cv.Model), '
        set @sql = @sql + ' 	ISNULL(i.NameLast,' + char(39) + char(39) + '), '
        set @sql = @sql + ' 	ISNULL(i.NameFirst,' + char(39) + char(39) + '), '
        set @sql = @sql + ' 	ISNULL(CI.NameLast,' + char(39) + char(39) + '), '
        set @sql = @sql + ' 	ISNULL(CI.NameFirst,' + char(39) + char(39) + '), '
        set @sql = @sql + ' 	c.LossDate '

        set @sql = @sql + ' FROM '
        set @sql = @sql + ' 	dbo.utb_claim_aspect CA'
        set @sql = @sql + ' 	LEFT JOIN dbo.utb_claim C ON CA.LynxID = C.LynxID'
        set @sql = @sql + ' 	LEFT JOIN dbo.utb_claim_vehicle CV ON CA.ClaimAspectID = CV.ClaimAspectID'
        set @sql = @sql + ' 	LEFT JOIN dbo.utb_Involved CI ON CV.ContactInvolvedID = CI.InvolvedID'
        set @sql = @sql + ' 	LEFT JOIN dbo.utb_claim_aspect_involved CAI ON CA.ClaimAspectID = CAI.ClaimAspectID'
        set @sql = @sql + ' 	LEFT JOIN dbo.utb_involved I on CAI.InvolvedID = I.InvolvedID'
        set @sql = @sql + ' 	LEFT JOIN dbo.utb_Claim_Coverage CC ON C.LynxID = CC.LynxID '
        set @sql = @sql + ' 	LEFT JOIN dbo.utb_Claim_Aspect_Service_Channel casc on CA.ClaimAspectID = casc.ClaimAspectID'
        set @sql = @sql + ' 	LEFT JOIN dbo.utb_Assignment VA on casc.ClaimAspectServiceChannelID = VA.ClaimAspectServiceChannelID'
        set @sql = @sql + ' 	LEFT JOIN dbo.utb_Shop_Location SL ON VA.ShopLocationID = SL.ShopLocationID '

        set @Where = ' WHERE     '
        set @Where = @Where + ' 	CA.ClaimAspectTypeID = (Select ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name = ' + char(39) + 'Vehicle'+ char(39) + ')' 
        --Where is used for the property select.  No vehicles in that, so we have all of the parameters
        --tha apply.


        IF @LynxID is not Null
        BEGIN
            set @Where = @Where + ' AND C.LynxID = ' + @LynxID
        END

        IF @LossDate is not Null
        BEGIN
            set @Where = @Where + ' AND C.LossDate between ' + char(39)
                + convert(varchar(20), @LossDate) + char(39) + ' AND '
                + char(39) + convert(varchar(20),dateadd(day, 1, @LossDate)) + char(39)
        END

        IF @PolicyNumber is not Null
        BEGIN
            set @Where = @Where + ' AND C.PolicyNumber like '  + char(39) + '%' + @PolicyNumber + '%' + char(39)
        END

        IF @ClaimNumber is not Null
        BEGIN
            set @Where = @Where + ' AND C.ClientClaimNumber like '  + char(39) + '%' + @ClaimNumber + '%' + char(39)
        END

        IF @LastName is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NameLast  LIKE ' + char(39) + '%' + @LastName + '%' + char(39)
            set @Where = @Where + ' OR I.NameLast like '  + char(39) + '%' + @LastName + '%' + char(39) + ')'
        END

        IF @FirstName is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NameFirst  LIKE ' + char(39) + '%' + @FirstName + '%' + char(39)
            set @Where = @Where + ' OR I.NameFirst like '  + char(39) + '%' + @FirstName + '%' + char(39) + ')'
        END

        IF @Address is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.Address1 like '  + char(39) + '%' + @Address + '%' + char(39)
            set @Where = @Where + ' OR I.Address1 like '  + char(39) + '%' + @Address + '%' + char(39) + ')'
        END

        IF @Address2 is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.Address2 like '  + char(39) + '%' + @Address2 + '%' + char(39)
            set @Where = @Where + ' OR I.Address2 like '  + char(39) + '%' + @Address2 + '%' + char(39) + ')'
        END

        IF @City is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.AddressCity like '  + char(39) + '%' + @City + '%' + char(39)
            set @Where = @Where + ' OR I.AddressCity like '  + char(39) + '%' + @City + '%' + char(39) + ')'
        END

        IF @State is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.AddressState like '  + char(39) + '%' + @State + '%' + char(39)
            set @Where = @Where + ' OR I.AddressState like '  + char(39) + '%' + @State + '%' + char(39) + ')'
        END

        IF @Zip is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.AddressZip like '  + char(39) + '%' + @Zip + '%' + char(39)
            set @Where = @Where + ' OR I.AddressZip like '  + char(39) + '%' + @Zip + '%' + char(39) + ')'
        END

        IF @InvolvedNightAreaCode is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NightAreaCode = ' + char(39) + @InvolvedNightAreaCode + char(39)
            set @Where = @Where + ' OR I.NightAreaCode like '  + char(39) + '%' + @InvolvedNightAreaCode + '%' + char(39) + ')'
        END

        IF @InvolvedNightExchange is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NightExchangeNumber = ' + char(39) + @InvolvedNightExchange + char(39)
            set @Where = @Where + ' OR I.NightExchangeNumber like '  + char(39) + '%' + @InvolvedNightExchange + '%' + char(39) + ')'
        END

        IF @InvolvedNIghtUnitNumber is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NightUnitNumber = ' + char(39) + @InvolvedNIghtUnitNumber + char(39)
            set @Where = @Where + ' OR I.NightUnitNumber like '  + char(39) + '%' + @InvolvedNIghtUnitNumber + '%' + char(39) + ')'
        END

        IF @InvolvedNightExtension is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.NightExtensionNumber = ' + char(39) + @InvolvedNightExtension + char(39)
            set @Where = @Where + ' OR I.NightExtensionNumber like '  + char(39) + '%' + @InvolvedNightExtension + '%' + char(39) + ')'
        END

        IF @InvolvedDayAreaCode is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.DayAreaCode = ' + char(39) + @InvolvedDayAreaCode + char(39)
            set @Where = @Where + ' OR I.DayAreaCode like '  + char(39) + '%' + @InvolvedDayAreaCode + '%' + char(39) + ')'
        END

        IF @InvolvedDayExchange is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.DayExchangeNumber = ' + char(39) + @InvolvedDayExchange + char(39)
            set @Where = @Where + ' OR I.DayExchangeNumber like '  + char(39) + '%' + @InvolvedDayExchange + '%' + char(39) + ')'
        END

        IF @InvolvedDayUnitNumber is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.DayUnitNumber = ' + char(39) + @InvolvedDayUnitNumber + char(39)
            set @Where = @Where + ' OR I.DayUnitNumber like '  + char(39) + '%' + @InvolvedDayUnitNumber + '%' + char(39) + ')'
        END

        IF @InvolvedDayExtension is not Null
        BEGIN
            set @Where = @Where + ' AND (CI.DayExtensionNumber = ' + char(39) + @InvolvedDayExtension + char(39)
            set @Where = @Where + ' OR I.DayExtensionNumber like '  + char(39) + '%' + @InvolvedDayExtension + '%' + char(39) + ')'
        END

        IF @ShopName is not Null
        BEGIN
            set @Where = @Where + ' AND SL.Name like '  + char(39) + '%' + @ShopName + '%' + char(39)
        END

        IF @ShopAddress is not Null
        BEGIN
            set @Where = @Where + ' AND SL.Address1 like '  + char(39) + '%' + @ShopAddress + '%' + char(39)
        END

        IF @ShopAddress2 is not Null
        BEGIN
            set @Where = @Where + ' AND SL.Address2 like '  + char(39) + '%' + @ShopAddress2 + '%' + char(39)
        END

        IF @ShopCity is not Null
        BEGIN
            set @Where = @Where + ' AND SL.AddressCity like '  + char(39) + '%' + @ShopCity + '%' + char(39)
        END

        IF @ShopState is not Null
        BEGIN
            set @Where = @Where + ' AND SL.AddressState like '  + char(39) + '%' + @ShopState + '%' + char(39)
        END

        IF @ShopZip is not Null
        BEGIN
            set @Where = @Where + ' AND SL.AddressZip = ' + char(39) + @ShopZip + char(39)
        END

        IF @ShopNumber is not Null
        BEGIN
            set @Where = @Where + ' AND convert(Varchar(10), SL.ShopLocationID) = ' + char(39) + @ShopNumber + char(39)
        END

        IF @VehicleYear is not Null
        BEGIN
            set @Where = @Where + ' AND Convert(varchar(4), CV.VehicleYear) = ' + char(39) + @VehicleYear + char(39)
        END

        IF @VehicleMake is not Null
        BEGIN
            set @Where = @Where + ' AND CV.Make like '  + char(39) + '%' + @VehicleMake + '%' + char(39)
        END

        IF @VehicleModel is not Null
        BEGIN
            set @Where = @Where + ' AND CV.Model like '  + char(39) + '%' + @VehicleModel + '%' + char(39)
        END

        IF @Vin is not Null
        BEGIN
            set @Where = @Where + ' AND CV.VIN = ' + char(39) + @Vin + char(39)
        END

        IF @LicensePlateNumber is not Null
        BEGIN
            set @Where = @Where + ' AND CV.LicensePlateNumber like '  + char(39) + '%' + @LicensePlateNumber + '%' + char(39)
        END

        IF @LicensePlateState is not Null
        BEGIN
            set @Where = @Where + ' AND CV.LicensePlateState like '  + char(39) + '%' + @LicensePlateState + '%' + char(39)
        END

    IF @Debug = 1
    BEGIN
        Print ''
        Print '3rd execution ...'
        Print '@SQL: ' + @SQL
        Print '@Where: ' + @Where
        Print '@order: ' + @order
    END
    
    exec (@sql + @Where) -- + @order)

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    IF @Debug = 1
    BEGIN
        Print ''
        SELECT * FROM #tmpSearchResults
    END

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


--set rowcount 0
    SELECT  
        * 
    
    FROM 
        #tmpSearchResults 
    
    WHERE
        EntityNumber is not null

    ORDER BY LynxID desc, Source, EntityNumber
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowClaimSearch' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowClaimSearch TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/