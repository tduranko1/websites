-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'GetClientCoverages' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.GetClientCoverages 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    GetClientCoverages
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all client coverage information from the database
*
* PARAMETERS:  
*				InsuranceCompanyID 
*
* RESULT SET:
*   All data related to insurance client coverages
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.GetClientCoverages
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON
    
	SELECT 
		ClientCoverageTypeID
		, InsuranceCompanyID
		, ISNULL(AdditionalCoverageFlag,'') AdditionalCoverageFlag
		, ISNULL(ClientCode,'') ClientCode
		, ISNULL(CoverageProfileCD,'') CoverageProfileCD
		, ISNULL(DisplayOrder,'') DisplayOrder
		, ISNULL(EnabledFlag,'') EnabledFlag
		, ISNULL([Name],'') CoverageName
		, SysLastUserID
		, SysLastUpdatedDate
	FROM
		utb_client_coverage_type
	WHERE 
		EnabledFlag = 1	 
		AND InsuranceCompanyID = @iInsuranceCompanyID
	ORDER BY
		[Name] 
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'GetClientCoverages' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.GetClientCoverages TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.GetClientCoverages TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/