-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDealerBusinessInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTDealerBusinessInsDetail 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspSMTDealerBusinessInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Creates a new Shop Dealer relationship
*
* PARAMETERS:  
* (I) @ShopID                The Shop's unique identity
* (I) @DealerID              The Dealer's unique identity
* (I) @SysLastUserID         The Shop Dealer's updating user identity
*
* RESULT SET:
* ShopID                     The Shop unique identity
* DealerID                   The Dealer unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTDealerBusinessInsDetail
(
	@Updates                            udt_std_desc_long,
  @DealerID            	              udt_std_int_big,
	@SysLastUserID                      udt_std_id,
  @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error                    udt_std_int
    DECLARE @rowcount                 udt_std_int
    DECLARE @now                      udt_std_datetime
    DECLARE @KeyDescription           udt_std_name
    
    DECLARE @ProcName                 varchar(30)       -- Used for raise error stmts 
    
    DECLARE @tmpBusiness        TABLE (ID         bigint          NOT NULL,
                                       Name       varchar(50)     NOT NULL)   
                                           

    SET @ProcName = 'uspSMTDealerBusinessInsDetail'
    SET @now = CURRENT_TIMESTAMP
    
    -- Apply edits
    
    IF @DealerID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT DealerID FROM utb_dealer WHERE DealerID = @DealerID)
        BEGIN
           -- Invalid Dealer
            RAISERROR('101|%s|@DealerID|%u', 16, 1, @ProcName, @DealerID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Dealer
        RAISERROR('101|%s|@DealerID|%u', 16, 1, @ProcName, @DealerID)
        RETURN
    END

    IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
    BEGIN
        -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END
    

    INSERT INTO @tmpBusiness
    SELECT value,
           (SELECT Name FROM dbo.utb_shop WHERE ShopID = CONVERT(bigint, f.value))
    FROM dbo.ufnUtilityParseString(@Updates, ',', 1) f
    
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|@tmpBusiness', 16, 1, @ProcName)
        RETURN
    END
    
    -- Ensure no duplicates will be inserted.
    DELETE @tmpBusiness
    FROM @tmpBusiness t INNER JOIN dbo.utb_shop_dealer d ON t.ID = d.ShopID
    WHERE DealerID = @DealerID
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('106|%s|@tmpBusiness', 16, 1, @ProcName)
        RETURN
    END
    
    -- Ensure all ShopIDs passed in @Updates are valid.
    IF EXISTS (SELECT ID 
               FROM @tmpBusiness t LEFT JOIN dbo.utb_shop s ON t.ID = s.ShopID 
               WHERE s.ShopID IS NULL)
    BEGIN
      -- Invalid ShopID passed in updates string
        RAISERROR('101|%s|@Updates|%u', 16, 1, @ProcName, @Updates)
        RETURN
    END
    

    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopDealerInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_shop_dealer	(ShopID,
                                     DealerID,
                                     OwnerFlag,
                                     SysLastUserID,
                                     SysLastUpdatedDate)
                              SELECT ID,
                                     @DealerID,
                                     1,
                                     @SysLastUserID,
                                     @now
                              FROM @tmpBusiness
    
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_shop_dealer', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    -- Audit Log code  --------------------------------------------------------------------------------------
    
    SET @KeyDescription = (SELECT Name FROM dbo.utb_dealer WHERE DealerID = @DealerID)
    
    INSERT INTO dbo.utb_audit_log (AuditTypeCD,
                                   KeyDescription,
                                   KeyID,
                                   LogComment,
                                   SysLastUserID,
                                   SysLastUpdatedDate)
                            SELECT 'P',
                                   @KeyDescription,
                                   @DealerID,
                                   'Business added to Dealer: ' + CONVERT(varchar(12), ID) + '-' + Name,
                                   @SysLastUserID,
                                   @now
                            FROM @tmpBusiness
    

    IF @error <> 0
    BEGIN
       -- Insertion failure
    
        RAISERROR('105|%s|utb_audit_log', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    
    
    -- End of Audit Log code  -------------------------------------------------------------------------------


    COMMIT TRANSACTION AdmShopDealerInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @Updates AS Updates, @DealerID AS DealerID

    RETURN @rowcount

END


GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDealerBusinessInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTDealerBusinessInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO