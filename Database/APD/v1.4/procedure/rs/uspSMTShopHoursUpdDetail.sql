-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopHoursUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopHoursUpdDetail 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopHoursUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Update a Shop Location Operating Hours
*
* PARAMETERS:  
* (I) @ShopID                             The Shop Location's unique identity
* (I) @OperatingMondayStartTime           The Shop Location's Monday Open Time
* (I) @OperatingMondayEndTime             The Shop Location's Monday Close Time
* (I) @OperatingTuesdayStartTime          The Shop Location's Tuesday Open Time
* (I) @OperatingTuesdayEndTime            The Shop Location's Tuesday Close Time
* (I) @OperatingWednesdayStartTime        The Shop Location's Wednesday Open Time
* (I) @OperatingWednesdayEndTime          The Shop Location's Wednesday Close Time
* (I) @OperatingThursdayStartTime         The Shop Location's Thursday Open Time
* (I) @OperatingThursdayEndTime           The Shop Location's Thursday Close Time
* (I) @OperatingFridayStartTime           The Shop Location's Friday Open Time
* (I) @OperatingFridayEndTime             The Shop Location's Friday Close Time
* (I) @OperatingSaturdayStartTime         The Shop Location's Saturday Open Time
* (I) @OperatingSaturdayEndTime           The Shop Location's Saturday Close Time
* (I) @OperatingSundayStartTime           The Shop Location's Sunday Open Time
* (I) @OperatingSundayEndTime             The Shop Location's Sunday Close Time
* (I) @SysLastUserID                      The Shop Location's updating user identity
* (I) @SysLastUpdatedDate                 The Shop Location's Hours last update date as string (VARCHAR(30))
*
* RESULT SET:
* ShopLocationID                          The updated Shop Location unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopHoursUpdDetail
(
    @ShopID                             udt_std_id_big,
    @OperatingMondayStartTime           udt_std_time=NULL,
    @OperatingMondayEndTime             udt_std_time=NULL,
    @OperatingTuesdayStartTime          udt_std_time=NULL,
    @OperatingTuesdayEndTime            udt_std_time=NULL,
    @OperatingWednesdayStartTime        udt_std_time=NULL,
    @OperatingWednesdayEndTime          udt_std_time=NULL,
    @OperatingThursdayStartTime         udt_std_time=NULL,
    @OperatingThursdayEndTime           udt_std_time=NULL,
    @OperatingFridayStartTime           udt_std_time=NULL,
    @OperatingFridayEndTime             udt_std_time=NULL,
    @OperatingSaturdayStartTime         udt_std_time=NULL,
    @OperatingSaturdayEndTime           udt_std_time=NULL,
    @OperatingSundayStartTime           udt_std_time=NULL,
    @OperatingSundayEndTime             udt_std_time=NULL,
    @SysLastUserID                      udt_std_id,
    @SysLastUpdatedDate                 VARCHAR(30),
    @ApplicationCD                      udt_std_cd='APD',
    @MergeFlag                          udt_std_flag=0
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @tupdated_date     AS DATETIME 
    DECLARE @temp_updated_date AS DATETIME 

    DECLARE @ProcName          AS VARCHAR(30)       -- Used for raise error stmts 

    DECLARE @LogComment           udt_std_desc_long
    DECLARE @Name                 udt_std_name

     -- Shop Hours Audit variables
    DECLARE @dbOperatingMondayStartTime           udt_std_time,
            @dbOperatingMondayEndTime             udt_std_time,
            @dbOperatingTuesdayStartTime          udt_std_time,
            @dbOperatingTuesdayEndTime            udt_std_time,
            @dbOperatingWednesdayStartTime        udt_std_time,
            @dbOperatingWednesdayEndTime          udt_std_time,
            @dbOperatingThursdayStartTime         udt_std_time,
            @dbOperatingThursdayEndTime           udt_std_time,
            @dbOperatingFridayStartTime           udt_std_time,
            @dbOperatingFridayEndTime             udt_std_time,
            @dbOperatingSaturdayStartTime         udt_std_time,
            @dbOperatingSaturdayEndTime           udt_std_time,
            @dbOperatingSundayStartTime           udt_std_time,
            @dbOperatingSundayEndTime             udt_std_time


    SET @ProcName = 'uspSMTShopHoursUpdDetail'
    SET @Name = (SELECT Name FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopID)

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@OperatingFridayEndTime))) = 0 SET @OperatingFridayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingFridayStartTime))) = 0 SET @OperatingFridayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingMondayEndTime))) = 0 SET @OperatingMondayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingMondayStartTime))) = 0 SET @OperatingMondayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayEndTime))) = 0 SET @OperatingSaturdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayStartTime))) = 0 SET @OperatingSaturdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSundayEndTime))) = 0 SET @OperatingSundayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSundayStartTime))) = 0 SET @OperatingSundayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingThursdayEndTime))) = 0 SET @OperatingThursdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingThursdayStartTime))) = 0 SET @OperatingThursdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayEndTime))) = 0 SET @OperatingTuesdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayStartTime))) = 0 SET @OperatingTuesdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayEndTime))) = 0 SET @OperatingWednesdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayStartTime))) = 0 SET @OperatingWednesdayStartTime = NULL


    -- Apply edits
    
    IF @ShopID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE ShopLocationID = @ShopID
                                                                      AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop Location
            RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Shop Location
        RAISERROR('101|%s|@ShopID|%u', 16, 1, @ProcName, @ShopID)
        RETURN
    END

    -- Validate the updated date parameter
    IF @MergeFlag = 0
    BEGIN
      EXEC uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_shop_location_hours', @ShopID

      IF @@ERROR <> 0
      BEGIN
          -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

          RETURN
      END  
    END
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

      
    
    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    SELECT @dbOperatingMondayStartTime = OperatingMondayStartTime,
           @dbOperatingMondayEndTime = OperatingMondayEndTime,
           @dbOperatingTuesdayStartTime = OperatingTuesdayStartTime,
           @dbOperatingTuesdayEndTime = OperatingTuesdayEndTime,
           @dbOperatingWednesdayStartTime = OperatingWednesdayStartTime,
           @dbOperatingWednesdayEndTime = OperatingWednesdayEndTime,
           @dbOperatingThursdayStartTime = OperatingThursdayStartTime,
           @dbOperatingThursdayEndTime = OperatingThursdayEndTime,
           @dbOperatingFridayStartTime = OperatingFridayStartTime,
           @dbOperatingFridayEndTime = OperatingFridayEndTime,
           @dbOperatingSaturdayStartTime = OperatingSaturdayStartTime,
           @dbOperatingSaturdayEndTime = OperatingSaturdayEndTime,
           @dbOperatingSundayStartTime = OperatingSundayStartTime,
           @dbOperatingSundayEndTime = OperatingSundayEndTime  
    FROM dbo.utb_shop_location_hours
    WHERE ShopLocationID = @ShopID
    
    
    
    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopLocationUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Begin Audit Log Code ------------------------------------------------------------------------------------------
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingMondayStartTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingMondayStartTime, ''))))
    BEGIN
      SET @LogComment = 'Monday Start Time was changed from ' + 
                        ISNULL(@dbOperatingMondayStartTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingMondayStartTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingTuesdayEndTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingTuesdayEndTime, ''))))
    BEGIN
      SET @LogComment = 'Monday End Time was changed from ' + 
                        ISNULL(@dbOperatingTuesdayEndTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingTuesdayEndTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingTuesdayStartTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingTuesdayStartTime, ''))))
    BEGIN
      SET @LogComment = 'Tuesday Start Time was changed from ' + 
                        ISNULL(@dbOperatingTuesdayStartTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingTuesdayStartTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingTuesdayEndTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingTuesdayEndTime, ''))))
    BEGIN
      SET @LogComment = 'Tuesday End Time was changed from ' + 
                        ISNULL(@dbOperatingTuesdayEndTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingTuesdayEndTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingWednesdayStartTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingWednesdayStartTime, ''))))
    BEGIN
      SET @LogComment = 'Wednesday Start Time was changed from ' + 
                        ISNULL(@dbOperatingWednesdayStartTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingWednesdayStartTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingWednesdayEndTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingWednesdayEndTime, ''))))
    BEGIN
      SET @LogComment = 'Wednesday End Time was changed from ' + 
                        ISNULL(@dbOperatingWednesdayEndTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingWednesdayEndTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingThursdayStartTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingThursdayStartTime, ''))))
    BEGIN
      SET @LogComment = 'Thursday Start Time was changed from ' + 
                        ISNULL(@dbOperatingThursdayStartTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingThursdayStartTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingThursdayEndTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingThursdayEndTime, ''))))
    BEGIN
      SET @LogComment = 'Thursday End Time was changed from ' + 
                        ISNULL(@dbOperatingThursdayEndTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingThursdayEndTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingFridayStartTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingFridayStartTime, ''))))
    BEGIN
      SET @LogComment = 'Friday Start Time was changed from ' + 
                        ISNULL(@dbOperatingFridayStartTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingFridayStartTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingFridayEndTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingFridayEndTime, ''))))
    BEGIN
      SET @LogComment = 'Friday End Time was changed from ' + 
                        ISNULL(@dbOperatingFridayEndTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingFridayEndTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingSaturdayStartTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingSaturdayStartTime, ''))))
    BEGIN
      SET @LogComment = 'Saturday Start Time was changed from ' + 
                        ISNULL(@dbOperatingSaturdayStartTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingSaturdayStartTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingSaturdayEndTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingSaturdayEndTime, ''))))
    BEGIN
      SET @LogComment = 'Sunday End Time was changed from ' + 
                        ISNULL(@dbOperatingSaturdayEndTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingSaturdayEndTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingSundayStartTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingSundayStartTime, ''))))
    BEGIN
      SET @LogComment = 'Sunday Start Time was changed from ' + 
                        ISNULL(@dbOperatingSundayStartTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingSundayStartTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbOperatingSundayEndTime, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@OperatingSundayEndTime, ''))))
    BEGIN
      SET @LogComment = 'Sunday End Time was changed from ' + 
                        ISNULL(@dbOperatingSundayEndTime, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@OperatingSundayEndTime, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    -- End Audit Log Code --------------------------------------------------------------------------------------------
    

    UPDATE  dbo.utb_shop_location_hours SET
            ShopLocationID = @ShopID,
            OperatingFridayEndTime = dbo.ufnUtilityPadChars( @OperatingFridayEndTime, 4, '0', 1 ),
            OperatingFridayStartTime = dbo.ufnUtilityPadChars( @OperatingFridayStartTime, 4, '0', 1 ),
            OperatingMondayEndTime = dbo.ufnUtilityPadChars( @OperatingMondayEndTime, 4, '0', 1 ),
            OperatingMondayStartTime = dbo.ufnUtilityPadChars( @OperatingMondayStartTime, 4, '0', 1 ),
            OperatingSaturdayEndTime = dbo.ufnUtilityPadChars( @OperatingSaturdayEndTime, 4, '0', 1 ),
            OperatingSaturdayStartTime = dbo.ufnUtilityPadChars( @OperatingSaturdayStartTime, 4, '0', 1 ),
            OperatingSundayEndTime = dbo.ufnUtilityPadChars( @OperatingSundayEndTime, 4, '0', 1 ),
            OperatingSundayStartTime = dbo.ufnUtilityPadChars( @OperatingSundayStartTime, 4, '0', 1 ),
            OperatingThursdayEndTime = dbo.ufnUtilityPadChars( @OperatingThursdayEndTime, 4, '0', 1 ),
            OperatingThursdayStartTime = dbo.ufnUtilityPadChars( @OperatingThursdayStartTime, 4, '0', 1 ),
            OperatingTuesdayEndTime = dbo.ufnUtilityPadChars( @OperatingTuesdayEndTime, 4, '0', 1 ),
            OperatingTuesdayStartTime = dbo.ufnUtilityPadChars( @OperatingTuesdayStartTime, 4, '0', 1 ),
            OperatingWednesdayEndTime = dbo.ufnUtilityPadChars( @OperatingWednesdayEndTime, 4, '0', 1 ),
            OperatingWednesdayStartTime = dbo.ufnUtilityPadChars( @OperatingWednesdayStartTime, 4, '0', 1 ),
            SysLastUserID = @SysLastUserID,
            SysLastUpdatedDate = CURRENT_TIMESTAMP
      WHERE ShopLocationID = @ShopID
        AND SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('104|%s|utb_shop_location_hours', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    COMMIT TRANSACTION AdmShopLocationUpdDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ShopID AS ShopID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopHoursUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopHoursUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
