-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditResultInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAuditResultInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAuditResultInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Procedure to insert the audit result
*
* PARAMETERS:  
* (I) @EstimateDocumentID       The Estimate Document ID
* (I) @EstimateDetailNumber     The estimate Detail number
* (I) @AuditRuleName            The Audit rule name
* (I) @AuditDescription         The Audit description - this will hold the rule action
* (I) @AppliedWeight            The Rule weight
* (I) @SysLastUserID            The User calling this procedure
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAuditResultInsDetail
(
    @EstimateDocumentID         bigint,
    @EstimateDetailNumber       int = NULL,
    @AuditRuleName              varchar(50),
    @AuditDescription           varchar(500),
    @AppliedWeight              int,
    @UserLastName               varchar(50)
)
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @ProcName           as varchar(50)
    DECLARE @Now                AS datetime
    DECLARE @error              int
    DECLARE @rowcount           int
    DECLARE @UserID             int

    SET @ProcName = 'uspAuditWeightUpdDetail'
    SET @Now = CURRENT_TIMESTAMP
    
    -- Check the validity of the document id
    
    IF NOT EXISTS(SELECT DocumentID
                  FROM dbo.utb_document
                  WHERE DocumentID = @EstimateDocumentID
                    AND DocumentTypeID in (SELECT DocumentTypeID
                                           FROM dbo.utb_document_type
                                           WHERE EstimateTypeFlag = 1))
    BEGIN
        -- Invalid Document ID. Passed in value does not correspond to a document type of estimate.
        
        RAISERROR('101|%s|"DocumentID"|Invalid Estimate Document ID', 16, 1, @ProcName)
        RETURN        
    END
    
    IF @AuditRuleName IS NULL OR LEN(LTRIM(RTRIM(@AuditRuleName))) = 0
    BEGIN
        -- Audit Rule name cannot be empty
        RAISERROR('101|%s|"AuditRuleName"|Invalid or empty Audit Rule Name', 16, 1, @ProcName)
        RETURN        
    END
    
    IF @AuditDescription IS NULL OR LEN(LTRIM(RTRIM(@AuditDescription))) = 0
    BEGIN
        -- Audit Rule description cannot be empty
        RAISERROR('101|%s|"AuditDescription"|Invalid or empty Audit Description', 16, 1, @ProcName)
        RETURN        
    END
    
    
    SELECT @UserID = UserID
    FROM dbo.utb_user
    WHERE NameLast = @UserLastName
    
    BEGIN TRANSACTION InsAuditResultTrans

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    INSERT INTO dbo.utb_estimate_audit_results
    (   
        EstimateDocumentID, 
        EstimateDetailNumber, 
        AuditRuleName,
        AuditDescription,
        AppliedWeight,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES
    (
        @EstimateDocumentID,
        @EstimateDetailNumber,
        @AuditRuleName,
        @AuditDescription,
        @AppliedWeight,
        @UserID,
        @NOW
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    IF @error <> 0
    BEGIN
       -- Insert failure
    
        RAISERROR('105|%s|utb_estimate_audit_results', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION InsAuditResultTrans

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    RETURN @rowcount
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditResultInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAuditResultInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/