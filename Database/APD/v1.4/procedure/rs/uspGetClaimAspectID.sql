-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClaimAspectID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClaimAspectID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetClaimAspectID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets the ClaimAspectID from the utb_claim_aspect table by LynxID 
*				and ClaimAspectTypeID
*
* PARAMETERS:  
*				LynxID
*				, ClaimAspectTypeID (Optional) 
* RESULT SET:
*				Integer Value
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetClaimAspectID
	@iLynxID INT
	, @iClaimAspectTypeID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	IF(@iClaimAspectTypeID > 0)
	BEGIN
		SELECT
			ClaimAspectID 
		FROM
			utb_claim_aspect 
		WHERE
			lynxid = @iLynxID
			AND ClaimAspectTypeID = @iClaimAspectTypeID
	END
	ELSE
	BEGIN
		SELECT
			ClaimAspectID 
		FROM
			utb_claim_aspect 
		WHERE
			lynxid = @iLynxID
			AND ClaimAspectTypeID = 0
	END

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClaimAspectID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClaimAspectID TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClaimAspectID TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/