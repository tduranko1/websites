-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION SPBuild


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateModelBundling' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateModelBundling 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateModelBundling
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates new bundling tables inserting a new record into the 
*				utb_bundling, utb_client_bunding and utb_bundling_document_type table modeled after @iModelInsuranceCompanyID
*
* PARAMETERS:  
*
*   @iNewInsuranceCompanyID
*   @iModelInsuranceCompanyID
*	@iCreatedByUserID
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateModelBundling
	@iNewInsuranceCompanyID INT
	, @iModelInsuranceCompanyID INT
	, @vCompanyNameShort VARCHAR(6)
	, @iCreatedByUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @dtNow AS DATETIME 
    DECLARE @ProcName AS VARCHAR(50)

    SET @ProcName = 'uspCreateModelBundling'

    -- Set Database options
    SET NOCOUNT ON
	SET @dtNow = CURRENT_TIMESTAMP
	
	DECLARE @Debug AS udt_std_flag
	SET @Debug = 0

    -- Set transaction isolation level
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

	-- Check to make sure the new insurance company id exists
    IF  (@iNewInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iNewInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iNewInsuranceCompanyID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure the model insurance company id exists
    IF  (@iModelInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iModelInsuranceCompanyID))
    BEGIN
        -- Invalid Model Insurance Company ID
        RAISERROR('101|%s|@iModelInsuranceCompanyID|%u', 16, 1, @ProcName, @iModelInsuranceCompanyID)
        RETURN
    END

	BEGIN TRANSACTION Build

	-- Get the next available identities and lock other from inserting new identities --
	DECLARE @NextBundlingID INT
	SELECT @NextBundlingID=MAX(BundlingID)+1 FROM utb_bundling

	DECLARE @NextMessageTemplateID INT
	SELECT @NextMessageTemplateID=MAX(MessageTemplateID)+1 FROM utb_message_template

	DECLARE @NextClientBundlingID INT
	SELECT @NextClientBundlingID=MAX(ClientBundlingID)+1 FROM utb_client_bundling

    IF @Debug = 1
    BEGIN
        PRINT 'Parameters:'
        PRINT '    @NextBundlingID = ' + Convert(varchar(10), @NextBundlingID)
        PRINT '    @NextMessageTemplateID = ' + Convert(varchar(10), @NextMessageTemplateID)
        PRINT '    @NextClientBundlingID = ' + Convert(varchar(10), @NextClientBundlingID)
    END
    
	-- Build memory tables to hold bundling data --
	DECLARE @ModelBundling TABLE
	(
		RecID						INT NOT NULL IDENTITY(1,1)
		, NewBundlingID				INT
		, ModelBundlingID			INT
		, ModelDocumentTypeID		INT
		, ModelMessageTemplateID	INT
		, ModelEnabledFlag			BIT
		, ModelName					VARCHAR(50)
	);

	DECLARE @ModelMTBundling TABLE
	(
		RecID						INT NOT NULL IDENTITY(1,1)
		, NewMessageTemplateID		INT 
		, ModelMTAppliesToCD		VARCHAR(4)
		, ModelMTEnabledFlag		BIT
		, ModelMTDescription		VARCHAR(8000)
		, ModelMTServiceChannelCD	VARCHAR(4)
	);

	DECLARE @ModelClientBundling TABLE
	(
		RecID							INT NOT NULL IDENTITY(1,1)
		, ModelCBServiceChannelCD		VARCHAR(4)
		, ModelCBAutoNotifyAdjusterFlag BIT
	);

		IF @Debug = 1
		BEGIN
			DECLARE @RC INT
			SELECT @RC=MAX(RecID) FROM @ModelBundling
			PRINT 'Parameters:'
			PRINT '    Pre ReqID: @ModelBundling = ' + CONVERT(VARCHAR(10), @RC)
		END

		-- Populate the memory tables from the real data --
		-- Insert model bundling into memory table --
		INSERT INTO @ModelBundling (ModelBundlingID, ModelDocumentTypeID, ModelMessageTemplateID, ModelEnabledFlag, ModelName)
		SELECT 
			b.BundlingID
			, b.DocumentTypeID
			, b.MessageTemplateID
			, b.EnabledFlag
			, b.[Name]
		FROM 
			utb_bundling b 
			INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @iModelInsuranceCompanyID

		-- Reseed NewBundlingID --
		UPDATE @ModelBundling SET NewBundlingID = @NextBundlingID + (RecID-1)

		IF @Debug = 1
		BEGIN
			--DECLARE @RC INT
			SELECT @RC=MAX(NewBundlingID) FROM @ModelBundling
			PRINT 'Parameters:'
			PRINT '    Post ReSeed: @ModelBundling = ' + CONVERT(VARCHAR(10), @RC)
			
			SELECT @RC=MAX(NewMessageTemplateID) FROM @ModelMTBundling
			PRINT '    Pre ReSeed: @ModelMTBundling = ' + CONVERT(VARCHAR(10), @RC)
		END

		-- Insert model message template into memory table --
		INSERT INTO @ModelMTBundling (ModelMTAppliesToCD, ModelMTEnabledFlag, ModelMTDescription, ModelMTServiceChannelCD)
		SELECT 
			mt.AppliesToCD
			, mt.EnabledFlag
			, mt.[Description]
			, mt.ServiceChannelCD
		FROM 
			utb_bundling b 
			INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID 
			INNER JOIN utb_message_template mt 
			ON mt.MessageTemplateID = b.MessageTemplateID
		WHERE 
			cb.InsuranceCompanyID = @iModelInsuranceCompanyID

		-- Reseed NewMessageTemplateID --
		UPDATE @ModelMTBundling SET NewMessageTemplateID = @NextMessageTemplateID + (RecID-1)

		IF @Debug = 1
		BEGIN
			SELECT @RC=MAX(NewMessageTemplateID) FROM @ModelMTBundling
			PRINT 'Parameters:'
			PRINT '    Post ReSeed: @ModelMTBundling = ' + CONVERT(VARCHAR(10), @RC)
			
			SELECT @RC=MAX(RecID) FROM @ModelClientBundling
			PRINT '    Pre ReSeed: @ModelClientBundling = ' + CONVERT(VARCHAR(10), @RC)
		END

		-- Insert model client bunding into memory table --
		INSERT INTO @ModelClientBundling (ModelCBServiceChannelCD, ModelCBAutoNotifyAdjusterFlag)
		SELECT 
			cb.ServiceChannelCD
			, cb.AutoNotifyAdjusterFlag
		FROM 
			utb_bundling b 
			INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID 
			INNER JOIN utb_message_template mt 
			ON mt.MessageTemplateID = b.MessageTemplateID
		WHERE 
			cb.InsuranceCompanyID = @iModelInsuranceCompanyID

		IF @Debug = 1
		BEGIN
			SELECT @RC=MAX(RecID) FROM @ModelClientBundling
			PRINT 'Parameters:'
			PRINT '    Post ReSeed: @ModelClientBundling = ' + CONVERT(VARCHAR(10), @RC)
			
			SELECT @RC=MAX(MessageTemplateID) FROM utb_message_template
			PRINT '    Pre LIVE: utb_message_template = ' + CONVERT(VARCHAR(10),@RC)
		END
	
		-- Insert the @ModelMTBundling memory table data into the utb_message_template table --
		INSERT INTO utb_message_template 
		SELECT
			mtb.ModelMTAppliesToCD
			, mtb.ModelMTEnabledFlag
			, SUBSTRING(mtb.[ModelMTDescription],0,CHARINDEX('|',mtb.[ModelMTDescription])) + '|Messages/' + SUBSTRING(REPLACE(SUBSTRING(mtb.[ModelMTDescription],CHARINDEX('|',mtb.[ModelMTDescription])+1,LEN(mtb.[ModelMTDescription])),'Messages/',''),0,4) + @vCompanyNameShort + mtb.ModelMTServiceChannelCD + REPLACE(SUBSTRING(mtb.[ModelMTDescription],0,CHARINDEX('|',mtb.[ModelMTDescription])),' ','') + '.xls' AS NewFileName
			, mtb.ModelMTServiceChannelCD
			, @iCreatedByUserID
			, @dtNow
		FROM 
			@ModelBundling mb
			INNER JOIN @ModelMTBundling mtb
			ON mtb.RecID = mb.RecID
			INNER JOIN @ModelClientBundling cb
			ON cb.RecID = mb.RecID

		IF @Debug = 1
		BEGIN
			SELECT @RC=MAX(MessageTemplateID) FROM utb_message_template
			PRINT 'Parameters:'
			PRINT '    Post LIVE: utb_message_template = ' + CONVERT(VARCHAR(10), @RC)
			
			SELECT @RC=MAX(BundlingID) FROM utb_bundling
			PRINT '    Pre LIVE: utb_bundling = ' + CONVERT(VARCHAR(10), @RC)
		END

		-- Insert the @ModelBundling memory table data into the utb_bundling table --
		INSERT INTO utb_bundling
		SELECT 
			mb.ModelDocumentTypeID
			, mtb.NewMessageTemplateID
			, mb.ModelEnabledFlag
			, mb.ModelName
			, @iCreatedByUserID
			, @dtNow
		FROM 
			@ModelBundling mb
			INNER JOIN @ModelMTBundling mtb
			ON mtb.RecID = mb.RecID
			INNER JOIN @ModelClientBundling cb
			ON cb.RecID = mb.RecID

		IF @Debug = 1
		BEGIN
			SELECT @RC=MAX(BundlingID) FROM utb_bundling
			PRINT 'Parameters:'
			PRINT '    Post LIVE: utb_bundling = ' + CONVERT(VARCHAR(10), @RC)
			
			SELECT @RC=MAX(ClientBundlingID) FROM utb_client_bundling
			PRINT '    Pre LIVE: utb_client_bundling = ' + CONVERT(VARCHAR(10), @RC)
		END

		-- Insert the @ModelBundling memory table data into the utb_client_bundling table --
		INSERT INTO utb_client_bundling
		SELECT 
			mb.NewBundlingID
			, @iNewInsuranceCompanyID
			, null
			, cb.ModelCBAutoNotifyAdjusterFlag
			, null 
			, null 
			, null 
			, cb.ModelCBServiceChannelCD 
			, @iCreatedByUserID
			, @dtNow
		FROM 
			@ModelBundling mb
			INNER JOIN @ModelMTBundling mtb
			ON mtb.RecID = mb.RecID
			INNER JOIN @ModelClientBundling cb
			ON cb.RecID = mb.RecID

		IF @Debug = 1
		BEGIN
			SELECT @RC=MAX(ClientBundlingID) FROM utb_client_bundling
			PRINT 'Parameters:'
			PRINT '    Post LIVE: utb_client_bundling = ' + CONVERT(VARCHAR(10), @RC)
		END

		-- Insert the bundling document type data into the utb_bundling_document_type table --
		INSERT INTO utb_bundling_document_type
		SELECT 
			mb.NewBundlingID
			, bdt.DocumentTypeID
			, bdt.ConditionValue
			, bdt.DirectionalCD
			, bdt.DirectionToPayFlag
			, bdt.DuplicateFlag
			, bdt.EstimateDuplicateFlag
			, bdt.EstimateTypeCD
			, bdt.FinalEstimateFlag
			, bdt.LossState
			, bdt.MandatoryFlag
			, bdt.SelectionOrder
			, bdt.ShopState
			, bdt.VANFlag
			, bdt.WarrantyFlag
			, @iCreatedByUserID
			, @dtNow
		FROM 
			utb_bundling_document_type bdt
			INNER JOIN utb_document_type dt
			ON dt.DocumentTypeID = bdt.DocumentTypeID 
			INNER JOIN @ModelBundling mb
			ON mb.ModelBundlingID = bdt.BundlingID
		WHERE 	
			bdt.BundlingID IN
			(
				SELECT 
					b.BundlingID 
				FROM 
					utb_message_template mt 
					INNER JOIN utb_bundling b 
					ON b.MessageTemplateID = mt.MessageTemplateID 
					INNER JOIN utb_client_bundling cb
					ON cb.BundlingID = b.BundlingID 
				WHERE 
					cb.InsuranceCompanyID = @iModelInsuranceCompanyID
			)
	
		-- Check Insert was processed --
		IF 	@NextClientBundlingID < (SELECT MAX(ClientBundlingID) FROM utb_client_bundling)
		BEGIN
			IF @Debug = 1
			BEGIN
				PRINT 'Parameters:'
				PRINT '    Committed '
			END

			COMMIT TRANSACTION Build
			SELECT 1 RetCode
			-- COMMIT TRANSACTION;
		END
		ELSE
		BEGIN
			IF @Debug = 1
			BEGIN
				PRINT 'Parameters:'
				PRINT '    Rolled Back '
			END

			ROLLBACK TRANSACTION Build
			SELECT 0 RetCode
			-- ROLLBACK TRANSACTION;
		END
	
	-- Done
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateModelBundling' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateModelBundling TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateModelBundling TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT TRANSACTION SPBuild
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK TRANSACTION SPBuild
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/