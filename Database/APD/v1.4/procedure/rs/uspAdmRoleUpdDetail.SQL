-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmRoleUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmRoleUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspAdmRoleUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates role information
*
* PARAMETERS:  
* (I) @RoleID               Role ID to update
* (I) @RoleName             Role name
* (I) @ProfileList          Comma delimited list of profile/value pairs
* (I) @PermissionList       Comma delimited list of permission/Create/Read/Update/Delete sets
* (I) @SysLastUserID        User performing the update
* (I) @SysLastUpdatedDate   The "previous" updated date
*
* RESULT SET:
*   The new last updated date
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdmRoleUpdDetail
    @RoleID                 udt_std_id,
    @RoleName               udt_std_name,
    @ProfileList            varchar(500),
    @PermissionList         varchar(500),
    @SysLastUserID          udt_std_id,
    @SysLastUpdatedDate     varchar(30)
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@RoleName))) = 0 SET @RoleName = NULL
    IF LEN(RTRIM(LTRIM(@ProfileList))) = 0 SET @ProfileList = NULL
    IF LEN(RTRIM(LTRIM(@PermissionList))) = 0 SET @PermissionList = NULL


    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @tupdated_date          AS datetime 
    DECLARE @temp_updated_date      AS datetime
    DECLARE @now                    AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspAdmRoleUpdDetail'


    -- SET NOCOUNT to ON and no longer display the count message or warnings
    
    SET NOCOUNT ON


    -- Validate the input parameters
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_role', @RoleID

    if @@error <> 0
    BEGIN
        RETURN
    END
    
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Apply edits


    IF @RoleName IS NULL
    BEGIN
        -- Role Name Required
   
        RAISERROR  ('101|%s|@RoleName|%s', 16, 1, @ProcName, @RoleName)
        RETURN
    END


    -- Declare and navigate cursor to get and populate Profile settings.  The structure of the profile list is pairs of
    -- values.  Each pair corresponds to a Profile ID and an associated value.  An example Profile List string will be
    -- as follows:    1,ABC,4,50
    -- In the example above, the first profile ID id 1 and it's associated value is ABC.  The second Profile ID is 4 and
    -- it's associated value is 50.


    DECLARE @tmpProfile TABLE
    (
        ProfileID       int,
        Value           varchar(50)
    )

    DECLARE @ProfileID              int
    DECLARE @ProfileValue           varchar(50)   

    DECLARE csrProfile CURSOR FOR
        SELECT value FROM dbo.ufnUtilityParseString( @ProfileList, ',', 1 ) -- 1=trim spaces
     
    OPEN csrProfile

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    -- Get Profile ID (1st value of the pair)

    FETCH next
    FROM csrProfile
    INTO @ProfileID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    -- Get Profile Value (2nd value of the pair)

    FETCH next
    FROM csrProfile
    INTO @ProfileValue

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        -- Insert the value pait into another table more suited to contain ID/Value pairs
        
        INSERT INTO @tmpProfile VALUES (@ProfileID, @ProfileValue)
 
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('105|%s|@tmpProfile', 16, 1, @ProcName)
            RETURN
        END    
    
    
        -- Fetch the next values and then repeat until the end of the list
        
        FETCH next
        FROM csrProfile
        INTO @ProfileID
   
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    
   

        FETCH next
        FROM csrProfile
        INTO @ProfileValue

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    
    END


    CLOSE csrProfile
    DEALLOCATE csrProfile

    
    -- Declare and navigate cursor to get and populate Permission Settings.  The structure of the permission list is as sets
    -- of 5 values.  Each set corresponds to a Permission ID and 4 associated values representing Create, Read, Update, and
    -- Delete flags.  An example Permission List string will be as follows:
    -- 1,1,1,0,1,4,0,1,1,1
    -- In the example above, the first Permission ID id 1 and it's associated CRUD values are 1, 1, 0, 1.  The second 
    -- Permission ID is 4 and it's associated CRUD values are 0, 1, 1, 1.


    DECLARE @tmpPermission TABLE
    (
        PermissionID    int,
        CreateFlag      bit,
        ReadFlag        bit,
        UpdateFlag      bit,
        DeleteFlag      bit
    )

    DECLARE @PermissionID   int
    DECLARE @CreateFlag     bit
    DECLARE @ReadFlag       bit
    DECLARE @UpdateFlag     bit
    DECLARE @DeleteFlag     bit

    DECLARE csrPermission CURSOR FOR
        SELECT value FROM dbo.ufnUtilityParseString( @PermissionList, ',', 1 ) -- 1=trim spaces
     
    OPEN csrPermission

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    -- Get Permission ID (1st value of the set)

    FETCH next
    FROM csrPermission
    INTO @PermissionID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    -- Get CRUD Values (next 4 values)

    FETCH next
    FROM csrPermission
    INTO @CreateFlag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrPermission
    INTO @ReadFlag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrPermission
    INTO @UpdateFlag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    FETCH next
    FROM csrPermission
    INTO @DeleteFlag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    


    WHILE @@Fetch_Status = 0
    BEGIN
        -- Insert the value sets into another table more suited to contain them
        
        INSERT INTO @tmpPermission VALUES (@PermissionID, @CreateFlag, @ReadFlag, @UpdateFlag, @DeleteFlag)

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('105|%s|@tmpPermission', 16, 1, @ProcName)
            RETURN
        END    


        -- Fetch the next set of values and then repeat until the end of the list
        
        FETCH next
        FROM csrPermission
        INTO @PermissionID

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        FETCH next
        FROM csrPermission
        INTO @CreateFlag

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        FETCH next
        FROM csrPermission
        INTO @ReadFlag

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        FETCH next
        FROM csrPermission
        INTO @UpdateFlag

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    


        FETCH next
        FROM csrPermission
        INTO @DeleteFlag

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END    
    END


    CLOSE csrPermission
    DEALLOCATE csrPermission
    
    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    -- Begin Update

    BEGIN TRANSACTION RoleUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    UPDATE  dbo.utb_role
    SET Name                    = @RoleName,
        SysLastUserID           = @SysLastUserID,
        SysLastUpdatedDate      = @now
    WHERE 
        RoleID = @RoleID AND
	    SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_role', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Update Profile Settings for this role

    -- First remove all profile settings

    DELETE FROM dbo.utb_role_profile
      WHERE RoleID = @RoleID


    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|utb_role_profile', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Now populate with values from Profile List

    INSERT INTO dbo.utb_role_profile
    (
        RoleID,
        ProfileID,
        Value,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @RoleID,
            ProfileID,
            Value,
            @SysLastUserID,
            @now
      FROM  @tmpProfile

      
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_role_profile', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Update Permission Settings for this role

    -- First remove all permission settings

    DELETE FROM dbo.utb_role_permission
      WHERE RoleID = @RoleID


    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|utb_role_permission', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Now populate with values from Permission List

    INSERT INTO dbo.utb_role_permission
    (
        RoleID,
        PermissionID,
        CreateFlag,
        ReadFlag,
        UpdateFlag,
        DeleteFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    SELECT  @RoleID,
            PermissionID,
            CreateFlag,
            ReadFlag,
            UpdateFlag,
            DeleteFlag,
            @SysLastUserID,
            @now
      FROM  @tmpPermission

      
    -- Check error value
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('105|%s|utb_role_permission', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION RoleUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Get New SysLastUpdatedDate
--    SELECT dbo.ufnUtilityGetDateString(SysLastUpdatedDate) AS SysLastUpdatedDate
--      FROM dbo.utb_role
--      WHERE RoleID = @RoleID

    -- Create XML Document to return updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- User Level
            NULL AS [Role!2!RoleID],
            NULL AS [Role!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Role Level
            @RoleID,
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmRoleUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmRoleUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/