-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientReports' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClientReports 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetClientReports
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all client assignments from the database by InsuranceCompanyID
*
* PARAMETERS:  
*				InsuranceCompanyID 
* RESULT SET:
*   All data related to insurance client assignments
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetClientReports
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT 
		cr.ClientReportID
		, cr.InsuranceCompanyID
		, ISNULL(cr.OfficeID,0) OfficeID
		, ISNULL(r.[Description],'') ReportName
		, ISNULL(r.DisplayOrder,'') DisplayOrder
		, ISNULL(r.EnabledFlag,'') EnabledFlag
		, ISNULL(r.[FileName],'') 'FileName'
		, ISNULL(r.OfficeLevelUserViewFlag,'') OfficeLevelUserViewFlag
		, ISNULL(r.ReportTypeCD,'') ReportTypeCD
		, ISNULL(r.SPName,'') SPName
		, ISNULL(r.StaticParameters,'') StaticParameters
		, ISNULL(r.SysMaintainedFlag,'') SysMaintainedFlag
		, r.SysLastUserID
		, r.SysLastUpdatedDate	 
	FROM
		utb_client_report cr
		INNER JOIN dbo.utb_report r
		ON r.ReportID = cr.ReportID
	WHERE 
		r.EnabledFlag = 1	 
		AND cr.InsuranceCompanyID = @iInsuranceCompanyID
	ORDER BY
		r.[Description] 
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientReports' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClientReports TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClientReports TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/