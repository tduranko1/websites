-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInsuranceUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopInsuranceUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopInsuranceUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Creates new Client Shop Location records with include and excludeflags
*
* PARAMETERS:  
* (I) @ShopID                   The Shop Location's unique identity
* (I) @SpecialtyIDs             The Specialty's unique identity
* (I) @SysLastUserID            The Shop Location Specialty's updating user identity
*
* RESULT SET:
* ShopLocationID                The Shop Location unique identity
* SpecialtyID                   The Specialty unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopInsuranceUpdDetail
(
	@ShopID                    	        udt_std_int_big,
	@InsuranceExcludeIDs                udt_std_desc_mid,
  @InsuranceIncludeIDs                udt_std_desc_mid,
  @SysLastUserID                      udt_std_id,
  @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error              udt_std_int
    DECLARE @rowcount           udt_std_int

    DECLARE @ProcName           varchar(30)       -- Used for raise error stmts 
    DECLARE @now                udt_std_datetime
    DECLARE @Duplicates         udt_std_desc_long,
            @KeyDescription     udt_std_name
    
    DECLARE @tmpInsCo           TABLE (InsuranceCompanyID   int           NOT NULL,
                                       Name                 varchar(50)       NULL,
                                       ExcludeFlag          bit           NOT NULL,
                                       IncludeFlag          bit           NOT NULL)
                                       
                                       
    DECLARE @dbInsCo            TABLE (InsuranceCompanyID   int           NOT NULL,
                                       ExcludeFlag          bit           NOT NULL,
                                       IncludeFlag          bit           NOT NULL)
                                       
    DECLARE @tmpChanges         TABLE (AuditTypeCD          varchar(4)    NOT NULL,
                                       KeyDescription       varchar(50)   NOT NULL,
                                       KeyID                int           NOT NULL,
                                       LogComment           varchar(500)  NOT NULL,
                                       SysLastUserID        int           NOT NULL,
                                       SysLastUpdatedDate   datetime      NOT NULL)
                                                      
                                
    SET @ProcName = 'uspSMTShopInsuranceUpdDetail'
    SET @now = CURRENT_TIMESTAMP
    SET @KeyDescription = (SELECT Name FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopID)
    SET @rowcount = 0


    -- Apply edits
    IF @ShopID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE ShopLocationID = @ShopID
                                                                      AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop Location
        
            RAISERROR  ('101|%s|@ShopID|%u', 16, 1, @procname, @ShopID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Shop Location
    
        RAISERROR  ('101|%s|@ShopID|%u', 16, 1, @procname)
        RETURN
    END

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('101|%s|@SysLastUserID|%u', 16, 1, @procname, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('101|%s|@SysLastUserID|%u', 16, 1, @procname)
        RETURN
    END
   
    -- Gather pertinent values from the updated exclude and include lists                            
    INSERT INTO @tmpInsCo (InsuranceCompanyID, Name, ExcludeFlag, IncludeFlag)
    
        -- Excludes
        SELECT value, 
                (SELECT Name FROM dbo.utb_insurance WHERE InsuranceCompanyID = CONVERT(int, f.value)),
                1, 
                0 
        FROM dbo.ufnUtilityParseString(@InsuranceExcludeIDs, ',', 1) f
               
        UNION ALL
    
        -- Includes
        SELECT value, 
               (SELECT Name FROM dbo.utb_insurance WHERE InsuranceCompanyID = CONVERT(int, f.value)),
               0, 
               1 
        FROM dbo.ufnUtilityParseString(@InsuranceIncludeIDs, ',', 1) f
    
    
    IF (@@ERROR <> 0)
    BEGIN
        -- Insertion Error
    
        RAISERROR  ('105|%s|@tmpInsCo', 16, 1, @procname)
        RETURN
    END
    
  
    -- Ensure this shop is not included in both the include and exclude list for any insurance company.
    SET @Duplicates = ''
    
    SELECT @Duplicates = @Duplicates + i.Name + ', '
    FROM @tmpInsCo t LEFT JOIN dbo.utb_insurance i ON (t.InsuranceCompanyID = i.InsuranceCompanyID)
    WHERE ExcludeFlag = 1
      AND EXISTS (SELECT InsuranceCompanyID FROM @tmpInsCo WHERE InsuranceCompanyID = t.InsuranceCompanyID
                                                             AND IncludeFlag = 1)
    
    IF LEN(@Duplicates) > 0
    BEGIN
        RAISERROR  ('1|The following companies exist in both the Include and Exclude List. %s. Please remove the reference in one of the lists and try saving again.', 16, 1, @Duplicates)
        RETURN
    END
    
    
    -- Grab database values for comparison
    INSERT INTO @dbInsCo
    SELECT InsuranceCompanyID, ExcludeFlag, IncludeFlag
    FROM dbo.utb_client_shop_location
    WHERE ShopLocationID = @ShopID
    
    
    IF (@@ERROR <> 0)
    BEGIN
        -- Insertion Error
    
        RAISERROR  ('105|%s|@dbInsCo', 16, 1, @procname)
        RETURN
    END
    
    
    INSERT INTO @tmpChanges
        SELECT 'P',
               @KeyDescription,
               @ShopID,
               'Moved from Exclude list to INCLUDE list for search result candidates for ' + CONVERT(varchar(10), t.InsuranceCompanyID) + '-' + t.Name,
               @SysLastUserID,
               @now
        FROM @tmpInsCo t INNER JOIN @dbInsCo d ON t.InsuranceCompanyID = d.InsuranceCompanyID                                                                  
        WHERE t.IncludeFlag = 1 and d.ExcludeFlag = 1
          
      
        UNION ALL
    
        -- Changed from Incl to Excl
        SELECT 'P',
               @KeyDescription,
               @ShopID,
               'Moved from Include list to EXCLUDE list for search result candidates for ' + CONVERT(varchar(10), t.InsuranceCompanyID) + '-' + t.Name,
               @SysLastUserID,
               @now
        FROM @tmpInsCo t INNER JOIN @dbInsCo d ON t.InsuranceCompanyID = d.InsuranceCompanyID
        WHERE t.ExcludeFlag = 1 and d.IncludeFlag = 1
      
        UNION ALL    
      
    
        -- Set to Incl
        SELECT 'P',
               @KeyDescription,
               @ShopID,
               'Added to INCLUDE list for search result candidates for ' + CONVERT(varchar(10), t.InsuranceCompanyID) + '-' + t.Name,
               @SysLastUserID,
               @now
        FROM @tmpInsCo t
        WHERE IncludeFlag = 1
          AND t.InsuranceCompanyID NOT IN (SELECT InsuranceCompanyID  FROM @dbInsCo)
                                    
                                       
        UNION ALL    

        -- Set to Excl
        SELECT 'P',
               @KeyDescription,
               @ShopID,
               'Added to EXCLUDE list for search result candidates for ' + CONVERT(varchar(10), t.InsuranceCompanyID) + '-' + t.Name,
               @SysLastUserID,
               @now
        FROM @tmpInsCo t 
        WHERE ExcludeFlag = 1
          AND InsuranceCompanyID NOT IN (SELECT InsuranceCompanyID  FROM @dbInsCo)
                                      
                                                
        UNION ALL
    
        -- Removed from Incl
        SELECT 'P',
               @KeyDescription,
               @ShopID,
               'Removed from INCLUDE list for search result candidates for ' + 
                                       CONVERT(varchar(10), d.InsuranceCompanyID) + '-' + 
                                       (SELECT Name FROM dbo.utb_insurance WHERE InsuranceCompanyID = d.InsuranceCompanyID),
               @SysLastUserID,
               @now
        FROM @dbInsCo d 
        WHERE IncludeFlag = 1
          AND InsuranceCompanyID NOT IN (SELECT InsuranceCompanyID FROM @tmpInsCo)
                                         
        UNION ALL                                 
    
        -- Removed from Excl
        SELECT 'P',
               @KeyDescription,
               @ShopID,
               'Removed from EXCLUDE list for search result candidates for ' + 
                                       CONVERT(varchar(10), d.InsuranceCompanyID) + '-' + 
                                       (SELECT Name FROM dbo.utb_insurance WHERE InsuranceCompanyID = d.InsuranceCompanyID),
               @SysLastUserID,
               @now
        FROM @dbInsCo d 
        WHERE ExcludeFlag = 1
          AND InsuranceCompanyID NOT IN (SELECT InsuranceCompanyID FROM @tmpInsCo)
                   
    
    -- Only perform updates and audits if something actually changed.
    IF (SELECT COUNT(*) FROM @tmpChanges) > 0
    BEGIN
    
      -- Begin Update(s)

      BEGIN TRANSACTION CSLTran1
        
    
      DELETE dbo.utb_client_shop_location
      WHERE ShopLocationID = @ShopID

      IF (@@ERROR <> 0)
      BEGIN
          -- Invalid User
        
          RAISERROR  ('106|%s|utb_client_shop_location', 16, 1, @procname)
          ROLLBACK TRANSACTION
          RETURN
      END
         
    
      INSERT INTO dbo.utb_client_shop_location 
      SELECT InsuranceCompanyID,
             @ShopID,
             ExcludeFlag,
             IncludeFlag,
             @SysLastUserID,
             @now
      FROM @tmpInsCo
        
      SELECT @rowcount = @@ROWCOUNT
    
      IF (@@ERROR <> 0)
      BEGIN
          -- Insertion Error
        
          RAISERROR  ('104|%s|utb_client_shop_location', 16, 1, @procname)
          ROLLBACK TRANSACTION
          RETURN
      END
    
        
      -- Begin Audit Log code  -----------------------------------------------------------------------------------
    
    
      INSERT INTO dbo.utb_audit_log (AuditTypeCD,
                                     KeyDescription,
                                     KeyID,
                                     LogComment,
                                     SysLastUserID,
                                     SysLastUpdatedDate)
                              SELECT AuditTypeCD,
                                     KeyDescription,
                                     KeyID,
                                     LogComment,
                                     SysLastUserID,
                                     SysLastUpdatedDate
                              FROM @tmpChanges
                                                               
          -- Changed From Excl to Incl
       
      IF (@@ERROR <> 0)
      BEGIN
        -- Error raised in calling proc
  
        ROLLBACK TRANSACTION
        RETURN
      END
    
    
         
      -- End of Audit Log code  ----------------------------------------------------------------------------------
    
    
      COMMIT TRANSACTION CSLTran1
         
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
    
          RAISERROR  ('99|%s', 16, 1, @procname)
          RETURN
      END
      
    END
    

    SELECT @ShopID AS ShopID
    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInsuranceUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopInsuranceUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/