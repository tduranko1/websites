-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopDocumentUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShopDocumentUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspShopDocumentUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspShopDocumentUpdDetail
(
    @ShopLocationID     bigint,
    @DocumentID         bigint,
    @EffectiveDate      datetime,
    @ExpirationDate     datetime,
    @DocumentType       udt_std_desc_short,
    @Comments           udt_std_note,
    @UserID             udt_std_id
)
AS
BEGIN
    DECLARE @now                        udt_std_datetime
    DECLARE @DocumentTypeID             int
    DECLARE @DocumentSourceID           int
        
    DECLARE @ProcName                   udt_std_desc_short
    SET @ProcName = 'uspShopDocumentUpdDetail'

    -- Validate that either an NTUserID or UserID was passed in 
    
    IF @UserID IS NULL
    BEGIN
       -- No user passed in

        RAISERROR('%s: @UserID required', 16, 1, @ProcName)
        RETURN
    END
    

    IF NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid UserID

        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    IF NOT EXISTS(SELECT  ShopLocationID FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopLocationID)
    BEGIN
    
            RAISERROR('101|%s|@ShopLocationID|%u', 16, 1, @ProcName, @ShopLocationID)
            RETURN
    END

    IF NOT EXISTS(SELECT  DocumentID FROM dbo.utb_document WHERE DocumentID = @DocumentID)
    BEGIN
    
            RAISERROR('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
            RETURN
    END


    -- Check Document Type
    
    SELECT  @DocumentTypeID = DocumentTypeID
      FROM  dbo.utb_document_type 
      WHERE Name = @DocumentType
        AND EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              

    IF @DocumentTypeID IS NULL
    BEGIN
        -- Invalid Document Type
        
        RAISERROR('101|%s|@DocumentType|%s', 16, 1, @ProcName, @DocumentType)
        RETURN
    END

    SET @now = CURRENT_TIMESTAMP

    -- Begin insert
    
    BEGIN TRANSACTION UpdateDocument
    
    UPDATE utb_Document
    SET  DocumentTypeID = @DocumentTypeID,
         Note = @Comments, 
         SysLastUserID = @UserID, 
         SysLastUpdatedDate = @Now
    WHERE DocumentID = @DocumentID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    

    UPDATE dbo.utb_shop_location_document
    SET EffectiveDate = @EffectiveDate,
        ExpirationDate = @ExpirationDate,
        SysLastUserID = @UserID,
        SysLastUpdatedDate = @Now
    WHERE ShopLocationID = @ShopLocationID
      AND DocumentID = @DocumentID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|utb_shop_location_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
            
    
    COMMIT TRANSACTION UpdateDocument

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END              


    RETURN @documentID 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopDocumentUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopDocumentUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/