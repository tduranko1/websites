-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClaimDetails' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClaimDetails 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetClaimDetails
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets client claim details from the utb_claim table by LynxID
*
* PARAMETERS:  
*				LynxID 
* RESULT SET:
*   All data related to insurance client office
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetClaimDetails
	@iLynxID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT
	  [LynxID]
      ,ISNULL([CallerInvolvedID],'') CallerInvolvedID
      ,ISNULL([CarrierRepUserID],'') CarrierRepUserID
      ,ISNULL(CatastrophicLossID,'') CatastrophicLossID
      ,ISNULL([ClaimantContactMethodID],'') ClaimantContactMethodID
      ,ISNULL([ContactInvolvedID],'') ContactInvolvedID
      ,[InsuranceCompanyID]
      ,ISNULL([IntakeUserID],'') IntakeUserID
      ,ISNULL([LossTypeID],'') LossTypeID
      ,ISNULL([RoadLocationID],'') RoadLocationID
      ,ISNULL([RoadtypeID],'') RoadtypeID
      ,ISNULL([WeatherConditionID],'') WeatherConditionID
      ,ISNULL([AgentAreaCode],'') AgentAreaCode
      ,ISNULL([AgentExchangeNumber],'') AgentExchangeNumber
      ,ISNULL([AgentExtensionNumber],'') AgentExtensionNumber
      ,ISNULL([AgentName],'') AgentName
      ,ISNULL([AgentUnitNumber],'') AgentUnitNumber
      ,ISNULL([ClientClaimNumber],'') ClientClaimNumber
      ,ISNULL([ClientClaimNumberSquished],'') ClientClaimNumberSquished
      ,ISNULL([ComplexityFinalScore],'') ComplexityFinalScore
      ,ISNULL([ComplexityInitialScore],'') ComplexityInitialScore
      ,[DemoFlag]
      ,ISNULL([IntakeFinishDate],'') IntakeFinishDate
      ,[IntakeStartDate]
      ,ISNULL([LossCity],'') LossCity
      ,ISNULL([LossCounty],'') LossCounty
      ,ISNULL([LossDate],'') LossDate
      ,ISNULL([LossDescription],'') LossDescription
      ,ISNULL([LossLocation],'') LossLocation
      ,ISNULL([LossState],'') LossState 
      ,ISNULL([LossZip],'') LossZip
      ,ISNULL([PoliceDepartmentName],'') PoliceDepartmentName
      ,ISNULL([PolicyNumber],'') PolicyNumber
      ,ISNULL([Remarks],'') Remarks
      ,ISNULL([RestrictedFlag],'') RestrictedFlag
      ,ISNULL([TripPurposeCD],'') TripPurposeCD
      ,[SysLastUserID]
      ,[SysLastUpdatedDate]
	FROM
		 dbo.utb_claim
	WHERE
		LynxID = @iLynxID
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClaimDetails' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClaimDetails TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClaimDetails TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/