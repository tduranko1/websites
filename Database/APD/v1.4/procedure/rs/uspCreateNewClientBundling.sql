-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewClientBundling' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateNewClientBundling 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateNewClientBundling
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client bundling by inserting a new record into the 
*				utb_client_bundling table modeled after @iModelInsuranceCompanyID
*
* PARAMETERS:  
*
*   @iNewInsuranceCompanyID
*   @iModelInsuranceCompanyID
*	@iCreatedByUserID
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateNewClientBundling
	@iNewInsuranceCompanyID INT
	, @iModelInsuranceCompanyID INT
	, @vCompanyNameShort VARCHAR(6)
	, @iCreatedByUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @dtNow AS DATETIME 
    DECLARE @ProcName AS VARCHAR(50)

    SET @ProcName = 'uspCreateNewClientBundling'

    -- Set Database options
    SET NOCOUNT ON
	SET @dtNow = CURRENT_TIMESTAMP

	-- Check to make sure the new insurance company id exists
    IF  (@iNewInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iNewInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iNewInsuranceCompanyID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure the model insurance company id exists
    IF  (@iModelInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iModelInsuranceCompanyID))
    BEGIN
        -- Invalid Model Insurance Company ID
        RAISERROR('101|%s|@iModelInsuranceCompanyID|%u', 16, 1, @ProcName, @iModelInsuranceCompanyID)
        RETURN
    END

	-- Add the new
	DECLARE db_cursor CURSOR FOR 
	SELECT cb.ServiceChannelCD FROM	utb_bundling b INNER JOIN utb_client_bundling cb ON b.BundlingId = cb.BundlingID INNER JOIN utb_message_template t ON t.MessageTemplateID = b.MessageTemplateID WHERE cb.InsuranceCompanyID = @iModelInsuranceCompanyID
	
	DECLARE @vName VARCHAR(100)
	DECLARE @vServiceChannelCD VARCHAR(5)

	DECLARE @iDocumentTypeID INT
	DECLARE @iNewMessageTemplateID INT
	DECLARE @iNewBundlingID INT
	DECLARE @iCnt INT
	SET @iCnt = 0

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO 	
		@vServiceChannelCD

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SELECT TOP 1 @iNewMessageTemplateID = MessageTemplateID FROM dbo.utb_message_template WHERE [Description] LIKE '%' + @vCompanyNameShort + '%' ORDER BY MessageTemplateID 
		SET @iNewMessageTemplateID = @iNewMessageTemplateID + @iCnt

		SELECT TOP 1 @iNewBundlingID=BundlingID FROM dbo.utb_bundling WHERE MessageTemplateID = @iNewMessageTemplateID
	
		INSERT INTO dbo.utb_client_bundling
		SELECT
			@iNewBundlingID
			, @iNewInsuranceCompanyID
			, NULL	
			, 0
			, NULL
			, NULL
			, NULL
			, @vServiceChannelCD
			, @iCreatedByUserID
			, @dtNow
			
			FETCH NEXT FROM db_cursor INTO @vServiceChannelCD
			SET @iCnt = @iCnt + 1
	END
	CLOSE db_cursor  
	DEALLOCATE db_cursor 		
	 
	IF EXISTS(
		SELECT 
			cb.ClientBundlingID
		FROM 
			utb_bundling b 
			INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @iNewInsuranceCompanyID
	)
	BEGIN
		SELECT 1 RetCode
	END
	ELSE
	BEGIN
		SELECT 0 RetCode
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewClientBundling' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateNewClientBundling TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateNewClientBundling TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/