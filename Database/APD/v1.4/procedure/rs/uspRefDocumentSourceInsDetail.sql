-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefDocumentSourceInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefDocumentSourceInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefDocumentSourceInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Insert a new Document Source
*
* PARAMETERS:  
* (I) @EstimatePackageID        Document Source Estimate Package identifier - Not required (Defaults to NULL)
* (I) @DisplayOrder             Document Source display order
* (I) @EnabledFlag              Document Source active/inactive flag
* (I) @Name                     Document Source name
* (I) @SysLastUserID            Document Source user
*
* RESULT SET:
* DocumentSourceID     The newly created Document Source unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefDocumentSourceInsDetail
(
	@EstimatePackageID                    udt_std_int_tiny = NULL,
    @DisplayOrder                       udt_std_int_tiny,
    @EnabledFlag                        udt_std_flag,
    @Name                               udt_std_name,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd = 'APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @DocumentSourceID AS udt_std_int_tiny

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now               AS datetime

    SET @ProcName = 'uspRefDocumentSourceInsDetail'
    SET @now = CURRENT_TIMESTAMP


    IF @EstimatePackageID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT EstimatePackageID FROM utb_estimate_package WHERE EstimatePackageID = @EstimatePackageID
                                                                            AND EnabledFlag = 1)
        BEGIN
           -- Invalid Estimate Package
        
            RAISERROR  ('1|Invalid Estimate Package', 16, 1, @ProcName)
            RETURN
        END
    END

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT u.UserID 
                       FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                           INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
                       WHERE u.UserID = @SysLastUserID
                         AND @SysLastUserID <> 0
                         AND ua.AccessBeginDate IS NOT NULL
                         AND ua.AccessBeginDate <= CURRENT_TIMESTAMP
                         AND ua.AccessEndDate IS NULL
                         AND a.Code = @ApplicationCD)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    SELECT @DocumentSourceID = MAX(DocumentSourceID) + 1
      FROM utb_document_source

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    BEGIN TRANSACTION RefDocumentSourceInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_document_source
   	(
        DocumentSourceID,
        EstimatePackageID,
        DisplayOrder,
        EnabledFlag,
        Name,
        SysMaintainedFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES	
    (
        @DocumentSourceID,
        @EstimatePackageID,
        @DisplayOrder,
        @EnabledFlag,
        @Name,
        0,
        @SysLastUserID,
        @now
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|utb_document_source', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    COMMIT TRANSACTION RefDocumentSourceInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Create XML Document to return new updated date time and updated vehicle involved list
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- New Involved Level
            NULL AS [Document!2!DocumentSourceID],
            NULL AS [Document!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- New Involved Level
            @DocumentSourceID,
            dbo.ufnUtilityGetDateString( @now )

    ORDER BY tag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefDocumentSourceInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefDocumentSourceInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/