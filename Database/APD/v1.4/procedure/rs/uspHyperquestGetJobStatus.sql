-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetJobStatus' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestGetJobStatus 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestGetJobStatus
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Returns the current JobStatus from the utb_hyperquestsvc_jobs table
* DATE:			10Feb2017
*
* PARAMETERS:  
*
* REVISIONS:	10Feb2017 - TVD - Initial Development
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestGetJobStatus]
	@iLynxID INT
	, @iClaimAspectID INT
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)

    SET @ProcName = 'uspHyperquestGetJobStatus'
    
    SET @ApplicationCD = 'HyperquestPartner'

    -- Validation
    
    -- Begin main code
	SELECT
		ISNULL(JobStatus,'NOAssignment') AS JobStatus
	FROM
		utb_hyperquestsvc_jobs
	WHERE
		LynxID = @iLynxID
		AND DocumentTypeID = 22
		AND ClaimAspectID = @iClaimAspectID
		AND AssignmentID > 0
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestGetJobStatus' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestGetJobStatus TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/