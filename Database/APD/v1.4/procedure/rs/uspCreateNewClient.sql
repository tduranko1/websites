-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewClient' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateNewClient 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateNewClient
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client by inserting a new recored into the utb_insurance table.
*
* PARAMETERS:  
*
*	InsuranceCompanyID
*   PaymentCompanyID
*   LynxContactPhone
*   CompanyName
*   CompanyNameShort
*   Address1
*   Address2
*   AddressCity
*   AddressState
*   AddressZip
*   AssignmentAtSelectionFlag
*	BillingModel
*	CFLogoDisplayFlag
*	ClaimPointDocumentUploadFlag
*	ClaimPointCarrierRepSelFlag
*	ClientAccessFlag
*   ClientProduct
*   CompanyFax
*   CompanyPhone
*	DemoFlag
*	DeskReviewLicenseReqFlag
*   EarlyBill
*	EnabledFlag
*	FedTaxID
*   PreferredCommunicationMethodID
*   TotalLossValuationPercentage
*   TotalLossPercentage
*   WarrantyWorkmanship
*   WarrantyRefinish
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateNewClient
	@iInsuranceCompanyID INT
    , @iPaymentCompanyID INT
    , @vLynxContactPhone VARCHAR(50)
    , @vCompanyName VARCHAR(50)
    , @vCompanyNameShort VARCHAR(4)
    , @vAddress1 VARCHAR(50)
    , @vAddress2 VARCHAR(50)
    , @vAddressCity VARCHAR(30)
    , @vAddressState VARCHAR(2)
    , @vAddressZip VARCHAR(8)
    , @AssignmentAtSelectionFlag BIT
	, @vBillingModel VARCHAR(4)
	, @bCFLogoDisplayFlag BIT
	, @bClaimPointDocumentUploadFlag BIT
	, @bClaimPointCarrierRepSelFlag BIT
	, @bClientAccessFlag BIT
    , @vClientProduct VARCHAR(4)
    , @vCompanyFax VARCHAR(12)
    , @vCompanyPhone VARCHAR(12)
	, @bDemoFlag BIT
	, @bDeskReviewLicenseReqFlag BIT
    , @bEarlyBill BIT
	, @bEnabledFlag BIT
	, @vFedTaxID VARCHAR(15)
    , @iPreferredCommunicationMethodID INT
    , @dTotalLossValuationPercentage DECIMAL(9,5)
    , @dTotalLossPercentage DECIMAL(9,5)
    , @vWarrantyWorkmanship VARCHAR(4)
    , @vWarrantyRefinish VARCHAR(4)
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS int
    DECLARE @rowcount AS int
    DECLARE @now AS datetime 
    
    -- Set Database options
    SET NOCOUNT ON
	SET @now = CURRENT_TIMESTAMP
	
    --Verify that a valid Insurance Company doesn't already exist
    IF (@iInsuranceCompanyID is Null) OR
    EXISTS (SELECT InsuranceCompanyID FROM utb_Insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID)
    BEGIN
        -- Insurance Company already exists
        RETURN 0
    END
    ELSE
    BEGIN
		INSERT INTO dbo.utb_insurance
			(
			 InsuranceCompanyID 
			 , DeskAuditPreferredCommunicationMethodID
			 , Address1
			 , Address2 
			 , AddressCity
			 , AddressState
			 , AddressZip
			 , AssignmentAtSelectionFlag
			 , BillingModelCD
			 , BusinessTypeCD
			 , CarrierLynxContactPhone
			 , CFLogoDisplayFlag
			 , ClaimPointCarrierRepSelFlag
			 , ClaimPointDocumentUploadFlag
			 , ClientAccessFlag
			 , DemoFlag
			 , DeskAuditCompanyCD
			 , DeskReviewLicenseReqFlag
			 , EarlyBillFlag
			 , EnabledFlag
			 , FaxAreaCode
			 , FaxExchangeNumber
			 , FaxUnitNumber
			 , FedTaxId
			 , IngresAccountingId
			 , InvoicingModelPaymentCD
			 , InvoiceMethodCD
			 , LicenseDeterminationCD
			 , Name
			 , PhoneAreaCode
			 , PhoneExchangeNumber
			 , PhoneUnitNumber
			 , ReturnDocDestinationCD
			 , ReturnDocPackageTypeCD
			 , ReturnDocRoutingCD
			 , TotalLossValuationWarningPercentage
			 , TotalLossWarningPercentage
			 , WarrantyPeriodRefinishMinCD
			 , WarrantyPeriodWorkmanshipMinCD
			 , SysLastUserID
			 , SysLastUpdatedDate 
			)
			VALUES( @iInsuranceCompanyID					-- InsuranceCompanyID
					, @iPreferredCommunicationMethodID -- DeskAuditPreferredCommunicationMethodID
					, @vAddress1						-- Address1 *
					, ISNULL(@vAddress2,'')				-- Address2
					, @vAddressCity         			-- AddressCity *
					, @vAddressState					-- AddressState *
					, @vAddressZip						-- AddressZip *
					, @AssignmentAtSelectionFlag		-- AssignmentAtSelectionFlag
					, @vBillingModel					-- BillingModelCD
					, @vClientProduct					-- BusinessTypeCD
					, @vLynxContactPhone			    -- CarrierLynxContactPhone
					, @bCFLogoDisplayFlag				-- CFLogoDisplayFlag
					, @bClaimPointCarrierRepSelFlag		-- ClaimPointCarrierRepSelFlag *
					, @bClaimPointDocumentUploadFlag	-- ClaimPointDocumentUploadFlag
					, @bClientAccessFlag				-- ClientAccessFlag
					, @bDemoFlag						-- DemoFlag
					, @vCompanyNameShort				-- DeskAuditCompanyCD *
					, @bDeskReviewLicenseReqFlag	    -- DeskReviewLicenseReqFlag
					, @bEarlyBill						-- EarlyBillFlag
					, @bEnabledFlag						-- EnabledFlag
					, SUBSTRING(@vCompanyFax,1,3)		-- FaxAreaCode *
					, SUBSTRING(@vCompanyFax,5,3)		-- FaxExchangeNumber *
					, SUBSTRING(@vCompanyFax,9,4)		-- FaxUnitNumber *
					, @vFedTaxId						-- FedTaxId
					, @iPaymentCompanyID			    -- IngresAccountingId
					, 'C'			                    -- InvoicingModelPaymentCD (Bulk / per claim)
					, 'P'			                    -- InvoiceMethodCD
					, 'LOSS'		                    -- LicenseDeterminationCD
					, @vCompanyName				 	    -- Name *
					, SUBSTRING(@vCompanyPhone,1,3)		-- PhoneAreaCode *
					, SUBSTRING(@vCompanyPhone,5,3)	    -- PhoneExchangeNumber *
					, SUBSTRING(@vCompanyPhone,9,4)	    -- PhoneUnitNumber *
					, 'REP'			                    -- ReturnDocDestinationCD
					, 'PDF'					            -- ReturnDocPackageTypeCD
					, 'EML'							    -- ReturnDocRoutingCD
					, @dTotalLossValuationPercentage    -- TotalLossValuationWarningPercentage
					, @dTotalLossPercentage			    -- TotalLossWarningPercentage
					, @vWarrantyWorkmanship			    -- WarrantyPeriodRefinishMinCD
					, @vWarrantyRefinish			    -- WarrantyPeriodWorkmanshipMinCD
					, 0			                        -- SysLastUserID
					, @now						        -- SysLastUpdatedDate
			)

			IF EXISTS(SELECT * FROM utb_insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID)
			BEGIN
				SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID
			END
			ELSE
			BEGIN
				SELECT 0
			END
		END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewClient' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateNewClient TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateNewClient TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/