-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditWeightUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAuditWeightUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAuditWeightUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This procedure will update the audit weight of a document
*
* PARAMETERS:  
* (I) @DocumentID           The Document ID
* (I) @AuditWeight          The audit weight
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAuditWeightUpdDetail
(
    @DocumentID     bigint,
    @AuditWeight    int
)
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @ProcName           as varchar(50)
    DECLARE @Now                AS datetime
    DECLARE @error              int
    DECLARE @rowcount           int

    SET @ProcName = 'uspAuditWeightUpdDetail'
    SET @Now = CURRENT_TIMESTAMP
    
    -- Check the validity of the document id
    
    IF NOT EXISTS(SELECT DocumentID
                  FROM dbo.utb_document
                  WHERE DocumentID = @DocumentID
                    AND DocumentTypeID in (SELECT DocumentTypeID
                                           FROM dbo.utb_document_type
                                           WHERE EstimateTypeFlag = 1))
    BEGIN
        -- Invalid Document ID. Passed in value does not correspond to a document type of estimate.
        
        RAISERROR('101|%s|"DocumentID"|Invalid Document ID', 16, 1, @ProcName)
        RETURN        
    END
    
    BEGIN TRANSACTION UpdAuditWeightTrans

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    UPDATE dbo.utb_document
    SET AuditWeight = @AuditWeight
    WHERE DocumentID = @DocumentID
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    IF @error <> 0
    BEGIN
       -- update failure
    
        RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION UpdAuditWeightTrans

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    RETURN @rowcount
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditWeightUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAuditWeightUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/