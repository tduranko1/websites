-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspInvoiceInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspInvoiceInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Adds a new charge to the claim billing journal
*
* PARAMETERS:  
* (I) @ClaimAspectID        Claim Aspect being billed for
* (I) @ClientFeeID          The client fee being added
* (I) @IncServices          The services included. A string with ServiceID, ExposureClaimAspectID, ServiceChannel, [ServiceID, ExposureClaimAspectID, ServiceChannel, [...]]
* (I) @SysLastUserID               User billing the charge
* (I) @NotifyEvent          Specifies whether the stored proc should initiate the call to workflow
*
* RESULT SET:
*   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspInvoiceInsDetail
    @ClaimAspectID        udt_std_id_big,
    @DocumentID           udt_std_id_big=null,
    @Amount               varchar(30)=null,
    @DeductibleAmt        varchar(30)=null,
    @TaxTotalAmt          varchar(30)=null,
    @ClientFeeID          udt_std_int=null,
    @Description          udt_std_name=null,
    @IncServices          udt_std_desc_xlong=null,
    @ItemTypeCD           udt_std_cd,
    @NotifyEvent          udt_std_flag = 1,
    @PayeeAddress1        udt_addr_line_1=null,
    @PayeeAddress2        udt_addr_line_2=null,
    @PayeeAddressCity     udt_addr_city=null,
    @PayeeAddressState    udt_addr_state=null,
    @PayeeAddressZip      udt_addr_zip_code=null,
    @PayeeID              udt_std_id_big=null,
    @PayeeName            udt_std_name=null,
    @PayeeTypeCD          udt_std_cd=null,
    @PaymentChannelCD     udt_std_cd = null,
    @SysLastUserID        udt_std_id_big,
    @ClaimAspectServiceChannelID udt_std_id_big = null  --Whether it is provided or not the Primary Service Channel ID will be used
       
        
    
                                 
AS
BEGIN
    -- Declare internal variables

    DECLARE @error                            AS udt_std_int
    DECLARE @rowcount                         AS udt_std_int
    DECLARE @now                              AS udt_std_datetime

    DECLARE @CategoryCD                       AS udt_std_cd
    DECLARE @ClientFeeCode                    AS udt_std_desc_short
    DECLARE @InvoiceID                        AS udt_std_id_big
    DECLARE @EventID                          AS udt_std_id
    DECLARE @HistoryDescription               AS udt_std_desc_long
    DECLARE @ItemizeFlag                      AS udt_std_flag
    DECLARE @InvoiceDescription               AS udt_std_name
    DECLARE @LynxID                           AS udt_std_id_big

    DECLARE @ServiceID                        AS udt_std_int_big
    DECLARE @ExpClaimAspectID                 AS udt_std_int_big
    DECLARE @ServiceChannelCD                 AS udt_std_cd
    DECLARE @DispositionTypeCD                AS udt_std_cd
    DECLARE @CarrierRepName                   AS udt_std_name
    DECLARE @InsuredName                      AS udt_std_name
    DECLARE @FeeItemTypeCD                    AS udt_std_cd
    DECLARE @StatusCD                         AS udt_std_cd
    DECLARE @CreateFlag                       AS udt_std_flag
    DECLARE @ItemAmt                          AS udt_std_money
    DECLARE @ItemDeductibleAmt                AS udt_std_money
    DECLARE @ItemTotalTaxAmt                  AS udt_std_money
    DECLARE @HideFromInvoiceFlag              AS udt_std_flag  --Project:210474 APD Added the variable when we did the code merge M.A.20061117
    DECLARE @ProcName                         AS varchar(30)       -- Used for raise error stmts 
    DECLARE @AdminFeeAmt                      AS decimal(9,2)
    DECLARE @AdminFeeCD                       AS varchar(1)
    DECLARE @AdminFeeID                       AS bigint
    DECLARE @AppliesToCD                      AS varchar(1)
    DECLARE @ShopZipCode                      AS varchar(5)
    DECLARE @ShopZipMSA                       AS varchar(5)
    DECLARE @ShopState                        AS varchar(2)

    SET @ProcName = 'uspInvoiceInsDetail'
    SET @ItemizeFlag = 1


    -- Set empty string parameters to NULL
    IF LEN(LTRIM(RTRIM(@Amount))) = 0 SET @Amount = NULL
    IF LEN(LTRIM(RTRIM(@DeductibleAmt))) = 0 SET @DeductibleAmt = NULL
    IF LEN(LTRIM(RTRIM(@TaxTotalAmt))) = 0 SET @TaxTotalAmt = NULL
    IF LEN(LTRIM(RTRIM(@Description))) = 0 SET @Description = NULL
    IF LEN(LTRIM(RTRIM(@IncServices))) = 0 SET @IncServices = NULL
    IF LEN(LTRIM(RTRIM(@PayeeAddress1))) = 0 SET @PayeeAddress1 = NULL
    IF LEN(LTRIM(RTRIM(@PayeeAddress2))) = 0 SET @PayeeAddress2 = NULL
    IF LEN(LTRIM(RTRIM(@PayeeAddressCity))) = 0 SET @PayeeAddressCity = NULL
    IF LEN(LTRIM(RTRIM(@PayeeAddressState))) = 0 SET @PayeeAddressState = NULL
    IF LEN(LTRIM(RTRIM(@PayeeAddressZip))) = 0 SET @PayeeAddressZip = NULL
    IF LEN(LTRIM(RTRIM(@PayeeName))) = 0 SET @PayeeName = NULL
    IF LEN(LTRIM(RTRIM(@PayeeTypeCD))) = 0 SET @PayeeTypeCD = NULL
    

    -- Set Database options
    
    SET NOCOUNT ON
    SET CONCAT_NULL_YIELDS_NULL OFF
    
    SELECT @FeeItemTypeCD = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD') WHERE Name = 'Fee'                                 
        
    -- Check to make sure a valid Claim Aspect ID was passed in
    
    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END 

   
    
    -- Check to make sure a valid User id was passed in
    IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, 'APD', NULL) = 0)
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END

    -- Check to make sure a valid ItemType was passed in
    IF (NOT EXISTS(SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD') WHERE Code = @ItemTypeCD))
    BEGIN
        -- Invalid Item Type
    
        RAISERROR('101|%s|@ItemTypeCD|%u', 16, 1, @ProcName, @ItemTypeCD)
        RETURN  
    END

    -- Get the Lynx ID and ClaimAspectType/Number for this claim aspect

    SELECT  @LynxID = LynxID
    FROM  dbo.utb_claim_aspect 
    WHERE ClaimAspectID = @ClaimAspectID

    IF (@@ERROR <> 0)
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Check if the claim aspect is open

    IF NOT EXISTS(SELECT * 
                    FROM dbo.ufnUtilityGetClaimEntityList ( @LynxID, 0, 1 )  -- Open exposures
                    WHERE claimAspectID = @ClaimAspectID)
    BEGIN
      IF (@ItemTypeCD = @FeeItemTypeCD)
        RAISERROR('1|Cannot add billing item for the closed exposure. To add the billing item, open the exposure and try again.', 16, 1)
      ELSE  
        RAISERROR('1|Cannot add payment for the closed exposure. To add payment, open the exposure and try again.', 16, 1)
      
      RETURN
    END


    -- Check if the claimAspect has claimant and insured attached.

    SELECT  @rowCount = Count(ClaimAspectID)
    FROM  dbo.utb_claim_aspect_involved cai
          LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
          LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
          LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
    WHERE cai.ClaimAspectID = @ClaimAspectID
      AND cai.EnabledFlag = 1
      AND (irt.Name = 'Claimant' OR irt.Name = 'Insured')
    
    IF @rowCount > 1
    BEGIN
        -- The data for this claim aspect is in invalid state. One claim aspect cannot have insured as well as claimant attached to it.
        
        RAISERROR('99|Invalid Data state for LynxID: %u; claimAspect: %u. This claimAspect has both insured and claimant attached.', 16, 1, @LynxID, @ClaimAspectID)
        RETURN
    END
    
         
    -- Verify APD Data State

    IF NOT EXISTS(SELECT Name FROM dbo.utb_involved_role_type WHERE Name = 'Insured')
    BEGIN
       -- Involved Role Type Not Found
    
        RAISERROR('102|%s|"Insured"|utb_involved_role_type', 16, 1, @ProcName)
        RETURN
    END


    -- Perform Fee Item validation.
    IF (@ItemTypeCD = @FeeItemTypeCD)
    BEGIN
        -- Check to make sure a valid Fee id was passed in

        IF  (@ClientFeeID IS NULL) OR
            (NOT EXISTS(SELECT ClientFeeID FROM dbo.utb_client_fee WHERE ClientFeeID = @ClientFeeID AND InsuranceCompanyID = (SELECT InsuranceCompanyID FROM dbo.utb_claim WHERE LynxID = @LynxID)))
        BEGIN
            -- Invalid Fee ID
        
            RAISERROR('101|%s|@ClientFeeID|%u', 16, 1, @ProcName, @ClientFeeID)
            RETURN
        END

        -- Check to make sure the user selected at least one service
        
        IF  (@IncServices IS NULL)
        BEGIN
            -- Must perform atleast one service.
        
            RAISERROR('101|%s|@IncServices|%u', 16, 1, @ProcName, @IncServices)
            RETURN
        END 
    
    
        -- Get Fee information
        SELECT  @CategoryCD         = CategoryCD,
                @ClientFeeCode      = ClientCode,
                @Description        = Description,
                @ItemAmt            = FeeAmount,
                @ItemizeFlag        = ItemizeFlag,
                @InvoiceDescription = InvoiceDescription,
                @HideFromInvoiceFlag = HideFromInvoiceFlag  --Project:210474 APD Added the column when we did the code merge M.A.20061117
        FROM  dbo.utb_client_fee
        WHERE ClientFeeID = @ClientFeeID

        IF @@ERROR <> 0
        BEGIN
          -- SQL Server Error
        
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END

        -- Create temporary Table to hold the Included Services
        DECLARE @tmpIncServicesInfo TABLE
        (
            value            varchar(50)     NOT NULL
        )
        
        INSERT INTO @tmpIncServicesInfo SELECT value FROM ufnUtilityParseString(@IncServices, ',', 1)
        
        SET @RowCount = @@rowcount
        
        IF (@rowCount % 4) <> 0
        BEGIN
          -- Each services included string should have 4 parts and they do not.
        
            RAISERROR('101|%s|"@IncludedServices"|%u', 16, 1, @ProcName, @IncServices)
            RETURN
        END
    END
    
    -- Payment only validations.
    IF (@ItemTypeCD <> @FeeItemTypeCD)
    BEGIN
        IF @PayeeAddressState IS NOT NULL
        BEGIN
          IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @PayeeAddressState AND EnabledFlag = 1)
          BEGIN
            -- Invalid Address State
              RAISERROR('101|%s|@PayeeAddressState|%s', 16, 1, @ProcName, @PayeeAddressState)
              RETURN
          END
        END

    /*
        IF (@PaymentTypeCD NOT IN (SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_payment', 'PaymentTypeCD')))
        BEGIN
          -- Invalid Payment Type
          RAISERROR('101|%s|@PaymentTypeCD|%s', 16, 1, @ProcName, @PaymentTypeCD)
          RETURN
        END
    */
    
        -- TODO:  Determine PaymentTypeCD and Validate DocumentID
    
        SET @ItemAmt = convert(money, @Amount)
        IF @@ERROR <> 0
        BEGIN
          -- Invalid Payment Amount
          RAISERROR('101|%s|@Amount|%s', 16, 1, @ProcName, @Amount)
          RETURN
        END
    
    
        SELECT @CreateFlag = CreateFlag FROM ufnUtilityGetCRUD(@SysLastUserID, 'Payment', DEFAULT, DEFAULT)

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END 


        IF @CreateFlag = 0 
        BEGIN    
            -- User does not have override right.
          RAISERROR  ('1|Cannot create Payment item.  You have insufficient rights to create a Payment Item.', 16, 1, @ProcName)
          RETURN     
        END
        
        IF @PayeeAddressState IN ('NY')
        BEGIN
          IF @DeductibleAmt IS NULL
          BEGIN
               -- Deductible Amount is required
             RAISERROR  ('1|Deductible Amount is required.', 16, 1, @ProcName)
             RETURN     
          END

          IF @TaxTotalAmt IS NULL
          BEGIN
               -- Total Tax Amount is required
             RAISERROR  ('1|Total Tax Amount is required.', 16, 1, @ProcName)
             RETURN     
          END
        END

        SET @ItemDeductibleAmt = convert(money, @DeductibleAmt)
   
         IF @@ERROR <> 0
         BEGIN
             -- SQL Server Error
             RAISERROR('99|%s', 16, 1, @ProcName)
             RETURN
         END
        
        SET @ItemTotalTaxAmt = convert(money, @TaxTotalAmt)
  
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END
           
    -- Resolve User IDs
    SET @InsuredName = (SELECT TOP 1
                               CASE 
                                  WHEN LEN(LTRIM(RTRIM(i.BusinessName))) > 0 THEN i.BusinessName
                                  WHEN LEN(LTRIM(RTRIM(i.NameLast)) + ',' + LTRIM(RTRIM(i.NameLast))) > 1 THEN LEFT(i.NameLast + ', ' + i.NameFirst, 50)
                                  ELSE NULL
                               END
                        FROM  dbo.utb_claim_aspect ca 
                              LEFT JOIN dbo.utb_claim_aspect_involved cai ON (ca.ClaimAspectID = cai.ClaimAspectID)
                              LEFT JOIN dbo.utb_involved i ON (cai.InvolvedID = i.InvolvedID)
                              LEFT JOIN dbo.utb_involved_role ir ON (i.InvolvedID = ir.InvolvedID)
                              LEFT JOIN dbo.utb_involved_role_type irt ON (ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID)
                        WHERE ca.LynxID = @LynxID
                          AND ca.ClaimAspectTypeID = 0   -- Claim aspect
                          AND cai.EnabledFlag = 1
                          AND irt.Name = 'Insured')
    
    SET @CarrierRepName = (SELECT  TOP 1 LEFT(cu.NameLast + ', ' + cu.NameFirst, 50)
                           FROM  dbo.utb_claim c LEFT JOIN dbo.utb_claim_coverage cc ON (c.LynxID = cc.LynxID)
                                                 LEFT JOIN dbo.utb_user cu ON (c.CarrierRepUserID = cu.UserID)
                           WHERE c.LynxID = @LynxID)
                           
       
    -- Determine the appropriate Status for this item.  Currently fees are not eligible for client pre-authorization.  
    -- Fees will default to 'Authorized' and will be picked up by the next invoicing process for which they are eligible.  
    -- Payments which require client pre-auth (according to utb_client_assignment_type.ClientAuthorizesPaymentFlag) will 
    -- be set to 'Awaiting Client Authorization'.  Otherwise, payments will also be set to 'Authorized'.  ** Note -- when
    -- system computed indemnity payments are implemented, the status, 'Awaiting Internal Authorization' will come into play.
    -- This status will be set on payments where the Claim Rep has overridden the system amount. **  A direct action
    -- by a client or an internal manager will be required to flip an 'Awaiting...' status to 'Authorized'.  When 
    -- authorized items are picked up by an invoicing process, their status will be set to 'Sent to Ingres'.
--    IF (@ItemTypeCD = @FeeItemTypeCD)
--    BEGIN
      -- This is a fee, set status to 'Authorized'.
        /********************************************************************************
        --Project:210474 APD Modified the code below when we did the code merge M.A.20061117
        ********************************************************************************/
      IF @HideFromInvoiceFlag = 1
      BEGIN
          SELECT @StatusCD = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD')
                                  WHERE Name = 'Non-billable'
      END
      ELSE
      BEGIN
          SELECT @StatusCD = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD')
                                  WHERE Name = 'APD'
      END
        /********************************************************************************
        --Project:210474 APD Modified the code above when we did the code merge M.A.20061117
        ********************************************************************************/
--    END
--    ELSE
--    BEGIN
--      IF 1 = (SELECT ISNULL(ClientAuthorizesPaymentFlag, 0)
--              FROM dbo.utb_claim_aspect ca INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
--                                           INNER JOIN dbo.utb_client_assignment_type cat 
--                                                       ON c.InsuranceCompanyID = cat.InsuranceCompanyID
--                                                      AND ca.CurrentAssignmentTypeID = cat.AssignmentTypeID
--              WHERE ca.ClaimAspectID = @ClaimAspectID)
--      BEGIN
--        -- This is a payment that requires client authorization.
--        SELECT @StatusCD = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD')
--                              WHERE Name = 'Awaiting Client Authorization'
--      END
--      ELSE
--      BEGIN
--        -- This is a payment that does NOT require client authorization.
--        SELECT @StatusCD = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD')
--                              WHERE Name = 'Authorized'
--      END
--    END




    --Get the Primary Service Channel for the ClaimAspectID provided Added on 02-01-2007

    select    @ClaimAspectServiceChannelID = ClaimAspectServiceChannelID
    from      utb_Claim_Aspect_Service_Channel
    where     ClaimAspectID = @ClaimAspectID
    and       PrimaryFlag = 1  --It is a primary service Channel
    and       EnabledFlag = 1 -- and it is enabled

    IF  @ClaimAspectServiceChannelID IS NULL
    BEGIN
        -- Invalid Claim Aspect Service Channel ID
    
        RAISERROR('101|%s|No active primary service channel for @LynxID|%u', 16, 1, @ProcName, @LynxID)
        RETURN
    END 

--print '@ItemTypeCD=' + @ItemTypeCD
--print '@FeeItemTypeCD = ' + @FeeItemTypeCD

    IF (@ItemTypeCD = @FeeItemTypeCD)
    BEGIN
        -- Determine the admin fee
        SET @AdminFeeAmt = NULL

        IF EXISTS(SELECT AdminFeeID
                    FROM utb_admin_fee
                    WHERE ClientFeeID = @ClientFeeID
                      AND EnabledFlag = 1)
        BEGIN
            -- Get the admin fee settings
            SELECT @AdminFeeCD = AdminFeeCD,
                   @AdminFeeAmt = AdminFeeAmt
            FROM dbo.ufnClaimGetAdminFee(@ClaimAspectServiceChannelID, @ClientFeeID)

            
            IF  (@AdminFeeCD IS NOT NULL AND @AdminFeeCD <> '')
            BEGIN
                IF  @AdminFeeAmt IS NULL
                BEGIN
                    RAISERROR('1|Admin Fee is not setup correctly.', 16, 1, @ProcName, 129)
                    RETURN
                END
            END 

            IF  @AdminFeeAmt IS NOT NULL
            BEGIN
                IF (@AdminFeeCD IS NULL OR @AdminFeeCD = '')
                BEGIN
                    RAISERROR('1|Admin Fee is not setup correctly.', 16, 1, @ProcName, 129)
                    RETURN
                END
            END 

--print '@AdminFeeCD = ' + @AdminFeeCD
--print '@AdminFeeAmt = ' + convert(varchar, @AdminFeeAmt)

            IF @AdminFeeCD = 'I'
            BEGIN
                -- Insurance company is paying the admin fee. So add it to the fee item
                SET @ItemAmt = @ItemAmt + @AdminFeeAmt
                SET @AdminFeeAmt = NULL
            END
        END
    END

    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP

--print '@ItemAmt = ' + convert(varchar, @ItemAmt)
--return

    -- Begin Insert
    BEGIN TRANSACTION ClaimBillingInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_invoice (ClaimAspectID,
                                 RecordingUserID,
                                 Amount,
                                 CarrierOfficeCode,
                                 CarrierRepName,
                                 ClientFeeCode,
                                 DeductibleAmt,
                                 Description,
                                 DocumentID,
                                 EnabledFlag,
                                 EntryDate,
                                 FeeCategoryCD,
                                 InsuredName,
                                 InvoiceDescription,
                                 ItemizeFlag,
                                 ItemTypeCD,
                                 PayeeAddress1,
                                 PayeeAddress2,
                                 PayeeAddressCity,
                                 PayeeAddressState,
                                 PayeeAddressZip,
                                 PayeeID,
                                 PayeeName,
                                 PayeeTypeCD,
                                 PaymentChannelCD,
                                 StatusCD,
                                 StatusDate,
                                 SystemAmount,
                                 SysLastUserID,
                                 SysLastUpdatedDate,
                                 TaxTotalAmt,
                                 ClaimAspectServiceChannelID,
                                 AdminFeeAmount,
                                 AdminFeeCD)  
                                 
                         VALUES (@ClaimAspectID,
                                 @SysLastUserID,
                                 @ItemAmt,
                                 NULL,     -- CarrierOfficeCode
                                 @CarrierRepName,
                                 @ClientFeeCode,
                                 @ItemDeductibleAmt,
                                 @Description,
                                 @DocumentID,
                                 1,          -- Enabled
                                 @now,
                                 @CategoryCD,
                                 @InsuredName,
                                 @InvoiceDescription,
                                 @ItemizeFlag,
                                 @ItemTypeCD,
                                 @PayeeAddress1,
                                 @PayeeAddress2,
                                 @PayeeAddressCity,
                                 @PayeeAddressState,
                                 @PayeeAddressZip,
                                 @PayeeID,
                                 @PayeeName,
                                 @PayeeTypeCD,
                                 @PaymentChannelCD,
                                 @StatusCD,
                                 @now,
                                 NULL,   -- System Amount -- system calculated indemnity to be implemented later.
                                 @SysLastUserID,
                                 @now,
                                 @ItemTotalTaxAmt,
                                 @ClaimAspectServiceChannelID,
                                 @AdminFeeAmt,
                                 @AdminFeeCD)
                                   
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF (@error <> 0)
    BEGIN
        -- SQL Server Error
    
        RAISERROR('105|%s|utb_invoice', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    ELSE
    BEGIN
        SET @InvoiceID = SCOPE_IDENTITY()
    END

    -- Insert the performed services
    
    DECLARE csrInclServices CURSOR FOR SELECT value FROM @tmpIncServicesInfo
     
    OPEN csrInclServices

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    

    FETCH NEXT FROM csrInclServices INTO @ServiceID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    

    FETCH NEXT FROM csrInclServices INTO @ExpClaimAspectID

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    

    FETCH NEXT FROM csrInclServices INTO @ServiceChannelCD

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END    
    
    FETCH NEXT FROM csrInclServices INTO @DispositionTypeCD

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END


    WHILE @@Fetch_Status = 0
    BEGIN
    
        IF Len(LTrim(RTrim(@ServiceChannelCD))) = 0 SET @ServiceChannelCD = NULL
        IF @DispositionTypeCD = 'null' SET @DispositionTypeCD = NULL
        
        -- insert the included service
        EXEC uspInvoiceServiceInsDetail @InvoiceID, @ClientFeeID, @ServiceID, 
                                         @ExpClaimAspectID, @ServiceChannelCD, @DispositionTypeCD, @SysLastUserID
                                         
                                         
        -- Check error value

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR  ('99|%s|"uspInvoiceServiceInsDetail"', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN                    
        END

        
        -- Update the Exposure's claim aspect record's service channel and disposition
        IF @ServiceChannelCD IS NOT NULL OR @DispositionTypeCD IS NOT NULL
        BEGIN
            UPDATE dbo.utb_claim_aspect_Service_Channel
            SET DispositionTypeCD  = IsNull(@DispositionTypeCD, DispositionTypeCD),
                SysLastUserID      = @SysLastUserID,
                SysLastUpdatedDate = @now
            WHERE ClaimAspectID = @ExpClaimAspectID

            IF @@ERROR <> 0
            BEGIN
               -- SQL Server Error
    
                RAISERROR  ('104|%s|"utb_claim_aspect"', 16, 1, @ProcName)
                ROLLBACK TRANSACTION
                RETURN
            END
            
        END
        

        
        FETCH NEXT FROM csrInclServices INTO @ServiceID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END    

        FETCH NEXT FROM csrInclServices INTO @ExpClaimAspectID

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END    

        FETCH NEXT FROM csrInclServices INTO @ServiceChannelCD

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END 
        
        FETCH NEXT FROM csrInclServices INTO @DispositionTypeCD

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END   
    END    

    CLOSE csrInclServices
    DEALLOCATE csrInclServices

    -- Notify APD of the billing event if we have been instructed to do so.  (If not, it is up to the caller to notify
    -- APD of the event.)

    IF (@NotifyEvent = 1 AND @ItemTypeCD = @FeeItemTypeCD)
    BEGIN
        -- Throw the event here

        SELECT  @EventID = EventID
          FROM  dbo.utb_event
          WHERE Name = 'Fee Billed'
    
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR('99|%s', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
    
        IF @EventID IS NULL
        BEGIN
            -- Reference record Not Found
    
            RAISERROR('102|%s|"Fee Billed"|utb_event', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END
    
    
        SET @HistoryDescription = 'Fee for ' + @Description + ' filed'
    
    
        exec uspWorkflowNotifyEvent @EventID = @EventID,
                                    @ClaimAspectID = @ClaimAspectID,
                                    @Description = @HistoryDescription,
                                    @UserID = @SysLastUserID

        -- Check error value
        
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('107|%s|"Fee Billed"', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END

    
    COMMIT TRANSACTION ClaimBillingInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Create XML Document to return information on the newly created billing record...this is needed to insert the 
    -- corresponding services

    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- New Billing Record
            NULL AS [NewBilling!2!ClientBillingID]
            
    UNION ALL
    
    SELECT  2,
            1,
            NULL,
            @InvoiceID


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END



GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspInvoiceInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
