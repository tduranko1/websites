-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLProcedureGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspFNOLProcedureGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspFNOLProcedureGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Retrieves a list of procedure names, the caller may optionally request specifically a single procedure level.
*
* PARAMETERS:  
* (I) @ProcedureLevel       Optional procedure level to request
*
* RESULT SET:   ProcedureID,
*               ProcedureLevelID,
*               Name
*   
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspFNOLProcedureGetList
    @ProcedureLevel         udt_std_int_tiny    = NULL
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @ExecStmt   varchar(1000)
    
    DECLARE @ProcName   varchar(20)
    
    SET @ProcName = 'uspFNOLProcedureGetList'
    
    
    -- Build the dynamic Select
    
    SET @ExecStmt = 'SELECT FNOLProcedureID AS ProcedureID, ' +
                           'ProcedureLevelID, ' +
                           'Name AS ProcName ' +
                      'FROM dbo.utb_fnol_procedure '
                                            
    IF @ProcedureLevel IS NOT NULL
    BEGIN
        SET @ExecStmt = @ExecStmt + 'WHERE ProcedureLevelID = ' + Convert(varchar(5), @ProcedureLevel) + ' '
    END
    
    SET @ExecStmt = @ExecStmt + 'ORDER BY ProcedureLevelID, ExecuteOrder'
    
    
    -- Exec it
    
    Exec(@ExecStmt)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END            
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLProcedureGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspFNOLProcedureGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/