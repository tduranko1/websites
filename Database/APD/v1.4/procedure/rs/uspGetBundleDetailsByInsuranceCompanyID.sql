-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetBundleDetailsByInsuranceCompanyID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetBundleDetailsByInsuranceCompanyID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetBundleDetailsByInsuranceCompanyID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets existing bundling information by InsuranceCompanyID
*
* PARAMETERS:  
*			@iInsuranceCompanyID
* RESULT SET:
*   All data related to client bundling details by insurance company ID
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetBundleDetailsByInsuranceCompanyID
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 
    DECLARE @ProcName AS varchar(30)

    SET @ProcName = 'uspGetBundleDetailsByInsuranceCompanyID'

    -- Set Database options
    
    SET NOCOUNT ON

    -- Check to make sure a valid Insurance Company ID was passed in
    IF (@iInsuranceCompanyID > 0) AND (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iInsuranceCompanyID|%u', 16, 1, @ProcName, @iInsuranceCompanyID)
        RETURN
    END

	SELECT
		b.BundlingID
		, udt.DocumentTypeID
		, b.MessageTemplateID 
		, udt.[Name]
		, b.EnabledFlag
		, mt.[Description]
		, ISNULL(bdt.DirectionalCD,'') AS DirectionalCD
		, bdt.DirectionToPayFlag
		, bdt.DuplicateFlag
		, bdt.EstimateTypeCD
		, bdt.FinalEstimateFlag
		, bdt.MandatoryFlag
		, bdt.SelectionOrder
		, bdt.VANFlag
		, bdt.WarrantyFlag
	FROM 
		utb_message_template mt 
		INNER JOIN utb_bundling b 
		ON b.MessageTemplateID = mt.MessageTemplateID 
		INNER JOIN utb_client_bundling cb
		ON cb.BundlingID = b.BundlingID 
		LEFT JOIN utb_bundling_document_type bdt
		ON bdt.BundlingID = b.BundlingID
		INNER JOIN dbo.utb_document_type udt
		ON udt.DocumentTypeID = bdt.DocumentTypeID
	WHERE 
		cb.InsuranceCompanyID = @iInsuranceCompanyID
	ORDER BY
		b.[Name]
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetBundleDetailsByInsuranceCompanyID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetBundleDetailsByInsuranceCompanyID TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetBundleDetailsByInsuranceCompanyID TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/