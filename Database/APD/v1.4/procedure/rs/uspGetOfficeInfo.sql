-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetOfficeInfo' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetOfficeInfo 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetOfficeInfo
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all client office information from the utb_office table
*
* PARAMETERS:  
*				InsuranceCompanyID 
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetOfficeInfo
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT
		OfficeID
		, InsuranceCompanyID
		, EnabledFlag
		, ISNULL(Address1,'') Address1
		, ISNULL(Address2,'') Address2
		, ISNULL(AddressCity,'') AddressCity
		, ISNULL(AddressState,'') AddressState
		, ISNULL(AddressZip,'') AddressZip
		, ISNULL(CCEmailAddress,'') CCEmailAddress
		, ISNULL(ClientOfficeID,'') ClientOfficeID
		, ISNULL(EnabledFlag,'') EnabledFlag 
		, ISNULL(FaxAreaCode,'') FaxAreaCode
		, ISNULL(FaxExchangeNumber,'') FaxExchangeNumber
		, ISNULL(FaxExtensionNumber,'') FaxExtensionNumber
		, ISNULL(FaxUnitNumber,'') FaxUnitNumber
		, ISNULL(MailingAddress1,'') MailingAddress1
		, ISNULL(MailingAddress2,'') MailingAddress2
		, ISNULL(MailingAddressCity,'') MailingAddressCity
		, ISNULL(MailingAddressState,'') MailingAddressState
		, ISNULL(MailingAddressZip,'') MailingAddressZip
		, ISNULL([Name],'') OfficeName                                               
		, ISNULL(PhoneAreaCode,'') PhoneAreaCode
		, ISNULL(PhoneExchangeNumber,'') PhoneExchangeNumber
		, ISNULL(PhoneExtensionNumber,'') PhoneExtensionNumber
		, ISNULL(PhoneUnitNumber,'') PhoneUnitNumber
		, ISNULL(ReturnDocDestinationValue,'') ReturnDocDestinationValue
		, ISNULL(ReturnDocEmailAddress,'') ReturnDocEmailAddress
		, ISNULL(ReturnDocFaxAreaCode,'') ReturnDocFaxAreaCode
		, ISNULL(ReturnDocFaxExchangeNumber,'') ReturnDocFaxExchangeNumber
		, ISNULL(ReturnDocFaxExtensionNumber,'') ReturnDocFaxExtensionNumber
		, ISNULL(ReturnDocFaxUnitNumber,'') ReturnDocFaxUnitNumber
		, ISNULL(SysLastUserID ,0) SysLastUserID
		, SysLastUpdatedDate
	FROM
		 dbo.utb_office
	WHERE
		InsuranceCompanyID = @iInsuranceCompanyID
	ORDER BY
		[Name] 
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetOfficeInfo' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetOfficeInfo TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetOfficeInfo TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/