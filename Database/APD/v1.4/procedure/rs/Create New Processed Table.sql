/****** Object:  Table [dbo].[utb_HQRepl_History]    Script Date: 7/21/2016 9:34:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
-- DROP TABLE utb_hyperquest_history_processed

CREATE TABLE [dbo].[utb_hyperquest_history_processed](
	[LogID] [dbo].[udt_std_id_big] NOT NULL,
	[LynxID] [dbo].[udt_std_int_big] NOT NULL,
	[EventID] [dbo].[udt_std_int_small] NULL,
	[ClaimAspectIDClaim] [dbo].[udt_std_id_big] NULL,
	[ClaimAspectIDVehicle] [dbo].[udt_std_id_big] NULL,
	[ClaimAspectServiceChannelID] [dbo].[udt_std_id_big] NULL,
	[CompletedDate] [dbo].[udt_std_datetime] NOT NULL,
	[ClaimAspectNumber] [dbo].[udt_std_int] NOT NULL,
	[SysLastUserID] [dbo].[udt_std_id] NOT NULL,
	[SysLastUpdatedDate] [dbo].[udt_sys_last_updated_date] NOT NULL,
 CONSTRAINT [upk_history_processed_log] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [ufg_workflow]
) ON [ufg_workflow]

GO

SET ANSI_PADDING ON
GO


