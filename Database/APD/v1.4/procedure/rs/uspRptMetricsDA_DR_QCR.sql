-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMetricsDA_DR_QCR' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptMetricsDA_DR_QCR 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptMetricsDA_DR_QCR
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the DA DR metric data
*
* PARAMETERS:  
*
* RESULT SET:
*   Returns the DA DR metric data
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptMetricsDA_DR_QCR
AS
BEGIN
   set nocount on
   set ANSI_WARNINGS off
   /******************************************************************************
   * DA/DR QCR metrics
   ******************************************************************************/

   DECLARE @RptMonthStart as datetime
   DECLARE @RptYesterdayStart as datetime
   DECLARE @RptYesterdayEnd as datetime
   DECLARE @now as datetime
   DECLARE @debug as bit

   DECLARE @EstimateSummaryIDRepairTotal as int
   DECLARE @EstimateSummaryIDPartsTaxable as int
   DECLARE @EstimateSummaryIDOEMParts as int
   DECLARE @DocumentTypeIDEstimate as int
   DECLARE @DocumentTypeIDSupplement as int

   DECLARE @tmpAuditedDocuments TABLE (
      ClaimAspectServiceChannelID   bigint,
      SupplementExists              bit
   )

   DECLARE @tmpClosedNew TABLE (
      LynxID                        bigint,
      ClaimAspectNumber             int,
      ClaimAspectID                 bigint,
      ClaimAspectServiceChannelID   bigint,
      ServiceChannelCD              varchar(2),
      AssignmentTypeName            varchar(50),
      StatusID                      int,
      StatusName                    varchar(50),
      OriginalEstimateAmount        decimal(9, 2),
      AuditedEstimateAmount         decimal(9, 2),
      VANAuditedEstimateAmount      decimal(9, 2),
      VANAuditedOEMParts            decimal(9, 2),
      VANAuditedPartsTaxable        decimal(9, 2),
      AnalystUserID                 bigint,
      AnalystName                   varchar(50),
      CreatedDate                   datetime,
      OriginalClosedDate            datetime,
      SupplementCreatedDate         datetime,
      SupplementSeqNumberOriginal   int,
      SCClosedDate                  datetime,
      SupplementSeqNumberAudited    int,
      CTBusinessHours               int,
      SupplementCount               int,
      SCStatus                      varchar(50)
   )

   DECLARE @tmpAnalyst TABLE (
      AnalystUserID     bigint,
      AnalystName       varchar(50)
   )

   DECLARE @tmpReport TABLE (
      idx                int,
      Metric             varchar(50),
      DA_Daily_Est       int,
      DA_Daily_Sup       int,
      DA_MTD_Est         int,
      DA_MTD_Sup         int,
      DR_Daily_Est       int,
      DR_Daily_Sup       int,
      DR_MTD_Est         int,
      DR_MTD_Sup         int,
      DA_Daily_Amt       decimal(9, 2),
      DA_MTD_Amt         decimal(9, 2),
      DR_Daily_Amt       decimal(9, 2),
      DR_MTD_Amt         decimal(9, 2)
   )


   SET @now = CURRENT_TIMESTAMP
   SET @RptMonthStart = convert(varchar, month(@now)) + '/1/' + convert(varchar, year(@now))
   IF DAY(@now) = 1 
   BEGIN
      SET @RptMonthStart = DATEADD(month, -1, @RptMonthStart)
   END
   SET @RptYesterdayStart = dateadd(day, -1, convert(datetime, convert(varchar, @now, 101)))
   SET @RptYesterdayEnd = dateadd(second, -1, convert(datetime, convert(varchar, @now, 101)))
   SET @debug = 1


   -- select @now, @RptMonthStart, @RptYesterdayStart, @RptYesterdayEnd

   SELECT @EstimateSummaryIDRepairTotal = EstimateSummaryTypeID
   FROM utb_estimate_summary_type
   WHERE CategoryCD = 'TT'
     AND Name = 'RepairTotal'
     
   SELECT @EstimateSummaryIDPartsTaxable = EstimateSummaryTypeID
   FROM utb_estimate_summary_type
   WHERE CategoryCD = 'PC'
     AND Name = 'PartsTaxable'

   SELECT @EstimateSummaryIDOEMParts = EstimateSummaryTypeID
   FROM utb_estimate_summary_type
   WHERE Name = 'OEMParts'
     AND CategoryCD = 'OT'

     
   SELECT @DocumentTypeIDEstimate = DocumentTypeID
   FROM utb_document_type
   WHERE EstimateTypeFlag = 1
     AND EnabledFlag = 1
     AND Name = 'Estimate'

   SELECT @DocumentTypeIDSupplement = DocumentTypeID
   FROM utb_document_type
   WHERE EstimateTypeFlag = 1
     AND EnabledFlag = 1
     AND Name = 'Supplement'

   INSERT INTO @tmpAuditedDocuments
   SELECT ClaimAspectServiceChannelID,
          0
   FROM dbo.utb_claim_aspect_service_channel
   WHERE ServiceChannelCD in ('DA', 'DR')
   --  AND OriginalCompleteDate between @RptYesterdayStart and @RptYesterdayEnd
     AND OriginalCompleteDate between @RptMonthStart and @RptYesterdayEnd
     
   INSERT INTO @tmpAuditedDocuments
   SELECT distinct cascd.ClaimAspectServiceChannelID,
          CASE 
            WHEN d.SupplementSeqNumber > 0 THEN 1
            ELSE 0
          END
   FROM utb_document d
   LEFT JOIN utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
   LEFT JOIN utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
   WHERE d.DocumentTypeID in (3, 10)
     AND d.EstimateTypeCD = 'A'
     AND d.DuplicateFlag = 0
     AND d.EnabledFlag = 1
   --  AND d.CreatedDate between @RptYesterdayStart and @RptYesterdayEnd
     AND d.CreatedDate between @RptMonthStart and @RptYesterdayEnd
     AND cascd.ClaimAspectServiceChannelID not in (select ClaimAspectServiceChannelID from @tmpAuditedDocuments)
   order by 1, 2


   INSERT INTO @tmpClosedNew
   SELECT ca.LynxID,
          ca.ClaimAspectNumber,
          ca.ClaimAspectID,
          casc.ClaimAspectServiceChannelID,
          casc.ServiceChannelCD,
          at.Name,
          cas.StatusID,
          case
            when casc.OriginalCompleteDate is not null then 'Complete'
            else ''
          end,
          (SELECT top 1 AgreedExtendedAmt
             FROM utb_claim_aspect_service_channel_document cascd
             LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
             LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
             LEFT JOIN utb_estimate_summary es on d.DocumentID = es.DocumentID
             where cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               and d.DocumentTypeID = @DocumentTypeIDEstimate
               and d.EstimateTypeCD = 'O'
               and d.EnabledFlag = 1
               and es.EstimateSummaryTypeID = @EstimateSummaryIDRepairTotal
             order by d.SupplementSeqNumber asc, ds.VANFlag desc, d.CreatedDate asc
          ),
          (SELECT top 1 AgreedExtendedAmt
             FROM utb_claim_aspect_service_channel_document cascd
             LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
             LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
             LEFT JOIN utb_estimate_summary es on d.DocumentID = es.DocumentID
             where cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               and d.DocumentTypeID = @DocumentTypeIDEstimate
               and d.EstimateTypeCD = 'A'
               --and d.DuplicateFlag = 0
               and d.EnabledFlag = 1
               and es.EstimateSummaryTypeID = @EstimateSummaryIDRepairTotal
             order by d.SupplementSeqNumber asc, ds.VANFlag asc, d.CreatedDate asc
          ),
          (SELECT top 1 AgreedExtendedAmt
             FROM utb_claim_aspect_service_channel_document cascd
             LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
             LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
             LEFT JOIN utb_estimate_summary es on d.DocumentID = es.DocumentID
             where cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               and d.DocumentTypeID = @DocumentTypeIDEstimate
               and d.EstimateTypeCD = 'A'
               and d.DuplicateFlag = 0
               and d.EnabledFlag = 1
               and es.EstimateSummaryTypeID = @EstimateSummaryIDRepairTotal
             order by d.SupplementSeqNumber asc, ds.VANFlag desc, d.CreatedDate asc
          ),
          (SELECT top 1 AgreedExtendedAmt
             FROM utb_claim_aspect_service_channel_document cascd
             LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
             LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
             LEFT JOIN utb_estimate_summary es on d.DocumentID = es.DocumentID
             where cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               and d.DocumentTypeID = @DocumentTypeIDEstimate
               and d.EstimateTypeCD = 'A'
               and d.DuplicateFlag = 0
               and d.EnabledFlag = 1
               and es.EstimateSummaryTypeID = @EstimateSummaryIDOEMParts
             order by d.SupplementSeqNumber asc, ds.VANFlag desc, d.CreatedDate asc
          ),
          (SELECT top 1 AgreedExtendedAmt
             FROM utb_claim_aspect_service_channel_document cascd
             LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
             LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
             LEFT JOIN utb_estimate_summary es on d.DocumentID = es.DocumentID
             where cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               and d.DocumentTypeID = @DocumentTypeIDEstimate
               and d.EstimateTypeCD = 'A'
               and d.DuplicateFlag = 0
               and d.EnabledFlag = 1
               and es.EstimateSummaryTypeID = @EstimateSummaryIDPartsTaxable
             order by d.SupplementSeqNumber asc, ds.VANFlag desc, d.CreatedDate asc
          ),
          ca.AnalystUserID,
          ua.NameLast + ', ' + ua.NameFirst,
          casc.CreatedDate,
          --convert(datetime, convert(varchar, casc.CreatedDate, 101) + ' ' + convert(varchar, datepart(hour, casc.CreatedDate)) + ':00'),
          casc.OriginalCompleteDate,
          --convert(datetime, convert(varchar, casc.OriginalCompleteDate, 101) + ' ' + convert(varchar, datepart(hour, casc.OriginalCompleteDate)) + ':00'),
          (select top 1 d.CreatedDate --convert(datetime, convert(varchar, d.CreatedDate, 101) + ' ' + convert(varchar, datepart(hour, d.CreatedDate)) + ':00')
           from utb_document d
           left join utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
           where cascd.ClaimAspectServiceChannelID = tmp.ClaimAspectServiceChannelID
             and d.DocumentTypeID = 10
             and d.EstimateTypeCD = 'O'
             and d.DuplicateFlag = 0
           order by d.SupplementSeqNumber desc
          ),
          (select top 1 d.SupplementSeqNumber
           from utb_document d
           left join utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
           where cascd.ClaimAspectServiceChannelID = tmp.ClaimAspectServiceChannelID
             and d.DocumentTypeID = 10
             and d.EstimateTypeCD = 'O'
             and d.DuplicateFlag = 0
           order by d.SupplementSeqNumber desc
          ),
          (select top 1 d.CreatedDate --convert(datetime, convert(varchar, d.CreatedDate, 101) + ' ' + convert(varchar, datepart(hour, d.CreatedDate)) + ':00')
           from utb_document d
           left join utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
           where cascd.ClaimAspectServiceChannelID = tmp.ClaimAspectServiceChannelID
             and d.DocumentTypeID = 10
             and d.EstimateTypeCD = 'A'
             and d.DuplicateFlag = 0
           order by d.SupplementSeqNumber desc
          ),
          (select top 1 d.SupplementSeqNumber
           from utb_document d
           left join utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
           where cascd.ClaimAspectServiceChannelID = tmp.ClaimAspectServiceChannelID
             and d.DocumentTypeID = 10
             and d.EstimateTypeCD = 'A'
             and d.DuplicateFlag = 0
           order by d.SupplementSeqNumber desc
          ),
          dbo.ufnUtilityTrueBusinessHoursDiff(casc.CreatedDate, casc.OriginalCompleteDate),
          --NULL,
          (SELECT count(d.DocumentID)
             FROM utb_claim_aspect_service_channel_document cascd
             LEFT JOIN utb_document d on cascd.DocumentID = d.DocumentID
             where cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               and d.DocumentTypeID = @DocumentTypeIDSupplement
               and d.EnabledFlag = 1
          ),
          s.Name
   FROM @tmpAuditedDocuments tmp
   LEFT JOIN utb_claim_aspect_service_channel casc on tmp.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
   LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
   LEFT JOIN utb_claim c on ca.LynxID = c.LynxID
   LEFT JOIN utb_assignment_type at on ca.InitialAssignmentTypeID = at.AssignmentTypeID
   LEFT JOIN utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
   LEFT JOIN utb_claim_aspect_status cas on ca.ClaimAspectID = cas.ClaimAspectID and cas.StatusTypeCD = 'SC'
   LEFT JOIN utb_status s on cas.StatusID = s.StatusID
   LEFT JOIN utb_user ua on ca.AnalystUserID = ua.UserID
   WHERE cas.ServiceChannelCD = casc.ServiceChannelCD
     AND ca.ClaimAspectTypeID = 9
     AND casc.ServiceChannelCD in ('DA', 'DR')
     AND c.DemoFlag = 0
     AND i.DemoFlag = 0
     --AND ca.AnalystUserID = 999

   /*update @tmpClosedNew
   set CTBusinessHours = dbo.ufnUtilityTrueBusinessHoursDiff(CreatedDate, OriginalClosedDate)*/
   
   update @tmpClosedNew
   set CTBusinessHours = NULL
   where SupplementCount > 0

   update @tmpClosedNew
   set CTBusinessHours = dbo.ufnUtilityTrueBusinessHoursDiff(SupplementCreatedDate, SCClosedDate) 
   where SupplementCount > 0
     and SupplementCreatedDate is not null
     and SCClosedDate is not null
     and SupplementSeqNumberOriginal = SupplementSeqNumberAudited

     
   insert into @tmpAnalyst
   select distinct AnalystUserID, AnalystName
   from @tmpClosedNew
   order by AnalystName


   -- DA/DR QCR closed header
   insert into @tmpReport (
      idx,
      Metric,
      DA_MTD_Est,
      DA_MTD_Sup,
      DR_MTD_Est,
      DR_MTD_Sup
   )
   select 1,
          'Count of Closed New/Supplements Finalized',
          isNull((select count(LynxID)
                  from @tmpClosedNew
                  WHERE ServiceChannelCD = 'DA'
                    AND AssignmentTypeName = 'Demand Estimate Audit'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'), ''),
          isNull((select count(LynxID)
                  from @tmpClosedNew
                  WHERE ServiceChannelCD = 'DA'
                    AND AssignmentTypeName = 'Demand Estimate Audit'
                    AND SupplementCount > 0
                    AND SCStatus = 'Complete'), ''),
          isNull((select count(LynxID)
                  from @tmpClosedNew
                  WHERE ServiceChannelCD = 'DR'
                    AND AssignmentTypeName = 'Adverse Subro Desk Review'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'), ''),
          isNull((select count(LynxID)
                  from @tmpClosedNew
                  WHERE ServiceChannelCD = 'DR'
                    AND AssignmentTypeName = 'Adverse Subro Desk Review'
                    AND SupplementCount > 0
                    AND SCStatus = 'Complete'), '')

   insert into @tmpReport (
      idx,
      Metric
   ) values (
      1,
      ''
   )

   -- DA/DR QCR closed 
   INSERT INTO @tmpReport (
      idx,
      Metric,
      DA_Daily_Est,
      DA_Daily_Sup,
      DR_Daily_Est,
      DR_Daily_Sup
   )
   SELECT 1,
          AnalystName,
          isNull((select count(tcn1.LynxID)
                  from @tmpClosedNew tcn1
                  WHERE tcn1.AnalystUserID = ta.AnalystUserID
                    AND tcn1.ServiceChannelCD = 'DA'
                    AND tcn1.AssignmentTypeName = 'Demand Estimate Audit'
                    AND tcn1.SupplementCount = 0
                    AND tcn1.OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd
                    AND tcn1.StatusName = 'Complete'), ''),
          isNull((select count(tcn1.LynxID)
                  from @tmpClosedNew tcn1
                  WHERE tcn1.AnalystUserID = ta.AnalystUserID
                    AND tcn1.ServiceChannelCD = 'DA'
                    AND tcn1.AssignmentTypeName = 'Demand Estimate Audit'
                    AND tcn1.SupplementCreatedDate between @RptYesterdayStart and @RptYesterdayEnd
                    AND tcn1.SupplementCount > 0
                    AND tcn1.SCStatus = 'Complete'), ''),
          isNull((select count(tcn1.LynxID)
                  from @tmpClosedNew tcn1
                  WHERE tcn1.AnalystUserID = ta.AnalystUserID
                    AND tcn1.ServiceChannelCD = 'DR'
                    AND tcn1.AssignmentTypeName = 'Adverse Subro Desk Review'
                    AND tcn1.SupplementCount = 0
                    AND tcn1.OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd
                    AND tcn1.StatusName = 'Complete'), ''),
          isNull((select count(tcn1.LynxID)
                  from @tmpClosedNew tcn1
                  WHERE tcn1.AnalystUserID = ta.AnalystUserID
                    AND tcn1.ServiceChannelCD = 'DR'
                    AND tcn1.AssignmentTypeName = 'Adverse Subro Desk Review'
                    AND tcn1.SupplementCount > 0
                    AND tcn1.SupplementCreatedDate between @RptYesterdayStart and @RptYesterdayEnd
                    AND tcn1.SCStatus = 'Complete'), '')
   from @tmpAnalyst ta


   -- DA/DR Average % Savings header
   insert into @tmpReport (
      idx,
      Metric,
      DA_Daily_Amt,
      DA_MTD_Amt,
      DR_Daily_Amt,
      DR_MTD_Amt
   )
   select 2,
          'Average % Savings (Estimates)',
          (select convert(decimal(9, 2), AVG(case
                                                when (OriginalEstimateAmount is not null) and (AuditedEstimateAmount is not null) and (OriginalEstimateAmount >= AuditedEstimateAmount) and (OriginalEstimateAmount > 0) then ((OriginalEstimateAmount - AuditedEstimateAmount)/OriginalEstimateAmount) * 100
                                                else 0
                                              end))
           from @tmpClosedNew
           where ServiceChannelCD = 'DA'
             AND AssignmentTypeName = 'Demand Estimate Audit'
             AND SupplementCount = 0
             AND OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd
             AND OriginalEstimateAmount is not null
             AND AuditedEstimateAmount is not null
             AND OriginalEstimateAmount > 0
             AND OriginalEstimateAmount >= AuditedEstimateAmount),
          (select convert(decimal(9, 2), AVG(case
                                                when (OriginalEstimateAmount is not null) and (AuditedEstimateAmount is not null) and (OriginalEstimateAmount >= AuditedEstimateAmount) and (OriginalEstimateAmount > 0) then ((OriginalEstimateAmount - AuditedEstimateAmount)/OriginalEstimateAmount) * 100
                                                else 0
                                              end))
           from @tmpClosedNew
           where ServiceChannelCD = 'DA'
             AND AssignmentTypeName = 'Demand Estimate Audit'
             AND SupplementCount = 0
             AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd
             AND OriginalEstimateAmount is not null
             AND AuditedEstimateAmount is not null
             AND OriginalEstimateAmount > 0
             AND OriginalEstimateAmount >= AuditedEstimateAmount),
          (select convert(decimal(9, 2), AVG(case
                                                when (OriginalEstimateAmount is not null) and (AuditedEstimateAmount is not null) and (OriginalEstimateAmount >= AuditedEstimateAmount) and (OriginalEstimateAmount > 0) then (OriginalEstimateAmount - AuditedEstimateAmount)/OriginalEstimateAmount
                                                else 0
                                              end))
           from @tmpClosedNew
           where ServiceChannelCD = 'DR'
             AND AssignmentTypeName = 'Adverse Subro Desk Review'
             AND SupplementCount = 0
             AND OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd
             AND OriginalEstimateAmount is not null
             AND AuditedEstimateAmount is not null
             AND OriginalEstimateAmount > 0
             AND OriginalEstimateAmount >= AuditedEstimateAmount),
          (select convert(decimal(9, 2), AVG(case
                                                when (OriginalEstimateAmount is not null) and (AuditedEstimateAmount is not null) and (OriginalEstimateAmount >= AuditedEstimateAmount) and (OriginalEstimateAmount > 0) then (OriginalEstimateAmount - AuditedEstimateAmount)/OriginalEstimateAmount
                                                else 0
                                              end))
           from @tmpClosedNew
           where ServiceChannelCD = 'DR'
             AND AssignmentTypeName = 'Adverse Subro Desk Review'
             AND SupplementCount = 0
             AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd
             AND OriginalEstimateAmount is not null
             AND AuditedEstimateAmount is not null
             AND OriginalEstimateAmount > 0
             AND OriginalEstimateAmount >= AuditedEstimateAmount)

   insert into @tmpReport (
      idx,
      Metric
   ) values (
      2,
      ''
   )
                    
   -- DA/DR Average % Savings 
   INSERT INTO @tmpReport (
      idx,
      Metric,
      DA_Daily_Amt,
      DA_MTD_Amt,
      DR_Daily_Amt,
      DR_MTD_Amt
   )
   SELECT 2,
           AnalystName,
          (select convert(decimal(9, 2), AVG(case
                                                when (OriginalEstimateAmount is not null) and (AuditedEstimateAmount is not null) and (OriginalEstimateAmount >= AuditedEstimateAmount) and (OriginalEstimateAmount > 0) then ((OriginalEstimateAmount - AuditedEstimateAmount)/OriginalEstimateAmount) * 100
                                                else 0
                                              end))
                    from @tmpClosedNew tcn
                    where tcn.ServiceChannelCD = 'DA'  
                      AND tcn.AssignmentTypeName = 'Demand Estimate Audit'      
                      AND tcn.SupplementCount = 0
                      AND tcn.AnalystUserID = ta.AnalystUserID
                      AND OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd
                      AND OriginalEstimateAmount is not null
                      AND AuditedEstimateAmount is not null
                      AND OriginalEstimateAmount > 0
                      AND OriginalEstimateAmount >= AuditedEstimateAmount),
          (select convert(decimal(9, 2), AVG(case
                                                when (OriginalEstimateAmount is not null) and (AuditedEstimateAmount is not null) and (OriginalEstimateAmount >= AuditedEstimateAmount) and (OriginalEstimateAmount > 0) then ((OriginalEstimateAmount - AuditedEstimateAmount)/OriginalEstimateAmount) * 100
                                                else 0
                                              end))
           from @tmpClosedNew tcn
           where tcn.ServiceChannelCD = 'DA'  
             AND tcn.AssignmentTypeName = 'Demand Estimate Audit'        
             AND tcn.SupplementCount = 0
             AND tcn.AnalystUserID = ta.AnalystUserID
             AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd
             AND OriginalEstimateAmount is not null
             AND AuditedEstimateAmount is not null
             AND OriginalEstimateAmount > 0
             AND OriginalEstimateAmount >= AuditedEstimateAmount),
          (select convert(decimal(9, 2), AVG(case
                                             when (OriginalEstimateAmount is not null) and (AuditedEstimateAmount is not null) and (OriginalEstimateAmount >= AuditedEstimateAmount) and (OriginalEstimateAmount > 0) then (OriginalEstimateAmount - AuditedEstimateAmount)/OriginalEstimateAmount
                                             else 0
                                           end))
           from @tmpClosedNew tcn
           where tcn.ServiceChannelCD = 'DR'
             AND tcn.AssignmentTypeName = 'Adverse Subro Desk Review'        
             AND tcn.SupplementCount = 0
             AND tcn.AnalystUserID = ta.AnalystUserID
             AND OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd
             AND OriginalEstimateAmount is not null
             AND AuditedEstimateAmount is not null
             AND OriginalEstimateAmount > 0
             AND OriginalEstimateAmount >= AuditedEstimateAmount),
          (select convert(decimal(9, 2), AVG(case
                                                when (OriginalEstimateAmount is not null) and (AuditedEstimateAmount is not null) and (OriginalEstimateAmount >= AuditedEstimateAmount) and (OriginalEstimateAmount > 0) then (OriginalEstimateAmount - AuditedEstimateAmount)/OriginalEstimateAmount
                                                else 0
                                              end))
           from @tmpClosedNew tcn
           where tcn.ServiceChannelCD = 'DR'
             AND tcn.AssignmentTypeName = 'Adverse Subro Desk Review'
             AND tcn.SupplementCount = 0
             AND tcn.AnalystUserID = ta.AnalystUserID
             AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd
             AND OriginalEstimateAmount is not null
             AND AuditedEstimateAmount is not null
             AND OriginalEstimateAmount > 0
             AND OriginalEstimateAmount >= AuditedEstimateAmount)

   from @tmpAnalyst ta


   -- Average Cycle Time (Estimates) in Business Hours
   insert into @tmpReport (
      idx,
      Metric,
      DA_Daily_Amt,
      DA_MTD_Amt,
      DR_Daily_Amt,
      DR_MTD_Amt
   )
   select 3,
          'Average Cycle Time (Estimates) in Business Hours',
          (select ROUND(AVG(CTBusinessHours * 1.0), 2)
                  from @tmpClosedNew
                  WHERE ServiceChannelCD = 'DA'
                    AND AssignmentTypeName = 'Demand Estimate Audit'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'
                    AND OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd),
          (select ROUND(AVG(CTBusinessHours * 1.0), 2)
                  from @tmpClosedNew
                  WHERE ServiceChannelCD = 'DA'
                    AND AssignmentTypeName = 'Demand Estimate Audit'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'
                    AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd),
          (select ROUND(AVG(CTBusinessHours * 1.0), 2)
                  from @tmpClosedNew
                  WHERE ServiceChannelCD = 'DR'
                    AND AssignmentTypeName = 'Adverse Subro Desk Review'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'
                    AND OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd),
          (select ROUND(AVG(CTBusinessHours * 1.0), 2)
                  from @tmpClosedNew
                  WHERE ServiceChannelCD = 'DR'
                    AND AssignmentTypeName = 'Adverse Subro Desk Review'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'
                    AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd)

   insert into @tmpReport (
      idx,
      Metric
   ) values (
      3,
      ''
   )

   -- Average Cycle Time (Estimates) in Business Hours for the QCR
   INSERT INTO @tmpReport (
      idx,
      Metric,
      DA_Daily_Amt,
      DA_MTD_Amt,
      DR_Daily_Amt,
      DR_MTD_Amt
   )
   SELECT 3,
          AnalystName,
          (select ROUND(AVG(CTBusinessHours * 1.0), 2)
                  from @tmpClosedNew tcn
                  WHERE ServiceChannelCD = 'DA'
                    AND AssignmentTypeName = 'Demand Estimate Audit'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'
                    AND tcn.AnalystUserID = ta.AnalystUserID
                    AND OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd),
          (select ROUND(AVG(CTBusinessHours * 1.0), 2)
                  from @tmpClosedNew tcn
                  WHERE ServiceChannelCD = 'DA'
                    AND AssignmentTypeName = 'Demand Estimate Audit'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'
                    AND tcn.AnalystUserID = ta.AnalystUserID
                    AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd),
          (select ROUND(AVG(CTBusinessHours * 1.0), 2)
                  from @tmpClosedNew tcn
                  WHERE ServiceChannelCD = 'DR'
                    AND AssignmentTypeName = 'Adverse Subro Desk Review'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'
                    AND tcn.AnalystUserID = ta.AnalystUserID
                    AND OriginalClosedDate between @RptYesterdayStart and @RptYesterdayEnd),
          (select ROUND(AVG(CTBusinessHours * 1.0), 2)
                  from @tmpClosedNew tcn
                  WHERE ServiceChannelCD = 'DR'
                    AND AssignmentTypeName = 'Adverse Subro Desk Review'
                    AND SupplementCount = 0
                    AND StatusName = 'Complete'
                    AND tcn.AnalystUserID = ta.AnalystUserID
                    AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd)

   from @tmpAnalyst ta                                  


   INSERT INTO @tmpReport  (
      idx,
      Metric,
      DA_MTD_Amt,
      DR_MTD_Amt
   )
   SELECT 4,
          'Average OEM %',
          (select CONVERT(decimal(9,2), AVG( CASE
                                                WHEN VANAuditedPartsTaxable > 0 THEN (VANAuditedOEMParts / VANAuditedPartsTaxable) * 100.00
                                                ELSE 0
                                             END))
            FROM @tmpClosedNew
            WHERE AssignmentTypeName = 'Demand Estimate Audit' 
              AND ServiceChannelCD = 'DA'
              AND SupplementCount = 0
              AND StatusName = 'Complete'
              AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd),
          (select CONVERT(decimal(9,2), AVG( CASE
                                                WHEN VANAuditedPartsTaxable > 0 THEN (VANAuditedOEMParts / VANAuditedPartsTaxable) * 100.00
                                                ELSE 0
                                             END))
            FROM @tmpClosedNew
            WHERE AssignmentTypeName = 'Adverse Subro Desk Review' 
              AND ServiceChannelCD = 'DR'
              AND SupplementCount = 0
              AND StatusName = 'Complete'
              AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd)

   INSERT INTO @tmpReport  (
      idx,
      Metric
   )
   SELECT 4,
          ''

   INSERT INTO @tmpReport  (
      idx,
      Metric,
      DA_MTD_Amt,
      DR_MTD_Amt
   )
   select 4,
          AnalystName,
          (select CONVERT(decimal(9,2), AVG( CASE
                                                WHEN VANAuditedPartsTaxable > 0 THEN (VANAuditedOEMParts / VANAuditedPartsTaxable) * 100.00
                                                ELSE 0
                                             END))
            FROM @tmpClosedNew tcn
            WHERE AssignmentTypeName = 'Demand Estimate Audit' --ServiceChannelCD = 'DA'
              AND SupplementCount = 0
              AND StatusName = 'Complete'
              AND tcn.AnalystUserID = ta.AnalystUserID
              AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd),
          (select CONVERT(decimal(9,2), AVG( CASE
                                                WHEN VANAuditedPartsTaxable > 0 THEN (VANAuditedOEMParts / VANAuditedPartsTaxable) * 100.00
                                                ELSE 0
                                             END))
            FROM @tmpClosedNew tcn
            WHERE AssignmentTypeName = 'Adverse Subro Desk Audit' --ServiceChannelCD = 'DR'
              AND SupplementCount = 0
              AND StatusName = 'Complete'
              AND tcn.AnalystUserID = ta.AnalystUserID
              AND OriginalClosedDate between @RptMonthStart and @RptYesterdayEnd)
   from @tmpAnalyst ta


   print 'APD Daily Metric Report per Monthly Cycle - DA/DR QCR'
   print '-----------------------------------------------------'
   print 'Daily Data for: ' + convert(varchar, @RptYesterdayStart, 101)
   print 'Month: ' + convert(varchar, Datepart("mm", @RptMonthStart)) + '/' + convert(varchar, Datepart("yyyy", @RptMonthStart))
   print '                                                           Desk Audit                 Adverse Subro       '
   print '                                                   --------------------------- ---------------------------'
   select Metric, 
          convert(varchar(13), isNull(space(5 - len(convert(varchar, DA_Daily_Est))) + convert(varchar, DA_Daily_Est) + ' / ' + space(5 - len(convert(varchar, DA_Daily_Sup))) + convert(varchar, DA_Daily_Sup), '')),
          convert(varchar(13), isNull(space(5 - len(convert(varchar, DA_MTD_Est))) + convert(varchar, DA_MTD_Est) + ' / ' + space(5 - len(convert(varchar, DA_MTD_Sup))) + convert(varchar, DA_MTD_Sup), '')),
          convert(varchar(13), isNull(space(5 - len(convert(varchar, DR_Daily_Est))) + convert(varchar, DR_Daily_Est) + ' / ' + space(5 - len(convert(varchar, DR_Daily_Sup))) + convert(varchar, DR_Daily_Sup), '')),
          convert(varchar(13), isNull(space(5 - len(convert(varchar, DR_MTD_Est))) + convert(varchar, DR_MTD_Est) + ' / ' + space(5 - len(convert(varchar, DR_MTD_Sup))) + convert(varchar, DR_MTD_Sup), ''))
   from @tmpReport
   where idx = 1

   print ''
   print ''
   print '                                                        Desk Audit           Adverse Subro    '
   print '                                                   --------------------- ---------------------'
   select Metric, 
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DA_Daily_Amt))) + convert(varchar, DA_Daily_Amt), '')) as Daily,
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DA_MTD_Amt))) + convert(varchar, DA_MTD_Amt), '')) as MTD,
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DR_Daily_Amt))) + convert(varchar, DR_Daily_Amt), '')) as Daily,
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DR_MTD_Amt))) + convert(varchar, DR_MTD_Amt), '')) as MTD
   from @tmpReport
   where idx = 2

   print ''
   print ''
   print '                                                        Desk Audit           Adverse Subro    '
   print '                                                   --------------------- ---------------------'
   select Metric, 
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DA_Daily_Amt))) + convert(varchar, DA_Daily_Amt), '')) as Daily,
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DA_MTD_Amt))) + convert(varchar, DA_MTD_Amt), '')) as MTD,
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DR_Daily_Amt))) + convert(varchar, DR_Daily_Amt), '')) as Daily,
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DR_MTD_Amt))) + convert(varchar, DR_MTD_Amt), '')) as MTD
   from @tmpReport
   where idx = 3

   print ''
   print ''
   print '                                                   Desk Audit Adv. Subro'
   print '-------------------------------------------------- ---------- ----------'
   select Metric, 
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DA_MTD_Amt))) + convert(varchar, DA_MTD_Amt), '')) as MTD,
          convert(varchar(10), isNull(space(10 - len(convert(varchar, DR_MTD_Amt))) + convert(varchar, DR_MTD_Amt), '')) as MTD
   from @tmpReport
   where idx = 4

   print '--- END OF REPORT ---'

END



GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMetricsDA_DR_QCR' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptMetricsDA_DR_QCR TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
