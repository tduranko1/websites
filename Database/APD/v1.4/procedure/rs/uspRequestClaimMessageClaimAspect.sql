-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRequestClaimMessageClaimAspect' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRequestClaimMessageClaimAspect 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRequestClaimMessageClaimAspect
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jon Leatherwood
* FUNCTION:     Retrieves a ClaimAspectID for a claim to attach a received external claim message to.
*               Will return an empty recordset if no available claim aspect.
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRequestClaimMessageClaimAspect
    @LynxID                 udt_std_int = null,
    @ClaimNumber            udt_cov_claim_number = null,
    @InsuranceCompanyID     udt_std_int = null,
    @SourceApplicationID    udt_std_int
AS
BEGIN
        
    declare @ClaimAspectID udt_std_int_big
    
    set @ClaimAspectID = 0
    
    if @LynxID is not null
    begin
        select top 1 @ClaimAspectID = ClaimAspectID
        from utb_claim_aspect 
        where lynxid = @LynxID 
          and SourceApplicationID = @SourceApplicationID
          and EnabledFlag = 1
          and ClaimAspectTypeID = 9
        order by ClaimAspectID
    end
    else
    begin
        
        if ((@ClaimNumber is null) or (@InsuranceCompanyID is null))
        begin
            -- Must have both of these
             raiserror('A Claim Number and Insurance Company ID must be passed if Lynx ID is not given.',16,1)                           
            return
        end            
    
        select top 1 @ClaimAspectID =  ca.ClaimAspectID
        from utb_claim_aspect ca inner join utb_claim c on ca.LynxID = c.LynxID
                                 inner join utb_claim_coverage cc on ca.LynxID = cc.LynxID
        where c.ClientClaimNumber = @ClaimNumber --Project:210474 APD Modified the WHERE clause when we did the code merge M.A.20061211
          and c.InsuranceCompanyID = @InsuranceCompanyID
          and ca.SourceApplicationID = @SourceApplicationID 
          and ca.EnabledFlag = 1  
          and ca.ClaimAspectTypeID = 9
        order by ca.ClaimAspectID                               
    end    
    
    return @ClaimAspectID                     
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRequestClaimMessageClaimAspect' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRequestClaimMessageClaimAspect TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/