-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadUpdSurvey' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopLoadUpdSurvey 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopLoadUpdSurvey
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Insert a new Shop
*
*
*
* RESULT SET:
* ShopLocationID                          The newly created Shop Location unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopLoadUpdSurvey
(
    @ShopLoadID                              udt_std_id_big,
    @Age                                     udt_std_int_tiny=null,
    @ASEFlag                                 udt_std_flag=null,
    @BusinessRegsFlag                        udt_std_flag=null,
    @BusinessStabilityFlag                   udt_std_flag=null,
    @CommercialLiabilityFlag                 udt_std_flag=null,
    @CompetentEstimatorsFlag                 udt_std_flag=null,
    @CustomaryHoursFlag                      udt_std_flag=null,
    @DigitalPhotosFlag                       udt_std_flag=null,
    @EFTFlag                                 udt_std_flag=null,
    @ElectronicEstimatesFlag                 udt_std_flag=null,
    @EmployeeEducationFlag                   udt_std_flag=null,
    @EmployerLiabilityFlag                   udt_std_flag=null,
    @FrameAnchoringPullingEquipFlag          udt_std_flag=null,
    @FrameDiagnosticEquipFlag                udt_std_flag=null,
    @HazMatFlag                              udt_std_flag=null,
    @HydraulicLiftFlag                       udt_std_flag=null,
    @ICARFlag                                udt_std_flag=null,
    @MechanicalRepairFlag                    udt_std_flag=null,
    @MIGWeldersFlag                          udt_std_flag=null,
    @MinimumWarrantyFlag                     udt_std_flag=null,
    @NoFeloniesFlag                          udt_std_flag=null,
    @ReceptionistFlag                        udt_std_flag=null,
    @RefinishCapabilityFlag                  udt_std_flag=null,
    @RefinishTrainedFlag                     udt_std_flag=null,
    @SecureStorageFlag                       udt_std_flag=null,
    @ValidEPALicenseFlag                     udt_std_flag=null,
    @VANInetFlag                             udt_std_flag=null,
    @WorkersCompFlag                         udt_std_flag=null
)

AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @tDataReviewDate    AS DATETIME 
    DECLARE @ProcName           AS VARCHAR(30)       -- Used for raise error stmts 
    DECLARE @WorkmanshipID      AS tinyint
    DECLARE @RefinishID         AS tinyint
    DECLARE @now                AS datetime

    SET @ProcName = 'uspSMTShopLoadUpdSurvey'
    SET @now = CURRENT_TIMESTAMP


    -- Begin Update(s)
    UPDATE dbo.utb_shop_load
   	SET
      Age                             = @Age,
      ASEFlag                         = @ASEFlag,
      BusinessRegsFlag                = @BusinessRegsFlag,
      BusinessStabilityFlag           = @BusinessStabilityFlag,
      CommercialLiabilityFlag         = @CommercialLiabilityFlag,
      CompetentEstimatorsFlag         = @CompetentEstimatorsFlag,
      CustomaryHoursFlag              = @CustomaryHoursFlag,
      DigitalPhotosFlag               = @DigitalPhotosFlag,
      EFTFlag                         = @EFTFlag,
      ElectronicEstimatesFlag         = @ElectronicEstimatesFlag,
      EmployeeEducationFlag           = @EmployeeEducationFlag,
      EmployerLiabilityFlag           = @EmployerLiabilityFlag,
      FrameAnchoringPullingEquipFlag  = @FrameAnchoringPullingEquipFlag,
      FrameDiagnosticEquipFlag        = @FrameDiagnosticEquipFlag,
      HazMatFlag                      = @HazMatFlag,
      HydraulicLiftFlag               = @HydraulicLiftFlag,
      ICARFlag                        = @ICARFlag,
      MechanicalRepairFlag            = @MechanicalRepairFlag,
      MIGWeldersFlag                  = @MIGWeldersFlag,
      MinimumWarrantyFlag             = @MinimumWarrantyFlag,
      NoFeloniesFlag                  = @NoFeloniesFlag,
      QualificationsSubmitedDate      = @now,
      ReceptionistFlag                = @ReceptionistFlag,
      RefinishCapabilityFlag          = @RefinishCapabilityFlag,
      RefinishTrainedFlag             = @RefinishTrainedFlag,
      SecureStorageFlag               = @SecureStorageFlag,
      ValidEPALicenseFlag             = @ValidEPALicenseFlag,
      VANInetFlag                     = @VANInetFlag,
      WorkersCompFlag                 = @WorkersCompFlag,
      SysLastUpdatedDate              = @now   
    WHERE ShopLoadID = @ShopLoadID     
    
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_shop_load', 16, 1, @ProcName)
        RETURN
    END
    
    
    SELECT @ShopLoadID AS ShopLoadID

    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopLoadUpdSurvey' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopLoadUpdSurvey TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO



