-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientServices' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2ClientServices 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2ClientServices
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Compiles data displayed on the Services Reports
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company the report is being run for
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
* (I) @ShowAllClients       Flag indicating whether to compile all clients information
* (I) @StateCode            Optional state code
* (I) @OfficeID             Optional Office ID
*
* RESULT SET:
*   LynxPhoneNumber         Customer call in number for Lynx
*   ServiceGroup            Indicates the group of services 'DA' - Damagae Assessment, 'CS' - Other Claim Services
*   State                   State (or "All" if the report is for all states
*   OfficeName              Office Name
*   Pos1Header              The header name of column 1
*   Pos2Header              The header name of column 2
*   Pos3Header              The header name of column 3
*   Pos4Header              The header name of column 4
*   Pos5Header              The header name of column 5
*   Pos6Header              The header name of column 6
*   Pos7Header              The header name of column 7
*   Pos8Header              The header name of column 8
*   Pos9Header              The header name of column 9
*   Pos10Header             The header name of column 10
*   Pos11Header             The header name of column 11
*   Pos12Header             The header name of column 12
*   Level2Display           The display name of the detail lines
*   Level2Sort              The sort order of the detail lines
*   Level1Group             Display name for the insurance company header
*   Level2Group             Detail Line code ("PS", "DR", "IA", "SA", "LU", "TL", "SUB", "THFT")
*   Pos1Total               Total for column 1
*   Pos1Percent             Percentage for column 1
*   Pos2Total               Total for column 2
*   Pos2Percent             Percentage for column 2
*   Pos3Total               Total for column 3
*   Pos3Percent             Percentage for column 3
*   Pos4Total               Total for column 4
*   Pos4Percent             Percentage for column 4
*   Pos5Total               Total for column 5
*   Pos5Percent             Percentage for column 5
*   Pos6Total               Total for column 6
*   Pos6Percent             Percentage for column 6
*   Pos7Total               Total for column 7
*   Pos7Percent             Percentage for column 7
*   Pos8Total               Total for column 8
*   Pos8Percent             Percentage for column 8
*   Pos9Total               Total for column 9
*   Pos9Percent             Percentage for column 9
*   Pos10Total              Total for column 10
*   Pos10Percent            Percentage for column 10
*   Pos11Total              Total for column 11
*   Pos11Percent            Percentage for column 11
*   Pos12Total              Total for column 12
*   Pos12Percent            Percentage for column 12
*   TotalTotal              Total for column 13
*   TotalPercent            Percentage for column 13
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2ClientServices
    @InsuranceCompanyID     udt_std_int_small,
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL,
    @ShowAllClients         udt_std_flag = 0,
    @StateCode              udt_addr_state = NULL,
    @OfficeID               udt_std_int_small = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare Local variables
    
    CREATE TABLE #tmpServiceDataAllDA 
    (
        Level1Group                 varchar(50)  NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Pos1Total                   bigint       NULL,
        Pos1Percent                 int          NULL,
        Pos2Total                   bigint       NULL,
        Pos2Percent                 int          NULL,
        Pos3Total                   bigint       NULL,
        Pos3Percent                 int          NULL,
        Pos4Total                   bigint       NULL,
        Pos4Percent                 int          NULL,
        Pos5Total                   bigint       NULL,
        Pos5Percent                 int          NULL,
        Pos6Total                   bigint       NULL,
        Pos6Percent                 int          NULL,
        Pos7Total                   bigint       NULL,
        Pos7Percent                 int          NULL,
        Pos8Total                   bigint       NULL,
        Pos8Percent                 int          NULL,
        Pos9Total                   bigint       NULL,
        Pos9Percent                 int          NULL,
        Pos10Total                  bigint       NULL,
        Pos10Percent                int          NULL,
        Pos11Total                  bigint       NULL,
        Pos11Percent                int          NULL,
        Pos12Total                  bigint       NULL,
        Pos12Percent                int          NULL,
        TotalTotal                  bigint       NULL,
        TotalPercent                int          NULL
    )

    CREATE TABLE #tmpServiceDataClientDA 
    (
        Level1Group                 varchar(50)  NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Pos1Total                   bigint       NULL,
        Pos1Percent                 int          NULL,
        Pos2Total                   bigint       NULL,
        Pos2Percent                 int          NULL,
        Pos3Total                   bigint       NULL,
        Pos3Percent                 int          NULL,
        Pos4Total                   bigint       NULL,
        Pos4Percent                 int          NULL,
        Pos5Total                   bigint       NULL,
        Pos5Percent                 int          NULL,
        Pos6Total                   bigint       NULL,
        Pos6Percent                 int          NULL,
        Pos7Total                   bigint       NULL,
        Pos7Percent                 int          NULL,
        Pos8Total                   bigint       NULL,
        Pos8Percent                 int          NULL,
        Pos9Total                   bigint       NULL,
        Pos9Percent                 int          NULL,
        Pos10Total                  bigint       NULL,
        Pos10Percent                int          NULL,
        Pos11Total                  bigint       NULL,
        Pos11Percent                int          NULL,
        Pos12Total                  bigint       NULL,
        Pos12Percent                int          NULL,
        TotalTotal                  bigint       NULL,
        TotalPercent                int          NULL
    )
        
    CREATE TABLE #tmpServiceDataAllCS 
    (
        Level1Group                 varchar(100)  NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Pos1Total                   bigint       NULL,
        Pos1Percent                 int          NULL,
        Pos2Total                   bigint       NULL,
        Pos2Percent                 int          NULL,
        Pos3Total                   bigint       NULL,
        Pos3Percent                 int          NULL,
        Pos4Total                   bigint       NULL,
        Pos4Percent                 int          NULL,
        Pos5Total                   bigint       NULL,
        Pos5Percent                 int          NULL,
        Pos6Total                   bigint       NULL,
        Pos6Percent                 int          NULL,
        Pos7Total                   bigint       NULL,
        Pos7Percent                 int          NULL,
        Pos8Total                   bigint       NULL,
        Pos8Percent                 int          NULL,
        Pos9Total                   bigint       NULL,
        Pos9Percent                 int          NULL,
        Pos10Total                  bigint       NULL,
        Pos10Percent                int          NULL,
        Pos11Total                  bigint       NULL,
        Pos11Percent                int          NULL,
        Pos12Total                  bigint       NULL,
        Pos12Percent                int          NULL,
        TotalTotal                  bigint       NULL,
        TotalPercent                int          NULL
    )

    CREATE TABLE #tmpServiceDataClientCS 
    (
        Level1Group                 varchar(50)  NOT NULL,
        Level2Group                 varchar(20)  NOT NULL,
        Pos1Total                   bigint       NULL,
        Pos1Percent                 int          NULL,
        Pos2Total                   bigint       NULL,
        Pos2Percent                 int          NULL,
        Pos3Total                   bigint       NULL,
        Pos3Percent                 int          NULL,
        Pos4Total                   bigint       NULL,
        Pos4Percent                 int          NULL,
        Pos5Total                   bigint       NULL,
        Pos5Percent                 int          NULL,
        Pos6Total                   bigint       NULL,
        Pos6Percent                 int          NULL,
        Pos7Total                   bigint       NULL,
        Pos7Percent                 int          NULL,
        Pos8Total                   bigint       NULL,
        Pos8Percent                 int          NULL,
        Pos9Total                   bigint       NULL,
        Pos9Percent                 int          NULL,
        Pos10Total                  bigint       NULL,
        Pos10Percent                int          NULL,
        Pos11Total                  bigint       NULL,
        Pos11Percent                int          NULL,
        Pos12Total                  bigint       NULL,
        Pos12Percent                int          NULL,
        TotalTotal                  bigint       NULL,
        TotalPercent                int          NULL
    )

    CREATE TABLE #tmpClosedData 
    (
        LineItemCD                  varchar(4)   NOT NULL,
        PositionID                  tinyint      NOT NULL,
        MonthValue                  tinyint      NOT NULL,
        YearValue                   smallint     NOT NULL,
        ClosedCount                 int          NOT NULL,
        ClosedTotal                 int          NULL,
        ClosedPercent               int          NULL
    )

    CREATE TABLE #tmpClosedTotals 
    (
        DataLevel                   varchar(6)   NOT NULL,
        MonthValue                  tinyint      NOT NULL,
        YearValue                   smallint     NOT NULL,
        ClosedTotal                 int          NULL
    )

    CREATE TABLE #tmpTimePeriods 
    (
        PositionID                  tinyint     NOT NULL,
        TimePeriod                  varchar(8)  NOT NULL,
        MonthValue                  tinyint     NOT NULL,
        YearValue                   smallint    NOT NULL
    )


    DECLARE @InsuranceCompanyName   udt_std_name
    DECLARE @InsuranceCompanyLCPhone udt_std_name
    DECLARE @LoopIndex              udt_std_int_tiny
    DECLARE @MonthName              udt_std_name
    DECLARE @StateCodeWork          varchar(2)
    DECLARE @StateName              udt_std_name
    DECLARE @OfficeIDWork           varchar(10)
    DECLARE @OfficeName             udt_std_name
	DECLARE @PSClosedCountClient    udt_std_int
	DECLARE @PSClosedCountAll       udt_std_int
    
    DECLARE @Debug      udt_std_flag
    DECLARE @now        udt_std_datetime
    DECLARE @DataWarehouseDate    udt_std_datetime
	DECLARE @CancelledDispositionID int
	DECLARE @VoidedDispositionID    int

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2ClientServices'

    SET @Debug = 0


    -- Validate Insurance Company ID
    
    IF (@InsuranceCompanyID IS NULL) OR
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID
        
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name,
                @InsuranceCompanyLCPhone = IsNull(CarrierLynxContactPhone, '239-337-4300')
          FROM  dbo.utb_insurance
          WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END          
        
   
    -- Validate State Code
    
    IF @StateCode IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT StateCode FROM dbo.utb_state_code WHERE StateCode = @StateCode)
        BEGIN
            -- Invalid StateCode
        
            RAISERROR  ('101|%s|@StateCode|%s', 16, 1, @ProcName, @StateCode)
            RETURN
        END
        ELSE
        BEGIN
            SET @StateCodeWork = @StateCode
            
            SELECT  @StateName = StateValue
              FROM  dbo.utb_state_code
              WHERE StateCode = @StateCode
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @StateCodeWork = '%'
        SET @StateName = 'All'
    END          
        
    -- Validate Office ID
    
    IF @OfficeID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT OfficeID FROM dbo.utb_office WHERE OfficeID = @OfficeID AND InsuranceCompanyID = @InsuranceCompanyID)
        BEGIN
            -- Invalid Office ID
    
            RAISERROR  ('101|%s|@OfficeID|%u', 16, 1, @ProcName, @OfficeID)
            RETURN
        END
        ELSE
        BEGIN
            SET @OfficeIDWork = convert(varchar(10), @OfficeID)
        
            SELECT  @OfficeName = Name
              FROM  dbo.utb_office
              WHERE OfficeID = @OfficeID
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @OfficeIDWork = '%'
        SET @OfficeName = 'All'
    END          
   
    -- Get Cancelled DispositionType ID
   
    SET @CancelledDispositionID = NULL
	
	SELECT @CancelledDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Cancelled'
    
    IF @CancelledDispositionID IS NULL OR LEN(@CancelledDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Cancelled in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
	-- Get Voided DispositionType ID
	
	SET @VoidedDispositionID = NULL

	SELECT @VoidedDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Voided'
    
    IF @VoidedDispositionID IS NULL OR LEN(@VoidedDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Voided in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
   
    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END


    -- Compile time periods
    
    SET @LoopIndex = 12

    WHILE @LoopIndex > 0
    BEGIN
        INSERT INTO #tmpTimePeriods
          SELECT 13 - @LoopIndex AS Position,
                 Convert(varchar(2), DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))))
                    + '/'
                    + Convert(varchar(4), DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))),
                 DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))), 
                 DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpTimePeriods', 16, 1, @ProcName)
            RETURN
        END
    
        SET @LoopIndex = @LoopIndex - 1
    END

    CREATE UNIQUE CLUSTERED INDEX uix_ak_tmptimeperiods
      ON #tmpTimePeriods(PositionID)
    WITH FILLFACTOR = 100
    
    CREATE INDEX uix_ak_tmptimeperiods_timeperiod
      ON #tmpTimePeriods(TimePeriod)
    WITH FILLFACTOR = 100
    
    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID = ' + Convert(varchar, @InsuranceCompanyID)
        PRINT '@InsuranceCompanyName = ' + @InsuranceCompanyName
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@ShowAllClients = ' + Convert(varchar, @ShowAllClients)
        PRINT '@StateCode = ' + @StateCode
        PRINT '@StateCodeWork = ' + @StateCodeWork
        PRINT '@OfficeID = ' + convert(varchar, @OfficeID)
        PRINT '@OfficeIDWork = ' + convert(varchar(10), @OfficeIDWork)
    END
    
    
    -- Compile data needed for the report

    -- Client Data for damage assessment

    INSERT INTO #tmpClosedData
      SELECT  'TTL',
              tmpTP.PositionID,
              dt.MonthofYear,
              dt.YearValue,
              Count(*) AS ClosedCount,
              NULL,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeClosingID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND ds.StateCode LIKE @StateCodeWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
   		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpClosedData
      SELECT  'TTL',
              13,
              99,
              9999,
              Count(*) AS ClosedCount,
              NULL,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeClosingID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND ds.StateCode LIKE @StateCodeWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
   		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END
        
    INSERT INTO #tmpClosedTotals
      SELECT  'Client',
              MonthValue,
              YearValue,
              ClosedCount
        FROM  #tmpClosedData
                 
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
        
        RAISERROR('105|%s|#tmpClosedTotals', 16, 1, @ProcName) 
        RETURN
    END
        
    INSERT INTO #tmpClosedData
      SELECT  dsc.ServiceChannelCD,
              tmpTP.PositionID,
              dt.MonthOfYear,
              dt.YearValue,
              Count(*) AS ClosedCount,
              (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'Client' AND MonthValue = dt.MonthOfYear AND YearValue = dt.YearValue),
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.CoverageTypeID IS NOT NULL
          AND ds.StateCode LIKE @StateCodeWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
        GROUP BY dsc.ServiceChannelCD, tmpTP.PositionID, dt.MonthOfYear, dt.YearValue
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END
	
	SELECT @PSClosedCountClient = SUM(ClosedCount) FROM #tmpClosedData WHERE LineItemCD = 'PS'
	
	IF @@ERROR <> 0
	BEGIN
	   -- selection failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END	   

    INSERT INTO #tmpClosedData
      SELECT  dsc.ServiceChannelCD,
              13,
              99,
              9999,
              Count(*) AS ClosedCount,
              (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'Client' AND MonthValue = 99 AND YearValue = 9999),
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.CoverageTypeID IS NOT NULL
          AND ds.StateCode LIKE @StateCodeWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
        GROUP BY dsc.ServiceChannelCD
        HAVING dsc.ServiceChannelCD IS NOT NULL

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    UPDATE  #tmpClosedData
      SET   ClosedPercent = Round(Convert(decimal(9,2), ClosedCount) / Convert(decimal(9,2), ClosedTotal), 2) * 100
      WHERE LineItemCD <> 'TTL'
        AND ClosedTotal > 0  --Prevent divide by zero error

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|#tmpClosedData', 16, 1, @ProcName)
        RETURN
    END


    -- Pivot and save the data extracted

    INSERT INTO #tmpServiceDataClientDA
      SELECT  @InsuranceCompanyName,
              LineItemCD,
              SUM(CASE PositionID WHEN 1 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 1 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 13 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 13 THEN ClosedPercent ELSE 0 END)
        FROM  #tmpClosedData
        GROUP BY LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpServiceDataClientDA', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpServiceDataClientDA
      SELECT  @InsuranceCompanyName,
              dsc.ServiceChannelCD,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0
        FROM  dbo.utb_dtwh_dim_service_channel dsc
        WHERE dsc.ServiceChannelCD NOT IN (SELECT Level2Group FROM #tmpServiceDataClientDA)
          AND dsc.ServiceChannelCD IN (SELECT  at.ServiceChannelDefaultCD 
                                         FROM  dbo.utb_client_assignment_type cat
                                         LEFT JOIN dbo.utb_assignment_type at ON (cat.AssignmentTypeID = at.AssignmentTypeID)
                                         WHERE cat.InsuranceCompanyID = @InsuranceCompanyID)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpServiceDataClientDA', 16, 1, @ProcName)
        RETURN
    END


  
    IF @ShowAllClients = 1
    BEGIN
        -- Reset the temporary tables for next cycle

        DELETE FROM #tmpClosedData

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR  ('106|%s|#tmpClosedData', 16, 1, @ProcName)
            RETURN
        END


        -- All Data

        INSERT INTO #tmpClosedData
          SELECT  'TTL',
                  tmpTP.PositionID,
                  dt.MonthofYear,
                  dt.YearValue,
                  Count(*) AS ClosedCount,
                  NULL,
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE fc.AssignmentTypeClosingID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
              AND ds.StateCode LIKE @StateCodeWork
              AND fc.DemoFlag = 0
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END
        
        INSERT INTO #tmpClosedData
          SELECT  'TTL',
          13,
                  99,
                  9999,
                  Count(*) AS ClosedCount,
                  NULL,
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE fc.AssignmentTypeClosingID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
              AND ds.StateCode LIKE @StateCodeWork
              AND fc.DemoFlag = 0
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END
        
        INSERT INTO #tmpClosedTotals
          SELECT  'All',
                  MonthValue,
                  YearValue,
                  ClosedCount
            FROM  #tmpClosedData
                     
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedTotals', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpClosedData
          SELECT  dsc.ServiceChannelCD,
                  tmpTP.PositionID,
                  dt.MonthOfYear,
                  dt.YearValue,
                  Count(*) AS ClosedCount,
                  (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'All' AND MonthValue = dt.MonthOfYear AND YearValue = dt.YearValue),
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE fc.CoverageTypeID IS NOT NULL
              AND ds.StateCode LIKE @StateCodeWork
              AND fc.DemoFlag = 0
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY dsc.ServiceChannelCD, tmpTP.PositionID, dt.MonthOfYear, dt.YearValue
            HAVING dsc.ServiceChannelCD IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END

		SELECT @PSClosedCountAll = SUM(ClosedCount) FROM #tmpClosedData WHERE LineItemCD = 'PS'
	
	    IF @@ERROR <> 0
	    BEGIN
	    -- selection failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
        END	   
		
        INSERT INTO #tmpClosedData
          SELECT  dsc.ServiceChannelCD,
                  13,
                  99,
                  9999,
                  Count(*) AS ClosedCount,
                  (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'All' AND MonthValue = 99 AND YearValue = 9999),
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        	LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE fc.CoverageTypeID IS NOT NULL
              AND ds.StateCode LIKE @StateCodeWork
              AND fc.DemoFlag = 0
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY dsc.ServiceChannelCD
            HAVING dsc.ServiceChannelCD IS NOT NULL

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END
         
        UPDATE  #tmpClosedData
          SET   ClosedPercent = Round(Convert(decimal(9,2), ClosedCount) / Convert(decimal(9,2), ClosedTotal), 2) * 100
          WHERE LineItemCD <> 'TTL'
            AND ClosedTotal > 0 --Prevent divide by zero error

        IF @@ERROR <> 0
        BEGIN
            -- Update failed

            RAISERROR('104|%s|#tmpClosedData', 16, 1, @ProcName)
            RETURN
        END
        

        -- Pivot and save the data extracted

        INSERT INTO #tmpServiceDataAllDA
          SELECT  '* All Clients *',
                  LineItemCD,
                  SUM(CASE PositionID WHEN 1 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 1 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 2 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 2 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 3 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 3 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 4 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 4 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 5 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 5 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 6 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 6 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 7 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 7 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 8 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 8 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 9 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 9 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 10 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 10 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 11 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 11 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 12 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 12 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 13 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 13 THEN ClosedPercent ELSE 0 END)
            FROM  #tmpClosedData
            GROUP BY LineItemCD

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpServiceDataAllDA', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpServiceDataAllDA
          SELECT  '* All Clients *',
                  dsc.ServiceChannelCD,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0
            FROM  dbo.utb_dtwh_dim_service_channel dsc
            WHERE dsc.ServiceChannelCD NOT IN (SELECT Level2Group FROM #tmpServiceDataAllDA)

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpServiceDataAllDA', 16, 1, @ProcName)
            RETURN
        END
    END
    


    -- Reset the temporary table for next cycle
    
    DELETE FROM #tmpClosedData

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('106|%s|#tmpClosedData', 16, 1, @ProcName)
        RETURN
    END


    -- Client Data for Claim Services
    
    -- Looking at closed claims only
    
    INSERT INTO #tmpClosedData
      SELECT  'LU',
              tmpTP.PositionID,
              dt.MonthofYear,
              dt.YearValue,
              Count(*) AS ClosedCount,
              (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'Client' AND MonthValue = dt.MonthOfYear AND YearValue = dt.YearValue),
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeClosingID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND ds.StateCode LIKE @StateCodeWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
          AND fc.ServiceLossOfUseFlag = 1
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpClosedData
      SELECT  'LU',
              13,
              99,
              9999,
              Count(*) AS ClosedCount,
              (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'Client' AND MonthValue = 99 AND YearValue = 9999),
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeClosingID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND ds.StateCode LIKE @StateCodeWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
          AND fc.ServiceLossOfUseFlag = 1
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpClosedData
      SELECT  'TL',
              tmpTP.PositionID,
              dt.MonthofYear,
              dt.YearValue,
              Count(*) AS ClosedCount,
              (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'Client' AND MonthValue = dt.MonthOfYear AND YearValue = dt.YearValue),
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeClosingID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND ds.StateCode LIKE @StateCodeWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
          AND fc.ServiceTotalLossFlag = 1
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpClosedData
      SELECT  'TL',
              13,
              99,
              9999,
              Count(*) AS ClosedCount,
              (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'Client' AND MonthValue = 99 AND YearValue = 9999),
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND fc.AssignmentTypeClosingID IS NOT NULL
          AND fc.CoverageTypeID IS NOT NULL
          AND ds.StateCode LIKE @StateCodeWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
          AND fc.ServiceTotalLossFlag = 1
		  AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
        RETURN
    END

    
    UPDATE  #tmpClosedData
      SET   ClosedPercent = Round(Convert(decimal(9,2), ClosedCount) / Convert(decimal(9,2), @PSClosedCountClient), 2) * 100
      WHERE ClosedTotal > 0  --Prevent divide by zero error

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|#tmpClosedData', 16, 1, @ProcName)
        RETURN
    END


    -- Pivot and save the data extracted

    INSERT INTO #tmpServiceDataClientCS
      SELECT  @InsuranceCompanyName,
              LineItemCD,
              SUM(CASE PositionID WHEN 1 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 1 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 13 THEN ClosedCount ELSE 0 END),
      SUM(CASE PositionID WHEN 13 THEN ClosedPercent ELSE 0 END)
        FROM  #tmpClosedData
        GROUP BY LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpServiceDataClientCS', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpServiceDataClientCS
      SELECT  @InsuranceCompanyName,
              value,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
              0, 0, 0, 0, 0, 0
        FROM  dbo.ufnUtilityParseString('LU,TL', ',', 1)
        WHERE value NOT IN (SELECT Level2Group FROM #tmpServiceDataClientCS)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpServiceDataClientCS', 16, 1, @ProcName)
        RETURN
    END


    IF @ShowAllClients = 1
    BEGIN
        -- Reset the temporary table for next cycle
    
        DELETE FROM #tmpClosedData

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR  ('106|%s|#tmpClosedData', 16, 1, @ProcName)
            RETURN
        END


        -- All Data for Claim Services
    
        INSERT INTO #tmpClosedData
          SELECT  'LU',
                  tmpTP.PositionID,
                  dt.MonthofYear,
                  dt.YearValue,
                  Count(*) AS ClosedCount,
                  (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'All' AND MonthValue = dt.MonthOfYear AND YearValue = dt.YearValue),
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE fc.AssignmentTypeClosingID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
              AND ds.StateCode LIKE @StateCodeWork
              AND fc.ServiceLossOfUseFlag = 1
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpClosedData
          SELECT  'LU',
                  13,
                  99,
                  9999,
                  Count(*) AS ClosedCount,
                  (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'All' AND MonthValue = 99 AND YearValue = 9999),
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE fc.AssignmentTypeClosingID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
              AND ds.StateCode LIKE @StateCodeWork
              AND fc.ServiceLossOfUseFlag = 1
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpClosedData
          SELECT  'TL',
                  tmpTP.PositionID,
                  dt.MonthofYear,
                  dt.YearValue,
                  Count(*) AS ClosedCount,
                  (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'All' AND MonthValue = dt.MonthOfYear AND YearValue = dt.YearValue),
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE fc.AssignmentTypeClosingID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
              AND ds.StateCode LIKE @StateCodeWork
              AND fc.ServiceTotalLossFlag = 1
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpClosedData
          SELECT  'TL',
                  13,
                  99,
                  9999,
                  Count(*) AS ClosedCount,
                  (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'All' AND MonthValue = 99 AND YearValue = 9999),
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            WHERE fc.AssignmentTypeClosingID IS NOT NULL
              AND fc.CoverageTypeID IS NOT NULL
              AND ds.StateCode LIKE @StateCodeWork
              AND fc.ServiceTotalLossFlag = 1
			  AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedData', 16, 1, @ProcName) 
            RETURN
        END

       
        UPDATE  #tmpClosedData
          SET   ClosedPercent = Round(Convert(decimal(9,2), ClosedCount) / Convert(decimal(9,2), @PSClosedCountAll), 2) * 100
          WHERE ClosedTotal > 0 --Prevent divide by zero error

        IF @@ERROR <> 0
        BEGIN
            -- Update failed

            RAISERROR('104|%s|#tmpClosedData', 16, 1, @ProcName)
            RETURN
        END


        -- Pivot and save the data extracted

        INSERT INTO #tmpServiceDataAllCS
          SELECT  '* All Clients *',
                  LineItemCD,
                  SUM(CASE PositionID WHEN 1 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 1 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 2 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 2 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 3 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 3 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 4 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 4 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 5 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 5 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 6 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 6 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 7 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 7 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 8 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 8 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 9 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 9 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 10 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 10 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 11 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 11 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 12 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 12 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 13 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 13 THEN ClosedPercent ELSE 0 END)
            FROM  #tmpClosedData
            GROUP BY LineItemCD

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpServiceDataAllCS', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpServiceDataAllCS
          SELECT  @InsuranceCompanyName,
                  value,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0
            FROM  dbo.ufnUtilityParseString('LU,TL', ',', 1)
            WHERE value NOT IN (SELECT Level2Group FROM #tmpServiceDataAllCS)

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpServiceDataAllCS', 16, 1, @ProcName)
            RETURN
        END
    END
    

    -- Final Select
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            'DA' AS ServiceGroup,
            @StateName AS State,
            @OfficeName AS OfficeName,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
            CASE tmp.Level2Group
              WHEN 'PS' THEN 'Program Shop'
              WHEN 'DR' THEN 'Desk Review'
              WHEN 'DA' THEN 'Desk Audit'
              WHEN 'IA' THEN 'Indepemdent Adj.'
              WHEN 'SA' THEN 'Client Field Staff'
              WHEN 'TTL' THEN 'Total Closed'
            END AS Level2Display,
            CASE tmp.Level2Group
              WHEN 'PS' THEN 1
              WHEN 'DR' THEN 2
              WHEN 'DA' THEN 3
              WHEN 'IA' THEN 4
              WHEN 'SA' THEN 5
              WHEN 'TTL' THEN 6
            END AS Level2Sort,
            tmp.*,
            @DataWarehouseDate as DataWareDate 
      FROM  #tmpServiceDataClientDA tmp
    
    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            'DA' AS ServiceGroup,
            @StateName AS State,
            @OfficeName AS OfficeName,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
            CASE tmp.Level2Group
              WHEN 'PS' THEN 'Program Shop'
              WHEN 'DR' THEN 'Desk Review'
              WHEN 'DA' THEN 'Desk Audit'
              WHEN 'IA' THEN 'Indepemdent Adj.'
              WHEN 'SA' THEN 'Client Field Staff'
              WHEN 'TTL' THEN 'Total Closed'
            END AS Level2Display,
            CASE tmp.Level2Group
              WHEN 'PS' THEN 1
              WHEN 'DR' THEN 2
              WHEN 'DA' THEN 3
              WHEN 'IA' THEN 4
              WHEN 'SA' THEN 5
              WHEN 'TTL' THEN 6
            END AS Level2Sort,
            tmp.*,
            @DataWarehouseDate as DataWareDate 
      FROM  #tmpServiceDataAllDA tmp

    
    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            'CS' AS ServiceGroup,
            @StateName AS State,
            @OfficeName AS OfficeName,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
            CASE tmp.Level2Group
              WHEN 'LU' THEN 'Loss of Use'
              WHEN 'TL' THEN 'Total Loss'
            END AS Level2Display,
            CASE tmp.Level2Group
              WHEN 'LU' THEN 1
              WHEN 'TL' THEN 2
            END AS Level2Sort,
            tmp.*,
            @DataWareHouseDate as DataWareDate 
      FROM  #tmpServiceDataClientCS tmp

    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            'CS' AS ServiceGroup,
            @StateName AS State,
            @OfficeName AS OfficeName,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
            CASE tmp.Level2Group
              WHEN 'LU' THEN 'Loss of Use'
              WHEN 'TL' THEN 'Total Loss'
            END AS Level2Display,
            CASE tmp.Level2Group
              WHEN 'LU' THEN 1
              WHEN 'TL' THEN 2
            END AS Level2Sort,
            tmp.*,
            @DataWareHouseDate as DataWareDate 
      FROM  #tmpServiceDataAllCS tmp
          
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END             
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientServices' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2ClientServices TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/