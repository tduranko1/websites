-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptOperOpenShopLic' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptOperOpenShopLic 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptOperOpenShopLic
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     This proc will return the open program shops that require a licensure
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptOperOpenShopLic
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare Local variables
    DECLARE @Debug      udt_std_flag
    DECLARE @now        udt_std_datetime

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRptOperOpenShopLic'

    SET @Debug = 0
    
    DECLARE @reportData TABLE
    (
        State               varchar(50),
        LYNXID              bigint,
        ClaimRep            varchar(100),
        ClaimCreatedDate    datetime,
        ReopenedDate        datetime,
        RecentEventDate     datetime,
        RecentEventDesc     varchar(500),
        RecentTaskID        int,
        RecentTaskDate      datetime,
        RecentTaskDesc      varchar(500)
    )
    
    -- Get all the open program shop claims
    INSERT INTO @reportData
    SELECT sc.StateValue,
           c.LYNXID,
           usr.NameLast + ', ' + usr.NameFirst,
           c.IntakeFinishDate,
           (SELECT max(hl.CompletedDate) 
                FROM utb_history_log hl
                LEFT JOIN dbo.utb_event e ON (hl.EventID = e.EventID)
                WHERE hl.LynxID = c.LynxID
                  AND hl.ClaimAspectTypeID = ca.ClaimAspectTypeID
                  AND hl.ClaimAspectNumber = ca.ClaimAspectNumber
                  AND e.Name = 'Vehicle Reopened'
           ),
           (SELECT top 1 CompletedDate 
                FROM utb_history_log hl
                WHERE hl.LynxID = c.LynxID
                  AND LogType = 'E'
                ORDER BY CompletedDate DESC
           ),
           (SELECT top 1 Description 
                FROM utb_history_log hl
                WHERE hl.LynxID = c.LynxID
                  AND LogType = 'E'
                ORDER BY CompletedDate DESC
           ),           
           (SELECT top 1 hl.TaskID
                FROM utb_history_log hl
                WHERE hl.LynxID = c.LynxID
                  AND LogType = 'T'
                ORDER BY CompletedDate DESC
           ),
           (SELECT top 1 CompletedDate 
                FROM utb_history_log hl
                WHERE hl.LynxID = c.LynxID
                  AND LogType = 'T'
                ORDER BY CompletedDate DESC
           ),
           (SELECT top 1 Description 
                FROM utb_history_log hl
                WHERE hl.LynxID = c.LynxID
                  AND LogType = 'T'
                ORDER BY CompletedDate DESC
           )
    FROM dbo.utb_claim c
    LEFT JOIN dbo.utb_claim_aspect ca ON (c.LYNXID = ca.LYNXID)
    LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc
    on casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN dbo.utb_assignment a ON casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID --(ca.ClaimAspectID = a.ClaimAspectID)
    LEFT JOIN dbo.utb_state_code sc ON (c.LossState = sc.StateCode)
    LEFT JOIN dbo.utb_user usr ON (ca.OwnerUserID = usr.UserID)
    WHERE ca.ClaimAspectTypeID = (select claimAspectTypeID from utb_claim_aspect_type where name = 'Vehicle')
      AND (sc.LicenseRequiredOwnerFlag = 1 OR sc.LicenseRequiredAnalystFlag = 1)
      AND sc.StateCode <> 'FL'
      AND casc.OriginalCompleteDate IS NULL
      AND a.ProgramTypeCD = 'LS'
      AND a.assignmentsequencenumber = 1
      AND a.CancellationDate IS NULL
      AND usr.OfficeID IS NULL
      AND c.DemoFlag = 0
      
    -- Delete any claim cancelled/claim voided
    
    DELETE FROM @reportData
    WHERE LYNXID IN (SELECT LynxID
                        FROM utb_history_log
                        WHERE Description IN ('Claim Cancelled', 'Claim Voided')
                    )
    
--    SELECT * FROM @reportData
    -- Final Select
    SELECT State,
           LYNXID,
           ClaimRep,
           ClaimCreatedDate,
           ReopenedDate,
           RecentEventDate,
           RecentEventDesc,
           RecentTaskDate,
           RecentTaskDesc + ' - ' + t.Name as RecentTaskDesc
    FROM @reportData tmp
    LEFT JOIN dbo.utb_task t ON (tmp.RecentTaskID = t.TaskID)
    ORDER BY State, LYNXID

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptOperOpenShopLic' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptOperOpenShopLic TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/