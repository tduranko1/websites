-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptReinspectionReport' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptReinspectionReport 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:   uspRptReinspectionReport
* SYSTEM:           Lynx Services APD
* AUTHOR:          Madhavi Kotipalli
* FUNCTION:       Retrieves the details for a Reinspection Calibration (Supervisor Review) 
*
* PARAMETERS:  
*     @RptMonth                
*     @RptYear   
* RESULT SET:
* List of claims with their inspection details for each shop within the month and year specified
* Adjuster( Company Name)
* Shop Name / Claim Number
* PositionID
* Repair vs Replace
* Straightening
* Unibody/Frame
* Refinish
* Non OEM Parts
* Mechanical
* Recycled Parts
* Equipment
* Prior danage
* Labor rates
* Alt. Repair method
* Overlap
* Appearance  Allow
* Depreciation
* Obvious no damage.
* Miscellaneous
* Missed damage.
* No visible damage
* Suppl. reconcillation.
* Aggrements
* Estimated amount
* Error amount
* percentage of rating
* 
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptReinspectionReport
    @RptMonth             udt_std_int_small,
    @RptYear               udt_std_int_small
AS
BEGIN

   -- Set Database Options
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

   -- Declare Local Variables  
    DECLARE @ProcName           varchar(30) 
    DECLARE @BaseEstimateAmt    money
    DECLARE @TotalAdditions     money
    DECLARE @DifferenceActual   money
    DECLARE @TotalSubtractions  money
    DECLARE @Supplement         varchar(30)
    DECLARE @TempString 	varchar(100)

    DECLARE @MonthName       varchar(10)

    DECLARE @Now datetime
    DECLARE @DataWarehouseDate    udt_std_datetime
    
	DECLARE @tmpReInspectionSheet  TABLE
	(
	AdjusterName		varchar(100),
	ShopID			bigint,
	LynxNo			int,
    PositionID		smallint,
	Repair_Replace		int,
	Straightening		int,
	Unibody_Frame		int,
	Refinish			int,
	Non_OEM_Parts	int,
	Mechanical		int,
	Recycled_Parts		int,
	Equipment		int,
	Prior_Damage		int,
	Labor_Rates		int,
	Alt_Repair_Method	int,
	Overlap			int,
	AppearnceAllow		int,
	Depreciation		int,
	Obvious_No_Damage	int,
	Miscellaneous		int,
	MissedDamage		int,
	No_Visible_Damage	int,
	Suppl_Recon		int,
	Aggrements		int,
    BasEstAmt		money,
	EstAmt			money,
	ErrorAmt		money,
    Percentage_Rating	float
	)		


    DECLARE @tmpFinalReport TABLE
    (
	AdjusterName		varchar(100),
	ShopID			bigint,
	LynxNo			int,
	Repair_Replace		int,
	Straightening		int,
	Unibody_Frame		int,
	Refinish			int,
	Non_OEM_Parts	int,
	Mechanical		int,
	Recycled_Parts		int,
	Equipment		int,
	Prior_Damage		int,
	Labor_Rates		int,
	Alt_Repair_Method	int,
	Overlap			int,
	AppearnceAllow		int,
	Depreciation		int,
	Obvious_No_Damage	int,
	Miscellaneous		int,
	MissedDamage		int,
	No_Visible_Damage	int,
	Suppl_Recon		int,
	Aggrements		int,
	BasEstAmt		money,
    CalEstAmt		money,
	ErrorAmt		money,
    Percentage_Rating	float,
	Total_Inspections	int,
	Total_Corrected_Data	int,
	TPert_Corrected_Data	float,
 	Total_Error_Amt		money,
	Total_Rating		float
	)		
         

    DECLARE @tmpFinalTotals  TABLE
	(
	AdjusterName		varchar(100),
	Total_Inspections	int,
 	Total_Error_Amt		money,
	Total_Rating		float
	)


    DECLARE @tmpCorrectTotals TABLE
	(
	AdjusterName		varchar(100),
	Total_Corrected_Data	int
	--TPert_Corrected_Data	float
	)


    --Verify Parameter List
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    SET @ProcName = 'uspRptReinspectionReport'        


    -- Check Report Date fields and set as necessary    
     IF @RptMonth is NULL
     BEGIN
        SET @RptMonth = Month(@now)
     END

     IF @RptYear is NULL
     BEGIN
        SET @RptYear = Year(@now)
     END

 -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    

    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN

    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END     


     IF @@ERROR <> 0
	 BEGIN
	       -- SQL Server Error   
   	 RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
    END

   
   
 INSERT INTO @tmpReinspectionSheet
        SELECT 
    	( U.NameLast  + ' ' + U.NameFirst) ,            
              RL.ShopLocationID,
             	 CA.LynxID,
              RD.ReinspectExceptionID,
    SUM  (CASE RD.REInspectExceptionID WHEN 1 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt)  ELSE 0 END),  --Repair_Replace
	SUM  (CASE RD.REInspectExceptionID WHEN 2 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Straightening
	SUM  (CASE RD.REInspectExceptionID WHEN 3 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), -- Unibody_Frame
	SUM  (CASE RD.REInspectExceptionID WHEN 4 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Refinish
	SUM  (CASE RD.REInspectExceptionID WHEN 5 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END),  --Non_PEM_Parts
	SUM  (CASE RD.REInspectExceptionID WHEN 6 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Mechanical
	SUM  (CASE RD.REInspectExceptionID WHEN 7 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Recycled_Parts
	SUM  (CASE RD.REInspectExceptionID WHEN 8 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Equipment
	SUM  (CASE RD.REInspectExceptionID WHEN 9 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Prior_Damage
	SUM  (CASE RD.REInspectExceptionID WHEN 10 THEN  ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0  END), --Labor_Rates
	SUM  (CASE RD.REInspectExceptionID WHEN 11 THEN  ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Alt_Repair_Method
	SUM  (CASE RD.REInspectExceptionID WHEN 12 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Overlap
	SUM  (CASE RD.REInspectExceptionID WHEN 13 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --AppearnceAllow
	SUM  (CASE RD.REInspectExceptionID WHEN 14 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Depreciation
	SUM  (CASE RD.REInspectExceptionID WHEN 15 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Obvious_No_Damage
	SUM  (CASE RD.REInspectExceptionID WHEN 16 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END),  --Miscellaneous
	SUM  (CASE RD.REInspectExceptionID WHEN 17 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --MissedDamage
	SUM  (CASE RD.REInspectExceptionID WHEN 18 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --No_Visible_Damage
	SUM  (CASE RD.REInspectExceptionID WHEN 19 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END), --Suppl_Recon
	SUM  (CASE RD.REInspectExceptionID WHEN 20 THEN   ABS(RD.ReinspectAmt - RD.EstimateAmt) ELSE 0 END),  --Aggrements
    R.BaseEstimateAmt,
    SUM( RD.EstimateAmt),
    0, -- Error Amt 
             dbo.ufnUtilityCalcReinspectPercentSat(R.ReinspectID)  -- Percentage of Error Amt 	
	FROM dbo.utb_dtwh_fact_estimate FE
	   LEFT JOIN dbo.utb_reinspect R ON FE.DocumentID = R.EstimateDocumentID
                LEFT JOIN dbo.utb_claim_aspect CA ON R.ClaimAspectID = CA.ClaimAspectID
                LEFT JOIN dbo.utb_user U ON R.ReinspectorUserID = U.UserID
                LEFT JOIN dbo.utb_reinspect_detail  RD  ON R.ReinspectID = RD.ReinspectID
                LEFT JOIN dbo.utb_dtwh_dim_repair_location RL ON FE.RepairLocationID = RL.RepairLocationID
              WHERE  R.EnabledFlag = 1 AND @RptMonth =Month(R.ReinspectionDate) AND @RptYear = Year ( R.ReinspectionDate)
               GROUP BY (U.NameLast + ' ' +  U.NameFirst ), RL.ShopLocationID, R.ClaimAspectID, CA.LynxID,RD.ReInspectExceptionID,R.BaseEstimateAmt,R.ReinspectID
   
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
   
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

      
      
    UPDATE @tmpReinspectionSheet
        SET ErrorAmt = ( Repair_Replace + Straightening + Unibody_Frame + Refinish + Non_OEM_Parts +
	Mechanical + Recycled_Parts + Equipment + Prior_Damage + Labor_Rates +
	Alt_Repair_Method + Overlap + AppearnceAllow + Depreciation  + Obvious_No_Damage +
	Miscellaneous + MissedDamage + No_Visible_Damage + Suppl_Recon + Aggrements)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
   
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	INSERT INTO @tmpFinalReport 
                 SELECT RS.AdjusterName, RS.ShopID, RS.LynxNo ,Sum( Repair_Replace) ,Sum(Straightening) ,Sum(Unibody_Frame) ,Sum( Refinish) ,Sum(Non_OEM_Parts),
	Sum(Mechanical), Sum(Recycled_Parts) ,Sum(Equipment) ,Sum(Prior_Damage),Sum(Labor_Rates),Sum(Alt_Repair_Method),Sum(Overlap),Sum(AppearnceAllow),
	Sum(Depreciation),Sum(Obvious_No_Damage),Sum(Miscellaneous),Sum(MissedDamage),Sum(No_Visible_Damage),Sum(Suppl_Recon),Sum(Aggrements), 
 	Sum(RS.BasEstAmt)/Count(RS.BasEstAmt), ISNULL(Sum(EstAmt),0),ISNULL(Sum(ErrorAmt),0) , Sum(RS.Percentage_Rating)/Count(RS.Percentage_Rating),
	0,0,0,0,0
              FROM @tmpReinspectionSheet RS
              GROUP BY  RS.AdjusterName,RS.ShopID,RS.LynxNo

	IF @@ERROR <> 0
	    BEGIN
   	    -- SQL Server Error
   
   	     RAISERROR  ('99|%s', 16, 1, @ProcName)
	     RETURN
	END


    INSERT INTO  @tmpFinalTotals 
	SELECT AdjusterName,  Count(*)  as Total_Inspections , Sum(ErrorAmt), Sum(Percentage_Rating) * 100 / Count(Percentage_Rating) 
              FROM @tmpFinalReport  FR GROUP BY AdjusterName

	IF @@ERROR <> 0
	    BEGIN
   	    -- SQL Server Error
   
   	     RAISERROR  ('99|%s', 16, 1, @ProcName)
	     RETURN
	END

        
 	               
      INSERT INTO @tmpCorrectTotals
	    SELECT AdjusterName, Count(*) from @tmpFinalReport TT Where ErrorAmt <> 0 Group By TT.AdjusterName
                      
	IF @@ERROR <> 0
	    BEGIN
   	    -- SQL Server Error
   
   	     RAISERROR  ('99|%s', 16, 1, @ProcName)
	     RETURN
	END

	 update @tmpFinalReport 
	  set Total_Inspections = t.Total_Inspections,
	        Total_Corrected_Data = ISNULL(ct.Total_Corrected_Data,0),
	        TPert_Corrected_Data = ISNULL(ct.Total_Corrected_Data,0) * 100 / t.Total_Inspections,    
	        Total_Error_Amt           = t.Total_Error_Amt,
	        Total_Rating	= t.Total_Rating 
               from @tmpFinalTotals t  
               left join  @tmpFinalReport fr ON ( fr.AdjusterName  = t.AdjusterName)
               left join @tmpCorrectTotals ct ON ( ct.AdjusterName = fr.AdjusterName)
               

	IF @@ERROR <> 0
	    BEGIN
   	    -- SQL Server Error
   
   	     RAISERROR  ('99|%s', 16, 1, @ProcName)
	     RETURN
	END

 

   SELECT TFR.*, @MonthName as MonthName , @RptYear as YearNumber ,  @DataWareHouseDate as DataWareDate  FROM @tmpFinalReport TFR

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptReinspectionReport' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptReinspectionReport TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/