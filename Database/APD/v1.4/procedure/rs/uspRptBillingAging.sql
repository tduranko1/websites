-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptBillingAging' AND type = 'P')
BEGIN
    DROP PROCEDURE uspRptBillingAging 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptBillingAging
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Compiles information to produce a key indicator report for APD
*
* PARAMETERS:   None  
*
* RESULT SET:   Key Indicator information
*
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptBillingAging
    @ReportDate AS udt_std_datetime = NULL
AS
BEGIN
    -- Set Database Options
    
    SET NOCOUNT ON 
    
    DECLARE @NotInvoiceStatusCD as varchar(5)
    DECLARE @AwaitingAuthStatusCD as varchar(5)
    DECLARE @now as datetime
    DECLARE @CRLF AS CHAR(2)

    SET @now = CURRENT_TIMESTAMP
    SET @CRLF = char(10) + char(13)

    SELECT @NotInvoiceStatusCD = Code
    FROM ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD')
    WHERE Name = 'APD'

    IF @@ERROR <> 0
    BEGIN

        RAISERROR('SQL Error', 16, 1)
        RETURN
    END

    SELECT @AwaitingAuthStatusCD = Code 
    FROM ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD')
    WHERE Name = 'Awaiting Client Authorization'

    IF @@ERROR <> 0
    BEGIN

        RAISERROR('SQL Error', 16, 1)
        RETURN
    END

    IF @NotInvoiceStatusCD IS NULL
    BEGIN
        RAISERROR('Cannot get the status code for APD', 16, 1)
        RETURN
    END

    IF @AwaitingAuthStatusCD IS NULL
    BEGIN
        RAISERROR('Cannot get the status code for Awaiting Client Authorization', 16, 1)
        RETURN
    END


    SELECT 'APD Billing Aging Report for ' + convert(varchar, @now, 101)
    SELECT ' ' AS ' '    
    SELECT ' ' AS 'Billed and not yet invoiced'

    SELECT        ca.LynxID, 
                  ins.Name as 'Insurance Company', 
                  (SELECT Name 
                   FROM ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD')
                   WHERE Code = inv.ItemTypeCD) as 'Item Type',
                  inv.StatusDate as 'Billing Date',
                  inv.Amount
    FROM          utb_claim c

    LEFT JOIN     utb_claim_aspect ca 
    ON            (c.LynxID = ca.LynxID)

    LEFT JOIN     utb_insurance ins 
    ON            (c.InsuranceCompanyID = ins.InsuranceCompanyID)

    LEFT JOIN     utb_invoice inv 
    ON            (ca.ClaimAspectID = inv.ClaimAspectID)
    
    LEFT JOIN     utb_claim_aspect_service_channel casc
    ON            (inv.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
    
    LEFT JOIN     utb_client_service_channel csc
    ON            (c.InsuranceCompanyID = csc.InsuranceCompanyID and casc.ServiceChannelCD = csc.ServiceChannelCD)

    WHERE         inv.SentToIngresDate is NULL
    AND           inv.StatusCD = @NotInvoiceStatusCD
    --            AND DATEDIFF(hour, inv.StatusDate, @now) < 24
    AND           inv.EnabledFlag = 1
    AND (
                 (inv.ItemTypeCD = 'I' AND DATEDIFF(day, inv.StatusDate, @now) < 365)
                 OR 
                 (inv.ItemTypeCD = 'F' AND DATEDIFF(day, inv.StatusDate, @now) BETWEEN 1 AND 365)
        ) 
    AND csc.InvoicingModelBillingCD <> 'B' -- exclude bulk billing

    ORDER BY inv.StatusDate
    
    
    IF @@ERROR <> 0
    BEGIN

        RAISERROR('SQL Error', 16, 1)
        RETURN
    END

    SELECT ' ' AS ' ' 

    SELECT ' ' AS 'Awaiting Authorization'

    SELECT        ca.LynxID, 
                  ins.Name as 'Insurance Company', 
                  (SELECT     Name 
                   FROM       ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD')
                   WHERE      Code = inv.ItemTypeCD) as 'Item Type',
                  inv.StatusDate as 'Billing Date',
                  inv.Amount

    FROM          utb_claim c

    LEFT JOIN     utb_claim_aspect ca 
    ON            (c.LynxID = ca.LynxID)

    LEFT JOIN     utb_insurance ins 
    ON            (c.InsuranceCompanyID = ins.InsuranceCompanyID)

    LEFT JOIN     utb_invoice inv 
    ON            (ca.ClaimAspectID = inv.ClaimAspectID)

    WHERE         inv.SentToIngresDate is NULL
      AND         inv.StatusCD = @AwaitingAuthStatusCD
      AND         DATEDIFF(hour, inv.StatusDate, @now) >= 24
      AND         inv.EnabledFlag = 1

    order by     inv.StatusDate

    IF @@ERROR <> 0
    BEGIN

        RAISERROR('SQL Error', 16, 1)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptBillingAging' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptBillingAging TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

