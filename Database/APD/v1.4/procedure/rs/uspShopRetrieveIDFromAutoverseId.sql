-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopRetrieveIDFromAutoverseId' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShopRetrieveIDFromAutoverseId 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspShopRetrieveIDFromAutoverseId
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspShopRetrieveIDFromAutoverseId
    @AutoverseID    udt_std_desc_mid
AS
BEGIN
    DECLARE @LynxSelectShopID udt_std_id_big
    
    SET @LynxSelectShopID = 0
    
    IF EXISTS (SELECT * FROM utb_shop_location WHERE AutoverseID = @AutoverseID)
    BEGIN
        SELECT @LynxSelectShopID = ShopLocationID 
        FROM utb_shop_location
        WHERE AutoverseID = @AutoverseID
    END
    
    RETURN @LynxSelectShopID        
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopRetrieveIDFromAutoverseId' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopRetrieveIDFromAutoverseId TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/