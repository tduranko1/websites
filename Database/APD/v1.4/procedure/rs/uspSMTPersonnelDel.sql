-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTPersonnelDel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTPersonnelDel 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspSMTPersonnelDel
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Removes a Shop Location Personnel relationship
*
* PARAMETERS:  
* (I) @EntityID                     The Shop or Business Info entity's unique identity
* (I) @EntityType                   Whether the EntityID refers to a Shop or Business Info entity
* (I) @PersonnelID                  The Personnel's unique identity
* (I) @SysLastUserID                The updating user identity
* (I) @RelationSysLastUpdatedDate   The last update date as string (VARCHAR(30))
*
* RESULT SET:
* EntityID                      The Entity's unique identity
* EntityType                    
* PersonnelID                   The Personnel unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTPersonnelDel
(
	@EntityID                    	      udt_std_int_big,
	@EntityType                         udt_std_cd,
  @PersonnelID                        udt_std_int_big,
  @SysLastUserID                      udt_std_id,
  @RelationSysLastUpdatedDate         VARCHAR(30),
  @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @tupdated_date      udt_std_datetime 
    DECLARE @temp_updated_date  udt_std_datetime 

    DECLARE @ProcName           varchar(30)       -- Used for raise error stmts 
    
    DECLARE @KeyDescription     udt_std_desc_short,
            @LogComment         udt_std_desc_long,
            @EntityName         udt_std_name,
            @EntityTypeName     udt_std_name,
            @Name               udt_std_name

    SET @ProcName = 'uspSMTPersonnelDel'

    
    -- Apply edits
    IF (@EntityType IS NULL) OR (@EntityType NOT IN ('B', 'S'))
        BEGIN
           -- Invalid EntityType
            RAISERROR('101|%s|@EntityType|%s', 16, 1, @ProcName, @EntityType)
            RETURN
        END
    END 
    
    
    IF (@EntityType = 'S')
    BEGIN
        
        IF (NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE ShopLocationID = @EntityID
                                                                          AND EnabledFlag = 1))
        BEGIN
           -- Invalid Shop 
            RAISERROR('101|%s|@EntityID|%u', 16, 1, @ProcName, @EntityID)
            RETURN
        END
    END
    ELSE
    BEGIN
        IF (NOT EXISTS (SELECT ShopID FROM utb_shop WHERE ShopID = @EntityID
                                                 AND EnabledFlag = 1))
        BEGIN
            -- Invalid Business Info
            RAISERROR('101|%s|@EntityID|%u', 16, 1, @ProcName, @EntityID)
            RETURN
        END
    END

    IF @PersonnelID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT PersonnelID FROM utb_personnel WHERE PersonnelID = @PersonnelID)
        BEGIN
           -- Invalid Personnel
            RAISERROR('101|%s|@PersonnelID|%u', 16, 1, @ProcName, @PersonnelID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Personnel
        RAISERROR('101|%s|@PersonnelID|%u', 16, 1, @ProcName, @PersonnelID)
        RETURN
    END

    -- Validate the updated date parameter
    
    IF @RelationSysLastUpdatedDate IS NULL OR 
       ISDATE(@RelationSysLastUpdatedDate) = 0
    BEGIN
        -- Invalid updated date value
        RAISERROR('101|%s|@RelationSysLastUpdatedDate|%s', 16, 1, @ProcName, @RelationSysLastUpdatedDate)
        RETURN
    END
    
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @RelationSysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    
    -- Check the date
    DECLARE @keyValues  AS VARCHAR(30)
    SET @keyValues = CONVERT(VARCHAR, @EntityID) + ',' + CONVERT(VARCHAR, @PersonnelID)
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF (@EntityType = 'S')
    BEGIN
        exec uspUtilityCheckLastUpdDate @RelationSysLastUpdatedDate, @SysLastUserID, 'utb_shop_location_personnel', @keyValues
    END
    ELSE
    BEGIN
        exec uspUtilityCheckLastUpdDate @RelationSysLastUpdatedDate, @SysLastUserID, 'utb_shop_personnel', @keyValues
    END

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopLocSpecDelDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF (@EntityType = 'S')
    BEGIN
      SET @EntityTypeName = 'Shop'
      SET @EntityName = (SELECT Name FROM dbo.utb_shop_location WHERE ShopLocationID = @EntityID)
       
      DELETE dbo.utb_shop_location_personnel
       WHERE ShopLocationID = @EntityID
         AND PersonnelID = @PersonnelID
         AND SysLastUpdatedDate = @tupdated_date
    END
    ELSE
    BEGIN
      SET @EntityTypeName = 'Business'
      SET @EntityName = (SELECT Name FROM dbo.utb_shop WHERE ShopID = @EntityID)
    
      DELETE dbo.utb_shop_personnel
       WHERE ShopID = @EntityID
         AND PersonnelID = @PersonnelID
         AND SysLastUpdatedDate = @tupdated_date
    END

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        IF (@EntityType = 'S')
            BEGIN     
            -- SQL Server Error
            RAISERROR('106|%s|utb_shop_location_personnel', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        ELSE
            BEGIN
            -- SQL Server Error
            RAISERROR('106|%s|utb_shop_personnel', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END
    
    SET @Name = (SELECT Name FROM dbo.utb_personnel WHERE PersonnelID = @PersonnelID)
    SET @KeyDescription = CONVERT(varchar(10), @PersonnelID) + ' - ' + @Name
    SET @LogComment = 'Personnel, ' + @Name + ', deleted from ' + @EntityTypeName + ', ' + @EntityName
    
    EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
            
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
        ROLLBACK TRANSACTION
        RETURN
    END 
    

    COMMIT TRANSACTION AdmShopLocSpecDelDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    --SELECT @EntityID AS EntityID, @PersonnelID AS PersonnelID

    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Personnel Level
            NULL AS [Personnel!2!EntityID],
            NULL AS [Personnel!2!EntityType],
            NULL AS [Personnel!2!PersonnelID]

    UNION ALL

    SELECT  2,
            1,
            NULL,
            -- Personnel Level
            @EntityID,
            @EntityType,
            @PersonnelID

    ORDER BY tag
    --    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
   

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTPersonnelDel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTPersonnelDel TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
