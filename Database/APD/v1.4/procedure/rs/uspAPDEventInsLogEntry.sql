-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAPDEventInsLogEntry' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAPDEventInsLogEntry 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspAPDEventInsLogEntry
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Logs events to the utb_apd_event_log
*
* PARAMETERS:  
*				 
* RESULT SET:
*   All data related to insurance client assignments
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspAPDEventInsLogEntry]
	@vEventType VARCHAR(50)
	, @vEventStatus VARCHAR(50)
	, @vEventDescription VARCHAR(100)
	, @vEventDetailedDescription VARCHAR(1000)
	, @vEventXML TEXT
	, @iRecID INT OUTPUT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @ProcName AS varchar(30)       -- Used for raise error stmts
    SET @ProcName = 'uspAPDEventInsLogEntry'

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now AS datetime 
	SET @now = CURRENT_TIMESTAMP
	
	SET @iRecID = 0

    -- Set Database options
    
    SET NOCOUNT ON

	BEGIN TRANSACTION EventLogInsTran1
		IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
			RAISERROR('99|%s', 16, 1, @ProcName)
			RETURN
		END
		
		INSERT INTO
			utb_apd_event_log
		(
			EventType
			, EventStatus
			, EventDescription
			, EventDetailedDescription
			, EventXML		
			, SysLastUserID
			, SysLastUpdatedDate
		)		
		VALUES
		(
			@vEventType
			, @vEventStatus
			, @vEventDescription
			, @vEventDetailedDescription
			, @vEventXML		
			, 0
			, @now
		)

		IF @error <> 0
		BEGIN
			-- Insertion failure
			RAISERROR('105|%s|utb_apd_event_log', 16, 1, @ProcName)
			ROLLBACK TRANSACTION
			RETURN
		END

		COMMIT TRANSACTION EventLogInsTran1
		IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
			RAISERROR('99|%s', 16, 1, @ProcName)
			RETURN
		END

SELECT @iRecID = SCOPE_IDENTITY()
RETURN @iRecID
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAPDEventInsLogEntry' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAPDEventInsLogEntry TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAPDEventInsLogEntry TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/