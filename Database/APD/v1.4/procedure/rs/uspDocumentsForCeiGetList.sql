-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentsForCeiGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDocumentsForCeiGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspDocumentsForCeiGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jorge Rodrigues
* FUNCTION:     Retrieve list of documents by date range
*
* PARAMETERS:  
* (I) @InsuranceCompanyIDList	InsuranceCompanyID comma delimited list
* (I) @FromDate                 Start date/time when documents were created
* (I) @ToDate                   End date/time when documents were created
* (I) @DocumentTypeList         Comma delimited list of documents to retrieve
* (I) @DocumentID               The Document ID to retrieve. Previous params are ignored
* (I) @LynxID                   The LynxID to retrieve. Previous params are ignored
* (I) @BaseLynxID               The base LynxID from which we started this wildcat project
*
* RESULT SET:
* recordset containing list of Document paths and name types
* or just a single document if the Document ID is passed in
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspDocumentsForCeiGetList
    @InsuranceCompanyIDList		varchar(30) = NULL,
    @FromDate               	varchar(30) = NULL,
    @ToDate                 	varchar(30) = NULL,
    @DocumentTypeList			varchar(200) = NULL,
    @DocumentID             	udt_std_int_big = NULL,
    @LynxID             	    udt_std_int_big = NULL,
    @BaseLynxID                 udt_std_int_big
AS
BEGIN
    DECLARE @ProcName               AS VARCHAR(30)       -- Used for raise error stmts 
	DECLARE @InsuranceCompanyName   AS VARCHAR(50)

    SET @ProcName = 'uspDocumentsForCeiGetList'
    

    -- Validate parameters
    
    IF ( @DocumentID = 0 OR LEN(RTRIM(LTRIM(@DocumentID))) = 0 ) SET @DocumentID = NULL

    IF  (@FromDate IS NOT NULL)
    BEGIN

	    -- Validate Insurance Company ID
	    IF (@InsuranceCompanyIDList IS NULL) OR
	       NOT EXISTS(SELECT InsuranceCompanyID
						FROM dbo.utb_insurance
						WHERE InsuranceCompanyID IN (SELECT Value FROM dbo.ufnUtilityParseString( @InsuranceCompanyIDList, ',', 1 )) )

		BEGIN
	        -- Invalid Insurance Company ID
	        
	        RAISERROR  ('101|%s|@InsuranceCompanyIDList|%n', 16, 1, @ProcName, @InsuranceCompanyIDList)
	        RETURN
	    END

	
	    -- Validate date range
	    IF @FromDate IS NOT NULL
	    BEGIN
	        IF ISDATE(@FromDate) = 1
	        BEGIN
	            -- Convert the value passed in into date format
	            SET @FromDate = CAST(@FromDate AS DATETIME)
	        END
	        ELSE
	        BEGIN
	            -- Invalid date value    
		    	RAISERROR('Invalid From Date format. Please reenter', 16, 1)
	            RETURN
	        END
	    END
	
	    IF @ToDate IS NOT NULL
	    BEGIN
	        IF ISDATE(@ToDate) = 1
	        BEGIN
	            -- Convert the value passed in into date format
	            SET @ToDate = CAST(@ToDate AS DATETIME)
	        END
	        ELSE
	        BEGIN
	            -- Invalid date value
		    	RAISERROR('Invalid To Date format. Please reenter', 16, 1)
	            RETURN
	        END
	    END


	    -- Validate Document Type List
	    IF LEN(LTRIM(RTRIM(@DocumentTypeList))) = 0 OR
	       NOT EXISTS(SELECT DocumentTypeID
						FROM dbo.utb_document_type
						WHERE DocumentTypeID IN (SELECT Value FROM dbo.ufnUtilityParseString( @DocumentTypeList, ',', 1 )) )

	    BEGIN
	        -- Invalid Document Type in the List
	    
	        RAISERROR('101|%s|@DocumentTypeList|%u', 16, 1, @ProcName, @DocumentTypeList)
	        RETURN        
	    END

	    -- Validate Insurance Company ID


	END
	ELSE IF @DocumentID IS NOT NULL
	BEGIN

	    -- Check to make sure a valid Document id was passed in
	    IF  (@DocumentID IS NOT NULL) AND
	        (NOT EXISTS(SELECT DocumentID FROM dbo.utb_document WHERE DocumentID = @DocumentID))
	    BEGIN
	        -- Invalid Document ID
	    
	        RAISERROR('101|%s|@DocumentID|%u', 16, 1, @ProcName, @DocumentID)
	        RETURN
	    END

	END
    ELSE IF @LynxID IS NOT NULL
    BEGIN
	    -- Check to make sure a valid Lynx id was passed in
	    IF  (@LynxID IS NOT NULL) AND
	        (NOT EXISTS(SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID))
	    BEGIN
	        -- Invalid Lynx ID
	    
	        RAISERROR('101|%s|@LynxID|%u', 16, 1, @ProcName, @LynxID)
	        RETURN
	    END
	    -- Validate Document Type List
	    IF LEN(LTRIM(RTRIM(@DocumentTypeList))) = 0 OR
	       NOT EXISTS(SELECT DocumentTypeID
						FROM dbo.utb_document_type
						WHERE DocumentTypeID IN (SELECT Value FROM dbo.ufnUtilityParseString( @DocumentTypeList, ',', 1 )) )

	    BEGIN
	        -- Invalid Document Type in the List
	    
	        RAISERROR('101|%s|@DocumentTypeList|%u', 16, 1, @ProcName, @DocumentTypeList)
	        RETURN        
	    END
    END
    
    -- Let's retrieve the documents
    -- Date Range selection
    IF  (@FromDate IS NOT NULL)
    BEGIN
    
		SELECT d.DocumentID,
               ca.ClaimAspectID,
               --a.AssignmentID,
               --a.ProgramTypeCD,
			   d.CreatedDate,
			   isNull(d.ImageLocation, '') as ImageLocation,
			   dt.DocumentTypeID,
			   dt.Name AS DocumentName,
               isNull(d.SupplementSeqNumber, 0) as SupplementSeqNumber,
			   i.InsuranceCompanyID,
			   i.Name AS InsuranceCompanyName,
			   c.LynxID,
               ca.ClaimAspectNumber,
			   isNull(c.ClientClaimNumber, '') AS ClaimNumber,
			   c.LossDate
		
		FROM utb_document d
		LEFT JOIN utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID )
		LEFT JOIN utb_claim_aspect_service_channel_document cascd ON (d.DocumentID = cascd.DocumentID )--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
		LEFT JOIN utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID ) --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
		LEFT JOIN utb_claim c ON (c.LynxID = ca.LynxID )
		LEFT JOIN utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID )
		--LEFT JOIN utb_claim_coverage cov ON (c.LynxID = cov.LynxID ) remarked-off on 02-08-2007 M.A.
		--LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON ca.ClaimAspectID = casc.ClaimAspectID --Project:210474 APD Added the reference when we did the code merge M.A.20061129
        LEFT JOIN utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID)
		INNER JOIN dbo.ufnUtilityParseString( @DocumentTypeList, ',', 1 ) dt1 ON (dt.DocumentTypeID = dt1.Value)
		WHERE d.CreatedDate between @FromDate and @ToDate
		  AND d.EnabledFlag = 1
		  AND c.InsuranceCompanyID IN (SELECT Value FROM dbo.ufnUtilityParseString( @InsuranceCompanyIDList, ',', 1 ))
          AND a.AssignmentID = (SELECT top 1 AssignmentID 
                                FROM utb_assignment a1
								INNER JOIN utb_Claim_Aspect_Service_Channel casc1 ON a1.ClaimAspectServiceChannelID = casc1.ClaimAspectServiceChannelID --Project:210474 APD Added the reference when we did the code merge M.A.20061129
                                WHERE casc1.ClaimAspectID = ca.ClaimAspectID 
                                order by AssignmentDate desc) -- latest assignment
          AND a.ProgramTypeCD <> 'CEI'
          AND ca.LynxID > @BaseLynxID
          and casc.EnabledFlag = 1 --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227

	    ORDER BY c.InsuranceCompanyID DESC, c.LynxID ASC, d.CreatedDate ASC

	END

    -- If Document ID is passed, just retrieve the document
    IF @DocumentID IS NOT NULL
	BEGIN

		SELECT d.DocumentID,
               ca.ClaimAspectID,
			   d.CreatedDate,
			   isNull(d.ImageLocation, '') as ImageLocation,
			   dt.DocumentTypeID,
			   dt.Name AS DocumentName,
               isNull(d.SupplementSeqNumber, 0) as SupplementSeqNumber,
			   i.InsuranceCompanyID,
			   i.Name AS InsuranceCompanyName,
			   c.LynxID,
               ca.ClaimAspectNumber,
			   isNull(c.ClientClaimNumber, '') AS ClaimNumber,  --Project:210474 APD Modified when we did the code merge M.A.20061120
			   c.LossDate
		
		FROM utb_document d
		LEFT JOIN utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID )
		LEFT JOIN utb_claim_aspect_Service_Channel_document cascd ON (d.DocumentID = cascd.DocumentID )--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
        LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc on casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
		LEFT JOIN utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID )
		LEFT JOIN utb_claim c ON (c.LynxID = ca.LynxID )
		LEFT JOIN utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID )
		--LEFT JOIN utb_claim_coverage cov ON (c.LynxID = cov.LynxID ) Remarked-off 02-08-2007 M.A.
		
		WHERE d.DocumentID = @DocumentID
		  AND d.EnabledFlag = 1
          AND ca.LynxID > @BaseLynxID

	    ORDER BY c.InsuranceCompanyID DESC, c.LynxID ASC, d.CreatedDate ASC
	
	END
    
    -- If Lynx ID is passed, just retrieve the document
    IF @LynxID IS NOT NULL
	BEGIN

		SELECT d.DocumentID,
               ca.ClaimAspectID,
			   d.CreatedDate,
			   isNull(d.ImageLocation, '') as ImageLocation,
			   dt.DocumentTypeID,
			   dt.Name AS DocumentName,
               isNull(d.SupplementSeqNumber, 0) as SupplementSeqNumber,
			   i.InsuranceCompanyID,
			   i.Name AS InsuranceCompanyName,
			   c.LynxID,
               ca.ClaimAspectNumber,
			   isNull(c.ClientClaimNumber, '') AS ClaimNumber,
			   c.LossDate
		
		FROM utb_claim c
		LEFT JOIN utb_claim_aspect ca ON (c.LynxID = ca.LynxID )
		LEFT OUTER JOIN utb_Claim_Aspect_Service_Channel casc ON ca.ClaimAspectID = casc.ClaimAspectID --Project:210474 APD Added the reference when we did the code merge M.A.20061129
        LEFT JOIN utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID)
		LEFT JOIN utb_claim_aspect_Service_Channel_document cascd ON (casc.ClaimAspectServiceChannelID = cascd.ClaimAspectServiceChannelID )--Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20061227
		LEFT JOIN utb_document d ON (cascd.DocumentID = d.DocumentID )
		LEFT JOIN utb_document_type dt ON (d.DocumentTypeID = dt.DocumentTypeID )
		LEFT JOIN utb_insurance i ON (c.InsuranceCompanyID = i.InsuranceCompanyID )
		--LEFT JOIN utb_claim_coverage cov ON (c.LynxID = cov.LynxID ) --remarked-off 02-08-2007
		INNER JOIN dbo.ufnUtilityParseString( @DocumentTypeList, ',', 1 ) dt1 ON (dt.DocumentTypeID = dt1.Value)
		
		WHERE c.LynxID = @LynxID
		  AND d.EnabledFlag = 1 AND a.assignmentsequencenumber = 1
          AND a.AssignmentID = (SELECT top 1 AssignmentID 
                                FROM utb_assignment a1 
								INNER JOIN utb_Claim_Aspect_Service_Channel casc1 ON a1.ClaimAspectServiceChannelID = casc1.ClaimAspectServiceChannelID --Project:210474 APD Added the reference when we did the code merge M.A.20061129
                                WHERE casc1.ClaimAspectID = ca.ClaimAspectID 
                                order by AssignmentDate desc) -- latest assignment
          AND a.ProgramTypeCD <> 'CEI'
          AND ca.LynxID > @BaseLynxID

	    ORDER BY c.InsuranceCompanyID DESC, c.LynxID ASC, d.CreatedDate ASC
	
	END
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDocumentsForCeiGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDocumentsForCeiGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/