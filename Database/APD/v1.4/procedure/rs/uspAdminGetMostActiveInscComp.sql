-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminGetMostActiveInscComp' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminGetMostActiveInscComp 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminGetMostActiveInscComp
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets the most active insurance companies base on the last 100 claims
*
* PARAMETERS:  
*				
* RESULT SET:
*				Record Set
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminGetMostActiveInscComp
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	/*******************************/
	/* Hold SQL Snapshot           */
	/*******************************/
	CREATE TABLE #APDMostActiveInscCoTmp
	(
	   InscCompanyName VARCHAR(100)
	   , NumberOfClaimsProcessed INT
	)

	/*********************************/
	/* Get Concurrent Users Snapshot */
	/*********************************/
	DECLARE db_cursor CURSOR FOR 
		SELECT 
			InsuranceCompanyID
			, [Name]
		FROM 
			utb_insurance
		WHERE
			EnabledFlag = 1
				
	DECLARE @InsuranceCompanyID INT
	DECLARE @InsuranceCompanyName VARCHAR(100)
				
	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO 
		@InsuranceCompanyID
		, @InsuranceCompanyName
		
		WHILE @@FETCH_STATUS = 0  
		BEGIN  		
			INSERT INTO #APDMostActiveInscCoTmp
			SELECT
				@InsuranceCompanyName
				, COUNT(LynxID)
			FROM 
				utb_claim
			WHERE
				LynxID IN	
				(
					SELECT TOP 100 
						LynxID
					FROM 
						utb_claim 
					ORDER BY SysLastUpdatedDate DESC		
				)
				AND InsuranceCompanyID = @InsuranceCompanyID 

			FETCH NEXT FROM db_cursor INTO 	
				@InsuranceCompanyID
				, @InsuranceCompanyName
		END  

	CLOSE db_cursor  
	DEALLOCATE db_cursor 					

	SELECT * FROM #APDMostActiveInscCoTmp ORDER BY NumberOfClaimsProcessed DESC

	DROP TABLE #APDMostActiveInscCoTmp
	
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminGetMostActiveInscComp' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminGetMostActiveInscComp TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAdminGetMostActiveInscComp TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/