-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceShopProcess' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceShopProcess 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspChoiceShopProcess
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Create/Updates HQ Choice shops into the APD Shop tables.
* Date:			21Oct2015
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	21Oct2015 - TVD - Initial development
*				21Jun2016 - Bug Fix for expired userid used in choice shop update.
*				11Jul2016 - TVD - Redesigning this process to not update all the info.  just the GUID.  Added Emailing on New
*				29Jul2016 - TVD - Corrected the email address
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspChoiceShopProcess]
	 @NewShopFlag			   bit
	, @HQBusinessTypeCD		   udt_std_cd
	, @HQAddress1              udt_addr_line_1
	, @HQAddress2              udt_addr_line_2
	, @HQAddressCity           udt_addr_city
	, @HQAddressState          udt_addr_state
	, @HQAddressZip            udt_addr_zip_code
	, @HQEmailAddress          udt_web_email
	, @HQEnabledFlag           udt_enabled_flag
	, @HQFaxAreaCode           udt_ph_area_code
	, @HQFaxExchangeNumber     udt_ph_exchange_number
	, @HQFaxUnitNumber         udt_ph_unit_number
	, @HQFedTaxId              udt_fed_tax_id
	, @HQName                  udt_std_name
	, @HQPhoneAreaCode         udt_ph_area_code
	, @HQPhoneExchangeNumber   udt_ph_exchange_number
	, @HQPhoneUnitNumber       udt_ph_unit_number
	, @HQSysLastUserID         udt_std_id
	, @HQApplicationCD         udt_std_cd
	, @HQShopUID			   VARCHAR(50)
	, @HQShopPartnerID		   VARCHAR(50)
	, @ReturnShopLocationID	   udt_std_id OUTPUT
AS
BEGIN
    SET NOCOUNT ON
 	
 	---------------------------------------
	-- Declare Vars
	---------------------------------------
	DECLARE @UpdateShopID								INT
	DECLARE @UpdateShopBillingID						INT
	DECLARE @UpdateShopLocationID						INT
	DECLARE @UpdateSysLastUserID						INT
	DECLARE @UpdateSysLastUpdatedDate					VARCHAR(30)
	DECLARE @UpdateShopSysLastUpdatedDate				VARCHAR(30)
	DECLARE @UpdateShopSysLastUID						INT
	DECLARE @UpdateShopLocationSysLastUpdatedDate		VARCHAR(30)
	DECLARE @UpdateShopLocationSysLastUID				INT
	DECLARE @UpdateShopLocationHoursSysLastUpdatedDate	VARCHAR(30)
	DECLARE @UpdateShopLocationHoursSysLastUID			INT

	DECLARE @LynxShopID							INT
	DECLARE @LynxShopBillingID					INT
	DECLARE @LynxShopLocationID					INT
	DECLARE @LynxSysLastUserID					INT
	DECLARE @LynxSysLastUpdatedDate				VARCHAR(30)
	DECLARE @LynxShopHoursSysLastUpdatedDate	VARCHAR(30)

	DECLARE @ProcName							VARCHAR(50)
	DECLARE @LogComment							VARCHAR(100)
	--DECLARE @ReturnShopLocationID				VARCHAR(30)
	DECLARE @Now								DATETIME
    DECLARE @Debug								udt_std_flag

	DECLARE @NewShopID							INT
	DECLARE @NewBillingID						INT
	DECLARE @NewShopLocationID					INT
	DECLARE @bMatchedShopLocationID				BIT
	DECLARE @AutoNewShopFlag					INT

	---------------------------------------
	-- Email Params
	---------------------------------------
	DECLARE @SenderName							VARCHAR(100)
	DECLARE @SenderAddress						VARCHAR(100)
	DECLARE @RecipientName						VARCHAR(100)
	DECLARE @RecipientAddress					VARCHAR(100)
	DECLARE @Subject							VARCHAR(200)
	DECLARE @Body 								VARCHAR(8000)
	DECLARE @MailServer							VARCHAR(100) = 'localhost'

	SET @SenderName = 'LynxChoiceShopCreation'
	SET @SenderAddress = 'LynxChoiceShopCreation@lynxservices.com'
	SET @RecipientName = 'Shop Maintenance'
	SET @RecipientAddress = 'apd@lynxservices.com;fieldsupport@lynxservices.com'
	--SET @RecipientAddress = 'tduranko@lynxservices.com'

	---------------------------------------
	-- Init Params
	---------------------------------------
    SET @Debug = 1 
	SET @ProcName = 'uspChoiceShopProcess'
	SET @Now = CURRENT_TIMESTAMP
	SET @bMatchedShopLocationID = 0
	SET @ReturnShopLocationID = 0
	SET @AutoNewShopFlag = 0

	---------------------------------------
	-- Validations
	---------------------------------------
	IF @HQPhoneAreaCode IS NULL
	BEGIN
		-- Make fax number the same as phone number if null
		SET @HQPhoneAreaCode = 111
		SET @HQPhoneExchangeNumber = 111
		SET @HQPhoneUnitNumber = 1111
	END

	IF @HQFaxAreaCode IS NULL
	BEGIN
		-- Make fax number the same as phone number if null
		SET @HQFaxAreaCode = @HQPhoneAreaCode
		SET @HQFaxExchangeNumber = @HQPhoneExchangeNumber
		SET @HQFaxUnitNumber = @HQPhoneUnitNumber
	END

    IF @Debug = 1
    BEGIN
        PRINT 'Passed in Parameters:'
		PRINT '    @NewShopFlag = ' + CONVERT(VARCHAR(1), @NewShopFlag)
		PRINT '    @HQBusinessTypeCD = ' + @HQBusinessTypeCD
		PRINT '    @HQAddress1 = ' + @HQAddress1
		PRINT '    @HQAddress2 = ' + @HQAddress2
		PRINT '    @HQAddressCity = ' + @HQAddressCity
		PRINT '    @HQAddressState = ' + @HQAddressState
		PRINT '    @HQAddressZip = ' + CONVERT(VARCHAR(12), @HQAddressZip)
		PRINT '    @HQEmailAddress = ' + @HQEmailAddress
		PRINT '    @HQEnabledFlag = ' + CONVERT(VARCHAR(1), @HQEnabledFlag)
		PRINT '    @HQFaxAreaCode = ' + CONVERT(VARCHAR(3), @HQFaxAreaCode)
		PRINT '    @HQFaxExchangeNumber = ' + CONVERT(VARCHAR(3), @HQFaxExchangeNumber)
		PRINT '    @HQFaxUnitNumber = ' + CONVERT(VARCHAR(4), @HQFaxUnitNumber)
		PRINT '    @HQFedTaxId = ' + CONVERT(VARCHAR(15), @HQFedTaxId)
		PRINT '    @HQName = ' + @HQName
		PRINT '    @HQPhoneAreaCode = ' + CONVERT(VARCHAR(3), @HQPhoneAreaCode)
		PRINT '    @HQPhoneExchangeNumber = ' + CONVERT(VARCHAR(3), @HQPhoneExchangeNumber)
		PRINT '    @HQPhoneUnitNumber = ' + CONVERT(VARCHAR(4), @HQPhoneUnitNumber)
		PRINT '    @HQSysLastUserID = ' + CONVERT(VARCHAR(10), @HQSysLastUserID)
		PRINT '    @HQApplicationCD = ' + @HQApplicationCD
		PRINT '    @HQShopUID = ' + CONVERT(VARCHAR(50), @HQShopUID)
		PRINT '    @HQShopPartnerID = ' + @HQShopPartnerID
    END

	------------------------------------------------------
	-- Check if @NewShopFlag = 1
	------------------------------------------------------
	IF @NewShopFlag=0
	BEGIN
		------------------------------------------------------
		-- Check if @HQShopID exists in DB
		------------------------------------------------------
		IF EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE PPGCTSId = @HQShopUID)
		BEGIN
			IF @Debug = 1
			BEGIN
				-- Do Nothing
				PRINT 'Process Type: ALREADY UPDATED'
			END

			-------------------------------------
			-- Return ShopLocationID
			-------------------------------------
			SELECT @ReturnShopLocationID = ShopLocationID FROM utb_shop_location WHERE PPGCTSId = @HQShopUID
			SELECT @ReturnShopLocationID AS ShopLocationID
			RETURN @ReturnShopLocationID

		END
		ELSE
		BEGIN
			------------------------------------------------------
			-- Check if @HQShopPartnerID is NOT NULL
			------------------------------------------------------
			IF LEN(@HQShopPartnerID) > 0
			BEGIN
				IF @Debug = 1
				BEGIN
					-- Do Nothing
					PRINT 'Process Type: UPDATED'
				END

				------------------------------------------------------
				-- UPDATE SHOP
				------------------------------------------------------
				BEGIN TRANSACTION ChoiceShopProcessTran1
				IF @@ERROR <> 0
				BEGIN
				   -- SQL Server Error
				   RAISERROR('99|%s', 16, 1, @ProcName)
		   		   ROLLBACK TRANSACTION
				   -- RETURN
				END

					UPDATE utb_shop_location SET PPGCTSId = @HQShopUID WHERE ShopLocationID = @HQShopPartnerID

				COMMIT TRANSACTION ChoiceShopProcessTran1    
				IF @@ERROR <> 0
				BEGIN
				   -- SQL Server Error
				   RAISERROR('99|%s', 16, 1, @ProcName)
		 		   ROLLBACK TRANSACTION
				   RETURN
				END

				-------------------------------------
				-- Make sure the Shop Location exists 
				-- by getting the ShopLocationID
				-------------------------------------
				SET @ReturnShopLocationID = @HQShopPartnerID
				SELECT @ReturnShopLocationID AS ShopLocationID
				RETURN @ReturnShopLocationID
			END
			ELSE
			BEGIN
				------------------------------------------------------
				-- NEW SHOP
				------------------------------------------------------
				SET @AutoNewShopFlag=1
			END
		END
	END

	------------------------------------------------------
	-- NEW SHOP
	------------------------------------------------------
	IF @NewShopFlag=1 OR @AutoNewShopFlag = 1
	BEGIN
		IF @Debug = 1
		BEGIN
			PRINT 'Process Type: NEW SHOP'
			PRINT 'Process Type: INSERT'
		END

		-- Begin Update(s)
		BEGIN TRANSACTION ChoiceShopProcessTran2
		IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
			RAISERROR('99|%s', 16, 1, @ProcName)
			RETURN
		END

		------------------------------
		-- Create the Business	
		------------------------------
		-- Check if exists before creating new.
		IF NOT EXISTS (SELECT ShopID FROM utb_shop WHERE Name = @HQName AND Address1 = @HQAddress1 AND Address2 = @HQAddress2 AND AddressCity = @HQAddressCity AND AddressState = @HQAddressState AND AddressZip = @HQAddressZip AND EnabledFlag = 1)
		BEGIN
			EXEC uspSMTBusinessInfoInsDetail @BusinessTypeCD=@HQBusinessTypeCD, @Address1=@HQAddress1, @Address2=@HQAddress2, @AddressCity=@HQAddressCity, @AddressState=@HQAddressState, @AddressZip=@HQAddressZip, @EnabledFlag=@HQEnabledFlag, @FaxAreaCode=@HQFaxAreaCode, @FaxExchangeNumber=@HQFaxExchangeNumber, @FaxUnitNumber=@HQFaxUnitNumber, @FedTaxId=NULL, @Name=@HQName, @PhoneAreaCode=@HQPhoneAreaCode, @PhoneExchangeNumber=@HQPhoneExchangeNumber, @PhoneUnitNumber=@HQPhoneUnitNumber, @SysLastUserID=@HQSysLastUserID, @ApplicationCD=@HQApplicationCD, @ShopID=@NewShopID OUTPUT
		END 

		IF @@ERROR <> 0
		BEGIN
		  SET @LogComment = 'Error(uspSMTBusinessInfoInsDetail): INSERT of business data failed.  @@ERROR: ' + CONVERT(VARCHAR(15), @@ERROR)
		  --EXEC xp_logevent 90000, @LogComment, Informational

		  IF @Debug = 1
		  BEGIN
			PRINT 'Debug: EXEC uspSMTBusinessInfoInsDetail, @@ERROR: ' + CONVERT(VARCHAR(50),@@ERROR)
		  END
		
		 -- SQL Server Error
		  RAISERROR  ('99|%s', 16, 1, @LogComment)
		  ROLLBACK TRANSACTION
		  RETURN
		END
		
		-------------------------------------
		-- Make sure the Business exists by 
		-- getting the BusinessInfoID
		-------------------------------------
		SELECT @LynxShopID = ShopID FROM utb_shop WHERE Name = @HQName AND Address1 = @HQAddress1 AND AddressCity = @HQAddressCity
		--SELECT ShopID FROM utb_shop WHERE Name = @HQName AND Address1 = @HQAddress1 AND Address2 = @HQAddress2 AND AddressCity = @HQAddressCity AND AddressState = @HQAddressState AND AddressZip = @HQAddressZip AND EnabledFlag = 1
		--SELECT @LynxShopID

		IF @Debug = 1
		BEGIN
			PRINT 'Debug: @NewShopID: ' + CONVERT(VARCHAR(50), @NewShopID)
		END

		-------------------------------------
		-- Create the Billing Address	
		-------------------------------------
		IF NOT EXISTS (SELECT BillingID FROM utb_billing WHERE Name = @HQName AND Address1 = @HQAddress1 AND Address2 = @HQAddress2 AND AddressCity = @HQAddressCity AND AddressState = @HQAddressState AND AddressZip = @HQAddressZip)
		BEGIN
			EXEC uspSMTShopBillingInsDetail @Address1=@HQAddress1, @Address2=@HQAddress2, @AddressCity=@HQAddressCity, @AddressState=@HQAddressState, @AddressZip=@HQAddressZip, @EFTContractSignedFlag=0, @FaxAreaCode=@HQFaxAreaCode, @FaxExchangeNumber=@HQFaxExchangeNumber, @FaxUnitNumber=@HQFaxUnitNumber, @Name=@HQName, @PhoneAreaCode=@HQPhoneAreaCode, @PhoneExchangeNumber=@HQPhoneExchangeNumber, @PhoneUnitNumber=@HQPhoneUnitNumber, @SysLastUserID=@HQSysLastUserID, @ApplicationCD=@HQApplicationCD, @BillingID=@NewBillingID OUTPUT
		END
		
		IF @@ERROR <> 0
		BEGIN
		  SET @LogComment = 'Error(uspSMTShopBillingInsDetail): INSERT of Billing data failed.  @@ERROR: ' + CONVERT(VARCHAR(15), @@ERROR)
		  --EXEC xp_logevent 90000, @LogComment, Informational

		  IF @Debug = 1
		  BEGIN
			PRINT 'Debug: EXEC uspSMTShopBillingInsDetail, @@ERROR: ' + CONVERT(VARCHAR(50),@@ERROR)
		  END
		
		 -- SQL Server Error
		  RAISERROR  ('99|%s', 16, 1, @LogComment)
		  ROLLBACK TRANSACTION
		  RETURN
		END

		-------------------------------------
		-- Make sure the Billing exists by 
		-- getting the ShopBillingID
		-------------------------------------
		--SELECT @LynxShopBillingID = BillingID FROM utb_billing WHERE Name = @HQName AND Address1 = @HQAddress1 AND AddressCity = @HQAddressCity AND ShopID = @LynxShopID
		--SELECT @LynxShopBillingID = BillingID FROM utb_billing WHERE Name = @HQName AND Address1 = @HQAddress1 AND Address2 = @HQAddress2 AND AddressCity = @HQAddressCity AND AddressState = @HQAddressState AND AddressZip = @HQAddressZip
		--SELECT @LynxShopBillingID

		IF @Debug = 1
		BEGIN
			PRINT 'Debug: @NewBillingID: ' + CONVERT(VARCHAR(50),@NewBillingID)
		END

		-------------------------------------
		-- Create the Shop using the ShopID 
		-- from above
		-------------------------------------
		IF NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE Name = @HQName AND Address1 = @HQAddress1 AND Address2 = @HQAddress2 AND AddressCity = @HQAddressCity AND AddressState = @HQAddressState AND AddressZip = @HQAddressZip AND EnabledFlag = 1 AND BillingID = @LynxShopBillingID AND ShopID = @LynxShopID)
		BEGIN
			EXEC uspSMTShopInfoInsDetail @ShopBillingID=@NewBillingID,@PreferredCommunicationMethodID=17,@PreferredEstimatePackageID=5,@BusinessInfoID=@NewShopID,@Address1=@HQAddress1,@Address2=@HQAddress2,@AddressCity=@HQAddressCity,@AddressState=@HQAddressState,@AddressZip=@HQAddressZip,@AvailableForSelectionFlag=0,@CertifiedFirstFlag=0,@DataReviewStatusCD='N',@EmailAddress=@HQEmailAddress,@EnabledFlag=1,@FaxAreaCode=@HQFaxAreaCode, @FaxExchangeNumber=@HQFaxExchangeNumber, @FaxUnitNumber=@HQFaxUnitNumber,@Name=@HQName,@PhoneAreaCode=@HQPhoneAreaCode, @PhoneExchangeNumber=@HQPhoneExchangeNumber, @PhoneUnitNumber=@HQPhoneUnitNumber,@PPGCTSCustomerFlag=0,@PPGCTSFlag=0,@PPGCTSId=@HQShopUID,@ProgramFlag=0,@ReferralFlag=0,@SysLastUserID=@HQSysLastUserID,@ApplicationCD=@HQApplicationCD, @ShopLocationID=@NewShopLocationID OUTPUT
		END
		
		IF @@ERROR <> 0
		BEGIN
		  SET @LogComment = 'Error(uspSMTShopInfoInsDetail): INSERT of Shop data failed.  @@ERROR: ' + CONVERT(VARCHAR(15), @@ERROR)
		  --EXEC xp_logevent 90000, @LogComment, Informational

		  IF @Debug = 1
		  BEGIN
			PRINT 'Debug: EXEC uspSMTShopInfoInsDetail, @@ERROR: ' + CONVERT(VARCHAR(50),@@ERROR)
		  END
		
		 -- SQL Server Error
		  RAISERROR  ('99|%s', 16, 1, @LogComment)
		  ROLLBACK TRANSACTION
		  RETURN
		END

		--SELECT TOP 1 @ReturnShopLocationID=ShopLocationID FROM utb_shop_location WHERE ShopID = @NewShopLocationID

		-------------------------------------
		-- Make sure the Shop Location exists 
		-- by getting the ShopLocationID
		-------------------------------------
		--SELECT ShopLocationID FROM utb_shop_location WHERE PPGCTSId = @HQShopPartnerID AND BillingID = @LynxShopBillingID AND ShopID = @LynxShopID
		--SELECT @LynxShopLocationID = ShopLocationID FROM utb_shop_location WHERE Name = @HQName AND Address1 = @HQAddress1 AND Address2 = @HQAddress2 AND AddressCity = @HQAddressCity AND AddressState = @HQAddressState AND AddressZip = @HQAddressZip AND EnabledFlag = 1 AND BillingID = @LynxShopBillingID AND ShopID = @LynxShopID
		--SELECT @LynxShopLocationID

		IF @Debug = 1
		BEGIN
			PRINT 'Debug: @NewShopLocationID: ' + CONVERT(VARCHAR(50),@NewShopLocationID)
			PRINT 'Debug: @HQSysLastUserID: ' + CONVERT(VARCHAR(50),@HQSysLastUserID)
			PRINT 'Debug: @HQApplicationCD: ' + @HQApplicationCD
		END

		-------------------------------------
		-- Create the Shop Hours using the ShopID 
		-- from above
		-------------------------------------
		IF NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location_hours WHERE ShopLocationID = @LynxShopLocationID)
		BEGIN
			EXEC uspSMTShopHoursInsDetail @ShopID=@NewShopLocationID,@SysLastUserID=@HQSysLastUserID,@ApplicationCD=@HQApplicationCD
		END
		
		IF @@ERROR <> 0
		BEGIN
		  SET @LogComment = 'Error(uspSMTShopHoursInsDetail): INSERT of Shop Hours data failed.  @@ERROR: ' + CONVERT(VARCHAR(15), @@ERROR)
		  --EXEC xp_logevent 90000, @LogComment, Informational

		  IF @Debug = 1
		  BEGIN
			PRINT 'Debug: EXEC uspSMTShopHoursInsDetail, @@ERROR: ' + CONVERT(VARCHAR(50),@@ERROR)
		  END
		
		 -- SQL Server Error
		  RAISERROR  ('99|%s', 16, 1, @LogComment)
		  ROLLBACK TRANSACTION
		  RETURN
		END

		-------------------------------------
		-- Create the Shop Location Personel 
		-- using the ShopID from above
		-------------------------------------
		IF NOT EXISTS (
			SELECT 
				p.PersonnelID 
			FROM 
				utb_personnel p
				INNER JOIN utb_shop_location_personnel slp
				ON slp.PersonnelID = p.PersonnelID
			WHERE 
				p.Name = 'Choice Shop Tech'
				AND slp.ShopLocationID = @LynxShopLocationID
			)
		BEGIN
			EXEC uspSMTPersonnelInsDetail @EntityID = @NewShopLocationID, @EntityType = 'S', @PreferredContactMethodID = 3, @MinorityFlag = 0, @Name = 'Choice Shop Tech', @ShopManagerFlag = 0, @SysLastUserID = 0, @ApplicationCD = 'APD'

			IF @@ERROR <> 0
			BEGIN
			  SET @LogComment = 'Error(uspSMTPersonnelInsDetail): INSERT of Shop Personnel data failed.  @@ERROR: ' + CONVERT(VARCHAR(15), @@ERROR)

			  IF @Debug = 1
			  BEGIN
				PRINT 'Debug: EXEC uspSMTPersonnelInsDetail, @@ERROR: ' + CONVERT(VARCHAR(50),@@ERROR)
			  END
			
			 -- SQL Server Error
			  RAISERROR  ('99|%s', 16, 1, @LogComment)
			  ROLLBACK TRANSACTION
			  RETURN
			END
		END


		IF @NewShopLocationID IS NULL
		BEGIN
		  SET @LogComment = 'Error(@LynxShopLocationID): No ShopLocationID created.  @LynxShopLocationID: ' + CONVERT(VARCHAR(15), @LynxShopLocationID)
		  --EXEC xp_logevent 90000, @LogComment, Informational

		 -- SQL Server Error

		  RAISERROR  ('99|%s', 16, 1, @LogComment)
		  ROLLBACK TRANSACTION
		  RETURN
		END

		--SELECT ShopLocationID FROM utb_shop_location WHERE ShopID = @NewShopLocationID

		----  End of Audit Log code   -----------------------------------------------------------------------------

		COMMIT TRANSACTION ChoiceShopProcessTran2    
		IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
			RAISERROR('99|%s', 16, 1, @ProcName)
		    ROLLBACK TRANSACTION
			RETURN
		END

		-------------------------------------
		-- Email Shop Maint that a new shop
		-- was created 
		-------------------------------------
		SET @Subject = 'LYNX Choice � APD Shop Record Created: ' + CONVERT(VARCHAR(10), @NewShopLocationID)
		SET @Body = N'APD / Field Support,
		 
A new LYNX Choice assignment has triggered a new APD Shop Record creation.

The below Shop was auto-created in APD during the new assignment process.  
Created On: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + 
'
Shop Location ID: ' +  ISNULL(CONVERT(VARCHAR(10), @NewShopLocationID),' ') +
'
Shop ID (Legal Entity): ' + ISNULL(CONVERT(VARCHAR(10), @LynxShopID),' ') +
'
HQ QUID Number: ' + ISNULL(@HQShopUID,' ') +
'
Shop Name: ' + ISNULL(@HQName,' ') +
'
Shop Address: ' + ISNULL(@HQAddress1 ,' ') + ', ' +	ISNULL(@HQAddressCity,' ') + ', ' + ISNULL(@HQAddressState,' ') + ' ' + ISNULL(CONVERT(VARCHAR(15), @HQAddressZip),' ') +
'
Shop Telephone Number: ' + ISNULL(CONVERT(VARCHAR(3), @HQPhoneAreaCode),' ') + '-' + ISNULL(CONVERT(VARCHAR(3), @HQPhoneExchangeNumber),' ') + '-' + ISNULL(CONVERT(VARCHAR(4), @HQPhoneUnitNumber),' ') +
'
Shop Fax Number: ' + ISNULL(CONVERT(VARCHAR(3), @HQFaxAreaCode),' ') + '-' + ISNULL(CONVERT(VARCHAR(3), @HQFaxExchangeNumber),' ') + '-' + ISNULL(CONVERT(VARCHAR(4), @HQFaxUnitNumber),' ') +
'
Shop Email Address: ' + ISNULL(@HQEmailAddress,' ') +
'

Please review and process as needed.'

        EXEC msdb..sp_send_dbmail
            @recipients = @RecipientAddress
            , @blind_copy_recipients = NULL
            , @query = NULL
            , @subject = @Subject
			, @body = @Body
            , @query_attachment_filename = NULL
            , @attach_query_result_as_file = 0
            , @query_result_width = 8000

		-------------------------------------
		-- Make sure the Shop Location exists 
		-- by getting the ShopLocationID
		-------------------------------------
		SET @ReturnShopLocationID = @NewShopLocationID
		SELECT @ReturnShopLocationID AS ShopLocationID
		RETURN @ReturnShopLocationID
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceShopProcess' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspChoiceShopProcess TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/