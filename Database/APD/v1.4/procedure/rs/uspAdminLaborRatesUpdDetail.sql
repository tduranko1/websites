-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminLaborRatesUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminLaborRatesUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminLaborRatesUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns the Labor Rates
*
* PARAMETERS:  
* (I) @StateCode  The State Code
* (I) @City       The City Name
* (I) @County     The County
*
* RESULT SET:
* An XML data stream
*
* Additional Notes:
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminLaborRatesUpdDetail
    @LaborRateID        udt_std_id_big = NULL,
    @InsuranceCompanyID udt_std_int_small,
    @City               udt_addr_city = NULL,
    @County             udt_addr_county = NULL,
    @StateCode          udt_addr_state,
    @BodyRateMin        decimal(9, 2) = NULL,
    @BodyRateMax        decimal(9, 2) = NULL,
    @FrameRateMin       decimal(9, 2) = NULL,
    @FrameRateMax       decimal(9, 2) = NULL,
    @MaterialRateMin    decimal(9, 2) = NULL,
    @MaterialRateMax    decimal(9, 2) = NULL,
    @MechRateMin        decimal(9, 2) = NULL,
    @MechRateMax        decimal(9, 2) = NULL,
    @RefinishRateMin    decimal(9, 2) = NULL,
    @RefinishRateMax    decimal(9, 2) = NULL,
    @LaborTax           decimal(9, 2) = NULL,
    @PartsTax           decimal(9, 2) = NULL,
    @MaterialsTax       decimal(9, 2) = NULL,
    @AgreedPriceVariance decimal(9, 2) = NULL,
    @UserID             udt_std_id_big
    
AS
BEGIN
    DECLARE @ProcName                 AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                      AS datetime
    DECLARE @tmpMoney                 AS udt_std_money

    SET @ProcName = 'uspAdminLaborRatesUpdDetail'
    SET @now = CURRENT_TIMESTAMP

    IF LEN(LTRIM(RTRIM(@City))) = 0 set @City = ''
    IF LEN(LTRIM(RTRIM(@County))) = 0 set @County = ''
    
    IF @BodyRateMax < @BodyRateMin
    BEGIN
      SET @tmpMoney = @BodyRateMax
      SET @BodyRateMax = @BodyRateMin
      set @BodyRateMin = @tmpMoney
    END
    
    IF @FrameRateMax < @FrameRateMin
    BEGIN
      SET @tmpMoney = @FrameRateMax
      SET @FrameRateMax = @FrameRateMin
      set @FrameRateMin = @tmpMoney
    END
    
    IF @MaterialRateMax < @MaterialRateMin
    BEGIN
      SET @tmpMoney = @MaterialRateMax
      SET @MaterialRateMax = @MaterialRateMin
      set @MaterialRateMin = @tmpMoney
    END
    
    IF @MechRateMax < @MechRateMin
    BEGIN
      SET @tmpMoney = @MechRateMax
      SET @MechRateMax = @MechRateMin
      set @MechRateMin = @tmpMoney
    END
    
    IF @RefinishRateMax < @RefinishRateMin
    BEGIN
      SET @tmpMoney = @RefinishRateMax
      SET @RefinishRateMax = @RefinishRateMin
      set @RefinishRateMin = @tmpMoney
    END
    
    IF @RefinishRateMax < @RefinishRateMin
    BEGIN
      SET @tmpMoney = @RefinishRateMax
      SET @RefinishRateMax = @RefinishRateMin
      set @RefinishRateMin = @tmpMoney
    END
    
    IF @LaborRateID IS NOT NULL
    BEGIN
      IF NOT EXISTS(SELECT LaborRateID
                     FROM utb_labor_rate
                     WHERE LaborRateID = @LaborRateID)
      BEGIN
           -- Invalid LaborRateID
        
            RAISERROR  ('1|Invalid LaborRateID', 16, 1, @ProcName)
            RETURN
      END
    END
    
    IF @UserID IS NOT NULL
    BEGIN
      IF NOT EXISTS(SELECT UserID
                     FROM utb_user
                     WHERE UserID = @UserID)
      BEGIN
           -- Invalid User ID
        
            RAISERROR  ('1|Invalid UserID', 16, 1, @ProcName)
            RETURN
      END
    END
    
    IF @InsuranceCompanyID IS NOT NULL
    BEGIN
      IF NOT EXISTS(SELECT InsuranceCompanyID
                     FROM utb_insurance
                     WHERE InsuranceCompanyID = @InsuranceCompanyID)
      BEGIN
           -- Invalid InsuranceCompanyID
        
            RAISERROR  ('1|Invalid InsuranceCompanyID', 16, 1, @ProcName)
            RETURN
      END
    END
    
    BEGIN TRANSACTION
    
    IF @LaborRateID IS NOT NULL
    BEGIN
      UPDATE utb_labor_rate
      SET City = @City,
          County = @County,
          StateCode = @StateCode,
          AuthorizedBy = @UserID,
          AuthorizationDate = @now,
          BodyRateMin = @BodyRateMin,
          BodyRateMax = @BodyRateMax,
          FrameRateMin = @FrameRateMin,
          FrameRateMax = @FrameRateMax,
          MaterialRateMin = @MaterialRateMin,
          MaterialRateMax = @MaterialRateMax,
          MechRateMin = @MechRateMin,
          MechRateMax = @MechRateMax,
          RefinishRateMin = @RefinishRateMin,
          RefinishRateMax = @RefinishRateMax,
          LaborTax = @LaborTax,
          PartsTax = @PartsTax,
          MaterialsTax = @MaterialsTax,
          AgreedPriceVariance = @AgreedPriceVariance,
          SysLastUserID = @UserID,
          SysLastUpdatedDate = @now
      WHERE LaborRateID = @LaborRateID

       IF @@ERROR <> 0
       BEGIN
           -- Update failure

           RAISERROR('104|%s|utb_labor_rate', 16, 1, @ProcName)
           ROLLBACK TRANSACTION 
           RETURN
       END
    END
    ELSE
    BEGIN
      INSERT INTO utb_labor_rate (
         InsuranceCompanyID,
         City,
         County,
         StateCode,
         AgreedPriceVariance,
         AuthorizedBy,
         AuthorizationDate,
         BodyRateMin,
         BodyRateMax,
         FrameRateMin,
         FrameRateMax,
         MaterialRateMin,
         MaterialRateMax,
         MechRateMin,
         MechRateMax,
         RefinishRateMin,
         RefinishRateMax,
         LaborTax,
         PartsTax,
         MaterialsTax,
         SysLastUserID,
         SysLastUpdatedDate
      ) VALUES (
         @InsuranceCompanyID,
         @City,
         @County,
         @StateCode,
         @AgreedPriceVariance,
         @UserID,
         @now,
         @BodyRateMin,
         @BodyRateMax,
         @FrameRateMin,
         @FrameRateMax,
         @MaterialRateMin,
         @MaterialRateMax,
         @MechRateMin,
         @MechRateMax,
         @RefinishRateMin,
         @RefinishRateMax,
         @LaborTax,
         @PartsTax,
         @MaterialsTax,
         @UserID,
         @now
      )
      
       IF @@ERROR <> 0
       BEGIN
           -- Insert failure

           RAISERROR('105|%s|utb_labor_rate', 16, 1, @ProcName)
           ROLLBACK TRANSACTION 
           RETURN
       END
       
       SET @LaborRateID = SCOPE_IDENTITY()
    END
    
    COMMIT TRANSACTION

   SELECT   1 AS Tag,
            NULL AS Parent,
            -- Root
            isNull(@InsuranceCompanyID, '')  as [Root!1!InsuranceCompanyID],
            -- Labor Rate Details
            NULL AS [LaborRate!2!LaborRateID],
            NULL AS [LaborRate!2!SysLastUpdatedDate]
            
   UNION ALL

   SELECT   2,
            1,
            -- Root
            NULL,
            -- Labor Rate Details
            LaborRateID,
            SysLastUpdatedDate
   FROM utb_labor_rate lr
   Where LaborRateID = @LaborRateID

   ORDER BY [LaborRate!2!LaborRateID], Tag  
   
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminLaborRatesUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminLaborRatesUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
************************************************************************************************************************/
