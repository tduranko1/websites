-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspNoteUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspNoteUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspNoteUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Updates an APD note
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID    The claim aspect service channel this note is being updated to
* (I) @DocumentID                     The note to update  
* (I) @NoteTypeID                     The note type
* (I) @StatusID                       The status the note pertains to
* (I) @Note                           The note itself
* (I) @OldClaimAspectServiceChannelID The claim aspect this note is being updated for
* (I) @PrivateFlag                    Flag to indicate weather the note is private or not
* (I) @UserID                         The user updating the note
* (I) @SysLastUpdatedDate             The "previous" updated date
*
* RESULT SET:   None
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspNoteUpdDetail
    --@ClaimAspectID                      udt_std_id_big, --Project:210474 APD Remarked-off to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102
    @ClaimAspectServiceChannelID        udt_std_id_big,
    @DocumentID                         udt_std_id_big,  
    @NoteTypeID                         udt_std_id,
    @StatusID                           udt_std_id,
    @Note                               udt_std_desc_huge,
    --@OldClaimAspectID                   udt_std_id_big,
    @OldClaimAspectServiceChannelID     udt_std_id_big,
    @PrivateFlag                        udt_std_flag = 0,  --Project:210474 APD Remarked-off the parameter when we did the code merge M.A.20061117
    @UserID                             udt_std_id,
    @SysLastUpdatedDate                 varchar(30)
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Note))) = 0 SET @Note = NULL

    -- Declare internal variables

    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspNoteUpdDetail'

    
    -- Set Database options
    
    SET NOCOUNT ON
    
    
    -- Check to make sure a valid Lynx id was passed in
/*  --Project:210474 APD Remarked-off the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102
    IF  (@ClaimAspectID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @OldClaimAspectID))
    --Project:210474 APD Remarked-off the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102*/

    --Project:210474 APD Added the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102
    IF  (@ClaimAspectServiceChannelID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectServiceChannelID FROM dbo.utb_claim_aspect_service_channel WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID))
    --Project:210474 APD Added the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectServiceChannelID|%u', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
        RETURN
    END

    --Project:210474 APD Added the following to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102
    IF  (@OldClaimAspectServiceChannelID IS NULL) OR
        (NOT EXISTS(SELECT ClaimAspectServiceChannelID FROM dbo.utb_claim_aspect_service_channel WHERE ClaimAspectServiceChannelID = @OldClaimAspectServiceChannelID))
    BEGIN
        -- Invalid Claim Aspect Service Channel ID
    
        RAISERROR('101|%s|@OldClaimAspectServiceChannelID|%u', 16, 1, @ProcName, @OldClaimAspectServiceChannelID)
        RETURN
    END
    --Project:210474 APD Added the above to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102


    
    
    -- Check to make sure a valid Status ID was passed in
    IF  (@StatusID IS NULL) OR
        (NOT EXISTS(SELECT StatusID FROM dbo.utb_status WHERE StatusID = @StatusID))
    BEGIN
        -- Invalid Status ID
    
        RAISERROR('101|%s|@StatusID|%u', 16, 1, @ProcName, @StatusID)
        RETURN
    END
    

    -- Check to make sure status is valid for the claim aspect
    IF NOT EXISTS(SELECT StatusID FROM utb_status WHERE StatusID = @StatusID AND ClaimAspectTypeID =
                                (SELECT ClaimAspectTypeID 
                                 FROM dbo.utb_claim_aspect ca
                                 INNER JOIN utb_Claim_Aspect_Service_Channel casc on ca.ClaimAspectID  = casc.ClaimAspectID --Project:210474 APD Added to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102
                                 WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)  )
    BEGIN
        -- Status not found for the claim aspect
    
        RAISERROR  ('%s: Invalid Status ID for ClaimAspectServiceChannelID', 16, 1, @ProcName)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    
    -- Validate the Last updated date

    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_document', @DocumentID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.
    
        RETURN
    END


    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP

    
    -- Begin Update

    BEGIN TRANSACTION NoteUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update the note

    UPDATE dbo.utb_document
       SET 
		NoteTypeID                = @NoteTypeID,
		StatusID                  = @StatusID,
		Note                      = @Note,
		PrivateFlag               = @PrivateFlag,  --Project:210474 APD Added the column when we did the code merge M.A.20061117
		SysLastUserID             = @UserID,
		SysLastUpdatedDate        = @now
     WHERE 
		DocumentID = @DocumentID

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|utb_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    -- Update the claim aspect the document is attached to
    
    UPDATE dbo.utb_claim_aspect_service_channel_document --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102
      SET   ClaimAspectServiceChannelID             = @ClaimAspectServiceChannelID, --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102
		    SysLastUserID             = @UserID,
		    SysLastUpdatedDate        = @now
      WHERE ClaimAspectServiceChannelID = @OldClaimAspectServiceChannelID --Project:210474 APD Modified to support the schema change (utb_Claim_Aspect_Document was removed) M.A.20070102
        AND DocumentID = @DocumentID
          

    -- Check error value

    IF @error <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('104|%s|utb_claim_aspect_service_channel_document', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION NoteUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspNoteUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspNoteUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/