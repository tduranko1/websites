-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditedEstimateSaved' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAuditedEstimateSaved 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAuditedEstimateSaved
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     This will check to see if an audited estimate has been saved
*
* PARAMETERS:  
*				@ClaimAspectID
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAuditedEstimateSaved
    @ClaimAspectID  udt_std_int_big
AS
BEGIN
    DECLARE @now DATETIME
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspAuditedEstimateSaved'

    SET NOCOUNT ON
    SET @now = CURRENT_TIMESTAMP
   
	IF EXISTS (SELECT * FROM utb_claim_aspect_audit_values WHERE ClaimAspectID = @ClaimAspectID)
	BEGIN
		SELECT 'Saved' AS 'Result'
		RETURN 'Saved'
	END
	ELSE
	BEGIN
		SELECT 'NotSaved' AS 'Result'
		RETURN 'NotSaved'
	END	

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAuditedEstimateSaved' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAuditedEstimateSaved TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/