-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceShopGetShopLocationID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceShopGetShopLocationID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspChoiceShopGetShopLocationID
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets the APD HQ Choice shops from the APD Shop tables.
* Date:			02Nov2015
*
* PARAMETERS:  
*				@HQShopPartnerID - HQ Choice Shop GUID
* RESULT SET:
*				ShopLocationID INT
*
* REVISIONS:	02Nov2015 - TVD - Initial development
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspChoiceShopGetShopLocationID]
	@HQShopPartnerID		   VARCHAR(50)
AS
BEGIN
    SET NOCOUNT ON
 	
 	---------------------------------------
	-- Declare Vars
	---------------------------------------
	DECLARE @ProcName				VARCHAR(50)
	DECLARE @Now					DATETIME
    DECLARE @Debug					udt_std_flag

	---------------------------------------
	-- Init Params
	---------------------------------------
    SET @Debug = 1 
	SET @ProcName = 'uspChoiceShopGetShopLocationID'
	SET @Now = CURRENT_TIMESTAMP

	---------------------------------------
	-- Debug Parameter(s)
	---------------------------------------
    IF @Debug = 1
    BEGIN
        PRINT 'Passed in Parameters:'
		PRINT '    @HQShopPartnerID = ' + @HQShopPartnerID
    END

	---------------------------------------
	-- Validations
	---------------------------------------
	IF @HQShopPartnerID IS NULL
	BEGIN
	   -- SQL Server Error
		RAISERROR('100|%s', 16, 1, '@HQShopPartnerID GUID parameter not received')
		RETURN
	END

	-- Start by checking if shop exists
	IF EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE PPGCTSId = @HQShopPartnerID)
	BEGIN
		------------------------------------------------------
		-- Choice Shop Exists, return ShopLocationID.
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'Process Type: EXISTS'
		END

		----------------------------------------------------------
		-- Get the ShopLocationID for the @HQShopPartnerID
		----------------------------------------------------------
		SELECT CONVERT(VARCHAR(50), ShopLocationID) FROM utb_shop_location WHERE PPGCTSId = @HQShopPartnerID
	END
	ELSE
	BEGIN
		------------------------------------------------------
		-- Choice Shop DOESN'T Exist
		------------------------------------------------------
		IF @Debug = 1
		BEGIN
			PRINT 'Process Type: Choice Shop does not exist'
		END

		SELECT '0'
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceShopGetShopLocationID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspChoiceShopGetShopLocationID TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/