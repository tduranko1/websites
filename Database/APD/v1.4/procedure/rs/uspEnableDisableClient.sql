-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminEnableDisableClient' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminEnableDisableClient 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminEnableDisableClient
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Enables/Disables a client by setting the EnabledFlag and ClientAccessFlag to 0 in the utb_insurance table.
*
* PARAMETERS:  
*
*	InsuranceCompanyID
*
* RESULT SET:
*			row updated
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminEnableDisableClient
	@iInsuranceCompanyID INT
	, @bIsEnabled BIT
AS
BEGIN
    DECLARE @now AS datetime 
    
    -- Set Database options
    SET NOCOUNT ON
	SET @now = CURRENT_TIMESTAMP
	
    --Verify that a valid Insurance Company doesn't already exist
    IF (@iInsuranceCompanyID is Null) OR
    EXISTS (SELECT InsuranceCompanyID FROM utb_Insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID)
    BEGIN
		UPDATE dbo.utb_insurance SET EnabledFlag = @bIsEnabled, ClientAccessFlag = @bIsEnabled, SysLastUpdatedDate = @now WHERE InsuranceCompanyID = @iInsuranceCompanyID

		IF EXISTS(SELECT * FROM utb_insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID AND EnabledFlag = @bIsEnabled AND ClientAccessFlag = @bIsEnabled)
		BEGIN
			SELECT 1 AS RC
		END
		ELSE
		BEGIN
			SELECT 0 AS RC
		END
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminEnableDisableClient' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminEnableDisableClient TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAdminEnableDisableClient TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/