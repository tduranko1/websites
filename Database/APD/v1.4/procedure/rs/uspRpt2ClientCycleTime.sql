-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientCycleTime' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2ClientCycleTime 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2ClientCycleTime
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Compiles data displayed on the Severity Report
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company the report is being run for
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
* (I) @ShowAllClients       Flag indicating whether to compile all clients information
* (I) @StateCode            Optional state code
* (I) @ServiceChannelCD     Optional ServiceChannelCD
* (I) @CoverageTypeCD       Optional Coverage Type
* (I) @OfficeID             Optional Office ID
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2ClientCycleTime
    @InsuranceCompanyID     udt_std_int_small,
    @ServiceChannelCD       udt_std_cd,
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL,
    @ShowAllClients         udt_std_flag = 0,
    @StateCode              udt_addr_state = NULL,
    @CoverageTypeCD         udt_std_cd = NULL,
    @OfficeID               udt_std_int_small = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare Local variables
    
    CREATE TABLE #tmpCycleTimeDataAll
    (
        Level1Group                 varchar(50)  NOT NULL,
        LineItemID                  tinyint      NOT NULL,
        LineItemCD                  varchar(3)   NOT NULL,
        Pos1Count                   decimal(9,2) NULL,
        Pos1Percent                 int          NULL,
        Pos2Count                   decimal(9,2) NULL,
        Pos2Percent                 int          NULL,
        Pos3Count                   decimal(9,2) NULL,
        Pos3Percent                 int          NULL,
        Pos4Count                   decimal(9,2) NULL,
        Pos4Percent                 int          NULL,
        Pos5Count                   decimal(9,2) NULL,
        Pos5Percent                 int          NULL,
        Pos6Count                   decimal(9,2) NULL,
        Pos6Percent                 int          NULL,
        Pos7Count                   decimal(9,2) NULL,
        Pos7Percent                 int          NULL,
        Pos8Count                   decimal(9,2) NULL,
        Pos8Percent                 int          NULL,
        Pos9Count                   decimal(9,2) NULL,
        Pos9Percent                 int          NULL,
        Pos10Count                  decimal(9,2) NULL,
        Pos10Percent                int          NULL,
        Pos11Count                  decimal(9,2) NULL,
        Pos11Percent                int          NULL,
        Pos12Count                  decimal(9,2) NULL,
        Pos12Percent                int          NULL,
        TotalCount                  decimal(9,2) NULL,
        TotalPercent                int          NULL
    )

    CREATE TABLE #tmpCycleTimeDataClient
    (
        Level1Group                 varchar(50)  NOT NULL,
        LineItemID                  tinyint      NOT NULL,
        LineItemCD                  varchar(3)   NOT NULL,
        Pos1Count                   decimal(9,2) NULL,
        Pos1Percent                 int          NULL,
        Pos2Count                   decimal(9,2) NULL,
        Pos2Percent                 int          NULL,
        Pos3Count                   decimal(9,2) NULL,
        Pos3Percent                 int          NULL,
        Pos4Count                   decimal(9,2) NULL,
        Pos4Percent                 int          NULL,
        Pos5Count                   decimal(9,2) NULL,
        Pos5Percent                 int          NULL,
        Pos6Count                   decimal(9,2) NULL,
        Pos6Percent                 int          NULL,
        Pos7Count                   decimal(9,2) NULL,
        Pos7Percent                 int          NULL,
        Pos8Count                   decimal(9,2) NULL,
        Pos8Percent                 int          NULL,
        Pos9Count                   decimal(9,2) NULL,
        Pos9Percent                 int          NULL,
        Pos10Count                  decimal(9,2) NULL,
        Pos10Percent                int          NULL,
        Pos11Count                  decimal(9,2) NULL,
        Pos11Percent                int          NULL,
        Pos12Count                  decimal(9,2) NULL,
        Pos12Percent                int          NULL,
        TotalCount                  decimal(9,2) NULL,
        TotalPercent                int          NULL
    )

    CREATE TABLE #tmpClosedTotals
    (
        DataLevel                   varchar(6)   NOT NULL,
        MonthValue                  tinyint      NOT NULL,
        YearValue                   smallint     NOT NULL,
        ClosedTotal                 int          NULL
    )

    CREATE TABLE #tmpClosedDataRaw
    (
        FactID                      bigint      NOT NULL,
        PositionID                  tinyint     NOT NULL,
        CycleTimeDay                smallint    NOT NULL,
        LineItemID                  tinyint     NOT NULL,
        LineItemCD                  varchar(3)  NOT NULL
    )
    
    CREATE TABLE #tmpClosedDataGrouped 
    (
        LineItemID                  tinyint      NOT NULL,
        LineItemCD                  varchar(3)   NOT NULL,
        PositionID                  tinyint      NOT NULL,
        MonthValue                  tinyint      NOT NULL,
        YearValue                   smallint     NOT NULL,
        ClosedCount                 decimal(9,2) NOT NULL,
        ClosedTotal                 int          NULL,
        ClosedPercent               int          NULL
    )

    CREATE TABLE #tmpTimePeriods 
    (
        PositionID                  tinyint     NOT NULL,
        TimePeriod                  varchar(8)  NOT NULL,
        MonthValue                  tinyint     NOT NULL,
        YearValue                   smallint    NOT NULL
    )


    DECLARE @InsuranceCompanyName   udt_std_name
    DECLARE @InsuranceCompanyLCPhone udt_std_name
    DECLARE @LoopIndex              udt_std_int_tiny
    DECLARE @MonthName              udt_std_name
    DECLARE @ServiceChannelName     udt_std_name
    DECLARE @StateCodeWork          varchar(2)
    DECLARE @StateName              udt_std_name
    DECLARE @CoverageTypeCDWork     udt_std_cd
    DECLARE @CoverageTypeName       udt_std_name
    DECLARE @OfficeIDWork           varchar(10)
    DECLARE @OfficeName             udt_std_name
    
    DECLARE @Debug      udt_std_flag
    DECLARE @now        udt_std_datetime
    DECLARE @DataWarehouseDate    udt_std_datetime
	DECLARE @CancelledDispositionID int
	DECLARE @VoidedDispositionID    int

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2ClientCycleTime'

    SET @Debug = 0


    -- Validate Insurance Company ID
    
    IF (@InsuranceCompanyID IS NULL) OR
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID
        
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name,
                @InsuranceCompanyLCPhone = IsNull(CarrierLynxContactPhone, '239-337-4300')
          FROM  dbo.utb_insurance
          WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END          
        
   
    -- Validate Service Channel Code
    
    IF NOT EXISTS(SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_Service_Channel', 'ServiceChannelCD') WHERE Code = @ServiceChannelCD)
    BEGIN
        -- Invalid Service Channel CD
    
        RAISERROR  ('101|%s|@ServiceChannelCD|%s', 16, 1, @ProcName, @ServiceChannelCD)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @ServiceChannelName = Name
          FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
          WHERE Code = @ServiceChannelCD
          
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END


    -- Validate State Code
    
    IF @StateCode IS NOT NULL
    BEGIN
     IF NOT EXISTS(SELECT StateCode FROM dbo.utb_state_code WHERE StateCode = @StateCode)
        BEGIN
            -- Invalid StateCode
        
            RAISERROR  ('101|%s|@StateCode|%s', 16, 1, @ProcName, @StateCode)
            RETURN
        END
        ELSE
        BEGIN
            SET @StateCodeWork = @StateCode
            
            SELECT  @StateName = StateValue
              FROM  dbo.utb_state_code
              WHERE StateCode = @StateCode
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @StateCodeWork = '%'
        SET @StateName = 'All'
    END          
                
   
    -- Validate Coverage Type Code
    
    IF @CoverageTypeCD IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'CoverageProfileCD') WHERE Code = @CoverageTypeCD)
        BEGIN
            -- Invalid Coverage Type CD
        
            RAISERROR  ('101|%s|@CoverageTypeCD|%s', 16, 1, @ProcName, @CoverageTypeCD)
            RETURN
        END
        ELSE
        BEGIN
            SET @CoverageTypeCDWork = @CoverageTypeCD
            
            SELECT  @CoverageTypeName = Name
              FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect', 'CoverageProfileCD')
              WHERE Code = @CoverageTypeCD
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @CoverageTypeCDWork = '%'
        SET @CoverageTypeName = 'All'
    END          

    -- Validate Office ID
    
    IF @OfficeID IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT OfficeID FROM dbo.utb_office WHERE OfficeID = @OfficeID AND InsuranceCompanyID = @InsuranceCompanyID)
        BEGIN
            -- Invalid Office ID
    
            RAISERROR  ('101|%s|@OfficeID|%u', 16, 1, @ProcName, @OfficeID)
            RETURN
        END
        ELSE
        BEGIN
            SET @OfficeIDWork = convert(varchar, @OfficeID)
        
            SELECT  @OfficeName = Name
              FROM  dbo.utb_office
              WHERE OfficeID = @OfficeID
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @OfficeIDWork = '%'
        SET @OfficeName = 'All'
    END          
        
   -- Get Cancelled DispositionType ID
   
    SET @CancelledDispositionID = NULL
	
	SELECT @CancelledDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Cancelled'
    
    IF @CancelledDispositionID IS NULL OR LEN(@CancelledDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Cancelled in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
	
	-- Get Voided DispositionType ID
	
	SET @VoidedDispositionID = NULL

	SELECT @VoidedDispositionID = DispositionTypeID
    FROM dbo.utb_dtwh_dim_disposition_type
    WHERE DispositionTypeDescription = 'Voided'
    
    IF @VoidedDispositionID IS NULL OR LEN(@VoidedDispositionID) = 0
    BEGIN
        RAISERROR  ('101|%s|Unable to find DispositionTypeID for Voided in utb_dtwh_dim_disposition_type table', 16, 1, @ProcName)
        RETURN        
    END
   
    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  

    
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END


    -- Compile time periods
    
    SET @LoopIndex = 12

    WHILE @LoopIndex > 0
    BEGIN
        INSERT INTO #tmpTimePeriods
          SELECT 13 - @LoopIndex AS Position,
                 Convert(varchar(2), DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))))
                    + '/'
                    + Convert(varchar(4), DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))),
                 DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))), 
                 DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpTimePeriods', 16, 1, @ProcName)
            RETURN
        END
    
        SET @LoopIndex = @LoopIndex - 1
    END

    CREATE UNIQUE CLUSTERED INDEX uix_ak_tmptimeperiods
      ON #tmpTimePeriods(PositionID)
    WITH FILLFACTOR = 100
    
    CREATE INDEX uix_ak_tmptimeperiods_timeperiod
      ON #tmpTimePeriods(TimePeriod)
    WITH FILLFACTOR = 100
    
           
    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID = ' + Convert(varchar, @InsuranceCompanyID)
        PRINT '@InsuranceCompanyName = ' + @InsuranceCompanyName
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@ShowAllClients = ' + Convert(varchar, @ShowAllClients)
        PRINT '@StateCode = ' + @StateCode
        PRINT '@StateCodeWork = ' + @StateCodeWork
        PRINT '@ServiceChannelCD = ' + @ServiceChannelCD
        PRINT '@CoverageTypeCD = ' + @CoverageTypeCD
        PRINT '@CoverageTypeCDWork = ' + @CoverageTypeCDWork
        PRINT '@OfficeID = ' + convert(varchar, @OfficeID)
        PRINT '@OfficeIDWork = ' + convert(varchar, @OfficeIDWork)
    END
    
    
    -- Compile data needed for the report

    -- Client Data

    INSERT INTO #tmpClosedDataGrouped
      SELECT  7,
              'TTL',
              tmpTP.PositionID,
              dt.MonthofYear,
              dt.YearValue,
              Count(*) AS ClosedCount,
              NULL,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD = @ServiceChannelCD
          AND ds.StateCode LIKE @StateCodeWork
          AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
          AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpClosedDataGrouped
      SELECT  7,
              'TTL',
              13,
              99,
              9999,
              Count(*) AS ClosedCount,
              NULL,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD = @ServiceChannelCD
          AND ds.StateCode LIKE @StateCodeWork
          AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
          AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
        RETURN
    END
        
    INSERT INTO #tmpClosedTotals
      SELECT  'Client',
              MonthValue,
              YearValue,
              ClosedCount
        FROM  #tmpClosedDataGrouped
                 
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedTotals', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpClosedDataRaw
      SELECT  fc.FactID,
              tmpTP.PositionID,
              fc.CycleTimeNewToCloseCalDay,
              CASE
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 5  THEN 1
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 10 THEN 2
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 15 THEN 3
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 20 THEN 4
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 30 THEN 5
                WHEN @ServiceChannelCD = 'PS' THEN 6
                WHEN fc.CycleTimeNewToCloseCalDay <= 1 THEN 1
                WHEN fc.CycleTimeNewToCloseCalDay <= 2 THEN 2
                WHEN fc.CycleTimeNewToCloseCalDay <= 3 THEN 3
                WHEN fc.CycleTimeNewToCloseCalDay <= 4 THEN 4
                WHEN fc.CycleTimeNewToCloseCalDay <= 5 THEN 5
                ELSE 6
              END AS LineItemID,
              CASE
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 5  THEN '5'
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 10 THEN '10'
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 15 THEN '15'
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 20 THEN '20'
                WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 30 THEN '30'
                WHEN @ServiceChannelCD = 'PS' THEN '30+'
                WHEN fc.CycleTimeNewToCloseCalDay <= 1 THEN '1'
                WHEN fc.CycleTimeNewToCloseCalDay <= 2 THEN '2'
                WHEN fc.CycleTimeNewToCloseCalDay <= 3 THEN '3'
                WHEN fc.CycleTimeNewToCloseCalDay <= 4 THEN '4'
                WHEN fc.CycleTimeNewToCloseCalDay <= 5 THEN '5'
                ELSE '5+'
              END AS LineItemCD              
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
          AND dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD = @ServiceChannelCD
          AND ds.StateCode LIKE @StateCodeWork
          AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
          AND fc.EnabledFlag = 1

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedDataRaw', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpClosedDataGrouped
      SELECT  tmpCDR.LineItemID,
              tmpCDR.LineItemCD,
              tmpTP.PositionID,
              tmpTP.MonthValue,
              tmpTP.YearValue,
              Count(*),
              (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'Client' AND MonthValue = tmpTP.MonthValue AND YearValue = tmpTP.YearValue),
              NULL
        FROM  #tmpClosedDataRaw tmpCDR
        LEFT JOIN #tmpTimePeriods tmpTP ON (tmpCDR.PositionID = tmpTP.PositionID)
        GROUP BY tmpCDR.LineItemID, tmpCDR.LineItemCD, tmpTP.PositionID, tmpTP.MonthValue, tmpTP.YearValue

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpClosedDataGrouped
      SELECT  tmpCDR.LineItemID,
              tmpCDR.LineItemCD,
              13,
              99,
              9999,
              Count(*),
              (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'Client' AND MonthValue = 99 AND YearValue = 9999),
              NULL
        FROM  #tmpClosedDataRaw tmpCDR
        LEFT JOIN #tmpTimePeriods tmpTP ON (tmpCDR.PositionID = tmpTP.PositionID)
        GROUP BY tmpCDR.LineItemID, tmpCDR.LineItemID, tmpCDR.LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
        RETURN
    END

    UPDATE  #tmpClosedDataGrouped
      SET   ClosedPercent = Round(Convert(decimal(9,2), ClosedCount) / Convert(decimal(9,2), ClosedTotal), 2) * 100
      WHERE LineItemCD <> 'TTL'
        AND ClosedTotal > 0 -- Prevent divide by zero error.

    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|#tmpClosedDataGrouped', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO #tmpClosedDataGrouped
      SELECT  8,
              'AVG',
              tmpTP.PositionID,
              dt.MonthofYear,
              dt.YearValue,
              IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1), 0) AS ClosedCount,
              NULL,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD = @ServiceChannelCD
          AND ds.StateCode LIKE @StateCodeWork
          AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
          AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
        RETURN
    END

    INSERT INTO #tmpClosedDataGrouped
      SELECT  8,
              'AVG',
              13,
              99,
              9999,
              IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1), 0) AS ClosedCount,
              NULL,
              NULL
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
        INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fc.CustomerID = dc.CustomerID)   
        LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dsc.ServiceChannelCD = @ServiceChannelCD
          AND ds.StateCode LIKE @StateCodeWork
          AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
          AND convert(varchar, dc.OfficeID) LIKE @OfficeIDWork
          AND fc.EnabledFlag = 1
		  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
        RETURN
    END


    -- Pivot and save the data extracted

    INSERT INTO #tmpCycleTimeDataClient
      SELECT  @InsuranceCompanyName,
              LineItemID,
              LineItemCD,
              SUM(CASE PositionID WHEN 1 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 1 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedPercent ELSE 0 END),
              SUM(CASE PositionID WHEN 13 THEN ClosedCount ELSE 0 END),
              SUM(CASE PositionID WHEN 13 THEN ClosedPercent ELSE 0 END)
        FROM  #tmpClosedDataGrouped
        GROUP BY LineItemID, LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpCycleTimeDataClient', 16, 1, @ProcName) 
        RETURN
    END

    IF @ServiceChannelCD = 'PS'
    BEGIN
        INSERT INTO #tmpCycleTimeDataClient
          SELECT  @InsuranceCompanyName,
                  strIndex,
                  value,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0
            FROM  dbo.ufnUtilityParseString('5,10,15,20,30,30+', ',', 1)
            WHERE strIndex NOT IN (SELECT LineItemID FROM #tmpCycleTimeDataClient)
    END
    ELSE
    BEGIN
        INSERT INTO #tmpCycleTimeDataClient
          SELECT  @InsuranceCompanyName,
                  strIndex,
                  value,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0
            FROM  dbo.ufnUtilityParseString('1,2,3,4,5,5+', ',', 1)
            WHERE strIndex NOT IN (SELECT LineItemID FROM #tmpCycleTimeDataClient)
    END
        
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|#tmpCycleTimeDataClient', 16, 1, @ProcName)
        RETURN
    END

  
    IF @ShowAllClients = 1
    BEGIN
        -- Reset the temporary tables for next cycle

        DELETE FROM #tmpClosedDataRaw

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR  ('106|%s|#tmpClosedDataRaw', 16, 1, @ProcName)
            RETURN
        END

        DELETE FROM #tmpClosedDataGrouped

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            RAISERROR  ('106|%s|#tmpClosedDataGrouped', 16, 1, @ProcName)
            RETURN
        END


        -- All Data

        INSERT INTO #tmpClosedDataGrouped
          SELECT  7,
                  'TTL',
                  tmpTP.PositionID,
                  dt.MonthofYear,
                  dt.YearValue,
                  Count(*) AS ClosedCount,
                  NULL,
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dsc.ServiceChannelCD = @ServiceChannelCD
              AND ds.StateCode LIKE @StateCodeWork
              AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
              AND fc.DemoFlag = 0
              AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpClosedDataGrouped
          SELECT  7, 
                  'TTL',
                  13,
                  99,
                  9999,
                  Count(*) AS ClosedCount,
                  NULL,
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dsc.ServiceChannelCD = @ServiceChannelCD
              AND ds.StateCode LIKE @StateCodeWork
              AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
              AND fc.DemoFlag = 0
              AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
            RETURN
        END
        
        INSERT INTO #tmpClosedTotals
          SELECT  'All',
                  MonthValue,
                  YearValue,
                  ClosedCount
            FROM  #tmpClosedDataGrouped
                     
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedTotals', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpClosedDataRaw
          SELECT  fc.FactID,
                  tmpTP.PositionID,
                  fc.CycleTimeNewToCloseCalDay,
                  CASE
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 5  THEN 1
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 10 THEN 2
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 15 THEN 3
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 20 THEN 4
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 30 THEN 5
                    WHEN @ServiceChannelCD = 'PS' THEN 6
                    WHEN fc.CycleTimeNewToCloseCalDay <= 1 THEN 1
                    WHEN fc.CycleTimeNewToCloseCalDay <= 2 THEN 2
                    WHEN fc.CycleTimeNewToCloseCalDay <= 3 THEN 3
                    WHEN fc.CycleTimeNewToCloseCalDay <= 4 THEN 4
                    WHEN fc.CycleTimeNewToCloseCalDay <= 5 THEN 5
                    ELSE 6
                  END AS LineItemID,
                  CASE
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 5  THEN '5'
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 10 THEN '10'
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 15 THEN '15'
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 20 THEN '20'
                    WHEN @ServiceChannelCD = 'PS' AND fc.CycleTimeNewToCloseCalDay <= 30 THEN '30'
                    WHEN @ServiceChannelCD = 'PS' THEN '30+'
                    WHEN fc.CycleTimeNewToCloseCalDay <= 1 THEN '1'
                    WHEN fc.CycleTimeNewToCloseCalDay <= 2 THEN '2'
                    WHEN fc.CycleTimeNewToCloseCalDay <= 3 THEN '3'
                    WHEN fc.CycleTimeNewToCloseCalDay <= 4 THEN '4'
                    WHEN fc.CycleTimeNewToCloseCalDay <= 5 THEN '5'
                    ELSE '5+'
                  END AS LineItemCD              
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dsc.ServiceChannelCD = @ServiceChannelCD
              AND ds.StateCode LIKE @StateCodeWork
              AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
              AND fc.DemoFlag = 0
              AND EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedDataRaw', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpClosedDataGrouped
          SELECT  tmpCDR.LineItemID,
                  tmpCDR.LineItemCD,
                  tmpTP.PositionID,
                  tmpTP.MonthValue,
                  tmpTP.YearValue,
                  Count(*),
                  (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'All' AND MonthValue = tmpTP.MonthValue AND YearValue = tmpTP.YearValue),
                  NULL
            FROM  #tmpClosedDataRaw tmpCDR
            LEFT JOIN #tmpTimePeriods tmpTP ON (tmpCDR.PositionID = tmpTP.PositionID)
            GROUP BY tmpCDR.LineItemID, tmpCDR.LineItemCD, tmpTP.PositionID, tmpTP.MonthValue, tmpTP.YearValue

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpClosedDataGrouped
          SELECT  tmpCDR.LineItemID,
                  tmpCDR.LineItemCD,
                  13,
                  99,
                  9999,
                  Count(*),
                  (SELECT ClosedTotal FROM #tmpClosedTotals WHERE DataLevel = 'All' AND MonthValue = 99 AND YearValue = 9999),
                  NULL
            FROM  #tmpClosedDataRaw tmpCDR
            LEFT JOIN #tmpTimePeriods tmpTP ON (tmpCDR.PositionID = tmpTP.PositionID)
            GROUP BY tmpCDR.LineItemID, tmpCDR.LineItemCD

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
            RETURN
        END

        UPDATE  #tmpClosedDataGrouped
          SET   ClosedPercent = Round(Convert(decimal(9,2), ClosedCount) / Convert(decimal(9,2), ClosedTotal), 2) * 100
          WHERE LineItemCD <> 'TTL'
           AND ClosedTotal > 0 -- Prevent divide by zero error

        IF @@ERROR <> 0
        BEGIN
            -- Update failed

            RAISERROR('104|%s|#tmpClosedDataGrouped', 16, 1, @ProcName)
            RETURN
        END

        INSERT INTO #tmpClosedDataGrouped
          SELECT  8,
                  'AVG',
                  tmpTP.PositionID,
                  dt.MonthofYear,
                  dt.YearValue,
                  IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1), 0) AS ClosedCount,
                  NULL,
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dsc.ServiceChannelCD = @ServiceChannelCD
              AND ds.StateCode LIKE @StateCodeWork
              AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
              AND fc.DemoFlag = 0
              AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )
            GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue 

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
            RETURN
        END

        INSERT INTO #tmpClosedDataGrouped
          SELECT  8, 
                  'AVG',
                  13,
                  99,
                  9999,
                  IsNull(Round(Avg(Convert(decimal(6, 1), fc.CycleTimeNewToCloseCalDay)), 1), 0) AS ClosedCount,
                  NULL,
                  NULL
            FROM  dbo.utb_dtwh_fact_claim fc
            LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fc.TimeIDClosed = dt.TimeID)
            INNER JOIN #tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            LEFT JOIN dbo.utb_dtwh_dim_coverage_type dct ON (fc.CoverageTypeID = dct.CoverageTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_state ds ON (fc.ClaimLocationID = ds.StateID)
            LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
            LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
            WHERE dsc.ServiceChannelCD = @ServiceChannelCD
              AND ds.StateCode LIKE @StateCodeWork
              AND dct.CoverageTypeCD LIKE @CoverageTypeCDWork
              AND fc.DemoFlag = 0
              AND fc.EnabledFlag = 1
			  AND fc.DispositionTypeID NOT IN ( @CancelledDispositionID, @VoidedDispositionID )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpClosedDataGrouped', 16, 1, @ProcName) 
            RETURN
        END


        -- Pivot and save the data extracted

        INSERT INTO #tmpCycleTimeDataAll
          SELECT  '* All Clients *',
                  LineItemID,
                  LineItemCD,
                  SUM(CASE PositionID WHEN 1 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 1 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 2 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 2 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 3 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 3 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 4 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 4 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 5 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 5 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 6 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 6 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 7 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 7 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 8 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 8 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 9 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 9 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 10 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 10 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 11 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 11 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 12 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 12 THEN ClosedPercent ELSE 0 END),
                  SUM(CASE PositionID WHEN 13 THEN ClosedCount ELSE 0 END),
                  SUM(CASE PositionID WHEN 13 THEN ClosedPercent ELSE 0 END)
            FROM  #tmpClosedDataGrouped
            GROUP BY LineItemID, LineItemCD

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpCycleTimeDataAll', 16, 1, @ProcName) 
            RETURN
        END

        IF @ServiceChannelCD = 'PS'
        BEGIN
            INSERT INTO #tmpCycleTimeDataAll
              SELECT  '* All Clients *',
                      strIndex,
                      value,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0
                FROM  dbo.ufnUtilityParseString('5,10,15,20,30,30+', ',', 1)
                WHERE strIndex NOT IN (SELECT LineItemID FROM #tmpCycleTimeDataAll)
        END
        ELSE
        BEGIN
            INSERT INTO #tmpCycleTimeDataAll
              SELECT  '* All Clients *',
                      strIndex,
                      value,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0
                FROM  dbo.ufnUtilityParseString('1,2,3,4,5,5+', ',', 1)
                WHERE strIndex NOT IN (SELECT LineItemID FROM #tmpCycleTimeDataAll)
        END

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|#tmpCycleTimeDataAll', 16, 1, @ProcName)
            RETURN
        END
    END
    

    -- Final Select
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            @StateName AS State,
            @ServiceChannelName AS ServiceChannelName,
            @CoverageTypeName AS CoverageTypeName,
            @OfficeName AS OfficeName,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
            CASE tmp.LineItemCD
              WHEN '1' THEN '1 day'
              WHEN '2' THEN '2 days'
              WHEN '3' THEN '3 days'
              WHEN '4' THEN '4 days'
              WHEN '5' THEN '5 days'
              WHEN '5+' THEN '>5 days'
              WHEN '10' THEN '10 days'
              WHEN '15' THEN '15 days'
              WHEN '20' THEN '20 days'
              WHEN '30' THEN '30 days'
              WHEN '30+' THEN '>30 days'
              WHEN 'TTL' THEN 'Total Closed'
              WHEN 'AVG' THEN 'Avg(in calendar days)'
            END AS LineItemDisplay,
            tmp.LineItemID AS LineItemSort,
            tmp.*,
            @DataWareHouseDate as DataWareDate  
      FROM  #tmpCycleTimeDataClient tmp
    
    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
  @MonthName + ' ' + Convert(varchar(4), @RptYear) AS ReportMonth,
            @StateName AS State,
            @ServiceChannelName AS ServiceChannelName,
            @CoverageTypeName AS CoverageTypeName,
            @OfficeName AS OfficeName,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  #tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
            CASE tmp.LineItemCD
              WHEN '1' THEN '1 day'
              WHEN '2' THEN '2 days'
              WHEN '3' THEN '3 days'
              WHEN '4' THEN '4 days'
              WHEN '5' THEN '5 days'
              WHEN '5+' THEN '>5 days'
              WHEN '10' THEN '10 days'
              WHEN '15' THEN '15 days'
              WHEN '20' THEN '20 days'
              WHEN '30' THEN '30 days'
              WHEN '30+' THEN '>30 days'
              WHEN 'TTL' THEN 'Total Closed'
              WHEN 'AVG' THEN 'Avg(in calendar days)'
            END AS LineItemDisplay,
            tmp.LineItemID AS LineItemSort,
            tmp.*,
            @DataWareHouseDate as DataWareDate  
      FROM  #tmpCycleTimeDataAll tmp

        
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END  

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientCycleTime' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2ClientCycleTime TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
