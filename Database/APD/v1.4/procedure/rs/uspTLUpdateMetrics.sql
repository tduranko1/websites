-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLUpdateMetrics' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLUpdateMetrics 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLUpdateMetrics
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Update a Total Loss metric value
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID  Claim Aspect Service Channel ID
* (I) @MetricDataPoint              The Data point column name we want to update
* (I) @DataValue                    Data value                         
*
* UPDATES:						1.1 - 27Mar2017 - Sergey - Modified to fix deadlock issues
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLUpdateMetrics
    @ClaimAspectServiceChannelID    udt_std_id_big,
    @MetricDataPoint                varchar(50),
    @DataValue                      varchar(50)
AS
BEGIN
    SET NOCOUNT ON

    -- Declare internal variables
    DECLARE @AssignmentDate as datetime
    DECLARE @LHPayoffExpirationDate as datetime
    DECLARE @LossDate as datetime
    DECLARE @ContactCount as int
    DECLARE @ServiceChannelCD as varchar(2)
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
 
    
    SET @ProcName = 'uspTLUpdateMetrics'
    SET @now = CURRENT_TIMESTAMP
    
    -- Check the metrics data point
    IF NOT EXISTS (SELECT Column_Name 
                   FROM INFORMATION_SCHEMA.COLUMNS
                   WHERE Table_Name = 'utb_total_loss_metrics' 
                   AND Column_Name = @MetricDataPoint)
    BEGIN
        -- Invalid metric data point
        RAISERROR('%s: Invalid Metrics data point %s.', 16, 1, @ProcName, @MetricDataPoint)
        RETURN
    END
    
    IF PATINDEX('%Date', @MetricDataPoint) > 0 AND ISDATE(@DataValue) = 0
    BEGIN
        -- Invalid metric data point value
        RAISERROR('%s: Invalid Metrics data point value %s for %s', 16, 1, @ProcName, @DataValue, @MetricDataPoint)
        RETURN
    END

    SELECT @AssignmentDate = casc.CreatedDate,
           @LHPayoffExpirationDate = PayoffExpirationDate,
           @LossDate = c.LossDate,
           @ServiceChannelCD = casc.ServiceChannelCD
    FROM dbo.utb_claim_aspect_service_channel casc
    LEFT JOIN dbo.utb_claim_aspect ca ON casc.ClaimASpectID = ca.ClaimASpectID
    LefT JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
    WHERE casc.ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
    
    IF @ServiceChannelCD = 'TL'
    BEGIN
       BEGIN TRANSACTION
       
       IF NOT EXISTS (SELECT ClaimAspectServiceChannelID
                       FROM dbo.utb_total_loss_metrics (NOLOCK)
                       WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
       BEGIN
           -- Total Loss metrics does not exist. Create a basic record.
           INSERT INTO dbo.utb_total_loss_metrics (
               ClaimAspectServiceChannelID,
               AssignmentDate,
               LHContactCount,
               VOContactCount,
               LHPayoffExpirationDate,
               DateOfLoss)
           SELECT @ClaimAspectServiceChannelID, @AssignmentDate, 0, 0, @LHPayoffExpirationDate, @LossDate
       END
       ELSE
       BEGIN
         UPDATE dbo.utb_total_loss_metrics
         SET LHPayoffExpirationDate = @LHPayoffExpirationDate,
             DateOfLoss = @LossDate
         WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID    
       END
       
       IF @MetricDataPoint = 'ClientFeeBillingDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET ClientFeeBillingDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'ClosingDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET ClosingDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'DateOfLoss'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET DateOfLoss = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'LHInitialContactDate'
       BEGIN
           SELECT @ContactCount = isNull(LHContactCount, 0) + 1
           FROM dbo.utb_total_loss_metrics
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
           
           UPDATE dbo.utb_total_loss_metrics
           SET LHInitialContactDate = convert(datetime, @DataValue),
               LHContactCount = @ContactCount
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'LHRecentContactDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET LHRecentContactDate = convert(datetime, @DataValue),
               LHContactCount = isNull(LHContactCount, 0) + 1
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'LHLoGTransmitDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET LHLoGTransmitDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'LHLoGReceivedDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET LHLoGReceivedDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'LHTitleTransmitDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET LHTitleTransmitDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'LHTitleReceivedDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET LHTitleReceivedDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'LHPayoffExpirationDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET LHPayoffExpirationDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'OriginalClosingDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET OriginalClosingDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'SettlementInvoiceDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET SettlementInvoiceDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'SettlementPaymentDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET SettlementPaymentDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'SettlementSalvageBundleDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET SettlementSalvageBundleDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'SettlementSalvageReceivedDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET SettlementSalvageReceivedDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'VOInitialContactDate'
       BEGIN
           SELECT @ContactCount = isNull(VOContactCount, 0) + 1
           FROM dbo.utb_total_loss_metrics
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
           
           
           UPDATE dbo.utb_total_loss_metrics
           SET VOInitialContactDate = convert(datetime, @DataValue),
               VOContactCount = @ContactCount
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'VORecentContactDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET VORecentContactDate = convert(datetime, @DataValue),
               VOContactCount = isNull(VOContactCount, 0) + 1
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'VOPoATransmitDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET VOPoATransmitDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'VOPoAReceivedDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET VOPoAReceivedDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'VOTitleTransmitDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET VOTitleTransmitDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
       
       IF @MetricDataPoint = 'VOTitleReceivedDate'
       BEGIN
           UPDATE dbo.utb_total_loss_metrics
           SET VOTitleReceivedDate = convert(datetime, @DataValue)
           WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
       END
    END
    
    COMMIT TRANSACTION
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLUpdateMetrics' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLUpdateMetrics TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/