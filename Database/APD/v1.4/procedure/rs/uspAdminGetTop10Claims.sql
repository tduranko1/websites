-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminGetTop10Claims' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminGetTop10Claims 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminGetTop10Claims
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets last 10 claims that were processed.
*
* PARAMETERS:  
*				
* RESULT SET:
*				Record Set
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminGetTop10Claims
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT TOP 10 
		u.EmailAddress AS CarrierRep
		, u1.EmailAddress AS ProcessedBy
		, c.ClientClaimNumber 
		, c.LossDate
		, c.LossDescription
		, c.LossState
		, u2.EmailAddress AS LastUpdatedBy
	FROM 
		utb_claim c
		INNER JOIN utb_user u
		ON u.UserID = c.CarrierRepUserID
		INNER JOIN utb_user u1
		ON u1.UserID = c.IntakeUserID
		INNER JOIN utb_user u2
		ON u2.UserID = c.SysLastUserID
	ORDER BY 
		c.SyslastUpdatedDate DESC
	
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminGetTop10Claims' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminGetTop10Claims TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAdminGetTop10Claims TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/