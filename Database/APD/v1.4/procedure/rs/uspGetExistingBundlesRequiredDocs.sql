-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetExistingBundlesRequiredDocs' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetExistingBundlesRequiredDocs 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetExistingBundlesRequiredDocs
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets existing bundling required documents by BundlingID
*
* PARAMETERS:  
*			@iBundlingID
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetExistingBundlesRequiredDocs
	@iBundlingID INT = 0
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	IF (@iBundlingID = 0)
	BEGIN
		SELECT 
			bdt.BundlingID
			, dt.DocumentTypeID
			, dt.[Name]
			, dt.EnabledFlag
			, ISNULL(bdt.DirectionalCD,'') AS DirectionalCD
			, bdt.DirectionToPayFlag
			, bdt.DuplicateFlag
			, ISNULL(bdt.EstimateTypeCD,'') AS EstimateTypeCD
			, bdt.FinalEstimateFlag
			, bdt.MandatoryFlag
			, bdt.SelectionOrder
			, bdt.VANFlag
			, bdt.WarrantyFlag 
			, bdt.SelectionOrder
		FROM 
			utb_bundling_document_type bdt
			INNER JOIN utb_document_type dt
			ON dt.DocumentTypeID = bdt.DocumentTypeID
		ORDER BY 
			dt.[Name]
		END
	ELSE
	BEGIN
		SELECT 
			bdt.BundlingID
			, dt.DocumentTypeID
			, dt.[Name]
			, dt.EnabledFlag
			, ISNULL(bdt.DirectionalCD,'') AS DirectionalCD
			, bdt.DirectionToPayFlag
			, bdt.DuplicateFlag
			, ISNULL(bdt.EstimateTypeCD,'') AS EstimateTypeCD
			, bdt.FinalEstimateFlag
			, bdt.MandatoryFlag
			, bdt.SelectionOrder
			, bdt.VANFlag
			, bdt.WarrantyFlag 
			, bdt.SelectionOrder
		FROM 
			utb_bundling_document_type bdt
			INNER JOIN utb_document_type dt
			ON dt.DocumentTypeID = bdt.DocumentTypeID
		WHERE 
			bdt.BundlingID = @iBundlingID 
		ORDER BY 
			dt.[Name]
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetExistingBundlesRequiredDocs' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetExistingBundlesRequiredDocs TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetExistingBundlesRequiredDocs TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/