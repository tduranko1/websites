-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspECADUpdateInvoiceStatus' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspECADUpdateInvoiceStatus 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspECADUpdateInvoiceStatus
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Returns a list of indemnity items to populate the ClaimPoint authorization screen
*
* PARAMETERS:   
* (I) @AuthorizedClaimAspectIDs   Comma delimited list of ClaimAspect IDs for which enabled payments with a status of AC
*                                 have been approved.
* (I) @UserID                     Approving User ID. 
*
* RESULT SET:
* LynxID, ClaimAspectID, ClientClaimNumber, CoverageTypeID, LossDate, LossState, ClaimantNameFirst, ClaimantNameLast, FeeName, FeeCode, IndemnityAmount
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspECADUpdateInvoiceStatus
    @AuthorizedClaimAspectIDs     udt_std_desc_mid,
    @UserID                       udt_std_id
    
AS
BEGIN
    -- Declare local variables

    DECLARE @ProcName               varchar(30)       -- Used for raise error stmts 
    DECLARE @now                    udt_std_datetime
    
    DECLARE @AwaitingClientStatus   udt_std_cd
    DECLARE @AuthorizedStatus       udt_std_cd
    DECLARE @FeeItemType            udt_std_cd
    DECLARE @EventID                udt_std_id_small
    DECLARE @EventName              udt_std_name
    DECLARE @ClaimAspectID          udt_std_id_big
    DECLARE @Amount                 varchar(20)
    DECLARE @HistoryDescription     udt_std_desc_long
    DECLARE @AlreadyApproved        varchar(5)
        
                        
    SET @ProcName = 'uspECADUpdateInvoiceStatus'
    SET @EventName = 'Client Approved Payment'                           
    SET @now = CURRENT_TIMESTAMP
    
    
    SELECT @AwaitingClientStatus = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD') 
                                        WHERE Name = 'Awaiting Client Authorization'
                                          
    SELECT @AuthorizedStatus = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'StatusCD') 
                                          WHERE Name = 'Authorized'
    
    SELECT @FeeItemType = Code FROM dbo.ufnUtilityGetReferenceCodes('utb_invoice', 'ItemTypeCD') 
                               WHERE Name = 'Fee'
    
    SELECT @EventID = EventID FROM dbo.utb_event WHERE Name = @EventName
    
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END 
    
    
    DECLARE @tmpClaimAspect TABLE (ClaimAspectID      bigint        NOT NULL)  
      
    DECLARE @tmpClaimAspect2 TABLE (ClaimAspectID     bigint        NOT NULL,
                                    AlreadyApproved   varchar(5)    NOT NULL)
    

    -- Break apart comma delimited string of 'Authorized' ClaimAspectIDs and insert into temp table. 
    INSERT INTO @tmpClaimAspect
    SELECT CONVERT(bigint, value) FROM dbo.ufnUtilityParseString(@AuthorizedClaimAspectIDs, ',', 1)  
    
    
    BEGIN TRANSACTION UpdateInvoiceStatusTran1 
    
    
    DECLARE csrClaimAspect CURSOR FOR SELECT ClaimAspectID FROM @tmpClaimAspect
    
    OPEN csrClaimAspect
    
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END 
    
    
    FETCH NEXT FROM csrClaimAspect INTO @ClaimAspectID      
    
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
        
        -- Get the total indemnity amount that has been authorized.
        SET @Amount = (SELECT CONVERT(varchar(20), SUM(Amount))
                       FROM dbo.utb_invoice
                       WHERE ClaimAspectID = @ClaimAspectID
                         AND ItemTypeCD <> @FeeItemtype
                         AND StatusCD = @AwaitingClientStatus
                         AND EnabledFlag = 1)
                         
        IF @@ERROR <> 0
        BEGIN
          -- SQL Server Error    
            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END 
        
        IF (0 < (SELECT COUNT(*) FROM dbo.utb_invoice WHERE ClaimAspectID = @ClaimAspectID
                                                   AND StatusCD = @AwaitingClientStatus))
        BEGIN
            SET @AlreadyApproved = 'false'
            
            --  Update authorized invoice items.
            UPDATE dbo.utb_invoice
            SET StatusCD = @AuthorizedStatus,
                AuthorizingUserID = @UserID,
                AuthorizedDate = @now,
                StatusDate = @now,
                SysLastUserID = @UserID
            WHERE ClaimAspectID = @ClaimAspectID
              AND EnabledFlag = 1
              AND StatusCD = @AwaitingClientStatus
          
          
            IF (@@Error <> 0)
            BEGIN      
                -- Insertion failure    
                RAISERROR('105|%s|@tmpClaimAspect', 16, 1, @ProcName)
                ROLLBACK TRANSACTION 
                RETURN    
            END          
                
        
            SET @HistoryDescription  = 'An indemnity payment in the amount of $' + @Amount + ' was approved.'
        
            -- Make the workflow call        
            EXEC uspWorkflowNotifyEvent @EventID = @EventID,
                                        @ClaimAspectID = @ClaimAspectID,
                                        @Description = @HistoryDescription,
                                        @UserID = @UserID

            IF @@ERROR <> 0
            BEGIN
                -- APD Workflow Notification failed
                RAISERROR  ('107|%s|%s', 16, 1, @ProcName, @EventName)
                ROLLBACK TRANSACTION
                RETURN
            END
        END
        ELSE
        BEGIN
            SET @AlreadyApproved = 'true'            
        
        END
        
        INSERT INTO @tmpClaimAspect2 VALUES(@ClaimAspectID, @AlreadyApproved)
    
        FETCH NEXT FROM csrClaimAspect INTO @ClaimAspectID
    END
    
    
    CLOSE csrClaimAspect
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END 
    
    
    DEALLOCATE csrClaimAspect
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END     
       
    
    COMMIT TRANSACTION UpdateInvoiceStatusTran1
    
    -- Begin final select.
    SELECT  1                   AS Tag,
            NULL                AS Parent,
            
            -- Root
            NULL                AS [Root!1!Dud],
            
            -- Claim Listing
            NULL                AS [Claim!2!LynxID], 
            NULL                AS [Claim!2!VehicleNumber], 
            NULL                AS [Claim!2!StatusCD],
            NULL                AS [Claim!2!AuthorizingUserNameFirst],
            NULL                AS [Claim!2!AuthorizingUserNameLast],
            NULL                AS [Claim!2!AlreadyApproved]
            
    UNION ALL
    
    SELECT DISTINCT  2,
            1,
            
            NULL,
            ca.LynxID,
            ca.ClaimAspectNumber,
            StatusCD,
            u.NameFirst,
            u.NameLast,
            t.AlreadyApproved  
            
    FROM dbo.utb_invoice i INNER JOIN dbo.utb_claim_aspect ca ON i.ClaimAspectID = ca.ClaimAspectID
                           INNER JOIN dbo.utb_user u ON i.AuthorizingUserID = u.UserID
                           INNER JOIN @tmpClaimAspect2 t ON i.ClaimAspectID = t.ClaimAspectID
        
    
    -- RETURN @@ROWCOUNT
       
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspECADUpdateInvoiceStatus' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspECADUpdateInvoiceStatus TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/