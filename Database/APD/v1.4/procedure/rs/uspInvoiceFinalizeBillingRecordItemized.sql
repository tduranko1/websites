-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceFinalizeBillingRecordItemized' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspInvoiceFinalizeBillingRecordItemized 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspInvoiceFinalizeBillingRecordItemized
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspInvoiceFinalizeBillingRecordItemized
	@EnvironmentCD		udt_std_cd,
    @BillingSessionID   udt_std_desc_short,
    @InvoiceID			udt_std_id_big,
    @DispatchNumber     udt_std_desc_short,
    @StatementEndDate	udt_std_datetime,
    @BillingHistoryID	udt_std_int = 0
	
AS
BEGIN

	
	--declare @BillingHistoryID	udt_std_int
	declare @BillingDatabase	udt_std_desc_mid
	declare @BillingHistoryProc udt_std_desc_mid
	declare @Table				udt_std_desc_mid
	declare @now				udt_std_datetime
	declare @vsql				nvarchar(4000)
	declare @params				nvarchar(4000)

	


	set @now = current_timestamp
	
	if lower(@EnvironmentCD) = 'dev'
	begin
		set @BillingDatabase = 'udb_billing_dev'
	end
	if lower(@EnvironmentCD) in ('STG','TST')
	begin
		set @BillingDatabase = 'udb_billing_tst'
	end
	if lower(@EnvironmentCD) = 'PRD'
	begin
		set @BillingDatabase = 'udb_billing'
	end
	
	if @BillingDatabase is null
	begin
		raiserror('Unknown environment code',16,1)
		return
	end

	--select @BillingDatabase = value
	--from dbo.utb_app_variable
	--where Name = 'BillingDatabase'

	set @Table = @BillingDatabase + '.dbo.utb_billing_fnol'
	
	if @BillingHistoryID = 0
	begin
	
		select @BillingHistoryProc = @BillingDatabase + '.dbo.uspNewBillingHistoryRecord'

		create table #billhist (BillingHistoryID int)
		insert into #billhist
		exec  @BillingHistoryProc @ApplicationCD='APD',@StatementEndDate=@StatementEndDate

		if @@error<>0
		begin		
			return
		end
		
		select @BillingHistoryID = BillingHistoryID from #billhist
	end 		
	
	if @BillingHistoryID is not null 
	begin
		if @BillingHistoryID <> 0
		begin

			begin transaction
			select @vSql = 'insert into ' + @Table + '
					select	InvoiceID,' + 
					convert(varchar(9),@BillingHistoryID) + ',
					Amount,
					BillingEntryDate,
					CarrierClaimRep,
					ClaimantName,
					ClaimNumber,
					ClientID, 
					ClientName
					CreateDate,
					Description,' + 					
					@DispatchNumber + ',
					InsuredName,
					LossDate,
					LossState,
					LynxID,
					PolicyNumber,
					OfficeID,
					OfficeName,					
					1,
					0,
					current_timestamp
			from dbo.utb_temp_invoice_prep
			where BillingSessionID = ''' + @BillingSessionID + '''
			  and InvoiceID = ''' + convert(varchar(13),@InvoiceID) + ''''

			
			exec (@vSql)

			if @@error <> 0
			begin
				rollback transaction 
				return
			end
											
			update dbo.utb_invoice
			set DispatchNumber = @DispatchNumber,
				StatusCD = 'FS',
				StatusDate = @now,
				SentToIngresDate = @now			
			where InvoiceID = @InvoiceID

			if @@error<>0
			begin
				rollback transaction
				return
			end

			commit
			
			select @BillingHistoryID as BillingHistoryID
		end
	end	
	
END

go


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceFinalizeBillingRecordItemized' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspInvoiceFinalizeBillingRecordItemized TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/		  