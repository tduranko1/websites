-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLTaskComplete' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLTaskComplete 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLTaskComplete
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Completes a TL Task
*
* PARAMETERS:  
* (I) @CheckListID      Check List ID
* (I) @StatusCD         Completion status (S=Success; F=Followup)
* (I) @TaskNotes        User entered Notes                         
* (I) @EditNotes        Edits to claim data
* (I) @UserID           User ID performing this action
*
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLTaskComplete
    @CheckListID            udt_std_id_big,
    @StatusCD               varchar(1),
    @EscalateFlag           bit = 0,
    @TaskNotes              varchar(1000),
    @EditNotes              varchar(6000) = NULL,
    @UserID                 udt_std_id_big
AS
BEGIN

    --Initialize string parameters
    
    -- Declare internal variables
    DECLARE @ChecklistSysLastUpdatedDate as varchar(50)
    DECLARE @ClaimAspectID as bigint
    DECLARE @ClaimAspectServiceChannelID as bigint
    DECLARE @StatuSIDTLOpen as int
    DECLARE @TaskUnsuccessfulExpDuration as int
    DECLARE @NoteTypeIDClaim as int
    DECLARE @NoteTypeIDClaimEdit as int
    DECLARE @TaskIDLHInitialContact as int
    DECLARE @TaskIDLHLoGFollowup as int
    DECLARE @TaskIDLHPOFollowup as int
    DECLARE @TaskIDVOInitialContact as int
    DECLARE @TaskIDVOFollowup as int
    DECLARE @MetricDataPoint as varchar(50)
    DECLARE @TaskID as int
    DECLARE @TaskIDNotesReviewRequired as int
    DECLARE @NotedReviewRequiredAlarmDt as datetime
    DECLARE @TaskDelayReasonIDOther as int
    DECLARE @TaskDelayReasonIDUnavailable as int
    DECLARE @TaskCreatedDate as datetime
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    
    If LEN(LTRIM(RTRIM(@EditNotes))) = 0 Set @EditNotes = NULL
    
    SET @ProcName = 'uspTLTaskComplete'
    SET @now = CURRENT_TIMESTAMP
    --SET @LockExpirationMinutes = 30
    SET @TaskUnsuccessfulExpDuration = 2 -- hours you want to set the expiration when the task was unsuccessful in execution. (Callee was not available)
    SET @TaskIDLHInitialContact = 94
    SET @TaskIDLHPOFollowup = 98
    SET @TaskIDLHLoGFollowup = 97
    SET @TaskIDVOInitialContact = 93
    SET @TaskIDVOFollowup = 96
    
    -- Check the user id
    IF NOT EXISTS (SELECT UserID FROM utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User
        RAISERROR('%s: Invalid User.', 16, 1, @ProcName)
        RETURN
    END
    
    -- Check the status code
    IF @StatusCD NOT IN ('S', 'F')
    BEGIN
        -- Invalid Status Code
        RAISERROR('%s: Invalid Status Code.', 16, 1, @ProcName)
        RETURN
    END
    
    -- check the check list locked by the user
    IF NOT EXISTS (SELECT CheckListID
                    FROM dbo.utb_checklist_lock
                    WHERE CheckListID = @CheckListID
                      AND UserID = @UserID)
    BEGIN
        -- Invalid state. Checklist is not locked by the user
        RAISERROR('%s: Invalid Lock state.', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @NoteTypeIDClaim = NoteTypeID
    FROM dbo.utb_note_type
    WHERE Name = 'Claim'
    
    IF @NoteTypeIDClaim IS NULL
    BEGIN
        -- Invalid state. Claim note type does not exist
        RAISERROR('%s: Invalid state. Claim not type does not exist', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @NoteTypeIDClaimEdit = NoteTypeID
    FROM dbo.utb_note_type
    WHERE Name = 'Claim Edit'
    
    IF @NoteTypeIDClaimEdit IS NULL
    BEGIN
        -- Invalid state. Claim Edit note type does not exist
        RAISERROR('%s: Invalid state. Claim Edit note type does not exist', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @TaskIDNotesReviewRequired = TaskID,
           @NotedReviewRequiredAlarmDt = DATEADD(minute, isNull(EscalationMinutes, 0), @now)
    FROM utb_task
    WHERE Name = 'Notes Review Required'

    IF @TaskIDNotesReviewRequired IS NULL
    BEGIN
        -- Invalid state. Claim Edit note type does not exist
        RAISERROR('%s: Invalid state. Notes Review Required task does not exist', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @StatuSIDTLOpen = StatusID
    FROM utb_status
    WHERE ClaimAspectTypeID = 9
      AND ServiceChannelCD = 'TL'
      AND Name = 'Active'
      
    IF @StatuSIDTLOpen IS NULL
    BEGIN
        -- Invalid state. TL Active status id does not exist
        RAISERROR('%s: Invalid Lock state. TL Active status id does not exist', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @TaskDelayReasonIDOther = TaskDelayReasonID
    FROM dbo.utb_task_delay_reason
    WHERE Name = 'Other'
      AND ReasonText = 'Other'
      AND TaskID IS NULL
    
      
    IF @TaskDelayReasonIDOther IS NULL
    BEGIN
        -- Invalid state. Task Delay reason Other does not exist
        RAISERROR('%s: Invalid Lock state. Task Delay Reason "Other" does not exist', 16, 1, @ProcName)
        RETURN
    END

    
    SELECT @TaskDelayReasonIDUnavailable = TaskDelayReasonID
    FROM dbo.utb_task_delay_reason
    WHERE Name = 'Other_Delay'
      AND ReasonText = 'Unavailable'
    
      
    IF @TaskDelayReasonIDUnavailable IS NULL
    BEGIN
        -- Invalid state. Task Delay reason Other does not exist
        RAISERROR('%s: Invalid Lock state. Task Delay Reason "Other_Delay Unavailable" does not exist', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ChecklistSysLastUpdatedDate = dbo.ufnUtilityGetDateString(cl.SysLastUpdatedDate),
           @ClaimAspectServiceChannelID = cl.ClaimAspectServiceChannelID,
           @ClaimAspectID = ClaimAspectID,
           @TaskID = cl.TaskID,
           @TaskCreatedDate = cl.CreatedDate
    FROM dbo.utb_checklist cl
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc ON cl.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    WHERE ChecklistID = @ChecklistID
    
    -- Lock the first available task
    BEGIN TRANSACTION
    
    -- Task was performed successfully
    IF @StatusCD = 'S'
    BEGIN
        
        -- delete the lock
        DELETE FROM dbo.utb_checklist_lock
        WHERE CheckListID = @ChecklistID
          AND UserID = @UserID

        -- trigger the task completion via the workflow
        EXEC dbo.uspWorkflowCompleteTask @ChecklistID = @ChecklistID,
                  @NotApplicableFlag = 0,
                  @NoteRequiredFlag = 0,
                  @Note = @TaskNotes,
                  @NoteTypeID = @NoteTypeIDClaim,
                  @UserID = @UserID,
                  @SysLastUpdatedDate = @ChecklistSysLastUpdatedDate,
                  @ApplicationCD = 'APD'
        
        IF @@ERROR <> 0
        BEGIN
         raiserror('104|%s|%s',16,1,@ProcName,'uspWorkflowCompleteTask')
         ROLLBACK TRANSACTION
         RETURN
        END
                
        IF @EditNotes IS NOT NULL
        BEGIN
           -- Need to add a task for the File Owner later to review the notes
           SET @EscalateFlag = 1
           
           -- add claim edit notes
           EXEC dbo.uspNoteInsDetail @ClaimAspectID = @ClaimAspectID,
                    @NoteTypeID = @NoteTypeIDClaimEdit,
                    @StatusID = @StatuSIDTLOpen,
                    @Note = @EditNotes,
                    @UserID = @UserID,
                    @PrivateFlag = 1,
                    @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
        END
        
        IF @@ERROR <> 0
        BEGIN
         raiserror('104|%s|%s',16,1,@ProcName,'uspNoteInsDetail')
         ROLLBACK TRANSACTION
         RETURN
        END

        IF @EscalateFlag = 1
        BEGIN
            EXEC dbo.uspDiaryInsDetail @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
                   @TaskID = @TaskIDNotesReviewRequired,
                   @AlarmDate = @NotedReviewRequiredAlarmDt,
                   @UserTaskDescription = '',
                   @AssignedUserID = NULL,
                   @AssignmentPoolFunctionCD = NULL,
                   @SupervisorFlag = 0,
                   @UserID = @UserID

           IF @@ERROR <> 0
           BEGIN
            raiserror('104|%s|%s',16,1,@ProcName,'uspNoteInsDetail')
            ROLLBACK TRANSACTION
            RETURN
           END
        END
    END
    
    -- Task was not performed successfully
    IF @StatusCD = 'F'
    BEGIN
        -- set expiration to 2 hours
        UPDATE dbo.utb_checklist_lock
        SET ExpirationDate = DATEADD(hour, @TaskUnsuccessfulExpDuration, @now)
        WHERE ChecklistID = @ChecklistID
          
         IF @@error <> 0
         BEGIN
             -- SQL Server Error

             RAISERROR  ('104|%s|utb_checklist_history', 16, 1, @ProcName)
             ROLLBACK TRANSACTION 
             RETURN
         END

        -- add to task history
        IF EXISTS(SELECT CheckListHistoryID
                  FROM dbo.utb_checklist_history
                  WHERE ChecklistID = @ChecklistID
                    AND DelayUserID = @UserID
                    AND TaskDelayReasonID = @TaskDelayReasonIDUnavailable)
        BEGIN
            UPDATE dbo.utb_checklist_history
            SET AlarmDateNew = @now
            WHERE ChecklistID = @ChecklistID
              AND DelayUserID = @UserID
              AND TaskDelayReasonID = @TaskDelayReasonIDUnavailable
             
            IF @@error <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR  ('104|%s|utb_checklist_history', 16, 1, @ProcName)
                ROLLBACK TRANSACTION 
                RETURN
            END
        END
        ELSE
        BEGIN
           INSERT INTO dbo.utb_checklist_history (
               CheckListID,
               DelayUserID,
               TaskDelayReasonID,
               AlarmDateNew,
               AlarmDateOld,
               DelayComment,
               DelayDate,
               SysLastUserID,
               SysLastUpdatedDate
           )
           SELECT @CheckListID,
                  @UserID,
                  @TaskDelayReasonIDUnavailable,
                  @now,
                  @TaskCreatedDate,
                  convert(varchar, @ClaimAspectServiceChannelID),
                  @now,
                  @UserID,
                  @now

            IF @@error <> 0
            BEGIN
                -- SQL Server Error

                RAISERROR  ('105|%s|utb_checklist_history', 16, 1, @ProcName)
                ROLLBACK TRANSACTION 
                RETURN
            END
        END

                
        -- add notes
        EXEC dbo.uspNoteInsDetail    @ClaimAspectID = @ClaimAspectID,
                 @NoteTypeID = @NoteTypeIDClaim,
                 @StatusID = @StatusIDTLOpen,
                 @Note = @TaskNotes,
                 @UserID = @UserID,
                 @PrivateFlag = 0,
                 @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

        IF @@ERROR <> 0
        BEGIN
         raiserror('104|%s|%s',16,1,@ProcName,'uspNoteInsDetail')
         ROLLBACK TRANSACTION
         RETURN
        END

    END
    
    -- Update the metrics
    
    SELECT @MetricDataPoint = CASE @TaskID
			                        WHEN @TaskIDLHInitialContact THEN 'LHInitialContactDate'
			                        WHEN @TaskIDLHLoGFollowup THEN 'LHRecentContactDate'
			                        WHEN @TaskIDLHPOFollowup THEN 'LHRecentContactDate'
			                        WHEN @TaskIDVOInitialContact THEN 'VOInitialContactDate'
			                        WHEN @TaskIDVOFollowup THEN 'VORecentContactDate'
			                        ELSE ''
			                     END
    
    
    IF @MetricDataPoint <> ''
    BEGIN
       -- update the total loss metric.
       EXEC dbo.uspTLUpdateMetrics @ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID,
							              @MetricDataPoint = @MetricDataPoint,
								           @DataValue = @now

        IF @@ERROR <> 0
        BEGIN
         raiserror('104|%s|%s',16,1,@ProcName,'uspTLUpdateMetrics')
         ROLLBACK TRANSACTION
         RETURN
        END

    END
    

    COMMIT TRANSACTION
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLTaskComplete' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLTaskComplete TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/