-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetExposuresByLynxID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetExposuresByLynxID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetExposuresByLynxID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets the claim rep ID assigned to the claim by LynxID
*
* PARAMETERS:  
*				LynxID
* RESULT SET:
*				Claim Aspect Data
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetExposuresByLynxID
	@LynxID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT 
		ClaimAspectID
		, ISNULL(AnalystUserID,'') AS AnalystUserID
		, ClaimAspectTypeID
		, ISNULL(ClientCoverageTypeID,0) AS ClientCoverageTypeID
		, ISNULL(CompletedAnalystUserID,0) AS CompletedAnalystUserID
		, ISNULL(CompletedOwnerUserID,0) AS CompletedOwnerUserID
		, ISNULL(CompletedSupportUserID,0) AS CompletedSupportUserID
		, ISNULL(InitialAssignmentTypeID,0) AS InitialAssignmentTypeID
		, LynxID
		, ISNULL(OwnerUserID,0) OwnerUserID 
		, ISNULL(SourceApplicationID,0) AS SourceApplicationID
		, ISNULL(SupportUserID,0) AS SupportUserID
		, ClaimAspectNumber
		, ISNULL(CoverageProfileCD,'') AS CoverageProfileCD
		, CreatedDate
		, EnabledFlag
		, ISNULL(ExposureCD,'0') AS ExposureCD
		, ISNULL(NewAssignmentAnalystFlag,0) AS NewAssignmentAnalystFlag
		, ISNULL(NewAssignmentOwnerFlag,0) AS NewAssignmentOwnerFlag
		, ISNULL(NewAssignmentSupportFlag,0) AS NewAssignmentSupportFlag
		, PriorityFlag
		, ISNULL(SourceApplicationPassThruData,'') AS SourceApplicationPassThruData
		, WarrantyExistsFlag
		, SysLastUserID
		, SysLastUpdatedDate
	FROM 
		utb_claim_aspect 
	WHERE 
		LynxID = @LynxID
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetExposuresByLynxID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetExposuresByLynxID TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetExposuresByLynxID TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/