-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspAssignmentUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAssignmentUpdDetail
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAssignmentUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       M. Ahmed
* FUNCTION:     Updates a row in utb_Assignment table.
*
* PARAMETERS:
* (I) @ClaimAspectServiceChannelID.........Service Channel ID for a vehicle claim
* (I) @ClaimAspectID.......................Claim Aspect ID given in utb_Claim_Aspect_Service_Channel
* (I) @ServiceChannelCD....................Service Channel Code for the Claim Aspect in utb_Claim_Aspect_Service_Channel
* (I) @AltRemarksServiceChannelCD..........optional - used to satisfy TT# 3570 whereby we fill the remarks on the ME tab with the glass assignment details.
* (I) @DispatchedFlag......................Dispatch flag (valid values 0, 1)
* (I) @ReferenceId.........................
* (I) @DispatchId..........................
* (I) @ShopName............................
* (I) @ShopPhoneAreaCode...................
* (I) @ShopPhoneExchangeNumber.............
* (I) @ShopPhoneUnitNumber.................
* (I) @AppointmentDate.....................
* (I) @AssignmentRemarks...................
* (I) @UserID..............................The USER ID updating the row
* (I) @SysLastUpdatedDate..................The "previous" updated date
*
* RESULT SET:   An XML document containing the new SysLastUpdatedDate value
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAssignmentUpdDetail
                    @ClaimAspectServiceChannelID    udt_std_id_big = null,
                    @ClaimAspectID                  udt_std_id_big = null,
                    @ServiceChannelCD               udt_std_cd = null,
                    @AssignmentSequenceNumber       udt_std_int_tiny = 1,
                    @AltRemarksServiceChannelCD     udt_std_cd = null,
                    @DispatchedFlag                 udt_std_flag,
                    @ReferenceId                    udt_std_desc_short = null,
                    @DispatchId                     udt_std_desc_short = null,
                    @ShopName                       udt_std_name = null,
                    @ShopPhoneAreaCode              udt_ph_area_code = null,
                    @ShopPhoneExchangeNumber        udt_ph_extension_number = null,
                    @ShopPhoneUnitNumber            udt_ph_unit_number = null,
                    @AppointmentDate                udt_std_datetime = null,
                    @AssignmentRemarks              udt_std_desc_xlong = null,
                    @UserID                         udt_std_id,
                    @SysLastUpdatedDate             udt_sys_last_updated_date = NULL
AS
BEGIN -- PROC LOGIC STARTS HERE

    -- Declare internal variables
    DECLARE @ProcName             varchar(30)       -- Used in raise error stmts
    declare @now                  datetime
    Declare @CRLF                 char(01)
    declare @AssignmentID         bigint
    declare @TempRemarks          varchar(2000)   -- used for concatenation of assignment remarks
    declare @ConcatenatedRemarks  varchar(2000)   -- used for concatenation of assignment remarks
                                                    /* The concat. rule is given below:
                                                		"Glass Dispatch Number: " + @DispatchId + CRLF +
                                                		"Glass Shop Name: " + @ShopName + CRLF +
                                                        "Glass Shop Phone: " + @ShopPhoneAreaCode + "-" + @ShopPhoneExchangeNumber + "-" + @ShopPhoneUnitNumber + CRLF +
                                                		"Glass Shop Appointment: " + @AppointmentDate + CRLF  +
                                                		AssignmentRemarks parameter
                                                    */
    SET @CRLF = char(13)
    SET @ProcName = 'uspAssignmentUpdDetail'


    -- Set Database options
    set nocount on

    --Start the validation of the data provided via input parameters

    --verify if the given value for @ClaimAspectServiceChannelID is valid
    if @ClaimAspectServiceChannelID is not null
       and (
            select    count(*)
            from      utb_Claim_Aspect_Service_Channel
            where     ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
            and       EnabledFlag = 1
           ) = 0
        BEGIN
            -- Invalid @ClaimAspectServiceChannelID
            RAISERROR('101|%s|@ClaimAspectServiceChannelID|%u', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
            RETURN
        END

    select    @ClaimAspectServiceChannelID = ClaimAspectServiceChannelID
    from      utb_Claim_Aspect_Service_Channel
    where     ClaimAspectID = @ClaimAspectID
    and       ServiceChannelCD = @ServiceChannelCD
    and       EnabledFlag = 1



    --If @ClaimAspectServiceChannelID is still NULL then Exit

    if    @ClaimAspectServiceChannelID is null
          BEGIN
            -- Invalid @ClaimAspectServiceChannelID
            RAISERROR('102|%s|Must provide either the service channel ID or the claim aspect ID with service channel code ', 16, 1, @ProcName)
            RETURN
          END

    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID

        RAISERROR('103|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    --If we've come this far that means we can get the value for the @AssignmentID

    Select  @AssignmentID = AssignmentID
    from    utb_assignment
    where   ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
      and   AssignmentSequenceNumber = @AssignmentSequenceNumber

    --If @ClaimAspectServiceChannelID is still NULL then Exit

    if    @AssignmentID is null
          BEGIN
            -- Invalid @ClaimAspectServiceChannelID
            RAISERROR('104|%s|Could not find the Assignment ID for the Service Channel ID|%u', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
            RETURN
          END


    -- Get current timestamp

     SET @now = getdate()

    -- Validate the updated date parameter
    if @SysLastUpdatedDate is not null
        begin
            declare @ID varchar(20)
            set @ID = convert(varchar(20),@AssignmentID)
               -- Validate the updated date parameter
            exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_assignment',  @ID

            IF @@ERROR <> 0
            BEGIN
                -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

                RETURN
            END
        end


    --End of validation

    if @DispatchedFlag = 1
        set @ConcatenatedRemarks = 'Glass Dispatch Number: ' + isnull(@DispatchId,'') + @CRLF +
                                   'Glass Shop Name: ' + isnull(@ShopName,'') + @CRLF +
                                   'Glass Shop Phone: ' + isnull(@ShopPhoneAreaCode,'') + '-' + isnull(@ShopPhoneExchangeNumber,'') + '-' + isnull(@ShopPhoneUnitNumber,'') + @CRLF +
                                   'Glass Shop Appointment: ' + isnull(dbo.ufnUtilityFormatDate(@AppointmentDate,'MM/dd/yy HHH:nn'),'') + @CRLF  +
                                    left(isnull(@AssignmentRemarks,''), 1000)
    else
        set @ConcatenatedRemarks = left(isnull(@AssignmentRemarks,''), 1000)

    update     utb_assignment
    set        ReferenceID = @ReferenceId,
               AssignmentRemarks = left(@ConcatenatedRemarks,1000),
               SysLastUserID = @UserID,
               SysLastUpdatedDate = @now
    where      AssignmentID = @AssignmentID

     -- Check error value

    IF @@error <> 0 or @@rowcount = 0
    BEGIN
        -- Update failure
        RAISERROR('104|%s|utb_assignment', 16, 1, @ProcName)
        RETURN
    END

    -- @AltRemarksServiceChannelCD is optional and is used to satisfy TT# 3570
    -- whereby we fill the remarks on the ME tab with the glass assignment details.
    if not @AltRemarksServiceChannelCD is null
    begin

        select      @AssignmentID = a.AssignmentID
        from        utb_assignment a
        inner join  utb_Claim_Aspect_Service_Channel casc
        on          a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        where       casc.ClaimAspectID = @ClaimAspectID
        and         casc.ServiceChannelCD = @AltRemarksServiceChannelCD
        and         a.AssignmentSequenceNumber = @AssignmentSequenceNumber
        and         casc.EnabledFlag = 1

        if not @AssignmentID is null
        begin

            select @TempRemarks = AssignmentRemarks
                from utb_assignment where AssignmentID = @AssignmentID

            if @DispatchedFlag = 1
                set @ConcatenatedRemarks = isnull(@TempRemarks,'') + @CRLF +
                                           'Glass Dispatch Number: ' + isnull(@DispatchId,'') + @CRLF +
                                           'Glass Shop Name: ' + isnull(@ShopName,'') + @CRLF +
                                           'Glass Shop Phone: ' + isnull(@ShopPhoneAreaCode,'') + '-' + isnull(@ShopPhoneExchangeNumber,'') + '-' + isnull(@ShopPhoneUnitNumber,'') + @CRLF +
                                           'Glass Shop Appointment: ' + isnull(dbo.ufnUtilityFormatDate(@AppointmentDate,'MM/dd/yy HHH:nn'),'')
            else
                set @ConcatenatedRemarks = left(isnull(@AssignmentRemarks,''), 1000)

            update     utb_assignment
            set        AssignmentRemarks = left(@ConcatenatedRemarks,1000),
                       SysLastUserID = @UserID,
                       SysLastUpdatedDate = @now
            where      AssignmentID = @AssignmentID

        end
    end

    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Vehicle Level
            NULL AS [Assignment!2!SysLastUpdatedDate]

    UNION

    SELECT  2,
            1,
            NULL,
            -- Vehicle Level
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
    --    FOR XML EXPLICIT      -- Comment for Client-side processing

END --PROC LOGIC ENDS HERE


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects
            WHERE name = 'uspAssignmentUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAssignmentUpdDetail TO
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

