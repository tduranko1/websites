-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientTowing' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClientTowing 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetClientTowing
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Get towing information by InsuranceCompanyID
*
* PARAMETERS:  
*   @InsuranceCompanyID - ID of the insurance company
*   @AnalystUserID - ID of the analyst submitting the form
*
* RESULT SET:
*   All data related to towing
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetClientTowing
    @InsuranceCompanyID      udt_std_id_big
    , @AnalystUserID		 udt_std_int
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspGetClientTowing'

    
    -- Set Database options
    
    SET NOCOUNT ON
    

    -- Check to make sure a valid Insurance Company id was passed in
    
    IF  (@InsuranceCompanyID IS NULL) OR
        (NOT EXISTS(SELECT @InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID))

    BEGIN
        -- Invalid Insurance Company ID
    
        RAISERROR('101|%s|@InsuranceCompanyID|%u', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END

	SELECT
		TowingID
		, InsuranceCompanyID
		, AnalystUserID
		, ClaimNumber
		, CusAddress
		, CusAltPhone
		, CusCity
		, CusEmail
		, CusHomePhone
		, CusName
		, CusState
		, CusWorkPhone
		, CusZip
		, DesAddress
		, DesCity
		, DesFacilityContactName
		, DesHoursOfOperation
		, DesPhone
		, DesState
		, DesZip
		, EnabledFlag
		, LynxID
		, LocAddress
		, LocCity
		, LocFacilityContactName
		, LocHoursOfOperation
		, LocPhone
		, LocState
		, LocZip
		, VehConditionDescription
		, VehLicensePlateNumber
		, VehMake
		, VehModel
		, VehVINState
		, VehYear
		, Comments
		, SysLastUserID
		, SysLastUpdatedDate
	FROM
		 dbo.utb_client_towing
	WHERE
		 InsuranceCompanyID = @InsuranceCompanyID  
		 AND AnalystUserID = @AnalystUserID
		 AND EnabledFlag = 1
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClientTowing' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClientTowing TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClientTowing TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/