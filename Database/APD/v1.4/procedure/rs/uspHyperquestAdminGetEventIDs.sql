-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestAdminGetEventIDs' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestAdminGetEventIDs 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestAdminGetEventIDs
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* DATE:			2015Dec08
* FUNCTION:     Get a list of Service transactions from the Hyperquest Log
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* Version: 1.0 - 2015Dec08 - TVD - Initial Build
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestAdminGetEventIDs]
	@vTransactionType VARCHAR(50)
AS
BEGIN
    -- Declare local variables
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
	DECLARE @dtNow			   AS DATETIME
	DECLARE @Debug			   AS BIT

    -- Initialize any empty string parameters
    SET @ProcName = 'uspHyperquestAdminGetEventIDs'
	SET @dtNow = CURRENT_TIMESTAMP
	SET @debug = 0

	SELECT DISTINCT
		EventTransactionID 
		, MAX(SysLastUpdatedDate) SysLastUpdateddate
	FROM
		utb_hyperquest_event_log 
	WHERE 
		EventType LIKE @vTransactionType + '%'
	GROUP BY 
		EventTransactionID	
	ORDER BY
		SysLastUpdatedDate DESC

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestAdminGetEventIDs' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestAdminGetEventIDs TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/