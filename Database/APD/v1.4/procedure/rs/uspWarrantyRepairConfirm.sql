-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyRepairConfirm' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWarrantyRepairConfirm 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWarrantyRepairConfirm
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc to update the warranty repair details.
*
* PARAMETERS:  

* RESULT SET:
* An XML document containing the new SysLastUpdatedDate value
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWarrantyRepairConfirm
    @AssignmentID     udt_std_id_big,
    @WarrantyCD       udt_std_cd,
    @WarrantyDate     datetime
AS
BEGIN
    -- Initialize any empty string parameters

    -- Declare internal variables
    
    DECLARE @error                  AS int
    DECLARE @rowcount               AS int
    
    DECLARE @now                    AS datetime 

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspWarrantyRepairConfirm'


    -- Set Database options
    
    SET NOCOUNT ON
    
    -- Check to make sure a valid Assignment id was passed in

    IF  (@AssignmentID IS NULL) OR
        (@AssignmentID = 0) OR
        (NOT EXISTS(SELECT AssignmentID FROM dbo.utb_warranty_assignment WHERE AssignmentID = @AssignmentID))
    BEGIN
        -- Invalid Assignment ID
    
        RAISERROR('101|%s|@AssignmentID|%u', 16, 1, @ProcName, @AssignmentID)
        RETURN
    END

    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP
    
    -- Begin Update

    BEGIN TRANSACTION 

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    IF @WarrantyCD = 'E'
    BEGIN
       -- Update 
       UPDATE utb_warranty_assignment
       SET WarrantyEndConfirmFlag = 1,
           WarrantyEndDate = @WarrantyDate,
           SysLastUpdatedDate = @now
       WHERE AssignmentID = @AssignmentID
    END
    
    IF @WarrantyCD = 'S'
    BEGIN
       -- Update 
       UPDATE utb_warranty_assignment
       SET WarrantyStartConfirmFlag = 1,
           WarrantyStartDate = @WarrantyDate,
           SysLastUpdatedDate = @now
       WHERE AssignmentID = @AssignmentID
    END
    
    
    SET @error = @@ERROR
    SET @rowCount = @@ROWCOUNT
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_warranty_assignment', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION 

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END



    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyRepairConfirm' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWarrantyRepairConfirm TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
