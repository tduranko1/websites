-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAssignmentReferenceIDUpd' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAssignmentReferenceIDUpd 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAssignmentReferenceIDUpd
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jeffery A. Lund
* FUNCTION:     Updates Assignment Date for a given lynxid and vehiclenumber
*
* PARAMETERS:  
* (I) @AssignmentID         Assignment ID to update
*
* RESULT SET:   NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAssignmentReferenceIDUpd
    @AssignmentID                   udt_std_id_big,
    @ClaimAspectServiceChannelID    udt_std_id_big,
    @ReferenceID                    udt_std_desc_short
AS
BEGIN
    -- Declare Internal variables
    
    DECLARE @ProcName               udt_std_Name        --For error checking
    DECLARE @Now                    udt_std_datetime    --Procedure Timestamp

    DECLARE @Debug                  udt_std_flag
    SET @Debug = 0
    
    SET @ProcName = 'uspAssignmentReferenceIDUpd'
    SET @now = CURRENT_TIMESTAMP


    -- Debug Code
    IF @debug = 1
    BEGIN
        Print 'Parameters:'
        Print '@AssignmentID = ' + Convert(varchar(10), @AssignmentID)
        Print '@ReferenceID = ' + @ReferenceID
    END
    -- End Debug


    -- Check to make sure a valid AssignmentID ID was passed in

    IF  (@AssignmentID IS NULL) OR
        (NOT EXISTS(SELECT AssignmentID FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID))
    BEGIN
        -- Invalid Assignment ID
    
        RAISERROR('101|%s|@AssignmentID|%u', 16, 1, @ProcName, @AssignmentID)
        RETURN
    END
    
    IF (@ClaimAspectServiceChannelID IS NULL) OR 
       (NOT EXISTS(SELECT AssignmentID FROM dbo.utb_assignment WHERE AssignmentID = @AssignmentID AND ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID))
    BEGIN
        -- Invalid ClaimAspectServiceChannelID OR AssignmentID does not belong to ClaimAspectServiceChannelID 
    
        RAISERROR('101|%s|@ClaimAspectServiceChannelID|%u', 16, 1, @ProcName, @ClaimAspectServiceChannelID)
        RETURN
      
    END
    
    BEGIN TRANSACTION
    
    UPDATE utb_assignment
    SET ReferenceID = @ReferenceID
    WHERE AssignmentID = @AssignmentID
      AND ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID

    IF  @@error <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_assignment', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    COMMIT
   
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAssignmentReferenceIDUpd' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAssignmentReferenceIDUpd TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
