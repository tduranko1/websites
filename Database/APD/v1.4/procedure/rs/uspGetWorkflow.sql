-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetWorkflow' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetWorkflow 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetWorkflow
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all workflows by InsuranceCompanyID
*
* PARAMETERS:  
*				InsuranceCompanyID
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetWorkflow
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

SELECT 
	w.WorkflowID
	, w.IgnoreDefaultFlag
	, t.[Name] AS TaskName
	, w.OriginatorTypeCD
	, w.SysLastUserID
	, w.SysLastUpdatedDate
FROM 
	utb_workflow w
	INNER JOIN utb_insurance i
	ON i.InsuranceCompanyID = w.InsuranceCompanyID
	INNER JOIN utb_task t
	ON t.TaskID = w.OriginatorID
WHERE
	w.InsuranceCompanyID = @iInsuranceCompanyID	
ORDER BY
	t.[Name]

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetWorkflow' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetWorkflow TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetWorkflow TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/