-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceFinalizeBillingRecordsBulk' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspInvoiceFinalizeBillingRecordsBulk 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspInvoiceFinalizeBillingRecordsBulk
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspInvoiceFinalizeBillingRecordsBulk
	@EnvironmentCD		udt_std_cd,
    @BillingSessionID   udt_std_desc_short,
    @DispatchNumber     udt_std_desc_short,
    @StatementEndDate	udt_std_datetime   
	
AS
BEGIN

	
	declare @BillingHistoryID	udt_std_int
	declare @BillingDatabase	udt_std_desc_mid
	declare @BillingHistoryProc udt_std_desc_mid
	declare @Table				udt_std_desc_mid
	declare @now				udt_std_datetime
	declare @vsql				nvarchar(4000)
	declare @params				nvarchar(4000)

	


	set @now = current_timestamp

	--if @EnvironmentCD = 'DEV'
	--begin
	--	set @BillingDatabase = 'FNOL_APD.udb_billing_dev'
	--end
	--if @EnvironmentCD in ('STG','TST')
	--begin
	--	set @BillingDatabase = 'FNOL_SVR.udb_billing_tst'
	--end
	--if @EnvironmentCD = 'PRD'
	--begin
	--	set @BillingDatabase = 'FNOL_APD.udb_billing'
	--end
	
	--if @BillingDatabase is null
	--begin
	--	raiserror('Unknown environment code',16,1)
	--	return
	--end	

	--select @BillingDatabase = value
	--from dbo.utb_app_variable
	--where Name = 'BillingDatabase'

	--set @Table = @BillingDatabase + '.dbo.utb_billing_apd'
	
	--select @BillingHistoryProc = @BillingDatabase + '.dbo.uspNewBillingHistoryRecord'

	--create table #billhist (BillingHistoryID int)
	--insert into #billhist
	--exec  @BillingHistoryProc @ApplicationCD='APD',@StatementEndDate=@StatementEndDate
	
	select @BillingHistoryID = (case when max(billinghistoryid) is null then 1 else (max(billinghistoryid) + 1) end) from utb_billing_apd

	if @@error<>0
	begin		
		return
	end
	
	--select @BillingHistoryID = BillingHistoryID from #billhist
	
	if @BillingHistoryID is not null 
	begin
		if @BillingHistoryID <> 0
		begin

			begin transaction
			--select @vSql = 'insert into ' + @Table + '
			--		select	InvoiceID,' + 
			--		convert(varchar(9),@BillingHistoryID) + ',
			--		Amount,
			--		BillingEntryDate,
			--		CarrierClaimRep,
			--		ClaimantName,
			--		ClaimNumber,
			--		ClientID, 
			--		ClientName
			--		CreateDate,
			--		Description,' + 					
			--		@DispatchNumber + ',
			--		InsuredName,
			--		LossDate,
			--		LossState,
			--		LynxID,
			--		OfficeID,
			--		OfficeName,
			--		1,
			--		0,
			--		current_timestamp
			--from dbo.utb_temp_invoice_prep
			--where BillingSessionID = ''' + @BillingSessionID + ''''

			
			--exec (@vSql)
			insert into dbo.utb_billing_apd
			select InvoiceID,
				   convert(varchar(9),@BillingHistoryID),
				   Amount,
				   BillingEntryDate,
				   CarrierClaimRep,
				   ClaimantName,
				   ClaimNumber,
				   ClientID,
				   ClientName,
				   CreateDate,
				   Description,
				   @DispatchNumber,
				   InsuredName,
				   LossDate,
				   LossState,
				   LossStateCD,
				   LynxID,
				   PolicyNumber,
				   OfficeID,
				   OfficeName,
				   1,
				   0,
				   @now
			from dbo.utb_temp_invoice_prep
			where BillingSessionID = @BillingSessionID			   

			if @@error <> 0
			begin
				rollback transaction 
				return
			end
											
			update dbo.utb_invoice
			set DispatchNumber = @DispatchNumber,
				StatusCD = 'FS',
				StatusDate = @now,
				SentToIngresDate = @now			
			where InvoiceID in (select InvoiceID 
								from dbo.utb_temp_invoice_prep
								where BillingSessionID = @BillingSessionID)

			if @@error<>0
			begin
				rollback transaction
				return
			end

			commit
			
			select @BillingHistoryID as BillingHistoryID
		end
	end	
	
END

go


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspInvoiceFinalizeBillingRecordsBulk' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspInvoiceFinalizeBillingRecordsBulk TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/		  