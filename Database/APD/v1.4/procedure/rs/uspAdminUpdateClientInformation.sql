-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminUpdateClientInformation' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminUpdateClientInformation 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminUpdateClientInformation
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Updates existing client information in the utb_insurance table.
*
* PARAMETERS:  
*
*   LynxContactPhone
*   CompanyName
*   CompanyNameShort
*   Address1
*   Address2
*   AddressCity
*   AddressState
*   AddressZip
*   ClientProduct
*   InvoicingModelPayment
*   CompanyFax
*   CompanyPhone
*   EarlyBill
*	EnabledFlag
*   TotalLossValuationPercentage
*   TotalLossPercentage
*   WarrantyWorkmanship
*   WarrantyRefinish
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminUpdateClientInformation
	@iInsuranceCompanyID INT
    , @vLynxContactPhone VARCHAR(50)
    , @vCompanyName VARCHAR(50)
    , @vCompanyNameShort VARCHAR(4)
    , @vAddress1 VARCHAR(50)
    , @vAddress2 VARCHAR(50)
    , @vAddressCity VARCHAR(30)
    , @vAddressState VARCHAR(2)
    , @vAddressZip VARCHAR(8)
    , @vClientProduct VARCHAR(4)
    , @vInvoicingModelPayment VARCHAR(1)
    , @vCompanyFax VARCHAR(12)
    , @vCompanyPhone VARCHAR(12)
    , @bEarlyBill BIT
    , @dTotalLossValuationPercentage DECIMAL(9,5)
    , @dTotalLossPercentage DECIMAL(9,5)
    , @vWarrantyWorkmanship VARCHAR(4)
    , @vWarrantyRefinish VARCHAR(4)
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS int
    DECLARE @rowcount AS int
    DECLARE @now AS datetime 
    
    -- Set Database options
    SET NOCOUNT ON
	SET @now = CURRENT_TIMESTAMP
	
    --Verify that a valid Insurance Company doesn't already exist
    IF NOT EXISTS (SELECT InsuranceCompanyID FROM utb_Insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID)
    BEGIN
        -- Insurance Company doesn't exist
        RETURN 0
    END
    ELSE
    BEGIN
		UPDATE dbo.utb_insurance
		SET
			 Address1 = @vAddress1
			 , Address2 = @vAddress2
			 , AddressCity = @vAddressCity
			 , AddressState = @vAddressState
			 , AddressZip = @vAddressZip
			 , CarrierLynxContactPhone = @vLynxContactPhone
			 , BusinessTypeCD = @vClientProduct
			 , InvoicingModelPaymentCD = @vInvoicingModelPayment
			 , DeskAuditCompanyCD = @vCompanyNameShort
			 , EnabledFlag = @bEarlyBill
			 , FaxAreaCode = SUBSTRING(@vCompanyFax,1,3)
			 , FaxExchangeNumber = SUBSTRING(@vCompanyFax,5,3)
			 , FaxUnitNumber = SUBSTRING(@vCompanyFax,9,4)
			 , [Name] = @vCompanyName
			 , PhoneAreaCode = SUBSTRING(@vCompanyPhone,1,3)
			 , PhoneExchangeNumber = SUBSTRING(@vCompanyPhone,5,3)
			 , PhoneUnitNumber = SUBSTRING(@vCompanyPhone,9,4)
			 , TotalLossValuationWarningPercentage = @dTotalLossValuationPercentage
			 , TotalLossWarningPercentage = @dTotalLossPercentage
			 , WarrantyPeriodRefinishMinCD = @vWarrantyRefinish
			 , WarrantyPeriodWorkmanshipMinCD = @vWarrantyWorkmanship
			 , SysLastUpdatedDate = @now
		WHERE
			InsuranceCompanyID = @iInsuranceCompanyID

			SELECT @@ERROR AS RetCode
		END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminUpdateClientInformation' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminUpdateClientInformation TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAdminUpdateClientInformation TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/