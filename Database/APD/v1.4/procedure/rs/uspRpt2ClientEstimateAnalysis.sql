-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientEstimateAnalysis' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2ClientEstimateAnalysis 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2ClientEstimateAnalysis
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Compiles data displayed on the Desk Audit Summary
*
* PARAMETERS:  
* (I) @InsuranceCompanyID   The insurance company the report is being run for
* (I) @RptMonth             The month the report is for
* (I) @RptYear              The year the report is for
* (I) @ServiceChannelCD     Optional service channel the report should be run for
* (I) @StateCode            Optional state the report may be run for 
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2ClientEstimateAnalysis
    @InsuranceCompanyID     udt_std_int_small,
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL,
    @ServiceChannelCD       udt_std_cd        = NULL,
    @StateCode              udt_addr_state    = NULL
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    -- Declare internal variables

    DECLARE @tmpReportDataMonth TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level1Display               varchar(50)  NOT NULL,
        Level2Group                 varchar(5)   NOT NULL,
        Level2GroupDisplay          varchar(50)  NOT NULL,
        TTLLineFlag                 bit          NOT NULL,
        LineItemID                  int          NOT NULL,
        LineItemDisplay             varchar(50)  NOT NULL,
        IndentLevel                 int          NOT NULL,
        NumberOfEstimates           int          NULL,
        AvgCycleTimeAssToEst        decimal(6,1) NULL,        
        TotalAmount                 decimal(9,2) NULL,
        TotalPercent                decimal(5,2) NULL,
        AvgAmount                   decimal(9,2) NULL,
        TotalHours                  decimal(6,1) NULL,
        AvgHours                    decimal(6,1) NULL,
        AvgRate                     decimal(9,2) NULL
    )

    DECLARE @tmpReportDataQuarter TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level1Display               varchar(50)  NOT NULL,
        Level2Group                 varchar(5)   NOT NULL,
        Level2GroupDisplay          varchar(50)  NOT NULL,
        TTLLineFlag                 bit          NOT NULL,
        LineItemID                  int          NOT NULL,
        LineItemDisplay             varchar(50)  NOT NULL,
        IndentLevel                 int          NOT NULL,
        NumberOfEstimates           int          NULL,
        AvgCycleTimeAssToEst        decimal(6,1) NULL,        
        TotalAmount                 decimal(9,2) NULL,
        TotalPercent                decimal(5,2) NULL,
        AvgAmount                   decimal(9,2) NULL,
        TotalHours                  decimal(6,1) NULL,
        AvgHours                    decimal(6,1) NULL,
        AvgRate                     decimal(9,2) NULL
    )


    DECLARE @tmpReportDataYTD TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level1Display               varchar(50)  NOT NULL,
        Level2Group                 varchar(5)   NOT NULL,
        Level2GroupDisplay          varchar(50)  NOT NULL,
        TTLLineFlag                 bit          NOT NULL,
        LineItemID                  int          NOT NULL,
        LineItemDisplay             varchar(50)  NOT NULL,
        IndentLevel                 int          NOT NULL,
        NumberOfEstimates           int          NULL,
        AvgCycleTimeAssToEst        decimal(6,1) NULL,        
        TotalAmount                 decimal(9,2) NULL,
        TotalPercent                decimal(5,2) NULL,
        AvgAmount                   decimal(9,2) NULL,
        TotalHours                  decimal(6,1) NULL,
        AvgHours                    decimal(6,1) NULL,
        AvgRate                     decimal(9,2) NULL        
    )
    
    DECLARE @tmpReportData12Mth TABLE 
    (
        Level1Group                 varchar(5)   NOT NULL,
        Level1Display               varchar(50)  NOT NULL,
        Level2Group                 varchar(5)   NOT NULL,
        Level2GroupDisplay          varchar(50)  NOT NULL,
        TTLLineFlag                 bit          NOT NULL,
        LineItemID                  int          NOT NULL,
        LineItemDisplay             varchar(50)  NOT NULL,
        IndentLevel                 int          NOT NULL,
        NumberOfEstimates           int          NULL,
        AvgCycleTimeAssToEst        decimal(6,1) NULL,        
        TotalAmount                 decimal(9,2) NULL,
        TotalPercent                decimal(5,2) NULL,
        AvgAmount                   decimal(9,2) NULL,
        TotalHours                  decimal(6,1) NULL,
        AvgHours                    decimal(6,1) NULL,
        AvgRate                     decimal(9,2) NULL        
    )

    DECLARE @tmpClosedEstimates TABLE
    (
        ClaimAspectID               bigint      NOT NULL,
        EstSuppSequenceNumber       tinyint     NOT NULL,
        FactID                      bigint      NULL
    )
    
    DECLARE @tmpTimePeriods TABLE
    (
        PositionID                  tinyint     NOT NULL,
        TimePeriod                  varchar(8)  NOT NULL,
        MonthValue                  tinyint     NOT NULL,
        YearValue                   smallint    NOT NULL
    )

    
    DECLARE @EstimateTotalMonth     decimal(9,2)
    DECLARE @PartsTotalMonth        decimal(9,2)
    DECLARE @LaborTotalMonth        decimal(9,2)
    DECLARE @EstimateTotalQuarter   decimal(9,2)
    DECLARE @PartsTotalQuarter      decimal(9,2)
    DECLARE @LaborTotalQuarter      decimal(9,2)
    DECLARE @EstimateTotalYTD       decimal(9,2)
    DECLARE @PartsTotalYTD          decimal(9,2)
    DECLARE @LaborTotalYTD          decimal(9,2)
    DECLARE @EstimateTotal12Mth     decimal(9,2)
    DECLARE @PartsTotal12Mth        decimal(9,2)
    DECLARE @LaborTotal12Mth        decimal(9,2)
    DECLARE @InsuranceCompanyName   udt_std_name
    DECLARE @InsuranceCompanyLCPhone udt_std_name
    DECLARE @LoopIndex              udt_std_int_tiny
    DECLARE @MonthName              udt_std_name
    DECLARE @RptQuarter             udt_std_int_tiny
    DECLARE @ServiceChannelCDWork   udt_std_cd
    DECLARE @ServiceChannelName     udt_std_name
    DECLARE @StateCodeWork          varchar(2)
    DECLARE @StateName              udt_std_name

    DECLARE @Debug      			udt_std_flag
    DECLARE @now        			udt_std_datetime
    DECLARE @DataWarehouseDate    	udt_std_datetime

    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRpt2ClientEstimateAnalysis'

    SET @Debug = 0


    -- Validate Insurance Company ID
    
    IF (@InsuranceCompanyID IS NULL) OR
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID
        
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
    ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name,
                @InsuranceCompanyLCPhone = IsNull(CarrierLynxContactPhone, '239-337-4300')
          FROM  dbo.utb_insurance
          WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END          
        
   
    -- Validate State Code
    
    IF @StateCode IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT StateCode FROM dbo.utb_state_code WHERE StateCode = @StateCode)
        BEGIN
            -- Invalid StateCode
        
            RAISERROR  ('101|%s|@StateCode|%s', 16, 1, @ProcName, @StateCode)
            RETURN
        END
        ELSE
        BEGIN
            SET @StateCodeWork = @StateCode
            
            SELECT  @StateName = StateValue
              FROM  dbo.utb_state_code
              WHERE StateCode = @StateCode
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @StateCodeWork = '%'
        SET @StateName = 'All'
    END          
        
   
    -- Validate Service Channel Code
    
    IF @ServiceChannelCD IS NOT NULL
    BEGIN
        IF NOT EXISTS(SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') WHERE Code = @ServiceChannelCD)
        BEGIN
            -- Invalid Service Channel CD
        
            RAISERROR  ('101|%s|@ServiceChannelCD|%s', 16, 1, @ProcName, @ServiceChannelCD)
            RETURN
        END
        ELSE
        BEGIN
            SET @ServiceChannelCDWork = @ServiceChannelCD
            
            SELECT  @ServiceChannelName = Name
              FROM  dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
              WHERE Code = @ServiceChannelCD
              
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error
    
                RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
        SET @ServiceChannelCDWork = '%'
        SET @ServiceChannelName = 'All'
    END          
            

    -- Get Current timestamp
    
    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  
    
    
    -- Check Report Date fields and set as necessary
    
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
        SET @RptQuarter = DatePart(qq,  Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END


    -- Compile time periods
    
    SET @LoopIndex = 12

    WHILE @LoopIndex > 0
    BEGIN
        INSERT INTO @tmpTimePeriods
          SELECT 13 - @LoopIndex AS Position,
                 Convert(varchar(2), DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))))
                    + '/'
                    + Convert(varchar(4), DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))),
                 DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))), 
                 DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

   RAISERROR('105|%s|@tmpTimePeriods', 16, 1, @ProcName)
            RETURN
        END
    
        SET @LoopIndex = @LoopIndex - 1
    END


    IF @Debug = 1
    BEGIN
        PRINT '@InsuranceCompanyID = ' + Convert(varchar, @InsuranceCompanyID)
        PRINT '@InsuranceCompanyName = ' + @InsuranceCompanyName
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptMonthName = ' + @MonthName
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear, 101)
        PRINT '@RptQuarter = ' + Convert(varchar, @RptQuarter)
        PRINT '@ServiceChannelCD = ' + @ServiceChannelCD
        PRINT '@ServiceChannelCDWork = ' + @ServiceChannelCDWork
        PRINT '@StateCode = ' + @StateCode
        PRINT '@StateCodeWork = ' + @StateCodeWork
    END
    
    
    -- Compile data needed for the report

    -- This analysis includes only final estimates received prior to claim closing.  Select this subset from the full list.
    
    INSERT INTO @tmpClosedEstimates(ClaimAspectID, EstSuppSequenceNumber)
      SELECT fe.ClaimAspectID,
             Max(fe.EstSuppSequenceNumber) AS MaxSupplement
        FROM dbo.utb_dtwh_fact_estimate fe
        LEFT JOIN dbo.utb_dtwh_dim_customer dc ON (fe.CustomerID = dc.CustomerID)
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (fe.ClaimAspectID = fc.ClaimAspectID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtr ON (fe.TimeIDReceived = dtr.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_disposition_type ddt ON (fc.DispositionTypeID = ddt.DispositionTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_repair_location drl ON (fe.RepairLocationID = drl.RepairLocationID)
        LEFT JOIN dbo.utb_dtwh_dim_state ds ON (drl.StateID = ds.StateID)
        WHERE dc.InsuranceCompanyID = @InsuranceCompanyID
          AND dtr.DateValue <= dtc.DateValue
          AND ddt.DispositionTypeCD <> 'TL'     -- exclude estimates on total loss vehicles
          AND dsc.ServiceChannelCD LIKE @ServiceChannelCDWork
          AND ds.StateCode LIKE @StateCodeWork
		  AND fc.DispositionTypeID NOT IN ( SELECT DispositionTypeID from utb_dtwh_dim_disposition_type WHERE DispositionTypeCD IN ('CA','VD'))
        GROUP BY fe.ClaimAspectID

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedEstimates', 16, 1, @ProcName) 
        RETURN
    END

    UPDATE  @tmpClosedEstimates
      SET   FactID = fe.FactID
      FROM  @tmpClosedEstimates tmp, dbo.utb_dtwh_fact_estimate fe
      WHERE tmp.ClaimAspectID = fe.ClaimAspectID
        AND tmp.EstSuppSequenceNumber = fe.EstSuppSequenceNumber
        
    IF @@ERROR <> 0
    BEGIN
        -- Update failed

        RAISERROR('104|%s|@tmpClosedEstimates', 16, 1, @ProcName)
        RETURN
    END


    ----------------------------------------------------------------------------------------  
    --  Month Data
    ----------------------------------------------------------------------------------------
    
    -- Estimate breakdown
    
    INSERT INTO @tmpReportDataMonth
      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              1,
              16,
              'Total',
              1,
              Count(*),
              Avg(fe.DaysSinceAssignment),
              Sum(fe.TotalAmount),
              NULL,
              Avg(fe.TotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @EstimateTotalMonth = TotalAmount
      FROM  @tmpReportDataMonth
      WHERE Level2Group='Est'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonth
      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              1,
              'Parts and Materials',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsTotalAmount) + Sum(MaterialsAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round((Sum(PartsTotalAmount) + Sum(MaterialsAmount)) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsTotalAmount + MaterialsAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              2,
              'Parts',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsTotalAmount),
              CASE 
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(PartsTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsTotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              3,
              'OEM',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsNewAmount),
              CASE 
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(PartsNewAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsNewAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              4,
              'LKQ',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsLKQAmount),
              CASE 
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(PartsLKQAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsLKQAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              5,
              'Aftermarket',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsAFMKAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(PartsAFMKAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsAFMKAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              6,
              'Remanufactured',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsRemanAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(PartsRemanAmount) / Convert(decimal(9,2), isNUll(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0 
              END AS TotalPercent,
              Avg(PartsRemanAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear

      UNION ALL
      
      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              7,
              'Materials',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(MaterialsAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(MaterialsAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(MaterialsAmount),
              Sum(MaterialsHours),
              Avg(MaterialsHours),
              Avg(MaterialsRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
             
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              8,
              'Labor',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborTotalAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(LaborTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborTotalAmount),
              Sum(LaborTotalHours),
              Avg(LaborTotalHours),
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              9,
              'Body',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborBodyAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(LaborBodyAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborBodyAmount),
              Sum(LaborBodyHours),
              Avg(LaborBodyHours),
              Avg(LaborBodyRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              10,
              'Frame',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborFrameAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(LaborFrameAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborFrameAmount),
              Sum(LaborFrameHours),
              Avg(LaborFrameHours),
              Avg(LaborFrameRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              11,
              'Mechanical',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborMechAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(LaborMechAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborMechAmount),
              Sum(LaborMechHours),
              Avg(LaborMechHours),
              Avg(LaborMechRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              12,
              'Refinish',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborRefinishAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(LaborRefinishAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborRefinishAmount),
              Sum(LaborRefinishHours),
              Avg(LaborRefinishHours),
              Avg(LaborRefinishRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              13,
              'Sublet',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(SubletAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(SubletAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(SubletAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              14,
              'Other Charges',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(OtherAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(OtherAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(OtherAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear

      UNION ALL

      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              15,
              'Tax',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(TaxTotalAmount),
              CASE
                WHEN @EstimateTotalMonth > 0 THEN
                    IsNull(Round(Sum(TaxTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotalMonth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(TaxTotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.MonthOfYear = @RptMonth
          AND dt.YearValue = @RptYear

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
        RETURN
    END
 
                     
    -- Parts Breakdown.  The data has already been pulled and in the table under the estimate breakdown.  We'll just 
    -- grab a copy of the pertinent records.
    
    INSERT INTO @tmpReportDataMonth
      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Parts',
              'Parts Usage Breakdown',
              1,
              5,
              'Total',
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              NULL,
              AvgAmount,
              NULL,
              NULL,
              NULL
        FROM  @tmpReportDataMonth tmp
        WHERE LineItemDisplay = 'Parts'
   
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @PartsTotalMonth = TotalAmount
      FROM  @tmpReportDataMonth
      WHERE Level2Group='Parts' 
        AND LineItemDisplay = 'Total'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonth
      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Parts',
              'Parts Usage Breakdown',
              0,
              CASE LineItemDisplay
                WHEN 'OEM' THEN 1
                WHEN 'LKQ' THEN 2
                WHEN 'Aftermarket' THEN 3
                WHEN 'Remanufactured' THEN 4
              END,
              LineItemDisplay,
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              CASE
                WHEN @PartsTotalMonth > 0 THEN
                    IsNull(Round(TotalAmount / isNull(@PartsTotalMonth, 1), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              AvgAmount,
              NULL,
              NULL,
              NULL
        FROM  @tmpReportDataMonth tmp
        WHERE LineItemDisplay IN ('OEM', 'LKQ', 'Aftermarket', 'Remanufactured')

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
        RETURN
    END


    -- Labor Breakdown.  The data has already been pulled and in the table under the estimate breakdown.  We'll just 
    -- grab a copy of the pertinent records.
    
    INSERT INTO @tmpReportDataMonth
      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Labor',
              'Labor Breakdown',
              1,
              5,
              'Total',
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              NULL,
              AvgAmount,
              TotalHours,
              AvgHours,
              NULL
        FROM  @tmpReportDataMonth tmp
        WHERE LineItemDisplay = 'Labor'
   
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @LaborTotalMonth = TotalAmount
      FROM  @tmpReportDataMonth
      WHERE Level2Group='Labor' 
        AND LineItemDisplay = 'Total'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataMonth
      SELECT  'Month',
              @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Labor',
              'Labor Breakdown',
              0,
              CASE LineItemDisplay
                WHEN 'Body' THEN 1
                WHEN 'Frame' THEN 2
                WHEN 'Mechanical' THEN 3
                WHEN 'Refinish' THEN 4
              END,
              LineItemDisplay,
              1,
 NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              CASE
                WHEN @LaborTotalMonth > 0 THEN
                    IsNull(Round(TotalAmount / isNull(@LaborTotalMonth, 1), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              AvgAmount,
              TotalHours,
              AvgHours,
              AvgRate
        FROM  @tmpReportDataMonth tmp
        WHERE LineItemDisplay IN ('Body', 'Frame', 'Mechanical', 'Refinish')

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
        RETURN
    END

  
    ----------------------------------------------------------------------------------------  
    --  Quarter Data
    ----------------------------------------------------------------------------------------
    
    -- Estimate breakdown
    
    INSERT INTO @tmpReportDataQuarter
      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              1,
              16,
              'Total',
              1,
              Count(*),
              Avg(fe.DaysSinceAssignment),
              Sum(fe.TotalAmount),
              NULL,
              Avg(fe.TotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataQuarter', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @EstimateTotalQuarter = TotalAmount
      FROM  @tmpReportDataQuarter
      WHERE Level2Group='Est'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataQuarter
      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              1,
              'Parts and Materials',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsTotalAmount) + Sum(MaterialsAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round((Sum(PartsTotalAmount) + Sum(MaterialsAmount)) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END  AS TotalPercent,                                              
              Avg(PartsTotalAmount + MaterialsAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              2,
              'Parts',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsTotalAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(PartsTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsTotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              3,
              'OEM',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsNewAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(PartsNewAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsNewAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              4,
              'LKQ',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsLKQAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(PartsLKQAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsLKQAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              5,
              'Aftermarket',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsAFMKAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(PartsAFMKAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsAFMKAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              6,
              'Remanufactured',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsRemanAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(PartsRemanAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsRemanAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear

      UNION ALL
      
      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              7,
              'Materials',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(MaterialsAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(MaterialsAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(MaterialsAmount),
              Sum(MaterialsHours),
              Avg(MaterialsHours),
              Avg(MaterialsRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
             
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              8,
              'Labor',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborTotalAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(LaborTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborTotalAmount),
              Sum(LaborTotalHours),
              Avg(LaborTotalHours),
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              9,
              'Body',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborBodyAmount),
              CASE
             WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(LaborBodyAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborBodyAmount),
              Sum(LaborBodyHours),
              Avg(LaborBodyHours),
              Avg(LaborBodyRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              10,
              'Frame',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborFrameAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(LaborFrameAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborFrameAmount),
              Sum(LaborFrameHours),
              Avg(LaborFrameHours),
              Avg(LaborFrameRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              11,
              'Mechanical',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborMechAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                 IsNull(Round(Sum(LaborMechAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborMechAmount),
              Sum(LaborMechHours),
              Avg(LaborMechHours),
              Avg(LaborMechRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              12,
              'Refinish',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborRefinishAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(LaborRefinishAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborRefinishAmount),
              Sum(LaborRefinishHours),
              Avg(LaborRefinishHours),
              Avg(LaborRefinishRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              13,
              'Sublet',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(SubletAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(SubletAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(SubletAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              14,
              'Other Charges',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(OtherAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(OtherAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,  
              Avg(OtherAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear

      UNION ALL

      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              15,
              'Tax',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(TaxTotalAmount),
              CASE
                WHEN @EstimateTotalQuarter > 0 THEN
                    IsNull(Round(Sum(TaxTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotalQuarter, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(TaxTotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.Quarter = @RptQuarter
          AND dt.YearValue = @RptYear

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataQuarter', 16, 1, @ProcName) 
        RETURN
    END
 
                     
    -- Parts Breakdown.  The data has already been pulled and in the table under the estimate breakdown.  We'll just 
    -- grab a copy of the pertinent records.
    
    INSERT INTO @tmpReportDataQuarter
      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Parts',
              'Parts Usage Breakdown',
              1,
              5,
              'Total',
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              NULL,
              AvgAmount,
              NULL,
              NULL,
              NULL
        FROM  @tmpReportDataQuarter tmp
        WHERE LineItemDisplay = 'Parts'
   
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @PartsTotalQuarter = TotalAmount
      FROM  @tmpReportDataQuarter
      WHERE Level2Group='Parts' 
        AND LineItemDisplay = 'Total'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataQuarter
      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Parts',
              'Parts Usage Breakdown',
              0,
              CASE LineItemDisplay
                WHEN 'OEM' THEN 1
                WHEN 'LKQ' THEN 2
                WHEN 'Aftermarket' THEN 3
                WHEN 'Remanufactured' THEN 4
              END,
             LineItemDisplay,
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              CASE
                WHEN @PartsTotalQuarter > 0 THEN
                    IsNull(Round(TotalAmount / isNull(@PartsTotalQuarter, 1), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              AvgAmount,
              NULL,
              NULL,
              NULL
        FROM  @tmpReportDataQuarter tmp
        WHERE LineItemDisplay IN ('OEM', 'LKQ', 'Aftermarket', 'Remanufactured')

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataQuarter', 16, 1, @ProcName) 
        RETURN
    END


    -- Labor Breakdown.  The data has already been pulled and in the table under the estimate breakdown.  We'll just 
    -- grab a copy of the pertinent records.
    
    INSERT INTO @tmpReportDataQuarter
      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Labor',
              'Labor Breakdown',
              1,
              5,
              'Total',
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              NULL,
              AvgAmount,
              TotalHours,
              AvgHours,
              NULL
        FROM  @tmpReportDataQuarter tmp
        WHERE LineItemDisplay = 'Labor'
   
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataQuarter', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @LaborTotalQuarter = TotalAmount
      FROM  @tmpReportDataQuarter
      WHERE Level2Group='Labor' 
        AND LineItemDisplay = 'Total'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataQuarter
      SELECT  'Qrtr',
              CASE 
                WHEN @RptQuarter = 1 AND @RptMonth = 1 THEN '1st Quarter: January ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 1 THEN '1st Quarter: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 2 AND @RptMonth = 4 THEN '2nd Quarter: April ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 2 THEN '2nd Quarter: April - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 3 AND @RptMonth = 7 THEN '3rd Quarter: July ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 3 THEN '3rd Quarter: July - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
                WHEN @RptQuarter = 4 AND @RptMonth = 10 THEN '4th Quarter: October ' + Convert(varchar(4), @RptYear) 
                WHEN @RptQuarter = 4 THEN '4th Quarter: October - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Labor',
              'Labor Breakdown',
      0,
              CASE LineItemDisplay
                WHEN 'Body' THEN 1
                WHEN 'Frame' THEN 2
                WHEN 'Mechanical' THEN 3
                WHEN 'Refinish' THEN 4
              END,
              LineItemDisplay,
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              CASE
                WHEN @LaborTotalQuarter > 0 THEN
                    IsNull(Round(TotalAmount / isNull(@LaborTotalQuarter, 1), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              AvgAmount,
              TotalHours,
              AvgHours,
              AvgRate
        FROM  @tmpReportDataQuarter tmp
        WHERE LineItemDisplay IN ('Body', 'Frame', 'Mechanical', 'Refinish')

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataQuarter', 16, 1, @ProcName) 
        RETURN
    END


    ----------------------------------------------------------------------------------------  
    --  YTD Data
    ----------------------------------------------------------------------------------------
    
    -- Estimate breakdown
    
    INSERT INTO @tmpReportDataYTD
      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              1,
              16,
              'Total',
              1,
              Count(*),
              Avg(fe.DaysSinceAssignment),
              Sum(fe.TotalAmount),
              NULL,
              Avg(fe.TotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataMonth', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @EstimateTotalYTD = TotalAmount
      FROM  @tmpReportDataYTD
      WHERE Level2Group='Est'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataYTD
      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              1,
              'Parts and Materials',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsTotalAmount) + Sum(MaterialsAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round((Sum(PartsTotalAmount) + Sum(MaterialsAmount)) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsTotalAmount + MaterialsAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              2,
              'Parts',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsTotalAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(PartsTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsTotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              3,
              'OEM',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsNewAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(PartsNewAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsNewAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              4,
              'LKQ',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsLKQAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(PartsLKQAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsLKQAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              5,
              'Aftermarket',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsAFMKAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(PartsAFMKAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsAFMKAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
              
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              6,
              'Remanufactured',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsRemanAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(PartsRemanAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsRemanAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear

      UNION ALL
      
      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              7,
              'Materials',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(MaterialsAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(MaterialsAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(MaterialsAmount),
              Sum(MaterialsHours),
              Avg(MaterialsHours),
              Avg(MaterialsRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
             
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              8,
              'Labor',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborTotalAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(LaborTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborTotalAmount),
              Sum(LaborTotalHours),
              Avg(LaborTotalHours),
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              9,
              'Body',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborBodyAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(LaborBodyAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborBodyAmount),
              Sum(LaborBodyHours),
              Avg(LaborBodyHours),
              Avg(LaborBodyRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              10,
              'Frame',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborFrameAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(LaborFrameAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborFrameAmount),
              Sum(LaborFrameHours),
              Avg(LaborFrameHours),
              Avg(LaborFrameRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              11,
              'Mechanical',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborMechAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(LaborMechAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborMechAmount),
              Sum(LaborMechHours),
              Avg(LaborMechHours),
              Avg(LaborMechRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              12,
              'Refinish',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborRefinishAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(LaborRefinishAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborRefinishAmount),
              Sum(LaborRefinishHours),
              Avg(LaborRefinishHours),
              Avg(LaborRefinishRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              13,
              'Sublet',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(SubletAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(SubletAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(SubletAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              14,
              'Other Charges',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(OtherAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(OtherAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(OtherAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear
            
      UNION ALL

      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              15,
              'Tax',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(TaxTotalAmount),
              CASE
                WHEN @EstimateTotalYTD > 0 THEN
                    IsNull(Round(Sum(TaxTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotalYTD, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(TaxTotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        WHERE dt.YearValue = @RptYear

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTD', 16, 1, @ProcName) 
        RETURN
    END
 
                     
    -- Parts Breakdown.  The data has already been pulled and in the table under the estimate breakdown.  We'll just 
    -- grab a copy of the pertinent records.
    
    INSERT INTO @tmpReportDataYTD
      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Parts',
              'Parts Usage Breakdown',
              1,
              5,
              'Total',
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              NULL,
              AvgAmount,
              NULL,
              NULL,
              NULL
        FROM  @tmpReportDataYTD tmp
        WHERE LineItemDisplay = 'Parts'
   
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTD', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @PartsTotalYTD = TotalAmount
      FROM  @tmpReportDataYTD
      WHERE Level2Group='Parts' 
        AND LineItemDisplay = 'Total'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataYTD
      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Parts',
              'Parts Usage Breakdown',
              0,
              CASE LineItemDisplay
                WHEN 'OEM' THEN 1
                WHEN 'LKQ' THEN 2
                WHEN 'Aftermarket' THEN 3
                WHEN 'Remanufactured' THEN 4
              END,
              LineItemDisplay,
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              CASE
                WHEN @PartsTotalYTD > 0 THEN
                    IsNull(Round(TotalAmount / isNull(@PartsTotalYTD, 1), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              AvgAmount,
              NULL,
              NULL,
              NULL
        FROM  @tmpReportDataYTD tmp
        WHERE LineItemDisplay IN ('OEM', 'LKQ', 'Aftermarket', 'Remanufactured')

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTD', 16, 1, @ProcName) 
        RETURN
    END


    -- Labor Breakdown.  The data has already been pulled and in the table under the estimate breakdown.  We'll just 
    -- grab a copy of the pertinent records.
    
    INSERT INTO @tmpReportDataYTD
      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Labor',
              'Labor Breakdown',
              1,
              5,
              'Total',
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              NULL,
              AvgAmount,
              TotalHours,
              AvgHours,
              NULL
        FROM  @tmpReportDataYTD tmp
        WHERE LineItemDisplay = 'Labor'
   
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTD', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @LaborTotalYTD = TotalAmount
      FROM  @tmpReportDataYTD
      WHERE Level2Group='Labor' 
        AND LineItemDisplay = 'Total'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportDataYTD
      SELECT  'YTD',
              CASE 
                WHEN @RptMonth = 1 THEN 'Year To Date: January ' + Convert(varchar(4), @RptYear) 
                ELSE 'Year To Date: January - ' + @MonthName + ' ' + Convert(varchar(4), @RptYear)
              END,
              'Labor',
              'Labor Breakdown',
              0,
              CASE LineItemDisplay
                WHEN 'Body' THEN 1
                WHEN 'Frame' THEN 2
                WHEN 'Mechanical' THEN 3
                WHEN 'Refinish' THEN 4
              END,
              LineItemDisplay,
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              CASE
                WHEN @LaborTotalYTD > 0 THEN
                    IsNull(Round(TotalAmount / isNull(@LaborTotalYTD, 1), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              AvgAmount,
              TotalHours,
              AvgHours,
              AvgRate
        FROM  @tmpReportDataYTD tmp
        WHERE LineItemDisplay IN ('Body', 'Frame', 'Mechanical', 'Refinish')

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportDataYTD', 16, 1, @ProcName) 
        RETURN
    END

  
    ----------------------------------------------------------------------------------------  
    --  Previous 12 Month Data
    ----------------------------------------------------------------------------------------
    
    -- Estimate breakdown
    
    INSERT INTO @tmpReportData12Mth
      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              1,
              16,
              'Total',
              1,
              Count(*),
              Avg(fe.DaysSinceAssignment),
              Sum(fe.TotalAmount),
              NULL,
              Avg(fe.TotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData12Mth', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @EstimateTotal12Mth = TotalAmount
      FROM  @tmpReportData12Mth
      WHERE Level2Group='Est'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportData12Mth
      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              1,
              'Parts and Materials',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsTotalAmount) + Sum(MaterialsAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round((Sum(PartsTotalAmount) + Sum(MaterialsAmount)) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsTotalAmount + MaterialsAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
              
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              2,
              'Parts',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsTotalAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(PartsTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsTotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
              
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              3,
              'OEM',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsNewAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(PartsNewAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsNewAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
              
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              4,
              'LKQ',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsLKQAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(PartsLKQAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsLKQAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
              
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              5,
              'Aftermarket',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsAFMKAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(PartsAFMKAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsAFMKAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
              
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              6,
              'Remanufactured',
              3,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(PartsRemanAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(PartsRemanAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(PartsRemanAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 

      UNION ALL
      
      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              7,
              'Materials',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(MaterialsAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(MaterialsAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(MaterialsAmount),
              Sum(MaterialsHours),
              Avg(MaterialsHours),
              Avg(MaterialsRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
             
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              8,
              'Labor',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborTotalAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(LaborTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborTotalAmount),
              Sum(LaborTotalHours),
              Avg(LaborTotalHours),
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              9,
              'Body',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborBodyAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(LaborBodyAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborBodyAmount),
              Sum(LaborBodyHours),
              Avg(LaborBodyHours),
              Avg(LaborBodyRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              10,
              'Frame',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborFrameAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(LaborFrameAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborFrameAmount),
              Sum(LaborFrameHours),
              Avg(LaborFrameHours),
              Avg(LaborFrameRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              11,
              'Mechanical',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborMechAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(LaborMechAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(LaborMechAmount),
              Sum(LaborMechHours),
              Avg(LaborMechHours),
              Avg(LaborMechRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              12,
              'Refinish',
              2,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(LaborRefinishAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(LaborRefinishAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
          END AS TotalPercent,
              Avg(LaborRefinishAmount),
              Sum(LaborRefinishHours),
              Avg(LaborRefinishHours),
              Avg(LaborRefinishRate)
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              13,
              'Sublet',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(SubletAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(SubletAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(SubletAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              14,
              'Other Charges',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(OtherAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(OtherAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(OtherAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 
            
      UNION ALL

      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Est',
              'Aggregate Estimate Breakdown',
              0,
              15,
              'Tax',
              1,
              Count(*),
              Avg(DaysSinceAssignment),
              Sum(TaxTotalAmount),
              CASE
                WHEN @EstimateTotal12Mth > 0 THEN
                    IsNull(Round(Sum(TaxTotalAmount) / Convert(decimal(9,2), isNull(@EstimateTotal12Mth, 1)), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              Avg(TaxTotalAmount),
              NULL,
              NULL,
              NULL
        FROM  @tmpClosedEstimates tmp
        LEFT JOIN dbo.utb_dtwh_fact_estimate fe ON (tmp.FactID = fe.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON (fe.TimeIDReceived = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON (Convert(varchar(2), dt.MonthOfYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod) 

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData12Mth', 16, 1, @ProcName) 
        RETURN
    END
 
                     
    -- Parts Breakdown.  The data has already been pulled and in the table under the estimate breakdown.  We'll just 
    -- grab a copy of the pertinent records.
    
    INSERT INTO @tmpReportData12Mth
      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Parts',
              'Parts Usage Breakdown',
              1,
              5,
              'Total',
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              NULL,
              AvgAmount,
              NULL,
              NULL,
              NULL
        FROM  @tmpReportData12Mth tmp
        WHERE LineItemDisplay = 'Parts'
   
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData12Mth', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @PartsTotal12Mth = TotalAmount
      FROM  @tmpReportData12Mth
      WHERE Level2Group='Parts' 
        AND LineItemDisplay = 'Total'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportData12Mth
      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Parts',
              'Parts Usage Breakdown',
              0,
              CASE LineItemDisplay
                WHEN 'OEM' THEN 1
                WHEN 'LKQ' THEN 2
                WHEN 'Aftermarket' THEN 3
                WHEN 'Remanufactured' THEN 4
              END,
              LineItemDisplay,
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              CASE
                WHEN @PartsTotal12Mth > 0 THEN
                    IsNull(Round(TotalAmount / isNull(@PartsTotal12Mth, 1), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              AvgAmount,
              NULL,
              NULL,
              NULL
        FROM  @tmpReportData12Mth tmp
        WHERE LineItemDisplay IN ('OEM', 'LKQ', 'Aftermarket', 'Remanufactured')

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData12Mth', 16, 1, @ProcName) 
        RETURN
    END


    -- Labor Breakdown.  The data has already been pulled and in the table under the estimate breakdown.  We'll just 
    -- grab a copy of the pertinent records.
    
    INSERT INTO @tmpReportData12Mth
      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Labor',
              'Labor Breakdown',
              1,
              5,
              'Total',
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              NULL,
              AvgAmount,
              TotalHours,
              AvgHours,
              NULL
        FROM  @tmpReportData12Mth tmp
        WHERE LineItemDisplay = 'Labor'
   
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData12Mth', 16, 1, @ProcName) 
        RETURN
    END

    SELECT  @LaborTotal12Mth = TotalAmount
      FROM  @tmpReportData12Mth
      WHERE Level2Group='Labor' 
        AND LineItemDisplay = 'Total'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO @tmpReportData12Mth
      SELECT  '12Mth',
              '12 Months Preceding ' + @MonthName + ' ' + Convert(varchar(4), @RptYear),
              'Labor',
              'Labor Breakdown',
              0,
              CASE LineItemDisplay
                WHEN 'Body' THEN 1
                WHEN 'Frame' THEN 2
                WHEN 'Mechanical' THEN 3
                WHEN 'Refinish' THEN 4
              END,
              LineItemDisplay,
              1,
              NumberOfEstimates,
              AvgCycleTimeAssToEst,        
              TotalAmount,
              CASE
                WHEN @LaborTotal12Mth > 0 THEN
                    IsNull(Round(TotalAmount / isNull(@LaborTotal12Mth, 1), 4) * 100, 0)
                ELSE 0
              END AS TotalPercent,
              AvgAmount,
              TotalHours,
              AvgHours,
              AvgRate
        FROM  @tmpReportData12Mth tmp
        WHERE LineItemDisplay IN ('Body', 'Frame', 'Mechanical', 'Refinish')

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpReportData12Mth', 16, 1, @ProcName) 
        RETURN
    END
 

    ----------------------------------------------------------------------------------------  
    --  Final Select
    ----------------------------------------------------------------------------------------
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @ServiceChannelName AS ServiceChannelName,
            @StateName AS StateName,
            1 AS Level1Sort,
            CASE Level2Group
              WHEN 'Est' THEN 1
              WHEN 'Parts' THEN 2
              WHEN 'Labor' THEN 3
            END AS Level2Sort,              
            tmp.*,
            @DataWareHouseDate as DataWareDate  
      FROM  @tmpReportDataMonth tmp
    
    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @ServiceChannelName AS ServiceChannelName,
            @StateName AS StateName,
            2 AS Level1Sort,
            CASE Level2Group
              WHEN 'Est' THEN 1
              WHEN 'Parts' THEN 2
              WHEN 'Labor' THEN 3
            END AS Level2Sort,              
            tmp.*,
            @DataWareHouseDate as DataWareDate  
      FROM  @tmpReportDataQuarter tmp
    
    UNION ALL

    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @ServiceChannelName AS ServiceChannelName,
            @StateName AS StateName,
            3 AS Level1Sort,
            CASE Level2Group
              WHEN 'Est' THEN 1
              WHEN 'Parts' THEN 2
              WHEN 'Labor' THEN 3
            END AS Level2Sort,              
            tmp.*,
            @DataWareHouseDate as DataWareDate   
      FROM  @tmpReportDataYTD tmp

    UNION ALL
    
    SELECT  @InsuranceCompanyLCPhone AS LynxPhoneNumber,
            @InsuranceCompanyName AS InsuranceCompanyName,
            @ServiceChannelName AS ServiceChannelName,
            @StateName AS StateName,
            4 AS Level1Sort,
            CASE Level2Group
              WHEN 'Est' THEN 1
              WHEN 'Parts' THEN 2
              WHEN 'Labor' THEN 3
            END AS Level2Sort,              
            tmp.*,
            @DataWareHouseDate as DataWareDate 
      FROM  @tmpReportData12Mth tmp
             
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2ClientEstimateAnalysis' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2ClientEstimateAnalysis TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
