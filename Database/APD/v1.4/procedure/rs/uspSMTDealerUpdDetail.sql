-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDealerUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTDealerUpdDetail 
END

GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTDealerUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Update a Dealer
*
* PARAMETERS:  
* (I) @DealerID                           The Dealer's unique identity
* (I) @Address1                           The Dealer's address line 1
* (I) @Address2                           The Dealer's address line 2
* (I) @AddressCity                        The Dealer's address city
* (I) @AddressCounty                      The Dealer's address county
* (I) @AddressState                       The Dealer's address state
* (I) @AddressZip                         The Dealer's address zip code
* (I) @FaxAreaCode                        The Dealer's fax area code
* (I) @FaxExchangeNumber                  The Dealer's fax exchange number
* (I) @FaxExtensionNumber                 The Dealer's fax extension number
* (I) @FaxUnitNumber                      The Dealer's fax unit number
* (I) @Name                               The Dealer's name
* (I) @PhoneAreaCode                      The Dealer's phone area code
* (I) @PhoneExchangeNumber                The Dealer's phone exchange number
* (I) @PhoneExtensionNumber               The Dealer's phone extension number
* (I) @PhoneUnitNumber                    The Dealer's phone unit number
* (I) @SysLastUserID                      The Dealer's updating user identity
* (I) @SysLastUpdatedDate                 The Dealer's last update date as string (VARCHAR(30))
*
* RESULT SET:
* DealerID                                The updated Dealer unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTDealerUpdDetail
(
	@DealerID                    	        udt_std_int_big,
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @Name                               udt_std_name,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @SysLastUserID                      udt_std_id,
    @SysLastUpdatedDate                 VARCHAR(30),
    @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @tupdated_date     AS DATETIME 
    DECLARE @temp_updated_date AS DATETIME 

    DECLARE @ProcName          AS VARCHAR(30)       -- Used for raise error stmts 

    DECLARE @LogComment                 udt_std_desc_long

    DECLARE @dbName                     udt_std_name,
            @dbAddress1                 udt_addr_line_1,
            @dbAddress2                 udt_addr_line_2,
            @dbAddressZip               udt_addr_zip_code,
            @dbAddressCity              udt_addr_city,
            @dbAddressState             udt_addr_state,
            @dbAddressCounty            udt_addr_county,
            @dbPhoneAreaCode            udt_ph_area_code,
            @dbPhoneExchangeNumber      udt_ph_exchange_number,
            @dbPhoneUnitNumber          udt_ph_unit_number,
            @dbPhoneExtensionNumber     udt_ph_extension_number,
            @dbFaxAreaCode              udt_ph_area_code,
            @dbFaxExchangeNumber        udt_ph_exchange_number,
            @dbFaxUnitNumber            udt_ph_unit_number,
            @dbFaxExtensionNumber       udt_ph_extension_number
            

    SET @ProcName = 'uspSMTDealerUpdDetail'


    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0 SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0 SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0 SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0 SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0 SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0 SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL


    -- Apply edits
    
    IF LEN(@Name) < 1
    BEGIN
       -- Missing Dealer Name
        RAISERROR('101|%s|@Name|%s', 16, 1, @ProcName, @Name)
        RETURN
    END

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Dealer Address State
            RAISERROR('101|%s|@AddressState|%s', 16, 1, @ProcName, @AddressState)
            RETURN
        END
    END

    -- Validate the updated date parameter
    
    IF @SysLastUpdatedDate IS NULL OR 
       ISDATE(@SysLastUpdatedDate) = 0
    BEGIN
        -- Invalid updated date value
        RAISERROR('101|%s|@SysLastUpdatedDate|%s', 16, 1, @ProcName, @SysLastUpdatedDate)
        RETURN
    END
    
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Validate the updated date
    
    IF @tupdated_date IS NULL
    BEGIN 
        -- Invalid updated date value
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END 
        
    -- Check the date
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_dealer', @DealerID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END    


    IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
    BEGIN
        -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END    
    
    
    SELECT  @dbName = Name,
            @dbAddress1 = Address1,
            @dbAddress2 = Address2,
            @dbAddressZip = AddressZip,
            @dbAddressCity = AddressCity,
            @dbAddressState = AddressState,
            @dbAddressCounty = AddressCounty,
            @dbPhoneAreaCode = PhoneAreaCode,
            @dbPhoneExchangeNumber = PhoneExchangeNumber,
            @dbPhoneUnitNumber = PhoneUnitNumber,
            @dbPhoneExtensionNumber = PhoneExtensionNumber,
            @dbFaxAreaCode = FaxAreaCode,
            @dbFaxExchangeNumber = FaxExchangeNumber,
            @dbFaxUnitNumber = FaxUnitNumber,
            @dbFaxExtensionNumber = FaxExtensionNumber
    FROM dbo.utb_dealer
    WHERE DealerID = @DealerID
    

    IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspSMTBusinessParentUpdDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    
    

    -- Begin Update(s)

    BEGIN TRANSACTION AdmDealerUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    UPDATE  dbo.utb_dealer SET
            Address1 = @Address1,
            Address2 = @Address2,
            AddressCity = @AddressCity,
            AddressCounty = @AddressCounty,
            AddressState = @AddressState,
            AddressZip = @AddressZip,
            FaxAreaCode = @FaxAreaCode,
            FaxExchangeNumber = @FaxExchangeNumber,
            FaxExtensionNumber = @FaxExtensionNumber,
            FaxUnitNumber = @FaxUnitNumber,
            Name = @Name,
            PhoneAreaCode = @PhoneAreaCode,
            PhoneExchangeNumber = @PhoneExchangeNumber,
            PhoneExtensionNumber = @PhoneExtensionNumber,
            PhoneUnitNumber = @PhoneUnitNumber,
            SysLastUserID = @SysLastUserID,
            SysLastUpdatedDate = CURRENT_TIMESTAMP
      WHERE DealerID = @DealerID
        AND SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('104|%s|utb_dealer', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    
    -- Begin Audit Log code  -------------------------------------------------------------------------------------
           
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbName, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Name, ''))))
    BEGIN
      SET @LogComment = 'Dealer Name changed from ' + @dbName + ' to ' + @Name
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddress1, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Address1, ''))))
    BEGIN
      SET @LogComment = 'Dealer Address 1 was changed from ' + 
                        ISNULL(@dbAddress1, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@Address1, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddress2, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Address2, ''))))
    BEGIN
      SET @LogComment = 'Dealer Address 2 was changed from ' + 
                        ISNULL(@dbAddress2, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@Address2, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressZip, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressZip, ''))))
    BEGIN
      SET @LogComment = 'Dealer Zip Code was changed from ' + 
                        ISNULL(convert(varchar(12), @dbAddressZip), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @AddressZip), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressCity, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressCity, ''))))
    BEGIN
      SET @LogComment = 'Dealer City was changed from ' + 
                        ISNULL(@dbAddressCity, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@AddressCity, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressState, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressState, ''))))
    BEGIN
      SET @LogComment = 'Dealer State was changed from ' + 
                        ISNULL(convert(varchar(12), @dbAddressState), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @AddressState), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressCounty, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressCounty, ''))))
    BEGIN
      SET @LogComment = 'Dealer County was changed from ' + 
                        ISNULL(@dbAddressCounty, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@AddressCounty, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Dealer Phone Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Dealer Phone Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
      
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Dealer Phone Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Dealer Phone Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Dealer Fax Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Dealer Fax Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Dealer Fax Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
          
          ROLLBACK TRANSACTION
          RETURN
      END
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Dealer Fax Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @DealerID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error
          
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    -- End of Audit Log code  ------------------------------------------------------------------------------------


    COMMIT TRANSACTION AdmDealerUpdDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @DealerID AS DealerID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTDealerUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTDealerUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO

