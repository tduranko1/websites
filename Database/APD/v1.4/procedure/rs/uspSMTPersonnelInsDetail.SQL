-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTPersonnelInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTPersonnelInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspSMTPersonnelInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Insert a new Personnel record for the Shop or Business Info based on the Entity type
*
* PARAMETERS:  
* (I) @EntityID                            The Personnel's Entity identity
* (I) @EntityType                          Resolves the EntityID to a Shop (utb_shop_location) or Business Info (utb_shop) ID
* (I) @PersonnelTypeID                     The Personnel's type
* (I) @PreferredContactMethodID            The Personnel's preferred contact method
* (I) @CellAreaCode                        The Personnel's cell area code
* (I) @CellExchangeNumber                  The Personnel's cell exchange number
* (I) @CellExtensionNumber                 The Personnel's cell extension number
* (I) @CellUnitNumber                      The Personnel's cell unit number
* (I) @EmailAddress                        The Personnel's email address
* (I) @FaxAreaCode                         The Personnel's fax area code
* (I) @FaxExchangeNumber                   The Personnel's fax exchange number
* (I) @FaxExtensionNumber                  The Personnel's fax extension number
* (I) @FaxUnitNumber                       The Personnel's fax unit number
* (I) @GenderCD                            The Personnel's gender cd
* (I) @MinorityFlag                        The Personnel's minority flag
* (I) @Name                                The Personnel's name
* (I) @PagerAreaCode                       The Personnel's pager area code
* (I) @PagerExchangeNumber                 The Personnel's pager exchange number
* (I) @PagerExtensionNumber                The Personnel's pager extension number
* (I) @PagerUnitNumber                     The Personnel's pager unit number
* (I) @PhoneAreaCode                       The Personnel's phone area code
* (I) @PhoneExchangeNumber                 The Personnel's phone exchange number
* (I) @PhoneExtensionNumber                The Personnel's phone extension number
* (I) @PhoneUnitNumber                     The Personnel's phone unit number
* (I) @ShopManagerFlag                     The Personnel's shop manager flag
* (I) @SysLastUserID                       The Personnel's updating user identity
*
* RESULT SET:
* EntityID                                 The Entity identity for the personnel (echoed)
* EntityType                               The Entity Type for the personnel (echoed)             
* PersonnelID                              The newly created Shop Personnel unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTPersonnelInsDetail
(
    @EntityID                           udt_std_id_big,
    @EntityType                         udt_std_cd,
    @PersonnelTypeID                    udt_std_int_tiny=NULL,
    @PreferredContactMethodID           udt_std_int_tiny,
    @CellAreaCode                       udt_ph_area_code=NULL,
    @CellExchangeNumber                 udt_ph_exchange_number=NULL,
    @CellExtensionNumber                udt_ph_extension_number=NULL,
    @CellUnitNumber                     udt_ph_unit_number=NULL,
    @EmailAddress                       udt_web_email=NULL,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @GenderCD                           udt_per_gender_cd=NULL,
    @MinorityFlag                       udt_std_flag=0,
    @Name                               udt_std_name,
    @PagerAreaCode                      udt_ph_area_code=NULL,
    @PagerExchangeNumber                udt_ph_exchange_number=NULL,
    @PagerExtensionNumber               udt_ph_extension_number=NULL,
    @PagerUnitNumber                    udt_ph_unit_number=NULL,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @ShopManagerFlag                    udt_std_flag=0,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error                            udt_std_int
    DECLARE @rowcount                         udt_std_int
    DECLARE @PersonnelID                      udt_std_id_big
    DECLARE @ProcName                         varchar(30)       -- Used for raise error stmts 
    DECLARE @ShopPrimaryContactCode           udt_std_int_tiny
    DECLARE @ShopContactCode                  udt_std_int_tiny
    DECLARE @BusinessPrimaryContactCode       udt_std_int_tiny
    DECLARE @BusinessContactCode              udt_std_int_tiny
    DECLARE @tmpPersonnelID                   udt_std_int
    DECLARE @tmpPersonnelTypeID               udt_std_int_tiny
            
    DECLARE @KeyDescription                   udt_std_desc_short,
            @LogComment                       udt_std_desc_long,
            @EntityName                       udt_std_name,
            @EntityTypeName                   udt_std_name,
            @newValue                         udt_std_desc_long
    
    SET @ProcName = 'uspSMTPersonnelInsDetail'
    SET @BusinessPrimaryContactCode = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Owner/Officer #1')
    SET @BusinessContactCode = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Owner/Officer #2')
    SET @ShopPrimaryContactCode = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Shop Manager')
    SET @ShopContactCode = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Estimator')
    
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@CellAreaCode))) = 0 SET @CellAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@CellExchangeNumber))) = 0 SET @CellExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@CellExtensionNumber))) = 0 SET @CellExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@CellUnitNumber))) = 0 SET @CellUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@EmailAddress))) = 0 SET @EmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PagerAreaCode))) = 0 SET @PagerAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PagerExchangeNumber))) = 0 SET @PagerExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PagerExtensionNumber))) = 0 SET @PagerExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PagerUnitNumber))) = 0 SET @PagerUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL


    -- Apply edits
    
    IF @GenderCD IS NOT NULL
    BEGIN
        IF @GenderCD NOT IN ('U', 'M', 'F')
        BEGIN
           -- Invalid Gender
            RAISERROR('101|%s|@GenderCD|%s', 16, 1, @ProcName, @GenderCD)
            RETURN
        END
    END

    IF LEN(@Name) < 1
    BEGIN
       -- Missing Personnel Name
        RAISERROR('101|%s|@Name|%s', 16, 1, @ProcName, @Name)
        RETURN
    END

    
    IF (@EntityType IS NULL) OR (@EntityType NOT IN ('S', 'B'))
    BEGIN
        -- Invalid Personnel Shop
        RAISERROR('101|%s|@EntityType|%s', 16, 1, @ProcName, @EntityType)
        RETURN
    END
    
    IF (@EntityID IS NOT NULL)
    BEGIN
        IF (@EntityType = 'S')
        BEGIN
            IF NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE ShopLocationID = @EntityID
                                                                      AND EnabledFlag = 1)
            BEGIN
                -- Invalid Personnel Shop
                RAISERROR('101|%s|@EntityID|%u', 16, 1, @ProcName, @EntityID)
                RETURN
            END
        END
        ELSE
        BEGIN
            IF NOT EXISTS (SELECT ShopID FROM utb_shop WHERE ShopID = @EntityID
                                                    AND EnabledFlag = 1)
            BEGIN
                -- Invalid Personnel Business Info
                RAISERROR('101|%s|@EntityID|%u', 16, 1, @ProcName, @EntityID)
                RETURN
            END
        END
    END
    ELSE
    BEGIN
       -- Invalid Personnel Shop Location
        RAISERROR('101|%s|@EntityID|%u', 16, 1, @ProcName, @EntityID)
        RETURN
    END

    IF @PersonnelTypeID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT PersonnelTypeID FROM utb_personnel_type WHERE PersonnelTypeID = @PersonnelTypeID
                                                                        AND EnabledFlag = 1)
        BEGIN
           -- Invalid Personnel Type
            RAISERROR('101|%s|@PersonnelTypeID|%u', 16, 1, @ProcName, @PersonnelTypeID)
            RETURN
        END
    END
    ELSE
    BEGIN
       SET @PersonnelTypeID = 2
       -- Invalid Personnel Type
       -- RAISERROR('101|%s|@PersonnelTypeID|%u', 16, 1, @ProcName, @PersonnelTypeID)
       -- RETURN
    END

    IF @PreferredContactMethodID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT PersonnelContactMethodID FROM utb_personnel_contact_method WHERE PersonnelContactMethodID = @PreferredContactMethodID
                                                                                           AND EnabledFlag = 1)
        BEGIN
           -- Invalid Preferred Contact Method
            RAISERROR('101|%s|@PreferredContactMethodID|%u', 16, 1, @ProcName, @PreferredContactMethodID)
            RETURN
        END
    END


    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopLocPerInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    DECLARE @tmpPersonnel TABLE (PersonnelID     bigint    NOT NULL)
    
    -----------------------------------------------------------------------------------------------------------------
    -- If this is a 'Shop Primary Contact', make sure it is the only primary contact for this Shop.
    IF (@PersonnelTypeID = @ShopPrimaryContactCode)
    BEGIN
      -- get a list of all personnel for the Shop (location) with which this person is associated 
      -- Gather any other 'Shop Primary Contact's associated with this shop
      INSERT INTO @tmpPersonnel
      SELECT slp.PersonnelID
      FROM dbo.utb_shop_location_personnel slp INNER JOIN dbo.utb_personnel p ON slp.PersonnelID = p.PersonnelID
      WHERE p.PersonnelTypeID = @ShopPrimaryContactCode
        AND slp.ShopLocationID = @EntityID
            
     
      -- Update any other 'Shop Primary Contact's for this shop to 'Shop Contact's
      UPDATE dbo.utb_personnel
      SET PersonnelTypeID = @ShopContactCode
      FROM dbo.utb_personnel p INNER JOIN @tmpPersonnel tp ON p.PersonnelID = tp.PersonnelID
      
      DELETE @tmpPersonnel      
      
    END
    
    
    -- If this is a 'Business Primary Contact', make sure it is the only primary contact for this Business.
    IF (@PersonnelTypeID = @BusinessPrimaryContactCode)
    BEGIN
      
      -- Gather any other 'Owner Officer #1's associated with this business
      INSERT INTO @tmpPersonnel
      SELECT sp.PersonnelID
      FROM dbo.utb_shop_personnel sp INNER JOIN dbo.utb_personnel p ON sp.PersonnelID = p.PersonnelID
      WHERE p.PersonnelTypeID = @BusinessPrimaryContactCode
        AND ShopID = @EntityID
                      
      
      -- Update any other 'Owner Officer #1's for this shop to 'Business Contact's
      UPDATE dbo.utb_personnel
      SET PersonnelTypeID = @BusinessContactCode
      FROM dbo.utb_personnel p INNER JOIN @tmpPersonnel tp ON p.PersonnelID = tp.PersonnelID
            
    END
    
    IF (@@ERROR <> 0)
    BEGIN
      -- SQL Server Error
      RAISERROR('99|%s', 16, 1, @ProcName)
      RETURN
    END 
    ----------------------------------------------------------------------------------------------------------------



    INSERT INTO dbo.utb_personnel
    (
        PersonnelTypeID,
        PreferredContactMethodID,
        CellAreaCode,
        CellExchangeNumber,
        CellExtensionNumber,
        CellUnitNumber,
        EmailAddress,
        FaxAreaCode,
        FaxExchangeNumber,
        FaxExtensionNumber,
        FaxUnitNumber,
        GenderCD,
        MinorityFlag,
        Name,
        PagerAreaCode,
        PagerExchangeNumber,
        PagerExtensionNumber,
        PagerUnitNumber,
        PhoneAreaCode,
        PhoneExchangeNumber,
        PhoneExtensionNumber,
        PhoneUnitNumber,
        ShopManagerFlag,
        SysLastUserID,
        SysLastUpdatedDate
    )
    VALUES	
	(
        @PersonnelTypeID,
        @PreferredContactMethodID,
        @CellAreaCode,
        @CellExchangeNumber,
        @CellExtensionNumber,
        @CellUnitNumber,
        @EmailAddress,
        @FaxAreaCode,
        @FaxExchangeNumber,
        @FaxExtensionNumber,
        @FaxUnitNumber,
        @GenderCD,
        @MinorityFlag,
        @Name,
        @PagerAreaCode,
        @PagerExchangeNumber,
        @PagerExtensionNumber,
        @PagerUnitNumber,
        @PhoneAreaCode,
        @PhoneExchangeNumber,
        @PhoneExtensionNumber,
        @PhoneUnitNumber,
        @ShopManagerFlag,
        @SysLastUserID,
        CURRENT_TIMESTAMP
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    SET @PersonnelID = SCOPE_IDENTITY()
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_personnel', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    IF (@EntityType = 'S')
    BEGIN
        SET @EntityTypeName = 'Shop'
        SET @EntityName = (SELECT Name FROM dbo.utb_shop_location WHERE ShopLocationID = @EntityID)
    
        INSERT INTO dbo.utb_shop_location_personnel
        (
            ShopLocationID,
            PersonnelID,
            SysLastUserID,
            SysLastUpdatedDate
        )
        VALUES
        (
            @EntityID,
            @PersonnelID,
            @SysLastUserID,
            CURRENT_TIMESTAMP
        )

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
            RAISERROR('105|%s|utb_shop_location_personnel', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END
    ELSE
    BEGIN
        SET @EntityTypeName = 'Business'
        SET @EntityName = (SELECT Name FROM dbo.utb_shop WHERE ShopID = @EntityID)
        
        INSERT INTO dbo.utb_shop_personnel
        (
            ShopID,
            PersonnelID,
            SysLastUserID,
            SysLastUpdatedDate
        )
        VALUES
        (
            @EntityID,
            @PersonnelID,
            @SysLastUserID,
            CURRENT_TIMESTAMP
        )

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
            RAISERROR('105|%s|utb_shop_personnel', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END
       
    -- Begin Audit Log Code  -----------------------------------------------------------------------------------
    
    SET @KeyDescription = CONVERT(varchar(10), @PersonnelID) + ' - ' + @Name
    SET @LogComment = 'Personnel created for ' + @EntityTypeName + ', ' + @EntityName + ':  ' + @Name
    
    EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
            
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
        ROLLBACK TRANSACTION
        RETURN
    END  
           
    
    IF @PersonnelTypeID IS NOT NULL
    BEGIN
      
      SET @newValue = (SELECT Name FROM dbo.utb_personnel_type WHERE PersonnelTypeID = @PersonnelTypeID)
    
      SET @LogComment = 'Preferred Contact Method established: ' + ISNULL(CONVERT(varchar(12), @newValue), 'NON-EXISTENT')
      
                       
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
          
    
    IF @ShopManagerFlag IS NOT NULL
    BEGIN
      SET @LogComment = 'Shop Manager flag established: ' + (SELECT CASE @ShopManagerFlag WHEN 0 THEN 'ON' ELSE 'OFF' END)
      
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF @EmailAddress IS NOT NULL
    BEGIN
      SET @LogComment = 'Email Address established: ' + @EmailAddress
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PreferredContactMethodID IS NOT NULL
    BEGIN
      
      SET @newValue = (SELECT Name FROM dbo.utb_personnel_contact_method WHERE PersonnelContactMethodID = @PreferredContactMethodID)
    
      SET @LogComment = 'Preferred Contact Method established: ' + ISNULL(CONVERT(varchar(12), @newValue), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END            
             
    
    ---------
    
    IF @PhoneAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Area Code established: ' + @PhoneAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF @PhoneExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Exchange Number established: ' + @PhoneExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PhoneUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Unit Number established: ' + @PhoneUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PhoneExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Extension Number established: ' + @PhoneExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    ------------      
          
    IF @FaxAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Area Code established: ' + @FaxAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF @FaxExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Exchange Number established: ' + @FaxExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Unit Number established: ' + @FaxUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @FaxExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Extension Number established: ' + @FaxExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
            
    ------------------
              
                
    IF @CellAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Cell Area Code established: ' + @CellAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF @CellExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Cell Exchange Number established: ' + @CellExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @CellUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Cell Unit Number established: ' + @CellUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                
                     
          
    IF @CellExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Cell Extension Number established: ' + @CellExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
         
                
    ------          
              
          
    IF @PagerAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Pager Area Code established: ' + @PagerAreaCode
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF @PagerExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Pager Exchange Number established: ' + @PagerExchangeNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PagerUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Pager Unit Number established: ' + @PagerUnitNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @PagerExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Pager Extension Number established: ' + @PagerExtensionNumber
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
     
    ----------------
    /*
    
    IF @GenderCD IS NOT NULL
    BEGIN
      
      SET @newValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_personnel', 'GenderCD') WHERE Code = @GenderCD)
    
      SET @LogComment = 'Gender established: ' + ISNULL(CONVERT(varchar(12), @newValue), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END          
     
            
    IF @MinorityFlag IS NOT NULL
    BEGIN
      SET @LogComment = 'Shop Manager flag established: ' + (SELECT CASE @MinorityFlag WHEN 0 THEN 'ON' ELSE 'OFF' END)
      
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @EntityID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
       
   */
    
    -- End of Audit Log code  ----------------------------------------------------------------------------------- 
    

    COMMIT TRANSACTION AdmShopLocPerInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    --SELECT @PersonnelID AS PersonnelID

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTPersonnelInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTPersonnelInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/