-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewSpawn' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateNewSpawn 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateNewSpawn
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client spawn by inserting a new record into the 
*				utb_spawn table modeled after @iModelInsuranceCompanyID
*
* PARAMETERS:  
*
*   @iNewInsuranceCompanyID
*   @iModelInsuranceCompanyID
*	@iCreatedByUserID
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateNewSpawn
	@iNewInsuranceCompanyID INT
	, @iModelInsuranceCompanyID INT
	, @iCreatedByUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @dtNow AS DATETIME 
    DECLARE @ProcName AS VARCHAR(50)

    SET @ProcName = 'uspCreateNewSpawn'

    -- Set Database options
    SET NOCOUNT ON
	SET @dtNow = CURRENT_TIMESTAMP

	-- Check to make sure the new insurance company id exists
    IF  (@iNewInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iNewInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iNewInsuranceCompanyID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure the model insurance company id exists
    IF  (@iModelInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iModelInsuranceCompanyID))
    BEGIN
        -- Invalid Model Insurance Company ID
        RAISERROR('101|%s|@iModelInsuranceCompanyID|%u', 16, 1, @ProcName, @iModelInsuranceCompanyID)
        RETURN
    END

	-- Add the new
	DECLARE db_cursor CURSOR FOR 
	SELECT WorkflowID FROM dbo.utb_workflow WHERE InsuranceCompanyID = @iModelInsuranceCompanyID ORDER BY WorkflowID 

	DECLARE @iWorkflowID INT
	DECLARE @iNewWorkflowID INT
	DECLARE @iCnt INT
	SET @iCnt = 0
	
	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO 	
		@iWorkflowID

		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			SELECT TOP 1 @iNewWorkflowID = WorkflowID FROM dbo.utb_workflow WHERE InsuranceCompanyID = @iNewInsuranceCompanyID
			SET @iNewWorkflowID = @iNewWorkflowID + @iCnt
			
			INSERT INTO dbo.utb_spawn
			SELECT 
				--(SELECT MAX(SpawnID)+1 FROM dbo.utb_spawn)
				@iNewWorkflowID
				, ConditionalValue
				, ContextNote
				, CustomProcName
				, EnabledFlag
				, SpawningID
				, SpawningServiceChannelCD
				, SpawningTypeCD
				, @iCreatedByUserID
				, @dtNow
			FROM 
				dbo.utb_spawn
			WHERE 
				WorkflowID = @iWorkflowID

			SET @iCnt = @iCnt + 1

			FETCH NEXT FROM db_cursor INTO 	
			@iWorkflowID
		END
	CLOSE db_cursor  
	DEALLOCATE db_cursor 		
	 
	IF EXISTS(
		SELECT 
			s.* 
		FROM 
			dbo.utb_workflow w
			INNER JOIN dbo.utb_spawn s
			ON s.WorkflowID = w.WorkflowID
		WHERE 
			InsuranceCompanyID = @iNewInsuranceCompanyID
	)
	BEGIN
		SELECT 1 RetCode
	END
	ELSE
	BEGIN
		SELECT 0 RetCode
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewSpawn' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateNewSpawn TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateNewSpawn TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/