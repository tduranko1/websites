-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCoverageDel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimCoverageDel 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimCoverageDel
* SYSTEM:       Lynx Services APD
* AUTHOR:       M. Ahmed
* FUNCTION:     Sets the enabled flag for a row to a "zero" to indicate that the row is no longer valid
*
* PARAMETERS:  
* (I) @ClaimCoverageID.......The id for the row that must be modified
* (I) @ClientCoverageTypeID..The client coverage type ID as defined in utb_Client_Coverage_Type
* (I) @LynxID................The LynxID for the claim
* (I) @UserID................The user updating the row
* (I) @SysLastUpdatedDate....The "previous" updated date for coverage information
*
* RESULT SET:
* 
*
************************************************************************************************************************/

-- Create the stored procedure
CREATE PROCEDURE dbo.uspClaimCoverageDel	
	@ClaimCoverageID              udt_std_int_big,
	@ClientCoverageTypeID         udt_std_id,
	@LynxID                       udt_std_int_big,
	@UserID                       udt_std_id,
	@SysLastUpdatedDate           varchar(30)

AS

BEGIN

    SET nocount on
    -- Declare local variables

    DECLARE @now               AS datetime
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    SET @ProcName = 'uspClaimCoverageDel'

 	-- If the Lynx id that was passed by the caller was invalid 
    IF  (
        SELECT     count(*) 
        FROM       dbo.utb_Claim_Coverage 
        WHERE      ClaimCoverageID = @ClaimCoverageID
        and        ClientCoverageTypeID = @ClientCoverageTypeID
        and        LynxID = @LynxID
        ) = 0
    	-- then raise error and return/exit
        BEGIN
            -- Invalid Lynx ID
                RAISERROR('101|%s|No record available for the parameters provided:@ClaimCoverageID, @ClientCoverageTypeID, @LynxID|%u|%u|%u', 16, 1, @ProcName,@ClaimCoverageID, @ClientCoverageTypeID, @LynxID)
            RETURN
        END
    -- Check to make sure a valid User id was passed in
    IF NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User ID

        RAISERROR('102|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END

    -- Get current timestamp
    SET @now = CURRENT_TIMESTAMP

    Begin Tran
        -- Check the date
    	exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @UserID, 'utb_claim_coverage', @ClaimCoverageID
        IF @@ERROR <> 0
            BEGIN
                -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.
        		ROLLBACK tran 
                RETURN 
            END
    
        Update    utb_Claim_Coverage
        set       Enabledflag = 0,
                  SysLastUserID = @UserID,
                  SysLastUpdatedDate = @now
 
        where     ClaimCoverageID = @ClaimCoverageID
        and       ClientCoverageTypeID = @ClientCoverageTypeID
        and       LynxID = @LynxID

        if @@error <> 0
            BEGIN
                Rollback tran
                Raiserror('103|%s|Record not modified',16,1,@ProcName)
                Return
            END

    --If the control has come this far it means that we can commit the transaction
    COMMIT Tran


    -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Coverage Level
			NULL AS [Coverage!2!ClaimCoverageID],
            NULL AS [Coverage!2!SysLastUpdatedDate]

    UNION ALL

    SELECT  2,
            1,
            NULL,
            -- Coverage Level
            @ClaimCoverageID,
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END






END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimCoverageDel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimCoverageDel TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO



