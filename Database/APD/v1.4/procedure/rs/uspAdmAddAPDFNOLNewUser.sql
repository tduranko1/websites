-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmAddAPDFNOLNewUser' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmAddAPDFNOLNewUser 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdmAddAPDFNOLNewUser
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Adds an FNOL user to the APD Database.
*
* PARAMETERS:  
* None.
*
* RESULT SET:
* None.
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE dbo.uspAdmAddAPDFNOLNewUser
    @CSRNo                          VARCHAR(50),
    @NameFirst                      udt_per_name,
    @NameLast                       udt_per_name,
    @EmailAddress                   udt_web_email = NULL,
    @OfficeID                       udt_std_id = NULL,
    @PhoneAreaCode                  udt_ph_area_code =         NULL,
    @PhoneExchangeNumber            udt_ph_exchange_number =   NULL,
    @PhoneExtensionNumber           udt_ph_extension_number =  NULL,
    @PhoneUnitNumber                udt_ph_unit_number =       NULL,
    @ApplicationID					udt_std_int_tiny =		   4
AS
BEGIN
    
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    -- SET TRANSACTION ISOLATION LEVEL to SERIALIZABLE to initiate pessimistic concurrency control
    
    SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

    -- Declare internal variables
    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @trancount          AS INT
    DECLARE @ModifiedDateTime AS DATETIME
    
    -- Procuedure Variables
    declare @AdmCsrNo        udt_sys_csr_no   
    declare @SysUserId udt_std_id
    declare @tmpuserid udt_std_id
    declare @chkuserid udt_std_id
    declare @chkfname  udt_per_name
    declare @chklname  udt_per_name
    declare @userexists bit
    declare @userappexists bit
    declare @olduserrecord bit
    declare  @msg varchar(60)

    
    SET @ModifiedDateTime = CURRENT_TIMESTAMP
    SET @error = 0
    set @AdmCsrNo = 'csr2416'


    -- Preparation
    
    select @sysuserid = userid from utb_user_application where logonid = @admcsrno
    if (@sysuserid is null)
    begin
        raiserror ('Unknown User can not modify APD information', 16,1)
        return
    end
    
    if (@emailaddress is null)
    begin
        set @emailaddress = @csrno + '@lynx1services.com'
    end
    
    -- First Determine if a user already exist
    set @olduserrecord = 0
    -- Is there a user_application record with the csr logon?
    if exists (select userid from utb_user_application where logonid = @csrno and applicationid = @ApplicationID)
    begin
        -- Yes now we need to see if this is the same user.
        select @tmpuserid = userid from utb_user_application where logonid = @csrno and applicationid = @ApplicationID
        if exists (select userid from utb_user where userid = @tmpuserid and namefirst = @namefirst and namelast = @namelast)
        begin
           -- User Exists
           set @userexists = 1
           set @userappexists = 1
        end
        else
        begin
           -- User exists but is not same user
           set @userexists = 0
           set @userappexists = 0
           set @olduserrecord = 1
        end
    end
    else
    begin
        -- No user_application record exists; now just check user records
        set @userappexists = 0
        if exists (select userid from utb_user where emailaddress = @emailaddress and namefirst = @namefirst and namelast = @namelast)
        begin
            -- User Record exists
            select @tmpuserid = userid from utb_user where emailaddress = @emailaddress and namefirst=@namefirst and namelast=@namelast
            set @userexists = 1
        end
        else
        begin 
            set @userexists = 0
        end
    end
            
    -- Start the new transaction
    
    IF 0 = @error
    BEGIN
        SET @trancount = @@TRANCOUNT

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        BEGIN TRANSACTION

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        if @olduserrecord = 1
        begin
            update utb_user_application
            set AccessEndDate = GetDate()
            where userid = @tmpuserid and applicationid = @ApplicationID
        end
    
        if @userappexists = 1
        begin
            -- User Exists
            print 'User_application record exists'
            print 'User Id = ' + convert(varchar(10),@tmpuserid)
            if ((select enabledflag from utb_user where userid = @tmpuserid) = 0)
            begin 
               print 'User was deactivated: reactivating...'
                -- reactivate user
                update utb_user
                set enabledflag = 1,
                    syslastuserid = @sysuserid,
                    syslastupdateddate = @ModifiedDateTime
                where userid = @tmpuserid    
                if (@@error <> 0)
                begin
                    set @msg = 'User existed but was unable to reactivate user: ' + convert(varchar(30),@@error)
                    print @msg
                    rollback transaction 
                    return
                end
            end
            update utb_user_application
            set accessenddate = null,
                syslastuserid = @sysuserid,
                syslastupdateddate = @ModifiedDateTime
            where userid = @tmpuserid and applicationid = @ApplicationID
            if (@@error <> 0)
            begin
                set @msg = 'Unable to modify user_application record: ' + convert(varchar(30),@@error)
               print @msg
                rollback transaction                 
                return
            end
        end
        else
        begin
            -- no userid match for user_applicaiton
           
            if @userexists = 0
            begin           
               print 'User does not exist: Creating User'
               insert into utb_user (officeid,
                                     emailaddress,
                                     enabledflag,
                                     namefirst,
                                     namelast,
                                     PhoneAreaCode,
                                     PhoneExchangeNumber,
                                     PhoneExtensionNumber,
                                     PhoneUnitNumber,
                                     supervisorflag,
                                     syslastuserid,
                                     syslastupdateddate)
                            values  (@officeid,
                                     @emailaddress,
                                     1,
                                     @namefirst,
                                     @namelast,
                                     @PhoneAreaCode,
                                     @PhoneExchangeNumber,
                                     @PhoneExtensionNumber,
                                     @PhoneUnitNumber,
                                     0,
                                     @sysuserid,
                                     @ModifiedDateTime)
            
                if (@@error <> 0)
                begin
                    set @msg = 'unable to insert user record: ' + convert(varchar(30),@@error)
                    print @msg
                    rollback transaction 
                    return
                end
                select @tmpuserid = userid from utb_user where emailaddress = @emailaddress and namefirst=@namefirst and namelast=@namelast
            end

            print 'UserId = ' + convert(varchar(10),@tmpuserid)   
            if ((select enabledflag from utb_user where userid = @tmpuserid) = 0)
            begin
               -- reactive user\
               print 'Reactivating user'
               update utb_user
               set enabledflag = 1,
                   syslastuserid = @sysuserid,
                   syslastupdateddate = @ModifiedDateTime
               where userid = @tmpuserid    
               if (@@error <> 0)
               begin
                  set @msg = 'User existed but was unable to reactivate user: ' + convert(varchar(30),@@error)
                 print @msg
                  rollback transaction 
                  return
               end
            end
            print 'Adding user_application record'
            insert into utb_user_application (userid,
                                              applicationid,
                                              accessbegindate,
                                              logonid,
                                              syslastuserid,
                                              syslastupdateddate)
                                      values (@tmpuserid,
                                              @ApplicationID,
                                              @ModifiedDateTime,
                                              @csrno,
                                              @sysuserid,
                                              @ModifiedDateTime)
           if (@@error <> 0)
           begin
                set @msg = 'unable to insert user_application record: ' + convert(varchar(30),@@error)
                print @msg
                rollback transaction 
                return
           end
        end
        if not exists (select * from utb_user_role where userid = @tmpuserid)
        begin
            print 'User_role record does not exist..Adding user_role record'
            insert into utb_user_role (userid, roleid, primaryroleflag, syslastuserid, syslastupdateddate)
            values (@tmpuserid, 1,1,@sysuserid,@ModifiedDateTime)
            if (@@error <> 0)
            begin
                set @msg = 'unable to insert user_role record: ' + convert(varchar(30),@@error)
                print @msg
                rollback transaction 
                return
            end  
        end     

        -- Capture any error
    
        SELECT @error = @@ERROR
    END

    IF @@TRANCOUNT > @trancount
    BEGIN
        IF 0 = @error 
            COMMIT TRANSACTION
        ELSE
            ROLLBACK TRANSACTION
    END

END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmAddAPDFNOLNewUser' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmAddAPDFNOLNewUser TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
