
-- Begin the transaction for dropping and creating the stored procedure
BEGIN TRANSACTION


-- If it already exists, drop the procedure
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSysGetStatus' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSysGetStatus 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- Create the stored procedure
CREATE PROCEDURE [dbo].[uspSysGetStatus]
	@ServerName                 varchar(50)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspSysGetStatus'

    SELECT SubName, Value
    FROM utb_app_variable
    WHERE NAME = 'SYS_STATUS'
    AND SubName = @ServerName
    AND Value = 'UP'

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END

GO


GO

-- Permissions
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSysGetStatus' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSysGetStatus TO ugr_lynxapd
    GRANT EXECUTE ON dbo.uspSysGetStatus TO ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO