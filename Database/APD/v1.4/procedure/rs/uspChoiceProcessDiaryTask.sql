-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceProcessDiaryTask' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceProcessDiaryTask 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspChoiceProcessDiaryTask
* SYSTEM:       Lynx Services / Hyperquest
* AUTHOR:       Thomas Duranko
* FUNCTION:     Processes Diary tasks in an automated fashion
* Date:			17Nov2015
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* REVISIONS:	17Nov2015 - TVD - Initial development
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspChoiceProcessDiaryTask]
	@iChecklistID				INT
	, @vNoteTypeName			VARCHAR(50)
	, @vNote					VARCHAR(1000)
	, @iChecklistAssignedTo	INT
	, @dtChecklistLastUpdated DATETIME
AS
BEGIN
    SET NOCOUNT ON
 	
 	---------------------------------------
	-- Declare Vars
	---------------------------------------
	DECLARE @iChoiceAssignmentsWaiting			INT
	DECLARE @ProcName							VARCHAR(50)
	DECLARE @Now								DATETIME
    DECLARE @Debug								udt_std_flag
	-----------------------------------------------------------
	DECLARE @iNoteTypeID						INT

	---------------------------------------
	-- Init Params
	---------------------------------------
    SET @Debug = 1 
	SET @ProcName = 'uspChoiceProcessDiaryTask'
	SET @Now = CURRENT_TIMESTAMP
	SET @iChoiceAssignmentsWaiting = 0

	---------------------------------------
	-- Validations
	---------------------------------------
	-- Get Summary NoteTypeID
	SELECT  @iNoteTypeID = NoteTypeID
      FROM  dbo.utb_note_type
      WHERE Name = @vNoteTypeName
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
        RAISERROR  ('%s: SQL Server Error.  Invalid NoteTypeID/Name', 16, 1, @ProcName)
        RETURN
    END    

	IF NOT EXISTS(SELECT * FROM utb_checklist WHERE CheckListID = @iChecklistID)
    BEGIN
        -- SQL Server Error
        RAISERROR  ('%s: SQL Server Error.  Invalid CheckListID', 16, 1, @ProcName)
        RETURN
    END    

	--============================================
	-- Main Code - Process Diary task Complete
	--============================================
	IF (@iNoteTypeID > 0)
	BEGIN
		EXEC uspWorkflowCompleteTask @CheckListID=@iChecklistID, @NotApplicableFlag=0, @NoteRequiredFlag=1, @Note=@vNote, @NoteTypeID=@iNoteTypeID, @UserID=@iChecklistAssignedTo, @SysLastUpdatedDate=@dtChecklistLastUpdated 
	END
	
	
/*
    IF @Debug = 1
    BEGIN
        PRINT 'Passed in Parameters:'
		PRINT '    @NewShopFlag = ' + CONVERT(VARCHAR(1), @NewShopFlag)
		PRINT '    @HQBusinessTypeCD = ' + @HQBusinessTypeCD
		PRINT '    @HQAddress1 = ' + @HQAddress1
		PRINT '    @HQAddress2 = ' + @HQAddress2
		PRINT '    @HQAddressCity = ' + @HQAddressCity
		PRINT '    @HQAddressState = ' + @HQAddressState
		PRINT '    @HQAddressZip = ' + CONVERT(VARCHAR(12), @HQAddressZip)
		PRINT '    @HQEmailAddress = ' + @HQEmailAddress
		PRINT '    @HQEnabledFlag = ' + CONVERT(VARCHAR(1), @HQEnabledFlag)
		PRINT '    @HQFaxAreaCode = ' + CONVERT(VARCHAR(3), @HQFaxAreaCode)
		PRINT '    @HQFaxExchangeNumber = ' + CONVERT(VARCHAR(3), @HQFaxExchangeNumber)
		PRINT '    @HQFaxUnitNumber = ' + CONVERT(VARCHAR(4), @HQFaxUnitNumber)
		PRINT '    @HQFedTaxId = ' + CONVERT(VARCHAR(15), @HQFedTaxId)
		PRINT '    @HQName = ' + @HQName
		PRINT '    @HQPhoneAreaCode = ' + CONVERT(VARCHAR(3), @HQPhoneAreaCode)
		PRINT '    @HQPhoneExchangeNumber = ' + CONVERT(VARCHAR(2), @HQPhoneExchangeNumber)
		PRINT '    @HQPhoneUnitNumber = ' + CONVERT(VARCHAR(4), @HQPhoneUnitNumber)
		PRINT '    @HQSysLastUserID = ' + CONVERT(VARCHAR(10), @HQSysLastUserID)
		PRINT '    @HQApplicationCD = ' + @HQApplicationCD
		PRINT '    @HQShopPartnerID = ' + @HQShopPartnerID
    END
*/
	---------------------------------------
	-- Tests
	---------------------------------------
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceProcessDiaryTask' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspChoiceProcessDiaryTask TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/