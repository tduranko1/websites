-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMetricsPS_CSR' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptMetricsPS_CSR 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptMetricsPS_CSR
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns a list of Audited estimates that are different from the electronic version.
*
* PARAMETERS:  
*
* RESULT SET:
*   Returns a list of Audited estimates that are different from the electronic version.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptMetricsPS_CSR
AS
BEGIN
   set nocount on
   set ANSI_WARNINGS off
   /******************************************************************************
   * PS QCR metrics
   ******************************************************************************/

   DECLARE @RptMonthStart as datetime
   DECLARE @RptYesterdayStart as datetime
   DECLARE @RptYesterdayEnd as datetime
   DECLARE @StatusSCComplete as int
   DECLARE @now as datetime
   DECLARE @debug as bit


   DECLARE @tmpClaims TABLE (
      LynxID            bigint,
      ClaimAspectServiceChannelID   bigint,
      WorkEndDate                   datetime,
      CompletedDate                 datetime,
      CTBusinessDays                decimal(9, 1),
      OwnerName                     varchar(50)
   )

   DECLARE @tmpOpenTasks TABLE (
      ClaimAspectServiceChannelID   bigint,
      OwnerUser                     varchar(50),
      AlarmDate                     datetime,
      OverdueHours                  int
   )

   DECLARE @tmpOwners TABLE (
      OwnerUser         varchar(50)
   )

   DECLARE @tmpReport TABLE (
      idx                int,
      Metric             varchar(60),
      DailyData          decimal(9, 1),
      MTDData            decimal(9, 1)
   )

   SET @now = CURRENT_TIMESTAMP
   --SET @now = convert(datetime, '4/11/2009 8:00:00')--CURRENT_TIMESTAMP
   SET @RptMonthStart = convert(varchar, month(@now)) + '/1/' + convert(varchar, year(@now))
   IF DAY(@now) = 1 
   BEGIN
      SET @RptMonthStart = DATEADD(month, -1, @RptMonthStart)
   END
   SET @RptYesterdayStart = dateadd(day, -1, convert(datetime, convert(varchar, @now, 101)))
   SET @RptYesterdayEnd = dateadd(second, -1, convert(datetime, convert(varchar, @now, 101)))

   SET @debug = 1

   select @StatusSCComplete = StatusID
   FROM utb_status
   where ClaimAspectTypeID = 9
     and StatusTypeCD = 'SC'
     and ServiceChannelCD = 'PS'
     and Name = 'Complete'
     
     
   insert into @tmpClaims
   select ca.LynxID,
          casc.ClaimAspectServiceChannelID,
          casc.WorkEndDate,
          cas.StatusStartDate,
          --dbo.ufnUtilityBusinessDaysDiff(casc.WorkEndDate, cas.StatusStartDate),
          (dbo.ufnUtilityTrueBusinessHoursDiff(casc.WorkEndDate, cas.StatusStartDate) * 1.0)/9.0,
          uo.NameFirst + ' ' + uo.NameLast
   from dbo.utb_claim_aspect_service_channel casc
   LEFT JOIN dbo.utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
   LEFT JOIN utb_claim_aspect_status cas on ca.ClaimAspectID = cas.ClaimAspectID and cas.StatusTypeCD = 'SC'
   LEFT JOIN dbo.utb_claim c on ca.LynxID = c.LynxID
   LEFT JOIN dbo.utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
   LEFT JOIN dbo.utb_user uo on ca.OwnerUserID = uo.UserID
   WHERE casc.ServiceChannelCD = 'PS'
     AND c.DemoFlag = 0
     AND i.DemoFlag = 0
     AND ca.OwnerUserID is not null
     AND uo.OfficeID is null
     AND cas.StatusID = @StatusSCComplete
     AND casc.WorkEndConfirmFlag = 1
     AND cas.StatusStartDate between @RptMonthStart and @RptYesterdayEnd

   DELETE FROM @tmpClaims WHERE OwnerName like '%- CEI%'

   --select * from @tmpClaims

   INSERT INTO @tmpReport 
   SELECT 1,
          'Average Days for Repair Complete to Close (Business Days)',
          NULL,
          AVG(CTBusinessDays)
   FROM @tmpClaims

   insert into @tmpReport
   select 1, NULL, NULL, NULL

   insert into @tmpReport
   select 1,
          OwnerName,
          NULL,
          AVG(CTBusinessDays)
   from @tmpClaims   
   group by OwnerName

   --select * from @tmpReport

   INSERT INTO @tmpOpenTasks
   SELECT distinct cl.ClaimAspectServiceChannelID,
          ua.NameFirst + ' ' + ua.NameLast ,
          cl.AlarmDate,
          dbo.ufnUtilityTrueBusinessHoursDiff(cl.AlarmDate, @now)

   FROM dbo.utb_checklist cl
   LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cl.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
   LEFT JOIN dbo.utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
   LEFT JOIN dbo.utb_claim c on ca.LynxID = c.LynxID
   LEFT JOIN dbo.utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
   LEFT JOIN dbo.utb_user ua on cl.AssignedUserID = ua.UserID
   WHERE casc.ServiceChannelCD = 'PS'
     AND cl.AssignedUserID = ca.OwnerUserID
     AND c.DemoFlag = 0
     AND i.DemoFlag = 0
     AND cl.CreatedDate between @RptMonthStart and @RptYesterdayEnd
     AND cl.AlarmDate < @now
     AND ua.OfficeID is null
   order by cl.ClaimAspectServiceChannelID 

   INSERT INTO @tmpOwners 
   select distinct OwnerUser
   from @tmpOpenTasks
   where owneruser not like '%- CEI%'
   order by OwnerUser

   INSERT INTO @tmpReport 
   SELECT 2,
          'Count of tasks overdue 48 business hours',
          convert(varchar, count(ClaimAspectServiceChannelID)),
          null
   FROM @tmpOpenTasks
   WHERE OverdueHours > 48

   insert into @tmpReport
   select 2, NULL, NULL, NULL

   insert into @tmpReport
   select 2,
          OwnerUser,
          ( select count(ClaimAspectServiceChannelID)
            from @tmpOpenTasks tot
            where tot.OwnerUser = tmp.OwnerUser
              and OverdueHours > 48
          ),
          NULL
   from @tmpOwners tmp

   INSERT INTO @tmpReport 
   SELECT 3,
          'Count of tasks overdue 5 business days',
          count(ClaimAspectServiceChannelID),
          NULL
   FROM @tmpOpenTasks
   WHERE OverdueHours > 120

   insert into @tmpReport
   select 3, NULL, NULL, NULL

   insert into @tmpReport
   select 3,
          OwnerUser,
          ( select count(ClaimAspectServiceChannelID)
            from @tmpOpenTasks tot
            where tot.OwnerUser = tmp.OwnerUser
              and OverdueHours > 120
          ),
          NULL
   from @tmpOwners tmp


   print 'APD Daily Metric Report per Monthly Cycle - PS CSR'
   print '-----------------------------------------------------'
   print 'Daily Data for: ' + convert(varchar, @RptYesterdayStart, 101)
   print 'Month: ' + convert(varchar, Datepart("mm", @RptMonthStart)) + '/' + convert(varchar, Datepart("yyyy", @RptMonthStart))
   print '                                                             Program Shop'
   print '                                                             ------------'

   select isNull(Metric, '') as Metric,
         '     ' as Daily,
         isNull(convert(varchar(5), space(5 - len(convert(varchar, MTDData))) + convert(varchar, MTDData)), '') as 'MTD'
   from @tmpReport
   where idx = 1

   print ''
   print '                                                             Program Shop'
   print '                                                             ------------'
   select isNull(Metric, '') as Metric,
          isNull(convert(varchar(5), space(5 - len(convert(varchar, convert(int, DailyData)))) + convert(varchar, convert(int, DailyData))), '     ') as Daily,
          isNull(convert(varchar(5), space(5 - len(convert(varchar, convert(int, MTDData)))) + convert(varchar, convert(int, MTDData))), '     ') as 'MTD'
   from @tmpReport
   where idx = 2

   print ''
   print '                                                             Program Shop'
   print '                                                             ------------'
   select isNull(Metric, '') as Metric,
          isNull(convert(varchar(5), space(5 - len(convert(varchar, convert(int, DailyData)))) + convert(varchar, convert(int, DailyData))), '     ') as Daily,
          isNull(convert(varchar(5), space(5 - len(convert(varchar, convert(int, MTDData)))) + convert(varchar, convert(int, MTDData))), '     ') as 'MTD'
   from @tmpReport
   where idx = 3

   print '--- END OF REPORT ---'

END



GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMetricsPS_CSR' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptMetricsPS_CSR TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
