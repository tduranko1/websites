-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmCEIShopMigrate' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdmCEIShopMigrate 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdmCEIShopMigrate
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Retrieves the CEI shops.
*
* PARAMETERS:  
* None.
*
* RESULT SET:
* None.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdmCEIShopMigrate
AS
BEGIN
    
    SELECT 'CEIShops Total (For licensed states)', COUNT(*) FROM CEIShops l
     WHERE l.[Shop Name] IS NOT NULL

    SELECT 'utb_shop_location Total', COUNT(*) FROM utb_shop_location 

    BEGIN TRANSACTION


    DECLARE @ModifiedDateTime AS DATETIME
    DECLARE @Count AS INT

    -- shop_cursor fields

    DECLARE @Address1 AS udt_addr_line_1
    DECLARE @AddressCity AS udt_addr_city
    DECLARE @AddressCounty AS udt_addr_county
    DECLARE @AddressState AS udt_addr_state
    DECLARE @AddressZip AS udt_addr_zip_code
    DECLARE @CEIProgramId AS udt_std_desc_short
    DECLARE @FaxAreaCode AS udt_ph_area_code
    DECLARE @FaxExchangeNumber AS udt_ph_exchange_number
    DECLARE @FaxUnitNumber AS udt_ph_unit_number
    DECLARE @Latitude AS udt_addr_latitude
    DECLARE @Longitude AS udt_addr_longitude
    DECLARE @Name AS udt_std_name
    DECLARE @PhoneAreaCode AS udt_ph_area_code
    DECLARE @PhoneExchangeNumber AS udt_ph_exchange_number
    DECLARE @PhoneUnitNumber AS udt_ph_unit_number
    DECLARE @EmailAddress AS udt_web_email
    DECLARE @ShopContact AS udt_std_desc_mid
    DECLARE @MFStart AS udt_std_desc_mid
    DECLARE @MFEnd AS udt_std_desc_mid
    DECLARE @SatStart AS udt_std_desc_mid
    DECLARE @SatEnd AS udt_std_desc_mid
    DECLARE @DrivingDirections AS udt_std_desc_mid


    -- Identies to capture

    DECLARE @ShopID AS udt_std_id_big
    DECLARE @ShopLocationID AS udt_std_id_big
    DECLARE @BillingID AS udt_std_id_big
    DECLARE @PersonnelId AS udt_std_id_big

    -- Declare Cursors

    DECLARE shop_cursor CURSOR FAST_FORWARD FOR 
        SELECT RTRIM(LTRIM(l.Address)), -- Address1
               RTRIM(LTRIM(l.City)),  -- AddressCity
               RTRIM(LTRIM(z.County)),  -- AddressCounty
               RTRIM(LTRIM(l.State)),  -- AddressState
               CASE LEN(LEFT(RTRIM(LTRIM(l.[Zip Code])), 5))
                   WHEN 4 THEN '0' + LEFT(RTRIM(LTRIM(l.[Zip Code])), 5)
                   ELSE LEFT(RTRIM(LTRIM(l.[Zip Code])), 5)
               END,  -- AddressZip
               DriverShieldShopID, -- CEIProgramId
               LEFT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(REPLACE(l.Fax, '-', ''), ' ', ''), '(', ''), ')', ''))),3),  -- FaxAreaCode
               SUBSTRING(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(REPLACE(l.Fax, '-', ''), ' ', ''), '(', ''), ')', ''))),4,3),  -- FaxExchangeNumber
               RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(REPLACE(l.Fax, '-', ''), ' ', ''), '(', ''), ')', ''))),4),  -- FaxUnitNumber
               l.Latitude,  -- Latitude
               l.Longitude,  -- Longitude
               RTRIM(LTRIM(l.[Shop Name])),  -- Name
               LEFT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(REPLACE(l.Phone, '-', ''), ' ', ''), '(', ''), ')', ''))),3),  -- PhoneAreaCode
               SUBSTRING(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(REPLACE(l.Phone, '-', ''), ' ', ''), '(', ''), ')', ''))),4,3),  -- PhoneExchangeNumber
               RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(REPLACE(l.Phone, '-', ''), ' ', ''), '(', ''), ')', ''))),4),  -- PhoneUnitNumber
               RTRIM(LTRIM(l.[E-Mail Address])),  
               RTRIM(LTRIM(l.[Shop Contact])),  
               LEFT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4),
               CASE LEFT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '01' THEN '13' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '02' THEN '14' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '03' THEN '15' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '04' THEN '16' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '05' THEN '17' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '06' THEN '18' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '07' THEN '19' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '08' THEN '20' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '09' THEN '21' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '10' THEN '22' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '11' THEN '23' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '12' THEN '12' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Monday - Friday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   ELSE NULL
               END,
               LEFT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4),
               CASE LEFT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '01' THEN '13' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '02' THEN '14' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '03' THEN '15' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '04' THEN '16' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '05' THEN '17' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '06' THEN '18' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '07' THEN '19' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '08' THEN '20' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '09' THEN '21' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '10' THEN '22' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '11' THEN '23' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   WHEN '12' THEN '12' + RIGHT(RIGHT(RTRIM(LTRIM(REPLACE(REPLACE(REPLACE(l.[Saturday Hours], '-', ''), ' ', ''), ':', ''))),4), 2)
                   ELSE NULL
               END,
               RTRIM(LTRIM(l.[Directions]))  
          FROM dbo.CEIShops l
         LEFT OUTER JOIN dbo.utb_zip_code z
    --                   ON LEFT(RTRIM(LTRIM(l.[Zip Code])), 5) = z.Zip
    ON  z.zip = CASE LEN(LEFT(RTRIM(LTRIM(l.[Zip Code])), 5))
                    WHEN 4 THEN '0' + LEFT(RTRIM(LTRIM(l.[Zip Code])), 5)
                    ELSE LEFT(RTRIM(LTRIM(l.[Zip Code])), 5)
                END  -- AddressZip

        WHERE l.[Shop Name] IS NOT NULL


    --- Initialize variables

    SET @ModifiedDateTime = CURRENT_TIMESTAMP


    --
    --
    -- Script for updating APD shop data.
    --

    PRINT '.'
    PRINT '.'
    PRINT 'Inserting new data...'
    PRINT '.'

    UPDATE utb_shop_location
           SET CEIProgramFlag = 0,
           SysLastUserID = 0,
           SysLastUpdatedDate = @ModifiedDateTime
     WHERE CEIProgramFlag = 1
       AND EnabledFlag = 1
     
    OPEN shop_cursor
    FETCH NEXT FROM shop_cursor INTO @Address1, @AddressCity, @AddressCounty, @AddressState, @AddressZip, @CEIProgramId, @FaxAreaCode, @FaxExchangeNumber, @FaxUnitNumber, @Latitude, @Longitude, @Name, @PhoneAreaCode, @PhoneExchangeNumber, @PhoneUnitNumber, @EmailAddress, @ShopContact, @MFStart, @MFEnd, @SatStart, @SatEnd, @DrivingDirections

    WHILE @@FETCH_STATUS = 0 
    BEGIN 
            -- Reinitialize
            SET @ShopID = NULL
            SET @ShopLocationID = NULL
            SET @BillingID = NULL
            SET @Count = NULL


            -- Check for duplicate shops

            SELECT @Count = COUNT(*)
              FROM dbo.utb_shop_location
             WHERE EnabledFlag = 1
               AND @PhoneAreaCode = PhoneAreaCode
               AND @PhoneExchangeNumber = PhoneExchangeNumber
               AND @PhoneUnitNumber = PhoneUnitNumber

            IF @Count = 0
            BEGIN
                -- Populate shops
        
                INSERT INTO dbo.utb_shop
                (
        		    ShopParentID,
        		    BusinessTypeCD,
        		    Address1,
        		    Address2,
        		    AddressCity,
                    AddressCounty,
        		    AddressState,
        		    AddressZip,
        		    DBBusinessId,
        		    EnabledFlag,
        		    FaxAreaCode,
        		    FaxExchangeNumber,
        		    FaxExtensionNumber,
        		    FaxUnitNumber,
        		    FedTaxId,
        		    Name,
        		    PhoneAreaCode,
        		    PhoneExchangeNumber,
        		    PhoneExtensionNumber,
        		    PhoneUnitNumber,
        		    SysLastUserID,
        		    SysLastUpdatedDate
                )
                VALUES
                (
                    NULL,  -- ShopParentID
                    NULL,  -- BusinessTypeCD
                    @Address1, -- Address1
                    NULL,  -- Address2
                    @AddressCity,  -- AddressCity
                    @AddressCounty,  -- AddressCounty
                    @AddressState,  -- AddressState
                    @AddressZip,  -- AddressZip
                    NULL,  -- DBBusinessId
                    1,  -- EnabledFlag
                    @FaxAreaCode,  -- FaxAreaCode
                    @FaxExchangeNumber,  -- FaxExchangeNumber
                    NULL,  -- FaxExtensionNumber
                    @FaxUnitNumber,  -- FaxUnitNumber
                    NULL,  -- FedTaxID
                    @Name,  -- Name
                    @PhoneAreaCode,  -- PhoneAreaCode
                    @PhoneExchangeNumber,  -- PhoneExchangeNumber,
                    NULL,  -- PhoneExtensionNumber
                    @PhoneUnitNumber,  -- PhoneUnitNumber
                    0,  -- ModifiedBy
                    @ModifiedDateTime
                )
    
                SET @ShopID = SCOPE_IDENTITY()
    
    
                -- Populate billing
    
                INSERT INTO dbo.utb_billing
        	    (
                    Address1,
                    Address2,
                    AddressCity,
                    AddressCounty,
                    AddressState,
                    AddressZip,
                    EFTAccountNumber,
                    EFTAccountTypeCD,
                    EFTContractSignedFlag,
                    EFTEffectiveDate,
                    EFTRoutingNumber,
                    FaxAreaCode,
                    FaxExchangeNumber,
                    FaxExtensionNumber,
                    FaxUnitNumber,
                    Name,
                    PhoneAreaCode,
                    PhoneExchangeNumber,
                    PhoneExtensionNumber,
                    PhoneUnitNumber,
                    SysLastUserID,
                    SysLastUpdatedDate
                )
                VALUES
                (
                    @Address1, -- Address1
                    NULL,  -- Address2
                    @AddressCity,  -- AddressCity
                    @AddressCounty,  -- AddressCounty
                    @AddressState,  -- AddressState
                    @AddressZip,  -- AddressZip
                    NULL,  -- EFTAccountNumber
                    NULL,  -- EFTAccountTypeCD
                    0,  -- EFTContractSignedFlag
                    NULL,  -- EFTEffectiveDate
                    NULL,  -- EFTRoutingNumber
                    @FaxAreaCode,  -- FaxAreaCode
                    @FaxExchangeNumber,  -- FaxExchangeNumber
                    NULL,  -- FaxExtensionNumber
                    @FaxUnitNumber,  -- FaxUnitNumber
                    @Name,  -- Name
                    @PhoneAreaCode,  -- PhoneAreaCode
                    @PhoneExchangeNumber,  -- PhoneExchangeNumber,
                    NULL,  -- PhoneExtensionNumber
                    @PhoneUnitNumber,  -- PhoneUnitNumber
                    0,  -- SysLastUserID
                    @ModifiedDateTime  -- SysLastUpdatedDate
                )
    
                SET @BillingID = SCOPE_IDENTITY()
    
    
                -- Populate shop locations
        
                INSERT INTO dbo.utb_shop_location
                (
        		    BillingID,
        		    DataReviewUserID,
        		    PreferredCommunicationMethodID,
        		    PreferredEstimatePackageID,
        		    ProgramManagerUserID,
        		    ShopID,
        		    Address1,
        		    Address2,
        		    AddressCity,
        		    AddressCounty,
        		    AddressState,
        		    AddressZip,
                    AvailableForSelectionFlag,
                    CEIProgramFlag,
                    CEIProgramId,
           		    CertifiedFirstFlag,
        		    CertifiedFirstId,
        		    DataReviewDate,
        		    DataReviewStatusCD,
                    DrivingDirections,
        		    EmailAddress,
        		    EnabledFlag,
        		    FaxAreaCode,
        		    FaxExchangeNumber,
        		    FaxExtensionNumber,
        		    FaxUnitNumber,
        		    LastAssignmentDate,
        		    Latitude,
        		    Longitude,
        		    MaxWeeklyAssignments,
        		    Name,
        		    PhoneAreaCode,
        		    PhoneExchangeNumber,
        		    PhoneExtensionNumber,
        		    PhoneUnitNumber,
        		    PreferredCommunicationAddress,
        		    ProgramFlag,
        		    ProgramScore,
                    WarrantyPeriodRefinishCD,
                    WarrantyPeriodWorkmanshipCD,
        		    WebSiteAddress,
        		    SysLastUserID,
        		    SysLastUpdatedDate
                )
                VALUES
                (
                    @BillingID,  -- BillingID
                    NULL,  -- DataReviewUserID
                    NULL,  -- PreferredCommunicationMethodID
                    NULL,  -- PreferredEstimatePackageID
                    NULL,  -- ProgramManagerUserID
                    @ShopID,  -- ShopID
                    @Address1, -- Address1
                    NULL,  -- Address2
                    @AddressCity,  -- AddressCity
                    @AddressCounty,  -- AddressCounty
                    @AddressState,  -- AddressState
                    @AddressZip,  -- AddressZip
                    1,  -- AvailableForSelectionFlag
                    1, --CEIProgramFlag
                    @CEIProgramId,  -- CEIProgramID
                    0,  -- CertifiedFirstFlag
                    NULL,  -- CertifiedFirstId
                    NULL,  -- DataReviewDate
                    'N',  -- DataReviewStatusCD
                    @DrivingDirections,  -- DrivingDirections
                    @EmailAddress,  -- EmailAddress
                    1,  -- EnabledFlag
                    @FaxAreaCode,  -- FaxAreaCode
                    @FaxExchangeNumber,  -- FaxExchangeNumber
                    NULL,  -- FaxExtensionNumber
                    @FaxUnitNumber,  -- FaxUnitNumber
                    NULL,  -- LastAssignmentDate
                    @Latitude,  -- Latitude
                    @Longitude,  -- Longitude
                    NULL,  -- MaxWeeklyAssignments
                    @Name,  -- Name
                    @PhoneAreaCode,  -- PhoneAreaCode
                    @PhoneExchangeNumber,  -- PhoneExchangeNumber,
                    NULL,  -- PhoneExtensionNumber
                    @PhoneUnitNumber,  -- PhoneUnitNumber
                    NULL,  -- PreferredCommunicationAddress
                    0,  -- ProgramFlag
                    NULL,  -- ProgramScore
                    '99',  -- WarrantyPeriodRefinishCD
                    '99',  -- WarrantyPeriodWorkmanshipCD
                    NULL,  -- WebSiteAddress
                    0,  -- ModifiedBy
                    @ModifiedDateTime
                )
    
                SET @ShopLocationID = SCOPE_IDENTITY()
    
                IF LEN(RTRIM(LTRIM(@ShopContact))) > 0
                BEGIN
                    INSERT INTO dbo.utb_personnel
                    (
                        PersonnelTypeID,
                        PreferredContactMethodID,
                        CellAreaCode,
                        CellExchangeNumber,
                        CellExtensionNumber,
                        CellUnitNumber,
                        EmailAddress,
                        FaxAreaCode,
                        FaxExchangeNumber,
                        FaxExtensionNumber,
                        FaxUnitNumber,
                        GenderCD,
                        MinorityFlag,
                        Name,
                        PagerAreaCode,
                        PagerExchangeNumber,
                        PagerExtensionNumber,
                        PagerUnitNumber,
                        PhoneAreaCode,
                        PhoneExchangeNumber,
                        PhoneExtensionNumber,
                        PhoneUnitNumber,
                        ShopManagerFlag,
                        SysLastUserID,
                        SysLastUpdatedDate
                    )
                    VALUES
                    (
                        1,  -- PersonnelTypeID
                        NULL,  -- PreferredlContactMethodID
                        NULL,  -- CellAreaCode
                        NULL,  -- CellExchangeNumber
                        NULL,  -- CellExtensionNumber
                        NULL,  -- CellUnitNumber
                        @EmailAddress,  -- EmailAddress
                        @FaxAreaCode,  -- FaxAreaCode
                        @FaxExchangeNumber,  -- FaxExchangeNumber
                        NULL,  -- FaxExtensionNumber
                        @FaxUnitNumber,  -- FaxUnitNumber
                        NULL,  -- GenderCD
                        NULL,  -- MinorityFlag
                        @ShopContact,  -- Name
                        NULL,  -- PagerAreaCode
                        NULL,  -- PagerExchangeNumber
                        NULL,  -- PagerExtensionNumber
                        NULL,  -- PagerUnitNumber
                        @PhoneAreaCode,  -- PhoneAreaCode
                        @PhoneExchangeNumber,  -- PhoneExchangeNumber
                        NULL,  -- PhoneExtensionNumber
                        @PhoneUnitNumber,  -- PhoneUnitNumber
                        0,  -- ShopManagerFlag
                        0,  -- SysLastUserID
                        @ModifiedDateTime  -- SysLastUpdatedDate
                    )
            
                    SET @PersonnelId = SCOPE_IDENTITY()
        
                    INSERT INTO dbo.utb_shop_location_personnel
                    (
                        ShopLocationId,
                        PersonnelId,
                        SysLastUserID,
                        SysLastUpdatedDate
                    )
                    VALUES
                    (
                        @ShopLocationId,  -- ShopLocationId
                        @PersonnelId,  -- PersonnelId
                        0,  -- SysLastUserID
                        @ModifiedDateTime  -- SysLastUpdatedDate
                    )
                END
                
                IF LEN(RTRIM(LTRIM(@MFStart))) > 0
                BEGIN
                    INSERT INTO dbo.utb_shop_location_hours
                    (
                        ShopLocationId,
                        OperatingFridayEndTime,
                        OperatingFridayStartTime,
                        OperatingMondayEndTime,
                        OperatingMondayStartTime,
                        OperatingSaturdayEndTime,
                        OperatingSaturdayStartTime,
                        OperatingThursdayEndTime,
                        OperatingThursdayStartTime,
                        OperatingTuesdayEndTime,
                        OperatingTuesdayStartTime,
                        OperatingWednesdayEndTime,
                        OperatingWednesdayStartTime,
                        SysLastUserID,
                        SysLastUpdatedDate
                    )
                    VALUES
                    (
                        @ShopLocationId,  -- ShopLocationId
                        CASE LEN(@MFEnd)
                            WHEN 4 THEN @MFEnd
                            ELSE NULL
                        END, -- OperatingFridayEndTime
                        CASE LEN(@MFStart)
                            WHEN 4 THEN @MFStart
                            ELSE NULL
                        END, -- OperatingFridayStartTime
                        CASE LEN(@MFEnd)
                            WHEN 4 THEN @MFEnd
                            ELSE NULL
                        END, -- OperatingMondayEndTime
                        CASE LEN(@MFStart)
                            WHEN 4 THEN @MFStart
                            ELSE NULL
                        END, -- OperatingMondayStartTime
                        CASE LEN(@SatEnd)
                            WHEN 4 THEN @SatEnd
                            ELSE NULL
                        END, -- OperatingSaturdayEndTime
                        CASE LEN(@SatStart)
                            WHEN 4 THEN @SatStart
                            ELSE NULL
                        END, -- OperatingSaturdayStartTime
                        CASE LEN(@MFEnd)
                            WHEN 4 THEN @MFEnd
                            ELSE NULL
                        END,  -- OperatingThursdayEndTime
                        CASE LEN(@MFStart)
                            WHEN 4 THEN @MFStart
                            ELSE NULL
                        END, -- OperatingThursdayStartTime
                        CASE LEN(@MFEnd)
                            WHEN 4 THEN @MFEnd
                            ELSE NULL
                        END, -- OperatingTuesdayEndTime
                        CASE LEN(@MFStart)
                            WHEN 4 THEN @MFStart
                            ELSE NULL
                        END, -- OperatingTuesdayStartTime
                        CASE LEN(@MFEnd)
                            WHEN 4 THEN @MFEnd
                            ELSE NULL
                        END, -- OperatingWednesdayEndTime
                        CASE LEN(@MFStart)
                            WHEN 4 THEN @MFStart
                            ELSE NULL
                        END,-- OperatingWednesdayStartTime
                        0,  -- SysLastUserID
                        @ModifiedDateTime  -- SysLastUpdatedDate
                    )
                END
                
        END  -- Duplicate shops
        ELSE
        BEGIN
            -- At this point, all CEIFlags for enabled shops have been disabled, and (at least one) enabled
            -- shop with a matching phone number has been located
            
            -- Update CEI flags for enabled shops with matching phone 
            
            UPDATE utb_shop_location
               SET CEIProgramFlag = 1,
                   CEIProgramId = @CEIProgramId,
                   SysLastUserID = 0,
                   SysLastUpdatedDate = @ModifiedDateTime
             WHERE EnabledFlag = 1
               AND @PhoneAreaCode = PhoneAreaCode
               AND @PhoneExchangeNumber = PhoneExchangeNumber
               AND @PhoneUnitNumber = PhoneUnitNumber
               

            -- Update Shop data for the enabled CEI shops with matching phone numbers that aren't Lynx Select program shops            
            
            UPDATE utb_shop
               SET Address1 = @Address1,
                   AddressCity = @AddressCity,
                   AddressCounty = @AddressCounty,
                   AddressState = @AddressState,
                   AddressZip = @AddressZip,
                   FaxAreaCode = @FaxAreaCode,
                   FaxExchangeNumber = @FaxExchangeNumber,
                   FaxUnitNumber = @FaxUnitNumber,
                   SysLastUserID = 0,
                   SysLastUpdatedDate = @ModifiedDateTime
             WHERE EnabledFlag = 1 
               AND ShopID IN 
                    (
                    SELECT ShopID FROM dbo.utb_shop_location 
                     WHERE EnabledFlag = 1
                       AND CertifiedFirstFlag = 0
                       AND ProgramFlag = 0
                       AND DataReviewStatusCD = 'N'
                       AND @PhoneAreaCode = PhoneAreaCode
                       AND @PhoneExchangeNumber = PhoneExchangeNumber
                       AND @PhoneUnitNumber = PhoneUnitNumber
                    )

               
            -- Update Shop Location Billing data for the enabled CEI shops with matching phone numbers that aren't program shops            

            UPDATE utb_billing
               SET Address1 = @Address1,
                   AddressCity = @AddressCity,
                   AddressCounty = @AddressCounty,
                   AddressState = @AddressState,
                   AddressZip = @AddressZip,
                   FaxAreaCode = @FaxAreaCode,
                   FaxExchangeNumber = @FaxExchangeNumber,
                   FaxUnitNumber = @FaxUnitNumber,
                   SysLastUserID = 0,
                   SysLastUpdatedDate = @ModifiedDateTime
             WHERE BillingID IN
                    (
                    SELECT BillingID FROM dbo.utb_shop_location 
                     WHERE EnabledFlag = 1
                       AND CertifiedFirstFlag = 0
                       AND ProgramFlag = 0
                       AND DataReviewStatusCD = 'N'
                       AND @PhoneAreaCode = PhoneAreaCode
                       AND @PhoneExchangeNumber = PhoneExchangeNumber
                       AND @PhoneUnitNumber = PhoneUnitNumber
                    )


            -- Update Shop Location data for the enabled CEI shops with matching phone numbers that aren't program shops            

            UPDATE utb_shop_location
               SET Address1 = @Address1,
                   AddressCity = @AddressCity,
                   AddressCounty = @AddressCounty,
                   AddressState = @AddressState,
                   AddressZip = @AddressZip,
                   DrivingDirections = @DrivingDirections,
                   EmailAddress = @EmailAddress,
                   FaxAreaCode = @FaxAreaCode,
                   FaxExchangeNumber = @FaxExchangeNumber,
                   FaxUnitNumber = @FaxUnitNumber,
                   Latitude = @Latitude,
                   Longitude = @Longitude,
                   WarrantyPeriodRefinishCD = '99',
                   WarrantyPeriodWorkmanshipCD = '99',
                   SysLastUserID = 0,
                   SysLastUpdatedDate = @ModifiedDateTime
             WHERE EnabledFlag = 1
               AND CertifiedFirstFlag = 0
               AND ProgramFlag = 0
               AND DataReviewStatusCD = 'N'
               AND @PhoneAreaCode = PhoneAreaCode
               AND @PhoneExchangeNumber = PhoneExchangeNumber
               AND @PhoneUnitNumber = PhoneUnitNumber

            UPDATE utb_shop_location_hours
               SET OperatingFridayEndTime = CASE LEN(@MFEnd) WHEN 4 THEN @MFEnd ELSE NULL END,
                    OperatingFridayStartTime = CASE LEN(@MFStart) WHEN 4 THEN @MFStart ELSE NULL END,
                    OperatingMondayEndTime = CASE LEN(@MFEnd) WHEN 4 THEN @MFEnd ELSE NULL END,
                    OperatingMondayStartTime = CASE LEN(@MFStart) WHEN 4 THEN @MFStart ELSE NULL END,
                    OperatingSaturdayEndTime = CASE LEN(@SATEnd) WHEN 4 THEN @SATEnd ELSE NULL END,
                    OperatingSaturdayStartTime = CASE LEN(@SATStart) WHEN 4 THEN @SATStart ELSE NULL END,
                    OperatingThursdayEndTime = CASE LEN(@MFEnd) WHEN 4 THEN @MFEnd ELSE NULL END,
                    OperatingThursdayStartTime = CASE LEN(@MFStart) WHEN 4 THEN @MFStart ELSE NULL END,
                    OperatingTuesdayEndTime = CASE LEN(@MFEnd) WHEN 4 THEN @MFEnd ELSE NULL END,
                    OperatingTuesdayStartTime = CASE LEN(@MFStart) WHEN 4 THEN @MFStart ELSE NULL END,
                    OperatingWednesdayEndTime = CASE LEN(@MFEnd) WHEN 4 THEN @MFEnd ELSE NULL END,
                    OperatingWednesdayStartTime = CASE LEN(@MFStart) WHEN 4 THEN @MFStart ELSE NULL END,
                    SysLastUserID = 0,
                    SysLastUpdatedDate = @ModifiedDateTime
               from utb_shop_location_hours h
               join utb_shop_location l on l.shoplocationid = h.shoplocationid
             WHERE l.EnabledFlag = 1
               AND l.ProgramFlag = 0
               AND CertifiedFirstFlag = 0
               AND DataReviewStatusCD = 'N'
               AND @PhoneAreaCode = l.PhoneAreaCode
               AND @PhoneExchangeNumber = l.PhoneExchangeNumber
               AND @PhoneUnitNumber = l.PhoneUnitNumber
            
        END
    
        FETCH NEXT FROM shop_cursor INTO @Address1, @AddressCity, @AddressCounty, @AddressState, @AddressZip, @CEIProgramId, @FaxAreaCode, @FaxExchangeNumber, @FaxUnitNumber, @Latitude, @Longitude, @Name, @PhoneAreaCode, @PhoneExchangeNumber, @PhoneUnitNumber, @EmailAddress, @ShopContact, @MFStart, @MFEnd, @SatStart, @SatEnd, @DrivingDirections

    END
    CLOSE shop_cursor
    DEALLOCATE shop_cursor

-- Set default shop location to CEI

    UPDATE utb_shop_location
           SET CEIProgramFlag = 1
     WHERE ShopLocationID = '60514'
     

    SELECT 'utb_shop_location Total', COUNT(*) FROM utb_shop_location 
    SELECT 'utb_shop_location CEI', COUNT(*) FROM utb_shop_location WHERE CEIProgramFlag = 1 AND EnabledFlag = 1

    COMMIT
END        

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdmCEIShopMigrate' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdmCEIShopMigrate TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/

