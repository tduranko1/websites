-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInfoInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopInfoInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopInfoInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Insert a new Shop
*
*
* RESULT SET:
* ShopLocationID                          The newly created Shop Location unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopInfoInsDetail
(
    @ShopBillingID                      udt_std_id_big,
    @DataReviewUserID                   udt_std_id=NULL,
    @PreferredCommunicationMethodID     udt_std_int_tiny=NULL,
    @PreferredEstimatePackageID         udt_std_int_tiny=NULL,
    @ProgramManagerUserID               udt_std_id=NULL,
    @BusinessInfoID                     udt_std_id_big,
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
    @AutoVerseId                        udt_std_desc_mid=NULL,
    @AvailableForSelectionFlag          udt_std_flag=NULL,
    @CertifiedFirstFlag                 udt_std_flag=NULL,
    @CertifiedFirstId                   udt_std_desc_short=NULL,
    @DataReviewDate                     VARCHAR(30)=NULL,
    @DataReviewStatusCD                 udt_std_cd,
    @DMVFacility                        udt_std_desc_short = NULL,
    @DrivingDirections                  udt_std_desc_long=NULL,
    @EmailAddress                       udt_web_email=NULL,
    @EnabledFlag                        udt_enabled_flag=1,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @Latitude                           udt_addr_latitude=NULL,
    @Longitude                          udt_addr_longitude=NULL,
    @MaxWeeklyAssignments               udt_std_int_tiny=NULL,
    @MergeDate                          udt_std_datetime=NULL,
    @MergeUserID                        udt_std_id=NULL,
    @Name                               udt_std_name,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @PPGCTSCustomerFlag                 udt_std_flag,
    @PPGCTSFlag                         udt_std_flag,
    @PPGCTSId                           udt_std_desc_short=NULL,
    @PPGCTSLevelCD                      udt_std_cd=NULL,
    @PreferredCommunicationAddress      udt_web_address=NULL,
    @ProgramFlag                        udt_std_flag=0,
    @ReferralFlag                        udt_std_flag=0,		--WJC
    @ProgramScore                       udt_std_int_tiny=NULL,
    @RegistrationNumber                 udt_std_desc_short=NULL,
    @RegistrationExpDate                VARCHAR(30)=NULL,
    @SalesTaxNumber                     udt_std_desc_short = NULL,
    @WebSiteAddress                     udt_web_address=NULL,
    @WarrantyPeriodRefinishCD           udt_std_cd=NULL,
    @WarrantyPeriodWorkmanshipCD        udt_std_cd=NULL,
    @SysLastUserID                      udt_std_id,
    @ApplicationCD                      udt_std_cd='APD',
    @ShopLocationID						udt_std_id = NULL OUTPUT
)

AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
        
    -- Declare internal variables

    DECLARE  @error              udt_std_int,
             @rowcount           udt_std_int,
             @tDataReviewDate    udt_std_datetime,
             @ShopID             udt_std_id_big,
             @ProcName           varchar(30),       -- Used for raise error stmts 
             @WorkmanshipID      udt_std_int_tiny,
             @RefinishID         udt_std_int_tiny,
             @LogComment         udt_std_desc_long,
             @newValue           udt_std_desc_long,
             @now                udt_std_datetime
    
    

    SET @ProcName = 'uspSMTShopInfoInsDetail'
    SET @now = CURRENT_TIMESTAMP

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0                       SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0                       SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0                    SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0                  SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0                   SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0                     SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@AutoVerseId))) = 0                    SET @AutoVerseId = NULL
    IF LEN(RTRIM(LTRIM(@AvailableForSelectionFlag))) = 0      SET @AvailableForSelectionFlag = NULL
    IF LEN(RTRIM(LTRIM(@CertifiedFirstId))) = 0               SET @CertifiedFirstId = NULL
    IF LEN(RTRIM(LTRIM(@DataReviewStatusCD))) = 0             SET @DataReviewStatusCD = NULL
    IF LEN(RTRIM(LTRIM(@DataReviewDate))) = 0                 SET @DataReviewDate = NULL
    IF LEN(RTRIM(LTRIM(@DMVFacility))) = 0                    SET @DMVFacility = NULL
    IF LEN(RTRIM(LTRIM(@DrivingDirections))) = 0              SET @DrivingDirections = NULL
    IF LEN(RTRIM(LTRIM(@EmailAddress))) = 0                   SET @EmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0                    SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0              SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0             SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0                  SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0                           SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0                  SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0            SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0           SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0                SET @PhoneUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@PPGCTSId))) = 0                       SET @PPGCTSId = NULL
    IF LEN(RTRIM(LTRIM(@PPGCTSLevelCD))) = 0                  SET @PPGCTSLevelCD = NULL
    IF LEN(RTRIM(LTRIM(@PreferredCommunicationAddress))) = 0  SET @PreferredCommunicationAddress = NULL
    IF LEN(RTRIM(LTRIM(@WebSiteAddress))) = 0                 SET @WebSiteAddress = NULL
    IF LEN(RTRIM(LTRIM(@RegistrationNumber)))     = 0         SET @RegistrationNumber = NULL
    IF LEN(RTRIM(LTRIM(@RegistrationExpDate)))     = 0        SET @RegistrationExpDate = NULL
    IF LEN(RTRIM(LTRIM(@SalesTaxNumber)))     = 0             SET @SalesTaxNumber = NULL

   

    -- Apply edits
    
    IF @DataReviewDate IS NOT NULL AND
       ISDATE(@DataReviewDate) = 0
    BEGIN
        -- Convert the value passed in effective date into date format
        
        SET @tDataReviewDate = CAST(@DataReviewDate AS DATETIME)
        
        -- Validate the effective date
        
        IF @tDataReviewDate IS NULL
        BEGIN 
            -- Invalid Data Review date value
            RAISERROR('101|%s|@DataReviewDate|%s', 16, 1, @ProcName, @DataReviewDate)
            RETURN
        END
    END
    
    IF @DataReviewStatusCD NOT IN (SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'DataReviewStatusCD'))
    BEGIN
       -- Invalid Data Review Status
        RAISERROR('101|%s|@DataReviewStatusCD|%s', 16, 1, @ProcName, @DataReviewDate)
        RETURN
    END
    
    IF @DataReviewStatusCD IN ('N') AND @DataReviewDate IS NOT NULL
    BEGIN
        -- Data Review Date not allowed
        SET @DataReviewDate = NULL
    END

    IF @DataReviewStatusCD IN ('I', 'A') AND @DataReviewDate IS NULL
    BEGIN
       -- Data Review Date required
        RAISERROR('101|%s|@DataReviewDate|%s', 16, 1, @ProcName, @DataReviewDate)
        RETURN
    END

    IF @DataReviewStatusCD IN ('N') AND @DataReviewUserID IS NOT NULL
    BEGIN
       -- Data Review User not allowed
       SET @DataReviewUserID = NULL
    END

    IF @DataReviewStatusCD IN ('I', 'A') AND @DataReviewUserID IS NULL
    BEGIN
       -- Data Review User required
        RAISERROR('101|%s|@DataReviewUserID|%u', 16, 1, @ProcName, @DataReviewUserID)
        RETURN
    END

    IF @ProgramScore IS NOT NULL
    BEGIN
        IF @ProgramScore > 100 OR @ProgramScore < 0
        BEGIN
           -- Invalid Program Score
            RAISERROR('101|%s|@ProgramScore|%u', 16, 1, @ProcName, @ProgramScore)
            RETURN
        END
    END

    IF LEN(@Name) < 1
    BEGIN
       -- Missing Shop Location Name
        RAISERROR('101|%s|@Name|%s', 16, 1, @ProcName, @Name)
        RETURN
    END


    IF @ShopBillingID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT BillingID FROM utb_billing WHERE BillingID = @ShopBillingID)
        BEGIN
           -- Invalid Shop Billing
            RAISERROR('101|%s|@ShopBillingID|%u', 16, 1, @ProcName, @ShopBillingID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Shop Billing
        RAISERROR('101|%s|@BillingID|%u', 16, 1, @ProcName, @ShopBillingID)
        RETURN
    END

    IF @DataReviewUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@DataReviewUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid Data Reviewer
            RAISERROR('101|%s|@DataReviewUserID|%u', 16, 1, @ProcName, @DataReviewUserID)
            RETURN
        END
    END

    IF @PPGCTSLevelCD IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'PPGCTSLevelCD'))
        BEGIN
           -- Invalid Communication Method
            RAISERROR('101|%s|@PPGCTSLevelCD|%s', 16, 1, @ProcName, @PPGCTSLevelCD)
            RETURN
        END
    END

    IF @PreferredCommunicationMethodID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT CommunicationMethodID FROM utb_communication_method WHERE CommunicationMethodID = @PreferredCommunicationMethodID
                                                                                    AND EnabledFlag = 1)
        BEGIN
           -- Invalid Communication Method
            RAISERROR('101|%s|@PreferredCommunicationMethodID|%u', 16, 1, @ProcName, @PreferredCommunicationMethodID)
            RETURN
        END
    END

    IF @PreferredEstimatePackageID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT EstimatePackageID FROM utb_estimate_package WHERE EstimatePackageID = @PreferredEstimatePackageID
                                                                            AND EnabledFlag = 1)
        BEGIN
           -- Invalid Estimate Package
            RAISERROR('101|%s|@PreferredEstimatePackageID|%u', 16, 1, @ProcName, @PreferredEstimatePackageID)
            RETURN
        END
    END

    IF @ProgramManagerUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@ProgramManagerUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid Program Manager
            RAISERROR('101|%s|@ProgramManagerUserID|%u', 16, 1, @ProcName, @ProgramManagerUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
        IF @ProgramFlag = 1
        BEGIN
           -- Missing Program Manager
            RAISERROR('101|%s|@ProgramManagerUserID|%u', 16, 1, @ProcName, @ProgramManagerUserID)
            RETURN
        END
    END

    IF @BusinessInfoID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT ShopID FROM utb_shop WHERE ShopID = @BusinessInfoID
                                                     AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop
            RAISERROR('101|%s|@BusinessInfoID|%u', 16, 1, @ProcName, @BusinessInfoID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Shop
        RAISERROR('101|%s|@BusinessInfoID|%u', 16, 1, @ProcName, @BusinessInfoID)
        RETURN
    END

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop Location Address State
            RAISERROR('101|%s|@AddressState|%s', 16, 1, @ProcName, @AddressState)
            RETURN
        END
        
        /*IF @AddressState IN ('NY')
        BEGIN
            IF @DMVFacility IS NULL
            BEGIN
               RAISERROR  ('1|Missing DMV Facility # for NY State', 16, 1, @ProcName)
               RETURN
            END

            IF @SalesTaxNumber IS NULL
            BEGIN
               RAISERROR  ('1|Missing Sales Tax # for NY State', 16, 1, @ProcName)
               RETURN
            END
        END*/
    END

    IF @MergeUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@MergeUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@MergeUserID|%u', 16, 1, @ProcName, @MergeUserID)
            RETURN
        END
    END
       
    

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END

    
   
    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopLocationInsDetailTran1
    
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
    INSERT INTO dbo.utb_shop_location	 (BillingID,
                                        DataReviewUserID,
                                        PreferredCommunicationMethodID,
                                        PreferredEstimatePackageID,
                                        ProgramManagerUserID,
                                        ShopID,
                                        Address1,
                                        Address2,
                                        AddressCity,
                                        AddressCounty,
                                        AddressState,
                                        AddressZip,
                                        AutoVerseId,
 					                    AvailableForSelectionFlag,
                                        CertifiedFirstFlag,
                                        CertifiedFirstId,
                                        DataReviewDate,
                                        DataReviewStatusCD,
                                        DMVFacilityNumber,
                                        DrivingDirections,
                                        EmailAddress,
                                        EnabledFlag,
                                        FaxAreaCode,
                                        FaxExchangeNumber,
                                        FaxExtensionNumber,
                                        FaxUnitNumber,
                                        LastAssignmentDate,
                                        Latitude,
                                        Longitude,
                                        MaxWeeklyAssignments,
                                        MergeDate,
                                        MergeUserID,
                                        Name,
                                        PhoneAreaCode,
                                        PhoneExchangeNumber,
                                        PhoneExtensionNumber,
                                        PhoneUnitNumber,
                                        PPGCTSCustomerFlag,
                                        PPGCTSFlag,
                                        PPGCTSId,
                                        PPGCTSLevelCD,
                                        PreferredCommunicationAddress,
                                        ProgramFlag,
                                        ReferralFlag,		--WJC
                                        ProgramScore,
                                        RegistrationExpDate,
                                        RegistrationNumber,
                                        SalesTaxNumber,
                                        WarrantyPeriodRefinishCD,
                                        WarrantyPeriodWorkmanshipCD,
                                        WebSiteAddress,
                                        SysLastUserID,
                                        SysLastUpdatedDate)
                                        
                                VALUES (@ShopBillingID,
                                        @DataReviewUserID,
                                        @PreferredCommunicationMethodID,
                                        @PreferredEstimatePackageID,
                                        @ProgramManagerUserID,
                                        @BusinessInfoID,
                                        @Address1,
                                        @Address2,
                                        @AddressCity,
                                        @AddressCounty,
                                        @AddressState,
                                        @AddressZip,
                                        @AutoVerseId,
                                        @AvailableForSelectionFlag,
                                        @CertifiedFirstFlag,
                                        @CertifiedFirstId,
                                        @DataReviewDate,
                                        @DataReviewStatusCD,
                                        @DMVFacility,
                                        @DrivingDirections,
                                        @EmailAddress,
                                        @EnabledFlag,
                                        @FaxAreaCode,
                                        @FaxExchangeNumber,
                                        @FaxExtensionNumber,
                                        @FaxUnitNumber,
                                        NULL,
                                        @Latitude,
                                        @Longitude,
                                        @MaxWeeklyAssignments,
                                        @MergeDate,
                                        @MergeUserID,
                                        @Name,
                                        @PhoneAreaCode,
                                        @PhoneExchangeNumber,
                                        @PhoneExtensionNumber,
                                        @PhoneUnitNumber,
                                        @PPGCTSCustomerFlag,
                                        @PPGCTSFlag,
                                        @PPGCTSId,
                                        @PPGCTSLevelCD,
                                        @PreferredCommunicationAddress,
                                        @ProgramFlag,
                                        @ReferralFlag,		--WJC
                                        @ProgramScore,
                                        @RegistrationExpDate,
                                        @RegistrationNumber,
                                        @SalesTaxNumber,
                                        @WarrantyPeriodRefinishCD,
                                        @WarrantyPeriodWorkmanshipCD,
                                        @WebSiteAddress,
                                        @SysLastUserID,
                                        @now)
        
    
    
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    SET @ShopID = SCOPE_IDENTITY()
    SET @ShopLocationID = @ShopID
       
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_shop_location', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END
    
    
    -- Audit Log Code ----------------------------------------------------------------------------------------- 
     
    /*   
    SET @LogComment = 'Shop created:  ' + @Name
    
    EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
    
            
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
        ROLLBACK TRANSACTION
        RETURN
    END  
    */
    
   
    IF @ShopID IS NOT NULL
    BEGIN
      SET @LogComment = 'New Shop Created: ' + CONVERT(varchar(12), @ShopID) + '--' + @Name
                        
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  


    IF @DataReviewUserID IS NOT NULL
    BEGIN
      
      SET @newValue = (SELECT NameFirst + ' ' + NameLast FROM dbo.utb_user WHERE UserID = @DataReviewUserID)
      
      SET @LogComment = 'Data Reviewed by established: ' + ISNULL(@newValue, @DataReviewUserID)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    IF @PreferredCommunicationMethodID IS NOT NULL
    BEGIN
      SET @newValue = (SELECT Name FROM dbo.utb_communication_method 
                                   WHERE CommunicationMethodID = @PreferredCommunicationMethodID)
    
      SET @LogComment = 'Preferred Communication Method established: ' + ISNULL(@newValue, @PreferredCommunicationMethodID)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
                  
    IF @PreferredEstimatePackageID IS NOT NULL
    BEGIN
      SET @newValue = (SELECT Name FROM dbo.utb_estimate_package 
                                   WHERE EstimatePackageID = @PreferredEstimatePackageID)
    
      SET @LogComment = 'Preferred Estimate Package established: ' + ISNULL(@newValue, @PreferredEstimatePackageID)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                
                  
      
    IF @ProgramManagerUserID IS NOT NULL
    BEGIN
      SET @newValue = (SELECT NameFirst + ' ' + NameLast FROM dbo.utb_user WHERE UserID = @ProgramManagerUserID)
    
      SET @LogComment = 'Program Manager established: ' + ISNULL(@newValue, @ProgramManagerUserID)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END       
     
          
    IF @BusinessInfoID IS NOT NULL
    BEGIN
      SET @newValue = (SELECT Name FROM dbo.utb_shop WHERE ShopID = @BusinessInfoID)
    
      SET @LogComment = 'Business established: ' + ISNULL(@newValue, @BusinessInfoID)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END       
    
        
    IF @Address1 IS NOT NULL
    BEGIN
      SET @LogComment = 'Address 1 established: ' + @Address1
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
     
     
    IF @Address2 IS NOT NULL
    BEGIN
      SET @LogComment = 'Address 2 established: ' + @Address2
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END               
                      
                            
    IF @AddressCity IS NOT NULL
    BEGIN
      SET @LogComment = 'City established: ' + @AddressCity
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
    
    
    IF @AddressCounty IS NOT NULL
    BEGIN
      SET @LogComment = 'County established: ' + @AddressCounty
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                         
             
             
    IF @AddressState IS NOT NULL
    BEGIN
      SET @LogComment = 'State established: ' + @AddressState
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END              
                         
                       
    IF @AddressZip IS NOT NULL
    BEGIN
      SET @LogComment = 'ZIP Code established: ' + @AddressZip
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
      

    IF @AvailableForSelectionFlag IS NOT NULL
    BEGIN
      SET @LogComment = 'Shop Availability: ' + (SELECT CASE @AvailableForSelectionFlag  WHEN 1 THEN 'YES' ELSE 'NO' END)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END     

                    
    
    IF @CertifiedFirstFlag IS NOT NULL
    BEGIN
      SET @LogComment = 'Certified First Participation turned: ' + (SELECT CASE @CertifiedFirstFlag WHEN 1 THEN 'ON' ELSE 'OFF' END)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END     
                 
                 
    IF @CertifiedFirstId IS NOT NULL
    BEGIN
      SET @LogComment = 'Certified First ID established: ' + @CertifiedFirstId
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                  
                 
                 
    IF @DataReviewDate IS NOT NULL
    BEGIN
      SET @LogComment = 'Data Review Date established: ' + @DataReviewDate
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                            
                  
     
    IF @DataReviewStatusCD IS NOT NULL
    BEGIN
      SET @newValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'DataReviewStatusCD')
                                   WHERE Code = @DataReviewStatusCD)
    
      SET @LogComment = 'Data Review Status established: ' + ISNULL(@newValue, @DataReviewStatusCD)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END       
          
                    
    IF @DrivingDirections IS NOT NULL
    BEGIN
      
      SET @LogComment = LEFT(('Driving Directions established: ' + @DrivingDirections), 500)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                         
       
    
    IF @EmailAddress IS NOT NULL
    BEGIN
      SET @LogComment = 'Email Address established: ' + @EmailAddress
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
                  
                   
    IF @FaxAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Area code established: ' + @FaxAreaCode
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
                        
         
    IF @FaxExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Exchange Number established: ' + @FaxExchangeNumber
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
                         
                         
    IF @FaxExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Extension Number established: ' + @FaxExtensionNumber
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    
    IF @FaxUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Fax Unit Number established: ' + @FaxUnitNumber
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                   
                  
    
    IF @Latitude IS NOT NULL
    BEGIN
      SET @LogComment = 'Latitude established: ' + CONVERT(varchar(30), @Latitude)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END       
    
    
    IF @Longitude IS NOT NULL
    BEGIN
      SET @LogComment = 'Longitude established: ' + CONVERT(varchar(30), @Longitude)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END 
    
    
    
    IF @MaxWeeklyAssignments IS NOT NULL
    BEGIN
      SET @LogComment = 'Max Weekly Assignments established: ' + CONVERT(varchar(6), @MaxWeeklyAssignments)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                    
                            
                           
    IF @PhoneAreaCode IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Area Code established: ' + @PhoneAreaCode
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                  
             
      
    IF @PhoneExchangeNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Exchange Code established: ' + @PhoneExchangeNumber
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                 
           
           
    IF @PhoneExtensionNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Extension Code established: ' + @PhoneExtensionNumber
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                                 
                       
                 
    IF @PhoneUnitNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Phone Unit Number established: ' + @PhoneUnitNumber
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END     
    
    
    IF @PPGCTSCustomerFlag IS NOT NULL
    BEGIN
      SET @LogComment = 'PPG CTS Customer Participation established: ' + (SELECT CASE @PPGCTSCustomerFlag WHEN 1 THEN 'ON' ELSE 'OFF' END)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                
            
            
    IF @PPGCTSFlag IS NOT NULL
    BEGIN
      SET @LogComment = 'PPG CTS Participation established: ' + (SELECT CASE @PPGCTSFlag WHEN 1 THEN 'ON' ELSE 'OFF' END)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                       
    
    
    IF @PPGCTSId IS NOT NULL
    BEGIN
      SET @LogComment = 'PPG CTS ID established: ' + @PPGCTSId
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    
                  
    IF @PPGCTSLevelCD IS NOT NULL
    BEGIN
      SET @newValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'PPGCTSLevelCD') WHERE Code = @PPGCTSLevelCD)
    
      SET @LogComment = 'PPG CTS Level established: ' + ISNULL(@newValue, @PPGCTSLevelCD)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                          
       
           
           
    IF @PreferredCommunicationAddress IS NOT NULL
    BEGIN
      SET @LogComment = 'Preferred Communication Address established: ' + @PreferredCommunicationAddress
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    IF @AutoVerseId IS NOT NULL
    BEGIN
      SET @LogComment = 'AutoVerse ID established: ' + @AutoVerseId
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
                            
    IF @ProgramFlag IS NOT NULL
    BEGIN
      SET @LogComment = 'LYNX Select Program Participation established: ' + (SELECT CASE @ProgramFlag WHEN 1 THEN 'ON' ELSE 'OFF' END)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                      
           
     
    IF @ProgramScore IS NOT NULL
    BEGIN
      SET @LogComment = 'Program Score established: ' + CONVERT(varchar(6), @ProgramScore)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    IF @RegistrationExpDate IS NOT NULL
    BEGIN
      SET @LogComment = 'Registration Expiration Date established: ' + CONVERT(varchar(6), @RegistrationExpDate)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    IF @RegistrationNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Registration Number established: ' + CONVERT(varchar(6), @RegistrationNumber)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
     
    IF @WebSiteAddress IS NOT NULL
    BEGIN
      SET @LogComment = 'Website Address established: ' + @WebSiteAddress
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
           
    IF @WarrantyPeriodRefinishCD IS NOT NULL
    BEGIN
      SET @newValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'WarrantyPeriodRefinishCD')
                                   WHERE Code = @WarrantyPeriodRefinishCD)
    
      SET @LogComment = 'Refinish Warranty Period established: ' + ISNULL(@newValue, @WarrantyPeriodRefinishCD)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                               
     
                        
    IF @WarrantyPeriodWorkmanshipCD IS NOT NULL
    BEGIN
      SET @newValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'WarrantyPeriodWorkmanshipCD')
                                   WHERE Code = @WarrantyPeriodWorkmanshipCD)
    
      SET @LogComment = 'Workmanship Warranty Period established: ' + ISNULL(@newValue, @WarrantyPeriodRefinishCD)
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                         
                    
     
    IF @DMVFacility IS NOT NULL
    BEGIN
      SET @LogComment = 'DMV Facility established: ' + @DMVFacility
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END

     
    IF @SalesTaxNumber IS NOT NULL
    BEGIN
      SET @LogComment = 'Sales Tax Number established: ' + @SalesTaxNumber
                            
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
         
    
    ----  End of Audit Log code   -----------------------------------------------------------------------------

    
    
    
    COMMIT TRANSACTION AdmShopLocationInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ShopID AS ShopID

    RETURN @rowcount

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInfoInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopInfoInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO



