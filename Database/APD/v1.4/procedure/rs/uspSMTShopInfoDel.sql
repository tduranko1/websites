-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInfoDel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopInfoDel 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopInfoDel
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Disable a Shop 
*
* PARAMETERS:  
* (I) @ShopID                             The Shop's unique identity
* (I) @SysLastUserID                      The Shop's updating user identity
* (I) @SysLastUpdatedDate                 The Shop's last update date as string (VARCHAR(30))
*
* RESULT SET:
* ShopID                          The updated Shop unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopInfoDel
(
	@ShopID                            	udt_std_int_big,
  @SysLastUserID                      udt_std_id,
  @SysLastUpdatedDate                 varchar(30),
  @ApplicationCD                      udt_std_cd='APD'
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error                udt_std_int
    DECLARE @rowcount             udt_std_int
    
    DECLARE @tupdated_date        udt_std_datetime 
    DECLARE @temp_updated_date    udt_std_datetime 
    
    DECLARE @ProcName             varchar(30)       -- Used for raise error stmts 
    DECLARE @Name                 udt_std_name
    

    SET @ProcName = 'uspSMTShopInfoDel'
    SET @Name = (SELECT Name FROM dbo.utb_shop_location WHERE ShopLocationID = @ShopID)

    -- Apply edits
    
    -- Validate the updated date parameter
    
    IF @SysLastUpdatedDate IS NULL OR 
       ISDATE(@SysLastUpdatedDate) = 0
    BEGIN
        -- Invalid updated date value
        RAISERROR('101|%s|@SysLastUpdatedDate|%s', 16, 1, @ProcName, @SysLastUpdatedDate)
        RETURN
    END
    
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    
    -- Check the date

    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_shop_location', @ShopID

    IF @@ERROR <> 0
    BEGIN
        -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

        RETURN
    END


    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
            RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopLocationEnableTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    UPDATE  dbo.utb_shop_location SET
            EnabledFlag = 0,
            SysLastUserID = @SysLastUserID,
            SysLastUpdatedDate = CURRENT_TIMESTAMP
      WHERE ShopLocationID = @ShopID
        AND SysLastUpdatedDate = @tupdated_date
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('104|%s|utb_shop_location', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END


    --  Audit Log Code  -----------------------------------------------------------------------------------------------

    EXEC dbo.uspAuditLogInsDetail 'P', NULL, @Name, @ShopID, 'Shop Deleted', @SysLastUserID
         
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
        ROLLBACK TRANSACTION
        RETURN
    END  
    
    -------------------------------------------------------------------------------------------------------------------
    
    
    COMMIT TRANSACTION AdmShopLocationEnableTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ShopID AS ShopID

    RETURN @rowcount
END


GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInfoDel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopInfoDel TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/