-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefDocumentTypeUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRefDocumentTypeUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRefDocumentTypeUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Update a specific Document Type
*
* PARAMETERS:  
* (I) @DocumentTypeID           Document Type unique identity
* (I) @DisplayOrder             Document Type display order
* (I) @EnabledFlag              Document Type active/inactive flag
* (I) @EstimateTypeFlag         Document Estimate Type flag
* (I) @Name                     Document Type name
* (I) @SysLastUserID            Document Type user performing action
* (I) @SysLastUpdatedDate       Document Type last update date as string (VARCHAR(30))
*
* RESULT SET:
* DocumentTypeID     The updated Document Type unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRefDocumentTypeUpdDetail
(
	@DocumentTypeID           udt_std_int_tiny,
	@DisplayOrder             udt_std_int_tiny,
	@EnabledFlag              udt_std_flag,
	@EstimateTypeFlag         udt_std_flag,
    @DocumentClassCD          udt_std_cd,
	@Name                     udt_std_name,
	@SysLastUserID            udt_std_id,
    @SysLastUpdatedDate       VARCHAR(30),
    @ApplicationCD            udt_std_cd = 'APD'
)

AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @tupdated_date     AS DATETIME 
    DECLARE @temp_updated_date AS DATETIME 

    DECLARE @SysMaintainedFlag AS udt_sys_maintained_flag

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now               AS datetime

    SET @ProcName = 'uspRefDocumentTypeUpdDetail'
    SET @now = CURRENT_TIMESTAMP

    
    -- Validate the updated date parameter
    
    exec uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_document_type', @DocumentTypeID

    if @@error <> 0
    BEGIN
        RETURN
    END
    
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    

    IF @SysMaintainedFlag = 1
    BEGIN
        -- raise a error if the user has modified the data other than the display order
        -- for the System Maintained records.
        IF NOT EXISTS (SELECT DocumentTypeID 
                            FROM utb_document_type
                            WHERE DocumentTypeID = @DocumentTypeID
                                AND EstimateTypeFlag = @EstimateTypeFlag
                                AND EnabledFlag = @EnabledFlag
                                AND Name = @Name)
        BEGIN
            RAISERROR  ('1|Database record may not be modified', 16, 1, @ProcName)
            RETURN
        END
    END 

    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT u.UserID 
                       FROM dbo.utb_user u INNER JOIN dbo.utb_user_application ua ON u.UserID = ua.UserID
                                           INNER JOIN dbo.utb_application a ON ua.ApplicationID = a.ApplicationID
                       WHERE u.UserID = @SysLastUserID
                         AND @SysLastUserID <> 0
                         AND ua.AccessBeginDate IS NOT NULL
                         AND ua.AccessBeginDate <= CURRENT_TIMESTAMP
                         AND ua.AccessEndDate IS NULL
                         AND a.Code = @ApplicationCD)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END


    BEGIN TRANSACTION RefDocumentTypeUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    UPDATE dbo.utb_document_type
       SET 
		DisplayOrder              = @DisplayOrder,
		EnabledFlag               = @EnabledFlag,
        EstimateTypeFlag          = @EstimateTypeFlag,
        DocumentClassCD           = @DocumentClassCD,
		Name                      = @Name,
		SysLastUserID             = @SysLastUserID,
		SysLastUpdatedDate        = @now
     WHERE 
		DocumentTypeID = @DocumentTypeID AND
		SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_document_type', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END

    COMMIT TRANSACTION RefDocumentTypeUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    -- Create XML Document to return new updated date time and updated vehicle involved list
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- New Involved Level
            NULL AS [DocumentType!2!DocumentTypeID],
            NULL AS [DocumentType!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- New Involved Level
            @DocumentTypeID,
            dbo.ufnUtilityGetDateString( @now )

    ORDER BY tag

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    RETURN @rowcount
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRefDocumentTypeUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRefDocumentTypeUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/