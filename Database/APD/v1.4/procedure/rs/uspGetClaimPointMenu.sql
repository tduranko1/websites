-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClaimPointMenu' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClaimPointMenu 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetClaimPointMenu
* SYSTEM:       Lynx Services APD 
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets Claimpoint navigation information from the utb_admin_claimpoint_menu
*
* PARAMETERS:  
*
* RESULT SET:
*   All data related to Claimpoint navigation
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetClaimPointMenu
	@vMenuLevel VARCHAR(50)
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT
		MenuID
		, MenuLevel
		, ButtonText
		, NavURL
		, ISNULL(MouseOverIcon,'') MouseOverIcon
		, ISNULL(MouseOutIcon,'') MouseOutIcon
		, SortOrder
		, ISNULL(EnabledFlag,'') EnabledFlag 
		, ISNULL(SysLastUserID ,0) SysLastUserID
		, SysLastUpdatedDate
	FROM
		 dbo.utb_admin_claimpoint_menu
	WHERE
		EnabledFlag = 1	 
		AND MenuLevel = @vMenuLevel
	ORDER BY
		SortOrder ASC
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClaimPointMenu' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClaimPointMenu TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClaimPointMenu TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/