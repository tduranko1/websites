-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetActiveServiceChannels' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetActiveServiceChannels 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetActiveServiceChannels
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all active Service Channels from the utb_service table
*
* PARAMETERS:  
*
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetActiveServiceChannels
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON
    
	SELECT 
		ServiceID
		, ISNULL(BillingModelCD,'') AS BillingModelCD
		, DisplayOrder
		, ISNULL(DispositionTypeCD,'') AS DispositionTypeCD
		, EnabledFlag
		, ExposureRequiredFlag
		, MultipleBillingFlag
		, [Name]
		, ISNULL(PartyCD,'') AS PartyCD
		, ISNULL(ServiceChannelCD,'') AS ServiceChannelCD
		, SysMaintainedFlag
		, SysLastUserID
		, SysLastUpdatedDate
	FROM 
		utb_service
	WHERE
		EnabledFlag = 1
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetActiveServiceChannels' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetActiveServiceChannels TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetActiveServiceChannels TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/