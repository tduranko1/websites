-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2OperSurveyList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRpt2OperSurveyList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRpt2OperSurveyList
* SYSTEM:       Lynx Services APD
* AUTHOR:       Madhavi Kotipalli
* FUNCTION:     Compiles data displayed on the Survey List Report
*
* PARAMETERS:  
* (I) @InsuranceCompanyID          The insurance company the report is being run for
* (I) @RptMonth                    The month the report is for
* (I) @RptYear                     The year the report is for
*
* RESULT SET:
*   Insurance Company Name
*   Report Date                    The date for which the report is run
*   Close Date		               Customer call in number for Lynx
*   Office Region		           Customer Office /Region
*   Lynx ID		                   Lynx ID
*   Claim Number		           Claim Coverage Number
*   Loss Date		               Date of Loss Date
*   First Name		               Customer First Name
*   Last Name		               Customer Last Name
*   Business Name	               Business Name  
*   DayTimePhone	               Day time contact phone number     
*   NightTimePhone	               Night time contact phone number
*   AlternativePhone	           Alternative phone number
*   VehicleDescription	           Vehicle Decription ( year + model)
*   Shop Name		               Shop Name
*   Shop State		               Shop State
*   Shop Phone                     Shop Phone Number
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRpt2OperSurveyList
    @InsuranceCompanyID     udt_std_int_small,
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL
AS
BEGIN
    -- Set database options
      
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

    -- Declare Local variables 

    DECLARE @tmpSurveyData TABLE
    (
    InsCompName varchar(50),
    ReportDate varchar(20),
	CloseDate varchar(20),
	OfficeRegion varchar(50),
    LynxID   varchar(15),
	ClaimNumber varchar(30),
	LossDate varchar(12),
	FirstName varchar(50),
	LastName varchar(50),
	BusinessName varchar(50),
	DayTimePhone varchar(20),
	NightTimePhone varchar(20),
	AlternativePhone varchar(20),
	VehicleDescription varchar(20),
	ShopName  varchar(50),
	ShopState char(2),
	ShopPhone varchar(20) 
    )


    DECLARE @InsuranceCompanyName   varchar(50)
    DECLARE @ReportDate              varchar(20)
    DECLARE @MonthName             varchar(15)
    DECLARE @ProcName   varchar(30)
    DECLARE @now   udt_std_datetime
    DECLARE @tmpRowCount udt_std_int_small
    SET @ProcName = 'uspRpt2OperSurveyList'
  

    -- Validate Insurance Company ID
    IF (@InsuranceCompanyID IS NULL) OR
       NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        -- Invalid Insurance Company ID      
        RAISERROR  ('101|%s|@InsuranceCompanyID|%n', 16, 1, @ProcName, @InsuranceCompanyID)
        RETURN
    END
	ELSE
    BEGIN
        SELECT  @InsuranceCompanyName = Name
          FROM  dbo.utb_insurance
          WHERE InsuranceCompanyID = @InsuranceCompanyID
          
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error
    
            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END
    END          
	         
        
 -- Get current timestamp 
    SET @now = CURRENT_TIMESTAMP
   
  -- Check Report Date fields and set as necessary
    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year
    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year
        
        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END
    
    IF @RptMonth < 1 OR @RptMonth > 12  
    BEGIN
        -- Invalid Report Month
        
        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END
 
    SET @ReportDate =  @MonthName + ' ' + Convert(varchar(4), @RptYear)

    -- Compile data needed for the report Survey List
    INSERT INTO @tmpSurveyData
    SELECT
	@InsuranceCompanyName,
    @ReportDate,
    convert(varchar(20), casc.OriginalCompleteDate,101) as 'Close Date',
    o.Name as 'Office/Region',
    c.lynxid as 'LYNX ID',
    upper(c.ClientClaimNumber) as 'Claim Number',
    convert(varchar(12), c.LossDate, 101) as 'Date of Loss',
    case when i.NameFirst is not null then i.NameFirst else '' end as 'First Name',
    case when i.NameLast is not null then i.NameLast else '' end as 'Last Name',
    case when i.BusinessName is not null then i.BusinessName else '' end as 'Business Name',
    case when i.DayAreaCode is null then '' else i.DayAreaCode + ' ' + i.DayExchangeNumber + ' ' + i.DayUnitNumber end as 'Day Phone',
    case when i.NightAreaCode is null then '' else i.NightAreaCode + ' ' + i.NightExchangeNumber + ' ' + i.NightUnitNumber end as  'Night Phone',
    case when i.AlternateAreaCode is null then '' else i.AlternateAreaCode + ' ' + i.AlternateExchangeNumber + ' ' +  i.AlternateUnitNumber end as 'Alternate Phone',
    IsNull(Convert(varchar(4), v.VehicleYear), '') + ' ' + IsNull(v.Make, '') + ' ' + IsNull(v.Model, '') + ' ' +  IsNull(v.BodyStyle, '') as 'Vehicle Description',
    s.Name as 'Shop Name',
    s.AddressState as 'Shop State',
    s.PhoneAreaCode + ' ' + s.PhoneExchangeNumber + ' ' + s.PhoneUnitNumber as 'Shop Phone'
    FROM  dbo.utb_claim_vehicle v
    
    LEFT JOIN dbo.utb_claim_aspect ca on v.ClaimAspectId = ca.ClaimAspectId
    LEFT JOIN dbo.utb_claim_aspect_service_channel casc on casc.ClaimAspectId = ca.ClaimAspectId
    LEFT JOIN dbo.utb_claim c on ca.lynxId = c.lynxId
    LEFT JOIN dbo.utb_user u on c.CarrierRepUserID = u.UserID
    LEFT JOIN dbo.utb_office o on u.OfficeID = o.OfficeID
    LEFT JOIN dbo.utb_claim_coverage cc on ca.lynxId = cc.lynxId
    LEFT JOIN dbo.utb_assignment a on casc.ClaimAspectServiceChannelId = a.ClaimAspectServiceChannelId
    LEFT JOIN dbo.utb_shop_location s on a.ShopLocationID = s.ShopLocationID
    LEFT JOIN dbo.utb_invoice ib on ca.ClaimAspectID = ib.ClaimAspectID
    LEFT JOIN dbo.utb_claim_aspect_involved cai on v.ClaimAspectId = cai.ClaimAspectId
    LEFT JOIN dbo.utb_involved i on cai.InvolvedID = i.InvolvedId
    LEFT JOIN dbo.utb_involved_role ir on i.InvolvedID = ir.InvolvedId
    WHERE
    c.InsuranceCompanyId = @InsuranceCompanyID AND           --  OCG 
    casc.serviceChannelCd = 'PS' AND            --  Program Shop
    ca.exposureCd = '1' AND a.assignmentsequencenumber = 1 AND                  --  1st Party
    a.cancellationDate is null AND           --  Assignment not cancelled
    casc.dispositiontypecd = 'RC' AND           --  Repair completed
    ir.InvolvedRoleTypeID = 4 AND           --  Vehicle Owner
    ca.enabledFlag = 1 AND
    ib.EnabledFlag = 1 AND
    ib.ItemTypeCD = 'F' AND -- Fee
    datepart(year, casc.OriginalCompleteDate) = @RptYear   AND
    datepart(month,casc.OriginalCompleteDate) = @RptMonth
    ORDER BY  'Close Date'

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
        RAISERROR('105|%s|@tmpSurveyData', 16, 1, @ProcName) 
        RETURN
    END

 
   SELECT @tmpRowCount = Count(*)   FROM @tmpSurveyData

   IF @tmpRowCount = 0   
       INSERT INTO @tmpSurveyData  VALUES (@ReportDate,@InsuranceCompanyName,'','','','','','','','','','','','','','','')

   IF @@ERROR <> 0
   BEGIN
           -- Insertion failure
          RAISERROR('105|%s|@tmpSurveyData', 16, 1, @ProcName)
          RETURN
   END
 
   SELECT *  FROM @tmpSurveyData

END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRpt2OperSurveyList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRpt2OperSurveyList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
