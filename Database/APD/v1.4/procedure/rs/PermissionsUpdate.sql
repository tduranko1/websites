-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION
-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopSearchByNameXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopSearchByNameXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspShopSearchByNameXML TO 
        wsAPDUser
    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END
GO

BEGIN TRANSACTION
-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShopSearchByDistanceXML' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShopSearchByDistanceXML TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspShopSearchByDistanceXML TO 
        wsAPDUser
    -- Commit the transaction for dropping and creating the stored procedure
    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END
GO
