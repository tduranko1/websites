-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewBundling' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateNewBundling 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateNewBundling
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client bundling by inserting a new record into the 
*				utb_bundling table modeled after @iModelInsuranceCompanyID
*
* PARAMETERS:  
*
*   @iNewInsuranceCompanyID
*   @iModelInsuranceCompanyID
*	@iCreatedByUserID
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateNewBundling
	@iNewInsuranceCompanyID INT
	, @iModelInsuranceCompanyID INT
	, @vCompanyNameShort VARCHAR(6)
	, @iCreatedByUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @dtNow AS DATETIME 
    DECLARE @ProcName AS VARCHAR(50)

    SET @ProcName = 'uspCreateNewBundling'

    -- Set Database options
    SET NOCOUNT ON
	SET @dtNow = CURRENT_TIMESTAMP

	-- Check to make sure the new insurance company id exists
    IF  (@iNewInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iNewInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iNewInsuranceCompanyID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure the model insurance company id exists
    IF  (@iModelInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iModelInsuranceCompanyID))
    BEGIN
        -- Invalid Model Insurance Company ID
        RAISERROR('101|%s|@iModelInsuranceCompanyID|%u', 16, 1, @ProcName, @iModelInsuranceCompanyID)
        RETURN
    END

	-- Add the new
	DECLARE db_cursor CURSOR FOR 
	SELECT DocumentTypeID, EnabledFlag, [Name] FROM utb_bundling b INNER JOIN utb_client_bundling cb ON b.BundlingId = cb.BundlingID WHERE cb.InsuranceCompanyID = @iModelInsuranceCompanyID
	
	DECLARE @iDocumentTypeID INT
	DECLARE @iEnabledFlag INT
	DECLARE @vName VARCHAR(100)

	DECLARE @iNewMessageTemplateID INT
	DECLARE @iCnt INT
	SET @iCnt = 0

	OPEN db_cursor  
	FETCH NEXT FROM db_cursor INTO 	
		@iDocumentTypeID, @iEnabledFlag, @vName

	WHILE @@FETCH_STATUS = 0  
	BEGIN  
		SELECT TOP 1 @iNewMessageTemplateID = MessageTemplateID FROM dbo.utb_message_template WHERE [Description] LIKE '%' + @vCompanyNameShort + '%' ORDER BY MessageTemplateID 
		SET @iNewMessageTemplateID = @iNewMessageTemplateID + @iCnt
	
		INSERT INTO dbo.utb_bundling
		SELECT
			@iDocumentTypeID
			, @iNewMessageTemplateID
			, @iEnabledFlag
			, @vName
			, @iCreatedByUserID
			, @dtNow
			
			SET @iCnt = @iCnt + 1

			FETCH NEXT FROM db_cursor INTO 	
			@iDocumentTypeID, @iEnabledFlag, @vName
	END
	CLOSE db_cursor  
	DEALLOCATE db_cursor 		
	 
	IF EXISTS(
		SELECT 
			b.BundlingID
		FROM 
			utb_bundling b 
			INNER JOIN utb_client_bundling cb 
			ON b.BundlingId = cb.BundlingID 
		WHERE 
			cb.InsuranceCompanyID = @iNewInsuranceCompanyID
	)
	BEGIN
		SELECT 1 RetCode
	END
	ELSE
	BEGIN
		SELECT 0 RetCode
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewBundling' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateNewBundling TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateNewBundling TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/