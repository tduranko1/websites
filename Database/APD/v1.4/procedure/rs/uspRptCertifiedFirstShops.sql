-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptCertifiedFirstShops' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptCertifiedFirstShops 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptCertifiedFirstShops
* SYSTEM:       Lynx Services APD
* AUTHOR:       Madhavi Kotipalli
* FUNCTION:     Compiles data displayed on the Certified Assignment Reports
*
* PARAMETERS:
* (I) @RptMonth             The month the report is for
* (I) @RptYear               The year the report is for
*
* RESULT SET:
*   Pos1Header              The header name of column 1
*   Pos2Header              The header name of column 2
*   Pos3Header              The header name of column 3
*   Pos4Header              The header name of column 4
*   Pos5Header              The header name of column 5
*   Pos6Header              The header name of column 6 
*   Pos7Header              The header name of column 7
*   Pos8Header              The header name of column 8
*   Pos9Header              The header name of column 9
*   Pos10Header            The header name of column 10
*   Pos11Header            The header name of column 11
*   Pos12Header            The header name of column 12
*   Level2Display           The display name of the detail lines ( Certified First, Non-Certified First)
*   Level1Group             Display name for the insurance company header ( 'Assignments'),Estimate Dollars
*   Level2Group             'CertifiedFirst' or 'Non CertifiedFirst'
    Level3Group              Detail Line code ("CLS", "CCE", "NLS", "NCE")
*   Pos1Total                Total for column 1
*   Pos2Total                Total for column 2
*   Pos3Total                Total for column 3
*   Pos4Total                Total for column 4
*   Pos5Total                Total for column 5
*   Pos6Total                Total for column 6
*   Pos7Total                Total for column 7
*   Pos8Total                Total for column 8
*   Pos9Total                Total for column 9
*   Pos10Total              Total for column 10
*   Pos11Total              Total for column 11
*   Pos12Total              Total for column 12
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

CREATE PROCEDURE dbo.uspRptCertifiedFirstShops
    @RptMonth               udt_std_int_small = NULL,
    @RptYear                udt_std_int_small = NULL
AS
BEGIN
    -- Set database options

    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

    -- Declare Local variables


   DECLARE @tmpRowCount  int

    DECLARE @tmpAssignmentData TABLE  -- ( This table contains all the data related to Certified LynxSelect & CEI )
    (
        Level1Group                 varchar(20)  NOT  NULL, -- (  Assignment, Estimate Dollars  )
        Level2Group                 varchar(20)   NOT NULL, -- (  Certified First, Non-Certified First )
        Level3Group                 varchar(20)   NOT NULL,  -- ( Lynx Select, CEI, Total )
        Pos1Total                   bigint       NULL,
        Pos2Total                   bigint       NULL,
        Pos3Total                   bigint       NULL,
        Pos4Total                   bigint        NULL,
        Pos5Total                   bigint       NULL,
        Pos6Total                   bigint       NULL,
        Pos7Total                   bigint       NULL,
        Pos8Total                   bigint       NULL,
        Pos9Total                   bigint       NULL,
        Pos10Total                  bigint       NULL,
        Pos11Total                  bigint       NULL,
        Pos12Total                  bigint       NULL,
        Total                              bigint      NULL
    )


   
    DECLARE @tmpEstimateData TABLE  -- ( This table contains all the data related to Certified LynxSelect & CEI )
    (
        Level1Group                 varchar(20)  NOT  NULL, 
   Level2Group                 varchar(20)   NOT NULL, 
        Level3Group                 varchar(20)   NOT NULL,  
        Pos1Total                   money       NULL,
        Pos2Total                   money       NULL,
        Pos3Total                   money       NULL,
        Pos4Total                   money       NULL,
        Pos5Total                   money       NULL,
        Pos6Total                   money       NULL,
        Pos7Total                  money      NULL,
        Pos8Total                   money       NULL,
        Pos9Total                   money       NULL,
        Pos10Total                  money       NULL,
        Pos11Total                  money       NULL,
        Pos12Total                  money       NULL,
        Total 		    money     NULL
    )




    DECLARE @tmpClosedData TABLE
    (
        Level1Group varchar(20)  NOT NULL,   --( Level1Group  refers to either Assignments group or Estimate Dollars group
        LineItemCD          varchar(4)   NULL,      --( level2group : CLS,CCE,NLS,NCE)
        PositionID                  tinyint          NULL, --( 1, 2, 3, 4,...12)
        MonthValue               tinyint          NULL,
        YearValue                 smallint        NULL,
        ClosedTotal               int               NULL
    )



    DECLARE @tmpClosedMoney TABLE
    (
        Level1Group varchar(20)  NOT NULL,      --(  Level1Griup refers to either Assignments group or Estimate Dollars group
        LineItemCD                varchar(4)   NULL,   --(  level2group : CLS,CCE,NLS,NCE)
        PositionID                  tinyint          NULL,    --( 1, 2, 3, 4,...12)
        MonthValue               tinyint          NULL,
        YearValue                 smallint        NULL,
        ClosedTotal               money               NULL
    )




    DECLARE @tmpTimePeriods TABLE
    (
        PositionID                  tinyint              NULL,
        TimePeriod                varchar(8)   NULL,
        MonthValue               tinyint          NULL,
        YearValue                 smallint        NULL
    )


    DECLARE @LoopIndex                 udt_std_int_tiny
    DECLARE @MonthName              udt_std_name
    DECLARE @Debug                      udt_std_flag
    DECLARE @now                          udt_std_datetime
    DECLARE @DataWarehouseDate    udt_std_datetime
    
    DECLARE @ProcName   varchar(30)
    SET @ProcName = 'uspRptCertifiedFirstShops'
    SET @Debug = 0

    -- Get Current timestamp

    SET @now = CURRENT_TIMESTAMP
    SELECT  @DataWarehouseDate = MAX(dt.DateValue)  from dbo.utb_dtwh_dim_time dt  


    -- Check Report Date fields and set as necessary

    IF @RptMonth is NULL
    BEGIN
        SET @RptMonth = Month(@now)
    END

    IF @RptYear is NULL
    BEGIN
        SET @RptYear = Year(@now)
    END

    -- Validate the Report Month and Year

    IF (@RptYear < 2002) OR (@RptYear > DatePart(yyyy, @now))
    BEGIN
        -- Invalid Report Year

        RAISERROR  ('%s: @RptYear must be between 2001 and the current year.', 16, 1, @ProcName)
        RETURN
    END


    IF @RptMonth < 1 OR @RptMonth > 12
    BEGIN
        -- Invalid Report Month

        RAISERROR  ('101|%s|@RptMonth|%n', 16, 1, @ProcName, @RptMonth)
        RETURN
    END
    ELSE
    BEGIN
        SET @MonthName = DateName(mm, Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))
    END


    -- Compile time periods

    SET @LoopIndex = 12

    WHILE @LoopIndex > 0
    BEGIN
        INSERT INTO @tmpTimePeriods
          SELECT 13 - @LoopIndex AS Position,
                 Convert(varchar(2), DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))))
                    + '/'
                    + Convert(varchar(4), DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))),
                 DatePart(mm, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear))),
                 DatePart(yyyy, DateAdd(mm, -1 * (@LoopIndex -1), Convert(varchar(2), @RptMonth) + '/1/' + Convert(varchar(4), @RptYear)))

        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure

            RAISERROR('105|%s|@tmpTimePeriods', 16, 1, @ProcName)
            RETURN
        END

        SET @LoopIndex = @LoopIndex - 1
    END


    IF @Debug = 1
    BEGIN
        PRINT '@RptMonth = ' + Convert(varchar, @RptMonth)
        PRINT '@RptYear = ' + Convert(varchar, @RptYear)
    END



   INSERT INTO @tmpClosedData
       SELECT ALL 'Assignment',  'CLS',   -- Certified First: Lynx Select
              tmpTP.PositionID,
              dt.MonthofYear,
              dt.YearValue,
              Count(*) AS ClosedTotal
        FROM  dbo.utb_dtwh_fact_claim fc
       LEFT JOIN dbo.utb_dtwh_dim_time dt ON ( fc.TimeIDClosed = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON ( Convert(varchar(2), dt.MonthOFYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod)
        WHERE   ( fc.CFProgramFlag =1     AND  ProgramCD='LS'  AND fc.EnabledFlag = 1 ) 
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END



   INSERT INTO @tmpClosedData
      SELECT ALL 'Assignment',  'CCE',   -- Certified First: CEI
             tmpTP.PositionID,
             dt.MonthofYear,
             dt.YearValue,
              Count(*) AS ClosedTotal
        FROM  dbo.utb_dtwh_fact_claim fc
       LEFT JOIN dbo.utb_dtwh_dim_time dt ON ( fc.TimeIDClosed = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON ( Convert(varchar(2), dt.MonthOFYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod)
        WHERE   ( fc.CFProgramFlag = 1 AND fc.ProgramCD = 'CEI' AND fc.EnabledFlag = 1 ) 
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END


INSERT INTO @tmpAssignmentData
      SELECT   ALL 'Assignment',  'CertifiedFirst',
              LineItemCD,
	     SUM(CASE PositionID WHEN 1 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedTotal ELSE 0 END),
             SUM(CASE PositionID WHEN 4 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedTotal ELSE 0 END),
	0  -- Totals
        FROM  @tmpClosedData
        GROUP BY Level1Group,LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpAssignmentData', 16, 1, @ProcName)
        RETURN
    END



SELECT @tmpRowCount = Count(*)
    FROM @tmpClosedData
   WHERE LineItemCD = 'CLS' AND Level1Group= 'Assignment'

  IF @tmpRowCount = 0   
     INSERT INTO @tmpAssignmentData  VALUES ('Assignment', 'CertifiedFirst', 'CLS',
              0,0,0,0,0,0,0,0,0,0,0,0,0   )


      IF @@ERROR <> 0
       BEGIN
           -- Insertion failure

          RAISERROR('105|%s|@tmpAssignmentData', 16, 1, @ProcName)
          RETURN
      END


SELECT @tmpRowCount = Count(*)
    FROM @tmpClosedData
   WHERE LineItemCD = 'CCE' AND Level1Group = 'Assignment'

  IF @tmpRowCount = 0   
     INSERT INTO @tmpAssignmentData  VALUES ('Assignment', 'CertifiedFirst', 'CCE',
              0,0,0,0,0,0,0,0,0,0,0,0,0    )


       IF @@ERROR <> 0
       BEGIN
           -- Insertion failure

          RAISERROR('105|%s|@tmpAssignmentData', 16, 1, @ProcName)
          RETURN
       END


   DELETE from @tmpClosedData
    IF @@ERROR <> 0
    BEGIN
        -- Delete failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END

   

  

-------------------------------------------------------------------------------------------------------------------------


  INSERT INTO @tmpClosedData
      SELECT ALL  'Assignment', 'NLS',   -- Non Certified First: Lynx Select
              tmpTP.PositionID,
              dt.MonthofYear,
              dt.YearValue,
              Count(*) AS ClosedTotal
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON ( fc.TimeIDClosed = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON ( Convert(varchar(2), dt.MonthOFYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod)
        WHERE  ( fc.CFProgramFlag = 0  AND fc.ProgramCD = 'LS' AND fc.EnabledFlag = 1 )
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END




  INSERT INTO @tmpClosedData
     SELECT ALL  'Assignment','NCE',   -- Non Certified First: CEI
            tmpTP.PositionID,
            dt.MonthofYear,
            dt.YearValue,
             Count(*) AS ClosedTotal
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON ( fc.TimeIDClosed = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON ( Convert(varchar(2), dt.MonthOFYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod)
       WHERE   ( fc.CFProgramFlag = 0   AND  fc.ProgramCD = 'CEI' AND fc.EnabledFlag = 1 ) 
        GROUP BY  tmpTP.PositionID, dt.MonthOfYear, dt.YearValue


    IF @@ERROR <> 0
   BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName)
      RETURN
 END


INSERT INTO @tmpAssignmentData
      SELECT ALL 'Assignment', 'Non CertifiedFirst',
              LineItemCD,
              SUM(CASE PositionID WHEN 1 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedTotal ELSE 0 END),
              0
        FROM  @tmpClosedData
        GROUP BY Level1Group,LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpAssignmentData', 16, 1, @ProcName)
        RETURN
    END


   UPDATE @tmpAssignmentData
        SET Total = ( Pos1Total + Pos2Total + Pos3Total + Pos4Total + Pos5Total + Pos6Total + Pos7Total + Pos8Total + 
           Pos9Total + Pos10Total + Pos11Total + Pos12Total)

    IF @@ERROR <> 0
    BEGIN
        -- updation failure

        RAISERROR('105|%s|@tmpAssignmentData', 16, 1, @ProcName)
        RETURN
    END



SELECT @tmpRowCount = Count(*)
    FROM @tmpClosedData
   WHERE LineItemCD = 'NLS' AND Level1Group ='Assignment'

  IF @tmpRowCount = 0   
     INSERT INTO @tmpAssignmentData  VALUES ( 'Assignment', 'Non CertifiedFirst', 'NLS',
              0,0,0,0,0,0,0,0,0,0,0,0 ,0  )


     IF @@ERROR <> 0
      BEGIN
       -- Insertion failure

          RAISERROR('105|%s|@tmpAssignmentData', 16, 1, @ProcName)
          RETURN
     END


SELECT @tmpRowCount = Count(*)

    FROM @tmpClosedData
   WHERE LineItemCD = 'NCE' AND Level1Group = 'Assignment'

  IF @tmpRowCount = 0   
     INSERT INTO @tmpAssignmentData VALUES ( 'Assignment', 'Non CertifiedFirst', 'NCE',
              0,0,0,0,0,0,0,0,0,0,0,0 ,0  )


    IF @@ERROR <> 0
    BEGIN
    -- Insertion failure

          RAISERROR('105|%s|@tmpAssignmentData', 16, 1, @ProcName)
          RETURN
    END
  
	

   DELETE from @tmpClosedData
    IF @@ERROR <> 0
    BEGIN
        -- Deletion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName)
        RETURN
    END

  


-------------------------------------------------------------------------------------------------------------------------


   INSERT INTO @tmpClosedMoney
       SELECT ALL 'Estimate Dollars',  'CLS',   -- Certified First: Lynx Select
              tmpTP.PositionID,
              dt.MonthofYear,
          dt.YearValue,
              Sum(fc.FinalEstimateGrossAmt) AS ClosedTotal
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON ( fc.TimeIDClosed = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON ( Convert(varchar(2), dt.MonthOFYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod)
        WHERE ( fc.CFProgramFlag = 1 AND fc.ProgramCD = 'LS' AND fc.EnabledFlag = 1 ) 
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedMoney', 16, 1, @ProcName)
        RETURN
    END

INSERT INTO @tmpClosedMoney
      SELECT ALL 'Estimate Dollars',  'CCE',   -- Certified First: CEI
             tmpTP.PositionID,
             dt.MonthofYear,
             dt.YearValue,
              Sum(fc.FinalEstimateGrossAmt) AS ClosedTotal
        FROM  dbo.utb_dtwh_fact_claim fc
       LEFT JOIN dbo.utb_dtwh_dim_time dt ON ( fc.TimeIDClosed = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON ( Convert(varchar(2), dt.MonthOFYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod)
        WHERE  ( fc.CFProgramFlag = 1 AND fc.ProgramCD ='CEI' AND fc.EnabledFlag = 1 ) 
        GROUP BY tmpTP.PositionID, dt.MonthOfYear, dt.YearValue


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedMoney', 16, 1, @ProcName)
        RETURN
    END

INSERT INTO @tmpEstimateData
      SELECT   ALL 'Estimate Dollars',  'CertifiedFirst',
              LineItemCD,
              SUM(CASE PositionID WHEN 1 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedTotal ELSE 0 END),
              0
        FROM  @tmpClosedMoney
        GROUP BY Level1Group,LineItemCD

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpEstimateData', 16, 1, @ProcName)
        RETURN
    END


SELECT @tmpRowCount = Count(*)
    FROM @tmpClosedMoney
   WHERE LineItemCD = 'CLS' AND Level1Group= 'Estimate Dollars'

  IF @tmpRowCount = 0   
     INSERT INTO @tmpEstimateData  VALUES ('Estimate Dollars', 'CertifiedFirst', 'CLS',
              0,0,0,0,0,0,0,0,0,0,0,0,0    )


            IF @@ERROR <> 0
            BEGIN
                -- Insertion failure

                  RAISERROR('105|%s|@tmpEstimateData', 16, 1, @ProcName)
                  RETURN
           END



SELECT @tmpRowCount = Count(*)
    FROM @tmpClosedMoney
   WHERE LineItemCD = 'CCE' AND Level1Group = 'Estimate Dollars'

  IF @tmpRowCount = 0  
     INSERT INTO @tmpEstimateData  VALUES ('Estimate Dollars', 'CertifiedFirst', 'CCE',
             0,0,0,0,0,0,0,0,0,0,0,0 ,0   )


            IF @@ERROR <> 0
            BEGIN
                -- Insertion failure

                  RAISERROR('105|%s|@tmpEstimateData', 16, 1, @ProcName)
                  RETURN
           END





           DELETE from @tmpClosedMoney
            IF @@ERROR <> 0
            BEGIN
              -- Deletion failure

                  RAISERROR('105|%s|@tmpClosedMoney', 16, 1, @ProcName)
                  RETURN
           END

        

-------------------------------------------------------------------------------------------------------------------------

   INSERT INTO @tmpClosedMoney
     SELECT ALL  'Estimate Dollars','NLS',   -- Non Certified First: Lynx Select 
            tmpTP.PositionID,
            dt.MonthofYear,
            dt.YearValue,
             Sum(fc.FinalEstimateGrossAmt) AS ClosedTotal
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON ( fc.TimeIDClosed = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON ( Convert(varchar(2), dt.MonthOFYear) + '/' + Convert(varchar(4),  dt.YearValue) = tmpTP.TimePeriod)
       WHERE  ( fc.CFProgramFlag = 0  AND fc.ProgramCD = 'LS' AND fc.EnabledFlag = 1 )
        GROUP BY  tmpTP.PositionID, dt.MonthOfYear, dt.YearValue


    IF @@ERROR <> 0
   BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedMoney', 16, 1, @ProcName)
      RETURN
 END



   INSERT INTO @tmpClosedMoney
     SELECT ALL  'Estimate Dollars','NCE',   -- Non Certified First: CEI
            tmpTP.PositionID,
            dt.MonthofYear,
            dt.YearValue,
        Sum(fc.FinalEstimateGrossAmt) AS ClosedTotal
        FROM  dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dt ON ( fc.TimeIDClosed = dt.TimeID)
        INNER JOIN @tmpTimePeriods tmpTP ON ( Convert(varchar(2), dt.MonthOFYear) + '/' + Convert(varchar(4), dt.YearValue) = tmpTP.TimePeriod)
       WHERE  ( fc.CFProgramFlag = 0 AND fc.ProgramCD = 'CEI' AND fc.EnabledFlag = 1 )
        GROUP BY  tmpTP.PositionID, dt.MonthOfYear, dt.YearValue


   IF @@ERROR <> 0
   BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpClosedData', 16, 1, @ProcName)
      RETURN
 END



INSERT INTO @tmpEstimateData
      SELECT ALL 'Estimate Dollars', 'Non CertifiedFirst',
              LineItemCD,
              SUM(CASE PositionID WHEN 1 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 2 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 3 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 4 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 5 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 6 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 7 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 8 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 9 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 10 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 11 THEN ClosedTotal ELSE 0 END),
              SUM(CASE PositionID WHEN 12 THEN ClosedTotal ELSE 0 END),
              0
        FROM  @tmpClosedMoney
        GROUP BY Level1Group,LineItemCD


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

       RAISERROR('105|%s|@tmpAssignmentData', 16, 1, @ProcName)
        RETURN
    END


   UPDATE @tmpEstimateData
        SET Total = ( Pos1Total + Pos2Total + Pos3Total + Pos4Total + Pos5Total + Pos6Total + Pos7Total + Pos8Total + 
                             Pos9Total + Pos10Total + Pos11Total + Pos12Total)

    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpEstimateData', 16, 1, @ProcName)
        RETURN
    END



SELECT @tmpRowCount = Count(*)
    FROM @tmpClosedMoney
   WHERE LineItemCD = 'NLS' AND Level1Group ='Estimate Dollars'

  IF @tmpRowCount = 0   
     INSERT INTO @tmpEstimateData  VALUES ( 'Estimate Dollars', 'Non CertifiedFirst', 'NLS',
              0,0,0,0,0,0,0,0,0,0,0,0,0  )




    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpEstimateData', 16, 1, @ProcName)
        RETURN
    END



SELECT @tmpRowCount = Count(*)
    FROM @tmpClosedMoney
   WHERE LineItemCD = 'NCE' AND Level1Group = 'Estimate Dollars'


  IF @tmpRowCount = 0 
     INSERT INTO @tmpEstimateData  VALUES ( 'Estimate Dollars', 'Non CertifiedFirst', 'NCE',
              0,0,0,0,0,0,0,0,0,0,0,0,0  )


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure

        RAISERROR('105|%s|@tmpEstimateData_Final', 16, 1, @ProcName)
        RETURN
    END



    DELETE from @tmpClosedMoney
    IF @@ERROR <> 0
    BEGIN
        -- Deletion failure

        RAISERROR('105|%s|@tmpClosedMoney', 16, 1, @ProcName)
        RETURN
    END


-------------------------------------------------------------------------------------------------------------------------


SELECT
            @MonthName + ' ' + Convert(varchar(4),
          @RptYear) AS ReportMonth,
   ---         'Assignment Report' AS ServiceGroup, --Assignment  Data
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
      FROM  @tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
            CASE tmp.Level3Group
              WHEN 'CLS' THEN 'Lynx Select'
              WHEN 'CCE' THEN 'CEI'
              WHEN 'NLS' THEN 'Lynx Select'
              WHEN 'NCE' THEN 'CEI'
              WHEN 'Total' THEN 'Total'
            END AS Level3Display,
            tmp.*,
            @DataWareHouseDate as DataWareDate
      FROM  @tmpAssignmentData tmp  --  group by tmp.Level3Group

-- tmp.Level2Group, 


 UNION ALL



SELECT
            @MonthName + ' ' + Convert(varchar(4),
          @RptYear) AS ReportMonth,
   ---         'Assignment Report' AS ServiceGroup, --Assignment  Data
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 1) AS Pos1Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 2) AS Pos2Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 3) AS Pos3Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 4) AS Pos4Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 5) AS Pos5Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 6) AS Pos6Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
         FROM  @tmpTimePeriods
               WHERE PositionID = 7) AS Pos7Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 8) AS Pos8Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 9) AS Pos9Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 10) AS Pos10Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 11) AS Pos11Header,
            (SELECT  Left(DateName(mm, Convert(varchar(2), MonthValue) + '/1/' + Convert(varchar(4), YearValue)), 3) + '-' + Right(Convert(varchar(4), YearValue), 2)
               FROM  @tmpTimePeriods
               WHERE PositionID = 12) AS Pos12Header,
        CASE tmp.Level3Group
              WHEN 'CLS' THEN 'Lynx Select'
              WHEN 'CCE' THEN 'CEI'
              WHEN 'NLS' THEN 'Lynx Select'
              WHEN 'NCE' THEN 'CEI'
              WHEN 'Total' THEN 'Total'
            END AS Level3Display,
            tmp.*,
            @DataWareHouseDate as DataWareDate
      FROM  @tmpEstimateData tmp    --group by Level3Group



 
   IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptCertifiedFirstShops' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptCertifiedFirstShops TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/