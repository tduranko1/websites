-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetFaxTemplates' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetFaxTemplates 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetFaxTemplates
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets all fax templates by InsuranceCompanyID
*
* PARAMETERS:  
*				InsuranceCompanyID
* RESULT SET:
*   All data related to insurance clients
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetFaxTemplates
	@iInsuranceCompanyID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT
		f.FormID
		, dt.[Name] AS DocType
		, f.[Name]
		, f.EnabledFlag
		, f.PDFPath
		, f.PertainsToCD
		, ISNULL(ServiceChannelCD,'') AS ServiceChannelCD
		, ISNULL(f.SQLProcedure,'') AS SQLProcedure
		, f.SysLastUpdatedDate
	FROM
		utb_form f
		INNER JOIN utb_document_type dt
		ON dt.DocumentTypeID = f.DocumentTypeID
	WHERE
		InsuranceCompanyID = @iInsuranceCompanyID
	ORDER BY
		f.[Name]
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetFaxTemplates' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetFaxTemplates TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetFaxTemplates TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/