-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspQCRWeeklySummary' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspQCRWeeklySummary 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspQCRWeeklySummary
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Generates a recordset of QCR weekly work status
*
* PARAMETERS:  
* (I) @UserID         The QCR User ID
*
* RESULT SET:
* A Recordset
*
* Additional Notes:
*
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspQCRWeeklySummary
    @UserID   udt_std_id_big
AS
BEGIN
    -- Set database options


    SET NOCOUNT ON

    DECLARE @StartDate as datetime
    DECLARE @EndDate as datetime

    SET @EndDate = CONVERT(datetime, CONVERT(varchar, CURRENT_TIMESTAMP, 110))
    SET @EndDate = DATEADD(day, -1, @EndDate)
    SET @StartDate = DATEADD(week, -1, @EndDate)
    SET @EndDate = DATEADD(second, -1, @EndDate)

    --SET @UserID = 3793

    DECLARE @tmpClaims TABLE (
        LynxID              varchar(10),
        AssignmentType		 varchar(50),
        InsuranceCompany    varchar(50),
        Office              varchar(50),
        CarrierRep          varchar(50),
        LynxRep             varchar(50),
        RawOriginalAmount   decimal(9, 2), 
        OriginalAmount      decimal(9, 2), 
        AuditAmount         decimal(9, 2),
        AuditSavings        decimal(9, 2),
        AuditSavingsPercent decimal(9, 2),
        DateReceived        datetime,
        OriginalDateClosed  datetime
    )

    INSERT INTO @tmpClaims
    SELECT  NULL,
            ' Report from ' + convert(varchar, @StartDate, 101) + ' to ' + convert(varchar, @EndDate, 101),
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL

    INSERT INTO @tmpClaims
    SELECT  convert(varchar, ca.LynxID) + '-' + convert(varchar, ca.ClaimAspectNumber),
			   at.Name,
            i.Name as 'Insurance Company',
            o.ClientOfficeId as 'Client Office',
            cu.NameFirst + ' ' + cu.NameLast as 'Carrier Rep',
            au.NameFirst + ' ' + au.NameLast as 'LYNX Analyst',
            fc.OriginalEstimatewoBettAmt,
            fc.OriginalEstimatewoBettAmt as 'Original Est.',  
            fc.FinalAuditedSuppwoBettAmt as 'Audited Est.', 
            CASE
                WHEN fc.OriginalEstimatewoBettAmt > 0 AND fc.AuditedEstimatewoBettAmt <= fc.OriginalEstimatewoBettAmt THEN isNull(fc.OriginalEstimatewoBettAmt, 0) - isNull(fc.AuditedEstimatewoBettAmt, 0)
                ELSE 0.00
            END as 'Audit Savings',
            CASE
                WHEN fc.OriginalEstimatewoBettAmt > 0 AND fc.AuditedEstimatewoBettAmt <= fc.OriginalEstimatewoBettAmt THEN ((isNull(fc.OriginalEstimatewoBettAmt, 0) - isNull(fc.AuditedEstimatewoBettAmt, 0)) / fc.OriginalEstimatewoBettAmt) * 100.00
                ELSE 0.00
            END 'Audit Savings Percent',
            convert(varchar, c.IntakeFinishDate, 101) as 'Claim Rcvd. On',
            convert(varchar, casc.OriginalCompleteDate, 101) as 'Claim Closed On'
    FROM utb_claim_aspect ca
    LEFT JOIN utb_claim c ON ca.LynxID = c.LynxID
    LEFT JOIN utb_claim_aspect_service_channel casc ON ca.ClaimAspectID = casc.ClaimAspectID
    LEFT JOIN utb_dtwh_fact_claim fc ON ca.ClaimAspectID = fc.ClaimAspectID
    LEFT JOIN utb_insurance i ON c.InsuranceCompanyID = i.InsuranceCompanyID
    LEFT JOIN utb_user cu ON c.CarrierRepUserID = cu.UserID
    LEFT JOIN utb_office o ON cu.OfficeID = o.OfficeID
    LEFT JOIN utb_user au ON ca.AnalystUserID = au.UserID
    LEFT JOIN utb_assignment_type at ON ca.InitialAssignmentTypeID = at.AssignmentTypeID
    WHERE AnalystUserID = @UserID
      and casc.OriginalCompleteDate between @StartDate and @EndDate
    order by at.Name, c.IntakeFinishDate, ca.LynxID

    INSERT INTO @tmpClaims
    SELECT  NULL, 
            AssignmentType + ' Summary',
            NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 
            AVG(AuditSavingsPercent), 
            NULL, NULL
    FROM @tmpClaims
    WHERE RawOriginalAmount IS NOT NULL
    GROUP BY AssignmentType

    SELECT  isNull(LynxID, '') as LynxID, 
			   isNull(AssignmentType, '') as 'Assignment Type',
            isNull(InsuranceCompany, '') as InsuranceCompany,
            isNull(Office, '') as Office,
            isNull(CarrierRep, '') as CarrierRep,
            isNull(LynxRep, '') as LynxRep,
            isNull(convert(varchar(10), OriginalAmount), '') as OriginalAmount, 
            isNull(convert(varchar(10), AuditAmount), '') as AuditAmount, 
            isNull(convert(varchar(10), AuditSavings), '') as AuditSavings, 
            isNull(convert(varchar(10), AuditSavingsPercent), '') as AuditSavingsPercent,
            isNull(convert(varchar, DateReceived, 101), '') as ClaimReceivedDate,
            isNull(convert(varchar, OriginalDateClosed, 101), '') as OriginalCompletionDate
    FROM @tmpClaims
    ORDER BY AssignmentType

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspQCRWeeklySummary' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspQCRWeeklySummary TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


