-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptDAElecEstControlReport' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptDAElecEstControlReport 
END

GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER OFF
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspRptDAElecEstControlReport
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     DA Electronic Estimates control report. This proc will return result sets for:
*               1. # of Desk Audits completed by client by LYNX rep 
*               2. # of Electronic estimates received from CCC in APD by client by LYNX rep 
*               3. # of Electronic estimates moved to the APD datawarehouse   

*
* PARAMETERS:  
*
* RESULT SET:
* Recordsets
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptDAElecEstControlReport
(
    @RptStartDate   datetime = NULL,
    @RptEndDate     datetime = NULL
)
AS
BEGIN

    SET NOCOUNT ON

    --DECLARE @RptStartDate as datetime
    --DECLARE @RptEndDate as datetime
    DECLARE @Yesterday as datetime
    DECLARE @InsCoNameWork as varchar(100)
    DECLARE @AnalystWork as varchar(100)
    DECLARE @warehouseCountWork as int
    DECLARE @rawCountWork as int
    DECLARE @ElecEstMissing  as int 

    DECLARE @ProcName as varchar(50)

    SET @Yesterday = DATEADD(day, -1, CURRENT_TIMESTAMP)

    IF @RptStartDate IS NULL
    BEGIN
        SET @RptStartDate = dateadd(day, -2, convert(datetime, convert(varchar, @Yesterday, 101)))
        SET @RptEndDate = convert(datetime, convert(varchar, @Yesterday, 101) + ' 23:59:59')
    END

    IF @RptEndDate IS NULL
    BEGIN
        SET @RptEndDate = convert(datetime, convert(varchar, @Yesterday, 101) + ' 23:59:59')
    END


    --SET @RptStartDate = convert(datetime, '5/1/2008')
    --SET @RptEndDate = convert(datetime, '4/1/2008 23:59:59')

    --select @RptStartDate, @RptEndDate

    DECLARE @tmpDACompleted TABLE
    (
        Client              varchar(60),
        Analyst             varchar(40),
        LynxID              varchar(11),
        ElecEstCount        int
    )

    DECLARE @tmpDASummary TABLE
    (
        Client              varchar(60),
        Analyst             varchar(40),
        DACompleted         int,
        ElecEstCount        int,
        ElecEstMissing      int,
        warehouseCount      int
    )

    /*-- # of Desk Audits completed by client by LYNX rep 
    print 'Desk Audits completed [' + convert(varchar, @RptStartDate, 101) + ' ' + convert(varchar, @RptStartDate, 108) + ' to ' + convert(varchar, @RptEndDate, 101) + ' ' + convert(varchar, @RptEndDate, 108)+ ']'
    print '------------------------------------------------------------------'
    print ' '*/

    INSERT INTO @tmpDACompleted
    (Client, Analyst, LynxID, ElecEstCount)
    SELECT  substring(i.Name, 1, 60), 
            substring(u.NameFirst + ' ' + u.NameLast, 1, 40), 
            substring(convert(varchar, c.LynxID) + '-' + convert(varchar, ca.ClaimAspectNumber), 1, 11),
            (select count(d.DocumentID)
               from utb_document d
               left join utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
               LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
               LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
             where cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
               and ds.VANFlag = 1
               and dt.EstimateTypeFlag = 1
               and d.EnabledFlag = 1
            )
    FROM utb_claim_aspect_service_channel casc 
    LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN utb_claim c on ca.LynxID = c.LynxID
    LEFT JOIN utb_claim_aspect_status cas on ca.ClaimAspectID = cas.ClaimAspectID and casc.ServiceChannelCD = cas.ServiceChannelCD
    LEFT JOIN utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
    LEFT JOIN utb_user u on ca.AnalystUserID = u.UserID
    WHERE cas.StatusID = 199
      AND casc.ServiceChannelCD = 'DA'
      AND cas.StatusStartDate between @RptStartDate and @RptEndDate
    order by 1, 2

    /*SELECT Client, Analyst, LynxID
    FROM @tmpDACompleted


    print '------'
    print ' '
    print ' '

    print '# of Desk Audits completed by client by LYNX rep [' + convert(varchar, @RptStartDate, 101) + ' ' + convert(varchar, @RptStartDate, 108) + ' to ' + convert(varchar, @RptEndDate, 101) + ' ' + convert(varchar, @RptEndDate, 108)+ ']'
    print '---------------------------------------------------------------------------------------------'
    print ' '*/

    /*SELECT substring(i.Name, 1, 60) as 'Client', substring(u.NameFirst + ' ' + u.NameLast, 1, 40) as 'Analyst', count(casc.ClaimAspectServiceChannelID) as 'Completed Count'
    FROM utb_claim_aspect_service_channel casc 
    LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN utb_claim c on ca.LynxID = c.LynxID
    LEFT JOIN utb_claim_aspect_status cas on ca.ClaimAspectID = cas.ClaimAspectID and casc.ServiceChannelCD = cas.ServiceChannelCD
    LEFT JOIN utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
    LEFT JOIN utb_user u on ca.AnalystUserID = u.UserID
    WHERE cas.StatusID = 199
      AND casc.ServiceChannelCD = 'DA'
      AND cas.StatusStartDate between @RptStartDate and @RptEndDate
    group by substring(i.Name, 1, 60), substring(u.NameFirst + ' ' + u.NameLast, 1, 40)
    order by 1, 2*/

    INSERT INTO @tmpDASummary
    (Client, Analyst, DACompleted)
    SELECT  Client,
            Analyst,
            Count(*) as 'DA Completed'
    FROM @tmpDACompleted
    GROUP BY Client, Analyst
    order by 1, 2

    /*SELECT Client, Analyst, DACompleted as 'DA Completed'
    FROM @tmpDASummary*/

    /*print '------'
    print ' '
    print ' '*/

    /*-- # of Electronic estimates received from CCC in APD by client by LYNX rep 
    print '# of Electronic estimates received from CCC in APD by client by LYNX rep [Estimates received between ' + convert(varchar, @RptStartDate, 101) + ' ' + convert(varchar, @RptStartDate, 108) + ' to ' + convert(varchar, @RptEndDate, 101) + ' ' + convert(varchar, @RptEndDate, 108)+ ']'
    print '------------------------------------------------------------------------------------------------------------------------------------------------'
    print ' '*/

    DECLARE @tmpDAElecEstimates TABLE
    (
        tmpDocumentID       bigint NOT NULL,
        InsCoName           varchar(60),
        Analyst             varchar(40),
        InWareHouse         bit DEFAULT 0
    )

    /*DECLARE @tmpDACheck TABLE
    (
        Client              varchar(60),
        Analyst             varchar(40),
        rawCount            int,
        warehouseCount      int
    )*/

    INSERT INTO @tmpDAElecEstimates
    (tmpDocumentID, InsCoName, Analyst)
    SELECT  d.DocumentID,
            substring(i.Name, 1, 60) as 'Client', 
            substring(u.NameFirst + ' ' + u.NameLast, 1, 40) as 'Analyst'
    FROM utb_document d
    LEFT JOIN utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
    LEFT JOIN utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEFT JOIN utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
    LEFT JOIN utb_claim c on ca.LynxID = c.LynxID
    LEFT JOIN utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
    LEFT JOIN utb_user u on ca.AnalystUserID = u.UserID
    LEFT JOIN utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
    LEFT JOIN utb_document_type dt on d.DocumentTypeID = dt.DocumentTypeID
    WHERE ds.VANFlag = 1
      AND dt.EstimateTypeFlag = 1
      AND d.CreatedDate between @RptStartDate and @RptEndDate
      AND d.EnabledFlag = 1
      AND casc.ServiceChannelCD = 'DA'

    /*INSERT INTO @tmpDACheck
    (Client, Analyst, rawCount)
    select  InsCoName as 'Client',
            Analyst,
            count(tmpDocumentID)
    from @tmpDAElecEstimates
    group by InsCoName, Analyst
    order by 1, 2

    select  Client,
            Analyst as 'Analyst',
            rawCount as 'Elec. Estimates'
    from @tmpDACheck

    print '------'
    print ' '
    print ' '

    -- # of Electronic estimates moved to the APD datawarehouse   

    print '# of Electronic estimates moved to the APD datawarehouse'
    print '--------------------------------------------------------'
    print ' '*/

    UPDATE @tmpDAElecEstimates
    SET InWareHouse = 1
    FROM dbo.utb_dtwh_fact_estimate
    WHERE tmpDocumentID = DocumentID


    DECLARE csrCheck CURSOR FOR
        SELECT Client, Analyst
        FROM @tmpDASummary

    OPEN csrCheck

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    FETCH NEXT FROM csrCheck
      INTO @InsCoNameWork,
           @AnalystWork

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @rawCountWork = 0
        SET @warehouseCountWork = 0
        SET @ElecEstMissing = 0

        SELECT @rawCountWork = COUNT(tmpDocumentID)
        FROM @tmpDAElecEstimates
        WHERE InsCoName = @InsCoNameWork
          AND Analyst = @AnalystWork
        GROUP BY InsCoName, Analyst
        
        SELECT @ElecEstMissing = COUNT(*)
        FROM @tmpDACompleted
        WHERE Client = @InsCoNameWork
          AND Analyst = @AnalystWork
          AND ElecEstCount = 0
        GROUP BY Client, Analyst
        
        
        SELECT @warehouseCountWork = COUNT(tmpDocumentID)
        FROM @tmpDAElecEstimates
        WHERE InsCoName = @InsCoNameWork
          AND Analyst = @AnalystWork
          AND inWarehouse = 1
        GROUP BY InsCoName, Analyst

        UPDATE @tmpDASummary
        SET warehouseCount = @warehouseCountWork,
            ElecEstCount = @rawCountWork,
            ElecEstMissing = @ElecEstMissing
        WHERE Client = @InsCoNameWork
          AND Analyst = @AnalystWork

        FETCH NEXT FROM csrCheck
          INTO @InsCoNameWork,
               @AnalystWork

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('99|%s', 16, 1, @ProcName)
            RETURN
        END
    END

    CLOSE csrCheck
    DEALLOCATE csrCheck

    /*SELECT  Client, 
            Analyst, 
            rawCount as '# of Desk Audit completed', 
            warehouseCount,
            (rawCount - warehouseCount) as 'missing'
    FROM @tmpDACheck*/

    print 'Desk Audit Control Report for ' + convert(varchar, @RptStartDate, 101) + ' ' + convert(varchar, @RptStartDate, 108) + ' to ' + convert(varchar, @RptEndDate, 101) + ' ' + convert(varchar, @RptEndDate, 108)
    --print '----------------------------------------------------'
    print ''

    select  Client,
            Analyst,
            DACompleted as '# of Desk Audits Completed',
            ElecEstCount as '# of Electronic estimates rec''d from CCC in APD',
            DACompleted - ElecEstCount as 'Electronic Count missing',
            warehouseCount as '# of electronic moved to warehouse',
            ElecEstCount - warehouseCount as 'Warehouse count missing'
    from @tmpDASummary

    print ''
    print 'Summary'
    print '-------'
    print ''

    select  sum(DACompleted) as 'Total DA completed',
            sum(ElecEstCount) as 'Total Elec. Estimates rec''d from CCC in APD',
            sum(ElecEstMissing) as 'Total Elec. missing',
            sum(warehouseCount) as 'Total Elec. moved to warehouse',
            sum(ElecEstCount - warehouseCount) as 'Total warehouse missing',
            case
                when sum(DACompleted) > 0 then convert(decimal(4, 1), (sum(ElecEstCount) * 1.00 / sum(DACompleted)) * 100)
                else 0
            end as 'Percent received electronically',
            case 
                when sum(ElecEstCount) > 0 then convert(decimal(4, 1), (sum(warehouseCount) * 1.00 / sum(ElecEstCount)) * 100)
                else 0
            end as 'Percent of electronic moved to warehouse'

    from @tmpDASummary
    

    print '------'
    print ' '

    IF EXISTS(SELECT tmpDocumentID
                FROM @tmpDAElecEstimates
                WHERE inWarehouse = 0)
    BEGIN
        PRINT 'Missing Electronic DA Estimates in warehouse'
        PRINT '--------------------------------------------'
        print ' '

        SELECT  InsCoName as 'Client',
                Analyst,
                tmpDocumentID as 'DocumentID',
                cascd.SysLastUpdatedDate as 'Document Created Date',
                ca.LynxID,
                ca.ClaimAspectNumber,
                casc.ServiceChannelCD,
                convert(varchar(15), s.Name) as 'Status'
        FROM @tmpDAElecEstimates tmp
        left join utb_claim_aspect_service_channel_document cascd on tmp.tmpDocumentID = cascd.DocumentID
        left join utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
        left join utb_claim_aspect ca on casc.ClaimASpectID = ca.ClaimAspectID
        left join utb_claim_aspect_status cs on ca.ClaimAspectID = cs.ClaimAspectID and cs.ServiceChannelCD = casc.ServiceChannelCD and StatusTypeCD = 'SC'
        left join utb_status s on cs.StatusID = s.StatusID
        WHERE inWarehouse = 0

        PRINT '------'
        print ' '
    END

    print '------'
    print ' '

    IF EXISTS(SELECT LynxID
                FROM @tmpDACompleted
                WHERE ElecEstCount = 0)
    BEGIN
        PRINT 'Missing Electronic DA Estimates in APD'
        PRINT '--------------------------------------'
        print ' '

        SELECT Analyst,
               LynxID

        FROM @tmpDACompleted
        WHERE ElecEstCount = 0

        PRINT '------'
        print ' '
    END

    print '--- END OF REPORT ---'

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptDAElecEstControlReport' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptDAElecEstControlReport TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
