-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTPersonnelUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTPersonnelUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTPersonnelUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Update a Personnel record
*
* PARAMETERS:  
* (I) @PersonnelID                         The Personnel's unique identity
* (I) @PersonnelTypeID                     The Personnel's type
* (I) @PreferredContactMethodID            The Personnel's preferred contact method
* (I) @CellAreaCode                        The Personnel's cell area code
* (I) @CellExchangeNumber                  The Personnel's cell exchange number
* (I) @CellExtensionNumber                 The Personnel's cell extension number
* (I) @CellUnitNumber                      The Personnel's cell unit number
* (I) @EmailAddress                        The Personnel's email address
* (I) @FaxAreaCode                         The Personnel's fax area code
* (I) @FaxExchangeNumber                   The Personnel's fax exchange number
* (I) @FaxExtensionNumber                  The Personnel's fax extension number
* (I) @FaxUnitNumber                       The Personnel's fax unit number
* (I) @GenderCD                            The Personnel's gender cd
* (I) @MinorityFlag                        The Personnel's minority flag
* (I) @Name                                The Personnel's name
* (I) @PagerAreaCode                       The Personnel's pager area code
* (I) @PagerExchangeNumber                 The Personnel's pager exchange number
* (I) @PagerExtensionNumber                The Personnel's pager extension number
* (I) @PagerUnitNumber                     The Personnel's pager unit number
* (I) @PhoneAreaCode                       The Personnel's phone area code
* (I) @PhoneExchangeNumber                 The Personnel's phone exchange number
* (I) @PhoneExtensionNumber                The Personnel's phone extension number
* (I) @PhoneUnitNumber                     The Personnel's phone unit number
* (I) @ShopManagerFlag                     The Personnel's shop manager flag
* (I) @SysLastUserID                       The Personnel's updating user identity
* (I) @SysLastUpdatedDate                  The Personnel's last update date as string (VARCHAR(30))
*
* RESULT SET:
* PersonnelID                              The updated Personnel unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTPersonnelUpdDetail
(
	@PersonnelID                    	udt_std_int_big,
  @PersonnelTypeID                  udt_std_int_tiny,
  @PreferredContactMethodID         udt_std_int_tiny,
  @CellAreaCode                     udt_ph_area_code=NULL,
  @CellExchangeNumber               udt_ph_exchange_number=NULL,
  @CellExtensionNumber              udt_ph_extension_number=NULL,
  @CellUnitNumber                   udt_ph_unit_number=NULL,
  @EmailAddress                     udt_web_email=NULL,
  @FaxAreaCode                      udt_ph_area_code=NULL,
  @FaxExchangeNumber                udt_ph_exchange_number=NULL,
  @FaxExtensionNumber               udt_ph_extension_number=NULL,
  @FaxUnitNumber                    udt_ph_unit_number=NULL,
  @GenderCD                         udt_per_gender_cd=NULL,
  @MinorityFlag                     udt_std_flag=0,
  @Name                             udt_std_name,
  @PagerAreaCode                    udt_ph_area_code=NULL,
  @PagerExchangeNumber              udt_ph_exchange_number=NULL,
  @PagerExtensionNumber             udt_ph_extension_number=NULL,
  @PagerUnitNumber                  udt_ph_unit_number=NULL,
  @PhoneAreaCode                    udt_ph_area_code=NULL,
  @PhoneExchangeNumber              udt_ph_exchange_number=NULL,
  @PhoneExtensionNumber             udt_ph_extension_number=NULL,
  @PhoneUnitNumber                  udt_ph_unit_number=NULL,
  @ShopManagerFlag                  udt_std_flag=0,
  @SysLastUserID                    udt_std_id,
  @SysLastUpdatedDate               VARCHAR(30),
  @ApplicationCD                    udt_std_cd='APD',
  @MergeFlag                        udt_std_flag=0
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error                            udt_std_int
    DECLARE @rowcount                         udt_std_int

    DECLARE @tupdated_date                    udt_std_datetime 
    DECLARE @temp_updated_date                udt_std_datetime 

    DECLARE @ProcName                         AS varchar(30)       -- Used for raise error stmts 
    DECLARE @now                              udt_std_datetime
    DECLARE @ShopPrimaryContactCode           udt_std_int_tiny
    DECLARE @ShopContactCode                  udt_std_int_tiny
    DECLARE @BusinessPrimaryContactCode       udt_std_int_tiny
    DECLARE @BusinessContactCode              udt_std_int_tiny
    DECLARE @AppliesToCD                      udt_std_cd
    
    DECLARE @LogComment                       udt_std_desc_long,
            @oldValue                         udt_std_desc_long,
            @newValue                         udt_std_desc_long,
            @ShopID                           udt_std_id_big,
            @ShopName                         udt_std_name,
            @PersonnelType                    udt_std_name,
            @KeyDescription                   udt_std_desc_short
                
    
    DECLARE @dbPersonnelTypeID                udt_std_int_tiny,
            @dbPreferredContactMethodID       udt_std_int_tiny,
            @dbCellAreaCode                   udt_ph_area_code,
            @dbCellExchangeNumber             udt_ph_exchange_number,
            @dbCellExtensionNumber            udt_ph_extension_number,
            @dbCellUnitNumber                 udt_ph_unit_number,
            @dbEmailAddress                   udt_web_email,
            @dbFaxAreaCode                    udt_ph_area_code,
            @dbFaxExchangeNumber              udt_ph_exchange_number,
            @dbFaxExtensionNumber             udt_ph_extension_number,
            @dbFaxUnitNumber                  udt_ph_unit_number,
            @dbGenderCD                       udt_per_gender_cd,
            @dbMinorityFlag                   udt_std_flag,
            @dbName                           udt_std_name,
            @dbPagerAreaCode                  udt_ph_area_code,
            @dbPagerExchangeNumber            udt_ph_exchange_number,
            @dbPagerExtensionNumber           udt_ph_extension_number,
            @dbPagerUnitNumber                udt_ph_unit_number,
            @dbPhoneAreaCode                  udt_ph_area_code,
            @dbPhoneExchangeNumber            udt_ph_exchange_number,
            @dbPhoneExtensionNumber           udt_ph_extension_number,
            @dbPhoneUnitNumber                udt_ph_unit_number,
            @dbShopManagerFlag                udt_std_flag
      
    
    SELECT  @dbPersonnelTypeID = PersonnelTypeID,
            @dbPreferredContactMethodID  = PreferredContactMethodID,
            @dbCellAreaCode = CellAreaCode,
            @dbCellExchangeNumber = CellExchangeNumber,
            @dbCellExtensionNumber = CellExtensionNumber,
            @dbCellUnitNumber = CellUnitNumber,
            @dbEmailAddress = EmailAddress,
            @dbFaxAreaCode = FaxAreaCode,
            @dbFaxExchangeNumber = FaxExchangeNumber,
            @dbFaxExtensionNumber = FaxExtensionNumber,
            @dbFaxUnitNumber = FaxUnitNumber,
            @dbGenderCD = GenderCD,
            @dbMinorityFlag = MinorityFlag,
            @dbName = Name,
            @dbPagerAreaCode = PagerAreaCode,
            @dbPagerExchangeNumber = PagerExchangeNumber,
            @dbPagerExtensionNumber = PagerExtensionNumber,
            @dbPagerUnitNumber = PagerUnitNumber,
            @dbPhoneAreaCode = PhoneAreaCode,
            @dbPhoneExchangeNumber = PhoneExchangeNumber,
            @dbPhoneExtensionNumber = PhoneExtensionNumber,
            @dbPhoneUnitNumber = PhoneUnitNumber,
            @dbShopManagerFlag = ShopManagerFlag 
    FROM dbo.utb_personnel
    WHERE PersonnelID = @PersonnelID
    
    SET @AppliesToCD = (SELECT AppliesToCD FROM dbo.utb_personnel_type WHERE PersonnelTypeID = @PersonnelTypeID) 
    SET @BusinessPrimaryContactCode = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Owner Officer #1')
    SET @BusinessContactCode = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Owner Officer #2')
    SET @ShopPrimaryContactCode = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Shop Manager')
    SET @ShopContactCode = (SELECT PersonnelTypeID FROM dbo.utb_personnel_type WHERE Name = 'Estimator')
    SET @ProcName = 'uspSMTPersonnelUpdDetail'
    SET @PersonnelType = (SELECT pt.Name 
                          FROM dbo.utb_personnel_type pt INNER JOIN dbo.utb_personnel p ON pt.PersonnelTypeID = p.PersonnelTypeID
                          WHERE p.PersonnelID = @PersonnelID)
                          
    
    IF @AppliesToCD = 'L'
    BEGIN
        SELECT @ShopID = s.ShopLocationID,
               @ShopName = s.Name
        FROM dbo.utb_shop_location s INNER JOIN dbo.utb_shop_location_personnel slp ON s.ShopLocationID = slp.ShopLocationID
        WHERE slp.PersonnelID = @PersonnelID
    END
    ELSE
    BEGIN
        SELECT @ShopID = s.ShopID,
               @ShopName = s.Name
        FROM dbo.utb_shop s INNER JOIN dbo.utb_shop_personnel sp ON s.ShopID = sp.ShopID
        WHERE sp.PersonnelID = @PersonnelID      
    END
    
    

    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@CellAreaCode))) = 0 SET @CellAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@CellExchangeNumber))) = 0 SET @CellExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@CellExtensionNumber))) = 0 SET @CellExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@CellUnitNumber))) = 0 SET @CellUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@EmailAddress))) = 0 SET @EmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0 SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0 SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0 SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0 SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@GenderCD))) = 0 SET @GenderCD = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0 SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PagerAreaCode))) = 0 SET @PagerAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PagerExchangeNumber))) = 0 SET @PagerExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PagerExtensionNumber))) = 0 SET @PagerExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PagerUnitNumber))) = 0 SET @PagerUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0 SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0 SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0 SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0 SET @PhoneUnitNumber = NULL


    -- Apply edits
    
    IF @GenderCD IS NOT NULL
    BEGIN
        IF @GenderCD NOT IN ('U', 'M', 'F')
        BEGIN
           -- Invalid Gender
            RAISERROR('101|%s|@GenderCD|%s', 16, 1, @ProcName, @GenderCD)
            RETURN
        END
    END

    IF LEN(@Name) < 1
    BEGIN
       -- Missing Personnel Name
        RAISERROR('101|%s|@Name|%s', 16, 1, @ProcName, @Name)
        RETURN
    END

    IF (@PersonnelTypeID IS NULL) OR (NOT EXISTS (SELECT PersonnelTypeID 
                                                  FROM utb_personnel_type 
                                                  WHERE PersonnelTypeID = @PersonnelTypeID
                                                    AND EnabledFlag = 1)) 
    BEGIN
      -- Invalid Personnel Type
      RAISERROR('101|%s|@PersonnelTypeID|%u', 16, 1, @ProcName, @PersonnelTypeID)
      RETURN
    END
    

    IF @PreferredContactMethodID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT PersonnelContactMethodID FROM utb_personnel_contact_method WHERE PersonnelContactMethodID = @PreferredContactMethodID
                                                                                           AND EnabledFlag = 1)
        BEGIN
           -- Invalid Preferred Contact Method
            RAISERROR('101|%s|@PreferredContactMethodID|%u', 16, 1, @ProcName, @PreferredContactMethodID)
            RETURN
        END
    END


    -- Validate the updated date parameter
    IF @MergeFlag = 0
    BEGIN
      EXEC uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_personnel', @PersonnelID

      IF @@ERROR <> 0
      BEGIN
          -- Problem checking Last Updated Date, error was thrown in the called proc. No need to raise another, just return.

          RETURN
      END
    END
    
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    
    
    IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
    BEGIN
      -- Invalid User
      RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)	
      RETURN
    END
    

    SET @now = CURRENT_TIMESTAMP
        

    -- Begin Update(s)

    BEGIN TRANSACTION AdmPersonnelUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    DECLARE @tmpPersonnel TABLE (PersonnelID     bigint    NOT NULL)
    
    
    -- If this is a 'Shop Manager', make sure it is the only primary contact for this Shop.
    IF (@PersonnelTypeID = @ShopPrimaryContactCode)
    BEGIN
      
      -- Gather any other 'Shop Primary Contact's associated with this shop
      INSERT INTO @tmpPersonnel
      SELECT slp.PersonnelID
      FROM dbo.utb_shop_location_personnel slp INNER JOIN dbo.utb_personnel p ON slp.PersonnelID = p.PersonnelID
      WHERE p.PersonnelID <> @PersonnelID
        AND p.PersonnelTypeID = @ShopPrimaryContactCode
        AND slp.ShopLocationID = (SELECT ShopLocationID 
                                  FROM dbo.utb_shop_location_personnel 
                                  WHERE PersonnelID = @PersonnelID)
            
     
      -- Update any other 'Shop Manager's for this shop to 'Estimator's
      UPDATE dbo.utb_personnel
      SET PersonnelTypeID = @ShopContactCode
      FROM dbo.utb_personnel p INNER JOIN @tmpPersonnel tp ON p.PersonnelID = tp.PersonnelID
      
      DELETE @tmpPersonnel            
    END
    
    
    -- If this is a 'Owner Officer #1', make sure it is the only primary contact for this Business.
    IF (@PersonnelTypeID = @BusinessPrimaryContactCode)
    BEGIN
      
      -- Gather any other 'Owner Officer #1's associated with this business
      INSERT INTO @tmpPersonnel
      SELECT sp.PersonnelID
      FROM dbo.utb_shop_personnel sp INNER JOIN dbo.utb_personnel p ON sp.PersonnelID = p.PersonnelID
      WHERE p.PersonnelID <> @PersonnelID
        AND p.PersonnelTypeID = @BusinessPrimaryContactCode
        AND ShopID = (SELECT ShopID 
                      FROM dbo.utb_shop_personnel 
                      WHERE PersonnelID = @PersonnelID)
                      
      
      -- Update any other 'Owner Officer #1's for this shop to 'Owner/Officer #2's
      UPDATE dbo.utb_personnel
      SET PersonnelTypeID = @BusinessContactCode
      FROM dbo.utb_personnel p INNER JOIN @tmpPersonnel tp ON p.PersonnelID = tp.PersonnelID
             
    END
    
    
    IF (@@ERROR <> 0)
    BEGIN
      -- SQL Server Error
      RAISERROR('99|%s', 16, 1, @ProcName)
      RETURN
    END 
    
    
    
    
    UPDATE  dbo.utb_personnel SET
            PersonnelTypeID = @PersonnelTypeID,
            PreferredContactMethodID = @PreferredContactMethodID,
            CellAreaCode = @CellAreaCode,
            CellExchangeNumber = @CellExchangeNumber,
            CellExtensionNumber = @CellExtensionNumber,
            CellUnitNumber = @CellUnitNumber,
            EmailAddress = @EmailAddress,
            FaxAreaCode = @FaxAreaCode,
            FaxExchangeNumber = @FaxExchangeNumber,
            FaxExtensionNumber = @FaxExtensionNumber,
            FaxUnitNumber = @FaxUnitNumber,
            GenderCD = @GenderCD,
            MinorityFlag = @MinorityFlag,
            Name = @Name,
            PagerAreaCode = @PagerAreaCode,
            PagerExchangeNumber = @PagerExchangeNumber,
            PagerExtensionNumber = @PagerExtensionNumber,
            PagerUnitNumber = @PagerUnitNumber,
            PhoneAreaCode = @PhoneAreaCode,
            PhoneExchangeNumber = @PhoneExchangeNumber,
            PhoneExtensionNumber = @PhoneExtensionNumber,
            PhoneUnitNumber = @PhoneUnitNumber,
            ShopManagerFlag = @ShopManagerFlag,
            SysLastUserID = @SysLastUserID,
            SysLastUpdatedDate = @now
      WHERE PersonnelID = @PersonnelID
       AND SysLastUpdatedDate = @tupdated_date

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('104|%s|utb_personnel', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END


    -- Audit Log code  --------------------------------------------------------------------------------------------
    
    SET @KeyDescription = CONVERT(varchar(50), @PersonnelID) + '-' + @dbName
    
       
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbName, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Name, ''))))
    BEGIN
      SET @LogComment = 'Name changed from ' + 
                        ISNULL(CONVERT(varchar(50), @dbName), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(50), @Name), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
        
    
    IF @dbPersonnelTypeID <> @PersonnelTypeID
    BEGIN
      
      SET @oldValue = (SELECT Name FROM dbo.utb_personnel_type WHERE PersonnelTypeID = @dbPersonnelTypeID)
      SET @newValue = (SELECT Name FROM dbo.utb_personnel_type WHERE PersonnelTypeID = @PersonnelTypeID)
    
      SET @LogComment = 'Preferred Contact Method changed from ' + 
                        ISNULL(CONVERT(varchar(12), @oldValue), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @newValue), 'NON-EXISTENT')
      
                       
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
          
    
    IF @dbShopManagerFlag <> @ShopManagerFlag
    BEGIN
      SET @LogComment = 'Shop Manager flag was turned ' + (SELECT CASE @dbShopManagerFlag WHEN 0 THEN 'ON' ELSE 'OFF' END)
      
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbEmailAddress, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@EmailAddress, ''))))
    BEGIN
      SET @LogComment = 'Email Address changed from ' + 
                        ISNULL(CONVERT(varchar(50), @dbEmailAddress), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(50), @EmailAddress), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF @dbPreferredContactMethodID <> @PreferredContactMethodID
    BEGIN
      
      SET @oldValue = (SELECT Name FROM dbo.utb_personnel_contact_method WHERE PersonnelContactMethodID = @dbPreferredContactMethodID)
      SET @newValue = (SELECT Name FROM dbo.utb_personnel_contact_method WHERE PersonnelContactMethodID = @PreferredContactMethodID)
    
      SET @LogComment = 'Preferred Contact Method changed from ' + 
                        ISNULL(CONVERT(varchar(12), @oldValue), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @newValue), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END            
             
    
    ---------
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Phone Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Phone Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Phone Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Phone Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    ------------      
          
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Fax Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Fax Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Fax Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Fax Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
            
    ------------------
              
                
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbCellAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@CellAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Cell Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbCellAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @CellAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbCellExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@CellExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Cell Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbCellExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @CellExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbCellUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@CellUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Cell Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbCellUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @CellUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END                
                     
          
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbCellExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@CellExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Cell Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbCellExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @CellExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
         
                
    ------          
              
          
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPagerAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PagerAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Pager Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPagerAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PagerAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPagerExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PagerExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Pager Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPagerExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PagerExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPagerUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PagerUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Pager Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPagerUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PagerUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPagerExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PagerExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Pager Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPagerExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PagerExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
     
    ----------------
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbGenderCD, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@GenderCD, ''))))
    BEGIN
      
      SET @oldValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_personnel', 'GenderCD') WHERE Code = @dbGenderCD)
      SET @newValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_personnel', 'GenderCD') WHERE Code = @GenderCD)
    
      SET @LogComment = 'Name changed from ' + 
                        ISNULL(CONVERT(varchar(12), @oldValue), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @newValue), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END          
     
            
    IF @dbMinorityFlag <> @MinorityFlag
    BEGIN
      SET @LogComment = 'Shop Manager flag was turned ' + (SELECT CASE @dbMinorityFlag WHEN 0 THEN 'ON' ELSE 'OFF' END)
      
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @KeyDescription, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          RAISERROR  ('99|%s', 16, 1, 'uspAuditLogInsDetail')
          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
       
    
    ---------------------------------------------------------------------------------------------------------------
    
    

    COMMIT TRANSACTION AdmPersonnelUpdDetailTran1    


    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @PersonnelID AS PersonnelID
    
    RETURN @rowcount
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTPersonnelUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTPersonnelUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/