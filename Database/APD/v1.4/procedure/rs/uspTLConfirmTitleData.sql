-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLConfirmTitleData' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspTLConfirmTitleData 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspTLConfirmTitleData
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Confirms and locks the Total Loss Title Data
*
* PARAMETERS:  
* (I) @ClaimAspectServiceChannelID  Checklist ID
* (I) @UserID       User ID
* RESULT SET:
* NONE
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspTLConfirmTitleData
    @ClaimAspectServiceChannelID    udt_std_id_big,
    @UserID                         varchar(50)
AS
BEGIN

    --Initialize string parameters
    
    -- Declare internal variables
    
    DECLARE @ProcName   AS varchar(20)
    DECLARE @now AS datetime
    DECLARE @ClaimAspectID as bigint
    DECLARE @TitleName as varchar(250)
    DECLARE @TitleState as varchar(2)
    
    SET @ProcName = 'uspTLConfirmTitleData'
    SET @now = CURRENT_TIMESTAMP
    
    IF NOT EXISTS(SELECT ClaimAspectServiceChannelID
                  FROM utb_claim_aspect_service_channel
                  WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)
    BEGIN
        -- Invalid ClaimAspectServiceChannelID
        RAISERROR('%s: Invalid ClaimAspectServiceChannelID.', 16, 1, @ProcName)
        RETURN
    END

    -- Check the user id
    IF NOT EXISTS (SELECT UserID FROM utb_user WHERE UserID = @UserID)
    BEGIN
        -- Invalid User
        RAISERROR('%s: Invalid User.', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @ClaimAspectID = ClaimAspectID
    FROM dbo.utb_claim_aspect_service_channel
    WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID
    
    SELECT @TitleName = TitleName,
           @TitleState = TitleState
    FROM dbo.utb_claim_vehicle
    WHERE ClaimAspectID = @ClaimAspectID
    
    IF LEN(LTRIM(RTRIM(@TitleName))) = 0 SET @TitleName = NULL
    IF LEN(LTRIM(RTRIM(@TitleState))) = 0 SET @TitleState = NULL
    
    IF @TitleName IS NULL OR
       @TitleState IS NULL
    BEGIN
        -- Invalid State
        RAISERROR('1|Title Name and State is required for it to be confirmed and locked.', 16, 1, @ProcName)
        RETURN
    END

    BEGIN TRANSACTION
    
    UPDATE dbo.utb_claim_vehicle
    SET TitleNameConfirmDate = @now,
        TitleNameConfirmFlag = 1,
        SysLastUserID = @UserID,
        SysLastUpdatedDate = @now
    WHERE ClaimAspectID = @ClaimAspectID

    IF @@error <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_claim_vehicle', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
      
    COMMIT TRANSACTION
    

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspTLConfirmTitleData' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspTLConfirmTitleData TO 
        ugr_fnolload

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/