-- Create new events
-- SELECT * FROM utb_Event
IF NOT EXISTS(SELECT EventID FROM utb_event WHERE EventID = 109)
BEGIN
	-- Doesn't exist, create it
	INSERT INTO utb_event VALUES (109, 9, 132, 1, 1, 'HQ Cancel Claim', 0, CURRENT_TIMESTAMP)
	INSERT INTO utb_event VALUES (110, 9, 133, 1, 1, 'HQ ReOpen Claim', 0, CURRENT_TIMESTAMP)
END
ELSE
BEGIN
	-- Already exists.
	SELECT 'Already Added...'
END
