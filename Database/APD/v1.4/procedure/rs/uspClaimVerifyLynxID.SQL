-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVerifyLynxID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimVerifyLynxID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspClaimVerifyLynxID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jeffery A. Lund
* FUNCTION:     Verifies that a given LynxID exists in the system
*
* PARAMETERS:  
* (I) @LynxID               ID of claim
* (I) @InsuranceCompanyID   ID of Insurance company (optional)
*
* RESULT SET:
* 1 if LynxID is valid, 0 if not.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimVerifyLynxID
    @LynxID                 varchar(20),
    @InsuranceCompanyID     varchar(10) = Null
AS
BEGIN
DECLARE @InsuranceCompanyIDWork as varchar(10)

    if @InsuranceCompanyID is not null
    BEGIN
        SET @InsuranceCompanyIDWork = '%' + @InsuranceCompanyID + '%'
    END
    ELSE
    BEGIN
        SET @InsuranceCompanyIDWork = '%'
    END

    SELECT
        Count(*)

    FROM
        dbo.utb_Claim

    WHERE
        LynxID = @LynxID
        AND convert(varchar(10), InsuranceCompanyID) like @InsuranceCompanyIDWork
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimVerifyLynxID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimVerifyLynxID TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/