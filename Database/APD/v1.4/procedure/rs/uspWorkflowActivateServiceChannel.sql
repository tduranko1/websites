-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowActivateServiceChannel' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWorkflowActivateServiceChannel 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWorkflowActivateServiceChannel
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWorkflowActivateServiceChannel
    @ClaimAspectServiceChannelID  udt_std_id_big output,
    @ServiceChannelReactivated    udt_std_flag output,
    @ClaimAspectID                udt_std_id_big,
    @ServiceChannelCD             udt_std_cd,
    @PrimaryFlag                  udt_std_flag = 0,
    @UserID                       udt_std_id,
    @Notify                       udt_std_flag = 0,
    @Comment                      udt_std_desc_long = null
AS
BEGIN
    
    DECLARE @ProcName                               varchar(30)
    DECLARE @EventIDServiceChannelActivated         udt_std_id
    DECLARE @EventIDServiceChannelReactivated       udt_std_id
    DECLARE @EventIDVehicleReopened                 udt_std_id
    DECLARE @EventIDClaimReopened                   udt_std_id
    DECLARE @NoteTypeIDSummary                      udt_std_id
    DECLARE @ModifiedDateTime                       udt_std_datetime
    DECLARE @Description                            udt_std_desc_short
    DECLARE @ClaimAspectTypeIDClaim                 udt_std_id
    DECLARE @ClaimAspectTypeID                      udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle               udt_std_id
    DECLARE @ServiceChannelName                     udt_std_desc_short
    DECLARE @debug                                  udt_std_flag
    DECLARE @PrimaryClaimAspectServiceChannelID     udt_std_id_big
    DECLARE @LynxID                                 udt_std_id_big
    
    DECLARE @w_ClaimAspectIDClaim                   udt_std_id_big
    DECLARE @v_ClaimWillReopen                      udt_std_flag
    DECLARE @v_VehicleWillReopen                    udt_std_flag
    DECLARE @w_ClaimAspectServiceChannelID          udt_std_id_big
    DECLARE @w_ClaimAspectStatusID                  udt_std_id_big
    DECLARE @w_StatusName                           udt_std_name
    DECLARE @w_StatusID                             udt_std_id
    DECLARE @w_EventID                              udt_std_id
    DECLARE @w_EventDescription                     udt_std_desc_long
    DECLARE @w_ReopenDescription                    udt_std_desc_long
    DECLARE @err                                    bigint
    SET @debug = 0
    
    if @debug = 1
    BEGIN
      PRINT '@@PrimaryFlag = ' + CONVERT(VARCHAR(1),@PrimaryFlag)
    END
         
    SET NOCOUNT ON
            
    SET @ProcName = 'uspWorkflowActivateServiceChannel'
    SET @ModifiedDateTime = GetDate()
    
    IF NOT EXISTS(SELECT * FROM utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID)
    BEGIN
        -- Invalid Claim Aspect ID Passed
      RAISERROR('101|%s|@ClaimAspectID|%u does not exist.', 16, 1, @ProcName, @ClaimAspectID)
      RETURN      
        
    END 
    
    SELECT @ClaimAspectTypeIDVehicle = ClaimAspectTypeID FROM utb_claim_aspect_type where Name='Vehicle'
    
    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
      RAISERROR('101|%s|Invalid APD Data State. Claim Apsect Type for Vehicle not defined.',16,1,@ProcName)
      RETURN
    END
    
    SELECT @ClaimAspectTypeIDClaim   = ClaimAspectTypeID FROM utb_claim_aspect_type where Name='Claim'
    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
      RAISERROR('101|%s|Invalid APD Data State. Claim Apsect Type for Claim not defined.',16,1,@ProcName)
      RETURN
    END
    
    SELECT @NoteTypeIDSummary = NoteTypeID FROM utb_note_type WHERE Name = 'Summary'
    IF @NoteTypeIDSummary IS NULL 
    BEGIN
      RAISERROR('101|%s|Invalid APD Data State. Note Type for Summary not defined.',16,1,@ProcName)
      RETURN    
    END    
    
    SELECT @ClaimAspectTypeID = ClaimAspectTypeID FROM utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID
    
    IF @ClaimAspectTypeID <> @ClaimAspectTypeIDVehicle 
    BEGIN
        -- Claim Aspect ID Passed is not a vehicle aspect
      RAISERROR('101|%s|@ClaimAspectID|%u is not valid aspect type to activate service channel on.', 16, 1, @ProcName, @ClaimAspectID)
      RETURN      
        
    END
    
    SELECT @ServiceChannelName =  Name FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD')
    WHERE Code = @ServiceChannelCD
    
    IF @ServiceChannelName IS NULL
    BEGIN
        -- Invalid Service Channel Code
      RAISERROR('101|%s|@ServiceChannelCD|%s is not valid.', 16, 1, @ProcName, @ServiceChannelCD)
      RETURN      
        
    END
    
    SELECT @LynxID = LynxID FROM dbo.utb_claim_aspect WHERE ClaimAspectID = @ClaimAspectID
    
    SELECT @w_ClaimAspectIDClaim = ClaimAspectID
    FROM dbo.utb_claim_aspect 
    WHERE LynxID = @LynxID
      AND ClaimAspectTypeID = @ClaimAspectTypeIDClaim


    IF EXISTS (SELECT cas.StatusID
                     FROM dbo.utb_claim_aspect_status cas
                     INNER JOIN utb_status s ON (cas.StatusID = s.StatusID)
                     WHERE cas.ClaimAspectID = @ClaimAspectID 
                       AND s.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
                       AND s.StatusTypeCD IS NULL
                       AND s.ServiceChannelCD IS NULL
                       AND s.Name ='Vehicle Voided')
    BEGIN 
      -- Can not reopen a voided vehicle
      RAISERROR('1|Service channel specified belongs to a voided vehicle. Can not reopen.',16,1)
      RETURN
    END                      

    -- Is Claim Closed?
    IF EXISTS (SELECT cas.StatusID
               FROM dbo.utb_claim_aspect_status cas
               INNER JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID)
               WHERE cas.ClaimAspectID = @w_ClaimAspectIDClaim
                 AND s.ClaimAspectTypeID = @ClaimAspectTypeIDClaim
                 AND s.StatusTypeCD IS NULL
                 AND s.ServiceChannelCD IS NULL
                 AND s.Name = 'Claim Closed')
    BEGIN
      SET @v_ClaimWillReopen = 1
      SET @v_VehicleWillReopen = 1
    END
    ELSE
    BEGIN
      SET @v_ClaimWillReopen = 0
      -- Determine Vehicle State
      IF EXISTS (SELECT cas.StatusID
                 FROM dbo.utb_claim_aspect_status cas
                 INNER JOIN utb_status s ON (cas.StatusID = s.StatusID)
                 WHERE cas.ClaimAspectID = @ClaimAspectID 
                   AND s.ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
                   AND s.StatusTypeCD IS NULL
                   AND s.ServiceChannelCD IS NULL
                   AND s.Name IN ('Vehicle Closed','Vehicle Cancelled'))
      BEGIN
        SET @v_VehicleWillReopen = 1
      END
      ELSE
      BEGIN
        SET @v_VehicleWillReopen = 0
      END
    END                   
                   
       
    -- Determine if service channel record exists
    SELECT @w_ClaimAspectServiceChannelID = ClaimAspectServiceChannelID 
    FROM dbo.utb_claim_aspect_service_channel
    WHERE ClaimAspectID = @ClaimAspectID 
      AND ServiceChannelCD = @ServiceChannelCD

    IF @@ERROR <> 0 
    BEGIN
      RAISERROR('99|%s',16,1,@ProcName)
      RETURN
    END     
    
    SELECT @w_ClaimAspectStatusID = ClaimAspectStatusID
    FROM dbo.utb_claim_aspect_status
    WHERE ClaimAspectID = @ClaimAspectID
      AND StatusTypeCD = 'SC' 
      AND ServiceChannelCD = @ServiceChannelCD
                 
    IF @@ERROR <> 0 
    BEGIN
      RAISERROR('99|%s',16,1,@ProcName)
      RETURN
    END     

    IF @w_ClaimAspectServiceChannelID IS NULL AND
       @w_ClaimAspectStatusID IS NOT NULL
    BEGIN
      -- Claim Aspect Status record should not exist for a Service Channel Record which does not exist
        RAISERROR  ('%s: Invalid APD Data State.  Claim Aspect Status ID %u exists for a Claim Aspect Service Channel Record which does not exist.', 16, 1, @ProcName,@w_ClaimAspectStatusID)
        RETURN
    END      
      
   
    IF  (@UserID IS NULL) OR
        (@UserID = 0) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END
    
    SELECT @EventIDServiceChannelActivated = EventID
    FROM utb_event
    WHERE Name = 'Service Channel Activated'
    
    IF @EventIDServiceChannelActivated IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Service Channel Activated Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END 

    SELECT @EventIDServiceChannelReactivated = EventID
    FROM utb_event
    WHERE Name = 'Vehicle Service Channel Reactivated'
    
    IF @EventIDServiceChannelActivated IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Vehicle Service Channel Reactivated Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END 
    
    SELECT @EventIDClaimReopened = EventID
    FROM utb_event
    WHERE Name = 'Claim Reopened'
    
    IF @EventIDClaimReopened IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Claim Reopened Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END 

    SELECT @EventIDVehicleReopened = EventID
    FROM utb_event
    WHERE Name = 'Vehicle Reopened'
    
    IF @EventIDVehicleReopened IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Vehicle Reopened Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END 

    
    BEGIN TRANSACTION WorkflowActivateServiceChannel1
    
    IF @PrimaryFlag = 1
    BEGIN
        -- Remove primary flag from any existing service channels for claim aspect
        UPDATE utb_claim_aspect_service_channel
        SET PrimaryFlag = 0
        WHERE ClaimAspectID = @ClaimAspectID
        
        IF @@ERROR <> 0 
        BEGIN
          -- SQL Server Error
        
          RAISERROR  ('104|%s|utb_claim_aspect_service_channel', 16, 1, @ProcName)
          ROLLBACK TRANSACTION
          RETURN
        END      
    END

    SET @ServiceChannelReactivated = 0
    SET @w_EventID = NULL
    
    IF (@w_ClaimAspectServiceChannelID IS NULL)
    BEGIN
      -- No Service Channel Record need to create one
      
      -- Create Service Channel Record
      INSERT INTO utb_claim_aspect_service_channel
      (
        ClaimAspectID,
        CreatedUserID,
        CreatedDate,
        EnabledFlag,
        PrimaryFlag,
        ServiceChannelCD,
        SysLastUserID,
        SysLastUpdatedDate
      )
      VALUES
      (
        @ClaimAspectID,
        @UserID,
        @ModifiedDateTime,
        1,
        @PrimaryFlag,
        @ServiceChannelCD,
        @UserID,
        @ModifiedDateTime
      )
      
      SET @err = @@ERROR
      SET @w_ClaimAspectServiceChannelID = SCOPE_IDENTITY() 
      
      
      IF @err <> 0
      BEGIN
          -- Insertion Failure
          RAISERROR('105|%s|utb_claim_aspect_service_channel', 16, 1, @ProcName)
          ROLLBACK TRANSACTION
          RETURN   
      END 
      
      
      SET @ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID  
      
      if @debug = 1
      begin
        print ' New ClaimAspectServiceChannelID = ' + convert(varchar,@ClaimAspectServiceChannelID)
        select 'New ClaimAspectServiceChannelID = ' + convert(varchar,max(ClaimAspectServiceChannelID)) from utb_claim_aspect_service_channel
      end 
      
      SET @w_EventID = @EventIDServiceChannelActivated 
      SET @w_EventDescription = 'Vehicle Service Channel Activated: ' + @ServiceChannelName 
        
    END
    ELSE
    BEGIN
      
      IF @PrimaryFlag = 1
      BEGIN      
        UPDATE dbo.utb_claim_aspect_service_channel
        SET PrimaryFlag = @PrimaryFlag
        WHERE ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
        
        IF @@ERROR<>0
        BEGIN
          RAISERROR('99|%s',16,1,@ProcName)
          ROLLBACK TRANSACTION
          RETURN
        END
      END      
    
      -- Service Channel Record exists.                
      IF @w_ClaimAspectStatusID IS NOT NULL
      BEGIN
        SELECT @w_StatusName = s.Name,
               @w_StatusID = cas.StatusID
            FROM dbo.utb_claim_aspect_status cas
            INNER JOIN dbo.utb_status s ON (cas.StatusID = s.StatusID)
            WHERE cas.ClaimAspectStatusID = @w_ClaimAspectStatusID
            
        
        IF @w_StatusName = 'Active'
        BEGIN
          -- Already an active service channel
          RAISERROR('101|%s|%s already exists for as an active service channel for Claim Aspect ID %u.',16,1,@ProcName,@ServiceChannelName, @ClaimAspectID)
          ROLLBACK TRANSACTION
          RETURN          
        END
        IF @w_StatusName IN ('Complete','Cancelled')
        BEGIN
          -- If reactivation Comments are required
          IF (@Comment IS NULL) OR
             (LEN(RTRIM(LTRIM(@Comment))) = 0)
          BEGIN
            RAISERROR('1|Comment is required for reactivation of service channel.',16,1)             
            ROLLBACK TRANSACTION
            RETURN
          END
          -- Service channel was cancelled or completed.  This will be a reactivation
          SET @w_EventID = @EventIDServiceChannelReactivated
          SET @w_EventDescription = 'Vehicle Service Channel Reactivated: ' + @ServiceChannelName 
          SET @ServiceChannelReactivated = 1
          
          
        END
        IF @w_StatusName = 'Voided'
        BEGIN
          RAISERROR('101|%s|Can not reactivate a voided service channel.',16,1,@ProcName)
          ROLLBACK TRANSACTION
          RETURN
        END
      END
      ELSE           
      BEGIN
         -- Status Record does not exist
        SET @w_EventID = @EventIDServiceChannelActivated 
        SET @w_EventDescription = 'Vehicle Service Channel Activated: ' + @ServiceChannelName 
         
      END        
    END 
    
    IF (@v_VehicleWillReopen = 1) AND
       ((@Comment is NULL) OR (LEN(RTRIM(LTRIM(@Comment)))=0))
    BEGIN
        IF (@Comment IS NULL) OR
           (LEN(RTRIM(LTRIM(@Comment))) = 0)
        BEGIN
          RAISERROR('Activating service channel will cause existing closed/cancelled vehicle to reopen.  Comment is required.',16,1)             
          ROLLBACK TRANSACTION
          RETURN
        END        
    END     
    
    -- Send out notification if need to
    IF @Notify = 1
    BEGIN
        IF @v_ClaimWillReopen = 1
        BEGIN
          --SET
          EXEC uspWorkFlowNotifyEvent @EventID = @EventIDClaimReopened,
                                      @ClaimAspectID = @w_ClaimAspectIDClaim,
                                      @Description = 'Claim Reopened',
                                      @UserID = @UserID
          IF @@ERROR <> 0
          BEGIN
              RAISERROR('%s: (Workflow Processing) Error notifying APD of the event Claim Reopened"', 10, 1, @ProcName)   
              ROLLBACK TRANSACTION           
              RETURN
          END
        END                                      
        IF @v_VehicleWillReopen = 1
        BEGIN 
          EXEC uspWorkFlowNotifyEvent @EventID = @EventIDVehicleReopened,
                                      @ClaimAspectID = @ClaimAspectID,
                                      @Description = 'Vehicle Reopened',
                                      @UserID = @UserID

          IF @@ERROR <> 0
          BEGIN
              RAISERROR('%s: (Workflow Processing) Error notifying APD of the event Claim Reopened"', 10, 1, @ProcName)
              ROLLBACK TRANSACTION
              RETURN
          END
        END          
        
        IF @w_EventID = @EventIDServiceChannelReactivated
        BEGIN
          -- Insert Comment into note concerning reactivation
          SET @Comment = 'Vehicle Service Channel (' + @ServiceChannelName + ') Reactivated: ' + @Comment
          EXEC uspNoteInsDetail @ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
                                @NoteTypeID = @NoteTypeIDSummary,
                                @Note = @Comment,
                                @StatusID = @w_StatusID,
                                @UserID = @UserID
          IF @@ERROR<> 0
          BEGIN
            RAISERROR('104|%s|%s',16,1,@ProcName,'uspNoteInsDetail')
            ROLLBACK
            RETURN                                
          END                                
                                
        
        END
        
        
        EXEC uspWorkflowNotifyEvent  @EventID = @w_EventID,
                                     @ClaimAspectID = @ClaimAspectID,
                                     @ServiceChannelCD = @ServiceChannelCD,
                                     @Description = @w_EventDescription,
                                     @ConditionValue = @ServiceChannelCD,
                                     @UserID = @UserID
        
        IF @@ERROR <> 0
        BEGIN
            RAISERROR('%s: (Workflow Processing) Error notifying APD of the event Service Channel Activated"', 10, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
        
        
                 
    END    
    
    COMMIT TRANSACTION WorkflowActivateServiceChannel1
    
    
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWorkflowActivateServiceChannel' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWorkflowActivateServiceChannel TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/