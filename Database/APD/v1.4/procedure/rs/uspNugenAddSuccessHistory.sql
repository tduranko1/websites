BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspNugenAddSuccessHistory' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspNugenAddSuccessHistory 
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspNugenAddSuccessHistory]
(
@LynxID varchar(15),
@Description varchar(500),
@VehicleNumber varchar(10)
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @ClaimAspectServiceChannelID varchar(20)
DECLARE @ClaimAspectID Varchar(20)
DECLARE @UserID varchar(10)
--DECLARE @ClaimAspectNumber Varchar(10)
DECLARE @ClaimAspectTypeID varchar(20)
DECLARE @ServiceChannelCD varchar(10)


IF EXISTS(Select ClaimAspectID from utb_claim_aspect where LynxID = @LynxID AND InitialAssignmentTypeID Is Not Null AND ClaimAspectNumber = @VehicleNumber)
		BEGIN
		SET @ClaimAspectID =(SELECT ClaimAspectID
			FROM utb_claim_aspect 
			WHERE LynxID = @LynxID and InitialAssignmentTypeID IS NOT NULL AND ClaimAspectNumber = @VehicleNumber)	

		--SET @ClaimAspectNumber =(SELECT ClaimAspectNumber
		--	FROM utb_claim_aspect 
		--	WHERE ClaimAspectID = @ClaimAspectID and InitialAssignmentTypeID IS NOT NULL)

		SET @ClaimAspectTypeID =(SELECT ClaimAspectTypeID
			FROM utb_claim_aspect 
			WHERE ClaimAspectID = @ClaimAspectID and InitialAssignmentTypeID IS NOT NULL)	
		

		SET @UserID =(SELECT SysLastUserID
			FROM utb_claim_aspect 
			WHERE ClaimAspectID = @ClaimAspectID and InitialAssignmentTypeID IS NOT NULL)	
		END
	 

IF EXISTS(Select ClaimAspectServiceChannelID from utb_claim_aspect_service_channel where ClaimAspectID = @ClaimAspectID)
		BEGIN
		SET @ClaimAspectServiceChannelID =(SELECT ClaimAspectServiceChannelID
			FROM utb_claim_aspect_service_channel 
			WHERE ClaimAspectID = @ClaimAspectID)		
		END
 




IF EXISTS(Select ServiceChannelCD from utb_claim_aspect_service_channel where ClaimAspectID = @ClaimAspectID)
		BEGIN
		SET @ServiceChannelCD =(SELECT ServiceChannelCD
			FROM utb_claim_aspect_service_channel 
			WHERE ClaimAspectServiceChannelID = @ClaimAspectServiceChannelID)		
		END




--print @ClaimAspectServiceChannelID
--print @ClaimAspectID
--print @UserID
--print @ClaimAspectNumber
--print @ClaimAspectTypeID
--print @ServiceChannelCD


  EXEC dbo.uspClaimHistoryEventInsDetail      @LynxID						= @LynxID,
                                              @EventID						= '9',
                                              @PertainsTo					= NULL,
                                              @ClaimAspectTypeID            = @ClaimAspectTypeID,
                                              @ClaimAspectNumber			= @VehicleNumber,
                                              @ServiceChannelCD				= @ServiceChannelCD,
                                              @ClaimAspectServiceChannelID  = @ClaimAspectServiceChannelID,
                                              @Description					= @Description,
                                              @ReferenceID                  = NULL,
                                              @UserID						= @UserID
                                             


SET NOCOUNT OFF
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspNugenAddSuccessHistory' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspNugenAddSuccessHistory TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO