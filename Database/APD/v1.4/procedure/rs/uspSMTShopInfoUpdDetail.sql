-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInfoUpdDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopInfoUpdDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopInfoUpdDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Update a Shop Location
*
*
* RESULT SET:
* ShopID                          The updated Shop Location unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopInfoUpdDetail
(
	  @ShopID                    	        udt_std_int_big,
    @BillingID                          udt_std_id_big,
    @DataReviewUserID                   udt_std_id=NULL,
    @PreferredCommunicationMethodID     udt_std_int_tiny=NULL,
    @PreferredEstimatePackageID         udt_std_int_tiny=NULL,
    @ProgramManagerUserID               udt_std_id=NULL,
    @BusinessInfoID                     udt_std_id_big,
    @Address1                           udt_addr_line_1=NULL,
    @Address2                           udt_addr_line_2=NULL,
    @AddressCity                        udt_addr_city=NULL,
    @AddressCounty                      udt_addr_county=NULL,
    @AddressState                       udt_addr_state=NULL,
    @AddressZip                         udt_addr_zip_code=NULL,
    @AutoVerseId                        udt_std_desc_mid=NULL,
    @AvailableForSelectionFlag  	    udt_std_flag=NULL,
    @CertifiedFirstFlag                 udt_std_flag=NULL,
    @CertifiedFirstId                   udt_std_desc_short=NULL,
    @DataReviewDate                     VARCHAR(30)=NULL,
    @DataReviewStatusCD                 udt_std_cd,
    @DMVFacility                        udt_std_desc_short = NULL,
    @DrivingDirections                  udt_std_desc_long=NULL,
    @EmailAddress                       udt_web_email=NULL,
    @EnabledFlag                        udt_enabled_flag=1,
    @FaxAreaCode                        udt_ph_area_code=NULL,
    @FaxExchangeNumber                  udt_ph_exchange_number=NULL,
    @FaxExtensionNumber                 udt_ph_extension_number=NULL,
    @FaxUnitNumber                      udt_ph_unit_number=NULL,
    @Latitude                           udt_addr_latitude=NULL,
    @Longitude                          udt_addr_longitude=NULL,
    @MaxWeeklyAssignments               udt_std_int_tiny=NULL,
    @Name                               udt_std_name,
    @PhoneAreaCode                      udt_ph_area_code=NULL,
    @PhoneExchangeNumber                udt_ph_exchange_number=NULL,
    @PhoneExtensionNumber               udt_ph_extension_number=NULL,
    @PhoneUnitNumber                    udt_ph_unit_number=NULL,
    @PPGCTSCustomerFlag                 udt_std_flag,
    @PPGCTSFlag                         udt_std_flag,
    @PPGCTSId                           udt_std_desc_short=NULL,
    @PPGCTSLevelCD                      udt_std_cd=NULL,
    @PreferredCommunicationAddress      udt_web_address=NULL,
    @ProgramFlag                        udt_std_flag=0,
    @ReferralFlag                       udt_std_flag=0,
    @ProgramScore                       udt_std_int_tiny=NULL,
    @RegistrationNumber                 udt_std_desc_short=NULL,
    @RegistrationExpDate                VARCHAR(30)=NULL,
    @SalesTaxNumber                     udt_std_desc_short = NULL,
    @WebSiteAddress                     udt_web_address=NULL,
    @WarrantyPeriodRefinishCD           udt_std_cd=NULL,
    @WarrantyPeriodWorkmanshipCD        udt_std_cd=NULL,
    @SysLastUserID                      udt_std_id,
    @SysLastUpdatedDate                 VARCHAR(30),

    -- Shop Hours Parms
    @OperatingMondayStartTime           udt_std_time=NULL,
    @OperatingMondayEndTime             udt_std_time=NULL,
    @OperatingTuesdayStartTime          udt_std_time=NULL,
    @OperatingTuesdayEndTime            udt_std_time=NULL,
    @OperatingWednesdayStartTime        udt_std_time=NULL,
    @OperatingWednesdayEndTime          udt_std_time=NULL,
    @OperatingThursdayStartTime         udt_std_time=NULL,
    @OperatingThursdayEndTime           udt_std_time=NULL,
    @OperatingFridayStartTime           udt_std_time=NULL,
    @OperatingFridayEndTime             udt_std_time=NULL,
    @OperatingSaturdayStartTime         udt_std_time=NULL,
    @OperatingSaturdayEndTime           udt_std_time=NULL,
    @OperatingSundayStartTime           udt_std_time=NULL,
    @OperatingSundayEndTime             udt_std_time=NULL,
    @ShopHoursSysLastUserID             udt_std_id,
    @ShopHoursSysLastUpdatedDate        VARCHAR(30),
    @ApplicationCD                      udt_std_cd='APD',
    @MergeFlag                          udt_std_flag=0
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
   
    
    
    -- Declare internal variables

    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    
    DECLARE @tDataReviewDate AS DATETIME 

    DECLARE @tupdated_date     AS DATETIME 
    DECLARE @temp_updated_date AS DATETIME 
    
    DECLARE @ProcName          AS VARCHAR(30)       -- Used for raise error stmts 

    DECLARE @WorkmanshipID     as tinyint
    DECLARE @RefinishID           as tinyint

    SET @ProcName = 'uspSMTShopInfoUpdDetail'


    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@Address1))) = 0                       SET @Address1 = NULL
    IF LEN(RTRIM(LTRIM(@Address2))) = 0                       SET @Address2 = NULL
    IF LEN(RTRIM(LTRIM(@AddressCity))) = 0                    SET @AddressCity = NULL
    IF LEN(RTRIM(LTRIM(@AddressCounty))) = 0                  SET @AddressCounty = NULL
    IF LEN(RTRIM(LTRIM(@AddressState))) = 0                   SET @AddressState = NULL
    IF LEN(RTRIM(LTRIM(@AddressZip))) = 0                     SET @AddressZip = NULL
    IF LEN(RTRIM(LTRIM(@AutoVerseId))) = 0                    SET @AutoVerseId = NULL
    IF LEN(RTRIM(LTRIM(@AvailableForSelectionFlag))) = 0      SET @AvailableForSelectionFlag = NULL
    IF LEN(RTRIM(LTRIM(@CertifiedFirstId))) = 0               SET @CertifiedFirstId = NULL
    IF LEN(RTRIM(LTRIM(@DataReviewDate ))) = 0                SET @DataReviewDate = NULL
    IF LEN(RTRIM(LTRIM(@DataReviewStatusCD))) = 0             SET @DataReviewStatusCD = NULL
    IF LEN(RTRIM(LTRIM(@DMVFacility))) = 0                    SET @DMVFacility = NULL
    IF LEN(RTRIM(LTRIM(@DrivingDirections))) = 0              SET @DrivingDirections = NULL
    IF LEN(RTRIM(LTRIM(@EmailAddress))) = 0                   SET @EmailAddress = NULL
    IF LEN(RTRIM(LTRIM(@FaxAreaCode))) = 0                    SET @FaxAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@FaxExchangeNumber))) = 0              SET @FaxExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxExtensionNumber))) = 0             SET @FaxExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@FaxUnitNumber))) = 0                  SET @FaxUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@Name))) = 0                           SET @Name = NULL
    IF LEN(RTRIM(LTRIM(@PhoneAreaCode))) = 0                  SET @PhoneAreaCode = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExchangeNumber))) = 0            SET @PhoneExchangeNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneExtensionNumber))) = 0           SET @PhoneExtensionNumber = NULL
    IF LEN(RTRIM(LTRIM(@PhoneUnitNumber))) = 0                SET @PhoneUnitNumber = NULL
    IF LEN(RTRIM(LTRIM(@PPGCTSId))) = 0                       SET @PPGCTSId = NULL
    IF LEN(RTRIM(LTRIM(@PPGCTSLevelCD))) = 0                  SET @PPGCTSLevelCD = NULL
    IF LEN(RTRIM(LTRIM(@PreferredCommunicationAddress))) = 0  SET @PreferredCommunicationAddress = NULL
    IF LEN(RTRIM(LTRIM(@WebSiteAddress)))     = 0             SET @WebSiteAddress = NULL
    IF LEN(RTRIM(LTRIM(@RegistrationNumber)))     = 0         SET @RegistrationNumber = NULL
    IF LEN(RTRIM(LTRIM(@RegistrationExpDate)))     = 0        SET @RegistrationExpDate = NULL
    IF LEN(RTRIM(LTRIM(@SalesTaxNumber)))     = 0             SET @SalesTaxNumber = NULL


    IF LEN(RTRIM(LTRIM(@OperatingMondayStartTime))) = 0       SET @OperatingMondayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingMondayEndTime))) = 0         SET @OperatingMondayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayStartTime))) = 0      SET @OperatingTuesdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingTuesdayEndTime))) = 0        SET @OperatingTuesdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayStartTime))) = 0    SET @OperatingWednesdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingWednesdayEndTime))) = 0      SET @OperatingWednesdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingThursdayStartTime))) = 0     SET @OperatingThursdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingThursdayEndTime))) = 0       SET @OperatingThursdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingFridayStartTime))) = 0       SET @OperatingFridayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingFridayEndTime))) = 0         SET @OperatingFridayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayStartTime))) = 0     SET @OperatingSaturdayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSaturdayEndTime))) = 0       SET @OperatingSaturdayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSundayStartTime))) = 0       SET @OperatingSundayStartTime = NULL
    IF LEN(RTRIM(LTRIM(@OperatingSundayEndTime))) = 0         SET @OperatingSundayEndTime = NULL
    IF LEN(RTRIM(LTRIM(@ShopHoursSysLastUpdatedDate))) = 0    SET @ShopHoursSysLastUpdatedDate = NULL

    -- Apply edits
    
    IF @DataReviewDate IS NOT NULL AND
       ISDATE(@DataReviewDate) = 0
    BEGIN
        -- Convert the value passed in effective date into date format
        
        SET @tDataReviewDate = CAST(@DataReviewDate AS DATETIME)
        
        -- Validate the effective date
        
        IF @tDataReviewDate IS NULL
        BEGIN 
            -- Invalid Data Review date value
        
            RAISERROR  ('1|Invalid Data Review Date value', 16, 1, @ProcName)
            RETURN
        END
    END
    
    IF @DataReviewStatusCD NOT IN (SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'DataReviewStatusCD'))
    BEGIN
       -- Invalid Data Review Status
    
        RAISERROR  ('1|Invalid Data Review Status', 16, 1, @ProcName)
        RETURN
    END
    
    IF @DataReviewStatusCD IN ('N') AND @DataReviewDate IS NOT NULL
    BEGIN
       -- Data Review Date not allowed
    
        SET @DataReviewDate = NULL
    END

    IF @DataReviewStatusCD IN ('I', 'A') AND @DataReviewDate IS NULL
    BEGIN
       -- Data Review Date required
    
      RAISERROR  ('1|Data Review Date required', 16, 1, @ProcName)
      RETURN
    END

    IF @DataReviewStatusCD IN ('N') AND @DataReviewUserID IS NOT NULL
    BEGIN
      -- Data Review User not allowed
      SET @DataReviewUserID = NULL
    END

    IF @DataReviewStatusCD IN ('I', 'A') AND @DataReviewUserID IS NULL
    BEGIN
       -- Data Review User required
    
      RAISERROR  ('1|Data Review User required', 16, 1, @ProcName)
      RETURN
    END

    IF @PPGCTSLevelCD IS NOT NULL AND @PPGCTSLevelCD NOT IN (SELECT Code FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'PPGCTSLevelCD'))
    BEGIN
       -- Invalid PPG CTS Level
    
        RAISERROR  ('101|Invalid PPG CTS Level|@PPGCTSLevelCD|%s', 16, 1, @ProcName,@PPGCTSLevelCD)
        RETURN
    END

    IF @ProgramScore IS NOT NULL
    BEGIN
        IF @ProgramScore > 100 OR @ProgramScore < 0
        BEGIN
           -- Invalid Program Score
        
            RAISERROR  ('1|Invalid Program Score', 16, 1, @ProcName)
            RETURN
        END
    END

    IF LEN(@Name) < 1
    BEGIN
       -- Missing Shop Location Name
    
        RAISERROR  ('1|Missing Shop Location Name', 16, 1, @ProcName)
        RETURN
    END

    IF @BillingID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT BillingID FROM utb_billing WHERE BillingID = @BillingID)
        BEGIN
           -- Invalid Shop Billing
        
            RAISERROR  ('1|Invalid Shop Billing', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Shop Billing
    
        RAISERROR  ('1|Invalid Shop Billing', 16, 1, @ProcName)
        RETURN
    END

    IF @DataReviewUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@DataReviewUserID, @ApplicationCD, null) = 0)
        BEGIN
           -- Invalid Data Reviewer
        
            RAISERROR  ('1|Invalid Data Reviewer', 16, 1, @ProcName)
            RETURN
        END
    END

    IF @PreferredCommunicationMethodID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT CommunicationMethodID FROM utb_communication_method WHERE CommunicationMethodID = @PreferredCommunicationMethodID
                                                                                    AND EnabledFlag = 1)
        BEGIN
           -- Invalid Communication Method
        
            RAISERROR  ('1|Invalid Communication Method', 16, 1, @ProcName)
            RETURN
        END
    END

    IF @PreferredEstimatePackageID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT EstimatePackageID FROM utb_estimate_package WHERE EstimatePackageID = @PreferredEstimatePackageID
                                                                            AND EnabledFlag = 1)
        BEGIN
           -- Invalid Estimate Package
        
            RAISERROR  ('1|Invalid Estimate Package', 16, 1, @ProcName)
            RETURN
        END
    END

    IF @ProgramManagerUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@ProgramManagerUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid Program Manager
        
            RAISERROR  ('1|Invalid Program Manager', 16, 1, @ProcName)
            RETURN
        END
    END
    ELSE
    BEGIN
        IF @ProgramFlag = 1
        BEGIN
           -- Missing Program Manager
        
            RAISERROR  ('1|Missing Program Manager', 16, 1, @ProcName)
            RETURN
        END
    END

    IF @ShopID IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT ShopLocationID FROM utb_shop_location WHERE ShopLocationID = @ShopID
                                                     AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop
        
            RAISERROR  ('101|%s|@ShopID|%n', 16, 1, @ProcName, @ShopID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid Shop
    
        RAISERROR  ('101|%s|@ShopID|NULL', 16, 1, @ProcName)
        RETURN
    END

    IF @AddressState IS NOT NULL
    BEGIN
        IF NOT EXISTS (SELECT StateCode FROM utb_state_code WHERE StateCode = @AddressState
                                                              AND EnabledFlag = 1)
        BEGIN
           -- Invalid Shop Location Address State
        
            RAISERROR  ('1|Invalid Shop Location Address State', 16, 1, @ProcName)
            RETURN
        END
        
        /*IF @AddressState IN ('NY')
        BEGIN
            IF @DMVFacility IS NULL
            BEGIN
               RAISERROR  ('1|Missing DMV Facility # for NY State', 16, 1, @ProcName)
               RETURN
            END

            IF @SalesTaxNumber IS NULL
            BEGIN
               RAISERROR  ('1|Missing Sales Tax # for NY State', 16, 1, @ProcName)
               RETURN
            END
        END*/
    END

    -- Validate the updated date parameter
    IF @MergeFlag = 0
    BEGIN
      EXEC uspUtilityCheckLastUpdDate @SysLastUpdatedDate, @SysLastUserID, 'utb_shop_location', @ShopID

      IF @@error <> 0
      BEGIN
          RETURN
      END
    END
    
    -- Convert the value passed in updated date into data format
    
    SELECT @tupdated_date = CONVERT(DATETIME, @SysLastUpdatedDate)
    
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    
    IF @SysLastUserID IS NOT NULL
    BEGIN
        IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
        BEGIN
           -- Invalid User
        
            RAISERROR  ('101|%s|@SysLastUserID|%n', 16, 1, @ProcName, @SysLastUserID)
            RETURN
        END
    END
    ELSE
    BEGIN
       -- Invalid User
    
        RAISERROR  ('101|%s|@SysLastUserID|NULL', 16, 1, @ProcName)
        RETURN
    END

    -- Begin Update(s)

    BEGIN TRANSACTION AdmShopLocationUpdDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
    --- Audit Log Code ---------------------------------------------------------------------------------------
    DECLARE @LogComment                           udt_std_desc_long
    DECLARE @oldValue                             udt_std_desc_long
    DECLARE @newValue                             udt_std_desc_long
    
        
    DECLARE @dbDataReviewUserID                   udt_std_id,
            @dbPreferredCommunicationMethodID     udt_std_int_tiny,
            @dbPreferredEstimatePackageID         udt_std_int_tiny,
            @dbProgramManagerUserID               udt_std_id,
            @dbBusinessInfoID                     udt_std_id_big,
            @dbAddress1                           udt_addr_line_1,
            @dbAddress2                           udt_addr_line_2,
            @dbAddressCity                        udt_addr_city,
            @dbAddressCounty                      udt_addr_county,
            @dbAddressState                       udt_addr_state,
            @dbAddressZip                         udt_addr_zip_code,
            @dbAutoVerseId                        udt_std_desc_mid,
            @dbAvailableForSelectionFlag          udt_std_flag,
            @dbCertifiedFirstFlag                 udt_std_flag,
            @dbCertifiedFirstId                   udt_std_desc_short,
            @dbDataReviewDate                     VARCHAR(30),
            @dbDataReviewStatusCD                 udt_std_cd,
            @dbDMVFacility                        udt_std_desc_short,
            @dbDrivingDirections                  udt_std_desc_long,
            @dbEmailAddress                       udt_web_email,
            @dbEnabledFlag                        udt_enabled_flag,
            @dbFaxAreaCode                        udt_ph_area_code,
            @dbFaxExchangeNumber                  udt_ph_exchange_number,
            @dbFaxExtensionNumber                 udt_ph_extension_number,
            @dbFaxUnitNumber                      udt_ph_unit_number,
            @dbLatitude                           udt_addr_latitude,
            @dbLongitude                          udt_addr_longitude,
            @dbMaxWeeklyAssignments               udt_std_int_tiny,
            @dbName                               udt_std_name,
            @dbPhoneAreaCode                      udt_ph_area_code,
            @dbPhoneExchangeNumber                udt_ph_exchange_number,
            @dbPhoneExtensionNumber               udt_ph_extension_number,
            @dbPhoneUnitNumber                    udt_ph_unit_number,
            @dbPPGCTSCustomerFlag                 udt_std_flag,
            @dbPPGCTSFlag                         udt_std_flag,
            @dbPPGCTSId                           udt_std_desc_short,
            @dbPPGCTSLevelCD                      udt_std_cd,
            @dbPreferredCommunicationAddress      udt_web_address,
            @dbProgramFlag                        udt_std_flag,
			@dbReferralFlag                       udt_std_flag,
            @dbProgramScore                       udt_std_int_tiny,
            @dbRegistrationNumber                 udt_std_desc_short,  
            @dbRegistrationExpDate                varchar(30),  
            @dbSalesTaxNumber                     udt_std_desc_short,
            @dbWebSiteAddress                     udt_web_address,
            @dbWarrantyPeriodRefinishCD           udt_std_cd,
            @dbWarrantyPeriodWorkmanshipCD        udt_std_cd,
            

            -- Shop Hours Parms
            @dbOperatingMondayStartTime           udt_std_time,
            @dbOperatingMondayEndTime             udt_std_time,
            @dbOperatingTuesdayStartTime          udt_std_time,
            @dbOperatingTuesdayEndTime            udt_std_time,
            @dbOperatingWednesdayStartTime        udt_std_time,
            @dbOperatingWednesdayEndTime          udt_std_time,
            @dbOperatingThursdayStartTime         udt_std_time,
            @dbOperatingThursdayEndTime           udt_std_time,
            @dbOperatingFridayStartTime           udt_std_time,
            @dbOperatingFridayEndTime             udt_std_time,
            @dbOperatingSaturdayStartTime         udt_std_time,
            @dbOperatingSaturdayEndTime           udt_std_time,
            @dbOperatingSundayStartTime           udt_std_time,
            @dbOperatingSundayEndTime             udt_std_time,
            @dbShopHoursSysLastUserID             udt_std_id,
            @dbShopHoursSysLastUpdatedDate        VARCHAR(30)
            
    
     SELECT @dbDataReviewUserID = DataReviewUserID,
            @dbPreferredCommunicationMethodID = PreferredCommunicationMethodID,
            @dbPreferredEstimatePackageID = PreferredEstimatePackageID,
            @dbProgramManagerUserID = ProgramManagerUserID,
            @dbAddress1 = Address1,
            @dbAddress2 = Address2,
            @dbAddressCity = AddressCity,
            @dbAddressCounty = AddressCounty,
            @dbAddressState = AddressState,
            @dbAddressZip = AddressZip,
            @dbAutoVerseId = AutoVerseId,
            @dbAvailableForSelectionFlag = AvailableForSelectionFlag,
            @dbCertifiedFirstFlag = CertifiedFirstFlag,
            @dbCertifiedFirstId = CertifiedFirstId,
            @dbDataReviewDate = DataReviewDate,
            @dbDataReviewStatusCD = DataReviewStatusCD,
            @dbDMVFacility = DMVFacilityNumber,
            @dbDrivingDirections = DrivingDirections,
            @dbEmailAddress = EmailAddress,
            @dbEnabledFlag = EnabledFlag,
            @dbFaxAreaCode = FaxAreaCode,
            @dbFaxExchangeNumber = FaxExchangeNumber,
            @dbFaxExtensionNumber = FaxExtensionNumber,
            @dbFaxUnitNumber = FaxUnitNumber,
            @dbLatitude = Latitude,
            @dbLongitude = Longitude,
            @dbMaxWeeklyAssignments = MaxWeeklyAssignments,
            @dbName = Name,
            @dbPhoneAreaCode = PhoneAreaCode,
            @dbPhoneExchangeNumber = PhoneExchangeNumber,
            @dbPhoneExtensionNumber = PhoneExtensionNumber,
            @dbPhoneUnitNumber = PhoneUnitNumber,
            @dbPPGCTSCustomerFlag = PPGCTSCustomerFlag,
            @dbPPGCTSFlag = PPGCTSFlag,
            @dbPPGCTSId = PPGCTSId,
            @dbPPGCTSLevelCD = PPGCTSLevelCD,
            @dbPreferredCommunicationAddress = PreferredCommunicationAddress,
            @dbProgramFlag = ProgramFlag,
            @dbReferralFlag = ReferralFlag,
            @dbProgramScore = ProgramScore,
            @dbRegistrationNumber = RegistrationNumber,
            @dbRegistrationExpDate = RegistrationExpDate,
            @dbSalesTaxNumber = SalesTaxNumber,
            @dbWarrantyPeriodRefinishCD = WarrantyPeriodRefinishCD,
            @dbWarrantyPeriodWorkmanshipCD = WarrantyPeriodWorkmanshipCD,
            @dbWebSiteAddress = WebSiteAddress
     FROM dbo.utb_shop_location
     WHERE ShopLocationID = @ShopID
    
       
    
    IF ISNULL(@dbDataReviewUserID, 0) <> ISNULL(@dbDataReviewUserID, 0)
    BEGIN
      SET @oldValue = (SELECT ISNULL((NameFirst + ' ' + NameLast), 'UNDEFINED') 
                       FROM dbo.utb_user
                       WHERE UserID = @dbDataReviewUserID)
                       
      SET @newValue = (SELECT ISNULL((NameFirst + ' ' + NameLast), 'UNDEFINED') 
                       FROM dbo.utb_user 
                       WHERE UserID = @DataReviewUserID)
    
    
      SET @LogComment = 'Data Reviewed by (non-display) was changed from ' + 
                        ISNULL(@oldValue, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@newValue, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
        
    
    
    IF ISNULL(@dbPreferredCommunicationMethodID, 0) <> ISNULL(@PreferredCommunicationMethodID, 0)
    BEGIN
      SET @oldValue = (SELECT ISNULL((Name), 'UNDEFINED') 
                       FROM dbo.utb_communication_method
                       WHERE CommunicationMethodID = @dbPreferredCommunicationMethodID)
                       
      SET @newValue = (SELECT ISNULL((Name), 'UNDEFINED') 
                       FROM dbo.utb_communication_method 
                       WHERE CommunicationMethodID = @PreferredCommunicationMethodID)
    
    
      SET @LogComment = 'Preferred Communication Method was changed from ' + 
                        ISNULL(@oldValue, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@newValue, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    
    IF ISNULL(@dbPreferredEstimatePackageID, 0) <> ISNULL(@dbPreferredEstimatePackageID, 0)
    BEGIN
      SET @oldValue = (SELECT ISNULL((Name), 'UNDEFINED') 
                       FROM dbo.utb_estimate_package 
                       WHERE EstimatePackageID = @dbPreferredEstimatePackageID)
                       
      SET @newValue = (SELECT ISNULL((Name), 'UNDEFINED') 
                       FROM dbo.utb_estimate_package 
                       WHERE EstimatePackageID = @PreferredEstimatePackageID)
    
    
      SET @LogComment = 'Preferred Estimate Package was changed from ' + 
                        ISNULL(@oldValue, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@newValue, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  


    
    IF ISNULL(@dbProgramManagerUserID, 0) <> ISNULL(@ProgramManagerUserID, 0)
    BEGIN
      SET @oldValue = (SELECT ISNULL((NameFirst + ' ' + NameLast), 'UNDEFINED') 
                       FROM dbo.utb_user 
                       WHERE UserID = @dbProgramManagerUserID)
                       
      SET @newValue = (SELECT ISNULL((NameFirst + ' ' + NameLast), 'UNDEFINED') 
                       FROM dbo.ufnUtilityProgramManagersGetList() 
                       WHERE UserID = @ProgramManagerUserID)
    
    
      SET @LogComment = 'Program Manager was changed from ' + 
                        ISNULL(@oldValue, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@newValue, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  



    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddress1, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Address1, ''))))
    BEGIN
      SET @LogComment = 'Address 1 was changed from ' + 
                        ISNULL(@dbAddress1, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@Address1, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    

    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddress2, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Address2, ''))))
    BEGIN
      SET @LogComment = 'Address 2 was changed from ' + 
                        ISNULL(@dbAddress2, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@Address2, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressCity, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressCity, ''))))
    BEGIN
      SET @LogComment = 'City was changed from ' + 
                        ISNULL(@dbAddressCity, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@AddressCity, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END   
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressCounty, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressCounty, ''))))
    BEGIN
      SET @LogComment = 'County was changed from ' + 
                        ISNULL(@dbAddressCounty, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@AddressCounty, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END      
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressState, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressState, ''))))
    BEGIN
      SET @LogComment = 'State was changed from ' + 
                        ISNULL(convert(varchar(12), @dbAddressState), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @AddressState), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAddressZip, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AddressZip, ''))))
    BEGIN
      SET @LogComment = 'Zip Code was changed from ' + 
                        ISNULL(convert(varchar(12), @dbAddressZip), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @AddressZip), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
      
    IF @dbAvailableForSelectionFlag <> @AvailableForSelectionFlag
    BEGIN
      SET @LogComment = (SELECT CASE @dbAvailableForSelectionFlag
                           WHEN 0 THEN 'Shop Not Available for Selection' 
                           ELSE 'Shop Available for Selection' 
                         END)
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
     END  




    
    IF @dbCertifiedFirstFlag <> @dbCertifiedFirstFlag
    BEGIN
      SET @LogComment = (SELECT CASE @dbCertifiedFirstFlag 
                           WHEN 0 THEN 'CertifiedFirst participation was activated' 
                           ELSE 'CertifiedFirst participation was deactivated' 
                         END)
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbCertifiedFirstId, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@CertifiedFirstId, ''))))
    BEGIN
      SET @LogComment = 'CertifiedFirst IS changed from ' + 
                        ISNULL(@dbCertifiedFirstId, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@CertifiedFirstId, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    
    /*
    IF ISNULL(@dbDataReviewDate, '1/1/1900') <> ISNULL(@DataReviewDate, '1/1/1900')
    BEGIN
      SET @LogComment = 'Data Review Date changed from ' + 
                        ISNULL(convert(varchar(30), @DataReviewDate), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(30), @dbDataReviewDate), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    */
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbDataReviewStatusCD, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@DataReviewStatusCD, ''))))
    BEGIN
    
      SET @oldValue = ISNULL((SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'DataReviewStatusCD')
                                          WHERE Code = @dbDataReviewStatusCD), 'NON-EXISTENT') 
                                          
                                          
      SET @newValue = ISNULL((SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'DataReviewStatusCD')
                                          WHERE Code = @DataReviewStatusCD), 'NON-EXISTENT')  
            
      
      SET @LogComment = 'Data Review Status changed from ' + @oldValue + ' to ' + @newValue
      
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbDrivingDirections, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@DrivingDirections, ''))))
    BEGIN
      SET @LogComment = LEFT('Driving Directions changed from ' + 
                        ISNULL(@dbDrivingDirections, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@DrivingDirections, 'NON-EXISTENT'), 500)
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbEmailAddress, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@EmailAddress, ''))))
    BEGIN
      SET @LogComment = 'Email Address changed from ' + 
                        ISNULL(@dbEmailAddress, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@EmailAddress, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    
    
    IF @dbEnabledFlag <> @EnabledFlag
    BEGIN
      SET @LogComment = (SELECT CASE @dbEnabledFlag WHEN 0 THEN 'Shop was undeleted' ELSE 'Shop was deleted' END)
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Fax Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Fax Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Fax Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbFaxUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@FaxUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Fax Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbFaxUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @FaxUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF @dbLatitude <> @Latitude
    BEGIN
      SET @LogComment = 'Latitude changed from ' + 
                        ISNULL(CONVERT(varchar(25), @dbLatitude), 'NON-EXISTENT') + ' to ' + 
                        ISNULL(CONVERT(varchar(25), @Latitude), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF @dbLongitude <> @Longitude
    BEGIN
      SET @LogComment = 'Longitude changed from ' + 
                        ISNULL(CONVERT(varchar(25), @dbLongitude), 'NON-EXISTENT') + ' to ' + 
                        ISNULL(CONVERT(varchar(25), @Longitude), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF @dbMaxWeeklyAssignments <> @dbMaxWeeklyAssignments
    BEGIN
      SET @LogComment = 'Max Weekly Assignments changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbMaxWeeklyAssignments), 'NON-EXISTENT') + ' to ' + 
                        ISNULL(CONVERT(varchar(12), @MaxWeeklyAssignments), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbName, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@Name, ''))))
    BEGIN
      SET @LogComment = 'Name changed from ' + @dbName + ' to ' + @Name
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneAreaCode, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneAreaCode, ''))))
    BEGIN
      SET @LogComment = 'Phone Area Code changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneAreaCode), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneAreaCode), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExchangeNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExchangeNumber, ''))))
    BEGIN
      SET @LogComment = 'Phone Exchange Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExchangeNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExchangeNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneExtensionNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneExtensionNumber, ''))))
    BEGIN
      SET @LogComment = 'Phone Extension Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneExtensionNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneExtensionNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPhoneUnitNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PhoneUnitNumber, ''))))
    BEGIN
      SET @LogComment = 'Phone Unit Number changed from ' + 
                        ISNULL(CONVERT(varchar(12), @dbPhoneUnitNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(CONVERT(varchar(12), @PhoneUnitNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF @dbPPGCTSCustomerFlag <> @PPGCTSCustomerFlag
    BEGIN
      SET @LogComment = 'PPG CTS Customer changed from ' + (SELECT CASE @dbPPGCTSCustomerFlag WHEN 0 THEN 'OFF to ON' ELSE 'ON to OFF' END)
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
        
    
    IF @dbPPGCTSFlag <> @PPGCTSFlag
    BEGIN
      SET @LogComment = 'PPG CTS changed from ' + (SELECT CASE @dbPPGCTSFlag WHEN 0 THEN 'OFF to ON' ELSE 'ON to OFF' END)
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END    
    
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPPGCTSId, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PPGCTSId, ''))))
    BEGIN
      SET @LogComment = 'PPG CTS ID changed from ' + 
                        ISNULL(@dbPPGCTSId, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@PPGCTSId, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPPGCTSLevelCD, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PPGCTSLevelCD, ''))))
    BEGIN
      SET @oldValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'PPGCTSLevelCD') WHERE Code = @dbPPGCTSLevelCD)
      SET @newValue = (SELECT Name FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'PPGCTSLevelCD') WHERE Code = @PPGCTSLevelCD)
    
      SET @LogComment = 'PPG CTS Level changed from ' + 
                        ISNULL(@oldValue, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@newValue, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbPreferredCommunicationAddress, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@PreferredCommunicationAddress, ''))))
    BEGIN
      SET @LogComment = 'Preferred Communication Address changed from ' + 
                        ISNULL(@dbPreferredCommunicationAddress, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@PreferredCommunicationAddress, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END

    IF UPPER(LTRIM(RTRIM(ISNULL(@dbAutoVerseId, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@AutoVerseId, ''))))
    BEGIN
      SET @LogComment = 'AutoVerse ID changed from ' + 
                        ISNULL(@dbAutoVerseId, 'NON-EXISTENT') + ' to ' +
                        ISNULL(@AutoVerseId, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    IF @dbProgramFlag <> @ProgramFlag
    BEGIN
      SET @LogComment = 'Program Shop changed from ' + (SELECT CASE @dbProgramFlag WHEN 0 THEN 'OFF to ON' ELSE 'ON to OFF' END) 
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    
    IF @dbProgramScore <> @ProgramScore
    BEGIN
      SET @LogComment = 'Program Score changed from ' + ISNULL(CONVERT(varchar(12), @dbProgramScore), 'NON-EXISTENT') + ' to ' +
                                                        ISNULL(CONVERT(varchar(12), @ProgramScore), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    IF @dbRegistrationNumber <> @RegistrationNumber
    BEGIN
      SET @LogComment = 'Registration Number changed from ' + ISNULL(CONVERT(varchar(12), @dbRegistrationNumber), 'NON-EXISTENT') + ' to ' +
                                                        ISNULL(CONVERT(varchar(12), @RegistrationNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END

    IF @dbRegistrationExpDate <> @RegistrationExpDate
    BEGIN
      SET @LogComment = 'Registration Expiration date changed from ' + ISNULL(CONVERT(varchar(12), @dbRegistrationExpDate), 'NON-EXISTENT') + ' to ' +
                                                        ISNULL(CONVERT(varchar(12), @RegistrationExpDate), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbWarrantyPeriodRefinishCD, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@WarrantyPeriodRefinishCD,''))))
    BEGIN
      SET @oldValue = ISNULL((SELECT Name 
                              FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'WarrantyPeriodRefinishCD')
                              WHERE Code = @dbWarrantyPeriodRefinishCD), 'NON-EXISTENT')
                      
      SET @newValue = ISNULL((SELECT Name 
                              FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'WarrantyPeriodRefinishCD')
                              WHERE Code = @WarrantyPeriodRefinishCD), 'NON-EXISTENT')
    
      SET @LogComment = 'Refinish warranty changed from ' + @oldValue + ' to ' + @newValue
                         
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    
        
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbWarrantyPeriodWorkmanshipCD, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@WarrantyPeriodWorkmanshipCD, ''))))
    BEGIN
      SET @oldValue = ISNULL((SELECT Name 
                              FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'WarrantyPeriodWorkmanshipCD')
                              WHERE Code = @dbWarrantyPeriodWorkmanshipCD), 'NON-EXISTENT')
                      
      SET @newValue = ISNULL((SELECT Name 
                              FROM dbo.ufnUtilityGetReferenceCodes('utb_shop_location', 'WarrantyPeriodWorkmanshipCD')
                              WHERE Code = @WarrantyPeriodWorkmanshipCD), 'NON-EXISTENT')
    
      SET @LogComment = 'Workmanship Warranty changed from ' + @oldValue + ' to ' + @newValue
                         
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
        
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbWebSiteAddress, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@WebSiteAddress, ''))))
    BEGIN
      SET @LogComment = 'Website Address changed from ' + ISNULL(@dbWebSiteAddress, 'NON-EXISTENT') + ' to ' + 
                                                          ISNULL(@WebSiteAddress, 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END
    
    IF UPPER(LTRIM(RTRIM(ISNULL(@dbDMVFacility, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@DMVFacility, ''))))
    BEGIN
      SET @LogComment = 'DMV Facility number was changed from ' + 
                        ISNULL(convert(varchar(12), @dbDMVFacility), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @DMVFacility), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  

    IF UPPER(LTRIM(RTRIM(ISNULL(@dbSalesTaxNumber, '')))) <> UPPER(LTRIM(RTRIM(ISNULL(@SalesTaxNumber, ''))))
    BEGIN
      SET @LogComment = 'Sales Tax Number was changed from ' + 
                        ISNULL(convert(varchar(12), @dbSalesTaxNumber), 'NON-EXISTENT') + ' to ' +
                        ISNULL(convert(varchar(12), @SalesTaxNumber), 'NON-EXISTENT')
    
      EXEC dbo.uspAuditLogInsDetail 'P', NULL, @dbName, @ShopID, @LogComment, @SysLastUserID
           
      IF @@ERROR <> 0
      BEGIN
         -- SQL Server Error

          ROLLBACK TRANSACTION
          RETURN
      END  
    END  
       
    
    -- End of Audit Log Code --------------------------------------------------------------------------------
    

    UPDATE  dbo.utb_shop_location 
    SET     BillingID = @BillingID,
            DataReviewUserID = @DataReviewUserID,
            PreferredCommunicationMethodID = @PreferredCommunicationMethodID,
            PreferredEstimatePackageID = @PreferredEstimatePackageID,
            ProgramManagerUserID = @ProgramManagerUserID,
            ShopID = @BusinessInfoID,
            Address1 = @Address1,
            Address2 = @Address2,
            AddressCity = @AddressCity,
            AddressCounty = @AddressCounty,
            AddressState = @AddressState,
            AddressZip = @AddressZip,
            AutoVerseId = @AutoVerseId,
            AvailableForSelectionFlag = @AvailableForSelectionFlag,
            CertifiedFirstFlag = @CertifiedFirstFlag,
            CertifiedFirstId = @CertifiedFirstId,
            DataReviewDate = @DataReviewDate,
            DataReviewStatusCD = @DataReviewStatusCD,
            DMVFacilityNumber = @DMVFacility,
            DrivingDirections = @DrivingDirections,
            EmailAddress = @EmailAddress,
            EnabledFlag = @EnabledFlag,
            FaxAreaCode = @FaxAreaCode,
            FaxExchangeNumber = @FaxExchangeNumber,
            FaxExtensionNumber = @FaxExtensionNumber,
            FaxUnitNumber = @FaxUnitNumber,
            Latitude = @Latitude,
            Longitude = @Longitude,
            MaxWeeklyAssignments = @MaxWeeklyAssignments,
            Name = @Name,
            PhoneAreaCode = @PhoneAreaCode,
            PhoneExchangeNumber = @PhoneExchangeNumber,
            PhoneExtensionNumber = @PhoneExtensionNumber,
            PhoneUnitNumber = @PhoneUnitNumber,
            PPGCTSCustomerFlag = @PPGCTSCustomerFlag,
            PPGCTSFlag = @PPGCTSFlag,
            PPGCTSId = @PPGCTSId,
            PPGCTSLevelCD = @PPGCTSLevelCD,
            PreferredCommunicationAddress = @PreferredCommunicationAddress,
            ProgramFlag = @ProgramFlag,
            ReferralFlag = @ReferralFlag,
            ProgramScore = @ProgramScore,
            RegistrationExpDate = @RegistrationExpDate,
            RegistrationNumber = @RegistrationNumber,
            SalesTaxNumber = @SalesTaxNumber,
            WarrantyPeriodRefinishCD = @WarrantyPeriodRefinishCD,
            WarrantyPeriodWorkmanshipCD = @WarrantyPeriodWorkmanshipCD,
            WebSiteAddress = @WebSiteAddress,
            SysLastUserID = @SysLastUserID,
            SysLastUpdatedDate = CURRENT_TIMESTAMP
      WHERE ShopLocationID = @ShopID
      
            
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    
    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('105|%s|utb_shop_location', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    
    END
    

    -- Call ShopHours update or insert proc depending on the current existence of a ShopHours record for this Shop
    IF (@ShopHoursSysLastUpdatedDate is NULL) AND NOT (@OperatingMondayStartTime IS NULL and @OperatingMondayEndTime IS NULL and
                                                       @OperatingTuesdayStartTime IS NULL and @OperatingTuesdayEndTime IS NULL and
                                                       @OperatingWednesdayStartTime IS NULL and @OperatingWednesdayEndTime IS NULL and
                                                       @OperatingThursdayStartTime IS NULL and @OperatingThursdayEndTime IS NULL and
                                                       @OperatingFridayStartTime IS NULL and @OperatingFridayEndTime IS NULL and
                                                       @OperatingSaturdayStartTime IS NULL and @OperatingSaturdayEndTime IS NULL and
                                                       @OperatingSundayStartTime IS NULL and @OperatingSundayEndTime IS NULL)
    BEGIN
        -- insert new ShopHours record
        exec uspSMTShopHoursInsDetail @ShopID, @OperatingMondayStartTime, @OperatingMondayEndTime, @OperatingTuesdayStartTime, @OperatingTuesdayEndTime,
                                      @OperatingWednesdayStartTime, @OperatingWednesdayEndTime, @OperatingThursdayStartTime, @OperatingThursdayEndTime,
                                      @OperatingFridayStartTime, @OperatingFridayEndTime, @OperatingSaturdayStartTime, @OperatingSaturdayEndTime,
                                      @OperatingSundayStartTime, @OperatingSundayEndTime, @ShopHoursSysLastUserID
        
        SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
        -- Check error value
        IF @error <> 0
        BEGIN
            -- SQL Server Error
            ROLLBACK TRANSACTION
            RETURN
        END
    END
    ELSE IF (@ShopHoursSysLastUpdatedDate IS NOT NULL)
    BEGIN
      
      -- update existing ShopHours record
      EXEC uspSMTShopHoursUpdDetail @ShopID, @OperatingMondayStartTime, @OperatingMondayEndTime, @OperatingTuesdayStartTime, @OperatingTuesdayEndTime,
                                    @OperatingWednesdayStartTime, @OperatingWednesdayEndTime, @OperatingThursdayStartTime, @OperatingThursdayEndTime,
                                    @OperatingFridayStartTime, @OperatingFridayEndTime, @OperatingSaturdayStartTime, @OperatingSaturdayEndTime,
                                    @OperatingSundayStartTime, @OperatingSundayEndTime, @ShopHoursSysLastUserID, @ShopHoursSysLastUpdatedDate,
                                    'APD', @MergeFlag
      
      
      SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
  
      -- Check error value
      IF @error <> 0
      BEGIN
        -- SQL Server Error
        ROLLBACK TRANSACTION
        RETURN
      END
    END


    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    
    COMMIT TRANSACTION AdmShopLocationUpdDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END

    SELECT @ShopID AS ShopID

    RETURN @rowcount

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopInfoUpdDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopInfoUpdDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/