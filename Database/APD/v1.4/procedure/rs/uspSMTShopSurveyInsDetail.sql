-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopSurveyInsDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSMTShopSurveyInsDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



/************************************************************************************************************************
*
* PROCEDURE:    uspSMTShopSurveyInsDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Dan Price
* FUNCTION:     Insert a new Shop Pricing record
*
*
* RESULT SET:
* ShopID                                  The newly created Shop unique identity
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspSMTShopSurveyInsDetail
(
 @ShopID                                  udt_std_id_big,
 @Age                                     varchar(10)=null,
 @ASEFlag                                 udt_std_flag=null,
 @BusinessRegsFlag                        udt_std_flag=null,
 @BusinessStabilityFlag                   udt_std_flag=null,
 @CommercialLiabilityFlag                 udt_std_flag=null,
 @CompetentEstimatorsFlag                 udt_std_flag=null,
 @CustomaryHoursFlag                      udt_std_flag=null,
 @DigitalPhotosFlag                       udt_std_flag=null,
 @EFTFlag                                 udt_std_flag=null,
 @ElectronicEstimatesFlag                 udt_std_flag=null,
 @EmployeeEducationFlag                   udt_std_flag=null,
 @EmployerLiabilityFlag                   udt_std_flag=null,
 @FrameAnchoringPullingEquipFlag          udt_std_flag=null,
 @FrameDiagnosticEquipFlag                udt_std_flag=null,
 @HazMatFlag                              udt_std_flag=null,
 @HydraulicLiftFlag                       udt_std_flag=null,
 @ICARFlag                                udt_std_flag=null,
 @MechanicalRepairFlag                    udt_std_flag=null,
 @MIGWeldersFlag                          udt_std_flag=null,
 @MinimumWarrantyFlag                     udt_std_flag=null,
 @NoFeloniesFlag                          udt_std_flag=null,
 @ReceptionistFlag                        udt_std_flag=null,
 @RefinishCapabilityFlag                  udt_std_flag=null,
 @RefinishTrainedFlag                     udt_std_flag=null,
 @SecureStorageFlag                       udt_std_flag=null,
 @ValidEPALicenseFlag                     udt_std_flag=null,
 @VANInetFlag                             udt_std_flag=null,
 @WorkersCompFlag                         udt_std_flag=null,
 @SysLastUserID                           udt_std_int,
 @ApplicationCD                           udt_std_cd='APD' 
)

AS

BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @error    AS INT
    DECLARE @rowcount AS INT
    DECLARE @now      AS datetime

    DECLARE @ProcName          AS VARCHAR(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspSMTShopSurveyInsDetail'


    -- Apply edits
    
    
    IF (dbo.ufnUtilityIsUserActive(@SysLastUserID, @ApplicationCD, default) = 0)
    BEGIN
       -- Invalid User
        RAISERROR('101|%s|@SysLastUserID|%u', 16, 1, @ProcName, @SysLastUserID)
        RETURN
    END
   
   
    -- initialize any empty string parameters
    IF (LEN(LTRIM(RTRIM(@Age))) = 0) SET @Age = NULL
    IF (LEN(LTRIM(RTRIM(@ASEFlag))) = 0) SET @ASEFlag = NULL
    IF (LEN(LTRIM(RTRIM(@HydraulicLiftFlag))) = 0) SET @HydraulicLiftFlag = NULL
    IF (LEN(LTRIM(RTRIM(@ICARFlag))) = 0) SET @ICARFlag = NULL
    IF (LEN(LTRIM(RTRIM(@RefinishTrainedFlag))) = 0) SET @RefinishTrainedFlag = NULL
    IF (LEN(LTRIM(RTRIM(@SecureStorageFlag))) = 0) SET @SecureStorageFlag = NULL
    IF (LEN(LTRIM(RTRIM(@ValidEPALicenseFlag))) = 0) SET @ValidEPALicenseFlag = NULL
    
    
    
    SET @now = CURRENT_TIMESTAMP

    -- Begin Update(s)

    BEGIN TRANSACTION ShopSurveyInsDetailTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    INSERT INTO dbo.utb_shop_location_survey
    (
        ShopLocationID,              
        Age,
        ASEFlag,
        BusinessRegsFlag,
        BusinessStabilityFlag,
        CommercialLiabilityFlag,
        CompetentEstimatorsFlag,
        CustomaryHoursFlag,
        DigitalPhotosFlag,
        EFTFlag,
        ElectronicEstimatesFlag,
        EmployeeEducationFlag,
        EmployerLiabilityFlag,
        FrameAnchoringPullingEquipFlag,
        FrameDiagnosticEquipFlag,
        HazMatFlag,
        HydraulicLiftFlag,
        ICARFlag,
        MechanicalRepairFlag,
        MIGWeldersFlag,
        MinimumWarrantyFlag,
        NoFeloniesFlag,
        ReceptionistFlag,
        RefinishCapabilityFlag,
        RefinishTrainedFlag,
        SecureStorageFlag,
        ValidEPALicenseFlag,
        VANInetFlag,
        WorkersCompFlag,
        SysLastUserID,
        SysLastUpdatedDate          
    )
    VALUES  
    (
        @ShopID,                
        @Age,
        @ASEFlag,
        @BusinessRegsFlag,
        @BusinessStabilityFlag,
        @CommercialLiabilityFlag,
        @CompetentEstimatorsFlag,
        @CustomaryHoursFlag,
        @DigitalPhotosFlag,
        @EFTFlag,
        @ElectronicEstimatesFlag,
        @EmployeeEducationFlag,
        @EmployerLiabilityFlag,
        @FrameAnchoringPullingEquipFlag,
        @FrameDiagnosticEquipFlag,
        @HazMatFlag,
        @HydraulicLiftFlag,
        @ICARFlag,
        @MechanicalRepairFlag,
        @MIGWeldersFlag,
        @MinimumWarrantyFlag,
        @NoFeloniesFlag,
        @ReceptionistFlag,
        @RefinishCapabilityFlag,
        @RefinishTrainedFlag,
        @SecureStorageFlag,
        @ValidEPALicenseFlag,
        @VANInetFlag,
        @WorkersCompFlag,
        @SysLastUserID,      
        @now
    )
    
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT

    -- Check error value
    
    IF @error <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('105|%s|utb_shop_location_survey', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    COMMIT TRANSACTION ShopSurveyInsDetailTran1    

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

       -- Create XML Document to return new updated date time
    SELECT  1 AS tag,
            NULL AS parent,
            NULL AS [Root!1!Root],
            -- Insured Level
            NULL AS [Survey!2!SysLastUpdatedDate]


    UNION ALL


    SELECT  2,
            1,
            NULL,
            -- Insured Level
            dbo.ufnUtilityGetDateString( @now )


    ORDER BY tag
--    FOR XML EXPLICIT      -- Comment for Client-side processing

IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    return @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTShopSurveyInsDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTShopSurveyInsDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/