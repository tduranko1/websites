-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWADocMissingGetList' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWADocMissingGetList 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWADocMissingGetList
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWADocMissingGetList
    @dteFrom    datetime = NULL
AS
BEGIN
    SET NOCOUNT ON
    
    DECLARE @dteTo datetime
    DECLARE @now datetime
    
    DECLARE @tmpClaimAspect TABLE  (
        ClaimAspectID       bigint,
        LynxID              bigint,
        CreatedDate         datetime
    )
    
    SET @now = CURRENT_TIMESTAMP
    
    IF @dteFrom IS NULL
    BEGIN
        SET @dteFrom = convert(datetime, convert(varchar, @now, 101))
    END
    
    SET @dteTo = convert(datetime, convert(varchar, @now, 101) + ' 23:59:59')

    -- Get the Claimaspects with the missing client assignment document
    INSERT INTO @tmpClaimAspect
    SELECT distinct casc.ClaimAspectID, ca.LynxID, ca.CreatedDate
    FROM dbo.utb_document d
    LEFT JOIN utb_claim_aspect_service_channel_document cascd ON (d.DocumentID = cascd.DocumentID)
    LEFT JOIN utb_claim_aspect_service_channel casc ON cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    LEFT JOIN utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
    WHERE d.DocumentTypeID = 1
      AND casc.createdDate BETWEEN @dteFrom AND @dteTo
      
    -- now add those claimAspects that came in together with other claimAspect (CreatedDate will be the same)
    
    INSERT INTO @tmpClaimAspect    
    SELECT distinct ca.ClaimAspectID, ca.LynxID, ca.CreatedDate
    FROM utb_claim_aspect CA, @tmpClaimAspect tmp
    where CA.LynxID = tmp.LynxID
      and ca.ClaimAspectID <> tmp.ClaimAspectID
      and ca.CreatedDate = tmp.CreatedDate
      and ca.ClaimAspectTypeID = 9
    
    -- now do the final select
    SELECT ca.LynxID, c.InsuranceCompanyID, ca.ClaimAspectNumber, ca.createdDate
    FROM dbo.utb_claim_aspect ca
    LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
    WHERE claimAspectTypeID = 9
      AND createdDate between @dteFrom AND @dteTo
      AND claimAspectID not in (SELECT ClaimAspectID FROM @tmpClaimAspect)
      AND SourceApplicationID in (SELECT ApplicationID FROM dbo.utb_application WHERE code in ('CP', 'FNOL'))
                              
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWADocMissingGetList' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWADocMissingGetList TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
