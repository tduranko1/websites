USE udb_apd

IF NOT EXISTS(
  SELECT *
  FROM INFORMATION_SCHEMA.COLUMNS
  WHERE 
    TABLE_NAME = 'utb_insurance'
    AND COLUMN_NAME = 'FeeCreditFlag')
BEGIN
  ALTER TABLE utb_insurance
    ADD FeeCreditFlag udt_std_flag NOT NULL DEFAULT(0)

  SELECT 'FeeCreditFlag field added...'
END
ELSE
BEGIN
  SELECT 'FeeCreditFlag field ALREADY added...'
END

