-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClaimRepIDByLynxID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspGetClaimRepIDByLynxID 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspGetClaimRepIDByLynxID
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Gets the claim rep ID assigned to the claim by LynxID
*
* PARAMETERS:  
*				LynxID
* RESULT SET:
*				ClaimRepID
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspGetClaimRepIDByLynxID
	@LynxID INT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @error AS int
    DECLARE @rowcount AS int
    
    DECLARE @now               AS datetime 

    -- Set Database options
    
    SET NOCOUNT ON

	SELECT
		CarrierRepUserID 
		, SysLastUpdateddate
	FROM
		utb_claim 
	WHERE
		LynxID = @LynxID
		
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspGetClaimRepIDByLynxID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspGetClaimRepIDByLynxID TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspGetClaimRepIDByLynxID TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/