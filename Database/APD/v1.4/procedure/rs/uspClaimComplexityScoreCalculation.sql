-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimComplexityScoreCalculation' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspClaimComplexityScoreCalculation 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspClaimComplexityScoreCalculation
* SYSTEM:       Lynx Services APD
* AUTHOR:       James Stein
* FUNCTION:     Compute a specific Claim Complexity Score
*
* PARAMETERS:  
* (I) @LynxID                   The claim's Lynx ID whose complexity score is to be calculated
*
* RESULT SET:
* LynxID                        The claim's Lynx ID
* ComplexityScore               The complexity score for the designated claim's Lynx ID
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspClaimComplexityScoreCalculation
(
    @LynxID         udt_std_int_big
)
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    
    -- Declare internal variables

    DECLARE @ComplexityScore AS udt_std_int_small
    DECLARE @ClaimTypeScore AS udt_std_int_small
    DECLARE @CoverageScore AS udt_std_int_small
    DECLARE @VehicleCount AS udt_std_int_small
    DECLARE @PropertyCount AS udt_std_int_small
    DECLARE @NonDrivableCount AS udt_std_int_small
    DECLARE @UnderwritingScore AS udt_std_int_small
    DECLARE @InjuredCount AS udt_std_int_small
    DECLARE @NegligenceTypeScore AS udt_std_int_small
    DECLARE @InjuryTypeScore AS udt_std_int_small
    DECLARE @LossTypeScore AS udt_std_int_small
    DECLARE @RoadLocationScore AS udt_std_int_small
    
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @debug AS BIT
    
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ComplexityScore = 0
    SET @ProcName = 'uspClaimComplexityScoreCalculation'
    SET @debug = 1

    
    -- Claim Complexity Factors: Claim Type (Collision, Comprehensive)

    -- TBD 
    SELECT @ClaimTypeScore = 0


    IF @debug = 1 PRINT ('Claim Complexity Factors: Claim Type Score = ' + CAST(@ClaimTypeScore AS VARCHAR(20)))

    SET @ComplexityScore = @ComplexityScore + @ClaimTypeScore

    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))


    -- Claim Complexity Factors: Coverages (Towing, Rental)

    -- TBD 
    SELECT @CoverageScore = 0


    IF @debug = 1 PRINT ('Claim Complexity Factors: Coverages Score = ' + CAST(@CoverageScore AS VARCHAR(20)))

    SET @ComplexityScore = @ComplexityScore + @CoverageScore

    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))


    -- Claim Complexity Factors: Number of Vehicles

    SELECT @VehicleCount = COUNT(ClaimAspectID)
      FROM dbo.utb_claim_aspect
     WHERE LynxID = @LynxID
       AND ClaimAspectTypeID = (SELECT ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name='Vehicle')
       AND EnabledFlag = 1
    
    IF (@VehicleCount IS NULL) SET @VehicleCount = 0
           
    IF @debug = 1 PRINT ('Claim Complexity Factors: Number of Vehicles = ' + CAST(@VehicleCount AS VARCHAR(20)))

    SELECT @ComplexityScore = @ComplexityScore + CASE WHEN cw.Value IS NULL THEN 0 ELSE @VehicleCount * cw.Value END
      FROM dbo.utb_complexity_weight AS cw
     INNER JOIN dbo.utb_claim_complexity AS cc
        ON cc.Description = 'Number of Vehicles'
       AND cc.ComplexityID = cw.ObjectRowID
     WHERE cw.ObjectId = OBJECT_ID('utb_claim_complexity')

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))

    
    -- Check error value
    
    IF @error <> 0 OR @rowcount <> 1
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END


    -- Claim Complexity Factors: Number of non-vehicle properties damaged

    SELECT @PropertyCount = COUNT(ClaimAspectID)
      FROM dbo.utb_claim_aspect
     WHERE LynxID = @LynxID
       AND ClaimAspectTypeID = (SELECT ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name='Property')
       AND EnabledFlag = 1
    
    IF (@PropertyCount IS NULL) SET @PropertyCount = 0
           
    IF @debug = 1 PRINT ('Claim Complexity Factors: Number of non-vehicle properties damaged = ' + CAST(@PropertyCount AS VARCHAR(20)))

    SELECT @ComplexityScore = @ComplexityScore + CASE WHEN cw.Value IS NULL THEN 0 ELSE @PropertyCount * cw.Value END
      FROM dbo.utb_complexity_weight AS cw
     INNER JOIN dbo.utb_claim_complexity AS cc
        ON cc.Description = 'Number of non-vehicle properties damaged'
       AND cc.ComplexityID = cw.ObjectRowID
     WHERE cw.ObjectId = OBJECT_ID('utb_claim_complexity')

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))

    
    -- Check error value
    
    IF @error <> 0 OR @rowcount <> 1
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END


    -- Claim Complexity Factors: Non Drivable

    SELECT @NonDrivableCount = COUNT(cv.ClaimAspectID)
      FROM dbo.utb_claim_aspect ca
      LEFT JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
     WHERE ca.LynxID = @LynxID
       AND ca.ClaimAspectTypeID = (SELECT ClaimAspectTypeID FROM dbo.utb_claim_aspect_type WHERE Name='Vehicle')
       AND ca.EnabledFlag = 1
       AND cv.DriveableFlag = 0

    IF (@NonDrivableCount IS NULL) SET @NonDrivableCount = 0
           
    IF @debug = 1 PRINT ('Claim Complexity Factors: Non Drivable = ' + CAST(@NonDrivableCount AS VARCHAR(20)))

    SELECT @ComplexityScore = @ComplexityScore + CASE WHEN cw.Value IS NULL THEN 0 ELSE @NonDrivableCount * cw.Value END
      FROM dbo.utb_complexity_weight AS cw
     INNER JOIN dbo.utb_claim_complexity AS cc
        ON cc.Description = 'Non Drivable'
       AND cc.ComplexityID = cw.ObjectRowID
     WHERE cw.ObjectId = OBJECT_ID('utb_claim_complexity')

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))


    -- Check error value
    
    IF @error <> 0 OR @rowcount <> 1
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END


    -- Claim Complexity Factors: Insurance company underwriting

    -- TBD 
    SELECT @UnderwritingScore = 0


    IF @debug = 1 PRINT ('Claim Complexity Factors: Insurance company underwriting Score = ' + CAST(@UnderwritingScore AS VARCHAR(20)))

    SET @ComplexityScore = @ComplexityScore + @UnderwritingScore

    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))


    -- Claim Complexity Factors: Number of injured

    SELECT @InjuredCount = COUNT(ii.InvolvedID)
      FROM dbo.utb_involved_injury AS ii
     WHERE EXISTS 
       -- Grab all the unique involved pedestrians, vehicle involved, and property involved...
       -- These are the only involved that may have injuries
           (SELECT  cai.InvolvedID
              FROM  dbo.utb_claim_aspect ca
              LEFT JOIN dbo.utb_claim_aspect_involved cai ON (ca.ClaimAspectID = cai.ClaimAspectID)
              WHERE ca.LynxID = @LynxID
                AND cai.EnabledFlag = 1
                AND cai.InvolvedID = ii.InvolvedID)

    IF (@InjuredCount IS NULL) SET @InjuredCount = 0
           
    IF @debug = 1 PRINT ('Claim Complexity Factors: Number of injured = ' + CAST(@InjuredCount AS VARCHAR(20)))

    SELECT @ComplexityScore = @ComplexityScore + CASE WHEN cw.Value IS NULL THEN 0 ELSE @InjuredCount * cw.Value END
      FROM dbo.utb_complexity_weight AS cw
     INNER JOIN dbo.utb_claim_complexity AS cc
        ON cc.Description = 'Number of injured'
       AND cc.ComplexityID = cw.ObjectRowID
     WHERE cw.ObjectId = OBJECT_ID('utb_claim_complexity')

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))

    
    -- Check error value
    
    IF @error <> 0 OR @rowcount <> 1
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END


    -- Claim Complexity Factors: Negligence Type

    SELECT @NegligenceTypeScore = CASE WHEN SUM(cw.Value) IS NULL THEN 0 ELSE SUM(cw.Value) END
      FROM dbo.utb_complexity_weight AS cw
     INNER JOIN dbo.utb_claim AS c
        ON c.LynxID = @LynxID
     INNER JOIN dbo.utb_state_code AS s
        ON c.LossState = s.StateCode
     INNER JOIN dbo.utb_claim_complexity AS cc
        ON cc.Description = 'Negligence Type'
       AND s.NegligenceTypeCD = cc.Code
       AND cc.ComplexityID = cw.ObjectRowID
     WHERE cw.ObjectId = OBJECT_ID('utb_claim_complexity')

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0 OR @rowcount <> 1
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @debug = 1 PRINT ('Claim Complexity Factors: Negligence Type Score = ' + CAST(@NegligenceTypeScore AS VARCHAR(20)))

    SET @ComplexityScore = @ComplexityScore + @NegligenceTypeScore

    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))


    -- Claim Complexity Factors: Injury Type
    
    SELECT @InjuryTypeScore = CASE WHEN SUM(cw.Value) IS NULL THEN 0 ELSE SUM(cw.Value) END
      FROM dbo.utb_complexity_weight AS cw
     INNER JOIN dbo.utb_involved_injury AS ii
        ON cw.ObjectRowID = ii.InjuryTypeID
     WHERE cw.ObjectId = OBJECT_ID('utb_injury_type')
       AND EXISTS 
       -- Grab all the unique involved pedestrians, vehicle involved, and property involved...
       -- These are the only involved that may have injuries
           (SELECT  cai.InvolvedID
              FROM  dbo.utb_claim_aspect ca
              LEFT JOIN dbo.utb_claim_aspect_involved cai ON (ca.ClaimAspectID = cai.ClaimAspectID)
              WHERE ca.LynxID = @LynxID
                AND cai.EnabledFlag = 1
                AND cai.InvolvedID = ii.InvolvedID)

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    -- Check error value
    
    IF @error <> 0 OR @rowcount <> 1
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @debug = 1 PRINT ('Claim Complexity Factors: Injury Type Score = ' + CAST(@InjuryTypeScore AS VARCHAR(20)))

    SET @ComplexityScore = @ComplexityScore + @InjuryTypeScore

    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))
    

    -- Claim Complexity Factors: Loss Type
    
    SELECT @LossTypeScore = CASE WHEN SUM(cw.Value) IS NULL THEN 0 ELSE SUM(cw.Value) END
      FROM dbo.utb_complexity_weight AS cw
     INNER JOIN dbo.utb_claim AS c
        ON c.LynxID = @LynxID
       AND cw.ObjectRowID = c.LossTypeID
     WHERE cw.ObjectId = OBJECT_ID('utb_loss_type')

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    
    
    -- Check error value
    
    IF @error <> 0 OR @rowcount <> 1
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @debug = 1 PRINT ('Claim Complexity Factors: Loss Type Score = ' + CAST(@LossTypeScore AS VARCHAR(20)))

    SET @ComplexityScore = @ComplexityScore + @LossTypeScore

    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))


    -- Claim Complexity Factors: Road Location
    
    SELECT @RoadLocationScore = CASE WHEN SUM(cw.Value) IS NULL THEN 0 ELSE SUM(cw.Value) END
      FROM dbo.utb_complexity_weight AS cw
     INNER JOIN dbo.utb_claim AS c
        ON c.LynxID = @LynxID
       AND cw.ObjectRowID = c.RoadLocationID
     WHERE cw.ObjectId = OBJECT_ID('utb_road_location')

    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT
    

    -- Check error value
    
    IF @error <> 0 OR @rowcount <> 1
    BEGIN
       -- SQL Server Error
    
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @debug = 1 PRINT ('Claim Complexity Factors: Road Location Score = ' + CAST(@RoadLocationScore AS VARCHAR(20)))

    SET @ComplexityScore = @ComplexityScore + @RoadLocationScore

    IF @debug = 1 PRINT ('Claim Complexity Cumulative Score = ' + CAST(@ComplexityScore AS VARCHAR(20)))


    -- Return final complexity score

    SELECT @LynxID AS LynxID, @ComplexityScore AS ComplexityScore

    RETURN 
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspClaimComplexityScoreCalculation' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspClaimComplexityScoreCalculation TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/