-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptClaimRegister' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptClaimRegister 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptClaimRegister
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Claim Register report
*
* PARAMETERS:  
* (I) @fromDate             Date from which the report starts
* (I) @toDate               Date to which the report ends
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptClaimRegister
(
    @RptFromDate       varchar(30) = NULL,
    @RptToDate         varchar(30) = NULL
)
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF

    declare @frmDate datetime
    declare @toDate  datetime
    declare @tmpDate datetime
    declare @ProcName varchar(50)

    set @ProcName = 'uspRptClaimRegister'

--    if @RptFromDate is null or len(@RptFromDate) = 0 set @RptFromDate = convert(varchar, GETDATE(), 101)
--    if @RptToDate is null or len(@RptToDate) = 0 set @RptToDate = convert(varchar, GETDATE(), 101)

    if @RptFromDate is null or len(@RptFromDate) = 0 
        set @frmDate = convert(datetime, convert(varchar, GETDATE(), 101))
    else
        set @frmDate = @RptFromDate

    if @RptToDate is null or len(@RptToDate) = 0
        set @toDate = convert(datetime, convert(varchar, GETDATE(), 101) + ' 23:59:59')
    else
        set @toDate = convert(datetime, @RptToDate +  ' 23:59:59')

    if @frmDate > @toDate
    begin
        set @tmpDate = @toDate
        set @toDate = @frmDate
        set @frmDate = @tmpDate
    end


    DECLARE @tmpClaimData TABLE 
    (
        Lynxid              bigint NOT NULL,
        InsuredName         varchar(200) NULL,
        BusinessName        varchar(200) NULL,
        InvolvedRole        varchar(15) NULL
    )

    DECLARE @tmpInsuredData TABLE 
    (
        Lynxid              bigint NOT NULL,
        InsuredClaimantName varchar(200) NULL
    )

    insert into @tmpClaimData
    select 
           (select top 1 ca1.LynxID
              from utb_claim_aspect ca1, utb_claim_aspect_involved cai1
              where ca1.ClaimAspectID = cai1.ClaimAspectID
                and cai1.InvolvedID = i.InvolvedID) as LynxID,
           isNull(i.NameLast + ', ' + i.NameFirst, '') as InsuredName,
           isNull(i.BusinessName, '') as BusinessName,
           irt.Name
    from utb_involved i, utb_involved_role ir, utb_involved_role_type irt
    where i.InvolvedID = ir.InvolvedID
      and ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
      and irt.Name in ('Insured', 'Claimant')
      and i.InvolvedID in ( select cai.InvolvedID 
                            from utb_claim c, utb_claim_aspect ca, utb_claim_aspect_involved cai
                            where c.LynxID = ca.LynxID
                              and ca.ClaimAspectID = cai.ClaimAspectID
                              and ca.EnabledFlag = 1
                              and cai.EnabledFlag = 1
                              and ca.ClaimAspectTypeID in (Select ClaimAspectTypeID from utb_claim_aspect_type where Name in ('Claim', 'Vehicle'))
                              and ((c.intakeStartDate >= @frmDate) and (c.intakeStartDate <= @toDate))
                           )
    order by LynxID


        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('105|%s|tmpClaimData', 16, 1, @ProcName)
            RETURN
        END
        
        -- Delete all LYNXIds that are Demo claims
        DELETE FROM @tmpClaimData
        WHERE Lynxid in (SELECT c.LynxID
                         FROM utb_claim c, utb_insurance i
                         WHERE c.InsuranceCompanyID = i.InsuranceCompanyID
                           and i.DemoFlag = 1)

        --select * from @tmpClaimData

        --print '3 or more Vehicles'
        declare @tmpLynxid bigint
        declare @tmpStr varchar(500)
        -- 3rd party claim -- 3 or more vehicles
        declare morethan3 cursor for
            select lynxid
            from @tmpClaimData
            group by lynxid
            having count(lynxid) >= 3

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END

        open morethan3

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END

        FETCH NEXT FROM morethan3 
        INTO @tmpLynxid

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END

        while @@fetch_status = 0
        begin
            set @tmpStr = ''
    
            select top 2 @tmpStr = @tmpStr +  case BusinessName
                                                when '' then InsuredName
                                                else BusinessName
                                              end + ' / ' 
            from @tmpClaimData
            where lynxid = @tmpLynxid
    
            set @tmpStr = rtrim(@tmpStr)
            if right(@tmpStr, 1) = '/'
                set @tmpStr = left(@tmpStr, len(@tmpStr) - 1)

            set @tmpStr = rtrim(@tmpStr)
            if right(@tmpStr, 1) = '/'
                set @tmpStr = left(@tmpStr, len(@tmpStr) - 1)

            set @tmpStr = rtrim(@tmpStr)
            if right(@tmpStr, 1) = '/'
                set @tmpStr = left(@tmpStr, len(@tmpStr) - 1)

            set @tmpStr = @tmpStr + '; et. al.'

            --print convert(varchar, @tmpLynxid) + '            ' + @tmpStr
            insert into @tmpInsuredData
            values (@tmpLynxid, @tmpStr)
            
            FETCH NEXT FROM morethan3 
            INTO @tmpLynxid
        end

        close morethan3
        deallocate morethan3

        --print '2 vehicles'
        -- 3rd party claim -- 2 vehicles
        declare twovehicles cursor for
            select lynxid
            from @tmpClaimData
            group by lynxid
            having count(lynxid) = 2

        open twovehicles

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END

        FETCH NEXT FROM twovehicles 
        INTO @tmpLynxid

        while @@fetch_status = 0
        begin
            set @tmpStr = ''
    
            select top 2 @tmpStr = @tmpStr + case BusinessName
                                                when '' then InsuredName
                                                else BusinessName
                                             end + ' / ' 
            from @tmpClaimData
            where lynxid = @tmpLynxid
            
            set @tmpStr = rtrim(@tmpStr)
            if right(@tmpStr, 1) = '/'
                set @tmpStr = left(@tmpStr, len(@tmpStr) - 1)

            set @tmpStr = rtrim(@tmpStr)
            if right(@tmpStr, 1) = '/'
                set @tmpStr = left(@tmpStr, len(@tmpStr) - 1)

            --set @tmpStr = substring(@tmpStr, 1, len(@tmpStr) - 2)
            --print convert(varchar, @tmpLynxid) + '            ' + @tmpStr
            insert into @tmpInsuredData
            values (@tmpLynxid, @tmpStr)
            
            FETCH NEXT FROM twovehicles 
            INTO @tmpLynxid
        end

        close twovehicles
        deallocate twovehicles

        --print '1 vehicle'
        -- 1st party claim -- 1 vehicles
        declare onevehicle cursor for
            select lynxid
            from @tmpClaimData
            group by lynxid
            having count(lynxid) = 1

        open onevehicle

        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error
        
            RAISERROR  ('99|%s', 16, 1, @ProcName)
            RETURN
        END

        FETCH NEXT FROM onevehicle 
        INTO @tmpLynxid

        while @@fetch_status = 0
        begin
            set @tmpStr = ''
    
            select top 1 @tmpStr =  case BusinessName
                                        when '' then InsuredName
                                        else BusinessName
                                    end
            from @tmpClaimData
            where lynxid = @tmpLynxid
    
            --print convert(varchar, @tmpLynxid) + '            ' + @tmpStr
            
            insert into @tmpInsuredData
            values (@tmpLynxid, @tmpStr)

            FETCH NEXT FROM onevehicle 
            INTO @tmpLynxid
        end

        close onevehicle
        deallocate onevehicle

/*    --select * from @tmpInsuredData
    select 
        1 as Tag,
        Null as Parent,
        --Root
        convert(varchar, @frmDate, 101) as [Root!1!FromDate],
        convert(varchar, @toDate, 101)  as [Root!1!ToDate],
        convert(varchar, CURRENT_TIMESTAMP, 101) + ' ' +  convert(varchar, CURRENT_TIMESTAMP, 108)as [Root!1!ReportDate],
        --Claim
        NULL as [Claim!2!Date], 
        NULL as [Claim!2!SortDate], 
        NULL as [Claim!2!LynxID], 
        NULL as [Claim!2!ClaimNumber], 
        NULL as [Claim!2!InsuredName], -- Policy Holder (Business Name or else Insured Name)
        NULL as [Claim!2!UserName],
        NULL as [Claim!2!ClaimRep]

    union all

    select 
        2 as Tag,
        1 as Parent,
        --Root
        NULL,
        NULL,
        NULL,
        --Claim
        convert(varchar, intakeStartDate, 101), 
        intakeStartDate, 
        clm.lynxid, 
        cov.clientClaimNumber, 
        tid.InsuredClaimantName,
        usr.NameLast + ', ' + usr.NameFirst,
        isNull((select NameLast + ', ' + NameFirst from dbo.utb_user where Userid = clm.OwnerUserID), '[Unassigned]')
    from dbo.utb_claim clm, dbo.utb_user usr, dbo.utb_claim_coverage cov, @tmpInsuredData tid
    where ((intakeStartDate >= @frmDate) and (intakeStartDate <= @toDate))
        and clm.IntakeUserID = usr.Userid
        and cov.lynxid = clm.lynxid
        and tid.lynxid = clm.lynxid
    order by [Claim!2!Date], [Claim!2!LynxID]*/
    
    SELECT @frmDate as ReportFromDate,
           @toDate as ReportToDate,
           intakeStartDate,
           clm.lynxid, 
           clm.clientClaimNumber, 
           tid.InsuredClaimantName,
           usr.NameLast + ', ' + usr.NameFirst as FNOLRep,
           isNull((select NameLast + ', ' + NameFirst from dbo.utb_user where Userid = ca.OwnerUserID), '[Unassigned]') as ClaimRep
    from dbo.utb_claim clm, dbo.utb_claim_aspect ca, dbo.utb_user usr, dbo.utb_claim_coverage cov, @tmpInsuredData tid
    where clm.LynxID = ca.LynxID
        and clm.IntakeUserID = usr.Userid
        and cov.lynxid = clm.lynxid
        and tid.lynxid = clm.lynxid
        and ca.claimAspectTypeID = (select claimAspectTypeID from utb_claim_aspect_type where name = 'Vehicle')
        and ((intakeStartDate >= @frmDate) and (intakeStartDate <= @toDate))
    order by convert(varchar, intakeStartDate, 101), clm.lynxid
--    for xml explicit
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptClaimRegister' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptClaimRegister TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/