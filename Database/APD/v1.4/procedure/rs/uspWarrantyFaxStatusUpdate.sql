-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyFaxStatusUpdate' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspWarrantyFaxStatusUpdate 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspWarrantyFaxStatusUpdate
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc to update the warranty details.
*
* PARAMETERS:  

* RESULT SET:
* An XML document containing the new SysLastUpdatedDate value
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspWarrantyFaxStatusUpdate
    @AssignmentID     udt_std_id_big,
    @StatusCD         udt_std_cd = 'S'
AS
BEGIN
    -- Initialize any empty string parameters

    -- Declare internal variables
    
    DECLARE @error                  AS int
    DECLARE @rowcount               AS int
    DECLARE @AssignmentStatus       AS varchar(300)
    DECLARE @FaxStatus              AS varchar(50)
    
    DECLARE @now                    AS datetime 

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspWarrantyFaxStatusUpdate'


    -- Set Database options
    
    SET NOCOUNT ON
    
    -- Check to make sure a valid Assignment id was passed in

    IF  (@AssignmentID IS NULL) OR
        (@AssignmentID = 0) OR
        (NOT EXISTS(SELECT AssignmentID FROM dbo.utb_warranty_assignment WHERE AssignmentID = @AssignmentID))
    BEGIN
        -- Invalid Assignment ID
    
        RAISERROR('101|%s|@AssignmentID|%u', 16, 1, @ProcName, @AssignmentID)
        RETURN
    END

    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP
    
    SELECT @AssignmentStatus = isNull(CommunicationAddress, '')
    FROM utb_warranty_assignment
    WHERE AssignmentID = @AssignmentID

    declare @tmpAssignmentStatus table
    (
      strIndex       int,
      strValue       varchar(30)
    )
    
    
    INSERT INTO @tmpAssignmentStatus
    SELECT strIndex, Value FROM dbo.ufnUtilityParseString(@AssignmentStatus, ';', 0)
    
    IF EXISTS(SELECT strIndex FROM @tmpAssignmentStatus WHERE strIndex = 1)
    BEGIN
        SET @FaxStatus = 'F=Sent'
        IF @StatusCD = 'C' SET @FaxStatus = 'F=Cancel'
        
        UPDATE @tmpAssignmentStatus
        SET strValue = @FaxStatus
        WHERE strIndex = 1
        
        SET @AssignmentStatus = ''
        
        SELECT @AssignmentStatus = @AssignmentStatus + strValue + ';'
        FROM @tmpAssignmentStatus
        
        SELECT @AssignmentStatus = LEFT(@AssignmentStatus, LEN(@AssignmentStatus)-1)
    END
    
    -- Begin Update

    BEGIN TRANSACTION 

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Update claim aspect
    UPDATE utb_warranty_assignment
    SET CommunicationAddress = @AssignmentStatus,
        AssignmentDate = convert(varchar, @now, 101),
        SysLastUpdatedDate = @now
    WHERE AssignmentID = @AssignmentID
    
    SET @error = @@ERROR
    SET @rowCount = @@ROWCOUNT
    
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_warranty_assignment', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END


    COMMIT TRANSACTION 

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END



    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspWarrantyFaxStatusUpdate' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspWarrantyFaxStatusUpdate TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
