BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspNugenGetLynxIDByDocumentID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspNugenGetLynxIDByDocumentID 
END

GO


GO
/****** Object:  StoredProcedure [dbo].[uspNugenGetLynxIDByDocumentID]    Script Date: 03/06/2013 07:01:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspNugenGetLynxIDByDocumentID]
(
@DocumentID varchar(10),
@LynxID varchar(10) OUTPUT
)
AS
BEGIN
DECLARE @TempID varchar(10)
SET NOCOUNT ON
	set @LynxID = (
SELECT dbo.utb_claim.lynxid
FROM dbo.utb_document INNER JOIN
dbo.utb_claim_aspect_service_channel_document ON dbo.utb_document.DocumentID = dbo.utb_claim_aspect_service_channel_document.DocumentID INNER JOIN
dbo.utb_claim_aspect_service_channel ON
dbo.utb_claim_aspect_service_channel_document.ClaimAspectServiceChannelID = dbo.utb_claim_aspect_service_channel.ClaimAspectServiceChannelID INNER JOIN
dbo.utb_claim INNER JOIN
dbo.utb_claim_aspect ON dbo.utb_claim.LynxID = dbo.utb_claim_aspect.LynxID ON
dbo.utb_claim_aspect_service_channel.ClaimAspectID = dbo.utb_claim_aspect.ClaimAspectID where utb_document.documentid = @DocumentID)

END
--Set @LynxID = @TempID
return @LynxID
SET NOCOUNT OFF
Go

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspNugenGetLynxIDByDocumentID' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspNugenGetLynxIDByDocumentID TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
