-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewClientOfficeContractState' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateNewClientOfficeContractState
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateNewClientOfficeContractState
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client Office Contract State by inserting a new record into the 
*				utb_contact_state table modeled after @iModelInsuranceCompanyID
*
* PARAMETERS:  
*
*   @iNewInsuranceCompanyID
*   @iUseCEIShop
*	@iCreatedByUserID
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateNewClientOfficeContractState
	@iNewInsuranceCompanyID INT
	, @iUseCEIShop INT
	, @iCreatedByUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @dtNow AS DATETIME 
    DECLARE @ProcName AS VARCHAR(50)

    SET @ProcName = 'uspCreateNewClientOfficeContractState'

    -- Set Database options
    SET NOCOUNT ON
	SET @dtNow = CURRENT_TIMESTAMP

	-- Check to make sure the new insurance company id exists
    IF  (@iNewInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iNewInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iNewInsuranceCompanyID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure a value was passed for @iUseCEIShop
    IF  (@iUseCEIShop IS NULL) 
		OR (@iUseCEIShop NOT IN (0,1))
    BEGIN
        -- Invalid @iUseCEIShop
        RAISERROR('101|%s|@iUseCEIShop|%u', 16, 1, @ProcName, @iUseCEIShop)
        RETURN
    END
	
	-- Check to make sure the model office exists
    IF NOT EXISTS (
		SELECT 
			OfficeID 
		FROM 
			utb_office
		WHERE 
			InsuranceCompanyID = @iNewInsuranceCompanyID
    )
    BEGIN
        -- Invalid Model Office ID
        RAISERROR('101|%s|@iNewInsuranceCompanyID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END
     
	-- Add the new
	INSERT INTO dbo.utb_office_contract_state
	SELECT 
		(SELECT TOP 1 OfficeID FROM utb_office WHERE InsuranceCompanyID = @iNewInsuranceCompanyID ORDER BY OfficeID )
		, StateCode
		, @iUseCEIShop
		, @iCreatedByUserID
		, @dtNow
	FROM  
		dbo.utb_state_code
	WHERE 
		EnabledFlag = 1
	 
	IF EXISTS(
		SELECT 
			* 
		FROM 
			dbo.utb_office_contract_state ocs
			INNER JOIN dbo.utb_office office 
			ON office.OfficeID = ocs.OfficeID
		WHERE 
			office.InsuranceCompanyID = @iNewInsuranceCompanyID
	)
	BEGIN
		SELECT 1 RetCode
	END
	ELSE
	BEGIN
		SELECT 0 RetCode
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateNewClientOfficeContractState' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateNewClientOfficeContractState TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateNewClientOfficeContractState TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/