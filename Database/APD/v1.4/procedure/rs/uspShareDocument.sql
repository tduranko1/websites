-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShareDocument' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspShareDocument 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspShareDocument
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Marks a document as authorized for client interface view.
*
* PARAMETERS:  
* (I) @DocumentID        The Document Id
*
* RESULT SET:
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspShareDocument
    @DocumentID          udt_std_int_big,
    @ApprovedFlag        bit
AS
BEGIN
    -- Declare local variables


    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspShareDocument'
    
    UPDATE utb_document
    SET ApprovedFlag = @ApprovedFlag
    WHERE DocumentID = @DocumentID
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspShareDocument' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspShareDocument TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO
