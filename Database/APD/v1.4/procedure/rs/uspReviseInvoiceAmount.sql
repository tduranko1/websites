-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspReviseInvoiceAmount' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspReviseInvoiceAmount 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspReviseInvoiceAmount
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Proc to update the invoice amount.
*
* PARAMETERS:  

* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspReviseInvoiceAmount
    @ClaimAspectID            udt_std_id_big = NULL,
    @DispatchNumber           varchar(50) = NULL,
    @RevisedIndemnityAmount   decimal(9, 2),
    @OldInvoiceIDFee          udt_std_id_big = NULL,
    @NewFeeID                 udt_std_id_big = NULL,
    @NewFeeServices           varchar(1000) = NULL,
    @UserID                   udt_std_id_big
AS
BEGIN
    -- Initialize any empty string parameters

    -- Declare internal variables
    
    DECLARE @error                  AS int
    DECLARE @rowcount               AS int
    
    DECLARE @now                    AS datetime 
    --DECLARE @DispatchNumber         AS varchar(50)
    DECLARE @IndemnityCount         AS int
    DECLARE @DispatchNumberTemp     AS varchar(50)
    DECLARE @NewInvoiceIDFee        AS udt_std_id_big
    DECLARE @NewFeeAmount           AS decimal(9, 2)
    DECLARE @NewFeeDesc             AS varchar(50)
    DECLARE @NewFeeInvoiceDesc      AS varchar(50)
    DECLARE @NewFeeInvoiceItemizeFlag  AS bit
    DECLARE @ClaimantName           AS udt_std_name
    DECLARE @UID                    AS udt_std_desc_short
    DECLARE @JournalDesc            AS varchar(250)
    DECLARE @InsuranceCompanyID     AS bigint
    DECLARE @FeeBulkBill            AS bit

    DECLARE @ProcName               AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspReviseInvoiceAmount'
    
    IF LEN(LTRIM(RTRIM(@NewFeeServices))) = 0 SET @NewFeeServices = NULL
    IF LEN(LTRIM(RTRIM(@DispatchNumber))) = 0 SET @DispatchNumber = NULL


    -- Set Database options
    
    SET NOCOUNT ON
    
    -- Validations
    
    IF @ClaimAspectID IS NULL AND @DispatchNumber IS NULL
    BEGIN
        RAISERROR('101|%s|@ClaimAspectID or @DispatchNumber is required|%u', 16, 1, @ProcName)
        RETURN
    END

    IF  (@ClaimAspectID IS NOT NULL) AND
        (NOT EXISTS(SELECT ClaimAspectID FROM dbo.utb_invoice WHERE ClaimAspectID = @ClaimAspectID AND EnabledFlag = 1))
    BEGIN
        -- Invalid Claim Aspect ID
    
        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END

    IF  (@DispatchNumber IS NOT NULL) AND
        (NOT EXISTS(SELECT InvoiceID FROM dbo.utb_invoice WHERE DispatchNumber = @DispatchNumber AND EnabledFlag = 1))
    BEGIN
        -- Invalid Dispatch Number
    
        RAISERROR('101|%s|@DispatchNumber|%u', 16, 1, @ProcName, @DispatchNumber)
        RETURN
    END

    IF  (@DispatchNumber IS NULL)
    BEGIN
       -- Get the information from the latest Invoice that had the indemnity
       SELECT top 1 @DispatchNumber = DispatchNumber
       FROM utb_invoice i
       WHERE i.ClaimAspectID = @ClaimAspectID
         AND i.StatusCD = 'FS'
         AND i.ItemTypeCD = 'I'
         AND i.EnabledFlag = 1
       ORDER BY InvoiceID DESC
    END
    
    IF @DispatchNumber IS NULL
    BEGIN
        RAISERROR('101|%s|@DispatchNumber|%s', 16, 1, @ProcName, @DispatchNumber)
        RETURN
    END
    
    IF  (@ClaimAspectID IS NULL)
    BEGIN
       -- Get the information from the latest Invoice
       SELECT top 1 @ClaimAspectID = i.ClaimAspectID
       FROM utb_invoice i
       WHERE i.DispatchNumber = @DispatchNumber
         AND i.StatusCD = 'FS'
         AND i.EnabledFlag = 1
       ORDER BY InvoiceID DESC
    END
    
    IF @ClaimAspectID IS NULL
    BEGIN
        RAISERROR('101|%s|@ClaimAspectID|%s', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END

    -- only one indemnity payment should exist for this dispatch
    SELECT @IndemnityCount = COUNT(InvoiceID)
    FROM dbo.utb_invoice
    WHERE DispatchNumber = @DispatchNumber
      AND ItemTypeCD = 'I'
      AND EnabledFlag = 1

    IF @IndemnityCount <> 1
    --IF @IndemnityCount = 0
    BEGIN
        RAISERROR('101|%s|No or Multiple Indemnity records found for the latest Invoice.', 16, 1, @ProcName)
        --RAISERROR('101|%s|No Indemnity records found for the latest Invoice.', 16, 1, @ProcName)
        RETURN
    END
    
    SET @DispatchNumberTemp = @DispatchNumber + '_' + convert(varchar, @ClaimAspectID)
    --select * from utb_invoice where dispatchNumber = @DispatchNumber
    
    SELECT @InsuranceCompanyID = InsuranceCompanyID
    FROM utb_claim c
    LEFT JOIN utb_claim_aspect ca ON c.LynxID = ca.LynxID
    WHERE ca.ClaimAspectID = @ClaimAspectID
    
    SET @FeeBulkBill = 0
    
    IF @OldInvoiceIDFee IS NULL
    BEGIN
      SELECT @OldInvoiceIDFee = InvoiceID 
      FROM utb_invoice
      WHERE DispatchNumber = @DispatchNumber
        AND ItemTypeCD = 'F'
        AND EnabledFlag = 1
        
      IF @OldInvoiceIDFee IS NULL
      BEGIN
         -- Is the fee for PS a bulk fee for this client
         IF EXISTS (SELECT InvoicingModelBillingCD
                     FROM dbo.utb_client_service_channel
                     WHERE InsuranceCompanyID = @InsuranceCompanyID
                       AND ServiceChannelCD = 'PS'
                       AND InvoicingModelBillingCD = 'B')
         BEGIN
            -- select the bulk fee that is still pending
            SELECT @OldInvoiceIDFee = InvoiceID
            FROM utb_invoice i
            WHERE ClaimAspectID = @ClaimAspectID
              AND ItemTypeCD = 'F'
              AND StatusCD = 'APD'
              AND EnabledFlag = 1
            
            SET @FeeBulkBill = 1
         END
      END
    END
    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP
    
    BEGIN TRANSACTION
    
    -- COPY the old invoice information
    INSERT INTO utb_invoice (
      AuthorizingUserID,
      ClaimAspectID,
      ClaimAspectServiceChannelID,
      DocumentID,
      RecordingUserID,
      AdminFeeAmount,
      AdminFeeCD,
      Amount,
      AuthorizedDate,
      CarrierOfficeCode,
      CarrierRepName,
      ClientFeeCode,
      DeductibleAmt,
      Description,
      DispatchNumber,
      EarlyPayReconcileCD,
      EnabledFlag,
      EntryDate,
      FeeCategoryCD,
      InsuredName,
      InvoiceDescription,
      ItemizeFlag,
      ItemTypeCD,
      PayeeAddress1,
      PayeeAddress2,
      PayeeAddressCity,
      PayeeAddressState,
      PayeeAddressZip,
      PayeeID,
      PayeeName,
      PayeeTypeCD,
      PaymentChannelCD,
      SentToIngresDate,
      StatusCD,
      StatusDate,
      SuspendPayFlag,
      SystemAmount,
      TaxTotalAmt,
      WarrantyFlag,
      SysLastUserID,
      SysLastUpdatedDate
    )
    SELECT AuthorizingUserID,
            ClaimAspectID,
            ClaimAspectServiceChannelID,
            DocumentID,
            RecordingUserID,
            AdminFeeAmount,
            AdminFeeCD,
            Amount,
            AuthorizedDate,
            CarrierOfficeCode,
            CarrierRepName,
            ClientFeeCode,
            DeductibleAmt,
            Description,
            @DispatchNumberTemp,
            EarlyPayReconcileCD,
            EnabledFlag,
            EntryDate,
            FeeCategoryCD,
            InsuredName,
            InvoiceDescription,
            ItemizeFlag,
            ItemTypeCD,
            PayeeAddress1,
            PayeeAddress2,
            PayeeAddressCity,
            PayeeAddressState,
            PayeeAddressZip,
            PayeeID,
            PayeeName,
            PayeeTypeCD,
            PaymentChannelCD,
            SentToIngresDate,
            StatusCD,
            StatusDate,
            SuspendPayFlag,
            SystemAmount,
            TaxTotalAmt,
            WarrantyFlag,
            SysLastUserID,
            SysLastUpdatedDate
    FROM utb_invoice
    WHERE DispatchNumber = @DispatchNumber

    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_invoice', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    
    -- Disable the old dispatch
    
    UPDATE dbo.utb_invoice
    SET EnabledFlag = 0,
        DispatchNumber = DispatchNumber + '_old'
    WHERE DispatchNumber = @DispatchNumber

    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_warranty_assignment', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    
    -- Update the new invoice records with old dispatch number
    UPDATE dbo.utb_invoice
    SET DispatchNumber = @DispatchNumber
    WHERE DispatchNumber = @DispatchNumberTemp

    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_warranty_assignment', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    
    SELECT @NewInvoiceIDFee = InvoiceID
    FROM utb_invoice
    WHERE DispatchNumber = @DispatchNumber
      AND ItemTypeCD = 'F'
      AND EnabledFlag = 1

    IF @FeeBulkBill = 1
    BEGIN
      SET @NewInvoiceIDFee = @OldInvoiceIDFee
    END
      
    IF @NewInvoiceIDFee IS NOT NULL
    BEGIN
      --print '@NewInvoiceIDFee = ' + convert(varchar, @NewInvoiceIDFee)
      
      IF @NewFeeID IS NULL
      BEGIN
         -- copy the old fee services
         INSERT INTO utb_invoice_service (
            InvoiceID,
            ServiceID,
            ClaimAspectID,
            ClaimantName,
            DispositionTypeCD,
            EnabledFlag,
            ItemizeFlag,
            ServiceChannelCD,
            UID,
            SysLastUserID,
            SysLastUpdatedDate
         )
         SELECT @NewInvoiceIDFee,
                ServiceID,
                ClaimAspectID,
                ClaimantName,
                DispositionTypeCD,
                EnabledFlag,
                ItemizeFlag,
                ServiceChannelCD,
                UID,
                @UserID,
                @now
         FROM utb_invoice_service
         WHERE InvoiceID = @OldInvoiceIDFee

         IF @@ERROR <> 0
         BEGIN
             -- Update failure
             RAISERROR('104|%s|utb_invoice_service', 16, 1, @ProcName)
             ROLLBACK TRANSACTION 
             RETURN
         END
      END
      ELSE
      BEGIN

         -- Get the new fee information
         SELECT @NewFeeAmount = FeeAmount,
                @NewFeeDesc = Description,
                @NewFeeInvoiceDesc = InvoiceDescription,
                @NewFeeInvoiceItemizeFlag = ItemizeFlag
         FROM dbo.utb_client_fee
         WHERE ClientFeeID = @NewFeeID
         
         -- GET some basic information from the old service
         SELECT top 1
                @ClaimantName = ClaimantName,
                @UID = UID
         FROM dbo.utb_invoice_service
         WHERE InvoiceID = @OldInvoiceIDFee
         
         -- Update the fee record with the new fee information
         UPDATE utb_invoice
         SET Amount = @NewFeeAmount,
             Description = @NewFeeDesc,
             InvoiceDescription = @NewFeeInvoiceDesc,
             ItemizeFlag = @NewFeeInvoiceItemizeFlag,
             RecordingUserID = @UserID
         WHERE InvoiceID = @NewInvoiceIDFee

         IF @@ERROR <> 0
         BEGIN
             -- Update failure
             RAISERROR('104|%s|utb_invoice', 16, 1, @ProcName)
             ROLLBACK TRANSACTION 
             RETURN
         END
         
         -- Add the Require service as performed
         INSERT INTO utb_invoice_service (
            InvoiceID,
            ServiceID,
            ClaimAspectID,
            ClaimantName,
            DispositionTypeCD,
            EnabledFlag,
            ItemizeFlag,
            ServiceChannelCD,
            UID,
            SysLastUserID,
            SysLastUpdatedDate
         )
         SELECT @NewInvoiceIDFee,
                s.ServiceID,
                @ClaimAspectID,
                @ClaimantName,
                s.DispositionTypeCD,
                1,
                cfd.ItemizeFlag,
                s.ServiceChannelCD,
                @UID,
                @UserID,
                @now
         FROM dbo.utb_client_fee_definition cfd
         LEFT JOIN dbo.utb_service s ON cfd.ServiceID = s.ServiceID
         WHERE cfd.ClientFeeID = @NewFeeID
           AND cfd.RequiredFlag = 1         

         IF @@ERROR <> 0
         BEGIN
             -- Insert failure
             RAISERROR('104|%s|utb_invoice_service', 16, 1, @ProcName)
             ROLLBACK TRANSACTION 
             RETURN
         END
      END
    END
    
    -- Update the new invoice indemnity amount
    UPDATE dbo.utb_invoice
    SET Amount = @RevisedIndemnityAmount 
    WHERE DispatchNumber = @DispatchNumber
      AND ItemTypeCD = 'I'
      
    SET @rowcount = @@ROWCOUNT

    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_warranty_assignment', 16, 1, @ProcName)
        ROLLBACK TRANSACTION 
        RETURN
    END
    
    --select * from utb_invoice where dispatchNumber like @DispatchNumber + '%'
    SET @JournalDesc = 'Indemnity amount was revised to ' + convert(varchar, @RevisedIndemnityAmount) + '.'
    IF @NewFeeID IS NOT NULL
    BEGIN
      SET @JournalDesc = @JournalDesc + ' Fee was revised to ' + @NewFeeDesc + '.'
    END
    
    --SET @JournalDesc = @JournalDesc + ' Email to FS was sent for manual corrections.'
    
    INSERT INTO dbo.utb_invoice_dispatch (
       Amount,
       Description,
       DispatchNumber,
       DisptachNumberNew,
       TransactionCD,
       TransactionDate,
       SysLastUserID,
       SysLastUpdatedDate
    ) VALUES (
       @RevisedIndemnityAmount,
       @JournalDesc,
       @DispatchNumber,
       NULL,
       'Q',
       @now,
       @UserID,
       @now
    )


    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR  ('104|%s|utb_invoice_dispatch', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    --select * from utb_invoice where claimAspectID = @ClaimAspectID

    COMMIT TRANSACTION 
    --rollback transaction

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    RETURN @rowcount

END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspReviseInvoiceAmount' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspReviseInvoiceAmount TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
