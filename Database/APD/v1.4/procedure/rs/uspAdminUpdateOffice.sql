-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminUpdateOffice' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspAdminUpdateOffice 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspAdminUpdateOffice
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Updates existing client office in the utb_office table.
*
* PARAMETERS:  
*
*      @iInsuranceCompanyID INT
*	   , @iOfficeID INT
*      , @vAddress1 VARCHAR(50)
*      , @vAddress2 VARCHAR(50)
*      , @vAddressCity VARCHAR(30)
*      , @vAddressState VARCHAR(2)
*      , @vAddressZip VARCHAR(8)
*      , @vCCEmailAddress VARCHAR(50)
*      , @vClaimNumberFormatJS VARCHAR(250)
*      , @vClaimNumberValidJS VARCHAR(250)
*      , @vClaimNumberMsgText VARCHAR(500)
*      , @vClientOfficeId VARCHAR(50)
*      , @bEnabledFlag BIT
*      , @cFaxAreaCode CHAR(3)
*      , @cFaxExchangeNumber CHAR(3)
*      , @cFaxExtensionNumber CHAR(5)
*      , @cFaxUnitNumber CHAR(4)
*      , @vMailingAddress1 VARCHAR(50)
*      , @vMailingAddress2 VARCHAR(50)
*      , @vMailingAddressCity VARCHAR(30)
*      , @vMailingAddressState VARCHAR(2)
*      , @vMailingAddressZip VARCHAR(8)
*      , @vName VARCHAR(50)
*      , @cPhoneAreaCode VARCHAR(3)
*      , @cPhoneExchangeNumber VARCHAR(3)
*      , @cPhoneExtensionNumber VARCHAR(5)
*      , @cPhoneUnitNumber VARCHAR(4)
*      , @vReturnDocDestinationValue VARCHAR(250)
*      , @vReturnDocEmailAddress VARCHAR(50)
*      , @cReturnDocFaxAreaCode CHAR(3)
*      , @cReturnDocFaxExchangeNumber CHAR(3)
*      , @cReturnDocFaxExtensionNumber CHAR(5)
*      , @cReturnDocFaxUnitNumber CHAR(4)
*      , @iSysLastUserID INT
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspAdminUpdateOffice
	@iInsuranceCompanyID INT
	  , @iOfficeID INT
      , @vAddress1 VARCHAR(50)
      , @vAddress2 VARCHAR(50)
      , @vAddressCity VARCHAR(30)
      , @vAddressState VARCHAR(2)
      , @vAddressZip VARCHAR(8)
      , @vCCEmailAddress VARCHAR(50)
      , @vClaimNumberFormatJS VARCHAR(250)
      , @vClaimNumberValidJS VARCHAR(250)
      , @vClaimNumberMsgText VARCHAR(500)
      , @vClientOfficeId VARCHAR(50)
      , @bEnabledFlag BIT
      , @cFaxAreaCode CHAR(3)
      , @cFaxExchangeNumber CHAR(3)
      , @cFaxExtensionNumber CHAR(5)
      , @cFaxUnitNumber CHAR(4)
      , @vMailingAddress1 VARCHAR(50)
      , @vMailingAddress2 VARCHAR(50)
      , @vMailingAddressCity VARCHAR(30)
      , @vMailingAddressState VARCHAR(2)
      , @vMailingAddressZip VARCHAR(8)
      , @vName VARCHAR(50)
      , @cPhoneAreaCode VARCHAR(3)
      , @cPhoneExchangeNumber VARCHAR(3)
      , @cPhoneExtensionNumber VARCHAR(5)
      , @cPhoneUnitNumber VARCHAR(4)
      , @vReturnDocDestinationValue VARCHAR(250)
      , @vReturnDocEmailAddress VARCHAR(50)
      , @cReturnDocFaxAreaCode CHAR(3)
      , @cReturnDocFaxExchangeNumber CHAR(3)
      , @cReturnDocFaxExtensionNumber CHAR(5)
      , @cReturnDocFaxUnitNumber CHAR(4)
      , @iSysLastUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS int
    DECLARE @rowcount AS int
    DECLARE @now AS datetime 
    DECLARE @ProcName AS varchar(30)
    DECLARE @bRequiredExists AS BIT

    SET @ProcName = 'uspAdminUpdateOffice'
    SET @bRequiredExists = 0

    -- Set Database options
    SET NOCOUNT ON
	SET @now = CURRENT_TIMESTAMP
	SET @error = 0

    -- Check to make sure a valid Insurance Company ID was passed in
    IF  (@iInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iInsuranceCompanyID))
    BEGIN
        -- Invalid InsuranceCompanyID 
        RAISERROR('101|%s|@iInsuranceCompanyID|%u', 16, 1, @ProcName, @iInsuranceCompanyID)
        RETURN
    END

    -- Check to make sure a valid Office ID was passed in
    IF  (@iOfficeID IS NULL) 
        OR (NOT EXISTS(SELECT OfficeID FROM utb_office WHERE OfficeID = @iOfficeID))
    BEGIN
        -- Invalid Message Template ID
        RAISERROR('101|%s|@iOfficeID|%u', 16, 1, @ProcName, @iOfficeID)
        RETURN
    END

	------------------------------------
	-- Do the utb_office table updates	
	------------------------------------
    IF  (@vName IS NOT NULL) 
    BEGIN
		UPDATE dbo.utb_office
		SET
			Address1 = @vAddress1
			, Address2 = @vAddress2
			, AddressCity = @vAddressCity
			, AddressState = @vAddressState
			, AddressZip = @vAddressZip
			, CCEmailAddress = @vCCEmailAddress
			, ClaimNumberFormatJS = @vClaimNumberFormatJS
			, ClaimNumberValidJS = @vClaimNumberValidJS
			, ClaimNumberMsgText = @vClaimNumberMsgText
			, ClientOfficeId = @vClientOfficeId
			, EnabledFlag = @bEnabledFlag
			, FaxAreaCode = @cFaxAreaCode
			, FaxExchangeNumber = @cFaxExchangeNumber
			, FaxExtensionNumber = @cFaxExtensionNumber
			, FaxUnitNumber = @cFaxUnitNumber
			, MailingAddress1 = @vMailingAddress1
			, MailingAddress2 = @vMailingAddress2
			, MailingAddressCity = @vMailingAddressCity
			, MailingAddressState = @vMailingAddressState
			, MailingAddressZip = @vMailingAddressZip
			, [Name] = @vName
			, PhoneAreaCode = @cPhoneAreaCode
			, PhoneExchangeNumber = @cPhoneExchangeNumber
			, PhoneExtensionNumber = @cPhoneExtensionNumber
			, PhoneUnitNumber = @cPhoneUnitNumber
			, ReturnDocDestinationValue = @vReturnDocDestinationValue
			, ReturnDocEmailAddress = @vReturnDocEmailAddress
			, ReturnDocFaxAreaCode = @cReturnDocFaxAreaCode
			, ReturnDocFaxExchangeNumber = @cReturnDocFaxExchangeNumber
			, ReturnDocFaxExtensionNumber = @cReturnDocFaxExtensionNumber
			, ReturnDocFaxUnitNumber = @cReturnDocFaxUnitNumber
			, SysLastUserID = @iSysLastUserID
			, SysLastUpdatedDate = @now
		WHERE
			OfficeID = @iOfficeID

		SET @error = @error + @@ERROR
	END
	ELSE
	BEGIN
        -- Invalid Name
        RAISERROR('101|%s|@vName|%u', 16, 1, @ProcName, @vName)
        RETURN
	END
	
	SELECT @error AS RetCode

END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspAdminUpdateOffice' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspAdminUpdateOffice TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspAdminUpdateOffice TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/