-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptShopActivity' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptShopActivity 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptShopActivity
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jeffery A. Lund
* FUNCTION:     Retrieves list of shops with assignments for a given Program Manager
*
* PARAMETERS:  
*  @RptFromDate  Starting date for the report filter
*  @RptToDate    Ending date for the report filter
*
* RESULT SET:
* List assignments in given Program Manager's area of responsibility.
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptShopActivity
    @RptFromDate          udt_std_datetime = null,
    @RptToDate            udt_std_datetime = null
             
AS
BEGIN
    -- Set database options
    
    SET NOCOUNT ON
    SET ANSI_WARNINGS OFF


    DECLARE @ProcName               udt_std_name 
    DECLARE @now                    udt_std_datetime
    
    SET @ProcName = 'uspRptShopActivity'
    SET @now = CURRENT_TIMESTAMP
    
    IF @RptFromDate IS NULL
        SET @RptFromDate = DATEDIFF(d, -1, @now)

    IF (@RptToDate IS NULL) OR (@RptToDate = '') SET @RptToDate = @now
    
    
     /*************************************************************************************
    *  Gather Reference Data
    **************************************************************************************/

    DECLARE @tmpReference TABLE 
    (
        ListName        varchar(50) NOT NULL,
        DisplayOrder    int         NULL,
        ReferenceId     varchar(10) NOT NULL,
        Name            varchar(50) NOT NULL
    )
    
    DECLARE @tmpData TABLE
    (
        StateValue	            varchar(50)     NOT NULL,
        ShopLocationID	        int             NULL,
        ShopLocationName	    varchar(100)    NULL,
        ShopContact	            varchar(100)    NULL,
        ShopType	            varchar(3)      NULL,
        ShopAddress	            varchar(50)     NULL,
        ShopCityState	        varchar(50)     NULL,
        ShopZip	                varchar(8)      NULL,
        PhoneAreaCode	        varchar(3)      NULL,
        PhoneExchangeNumber	    varchar(3)      NULL,
        PhoneExtensionNumber    varchar(5)      NULL,
        PhoneUnitNumber	        varchar(4)      NULL,
        FaxAreaCode	            varchar(3)      NULL,
        FaxExchangeNumber	    varchar(3)      NULL,
        FaxExtensionNumber	    varchar(5)      NULL,
        FaxUnitNumber	        varchar(4)      NULL,
        EmailAddress	        varchar(100)    NULL,
        LastReinspectionDate    datetime        NULL,
        LynxID	                bigint          NULL,
        VehicleYrMakeModel	    varchar(150)    NULL,
        LicensePlateState	    varchar(2)      NULL,
        LicensePlaceNumber	    varchar(10)     NULL,
        OwnerName	            varchar(100)    NULL,
        BusinessName	        varchar(100)    NULL,
        AssignmentDate	        datetime        NULL,
        CancellationDate        datetime        NULL,
        DocumentID              bigint          NULL,
        DocDateCreated          datetime        NULL,
        DocName                 varchar(50)     NULL
    )

    -- States
    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceId, Name)
        
    Select 'State' AS ListName, DisplayOrder, StateCode, StateValue
    From dbo.utb_state_code
    Where EnabledFlag = 1
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END
    
    

    --Begin final Select 
    /*SELECT
        1               AS Tag,
        Null            AS Parent,
        
        -- Root
        (convert(varchar(2), Month(@RptFromDate)) + '/' +
         convert(varchar(2), Day(@RptFromDate)) + '/' + 
         convert(varchar(4), Year(@RptFromDate)))       AS [Root!1!FromDate],
            
        (convert(varchar(2), Month(@RptToDate)) + '/' +
         convert(varchar(2), Day(@RptToDate)) + '/' + 
         convert(varchar(4), Year(@RptToDate)))         AS [Root!1!ToDate],
         
        (convert(varchar(2), Month(@now)) + '/' +
         convert(varchar(2), Day(@now)) + '/' + 
         convert(varchar(4), Year(@now)))            AS [Root!1!ReportDate],
            
       
       -- State Header 
        Null            AS [State!2!AddressState],
                
        -- Shop
        Null            AS [Shop!3!ShopLocationID],
        Null            AS [Shop!3!Address1],
        Null            AS [Shop!3!Address2],
        Null            AS [Shop!3!AddressCity],
        Null            AS [Shop!3!AddressCounty],
        Null            AS [Shop!3!AddressState],
        Null            AS [Shop!3!AddressZip],
        Null            AS [Shop!3!ContactName],
        Null            AS [Shop!3!EmailAddress],
        Null            AS [Shop!3!FaxAreaCode],
        Null            AS [Shop!3!FaxExchangeNumber],
        Null            AS [Shop!3!FaxExtensionNumber],
        Null            AS [Shop!3!FaxUnitNumber],
        Null            AS [Shop!3!LastReinspectionDate],
        Null            AS [Shop!3!Name],
        Null            AS [Shop!3!PhoneAreaCode],
        Null            AS [Shop!3!PhoneExchangeNumber],
        Null            AS [Shop!3!PhoneExtensionNumber],
        Null            AS [Shop!3!PhoneUnitNumber],
        Null            AS [Shop!3!Type],
             
        -- Assignment
        Null            AS [Assignment!4!AssignmentID],
        Null            AS [Assignment!4!ShopLocationID],
        Null            AS [Assignment!4!ClaimAspectID],
        Null            AS [Assignment!4!ClaimAspectNumber],
        Null            AS [Assignment!4!LynxID],
        Null            AS [Assignment!4!VehicleYMM],
        Null            AS [Assignment!4!AssignmentDate],
        Null            AS [Assignment!4!CancellationDate],
        Null            AS [Assignment!4!LicensePlateState],
        Null            AS [Assignment!4!LicensePlateNumber],
        Null            AS [Assignment!4!OwnerName],
        Null            AS [Assignment!4!OwnerBusinessName],
        
        -- Estimate
        Null            AS [Estimate!5!DocumentID],
        Null            AS [Estimate!5!AssignmentID],
        Null            AS [Estimate!5!EstimateDate],
        Null            AS [Estimate!5!EstimateType],
        Null            AS [Estimate!5!Amount],
        
        --Reference
        Null            AS [Reference!6!ListName],
        Null            AS [Reference!6!ReferenceID],
        Null            AS [Reference!6!Name]
       

    UNION ALL

    
    -- State Header Level
    SELECT DISTINCT
        2,
        1,
        -- Root
        Null, Null, Null,
        
        -- State  
        sl.AddressState,
                
        -- Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
             
        -- Assignment
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null,
        
        -- Estimate
        Null, Null, Null, Null, Null, 
        
        -- Reference
        Null, Null, Null
        
        FROM dbo.utb_shop_location sl LEFT JOIN dbo.utb_assignment a ON sl.ShopLocationID = a.ShopLocationID
                                      INNER JOIN dbo.utb_claim_aspect ca ON a.ClaimAspectID = ca.ClaimAspectID
                                      INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                                      LEFT JOIN dbo.utb_document d ON a.AssignmentID = d.AssignmentID
                                      LEFT JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
                                      
        WHERE c.DemoFlag = 0
          AND (a.ProgramFlag = 1 OR a.CEIProgramFlag = 1)
          AND ((a.AssignmentDate >= @RptFromDate AND a.AssignmentDate <= @RptToDate) 
               OR (d.CreatedDate >= @RptFromDate AND d.CreatedDate <= @RptToDate AND dt.EstimateTypeFlag = 1))
          
    UNION ALL


    -- Shop Level
    SELECT DISTINCT
        3,
        2,
        
        -- Root
        Null, Null, Null,
        
        -- StateHeader
        sl.AddressState,
        
        -- Shop
        sl.ShopLocationID,
        IsNull(sl.Address1, ''),
        IsNull(sl.Address2, ''),
        IsNull(sl.AddressCity, ''),
        IsNull(sl.AddressCounty, ''),
        IsNull(sl.AddressState, ''),
        IsNull(sl.AddressZip, ''),
        IsNull((SELECT TOP 1 p.Name 
                FROM dbo.utb_shop_location_personnel lp INNER JOIN dbo.utb_personnel p ON lp.PersonnelID = p.PersonnelID
                                                        INNER JOIN dbo.utb_personnel_type pt ON p.PersonnelTypeID = pt.PersonnelTypeID                     
                WHERE lp.ShopLocationID = sl.ShopLocationID
                  AND pt.Name = 'Shop Manager'), ''),
        IsNull(sl.EmailAddress, ''),
        IsNull(sl.FaxAreaCode, ''),
        IsNull(sl.FaxExchangeNumber, ''),
        IsNull(sl.FaxExtensionNumber, ''),
        IsNull(sl.FaxUnitNumber, ''),
        IsNull((SELECT convert(varchar(2), Month(ReinspectionDate)) + '/' +
                       convert(varchar(2), Day(ReinspectionDate)) + '/' + 
                       convert(varchar(4), Year(ReinspectionDate))
                FROM dbo.utb_reinspect
                WHERE ReinspectID = (SELECT MAX(ReinspectID) 
                                     FROM dbo.utb_reinspect r INNER JOIN dbo.utb_assignment a 
                                                                     ON r.ClaimAspectID = a.ClaimAspectID
                                     WHERE a.ShopLocationID = sl.ShopLocationID
                                       AND r.EnabledFlag = 1)), ''),
        IsNull(sl.Name, ''),
        IsNull(sl.PhoneAreaCode, ''),
        IsNull(sl.PhoneExchangeNumber, ''),
        IsNull(sl.PhoneExtensionNumber, ''),
        IsNull(sl.PhoneUnitNumber, ''),
        (CASE
          WHEN a.ProgramFlag = 1 THEN 'LS'
          WHEN a.CEIProgramFlag = 1 THEN 'CEI'
          ELSE ''        
        END),               
        
        -- Assignment
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null,
        
        -- Estimate
        Null, Null, Null, Null, Null, 
        
        -- Reference
        Null, Null, Null        
        
        
    FROM dbo.utb_shop_location sl LEFT JOIN dbo.utb_assignment a ON sl.ShopLocationID = a.ShopLocationID
                                      INNER JOIN dbo.utb_claim_aspect ca ON a.ClaimAspectID = ca.ClaimAspectID
                                      INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                                      LEFT JOIN dbo.utb_document d ON a.AssignmentID = d.AssignmentID
                                      LEFT JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    WHERE  c.DemoFlag = 0
      AND (a.ProgramFlag = 1 OR a.CEIProgramFlag = 1)
      AND ((a.AssignmentDate >= @RptFromDate AND a.AssignmentDate <= @RptToDate) 
           OR (d.CreatedDate >= @RptFromDate AND d.CreatedDate <= @RptToDate AND dt.EstimateTypeFlag = 1))
        
       
    UNION ALL


    --Assignment Level
    SELECT DISTINCT
        4,
        1,
        
        -- Root
        Null, Null, Null,
        
        -- StateHeader
        'ZZ-Reference',
        
        -- Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        
        -- Assignment
        a.AssignmentID,
        a.ShopLocationID,
        a.ClaimAspectID,
        ca.ClaimAspectNumber,
        ca.LynxID,
                
        IsNull(convert(varchar(4), VehicleYear) + ' ' + Make + ' ' + Model, ''),
        
        (convert(varchar(2), Month(a.AssignmentDate)) + '/' +
            convert(varchar(2), Day(a.AssignmentDate)) + '/' + 
            convert(varchar(4), Year(a.AssignmentDate))),
            
        (convert(varchar(2), Month(a.CancellationDate)) + '/' +
            convert(varchar(2), Day(a.CancellationDate)) + '/' + 
            convert(varchar(4), Year(a.CancellationDate))),
            
        IsNull(cv.LicensePlateState, ''),
        IsNull(cv.LicensePlateNumber, ''),
        
        IsNull((SELECT TOP 1 i.NameFirst + ' ' + i.NameLast
                FROM dbo.utb_involved i INNER JOIN dbo.utb_claim_aspect_involved cai ON i.InvolvedID = cai.InvolvedID
                                        INNER JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                        INNER JOIN dbo.utb_involved_role_type irt On ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                WHERE irt.Name = 'Owner'
                  AND cai.ClaimAspectID = a.ClaimAspectID), '') OwnerName,
        
        Isnull((SELECT TOP 1 i.BusinessName
                FROM dbo.utb_involved i INNER JOIN dbo.utb_claim_aspect_involved cai ON i.InvolvedID = cai.InvolvedID
                                        INNER JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                        INNER JOIN dbo.utb_involved_role_type irt On ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                WHERE irt.Name = 'Owner'
                  AND cai.ClaimAspectID = a.ClaimAspectID), '') BusinessName,
        
        -- Estimate
        Null, Null, Null, Null, Null, 
        
        -- Reference
        Null, Null, Null
        
    FROM dbo.utb_shop_location sl LEFT JOIN dbo.utb_assignment a ON sl.ShopLocationID = a.ShopLocationID
                                  LEFT JOIN dbo.utb_claim_aspect ca ON a.ClaimAspectID = ca.ClaimAspectID
                                  INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                                  LEFT JOIN dbo.utb_document d ON a.AssignmentID = d.AssignmentID
                                  LEFT JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
                                  LEFT JOIN dbo.utb_claim_vehicle cv ON a.ClaimAspectID = cv.ClaimAspectID
    WHERE  sl.EnabledFlag = 1
      AND c.DemoFlag = 0
      AND (a.ProgramFlag = 1 OR a.CEIProgramFlag = 1)
      AND ((a.AssignmentDate >= @RptFromDate AND a.AssignmentDate <= @RptToDate) 
               OR (d.CreatedDate >= @RptFromDate AND d.CreatedDate <= @RptToDate AND dt.EstimateTypeFlag = 1))


    UNION ALL

    -- Estimate Level
    SELECT DISTINCT
        5,
        1,
        
        -- Root
        Null, Null, Null,
        
        -- State Header
        Null,
        
        -- Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        
        -- Assignment
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null,
        
        -- Estimate
        d.DocumentID, 
        d.AssignmentID,
        (convert(varchar(2), Month(d.CreatedDate)) + '/' +
            convert(varchar(2), Day(d.CreatedDate)) + '/' + 
            convert(varchar(4), Year(d.CreatedDate))),
        IsNull(dt.Name, ''),
        IsNull(convert(varchar(20), es.OriginalExtendedAmt), ''), 
        
        -- Reference
        Null, Null, Null
        
     
    FROM dbo.utb_shop_location sl INNER JOIN dbo.utb_assignment a ON sl.ShopLocationID = a.ShopLocationID
                                  INNER JOIN dbo.utb_claim_aspect ca ON a.ClaimAspectID = ca.ClaimAspectID
                                  INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
                                  INNER JOIN dbo.utb_document d ON a.AssignmentID = d.AssignmentID
                                  INNER JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
                                  INNER JOIN dbo.utb_estimate_summary es ON d.DocumentID = es.DocumentID
                                  INNER JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
    
    WHERE c.DemoFlag = 0
      AND (a.ProgramFlag = 1 OR a.CEIProgramFlag = 1)
      AND ((a.AssignmentDate >= @RptFromDate AND a.AssignmentDate <= @RptToDate) 
               OR (d.CreatedDate >= @RptFromDate AND d.CreatedDate <= @RptToDate AND dt.EstimateTypeFlag = 1))
      AND (est.Name = 'RepairTotal')

  
    UNION ALL

   
    -- Reference Level
    SELECT
        6,
        1,
        
        -- Root
        Null, Null, Null,
        
        -- State Header
        Null,
        
        -- Shop
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, 
        
        -- Assignment
        Null, Null, Null, Null, Null, Null, Null, Null, Null, Null,
        Null, Null,
        
        -- Estimate
        Null, Null, Null, Null, Null,

        --Reference
        ListName,
        ReferenceID,
        Name

    FROM    @tmpReference
    


    ORDER BY [State!2!AddressState], Tag*/
    
    
    INSERT INTO @tmpData
    SELECT  distinct sc.StateValue,
            sl.ShopLocationID,
            IsNull(sl.Name, ''),
            IsNull((SELECT TOP 1 p.Name 
                    FROM dbo.utb_shop_location_personnel lp INNER JOIN dbo.utb_personnel p ON lp.PersonnelID = p.PersonnelID
                                                            INNER JOIN dbo.utb_personnel_type pt ON p.PersonnelTypeID = pt.PersonnelTypeID                     
                    WHERE lp.ShopLocationID = sl.ShopLocationID
                      AND pt.Name = 'Shop Manager'), ''),
            /*(CASE
              WHEN a.CEIProgramFlag = 1 THEN 'CEI'
              WHEN a.ProgramFlag = 1 THEN 'PS'
              ELSE ''        
            END),*/
            a.ProgramTypeCD,
            IsNull(sl.Address1, ''),
            RTrim(IsNull(sl.AddressCity, '')) + ', ' + IsNull(sl.AddressState, ''),
            IsNull(sl.AddressZip, ''),
            IsNull(sl.PhoneAreaCode, ''),
            IsNull(sl.PhoneExchangeNumber, ''),
            IsNull(sl.PhoneExtensionNumber, ''),
            IsNull(sl.PhoneUnitNumber, ''),
            IsNull(sl.FaxAreaCode, ''),
            IsNull(sl.FaxExchangeNumber, ''),
            IsNull(sl.FaxExtensionNumber, ''),
            IsNull(sl.FaxUnitNumber, ''),
            IsNull(sl.EmailAddress, ''),
            IsNull((SELECT ReinspectionDate
                    FROM dbo.utb_reinspect
                    WHERE ReinspectID = (SELECT MAX(ReinspectID) 
                                         from dbo.utb_assignment a
                                         INNER JOIN utb_Claim_Aspect_Service_Channel casc
                                            on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
                                         INNER join dbo.utb_reinspect r 
                                             ON r.ClaimAspectID = casc.ClaimAspectID
                                         WHERE a.ShopLocationID = sl.ShopLocationID
                                           AND r.EnabledFlag = 1)), ''),
            --a.AssignmentID,
            --ca.ClaimAspectNumber,
            ca.LynxID,
            IsNull(convert(varchar(4), VehicleYear) + ' ' + Make + ' ' + Model, ''),
            IsNull(cv.LicensePlateState, ''),
            IsNull(cv.LicensePlateNumber, ''),
            IsNull((SELECT TOP 1 i.NameFirst + ' ' + i.NameLast
                    FROM dbo.utb_involved i INNER JOIN dbo.utb_claim_aspect_involved cai ON i.InvolvedID = cai.InvolvedID
                                            INNER JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                            INNER JOIN dbo.utb_involved_role_type irt On ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                    WHERE irt.Name = 'Owner'
                      AND cai.ClaimAspectID = ca.ClaimAspectID), ''),

            Isnull((SELECT TOP 1 i.BusinessName
                    FROM dbo.utb_involved i INNER JOIN dbo.utb_claim_aspect_involved cai ON i.InvolvedID = cai.InvolvedID
                                            INNER JOIN dbo.utb_involved_role ir ON i.InvolvedID = ir.InvolvedID
                                            INNER JOIN dbo.utb_involved_role_type irt On ir.InvolvedRoleTypeID = irt.InvolvedRoleTypeID
                    WHERE irt.Name = 'Owner'
                      AND cai.ClaimAspectID = ca.ClaimAspectID), ''),
            a.AssignmentDate,
            a.CancellationDate,
            d.documentID,
            d.CreatedDate,
            ISNULL(dt.Name, '')

    FROM dbo.utb_shop_location sl 
    LEFT JOIN dbo.utb_assignment a ON sl.ShopLocationID = a.ShopLocationID
    INNER JOIN utb_Claim_Aspect_Service_Channel casc on a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
    INNER JOIN dbo.utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
    INNER JOIN dbo.utb_claim c ON ca.LynxID = c.LynxID
    LEFT JOIN dbo.utb_document d ON a.AssignmentID = d.AssignmentID
    LEFT JOIN dbo.utb_document_type dt ON d.DocumentTypeID = dt.DocumentTypeID
    LEFT JOIN dbo.utb_state_code sc ON sl.AddressState = sc.StateCode
    LEFT JOIN dbo.utb_claim_vehicle cv ON ca.ClaimAspectID = cv.ClaimAspectID
    WHERE  c.DemoFlag = 0
      --AND (a.ProgramFlag = 1 OR a.CEIProgramFlag = 1)
      AND (a.ProgramTypeCD = 'CEI' OR a.ProgramTypeCD = 'LS')
      AND a.assignmentsequencenumber = 1
      AND ((a.AssignmentDate >= @RptFromDate AND a.AssignmentDate <= @RptToDate) 
           OR (d.CreatedDate >= @RptFromDate AND d.CreatedDate <= @RptToDate AND dt.EstimateTypeFlag = 1))


    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpData', 16, 1, @ProcName)
        RETURN
    END
    
    /*UPDATE @tmpData t
    SET OriginalExtendedAmt = (SELECT TOP 1 es.OriginalExtendedAmt
                                FROM dbo.utb_estimate_summary es
                                LEFT JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
                                WHERE es.DocumentID = t.DocumentID
                                  AND est.Name = 'RepairTotal')*/

    
    SELECT  @RptFromDate as RptFromDate,
            @RptToDate as RptToDate,
            t.*,
            es.OriginalExtendedAmt
    FROM @tmpData t
    LEFT JOIN dbo.utb_estimate_summary es ON t.DocumentID = es.DocumentID
    INNER JOIN dbo.utb_estimate_summary_type est ON es.EstimateSummaryTypeID = est.EstimateSummaryTypeID
    WHERE est.Name = 'RepairTotal'
      AND t.DocumentID IS NOT NULL
      
      
    UNION ALL
    
    SELECT  @RptFromDate,
            @RptToDate,
            t.*,
            NULL
    FROM @tmpData t
    WHERE t.DocumentID IS NULL
    
    ORDER BY stateValue, ShopLocationName, LYNXId
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptShopActivity' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptShopActivity TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/

