-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionIns' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspSessionIns 
END

GO
/****** Object:  StoredProcedure [dbo].[uspSessionIns]    Script Date: 08/08/2014 12:25:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspSessionIns
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jim Stein
* FUNCTION:     Creates a new session
*
* PARAMETERS:  
* (I) ModifiedBy            The csr creating the new session
*
* RESULT SET:
* SessionID                 The newly created session key
*
*
* VSS
* $Workfile: uspSessionIns.sql $
* $Archive: /Database/APD/v1.0.0/procedure/uspSessionIns.sql $
* $Revision: 2 $
* $Author: Jim $
* $Date: 10/16/01 11:53a $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE [dbo].[uspSessionIns]
	@ModifiedBy AS udt_std_id
AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT ON
    
    -- SET TRANSACTION ISOLATION LEVEL to SERIALIZABLE to initiate pessimistic concurrency control
    
    -- SET TRANSACTION ISOLATION LEVEL SERIALIZABLE --glsd451

    -- Declare internal variables

    DECLARE @error              AS INT
    DECLARE @rowcount           AS INT
    DECLARE @trancount          AS INT

    SET @error = 0
    
    IF 0 = @error
    BEGIN
        DECLARE @SessionKey UNIQUEIDENTIFIER

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    -- Start the new transaction
    
    IF 0 = @error
    BEGIN
        SET @trancount = @@TRANCOUNT

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        BEGIN TRANSACTION

        -- Capture any error
        
        SELECT @error = @@ERROR
    END
    
    IF 0 = @error
    BEGIN
        SET @SessionKey = NEWID()

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    IF 0 = @error
    BEGIN
        INSERT INTO dbo.utb_session
        (
            SessionID,
            SysLastUserID
        )
        VALUES	
        (
            @SessionKey,
            @ModifiedBy
        )

        -- Capture any error
        
        SELECT @error = @@ERROR
    END

    -- Final transaction success determination
        
    IF @@TRANCOUNT > @trancount
    BEGIN
        IF 0 = @error
            COMMIT TRANSACTION
        ELSE
            ROLLBACK TRANSACTION
    END

    SELECT @SessionKey
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSessionIns' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSessionIns TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END