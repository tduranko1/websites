-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptProgramShopSupportTasks' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptProgramShopSupportTasks 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptProgramShopSupportTasks
* SYSTEM:       Lynx Services APD
* AUTHOR:       [your name here]
* FUNCTION:     [procedure description here]
*
* PARAMETERS:  
* (I) @input                description
* (O) @output               description
* (I) @ModifiedDateTime     The "previous" updated date
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptProgramShopSupportTasks
AS
BEGIN
    SET NOCOUNT ON
    
set nocount on

declare @now as datetime
declare @yesterdaySlab1 as datetime
declare @yesterdaySlab2 as datetime
declare @yesterdaySlab3 as datetime
declare @yesterdaySlab4 as datetime
declare @userid_work    as bigint
declare @procName as varchar(50)
declare @created_count as bigint
declare @edited_count as bigint
declare @completed_count as bigint
declare @notescreated_count as bigint
declare @notesedited_count as bigint
declare @GrandTotalTaskCount as bigint
declare @GrandTotalFileCount as bigint

set @ProcName = 'script'
set @now = current_timestamp

set @yesterdaySlab1 = convert(datetime, convert(varchar, dateadd(day, -1, @now), 110))
set @yesterdaySlab2 = dateadd(hour, 12, @yesterdaySlab1)
set @yesterdaySlab3 = dateadd(hour, 5, @yesterdaySlab2)
set @yesterdaySlab4 = dateadd(hour, 7, @yesterdaySlab3)

-- select @yesterdaySlab1, @yesterdaySlab2, @yesterdaySlab3, @yesterdaySlab4

print 'Report from ' + convert(varchar, @yesterdaySlab1) + ' to ' + convert(varchar, @yesterdaySlab4)
PRINT ''

DECLARE @tmpUser TABLE 
(
    UserID              bigint NOT NULL,
    UserName            varchar(25),
    TasksCreated12        int,
    TasksEdited12         int,
    TasksCompleted12      int,
    NotesCreated12        int,
    NotesEdited12         int,
    TasksCreated6        int,
    TasksEdited6         int,
    TasksCompleted6      int,
    NotesCreated6        int,
    NotesEdited6         int,
    TasksCreated0        int,
    TasksEdited0         int,
    TasksCompleted0      int,
    NotesCreated0        int,
    NotesEdited0         int
)

insert into @tmpUser
(UserID, UserName)
select u.UserID, u.NameFirst + ' ' + u.NameLast
from utb_user u
where userid in (2350, 3755, 2494)

/*select u.UserID, u.NameFirst + ' ' + u.NameLast
from dbo.utb_assignment_pool_user apu
left join utb_user u on apu.userid = u.userid
where AssignmentPoolID = 8
  and u.EnabledFlag = 1
  and u.SupervisorUserID = 2
  and u.OfficeID is null*/

DECLARE @tmpChecklist TABLE 
(
    CheckListID     bigint,
    ClaimAspectServiceChannelID bigint,
    CreatedUserID   bigint,
    SysLastUserID    bigint,
    CreatedDate     datetime,
    SysLastUpdatedDate  datetime,
    TaskID          bigint,
    AlarmDate       datetime,
    AlarmStatus     varchar(10),
    AssignedUserID  bigint,
    LynxID          bigint
)

DECLARE @tmpHistory TABLE 
(
    LogID           bigint,
    CompletedUserID   bigint,
    CompletedDate    datetime
)

DECLARE @tmpNotes TABLE 
(
    DocumentID     bigint,
    CreatedUserID   bigint,
    SysLastUserID    bigint,
    CreatedDate     datetime,
    SysLastUpdatedDate  datetime
)

DECLARE @tmpSummary TABLE 
(
    OrderNum     tinyInt,
    SubOrderNum  tinyInt,
    TaskName     varchar(50),
    Status   varchar(50),
    TaskCount   varchar(4),
    FileCount   varchar(4)
)

INSERT INTO @tmpChecklist
SELECT cl.CheckListID, cl.ClaimAspectServiceChannelID, cl.CreatedUserID, cl.SysLastUserID, cl.CreatedDate, 
        cl.SysLastUpdatedDate, cl.TaskID, convert(varchar, cl.AlarmDate, 110),
    CASE
        WHEN cl.AlarmDate < @yesterdaySlab1 THEN 'overdue'
        --WHEN AlarmDate BETWEEN  @yesterdaySlab1 AND @yesterdaySlab2 then 'today'
        WHEN cl.AlarmDate > @yesterdaySlab4 then 'future'
        else 'today'
    END,
    cl.AssignedUserID,
    ca.LynxID
from utb_checklist cl
left join utb_claim_aspect_service_channel casc ON cl.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
left join utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
where AssignedUserID in (select userID from @tmpUser)

insert into @tmpHistory
select LogID, CompletedUserID, CompletedDate
from @tmpUser t
left join utb_history_log h on t.UserID = h.CompletedUserID
where CompletedDate between @yesterdaySlab1 and @yesterdaySlab4

INSERT INTO @tmpNotes
SELECT DocumentID, CreatedUserID, SysLastUserID, CreatedDate, SysLastUpdatedDate
from utb_document
where (CreatedDate between @yesterdaySlab1 and @yesterdaySlab4
   or SysLastUpdatedDate between @yesterdaySlab1 and @yesterdaySlab4)
  and DocumentTypeID = 7


DECLARE csrUser CURSOR FOR
    SELECT userid FROM @tmpUser
    


OPEN csrUser

IF @@ERROR <> 0
BEGIN
   -- SQL Server Error

    RAISERROR  ('99|%s', 16, 1, @ProcName)
    RETURN
END    


FETCH next
FROM csrUser
INTO @userid_work

IF @@ERROR <> 0
BEGIN
   -- SQL Server Error

    RAISERROR  ('99|%s', 16, 1, @ProcName)
    RETURN
END    


WHILE @@Fetch_Status = 0
BEGIN
    -- print '@userid_work=' + convert(varchar, @userid_work)
    set @created_count = 0
    set @edited_count = 0
    set @completed_count = 0
    set @notescreated_count = 0
    set @notesedited_count = 0

    -- task created
    select @created_count = count(CheckListID)
    from @tmpChecklist
    where CreatedUserID = @userid_work
      and CreatedDate between @yesterdaySlab1 and @yesterdaySlab2

    -- task edited
    select @edited_count = count(CheckListID)
    from @tmpChecklist
    where SysLastUserID = @userid_work
      and CreatedDate between @yesterdaySlab1 and @yesterdaySlab2

    -- task completed
    select @completed_count = count(LogID)
    from @tmpHistory
    where CompletedUserID = @userid_work
      and CompletedDate between @yesterdaySlab1 and @yesterdaySlab2

    -- notes created
    select @notescreated_count = count(DocumentID)
    from @tmpNotes
    where CreatedUserID = @userid_work
      and CreatedDate between @yesterdaySlab1 and @yesterdaySlab2

    -- notes edited
    select @notescreated_count = count(DocumentID)
    from @tmpNotes
    where SysLastUserID = @userid_work
      and SysLastUpdatedDate between @yesterdaySlab1 and @yesterdaySlab2

    update @tmpUser
    set TasksCreated12 = @created_count,
        TasksEdited12 = @edited_count,
        TasksCompleted12 = @completed_count,
        NotesCreated12 = @notescreated_count,
        NotesEdited12 = @notesedited_count
    where userid = @userid_work

    -- 2nd timefram
    set @created_count = 0
    set @edited_count = 0
    set @completed_count = 0
    set @notescreated_count = 0
    set @notesedited_count = 0

    -- task created
    select @created_count = count(CheckListID)
    from @tmpChecklist
    where CreatedUserID = @userid_work
      and CreatedDate between @yesterdaySlab2 and @yesterdaySlab3

    -- task edited
    select @edited_count = count(CheckListID)
    from @tmpChecklist
    where SysLastUserID = @userid_work
      and CreatedDate between @yesterdaySlab2 and @yesterdaySlab3

    -- task completed
    select @completed_count = count(LogID)
    from @tmpHistory
    where CompletedUserID = @userid_work
      and CompletedDate between @yesterdaySlab2 and @yesterdaySlab3

    -- notes created
    select @notescreated_count = count(DocumentID)
    from @tmpNotes
    where CreatedUserID = @userid_work
      and CreatedDate between @yesterdaySlab2 and @yesterdaySlab3

    -- notes edited
    select @notescreated_count = count(DocumentID)
    from @tmpNotes
    where SysLastUserID = @userid_work
      and SysLastUpdatedDate between @yesterdaySlab2 and @yesterdaySlab3

    update @tmpUser
    set TasksCreated6 = @created_count,
        TasksEdited6 = @edited_count,
        TasksCompleted6 = @completed_count,
        NotesCreated6 = @notescreated_count,
        NotesEdited6 = @notesedited_count
    where userid = @userid_work

    -- 3rd timefram
    set @created_count = 0
    set @edited_count = 0
    set @completed_count = 0
    set @notescreated_count = 0
    set @notesedited_count = 0

    -- task created
    select @created_count = count(CheckListID)
    from @tmpChecklist
    where CreatedUserID = @userid_work
      and CreatedDate between @yesterdaySlab3 and @yesterdaySlab4

    -- task edited
    select @edited_count = count(CheckListID)
    from @tmpChecklist
    where SysLastUserID = @userid_work
      and CreatedDate between @yesterdaySlab3 and @yesterdaySlab4

    -- task completed
    select @completed_count = count(LogID)
    from @tmpHistory
    where CompletedUserID = @userid_work
      and CompletedDate between @yesterdaySlab3 and @yesterdaySlab4

    -- notes created
    select @notescreated_count = count(DocumentID)
    from @tmpNotes
    where CreatedUserID = @userid_work
      and CreatedDate between @yesterdaySlab3 and @yesterdaySlab4

    -- notes edited
    select @notescreated_count = count(DocumentID)
    from @tmpNotes
    where SysLastUserID = @userid_work
      and SysLastUpdatedDate between @yesterdaySlab3 and @yesterdaySlab4

    update @tmpUser
    set TasksCreated0 = @created_count,
        TasksEdited0 = @edited_count,
        TasksCompleted0 = @completed_count,
        NotesCreated0 = @notescreated_count,
        NotesEdited0 = @notesedited_count
    where userid = @userid_work

    FETCH next
    FROM csrUser
    INTO @userid_work

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR  ('99|%s', 16, 1, @ProcName)
        RETURN
    END    
END

CLOSE csrUser
DEALLOCATE csrUser


insert into @tmpUser
select  99,
        'TOTAL',
        sum(TasksCreated12),
        sum(TasksEdited12),
        sum(TasksCompleted12),
        sum(NotesCreated12),
        sum(NotesEdited12),
        sum(TasksCreated6),
        sum(TasksEdited6),
        sum(TasksCompleted6),
        sum(NotesCreated6),
        sum(NotesEdited6),
        sum(TasksCreated0),
        sum(TasksEdited0),
        sum(TasksCompleted0),
        sum(NotesCreated0),
        sum(NotesEdited0)
from @tmpUser
where userID not in (2494)


select convert(varchar(11), @yesterdaySlab1, 110) as 'Date', 
        UserName,
        TasksCreated12,
        TasksEdited12,
        TasksCompleted12,
        NotesCreated12,
        NotesEdited12,
        TasksCreated6,
        TasksEdited6,
        TasksCompleted6,
        NotesCreated6,
        NotesEdited6,
        TasksCreated0,
        TasksEdited0,
        TasksCompleted0,
        NotesCreated0,
        NotesEdited0,
        TasksCreated12 + TasksCreated6 + TasksCreated0 as TotalTasksCreated, 
        TasksEdited12 + TasksEdited6 + TasksEdited0 as TotalTasksEdited, 
        TasksCompleted12 + TasksCompleted6 + TasksCompleted0 as TotalTasksCompleted, 
        NotesCreated12 + NotesCreated6 + NotesCreated0 as TotalNotesCreated, 
        NotesEdited12 + NotesEdited6 + NotesEdited0 as TotalNotesEdited
from @tmpUser
where userID not in (2494)

-- Summary

insert into @tmpSummary
SELECT  case 
            when cl.AlarmStatus = 'overdue' then 1
            when cl.AlarmStatus = 'today' then 2
            else 3
        end,
        0,
t.Name, cl.AlarmStatus, convert(varchar, count(cl.AlarmStatus)), 
''
from @tmpChecklist cl
left join utb_task t on cl.TaskID = t.TaskID
where cl.AssignedUserID = 2494
group by cl.AlarmStatus, t.Name, cl.TaskID
order by t.name, cl.AlarmStatus


insert into @tmpSummary
select 1,
       1,
    'Sub Total - Overdue',
    '',
    (select convert(varchar, count(AlarmStatus))
     from @tmpCheckList cl
     where cl.AssignedUserID = 2494
      and cl.AlarmStatus = 'Overdue'),
    (select distinct convert(varchar, count(LynxID))
     from @tmpCheckList cl
     where cl.AssignedUserID = 2494
      and cl.AlarmStatus = 'Overdue')

insert into @tmpSummary
select 2,
       1,
    'Sub Total - Today',
    '',
    (select convert(varchar, count(AlarmStatus))
     from @tmpCheckList cl
     where cl.AssignedUserID = 2494
      and cl.AlarmStatus = 'Today'),
    (select distinct convert(varchar, count(LynxID))
     from @tmpCheckList cl
     where cl.AssignedUserID = 2494
      and cl.AlarmStatus = 'Today')

insert into @tmpSummary
select 3,
       1,
    'Sub Total - Future',
    '',
    (select convert(varchar, count(AlarmStatus))
     from @tmpCheckList cl
     where cl.AssignedUserID = 2494
      and cl.AlarmStatus = 'Future'),
    (select distinct convert(varchar, count(LynxID))
     from @tmpCheckList cl
     where cl.AssignedUserID = 2494
      and cl.AlarmStatus = 'Future')

insert into @tmpSummary
select 4,
       1,
    'Claims with no Task',
    '',
    0,
    (select count(ca.LynxID)
        from dbo.utb_claim_aspect_service_channel casc
        left join utb_claim_aspect ca ON casc.ClaimAspectID = ca.ClaimAspectID
        left join utb_claim_aspect_status cas on ca.ClaimAspectID = cas.ClaimAspectID
        where ca.ClaimAspectTypeID = 9
          and StatusTypeCD is null
          and cas.StatusID = 100
          and ca.SupportUserID = 2494
          and casc.ClaimAspectServiceChannelID not in (
            select distinct ClaimAspectServiceChannelID
                from @tmpCheckList))

-- grand total


select  @GrandTotalTaskCount = sum(convert(int, TaskCount))
from @tmpSummary
where SubOrderNum = 0

select  @GrandTotalFileCount = sum(convert(int, FileCount))
from @tmpSummary
where Status = ''

insert into @tmpSummary
select 5,
       1,
    'GRAND TOTAL',
    '',
    @GrandTotalTaskCount,
    @GrandTotalFileCount

--select * from @tmpSummary


print '-- Summary --'
print ''
select convert(varchar(11), @yesterdaySlab1, 110) as 'Date',
        TaskName,
        substring(Status, 1, 8) as 'Status',
        TaskCount,
        FileCount
from @tmpSummary
order by OrderNum, SubOrderNum, TaskName

    
    
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptProgramShopSupportTasks' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptProgramShopSupportTasks TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/