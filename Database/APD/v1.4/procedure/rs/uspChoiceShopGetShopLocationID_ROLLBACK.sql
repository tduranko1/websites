-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspChoiceShopGetShopLocationID' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspChoiceShopGetShopLocationID 
END

GO
