-- If it already exists, drop the procedure 
IF EXISTS (SELECT name FROM sysobjects 
WHERE name = 'uspSMTWebSignupGetCodes' AND type = 'P') 
BEGIN 
DROP PROCEDURE dbo.uspSMTWebSignupGetCodes 
END 
GO
/****** Object:  StoredProcedure [dbo].[uspSMTWebSignupGetCodes]    Script Date: 08/05/2011 11:11:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/***********************************************************************************************************************
*
* PROCEDURE:    uspSMTWebSignupGetCodes
* SYSTEM:       Lynx Services APD
* AUTHOR:       Sarah Liss
* FUNCTION:     Retrieve data to populate Web Signup
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].uspSMTWebSignupGetCodes

AS
BEGIN
    -- SET NOCOUNT to ON and no longer display the count message
    
    SET NOCOUNT off
    
    -- Declare internal variables
    DECLARE @ProcName         AS varchar(30)       -- Used for raise error stmts 
    
    SET @ProcName = 'uspSMTWebSignupGetCodes'
   
    DECLARE @tmpReference TABLE 
    (
        ListName        varchar(50) NOT NULL,
        DisplayOrder    int         NULL,
        ReferenceId     varchar(10) NOT NULL,
        Name            varchar(50) NOT NULL
    )

    INSERT INTO @tmpReference (ListName, DisplayOrder, ReferenceId, Name)
        
    Select 'State' AS ListName, DisplayOrder, StateCode, StateValue
    From dbo.utb_state_code
    Where EnabledFlag = 1
    
    UNION ALL
    
    Select 'EstimatePackage', DisplayOrder, EstimatePackageID, Name
    From dbo.utb_estimate_package
    Where EnabledFlag = 1
    
    UNION ALL
    
    Select 'CommunicationMethod', DisplayOrder, CommunicationMethodID, Name
    From dbo.utb_communication_method
    Where EnabledFlag = 1
    
    UNION ALL
    
    SELECT  'RepairFacilityType' AS ListName, Null, Code, Name 
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_shop_load', 'RepairFacilityTypeCD' ) 
    
    UNION ALL
    
    SELECT 'WarrantyPeriod',
           NULL,
           Code,
           Name
    FROM dbo.ufnUtilityGetReferenceCodes( 'utb_shop_load', 'WarrantyPeriodRefinishCD' )
    
    UNION ALL
    
    SELECT 'BusinessType',
           NULL,
           Code,
           Name
    FROM dbo.ufnUtilityGetReferenceCodes( 'utb_shop_load', 'BusinessTypeCD' )  
    
    UNION ALL
    
    SELECT  'ContactMethod' AS ListName,DisplayOrder, PersonnelContactMethodID, Name 
    FROM    dbo.utb_personnel_contact_method
    WHERE   EnabledFlag = 1
      AND   DisplayOrder IS NOT NULL
      
    UNION ALL
    
    SELECT  'GlassReplacementChargeType' AS ListName, Null, Code, Name 
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_shop_location_pricing', 'GlassReplacementChargeTypeCD' ) 
      
    UNION ALL
    
    SELECT  'TowInChargeType' AS ListName, Null, Code, Name 
    FROM    dbo.ufnUtilityGetReferenceCodes( 'utb_shop_location_pricing', 'TowInChargeTypeCD' )  
    
    UNION ALL
    
    SELECT 'PersonnelType' AS ListName, DisplayOrder, PersonnelTypeID, Name
    FROM dbo.utb_personnel_type
    WHERE EnabledFlag = 1
      AND AppliesToCD = 'L'
      
    ORDER BY ListName, DisplayOrder
    
    
    IF @@ERROR <> 0
    BEGIN
        -- Insertion failure
    
        RAISERROR('105|%s|@tmpReference', 16, 1, @ProcName)
        RETURN
    END
   
    SELECT * from @tmpReference
 END

-- Permissions
IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspSMTWebSignupGetCodes' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspSMTWebSignupGetCodes TO 
        ugr_lynxapd
    -- Commit the transaction for dropping and creating the stored procedure
    -- COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure
    RAISERROR ('Stored procedure creation failure.', 16, 1)
    -- ROLLBACK
END

GO