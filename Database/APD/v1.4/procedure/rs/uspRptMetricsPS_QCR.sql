-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMetricsPS_QCR' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspRptMetricsPS_QCR 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspRptMetricsPS_QCR
* SYSTEM:       Lynx Services APD
* AUTHOR:       Ramesh Vishegu
* FUNCTION:     Returns a list of Audited estimates that are different from the electronic version.
*
* PARAMETERS:  
*
* RESULT SET:
*   Returns a list of Audited estimates that are different from the electronic version.
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspRptMetricsPS_QCR
AS
BEGIN
   set nocount on
   set ANSI_WARNINGS off
   /******************************************************************************
   * PS QCR metrics
   ******************************************************************************/

   DECLARE @RptMonthStart as datetime
   DECLARE @RptYesterdayStart as datetime
   DECLARE @RptYesterdayEnd as datetime
   DECLARE @EstimateTypeOEMParts as int
   DECLARE @EstimateTypeRepairTotal as int
   DECLARE @EstimateSummaryIDPartsTaxable as int
   DECLARE @now as datetime
   DECLARE @debug as bit


   DECLARE @tmpOEMClaims TABLE (
      LynxID            bigint,
      OEMPartsAmt       decimal(9, 2),
      PartsTaxable      decimal(9, 2),
      RepairTotal       decimal(9, 2),
      AnalystUser       varchar(50)
   )

   DECLARE @tmpRepairClaims TABLE (
      LynxID            bigint,
      WorkStartDate     datetime,
      WorkEndDate       datetime,
      CTBusinessDays    decimal(9,1),
      AnalystUser       varchar(50)
   )

   DECLARE @tmpOpenTasks TABLE (
      ClaimAspectServiceChannelID   bigint,
      AnalystUser                   varchar(50)
   )

   DECLARE @tmpReport TABLE (
      idx                int,
      Metric             varchar(55),
      DailyData          varchar(10),
      MTDData            varchar(10)
   )

   SET @now = CURRENT_TIMESTAMP
   --SET @now = convert(datetime, '4/11/2009 8:00:00')--CURRENT_TIMESTAMP
   SET @RptMonthStart = convert(varchar, month(@now)) + '/1/' + convert(varchar, year(@now))
   IF DAY(@now) = 1 
   BEGIN
      SET @RptMonthStart = DATEADD(month, -1, @RptMonthStart)
   END
   SET @RptYesterdayStart = dateadd(day, -1, convert(datetime, convert(varchar, @now, 101)))
   SET @RptYesterdayEnd = dateadd(second, -1, convert(datetime, convert(varchar, @now, 101)))

   SET @debug = 1

   SELECT @EstimateTypeOEMParts = EstimateSummaryTypeID
   FROM utb_estimate_summary_type
   WHERE Name = 'OEMParts'
     AND CategoryCD = 'OT'
     
   SELECT @EstimateTypeRepairTotal = EstimateSummaryTypeID
   FROM utb_estimate_summary_type
   WHERE Name = 'RepairTotal'
     AND CategoryCD = 'TT'

   SELECT @EstimateSummaryIDPartsTaxable = EstimateSummaryTypeID
   FROM utb_estimate_summary_type
   WHERE CategoryCD = 'PC'
     AND Name = 'PartsTaxable'
     
   INSERT INTO @tmpOEMClaims
   SELECT ca.LynxID,
          (select es.AgreedExtendedAmt
           from dbo.utb_estimate_summary es
           where es.DocumentID = d.DocumentID
             and es.EstimateSummaryTypeID = @EstimateTypeOEMParts),
          (select es.AgreedExtendedAmt
           from dbo.utb_estimate_summary es
           where es.DocumentID = d.DocumentID
             and es.EstimateSummaryTypeID = @EstimateSummaryIDPartsTaxable),
          (select es.AgreedExtendedAmt
           from dbo.utb_estimate_summary es
           where es.DocumentID = d.DocumentID
             and es.EstimateSummaryTypeID = @EstimateTypeRepairTotal),
           ua.NameFirst + ' ' + ua.NameLast  
   FROM utb_document d
   LEFT JOIN dbo.utb_claim_aspect_service_channel_document cascd on d.DocumentID = cascd.DocumentID
   LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cascd.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
   LEFT JOIN dbo.utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
   LEFT JOIN dbo.utb_document_source ds on d.DocumentSourceID = ds.DocumentSourceID
   LEFT JOIN dbo.utb_user ua on ca.AnalystUserID = ua.UserID
   LEFT JOIN dbo.utb_claim c on ca.LynxID = c.LynxID
   LEFT JOIN dbo.utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
   WHERE casc.ServiceChannelCD = 'PS'
     AND d.DocumentTypeID in (3, 10)
     AND ds.VANFlag = 1
     AND c.DemoFlag = 0
     AND i.DemoFlag = 0
     AND d.CreatedDate between @RptMonthStart and @RptYesterdayEnd
     AND ca.AnalystUserID is not null
   order by ca.LynxID asc

   INSERT INTO @tmpReport 
   SELECT 1,
          'Average OEM %',
          '',
          convert(varchar, CONVERT(decimal(9,2), AVG( CASE
                                                         WHEN PartsTaxable > 0 THEN (OEMPartsAmt / PartsTaxable) * 100.00
                                                         ELSE 0
                                                      END)))
   FROM @tmpOEMClaims

   insert into @tmpReport
   select 1, '', '', ''

   insert into @tmpReport
   select 1,
          AnalystUser,
          '',
          convert(varchar, CONVERT(decimal(9,2), AVG( CASE
                                                         WHEN PartsTaxable > 0 THEN (OEMPartsAmt / PartsTaxable) * 100.00
                                                         ELSE 0
                                                      END)))
   from @tmpOEMClaims   
   group by AnalystUser

   -- select * from @tmpOEMClaims


   insert into @tmpRepairClaims
   SELECT c.LynxID,
          casc.WorkStartDate,
          casc.WorkEndDate,
          dbo.ufnUtilityTrueBusinessHoursDiff(casc.WorkStartDate, casc.WorkEndDate) / 9.0,
          ua.NameFirst + ' ' + ua.NameLast 
   FROM dbo.utb_claim_aspect_service_channel casc
   LEFT JOIN dbo.utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
   LEFT JOIN dbo.utb_claim c on ca.LynxID = c.LynxID
   LEFT JOIN dbo.utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
   LEFT JOIN dbo.utb_user ua on ca.AnalystUserID = ua.UserID
   WHERE ServiceChannelCD = 'PS'
     AND c.DemoFlag = 0
     AND i.DemoFlag = 0
     AND ca.AnalystUserID is not null
     AND (WorkStartConfirmFlag = 1 AND WorkEndConfirmFlag = 1)
     AND ((WorkStartDate between @RptMonthStart and @RptYesterdayEnd)
      OR  (WorkEndDate between @RptMonthStart and @RptYesterdayEnd))
      
   INSERT INTO @tmpReport 
   SELECT 2,
          'Average Repair Start to Repair End in Business Days',
          '',
          SUBSTRING(convert(varchar, ROUND(AVG(CTBusinessDays), 1)), 1, 3)
   FROM @tmpRepairClaims

   insert into @tmpReport
   select 2, '', '', ''

   INSERT INTO @tmpReport 
   SELECT 2,
          AnalystUser,
          '',
          SUBSTRING(convert(varchar, ROUND(AVG(CTBusinessDays), 1)), 1, 3)
   FROM @tmpRepairClaims
   GROUP BY AnalystUser


   INSERT INTO @tmpOpenTasks
   SELECT distinct cl.ClaimAspectServiceChannelID,
          ua.NameFirst + ' ' + ua.NameLast 
   FROM dbo.utb_checklist cl
   LEFT JOIN dbo.utb_claim_aspect_service_channel casc on cl.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
   LEFT JOIN dbo.utb_claim_aspect ca on casc.ClaimAspectID = ca.ClaimAspectID
   LEFT JOIN dbo.utb_claim c on ca.LynxID = c.LynxID
   LEFT JOIN dbo.utb_insurance i on c.InsuranceCompanyID = i.InsuranceCompanyID
   LEFT JOIN dbo.utb_user ua on cl.AssignedUserID = ua.UserID
   WHERE casc.ServiceChannelCD = 'PS'
     AND cl.AssignedUserID = ca.AnalystUserID
     AND c.DemoFlag = 0
     AND i.DemoFlag = 0
     AND cl.CreatedDate between @RptMonthStart and @RptYesterdayEnd
   order by cl.ClaimAspectServiceChannelID 


   INSERT INTO @tmpReport 
   SELECT 3,
          'Count of Files with Open Tasks',
          convert(varchar, COUNT(ClaimAspectServiceChannelID)),
          ''
   FROM @tmpOpenTasks

   insert into @tmpReport
   select 3, '', '', ''

   INSERT INTO @tmpReport 
   SELECT 3,
          AnalystUser,
          convert(varchar, COUNT(ClaimAspectServiceChannelID)),
          ''
   FROM @tmpOpenTasks
   GROUP BY AnalystUser


     
   print 'APD Daily Metric Report per Monthly Cycle - PS QCR'
   print '-----------------------------------------------------'
   print 'Daily Data for: ' + convert(varchar, @RptYesterdayStart, 101)
   print 'Month: ' + convert(varchar, Datepart("mm", @RptMonthStart)) + '/' + convert(varchar, Datepart("yyyy", @RptMonthStart))
   print '                                                            Program Shop     '
   print '                                                        ---------------------'

   SELECT Metric, 
          '          ' as 'Daily',
          convert(varchar(10), space(10 - len(MTDData)) + MTDData) as 'MTD'
   FROM @tmpReport
   WHERE idx = 1

   SELECT Metric, 
          '          ' as 'Daily',
          convert(varchar(10), space(10 - len(MTDData)) + MTDData) as 'MTD'
   FROM @tmpReport
   WHERE idx = 2

   SELECT Metric, 
          convert(varchar(10), space(10 - len(DailyData)) + DailyData) as 'Daily',
          '          ' as 'MTD'
   FROM @tmpReport
   WHERE idx = 3

   print '--- END OF REPORT ---'
   
END



GO


-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspRptMetricsPS_QCR' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspRptMetricsPS_QCR TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
