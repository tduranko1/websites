-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhGetExceptions' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspDtwhGetExceptions 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspDtwhGetExceptions
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     This procedure scans the APD datawarehouse looking for conditions that need to be quickly corrected
*               by operations.  
*
* PARAMETERS:   @ThresholdDate  - cutoff date of report.  Only vehicle activity back to this date will be considered
*                                 If this is NULL, all records are included
*
* RESULT SET:
*     LynxID-VehicleNumber,
*     DateClosed,
*     ExceptionFound
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspDtwhGetExceptions
    @ThresholdDate  udt_std_datetime = NULL --'4/10/2002 0:00:00'  --Project:210474 APD Modified the defaults for the input parameter when we did the code merge M.A.20061116
AS
BEGIN
    DECLARE @tmpFact TABLE
    (
        FactID              bigint
    )


    DECLARE @tmpException TABLE
    (
        LynxID              bigint,
        VehicleNumber       int,
        LastCloseDate       datetime,
        ExceptionString     varchar(250),
        SuggestedAction     varchar(250)
    )
    
    DECLARE @EstimateExceptionAmt   money
    
    DECLARE @ProcName           AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspDtwhGetExceptions'

    
    SET @EstimateExceptionAmt = 20000

    /************************************************************************************
    --Project:210474 APD Added the following code when we did the code merge M.A.20061114
    ************************************************************************************/    
    IF @ThresholdDate IS NULL
    BEGIN
        SET @ThresholdDate = CURRENT_TIMESTAMP - 5
    END
    /************************************************************************************
    --Project:210474 APD Added the code above when we did the code merge M.A.20061114
    ************************************************************************************/
    
    -- Now compile the subset of records from the datawarehouse that we are interested in looking at based
    -- on whether the close or reclosed date > @ThresholdDate
    
    INSERT INTO @tmpFact
      SELECT FactID
        FROM dbo.utb_dtwh_fact_claim fc
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        WHERE dtc.DateValue > @ThresholdDate OR dtrc.DateValue > @ThresholdDate
        
    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpFact', 16, 1, @ProcName)
        RETURN
    END

    
    -- Search for any estimate amounts above the threshold amount
    
    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Estimate Amount exceeds $' + Convert(varchar(10), @EstimateExceptionAmt) + ' threshold.',
              'Verify Estimate data is not erroneous.'
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE fc.TimeIDClosed IS NOT NULL
          AND (fc.OriginalEstimateGrossAmt > @EstimateExceptionAmt OR
               fc.AuditedEstimateGrossAmt > @EstimateExceptionAmt OR
               fc.FinalEstimateGrossAmt > @EstimateExceptionAmt OR
               fc.FinalSupplementGrossAmt > @EstimateExceptionAmt)

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END


    -- Search for any audited estimates on Program Shop/IA channeled vehicles
    
    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Audited estimate found on Program Shop/IA channelled vehicle.',
              'Set all estimates to "Original",'
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE fc.TimeIDClosed IS NOT NULL
          AND dsc.ServiceChannelCD IN ('PS', 'IA')
          AND fc.AuditedEstimateGrossAmt > 0

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END

               
    -- Search for any PS/IA vehicles dispositioned as "RC", "CO", or "TL" without no original estimates > 0. 
    
    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Missing original estimate(s) on Program Shop/IA Repaired, Cashed Out, or Total Loss vehicle.',
              'Verify at least one estimate is marked as "Original", Notify IT if this is already the case'
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_disposition_type dt ON (fc.DispositionTypeID = dt.DispositionTypeID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE fc.TimeIDClosed IS NOT NULL
          AND dsc.ServiceChannelCD IN ('PS', 'IA')
          AND dt.DispositionTypeCD IN ('RC', 'CO', 'TL')
          AND (fc.OriginalEstimateGrossAmt IS NULL OR fc.OriginalEstimateGrossAmt = 0)
               
    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END

               
    -- Search for any PS/IA vehicles dispositioned as "RC", "CO", or "TL" without a final estimate
    
    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Missing final estimate(s) on Program Shop/IA Repaired, Cashed Out, or Total Loss vehicle.',
              'Verify at least one estimate is marked as "Original", Notify IT if this is already the case'
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_disposition_type dt ON (fc.DispositionTypeID = dt.DispositionTypeID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE fc.TimeIDClosed IS NOT NULL
          AND dsc.ServiceChannelCD IN ('PS', 'IA')
          AND dt.DispositionTypeCD IN ('RC', 'CO', 'TL')
          AND (fc.FinalEstimateGrossAmt IS NULL OR fc.FinalEstimateGrossAmt = 0)
               
    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END

               
    -- Search for any completed Desk Audits without original estimates > 0. 
    
    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Desk Audit missing original estimate(s).',
              'Verify at least one estimate is marked as "Original", Notify IT if this is already the case'
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_disposition_type dt ON (fc.DispositionTypeID = dt.DispositionTypeID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE fc.TimeIDClosed IS NOT NULL
          AND dsc.ServiceChannelCD = 'DA'
          AND dt.DispositionTypeCD = 'RC'
          AND (fc.OriginalEstimateGrossAmt IS NULL OR fc.OriginalEstimateGrossAmt = 0)
               
    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END

               
    -- Search for any completed Desk Audits without audited estimates > 0. 
    
    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Desk Audit missing audited estimate(s).',
              'Verify at least one estimate is marked as "Audited", Notify IT if this is already the case'              
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_disposition_type dt ON (fc.DispositionTypeID = dt.DispositionTypeID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE fc.TimeIDClosed IS NOT NULL
          AND dsc.ServiceChannelCD = 'DA'
          AND dt.DispositionTypeCD = 'RC'
          AND (fc.AuditedEstimateGrossAmt IS NULL OR fc.AuditedEstimateGrossAmt = 0)

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END


    -- Search for any completed Desk Audits with the audited amount more than 110% greater than the original estimate. 
    
    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Desk Audit - Audited estimate amount more than 5% above original estimate.',
              'Verify Estimate data is not erroneous'              
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc ON (dat.ServiceChannelID = dsc.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_disposition_type dt ON (fc.DispositionTypeID = dt.DispositionTypeID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE fc.TimeIDClosed IS NOT NULL
          AND dsc.ServiceChannelCD = 'DA'
          AND dt.DispositionTypeCD = 'RC'
          AND fc.AuditedEstimateGrossAmt / fc.OriginalEstimateGrossAmt > 1.05

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END


    -- Search for any closed vehicles with missing initial or closing assignment types

    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Initial or closing assignment type not set.',
              'Verify Assignment Type is set, Notify IT if this is already the case'
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE fc.TimeIDClosed IS NOT NULL
          AND (fc.AssignmentTypeID IS NULL OR fc.AssignmentTypeClosingID IS NULL)

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END 
    
    
    -- Search for any vehicles where the completed assignment type service channel and billing service channel do not match. 
    
    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Fee billed does not match assignment type.',
              'Verify all billed fees match the type of assignment'              
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_assignment_type dat ON (fc.AssignmentTypeClosingID = dat.AssignmentTypeID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc_at ON (dat.ServiceChannelID = dsc_at.ServiceChannelID)
        LEFT JOIN dbo.utb_dtwh_dim_service_channel dsc_bill ON (fc.ServiceChannelID = dsc_bill.ServiceChannelID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE fc.TimeIDClosed IS NOT NULL
          AND dsc_at.ServiceChannelCD <> dsc_bill.ServiceChannelCD

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END


    -- Search for negative supplement amounts below 85% of the original or audited estimate
    
    INSERT INTO @tmpException
      SELECT  fc.LynxID,
              ca.ClaimAspectNumber,
              CASE
                WHEN dtrc.DateValue IS NOT NULL THEN dtrc.DateValue
                ELSE dtc.DateValue
              END,
              'Supplement amount is less than 85% of original or audited estimate amount.',
              'Verify Estimate data is not erroneous.'              
        FROM  @tmpFact tmp
        LEFT JOIN dbo.utb_dtwh_fact_claim fc ON (tmp.FactID = fc.FactID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtc ON (fc.TimeIDClosed = dtc.TimeID)
        LEFT JOIN dbo.utb_dtwh_dim_time dtrc ON (fc.TimeIDReclosed = dtrc.TimeID)
        LEFT JOIN dbo.utb_claim_aspect ca ON (fc.ClaimAspectID = ca.ClaimAspectID)
        WHERE ((fc.AuditedEstimateGrossAmt IS NOT NULL) AND (fc.FinalSupplementGrossAmt / fc.AuditedEstimateGrossAmt < .85))
          OR  ((fc.AuditedEstimateGrossAmt IS NULL) AND (fc.FinalSupplementGrossAmt / fc.OriginalEstimateGrossAmt < .85))        

    IF @@ERROR <> 0
    BEGIN
        RAISERROR('%s: SQL Server Error inserting into @tmpException', 16, 1, @ProcName)
        RETURN
    END


    -- Do the final select
    
    SELECT LastCloseDate AS LastClosed, Convert(varchar(10), LynxID) + '-' +  Convert(varchar(2), VehicleNumber) AS Vehicle, ExceptionString, SuggestedAction
      FROM @tmpException 
      ORDER BY LastCloseDate DESC, LynxID, VehicleNumber

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
    END                            
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspDtwhGetExceptions' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspDtwhGetExceptions TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/