-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateModelFormSupplement' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspCreateModelFormSupplement 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspCreateModelFormSupplement
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* FUNCTION:     Creates a new client form supplement by inserting a new record into the 
*				utb_form_supplement table modeled after @iModelInsuranceCompanyID
*
* PARAMETERS:  
*
*   @iNewInsuranceCompanyID
*   @iModelInsuranceCompanyID
*	@iCreatedByUserID
*
* RESULT SET:
*			Identity of new inserted row
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspCreateModelFormSupplement
	@iNewInsuranceCompanyID INT
	, @iModelInsuranceCompanyID INT
	, @iCreatedByUserID INT
AS
BEGIN
    -- Declare internal variables
    DECLARE @error AS INT
    DECLARE @rowcount AS INT
    DECLARE @dtNow AS DATETIME 
    DECLARE @ProcName AS VARCHAR(50)
	DECLARE @iNewFormID INT

    SET @ProcName = 'uspCreateModelFormSupplement'

    -- Set Database options
    SET NOCOUNT ON
	SET @dtNow = CURRENT_TIMESTAMP
	SET @iNewFormID = 0 

	-- Check to make sure the new insurance company id exists
    IF  (@iNewInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iNewInsuranceCompanyID))
    BEGIN
        -- Invalid Insurance Company ID
        RAISERROR('101|%s|@iNewInsuranceCompanyID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure the model insurance company id exists
    IF  (@iModelInsuranceCompanyID IS NULL) 
        OR (NOT EXISTS(SELECT InsuranceCompanyID FROM utb_insurance WHERE InsuranceCompanyID = @iModelInsuranceCompanyID))
    BEGIN
        -- Invalid Model Insurance Company ID
        RAISERROR('101|%s|@iModelInsuranceCompanyID|%u', 16, 1, @ProcName, @iModelInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure the new FormID exists
    IF (NOT EXISTS(
		SELECT 
			FormID 
		FROM 
			dbo.utb_form
		WHERE 
			FormID IN 
			(
				SELECT 
					FormID 
				FROM 
					utb_form
				WHERE 
					InsuranceCompanyID = @iNewInsuranceCompanyID
			)
		)
    )
    BEGIN
        -- Invalid New FormID
        RAISERROR('101|%s|@iNewFormID|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Check to make sure the form supplement doesn't already exist
    IF  (EXISTS(
		SELECT
			FormID
		FROM  
			dbo.utb_form_supplement
		WHERE 
			FormID IN 
			(
				SELECT 
					FormID 
				FROM 
					utb_form
				WHERE 
					InsuranceCompanyID = @iNewInsuranceCompanyID
			)
		)
	)
    BEGIN
        -- Form Supplement already exists
        RAISERROR('101|%s|@iFormSupplement|%u', 16, 1, @ProcName, @iNewInsuranceCompanyID)
        RETURN
    END

	-- Get new FormID
	SELECT 
		@iNewFormID = FormID 
	FROM 
		dbo.utb_form
	WHERE 
		FormID IN 
		(
			SELECT 
				FormID 
			FROM 
				utb_form
			WHERE 
				InsuranceCompanyID = @iNewInsuranceCompanyID
		)

	-- Add the new 
	INSERT INTO dbo.utb_form_supplement
	SELECT
		@iNewFormID
		, LossStateCode
		, ShopStateCode
		, EnabledFlag
		, HTMLPath
		, [Name]
		, PDFPath
		, ServiceChannelCD
		, SQLProcedure
		, @iCreatedByUserID
		, @dtNow
	FROM  
		dbo.utb_form_supplement
	WHERE 
		FormID IN 
		(
			SELECT 
				FormID 
			FROM 
				utb_form
			WHERE 
				InsuranceCompanyID = @iModelInsuranceCompanyID
		) 

	IF EXISTS(
		SELECT
			FormID
		FROM  
			dbo.utb_form_supplement
		WHERE 
			FormID IN 
			(
				SELECT 
					FormID 
				FROM 
					utb_form
				WHERE 
					InsuranceCompanyID = @iNewInsuranceCompanyID
			)
		)
	BEGIN
		SELECT 1 RetCode
	END
	ELSE
	BEGIN
		SELECT 0 RetCode
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspCreateModelFormSupplement' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspCreateModelFormSupplement TO 
        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspCreateModelFormSupplement TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/