BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspOutboundInvoiceInsUpd' AND type = 'sP')
BEGIN
    DROP PROCEDURE dbo.uspOutboundInvoiceInsUpd 
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************      
*      
* PROCEDURE:    uspOutboundInvoiceInsUpd      
* SYSTEM:       Lynx Services APD      
* AUTHOR:       Mahes Kumar      
* FUNCTION:     Insert and Update a indicator      
*      
* PARAMETERS:        
* (I) @LynxID                       OutBoundInvoice LynxID      
* (I) @ClaimAspectID                OutBoundInvoice ClaimAspectID      
* (I) @@SentFlag					OutBoundInvoice SentFlag
* (I) @SysLastUserID                OutBoundInvoice SysLastUserID      
*      
* RESULT SET:      
* PotentialTotalLossIndicator     The newly created PotentialTotalLossIndicator flag      
*      
*      
* VSS      
* $Workfile: $      
* $Archive:  $      
* $Revision: $      
* $Author:   $      
* $Date:     $      
*      
************************************************************************************************************************/      
      
-- Create the stored procedure      
      
      
CREATE PROCEDURE [dbo].[uspOutboundInvoiceInsUpd]       
    @LynxID                       udt_std_int_big,      
    @VehicleNumber                udt_std_int_tiny,  
    @SentFlag					  udt_std_flag,
    @SysLastUserID                udt_std_id       
AS      
BEGIN      
    -- SET NOCOUNT to ON and no longer display the count message      
          
    SET NOCOUNT ON      
          
          
    -- Declare internal variables      
      
    DECLARE @error AS INT      
    DECLARE @rowcount AS INT     
    
    DECLARE @ClaimAspectID As udt_std_int_big
          
  
      
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts       
      
    SET @ProcName = 'uspOutboundInvoiceInsUpd'         
       
      
        
    /*Validate LynxID*/    
    IF ((@LynxID IS NULL) OR (NOT EXISTS (SELECT LynxID FROM dbo.utb_claim WHERE LynxID = @LynxID)))    
         BEGIN      
           -- Invalid LynxID      
              
            RAISERROR  ('1|Invalid LynxID', 16, 1, @ProcName)      
            RETURN      
        END   
        
  SELECT @ClaimAspectID = ClaimAspectID FROM utb_claim_aspect WHERE LynxID = @LynxID AND ClaimAspectNumber = @VehicleNumber    
            
 
   IF @@ERROR <> 0      
    BEGIN      
       -- SQL Server Error      
          
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)   
        RETURN      
    END      
      
    BEGIN TRANSACTION uspTotalLossIndicatorInsUpd      
      
    IF @@ERROR <> 0      
    BEGIN      
       -- SQL Server Error      
          
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)      
        RETURN      
    END      
      
  IF((NOT EXISTS (SELECT LynxID FROM dbo.utb_outbound_invoice WHERE LynxID = @LynxID))    
     AND (NOT EXISTS (SELECT ClaimAspectID FROM dbo.utb_outbound_invoice WHERE ClaimAspectID = @ClaimAspectID)))    
 BEGIN    
 PRINT 'ENTER'
  INSERT INTO dbo.utb_elec_outbound_invoice       
   SELECT 
		c.LynxID
		,ca.ClaimAspectID
		,casc.ClaimAspectServiceChannelID
		,i.InvoiceID
		,i.DispatchNumber
		,i.ItemTypeCD
		,i.Amount
		,i.DeductibleAmt
		,@SentFlag
		,'Sent status Updated on ' + CONVERT(VARCHAR, CURRENT_TIMESTAMP, 120)
		,@SysLastUserID
		,CURRENT_TIMESTAMP
		 FROM utb_claim c WITH(NOLOCK)
			INNER JOIN utb_claim_aspect ca WITH(NOLOCK)
				ON ca.LynxID = c.LynxID
			INNER JOIN utb_claim_aspect_service_channel casc WITH(NOLOCK)
				ON casc.ClaimAspectID = ca.ClaimAspectID 
			INNER JOIN utb_invoice i WITH(NOLOCK)
				ON i.ClaimAspectID = ca.ClaimAspectID 
		WHERE 
				ca.LynxID = @LynxID
				AND ca.ClaimAspectNumber =  @VehicleNumber  
				AND i.EnabledFlag = 1         
 END    
  ELSE    
   BEGIN    
   UPDATE dbo.utb_elec_outbound_invoice      
   SET SentFlag = @SentFlag
       ,Description = 'Status Updated on ' + CURRENT_TIMESTAMP
       ,SysLastUserID               = @SysLastUserID    
       ,SysLastUpdatedDate          = CURRENT_TIMESTAMP    
   WHERE ClaimAspectID  = @ClaimAspectID    
   END    
          
    SELECT @error = @@ERROR, @rowcount = @@ROWCOUNT      
          
          
    -- Check error value      
          
    IF @error <> 0      
    BEGIN      
       -- SQL Server Error      
          
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)      
        ROLLBACK TRANSACTION      
        RETURN      
    END      
      
    COMMIT TRANSACTION uspTotalLossIndicatorInsUpd          
      
    IF @@ERROR <> 0      
    BEGIN      
       -- SQL Server Error      
          
        RAISERROR  ('%s: SQL Server Error', 16, 1, @ProcName)      
        RETURN      
    END      
           
END

GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspOutboundInvoiceInsUpd' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspOutboundInvoiceInsUpd TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO