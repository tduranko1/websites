-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLInsClaim' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspFNOLInsClaim 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspFNOLInsClaim
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jeffery A. Lund/Jonathan Perrigo
* FUNCTION:     Inserts a claim from FNOL Load tables into the APD system
*
* PARAMETERS:  
* (I) @LynxID               LynxID to transfer to APD
* (I) @NewClaimFlag         1 - Indicates this is a new claim, 0 - Adding Vehicles or properties only
*
* RESULT SET:
* None
*
* Updates:
*		- 01Oct2012 - TVD - Added EXEC user wsAPDUser - so that the Web Service and execute this SP
*		- 22Dec2015 - TVD - Added Choice process
*		- 30Sep2016 - TVD - Correcting DTWH issue where Choice and DRP claims in the same LynxID
*		- 01Mar2017 - TVD - Added NO(LOCK) to all the validation steps prior to the Begin Trans
*						  - Adding better debugging and logging
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspFNOLInsClaim
    @LynxID         udt_std_id_big,
    @NewClaimFlag   udt_std_flag
AS
BEGIN
    -- Set internal options
    SET NOCOUNT ON

	--============================--
	-- BEGIN Processing
	--============================--
    --Declare common internal variables
    DECLARE @tmpAddedClaimAspects  TABLE 
    (   
        ClaimAspectID       bigint  NOT NULL,
        ClaimAspectTypeID   int     NOT NULL
    )
    
    DECLARE @tmpActivatedServiceChannels TABLE
    (
        ClaimAspectServiceChannelID   bigint      NOT NULL,
        ClaimAspectID                 bigint      NOT NULL,
        ServiceChannelCD              varchar(4)  NOT NULL,        
        PrimaryFlag                   bit         NOT NULL,
        ReactivatedFlag               bit         NOT NULL
    )
     
    DECLARE @AirBagDriverFrontID            AS udt_std_id
    DECLARE @AirBagDriverSideID             AS udt_std_id
    DECLARE @AirBagHeadlinerID              AS udt_std_id
    DECLARE @AirBagPassengerFrontID         AS udt_std_id
    DECLARE @AirBagPassengerSideID          AS udt_std_id
    DECLARE @ApplicationID                  AS udt_std_id
    DECLARE @ClaimAspectIDClaim             AS udt_std_id_big
    DECLARE @ClaimAspectTypeIDClaim         AS udt_std_id
    DECLARE @ClaimAspectTypeIDProperty      AS udt_std_id
    DECLARE @ClaimAspectTypeIDVehicle       AS udt_std_id
    DECLARE @DeskAuditAppraiserType         AS udt_std_cd
    DECLARE @DeskAuditID                    AS udt_std_id_big
    DECLARE @EventIDClaimCreated            AS udt_std_id
    DECLARE @EventIDClaimOwnershipTransfer  AS udt_std_id
    DECLARE @EventIDClaimReopened           AS udt_std_id
    DECLARE @EventIDIASelected              AS udt_std_id
    DECLARE @EventIDAppraiserAssigned       AS udt_std_id
    DECLARE @EventIDLynxDAUSelected         AS udt_std_id
    DECLARE @EventIDPropertyAssigned        AS udt_std_id
    DECLARE @EventIDPropertyCreated         AS udt_std_id
    DECLARE @EventIDServiceChannelActivated AS udt_std_id
    DECLARE @EventIDShopSelected            AS udt_std_id
    --DECLARE @EventIDVehicleAssigned         AS udt_std_id
	--Project:210474 APD Added the following three variables when we did the code merge M.A.20061208
    DECLARE @EventIDVehicleOwnerAssigned    AS udt_std_id
    DECLARE @EventIDVehicleAnalystAssigned  AS udt_std_id
    DECLARE @EventIDVehicleSupportAssigned  AS udt_std_id
	--Project:210474 APD Added three variables above,  when we did the code merge M.A.20061208
    DECLARE @EventIDVehicleCreated          AS udt_std_id
    DECLARE @EventIDTLLienHolderCreated     AS udt_std_id
    DECLARE @EventIDTLLienHolderMissingBilling AS udt_std_id
    DECLARE @EventIDTLVehicleOwnerCreated   AS udt_std_id
    DECLARE @FNOLUserID                     AS udt_std_id_big
    DECLARE @Gender                         AS udt_per_gender_cd
    DECLARE @GlassAGCAppraiserID            as udt_std_id
    DECLARE @InsuranceCompanyID             AS udt_std_id
    DECLARE @InvolvedRoleTypeIDClaimant     AS udt_std_id
    DECLARE @InvolvedRoleTypeIDDriver       AS udt_std_id
    DECLARE @InvolvedRoleTypeIDInsured      AS udt_std_id
    DECLARE @InvolvedRoleTypeIDOccupant     AS udt_std_id
    DECLARE @InvolvedRoleTypeIDOwner        AS udt_std_id
    DECLARE @InvolvedRoleTypeIDPassenger    AS udt_std_id
    DECLARE @InvolvedRoleTypeIDWitness      AS udt_std_id
    DECLARE @MobileElectronicsAppraiserType AS udt_std_cd
    DECLARE @MobileElectronicsID            AS udt_std_id_big
    DECLARE @ModifiedDateTime               AS udt_sys_last_updated_date
    DECLARE @RelationIDAttorney             AS udt_std_id
    DECLARE @RelationIDDoctor               AS udt_std_id
    DECLARE @RelationIDEmployer             AS udt_std_id
    DECLARE @SourceApplicationCD            AS udt_std_cd
    DECLARE @SourceApplicationPassThruData  AS udt_std_desc_huge 	--Project:210474 APD changed the data type when we did the code merge M.A.20061208
    DECLARE @StatusIDClaimClosed            AS udt_std_id
    DECLARE @StatusIDClaimOpen              AS udt_std_id
    DECLARE @StatusIDVehicleOpen            AS udt_std_id
    DECLARE @ServiceChannelName             AS udt_std_desc_short
    DECLARE @UserID                         AS udt_std_id_big
    DECLARE @Comment                        varchar(500)
    Declare @ApplicationName                varchar(50)
    DECLARE @UnmatchedShopLocationID        AS udt_std_id 
    DECLARE @v_InspectionDate                 AS datetime
    DECLARE @delimiter                      As varchar(1)
    DECLARE @ProcName                       AS varchar(20)

    DECLARE @SecondaryLDAUShopLocationID    AS udt_std_id_big  
    DECLARE @SecondaryLDAUAppraiserID       AS udt_std_id_big
    DECLARE @RRP_LDAU_AssignmentID          AS udt_std_id_big
    
    -- TVD ----------------------------------------------------
    DECLARE @ProcessingType					AS VARCHAR(50)
    DECLARE @ProcGUID						AS VARCHAR(50) = NULL
    DECLARE @DebugMsg						AS VARCHAR(5000) = NULL
    DECLARE @DebugCmd						AS VARCHAR(5000) = NULL
    DECLARE @Debug                          AS INT
	DECLARE @EventSeq						AS INT = 0
    
	-- Check the utb_app_variable table to 
	-- see if we need to turn debugging on
	--DECLARE @vDebug VARCHAR(10) = 'FALSE'
    SET @Debug = 0
	SELECT @Debug = CONVERT(INT, Value) FROM utb_app_variable WITH(NOLOCK) WHERE UPPER(Name) = 'FNOLLOADDEBUGGING'

    -- Initialize variables
    SET @ProcName = 'uspFNOLInsClaim'
    SET @ModifiedDateTime = CURRENT_TIMESTAMP

	------------------
	-- Debugging 
	------------------
	IF @Debug = 1
	BEGIN
		PRINT '  Debug mode = PRINT'
        PRINT '    @LynxID = ' + Convert(varchar(10), @LynxID)
        PRINT '    @NewClaimFlag = ' + Convert(varchar(1), @NewClaimFlag)
	END
	ELSE IF @Debug = 2
	BEGIN
			-- Debug mode = SQL
			SET @ProcGUID = NEWID()
			SET @EventSeq += 1

			SET @DebugMsg = '@vProcessingServer = ''UNKNOWN'''
			SET @DebugMsg += ', @vEventTransactionID = ''' + @ProcGUID + ''''
			SET @DebugMsg += ', @vEventType = ''FNOLInscClaim(PROCESS)'''
			SET @DebugMsg += ', @iEventSeq = ' + CONVERT(VARCHAR(5), @EventSeq)
			SET @DebugMsg += ', @vEventStatus = ''PROCESSING'''
			SET @DebugMsg += ', @vEventDescription = ''====>>>>>> Processing started: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
			SET @DebugMsg += ', @vEventDetailedDescription = ''Processing FNOLInscClaim.  Param data in EventXML'''
			SET @DebugMsg += ', @vEventXML = ''@LynxID=' + CONVERT(VARCHAR(15), @LynxID) + ', @NewClaimFlag=' +  CONVERT(VARCHAR(5), @NewClaimFlag) +''''
			SET @DebugMsg += ', @iRecID = 0'

			--SELECT ('uspFNOLEventInsLogEntry ' + @DebugMsg)
			EXEC ('uspFNOLEventInsLogEntry ' + @DebugMsg)
	END

    --First validate LynxID
    
    IF NOT EXISTS (SELECT LynxID FROM dbo.utb_fnol_claim_load WITH(NOLOCK) WHERE LynxID = @LynxID)
    BEGIN
        RAISERROR('%s: Transfer Failed (New Claim). @LynxID not found in Claim Load Table.', 16, 1, @ProcName)
        RETURN
    END

    IF @NewClaimFlag = 0
    BEGIN
        -- We are adding something to this claim, it should already exist in APD
        
        IF NOT EXISTS (SELECT LynxID FROM dbo.utb_claim WITH(NOLOCK) WHERE LynxID = @LynxID)
        BEGIN
            RAISERROR('%s: Transfer Failed (Add Vehicle). @LynxID not found in Claim Table.', 16, 1, @ProcName)
            RETURN
        END 
    END
    
    
    -- Validate that we have at least one claim vehicle record to process.
    
    IF (SELECT Count(*) FROM dbo.utb_fnol_claim_vehicle_load WITH(NOLOCK) WHERE LynxID = @LynxID) = 0
    BEGIN
        RAISERROR('%s: Transfer Failed.  No claim vehicle record found in Claim Vehicle Load Table for this @LynxID.', 16, 1, @ProcName)
        RETURN
    END

    -- Validate Source Application CD, Insurance Company ID, and FNOL User
    
    SELECT  @SourceApplicationCD = SourceApplicationCD,
            @SourceApplicationPassThruData = SourceApplicationPassThruData,
            @InsuranceCompanyID = InsuranceCompanyID,
            @FNOLUserID = FNOLUserID
      FROM  utb_fnol_claim_load WITH(NOLOCK) 
      WHERE LynxID = @LynxID            
            
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    SELECT  @ApplicationID = ApplicationID,
            @ApplicationName = Name
      FROM  dbo.utb_application WITH(NOLOCK)
      WHERE Code = @SourceApplicationCD

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
     
    
    IF @ApplicationID IS NULL
    BEGIN
        RAISERROR('%s: Transfer Failed.  Unknown Source Application CD passed.', 16, 1, @ProcName)
        RETURN
    END
   
   
    -- Validate Insurance Company

    IF NOT EXISTS(SELECT InsuranceCompanyID FROM dbo.utb_insurance WITH(NOLOCK) WHERE InsuranceCompanyID = @InsuranceCompanyID)
    BEGIN
        RAISERROR('%s: Transfer Failed.  The FNOL Insurance Company ID is not defined in APD.', 16, 1, @ProcName)
        RETURN
    END


    -- Check to make sure the FNOL User is valid (prevents invalid data entry/access)
    
    SELECT  @UserID = UserID
      FROM  dbo.utb_user WITH(NOLOCK)
      WHERE UserID = @FNOLUserID
        AND dbo.ufnUtilityIsUserActive(@FNOLUserID, NULL, @ApplicationID) = 1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
    IF @UserID IS NULL
    BEGIN
        RAISERROR('%s: Transfer Failed.  FNOL User ID %u not found or inactive for application "%s".', 16, 1, @ProcName, @FNOLUserID, @SourceApplicationCD)
        RETURN
    END


    -- Validate that any selected shops are valid, first check to see if CEI shops are valid
    SELECT @UnmatchedShopLocationID = Value
    FROM utb_app_variable WITH(NOLOCK)
    WHERE Name = 'Unmatched_Shop_Location_ID'
    
    IF ((@UnmatchedShopLocationID IS NULL) OR 
       (EXISTS(SELECT ShopLocationID FROM dbo.utb_fnol_claim_vehicle_load WITH(NOLOCK) WHERE LynxID = @LynxID AND ShopLocationID <> @UnmatchedShopLocationID AND ShopLocationID IS NOT NULL)))
    BEGIN        
      
        IF EXISTS(SELECT * FROM dbo.utb_client_contract_state WITH(NOLOCK) WHERE InsuranceCompanyID = @InsuranceCompanyID AND UseCEIShopsFlag = 1)
        BEGIN
            --- CEI Shops valid.  Make sure they are taken into account when checking the shop
          
            IF EXISTS(SELECT  fnol.ShopLocationID,
                              tmp.ShopLocationID
                        FROM  dbo.utb_fnol_claim_vehicle_load fnol WITH(NOLOCK)
                        LEFT JOIN dbo.utb_assignment_type at on (fnol.AssignmentTypeID = at.AssignmentTypeID)
                        LEFT JOIN (SELECT ShopLocationID 
                                     FROM dbo.utb_shop_location WITH(NOLOCK)
                                     WHERE /*AvailableForSelectionFlag = 1 -- this attribute only applies for LYNX shops
                                     AND*/ EnabledFlag = 1
                                     AND (ProgramFlag = 1 OR CEIProgramFlag = 1)) tmp ON (fnol.ShopLocationID = tmp.ShopLocationID)
                        WHERE fnol.LynxID = @LynxID
                          AND fnol.ShopLocationID IS NOT NULL
                          AND at.ServiceChannelDefaultCD = 'PS'
                          AND tmp.ShopLocationID IS NULL)
            BEGIN
                RAISERROR('%s: Transfer Failed.  One or more Shop Location IDs are not available for selection.', 16, 1, @ProcName)
                RETURN
            END            
        END
        ELSE
        BEGIN
            -- Just check against Lynx Select shops
            IF NOT EXISTS(SELECT  fnol.ShopLocationID,
                                  tmp.ShopLocationID
                                  FROM  dbo.utb_fnol_claim_vehicle_load fnol WITH(NOLOCK)
                                  LEFT JOIN dbo.utb_assignment_type at on (fnol.AssignmentTypeID = at.AssignmentTypeID)
                                  LEFT JOIN (SELECT ShopLocationID, ProgramFlag, ReferralFlag 
                                               FROM dbo.utb_shop_location WITH(NOLOCK)
                                               WHERE AvailableForSelectionFlag = 1
                                                 AND EnabledFlag = 1
                                                 AND (ProgramFlag = 1 OR ReferralFlag = 1)) tmp ON (fnol.ShopLocationID = tmp.ShopLocationID)
                                  WHERE fnol.LynxID = @LynxID
                                    AND fnol.ShopLocationID IS NOT NULL
                                    AND ((at.ServiceChannelDefaultCD = 'PS' AND tmp.ProgramFlag = 1) OR (at.ServiceChannelDefaultCD = 'RRP' AND tmp.ReferralFlag = 1)) )
            BEGIN
				-- 11Nov2015 - TVD _ Removed during Choice shop assignment.  Basically accept all shops
                --RAISERROR('%s: Transfer Failed.  One or more Shop Location IDs are not available for selection.', 16, 1, @ProcName)
                --RETURN
                SET @ProcessingType = 'Choice'
            END   
            ELSE
            BEGIN
                SET @ProcessingType = 'LYNX Select'
            END         
        END
    END
    
    -- Get data we'll need during the inserts later
    
    
    
    -- Airbag IDs

    SELECT  @AirBagDriverFrontID = SafetyDeviceID
    FROM    dbo.utb_safety_device WITH(NOLOCK)
    WHERE   Name = 'Airbag - Driver Front'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @AirBagDriverFrontID IS NULL
    BEGIN
        RAISERROR('%s: Invalid APD Data State.  SafetyDeviceID Not found for "Airbag - Driver Front"', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @AirBagDriverSideID = SafetyDeviceID
    FROM    dbo.utb_safety_device WITH(NOLOCK)
    WHERE   Name = 'Airbag - Driver Side'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @AirBagDriverSideID IS NULL
    BEGIN
        RAISERROR('%s: Invalid APD Data State.  SafetyDeviceID Not found for "Airbag - Driver Side"', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @AirBagPassengerFrontID = SafetyDeviceID
    FROM    dbo.utb_safety_device WITH(NOLOCK)
    WHERE   Name = 'Airbag - Passenger Front'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @AirBagPassengerFrontID IS NULL
    BEGIN
        RAISERROR('%s: Invalid APD Data State.  SafetyDeviceID Not found for "Airbag - Passenger Front"', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @AirBagPassengerSideID = SafetyDeviceID
    FROM    dbo.utb_safety_device WITH(NOLOCK)
    WHERE   Name = 'Airbag - Passenger Side'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @AirBagPassengerSideID IS NULL
    BEGIN
        RAISERROR('%s: Invalid APD Data State.  SafetyDeviceID Not found for "Airbag - Passenger Side"', 16, 1, @ProcName)
        RETURN
    END
        

    SELECT  @AirBagHeadlinerID = SafetyDeviceID
    FROM    dbo.utb_safety_device WITH(NOLOCK)
    WHERE   Name = 'Airbag - Headliner'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF  @AirBagHeadlinerID IS NULL
    BEGIN
        RAISERROR('%s: Invalid APD Data State.  SafetyDeviceID Not found for "Airbag - Headliner"', 16, 1, @ProcName)
        RETURN
    END


    -- Claim Aspect Type IDs
    
    SELECT  @ClaimAspectTypeIDClaim = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type WITH(NOLOCK) 
      WHERE Name = 'Claim'        

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
    IF @ClaimAspectTypeIDClaim IS NULL
    BEGIN
        RAISERROR('%s: Invalid APD Data State.  ClaimAspectTypeID for "Claim" not found.', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @ClaimAspectTypeIDProperty = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type WITH(NOLOCK) 
      WHERE Name = 'Property'        

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
    IF @ClaimAspectTypeIDProperty IS NULL
    BEGIN
        RAISERROR('%s: Invalid APD Data State.  ClaimAspectTypeID for "Property" not found.', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @ClaimAspectTypeIDVehicle = ClaimAspectTypeID
      FROM  dbo.utb_claim_aspect_type WITH(NOLOCK) 
      WHERE Name = 'Vehicle'        

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
    IF @ClaimAspectTypeIDVehicle IS NULL
    BEGIN
        RAISERROR('%s: Invalid APD Data State.  ClaimAspectTypeID for "Vehicle" not found.', 16, 1, @ProcName)
        RETURN
    END


    -- Event IDs
    
    SELECT  @EventIDClaimCreated = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Claim Created'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDClaimCreated IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Claim Created" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EventIDClaimOwnershipTransfer = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Claim Ownership Transfer'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDClaimOwnershipTransfer IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Claim Ownership Transfer" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EventIDClaimReopened = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Claim Reopened'

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDClaimReopened IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Claim Reopened" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EventIDPropertyAssigned = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Property Assigned'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDPropertyAssigned IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Property Assigned" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EventIDPropertyCreated = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Property Created'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDPropertyCreated IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Property Created" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EventIDIASelected = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'IA Selected for Vehicle'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDIASelected IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "IA Selected for Vehicle" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EventIDLynxDAUSelected = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'LYNX Desk Audit Unit Selected for Vehicle'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDLynxDAUSelected IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "LYNX Desk Audit Unit Selected for Vehicle" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EventIDShopSelected = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Shop Selected for Vehicle'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDShopSelected IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Shop Selected for Vehicle" Not Found', 16, 1, @ProcName)
        RETURN
    END
    
    SELECT @EventIDAppraiserAssigned = EventID
    FROM dbo.utb_event WITH(NOLOCK)
    WHERE Name='Appraiser Assigned'
    
    IF @EventIDAppraiserAssigned IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Appraiser Assigned" Not Found', 16, 1, @ProcName)
        RETURN
    END
    


--Project:210474 APD Commented-out the following when we did the code merge M.A.20061208
    /*
	SELECT  @EventIDVehicleAssigned = EventID
      FROM  dbo.utb_event
      WHERE Name = 'Vehicle Assigned'
	*/
--Project:210474 APD Added the following SELECT stmnt when we did the code merge M.A.20061208
	SELECT  @EventIDVehicleOwnerAssigned = EventID
	  FROM  dbo.utb_event WITH(NOLOCK)
	  WHERE Name = 'Vehicle Owner Assigned'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDVehicleOwnerAssigned IS NULL --Project:210474 APD Modified the variable name when we did the code merge M.A.20061114
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Vehicle Owner Assigned" Not Found', 16, 1, @ProcName)
        RETURN
    END

    SELECT  @EventIDVehicleAnalystAssigned = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Vehicle Analyst Assigned'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDVehicleAnalystAssigned IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Vehicle Analyst Assigned" Not Found', 16, 1, @ProcName)
        RETURN
    END

    SELECT  @EventIDVehicleSupportAssigned = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Vehicle Administrator Assigned'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDVehicleSupportAssigned IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Vehicle Administrator Assigned" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @EventIDVehicleCreated = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Vehicle Created'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @EventIDVehicleCreated IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  Event ID for "Vehicle Created" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT @EventIDServiceChannelActivated = EventID
    FROM utb_event WITH(NOLOCK)
    WHERE Name = 'Service Channel Activated'
    
    IF @EventIDServiceChannelActivated IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Service Channel Activated Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END    

    SELECT  @EventIDTLLienHolderCreated = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Total Loss Lien Holder Created'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
    IF @EventIDTLLienHolderCreated IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Total Loss Lien Holder Created Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END    

    SELECT  @EventIDTLLienHolderMissingBilling = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Total Loss Lien Holder Missing Billing'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
    IF @EventIDTLLienHolderMissingBilling IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Total Loss Lien Holder Missing Billing Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END    

    SELECT  @EventIDTLVehicleOwnerCreated = EventID
      FROM  dbo.utb_event WITH(NOLOCK)
      WHERE Name = 'Total Loss Vehicle Owner Created'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END
    
    IF @EventIDTLVehicleOwnerCreated IS NULL
    BEGIN
        -- No Event Defined
        RAISERROR('101|%s|Service Channel Activated Event Not Defined.', 16, 1, @ProcName)
        RETURN        
    END    

    -- Involved Role Type IDs
    
    SELECT  @InvolvedRoleTypeIDClaimant = InvolvedRoleTypeID 
      FROM  dbo.utb_involved_role_type WITH(NOLOCK) 
      WHERE Name = 'Claimant'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @InvolvedRoleTypeIDClaimant IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  InvolvedRoleTypeID for "Claimant" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @InvolvedRoleTypeIDDriver = InvolvedRoleTypeID 
      FROM  dbo.utb_involved_role_type WITH(NOLOCK) 
      WHERE Name = 'Driver'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @InvolvedRoleTypeIDDriver IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  InvolvedRoleTypeID for "Driver" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @InvolvedRoleTypeIDInsured = InvolvedRoleTypeID 
      FROM  dbo.utb_involved_role_type WITH(NOLOCK) 
      WHERE Name = 'Insured'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @InvolvedRoleTypeIDInsured IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  InvolvedRoleTypeID for "Insured" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @InvolvedRoleTypeIDOccupant = InvolvedRoleTypeID 
      FROM  dbo.utb_involved_role_type WITH(NOLOCK) 
      WHERE Name = 'Occupant'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @InvolvedRoleTypeIDOccupant IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  InvolvedRoleTypeID for "Occupant" Not Found', 16, 1, @ProcName)
        RETURN
    END
    
    
    SELECT  @InvolvedRoleTypeIDOwner = InvolvedRoleTypeID 
      FROM  dbo.utb_involved_role_type WITH(NOLOCK) 
      WHERE Name = 'Owner'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @InvolvedRoleTypeIDOwner IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  InvolvedRoleTypeID for "Owner" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @InvolvedRoleTypeIDPassenger = InvolvedRoleTypeID 
      FROM  dbo.utb_involved_role_type WITH(NOLOCK) 
      WHERE Name = 'Passenger'
      
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @InvolvedRoleTypeIDPassenger IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  InvolvedRoleTypeID for "Passenger" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @InvolvedRoleTypeIDWitness = InvolvedRoleTypeID 
      FROM  dbo.utb_involved_role_type WITH(NOLOCK) 
      WHERE Name = 'Witness'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @InvolvedRoleTypeIDWitness IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  InvolvedRoleTypeID for "Witness" Not Found', 16, 1, @ProcName)
        RETURN
    END


    -- Relation IDs
    
    SELECT  @RelationIDAttorney = RelationID 
      FROM  dbo.utb_relation WITH(NOLOCK)
      WHERE Name = 'Attorney'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @RelationIDAttorney IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  RelationID for "Attorney" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @RelationIDDoctor = RelationID 
      FROM  dbo.utb_relation WITH(NOLOCK)
      WHERE Name = 'Doctor'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @RelationIDDoctor IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  RelationID for "Doctor" Not Found', 16, 1, @ProcName)
        RETURN
    END


    SELECT  @RelationIDEmployer = RelationID 
      FROM  dbo.utb_relation WITH(NOLOCK)
      WHERE Name = 'Employer'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @RelationIDEmployer IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  RelationID for "Employer" Not Found', 16, 1, @ProcName)
        RETURN
    END


    -- Statuses
    
    SELECT  @StatusIDClaimClosed = StatusID 
      FROM  dbo.utb_status WITH(NOLOCK)
      WHERE Name = 'Claim Closed'
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @StatusIDClaimClosed IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  StatusID for "Claim Closed" Not Found', 16, 1, @ProcName)
        RETURN
    END
   
    SELECT  @StatusIDClaimOpen = StatusID 
      FROM  dbo.utb_status WITH(NOLOCK)
      WHERE Name = 'Open' 
        AND ClaimAspectTypeID = @ClaimAspectTypeIDClaim
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @StatusIDClaimOpen IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  StatusID for "Open" for Claim Not Found', 16, 1, @ProcName)
        RETURN
    END

    SELECT  @StatusIDVehicleOpen = StatusID 
      FROM  dbo.utb_status WITH(NOLOCK)
      WHERE Name = 'Open' 
        AND ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END

    IF @StatusIDVehicleOpen IS NULL
    BEGIN
        -- Invalid APD Data

        RAISERROR('%s: Invalid APD Data State.  StatusID for "Open" for Vehicle Not Found', 16, 1, @ProcName)
        RETURN
    END
   
      
    -- Look up the setting for LYNX desk audit unit assignments
    
    SELECT  @DeskAuditAppraiserType = Left(LTrim(RTrim(value)), CharIndex(',', LTrim(RTrim(value))) - 1),
            @DeskAuditID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable WITH(NOLOCK)
      WHERE Name = 'DESK_AUDIT_AUTOID'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    
    -- Look up the setting for Mobile electronics assignments
    
    SELECT  @MobileElectronicsAppraiserType = Left(LTrim(RTrim(value)), CharIndex(',', LTrim(RTrim(value))) - 1),
            @MobileElectronicsID = Right(LTrim(RTrim(value)), Len(LTrim(RTrim(value))) - CharIndex(',', LTrim(RTrim(value))))
      FROM  dbo.utb_app_variable WITH(NOLOCK)
      WHERE Name = 'MOBILE_ELECTRONICS_AUTOID'

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    -- Look up setting for AGC Appraiser ID
    
    SELECT @GlassAGCAppraiserID = value
    FROM   dbo.utb_app_variable WITH(NOLOCK)
    WHERE  Name = 'AGC_APPRAISER_ID'
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN       
    END

	------------------
	-- Debugging 
	------------------
	IF @Debug = 1
	BEGIN
        PRINT ''
		PRINT 'Derived Common Variables:'
        PRINT '    @ApplicationID = ' + Convert(varchar(10), @ApplicationID)
        PRINT '    @AirBagDriverFrontID = ' + Convert(char(1), @AirBagDriverFrontID)
        PRINT '    @AirBagDriverSideID = ' + Convert(char(1), @AirBagDriverSideID)
        PRINT '    @AirBagPassengerFrontID = ' + Convert(char(1), @AirBagPassengerFrontID)
        PRINT '    @AirBagPassengerSideID = ' + Convert(char(1), @AirBagPassengerSideID)
        PRINT '    @AirBagHeadlinerID = ' + Convert(char(1), @AirBagHeadlinerID)
        PRINT '    @ClaimAspectTypeIDClaim = ' + Convert(varchar(10), @ClaimAspectTypeIDClaim)
        PRINT '    @ClaimAspectTypeIDProperty = ' + Convert(varchar(10), @ClaimAspectTypeIDProperty)
        PRINT '    @ClaimAspectTypeIDVehicle = ' + Convert(varchar(10), @ClaimAspectTypeIDVehicle)
        PRINT '    @EventIDClaimOwnershipTransfer = ' + Convert(varchar(10), @EventIDClaimOwnershipTransfer)
        PRINT '    @EventIDClaimCreated = ' + Convert(varchar(10), @EventIDClaimCreated)
        PRINT '    @EventIDPropertyCreated = ' + Convert(varchar(10), @EventIDPropertyCreated)
        PRINT '    @EventIDShopSelected = ' + Convert(varchar(10), @EventIDShopSelected)
        PRINT '    @EventIDVehicleCreated = ' + Convert(varchar(10), @EventIDVehicleCreated)
        PRINT '    @InvolvedRoleTypeIDClaimant = ' + Convert(varchar(10), @InvolvedRoleTypeIDClaimant)
        PRINT '    @InvolvedRoleTypeIDDriver = ' + Convert(varchar(10), @InvolvedRoleTypeIDDriver)
        PRINT '    @InvolvedRoleTypeIDInsured = ' + Convert(varchar(10), @InvolvedRoleTypeIDInsured)
        PRINT '    @InvolvedRoleTypeIDOccupant = ' + Convert(varchar(10), @InvolvedRoleTypeIDOccupant)
        PRINT '    @InvolvedRoleTypeIDOwner = ' + Convert(varchar(10), @InvolvedRoleTypeIDOwner)
        PRINT '    @InvolvedRoleTypeIDPassenger = ' + Convert(varchar(10), @InvolvedRoleTypeIDPassenger)
        PRINT '    @InvolvedRoleTypeIDWitness = ' + Convert(varchar(10), @InvolvedRoleTypeIDWitness)
        PRINT '    @RelationIDAttorney = ' + Convert(varchar(10), @RelationIDAttorney)
        PRINT '    @RelationIDDoctor = ' + Convert(varchar(10), @RelationIDDoctor)
        PRINT '    @RelationIDEmployer = ' + Convert(varchar(10), @RelationIDEmployer)
        PRINT '    @DeskAuditAppraiserType = ' + @DeskAuditAppraiserType
        PRINT '    @DeskAuditID = ' + Convert(varchar(10), @DeskAuditID)
	END
	ELSE IF @Debug = 2
	BEGIN
		SET @DebugMsg = ''
		SET @DebugMsg = 'Derived Common Variables:'
        SET @DebugMsg += '    @ApplicationID = ' + Convert(varchar(10), ISNULL(@ApplicationID,0))
        SET @DebugMsg += '    @AirBagDriverFrontID = ' + Convert(char(1), ISNULL(@AirBagDriverFrontID,0))
        SET @DebugMsg += '    @AirBagDriverSideID = ' + Convert(char(1), ISNULL(@AirBagDriverSideID,0))
        SET @DebugMsg += '    @AirBagPassengerFrontID = ' + Convert(char(1), ISNULL(@AirBagPassengerFrontID,0))
        SET @DebugMsg += '    @AirBagPassengerSideID = ' + Convert(char(1), ISNULL(@AirBagPassengerSideID,0))
        SET @DebugMsg += '    @AirBagHeadlinerID = ' + Convert(char(1), ISNULL(@AirBagHeadlinerID,0))
        SET @DebugMsg += '    @ClaimAspectTypeIDClaim = ' + Convert(varchar(10), ISNULL(@ClaimAspectTypeIDClaim,0))
        SET @DebugMsg += '    @ClaimAspectTypeIDProperty = ' + Convert(varchar(10), ISNULL(@ClaimAspectTypeIDProperty,0))
        SET @DebugMsg += '    @ClaimAspectTypeIDVehicle = ' + Convert(varchar(10), ISNULL(@ClaimAspectTypeIDVehicle,0))
        SET @DebugMsg += '    @EventIDClaimOwnershipTransfer = ' + Convert(varchar(10), ISNULL(@EventIDClaimOwnershipTransfer,0))
        SET @DebugMsg += '    @EventIDClaimCreated = ' + Convert(varchar(10), ISNULL(@EventIDClaimCreated,0))
        SET @DebugMsg += '    @EventIDPropertyCreated = ' + Convert(varchar(10), ISNULL(@EventIDPropertyCreated,0))
        SET @DebugMsg += '    @EventIDShopSelected = ' + Convert(varchar(10), ISNULL(@EventIDShopSelected,0))
        SET @DebugMsg += '    @EventIDVehicleCreated = ' + Convert(varchar(10), ISNULL(@EventIDVehicleCreated,0))
        SET @DebugMsg += '    @InvolvedRoleTypeIDClaimant = ' + Convert(varchar(10), ISNULL(@InvolvedRoleTypeIDClaimant,0))
        SET @DebugMsg += '    @InvolvedRoleTypeIDDriver = ' + Convert(varchar(10), ISNULL(@InvolvedRoleTypeIDDriver,0))
        SET @DebugMsg += '    @InvolvedRoleTypeIDInsured = ' + Convert(varchar(10), ISNULL(@InvolvedRoleTypeIDInsured,0))
        SET @DebugMsg += '    @InvolvedRoleTypeIDOccupant = ' + Convert(varchar(10), ISNULL(@InvolvedRoleTypeIDOccupant,0))
        SET @DebugMsg += '    @InvolvedRoleTypeIDOwner = ' + Convert(varchar(10), ISNULL(@InvolvedRoleTypeIDOwner,0))
        SET @DebugMsg += '    @InvolvedRoleTypeIDPassenger = ' + Convert(varchar(10), ISNULL(@InvolvedRoleTypeIDPassenger,0))
        SET @DebugMsg += '    @InvolvedRoleTypeIDWitness = ' + Convert(varchar(10), ISNULL(@InvolvedRoleTypeIDWitness,0))
        SET @DebugMsg += '    @RelationIDAttorney = ' + Convert(varchar(10), ISNULL(@RelationIDAttorney,0))
        SET @DebugMsg += '    @RelationIDDoctor = ' + Convert(varchar(10), ISNULL(@RelationIDDoctor,0))
        SET @DebugMsg += '    @RelationIDEmployer = ' + Convert(varchar(10), ISNULL(@RelationIDEmployer,0))
        SET @DebugMsg += '    @DeskAuditAppraiserType = ' + ISNULL(@DeskAuditAppraiserType,'')
        SET @DebugMsg += '    @DeskAuditID = ' + Convert(varchar(10), ISNULL(@DeskAuditID,0))
		
		SET @EventSeq += 1
		--SET @vEventXML = @DebugMsg

		SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
		SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
		SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(COMMOM_VARS)'''
		SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
		SET @DebugCmd += ', @vEventStatus = ''COMMOM_VARS'''
		SET @DebugCmd += ', @vEventDescription = ''====> Processing Derived Common Variables: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
		SET @DebugCmd += ', @vEventDetailedDescription = ''Derived Common Variables.  Param data in EventXML'''
		SET @DebugCmd += ', @vEventXML = ''COMMOM_VARS=' + @DebugMsg + ''''
		SET @DebugCmd += ', @iRecID = 0'

		--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
		EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
	END



    /*********************************************************************************************
    **********************************************************************************************
    *   BEGIN PROCESSING FOR CLAIM DATA  (if new claim)
    **********************************************************************************************
    *********************************************************************************************/

    --DECLARE vars for Claim Table

    DECLARE @c_AgentName                        AS udt_std_name            
    DECLARE @c_AgentPhone                       AS varchar(15)             
    DECLARE @c_AgentExt                         AS udt_ph_extension_number 
    DECLARE @c_CallerAddress1                   AS udt_addr_line_1         
    DECLARE @c_CallerAddress2                   AS udt_addr_line_2         
    DECLARE @c_CallerAddressCity                AS udt_addr_city           
    DECLARE @c_CallerAddressState               AS udt_addr_state          
    DECLARE @c_CallerAddressZip                 AS udt_addr_zip_code       
    DECLARE @c_CallerBestPhoneCode              AS udt_std_cd              
    DECLARE @c_CallerBestTimeToCall             AS udt_std_desc_short      
    DECLARE @c_CallerNameFirst                  AS udt_per_name            
    DECLARE @c_CallerNameLaSt                   AS udt_per_name            
    DECLARE @c_CallerNameTitle                  AS udt_per_title           
    DECLARE @c_CallerPhoneAlternate             AS varchar(15)             
    DECLARE @c_CallerPhoneAlternateExt          AS udt_ph_extension_number 
    DECLARE @c_CallerPhoneDay                   AS varchar(15)             
    DECLARE @c_CallerPhoneDayExt                AS udt_ph_extension_number 
    DECLARE @c_CallerPhoneNight                 AS varchar(15)             
    DECLARE @c_CallerPhoneNightExt              AS udt_ph_extension_number 
    DECLARE @c_CallersRelationToInsuredID       AS udt_std_id              
    DECLARE @c_CarrierRepUserID                 AS udt_std_id
    DECLARE @c_ContactAddress1                  AS udt_addr_line_1         
    DECLARE @c_ContactAddress2                  AS udt_addr_line_2         
    DECLARE @c_ContactAddressCity               AS udt_addr_city           
    DECLARE @c_ContactAddressState              AS udt_addr_state          
    DECLARE @c_ContactAddressZip                AS udt_addr_zip_code       
    DECLARE @c_ContactBestPhoneCode             AS udt_std_cd              
    DECLARE @c_ContactBestTimeToCall            AS udt_std_desc_short      
    DECLARE @c_ContactEmailAddress              AS udt_web_email           
    DECLARE @c_ContactNameFirst                 AS udt_per_name            
    DECLARE @c_ContactNameLast                  AS udt_per_name            
    DECLARE @c_ContactNameTitle                 AS udt_per_title           
    DECLARE @c_ContactPhoneAlternate            AS varchar(15)             
    DECLARE @c_ContactPhoneAlternateExt         AS udt_ph_extension_number 
    DECLARE @c_ContactPhoneDay                  AS varchar(15)             
    DECLARE @c_ContactPhoneDayExt               AS udt_ph_extension_number 
    DECLARE @c_ContactPhoneNight                AS varchar(15)             
    DECLARE @c_ContactPhoneNightExt             AS udt_ph_extension_number 
    DECLARE @c_ContactsRelationToInsuredID      AS udt_std_id              
    DECLARE @c_CoverageClaimNumber              AS udt_cov_claim_number    
    DECLARE @c_DemoFlag                         AS udt_std_flag
    DECLARE @c_IntakeSeconds                    AS udt_std_int
    DECLARE @c_LossDate                         AS varchar(30)             
    DECLARE @c_LossAddressCity                  AS udt_addr_city           
    DECLARE @c_LossAddressCounty                AS udt_addr_county         
    DECLARE @c_LossAddressState                 AS udt_addr_state          
    DECLARE @c_LossAddressStreet                AS udt_addr_line_1         
    DECLARE @c_LossAddressZip                   AS udt_addr_zip_code       
    DECLARE @c_LossDescription                  AS udt_std_desc_xlong       
    DECLARE @c_LossTime                         AS varchar(30)             
    DECLARE @c_LossTypeLevel1ID                 AS udt_std_id              
    DECLARE @c_LossTypeLevel2ID                 AS udt_std_id              
    DECLARE @c_LossTypeLevel3ID                 AS udt_std_id              
    DECLARE @c_NoticeDate                       AS varchar(30)             
    DECLARE @c_NoticeMethodID                   AS udt_std_id              
    DECLARE @c_PoliceDepartmentName             AS udt_std_name            
    DECLARE @c_PolicyNumber                     AS udt_cov_policy_number   
    DECLARE @c_Remarks                          AS udt_std_desc_xlong       
    DECLARE @c_RoadLocationID                   AS udt_std_id              
    DECLARE @c_RoadTypeID                       AS udt_std_id              
    DECLARE @c_TripPurpose                      AS udt_std_cd              
    DECLARE @c_WeatherConditionID               AS udt_std_id          
    DECLARE @c_SettlementDate                   AS udt_std_datetime    
    DECLARE @c_SettlementAmount                 AS udt_std_money       
    DECLARE @c_AdvanceAmount                    AS udt_std_money       
    DECLARE @c_LetterOfGuaranteeAmount          AS udt_std_money
    DECLARE @c_LienHolderID                     AS udt_std_id
    DECLARE @c_LienHolderName                   AS udt_std_name          
    DECLARE @c_LienHolderAddress1               AS udt_addr_line_1
    DECLARE @c_LienHolderAddress2               AS udt_addr_line_2      
    DECLARE @c_LienHolderAddressCity            AS udt_addr_city          
    DECLARE @c_LienHolderAddressState           AS udt_addr_state          
    DECLARE @c_LienHolderAddressZip             AS udt_addr_zip_code       
    DECLARE @c_LienHolderPhoneAreaCode          AS udt_ph_area_code          
    DECLARE @c_LienHolderPhoneExchange          AS udt_ph_exchange_number          
    DECLARE @c_LienHolderPhoneUnitNumber        AS udt_ph_unit_number          
    DECLARE @c_LienHolderPhoneExtension         AS udt_ph_extension_number          
    DECLARE @c_LienHolderFaxAreaCode            AS udt_ph_area_code          
    DECLARE @c_LienHolderFaxExchange            AS udt_ph_exchange_number          
    DECLARE @c_LienHolderFaxUnitNumber          AS udt_ph_unit_number          
    DECLARE @c_LienHolderEmailAddress           AS udt_web_email          
    DECLARE @c_LienHolderContactName            AS udt_std_name          
    DECLARE @c_LienHolderPayoffAmount           AS udt_std_money
    DECLARE @c_LienHolderExpirationDate         AS udt_std_datetime          
    DECLARE @c_LienHolderAccountNumber          AS udt_std_desc_mid          
    DECLARE @c_SalvageVendorID                  As udt_std_id_big
    DECLARE @c_SalvageName                      AS udt_std_name
    DECLARE @c_SalvageAddress1                  AS udt_addr_line_1          
    DECLARE @c_SalvageAddress2                  AS udt_addr_line_2          
    DECLARE @c_SalvageAddressCity               AS udt_addr_city          
    DECLARE @c_SalvageAddressState              AS udt_addr_state          
    DECLARE @c_SalvageAddressZip                AS udt_addr_zip_code          
    DECLARE @c_SalvagePhoneAreaCode             AS udt_ph_area_code         
    DECLARE @c_SalvagePhoneExchange             AS udt_ph_exchange_number          
    DECLARE @c_SalvagePhoneUnitNumber           AS udt_ph_unit_number          
    DECLARE @c_SalvagePhoneExtension            AS udt_ph_extension_number
    DECLARE @c_SalvageFaxAreaCode               AS udt_ph_area_code          
    DECLARE @c_SalvageFaxExchange               AS udt_ph_exchange_number          
    DECLARE @c_SalvageFaxUnitNumber             AS udt_ph_unit_number         
    DECLARE @c_SalvageEmailAddress              AS udt_web_email         
    DECLARE @c_SalvageContactName               AS udt_std_name          
    DECLARE @c_SalvageControlNumber             AS udt_std_desc_mid

    DECLARE @cw_AgentAreaCode                   AS udt_ph_area_code
    DECLARE @cw_AgentExchangeNumber             AS udt_ph_exchange_number
    DECLARE @cw_AgentUnitNumber                 AS udt_ph_unit_number
    DECLARE @cw_CallerDayAreaCode               AS udt_ph_area_code
    DECLARE @cw_CallerDayExchangeNumber         AS udt_ph_exchange_number
    DECLARE @cw_CallerDayUnitNumber             AS udt_ph_unit_number
    DECLARE @cw_CallerNightAreaCode             AS udt_ph_area_code
    DECLARE @cw_CallerNightExchangeNumber       AS udt_ph_exchange_number
    DECLARE @cw_CallerNightUnitNumber           AS udt_ph_unit_number
    DECLARE @cw_CallerAltAreaCode               AS udt_ph_area_code
    DECLARE @cw_CallerAltExchangeNumber         AS udt_ph_exchange_number
    DECLARE @cw_CallerAltUnitNumber             AS udt_ph_unit_number
    DECLARE @cw_ContactDayAreaCode              AS udt_ph_area_code
    DECLARE @cw_ContactDayExchangeNumber        AS udt_ph_exchange_number
    DECLARE @cw_ContactDayUnitNumber            AS udt_ph_unit_number
    DECLARE @cw_ContactNightAreaCode            AS udt_ph_area_code
    DECLARE @cw_ContactNightExchangeNumber      AS udt_ph_exchange_number
    DECLARE @cw_ContactNightUnitNumber          AS udt_ph_unit_number
    DECLARE @cw_ContactAltAreaCode              AS udt_ph_area_code
    DECLARE @cw_ContactAltExchangeNumber        AS udt_ph_exchange_number
    DECLARE @cw_ContactAltUnitNumber            AS udt_ph_unit_number   

    DECLARE @cw_CallerInvolvedID                AS udt_std_id_big
    DECLARE @cw_ContactInvolvedID               AS udt_std_id_big
    DECLARE @cw_LossTypeID                      AS udt_std_id
    DECLARE @cw_tLossDateTime                   AS DATETIME 
    DECLARE @cw_vLossDateTime                   AS VARCHAR(30) 
    DECLARE @cw_tTimeFinished                   AS DATETIME 
    DECLARE @cw_tTimeStarted                    AS DATETIME 


    -- Get record from load table

    SELECT  @c_AgentExt = AgentExt,
            @c_AgentName = AgentName,
            @c_AgentPhone = AgentPhone,
            @c_CallerAddress1 = CallerAddress1,
            @c_CallerAddress2 = CallerAddress2,
            @c_CallerAddressCity = CallerAddressCity,
            @c_CallerAddressState = CallerAddressState,
            @c_CallerAddressZip = CallerAddressZip,
            @c_CallerBestPhoneCode = CallerBestPhoneCode,
            @c_CallerBestTimeToCall = CallerBestTimeToCall,
            @c_CallerNameFirst = CallerNameFirst,
            @c_CallerNameLast = CallerNameLast,
            @c_CallerNameTitle = CallerNameTitle,
            @c_CallerPhoneAlternate = CallerPhoneAlternate,
            @c_CallerPhoneAlternateExt = CallerPhoneAlternateExt,
            @c_CallerPhoneDay = CallerPhoneDay,
            @c_CallerPhoneDayExt = CallerPhoneDayExt,
            @c_CallerPhoneNight = CallerPhoneNight,
            @c_CallerPhoneNightExt = CallerPhoneNightExt,
            @c_CallersRelationToInsuredID = CallersRelationToInsuredID,
            @c_CarrierRepUserID = CarrierRepUserID,
            @c_ContactAddress1 = ContactAddress1,
            @c_ContactAddress2 = ContactAddress2,
            @c_ContactAddressCity = ContactAddressCity,
            @c_ContactAddressState = ContactAddressState,
            @c_ContactAddressZip = ContactAddressZip,
            @c_ContactBestPhoneCode = ContactBestPhoneCode,
            @c_ContactBestTimeToCall = ContactBestTimeToCall,
            @c_ContactEmailAddress = ContactEmailAddress,
            @c_ContactNameFirst = ContactNameFirst,
            @c_ContactNameLASt = ContactNameLASt,
            @c_ContactNameTitle = ContactNameTitle,
            @c_ContactPhoneAlternate = ContactPhoneAlternate,
            @c_ContactPhoneAlternateExt = ContactPhoneAlternateExt,
            @c_ContactPhoneDay = ContactPhoneDay,
            @c_ContactPhoneDayExt = ContactPhoneDayExt,
            @c_ContactPhoneNight = ContactPhoneNight,
            @c_ContactPhoneNightExt = ContactPhoneNightExt,
            @c_ContactsRelationToInsuredID = ContactsRelationToInsuredID,
            @c_CoverageClaimNumber = CoverageClaimNumber,
            @c_DemoFlag = DemoFlag,
            @c_IntakeSeconds = IntakeSeconds,
            @c_LossAddressCity = LossAddressCity,
            @c_LossAddressCounty = LossAddressCounty,
            @c_LossAddressState = LossAddressState,
            @c_LossAddressStreet = LossAddressStreet,
            @c_LossAddressZip = LossAddressZip,
            @c_LossDate = LossDate,
            @c_LossDescription = LossDescription,
            @c_LossTime = LossTime,
            @c_LossTypeLevel1ID = LossTypeLevel1ID,
            @c_LossTypeLevel2ID = LossTypeLevel2ID,
            @c_LossTypeLevel3ID = LossTypeLevel3ID,
            @c_NoticeDate = NoticeDate,
            @c_NoticeMethodID = NoticeMethodID,
            --@c_PayoffAmount = PayoffAmount,
            --@c_PayoffExpirationDate = PayoffExpirationDate,
            @c_PoliceDepartmentName = PoliceDepartmentName,
            @c_PolicyNumber = PolicyNumber,
            @c_Remarks = Remarks,
            @c_RoadLocationID = RoadLocationID,
            @c_RoadTypeID = RoadTypeID,
            @c_TripPurpose = TripPurpose,
            @c_WeatherConditionID = WeatherConditionID,
            @c_SettlementDate = SettlementDate,
            @c_SettlementAmount = SettlementAmount,
            @c_AdvanceAmount = AdvanceAmount,
            @c_LetterOfGuaranteeAmount = LetterOfGuaranteeAmount,
            @c_LienHolderName = LienHolderName,
            @c_LienHolderAddress1 = LienHolderAddress1,
            @c_LienHolderAddress2 = LienHolderAddress2,
            @c_LienHolderAddressCity = LienHolderAddressCity,
            @c_LienHolderAddressState = LienHolderAddressState,
            @c_LienHolderAddressZip = LienHolderAddressZip,
            @c_LienHolderPhoneAreaCode = LienHolderPhoneAreaCode,
            @c_LienHolderPhoneExchange = LienHolderPhoneExchange,
            @c_LienHolderPhoneUnitNumber = LienHolderPhoneUnitNumber,
            @c_LienHolderPhoneExtension = LienHolderPhoneExtension,
            @c_LienHolderFaxAreaCode = LienHolderFaxAreaCode,
            @c_LienHolderFaxExchange = LienHolderFaxExchange,
            @c_LienHolderFaxUnitNumber = LienHolderFaxUnitNumber,
            @c_LienHolderEmailAddress = LienHolderEmailAddress,
            @c_LienHolderContactName = LienHolderContactName,
            @c_LienHolderPayoffAmount = LienHolderPayoffAmount,
            @c_LienHolderExpirationDate = LienHolderExpirationDate,
            @c_LienHolderAccountNumber = LienHolderAccountNumber,
            @c_SalvageName = SalvageName,
            @c_SalvageAddress1 = SalvageAddress1,
            @c_SalvageAddress2 = SalvageAddress2,
            @c_SalvageAddressCity = SalvageAddressCity,
            @c_SalvageAddressState = SalvageAddressState,
            @c_SalvageAddressZip = SalvageAddressZip,
            @c_SalvagePhoneAreaCode = SalvagePhoneAreaCode,
            @c_SalvagePhoneExchange = SalvagePhoneExchange,
            @c_SalvagePhoneUnitNumber = SalvagePhoneUnitNumber,
            @c_SalvagePhoneExtension = SalvagePhoneExtension,
            @c_SalvageFaxAreaCode = SalvageFaxAreaCode,
            @c_SalvageFaxExchange = SalvageFaxExchange,
            @c_SalvageFaxUnitNumber = SalvageFaxUnitNumber,
            @c_SalvageEmailAddress = SalvageEmailAddress,
            @c_SalvageContactName = SalvageContactName,
            @c_SalvageControlNumber = SalvageControlNumber
            
      FROM  dbo.utb_fnol_claim_load WITH(NOLOCK)
      WHERE LynxID = @LynxID

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END


    -- Validate Carrier Representative User ID

    IF NOT EXISTS(SELECT UserID FROM dbo.utb_user WITH(NOLOCK) WHERE UserID = @c_CarrierRepUserID)
    BEGIN
        RAISERROR('%s: (Claim Processing)  Claim Transfer Failed.  The Carrier Representative ID is not defined in APD.', 16, 1, @ProcName)
        RETURN
    END


    -- Default the gender to unknown

    SET @Gender = 'U'


    IF @NewClaimFlag = 1
    BEGIN
        -- FNOL Intake Start and End Date/Time must be computed.  We assume that the current timestamp of the SQL server
        -- is the end time.  We also receive as a parameter the intake seconds.  These seconds are subtracted from the
        -- end date/time to get the start date/time.
    
        SET @cw_tTimeFinished = @ModifiedDateTime   -- (This is CURRENT_TIMESTAMP)
        SET @cw_tTimeStarted = DateAdd(s, -1 * @c_IntakeSeconds, @ModifiedDateTime)
    

        -- Validate the loss date parameter
    
        IF IsNumeric(@c_LossTime) = 1
        BEGIN
            -- Loss time is a numeric value
            
            IF LEN(RTRIM(LTRIM(@c_LossTime))) = 4
            BEGIN
                SET @c_LossTime = LEFT(LTRIM(@c_LossTime), 2) + ':' + RIGHT(RTRIM(@c_LossTime), 2)
            END
    
            SET @cw_vLossDateTime = @c_LossDate + ' ' + @c_LossTime
        END
        ELSE
        BEGIN
            -- Loss time is not numeric/invalid, just go with loss date
            SET @cw_vLossDateTime = @c_LossDate
        END
            
        IF @cw_vLossDateTime IS NULL OR 
           ISDATE(@cw_vLossDateTime) = 0
        BEGIN
            -- Invalid updated date value
    
            SET @cw_tLossDateTime = NULL 
        END
        ELSE    
        BEGIN
            -- Convert the value passed into date format
        
            SELECT @cw_tLossDateTime = CONVERT(DATETIME, @cw_vLossDateTime)
        END
    

        -- Determine the loss type id.  We receive 3 values from FNOL for this, LossTypeLevel1ID, LossTypeLevel2ID,
        -- and LossTypeLevel3ID.  These correspond to each of the 3 cascading drop-down boxes the user selects
        -- loss type with.  The "real" Loss Type value is the first non-zero value as you traverse each of
        -- these levels in descending order (for example, we check LossTypeLevel3ID first, if it is non-zero,
        -- it is the loss type ID.  If it is 0, our search then checks LossTypeLevel2Id for the same.)  This logic
        -- is most directly implemented by using the Search Case statement below, which evaluates each when clause
        -- in order.

        SET @cw_LossTypeID = CASE
                               WHEN @c_LossTypeLevel3ID > 0 THEN @c_LossTypeLevel3ID
                               WHEN @c_LossTypeLevel2ID > 0 THEN @c_LossTypeLevel2ID
                               ELSE @c_LossTypeLevel1ID
                             END


        -- Split Agent Phone into constituent parts

        IF (@c_AgentPhone IS NOT NULL AND Len(@c_AgentPhone) = 10 AND IsNumeric(@c_AgentPhone) = 1)
        BEGIN
            -- Assume valid phone

            SET @cw_AgentAreaCode = LEFT(@c_AgentPhone, 3)
            SET @cw_AgentExchangeNumber = SUBSTRING(@c_AgentPhone, 4, 3)
            SET @cw_AgentUnitNumber = RIGHT(@c_AgentPhone, 4)
        END
        ELSE
        BEGIN
            -- Invalid or no phone

            SET @cw_AgentAreaCode = NULL
            SET @cw_AgentExchangeNumber = NULL
            SET @cw_AgentUnitNumber = NULL
        END

    
        -- Split Caller Day Phone into constituent parts

        IF (@c_CallerPhoneDay IS NOT NULL AND Len(@c_CallerPhoneDay) = 10 AND IsNumeric(@c_CallerPhoneDay) = 1)
        BEGIN
            -- Assume valid phone

            SET @cw_CallerDayAreaCode = LEFT(@c_CallerPhoneDay, 3)
            SET @cw_CallerDayExchangeNumber = SUBSTRING(@c_CallerPhoneDay, 4, 3)
            SET @cw_CallerDayUnitNumber = RIGHT(@c_CallerPhoneDay, 4)
        END
        ELSE
        BEGIN
            -- Invalid or no phone

            SET @cw_CallerDayAreaCode = NULL
            SET @cw_CallerDayExchangeNumber = NULL
            SET @cw_CallerDayUnitNumber = NULL
        END

    
        -- Split Caller Night Phone into constituent parts

        IF (@c_CallerPhoneNight IS NOT NULL AND Len(@c_CallerPhoneNight) = 10 AND IsNumeric(@c_CallerPhoneNight) = 1)
        BEGIN
            -- Assume valid phone

            SET @cw_CallerNightAreaCode = LEFT(@c_CallerPhoneNight, 3)
            SET @cw_CallerNightExchangeNumber = SUBSTRING(@c_CallerPhoneNight, 4, 3)
            SET @cw_CallerNightUnitNumber = RIGHT(@c_CallerPhoneNight, 4)
        END
        ELSE
        BEGIN
            -- Invalid or no phone

            SET @cw_CallerNightAreaCode = NULL
            SET @cw_CallerNightExchangeNumber = NULL
            SET @cw_CallerNightUnitNumber = NULL
        END


        -- Split Caller Alternate Phone into constituent parts

        IF (@c_CallerPhoneAlternate IS NOT NULL AND Len(@c_CallerPhoneAlternate) = 10 AND IsNumeric(@c_CallerPhoneAlternate) = 1)
        BEGIN
            -- Assume valid phone

            SET @cw_CallerAltAreaCode = LEFT(@c_CallerPhoneAlternate, 3)
            SET @cw_CallerAltExchangeNumber = SUBSTRING(@c_CallerPhoneAlternate, 4, 3)
            SET @cw_CallerAltUnitNumber = RIGHT(@c_CallerPhoneAlternate, 4)
        END
        ELSE
        BEGIN
            -- Invalid or no phone

            SET @cw_CallerAltAreaCode = NULL
            SET @cw_CallerAltExchangeNumber = NULL
            SET @cw_CallerAltUnitNumber = NULL
        END


        -- Split Contact Day Phone into constituent parts

        IF (@c_ContactPhoneDay IS NOT NULL AND Len(@c_ContactPhoneDay) = 10 AND IsNumeric(@c_ContactPhoneDay) = 1)
        BEGIN
            -- Assume valid phone

            SET @cw_ContactDayAreaCode = LEFT(@c_ContactPhoneDay, 3)
            SET @cw_ContactDayExchangeNumber = SUBSTRING(@c_ContactPhoneDay, 4, 3)
            SET @cw_ContactDayUnitNumber = RIGHT(@c_ContactPhoneDay, 4)
        END
        ELSE
        BEGIN
            -- Invalid or no phone

            SET @cw_ContactDayAreaCode = NULL
            SET @cw_ContactDayExchangeNumber = NULL
            SET @cw_ContactDayUnitNumber = NULL
        END

    
        -- Split Contact Night Phone into constituent parts

        IF (@c_ContactPhoneNight IS NOT NULL AND Len(@c_ContactPhoneNight) = 10 AND IsNumeric(@c_ContactPhoneNight) = 1)
        BEGIN
            -- Assume valid phone

            SET @cw_ContactNightAreaCode = LEFT(@c_ContactPhoneNight, 3)
            SET @cw_ContactNightExchangeNumber = SUBSTRING(@c_ContactPhoneNight, 4, 3)
            SET @cw_ContactNightUnitNumber = RIGHT(@c_ContactPhoneNight, 4)
        END
        ELSE
        BEGIN
            -- Invalid or no phone

            SET @cw_ContactNightAreaCode = NULL
            SET @cw_ContactNightExchangeNumber = NULL
            SET @cw_ContactNightUnitNumber = NULL
        END


        -- Split Contact Alternate Phone into constituent parts

        IF (@c_ContactPhoneAlternate IS NOT NULL AND Len(@c_ContactPhoneAlternate) = 10 AND IsNumeric(@c_ContactPhoneAlternate) = 1)
        BEGIN
            -- Assume valid phone

            SET @cw_ContactAltAreaCode = LEFT(@c_ContactPhoneAlternate, 3)
            SET @cw_ContactAltExchangeNumber = SUBSTRING(@c_ContactPhoneAlternate, 4, 3)
            SET @cw_ContactAltUnitNumber = RIGHT(@c_ContactPhoneAlternate, 4)
        END
        ELSE
        BEGIN
            -- Invalid or no phone

            SET @cw_ContactAltAreaCode = NULL
            SET @cw_ContactAltExchangeNumber = NULL
            SET @cw_ContactAltUnitNumber = NULL
        END
    END


    --Provide the comments and the variable is passed when calling uspWorkFlowActivateServiceChannel
    Set @Comment = ' party claim added from ' + ltrim(rtrim(@ApplicationName))

          
    BEGIN TRANSACTION utrFNOLLoad        


    IF @NewClaimFlag = 1
    BEGIN        
        -- If any of regular caller fields are populated, Insert the caller as an involved
        -- Otherwise skip and set CallerInvolvedID = Null

        IF (@c_CallersRelationToInsuredID > 0) OR
           (@c_CallerAddress1 IS NOT NULL) OR
           (@c_CallerAddress2 IS NOT NULL) OR
           (@c_CallerAddressCity IS NOT NULL) OR
           (@c_CallerAddressState IS NOT NULL) OR
           (@c_CallerAddressZip IS NOT NULL) OR
           (@c_CallerPhoneAlternate IS NOT NULL) OR
           (@c_CallerPhoneDay IS NOT NULL) OR
           (@c_CallerNameFirst IS NOT NULL) OR
           (@c_CallerNameLast IS NOT NULL) OR
           (@c_CallerPhoneNight IS NOT NULL)
        BEGIN        
            INSERT INTO dbo.utb_involved  
            (
                InsuredRelationID,
                Address1,
                Address2,
                AddressCity,
                AddressState,
                AddressZip,
                AlternateAreaCode,
                AlternateExchangeNumber,
                AlternateExtensionNumber,
                AlternateUnitNumber,
                BestContactPhoneCD,
                BestContactTime,
                DayAreaCode,
                DayExchangeNumber,
                DayExtensionNumber,
                DayUnitNumber,
                GenderCD,
                NameFirst,
                NameLast,
                NameTitle,
                NightAreaCode,
                NightExchangeNumber,
                NightExtensionNumber,
                NightUnitNumber,
                SysLastUserID,
                SysLastUpdatedDate
            )
            VALUES
            (
                @c_CallersRelationToInsuredID,
                LTrim(RTrim(@c_CallerAddress1)),
                LTrim(RTrim(@c_CallerAddress2)),
                LTrim(RTrim(@c_CallerAddressCity)),
                LTrim(RTrim(@c_CallerAddressState)),
                LTrim(RTrim(@c_CallerAddressZip)),
                @cw_CallerAltAreaCode,
                @cw_CallerAltExchangeNumber,
                @c_CallerPhoneAlternateExt,
                @cw_CallerAltUnitNumber,
                LTrim(RTrim(@c_CallerBestPhoneCode)),
                LTrim(RTrim(@c_CallerBestTimeToCall)),
                @cw_CallerDayAreaCode,
                @cw_CallerDayExchangeNumber,
                @c_CallerPhoneDayExt,
                @cw_CallerDayUnitNumber,
                @Gender,
                LTrim(RTrim(@c_CallerNameFirst)),
                LTrim(RTrim(@c_CallerNameLast)),
                LTrim(RTrim(@c_CallerNameTitle)),
                @cw_CallerNightAreaCode,
                @cw_CallerNightExchangeNumber,
                @c_CallerPhoneNightExt,
                @cw_CallerNightUnitNumber,
                @UserId,
                @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
               
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Claim Processing) Error inserting caller into utb_involved.', 16, 1, @ProcName)
                RETURN
            END
           
            -- Get the newly inserted caller's ID 
    
            SELECT @cw_CallerInvolvedID = SCOPE_IDENTITY()
        END
        ELSE
        BEGIN
            -- No caller information inserted

            SET @cw_CallerInvolvedID = NULL
        END


        -- If any of regular contact fields are populated, Insert the contact as an involved
        -- Otherwise skip and set ContactInvolvedID = Null

        IF (@c_ContactsRelationToInsuredID > 0) OR
           (@c_ContactAddress1 IS NOT NULL) OR
           (@c_ContactAddress2 IS NOT NULL) OR
           (@c_ContactAddressCity IS NOT NULL) OR
           (@c_ContactAddressState IS NOT NULL) OR
           (@c_ContactAddressZip IS NOT NULL) OR
           (@c_ContactEmailAddress IS NOT NULL) OR
           (@c_ContactPhoneAlternate IS NOT NULL) OR
           (@c_ContactPhoneDay IS NOT NULL) OR
           (@c_ContactNameFirst IS NOT NULL) OR
           (@c_ContactNameLast IS NOT NULL) OR
           (@c_ContactPhoneNight IS NOT NULL)
        BEGIN
            INSERT INTO dbo.utb_involved  
            (
                InsuredRelationID,
                Address1,
                Address2,
                AddressCity,
                AddressState,
                AddressZip,
                AlternateAreaCode,
                AlternateExchangeNumber,
                AlternateExtensionNumber,
                AlternateUnitNumber,
                BestContactPhoneCD,
                BestContactTime,
                DayAreaCode,
                DayExchangeNumber,
                DayExtensionNumber,
                DayUnitNumber,
                EmailAddress,
                GenderCD,
                NameFirst,
                NameLast,
                NameTitle,
                NightAreaCode,
                NightExchangeNumber,
                NightExtensionNumber,
                NightUnitNumber,
                SysLastUserID,
                SysLastUpdatedDate
            )
            VALUES
            (
                @c_ContactsRelationToInsuredID,
                LTrim(RTrim(@c_ContactAddress1)),
                LTrim(RTrim(@c_ContactAddress2)),
                LTrim(RTrim(@c_ContactAddressCity)),
                LTrim(RTrim(@c_ContactAddressState)),
                LTrim(RTrim(@c_ContactAddressZip)),
                @cw_ContactAltAreaCode,
                @cw_ContactAltExchangeNumber,
                @c_ContactPhoneAlternateExt,
                @cw_ContactAltUnitNumber,
                LTrim(RTrim(@c_ContactBestPhoneCode)),
                LTrim(RTrim(@c_ContactBestTimeToCall)),
                @cw_ContactDayAreaCode,
                @cw_ContactDayExchangeNumber,
                @c_ContactPhoneDayExt,
                @cw_ContactDayUnitNumber,
                LTrim(RTrim(@c_ContactEmailAddress)),
                @Gender,
                LTrim(RTrim(@c_ContactNameFirst)),
                LTrim(RTrim(@c_ContactNameLast)),
                LTrim(RTrim(@c_ContactNameTitle)),
                @cw_ContactNightAreaCode,
                @cw_ContactNightExchangeNumber,
                @c_ContactPhoneNightExt,
                @cw_ContactNightUnitNumber,
                @UserId,
                @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
            
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Claim Processing) Error inserting contact into utb_involved.', 16, 1, @ProcName)
                RETURN
            END
        
            -- Get the newly inserted contact's ID
    
            SELECT @cw_ContactInvolvedID = SCOPE_IDENTITY()
        END
        ELSE
        BEGIN
            -- No contact information inserted

            SET @cw_ContactInvolvedID = NULL
        END


        -- Insert the claim

        INSERT INTO dbo.utb_claim	
        (
            LynxID,
            CallerInvolvedID,
            CarrierRepUserID,
            ClaimantContactMethodId,
            ContactInvolvedID,
            InsuranceCompanyID,
            IntakeUserID,
            LossTypeID,
            RoadLocationID,
            RoadtypeID,
            WeatherConditionID,
            AgentAreaCode,
            AgentExchangeNumber,
            AgentExtensionNumber,
            AgentName,
            AgentUnitNumber,
            ClientClaimNumber,
            ClientClaimNumberSquished,
            DemoFlag,
            IntakeFinishDate,
            IntakeStartDate,
            LossCity,
            LossCounty,
            LossDate,
            LossDescription,
            LossLocation,
            LossState,
            LossZip,
            PoliceDepartmentName,
            PolicyNumber,
            Remarks,
            RestrictedFlag,
            TripPurposeCD,
            SysLastUserId,
            SysLastUpdatedDate
        )
        VALUES	
        (
            @LynxID,
            @cw_CallerInvolvedID,
            @c_CarrierRepUserID,
            @c_NoticeMethodId,
            @cw_ContactInvolvedID,
            @InsuranceCompanyID,
            @UserID,
            @cw_LossTypeID,
            @c_RoadLocationID,
            @c_RoadtypeID,
            @c_WeatherConditionID,
            @cw_AgentAreaCode,
            @cw_AgentExchangeNumber,
            @c_AgentExt,
            LTrim(RTrim(@c_AgentName)),
            @cw_AgentUnitNumber,
            @c_CoverageClaimNumber,
            dbo.ufnUtilitySquishString(LTrim(RTrim(@c_CoverageClaimNumber)), 1, 1, 0, NULL),
            @c_DemoFlag,
            @cw_tTimeFinished,
            @cw_tTimeStarted,
            LTrim(RTrim(@c_LossAddressCity)),
            LTrim(RTrim(@c_LossAddressCounty)),
            @cw_tLossDateTime,
            LTrim(RTrim(@c_LossDescription)),
            LTrim(RTrim(@c_LossAddressStreet)),
            LTrim(RTrim(@c_LossAddressState)),
            LTrim(RTrim(@c_LossAddressZip)),
            LTrim(RTrim(@c_PoliceDepartmentName)),
            LTrim(RTrim(@c_PolicyNumber)),
            LTrim(RTrim(@c_Remarks)),
            0,                      -- Default RestrictedFlag to false
            LTrim(RTrim(@c_TripPurpose)),
            @UserId,
            @ModifiedDateTime
        )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion Failure
        
            ROLLBACK TRANSACTION
            RAISERROR('%s: (Claim Processing) Error inserting into utb_claim.', 16, 1, @ProcName)
            RETURN
        END
    END
    
   
    
    
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Insert Claim Coverages

	*********************************************************************************/
 
    DECLARE @cc_AdditionalCoverageFlag      udt_std_flag
    DECLARE @cc_ClientCode                  udt_std_desc_short
    DECLARE @cc_ClientCoverageTypeID        udt_std_id
    DECLARE @cc_CoverageTypeCD              udt_std_cd
    DECLARE @cc_Description                 udt_std_desc_mid
    DECLARE @cc_DeductibleAmt               udt_std_money
    DECLARE @cc_LimitAmt                    udt_std_money
    DECLARE @cc_LimitDailyAmt               udt_std_money
    DECLARE @cc_MaximumDays                 udt_std_int_small
    DECLARE @cw_CoverageOK                  udt_std_flag
   
    DECLARE covcur CURSOR FOR
      SELECT ClientCode, ClientCoverageTypeID, CoverageTypeCD, DeductibleAmt, LimitAmt, LimitDailyAmt, MaximumDays 
        FROM utb_fnol_claim_coverage_load
        WHERE LynxID = @LynxID
    
    OPEN covcur
    
    FETCH NEXT FROM covcur INTO  @cc_ClientCode, @cc_ClientCoverageTypeID, @cc_CoverageTypeCD, @cc_DeductibleAmt, @cc_LimitAmt, @cc_LimitDailyAmt, @cc_MaximumDays 
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @cw_CoverageOK = 1
        IF (@cc_ClientCoverageTypeID IS NULL) AND (@cc_ClientCode IS NULL)
        BEGIN
            -- Attempt to determine ClientCoverageTypeID from CoverageTypeCD, if passed
            SELECT  TOP 1 @cc_ClientCoverageTypeID = ClientCoverageTypeID 
            FROM utb_client_coverage_type
            WHERE CoverageProfileCD = @cc_CoverageTypeCD
              AND InsuranceCompanyID = @InsuranceCompanyID
              
        END
        IF (@cc_ClientCoverageTypeID IS NULL)
        BEGIN
            -- Attempt to determine ClientCoverageTypeID from ClientCode, if passed
            SELECT TOP 1 @cc_ClientCoverageTypeID = ClientCoverageTypeID
            FROM utb_client_coverage_type
            WHERE ClientCode = @cc_ClientCode
              AND InsuranceCompanyID = @InsuranceCompanyID
        END
        
        IF @cc_ClientCoverageTypeID IS NULL 
        BEGIN
            SET @cw_CoverageOK = 0
        END
        
        IF NOT EXISTS (SELECT * 
                       FROM utb_client_coverage_type 
                       WHERE ClientCoverageTypeID = @cc_ClientCoverageTypeID)
        BEGIN
            -- Try to revese back up other possibilities to see if we can determine an ID
            SET @cc_ClientCoverageTypeID = NULL
            
            IF @cc_ClientCode IS NOT NULL
            BEGIN
                SELECT  TOP 1 @cc_ClientCoverageTypeID = ClientCoverageTypeID FROM utb_client_coverage_type WHERE ClientCode = @cc_ClientCode AND InsuranceCompanyID = @InsuranceCompanyID
            END
            IF @cc_ClientCoverageTypeID IS NULL AND @cc_CoverageTypeCD IS NOT NULL              
            BEGIN
               SELECT TOP 1 @cc_ClientCoverageTypeID = ClientCoverageTypeID FROM utb_client_coverage_type WHERE CoverageProfileCD = @cc_CoverageTypeCD AND InsuranceCompanyID = @InsuranceCompanyID
            END
            
            IF @cc_ClientCoverageTypeID IS NOT NULL
            BEGIN                    
                -- We found a match
                SET @cw_CoverageOK = 1
            END
        END
        
        -- Now Check to see if coverage already exists in utb_claim_coverage for claim
        IF EXISTS (SELECT *
                   FROM dbo.utb_claim_coverage cc
                   WHERE LynxID = @LynxID
                       AND ClientCoverageTypeID = @cc_ClientCoverageTypeID)
        BEGIN
            -- Coverage already exists dont add           
            SET @cw_CoverageOK = 0
        END                           
                       
        
        IF @cw_CoverageOK = 1 
        BEGIN
            
            INSERT INTO utb_claim_coverage
            (
                ClientCoverageTypeID,
                LynxID,
                AddtlCoverageFlag,
                CoverageTypeCD,
                Description,
                DeductibleAmt,
                EnabledFlag,
                LimitAmt,
                LimitDailyAmt,
                MaximumDays,
                SysLastUserID,
                SysLastUpdatedDate
            )
            SELECT cct.ClientCoverageTypeID,
                   @LynxID,
                   cct.AdditionalCoverageFlag,
                   cct.CoverageProfileCD,
                   cct.Name,
                   @cc_DeductibleAmt,
                   cct.EnabledFlag,
                   @cc_LimitAmt,
                   @cc_LimitDailyAmt,
                   @cc_MaximumDays,
                   @UserID,
                   @ModifiedDateTime
              FROM utb_client_coverage_type cct
              WHERE cct.ClientCoverageTypeID = @cc_ClientCoverageTypeID
        END
        
        FETCH NEXT FROM covcur INTO @cc_ClientCode, @cc_ClientCoverageTypeID, @cc_CoverageTypeCD, @cc_DeductibleAmt, @cc_LimitAmt, @cc_LimitDailyAmt, @cc_MaximumDays 
    END                                                
                    
  

    IF @NewClaimFlag = 1
    BEGIN        
        -- Insert the claim aspect record
    
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Revisit this with Jorge/Jonathan regarding the inserts and the updates
			M.A. 20061122
	*********************************************************************************/
        INSERT INTO dbo.utb_claim_aspect (
            ClaimAspectTypeID, 
            --CreatedUserID, --Project:210474 APD Remarked-off column when we did the code merge M.A.20061120
            LynxID,
            SourceApplicationID,             
            ClaimAspectNumber,
            CreatedDate,
            SysLastUserID, 
            SysLastUpdatedDate)
          VALUES (@ClaimAspectTypeIDClaim,
                  --@UserID,
                  @LynxID, 
                  @ApplicationID,
                  0,
                  @ModifiedDateTime,
                  @UserID, 
                  @ModifiedDateTime)

        IF @@ERROR <> 0
        BEGIN
            -- Insertion Failure
        
            ROLLBACK TRANSACTION
            RAISERROR('%s: (Claim Processing) Error inserting into utb_claim_aspect.', 16, 1, @ProcName)
            RETURN
        END
        ELSE
        BEGIN
            -- Capture the claim aspect id for use later on
        
            SET @ClaimAspectIDClaim = SCOPE_IDENTITY()
            
            INSERT INTO @tmpAddedClaimAspects 
              VALUES (@ClaimAspectIDClaim, @ClaimAspectTypeIDClaim)
            
            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
        
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Claim Processing) Error inserting into @tmpAddedClaimAspects.', 16, 1, @ProcName)
                RETURN
            END
        END
    END -- New claim flag    
    ELSE
    BEGIN 
        -- If an new claim was not created, we will need some stuff off the existing claim.
      
        SELECT  @ClaimAspectIDClaim = ClaimAspectID
          FROM  dbo.utb_claim_aspect ca 
          WHERE LynxID = @LynxID
            AND ClaimAspectTypeID = @ClaimAspectTypeIDClaim
            
        IF @@ERROR <> 0
        BEGIN
           -- SQL Server Error

            RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
            RETURN
        END            
    END

    
    /*********************************************************************************************
    **********************************************************************************************
    *   BEGIN PROCESSING FOR CLAIM VEHICLE DATA
    **********************************************************************************************
    *********************************************************************************************/
 
     --Variables for Claim Vehicle

    DECLARE @v_VehicleNumber                AS udt_std_int
    DECLARE @v_ClientCoverageTypeID         AS udt_std_int
    DECLARE @v_AirBagDriverFront            AS udt_std_flag
    DECLARE @v_AirBagDriverSide             AS udt_std_flag
    DECLARE @v_AirBagHeadliner              AS udt_std_flag
    DECLARE @v_AirBagPassengerFront         AS udt_std_flag
    DECLARE @v_AirBagPassengerSide          AS udt_std_id
    DECLARE @v_AssignmentTypeID             AS udt_std_id
    DECLARE @v_BodyStyle                    AS udt_auto_body
    DECLARE @v_Color                        AS udt_auto_color
    DECLARE @v_ContactAddress1              AS udt_addr_line_1         
    DECLARE @v_ContactAddress2              AS udt_addr_line_2         
    DECLARE @v_ContactAddressCity           AS udt_addr_city           
    DECLARE @v_ContactAddressState          AS udt_addr_state          
    DECLARE @v_ContactAddressZip            AS udt_addr_zip_code       
    DECLARE @v_ContactBestPhoneCD           AS udt_std_cd
    DECLARE @v_ContactNameFirst             AS udt_per_name            
    DECLARE @v_ContactNameLast              AS udt_per_name            
    DECLARE @v_ContactNameTitle             AS udt_per_title           
    DECLARE @v_ContactPhone                 AS VARCHAR(15)
    DECLARE @v_ContactPhoneExt              AS udt_ph_extension_number
    DECLARE @v_ContactNightPhone            AS VARCHAR(15)
    DECLARE @v_ContactNightPhoneExt         AS udt_ph_extension_number
    DECLARE @v_ContactAltPhone              AS VARCHAR(15)
    DECLARE @v_ContactAltPhoneExt           AS udt_ph_extension_number
    DECLARE @v_CoverageProfileCD            AS udt_std_cd
    DECLARE @v_Drivable                     AS udt_std_Flag
    DECLARE @v_ExposureCD                   AS udt_std_cd
    DECLARE @v_GlassDamageFlag              AS udt_std_flag
    DECLARE @v_ImpactLocations              AS VARCHAR(100)
    DECLARE @v_ImpactSpeed                  AS VARCHAR(10)
    DECLARE @v_LicensePlateNumber           AS udt_auto_plate_number
    DECLARE @v_LicensePlateState            AS udt_addr_state
    DECLARE @v_LocationAddress1             AS udt_addr_line_1
    DECLARE @v_LocationAddress2             AS udt_addr_line_2
    DECLARE @v_LocationCity                 AS udt_addr_city
    DECLARE @v_LocationName                 AS udt_std_name
    DECLARE @v_LocationPhone                AS VARCHAR(15)
    DECLARE @v_LocationState                AS udt_addr_state
    DECLARE @v_LocationZip                  AS udt_addr_zip_code
    DECLARE @v_Make                         AS udt_auto_make
    DECLARE @v_Mileage                      AS udt_auto_speed
    DECLARE @v_Model                        AS udt_auto_model
    DECLARE @v_NADAID                       AS udt_auto_nada_id
    DECLARE @v_PermissionToDrive            AS udt_std_cd
    DECLARE @v_PhysicalDamageFlag           AS udt_std_flag
    DECLARE @v_PostedSpeed                  AS udt_auto_speed
    DECLARE @v_PriorDamage                  AS VARCHAR(100)
    DECLARE @v_PriorityFlag                 AS udt_std_flag
    DECLARE @v_Remarks                      AS udt_std_desc_xlong
    DECLARE @v_RentalDaysAuthorized         AS udt_dt_day
    DECLARE @v_RentalInstructions           AS udt_std_desc_mid
    DECLARE @v_SelectedShopRank             AS udt_std_int_tiny
    DECLARE @v_SelectedShopScore            AS udt_std_int
    DECLARE @v_ShopLocationID               AS udt_std_id_big
    DECLARE @v_ShopRemarks                  AS udt_std_desc_xlong
    DECLARE @v_ShopSearchLogID              AS udt_std_id_big
    DECLARE @v_VehicleYear                  AS udt_dt_year
    DECLARE @v_VIN                          AS udt_auto_vin
    
    DECLARE @v_TitleName                    AS varchar(100)
    DECLARE @v_TitleState                   AS varchar(2)
    DECLARE @v_TitleStatus                  AS varchar(50)
    
    DECLARE @v_RepairLocationCity           AS varchar(50)
    DECLARE @v_RepairLocationCounty         AS varchar(50)
    DECLARE @v_RepairLocationState          AS varchar(50)

    DECLARE @vw_ContactAreaCode             AS udt_ph_area_code
    DECLARE @vw_ContactExchangeNumber       AS udt_ph_exchange_number
    DECLARE @vw_ContactUnitNumber           AS udt_ph_unit_number
    DECLARE @vw_ContactNightAreaCode        AS udt_ph_area_code
    DECLARE @vw_ContactNightExchangeNumber  AS udt_ph_exchange_number
    DECLARE @vw_ContactNightUnitNumber      AS udt_ph_unit_number
    DECLARE @vw_ContactAltAreaCode          AS udt_ph_area_code
    DECLARE @vw_ContactAltExchangeNumber    AS udt_ph_exchange_number
    DECLARE @vw_ContactAltUnitNumber        AS udt_ph_unit_number

    DECLARE @vw_AppraiserID                 AS udt_std_id_big
    DECLARE @vw_ClaimAspectID               AS udt_std_id_big
    DECLARE @vw_ClientCoverageTypeID        AS udt_std_id
    DECLARE @vw_ContactInvolvedID           AS udt_std_id_big
    DECLARE @vw_CoverageProfileCDWork       AS udt_std_cd
    DECLARE @vw_ExposureCDWork              AS udt_std_cd
    DECLARE @vw_LocationAreaCode            AS udt_ph_area_code
    DECLARE @vw_LocationExchangeNumber      AS udt_ph_exchange_number
    DECLARE @vw_LocationUnitNumber          AS udt_ph_unit_number
    DECLARE @vw_ServiceChannelCDWork        AS udt_std_cd
    DECLARE @vw_ShopLocationID              AS udt_std_id_big

	DECLARE @vw_ClaimAspectServiceChannelID     AS udt_std_id_big
    DECLARE @vw_GlassClaimAspectSvcChannelID    AS udt_std_id_big
    DECLARE @vw_PhysDmgClaimAspectSvcChannelID  AS udt_std_id_big
    DECLARE @vw_ReactivatedFlag                 AS udt_std_flag
    --DECLARE @vw_ClaimAspectServiceChannelID     as bigint

	-- 03Feb2012 - TVD - Elephant
    DECLARE @v_SourceApplicationPassthruDataVeh AS VARCHAR(8000)
    
    -- Declare a cursor to walk through the vehicle load records for this claim
    
    DECLARE csrClaimVehicle CURSOR FOR
      SELECT  VehicleNumber, 
              ClientCoverageTypeID,
              AirBagDriverFront, 
              AirBagDriverSide, 
              AirBagHeadliner, 
              AirBagPassengerFront,
              AirBagPassengerSide, 
              AssignmentTypeID,
              BodyStyle, 
              Color, 
              ContactAddress1, 
              ContactAddress2,
              ContactAddressCity, 
              ContactAddressState, 
              ContactAddressZip,
              ContactBestPhoneCD, 
              ContactNameFirst,
              ContactNameLast, 
              ContactNameTitle, 
              ContactPhone, 
              ContactPhoneExt,
              ContactNightPhone, 
              ContactNightPhoneExt,
              ContactAltPhone, 
              ContactAltPhoneExt,
              CoverageProfileCD, 
              Drivable,
              ExposureCD,
              GlassDamageFlag,
--               GlassDispatchedFlag,
--               GlassDispatchNumber,
--               GlassNotDispatchedReason,
--               GlassReferenceNumber,
--               GlassShopAppointmentDate,
--               GlassShopName,
--               GlassShopPhoneNumber,
              ImpactLocations, 
              ImpactSpeed, 
              LicensePlateNumber, 
              LicensePlateState,
              LocationAddress1, 
              LocationAddress2, 
              LocationCity, 
              LocationName,
              LocationPhone, 
              LocationState,
              LocationZip, 
              Make, 
              Mileage, 
              Model, 
              NADAId, 
              PermissionToDrive, 
              PhysicalDamageFlag,
              PostedSpeed,
              PriorDamage,
              PriorityFlag, 
              Remarks,
              RentalDaysAuthorized,
              RentalInstructions,
              RepairLocationCity,
              RepairLocationCounty,
              RepairLocationState,
              SelectedShopRank,
              SelectedShopScore,
              ShopLocationID, 
              ShopRemarks,
              ShopSearchLogID,
              TitleName,
              TitleState,
              TitleStatus,
              VehicleYear, 
              Vin,
              SourceApplicationPassthruDataVeh   -- 31Jan2012 - TVD - Elephant
        FROM  dbo.utb_fnol_claim_vehicle_load
        WHERE LynxID = @LynxID
    
    OPEN csrClaimVehicle
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        ROLLBACK TRANSACTION
        RAISERROR('%s: (Vehicle Processing) SQL Server Error Opening Cursor.', 16, 1, @ProcName)
        RETURN
    END

    
    -- Get the first vehicle
    
    FETCH NEXT 
      FROM csrClaimVehicle 
      INTO @v_VehicleNumber, 
           @v_ClientCoverageTypeID,
           @v_AirBagDriverFront, 
           @v_AirBagDriverSide, 
           @v_AirBagHeadliner, 
           @v_AirBagPassengerFront, 
           @v_AirBagPassengerSide, 
           @v_AssignmentTypeID,
           @v_BodyStyle, 
           @v_Color, 
           @v_ContactAddress1, 
           @v_ContactAddress2, 
           @v_ContactAddressCity,
           @v_ContactAddressState,
           @v_ContactAddressZip, 
           @v_ContactBestPhoneCD, 
           @v_ContactNameFirst, 
           @v_ContactNameLast, 
           @v_ContactNameTitle,
           @v_ContactPhone, 
           @v_ContactPhoneExt,
           @v_ContactNightPhone, 
           @v_ContactNightPhoneExt,
           @v_ContactAltPhone, 
           @v_ContactAltPhoneExt,
           @v_CoverageProfileCD, 
           @v_Drivable, 
           @v_ExposureCD,
           @v_GlassDamageFlag,
--            @v_GlassDispatchNumber,
--            @v_GlassNotDispatchedReason,
--            @v_GlassReferenceNumber,
--            @v_GlassShopAppointmentDate,
--            @v_GlassShopName,
--            @v_GlassShopPhoneNumber,           
           @v_ImpactLocations, 
           @v_ImpactSpeed, 
           @v_LicensePlateNumber, 
           @v_LicensePlateState, 
           @v_LocationAddress1, 
           @v_LocationAddress2, 
           @v_LocationCity, 
           @v_LocationName,
           @v_LocationPhone, 
           @v_LocationState, 
           @v_LocationZip, 
           @v_Make, 
           @v_Mileage, 
           @v_Model, 
           @v_NADAId, 
           @v_PermissionToDrive,
           @v_PhysicalDamageFlag, 
           @v_PostedSpeed, 
           @v_PriorDamage,
           @v_PriorityFlag, 
           @v_Remarks, 
           @v_RentalDaysAuthorized,
           @v_RentalInstructions,
           @v_RepairLocationCity,
           @v_RepairLocationCounty,
           @v_RepairLocationState,
           @v_SelectedShopRank,
           @v_SelectedShopScore,
           @v_ShopLocationID,
           @v_ShopRemarks,
           @v_ShopSearchLogID,
           @v_TitleName,
           @v_TitleState,
           @v_TitleStatus,
           @v_VehicleYear, 
           @v_Vin,
           @v_SourceApplicationPassthruDataVeh    -- 31Jan2012 - TVD - Elephant
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        ROLLBACK TRANSACTION
        RAISERROR('%s: (Vehicle Processing) SQL Server Error fetching first cursor record.', 16, 1, @ProcName)
        RETURN
    END

    
    -- Begin loop
    
    WHILE @@FETCH_STATUS = 0
    BEGIN
        -- We need to first check to see if there is really vehicle data.  Data from FNOL will include a vehicle 1 (insured's vehicle)
        -- regardless of whether it was actually involved in the accident or whether we have information on it.  This happens
        -- because FNOL attaches the insured's information to Vehicle 1.  This is not required for APD (we extract the insured's
        -- information and attach it directly to the claim), so we'll only insert a vehicle if there is real vehicle data included.

        IF (@v_BodyStyle IS NOT NULL) OR
           (@v_Color IS NOT NULL) OR
           (@v_ContactAddress1 IS NOT NULL) OR
           (@v_ContactAddress2 IS NOT NULL) OR
           (@v_ContactAddressCity IS NOT NULL) OR
           (@v_ContactAddressState IS NOT NULL) OR
           (@v_ContactAddressZip IS NOT NULL) OR
           (@v_ContactNameFirst IS NOT NULL) OR
           (@v_ContactNameLast IS NOT NULL) OR
           (@v_ContactNameTitle IS NOT NULL) OR
           (@v_ContactPhone IS NOT NULL) OR
           (@v_ContactPhoneExt IS NOT NULL) OR
           (@v_ContactNightPhone IS NOT NULL) OR
           (@v_ContactNightPhoneExt IS NOT NULL) OR
           (@v_ContactAltPhone IS NOT NULL) OR
           (@v_ContactAltPhoneExt IS NOT NULL) OR
           (@v_ImpactLocations IS NOT NULL) OR
           (@v_ImpactSpeed IS NOT NULL) OR
           (@v_LicensePlateNumber IS NOT NULL) OR
           (@v_LicensePlateState IS NOT NULL) OR
           (@v_LocationAddress1 IS NOT NULL) OR
           (@v_LocationAddress2 IS NOT NULL) OR
           (@v_LocationCity IS NOT NULL) OR
           (@v_LocationName IS NOT NULL) OR
           (@v_LocationPhone IS NOT NULL) OR
           (@v_LocationState IS NOT NULL) OR
           (@v_LocationZip IS NOT NULL) OR
           (@v_Make IS NOT NULL) OR
           (@v_Mileage IS NOT NULL) OR
           (@v_Model IS NOT NULL) OR
           (@v_PostedSpeed IS NOT NULL) OR
           (@v_PriorDamage IS NOT NULL) OR
           (@v_Remarks IS NOT NULL) OR
           (@v_RentalDaysAuthorized IS NOT NULL) OR
           (@v_RentalInstructions IS NOT NULL) OR
           (@v_VehicleYear IS NOT NULL) OR
           (@v_Vin IS NOT NULL)
        BEGIN        
		------------------
		-- Debugging 
		------------------
		IF @Debug = 1
		BEGIN
			PRINT ''
			PRINT 'Aspect Variables from the vehicle load record:'
			PRINT '    @v_VehicleNumber = ' + Convert(varchar(10), @v_VehicleNumber)
			PRINT '    @v_ClientCoverageTypeID = ' + Convert(varchar(10), @v_ClientCoverageTypeID)
			PRINT '    @v_CoverageProfileCD = ' + @v_CoverageProfileCD
			PRINT '    @v_ExposureCD = ' + @v_ExposureCD
			PRINT '    @v_AssignmentTypeID = ' + Convert(varchar(10), @v_AssignmentTypeID)
			PRINT '    @v_GlassDamage = ' + Convert(varchar(10),@v_GlassDamageFlag)
			PRINT '    @v_PhysicalDamage = ' + Convert(varchar(10),@v_PhysicalDamageFlag)
		END
		ELSE IF @Debug = 2
		BEGIN
			SET @DebugMsg = ''
			SET @DebugMsg += 'Aspect Variables from the vehicle load record:'
			SET @DebugMsg += '    @v_VehicleNumber = ' + Convert(varchar(10), ISNULL(@v_VehicleNumber,0))
			SET @DebugMsg += '    @v_ClientCoverageTypeID = ' + Convert(varchar(10), ISNULL(@v_ClientCoverageTypeID,0))
			SET @DebugMsg += '    @v_CoverageProfileCD = ' + ISNULL(@v_CoverageProfileCD,'')
			SET @DebugMsg += '    @v_ExposureCD = ' + ISNULL(@v_ExposureCD,'')
			SET @DebugMsg += '    @v_AssignmentTypeID = ' + Convert(varchar(10), ISNULL(@v_AssignmentTypeID,0))
			SET @DebugMsg += '    @v_GlassDamage = ' + Convert(varchar(10), ISNULL(@v_GlassDamageFlag,0))
			SET @DebugMsg += '    @v_PhysicalDamage = ' + Convert(varchar(10),ISNULL(@v_PhysicalDamageFlag,0))

			SET @EventSeq += 1

			SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
			SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
			SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(ASPECT_VARS)'''
			SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
			SET @DebugCmd += ', @vEventStatus = ''ASPECT_VARS'''
			SET @DebugCmd += ', @vEventDescription = ''====> Processing Aspect Variables: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
			SET @DebugCmd += ', @vEventDetailedDescription = ''Aspect Variables from the vehicle load record.  Param data in EventXML'''
			SET @DebugCmd += ', @vEventXML = ''ASPECT_VARS=' + @DebugMsg + ''''

			SET @DebugCmd += ', @iRecID = 0'

			--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
			EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
		END


            -- Create an Aspect record for this vehicle
            
            IF @v_ExposureCD IS NULL
                BEGIN
                    IF @v_VehicleNumber = 1 
                        BEGIN
                            SET @vw_ExposureCDWork = '1'
                            SET @Comment = '1st ' + ltrim(rtrim(@Comment))
                        END
                    ELSE
                        BEGIN
                            SET @vw_ExposureCDWork = '3'
                            SET @Comment = '3rd ' +  ltrim(rtrim(@Comment))
                        END
                END
            ELSE
            BEGIN
                SET @vw_ExposureCDWork = @v_ExposureCD
                /********************************************/
                /*  the following code added for ClaimPoint */
                /********************************************/
                select @Comment = 
                            case
                                when @v_ExposureCD = '1' then '1st ' + ltrim(rtrim(@Comment))
                                else '3rd ' + ltrim(rtrim(@Comment))
                            end 
                /********************************************/
                /*  the code above added for ClaimPoint     */
                /********************************************/

            END
                

            SET @v_Drivable = IsNull(@v_Drivable, 0)        
            SET @v_PriorityFlag = IsNull(@v_PriorityFlag, 0)
            
            
            -- Reconcile Client Coverage Type and Coverage Profile CD
            
            SET @vw_CoverageProfileCDWork = @v_CoverageProfileCD
            
            -- First, if both Client Coverage Type ID and CoverageProfileCD were passed in, make sure they reconcile
            
            IF @vw_CoverageProfileCDWork IS NOT NULL AND
               @v_ClientCoverageTypeID IS NOT NULL
            BEGIN
                IF NOT EXISTS(SELECT  ClientCoverageTypeID 
                                FROM  dbo.utb_client_coverage_type 
                                WHERE ClientCoverageTypeID = @v_ClientCoverageTypeID
                                  AND CoverageProfileCD = @vw_CoverageProfileCDWork 
                                  AND InsuranceCompanyID = @InsuranceCompanyID)
                BEGIN
                    -- They don't match, throw back an error
                    
                    RAISERROR('%s: (Vehicle Processing) Client Coverage Type ID %u does not map to Coverage ProfileCD "%s" (Insurance Company %u)', 16, 1, @ProcName, @v_ClientCoverageTypeID, @vw_CoverageProfileCDWork, @InsuranceCompanyID)
                    RETURN
                END                    
            END  

            -- Now reconcile if one or the other is missing
            
            IF @vw_CoverageProfileCDWork IS NULL AND
               @v_ClientCoverageTypeID IS NULL
            BEGIN
                IF @v_VehicleNumber = 1 
                BEGIN
                    SET @vw_CoverageProfileCDWork = 'COLL'
                END
                ELSE
                BEGIN
                    SET @vw_CoverageProfileCDWork = 'LIAB'
                END
            END
            
            IF @v_ClientCoverageTypeID IS NULL
            BEGIN
                -- We need to set it based on the Coverage Profile CD 
                
                SELECT TOP 1 @vw_ClientCoverageTypeID = ClientCoverageTypeID
                  FROM  dbo.utb_client_coverage_type
                  WHERE InsuranceCompanyID = @InsuranceCompanyID
                    AND CoverageProfileCD = @vw_CoverageProfileCDWork
                            
                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error

                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle Processing) SQL Server Error getting Client Coverage Type ID.', 16, 1, @ProcName)
                    RETURN
                END
                
                IF @vw_ClientCoverageTypeID IS NULL
                BEGIN
                    -- Client Coverage Type not found, throw back an error
                    
                    RAISERROR('%s: (Vehicle Processing) Client Coverage Type ID not found for Code "%s" (Insurance Company %u)', 16, 1, @ProcName, @vw_CoverageProfileCDWork, @InsuranceCompanyID)
                    RETURN
                END                    
            END
            ELSE
            BEGIN
                IF EXISTS(SELECT  ClientCoverageTypeID 
                            FROM  dbo.utb_client_coverage_type 
                            WHERE ClientCoverageTypeID = @v_ClientCoverageTypeID 
                              AND InsuranceCompanyID = @InsuranceCompanyID)
                BEGIN
                    SET  @vw_ClientCoverageTypeID = @v_ClientCoverageTypeID
                END
                ELSE
                BEGIN
                    -- The Client Coverage Type ID passed in is not defined as belonging to the Insurance Company
                    
                    RAISERROR('%s: (Vehicle Processing) Client Coverage Type ID %u not defined (Insurance Company %u)', 16, 1, @ProcName, @v_ClientCoverageTypeID, @InsuranceCompanyID)
                    RETURN
                END                    
                    
            END
            
            IF @vw_CoverageProfileCDWork IS NULL
            BEGIN
                -- We must have gotten a Client Coverage Type ID but no CoverageProfileCD.  Look it up.
                
                SELECT  @vw_CoverageProfileCDWork = CoverageProfileCD
                  FROM  dbo.utb_client_coverage_type 
                  WHERE ClientCoverageTypeID = @vw_ClientCoverageTypeID
            
                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error

                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle Processing) SQL Server Error getting Coverage Profile CD.', 16, 1, @ProcName)
                    RETURN
                END

                IF @vw_CoverageProfileCDWork IS NULL
                BEGIN
                    -- Unable to determine the CoverageProfileCD, raise error
                    
                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle Processing) Unable to determine CoverageProfileCD for Client Coverage Type ID %u (Insurance Company %u)', 16, 1, @ProcName, @vw_ClientCoverageTypeID, @InsuranceCompanyID)
                    RETURN
                END                                        
            END                  
                              

            SELECT  @vw_ServiceChannelCDWork = ServiceChannelDefaultCD 
              FROM  dbo.utb_assignment_type 
              WHERE AssignmentTypeID = @v_AssignmentTypeID
            
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Vehicle Processing) SQL Server Error getting Default Service Channel CD.', 16, 1, @ProcName)
                RETURN
            END

          
            -- If this is a desk audit or Mobile Electronics vehicle 
            -- look up the shop/appraiser the assignment should be sent to.
            
            IF (@vw_ServiceChannelCDWork IN ('DA', 'DR'))
            BEGIN
                -- Desk Audit
                
                IF @DeskAuditAppraiserType = 'S'
                BEGIN
                    SET @vw_ShopLocationID = @DeskAuditID
                END
                ELSE
                BEGIN
                    SET @vw_AppraiserID = @DeskAuditID
                END
            END
            ELSE
            BEGIN
                IF (@vw_ServiceChannelCDWork = 'ME')
                BEGIN
                    -- Mobile Electronics
                    
                    IF @MobileElectronicsAppraiserType = 'S'
                    BEGIN
                        SET @vw_ShopLocationID = @MobileElectronicsID
                    END
                    ELSE
                    BEGIN
                        SET @vw_AppraiserID = @MobileElectronicsID
                    END
                END
                ELSE
                BEGIN
                    -- Regular shop or IA assignment
                
                    SET @vw_ShopLocationID = @v_ShopLocationID
                    SET @vw_AppraiserID = NULL      -- When we start collecting appraiser id in FNOL Claim Vehicle Load, change this
                END
            END
            
			------------------
			-- Debugging 
			------------------
			IF @Debug = 1
			BEGIN
                PRINT ''
                PRINT 'Derived aspect variables:'
                PRINT '    @vw_ExposureCDWork = ' + @vw_ExposureCDWork
                PRINT '    @vw_ClientCoverageTypeID = ' + Convert(varchar(5), @vw_ClientCoverageTypeID)
                PRINT '    @vw_CoverageProfileCDWork = ' + @vw_CoverageProfileCDWork
                PRINT '    @v_AssignmentTypeID = ' + convert(varchar(5),@v_AssignmentTypeID)
                PRINT '    @vw_ServiceChannelCDWork = ' + @vw_ServiceChannelCDWork
                PRINT '    @vw_AppraiserID = ' + Convert(varchar(10), @vw_AppraiserID)
                PRINT '    @vw_ShopLocationID = ' + Convert(varchar(10), @vw_ShopLocationID)
			END
			ELSE IF @Debug = 2
			BEGIN
				SET @DebugMsg = ''
                SET @DebugMsg += 'Derived aspect variables:'
                SET @DebugMsg += '    @vw_ExposureCDWork = ' + ISNULL(@vw_ExposureCDWork,'')
                SET @DebugMsg += '    @vw_ClientCoverageTypeID = ' + Convert(varchar(5), ISNULL(@vw_ClientCoverageTypeID,0))
                SET @DebugMsg += '    @vw_CoverageProfileCDWork = ' + ISNULL(@vw_CoverageProfileCDWork,'')
                SET @DebugMsg += '    @v_AssignmentTypeID = ' + convert(varchar(5), ISNULL(@v_AssignmentTypeID,0))
                SET @DebugMsg += '    @vw_ServiceChannelCDWork = ' + ISNULL(@vw_ServiceChannelCDWork,'')
                SET @DebugMsg += '    @vw_AppraiserID = ' + Convert(varchar(10), ISNULL(@vw_AppraiserID,0))
                SET @DebugMsg += '    @vw_ShopLocationID = ' + Convert(varchar(10), ISNULL(@vw_ShopLocationID,0))

				SET @EventSeq += 1

				SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
				SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
				SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(DERIVED_ASPECT_VARS)'''
				SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
				SET @DebugCmd += ', @vEventStatus = ''DERIVED_ASPECT_VARS'''
				SET @DebugCmd += ', @vEventDescription = ''====> Processing Derived aspect variables: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
				SET @DebugCmd += ', @vEventDetailedDescription = ''Derived aspect variables.  Param data in EventXML'''
				SET @DebugCmd += ', @vEventXML = ''DERIVED_ASPECT_VARS=' + @DebugMsg + ''''
				SET @DebugCmd += ', @iRecID = 0'

				--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
			END

            
            set @v_InspectionDate = NULL
            
            IF @v_AssignmentTypeID = 15 -- Pursuit Audit
            BEGIN
                SET @SourceApplicationPassThruData = @v_ShopRemarks
                SET @v_ShopRemarks = NULL
                SET @delimiter = char(255)
                SELECT @v_InspectionDate =  case
                                                when isDate(value) = 1 then convert(datetime, value)
                                                else null
                                            end
                FROM dbo.ufnUtilityParseString(@SourceApplicationPassThruData , @delimiter, 1)
                wHERE strIndex = 10
            END

			/**************************************
			  03Feb2012 - TVD - SourceApplicationPassthruDataVehicle
			  data add
			**************************************/
			SET @delimiter = char(124)  -- |

			IF LEN(@v_SourceApplicationPassthruDataVeh) > 0
			BEGIN
				SET @SourceApplicationPassThruData = @v_SourceApplicationPassthruDataVeh
			END

			------------------
			-- Debugging 
			------------------
			IF @Debug = 1
			BEGIN
				PRINT ''
				PRINT 'SourceApplicationPassthruDataVeh Variables from the vehicle load record:'
				PRINT '    @v_SourceApplicationPassthruDataVeh = ' + @v_SourceApplicationPassthruDataVeh
				PRINT '    @SourceApplicationPassThruData = ' + @SourceApplicationPassThruData
			END
			ELSE IF @Debug = 2
			BEGIN
				SET @DebugMsg = ''
				SET @DebugMsg += 'SourceApplicationPassthruDataVeh Variables from the vehicle load record:'
				SET @DebugMsg += '    @v_SourceApplicationPassthruDataVeh = ' + ISNULL(@v_SourceApplicationPassthruDataVeh,'')
				SET @DebugMsg += '    @SourceApplicationPassThruData = ' + ISNULL(@SourceApplicationPassThruData,'')

				SET @EventSeq += 1

				SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
				SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
				SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(PASSTHRU_VARS)'''
				SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
				SET @DebugCmd += ', @vEventStatus = ''PASSTHRU_VARS'''
				SET @DebugCmd += ', @vEventDescription = ''====> SourceApplicationPassthruDataVeh Variables: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
				SET @DebugCmd += ', @vEventDetailedDescription = ''SourceApplicationPassthruDataVeh Variables from the vehicle load record'''
				SET @DebugCmd += ', @vEventXML = ''PASSTHRU_VARS=' + @DebugMsg + ''''
				SET @DebugCmd += ', @iRecID = 0'

				--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
			END

	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Revisit this with Jorge/Jonathan regarding the inserts and the updates
			M.A. 20061122
	*********************************************************************************/
            INSERT INTO dbo.utb_claim_aspect (
                ClaimAspectTypeID, 
                ClientCoverageTypeID,
                --CreatedUserID, --Project:210474 APD Remarked-off the column when we did the code merge M.A.20061120
                --CurrentAssignmentTypeID, 
                InitialAssignmentTypeID,
                LynxID,
                SourceApplicationID,
                ClaimAspectNumber,
                CoverageProfileCD,
                CreatedDate,
                EnabledFlag, 
                ExposureCD,
                PriorityFlag, 
                --ServiceChannelCD, 
                SourceApplicationPassThruData,
                SysLastUserID,
                SysLastUpdatedDate)
              VALUES (@ClaimAspectTypeIDVehicle, 
                      @vw_ClientCoverageTypeID, 
                      --@UserID,
                      --IsNull(@v_AssignmentTypeID, 0),
                      IsNull(@v_AssignmentTypeID, 0),                                    
                      @LynxID, 
                      @ApplicationID,
                      @v_VehicleNumber,
                      @vw_CoverageProfileCDWork, 
                      @ModifiedDateTime,
                      1,        -- Set vehicle as enabled
                      @vw_ExposureCDWork,
                      @v_PriorityFlag,
                      --@vw_ServiceChannelCDWork,
                      @SourceApplicationPassThruData,
                      @UserID, 
                      @ModifiedDateTime)

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
        
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Vehicle %u Processing) Error inserting into utb_claim_aspect.', 16, 1, @ProcName, @v_VehicleNumber)
                RETURN
            END

            SET @vw_ClaimAspectID = SCOPE_IDENTITY()
            
--            EXEC uspWorkflowUpdateStatus @ClaimAspectID = @vw_ClaimAspectID,
--                                         @ClaimAspectTypeID = @ClaimAspectTypeIDVehicle,
--                                         @StatusID = @StatusIDVehicleOpen,
--                                         @UserID = @UserID
            

			/*********************************************************************************
			Project: 210474 APD - Enhancements to support multiple concurrent service channels
			Note:	Create a record in utb_Claim_Aspect_Service_Channel table
					M.A. 20061211
			*********************************************************************************/
             
            -- This proc will create a claim aspect service channel record for claim aspect, the last parameter is
            -- @Notify parameter and is set to false since we'll handle notification later
            EXEC  dbo.uspWorkflowActivateServiceChannel @vw_ClaimAspectServiceChannelID output, 
                                                        @vw_ReactivatedFlag output, 
                                                        @vw_ClaimAspectID, 
                                                        @vw_ServiceChannelCDWork,
                                                        1, 
                                                        @UserID, 
                                                        0,
                                                        @Comment

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
        
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Vehicle %u Processing) Error inserting into utb_claim_aspect_service_Channel.', 16, 1, @ProcName, @v_VehicleNumber)
                RETURN
            END
            
            -- This is for the MET Process (This needs to be changed when the Pursuit Audit service channel is created)
            IF @v_AssignmentTypeID = 15
            BEGIN
                UPDATE dbo.utb_claim_aspect_service_channel
                SET InspectionDate = @v_InspectionDate
                WHERE ClaimAspectServiceChannelID = @vw_ClaimAspectServiceChannelID
            END
            
            --set @vw_ClaimAspectServiceChannelID = @@IDENTITY --Project:210474 This value is needed for the execution of uspWorkflowSelectShop M.A.20061211
            
            -- Save in temporary table so we can properly handle workflow later
            
            INSERT INTO @tmpAddedClaimAspects 
              VALUES (@vw_ClaimAspectID, @ClaimAspectTypeIDVehicle)
            
            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
        
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Vehicle %u Processing) Error inserting into @tmpClaimAspect.', 16, 1, @ProcName, @v_VehicleNumber)
                RETURN
            END
            
            -- Now Add ServiceChannel to temp table for workflow later
            INSERT INTO @tmpActivatedServiceChannels
              VALUES (@vw_ClaimAspectServiceChannelID, @vw_ClaimAspectID, @vw_ServiceChannelCDWork, 1, @vw_ReactivatedFlag)
            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
        
                ROLLBACK TRANSACTION
                RAISERROR('%s: Insert into tmpActivatedServiceChannels failed for ClaimAspectID=%u.', 16, 1, @ProcName, @vw_ClaimAspectID)
                RETURN
            END

			------------------
			-- Debugging 
			------------------
			IF @Debug = 1
			BEGIN
                PRINT '@vw_ServiceChannelCDWork=' + ISNULL(@vw_ServiceChannelCDWork,'NULL')
                PRINT '@v_GlassDamageFlag=' + convert(varchar(5),@v_GlassDamageFlag)
                PRINT '@v_PhysicalDamageFlag=' + convert(varchar(5),@v_PhysicalDamageFlag)
			END
			ELSE IF @Debug = 2
			BEGIN
				SET @DebugMsg = ''
                SET @DebugMsg += '@vw_ServiceChannelCDWork=' + ISNULL(@vw_ServiceChannelCDWork,'')
                SET @DebugMsg += '@v_GlassDamageFlag=' + convert(varchar(5), ISNULL(@v_GlassDamageFlag,0))
                SET @DebugMsg += '@v_PhysicalDamageFlag=' + convert(varchar(5), ISNULL(@v_PhysicalDamageFlag,0))

				SET @EventSeq += 1

				SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
				SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
				SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(ADDITIONAL_VARS)'''
				SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
				SET @DebugCmd += ', @vEventStatus = ''ADDITIONAL_VARS'''
				SET @DebugCmd += ', @vEventDescription = ''====> Additional SourceApplicationPassthruDataVeh Variables: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
				SET @DebugCmd += ', @vEventDetailedDescription = ''Additional SourceApplicationPassthruDataVeh Variables from the vehicle load record'''
				SET @DebugCmd += ', @vEventXML = ''ADDITIONAL_VARS=' + @DebugMsg + ''''
				SET @DebugCmd += ', @iRecID = 0'

				--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
			END

            IF @vw_ServiceChannelCDWork = 'ME'
            BEGIN
				------------------
				-- Debugging 
				------------------
				IF @Debug = 1
				BEGIN
                    PRINT 'Process possible additional service channels for ME Claim'
				END
				ELSE IF @Debug = 2
				BEGIN
					SET @DebugMsg = 'Process possible additional service channels for ME Claim'

					SET @EventSeq += 1

					SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
					SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
					SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(ME_CLAIM)'''
					SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
					SET @DebugCmd += ', @vEventStatus = ''ME_CLAIM'''
					SET @DebugCmd += ', @vEventDescription = ''====> Process possible additional service channels for ME Claim: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
					SET @DebugCmd += ', @vEventDetailedDescription = ''Process possible additional service channels for ME Claim'''
					SET @DebugCmd += ', @vEventXML = ''ME_CLAIM' + @DebugMsg + ''''
					SET @DebugCmd += ', @iRecID = 0'

					--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				END

                -- ME Claims may have additional services channels.  We need to determine if additional service channels
                -- need to be created and create them if they are needed.
                IF @v_GlassDamageFlag = 1
                BEGIN
                    -- Glass Damage indicate.  Activate Glass Service Channel
                    SET @vw_ReactivatedFlag = 0
                    EXEC uspWorkflowActivateServiceChannel @vw_GlassClaimAspectSvcChannelID output,
                                                           @vw_ReactivatedFlag output,
                                                            @vw_ClaimAspectID,
                                                            'GL',
                                                            0,
                                                            @UserID,
                                                            0,
                                                            @Comment
                    
                    IF @@ERROR <> 0 
                    BEGIN
                        ROLLBACK TRANSACTION
                        RAISERROR('%s: Glass service channel failed to activate for ClaimAspectID=%u.', 16, 1, @ProcName, @vw_ClaimAspectID)                        
                        RETURN                    
                    END 
                    

                    -- Add service channel activation to list for workflow notification
                    INSERT INTO @tmpActivatedServiceChannels                                                               
                      VALUES (@vw_GlassClaimAspectSvcChannelID, @vw_ClaimAspectID, 'GL', 0, @vw_ReactivatedFlag)
                                          
                    IF  @@ERROR <> 0 
                    BEGIN
                        ROLLBACK TRANSACTION
                        RAISERROR('%s: Insert into tmpActivatedServiceChannels failed for ClaimAspectID=%u for Glass.', 16, 1, @ProcName, @vw_ClaimAspectID)
                        RETURN                                        
                    END
--                    
--                    -- Create Assignment Record for Glass

					------------------
					-- Debugging 
					------------------
					IF @Debug = 1
					BEGIN
						PRINT 'Creating Assignment Record for Glass'
					END
					ELSE IF @Debug = 2
					BEGIN
						SET @DebugMsg = 'Creating Assignment Record for Glass'

						SET @EventSeq += 1

						SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
						SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
						SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(GLASS_CLAIM)'''
						SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
						SET @DebugCmd += ', @vEventStatus = ''GLASS_CLAIM'''
						SET @DebugCmd += ', @vEventDescription = ''====> Creating Assignment Record for Glass: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
						SET @DebugCmd += ', @vEventDetailedDescription = ''Creating Assignment Record for Glass'''
						SET @DebugCmd += ', @vEventXML = ''GLASS_CLAIM' + @DebugMsg + ''''
						SET @DebugCmd += ', @iRecID = 0'

						--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
						EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					END

                    EXEC uspWorkflowSelectShop @ClaimAspectServiceChannelID = @vw_GlassClaimAspectSvcChannelID,
                                               @SelectOperationCD   = 'S',
                                               @AppraiserID         = @GlassAGCAppraiserID,
                                               @AssignmentRemarks   = @v_ShopRemarks,
                                               @UserID              = @UserID,
                                               @NotifyEvent         = 0     -- Tell the proc not to notify APD workflow.  We will do it manually later.
                    
                    IF  @@ERROR <> 0 
                    BEGIN
                        ROLLBACK TRANSACTION
                        RAISERROR('%s: Unable to create assignment record for glass service channel for ClaimAspectID = %u .', 16, 1, @ProcName, @vw_ClaimAspectID)
                        RETURN                                        
                    END
                END
                
                IF @v_PhysicalDamageFlag = 1
                BEGIN
                    -- Vehicle also had physical damage requiring program shop service channel.
                    SET @vw_ReactivatedFlag = NULL
                    EXEC uspWorkflowActivateServiceChannel @vw_PhysDmgClaimAspectSvcChannelID output,
                                                           @vw_ReactivatedFlag output,
                                                           @vw_ClaimAspectID,
                                                           'PS',
                                                           0,
                                                           @UserID,
                                                           0,
                                                           @Comment
                    
                    IF @@ERROR <> 0 
                    BEGIN
                        ROLLBACK TRANSACTION
                        RAISERROR('%s: Program shop service channel failed to activate for ClaimAspectID=%u.', 16, 1, @ProcName,  @vw_ClaimAspectID)                        
                        RETURN                                        
                    END 
                    
                    -- A Program shop service channel off of a ME claim would not initially have a shop assigned.
                    INSERT INTO @tmpActivatedServiceChannels                                                               
                      VALUES (@vw_PhysDmgClaimAspectSvcChannelID, @vw_ClaimAspectID, 'PS', 0, @vw_ReactivatedFlag)
                    
                END
            END
            
            IF @vw_ServiceChannelCDWork = 'TL'
            BEGIN
                IF @c_LienHolderName IS NOT NULL AND
                   @c_LienHolderAddress1 IS NOT NULL AND
                   @c_LienHolderAddressZip IS NOT NULL
                BEGIN
                    -- insert the Lien Holder
                    EXEC uspLienHolderUpdDetail  @LienHolderID = @c_LienHolderID output,
                                                 @Name = @c_LienHolderName,
                                                 @Address1 = @c_LienHolderAddress1,
                                                 @Address2 = @c_LienHolderAddress2,
                                                 @AddressCity = @c_LienHolderAddressCity,
                                                 @AddressState = @c_LienHolderAddressState,
                                                 @AddressZip = @c_LienHolderAddressZip,
                                                 @ContactName = @c_LienHolderContactName,
                                                 @EmailAddress = @c_LienHolderEmailAddress,
                                                 @FaxAreaCode = @c_LienHolderFaxAreaCode,
                                                 @FaxExchangeNumber = @c_LienHolderFaxExchange,
                                                 @FaxUnitNumber = @c_LienHolderFaxUnitNumber,
                                                 @PhoneAreaCode = @c_LienHolderPhoneAreaCode,
                                                 @PhoneExchangeNumber = @c_LienHolderPhoneExchange,
                                                 @PhoneUnitNumber = @c_LienHolderPhoneUnitNumber,
                                                 @PhoneExtensionNumber = @c_LienHolderPhoneExtension,
                                                 @UpdateIfExists = 0,
                                                 @UserID = 0

                    IF @@ERROR <> 0
                    BEGIN
                        -- Insertion Failure
                
                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (ClaimAspectServiceChannelID %u Processing) Error add/update Lien Holder', 16, 1, @ProcName, @vw_ClaimAspectServiceChannelID)
                        RETURN
                    END
                    
                    IF @c_LienHolderID IS NULL
                    BEGIN
                        -- Error getting Lien Holder ID
                
                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (ClaimAspectServiceChannelID %u Processing) Error obtaining Lien Holder ID.', 16, 1, @ProcName, @vw_ClaimAspectServiceChannelID)
                        RETURN
                    END
                    
                END
                
                IF @c_SalvageName IS NOT NULL /*AND
                   @c_SalvageAddress1 IS NOT NULL AND
                   @c_SalvageAddressZip IS NOT NULL*/
                BEGIN
                    -- insert the Salvage Vendor
                    EXEC uspSalvageVendorUpdDetail   @SalvageVendorID = @c_SalvageVendorID output,
                                                     @Name = @c_SalvageName,
                                                     @Address1 = @c_SalvageAddress1,
                                                     @Address2 = @c_SalvageAddress2,
                                                     @AddressCity = @c_SalvageAddressCity,
                                                     @AddressState = @c_SalvageAddressState,
                                                     @AddressZip = @c_SalvageAddressZip,
                                                     @ContactName = @c_SalvageContactName,
                                                     @EmailAddress = @c_SalvageEmailAddress,
                                                     @FaxAreaCode = @c_SalvageFaxAreaCode,
                                                     @FaxExchangeNumber = @c_SalvageFaxExchange,
                                                     @FaxUnitNumber = @c_SalvageFaxUnitNumber,
                                                     @PhoneAreaCode = @c_SalvagePhoneAreaCode,
                                                     @PhoneExchangeNumber = @c_SalvagePhoneExchange,
                                                     @PhoneUnitNumber = @c_SalvagePhoneUnitNumber,
                                                     @PhoneExtensionNumber = @c_SalvagePhoneExtension,
                                                     @UpdateIfExists = 0,
                                                     @UserID = 0

                    IF @c_SalvageVendorID IS NULL
                    BEGIN
                        -- Error getting Salvage Vendor ID
                
                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (ClaimAspectServiceChannelID %u Processing) Error add/update Salvage Vendor.', 16, 1, @ProcName, @vw_ClaimAspectServiceChannelID)
                        RETURN
                    END
                    
                    IF @c_SalvageVendorID IS NULL
                    BEGIN
                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (ClaimAspectServiceChannelID %u Processing) Error obtaining Lien Holder ID.', 16, 1, @ProcName, @vw_ClaimAspectServiceChannelID)
                        RETURN                                        
                    END
                END
                ELSE
                BEGIN
                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (ClaimAspectServiceChannelID %u Processing) Cannot create Total Loss claim without Salvage Vendor information', 16, 1, @ProcName, @vw_ClaimAspectServiceChannelID)
                    RETURN                                        
                END
                
                -- update the service channel with the Lien Holder and Salvage vendor information
                UPDATE dbo.utb_claim_aspect_service_channel 
                SET AdvanceAmount = @c_AdvanceAmount,
                    LetterOfGuaranteeAmount = @c_LetterOfGuaranteeAmount,
                    LienHolderID = @c_LienHolderID,
                    LeinHolderAccountNumber = @c_LienHolderAccountNumber,
                    PayoffAmount = @c_LienHolderPayoffAmount,
                    PayoffExpirationDate = @c_LienHolderExpirationDate,
                    SalvageControlNumber = @c_SalvageControlNumber,
                    SalvageVendorID = @c_SalvageVendorID,
                    SettlementAmount = @c_SettlementAmount,
                    SettlementDate = @c_SettlementDate
                WHERE ClaimAspectServiceChannelID = @vw_ClaimAspectServiceChannelID
                
                -- for some reason the loss date does not get update from script above. 
                -- had to update the claim table.
                IF ISDATE(@c_LossDate) = 1
                BEGIN
                    UPDATE dbo.utb_claim
                    SET LossDate = @c_LossDate
                    WHERE LynxID = @LynxID
                END
            END

            -- Save the Repair Location information
            IF (@vw_ServiceChannelCDWork IN ('DA', 'DR')) AND
               (@v_RepairLocationCity IS NOT NULL AND
                @v_RepairLocationCounty IS NOT NULL AND
                @v_RepairLocationState IS NOT NULL)
            BEGIN
               
                SELECT @vw_ClaimAspectServiceChannelID = ClaimAspectServiceChannelID
                FROM utb_claim_aspect_service_channel
                WHERE ClaimAspectID = @vw_ClaimAspectID
                  AND ServiceChannelCD = @vw_ServiceChannelCDWork
                 
                UPDATE utb_claim_aspect_service_channel
                SET RepairLocationCity = @v_RepairLocationCity,
                    RepairLocationCounty = @v_RepairLocationCounty,
                    RepairLocationState = @v_RepairLocationState
                WHERE ClaimAspectServiceChannelID = @vw_ClaimAspectServiceChannelID               
            END
                        
            -- Split LocationPhone into constituent parts

            IF (@v_LocationPhone IS NOT NULL AND Len(@v_LocationPhone) = 10 AND IsNumeric(@v_LocationPhone) = 1)
            BEGIN
                SET @vw_LocationAreaCode = LEFT(@v_LocationPhone, 3)
                SET @vw_LocationExchangeNumber = SUBSTRING(@v_LocationPhone, 4, 3)
                SET @vw_LocationUnitNumber = RIGHT(@v_LocationPhone, 4)
            END
            ELSE
            BEGIN
                SET @vw_LocationAreaCode = NULL
                SET @vw_LocationExchangeNumber = NULL
                SET @vw_LocationUnitNumber = NULL
            END


            -- If any of regular contact fields are populated, Insert the contact as an involved
            -- Otherwise skip

            IF (@v_ContactAddress1 IS NOT NULL) OR
               (@v_ContactAddress2 IS NOT NULL) OR
               (@v_ContactAddressCity IS NOT NULL) OR
               (@v_ContactAddressState IS NOT NULL) OR
               (@v_ContactAddressZip IS NOT NULL) OR
               (@v_ContactNameFirst IS NOT NULL) OR
               (@v_ContactNameLast IS NOT NULL) OR
               (@v_ContactPhone IS NOT NULL) OR
               (@v_ContactNightPhone IS NOT NULL) OR
               (@v_ContactAltPhone IS NOT NULL)
            BEGIN
                -- Split Contact Day, Night and Alt Phone into constituent parts 

                IF (@v_ContactPhone IS NOT NULL AND Len(@v_ContactPhone) = 10 AND IsNumeric(@v_ContactPhone) = 1)
                BEGIN
                    SET @vw_ContactAreaCode = LEFT(@v_ContactPhone, 3)
                    SET @vw_ContactExchangeNumber = SUBSTRING(@v_ContactPhone, 4, 3)
                    SET @vw_ContactUnitNumber = RIGHT(@v_ContactPhone, 4)
                END
                ELSE
                BEGIN
                    SET @vw_ContactAreaCode = NULL
                    SET @vw_ContactExchangeNumber = NULL
                    SET @vw_ContactUnitNumber = NULL
                END

                IF (@v_ContactNightPhone IS NOT NULL AND Len(@v_ContactNightPhone) = 10 AND IsNumeric(@v_ContactNightPhone) = 1)
                BEGIN
                    SET @vw_ContactNightAreaCode = LEFT(@v_ContactNightPhone, 3)
                    SET @vw_ContactNightExchangeNumber = SUBSTRING(@v_ContactNightPhone, 4, 3)
                    SET @vw_ContactNightUnitNumber = RIGHT(@v_ContactNightPhone, 4)
                END
                ELSE
                BEGIN
                    SET @vw_ContactNightAreaCode = NULL
                    SET @vw_ContactNightExchangeNumber = NULL
                    SET @vw_ContactNightUnitNumber = NULL
                END

                IF (@v_ContactAltPhone IS NOT NULL AND Len(@v_ContactAltPhone) = 10 AND IsNumeric(@v_ContactAltPhone) = 1)
                BEGIN
                    SET @vw_ContactAltAreaCode = LEFT(@v_ContactAltPhone, 3)
                    SET @vw_ContactAltExchangeNumber = SUBSTRING(@v_ContactAltPhone, 4, 3)
                    SET @vw_ContactAltUnitNumber = RIGHT(@v_ContactAltPhone, 4)
                END
                ELSE
                BEGIN
                    SET @vw_ContactAltAreaCode = NULL
                    SET @vw_ContactAltExchangeNumber = NULL
                    SET @vw_ContactAltUnitNumber = NULL
                END

            
                -- Default Best Phone CD if not already set
                
                IF @v_ContactBestPhoneCD IS NULL
                BEGIN
                    SET @v_ContactBestPhoneCD = 'D'
                END
                    
            
      	        INSERT INTO dbo.utb_involved	
                (
  	              Address1,
  	              Address2,
   	              AddressCity,
	              AddressState,
	              AddressZip,
                  AlternateAreaCode,
                  AlternateExchangeNumber,
                  AlternateExtensionNumber,
                  AlternateUnitNumber,
                  BestContactPhoneCD,
                  DayAreaCode,
                  DayExchangeNumber,
                  DayExtensionNumber,
                  DayUnitNumber,
                  GenderCD,
                  NameFirst,
	              NameLast,
                  NameTitle,
                  NightAreaCode,
                  NightExchangeNumber,
                  NightExtensionNumber,
                  NightUnitNumber,
	              SysLastUserId,
	              SysLastUpdatedDate
                )
	            VALUES	
	            (
    	          LTrim(RTrim(@v_ContactAddress1)),
    	          LTrim(RTrim(@v_ContactAddress2)),
    	          LTrim(RTrim(@v_ContactAddressCity)),
    	          LTrim(RTrim(@v_ContactAddressState)),
	              LTrim(RTrim(@v_ContactAddressZip)),
                  @vw_ContactAltAreaCode,
                  @vw_ContactAltExchangeNumber,
                  LTrim(RTrim(@v_ContactAltPhoneExt)),
                  @vw_ContactAltUnitNumber,
                  LTrim(RTrim(@v_ContactBestPhoneCD)),
                  @vw_ContactAreaCode,
                  @vw_ContactExchangeNumber,
                  LTrim(RTrim(@v_ContactPhoneExt)),
                  @vw_ContactUnitNumber,
                  @Gender,
    	          LTrim(RTrim(@v_ContactNameFirst)),
	              LTrim(RTrim(@v_ContactNameLast)),
                  LTrim(RTrim(@v_ContactNameTitle)),
                  @vw_ContactNightAreaCode,
                  @vw_ContactNightExchangeNumber,
                  LTrim(RTrim(@v_ContactNightPhoneExt)),
                  @vw_ContactNightUnitNumber,
		          @UserID,
		          @ModifiedDateTime
                )

                IF @@ERROR <> 0
                BEGIN
                    -- Insertion Failure
        
                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle %u Processing) Error inserting contact into utb_involved.', 16, 1, @ProcName, @v_VehicleNumber)
                    RETURN
                END

                SET @vw_ContactInvolvedID = SCOPE_IDENTITY()
            
            END
            ELSE
            BEGIN
                SET @vw_ContactInvolvedID = NULL
            END
    
    
            -- Insert the claim vehicle

            INSERT INTO dbo.utb_claim_vehicle	
            (
                ClaimAspectID,
                ContactInvolvedID,
                BodyStyle,
                Color,
                DriveableFlag,
                ImpactSpeed,
                LicensePlateNumber,
                LicensePlateState,
                LocationAddress1,  
                LocationAddress2,  
                LocationAreaCode,
                LocationCity,
                LocationExchangeNumber,
                LocationName,      
                LocationState,
                LocationUnitNumber,
                LocationZip,
                Make,
                Mileage,
                Model,
                NADAId,
                PermissionToDriveCD,
                PostedSpeed,
                Remarks,
                RentalDaysAuthorized,
                RentalInstructions,
                TitleName,
                TitleState,
                TitleStatus,
                VehicleYear,
                Vin,
                SysLastUserId,
                SysLastUpdatedDate
            )
            VALUES	
            (
                @vw_ClaimAspectID,
                @vw_ContactInvolvedID,
                LTrim(RTrim(@v_BodyStyle)),
                LTrim(RTrim(@v_Color)),
                @v_Drivable,
                @v_ImpactSpeed,
                LTrim(RTrim(@v_LicensePlateNumber)),
                LTrim(RTrim(@v_LicensePlateState)),
                LTrim(RTrim(@v_LocationAddress1)),
                LTrim(RTrim(@v_LocationAddress2)),
                @vw_LocationAreaCode,
                LTrim(RTrim(@v_LocationCity)),
                @vw_LocationExchangeNumber,
                LTrim(RTrim(@v_LocationName)),
                LTrim(RTrim(@v_LocationState)),
                @vw_LocationUnitNumber,
                LTrim(RTrim(@v_LocationZip)),
                LTrim(RTrim(@v_Make)),
                LTrim(RTrim(@v_Mileage)),
                LTrim(RTrim(@v_Model)),
                @v_NADAId,
                LTrim(RTrim(@v_PermissionToDrive)),
                @v_PostedSpeed,
                LTrim(RTrim(@v_Remarks)),
                @v_RentalDaysAuthorized,
                LTrim(RTrim(@v_RentalInstructions)),
                LTrim(RTrim(@v_TitleName)),
                LTrim(RTrim(@v_TitleState)),
                LTrim(RTrim(@v_TitleStatus)),
                @v_VehicleYear,
                LTrim(RTrim(@v_Vin)),
                @UserID,
                @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
        
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Vehicle %u Processing) Error inserting into utb_claim_vehicle.', 16, 1, @ProcName, @v_VehicleNumber)
                RETURN
            END


            -- Process Airbag deployment

            IF @v_AirBagDriverFront = 1
            BEGIN
                -- Insert airbag deployment record
                
                INSERT INTO dbo.utb_vehicle_safety_device 
                (
                    ClaimAspectID,
                    SafetyDeviceID,
                    SysLastUserID,
                    SysLastUpdatedDate
                )
                VALUES
                (
                    @vw_ClaimAspectID,
                    @AirBagDriverFrontID,
                    @UserID,
                    @ModifiedDateTime
                )

                IF @@ERROR <> 0
                BEGIN
                    -- Insertion Failure
        
                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle %u Processing) Error inserting "Airbag - Driver Front" into utb_vehicle_safety_device.', 16, 1, @ProcName, @v_VehicleNumber)
                    RETURN
                END
            END


            IF @v_AirBagDriverSide = 1
            BEGIN
                -- Insert airbag deployment record
                
                INSERT INTO dbo.utb_vehicle_safety_device 
                (
                    ClaimAspectID,
                    SafetyDeviceID,
                    SysLastUserID,
                    SysLastUpdatedDate
                )
                VALUES
                (
                    @vw_ClaimAspectID,
                    @AirBagDriverSideID,
                    @UserID,
                    @ModifiedDateTime
                )

                IF @@ERROR <> 0
                BEGIN
                    -- Insertion Failure
        
                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle %u Processing) Error inserting "Airbag - Driver Side" into utb_vehicle_safety_device.', 16, 1, @ProcName, @v_VehicleNumber)
                    RETURN
                END
            END


            IF @v_AirBagPassengerFront = 1
            BEGIN
                -- Insert airbag deployment record
                
                INSERT INTO dbo.utb_vehicle_safety_device 
                (
                    ClaimAspectID,
                    SafetyDeviceID,
                    SysLastUserID,
                    SysLastUpdatedDate
                )
                VALUES
                (
                    @vw_ClaimAspectID,
                    @AirBagPassengerFrontID,
                    @UserID,
                    @ModifiedDateTime
                )

                IF @@ERROR <> 0
                BEGIN
                    -- Insertion Failure
        
                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle %u Processing) Error inserting "Airbag - Passenger Front" into utb_vehicle_safety_device.', 16, 1, @ProcName, @v_VehicleNumber)
                    RETURN
                END
            END


            IF @v_AirBagPassengerSide = 1
            BEGIN
                -- Insert airbag deployment record
                
                INSERT INTO dbo.utb_vehicle_safety_device 
                (
                    ClaimAspectID,
                    SafetyDeviceID,
                    SysLastUserID,
                    SysLastUpdatedDate
                )
                VALUES
                (
                    @vw_ClaimAspectID,
                    @AirBagPassengerSideID,
                    @UserID,
                    @ModifiedDateTime
                )

                IF @@ERROR <> 0
                BEGIN
                    -- Insertion Failure
        
                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle %u Processing) Error inserting "Airbag - Passenger Side" into utb_vehicle_safety_device.', 16, 1, @ProcName, @v_VehicleNumber)
                    RETURN
                END
            END


            IF @v_AirBagHeadliner = 1
            BEGIN
                -- Insert airbag deployment record
                
                INSERT INTO dbo.utb_vehicle_safety_device 
                (
                    ClaimAspectID,
                    SafetyDeviceID,
                    SysLastUserID,
                    SysLastUpdatedDate
                )
                VALUES
                (
                    @vw_ClaimAspectID,
                    @AirBagHeadlinerID,
                    @UserID,
                    @ModifiedDateTime
                )

                IF @@ERROR <> 0
                BEGIN
                    -- Insertion Failure
        
                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle %u Processing) Error inserting "Airbag - Headliner" into utb_vehicle_safety_device.', 16, 1, @ProcName, @v_VehicleNumber)
                    RETURN
                END
            END


            -- Process impact(s) to the vehicle

            INSERT INTO dbo.utb_vehicle_impact
            (
                ClaimAspectID,
                ImpactID,
                CurrentImpactFlag,
                PrimaryImpactFlag,
                PriorImpactFlag,
                SysLastUserID,
                SysLastUpdatedDate
            )
            SELECT
                @vw_ClaimAspectID,
                value,
                1,
                0,
                0,
                @UserID,
                @ModifiedDateTime
              FROM 
                dbo.ufnUtilityParseString( @v_ImpactLocations, ',', 1 )     -- 1 means to trim spaces
              WHERE VALUE <> ''
        
            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
    
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Vehicle %u Processing) Error inserting impact points into utb_vehicle_impact.', 16, 1, @ProcName, @v_VehicleNumber)
                RETURN
            END
    

            -- Now assume the first impact is the primary and set the flag in the table

            UPDATE dbo.utb_vehicle_impact
               SET
                PrimaryImpactFlag = 1
               WHERE
                ClaimAspectID = @vw_ClaimAspectID AND
                ImpactID = (SELECT TOP 1 value FROM dbo.ufnUtilityParseString( @v_ImpactLocations, ',', 1 ) WHERE VALUE <> '')

            IF @@ERROR <> 0
            BEGIN
                -- Update Failure
    
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Vehicle %u Processing) Error updating primary impact point in utb_vehicle_impact.', 16, 1, @ProcName, @v_VehicleNumber)
                RETURN
            END
    

            -- Now update existing impact records to set the prior impact flag on any duplicates in the Prior Damage parameter
        
            UPDATE dbo.utb_vehicle_impact
               SET
                PriorImpactFlag = 1
               WHERE
                ClaimAspectID = @vw_ClaimAspectID AND
                ImpactID IN (SELECT value FROM dbo.ufnUtilityParseString( @v_PriorDamage, ',', 1 ) WHERE VALUE <> '')     -- 1 means to trim spaces
        
            IF @@ERROR <> 0
            BEGIN
                -- Update Failure
    
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Vehicle %u Processing) Error updating prior impact points in utb_vehicle_impact.', 16, 1, @ProcName, @v_VehicleNumber)
                RETURN
            END

    
            -- Now insert any prior impact records that do not yet exist 

            INSERT INTO dbo.utb_vehicle_impact
            (
                ClaimAspectID,
                ImpactID,
                CurrentImpactFlag,
                PrimaryImpactFlag,
                PriorImpactFlag,
                SysLastUserID,
                SysLastUpdatedDate
            )
            SELECT
                @vw_ClaimAspectID,
                value,
                0,
                0,
                1,
                @UserID,
                @ModifiedDateTime
              FROM 
                dbo.ufnUtilityParseString( @v_PriorDamage, ',', 1 )     -- 1 means to trim spaces
              WHERE value NOT IN (SELECT ImpactID 
                                      FROM dbo.utb_vehicle_impact 
                                      WHERE
                                         ClaimAspectID = @vw_ClaimAspectID)

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
    
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Vehicle %u Processing) Error inserting prior impact points into utb_vehicle_impact.', 16, 1, @ProcName, @v_VehicleNumber)
                RETURN
            END
        
        
            -- If a Shop Location has already been selected for this vehicle, create an assignment record
            
            IF @vw_ShopLocationID IS NOT NULL OR 
               @vw_AppraiserID IS NOT NULL
            BEGIN
				------------------
				-- Debugging 
				------------------
				IF @Debug = 1
				BEGIN
                    PRINT ''
                    PRINT 'Executing uspWorkflowSelectShop for primary assignment'
				END
				ELSE IF @Debug = 2
				BEGIN
					SET @DebugMsg = 'Executing uspWorkflowSelectShop for primary assignment'

					SET @EventSeq += 1

					SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
					SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
					SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
					SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
					SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
					SET @DebugCmd += ', @vEventDescription = ''====> Executing *** uspWorkflowSelectShop *** for primary assignment: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
					SET @DebugCmd += ', @vEventDetailedDescription = ''Executing uspWorkflowSelectShop for primary assignment'''
					SET @DebugCmd += ', @vEventXML = ''EXEC_SP' + @DebugMsg + ''''
					SET @DebugCmd += ', @iRecID = 0'

					--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				END
			                     
                exec uspWorkflowSelectShop @ClaimAspectServiceChannelID     = @vw_ClaimAspectServiceChannelID,
                                           @SelectOperationCD               = 'S',
                                           @ShopLocationID                  = @vw_ShopLocationID,
                                           @AppraiserID                     = @vw_AppraiserID,
                                           @AssignmentRemarks               = @v_ShopRemarks,
                                           @UserID                          = @UserID,
                                           @NotifyEvent                     = 0,     -- Tell the proc not to notify APD workflow.  We will do it manually later.
                                           @ShopSearchLogID                 = @v_ShopSearchLogID,   
                                           @SelectedShopRank                = @v_SelectedShopRank,
                                           @SelectedShopScore               = @V_SelectedShopScore,
                                           @AssignmentSequenceNumber        = 1     -- This will be the primary assignment for the vehicle   
               
                IF @@ERROR <> 0
                BEGIN
                    -- Error executing procedure

                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle %u Processing) Error selecting shop for vehicle"', 16, 1, @ProcName, @v_VehicleNumber)
                    RETURN
                END 
            END
            
            
            -- If this is an RRP claim, also pre-select the LDAU as per service channel requirements
            IF @vw_ServiceChannelCDWork = 'RRP'
            BEGIN
                SELECT @SecondaryLDAUShopLocationID = CASE 
                                                        WHEN @DeskAuditAppraiserType = 'S' THEN @DeskAuditID 
                                                        ELSE NULL 
                                                      END,
                       @SecondaryLDAUAppraiserID    = CASE 
                                                        WHEN @DeskAuditAppraiserType = 'A' THEN @DeskAuditID 
                                                        ELSE NULL 
                                                      END

				------------------
				-- Debugging 
				------------------
				IF @Debug = 1
				BEGIN
                    PRINT ''
                    PRINT 'Executing uspWorkflowSelectShop for secondary assignment'
				END
				ELSE IF @Debug = 2
				BEGIN
					SET @DebugMsg = 'Executing uspWorkflowSelectShop for primary assignment'

					SET @EventSeq += 1

					SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
					SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
					SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
					SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
					SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
					SET @DebugCmd += ', @vEventDescription = ''====> Executing *** uspWorkflowSelectShop *** for secondary assignmentt: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
					SET @DebugCmd += ', @vEventDetailedDescription = ''Executing uspWorkflowSelectShop for secondary assignment'''
					SET @DebugCmd += ', @vEventXML = ''EXEC_SP' + @DebugMsg + ''''
					SET @DebugCmd += ', @iRecID = 0'

					--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				END
                
                exec uspWorkflowSelectShop @ClaimAspectServiceChannelID     = @vw_ClaimAspectServiceChannelID,
                                           @SelectOperationCD               = 'S',
                                           @ShopLocationID                  = @SecondaryLDAUShopLocationID,
                                           @AppraiserID                     = @SecondaryLDAUAppraiserID,
                                           @AssignmentRemarks               = 'Secondary assignment',
                                           @UserID                          = @UserID,
                                           @NotifyEvent                     = 0,     -- Tell the proc not to notify APD workflow.  We will do it manually later.
                                           @AssignmentSequenceNumber        = 2      -- This will be the secondary assignment for the vehicle   
               
                IF @@ERROR <> 0
                BEGIN
                    -- Error executing procedure

                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Vehicle %u Processing) Error selecting shop for vehicle"', 16, 1, @ProcName, @v_VehicleNumber)
                    RETURN
                END             
                
                SELECT @RRP_LDAU_AssignmentID = AssignmentID
                FROM dbo.utb_assignment
                WHERE ClaimAspectServiceChannelID = @vw_ClaimAspectServiceChannelID
                  AND AssignmentSequenceNumber = 2
                  AND CancellationDate is NULL
                  
                IF @RRP_LDAU_AssignmentID > 0
                BEGIN
                    -- looks like we auto assign LDAU assignment. We will do the same for RRP secondary LDAU assignment
                    EXEC uspWorkflowAssignShop @AssignmentID = @RRP_LDAU_AssignmentID

                    IF @@ERROR <> 0
                    BEGIN
                        -- Error executing procedure

                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (Vehicle %u Processing) Error assigning shop for vehicle"', 16, 1, @ProcName, @v_VehicleNumber)
                        RETURN
                    END             
                END
            END            
        END    

    
        -- Get the next vehicle
        
        FETCH NEXT 
          FROM csrClaimVehicle 
          INTO @v_VehicleNumber, 
               @v_ClientCoverageTypeID,
               @v_AirBagDriverFront, 
               @v_AirBagDriverSide, 
               @v_AirBagHeadliner, 
               @v_AirBagPassengerFront, 
               @v_AirBagPassengerSide, 
               @v_AssignmentTypeID,
               @v_BodyStyle, 
               @v_Color, 
               @v_ContactAddress1, 
               @v_ContactAddress2, 
               @v_ContactAddressCity,
               @v_ContactAddressState,
               @v_ContactAddressZip, 
               @v_ContactBestPhoneCD, 
               @v_ContactNameFirst, 
               @v_ContactNameLast, 
               @v_ContactNameTitle,
               @v_ContactPhone, 
               @v_ContactPhoneExt,
               @v_ContactNightPhone, 
               @v_ContactNightPhoneExt,
               @v_ContactAltPhone, 
               @v_ContactAltPhoneExt,
               @v_CoverageProfileCD, 
               @v_Drivable, 
               @v_ExposureCD,
               @v_GlassDamageFlag,
               @v_ImpactLocations, 
               @v_ImpactSpeed, 
               @v_LicensePlateNumber, 
               @v_LicensePlateState, 
               @v_LocationAddress1, 
               @v_LocationAddress2, 
               @v_LocationCity, 
               @v_LocationName, 
               @v_LocationPhone, 
               @v_LocationState, 
               @v_LocationZip, 
               @v_Make, 
               @v_Mileage, 
               @v_Model, 
               @v_NADAId, 
               @v_PermissionToDrive,
               @v_PhysicalDamageFlag, 
               @v_PostedSpeed, 
               @v_PriorDamage, 
               @v_PriorityFlag,
               @v_Remarks, 
               @v_RentalDaysAuthorized,
               @v_RentalInstructions,
               @v_RepairLocationCity,
               @v_RepairLocationCounty,
               @v_RepairLocationState,
               @v_SelectedShopRank,
               @V_SelectedShopScore,
               @v_ShopLocationID,
               @v_ShopRemarks,
               @v_ShopSearchLogID,
               @v_TitleName,
               @v_TitleState,
               @v_TitleStatus,
               @v_VehicleYear, 
               @v_Vin,
               @v_SourceApplicationPassthruDataVeh               

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            ROLLBACK TRANSACTION
            RAISERROR('%s: (Vehicle Processing) SQL Server Error fetching next cursor record.', 16, 1, @ProcName)
            RETURN
        END
    END

    CLOSE csrClaimVehicle
    DEALLOCATE csrClaimVehicle



    /*********************************************************************************************
    **********************************************************************************************
    *   BEGIN PROCESSING FOR INVOLVED DATA
    **********************************************************************************************
    *********************************************************************************************/

     --Variables for Involved

    DECLARE @i_VehicleNumber                AS  udt_std_int
    DECLARE @i_Address1                     AS  udt_addr_line_1
    DECLARE @i_Address2                     AS  udt_addr_line_2
    DECLARE @i_AddressCity                  AS  udt_addr_city
    DECLARE @i_AddressState                 AS  udt_addr_state
    DECLARE @i_AddressZip                   AS  udt_addr_zip_code
    DECLARE @i_Age                          AS  udt_std_int_tiny
    DECLARE @i_AttorneyAddress1             AS  udt_addr_line_1  
    DECLARE @i_AttorneyAddress2             AS  udt_addr_line_2
    DECLARE @i_AttorneyAddressCity          AS  udt_addr_city
    DECLARE @i_AttorneyAddressState         AS  udt_addr_state
    DECLARE @i_AttorneyAddressZip           AS  udt_addr_zip_code
    DECLARE @i_AttorneyName                 AS  udt_std_name
    DECLARE @i_AttorneyPhone                AS  VARCHAR(15)
    DECLARE @i_AttorneyPhoneExt             AS  udt_ph_extension_number
    DECLARE @i_BestPhoneCode                AS  udt_std_cd
    DECLARE @i_BestTimeToCall               AS  udt_std_desc_short
    DECLARE @i_BusinessName                 AS  udt_std_name
    DECLARE @i_DateOfBirth                  AS  VARCHAR(10)
    DECLARE @i_DoctorAddress1               AS  udt_addr_line_1 
    DECLARE @i_DoctorAddress2               AS  udt_addr_line_2
    DECLARE @i_DoctorAddressCity            AS  udt_addr_city
    DECLARE @i_DoctorAddressState           AS  udt_addr_state
    DECLARE @i_DoctorAddressZip             AS  udt_addr_zip_code
    DECLARE @i_DoctorName                   AS  udt_std_name
    DECLARE @i_DoctorPhone                  AS  VARCHAR(15)
    DECLARE @i_DoctorPhoneExt               AS  udt_ph_extension_number
    DECLARE @i_DriverLicenseNumber          AS  udt_per_license_no
    DECLARE @i_DriverLicenseState           AS  udt_addr_state
    DECLARE @i_EmployerAddress1             AS  udt_addr_line_1 
    DECLARE @i_EmployerAddress2             AS  udt_addr_line_2
    DECLARE @i_EmployerAddressCity          AS  udt_addr_city
    DECLARE @i_EmployerAddressState         AS  udt_addr_state
    DECLARE @i_EmployerAddressZip           AS  udt_addr_zip_code
    DECLARE @i_EmployerName                 AS  udt_std_name
    DECLARE @i_EmployerPhone                AS  VARCHAR(15)
    DECLARE @i_EmployerPhoneExt             AS  udt_ph_extension_number
    DECLARE @i_Gender                       AS  udt_std_cd
    DECLARE @i_Injured                      AS  udt_std_cd
    DECLARE @i_InjuryDescription            AS  udt_std_desc_long
    DECLARE @i_InjuryTypeId                 AS  udt_std_id
    DECLARE @i_InvolvedTypeClaimant         AS  udt_std_flag
    DECLARE @i_InvolvedTypeDriver           AS  udt_std_flag
    DECLARE @i_InvolvedTypeInsured          AS  udt_std_flag
    DECLARE @i_InvolvedTypeOwner            AS  udt_std_flag
    DECLARE @i_InvolvedTypePassenger        AS  udt_std_flag
    DECLARE @i_InvolvedTypePedestrian       AS  udt_std_flag
    DECLARE @i_InvolvedTypeWitness          AS  udt_std_flag
    DECLARE @i_LocationInVehicleId          AS  udt_std_id
    DECLARE @i_NameFirst                    AS  udt_per_name
    DECLARE @i_NameLast                     AS  udt_per_name
    DECLARE @i_NameTitle                    AS  udt_per_title
    DECLARE @i_PhoneAlternate               AS  VARCHAR(15)
    DECLARE @i_PhoneAlternateExt            AS  udt_ph_extension_number
    DECLARE @i_PhoneDay                     AS  VARCHAR(15)
    DECLARE @i_PhoneDayExt                  AS  udt_ph_extension_number  
    DECLARE @i_PhoneNight                   AS  VARCHAR(15)
    DECLARE @i_PhoneNightExt                AS  udt_ph_extension_number
    DECLARE @i_SeatBelt                     AS  udt_std_cd
    DECLARE @i_TaxID                        AS  udt_fed_tax_id
    DECLARE @i_ThirdPartyInsuranceName      AS  udt_std_name
    DECLARE @i_ThirdPartyInsurancePhone     AS  VARCHAR(15)
    DECLARE @i_ThirdPartyInsurancePhoneExt  AS  udt_ph_extension_number
    DECLARE @i_Violation                    AS  udt_std_flag
    DECLARE @i_ViolationDescription         AS  udt_std_desc_short
    DECLARE @i_WitnessLocation              AS  udt_std_desc_long
  
    DECLARE @iw_AlternateAreaCode           AS udt_ph_area_code
    DECLARE @iw_AlternateExchangeNumber     AS udt_ph_exchange_number
    DECLARE @iw_AlternateUnitNumber         AS udt_ph_unit_number
    DECLARE @iw_AttorneyAreaCode            AS udt_ph_area_code
    DECLARE @iw_AttorneyExchangeNumber      AS udt_ph_exchange_number
    DECLARE @iw_AttorneyUnitNumber          AS udt_ph_unit_number
    DECLARE @iw_DayAreaCode                 AS udt_ph_area_code
    DECLARE @iw_DayExchangeNumber           AS udt_ph_exchange_number
    DECLARE @iw_DayUnitNumber               AS udt_ph_unit_number
    DECLARE @iw_DoctorAreaCode              AS udt_ph_area_code
    DECLARE @iw_DoctorExchangeNumber        AS udt_ph_exchange_number
    DECLARE @iw_DoctorUnitNumber            AS udt_ph_unit_number
    DECLARE @iw_EmployerAreaCode            AS udt_ph_area_code
    DECLARE @iw_EmployerExchangeNumber      AS udt_ph_exchange_number
    DECLARE @iw_EmployerUnitNumber          AS udt_ph_unit_number
    DECLARE @iw_NightAreaCode               AS udt_ph_area_code
    DECLARE @iw_NightExchangeNumber         AS udt_ph_exchange_number
    DECLARE @iw_NightUnitNumber             AS udt_ph_unit_number
    DECLARE @iw_InsuranceAreaCode           AS udt_ph_area_code
    DECLARE @iw_InsuranceExchangeNumber     AS udt_ph_exchange_number
    DECLARE @iw_InsuranceUnitNumber         AS udt_ph_unit_number

    DECLARE @iw_AttorneyInvolvedID          AS udt_std_id_big
    DECLARE @iw_ClaimAspectID               AS udt_std_id_big
    DECLARE @iw_DoctorInvolvedID            AS udt_std_id_big
    DECLARE @iw_EmployerInvolvedID          AS udt_std_id_big
    DECLARE @iw_InvolvedID                  AS udt_std_id_big


    -- Insert involved data
    
    DECLARE csrFNOLInsertInvolved CURSOR FOR
      SELECT  VehicleNumber,
              Address1, 
              Address2, 
              AddressCity, 
              AddressState, 
              AddressZip,
              Age, 
              AttorneyAddress1, 
              AttorneyAddress2, 
              AttorneyAddressCity, 
              AttorneyAddressState, 
              AttorneyAddressZip, 
              AttorneyName, 
              AttorneyPhone, 
              AttorneyPhoneExt, 
              BestPhoneCode, 
              BestTimeToCall, 
              BusinessName, 
              DateOfBirth, 
              DoctorAddress1, 
              DoctorAddress2, 
              DoctorAddressCity, 
              DoctorAddressState, 
              DoctorAddressZip, 
              DoctorName, 
              DoctorPhone, 
              DoctorPhoneExt, 
              DriverLicenseNumber, 
              DriverLicenseState, 
              EmployerAddress1, 
              EmployerAddress2, 
              EmployerAddressCity, 
              EmployerAddressState, 
              EmployerAddressZip, 
              EmployerName, 
              EmployerPhone, 
              EmployerPhoneExt, 
              Gender, 
              Injured, 
              InjuryDescription, 
              InjuryTypeId, 
              InvolvedTypeClaimant, 
              InvolvedTypeDriver, 
              InvolvedTypeInsured, 
              InvolvedTypeOwner, 
              InvolvedTypePassenger, 
              InvolvedTypePedestrian, 
              InvolvedTypeWitness, 
              LocationInVehicleId, 
              NameFirst, 
              NameLast, 
              NameTitle, 
              PhoneAlternate, 
              PhoneAlternateExt, 
              PhoneDay, 
              PhoneDayExt, 
              PhoneNight, 
              PhoneNightExt, 
              SeatBelt, 
              TaxID, 
              ThirdPartyInsuranceName, 
              ThirdPartyInsurancePhone, 
              ThirdPartyInsurancePhoneExt, 
              Violation, 
              ViolationDescription, 
              WitnessLocation
        FROM  dbo.utb_fnol_involved_load
        WHERE LynxID = @LynxID
        ORDER BY VehicleNumber
            
    OPEN csrFNOLInsertInvolved
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        ROLLBACK TRANSACTION
        RAISERROR('%s: (Involved Processing) SQL Server Error Opening Cursor.', 16, 1, @ProcName)
        RETURN
    END

    
    -- Get first involved
    
    FETCH NEXT 
      FROM csrFNOLInsertInvolved 
      INTO @i_VehicleNumber,
           @i_Address1,
           @i_Address2,
           @i_AddressCity, 
           @i_AddressState, 
           @i_AddressZip, 
           @i_Age,
           @i_AttorneyAddress1, 
           @i_AttorneyAddress2,
           @i_AttorneyAddressCity, 
           @i_AttorneyAddressState, 
           @i_AttorneyAddressZip, 
           @i_AttorneyName, 
           @i_AttorneyPhone, 
           @i_AttorneyPhoneExt, 
           @i_BestPhoneCode, 
           @i_BestTimeToCall, 
           @i_BusinessName, 
           @i_DateOfBirth, 
           @i_DoctorAddress1, 
           @i_DoctorAddress2, 
           @i_DoctorAddressCity, 
           @i_DoctorAddressState, 
           @i_DoctorAddressZip, 
           @i_DoctorName, 
           @i_DoctorPhone, 
           @i_DoctorPhoneExt, 
           @i_DriverLicenseNumber, 
           @i_DriverLicenseState, 
           @i_EmployerAddress1, 
           @i_EmployerAddress2, 
           @i_EmployerAddressCity, 
           @i_EmployerAddressState, 
           @i_EmployerAddressZip, 
           @i_EmployerName, 
           @i_EmployerPhone, 
           @i_EmployerPhoneExt, 
           @i_Gender, 
           @i_Injured, 
           @i_InjuryDescription, 
           @i_InjuryTypeId, 
           @i_InvolvedTypeClaimant, 
           @i_InvolvedTypeDriver, 
           @i_InvolvedTypeInsured, 
           @i_InvolvedTypeOwner, 
           @i_InvolvedTypePassenger, 
           @i_InvolvedTypePedestrian, 
           @i_InvolvedTypeWitness, 
           @i_LocationInVehicleId, 
           @i_NameFirst, 
           @i_NameLast, 
           @i_NameTitle, 
           @i_PhoneAlternate, 
           @i_PhoneAlternateExt, 
           @i_PhoneDay, 
           @i_PhoneDayExt, 
           @i_PhoneNight, 
           @i_PhoneNightExt, 
           @i_SeatBelt, 
           @i_TaxID, 
           @i_ThirdPartyInsuranceName, 
           @i_ThirdPartyInsurancePhone, 
           @i_ThirdPartyInsurancePhoneExt,  
           @i_Violation, 
           @i_ViolationDescription, 
           @i_WitnessLocation

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        ROLLBACK TRANSACTION
        RAISERROR('%s: (Involved Processing) SQL Server Error fetching first cursor record.', 16, 1, @ProcName)
        RETURN
    END


    WHILE @@Fetch_status = 0
    BEGIN
        -- First determine if this is the insured we are processing for an added vehicle.  If so, we'll skip
        -- inserting the insured because they already exist.
        
        IF (@NewClaimFlag = 0) AND (@i_InvolvedTypeInsured = 1)
        BEGIN
            -- We're adding an insured vehicle after the fact...since it already exists, let's just get the InvolvedID
            -- for use later
            
            SELECT  @iw_InvolvedID = cai.InvolvedID
              FROM  dbo.utb_claim_aspect ca
              LEFT JOIN dbo.utb_claim_aspect_involved cai ON (ca.ClaimAspectID = cai.ClaimAspectID)
              LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
              WHERE ca.LynxID = @LynxID
                AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDClaim
                AND ir.InvolvedRoleTypeID = @InvolvedRoleTypeIDInsured
                
            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved Processing) SQL Server Error getting existing insured involved id.', 16, 1, @ProcName)
                RETURN
            END
            
            IF @iw_InvolvedID IS NULL
            BEGIN
                -- Insured Involved ID not found

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved Processing) Insured Involved ID not found for claim.', 16, 1, @ProcName)
                RETURN
            END            

            IF (@vw_ServiceChannelCDWork = 'DA')
            BEGIN
                UPDATE utb_involved
                  SET  Address1 = @i_Address1,
                       Address2 = @i_Address2,
                       AddressCity = @i_AddressCity,
                       AddressState = @i_AddressState, 
                       AddressZip = @i_AddressZip
                  WHERE InvolvedID = @iw_InvolvedID
                    AND (Address1 IS NULL OR Address1 = '')
                    AND (Address2 IS NULL OR Address2 = '')
                    AND (AddressCity IS NULL OR AddressCity = '')
                    AND (AddressZip IS NULL OR AddressZip = '')

                IF @@ERROR <> 0
                BEGIN
                    -- SQL Server Error

                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Involved Processing) SQL Server Error updating insured involved address.', 16, 1, @ProcName)
                    RETURN
                END
            END
        END
        ELSE
        BEGIN
            -- We need to insert a new involved and process normally
            
            -- Default the gender to unknown if not provided

            IF @i_Gender IS NULL
            BEGIN
                SET @i_Gender = 'U'
            END
        

            -- Default BestPhoneCode to day if not provided

            IF @i_BestPhoneCode IS NULL
            BEGIN
                SET @i_BestPhoneCode = 'D'
            END
        
        
            -- Validate BirthDate if present
        
            IF (@i_DateOfBirth IS NOT NULL AND IsDate(@i_DateOfBirth) = 0)
            BEGIN
                SET @i_DateOfBirth = NULL
            END
            ELSE
            BEGIN
                -- Set Age if not already provided
            
                IF @i_Age IS NULL
                BEGIN
                    SET @i_Age = DateDiff(yy, @i_DateOfBirth, @ModifiedDateTime) 
                END
            END
        

            -- Split Alternate Phone into constituent parts

            IF (@i_PhoneAlternate IS NOT NULL AND Len(@i_PhoneAlternate) = 10 AND IsNumeric(@i_PhoneAlternate) = 1)
            BEGIN
                SET @iw_AlternateAreaCode = LEFT(@i_PhoneAlternate, 3)
                SET @iw_AlternateExchangeNumber = SUBSTRING(@i_PhoneAlternate, 4, 3)
                SET @iw_AlternateUnitNumber = RIGHT(@i_PhoneAlternate, 4)
            END
            ELSE
            BEGIN
                SET @iw_AlternateAreaCode = NULL
                SET @iw_AlternateExchangeNumber = NULL
                SET @iw_AlternateUnitNumber = NULL
            END


            -- Split Day Phone into constituent parts

            IF (@i_PhoneDay IS NOT NULL AND Len(@i_PhoneDay) = 10 AND IsNumeric(@i_PhoneDay) = 1)
            BEGIN
                SET @iw_DayAreaCode = LEFT(@i_PhoneDay, 3)
                SET @iw_DayExchangeNumber = SUBSTRING(@i_PhoneDay, 4, 3)
                SET @iw_DayUnitNumber = RIGHT(@i_PhoneDay, 4)
            END
            ELSE
            BEGIN
                SET @iw_DayAreaCode = NULL
                SET @iw_DayExchangeNumber = NULL
                SET @iw_DayUnitNumber = NULL
            END


            -- Split Night Phone into constituent parts

            IF (@i_PhoneNight IS NOT NULL AND Len(@i_PhoneNight) = 10 AND IsNumeric(@i_PhoneNight) = 1)
            BEGIN
                SET @iw_NightAreaCode = LEFT(@i_PhoneNight, 3)
                SET @iw_NightExchangeNumber = SUBSTRING(@i_PhoneNight, 4, 3)
                SET @iw_NightUnitNumber = RIGHT(@i_PhoneNight, 4)
            END
            ELSE
            BEGIN
                SET @iw_NightAreaCode = NULL
                SET @iw_NightExchangeNumber = NULL
                SET @iw_NightUnitNumber = NULL
            END


            -- Split Third Party Insurance Phone into constituent parts

            IF (@i_ThirdPartyInsurancePhone IS NOT NULL AND Len(@i_ThirdPartyInsurancePhone) = 10 AND IsNumeric(@i_ThirdPartyInsurancePhone) = 1)
            BEGIN
                SET @iw_InsuranceAreaCode = LEFT(@i_ThirdPartyInsurancePhone, 3)
                SET @iw_InsuranceExchangeNumber = SUBSTRING(@i_ThirdPartyInsurancePhone, 4, 3)
                SET @iw_InsuranceUnitNumber = RIGHT(@i_ThirdPartyInsurancePhone, 4)
            END
            ELSE
            BEGIN
                SET @iw_InsuranceAreaCode = NULL
                SET @iw_InsuranceExchangeNumber = NULL
                SET @iw_InsuranceUnitNumber = NULL
            END


            -- Split Attorney Phone into constituent parts

            IF (@i_AttorneyPhone IS NOT NULL AND Len(@i_AttorneyPhone) = 10 AND IsNumeric(@i_AttorneyPhone) = 1)
            BEGIN
                SET @iw_AttorneyAreaCode = LEFT(@i_AttorneyPhone, 3)
                SET @iw_AttorneyExchangeNumber = SUBSTRING(@i_AttorneyPhone, 4, 3)
                SET @iw_AttorneyUnitNumber = RIGHT(@i_AttorneyPhone, 4)
            END
            ELSE
            BEGIN
                SET @iw_AttorneyAreaCode = NULL
                SET @iw_AttorneyExchangeNumber = NULL
                SET @iw_AttorneyUnitNumber = NULL
            END


            -- Split Doctor Phone into constituent parts

            IF (@i_DoctorPhone IS NOT NULL AND Len(@i_DoctorPhone) = 10 AND IsNumeric(@i_DoctorPhone) = 1)
            BEGIN
                SET @iw_DoctorAreaCode = LEFT(@i_DoctorPhone, 3)
                SET @iw_DoctorExchangeNumber = SUBSTRING(@i_DoctorPhone, 4, 3)
                SET @iw_DoctorUnitNumber = RIGHT(@i_DoctorPhone, 4)
            END
            ELSE
            BEGIN
                SET @iw_DoctorAreaCode = NULL
                SET @iw_DoctorExchangeNumber = NULL
                SET @iw_DoctorUnitNumber = NULL
            END


            -- Split Employer Phone into constituent parts

            IF (@i_EmployerPhone IS NOT NULL AND Len(@i_EmployerPhone) = 10 AND IsNumeric(@i_EmployerPhone) = 1)
            BEGIN
                SET @iw_EmployerAreaCode = LEFT(@i_EmployerPhone, 3)
                SET @iw_EmployerExchangeNumber = SUBSTRING(@i_EmployerPhone, 4, 3)
                SET @iw_EmployerUnitNumber = RIGHT(@i_EmployerPhone, 4)
            END
            ELSE
            BEGIN
                SET @iw_EmployerAreaCode = NULL
                SET @iw_EmployerExchangeNumber = NULL
                SET @iw_EmployerUnitNumber = NULL
            END


            -- Create the new involved record

	        INSERT INTO dbo.utb_involved	
            (
		        PersonLocationID,
                Address1,
                Address2,
		        AddressCity,
		        AddressState,
		        AddressZip,
                Age,
		        AlternateAreaCode,
		        AlternateExchangeNumber,
                AlternateExtensionNumber,
		        AlternateUnitNumber,
                BestContactPhoneCD,
		        BestContactTime,
		        BirthDate,
                BusinessName,
		        DayAreaCode,
		        DayExchangeNumber,
                DayExtensionNumber,
		        DayUnitNumber,
                DriverLicenseNumber,
                DriverLicenseState,
		        FedTaxID,
		        GenderCD,
                InjuredCD,
                Location,
		        NameFirst,
		        NameLast,
                NameTitle,
		        NightAreaCode,
		        NightExchangeNumber,
                NightExtensionNumber,
		        NightUnitNumber,
                SeatBeltCD,
		        ThirdPartyInsuranceAreaCode,
		        ThirdPartyInsuranceExchangeNumber,
		        ThirdPartyInsuranceExtensionNumber,
                ThirdPartyInsuranceName,
		        ThirdPartyInsuranceUnitNumber,
                ViolationDescription,
                ViolationFlag,
		        SysLastUserId,
		        SysLastUpdatedDate
            )
	        VALUES	
	        (
		        @i_LocationInVehicleId,
                LTrim(RTrim(@i_Address1)),
                LTrim(RTrim(@i_Address2)),
		        LTrim(RTrim(@i_AddressCity)),
		        LTrim(RTrim(@i_AddressState)),
		        LTrim(RTrim(@i_AddressZip)),
                @i_Age,
		        @iw_AlternateAreaCode,
		        @iw_AlternateExchangeNumber,
                @i_PhoneAlternateExt,
		        @iw_AlternateUnitNumber,
                LTrim(RTrim(@i_BestPhoneCode)),
		        LTrim(RTrim(@i_BestTimeToCall)),
		        @i_DateOfBirth,
                LTrim(RTrim(@i_BusinessName)),
		        @iw_DayAreaCode,
		        @iw_DayExchangeNumber,
                @i_PhoneDayExt,
		        @iw_DayUnitNumber,
                LTrim(RTrim(@i_DriverLicenseNumber)),
                LTrim(RTrim(@i_DriverLicenseState)),
		        LTrim(RTrim(@i_TaxId)),
		        @i_Gender,
                @i_Injured,
                LTrim(RTrim(@i_WitnessLocation)),
		        LTrim(RTrim(@i_NameFirst)),
		        LTrim(RTrim(@i_NameLast)),
                LTrim(RTrim(@i_NameTitle)),
		        @iw_NightAreaCode,
		        @iw_NightExchangeNumber,
                @i_PhoneNightExt,
		        @iw_NightUnitNumber,
                @i_SeatBelt,
                @iw_InsuranceAreaCode,
                @iw_InsuranceExchangeNumber,
                @i_ThirdPartyInsurancePhoneExt,
                LTrim(RTrim(@i_ThirdPartyInsuranceName)),
                @iw_InsuranceUnitNumber,
                LTrim(RTrim(@i_ViolationDescription)),
                @i_Violation,
		        @UserID,
		        @ModifiedDateTime
	        )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting involved into utb_involved.', 16, 1, @ProcName, @i_VehicleNumber)
                RETURN
            END

            SET @iw_InvolvedID = SCOPE_IDENTITY()
        

            -- Link The Involved to the appropriate entity

			------------------
			-- Debugging 
			------------------
			IF @Debug = 1
			BEGIN
                PRINT ''
                PRINT 'Involved Types:'
                PRINT '     @InvolvedTypeClaimant = ' + convert(varchar(2), @i_InvolvedTypeClaimant)
                PRINT '     @InvolvedTypeDriver = ' + convert(varchar(2), @i_InvolvedTypeDriver)
                PRINT '     @InvolvedTypeInsured = ' + convert(varchar(2), @i_InvolvedTypeInsured)
                PRINT '     @InvolvedTypeOwner = ' + convert(varchar(2), @i_InvolvedTypeOwner)
                PRINT '     @InvolvedTypePassenger = ' + convert(varchar(2), @i_InvolvedTypePassenger)
                PRINT '     @InvolvedTypePedestrian = ' + convert(varchar(2), @i_InvolvedTypePedestrian)
                PRINT '     @InvolvedTypeWitness = ' + convert(varchar(2), @i_InvolvedTypeWitness)
			END
			ELSE IF @Debug = 2
			BEGIN
				SET @DebugMsg = ''
                SET @DebugMsg += 'Involved Types:'
                SET @DebugMsg += '     @InvolvedTypeClaimant = ' + convert(varchar(2), ISNULL(@i_InvolvedTypeClaimant,0))
                SET @DebugMsg += '     @InvolvedTypeDriver = ' + convert(varchar(2), ISNULL(@i_InvolvedTypeDriver,0))
                SET @DebugMsg += '     @InvolvedTypeInsured = ' + convert(varchar(2), ISNULL(@i_InvolvedTypeInsured,0))
                SET @DebugMsg += '     @InvolvedTypeOwner = ' + convert(varchar(2), ISNULL(@i_InvolvedTypeOwner,0))
                SET @DebugMsg += '     @InvolvedTypePassenger = ' + convert(varchar(2), ISNULL(@i_InvolvedTypePassenger,0))
                SET @DebugMsg += '     @InvolvedTypePedestrian = ' + convert(varchar(2), ISNULL(@i_InvolvedTypePedestrian,0))
                SET @DebugMsg += '     @InvolvedTypeWitness = ' + convert(varchar(2), ISNULL(@i_InvolvedTypeWitness,0))

				SET @EventSeq += 1

				SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
				SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
				SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(INVOLVED_TYPES_VARS)'''
				SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
				SET @DebugCmd += ', @vEventStatus = ''INVOLVED_TYPES_VARS'''
				SET @DebugCmd += ', @vEventDescription = ''====> Processing Involved Types variables: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
				SET @DebugCmd += ', @vEventDetailedDescription = ''Involved Types variables.  Param data in EventXML'''
				SET @DebugCmd += ', @vEventXML = ''INVOLVED_TYPES_VARS=' + @DebugMsg + ''''
				SET @DebugCmd += ', @iRecID = 0'

				--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
			END        
        
            -- Check for claim level involvement
        
            IF  (@i_InvolvedTypeInsured = 1) OR
                (@i_InvolvedTypePedestrian = 1) OR
                (@i_InvolvedTypeWitness = 1)
            BEGIN
                -- This involved needs to be linked to the claim
            
                INSERT INTO dbo.utb_claim_aspect_involved 
                   (ClaimAspectID,
                    InvolvedID,
                    EnabledFlag,
                    SysLastUserID,
                    SysLastUpdatedDate)
                  VALUES (@ClaimAspectIDClaim, 
                          @iw_InvolvedID,
                          1,     -- Set involved as enabled 
                          @UserID,
                          @ModifiedDateTime)

                IF @@ERROR <> 0
                BEGIN
                    -- Insertion Failure

                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting involved-claim link into utb_claim_aspect_involved.', 16, 1, @ProcName, @i_VehicleNumber)
                    RETURN
                END
            
            
                -- Insert Involved Role records
            
                IF @i_InvolvedTypeInsured = 1
                BEGIN
                    INSERT INTO dbo.utb_involved_role
                        (InvolvedID,
                         InvolvedRoleTypeID,
                         SysLastUserID,
                         SysLastUpdatedDate)  
                      VALUES (@iw_InvolvedID, 
                              @InvolvedRoleTypeIDInsured,
                              @UserID, 
                              @ModifiedDateTime)

                    IF @@ERROR <> 0
                    BEGIN
                        -- Insertion Failure

                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting "Insured" into utb_involved_role.', 16, 1, @ProcName, @i_VehicleNumber)
                        RETURN
                    END
                END

            
                IF (@i_InvolvedTypePedestrian = 1) OR
                   (@i_InvolvedTypeWitness = 1) 
                BEGIN
                    -- Note:  APD does not support "pedestrians", the definition being a claimant not in a vehicle.  Pedestrians
                    -- who are not claimants are witnesses.
                    
                    INSERT INTO dbo.utb_involved_role
                        (InvolvedID,
                         InvolvedRoleTypeID,
                         SysLastUserID,
                         SysLastUpdatedDate)  
                      VALUES (@iw_InvolvedID, 
                              @InvolvedRoleTypeIDWitness,
                              @UserID, 
                              @ModifiedDateTime)

                    IF @@ERROR <> 0
                    BEGIN
                        -- Insertion Failure

                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting "Witness" into utb_involved_role.', 16, 1, @ProcName, @i_VehicleNumber)
                        RETURN
                    END
                END
            END
        END
                
        
        -- Now vehicle level involvement

        IF (@i_InvolvedTypeClaimant = 1) OR
           (@i_InvolvedTypeDriver = 1) OR             
           (@i_InvolvedTypeInsured = 1) OR
           (@i_InvolvedTypeOwner = 1) OR
           (@i_InvolvedTypePassenger = 1) 
        BEGIN
            SELECT  @iw_ClaimAspectID = ClaimAspectID 
              FROM  dbo.utb_claim_aspect 
              WHERE LynxID = @LynxID
                AND ClaimAspectTypeID = @ClaimAspectTypeIDVehicle
                AND ClaimAspectNumber = @i_VehicleNumber

            IF @@ERROR <> 0
            BEGIN
                -- SQL Server Error

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved for Vehicle %u Processing) Error getting ClaimAspectID for involved vehicle.', 16, 1, @ProcName, @i_VehicleNumber)
                RETURN
            END

            
            -- Make sure a vehicle was found.  If vehicle information was not sent in, a vehicle may not have been created.
            
            IF @iw_ClaimAspectID IS NOT NULL
            BEGIN             
                -- The vehicle was found, go ahead with insert
                
                INSERT INTO dbo.utb_claim_aspect_involved 
                   (ClaimAspectID,
                    InvolvedID,
                    EnabledFlag,
                    SysLastUserID,
                    SysLastUpdatedDate)
                  VALUES (@iw_ClaimAspectID, 
                          @iw_InvolvedID,
                          1,     -- Set involved as enabled 
                          @UserID,
                          @ModifiedDateTime)

                IF @@ERROR <> 0
                BEGIN
                    -- Insertion Failure

                    ROLLBACK TRANSACTION
                    RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting involved-vehicle link into utb_claim_aspect_involved.', 16, 1, @ProcName, @i_VehicleNumber)
                    RETURN
                END
            
                        
                IF @i_InvolvedTypeDriver = 1
                BEGIN
                    INSERT INTO dbo.utb_involved_role
                        (InvolvedID,
                         InvolvedRoleTypeID,
                         SysLastUserID,
                         SysLastUpdatedDate)  
                      VALUES (@iw_InvolvedID, 
                              @InvolvedRoleTypeIDDriver,
                              @UserID, 
                              @ModifiedDateTime)

                    IF @@ERROR <> 0
                    BEGIN
                        -- Insertion Failure

                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting "Driver" into utb_involved_role.', 16, 1, @ProcName, @i_VehicleNumber)
                        RETURN
                    END
                END

            
                IF @i_InvolvedTypeClaimant = 1
                BEGIN
                    INSERT INTO dbo.utb_involved_role
                        (InvolvedID,
                         InvolvedRoleTypeID,
                         SysLastUserID,
                         SysLastUpdatedDate)  
                      VALUES (@iw_InvolvedID, 
                              @InvolvedRoleTypeIDClaimant,
                              @UserID, 
                              @ModifiedDateTime)

                    IF @@ERROR <> 0
                    BEGIN
                        -- Insertion Failure

                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting "Claimant" into utb_involved_role.', 16, 1, @ProcName, @i_VehicleNumber)
                        RETURN
                    END
                END

            
                IF @i_InvolvedTypeOwner = 1
                BEGIN
                    INSERT INTO dbo.utb_involved_role
                        (InvolvedID,
                         InvolvedRoleTypeID,
                         SysLastUserID,
                         SysLastUpdatedDate)  
                      VALUES (@iw_InvolvedID, 
                              @InvolvedRoleTypeIDOwner,
                              @UserID, 
                              @ModifiedDateTime)

                    IF @@ERROR <> 0
                    BEGIN
                        -- Insertion Failure

                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting "Owner" into utb_involved_role.', 16, 1, @ProcName, @i_VehicleNumber)
                        RETURN
                    END


                    -- If InvolvedTypeClaimant was not passed, we will need to determine if this is a claimant.  To do that, we
                    -- assume that the owner can be the claimant if this is an owner of a 3rd party vehicle (ie. not Vehicle 1)
                    -- and that no other involved for this vehicle has been defined as a claimant.  If there hasn't, then
                    -- add a claimant record
                
                    IF @i_InvolvedTypeClaimant IS NULL AND
                       NOT (@i_VehicleNumber = 1) AND
                       NOT EXISTS (SELECT  cai.InvolvedID 
                                     FROM  dbo.utb_claim_aspect_involved cai
                                     LEFT JOIN dbo.utb_involved_role ir ON (cai.InvolvedID = ir.InvolvedID)
                                     WHERE cai.ClaimAspectID = @iw_ClaimAspectID
                                       AND ir.InvolvedRoleTypeID = @InvolvedRoleTypeIDClaimant)                                     
                    BEGIN
                        -- Insert as claimant
                    
                        INSERT INTO dbo.utb_involved_role
                            (InvolvedID,
                             InvolvedRoleTypeID,
                             SysLastUserID,
                             SysLastUpdatedDate)  
                          VALUES (@iw_InvolvedID, 
                                  @InvolvedRoleTypeIDClaimant,
                                  @UserID, 
                                  @ModifiedDateTime)

                        IF @@ERROR <> 0
                        BEGIN
                            -- Insertion Failure

                            ROLLBACK TRANSACTION
                            RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting "Claimant" into utb_involved_role.', 16, 1, @ProcName, @i_VehicleNumber)
                            RETURN
                        END
                    END
                END

            
                IF @i_InvolvedTypePassenger = 1
                BEGIN
                    INSERT INTO dbo.utb_involved_role
                        (InvolvedID,
                         InvolvedRoleTypeID,
                         SysLastUserID,
                         SysLastUpdatedDate)  
                      VALUES (@iw_InvolvedID, 
                              @InvolvedRoleTypeIDPassenger,
                              @UserID, 
                              @ModifiedDateTime)

                    IF @@ERROR <> 0
                    BEGIN
                        -- Insertion Failure

                        ROLLBACK TRANSACTION
                        RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting "Passenger" into utb_involved_role.', 16, 1, @ProcName, @i_VehicleNumber)
                        RETURN
                    END
                END
            END
        END
        
        
        -- Insert Involved's Doctor information (if existing)

        IF (@i_DoctorAddress1 IS NOT NULL) OR
           (@i_DoctorAddress2 IS NOT NULL) OR
           (@i_DoctorAddressCity IS NOT NULL) OR
           (@i_DoctorAddressState IS NOT NULL) OR
           (@i_DoctorAddressZip IS NOT NULL) OR
           (@i_DoctorName IS NOT NULL) OR
           (@i_DoctorPhone IS NOT NULL)
        BEGIN
            -- Split Doctor Phone into constituent parts

            IF (@i_DoctorPhone IS NOT NULL AND Len(@i_DoctorPhone) = 10 AND IsNumeric(@i_DoctorPhone) = 1)

            BEGIN
                SET @iw_DoctorAreaCode = LEFT(@i_DoctorPhone, 3)
                SET @iw_DoctorExchangeNumber = SUBSTRING(@i_DoctorPhone, 4, 3)
                SET @iw_DoctorUnitNumber = RIGHT(@i_DoctorPhone, 4)
            END
            ELSE
            BEGIN
                SET @iw_DoctorAreaCode = NULL
                SET @iw_DoctorExchangeNumber = NULL
                SET @iw_DoctorUnitNumber = NULL
            END

       	
            -- Insert doctor record in involved
            
            INSERT INTO dbo.utb_involved 
            (
              Address1,
              Address2,
    	      AddressCity,
        	  AddressState,
        	  AddressZip,
              BestContactPhoneCD,
              BusinessName,
    	      DayAreaCode,
    	      DayExchangeNumber,
              DayExtensionNumber,
    	      DayUnitNumber,
              GenderCD,
              SysLastUserId,
		      SysLastUpdatedDate
            )
            VALUES
            (
       	      LTrim(RTrim(@i_DoctorAddress1)),
       	      LTrim(RTrim(@i_DoctorAddress2)),
	   	      LTrim(RTrim(@i_DoctorAddressCity)),
              LTrim(RTrim(@i_DoctorAddressState)),
              LTrim(RTrim(@i_DoctorAddressZip)),
              'D',                -- Default Best Phone to Day
              LTrim(RTrim(@i_DoctorName)),
              @iw_DoctorAreaCode,
              @iw_DoctorExchangeNumber,
              @i_DoctorPhoneExt,
              @iw_DoctorUnitNumber,
              'U',                -- Default Gender to Unknown
       		  @UserID,   
    		  @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting doctor into utb_involved.', 16, 1, @ProcName, @i_VehicleNumber)
                RETURN
            END
    
        
            SET @iw_DoctorInvolvedID = SCOPE_IDENTITY()

        
            -- Associate the Involved and Doctor

            INSERT INTO dbo.utb_involved_relation 
            (
              InvolvedId,
    	        RelatedInvolvedId,
              RelationId,
              SysLastUserId,
		          SysLastUpdatedDate
            )
            VALUES
            (
              @iw_InvolvedId,
    	        @iw_DoctorInvolvedId,
       		    @RelationIDDoctor,
              @UserID,
    		      @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting doctor into utb_involved_relation.', 16, 1, @ProcName, @i_VehicleNumber)
                RETURN
            END
        END        


        -- Insert Involved's Attorney information (if existing)

        IF (@i_AttorneyAddress1 IS NOT NULL) OR
           (@i_AttorneyAddress2 IS NOT NULL) OR
           (@i_AttorneyAddressCity IS NOT NULL) OR
           (@i_AttorneyAddressState IS NOT NULL) OR
           (@i_AttorneyAddressZip IS NOT NULL) OR
           (@i_AttorneyName IS NOT NULL) OR
           (@i_AttorneyPhone IS NOT NULL)
        BEGIN
            -- Split Attorney Phone into constituent parts

            IF (@i_AttorneyPhone IS NOT NULL AND Len(@i_AttorneyPhone) = 10 AND IsNumeric(@i_AttorneyPhone) = 1)
            BEGIN
                SET @iw_AttorneyAreaCode = LEFT(@i_AttorneyPhone, 3)
                SET @iw_AttorneyExchangeNumber = SUBSTRING(@i_AttorneyPhone, 4, 3)
                SET @iw_AttorneyUnitNumber = RIGHT(@i_AttorneyPhone, 4)
            END
            ELSE
            BEGIN
                SET @iw_AttorneyAreaCode = NULL
                SET @iw_AttorneyExchangeNumber = NULL
                SET @iw_AttorneyUnitNumber = NULL
            END
    
   	
            -- Insert Attorney record in involved
            
            INSERT INTO dbo.utb_involved 
            (
              Address1,
              Address2,
    	      AddressCity,
        	  AddressState,
        	  AddressZip,
              BestContactPhoneCD,
              BusinessName,
    	      DayAreaCode,
    	      DayExchangeNumber,
              DayExtensionNumber,
    	      DayUnitNumber,
              GenderCD,
              SysLastUserId,
		      SysLastUpdatedDate
            )
            VALUES
            (
       	      LTrim(RTrim(@i_AttorneyAddress1)),
       	      LTrim(RTrim(@i_AttorneyAddress2)),
	   	      LTrim(RTrim(@i_AttorneyAddressCity)),
              LTrim(RTrim(@i_AttorneyAddressState)),
              LTrim(RTrim(@i_AttorneyAddressZip)),
              'D',                -- Default Best Phone to Day
              LTrim(RTrim(@i_AttorneyName)),
              @iw_AttorneyAreaCode,
              @iw_AttorneyExchangeNumber,
              @i_AttorneyPhoneExt,
              @iw_AttorneyUnitNumber,
              'U',                -- Default Gender to Unknown
       		  @UserID,
    		  @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting attorney into utb_involved.', 16, 1, @ProcName, @i_VehicleNumber)
                RETURN
            END
    
        
            SET @iw_AttorneyInvolvedID = SCOPE_IDENTITY()


            -- Associate the Involved and Attorney

            INSERT INTO dbo.utb_involved_relation 
            (
              InvolvedId,
    	      RelatedInvolvedId,
              RelationId,
              SysLastUserId,
		      SysLastUpdatedDate
            )
            VALUES
            (
              @iw_InvolvedId,
    	      @iw_AttorneyInvolvedId,
       		  @RelationIDAttorney,
              @UserID,
    		  @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting attorney into utb_involved_relation.', 16, 1, @ProcName, @i_VehicleNumber)
                RETURN
            END
        END        


        -- Insert Involved's Employer information (if existing)

        IF (@i_EmployerAddress1 IS NOT NULL) OR
           (@i_EmployerAddress2 IS NOT NULL) OR
           (@i_EmployerAddressCity IS NOT NULL) OR
           (@i_EmployerAddressState IS NOT NULL) OR
           (@i_EmployerAddressZip IS NOT NULL) OR
           (@i_EmployerName IS NOT NULL) OR
           (@i_EmployerPhone IS NOT NULL)
        BEGIN
            -- Split Employer Phone into constituent parts

            IF (@i_EmployerPhone IS NOT NULL AND Len(@i_EmployerPhone) = 10 AND IsNumeric(@i_EmployerPhone) = 1)
            BEGIN
                SET @iw_EmployerAreaCode = LEFT(@i_EmployerPhone, 3)
                SET @iw_EmployerExchangeNumber = SUBSTRING(@i_EmployerPhone, 4, 3)
                SET @iw_EmployerUnitNumber = RIGHT(@i_EmployerPhone, 4)
            END
            ELSE
            BEGIN
                SET @iw_EmployerAreaCode = NULL
                SET @iw_EmployerExchangeNumber = NULL
                SET @iw_EmployerUnitNumber = NULL
            END

       	
            -- Insert employer record in involved
            
            INSERT INTO dbo.utb_involved 
            (
              Address1,
              Address2,
    	      AddressCity,
        	  AddressState,
        	  AddressZip,
              BestContactPhoneCD,
              BusinessName,
    	      DayAreaCode,
    	      DayExchangeNumber,
              DayExtensionNumber,
    	      DayUnitNumber,
              GenderCD,
              SysLastUserId,
		      SysLastUpdatedDate
            )
            VALUES
            (
       	      LTrim(RTrim(@i_EmployerAddress1)),
       	      LTrim(RTrim(@i_EmployerAddress2)),
	   	      LTrim(RTrim(@i_EmployerAddressCity)),
              LTrim(RTrim(@i_EmployerAddressState)),
              LTrim(RTrim(@i_EmployerAddressZip)),
              'D',                -- Default Best Phone to Day
              LTrim(RTrim(@i_EmployerName)),
              @iw_EmployerAreaCode,
              @iw_EmployerExchangeNumber,
              @i_EmployerPhoneExt,
              @iw_EmployerUnitNumber,
              'U',                -- Default Gender to Unknown
       		  @UserID,
    		  @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting employer into utb_involved.', 16, 1, @ProcName, @i_VehicleNumber)
                RETURN
            END

        
            SET @iw_EmployerInvolvedID = SCOPE_IDENTITY()

        
            -- Associate the Involved and Employer

            INSERT INTO dbo.utb_involved_relation 
            (
                InvolvedId,
    	        RelatedInvolvedId,
                RelationId,
                SysLastUserId,
		        SysLastUpdatedDate
            )
            VALUES
            (
                @iw_InvolvedId,
    	        @iw_EmployerInvolvedId,
       		    @RelationIDEmployer,
                @UserID,
    		    @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting employer into utb_involved_relation.', 16, 1, @ProcName, @i_VehicleNumber)
                RETURN
            END
        END        


        -- If this involved was injured, create an injury record

        IF ((@i_Injured = 'Y') AND
            (@i_InjuryTypeID IS NOT NULL OR @i_InjuryDescription IS NOT NULL))
        BEGIN
       	    INSERT INTO dbo.utb_involved_injury
            (
    		    InvolvedID,
    		    InjuryTypeID,
    		    Description,
    		    SysLastUserId,
    		    SysLastUpdatedDate
            )
    	    VALUES	
    	    (
    		    @iw_InvolvedID,
    		    @i_InjuryTypeID,
    		    LTrim(RTrim(@i_InjuryDescription)),
    		    @UserID,
    		    @ModifiedDateTime
    	    )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure

                ROLLBACK TRANSACTION
                RAISERROR('%s: (Involved for Vehicle %u Processing) Error inserting into utb_involved_injury.', 16, 1, @ProcName, @i_VehicleNumber)
                RETURN
            END
        END
                
        
        SET @i_VehicleNumber = NULL
        SET @i_Address1 = NULL
        SET @i_Address2 = NULL
        SET @i_AddressCity  = NULL
        SET @i_AddressState  = NULL
        SET @i_AddressZip  = NULL
        SET @i_Age = NULL
        SET @i_AttorneyAddress1  = NULL
        SET @i_AttorneyAddress2 = NULL
        SET @i_AttorneyAddressCity  = NULL
        SET @i_AttorneyAddressState  = NULL
        SET @i_AttorneyAddressZip  = NULL
        SET @i_AttorneyName  = NULL
        SET @i_AttorneyPhone  = NULL
        SET @i_AttorneyPhoneExt  = NULL
        SET @i_BestPhoneCode  = NULL
        SET @i_BestTimeToCall  = NULL
        SET @i_BusinessName  = NULL
        SET @i_DateOfBirth  = NULL
        SET @i_DoctorAddress1  = NULL
        SET @i_DoctorAddress2  = NULL
        SET @i_DoctorAddressCity  = NULL
        SET @i_DoctorAddressState  = NULL
        SET @i_DoctorAddressZip  = NULL
        SET @i_DoctorName  = NULL
        SET @i_DoctorPhone  = NULL
        SET @i_DoctorPhoneExt  = NULL
        SET @i_DriverLicenseNumber  = NULL
        SET @i_DriverLicenseState  = NULL
        SET @i_EmployerAddress1  = NULL
        SET @i_EmployerAddress2  = NULL
        SET @i_EmployerAddressCity  = NULL
        SET @i_EmployerAddressState  = NULL
        SET @i_EmployerAddressZip  = NULL
        SET @i_EmployerName  = NULL
        SET @i_EmployerPhone  = NULL
        SET @i_EmployerPhoneExt  = NULL
        SET @i_Gender  = NULL
        SET @i_Injured  = NULL
        SET @i_InjuryDescription  = NULL
        SET @i_InjuryTypeId  = NULL
        SET @i_InvolvedTypeClaimant  = NULL
        SET @i_InvolvedTypeDriver  = NULL
        SET @i_InvolvedTypeInsured  = NULL
        SET @i_InvolvedTypeOwner  = NULL
        SET @i_InvolvedTypePassenger  = NULL
        SET @i_InvolvedTypePedestrian  = NULL
        SET @i_InvolvedTypeWitness  = NULL
        SET @i_LocationInVehicleId  = NULL
        SET @i_NameFirst  = NULL
        SET @i_NameLast  = NULL
        SET @i_NameTitle  = NULL
        SET @i_PhoneAlternate  = NULL
        SET @i_PhoneAlternateExt  = NULL
        SET @i_PhoneDay  = NULL
        SET @i_PhoneDayExt  = NULL
        SET @i_PhoneNight  = NULL
        SET @i_PhoneNightExt  = NULL
        SET @i_SeatBelt  = NULL
        SET @i_TaxID  = NULL
        SET @i_ThirdPartyInsuranceName = NULL
        SET @i_ThirdPartyInsurancePhone  = NULL
        SET @i_ThirdPartyInsurancePhoneExt = NULL
        SET @i_Violation = NULL
        SET @i_ViolationDescription = NULL
        SET @i_WitnessLocation = NULL
                 
        
        FETCH NEXT 
          FROM csrFNOLInsertInvolved 
          INTO @i_VehicleNumber,
               @i_Address1,
               @i_Address2,
               @i_AddressCity, 
               @i_AddressState, 
               @i_AddressZip,
               @i_Age, 
               @i_AttorneyAddress1, 
               @i_AttorneyAddress2,
               @i_AttorneyAddressCity, 
               @i_AttorneyAddressState, 
               @i_AttorneyAddressZip, 
               @i_AttorneyName, 
               @i_AttorneyPhone, 
               @i_AttorneyPhoneExt, 
               @i_BestPhoneCode, 
               @i_BestTimeToCall, 
               @i_BusinessName, 
               @i_DateOfBirth, 
               @i_DoctorAddress1, 
               @i_DoctorAddress2, 
               @i_DoctorAddressCity, 
               @i_DoctorAddressState, 
               @i_DoctorAddressZip, 
               @i_DoctorName, 
               @i_DoctorPhone, 
               @i_DoctorPhoneExt, 
               @i_DriverLicenseNumber, 
               @i_DriverLicenseState, 
               @i_EmployerAddress1, 
               @i_EmployerAddress2, 
               @i_EmployerAddressCity, 
               @i_EmployerAddressState, 
               @i_EmployerAddressZip, 
               @i_EmployerName, 
               @i_EmployerPhone, 
               @i_EmployerPhoneExt, 
               @i_Gender, 
               @i_Injured, 
               @i_InjuryDescription, 
               @i_InjuryTypeId, 
               @i_InvolvedTypeClaimant,
               @i_InvolvedTypeDriver, 
               @i_InvolvedTypeInsured, 
               @i_InvolvedTypeOwner, 
               @i_InvolvedTypePassenger, 
               @i_InvolvedTypePedestrian, 
               @i_InvolvedTypeWitness, 
               @i_LocationInVehicleId, 
               @i_NameFirst, 
               @i_NameLast, 
               @i_NameTitle, 
               @i_PhoneAlternate, 
               @i_PhoneAlternateExt, 
               @i_PhoneDay, 
               @i_PhoneDayExt, 
               @i_PhoneNight, 
               @i_PhoneNightExt, 
               @i_SeatBelt, 
               @i_TaxID, 
               @i_ThirdPartyInsuranceName, 
               @i_ThirdPartyInsurancePhone, 
               @i_ThirdPartyInsurancePhoneExt,  
               @i_Violation, 
               @i_ViolationDescription, 
               @i_WitnessLocation        
            
        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            ROLLBACK TRANSACTION
            RAISERROR('%s: (Involved Processing) SQL Server Error fetching next cursor record.', 16, 1, @ProcName)
            RETURN
        END
    END
   
    CLOSE csrFNOLInsertInvolved
    DEALLOCATE csrFNOLINsertInvolved
    
  
    
    /*********************************************************************************************
    **********************************************************************************************
    *   BEGIN PROCESSING FOR CLAIM PROPERTY DATA
    **********************************************************************************************
    *********************************************************************************************/

    DECLARE @p_PropertyNumber           AS udt_std_id
    DECLARE @p_ContactAddress1          AS udt_addr_line_1         
    DECLARE @p_ContactAddress2          AS udt_addr_line_2         
    DECLARE @p_ContactAddressCity       AS udt_addr_city           
    DECLARE @p_ContactAddressState      AS udt_addr_state          
    DECLARE @p_ContactAddressZip        AS udt_addr_zip_code       
    DECLARE @p_ContactNameFirst         AS udt_per_name            
    DECLARE @p_ContactNameLast          AS udt_per_name            
    DECLARE @p_ContactNameTitle         AS udt_per_title           
    DECLARE @p_ContactPhone             AS VARCHAR(15)
    DECLARE @p_ContactPhoneExt          AS udt_ph_extension_number
    DECLARE @p_DamageAmount             AS varchar(20)
    DECLARE @p_DamageDescription        AS udt_std_desc_long
    DECLARE @p_OwnerAddress1            AS udt_addr_line_1
    DECLARE @p_OwnerAddress2            AS udt_addr_line_2 
    DECLARE @p_OwnerAddressCity         AS udt_addr_city
    DECLARE @p_OwnerAddressState        AS udt_addr_state
    DECLARE @p_OwnerAddressZip          AS udt_addr_zip_code
    DECLARE @p_OwnerBusinessName        AS udt_std_name
    DECLARE @p_OwnerNameFirst           AS udt_std_name
    DECLARE @p_OwnerNameLast            AS udt_std_name
    DECLARE @p_OwnerNameTitle           AS udt_per_title
    DECLARE @p_OwnerPhone               AS VARCHAR(15)
    DECLARE @p_OwnerPhoneExt            AS udt_ph_extension_number
    DECLARE @p_OwnerTaxID               AS udt_fed_tax_id
    DECLARE @p_PropertyAddress1         AS udt_addr_line_1
    DECLARE @p_PropertyAddress2         AS udt_addr_line_2 
    DECLARE @p_PropertyAddressCity      AS udt_addr_city
    DECLARE @p_PropertyAddressState     AS udt_addr_state
    DECLARE @p_PropertyAddressZip       AS udt_addr_zip_code
    DECLARE @p_PropertyDescription      AS udt_std_desc_long
    DECLARE @p_PropertyName             AS udt_std_name
    DECLARE @p_Remarks                  AS udt_std_Desc_xlong

    DECLARE @pw_ContactAreaCode         AS udt_ph_area_code
    DECLARE @pw_ContactExchangeNumber   AS udt_ph_exchange_number
    DECLARE @pw_ContactUnitNumber       AS udt_ph_unit_number
    DECLARE @pw_OwnerAreaCode           AS udt_ph_area_code
    DECLARE @pw_OwnerExchangeNumber     AS udt_ph_exchange_number
    DECLARE @pw_OwnerUnitNumber         AS udt_ph_unit_number
    
    DECLARE @pw_ClaimAspectID           AS udt_std_id_big
    DECLARE @pw_BestContactPhone        AS udt_ph_area_code
    DECLARE @pw_ContactInvolvedID       AS udt_std_id_big
    DECLARE @pw_OwnerInvolvedID         AS udt_std_id_big

    
    -- Default BestPhoneCode to day for contact and any involved

    SET @pw_BestContactPhone = 'D'


    -- Declare a cursor to walk through the property load records for this claim

    DECLARE csrFNOLPropertyLoad CURSOR FOR
      SELECT  PropertyNumber,
              ContactAddress1, 
              ContactAddress2, 
              ContactAddressCity, 
              ContactAddressState,
              ContactAddressZip, 
              ContactNameFirst, 
              ContactNameLast, 
              ContactNameTitle,
              ContactPhone, 
              ContactPhoneExt, 
              DamageAmount, 
              DamageDescription, 
              OwnerAddress1, 
              OwnerAddress2, 
              OwnerAddressCity, 
              OwnerAddressState, 
              OwnerAddressZip,
              OwnerBusinessName, 
              OwnerNameFirst, 
              OwnerNameLast, 
              OwnerNameTitle, 
              OwnerPhone,
              OwnerPhoneExt, 
              OwnerTaxId, 
              PropertyAddress1, 
              PropertyAddress2, 
              PropertyAddressCity,
              PropertyAddressState, 
              PropertyAddressZip, 
              PropertyDescription, 
              PropertyName,
              Remarks
        FROM  dbo.utb_fnol_property_load
        WHERE LynxID = @LynxID
            
    OPEN csrFNOLPropertyLoad
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        ROLLBACK TRANSACTION
        RAISERROR('%s: (Property Processing) SQL Server Error opening cursor.', 16, 1, @ProcName)
        RETURN
    END

    
    -- Get the first property
    
    FETCH NEXT 
      FROM csrFNOLPropertyLoad 
      INTO @p_PropertyNumber, 
           @p_ContactAddress1, 
           @p_ContactAddress2, 
           @p_ContactAddressCity, 
           @p_ContactAddressState,
           @p_ContactAddressZip, 
           @p_ContactNameFirst, 
           @p_ContactNameLast, 
           @p_ContactNameTitle,
           @p_ContactPhone, 
           @p_ContactPhoneExt, 
           @p_DamageAmount, 
           @p_DamageDescription,
           @p_OwnerAddress1, 
           @p_OwnerAddress2, 
           @p_OwnerAddressCity,
           @p_OwnerAddressState,
           @p_OwnerAddressZip, 
           @p_OwnerBusinessName, 
           @p_OwnerNameFirst, 
           @p_OwnerNameLast, 
           @p_OwnerNameTitle,
           @p_OwnerPhone, 
           @p_OwnerPhoneExt, 
           @p_OwnerTaxId, 
           @p_PropertyAddress1, 
           @p_PropertyAddress2,
           @p_PropertyAddressCity, 
           @p_PropertyAddressState, 
           @p_PropertyAddressZip, 
           @p_PropertyDescription,
           @p_PropertyName,  
           @p_Remarks

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        ROLLBACK TRANSACTION
        RAISERROR('%s: (Property Processing) SQL Server Error fetching first cursor record.', 16, 1, @ProcName)
        RETURN
    END

        
    WHILE @@Fetch_Status = 0
    BEGIN
        --Create a claim aspect record for the property
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	Revisit this with Jorge/Jonathan regarding the inserts and the updates
			M.A. 20061122
	*********************************************************************************/        
        INSERT INTO dbo.utb_claim_aspect 
                 (ClaimAspectTypeID,
                  --CreatedUserID, --Project:210474 APD Remarked-off the column when we did the code merge M.A.20061120
                  LynxID,
                  SourceApplicationID,
                  ClaimAspectNumber,
                  CoverageProfileCD,
                  CreatedDate,
                  EnabledFlag, 
                  ExposureCD,
                  PriorityFlag,
                  SysLastUserID,
                  SysLastUpdatedDate)
          VALUES (@ClaimAspectTypeIDProperty, 
                  --@UserID,
                  @LynxID,
                  @ApplicationID, 
                  @p_PropertyNumber,
                  'LIAB',
                  @ModifiedDateTime,
                  1,        -- Set property to enabled
                  '3', 
                  0,        -- Priority Flag off
                  @UserID, 
                  @ModifiedDateTime)
            
        IF @@ERROR <> 0
        BEGIN
            -- Insertion Failure
    
            ROLLBACK TRANSACTION
            RAISERROR('%s: (Property %u Processing) Error inserting into utb_claim_aspect.', 16, 1, @ProcName, @p_PropertyNumber)
            RETURN
        END

        SET @pw_ClaimAspectID = SCOPE_IDENTITY()
            

        -- Save in temporary table so we can properly handle workflow later
        
        INSERT INTO @tmpAddedClaimAspects
          VALUES (@pw_ClaimAspectID, @ClaimAspectTypeIDProperty)
        

        -- If any of regular contact fields are populated, Insert the contact as an involved
        -- Otherwise skip

        IF (@p_ContactAddress1 IS NOT NULL) OR
           (@p_ContactAddress2 IS NOT NULL) OR
           (@p_ContactAddressCity IS NOT NULL) OR
           (@p_ContactAddressState IS NOT NULL) OR
           (@p_ContactAddressZip IS NOT NULL) OR
           (@p_ContactNameFirst IS NOT NULL) OR
           (@p_ContactNameLast IS NOT NULL) OR
           (@p_ContactPhone IS NOT NULL)
        BEGIN
            -- Split Contact Phone into constituent parts 

            IF (@p_ContactPhone IS NOT NULL AND Len(@p_ContactPhone) = 10 AND IsNumeric(@p_ContactPhone) = 1)
            BEGIN
                SET @pw_ContactAreaCode = LEFT(@p_ContactPhone, 3)
                SET @pw_ContactExchangeNumber = SUBSTRING(@p_ContactPhone, 4, 3)
                SET @pw_ContactUnitNumber = RIGHT(@p_ContactPhone, 4)
            END
            ELSE
            BEGIN
                SET @pw_ContactAreaCode = NULL
                SET @pw_ContactExchangeNumber = NULL
                SET @pw_ContactUnitNumber = NULL
            END

            
      	    INSERT INTO dbo.utb_involved	
            (
  	            Address1,
  	            Address2,
   	            AddressCity,
	            AddressState,
	            AddressZip,
                BestContactPhoneCD,
                DayAreaCode,
                DayExchangeNumber,
                DayExtensionNumber,
                DayUnitNumber,
                GenderCD,
                NameFirst,
	            NameLast,
                NameTitle,
	            SysLastUserId,
	            SysLastUpdatedDate
            )
	          VALUES	
	          (
    	          LTrim(RTrim(@p_ContactAddress1)),
    	          LTrim(RTrim(@p_ContactAddress2)),
    	          LTrim(RTrim(@p_ContactAddressCity)),
    	          LTrim(RTrim(@p_ContactAddressState)),
	              LTrim(RTrim(@p_ContactAddressZip)),
                  LTrim(RTrim(@pw_BestContactPhone)),
                  @pw_ContactAreaCode,
                  @pw_ContactExchangeNumber,
                  @p_ContactPhoneExt,
                  @pw_ContactUnitNumber,
                  @Gender,
    	          LTrim(RTrim(@p_ContactNameFirst)),
	              LTrim(RTrim(@p_ContactNameLast)),
                  LTrim(RTrim(@p_ContactNameTitle)),
		          @UserID,
		          @ModifiedDateTime
              )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
    
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Property %u Processing) Error inserting contact into utb_involved.', 16, 1, @ProcName, @p_PropertyNumber)
                RETURN
            END
       
            SET @pw_ContactInvolvedID = SCOPE_IDENTITY()
        END
        ELSE
        BEGIN
            SET @pw_ContactInvolvedId = NULL
        END


        -- Create the new property damage record for this claim

        INSERT INTO dbo.utb_claim_property
   	    (
            ClaimAspectID,
            ContactInvolvedId,
            DamageAmt,
            DamageDescription,
            LocationAddress1,
            LocationAddress2,
            LocationCity,
            LocationState,
            LocationZip,
            Name,
            PropertyDescription,
            Remarks,
            SysLastUserID,
            SysLastUpdatedDate
        )
        VALUES	
        (
            @pw_ClaimAspectID,
            @pw_ContactInvolvedId,
            Convert(decimal(9,2), @p_DamageAmount),
            LTrim(RTrim(@p_DamageDescription)),
            LTrim(RTrim(@p_PropertyAddress1)),
            LTrim(RTrim(@p_PropertyAddress2)),
            LTrim(RTrim(@p_PropertyAddressCity)),
            LTrim(RTrim(@p_PropertyAddressState)),
            LTrim(RTrim(@p_PropertyAddressZip)),
            LTrim(RTrim(@p_PropertyName)),
            LTrim(RTrim(@p_PropertyDescription)),
            LTrim(RTrim(@p_Remarks)),
            @UserID,
            @ModifiedDatetime
        )

        IF @@ERROR <> 0
        BEGIN
            -- Insertion Failure
    
            ROLLBACK TRANSACTION
            RAISERROR('%s: (Property %u Processing) Error inserting into utb_claim_property.', 16, 1, @ProcName, @p_PropertyNumber)
            RETURN
        END

    
        -- If any of owner fields are populated, Insert the owner as an involved
        -- Otherwise skip
    
        IF (@p_OwnerAddress1 IS NOT NULL) OR
           (@p_OwnerAddress2 IS NOT NULL) OR
           (@p_OwnerAddressCity IS NOT NULL) OR
           (@p_OwnerAddressState IS NOT NULL) OR
           (@p_OwnerAddressZip IS NOT NULL) OR
           (@p_OwnerBusinessName IS NOT NULL) OR
           (@p_OwnerNameFirst IS NOT NULL) OR
           (@p_OwnerNameLast IS NOT NULL) OR
           (@p_OwnerPhone IS NOT NULL) OR
           (@p_OwnerTaxId IS NOT NULL)
        BEGIN
            -- Split Owner Phone into constituent parts 

            IF (@p_OwnerPhone IS NOT NULL AND Len(@p_OwnerPhone) = 10 AND IsNumeric(@p_OwnerPhone) = 1)
            BEGIN
                SET @pw_OwnerAreaCode = LEFT(@p_OwnerPhone, 3)
                SET @pw_OwnerExchangeNumber = SUBSTRING(@p_OwnerPhone, 4, 3)
                SET @pw_OwnerUnitNumber = RIGHT(@p_OwnerPhone, 4)
            END
            ELSE
            BEGIN
                SET @pw_OwnerAreaCode = NULL
                SET @pw_OwnerExchangeNumber = NULL
                SET @pw_OwnerUnitNumber = NULL
            END


            -- Create the new involved record for this property

    	    INSERT INTO dbo.utb_involved	
            (
		        Address1,
                Address2,
		        AddressCity,
		        AddressState,
		        AddressZip,
                BestContactPhoneCD,
                BusinessName,
                DayAreaCode,
                DayExchangeNumber,
                DayExtensionNumber,
                DayUnitNumber,
                FedTaxId,
                GenderCD,
		        NameFirst,
		        NameLast,
                NameTitle,
		        SysLastUserId,
		        SysLastUpdatedDate
            )
	        VALUES	
	        (
    		    LTrim(RTrim(@p_OwnerAddress1)),
                LTrim(RTrim(@p_OwnerAddress2)),
    		    LTrim(RTrim(@p_OwnerAddressCity)),
    		    LTrim(RTrim(@p_OwnerAddressState)),
		        LTrim(RTrim(@p_OwnerAddressZip)),
                LTrim(RTrim(@pw_BestContactPhone)),
                LTrim(RTrim(@p_OwnerBusinessName)),
                @pw_OwnerAreaCode,
                @pw_OwnerExchangeNumber,
                @p_OwnerPhoneExt,
                @pw_OwnerUnitNumber,
                LTrim(RTrim(@p_OwnerTaxId)),
     		    @Gender,
    		    LTrim(RTrim(@p_OwnerNameFirst)),
       	        LTrim(RTrim(@p_OwnerNameLast)),
                LTrim(RTrim(@p_OwnerNameTitle)),
		        @UserID,
		        @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
    
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Property %u Processing) Error inserting owner into utb_involved.', 16, 1, @ProcName, @p_PropertyNumber)
                RETURN
            END
    
            SET @pw_OwnerInvolvedID = SCOPE_IDENTITY()


            -- Attach the owner involved to the property
     
            INSERT INTO dbo.utb_claim_aspect_involved
            (
                ClaimAspectID,
                InvolvedId,
                EnabledFlag,
                SysLastUserId,
                SysLastUpdatedDate
            )
            VALUES
            (   
                @pw_ClaimAspectID,
                @pw_OwnerInvolvedID,
                1,              -- Set property involved to enabled
                @UserID,
                @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
    
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Property %u Processing) Error inserting owner into utb_claim_aspect_involved.', 16, 1, @ProcName, @p_PropertyNumber)
                RETURN
            END
    
        
            -- Associate involved persons as involved type 'owner'

            INSERT INTO dbo.utb_involved_role
     	    (
                InvolvedID,
                InvolvedRoleTypeID,
                SysLastUserId,
                SysLastUpdatedDate
            )
            VALUES
            (
                @pw_OwnerInvolvedID,
                @InvolvedRoleTypeIDOwner,
                @UserID,
                @ModifiedDateTime
            )

            IF @@ERROR <> 0
            BEGIN
                -- Insertion Failure
    
                ROLLBACK TRANSACTION
                RAISERROR('%s: (Property %u Processing) Error inserting "Owner" into utb_involved_role.', 16, 1, @ProcName, @p_PropertyNumber)
                RETURN
            END
        END


        -- Get the next property
    
        FETCH NEXT 
          FROM csrFNOLPropertyLoad 
          INTO @p_PropertyNumber, 
               @p_ContactAddress1, 
               @p_ContactAddress2, 
               @p_ContactAddressCity, 
               @p_ContactAddressState,
               @p_ContactAddressZip, 
               @p_ContactNameFirst, 
               @p_ContactNameLast, 
               @p_ContactNameTitle,
               @p_ContactPhone, 
               @p_ContactPhoneExt, 
               @p_DamageAmount, 
               @p_DamageDescription,
               @p_OwnerAddress1, 
               @p_OwnerAddress2, 
               @p_OwnerAddressCity,
               @p_OwnerAddressState,
               @p_OwnerAddressZip, 
               @p_OwnerBusinessName, 
               @p_OwnerNameFirst, 
               @p_OwnerNameLast, 
               @p_OwnerNameTitle,
               @p_OwnerPhone, 
               @p_OwnerPhoneExt, 
               @p_OwnerTaxId, 
               @p_PropertyAddress1, 
               @p_PropertyAddress2,
               @p_PropertyAddressCity, 
               @p_PropertyAddressState, 
               @p_PropertyAddressZip, 
               @p_PropertyDescription,
               @p_PropertyName,  
               @p_Remarks    

        IF @@ERROR <> 0
        BEGIN
            -- SQL Server Error

            ROLLBACK TRANSACTION
            RAISERROR('%s: (Property Processing) SQL Server Error fetching next cursor record.', 16, 1, @ProcName)
            RETURN
        END
    END
    
    CLOSE csrFNOLPropertyLoad
    DEALLOCATE csrFNOLPropertyLoad



    /*********************************************************************************************
    **********************************************************************************************
    *   BEGIN PROCESSING FNOL WORKFLOW
    **********************************************************************************************
    *********************************************************************************************/

------------------------------------
-- 20Nov2015 - TVD
-- Choice CS will not fire workflow
-- so skip this code.
------------------------------------
--IF (@vw_ServiceChannelCDWork <> 'CS')
--BEGIN
    -- Declare internal variables
    DECLARE @w_ApplicationName              AS udt_std_name
    DECLARE @w_AppraiserID                  AS udt_std_id
    DECLARE @w_AppraiserName                AS udt_std_name
    --DECLARE @w_AssignedRep          AS udt_std_id_big
    --DECLARE @w_AssignedRepName      AS udt_per_name
    DECLARE @w_CarrierUsesCEIFlag           AS udt_std_name
    DECLARE @w_ClaimAspectID                AS udt_std_id_big
    DECLARE @w_ClaimAspectIDClaim           AS udt_std_id_big
    DECLARE @w_ClaimAspectIDVehicle         AS udt_std_id_big
    DECLARE @w_ClaimAspectNumber            AS udt_std_int
    DECLARE @w_ClaimAspectServiceChannelID  AS udt_std_id_big    
    DECLARE @w_ClaimAspectTypeID            AS udt_std_id
    DECLARE @w_EventID                      AS udt_std_id
    DECLARE @w_EventName                    AS udt_std_name
    DECLARE @w_EventConditionValue          AS udt_std_desc_short
    DECLARE @w_ExposureCount                AS udt_std_int_tiny
    DECLARE @w_HistoryDescription           AS udt_std_desc_long
    DECLARE @w_PrimaryFlag                  AS udt_std_flag
    DECLARE @w_ReactivatedFlag              AS udt_std_flag
    DECLARE @w_ProgramTypeCD                AS udt_std_cd
    DECLARE @w_ServiceChannelCD             AS udt_std_cd
    DECLARE @w_ShopLocationID               AS udt_std_id
    DECLARE @w_ShopLocationName             AS udt_std_name
    DECLARE @w_StatusID                     AS udt_std_id
    DECLARE @w_StatusIDClaim                AS udt_std_id
    DECLARE @w_AssignedOwner                AS udt_std_id_big
    DECLARE @w_AssignedOwnerName            AS udt_per_name
    DECLARE @w_AssignedAnalyst              AS udt_std_id_big
    DECLARE @w_AssignedAnalystName          AS udt_per_name
    DECLARE @w_AssignedSupport              AS udt_std_id_big
    DECLARE @w_AssignedSupportName          AS udt_per_name

    
    -- Get the application that submitted this claim
    
    SELECT  @w_ApplicationName = Name
      FROM  dbo.utb_application
      WHERE ApplicationID = @ApplicationID
      
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        ROLLBACK TRANSACTION
        RAISERROR('%s: (Workflow Processing) SQL Server Error getting Application Name.', 16, 1, @ProcName)
        RETURN
    END
   
	------------------
	-- Debugging 
	------------------
	IF @Debug = 1
	BEGIN
        PRINT 'Added Claim Aspects for workflow'
        SELECT * FROM @tmpAddedClaimAspects
	END
	ELSE IF @Debug = 2
	BEGIN
		SET @DebugMsg = 'Added Claim Aspects for workflow'

		SET @EventSeq += 1

		SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
		SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
		SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(CLAIM_ASPECT_WORKFLOW)'''
		SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
		SET @DebugCmd += ', @vEventStatus = ''CLAIM_ASPECT_WORKFLOW'''
		SET @DebugCmd += ', @vEventDescription = ''====> Process Added Claim Aspects for workflow: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
		SET @DebugCmd += ', @vEventDetailedDescription = ''Process Added Claim Aspects for workflow'''
		SET @DebugCmd += ', @vEventXML = ''CLAIM_ASPECT_WORKFLOW' + @DebugMsg + ''''
		SET @DebugCmd += ', @iRecID = 0'

		--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
		EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
	END
    
    -- New walktrhough for workflow
    DECLARE csrAddedAspects CURSOR FOR
      SELECT tmp.ClaimAspectID,
             tmp.ClaimAspectTypeID,
             ca.ClaimAspectNumber
        FROM @tmpAddedClaimAspects tmp
        INNER JOIN utb_claim_aspect ca ON (tmp.ClaimAspectID = ca.ClaimAspectID)
        WHERE (ca.ExposureCD IS NULL) or (ca.ExposureCD <> 'N')
        ORDER BY tmp.ClaimAspectID                          


    OPEN csrAddedAspects
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        ROLLBACK TRANSACTION
        RAISERROR('%s: (Workflow Processing) SQL Server Error opening workflow cursor.', 16, 1, @ProcName)
        RETURN
    END

    FETCH NEXT FROM csrAddedAspects
    INTO @w_ClaimAspectID, @w_ClaimAspectTypeID, @w_ClaimAspectNumber
    
    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error

        ROLLBACK TRANSACTION
        RAISERROR('%s: (Workflow Processing) SQL Server Error fetching workflow cursor record.', 16, 1, @ProcName)
        RETURN
    END

        
    WHILE @@Fetch_Status = 0
    BEGIN
		------------------------------------
		-- Begin - Assigning User to Claim      
		------------------------------------
		IF @w_ClaimAspectTypeID IN (@ClaimAspectTypeIDVehicle, @ClaimAspectTypeIDProperty)
		BEGIN
			IF (@vw_ServiceChannelCDWork = 'CS')
			BEGIN
				-- Assign Owner
				UPDATE utb_claim_aspect SET OwnerUserID = @c_CarrierRepUserID WHERE @w_ClaimAspectTypeID = 9 AND ClaimAspectID = @w_ClaimAspectID
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Choice User Assignment) Error assigning claim aspect from FNOL Load.', 16, 1, @ProcName)
					RETURN
				END
				
				SET @w_AssignedOwner = @c_CarrierRepUserID

				-- Assign Support
				DECLARE @c_SupportUserID INT
				
				IF EXISTS (SELECT UserID FROM utb_user WHERE EmailAddress = 'customerservice@hyperquest.com')
				BEGIN
					SELECT @c_SupportUserID=UserID FROM utb_user WHERE EmailAddress = 'customerservice@hyperquest.com'
					
					UPDATE utb_claim_aspect SET SupportUserID = @c_SupportUserID WHERE @w_ClaimAspectTypeID = 9 AND ClaimAspectID = @w_ClaimAspectID
					IF @@ERROR <> 0
					BEGIN
						-- Error executing procedure

						ROLLBACK TRANSACTION
						RAISERROR('%s: (Choice User Support Assignment) Error assigning claim aspect from FNOL Load.', 16, 1, @ProcName)
						RETURN
					END
					
					SET @w_AssignedSupport = @c_SupportUserID
				END
			END
			ELSE
			BEGIN				
				EXEC uspFNOLAssignClaim @ClaimAspectID = @w_ClaimAspectID,
								   @UserID = @UserID,
	 							   @OwnerUserID = @w_AssignedOwner output,
								   @AnalystUserID = @w_AssignedAnalyst output,
								   @SupportUserID = @w_AssignedSupport output   -- FNOLUserID passed from calling App (i.e. ClaimPoint).
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing) Error assigning claim aspect from FNOL.', 16, 1, @ProcName)
					RETURN
				END
			END
			
			IF (@w_AssignedOwner IS NULL) AND (@w_AssignedAnalyst IS NULL) AND (@w_AssignedSupport IS NULL)
			BEGIN
				ROLLBACK TRANSACTION
				RAISERROR('%s: (Workflow Processing) Error assigning claim aspect from FNOL. All Three Functions are null.',16,1,@ProcName)
				RETURN
			END

			-- Get the rep name so that we can include it in the event notification
			IF NOT @w_AssignedOwner IS NULL
			BEGIN
				SELECT  @w_AssignedOwnerName = NameFirst + ' ' + Namelast 
				  FROM  dbo.utb_user 
				  WHERE UserID = @w_AssignedOwner

				IF @@ERROR <> 0
				BEGIN
					-- SQL Server Error
               
					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing) SQL Server Error getting Assigned Owner Name', 16, 1)
					RETURN
				END
			END

			IF NOT @w_AssignedAnalyst IS NULL
			BEGIN
				SELECT  @w_AssignedAnalystName = NameFirst + ' ' + Namelast 
				  FROM  dbo.utb_user 
				  WHERE UserID = @w_AssignedAnalyst

				IF @@ERROR <> 0
				BEGIN
					-- SQL Server Error
               
					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing) SQL Server Error getting Assigned Analyst Name', 16, 1)
					RETURN
				END
			END
			                       
			IF NOT @w_AssignedSupport IS NULL
			BEGIN
				SELECT  @w_AssignedSupportName = NameFirst + ' ' + Namelast 
				  FROM  dbo.utb_user 
				  WHERE UserID = @w_AssignedSupport

				IF @@ERROR <> 0
				BEGIN
					-- SQL Server Error
               
					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing) SQL Server Error getting Assigned Support Name', 16, 1)
					RETURN
				END
			END
		END
		------------------------------------
		-- End - Assigning User to Claim      
		------------------------------------

		------------------
		-- Debugging 
		------------------
		IF @Debug = 1
		BEGIN
            PRINT ''
			PRINT 'User Assignment:'
			PRINT '    @c_CarrierRepUserID = ' + Convert(varchar(10), @c_CarrierRepUserID)
			PRINT '    @w_AssignedOwner = ' + Convert(varchar(10), @w_AssignedOwner)
			PRINT '    @w_AssignedOwnerName = ' + @w_AssignedOwnerName
			PRINT '    @w_AssignedAnalyst = ' + Convert(varchar(10), @w_AssignedAnalyst)
			PRINT '    @w_AssignedAnalystName = ' + @w_AssignedAnalystName
			PRINT '    @w_AssignedSupport = ' + Convert(varchar(10), @w_AssignedSupport)
			PRINT '    @w_AssignedSupportName = ' + @w_AssignedSupportName
		END
		ELSE IF @Debug = 2
		BEGIN
			SET @DebugMsg = ''
			SET @DebugMsg += 'User Assignment:'
			SET @DebugMsg += '    @c_CarrierRepUserID = ' + Convert(varchar(10), ISNULL(@c_CarrierRepUserID,0))
			SET @DebugMsg += '    @w_AssignedOwner = ' + Convert(varchar(10), ISNULL(@w_AssignedOwner,0))
			SET @DebugMsg += '    @w_AssignedOwnerName = ' + ISNULL(@w_AssignedOwnerName,'')
			SET @DebugMsg += '    @w_AssignedAnalyst = ' + Convert(varchar(10), ISNULL(@w_AssignedAnalyst,0))
			SET @DebugMsg += '    @w_AssignedAnalystName = ' + ISNULL(@w_AssignedAnalystName,'')
			SET @DebugMsg += '    @w_AssignedSupport = ' + Convert(varchar(10), ISNULL(@w_AssignedSupport,0))
			SET @DebugMsg += '    @w_AssignedSupportName = ' + ISNULL(@w_AssignedSupportName,'')

			SET @EventSeq += 1

			SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
			SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
			SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(USER_ASSIGN)'''
			SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
			SET @DebugCmd += ', @vEventStatus = ''USER_ASSIGN'''
			SET @DebugCmd += ', @vEventDescription = ''====> Processing User Assignment: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
			SET @DebugCmd += ', @vEventDetailedDescription = ''User Assignment.  Param data in EventXML'''
			SET @DebugCmd += ', @vEventXML = ''USER_ASSIGN=' + @DebugMsg + ''''
			SET @DebugCmd += ', @iRecID = 0'

			--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
			EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
		END

		------------------------------------
		-- 30Nov2015 - TVD - Bypass Workflow
		-- for CS Choice Shops
		------------------------------------
		IF (@vw_ServiceChannelCDWork <> 'CS')
		BEGIN
			------------------------------------
			-- Begin - Real workflow starts here      
			------------------------------------
			-- If this is a new vehicle or property on an existing claim, validate the current status of the claim.  
			-- If it is closed, we need to reopen it
	        
			--------------------------------------------
			-- Begin - Check Current Status and ReOpen
			--------------------------------------------
			IF  @NewClaimFlag = 0 AND 
				@w_ClaimAspectTypeID IN (@ClaimAspectTypeIDVehicle, @ClaimAspectTypeIDProperty)
			BEGIN
				-- Check claim status
	            
				SELECT  @w_ClaimAspectIDClaim = ca.ClaimAspectID,
						@w_StatusIDClaim = cas.StatusID
				  FROM  dbo.utb_claim_aspect ca
				  INNER JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
				  WHERE ca.LynxID = @LynxID 
					AND ca.ClaimAspectTypeID = @ClaimAspectTypeIDClaim
	                
				IF @@ERROR <> 0
				BEGIN
					-- SQL Server Error
	               
					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing) SQL Server Error getting Claim Status', 16, 1)
					RETURN
				END
	            
	            
				IF @w_StatusIDClaim = @StatusIDClaimClosed
				BEGIN
					-- Send APD workflow the "Claim Reopened" event
	                
					SET @w_EventID = @EventIDClaimReopened
					SET @w_EventName = 'Claim Reopened'
					SET @w_HistoryDescription = 'Claim reopened, new exposure being added to APD'
	                
					------------------
					-- Debugging 
					------------------
					IF @Debug = 1
					BEGIN
						PRINT ''
						PRINT 'Call to uspWorkflowNotifyEvent for CHOICE claim reopen:'
						PRINT '     @EventID = ' + convert(varchar(10), @w_EventID)
						PRINT '     @ClaimAspectID = ' + convert(varchar(10), @w_ClaimAspectIDClaim)
						PRINT '     @Description = ' + @w_HistoryDescription
						PRINT '     @UserID = ' + convert(varchar(10), @UserID)        
					END
					ELSE IF @Debug = 2
					BEGIN
						SET @DebugMsg = ''
						SET @DebugMsg += 'Call to uspWorkflowNotifyEvent for CHOICE claim reopen:'
						SET @DebugMsg += '     @EventID = ' + convert(varchar(10), ISNULL(@w_EventID,0))
						SET @DebugMsg += '     @ClaimAspectID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectIDClaim,0))
						SET @DebugMsg += '     @Description = ' + ISNULL(@w_HistoryDescription,'')
						SET @DebugMsg += '     @UserID = ' + convert(varchar(10), ISNULL(@UserID,0))        

						SET @EventSeq += 1

						SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
						SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
						SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
						SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
						SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
						SET @DebugCmd += ', @vEventDescription = ''====> Processing *** uspWorkflowNotifyEvent *** for CHOICE claim reopen: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
						SET @DebugCmd += ', @vEventDetailedDescription = ''EXEC uspWorkflowNotifyEvent for CHOICE claim reopen.  Param data in EventXML'''
						SET @DebugCmd += ', @vEventXML = ''EXEC_SP=' + @DebugMsg + ''''
						SET @DebugCmd += ', @iRecID = 0'

						--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
						EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					END
	    
					EXEC uspWorkflowNotifyEvent @EventID = @w_EventID,
												@ClaimAspectID = @w_ClaimAspectIDClaim,
												@Description = @w_HistoryDescription,
												@UserID = @UserID

					-- Check error value

					IF @@ERROR <> 0
					BEGIN
						-- Error executing procedure

						ROLLBACK TRANSACTION
						RAISERROR('%s: (Workflow Processing) Error notifying APD of the event "%s"', 16, 1, @ProcName, @w_EventName)
						RETURN
					END
				END
			END
			--------------------------------------------
			-- End - Check Current Status and ReOpen
			--------------------------------------------
	         
			---------------------------------------------
			-- Begin - Prep to fire WorkflowNotifyEvent      
			-- Claim Aspect Creation
			---------------------------------------------
			SELECT @w_EventID            = CASE @w_ClaimAspectTypeID
											 WHEN @ClaimAspectTypeIDClaim THEN @EventIDClaimCreated
											 WHEN @ClaimAspectTypeIDProperty THEN @EventIDPropertyCreated
											 ELSE @EventIDVehicleCreated
										   END,
				   @w_EventName          = CASE @w_ClaimAspectTypeID
											 WHEN @ClaimAspectTypeIDClaim THEN 'Claim Created'
											 WHEN @ClaimAspectTypeIDProperty THEN 'Property Created'
											 ELSE 'Vehicle Created'
										   END,
				   @w_StatusID          =  CASE @w_ClaimAspectTypeID
											  WHEN @ClaimAspectTypeIDClaim THEN @StatusIDClaimOpen
											  ELSE @StatusIDVehicleOpen
										   END,                                       
				   @w_HistoryDescription = CASE
											 WHEN @w_ClaimAspectTypeID = @ClaimAspectTypeIDClaim AND (SELECT DemoFlag FROM dbo.utb_claim WHERE LynxID = @LynxID) = 1
													THEN 'Demo Claim Transferred to APD from ' + @w_ApplicationName + ', Status Assigned to "Open"'
											 WHEN @w_ClaimAspectTypeID = @ClaimAspectTypeIDClaim THEN 'Claim Transferred to APD from ' + @w_ApplicationName + ', Status Assigned to "Open"'
											 WHEN @w_ClaimAspectTypeID = @ClaimAspectTypeIDProperty THEN 'Property ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' created, Status Assigned to "Open"'    
											 ELSE 'Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' created, Status Assigned to "Open"'
											END,
				   @w_EventConditionValue = null

			------------------
			-- Debugging 
			------------------
			IF @Debug = 1
			BEGIN
                PRINT ''
				PRINT 'Call to uspWorkflowNotifyEvent for CHOICE claim aspect creation:'
				PRINT '     @EventID = ' + convert(varchar(10), @w_EventID)
				PRINT '     @ClaimAspectID = ' + convert(varchar(10), @w_ClaimAspectID)
				PRINT '     @Description = ' + @w_HistoryDescription
				PRINT '     @UserID = ' + convert(varchar(10), @UserID)        
				PRINT '     @ConditionValue = ' + convert(varchar(10), @w_EventConditionValue)        
			END
			ELSE IF @Debug = 2
			BEGIN
				SET @DebugMsg = ''
				SET @DebugMsg += 'Call to uspWorkflowNotifyEvent for CHOICE claim aspect creation:'
				SET @DebugMsg += '     @EventID = ' + convert(varchar(10), ISNULL(@w_EventID,0))
				SET @DebugMsg += '     @ClaimAspectID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectID,0))
				SET @DebugMsg += '     @Description = ' + ISNULL(@w_HistoryDescription,'')
				SET @DebugMsg += '     @UserID = ' + convert(varchar(10), ISNULL(@UserID,0))        
				SET @DebugMsg += '     @ConditionValue = ' + convert(varchar(10), ISNULL(@w_EventConditionValue,0))        

				SET @EventSeq += 1

				SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
				SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
				SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
				SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
				SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
				SET @DebugCmd += ', @vEventDescription = ''====> Processing Call to *** uspWorkflowNotifyEvent *** for CHOICE claim aspect creation: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
				SET @DebugCmd += ', @vEventDetailedDescription = ''EXEC Call to uspWorkflowNotifyEvent for CHOICE claim aspect creation.  Param data in EventXML'''
				SET @DebugCmd += ', @vEventXML = ''EXEC_SP=' + @DebugMsg + ''''
				SET @DebugCmd += ', @iRecID = 0'

				--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
			END	        
	   
			IF @@ERROR <> 0
			BEGIN
				-- Error executing procedure

				ROLLBACK TRANSACTION
				RAISERROR('%s: (Workflow Processing) Error updating status of aspect', 16, 1, @ProcName)
				RETURN
			END 
	       
			-- Notification of Claim Aspect Creation
			EXEC uspWorkflowNotifyEvent @EventID = @w_EventID,
										@ClaimAspectID = @w_ClaimAspectID,
										@Description = @w_HistoryDescription,
										@UserID = @UserID,
										@ConditionValue = @w_EventConditionValue
	                                   
			-- Check error value

			IF @@ERROR <> 0
			BEGIN
				-- Error executing procedure

				ROLLBACK TRANSACTION
				RAISERROR('%s: (Workflow Processing) Error notifying APD of the event "%s"', 16, 1, @ProcName, @w_EventName)
				RETURN
			END 
			---------------------------------------------
			-- End - Prep to fire WorkflowNotifyEvent      
			-- Claim Aspect Creation
			---------------------------------------------

			---------------------------------------------
			-- Begin - Prep to fire WorkflowNotifyEvent      
			-- Claim Assignments
			---------------------------------------------
			-- Now notify APD workflow that the claim aspect has been assigned to someone
			IF @w_ClaimAspectTypeID IN (@ClaimAspectTypeIDVehicle, @ClaimAspectTypeIDProperty)
			BEGIN
				---------------------------------------------
				-- Begin - Prep to fire WorkflowNotifyEvent      
				-- Claim Assigned - Owner
				---------------------------------------------
				IF @w_AssignedOwner IS NOT NULL
				BEGIN   
					SELECT @w_EventID            = CASE @w_ClaimAspectTypeID
													 --WHEN @ClaimAspectTypeIDClaim THEN @EventIDClaimOwnershipTransfer
													 WHEN @ClaimAspectTypeIDProperty THEN @EventIDPropertyAssigned
													 ELSE @EventIDVehicleOwnerAssigned
												   END,
						   @w_EventName          = CASE @w_ClaimAspectTypeID
													 --WHEN @ClaimAspectTypeIDClaim THEN 'Claim Ownership Transfer'
													 WHEN @ClaimAspectTypeIDProperty THEN 'Property Assigned'
													 ELSE 'Vehicle Owner Assigned'
												   END,
						   @w_HistoryDescription = CASE
													 --WHEN @w_ClaimAspectTypeID = @ClaimAspectTypeIDClaim THEN 'Claim Ownership Transferred to ' + @w_AssignedRepName
													 WHEN @w_ClaimAspectTypeID = @ClaimAspectTypeIDProperty THEN 'Property ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' assigned to '  + @w_AssignedOwnerName    
													 ELSE 'Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' assigned to '  + @w_AssignedOwnerName + ' as handler.'
												   END
	                                         
					------------------
					-- Debugging 
					------------------
					IF @Debug = 1
					BEGIN
						PRINT ''
						PRINT 'Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user:'
						PRINT '     @EventID = ' + convert(varchar(10), @w_EventID)
						PRINT '     @ClaimAspectID = ' + convert(varchar(10), @w_ClaimAspectID)
						PRINT '     @Description = ' + @w_HistoryDescription
						PRINT '     @UserID = ' + convert(varchar(10), @UserID)        
					END
					ELSE IF @Debug = 2
					BEGIN
						SET @DebugMsg = ''
						SET @DebugMsg += 'Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user:'
						SET @DebugMsg += '     @EventID = ' + convert(varchar(10), ISNULL(@w_EventID,0))
						SET @DebugMsg += '     @ClaimAspectID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectID,0))
						SET @DebugMsg += '     @Description = ' + ISNULL(@w_HistoryDescription,'')
						SET @DebugMsg += '     @UserID = ' + convert(varchar(10), ISNULL(@UserID,0))        

						SET @EventSeq += 1

						SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
						SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
						SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
						SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
						SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
						SET @DebugCmd += ', @vEventDescription = ''====> Processing Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
						SET @DebugCmd += ', @vEventDetailedDescription = ''EXEC Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user.  Param data in EventXML'''
						SET @DebugCmd += ', @vEventXML = ''EXEC_SP=' + @DebugMsg + ''''
						SET @DebugCmd += ', @iRecID = 0'

						--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
						EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					END
				      
					EXEC uspWorkflowNotifyEvent @EventID = @w_EventID,
												@ClaimAspectID = @w_ClaimAspectID,
												@Description = @w_HistoryDescription,
												@UserID = @UserID
	                                          
					-- Check error value
	      
					IF @@ERROR <> 0
					BEGIN
						-- Error executing procedure

						ROLLBACK TRANSACTION
						RAISERROR('%s: (Workflow Processing) Error notifying APD of the event "%s"', 16, 1, @ProcName, @w_EventName)
						RETURN
					END 
				END
				---------------------------------------------
				-- End - Prep to fire WorkflowNotifyEvent      
				-- Claim Assigned - Owner
				---------------------------------------------
	            
				---------------------------------------------
				-- Begin - Prep to fire WorkflowNotifyEvent      
				-- Claim Assigned - Analyst
				---------------------------------------------
				IF @w_AssignedAnalyst IS NOT NULL
				BEGIN
					SELECT @w_EventID            = CASE @w_ClaimAspectTypeID
													 --WHEN @ClaimAspectTypeIDClaim THEN @EventIDClaimOwnershipTransfer
													 WHEN @ClaimAspectTypeIDProperty THEN @EventIDPropertyAssigned
													 ELSE @EventIDVehicleAnalystAssigned
												   END,
						   @w_EventName          = CASE @w_ClaimAspectTypeID
													 --WHEN @ClaimAspectTypeIDClaim THEN 'Claim Ownership Transfer'
													 WHEN @ClaimAspectTypeIDProperty THEN 'Property Assigned'
													 ELSE 'Vehicle Analyst Assigned'
												   END,
						   @w_HistoryDescription = CASE
													 --WHEN @w_ClaimAspectTypeID = @ClaimAspectTypeIDClaim THEN 'Claim Ownership Transferred to ' + @w_AssignedRepName
													 WHEN @w_ClaimAspectTypeID = @ClaimAspectTypeIDProperty THEN 'Property ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' assigned to '  + @w_AssignedAnalystName    
													 ELSE 'Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' assigned to '  + @w_AssignedAnalystName + ' as analyst.'
												   END
	                                         
					------------------
					-- Debugging 
					------------------
					IF @Debug = 1
					BEGIN
						PRINT ''
						PRINT 'Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user:'
						PRINT '     @EventID = ' + convert(varchar(10), @w_EventID)
						PRINT '     @ClaimAspectID = ' + convert(varchar(10), @w_ClaimAspectID)
						PRINT '     @Description = ' + @w_HistoryDescription
						PRINT '     @UserID = ' + convert(varchar(10), @UserID)        
					END
					ELSE IF @Debug = 2
					BEGIN
						SET @DebugMsg = ''
						SET @DebugMsg += 'Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user:'
						SET @DebugMsg += '     @EventID = ' + convert(varchar(10), ISNULL(@w_EventID,0))
						SET @DebugMsg += '     @ClaimAspectID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectID,0))
						SET @DebugMsg += '     @Description = ' + ISNULL(@w_HistoryDescription,'')
						SET @DebugMsg += '     @UserID = ' + convert(varchar(10), ISNULL(@UserID,0))        

						SET @EventSeq += 1

						SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
						SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
						SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
						SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
						SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
						SET @DebugCmd += ', @vEventDescription = ''====> Processing Call to *** uspWorkflowNotifyEvent *** for CHOICE claim aspect assignment to user: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
						SET @DebugCmd += ', @vEventDetailedDescription = ''EXEC Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user.  Param data in EventXML'''
						SET @DebugCmd += ', @vEventXML = ''EXEC_SP=' + @DebugMsg + ''''
						SET @DebugCmd += ', @iRecID = 0'

						--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
						EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					END
	      
					EXEC uspWorkflowNotifyEvent @EventID = @w_EventID,
												@ClaimAspectID = @w_ClaimAspectID,
												@Description = @w_HistoryDescription,
												@UserID = @UserID
	                                          
					-- Check error value
	      
					IF @@ERROR <> 0
					BEGIN
						-- Error executing procedure

						ROLLBACK TRANSACTION
						RAISERROR('%s: (Workflow Processing) Error notifying APD of the event "%s"', 16, 1, @ProcName, @w_EventName)
						RETURN
					END 
				END
				---------------------------------------------
				-- End - Prep to fire WorkflowNotifyEvent      
				-- Claim Assigned - Analyst
				---------------------------------------------
	          
				---------------------------------------------
				-- Begin - Prep to fire WorkflowNotifyEvent      
				-- Claim Assigned - Support
				---------------------------------------------
				IF @w_AssignedSupport IS NOT NULL
				BEGIN
					SELECT @w_EventID            = CASE @w_ClaimAspectTypeID
													 --WHEN @ClaimAspectTypeIDClaim THEN @EventIDClaimOwnershipTransfer
													 WHEN @ClaimAspectTypeIDProperty THEN @EventIDPropertyAssigned
													 ELSE @EventIDVehicleSupportAssigned
												   END,
						   @w_EventName          = CASE @w_ClaimAspectTypeID
													 --WHEN @ClaimAspectTypeIDClaim THEN 'Claim Ownership Transfer'
													 WHEN @ClaimAspectTypeIDProperty THEN 'Property Assigned'
													 ELSE 'Vehicle Administrator Assigned'
												   END,
						   @w_HistoryDescription = CASE
													 --WHEN @w_ClaimAspectTypeID = @ClaimAspectTypeIDClaim THEN 'Claim Ownership Transferred to ' + @w_AssignedRepName
													 WHEN @w_ClaimAspectTypeID = @ClaimAspectTypeIDProperty THEN 'Property ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' assigned to '  + @w_AssignedSupportName    
													 ELSE 'Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' assigned to '  + @w_AssignedSupportName + ' as administrator.'
												   END

					------------------
					-- Debugging 
					------------------
					IF @Debug = 1
					BEGIN
						PRINT ''
						PRINT 'Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user:'
						PRINT '     @EventID = ' + convert(varchar(10), @w_EventID)
						PRINT '     @ClaimAspectID = ' + convert(varchar(10), @w_ClaimAspectID)
						PRINT '     @Description = ' + @w_HistoryDescription
						PRINT '     @UserID = ' + convert(varchar(10), @UserID)        
					END
					ELSE IF @Debug = 2
					BEGIN
						SET @DebugMsg = ''
						SET @DebugMsg += 'Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user:'
						SET @DebugMsg += '     @EventID = ' + convert(varchar(10), ISNULL(@w_EventID,0))
						SET @DebugMsg += '     @ClaimAspectID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectID,0))
						SET @DebugMsg += '     @Description = ' + ISNULL(@w_HistoryDescription,'')
						SET @DebugMsg += '     @UserID = ' + convert(varchar(10), ISNULL(@UserID,0))        

						SET @EventSeq += 1

						SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
						SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
						SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
						SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
						SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
						SET @DebugCmd += ', @vEventDescription = ''====> Processing Call to *** uspWorkflowNotifyEvent *** for CHOICE claim aspect assignment to user: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
						SET @DebugCmd += ', @vEventDetailedDescription = ''EXEC Call to uspWorkflowNotifyEvent for CHOICE claim aspect assignment to user.  Param data in EventXML'''
						SET @DebugCmd += ', @vEventXML = ''EXEC_SP=' + @DebugMsg + ''''
						SET @DebugCmd += ', @iRecID = 0'

						--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
						EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					END
						      
					EXEC uspWorkflowNotifyEvent @EventID = @w_EventID,
												@ClaimAspectID = @w_ClaimAspectID,
												@Description = @w_HistoryDescription,
												@UserID = @UserID
	                                          
					-- Check error value
	      
					IF @@ERROR <> 0
					BEGIN
						-- Error executing procedure

						ROLLBACK TRANSACTION
						RAISERROR('%s: (Workflow Processing) Error notifying APD of the event "%s"', 16, 1, @ProcName, @w_EventName)
						RETURN
					END 
				END
				---------------------------------------------
				-- End - Prep to fire WorkflowNotifyEvent      
				-- Claim Assigned - Support
				---------------------------------------------
	          
				---------------------------------------------
				-- Begin - Activate the service channels
				---------------------------------------------
				IF @Debug = 1
				begin
					declare @sccount int
					declare @rcount int
	              
					select @rcount = count(*)
					  from @tmpActivatedServiceChannels
	              
					PRINT 'Raw Svc Ch Count: ' + convert(varchar(2),@rcount)

					SELECT @sccount = Count(*)
					  FROM @tmpActivatedServiceChannels tasc
					  INNER JOIN utb_claim_aspect_service_channel casc ON (tasc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
					  LEFT OUTER JOIN utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)     
					  LEFT OUTER JOIN utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID AND 1 = a.AssignmentSequenceNumber)
					  LEFT OUTER JOIN utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
					  LEFT OUTER JOIN utb_fnol_claim_vehicle_load f ON (ca.LynxID = f.LynxID AND ca.ClaimAspectNumber = f.VehicleNumber)
					  LEFT OUTER JOIN utb_shop_search_log ssl ON (f.ShopSearchLogID = ssl.ShopSearchLogID)
					  LEFT OUTER JOIN utb_appraiser ap ON (a.AppraiserID = ap.AppraiserID)
					  WHERE tasc.ClaimAspectID = @w_ClaimAspectID      

					PRINT 'Service Channels Needing Activating = ' + CONVERT(Varchar(2),@sccount)
	              
					SELECT casc.ClaimAspectServiceChannelID,
						   casc.ServiceChannelCD,
						   tasc.PrimaryFlag,
						   tasc.ReactivatedFlag,                          
						   a.ShopLocationID,
						   a.ProgramTypeCD,
						   sl.Name,
						   ssl.CEIShopsIncludedFlag,
						   a.AppraiserID,
						   ap.Name
					  FROM @tmpActivatedServiceChannels tasc
					  INNER JOIN utb_claim_aspect_service_channel casc ON (tasc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
					  LEFT OUTER JOIN utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)     
					  LEFT OUTER JOIN utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID AND 1 = a.AssignmentSequenceNumber)
					  LEFT OUTER JOIN utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
					  LEFT OUTER JOIN utb_fnol_claim_vehicle_load f ON (ca.LynxID = f.LynxID AND ca.ClaimAspectNumber = f.VehicleNumber)
					  LEFT OUTER JOIN utb_shop_search_log ssl ON (f.ShopSearchLogID = ssl.ShopSearchLogID)
					  LEFT OUTER JOIN utb_appraiser ap ON (a.AppraiserID = ap.AppraiserID)
					  WHERE tasc.ClaimAspectID = @w_ClaimAspectID            
	          
					PRINT 'Processing Service Channel Workflow...'
				END

				DECLARE @w_RentalFlag       AS udt_std_flag
				DECLARE @w_ExposureCD       AS udt_std_cd
				DECLARE @w_ConditionValue   AS udt_std_desc_mid

				-- 16Feb2012 - TVD - Elephant
				DECLARE @w_Passthru		    AS VARCHAR(200)
				SET @w_Passthru = NULL
	          
				SET @w_RentalFlag = 0
	          
				DECLARE csrServiceChannels CURSOR FOR
				  SELECT casc.ClaimAspectServiceChannelID,
						 casc.ServiceChannelCD,
						 tasc.PrimaryFlag,
						 tasc.ReactivatedFlag,                          
						 a.ShopLocationID,
						 a.ProgramTypeCD,
						 sl.Name,
						 ssl.CEIShopsIncludedFlag,
						 a.AppraiserID,
						 ap.Name,
						 ca.SourceApplicationPassThruData  -- 16Feb2012 - TVD - Elephant
					FROM @tmpActivatedServiceChannels tasc
					INNER JOIN utb_claim_aspect_service_channel casc ON (tasc.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID)
					LEFT OUTER JOIN utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)     
					LEFT OUTER JOIN utb_assignment a ON (casc.ClaimAspectServiceChannelID = a.ClaimAspectServiceChannelID AND 1 = a.AssignmentSequenceNumber)
					LEFT OUTER JOIN utb_shop_location sl ON (a.ShopLocationID = sl.ShopLocationID)
					LEFT OUTER JOIN utb_fnol_claim_vehicle_load f ON (ca.LynxID = f.LynxID AND ca.ClaimAspectNumber = f.VehicleNumber)
					LEFT OUTER JOIN utb_shop_search_log ssl ON (f.ShopSearchLogID = ssl.ShopSearchLogID)
					LEFT OUTER JOIN utb_appraiser ap ON (a.AppraiserID = ap.AppraiserID)
					WHERE tasc.ClaimAspectID = @w_ClaimAspectID      
					ORDER BY tasc.PrimaryFlag DESC, casc.ClaimAspectServiceChannelID        
	          
				OPEN csrServiceChannels

				IF @@ERROR <> 0
				BEGIN
					-- SQL Server Error

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing) SQL Server Error opening workflow - Activate Service Channels cursor.', 16, 1, @ProcName)
					RETURN
				END
	          
	          
				FETCH NEXT FROM csrServiceChannels
				INTO @w_ClaimAspectServiceChannelID,
					 @w_ServiceChannelCD,
					 @w_PrimaryFlag,
					 @w_ReactivatedFlag,
					 @w_ShopLocationID,
					 @w_ProgramTypeCD,
					 @w_ShopLocationName,
					 @w_CarrierUsesCEIFlag,
					 @w_AppraiserID,
					 @w_AppraiserName,
					 @w_Passthru  -- 16Feb2012 - TVD - Elephant

				IF @@ERROR <> 0
				BEGIN
					-- SQL Server Error

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing) SQL Server Error fetching workflow - Activate Service Channels record.', 16, 1, @ProcName)
					RETURN
				END
	        
				------------------
				-- Debugging 
				------------------
				IF @Debug = 1
				BEGIN
					PRINT ''
					PRINT 'Initial Fetch from Service Channels:'
					PRINT '     @w_ClaimAspectServiceChannelID = ' + convert(varchar(10), @w_ClaimAspectServiceChannelID)
					PRINT '                @w_ServiceChannelCD = ' + ISNULL(@w_ServiceChannelCD, 'NULL')
					PRINT '                @w_PrimaryFlag = ' + CONVERT(varchar,@w_PrimaryFlag)
				END
				ELSE IF @Debug = 2
				BEGIN
					SET @DebugMsg = ''
					SET @DebugMsg += 'Initial Fetch from Service Channels:'
					SET @DebugMsg += '     @w_ClaimAspectServiceChannelID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectServiceChannelID,0))
					SET @DebugMsg += '                @w_ServiceChannelCD = ' + ISNULL(@w_ServiceChannelCD, 'NULL')
					SET @DebugMsg += '                @w_PrimaryFlag = ' + CONVERT(varchar, ISNULL(@w_PrimaryFlag,0))

					SET @EventSeq += 1

					SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
					SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
					SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(INIT_SERV_CHAN)'''
					SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
					SET @DebugCmd += ', @vEventStatus = ''INIT_SERV_CHAN'''
					SET @DebugCmd += ', @vEventDescription = ''====> Processing Initial Fetch from Service Channels: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
					SET @DebugCmd += ', @vEventDetailedDescription = ''Initial Fetch from Service Channels.  Param data in EventXML'''
					SET @DebugCmd += ', @vEventXML = ''INIT_SERV_CHAN=' + @DebugMsg + ''''
					SET @DebugCmd += ', @iRecID = 0'

					--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				END				  
				   
				WHILE @@FETCH_STATUS = 0
				BEGIN
					------------------
					-- Debugging 
					------------------
					IF @Debug = 1
					BEGIN
						PRINT 'Fetch SC Records'
					END
					ELSE IF @Debug = 2
					BEGIN
						SET @DebugMsg = 'Fetch SC Records'

						SET @EventSeq += 1

						SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
						SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
						SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(CUR_FETCH)'''
						SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
						SET @DebugCmd += ', @vEventStatus = ''CUR_FETCH'''
						SET @DebugCmd += ', @vEventDescription = ''====> Process Fetch SC Records: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
						SET @DebugCmd += ', @vEventDetailedDescription = ''Process Fetch SC Records'''
						SET @DebugCmd += ', @vEventXML = ''CUR_FETCH' + @DebugMsg + ''''
						SET @DebugCmd += ', @iRecID = 0'

						--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
						EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					END

					-- If this is primary service channel we need to check rental and indicate rental task is needed if needed
					IF @w_PrimaryFlag = 1                            
					BEGIN
	              
						SELECT @w_ExposureCD = ca.ExposureCD
						  FROM dbo.utb_claim_aspect_service_channel casc
						  INNER JOIN dbo.utb_claim_aspect ca on (casc.ClaimAspectID = ca.ClaimAspectID)
						  WHERE casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID

	            
						IF @w_ExposureCD = '1'
						BEGIN            
							-- If 1st party vehicle the task will be thrown if rental coverage is on policy
							IF EXISTS (SELECT cc.ClaimCoverageID 
										 FROM dbo.utb_claim_aspect_service_channel casc
										 INNER JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
										 INNER JOIN dbo.utb_claim_coverage cc ON (ca.LynxID = cc.LynxID)
										 WHERE casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
										   AND cc.CoverageTypeCD = 'RENT')
							BEGIN                   
								SET @w_RentalFlag = 1
							END
						END               
						ELSE
						BEGIN
							-- 3rd party vehicle the task generation will depend on if rental is authorized since
							-- rental here would be covered under liability rather then rental coverage
							DECLARE @w_RentDaysAuth as udt_std_int
	              
							SELECT @w_RentDaysAuth = cv.RentalDaysAuthorized
							  FROM dbo.utb_claim_aspect_service_channel casc
							  INNER JOIN dbo.utb_claim_aspect ca ON (casc.ClaimAspectID = ca.ClaimAspectID)
							  INNER JOIN dbo.utb_claim_vehicle cv ON (ca.ClaimAspectID = cv.ClaimAspectID)
							  WHERE casc.ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID
	              
							------------------
							-- Debugging 
							------------------
							IF @Debug = 1
							BEGIN
								PRINT ''
								PRINT 'Rental Days Authorized: ' + ISNULL(convert(varchar,@w_RentDaysAuth),'Null')
							END
							ELSE IF @Debug = 2
							BEGIN
								SET @DebugMsg = ''
								SET @DebugMsg += 'Rental Days Authorized: ' + ISNULL(convert(varchar,@w_RentDaysAuth),'Null')

								SET @EventSeq += 1

								SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
								SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
								SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(RENT_AUTH)'''
								SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
								SET @DebugCmd += ', @vEventStatus = ''RENT_AUTH'''
								SET @DebugCmd += ', @vEventDescription = ''====> Processing Rental Days Authorized: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
								SET @DebugCmd += ', @vEventDetailedDescription = ''Rental Days Authorized.  Param data in EventXML'''
								SET @DebugCmd += ', @vEventXML = ''RENT_AUTH=' + @DebugMsg + ''''
								SET @DebugCmd += ', @iRecID = 0'

								--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
								EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
							END

							IF @w_RentDaysAuth IS NOT NULL AND @w_RentDaysAuth > 0
							BEGIN
								SET @w_RentalFlag = 1
							END
						END
					END
	                
	                    
					SET @w_ConditionValue = @w_ServiceChannelCD
					IF @w_PrimaryFlag = 1
					BEGIN
						SET @w_ConditionValue = @w_ConditionValue + ',' + @w_ApplicationName
					END
					IF @w_RentalFlag = 1 
					BEGIN
						SET @w_ConditionValue = @w_ConditionValue + ',RENTAL'
					END                        
	          
	            
					-- Notify APD that service channel was activated.         
					SELECT @ServiceChannelName = Name
					  FROM dbo.ufnUtilityGetReferenceCodes('utb_claim_aspect_service_channel', 'ServiceChannelCD') 
					  WHERE Code = @w_ServiceChannelCD

					SET  @w_HistoryDescription = 'Service Channel Activated: ' + @ServiceChannelName
	            
					/**************************************
					  09Feb2012 - TVD - SourceApplicationPassthruDataVehicle
					  data add for Elephant
					**************************************/
					DECLARE @Key udt_std_desc_mid
					SET @Key = NULL
					
					IF (@w_Passthru) = 'E'
					BEGIN				
						SET @Key = '*** Estimate Only ***'
						SET @w_HistoryDescription = @w_HistoryDescription + ' - *** Estimate Only ***'
					END						
	            
					------------------
					-- Debugging 
					------------------
					IF @Debug = 1
					BEGIN
						PRINT '@w_RentalFlag = ' + CONVERT(varchar,@w_rentalflag)
						PRINT 'Call to uspWorkflowNotifyEvent for Service Channel Activation:'
						PRINT '     @EventID = ' + convert(varchar(10), @EventIDServiceChannelActivated)
						PRINT '     @ClaimAspectServiceChannelID = ' + convert(varchar(10), @w_ClaimAspectServiceChannelID)
						PRINT '     @Description = ' + @w_HistoryDescription
						PRINT '     @ServiceChannelCD = ' + @w_ServiceChannelCD
						PRINT '     @UserID = ' + convert(varchar(10), @UserID)
						PRINT '     @ConditionValue = ' + @w_ConditionValue
					END
					ELSE IF @Debug = 2
					BEGIN
						SET @DebugMsg = ''
						SET @DebugMsg += '@w_RentalFlag = ' + CONVERT(varchar, ISNULL(@w_rentalflag,0))
						SET @DebugMsg += 'Call to uspWorkflowNotifyEvent for Service Channel Activation:'
						SET @DebugMsg += '     @EventID = ' + convert(varchar(10), ISNULL(@EventIDServiceChannelActivated,0))
						SET @DebugMsg += '     @ClaimAspectServiceChannelID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectServiceChannelID,0))
						SET @DebugMsg += '     @Description = ' + ISNULL(@w_HistoryDescription,'')
						SET @DebugMsg += '     @ServiceChannelCD = ' + ISNULL(@w_ServiceChannelCD,'')
						SET @DebugMsg += '     @UserID = ' + convert(varchar(10), ISNULL(@UserID,0))
						SET @DebugMsg += '     @ConditionValue = ' + ISNULL(@w_ConditionValue,0)

						SET @EventSeq += 1

						SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
						SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
						SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
						SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
						SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
						SET @DebugCmd += ', @vEventDescription = ''====> Processing Call to *** uspWorkflowNotifyEvent *** for Service Channel Activation: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
						SET @DebugCmd += ', @vEventDetailedDescription = ''EXEC Call to uspWorkflowNotifyEvent for Service Channel Activation.  Param data in EventXML'''
						SET @DebugCmd += ', @vEventXML = ''EXEC_SP=' + @DebugMsg + ''''
						SET @DebugCmd += ', @iRecID = 0'

						--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
						EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
					END
	    
					EXEC uspWorkflowNotifyEvent @EventID = @EventIDServiceChannelActivated,
												@ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
												@Description = @w_HistoryDescription,
												@UserID = @UserID,
												@ConditionValue = @w_ConditionValue, --@w_ServiceChannelCD 
												@Key = @Key		-- Elephant Added   

					-- Check error value

					IF @@ERROR <> 0
					BEGIN
						-- Error executing procedure

						ROLLBACK TRANSACTION
						RAISERROR('%s: (Workflow Processing for Vehicle %u) Error notifying APD of "Service Channel Activation" event', 16, 1, @ProcName, @w_ClaimAspectNumber)
						RETURN
					END 
	            
					IF @vw_ServiceChannelCDWork = 'TL'
					BEGIN
						-- Fire off events that are Vehicle Owner specific
						EXEC uspWorkflowNotifyEvent @EventID = @EventIDTLVehicleOwnerCreated,
													@ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
													@Description = 'Vehicle Owner added',
													@UserID = @UserID,
													@ConditionValue = 'TL'
	               
						-- Check error value

						IF @@ERROR <> 0
						BEGIN
							-- Error executing procedure

							ROLLBACK TRANSACTION
							RAISERROR('%s: (Workflow Processing for Vehicle %u) Error notifying APD of "Total Loss Vehicle Owner Created" event', 16, 1, @ProcName, @w_ClaimAspectNumber)
							RETURN
						END 

						IF @c_LienHolderID IS NOT NULL
						BEGIN
							-- Fire off events that are Lien Holder specific
							EXEC uspWorkflowNotifyEvent @EventID = @EventIDTLLienHolderCreated,
														@ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
														@Description = 'Lien Holder added',
														@UserID = @UserID,
														@ConditionValue = 'TL'
	                  
							-- Check error value

							IF @@ERROR <> 0
							BEGIN
								-- Error executing procedure

								ROLLBACK TRANSACTION
								RAISERROR('%s: (Workflow Processing for Vehicle %u) Error notifying APD of "Total Loss Lien Holder Created" event', 16, 1, @ProcName, @w_ClaimAspectNumber)
								RETURN
							END 
	                  
							IF EXISTS (SELECT b.BillingID
										 FROM utb_lien_holder lh
										 LEFT JOIN utb_billing b ON lh.BillingID = b.BillingID
										 WHERE lh.LienHolderID = @c_LienHolderID
										   AND (b.EFTContractSignedFlag = 0 
												OR (lh.FedTaxId is null OR LEN(RTRIM(LTRIM(lh.FedTaxId))) = 0)
											   )
									  )
							BEGIN
								-- Fire off events that are Lien Holder specific
								EXEC uspWorkflowNotifyEvent @EventID = @EventIDTLLienHolderMissingBilling,
															@ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
															@Description = 'Lien Holder setup required',
															@UserID = @UserID,
															@ConditionValue = 'TL'
	                     
								-- Check error value

								IF @@ERROR <> 0
								BEGIN
									-- Error executing procedure

									ROLLBACK TRANSACTION
									RAISERROR('%s: (Workflow Processing for Vehicle %u) Error notifying APD of "Total Loss Lien Holder set up required" event', 16, 1, @ProcName, @w_ClaimAspectNumber)
									RETURN
								END 
							END
						END
					END

	            
					SET @w_EventID = NULL
					SET @w_HistoryDescription = NULL

					IF (@w_ShopLocationID IS NOT NULL) OR (@w_AppraiserID IS NOT NULL)
					BEGIN
						-- An Assignment record was created, must determine and toss the proper event notifying APD.
						SET @w_EventID = NULL

						IF  ((@w_ShopLocationID = @DeskAuditID) AND (@DeskAuditAppraiserType='S')) OR
							((@w_AppraiserID = @DeskAuditID) AND (@DeskAuditAppraiserType = 'A'))
						BEGIN
							-- Assignment is to the LYNX Desk Audit unit.
	                
							SET @w_EventID = @EventIDLynxDAUSelected
							SET @w_HistoryDescription = 'Desk Audit Assignment: Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' was fowarded to LYNX Services desk audit unit.'
						END

						IF ((@w_ShopLocationID = @GlassAGCAppraiserID) AND (@w_ServiceChannelCD = 'GL'))
						BEGIN
							-- Assignment Appraiser Assigned
							SET @w_EventID = @EventIDAppraiserAssigned
							SET @w_HistoryDescription = 'Glass Assignment: Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' was fowarded to LYNX Services glass unit.'
						END

						IF @w_EventID IS NULL
						BEGIN
							-- This is a regular shop or appraiser assignment
	                
							IF @w_ShopLocationID IS NOT NULL
							BEGIN
								-- Shop Selected
	                  
								SET @w_EventID = @EventIDShopSelected
								SELECT @w_HistoryDescription = 
												  CASE
													  WHEN @w_ProgramTypeCD = 'LS' THEN 'Program Shop Assignment: Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' was assigned to ' +
																					  @w_ShopLocationName + ' (Shop ID ' + Convert(varchar(5), @w_ShopLocationID) + ')'
													  WHEN @w_CarrierUsesCEIFlag = 1 AND @w_ProgramTypeCD = 'CEI' THEN 'C-Program Shop Assignment: Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' was assigned to ' +
																													  @w_ShopLocationName + ' (Shop ID ' + Convert(varchar(5), @w_ShopLocationID) + ')'
													  ELSE 'Non Program Shop Assignment: Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' was assigned to ' +
																	@w_ShopLocationName + ' (Shop ID ' + Convert(varchar(5), @w_ShopLocationID) + ')'
												  END
							END
							ELSE
							BEGIN
								-- Appraiser selected
	                    
								SET @w_EventID = @EventIDIASelected
								SET @w_HistoryDescription = @ServiceChannelName + ' Appraiser Assignment: Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' was assigned to ' +
														  @w_AppraiserName + ' (Appraiser ID ' + Convert(varchar(5), @w_AppraiserID) + ')'
							END
						END

						------------------
						-- Debugging 
						------------------
						IF @Debug = 1
						BEGIN
							PRINT ''
							PRINT 'Call to uspWorkflowNotifyEvent for Vehicle Shop/IA/Lynx DAU Selection:'
							PRINT '     @EventID = ' + convert(varchar(10), @w_EventID)
							PRINT '     @ClaimAspectID = ' + convert(varchar(10), @w_ClaimAspectID)
							PRINT '     @Description = ' + @w_HistoryDescription
							PRINT '     @UserID = ' + convert(varchar(10), @UserID)
						END
						ELSE IF @Debug = 2
						BEGIN
							SET @DebugMsg = ''
							SET @DebugMsg += 'Call to uspWorkflowNotifyEvent for Vehicle Shop/IA/Lynx DAU Selection:'
							SET @DebugMsg += '     @EventID = ' + convert(varchar(10), ISNULL(@w_EventID,0))
							SET @DebugMsg += '     @ClaimAspectID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectID,0))
							SET @DebugMsg += '     @Description = ' + ISNULL(@w_HistoryDescription,'')
							SET @DebugMsg += '     @UserID = ' + convert(varchar(10), ISNULL(@UserID,0))

							SET @EventSeq += 1

							SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
							SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
							SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
							SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
							SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
							SET @DebugCmd += ', @vEventDescription = ''====> Processing Call to *** uspWorkflowNotifyEvent *** for Vehicle Shop/IA/Lynx DAU Selection: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
							SET @DebugCmd += ', @vEventDetailedDescription = ''EXEC Call to uspWorkflowNotifyEvent for Vehicle Shop/IA/Lynx DAU Selection.  Param data in EventXML'''
							SET @DebugCmd += ', @vEventXML = ''EXEC_SP=' + @DebugMsg + ''''
							SET @DebugCmd += ', @iRecID = 0'

							--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
							EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
						END
	    
						EXEC uspWorkflowNotifyEvent @EventID = @w_EventID,
													@ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
													@Description = @w_HistoryDescription,
													@UserID = @UserID,
													@ConditionValue = @w_ServiceChannelCD,
													@Key = 1      -- Denotes primary assignment

						-- Check error value

						IF @@ERROR <> 0
						BEGIN
							-- Error executing procedure

							ROLLBACK TRANSACTION
							RAISERROR('%s: (Workflow Processing for Vehicle %u) Error notifying APD of "Shop/IA/LYNX Desk Audit Unit Selected" event', 16, 1, @ProcName, @w_ClaimAspectNumber)
							RETURN
						END
					END  
	              
					-- If serrvice channel is 'RRP' we also need to throw a notify event for the secondary LDAU selection
					IF @w_ServiceChannelCD = 'RRP'             
					BEGIN
						SET @w_EventID = @EventIDLynxDAUSelected
						SET @w_HistoryDescription = 'Desk Audit Assignment: Vehicle ' + Convert(varchar(5), @w_ClaimAspectNumber) + ' was fowarded to LYNX Services desk audit unit (Secondary Assignment).'

						------------------
						-- Debugging 
						------------------
						IF @Debug = 1
						BEGIN
							PRINT ''
							PRINT 'Call to uspWorkflowNotifyEvent for RRP LDAU Secondary Assignment:'
							PRINT '     @EventID = ' + convert(varchar(10), @w_EventID)
							PRINT '     @ClaimAspectID = ' + convert(varchar(10), @w_ClaimAspectID)
							PRINT '     @Description = ' + @w_HistoryDescription
							PRINT '     @UserID = ' + convert(varchar(10), @UserID)
						END
						ELSE IF @Debug = 2
						BEGIN
							SET @DebugMsg = ''
							SET @DebugMsg += 'Call to uspWorkflowNotifyEvent for RRP LDAU Secondary Assignment:'
							SET @DebugMsg += '     @EventID = ' + convert(varchar(10), ISNULL(@w_EventID,0))
							SET @DebugMsg += '     @ClaimAspectID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectID,0))
							SET @DebugMsg += '     @Description = ' + ISNULL(@w_HistoryDescription,'')
							SET @DebugMsg += '     @UserID = ' + convert(varchar(10), ISNULL(@UserID,0))

							SET @EventSeq += 1

							SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
							SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
							SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
							SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
							SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
							SET @DebugCmd += ', @vEventDescription = ''====> Processing Call to *** uspWorkflowNotifyEvent *** for RRP LDAU Secondary Assignment: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
							SET @DebugCmd += ', @vEventDetailedDescription = ''EXEC Call to uspWorkflowNotifyEvent for RRP LDAU Secondary Assignment.  Param data in EventXML'''
							SET @DebugCmd += ', @vEventXML = ''EXEC_SP=' + @DebugMsg + ''''
							SET @DebugCmd += ', @iRecID = 0'

							--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
							EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
						END
	    	          
						EXEC uspWorkflowNotifyEvent @EventID = @w_EventID,
													@ClaimAspectServiceChannelID = @w_ClaimAspectServiceChannelID,
													@Description = @w_HistoryDescription,
													@UserID = @UserID,
													@ConditionValue = @w_ServiceChannelCD,
													@Key = 2      -- Denotes secondary assignment

						-- Check error value

						IF @@ERROR <> 0
						BEGIN
							-- Error executing procedure

							ROLLBACK TRANSACTION
							RAISERROR('%s: (Workflow Processing for Vehicle %u) Error notifying APD of "Shop/IA/LYNX Desk Audit Unit Selected" event (Secondary Assignment)', 16, 1, @ProcName, @w_ClaimAspectNumber)
							RETURN
						END
					END
	            
					-- Get Next Service Channel for Claim Aspect.
					FETCH NEXT FROM csrServiceChannels
					INTO @w_ClaimAspectServiceChannelID,
						 @w_ServiceChannelCD,
						 @w_PrimaryFlag,
						 @w_ReactivatedFlag,
						 @w_ShopLocationID,
						 @w_ProgramTypeCD,
						 @w_ShopLocationName,
						 @w_CarrierUsesCEIFlag,
						 @w_AppraiserID,
						 @w_AppraiserName,
						 @w_Passthru  -- 16Feb2012 - TVD - Elephant
	                
					IF @@ERROR <> 0
					BEGIN
						-- SQL Server Error

						ROLLBACK TRANSACTION
						RAISERROR('%s: (Workflow Processing) SQL Server Error fetching workflow - activate service channel cursor record.', 16, 1, @ProcName)
						RETURN
					END
				END
	                            
				CLOSE csrServiceChannels
				DEALLOCATE csrServiceChannels         
	        
			END     -- For "IF @w_ClaimAspectTypeID IN (@ClaimAspectTypeIDVehicle, @ClaimAspectTypeIDProperty)"      

		------------------------------------
		------------------------------------
		END -- CS Workflow Skip
        ELSE
        BEGIN
			-------------------------------------
			-- Everything in here is for CS only
			-------------------------------------

			-------------------------------------
			-- Get Claim level AspectID
			-- 30Sep2016 - TVD - Changed to Left join
			-------------------------------------
			SELECT  
				@w_ClaimAspectIDClaim = ca.ClaimAspectID,
				@w_StatusIDClaim = cas.StatusID
			FROM  
				dbo.utb_claim_aspect ca
				LEFT JOIN utb_Claim_Aspect_Status cas ON ca.ClaimAspectID = cas.ClaimAspectID
		    WHERE 
				ca.LynxID = @LynxID 
				AND ca.ClaimAspectTypeID = 0

			-------------------------------------
			-- Get Vehicle level Details
			-------------------------------------
			SELECT 
				@w_ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
				, @w_ClaimAspectIDVehicle = @w_ClaimAspectID
			FROM 
				utb_claim_aspect_service_channel casc 
				LEFT OUTER JOIN utb_claim_aspect ca 
					ON (casc.ClaimAspectID = ca.ClaimAspectID)     
			WHERE 
				ca.ClaimAspectID = @w_ClaimAspectID      
			ORDER BY 
				casc.ClaimAspectServiceChannelID        

			-------------------------------------
			-- Update the Status to:
			--    Claim Open
			--    Vehicle Open
			--    Appraiser Selected - Assignment Not Sent
			--    Appraiser Selected - Fax Not Sent
			--    Active
			-------------------------------------

			------------------
			-- Debugging 
			------------------
			IF @Debug = 1
			BEGIN
                PRINT ''
				PRINT 'Claim Aspects, Claim & Vehicle'
				PRINT '@w_ClaimAspectID: ' + CONVERT(VARCHAR(50), @w_ClaimAspectID)
				PRINT '@w_ClaimAspectIDClaim: ' + CONVERT(VARCHAR(50), @w_ClaimAspectIDClaim)
				PRINT '@w_ClaimAspectIDVehicle: ' + CONVERT(VARCHAR(50), @w_ClaimAspectIDVehicle)
			END
			ELSE IF @Debug = 2
			BEGIN
				SET @DebugMsg = ''
				SET @DebugMsg += 'Claim Aspects, Claim & Vehicle'
				SET @DebugMsg += '@w_ClaimAspectID: ' + CONVERT(VARCHAR(50), ISNULL(@w_ClaimAspectID,0))
				SET @DebugMsg += '@w_ClaimAspectIDClaim: ' + CONVERT(VARCHAR(50), ISNULL(@w_ClaimAspectIDClaim,0))
				SET @DebugMsg += '@w_ClaimAspectIDVehicle: ' + CONVERT(VARCHAR(50), ISNULL(@w_ClaimAspectIDVehicle,0))

				SET @EventSeq += 1

				SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
				SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
				SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(CLAIM_VARS)'''
				SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
				SET @DebugCmd += ', @vEventStatus = ''CLAIM_VARS'''
				SET @DebugCmd += ', @vEventDescription = ''====> Processing Claim Aspects, Claim & Vehicle: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
				SET @DebugCmd += ', @vEventDetailedDescription = ''Claim Aspects, Claim & Vehicle.  Param data in EventXML'''
				SET @DebugCmd += ', @vEventXML = ''CLAIM_VARS=' + @DebugMsg + ''''
				SET @DebugCmd += ', @iRecID = 0'

				--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
			END

			---------------------------------------------
			-- 30Spe2016 - TVD - ReDesigning this
			-- code since it is never firing and causing
			-- data warehouse issues.
			---------------------------------------------
			-- If this is the Claim 
			---------------------------------------------
			-- Check if Claim is open or not.  If not open it.
			IF NOT EXISTS(
				SELECT 
					ClaimAspectStatusID
				FROM
					utb_claim_aspect_status
				WHERE 
					ClaimAspectID = @w_ClaimAspectIDClaim  -- Claim Level
					AND StatusID = 2 -- Open (Claim)	 		
			)
			BEGIN
				EXEC uspWorkflowUpdateStatus @ClaimAspectID = @w_ClaimAspectIDClaim, @StatusID = 2, @UserID = 0 -- Open (Claim)

				-- Check error value
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing for Vehicle %u) Error updating the status to Open (Claim))', 16, 1, @ProcName, @w_ClaimAspectIDClaim)
					RETURN
				END
			END
/*
			IF @w_ClaimAspectTypeID IN (@w_ClaimAspectIDClaim)
			BEGIN
				EXEC uspWorkflowUpdateStatus @ClaimAspectID = @w_ClaimAspectIDClaim, @StatusID = 2, @UserID = 0 -- Open (Claim)

				-- Check error value
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing for Vehicle %u) Error updating the status to Open (Claim))', 16, 1, @ProcName, @w_ClaimAspectIDClaim)
					RETURN
				END
			END
*/			
			-- If this is the Vehicle 
			IF @w_ClaimAspectTypeID IN (@ClaimAspectTypeIDVehicle)
			BEGIN
				EXEC uspWorkflowUpdateStatus @ClaimAspectID = @w_ClaimAspectIDVehicle, @StatusID = 100, @UserID = 0 -- Open (Vehicle)

				-- Check error value
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing for Vehicle %u) Error updating the status to Open (Claim))', 16, 1, @ProcName, @w_ClaimAspectID)
					RETURN
				END

				EXEC uspWorkflowUpdateStatus @ClaimAspectID = @w_ClaimAspectIDVehicle, @StatusID = 11, @OriginatorServiceChannelCD = 'CS', @UserID = 0 -- Appraiser Selected - Assignment Not Sent
				-- Check error value
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing for Vehicle %u) Error updating the status to Appraiser Selected - Assignment Not Sent)', 16, 1, @ProcName, @w_ClaimAspectID)
					RETURN
				END

				EXEC uspWorkflowUpdateStatus @ClaimAspectID = @w_ClaimAspectIDVehicle, @StatusID = 22, @OriginatorServiceChannelCD = 'CS', @UserID = 0 -- Appraiser Selected - Fax Not Sent
				-- Check error value
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing for Vehicle %u) Error updating the status to Appraiser Selected - Fax Not Sent)', 16, 1, @ProcName, @w_ClaimAspectID)
					RETURN
				END

				EXEC uspWorkflowUpdateStatus @ClaimAspectID = @w_ClaimAspectIDVehicle, @StatusID = 800, @UserID = 0 -- Active
				-- Check error value
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing for Vehicle %u) Error updating the status to Active)', 16, 1, @ProcName, @w_ClaimAspectID)
					RETURN
				END

				----------------------------------------
				-- Update Initial Claim History
				-- 6 Event - Claim Assigned and
				-- Status Assigned to "Open"
				----------------------------------------
				SET @w_HistoryDescription = 'Choice Claim Transferred to APD from ' + @w_ApplicationName + ', Status Assigned to "Open"'
				EXEC uspChoiceClaimHistoryEventInsDetail @LynxID, 6, 0, @w_ClaimAspectNumber, @w_ServiceChannelCD, @w_ClaimAspectServiceChannelID, @w_HistoryDescription, 0, 6 , 100, 0
				-- Check error value
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing for Claim %u) Error updating the claim status to Open (Claim))', 16, 1, @ProcName, @w_ClaimAspectIDClaim)
					RETURN
				END

				----------------------------------------
				-- Update Initial Claim History
				-- 29 Event - Vehicle Assigned and
				-- Status Assigned to "Open"
				----------------------------------------
				SET @w_HistoryDescription = 'Vehicle ' + CONVERT(VARCHAR(3),@w_ClaimAspectNumber)  +  ' created, Status Assigned to "Open"'
				EXEC uspChoiceClaimHistoryEventInsDetail @LynxID, 29, 9, @w_ClaimAspectNumber, @w_ServiceChannelCD, @w_ClaimAspectServiceChannelID, @w_HistoryDescription, 0, 6 , 100, 0
				-- Check error value
				IF @@ERROR <> 0
				BEGIN
					-- Error executing procedure

					ROLLBACK TRANSACTION
					RAISERROR('%s: (Workflow Processing for Vehicle %u) Error updating the vehicle status to Open (Vehicle))', 16, 1, @ProcName, @w_ClaimAspectIDClaim)
					RETURN
				END
			END
		
			------------------
			-- Debugging 
			------------------
			IF @Debug = 1
			BEGIN
				PRINT ''
				PRINT 'Call to uspChoiceClaimHistoryEventInsDetail for Choice Shop:'
				PRINT '     @EventID = ' + convert(varchar(10), @w_EventID)
				PRINT '     @ClaimAspectID = ' + convert(varchar(10), @w_ClaimAspectID)
				PRINT '     @ClaimAspectIDClaim = ' + convert(varchar(10), @w_ClaimAspectIDClaim)
				PRINT '     @ClaimAspectIDVehicle = ' + convert(varchar(10), @w_ClaimAspectIDVehicle)
				PRINT '     @Description = ' + @w_HistoryDescription
				PRINT '     @UserID = ' + convert(varchar(10), @UserID)
			END
			ELSE IF @Debug = 2
			BEGIN
				SET @DebugMsg = ''
				SET @DebugMsg += 'Call to uspChoiceClaimHistoryEventInsDetail for Choice Shop:'
				SET @DebugMsg += '     @EventID = ' + convert(varchar(10), ISNULL(@w_EventID,0))
				SET @DebugMsg += '     @ClaimAspectID = ' + convert(varchar(10), ISNULL(@w_ClaimAspectID,0))
				SET @DebugMsg += '     @ClaimAspectIDClaim = ' + convert(varchar(10), ISNULL(@w_ClaimAspectIDClaim,0))
				SET @DebugMsg += '     @ClaimAspectIDVehicle = ' + convert(varchar(10), ISNULL(@w_ClaimAspectIDVehicle,0))
				SET @DebugMsg += '     @Description = ' + ISNULL(@w_HistoryDescription,'')
				SET @DebugMsg += '     @UserID = ' + convert(varchar(10), ISNULL(@UserID,0))

				SET @EventSeq += 1

				SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
				SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
				SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(EXEC_SP)'''
				SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
				SET @DebugCmd += ', @vEventStatus = ''EXEC_SP'''
				SET @DebugCmd += ', @vEventDescription = ''====> Processing Call to *** uspChoiceClaimHistoryEventInsDetail *** for Choice Shop: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
				SET @DebugCmd += ', @vEventDetailedDescription = ''EXEC Call to uspChoiceClaimHistoryEventInsDetail for Choice Shop.  Param data in EventXML'''
				SET @DebugCmd += ', @vEventXML = ''EXEC_SP=' + @DebugMsg + ''''
				SET @DebugCmd += ', @iRecID = 0'

				--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
				EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
			END

        END
        
        -- Get next claim aspect
        FETCH NEXT FROM csrAddedAspects
            INTO @w_ClaimAspectID, @w_ClaimAspectTypeID, @w_ClaimAspectNumber
    END        
                             
    CLOSE csrAddedAspects    
    DEALLOCATE csrAddedAspects
	---------------------------------------------
	-- End - Activate the service channels
	---------------------------------------------

--END -- CS Workflow Skip


     /*********************************************************************************************
    **********************************************************************************************
    *   BEGIN CLEANUP OF LOAD TABLES
    **********************************************************************************************
    *********************************************************************************************/

    DELETE FROM dbo.utb_fnol_claim_load 
      WHERE LynxID = @LynxID
      
    IF @@ERROR <> 0
    BEGIN
        -- Deletion Error
       
        ROLLBACK TRANSACTION
        RAISERROR('%s: (Cleanup) Error deleting from utb_fnol_claim_load', 16, 1, @ProcName)
        RETURN
    END 
    
    
    DELETE FROM dbo.utb_fnol_claim_vehicle_load
      WHERE LynxID = @LynxID
      
    IF @@ERROR <> 0
    BEGIN
        -- Deletion Error
       
        ROLLBACK TRANSACTION
        RAISERROR('%s: (Cleanup) Error deleting from utb_fnol_claim_vehicle_load', 16, 1, @ProcName)
        RETURN
    END 
    
    
    DELETE FROM dbo.utb_fnol_involved_load
      WHERE LynxID = @LynxID
      
    IF @@ERROR <> 0
    BEGIN
        -- Deletion Error
       
        ROLLBACK TRANSACTION
        RAISERROR('%s: (Cleanup) Error deleting from utb_fnol_involved_load', 16, 1, @ProcName)
        RETURN
    END 
     
    DELETE FROM dbo.utb_fnol_claim_coverage_load
      WHERE LynxID = @LynxID
      
    IF @@ERROR <> 0
    BEGIN
        -- Deletion Error
       
        ROLLBACK TRANSACTION
        RAISERROR('%s: (Cleanup) Error deleting from utb_fnol_claim_coverage_load', 16, 1, @ProcName)
        RETURN
    END 

     
    DELETE FROM dbo.utb_fnol_property_load 
      WHERE LynxID = @LynxID
      
    IF @@ERROR <> 0
    BEGIN
        -- Deletion Error
       
        ROLLBACK TRANSACTION
        RAISERROR('%s: (Cleanup) Error deleting from utb_fnol_property_load', 16, 1, @ProcName)
        RETURN
    END 

   
    COMMIT TRANSACTION utrFNOLLoad
    --rollback transaction utrFNOLLoad

    IF @@ERROR <> 0
    BEGIN
        -- SQL Server Error
       
        RAISERROR('%s: SQL Server Error', 16, 1, @ProcName)
        RETURN
    END  

	------------------
	-- Debugging 
	------------------
	IF @Debug = 1
	BEGIN
        PRINT 'Processing Complete'
	END
	ELSE IF @Debug = 2
	BEGIN
		SET @DebugMsg = 'Process possible additional service channels for ME Claim'

		SET @EventSeq += 1

		SET @DebugCmd = '@vProcessingServer = ''UNKNOWN'''
		SET @DebugCmd += ', @vEventTransactionID = ''' + @ProcGUID + ''''
		SET @DebugCmd += ', @vEventType = ''FNOLInscClaim(PROCESS)'''
		SET @DebugCmd += ', @iEventSeq = ' + CONVERT(VARCHAR(5),@EventSeq)
		SET @DebugCmd += ', @vEventStatus = ''PROCESS'''
		SET @DebugMsg += ', @vEventDescription = ''====>>>>>> Processing ended: ' + CONVERT(VARCHAR(50), CURRENT_TIMESTAMP, 121) + ''''
		SET @DebugCmd += ', @vEventDetailedDescription = ''Process possible additional service channels for ME Claim'''
		SET @DebugCmd += ', @vEventXML = ''PROCESS' + @DebugMsg + ''''
		SET @DebugCmd += ', @iRecID = 0'

		--SELECT ('uspFNOLEventInsLogEntry ' + @DebugCmd)
		EXEC ('uspFNOLEventInsLogEntry ' + @DebugCmd)
	END
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspFNOLInsClaim' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspFNOLInsClaim TO 
        ugr_fnolload
    GRANT EXECUTE ON dbo.uspFNOLInsClaim TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There WAS an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/
