-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestInsCSJobAssignment' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestInsCSJobAssignment 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestInsCSJobAssignment
* SYSTEM:       Lynx Services APD
* AUTHOR:       Thomas Duranko
* DATE:			2015Dec03
* FUNCTION:     Inserts a new CS job document the utb_hyperquestsvc_jobs table for processing.
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
* Version: 1.0 - 2015Dec03 - TVD - Initial Build
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestInsCSJobAssignment]
	@vJobID VARCHAR(255)
	, @iInscCompID INT
	, @iLynxID INT
	, @vClaimNumber VARCHAR(30)
	, @iDocumentID BIGINT
	, @iAssignmentID BIGINT
	, @iVehicleID INT = 0 
	, @iShopLocationID INT = 0 
	, @iSeqID INT = 0
	, @dtReceivedDate DATETIME
	, @iDocumentTypeID INT
	, @vDocumentTypeName VARCHAR(50)
	, @vDocumentSource VARCHAR(100)
	, @vDocumentImageType VARCHAR(10)
	, @vDocumentImageLocation VARCHAR(255)
	, @iClaimAspectServiceChannelID BIGINT
	, @iClaimAspectID BIGINT
	, @vJobXSLFile VARCHAR(100)
	, @vJobStatus VARCHAR(25)
	, @vJobStatusDetails VARCHAR(255)
	, @iJobPriority INT
	, @dtJobCreatedDate DATETIME
	, @bEnabledFlag BIT
	, @xJobXML XML
	, @iSysLastUserID INT
AS
BEGIN
    -- Declare local variables
    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts
    DECLARE @ApplicationCD     AS varchar(15)
	DECLARE @dtNow			   AS DATETIME
	DECLARE @vError			   AS VARCHAR(100)
	DECLARE @Debug			   AS BIT
	---------------------------
	DECLARE @iCurrentSeqID					AS INT

    -- Initialize any empty string parameters
    SET @ProcName = 'uspHyperquestInsCSJobAssignment'
    SET @ApplicationCD = 'APDHyperquestPartner'
	SET @dtNow = CURRENT_TIMESTAMP
	SET @iCurrentSeqID = 0
	SET @vError = NULL
	SET @debug = 0

	------------------------------
	-- DEBUG Params
	------------------------------
	IF @debug = 1
	BEGIN
		PRINT '------------------------------------'
		PRINT 'Parameters:'
		PRINT '------------------------------------'
		PRINT '@vJobID = ' + @vJobID
		PRINT '@iInscCompID = ' + Convert(varchar(20), @iInscCompID)
		PRINT '@iLynxID = ' + Convert(varchar(20), @iLynxID)
		PRINT '@vClaimNumber = ' + @vClaimNumber
		PRINT '@iDocumentID = ' + Convert(varchar(20), @iDocumentID)
		PRINT '@iAssignmentID = ' + Convert(varchar(20), @iAssignmentID)
		PRINT '@iVehicleID = ' + Convert(varchar(20), @iVehicleID)
		PRINT '@iShopLocationID = ' + Convert(varchar(20), @iShopLocationID)
		PRINT '@iSeqID = ' + Convert(varchar(20), @iSeqID)
		PRINT '@dtReceivedDate = ' + @dtReceivedDate
		PRINT '@iDocumentTypeID = ' + Convert(varchar(20), @iDocumentTypeID)
		PRINT '@vDocumentTypeName = ' + @vDocumentTypeName
		PRINT '@vDocumentSource = ' + @vDocumentSource
		PRINT '@vDocumentImageType = ' + @vDocumentImageType
		PRINT '@vDocumentImageLocation = ' + @vDocumentImageLocation
		PRINT '@iClaimAspectServiceChannelID = ' + Convert(varchar(20), @iClaimAspectServiceChannelID)
		PRINT '@iClaimAspectID = ' + Convert(varchar(20), @iClaimAspectID)
		PRINT '@vJobXSLFile = ' + @vJobXSLFile
		PRINT '@vJobStatus = ' + @vJobStatus
		PRINT '@vJobStatusDetails = ' + @vJobStatusDetails
		PRINT '@iJobPriority = ' + Convert(varchar(20), @iJobPriority)
		PRINT '@dtJobCreatedDate = ' + @dtJobCreatedDate
		PRINT '@bEnabledFlag = ' + Convert(varchar(20), @bEnabledFlag)
		PRINT '@xJobXML = ' + Convert(varchar(1000), @xJobXML)
		PRINT '@iSysLastUserID = ' + Convert(varchar(20), @iSysLastUserID)
	END    
	
	------------------------------
	-- Process the ShopAssignment
	------------------------------
	IF (@iDocumentTypeID = 22)  -- CS can only do 22 = ShopAssignment.  No Cancels
	BEGIN
		------------------------------
		-- Validate - Shop Assignment
		------------------------------
		-- Validate we have a good Unprocessed Assignment
		IF EXISTS (
			SELECT *
				, a.ShopLocationID
				, a.CommunicationMethodID
				, a.AssignmentSuffix
				, a.AssignmentDate
				, a.CancellationDate
				, a.SelectionDate
			FROM 
				utb_claim_aspect ca
				INNER JOIN utb_claim_aspect_service_channel casc
				ON casc.ClaimAspectID = ca.ClaimAspectID
				INNER JOIN utb_assignment a
				ON a.ClaimAspectServiceChannelID = casc.ClaimAspectServiceChannelID
				INNER JOIN utb_hyperquestsvc_jobs hj
				ON hj.LynxID = ca.LynxID
			WHERE 
				ca.LynxID = 1808600
				AND casc.ClaimAspectServiceChannelID = @iClaimAspectServiceChannelID 
				AND ca.InitialAssignmentTypeID = 17
				AND a.AssignmentDate IS NULL
				AND a.CancellationDate IS NULL
				AND hj.JobStatus <> 'Unprocessed'
		)
		BEGIN
			-- Not Assigned or Already processed
			PRINT 'Not Assigned or Already processed'
		END
		ELSE 
		BEGIN
			-- Good to process
			BEGIN TRAN utrInsertCSJobAssignment
			INSERT INTO
				utb_hyperquestsvc_jobs
				(
				  [JobID]
				  , [InscCompID]
				  , [LynxID]
				  , [ClaimNumber]
				  , [DocumentID]
				  , [AssignmentID]
				  , [VehicleID]
				  , [ShopLocationID]
				  , [SeqID]
				  , [ReceivedDate]
				  , [DocumentTypeID]
				  , [DocumentTypeName]
				  , [DocumentSource]
				  , [DocumentImageType]
				  , [DocumentImageLocation]
				  , [ClaimAspectServiceChannelID]
				  , [ClaimAspectID]
				  , [JobXSLFile]
				  , [JobStatus]
				  , [JobStatusDetails]
				  , [JobPriority]
				  , [JobCreatedDate]
				  , [JobProcessedDate]
				  , [JobTransformedDate]
				  , [JobTransmittedDate]
				  , [JobArchivedDate]
				  , [JobFinishedDate]
				  , [EnabledFlag]
				  , [JobXML]
				  , [SysLastUserID]
				  , [SysLastUpdatedDate]
				)
				VALUES
				(
				  @vJobID
				  , @iInscCompID
				  , @iLynxID
				  , @vClaimNumber
				  , @iDocumentID
				  , @iAssignmentID
				  , @iVehicleID
				  , @iShopLocationID
				  , @iCurrentSeqID
				  , @dtReceivedDate
				  , @iDocumentTypeID
				  , @vDocumentTypeName
				  , @vDocumentSource
				  , @vDocumentImageType
				  , @vDocumentImageLocation
				  , @iClaimAspectServiceChannelID
				  , @iClaimAspectID
				  , @vJobXSLFile
				  , @vJobStatus
				  , @vJobStatusDetails
				  , @iJobPriority
				  , @dtJobCreatedDate
				  , NULL
				  , NULL
				  , NULL
				  , NULL
				  , NULL
				  , @bEnabledFlag
				  , @xJobXML
				  , @iSysLastUserID
				  , @dtNow
				)

				IF @@ERROR <> 0
				BEGIN
					-- Insert Failure
					ROLLBACK TRANSACTION
				    SET @vError = '(Hyperquest CS Job Insert) Error inserting the new CS Job into utb_hyperquestsvc_jobs.  @@ERROR: ' + CONVERT(VARCHAR(15), @@ERROR)
					RAISERROR('100|%s: %s ', 16, 1, @ProcName, @vError)
			    SET @vError = 'Failed: ' + @vError
				END
				ELSE
				BEGIN
					COMMIT TRANSACTION utrInsertCSJobAssignment

					IF @@ERROR <> 0
					BEGIN
						-- SQL Server Error
						SET @vError = '(Hyperquest CS Job Insert) SQL Server Error during new CS Job insert Commit.  @@ERROR: ' + CONVERT(VARCHAR(15), @@ERROR)
						RAISERROR('101|%s: %s ', 16, 1, @ProcName, @vError)
					END    
			    SET @vError = 'SUCCESSFUL'
				END
		END				
	END
	ELSE
	BEGIN
			-- Bad Assignment Type
		    SET @vError = '(Hyperquest CS Job Insert) Error Bad Assignment Type.  @iAssignmentID: ' + CONVERT(VARCHAR(10), @iAssignmentID)
			RAISERROR('102|%s: %s ', 16, 1, @ProcName, @vError)
	END

	SELECT @vError
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestInsCSJobAssignment' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspHyperquestInsCSJobAssignment TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/