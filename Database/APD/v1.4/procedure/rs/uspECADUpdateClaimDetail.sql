-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspECADUpdateClaimDetail' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspECADUpdateClaimDetail 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


/************************************************************************************************************************
*
* PROCEDURE:    uspECADUpdateClaimDetail
* SYSTEM:       Lynx Services APD
* AUTHOR:       Jonathan Perrigo
* FUNCTION:     Allows users to updates certain critical pieces of claim information from ClaimPoint.  This procedure
*               will log a note informing the claim rep of the change
*
* PARAMETERS:  
* (I) @ClaimAspectID        The Claim Aspect to update
* (I) @ClientClaimNumber    The client claim number
* (I) @LossDate             The loss date
* (I) @LossState            The loss state
* (I) @ClientCoverageTypeID The client coverage type
* (I) @UserID               The user making the changes
*
* RESULT SET:
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure


CREATE PROCEDURE dbo.uspECADUpdateClaimDetail
    @ClaimAspectID          udt_std_id_big,
    @ClientClaimNumber      udt_cov_claim_number,
    @LossDate               udt_std_datetime,
    @LossState              udt_addr_state,
    @ClientCoverageTypeID   udt_std_id,
    @UserID                 udt_std_id
AS
BEGIN
    -- Initialize any empty string parameters

    IF LEN(RTRIM(LTRIM(@ClientClaimNumber))) = 0 SET @ClientClaimNumber = NULL


    -- Declare internal variables

    DECLARE @ClientCovTypeDesc  udt_std_name
    DECLARE @InsuranceCompanyID udt_std_id
    DECLARE @LynxID             udt_std_id
    DECLARE @Note               udt_std_desc_mid
    DECLARE @NoteTypeIDClaim    udt_std_id
    DECLARE @StatusID           udt_std_id
    DECLARE @VehicleNumber      udt_std_int
    
    DECLARE @now        AS datetime

    DECLARE @ProcName          AS varchar(30)       -- Used for raise error stmts 

    SET @ProcName = 'uspECADUpdateClaimDetail'


    -- Set Database options
    
    SET NOCOUNT ON
    
    
    -- Make sure a valid claim aspect id was passed in
    
    IF (@ClaimAspectID IS NULL) OR 
       (NOT EXISTS (SELECT ClaimAspectID FROM dbo.utb_Claim_Aspect WHERE ClaimAspectID = @ClaimAspectID))
    BEGIN
        --Invalid ClaimAspectID

        RAISERROR('101|%s|@ClaimAspectID|%u', 16, 1, @ProcName, @ClaimAspectID)
        RETURN
    END


    -- Check to make sure a valid User id was passed in

    IF  (@UserID IS NULL) OR
        (NOT EXISTS(SELECT UserID FROM dbo.utb_user WHERE UserID = @UserID))
    BEGIN
        -- Invalid User ID
    
        RAISERROR('101|%s|@UserID|%u', 16, 1, @ProcName, @UserID)
        RETURN
    END


    -- Check to make sure none of the other parameters are NULL
    
    IF (@ClientClaimNumber IS NULL) OR
       (@LossDate IS NULL) OR
       (@LossDate = '1/1/1900 12:00:00 AM') OR
       (@LossState IS NULL) OR
       (NOT EXISTS(SELECT StateCode FROM dbo.utb_state_code WHERE StateCode = @LossState)) OR
       (@ClientCoverageTypeID IS NULL) OR
       (NOT EXISTS(SELECT ClientCoverageTypeID FROM dbo.utb_client_coverage_type WHERE ClientCoverageTypeID = @ClientCoverageTypeID))
    BEGIN
        -- One of the data points was not passed in or is invalid
    
        RAISERROR('%s: @ClientClaimNumber, @LossDate, @LossState, and @ClientCoverageTypeID must be valid values', 16, 1, @ProcName)
        RETURN
    END
  
  
    -- Validate APD Data state
    
    SELECT  @NoteTypeIDClaim  = NoteTypeID 
      FROM  dbo.utb_note_type
      WHERE Name = 'Claim'
      
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END

    IF @NoteTypeIDClaim IS NULL
    BEGIN
        -- Note Type Not Found
    
        RAISERROR('102|%s|"Claim"|utb_note_type', 16, 1, @ProcName)
        RETURN
    END
    
                  
    -- Get the claim aspect id information needed for use later
    
    SELECT  @LynxID             = ca.LynxID,
            @StatusID           = cas.StatusID, --Project: 210474 APD - Enhancements to support multiple concurrent service channels.  The value now comes from utb_Claim_Aspect_Status instead of utb_Claim_Aspect table.  M.A. 20061109
            @VehicleNumber      = ca.ClaimAspectNumber,
            @InsuranceCompanyID = c.InsuranceCompanyID
      FROM  dbo.utb_claim_aspect ca
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	"StatusID" now comes from utb_Claim_Aspect_Status instead on utb_Claim_Aspect.
		Joined the table to utb_Claim_Aspect to get the StatusID value for ClaimAspectID
		M.A. 20061109
	*********************************************************************************/
      INNER JOIN utb_Claim_Aspect_Status cas on cas.ClaimAspectID = ca.ClaimAspectID
      LEFT JOIN dbo.utb_claim c ON (ca.LynxID = c.LynxID)
      WHERE ca.ClaimAspectID = @ClaimAspectID
      
    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
            

    -- Get client coverage type information, verifying the type id is for this insurance company
    
    SELECT  @ClientCovTypeDesc = Name
      FROM  dbo.utb_client_coverage_type
      WHERE ClientCoverageTypeID = @ClientCoverageTypeID
        AND InsuranceCompanyID = @InsuranceCompanyID            

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END
    
    IF @ClientCovTypeDesc IS NULL
    BEGIN
        -- The Client Coverage Type is invalid for this insurance company
    
        RAISERROR('%s: @ClientCoverageTypeID %u is invalid for InsuranceCompany %u', 16, 1, @ProcName, @ClientCoverageTypeID, @InsuranceCompanyID)
        RETURN
    END
    

    -- Create text that will be inserted as a note
    
    SET @Note = 'From ClaimPoint I updated claim data for vehicle ' + Convert(varchar(3), @VehicleNumber) + '; Claim Number set to ' 
                    + @ClientClaimNumber + ', Loss Date set to ' + Convert(varchar(12), @LossDate, 101) + ', Loss State set to ' 
                    + @LossState + ', and Coverage Type set to ' + @ClientCovTypeDesc
                    
                    
    -- Get current timestamp

    SET @now = CURRENT_TIMESTAMP


    BEGIN TRANSACTION ECADUpdateClaimTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    -- Check to make sure a claim coverage record exists
    
    IF NOT EXISTS(SELECT * FROM dbo.utb_claim_coverage WHERE LynxID = @LynxID)
    BEGIN
        -- We need to create a claim coverage record
        
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	There might be a need to insert more data to complete a rowset in the 
		table below.
		Talked to Jorge and we are going to revisit issues like this when we start
		testing	the application.
		M.A. 20061109
	*********************************************************************************/  
        INSERT INTO dbo.utb_claim_coverage (
					LynxID, 
					--ClientClaimNumber, --Project: 210474 APD - Enhancements to support multiple concurrent service channels.  ClientClaimNumber column removed.  It should already exist in utb_Claim table.  M.A. 20061109
					SysLastUserID, 
					sysLastUpdatedDate
					)
            VALUES 			(
					@LynxID, 
					--@ClientClaimNumber, --Project: 210474 APD - Enhancements to support multiple concurrent service channels.  Please see the note above.  M.A. 20061109
					@UserID, 
					@now
					)
            
        IF @@ERROR <> 0
        BEGIN
            -- Insertion failure
    
            RAISERROR('105|%s|utb_claim_coverage', 16, 1, @ProcName)
            ROLLBACK TRANSACTION 
            RETURN
        END
    END
    ELSE
    BEGIN
        -- Update the existing claim coverage record
	/*********************************************************************************
	Project: 210474 APD - Enhancements to support multiple concurrent service channels
	Note:	There might be a need to update more data in the table below.
		Talked to Jorge and we are going to revisit issues like this when we start
		testing	the application.
		M.A. 20061109
	*********************************************************************************/            
        UPDATE  utb_claim_coverage
          SET   --ClientClaimNumber = @ClientClaimNumber, --Project: 210474 APD - Enhancements to support multiple concurrent service channels.  ClientClaimNumber column removed.  It should already exist in utb_Claim table.  M.A. 20061109
                SysLastUserId = @UserID,
                SyslastUpdatedDate = @now
          WHERE LynxID = @LynxID
          
        IF @@ERROR <> 0
        BEGIN
            -- Update failure

            RAISERROR('104|%s|utb_claim_coverage', 16, 1, @ProcName)
            ROLLBACK TRANSACTION
            RETURN
        END
    END
    
    
    -- Update the claim record
    
    UPDATE  utb_claim
      SET   LossDate = @LossDate,
            LossState = @LossState,
            SysLastUserId = @UserID,
            SyslastUpdatedDate = @now
      WHERE LynxID = @LynxID
      
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    
    -- Update the claim aspect record
    
    UPDATE  utb_claim_aspect
      SET   ClientCoverageTypeID = @ClientCoverageTypeID,
            SysLastUserId = @UserID,
            SyslastUpdatedDate = @now      
      WHERE ClaimAspectID = @ClaimAspectID
      
    IF @@ERROR <> 0
    BEGIN
        -- Update failure

        RAISERROR('104|%s|utb_claim', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END

    
    -- Insert a note in the claim file to document the change for the claim rep
    
    exec uspNoteInsDetail   @ClaimAspectID = @ClaimAspectID,
                            @NoteTypeID = @NoteTypeIDClaim,
                            @StatusId = @StatusID,
                            @Note = @Note,
                            @UserID = @UserID
                            
    IF @@ERROR <> 0
    BEGIN
        -- insert failure

        RAISERROR  ('%s: Error executing uspNoteinsDetail', 16, 1, @ProcName)
        ROLLBACK TRANSACTION
        RETURN
    END 

    
    COMMIT TRANSACTION WorkflowUpdateStatusTran1

    IF @@ERROR <> 0
    BEGIN
       -- SQL Server Error
    
        RAISERROR('99|%s', 16, 1, @ProcName)
        RETURN
    END


    RETURN                  
END


GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspECADUpdateClaimDetail' AND type = 'P')
BEGIN
    GRANT EXECUTE ON dbo.uspECADUpdateClaimDetail TO 
        ugr_lynxapd

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/