-- Begin the transaction for dropping and creating the stored procedure

BEGIN TRANSACTION


-- If it already exists, drop the procedure

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestEventInsLogEntry' AND type = 'P')
BEGIN
    DROP PROCEDURE dbo.uspHyperquestEventInsLogEntry 
END

GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/************************************************************************************************************************
*
* PROCEDURE:    uspHyperquestEventInsLogEntry
* SYSTEM:       Lynx Services Hyperquest Partner
* AUTHOR:       Thomas Duranko
* FUNCTION:     Inserts new log entries into the utb_hyperquest_event_lod
* Date:			2014Oct21
*
* PARAMETERS:  
*
* RESULT SET:
* [result set details here]
*
*
* VSS
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
*
************************************************************************************************************************/

-- Create the stored procedure

CREATE PROCEDURE [dbo].[uspHyperquestEventInsLogEntry]
	@vProcessingServer VARCHAR(100)
	, @vEventTransactionID VARCHAR(100)
	, @vEventType VARCHAR(50)
	, @iEventSeq INT
	, @vEventStatus VARCHAR(150)
	, @vEventDescription VARCHAR(150)
	, @vEventDetailedDescription VARCHAR(1000)
	, @vEventXML TEXT
	, @iRecID INT OUTPUT
AS
BEGIN
    -- Declare internal variables
    
    DECLARE @ProcName AS varchar(30)       -- Used for raise error stmts
    SET @ProcName = 'uspHyperquestEventInsLogEntry'

    DECLARE @error AS int
    DECLARE @rowcount AS int
	DECLARE @vErrorMessage VARCHAR(100)
    SET @vErrorMessage = ''

    DECLARE @now AS datetime 
	SET @now = CURRENT_TIMESTAMP
	
	SET @iRecID = 0

    -- Set Database options
    
    SET NOCOUNT ON

	BEGIN TRANSACTION EventLogInsTran1
		IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
			SET @vErrorMessage = @ProcName + ' Transaction EventLogInsTran1 Failed'
	        RAISERROR('SQL-ERROR-100|%s|%u', 16, 1, @vErrorMessage, @@ERROR)
			SET @iRecID = 100
		END
		
		INSERT INTO
			utb_hyperquest_event_log
		(
			ProcessingServer
			, EventTransactionID
			, EventType
			, EventSeq
			, EventStatus
			, EventDescription
			, EventDetailedDescription
			, EventXML		
			, SysLastUserID
			, SysLastUpdatedDate
		)		
		VALUES
		(
			@vProcessingServer
			, @vEventTransactionID
			, @vEventType
			, @iEventSeq
			, @vEventStatus
			, @vEventDescription
			, @vEventDetailedDescription
			, @vEventXML		
			, 0
			, @now
		)

		IF @@ERROR <> 0
		BEGIN
			-- Insertion failure
			SET @vErrorMessage = @ProcName + ' Row insert failed and transaction rolled back'
	        RAISERROR('SQL-ERROR-101|%s|%u', 16, 1, @vErrorMessage, @@ERROR)
			ROLLBACK TRANSACTION
			SET @iRecID = 101
		END

		COMMIT TRANSACTION EventLogInsTran1
		IF @@ERROR <> 0
		BEGIN
		   -- SQL Server Error
			SET @vErrorMessage = @ProcName + ' Transaction EventLogInsTran1 COMMIT Failed'
	        RAISERROR('SQL-ERROR-102|%s|%u', 16, 1, @vErrorMessage, @@ERROR)
			SET @iRecID = 102
		END

	--SELECT @iRecID 
END
GO

-- Permissions

IF EXISTS (SELECT name FROM sysobjects 
            WHERE name = 'uspHyperquestEventInsLogEntry' AND type = 'P')
BEGIN
--    GRANT EXECUTE ON dbo.uspHyperquestEventInsLogEntry TO 
--        ugr_lynxapd
    GRANT EXECUTE ON dbo.uspHyperquestEventInsLogEntry TO 
        wsAPDUser

    -- Commit the transaction for dropping and creating the stored procedure

    COMMIT
END
ELSE
BEGIN
    -- There was an error...rollback the transaction for dropping and creating the stored procedure

    RAISERROR ('Stored procedure creation failure.', 16, 1)
    ROLLBACK
END

GO


/************************************************************************************************************************
*
* VSS HISTORY
*
* $Workfile: $
* $Archive:  $
* $Revision: $
* $Author:   $
* $Date:     $
* $History:  $
*
************************************************************************************************************************/